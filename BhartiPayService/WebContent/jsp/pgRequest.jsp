<%@page import="com.bhartipay.util.ChecksumUtils"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Set"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.transaction.bean.PGAggregatorBean"%>
<%@page import="com.bhartipay.util.persistence.CommanUtilDaoImpl"%>
<%@page import="com.bhartipay.util.bean.WalletConfiguration"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDao"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDaoImpl"%>
<%@page import="com.bhartipay.config.pg.PGDetails"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.InputStream"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<%!
HttpServletRequest request;
HttpSession session;
String responseparams="";
String shippingDtls ="";
String billingDtls="";
String mid="";
String requestparams="";	
String SubmitTo="";
String result="";
String pgTxnId="";
String pgPaymentUrl="";

boolean flag=false;
String trxId ="";
double serviceCharge=0.0;
double pgAmount=0.0;
TransactionDao transactionDao=new TransactionDaoImpl();
Map<String,String> pgpayementPageParams=new HashMap<String,String>();
final Logger LOGGER=Logger.getLogger(this.getClass());
%>

<%
System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~aggreatorid Id~~~~~~~~~~~~~~~~~~~~~~~~~~"+request.getParameter("aggreatorid"));
WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(request.getParameter("aggreatorid").toString());

result=transactionDao.savePGTrx(Double.parseDouble(request.getParameter("txnAmount")), request.getParameter("walletId"), request.getParameter("mobileNumber"), request.getParameter("txnReason"), request.getParameter("emailId"),request.getParameter("appName"),request.getParameter("imeiIP"),request.getParameter("userId"),request.getParameter("aggreatorid"));
//String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
//System.out.println("__________________result______________"+result);

PGAggregatorBean bean = transactionDao.getPGAGGId(request.getParameter("aggreatorid"));
 
if(result.contains("1000|")){
	 
	 	pgTxnId = result.substring(result.indexOf("|") + 1);
		int amount = ((int)Double.parseDouble(request.getParameter("txnAmount")) * 100);
		String dicemalAmount= ""+amount;
		
		
		
		String orderId=pgTxnId;
		String pay_id=bean.getMid();
		String salt=bean.getTransactionkey();
		String name=request.getParameter("userId");
		String mail=request.getParameter("emailId");
		String contactNumber=request.getParameter("mobileNumber");
		String returnurl=bean.getDomainname()+"/BhartiPayService/jsp/pgResponse.jsp";
		//String returnurl = "http://localhost:8080/BhartiPayService/jsp/pgResponse.jsp";
		
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("AMOUNT", dicemalAmount);
		map.put("CURRENCY_CODE", "356");
		map.put("CUST_EMAIL", mail.toUpperCase());
		map.put("CUST_NAME", name.toUpperCase());
		map.put("CUST_PHONE", contactNumber);
		map.put("CUST_STREET_ADDRESS1", "Bhartipay Services");
		map.put("CUST_ZIP", "201304");
		map.put("MERCHANTNAME", "Bhartipay");
		map.put("ORDER_ID", orderId);
		map.put("PAY_ID", pay_id);
		map.put("PRODUCT_DESC", "NA");
		map.put("RETURN_URL", returnurl);
		map.put("TXNTYPE", "SALE");
		
		pgpayementPageParams=map;
		
		String  hash=null;
		
		try {
			hash = ChecksumUtils.generateCheckSum(map,salt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		pgpayementPageParams.put("HASH",hash);
	  //  pgPaymentUrl="https://merchant.bhartipay.com/crm/jsp/paymentrequest";
	  pgPaymentUrl = bean.getPgurl();
	  System.out.print(pgPaymentUrl);
	    flag=true;

	} else {
		responseparams = pgTxnId + "|FAIL|||FAIL";
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + responseparams);
		flag = false;
	}

/* if(result.equalsIgnoreCase("1001")){
	addActionError("There is some problem please try again.");
	return "fail";
}

if(result.equalsIgnoreCase("7023")){
	addActionError("Invalid mobile no.");
	return "fail";
}
if(result.equalsIgnoreCase("7014")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
	return "fail";
}
if(result.equalsIgnoreCase("7017")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
	return "fail";
}
if(result.equalsIgnoreCase("7018")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
	return "fail";
}
if(result.equalsIgnoreCase("7019")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
	return "fail";
}
if(result.equalsIgnoreCase("7020")){
	addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
	return "fail";
}
if(result.equalsIgnoreCase("7021")){
	addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
	return "fail";
}
if(result.equalsIgnoreCase("7022")){
	addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
	return "fail";
}
if(result.equalsIgnoreCase("7045")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
	return "fail";
}
if(result.equalsIgnoreCase("7046")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
	return "fail";
}
if(result.equalsIgnoreCase("7047")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
	return "fail";
}
if(result.equalsIgnoreCase("7048")){
	addActionError("Transaction failed, Wallet balance violated.");
	return "fail";
}
if(result.equalsIgnoreCase("7000")){
	addActionError("Transaction fail.");
	return "fail";
}
if(result.equalsIgnoreCase("7023")){
	addActionError("Invalid reciver mobile number.");
	return "fail";
}
if(result.equalsIgnoreCase("7024")){
	addActionError("Transaction failed due to insufficient balance.");
	return "fail";
}
if(result.equalsIgnoreCase("7042")){
	addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
	return "fail";
} */
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript">
	function showAndroid(txnId) {
		try{
			Bhartipay.getTransactionId(txnId);
		}catch(e){
			
		}
		
	}
</script>
</head>

<%
LOGGER.debug("**********************************************flag************************  "+flag);
if(flag){
%>

<body onload="showAndroid('<%=pgTxnId%>');document.createElement('form').submit.call(document.getElementById('myForm'))">
<form name="ecom" method="post" name="pgWallet"  action="<%=pgPaymentUrl%>" id="myForm">
		<%
		Set<Entry<String,String>> e = pgpayementPageParams.entrySet();
		Iterator<Entry<String,String>> ite= e.iterator();
		while(ite.hasNext()) {
					Entry<String, String> entry = ite.next();
					LOGGER.debug(entry.getKey()+"-----------"+entry.getValue());
		%>
			<input type="hidden" name="<%=entry.getKey()%>" value="<%=entry.getValue()%>">
		<%
			}
		%>
		
 </form>

<%}else{
	LOGGER.debug("**********************************************flag********* false***************  "+flag);
	LOGGER.debug("*************************************************responseparams************************"+responseparams);
	
	%>
	
<script type="text/javascript">
	function showAndroid(result) {
		Bhartipay.success(result);
	}
</script>
<body onload="showAndroid('<%=responseparams%>')">
<form name="frm" method="post" id="myForm"> 
<input type="hidden" name="responseparams" value="<%=responseparams%>">
</form>


<% }%>


</body>
</html>
</html>
