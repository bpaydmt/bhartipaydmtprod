<%@page import="com.bhartipay.util.thirdParty.EncryptionByEnc256"%>
<%@page import="com.bhartipay.util.thirdParty.ShippingDtls"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.util.Properties"%>
<%@page import="com.bhartipay.user.bean.UserProfileBean"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDaoImpl"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDao"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>



<%!
HttpServletRequest request;
TransactionDao transactionDao=new TransactionDaoImpl();
UserProfileBean UserProfileBean=new UserProfileBean();
%>

<%
String responseparams = request.getParameter("responseparams");
String hash=request.getParameter("hash");
Logger logger = Logger.getLogger(this.getClass().getName());
logger.debug("********************************************** response come from Payment gateway = "	+ responseparams);
if (responseparams == null) {
	logger.debug("********************************************** getting resp Parameter as null so flight booking could not be done   ****************************************");
} else {
	String method = request.getMethod();
	//status=Success|responseCode=01|plutusTxnId=2001609000468575|appTransId=PGTX001121|appUserId=sandeep|amount=10.00|appName=xyz
	StringTokenizer tokenizer = new StringTokenizer(responseparams, "|");
	String status = tokenizer.nextToken().substring(7);
	logger.debug("********************************************** response come from Payment gateway status = "	+ status);
	String id = tokenizer.nextToken();
	String pgtxn = tokenizer.nextToken().substring(12);
	String txnid = tokenizer.nextToken().substring(11);
	String appUserId = tokenizer.nextToken().substring(11);
	String amount = tokenizer.nextToken();
	 amount = amount.substring(7,amount.indexOf('.'));
	String appName = tokenizer.nextToken().substring(8);
	 String txnResult= transactionDao.updatePGTrxFlight(hash,responseparams,txnid,pgtxn,Double.parseDouble(amount),status);


	 responseparams= txnResult.toString();
	 
	 logger.debug("********************************************** response come from BhartiPay = "	+ responseparams);
}

%> 


<%-- <%
String result = "fail";
String msg = "";
Logger logger = Logger.getLogger(this.getClass().getName());
String txn_response = request.getParameter("txn_response");
String pg_details = request.getParameter("pg_details");
String fraud_details = request.getParameter("fraud_details");
String other_details = request.getParameter("other_details");
logger.debug("**********************************************txn_response:-"	+ txn_response);
logger.debug("**********************************************pg_details:-"		+ pg_details);
logger.debug("**********************************************fraud_details:-"		+ fraud_details);
logger.debug("**********************************************other_details:-"		+ other_details);

if (txn_response != null) {
	txn_response = decryptString(txn_response, "");
}
if (pg_details != null) {
	pg_details = decryptString(pg_details, "");
}
if (fraud_details != null) {
	fraud_details = decryptString(fraud_details, "");
}
if (other_details != null) {
	other_details = decryptString(other_details, "");
}
logger.debug("**********************************************txn_response:-"		+ txn_response);
logger.debug("**********************************************pg_details:-"		+ pg_details);
logger.debug("**********************************************fraud_details:-"		+ fraud_details);
logger.debug("**********************************************other_details:-"		+ other_details);

String ag_id = "";
String me_id = "";
String order_no = "";
String amount = "";
String country = "";
String currency = "";
String txn_date = "";
String txn_time = "";
String ag_ref = "";
String pg_ref = "";
String statusfrompaygate = "";
String res_code = "";// 0 for Success.
String res_message = "";
StringTokenizer token = new StringTokenizer(txn_response, "|");
if (token.hasMoreElements())
	ag_id = token.nextToken();
if (token.hasMoreElements())
	me_id = token.nextToken();
if (token.hasMoreElements())
	order_no = token.nextToken();
if (token.hasMoreElements())
	amount = token.nextToken();
if (token.hasMoreElements())
	country = token.nextToken();
if (token.hasMoreElements())
	currency = token.nextToken();
if (token.hasMoreElements())
	txn_date = token.nextToken();
if (token.hasMoreElements())
	txn_time = token.nextToken();
if (token.hasMoreElements())
	ag_ref = token.nextToken();
if (token.hasMoreElements())
	pg_ref = token.nextToken();
if (token.hasMoreElements())
	statusfrompaygate = token.nextToken();
if (token.hasMoreElements())
	res_code = token.nextToken();
if (token.hasMoreElements())
	res_message = token.nextToken();


String authorisationStatus = statusfrompaygate;
if (authorisationStatus.indexOf("Success") > -1) {
	msg = "Amount successfully added to your wallet.";
	} else if (authorisationStatus.indexOf("Failed") > -1) {
	msg = "Transaction failed.";

}




String txnResult= transactionDao.updatePGTrx(order_no,pg_ref,Double.parseDouble(amount),msg);
String responseparams= txnResult.toString();


%> --%>

<html>
<head>
<script type="text/javascript">
	function showAndroid(result) {
		Bhartipay.success(result);
	}
</script>
</head>
<body onload="showAndroid('<%=responseparams%>')">
	
</body>
</html>

<%!
public String decryptString(String encString,String secureKey){
	
	Properties prop = new Properties();
	InputStream in = ShippingDtls.class.getResourceAsStream("PayGate.properties");
	try {
		prop.load(in);
		in.close();
	} catch (Exception e) {
		System.out.println(e);
		e.printStackTrace();
	}
	secureKey=prop.getProperty("EncKey");
	String decryptString=new String();
	try{
	decryptString=EncryptionByEnc256.decrypt(encString,secureKey);
	}catch(Exception e){
		e.printStackTrace();
	}
	return decryptString;
}
%>
