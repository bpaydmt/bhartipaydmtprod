<%@page import="com.bhartipay.airTravel.persistence.AirTravelDaoImpl"%>
<%@page import="com.bhartipay.airTravel.persistence.AirTravelDao"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.ArrayList" %>
<%@page import="com.bhartipay.airTravel.bean.AirFareBean" %>
<%@page import="com.bhartipay.util.persistence.CommanUtilDaoImpl"%>
<%@page import="com.bhartipay.util.bean.WalletConfiguration"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDao"%>
<%@page import="com.bhartipay.transaction.persistence.TransactionDaoImpl"%>
<%@page import="com.bhartipay.config.pg.PGDetails"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.InputStream"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<%!
HttpServletRequest request;
HttpSession session;
String responseparams="";
String shippingDtls ="";
String billingDtls="";
String mid="";
String requestparams="";
String SubmitTo="";


boolean flag=false;
String trxId ="";
double serviceCharge=0.0;

TransactionDao transactionDao=new TransactionDaoImpl();
final Logger LOGGER=Logger.getLogger(this.getClass());
AirTravelDao airTravelDao = new AirTravelDaoImpl();
List<AirFareBean> airFareList = new ArrayList<AirFareBean>();
%>

<%
Properties prop = new Properties();
double pgAmt=0.0;
double walletAmt=0.0;
double availWalletBal=0.0;
double totalAmt=0.0;
double tableWalletAmt=0.0;
double tablePGAmt=0.0;
double totalRefundAmt=0.0;
String[] airFareId=new String[5];
System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~aggreatorid Id~~~~~~~~~~~~~~~~~~~~~~~~~~"+request.getParameter("aggreatorid"));
WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(request.getParameter("aggreatorid").toString());
InputStream in = PGDetails.class.getResourceAsStream("WalletPaymentGateway.properties");
prop.load(in);
in.close();

//total Amount validation
 airFareList = airTravelDao.getFareQuoteByTripId(request.getParameter("TravelId").toString(), "");
 
for(int i=0 ; i<airFareList.size();i++)
{
	totalAmt += airFareList.get(i).getTotalFare();
	airFareId[i]=airFareList.get(i).getAirFareId();
	totalRefundAmt+=  airFareList.get(i).getTotalRefundAmt();
}


if(Double.parseDouble(request.getParameter("txnAmount")) == totalAmt)
{

if (request.getParameter("IswalletUsed").toString().trim().equalsIgnoreCase("true")){
	//pg amount
	//wallet amount
	availWalletBal = transactionDao.getWalletBalance(request.getParameter("walletId"));
	if(availWalletBal >= (totalAmt-(walletAmt+airFareList.get(0).getPgAmt()+airFareList.get(0).getWalletAmt())))
	{
		walletAmt=totalAmt-(walletAmt+airFareList.get(0).getPgAmt()+airFareList.get(0).getWalletAmt());
	}
	else
	{
		walletAmt=availWalletBal;
	}
	tableWalletAmt=airFareList.get(0).getWalletAmt()+walletAmt;
	pgAmt = totalAmt - (walletAmt+airFareList.get(0).getPgAmt()+airFareList.get(0).getWalletAmt())+(airFareList.get(0).getPgAmtRefund()+airFareList.get(0).getWalletAmtRefund());
	tablePGAmt = pgAmt + airFareList.get(0).getPgAmt();
}
else
{
	pgAmt= totalAmt - (airFareList.get(0).getPgAmt() + airFareList.get(0).getWalletAmt())+walletAmt+airFareList.get(0).getPgAmtRefund()+airFareList.get(0).getWalletAmtRefund();
	tablePGAmt = pgAmt + airFareList.get(0).getPgAmt();
	tableWalletAmt=airFareList.get(0).getWalletAmt();
}
LOGGER.info("-------------------------------------------------------------------- totalRefundAmt ##"+totalRefundAmt);

if(pgAmt > 0)
{
	
if(Double.parseDouble(request.getParameter("pgAmount")) == pgAmt)
{
if(pgAmt > totalRefundAmt)
{
String pgTxnId1=transactionDao.savePGTrxFlight(request.getParameter("TravelId"),pgAmt, request.getParameter("walletId"), request.getParameter("mobileNumber"), request.getParameter("txnReason"), request.getParameter("emailId"),request.getParameter("appName"),request.getParameter("imeiIP"),request.getParameter("userId"),request.getParameter("aggreatorid"));
String pgTxnId = pgTxnId1.substring(5);
if(airFareList.get(0).getPgTxn() != null && !airFareList.get(0).getPgTxn().trim().equals(""))
{
	pgTxnId = airFareList.get(0).getPgTxn()+","+pgTxnId;
}
	

int flag1=airTravelDao.updateFareQuote(totalAmt, -1,-1,-1,-1, "", request.getParameter("mobileNumber"),request.getParameter("walletId"),"","", request.getParameter("TravelId"));

if(pgTxnId1.contains("1000|")){
	String callBackURL=walletConfiguration.getProtocol()+"://"+walletConfiguration.getDomainName()+"/BhartiPayService/jsp/travelPGResponse.jsp";
	System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~callBackURL~~~~~~~~~~~~~~~~~~~~~~~~~~"+callBackURL);	
	
	flag=true;
pgTxnId1=pgTxnId1.substring(pgTxnId1.indexOf("|")+1);

mid=walletConfiguration.getPgMid();
String appname = walletConfiguration.getPgAgId();
String appuser = walletConfiguration.getPgAgId();
SubmitTo=walletConfiguration.getPgUrl();
//String callBackURL = prop.getProperty("successURL");
String mobile = request.getParameter("mobileNumber");
String email = request.getParameter("emailId");
Double amount=pgAmt;//Double.parseDouble(request.getParameter("txnAmount"));
String key =walletConfiguration.getPgEncKey();

LOGGER.info("############################################################################## Amount ##"+pgAmt);
requestparams="Appname="+appname+"|TransID="+pgTxnId1+"|Amount="+amount+"|Appuser="+appuser+"|CallBackURL="+callBackURL+"|MID="+mid;
LOGGER.info("############################################################################## Amount ##"+requestparams);
shippingDtls = request.getParameter("appName")+"| 403, Tower A, 4th Floor, Logix Technova, sector 132|Noida|UP|201301|IN|"+mobile+"|"+mobile+"|"+mobile+"|"+mobile;
billingDtls=request.getParameter("appName")+"| 403, Tower A, 4th Floor, Logix Technova, sector 132|Noida|UP|201301|IN|"+mobile+"|"+mobile+"|"+mobile+"|"+mobile+"|"+email+"|appnit";
LOGGER.info("############################################################################## Amount ##"+shippingDtls);
LOGGER.info("############################################################################## Amount ##"+billingDtls);
LOGGER.info("############################################################################## requestparams de ##"+requestparams);
LOGGER.info("############################################################################## key de ##"+key+"##");

LOGGER.info("############################################################################## requestparams ##"+requestparams);
//LOGGER.info("############################################################################## requestparams de ##"+AES128Bit.decrypt(requestparams, key));
requestparams = requestparams.replaceAll("\n", "");
billingDtls = billingDtls.replaceAll("\n", "");
shippingDtls = shippingDtls.replaceAll("\n", ""); 


System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+session.getAttribute("pgTxnId"));
}else{
	responseparams="9|"+pgTxnId1+"|FAIL|||FAIL";
	System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+responseparams);
	flag=false;
}
}
else
{
	totalRefundAmt=totalRefundAmt-pgAmt;
	airTravelDao.updateTotalRefundFareQuote(totalRefundAmt , airFareId[0]);
	for(int i=1 ; i <airFareId.length ; i++)
	{
		airTravelDao.updateTotalRefundFareQuote(0.00 , airFareId[i]);
	}	
	responseparams="1000|Success|||";
	System.out.println("~~~~~~~~~~~~~~~Amount deducted from Refund Amount table~~~~~~~~~~~~~~~~~~~~~~~~~");
	flag=false;
}
}
else
{
	responseparams="9|FAIL|||FAIL";
	System.out.println("~~~~~~~~~~~~~~~PG Amount Validation Failed~~~~~~~~~~~~~~~~~~~~~~~~~");
	flag=false;
	
}
}
else
	{responseparams="9|FAIL|||FAIL";
System.out.println("~~~~~~~~~~~~~~~Amount Cannot be zero or less~~~~~~~~~~~~~~~~~~~~~~~~~");
flag=false;
}
}

else
{
	responseparams="9|FAIL|||FAIL";
	System.out.println("~~~~~~~~~~~~~~~Amount Validation Failed~~~~~~~~~~~~~~~~~~~~~~~~~");
	flag=false;
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%
LOGGER.debug("**********************************************flag************************  "+flag);
if(flag){
	LOGGER.debug("********************************************** calling payment gateway URL :"+SubmitTo);
	LOGGER.debug("********************************************** calling payment gateway with mid :"+mid);
	LOGGER.debug("********************************************** calling payment gateway with parameter :"+requestparams);
	LOGGER.debug("********************************************** calling payment gateway with billingDtls :"+billingDtls);
	LOGGER.debug("********************************************** calling payment gateway with shippingDtls :"+shippingDtls);
%>

<body onload="document.createElement('form').submit.call(document.getElementById('myForm'))">
<form name="ecom" method="post" name="pgWallet"  action="<%=SubmitTo%>" id="myForm"> 

<input type="hidden" name="requestparameter" value="<%=requestparams%>">
<input type="hidden" name="billingDtls" value="<%=billingDtls%>">
<input type="hidden" name="shippingDtls" value="<%=shippingDtls%>">
<input type="hidden" name="MID" value ="<%=mid%>"/>
<input type="hidden" name="submit" value="Submit">
 </form>

<%}else{
	LOGGER.debug("**********************************************flag********* false***************  "+flag);
	LOGGER.debug("*************************************************responseparams************************"+responseparams);
	
	%>
	
<script type="text/javascript">
	function showAndroid(result) {
		Bhartipay.success(result);
	}
</script>
<body onload="showAndroid('<%=responseparams%>')">


<% }%>


</body>
</html>
</html>