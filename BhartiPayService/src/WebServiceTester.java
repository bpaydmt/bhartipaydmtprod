import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class WebServiceTester {
	private Client client;
	
	 private String REST_SERVICE_URL = "http://localhost:8080/digitalpurseService/rest/WalletUser/mobileValidation";
	 
	/* private void init(){
	      this.client = ClientBuilder.newClient();
	   }
	 */
	 
	 public static void main(String[] args) throws IOException{
		   /*WebServiceTester tester = new WebServiceTester();
		   tester.init();
		   tester.mobilevalidation();*/
		 String xmlInput = "<impsTransactionRequest><remAccountNo>143200100201152</remAccountNo><custNo>5863713</custNo><accountNo>7211406164</accountNo><ifscCode>KKBK0000631</ifscCode><transAmt>12</transAmt><source>B</source><benefName>TEST</benefName><benefMobileNo>8983389108</benefMobileNo><narration>DMT test</narration><channel>DMT</channel></impsTransactionRequest>";
		 final String TEST_URL="https://125.16.108.22/swiftcore/faces/services/Transaction/dmtP2ATransfer/";
		 try {
			    SSLContext sc = SSLContext.getInstance("TLS");
			    sc.init(null, trustAllCerts, new SecureRandom());
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			} catch (Exception e) {
			    ;
			}
		 URL url = new URL(TEST_URL);
		 HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
		 httpsCon.setHostnameVerifier(new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		// httpsCon.connect();
		 
		 httpsCon.setRequestMethod("POST");
		 httpsCon.setDoOutput(true);
		 httpsCon.setRequestProperty("Content-Type", "text/xml");
		 DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
			
			if(xmlInput!=null && xmlInput.length()>1){
				wr.writeBytes(xmlInput);	
			}
			
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("xmlInput********** : " + xmlInput);
			wr.flush();
			wr.close();
			int responseCode = httpsCon.getResponseCode();
		 
		 
		 
		 InputStream is = httpsCon.getInputStream();
		 int nread = 0;
		 byte [] buf = new byte[8192];
		 while ((nread = is.read(buf)) != -1)
		 {
		     System.out.write(buf, 0, nread);
		 }
		 
	   }
	 
	 static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
		    public X509Certificate[] getAcceptedIssuers(){return null;}
		    public void checkClientTrusted(X509Certificate[] certs, String authType){}
		    public void checkServerTrusted(X509Certificate[] certs, String authType){}
		}};
	 
	  /* private void mobilevalidation(){
		      Form form = new Form();
		      form.param("mobileno", "9935041287");
		      String callResult = client.target(REST_SERVICE_URL).request(MediaType.APPLICATION_XML).post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE),String.class);
		      System.out.println("Test case name: forgetPassword, Result: "+callResult  );
		   }*/

}
