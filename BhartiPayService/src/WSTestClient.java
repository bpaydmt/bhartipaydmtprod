
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class WSTestClient {
	 public static void main(String[] args) {
				 
		 String SampleXml = "<impsTransactionRequest><remAccountNo>143200100201152</remAccountNo><custNo>5863713</custNo><accountNo>7211406164</accountNo><ifscCode>KKBK0000631</ifscCode><transAmt>12</transAmt><source>B</source><benefName>TEST</benefName><benefMobileNo>8983389108</benefMobileNo><narration>DMT test</narration><channel>DMT</channel></impsTransactionRequest>";
		 String urlStr = "https://125.16.108.22/swiftcore/faces/services/Transaction/dmtP2ATransfer/";
		 executeGetURL(urlStr,SampleXml);
	 
	}
 

	 public static String executeGetURL(String url,String xmlInput) {

			HttpsURLConnection con=null; 
			String responseString="Fail";
		    try{
		    		    	
		    	URL obj = new URL(url);
		    	//HttpsURLConnection.setDefaultHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		    	//System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		    	
		    /*	SSLContext ssl_ctx = SSLContext.getInstance("TLS");
		        TrustManager[ ] trust_mgr = get_trust_mgr();
		        ssl_ctx.init(null,                // key manager
		                     trust_mgr,           // trust manager
		                     new SecureRandom()); // random number generator
		        HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());*/
		    	
		//    	System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");
		  //  	Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		    	
				con = (HttpsURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				//con.setDoInput(true);
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "text/xml");
				//con.setRequestProperty("Content-length", String.valueOf(jsonData.length));
					
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				
				if(xmlInput!=null && xmlInput.length()>1){
					wr.writeBytes(xmlInput);	
				}
				
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("xmlInput********** : " + xmlInput);
				wr.flush();
				wr.close();
				int responseCode = con.getResponseCode();
				
				System.out.println("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				System.out.println("swiftcore Response************************************"+response.toString());
				responseString =response.toString();
			  } catch (Exception e) {
				  e.printStackTrace();
		      System.err.println(e);
		    } finally {
		    	con.disconnect();
		    }
		    return responseString;

		  }	
	 
	 
	 
	 
	 
/*	 private static TrustManager[ ] get_trust_mgr() {
		    TrustManager[ ] certs = new TrustManager[ ] {
		       new X509TrustManager() {
		          public X509Certificate[ ] getAcceptedIssuers() { return null; }
		          public void checkClientTrusted(X509Certificate[ ] certs, String t) { }
		          public void checkServerTrusted(X509Certificate[ ] certs, String t) { }
		        }
		     };
		     return certs;
		 }*/
	
}