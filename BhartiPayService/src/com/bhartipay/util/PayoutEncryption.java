package com.bhartipay.util;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface PayoutEncryption {

	public String encryptOpenSSL(String valueToEnc, String secretKey) throws IOException, GeneralSecurityException;
	
	public String decryptOpenSSL(String secretKey, String encryptedValue) throws IOException, GeneralSecurityException;
	
}
