package com.bhartipay.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;

public enum PoolingManager {


	
	INSTANCE(2000,200);
 	private int connectTime=60000;
	private PoolingHttpClientConnectionManager  cm= null;
  	private HttpClient httpClient;
	private int totalConnection=6000;
	private int maxConnectionPerRoute=2000;
	private int socketTime=60000;
	
	public int getSocketTime() 
	{
		return socketTime;
	}

	public void setSocketTime(int socketTime) {
		this.socketTime = socketTime;
	}

	public int getConnectTime() {
		return connectTime;
	}

	public void setConnectTime(int connectTime) {
		this.connectTime = connectTime;
	}


	
	public HttpClient getHttpClient() {
		return httpClient;
	}
 

 
	PoolingManager(int totalConnection, int maxConnectionPerRoute)
	{
		this.totalConnection=totalConnection;
		this.maxConnectionPerRoute=maxConnectionPerRoute;
		create();
	}
	
 
	
	private void create()
	{
		SSLContextBuilder sslContextBuilder = SSLContexts.custom();
		try {
			sslContextBuilder.loadTrustMaterial(null, new TrustStrategy() {
			    @Override
			    public boolean isTrusted(X509Certificate[] chain, String authType)
			            throws CertificateException {
			        return true;
			    }

			 
			});
			
			SSLContext sslContext = sslContextBuilder.build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(      sslContext, new String[] { "SSLv2Hello","SSLv3","TLSv1","TLSv1.1","TLSv1.2"},null,new HostnameVerifier() {
				
				@Override
				public boolean verify(String arg0, SSLSession arg1) {
					// TODO Auto-generated method stub
					return true;
				}
			});
 			
			Registry<ConnectionSocketFactory> socketFactoryRegistry = 
					  RegistryBuilder.<ConnectionSocketFactory> create()
					  .register("https", sslsf)
					  .register("http", new PlainConnectionSocketFactory()).build();

	 		
			
	  	    cm= new PoolingHttpClientConnectionManager(socketFactoryRegistry);
	 	    cm.setDefaultMaxPerRoute(maxConnectionPerRoute);
		    cm.setMaxTotal(totalConnection);
		    cm.setValidateAfterInactivity(2000);
		    
		    RequestConfig config = RequestConfig.custom()
		    	    .setSocketTimeout(socketTime)
		    	    .setConnectTimeout(connectTime)
		    	    .setConnectionRequestTimeout(socketTime)
		    	    .build();
		    
		    HttpClientBuilder builder = HttpClientBuilder.create();
		    
		    httpClient = builder.setConnectionManager(cm).setDefaultRequestConfig(config).build();	

		 
		    
		}
		catch (Exception exception)
		{
			
		}
			
	}




}
