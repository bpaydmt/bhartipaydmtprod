package com.bhartipay.util.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="smssenddetails")
public class SMSSendDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	@Column(name="aggreatorid",length=50)
	private String aggreatorid;
	
	@Column(name="recipientno",length=11)
	private String recipientno;
	
	@Column(name="walletid",length=30)
	private String walletid;
	
	@Column(name="sendername",length=100)
	private String sendername;
	
	@Column(name="status",length=500)
	private String status;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date entryon;
	
	
	
	@Transient
	public String stDate;
	
	@Transient
	public String endDate;
	
	
	
	
	
	
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRecipientno() {
		return recipientno;
	}

	public void setRecipientno(String recipientno) {
		this.recipientno = recipientno;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getSendername() {
		return sendername;
	}

	public void setSendername(String sendername) {
		this.sendername = sendername;
	}

	public Date getEntryon() {
		return entryon;
	}

	public void setEntryon(Date entryon) {
		this.entryon = entryon;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	
	
	
	
	
	

}
