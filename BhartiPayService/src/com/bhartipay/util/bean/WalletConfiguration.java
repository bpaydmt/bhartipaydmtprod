package com.bhartipay.util.bean;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.bhartipay.commission.bean.CommPercRangeMaster;

@Entity
@Table(name = "walletconfig")

public class WalletConfiguration {

	@Id
	@Column(name = "aggreatorid", nullable = false, updatable = false)
	private String aggreatorid;

	@Column(name = "countryid", nullable = false, updatable = true)
	private int countryid;

	@Column(name = "appname", nullable = false, length = 100)
	private String appname;

	@Column(name = "protocol", length = 10)
	private String protocol;

	/*
	 * @Column(name="country",nullable = true, length = 100 ) private String
	 * country;
	 */

	@Column(name = "countrycurrency", nullable = false, length = 100)
	private String countrycurrency;

	@Column(name = "smssend", nullable = false, length = 1, columnDefinition = "int default 0")
	private int smssend;

	@Column(name = "kycvalid", nullable = false, length = 1, columnDefinition = "int default 0")
	private int kycvalid;

	@Column(name = "emailvalid", nullable = false, length = 1, columnDefinition = "int default 0")
	private int emailvalid;

	@Column(name = "otpsendtomail", nullable = false, length = 1, columnDefinition = "int default 0")
	private int otpsendtomail;

	@Column(name = "reqagentapproval", nullable = false, length = 1, columnDefinition = "int default 0")
	private int reqAgentApproval;

	@Column(name = "status", nullable = false, length = 1, columnDefinition = "int default 0")
	private int status;

	@Column(name = "logopic", nullable = true, length = 500)
	private String logopic;

	@Column(name = "bannerpic", nullable = true, length = 500)
	private String bannerpic;

	@Column(name = "domainname", length = 100)
	private String domainName;

	@Column(name = "themes", length = 100)
	private String themes;

	@Column(name = "gcmappkey")
	private String gcmAppKey;

	@Column(name = "caption", length = 100)
	private String caption;

	@Column(name = "pgagid", length = 100)
	private String pgAgId;

	@Column(name = "pgagidmid", length = 100)
	private String pgMid;

	@Column(name = "pgurl", length = 100)
	private String pgUrl;

	@Column(name = "pgenckey", length = 100)
	private String pgEncKey;

	@Column(name = "pgtxntype", length = 100)
	private String pgTxnType;

	@Column(name = "pgcountry", length = 100)
	private String pgCountry;

	@Column(name = "pgcurrency", length = 100)
	private String pgCurrency;

	@Column(name = "pgcallbackurl", length = 100)
	private String pgCallBackUrl;

	@Column(name = "ipiemi", length = 20)
	private String ipiemi;

	@Column(name = "agent", length = 300)
	private String agent;

	@Column(name = "customercare", length = 300)
	private String customerCare;

	@Column(name = "supportemailid", length = 300)
	private String supportEmailId;

	@Column(name = "flashmessage", length = 500)
	private String flaxMessage;

	@Column(name = "favicon")
	private String favicon;

//	@OneToMany(mappedBy = "aggreatorid", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
//	@Fetch(FetchMode.SELECT)
//    private Set<ServiceMasterRequestBean> serviceMaster = new HashSet<ServiceMasterRequestBean>(0);

	@Column(name = "retailerappurl")
	private String retailerAppUrl;

	@Column(name = "distributerappurl")
	private String distributorAppUrl;

	
	/**********************************/
	@Column(name = "fimps", length = 1)
	private String fImps;
	
	@Column(name = "fneft", length = 1)
	private String fNeft;
	
	@Column(name = "asimps", length = 1)
	private String asImps;
	
	@Column(name = "asneft", length = 1)
	private String asNeft;
	
	
	@Column(name = "pimps", length = 1)
	private String pImps;
	
	@Column(name = "pneft", length = 1)
	private String pNeft;
	
	@Column(name = "pmode", length = 10)
	private String pMode;
	
	
	@Column(name = "finoaeps", length = 1)
	private String finoAeps;
	
	@Column(name = "yesaeps", length = 1)
	private String yesAeps;
	
	@Column(name = "iciciaeps", length = 1)
	private String iciciAeps;
	
	@Column(name = "matm", length = 1)
	private String matm;
	
	
	
	
	
	
	public String getpImps() {
		return pImps;
	}

	public void setpImps(String pImps) {
		this.pImps = pImps;
	}

	public String getpNeft() {
		return pNeft;
	}

	public void setpNeft(String pNeft) {
		this.pNeft = pNeft;
	}

	public String getpMode() {
		return pMode;
	}

	public void setpMode(String pMode) {
		this.pMode = pMode;
	}

	public String getfImps() {
		return fImps;
	}

	public void setfImps(String fImps) {
		this.fImps = fImps;
	}

	public String getfNeft() {
		return fNeft;
	}

	public void setfNeft(String fNeft) {
		this.fNeft = fNeft;
	}

	public String getAsImps() {
		return asImps;
	}

	public void setAsImps(String asImps) {
		this.asImps = asImps;
	}

	public String getAsNeft() {
		return asNeft;
	}

	public void setAsNeft(String asNeft) {
		this.asNeft = asNeft;
	}

	public String getFinoAeps() {
		return finoAeps;
	}

	public void setFinoAeps(String finoAeps) {
		this.finoAeps = finoAeps;
	}

	public String getYesAeps() {
		return yesAeps;
	}

	public void setYesAeps(String yesAeps) {
		this.yesAeps = yesAeps;
	}

	public String getIciciAeps() {
		return iciciAeps;
	}

	public void setIciciAeps(String iciciAeps) {
		this.iciciAeps = iciciAeps;
	}

	public String getMatm() {
		return matm;
	}

	public void setMatm(String matm) {
		this.matm = matm;
	}

	public String getRetailerAppUrl() {
		return retailerAppUrl;
	}

	public void setRetailerAppUrl(String retailerAppUrl) {
		this.retailerAppUrl = retailerAppUrl;
	}

	public String getDistributorAppUrl() {
		return distributorAppUrl;
	}

	public void setDistributorAppUrl(String distributorAppUrl) {
		this.distributorAppUrl = distributorAppUrl;
	}

	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public String getFlaxMessage() {
		return flaxMessage;
	}

	public void setFlaxMessage(String flaxMessage) {
		this.flaxMessage = flaxMessage;
	}

	public String getSupportEmailId() {
		return supportEmailId;
	}

	public void setSupportEmailId(String supportEmailId) {
		this.supportEmailId = supportEmailId;
	}

	public String getCustomerCare() {
		return customerCare;
	}

	public void setCustomerCare(String customerCare) {
		this.customerCare = customerCare;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getPgCallBackUrl() {
		return pgCallBackUrl;
	}

	public void setPgCallBackUrl(String pgCallBackUrl) {
		this.pgCallBackUrl = pgCallBackUrl;
	}

	public String getPgAgId() {
		return pgAgId;
	}

	public void setPgAgId(String pgAgId) {
		this.pgAgId = pgAgId;
	}

	public String getPgMid() {
		return pgMid;
	}

	public void setPgMid(String pgMid) {
		this.pgMid = pgMid;
	}

	public String getPgUrl() {
		return pgUrl;
	}

	public void setPgUrl(String pgUrl) {
		this.pgUrl = pgUrl;
	}

	public String getPgEncKey() {
		return pgEncKey;
	}

	public void setPgEncKey(String pgEncKey) {
		this.pgEncKey = pgEncKey;
	}

	public String getPgTxnType() {
		return pgTxnType;
	}

	public void setPgTxnType(String pgTxnType) {
		this.pgTxnType = pgTxnType;
	}

	public String getPgCountry() {
		return pgCountry;
	}

	public void setPgCountry(String pgCountry) {
		this.pgCountry = pgCountry;
	}

	public String getPgCurrency() {
		return pgCurrency;
	}

	public void setPgCurrency(String pgCurrency) {
		this.pgCurrency = pgCurrency;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getGcmAppKey() {
		return gcmAppKey;
	}

	public void setGcmAppKey(String gcmAppKey) {
		this.gcmAppKey = gcmAppKey;
	}

	public String getThemes() {
		return themes;
	}

	public void setThemes(String themes) {
		this.themes = themes;
	}

	@Transient
	private String[] txnType;

	@Transient
	private String actionResult;

	public String[] getTxnType() {
		return txnType;
	}

	public void setTxnType(String[] txnType) {
		this.txnType = txnType;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public int getReqAgentApproval() {
		return reqAgentApproval;
	}

	public void setReqAgentApproval(int reqAgentApproval) {
		this.reqAgentApproval = reqAgentApproval;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public int getSmssend() {
		return smssend;
	}

	public void setSmssend(int smssend) {
		this.smssend = smssend;
	}

	public int getCountryid() {
		return countryid;
	}

	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}

	/*
	 * public String getCountry() { return country; }
	 * 
	 * 
	 * public void setCountry(String country) { this.country = country; }
	 */

	public String getCountrycurrency() {
		return countrycurrency;
	}

	public void setCountrycurrency(String countrycurrency) {
		this.countrycurrency = countrycurrency;
	}

	public int getKycvalid() {
		return kycvalid;
	}

	public void setKycvalid(int kycvalid) {
		this.kycvalid = kycvalid;
	}

	public int getEmailvalid() {
		return emailvalid;
	}

	public void setEmailvalid(int emailvalid) {
		this.emailvalid = emailvalid;
	}

	public int getOtpsendtomail() {
		return otpsendtomail;
	}

	public void setOtpsendtomail(int otpsendtomail) {
		this.otpsendtomail = otpsendtomail;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLogopic() {
		return logopic;
	}

	public void setLogopic(String logopic) {
		this.logopic = logopic;
	}

	public String getBannerpic() {
		return bannerpic;
	}

	public void setBannerpic(String bannerpic) {
		this.bannerpic = bannerpic;
	}

//	public Set<ServiceMasterRequestBean> getServiceMaster() {
//		return serviceMaster;
//	}
//
//
//	public void setServiceMaster(Set<ServiceMasterRequestBean> serviceMaster) {
//		this.serviceMaster = serviceMaster;
//	}
}
