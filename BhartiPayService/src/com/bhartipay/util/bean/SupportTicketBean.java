package com.bhartipay.util.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="supportticket")
public class SupportTicketBean {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	@Column(name="aggreatorid",length=50)
	private String aggreatorId;
	
	@Column(name="userid",length=50)
	private String userId;
	
	@Column(name="mobileno",length=11)
	private String mobileNo;
	
	@Column(name="emailid",length=100)
	private String emailId;
	
	@Column(name="tickettype",length=50)
	private String ticketType;
	
	@Column(name="description",length=500)
	private String description;
	
	
	@Column(name="status",length=20)
	private String status;
	
	@Column(name="ipiemi",length=20)
	private String ipiemi;
	
	@Column(name="useragent",length=500)
	private String userAgent;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date entryon;
	
	@Column(name="remarks", length=500)
	private String remarks;
	
	@Transient
	private String statusCode;

	@Transient
	private String stDate;
	@Transient
	private String endDate;

	
	
	
	
	public String getStDate() {
		return stDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setStDate(String stDate) {
		this.stDate = stDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAggreatorId() {
		return aggreatorId;
	}


	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getTicketType() {
		return ticketType;
	}


	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}





	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getEntryon() {
		return entryon;
	}


	public void setEntryon(Date entryon) {
		this.entryon = entryon;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getIpiemi() {
		return ipiemi;
	}


	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}


	public String getUserAgent() {
		return userAgent;
	}


	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	
		
	

}
