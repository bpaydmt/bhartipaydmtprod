package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="versioncontrol")
public class VersionControl {
	
 @Id
 @GeneratedValue(strategy=GenerationType.AUTO)
 @Column(name="id",length=12)
 private int id;
	
 @Column(name="aggreatorid",nullable=false,updatable=false,length=50)
 private String aggreatorId;
 
 @Column(name="versionid",nullable=false,updatable=true,length=6)
 private int versionId;
 
 @Column(name="versionstatus",nullable=false,updatable=true,length=50)
 private String versionStatus;
 
 
 @Transient
	private String statusCode;
	
 @Transient
 private String statusMsg;
	
	
	
	
 

public String getStatusCode() {
	return statusCode;
}

public void setStatusCode(String statusCode) {
	this.statusCode = statusCode;
}

public String getStatusMsg() {
	return statusMsg;
}

public void setStatusMsg(String statusMsg) {
	this.statusMsg = statusMsg;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}



public String getAggreatorId() {
	return aggreatorId;
}

public void setAggreatorId(String aggreatorId) {
	this.aggreatorId = aggreatorId;
}

public int getVersionId() {
	return versionId;
}

public void setVersionId(int versionId) {
	this.versionId = versionId;
}

public String getVersionStatus() {
	return versionStatus;
}

public void setVersionStatus(String versionStatus) {
	this.versionStatus = versionStatus;
}
 
 
 
 
 
 

}
