package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="couponsmast")
public class CouponsBean {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	@Column(name="featured",length=100)
	@SerializedName("featured")
	@Expose
	private String featured;
	
	@Column(name="exclusive",length=100)
	@SerializedName("exclusive")
	@Expose
	private String exclusive;
	
	@Column(name="promoid",length=100)
	@SerializedName("promo_id")
	@Expose
	private String promoId;
	
	@Column(name="offerid",length=100)
	@SerializedName("offer_id")
	@Expose
	private String offerId;
	
	@Column(name="offername",length=100)
	@SerializedName("offer_name")
	@Expose
	private String offerName;
	
	@Column(name="coupontitle",length=500)
	@SerializedName("coupon_title")
	@Expose
	private String couponTitle;
	
	@Column(name="category",length=100)
	@SerializedName("category")
	@Expose
	private String category;
	
	@Column(name="coupondescription",length=5000)
	@SerializedName("coupon_description")
	@Expose
	private String couponDescription;
	
	@Column(name="coupontype",length=100)
	@SerializedName("coupon_type")
	@Expose
	private String couponType;
	
	@Column(name="couponcode",length=100)
	@SerializedName("coupon_code")
	@Expose
	private String couponCode;
	
	@Column(name="refid",length=500)
	@SerializedName("ref_id")
	@Expose
	private String refId;
		
	@Column(name="link",length=500)
	@SerializedName("link")
	@Expose
	private String link;
	
	@Column(name="couponexpiry",length=50)
	@SerializedName("coupon_expiry")
	@Expose
	private String couponExpiry;
	
	@Column(name="added",length=50)
	@SerializedName("added")
	@Expose
	private String added;
	
	@Column(name="previewurl",length=500)
	@SerializedName("preview_url")
	@Expose
	private String previewUrl;
	
	@Column(name="storelink",length=500)
	@SerializedName("store_link")
	@Expose
	private String storeLink;
	
	@Column(name="storeimage",length=500)
	@SerializedName("store_image")
	@Expose
	private String storeImage;

	
	@Transient
	private String userId;
	@Transient
	private String ids;
	@Transient
	private String rechargeAmount;
	@Transient
	private String rechargeMobile;
	
	
	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(String rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public String getRechargeMobile() {
		return rechargeMobile;
	}

	public void setRechargeMobile(String rechargeMobile) {
		this.rechargeMobile = rechargeMobile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFeatured() {
		return featured;
	}

	public void setFeatured(String featured) {
		this.featured = featured;
	}

	public String getExclusive() {
		return exclusive;
	}

	public void setExclusive(String exclusive) {
		this.exclusive = exclusive;
	}

	public String getPromoId() {
		return promoId;
	}

	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getCouponTitle() {
		return couponTitle;
	}

	public void setCouponTitle(String couponTitle) {
		this.couponTitle = couponTitle;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category.trim();
	}

	public String getCouponDescription() {
		return couponDescription;
	}

	public void setCouponDescription(String couponDescription) {
		this.couponDescription = couponDescription;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getCouponExpiry() {
		return couponExpiry;
	}

	public void setCouponExpiry(String couponExpiry) {
		this.couponExpiry = couponExpiry;
	}

	public String getAdded() {
		return added;
	}

	public void setAdded(String added) {
		this.added = added;
	}

	public String getPreviewUrl() {
		return previewUrl;
	}

	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}

	public String getStoreLink() {
		return storeLink;
	}

	public void setStoreLink(String storeLink) {
		this.storeLink = storeLink;
	}

	public String getStoreImage() {
		return storeImage;
	}

	public void setStoreImage(String storeImage) {
		this.storeImage = storeImage;
	}	
	
	
	
	
	
	

}
