package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="userrolemapping")
public class UserRoleMapping {
	
	
	
	@Id
	@Column(name="userid",nullable=false,updatable=false)
	private String userId;
	
	@Column(name ="roleid")
	private String roleId;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;	
	
	
	
	@Transient
	private String[] roleType;
	
	@Transient
	private String actionResult;
	
	@Transient
	private double amount;

	
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String[] getRoleType() {
		return roleType;
	}

	public void setRoleType(String[] roleType) {
		this.roleType = roleType;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	
	
	
	
	
	
	
	

}
