package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="mailurl")
public class MailConfigMast {
	
	@Id
	@Column(name="aggreatorid",nullable=false,updatable=false)
	private String aggreatorid;
	
	@Column(name ="hosturl")
	private String hosturl;
	
	@Column(name ="from_mail")
	private String from_mail;
	
	
	@Column(name ="fromname")
	private String fromname;
	
	@Column(name ="username")
	private String username;
	
	@Column(name ="password")
	private String password;
	
	@Column(name ="port")
	private String port;

	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	
	@Transient
	private String actionResult;
	
	
	
	
	

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getHosturl() {
		return hosturl;
	}

	public void setHosturl(String hosturl) {
		this.hosturl = hosturl;
	}

	public String getFrom_mail() {
		return from_mail;
	}

	public void setFrom_mail(String from_mail) {
		this.from_mail = from_mail;
	}

	public String getFromname() {
		return fromname;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
	

	

}
