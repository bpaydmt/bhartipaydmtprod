package com.bhartipay.util.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="paymentgatewayurl")
public class PaymentGatewayURL implements Serializable{
	
	@Id
	@Column(name="txnid",nullable=false,updatable=false)
	String txnId;
	
	
	@Column(name ="url")
	String URL;
	
	@Column(name ="appname")
	String appName;

	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMsg;
	
	
	
	
	
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	
	
	
	

}
