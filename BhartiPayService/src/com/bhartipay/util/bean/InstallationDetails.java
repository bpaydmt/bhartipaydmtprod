package com.bhartipay.util.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="installationdetails")

public class InstallationDetails implements Serializable{
	
		
	@Id
	@Column(name="imei",nullable=false,updatable=true)
	private long imei;
	
	
	@Column(name ="dvcname")
	private String deviceName;
	
	@Column(name ="dvcmanf")
	private String deviceMan;
	
	@Column(name ="dvc")
	private String device;
	
	@Column(name ="dvcbrand")
	private String deviceBrand;
	
	@Column(name ="dvcversion")
	private String deviceVersion;
	
	@Column(name ="appversionname")
	private String versionName;
	
	@Column(name ="appversionnumber")
	private int versionNumber;
	
	@Column(name ="count")
	private int count;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private String dateTime;
	
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMsg;
	
	
		

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceMan() {
		return deviceMan;
	}

	public void setDeviceMan(String deviceMan) {
		this.deviceMan = deviceMan;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getDeviceVersion() {
		return deviceVersion;
	}

	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public int getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
	
	

}
