package com.bhartipay.util.bean;

import java.util.List;

import com.bhartipay.airTravel.bean.fareQuote.FareQuote;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionResponseBean 
{
	@SerializedName("statusCode")
    @Expose
    private String statusCode;

 
	@SerializedName("status")
    @Expose
    private String status;
	
	@SerializedName("amount")
    @Expose
    private double amount;
 
	@SerializedName("travelId")
    @Expose
    private String travelId;
 
	@SerializedName("statusMsg")
    @Expose
    private String statusMsg;
	
	
	@SerializedName("version")
    @Expose
    private String version;
	

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
 
}
