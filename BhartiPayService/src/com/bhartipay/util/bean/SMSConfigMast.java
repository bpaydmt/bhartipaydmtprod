package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="smsconfmast")
public class SMSConfigMast {

	@Id
	@Column(name="aggreatorid",nullable=false,updatable=false)
	private String aggreatorid;
	
	@Column(name ="smsurl")
	private String smsurl;
	
	@Column(name ="smsfeedid")
	private String smsfeedid;
	
	@Column(name ="smsusername")
	private String smsusername;
	
	@Column(name ="smspassword")
	private String smspassword;
	
	@Column(name ="smssenderid")
	private String smssenderid;

	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	@Transient
	private String actionResult;
	
	
	
	

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getSmsurl() {
		return smsurl;
	}

	public void setSmsurl(String smsurl) {
		this.smsurl = smsurl;
	}

	public String getSmsfeedid() {
		return smsfeedid;
	}

	public void setSmsfeedid(String smsfeedid) {
		this.smsfeedid = smsfeedid;
	}

	public String getSmsusername() {
		return smsusername;
	}

	public void setSmsusername(String smsusername) {
		this.smsusername = smsusername;
	}

	public String getSmspassword() {
		return smspassword;
	}

	public void setSmspassword(String smspassword) {
		this.smspassword = smspassword;
	}

	public String getSmssenderid() {
		return smssenderid;
	}

	public void setSmssenderid(String smssenderid) {
		this.smssenderid = smssenderid;
	}
	
	
	
	
	
	
	
	
}
