package com.bhartipay.util.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="partnerprefundmast")
public class PartnerPrefundMastBean {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	
	@Column(name="aggreatorid",nullable=false,length=50)
	private String aggreatorid;
	
	@Column(name="userid",nullable=false,length=50)
	private String userId;
	
	@Column(name="partnername",nullable=false,length=50)
	private String partnerName;
	
	
	@Column(name="txntype",nullable=false,length=10)
	private String txnType;
	
	
	@Column(name="amount",nullable = false, columnDefinition="Decimal(12,2)")
	private double amount;
	
	
	@Column(name="txnsource",length=50)
	private String txnSource;
	
	@Column(name="txnrefno",length=50)
	private String txnRefNo;
	
	
	@Column(name="partnerrefno",length=50)
	private String partnerRefNo;
	
	@Column(name="description",length=250)
	private String description;
	
	@Temporal(TemporalType.DATE)
	@Column(name="txndate")
	private Date txnDate;


	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMsg;
	
	@Transient
	private String stDate;
	
	
	
	
	
	
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getStDate() {
		return stDate;
	}


	public void setStDate(String stDate) {
		this.stDate = stDate;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAggreatorid() {
		return aggreatorid;
	}


	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}


	public String getPartnerName() {
		return partnerName;
	}


	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}


	public String getTxnType() {
		return txnType;
	}


	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getTxnSource() {
		return txnSource;
	}


	public void setTxnSource(String txnSource) {
		this.txnSource = txnSource;
	}


	public String getTxnRefNo() {
		return txnRefNo;
	}


	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}


	public String getPartnerRefNo() {
		return partnerRefNo;
	}


	public void setPartnerRefNo(String partnerRefNo) {
		this.partnerRefNo = partnerRefNo;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getTxnDate() {
		return txnDate;
	}


	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getStatusMsg() {
		return statusMsg;
	}


	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	

	

}
