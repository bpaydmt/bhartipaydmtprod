package com.bhartipay.util.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="menumastercode")
public class MenuConfigMast {
	
	@Id
	@Column(name="code",nullable=false,updatable=false)
	private String code;
	
	@Column(name ="menudesc")
	private String menudesc;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date entryon;


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMenudesc() {
		return menudesc;
	}


	public void setMenudesc(String menudesc) {
		this.menudesc = menudesc;
	}


	public Date getEntryon() {
		return entryon;
	}


	public void setEntryon(Date entryon) {
		this.entryon = entryon;
	}
	
	
	
	
	
	
	
}
