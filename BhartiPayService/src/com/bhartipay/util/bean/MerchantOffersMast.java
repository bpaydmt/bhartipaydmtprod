package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="merchantoffersmast")
public class MerchantOffersMast {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	@Column(name="aggreatorid",length=50)
	private String aggreatorId;
	
	@Column(name="shopname",length=50)
	private String shopName;
	
	@Column(name="merchantid",length=50)
	private String merchantId;
	
	@Column(name="terminalid",length=50)
	private String terminalId;
		
	@Column(name="cashback",length=3)
	private int cashBack;
	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public int getCashBack() {
		return cashBack;
	}

	public void setCashBack(int cashBack) {
		this.cashBack = cashBack;
	}
	
	
	
}
