package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="usermenumapping")
public class UserMenuMapping {

	@Id
	@Column(name="userid",nullable=false,updatable=false)
	private String userId;
	
	@Column(name ="menuid")
	private String menuId;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;	
	
	
	
	@Transient
	private String[] menuType;
	
	@Transient
	private String actionResult;
	
	
	
	
	
	
	

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String[] getMenuType() {
		return menuType;
	}

	public void setMenuType(String[] menuType) {
		this.menuType = menuType;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	
	
	
	
	
	
	
	
	
	
}
