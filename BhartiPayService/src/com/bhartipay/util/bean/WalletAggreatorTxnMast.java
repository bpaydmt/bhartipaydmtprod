package com.bhartipay.util.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="walletaggreatortxnmast")

public class WalletAggreatorTxnMast {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",nullable=false)
	private String id;
	
	@Column(name="aggreatorid",nullable=false)
	private String aggreatorId;
	
	@Column(name="txncode",nullable=false)
	private String txnCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getTxnCode() {
		return txnCode;
	}

	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	

	
	
	
	
	
}
