//package com.bhartipay.util.bean;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//
//@Entity
//@Table(name="servicemaster")
//public class ServiceMasterRequestBean implements Serializable{
//	
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@Column(name="id")
//	private Long id;
//	
//	@Column(name="aggreatorid",nullable = false,updatable=false)
//	private String aggreatorid;
//	
//	@Column(name="servicesname",nullable = false,length = 100)
//	private String servicesname;
//	
//	@Column(name="status",nullable = false,length = 5)
//	private String status;
//	
//	@Column(name="type",length = 30)
//	private String type;
//	
//	@Column(name="servicescode",length = 15)
//	private String servicescode;
//	
//	@Column(name="servicetype",length = 100)
//	private String serviceType;
//
//	public String getAggreatorid() {
//		return aggreatorid;
//	}
//	public void setAggreatorid(String aggreatorid) {
//		this.aggreatorid = aggreatorid;
//	}
//	public String getServiceType() {
//		return serviceType;
//	}
//	public void setServiceType(String serviceType) {
//		this.serviceType = serviceType;
//	}
//	public String getStatus() {
//		return status;
//	}
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	public String getServicesname() {
//		return servicesname;
//	}
//	public void setServicesname(String servicesname) {
//		this.servicesname = servicesname;
//	}
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
//	public String getServicescode() {
//		return servicescode;
//	}
//	public void setServicescode(String servicescode) {
//		this.servicescode = servicescode;
//	}
//	
//}
