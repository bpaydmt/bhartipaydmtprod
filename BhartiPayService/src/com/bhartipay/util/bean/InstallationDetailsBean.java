package com.bhartipay.util.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="installationdetails")
public class InstallationDetailsBean {

	@Id
	@Column(name="imei",length=30, nullable = false)
	public String imei;
	
	@Column(name="devicemanf",length=100)
	public String dvcManf;
	
	@Column(name="device",length=100)
	public String dvc;
	
	@Column(name="devicename",length=100)
	public String dvcName;
	
	@Column(name="devicebrand",length=100)
	public String dvcBrand;
	
	@Column(name="deviceversion",length=100)
	public String dvcVersion;
	
	@Column(name="appname",length=100)
	public String appName;
	
	@Column(name="appversionname",length=100)
	public String appVersionName;
	
	@Column(name="appversionnumber",length=100)
	public String appVersionNumber;
	
	@Column(name="count",length=2)
	public int count;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date installationdate;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date updatedate;
	
	
	@Transient
	private String status; 
	

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDvcManf() {
		return dvcManf;
	}

	public void setDvcManf(String dvcManf) {
		this.dvcManf = dvcManf;
	}

	public String getDvc() {
		return dvc;
	}

	public void setDvc(String dvc) {
		this.dvc = dvc;
	}

	public String getDvcName() {
		return dvcName;
	}

	public void setDvcName(String dvcName) {
		this.dvcName = dvcName;
	}

	public String getDvcBrand() {
		return dvcBrand;
	}

	public void setDvcBrand(String dvcBrand) {
		this.dvcBrand = dvcBrand;
	}

	public String getDvcVersion() {
		return dvcVersion;
	}

	public void setDvcVersion(String dvcVersion) {
		this.dvcVersion = dvcVersion;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppVersionName() {
		return appVersionName;
	}

	public void setAppVersionName(String appVersionName) {
		this.appVersionName = appVersionName;
	}

	public String getAppVersionNumber() {
		return appVersionNumber;
	}

	public void setAppVersionNumber(String appVersionNumber) {
		this.appVersionNumber = appVersionNumber;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getInstallationdate() {
		return installationdate;
	}

	public void setInstallationdate(Date installationdate) {
		this.installationdate = installationdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
          

}
