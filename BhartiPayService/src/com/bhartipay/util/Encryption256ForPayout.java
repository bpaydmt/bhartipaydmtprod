package com.bhartipay.util;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import appnit.com.base64.BASE64Decoder;
import appnit.com.base64.BASE64Encoder;

import java.security.GeneralSecurityException;



import org.apache.commons.ssl.OpenSSL;
import java.io.*;

public class Encryption256ForPayout implements PayoutEncryption
{
Logger logger=Logger.getLogger(Encryption256ForPayout.class);
 public Encryption256ForPayout()
 {
 }


	  public  String decryptOpenSSL(String key,String data) throws IOException, GeneralSecurityException{
	   
	    OpenSSL opensll=new OpenSSL();
	    InputStream  is=OpenSSL.decrypt("AES256", key.getBytes(), new ByteArrayInputStream(data.getBytes()));
	    Base64 encode=new Base64();
	    
	     
	    BufferedReader in = new BufferedReader(new InputStreamReader(is));
	    String inputLine;
	   StringBuffer response = new StringBuffer();

	   while ((inputLine = in.readLine()) != null) {
	    response.append(inputLine);
	   }
	   in.close();
	      System.out.println(response);
	      //return encode.encode(response.toString().getBytes()).toString();
	      return response.toString();
	   }
public String encryptOpenSSL(String data,String key) throws IOException, GeneralSecurityException{
	  
  	OpenSSL opensll=new OpenSSL();
  	InputStream  is=OpenSSL.encrypt("AES256", key.getBytes(), new ByteArrayInputStream(data.getBytes()), true);
  	BufferedReader in = new BufferedReader(new InputStreamReader(is));
  	String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
     System.out.println(response);
     return response.toString();
  }


 public static String encrypt(String textToEncrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(1, makeKey(key), makeIv());
         return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 public static String decrypt(String textToDecrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(2, makeKey(key), makeIv());
         return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 private static AlgorithmParameterSpec makeIv()
 {
     try
     {
         return new IvParameterSpec("0123456789abcdef".getBytes("UTF-8"));
     }
     catch(UnsupportedEncodingException e)
     {
         e.printStackTrace();
     }
     return null;
 }

 private static Key makeKey(String encryptionKey)
 {
     try
     {
         byte key[] = Base64.decodeBase64(encryptionKey.getBytes());
         return new SecretKeySpec(key, "AES");
     }
     catch(Exception e)
     {
         e.printStackTrace();
     }
     return null;
 }

 public static String generateMerchantKey()
 {
     String newKey = null;
     try
     {
         KeyGenerator kgen = KeyGenerator.getInstance("AES");
         kgen.init(256);
         SecretKey skey = kgen.generateKey();
         byte raw[] = skey.getEncoded();
         newKey = new String(Base64.encodeBase64(raw));
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
     return newKey;
 }

 private static final String ENCRYPTION_IV = "0123456789abcdef";
 private static final String PADDING = "AES/CBC/PKCS5Padding";
 private static final String ALGORITHM = "AES";
 private static final String CHARTSET = "UTF-8";

 static 
 {
     try
     {
         Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
         field.setAccessible(true);
         field.set(null, Boolean.FALSE);
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
 }
 
 public String decryptSBI(String encData,String merchantCode) {
     logger.info("decrypt(String encData, String merchantCode, String delimiter) method begin");
     String decdata=null;
      
     String path =merchantCode + ".key";

         byte[] key = null;
         try {
             key = returnbyte(path);
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
      
     try{
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding","SunJCE");
         logger.info("Provider Info " + cipher.getProvider().getInfo());
         byte[] keyBytes= new byte[16];
         //byte[] b= key.getBytes("UTF-8");
         int len= key.length;
         if (len > keyBytes.length) len = keyBytes.length;
         System.arraycopy(key, 0, keyBytes, 0, len);
         SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
         logger.info("After SecretKeySpec");
         IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
         cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);
         BASE64Decoder decoder = new BASE64Decoder();
         logger.info("before final");
         byte [] results= decoder.decodeBuffer(encData);
         byte [] ciphertext=cipher.doFinal(results);
         logger.info("after");
         decdata=new String(ciphertext,"UTF-8");
         System.out.println(decdata); 
     }catch(Exception ex){
         logger.info("Exception occured :" + ex);
         ex.printStackTrace();
     }
     logger.info("decrypt(String encData, String merchantCode, String delimiter) method end");
     return decdata;
 }

public String encryptSBI(String data,String merchantCode) {

     String path = merchantCode + ".key";

     byte[] key = null;
     try {
         key = returnbyte(path);
     } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
     }
  
     String encData=null;
     try{
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding","SunJCE");
         logger.info("Provider Info " + cipher.getProvider().getInfo());
         byte[] keyBytes= new byte[16];
         //byte[] b= path.getBytes("UTF-8");
         int len= key.length;
         if (len > keyBytes.length) len = keyBytes.length;
         System.arraycopy(key, 0, keyBytes, 0, len);
         SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
         IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
         cipher.init(Cipher.ENCRYPT_MODE,keySpec, ivSpec);
         byte[] results = cipher.doFinal(data.getBytes("UTF-8"));
         BASE64Encoder encoder = new BASE64Encoder();
         encData= encoder.encode(results);
         System.out.println(encData); 
          
     }catch(Exception ex){
         logger.info("Exception occured :" + ex);
     }
     logger.info("encrypt(String data,String merchantCode) method end");
     return encData;
 }

private byte[] returnbyte(String filepath) throws IOException {
Path path = Paths.get(filepath);
byte[] data = java.nio.file.Files.readAllBytes(path);
return data;
}
 
 public static void main(String[] args) {
//	String encString=encrypt("appnit technologies", "3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs=");
//	System.out.println(encString);
//	System.out.println(decrypt("EjKDo+4I4Rk4Mo4qadmukr41fxC21TdyQ22iK16i0hHR6DkNqW4ZR1p6k7y97szOO2NbLjYc2Y2BjjkLpASLWsktxFhaM6anU4+Cobiw1NZklowH1+tBXmA4OzdcLINa5PdiiKdwXnYdfQGgr5XF+FPvMbbjO2L0AGlN5UmaRunsdRXaYwzysT+MG1MBkWiyWLB/Tr6PX2bBGYnhyk5Gng==","3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs="));
//	System.out.println(decrypt("+cqboEWHNWXHc7b4lQzmUQ==","3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs="));
//	System.out.println(decrypt("gG41szsnHFer9Ic2MV5jLA==","3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs="));
//	System.out.println(decrypt("dH826JekGYyy+34lqdWLQXb+5PF0BrNpHyRSNrKyjJXaS6NrN3yVKihJFhe0fbqy73XL4qr74EjIs+BJByi1Uw==","3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs="));
//	System.out.println(decrypt("ghEiOUXc+KWiAajWK3QnYfLz3pC6W8pxdyWOtc2LqlkyYYnS1uB+Zxd5CyaBpeeTERRFPMTVmTTogopNplV7TQ==","3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs="));
	
//	 {"ResponseCode":0,"Messag
//		 eString":"Transaction Failed","DisplayMessage":"Transaction Failed","RequestID":"ECO_SYS_USR3_0712201818081344","ClientUniqueID":
//		 "TB1FBTX139782","ResponseData":"U2FsdGVkX1\/5I2\/7cSGUKk7XUaSwmGSfvST3jlO4SGdVAZyJc4Fr6yCZW6Y080FQRvqiS4CifRh\/NuZVzG9kH\/+oi9tP4
//		 JcPOYASYZa\/JnISJu1qpAgYX8UPtq8iIko7QEkc+mSJ\/Uw0HOWls9EzG7Tcn\/vqPxH7fcv4i51nFdNQUAoXV\/oXtsRWUhd2rvCBzpNQ\/zSnxE7YGGgx8BQZIGFWo
//		 9qIGHvCLgWESIyi5tG1f2XAczhdPgit+Qyk9d7ktbdmMlIv+hl4G2jhNLvhSandRrvcVcUwfpzcQEfaiLRI7JYYbnt6FP3QW6atKjyXbb7ndV8vFLV1anSdJ0NppNY6qd
//		 HQGovGGtWib9l7kzUpC8GBeu1TUqk2emFGlkcbUG+hI6bSOmFTkogTa1B6d27uyNtR2JWJ4QeCvHTjQCaRJqzwI2zFXlBJpkxvPnrmyPFxQ6\/cHGzSZCAR8VETPg=="}
//	 
	// System.out.println(decryptOpenSSL("U2FsdGVkX1\/5I2/7cSGUKk7XUaSwmGSfvST3jlO4SGdVAZyJc4Fr6yCZW6Y080FQRvqiS4CifRh\\/NuZVzG9kH\/+oi9tP4JcPOYASYZa\/JnISJu1qpAgYX8UPtq8iIko7QEkc+mSJ\/Uw0HOWls9EzG7Tcn\/vqPxH7fcv4i51nFdNQUAoXV\/oXtsRWUhd2rvCBzpNQ\/zSnxE7YGGgx8BQZIGFWo9qIGHvCLgWESIyi5tG1f2XAczhdPgit+Qyk9d7ktbdmMlIv+hl4G2jhNLvhSandRrvcVcUwfpzcQEfaiLRI7JYYbnt6FP3QW6atKjyXbb7ndV8vFLV1anSdJ0NppNY6qdHQGovGGtWib9l7kzUpC8GBeu1TUqk2emFGlkcbUG+hI6bSOmFTkogTa1B6d27uyNtR2JWJ4QeCvHTjQCaRJqzwI2zFXlBJpkxvPnrmyPFxQ6\/cHGzSZCAR8VETPg==","db868108-5b65-4477-ab79-618d724eb3b8"));
	 
	 String k =new Encryption256ForPayout().generateMerchantKey();
	 System.out.println(k);
////	 System.out.println(k);
//	 try{
//	 String val = "hello";
//	 String encr = new Encryption256ForPayout().encryptOpenSSL( val,"FebsRla3kDL/8fr3rrpzhhoW/FVyL5uBQIYit8pYDYc=");
//	 System.out.println("encr "+encr );
//	 System.out.println("dec "+ new Encryption256ForPayout().decryptOpenSSL("FebsRla3kDL/8fr3rrpzhhoW/FVyL5uBQIYit8pYDYc=",encr) );
//	 }
//	 catch(Exception e)
//	 {
//		 e.printStackTrace();
//	 }
 }
}

