package com.bhartipay.util;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class XmlParser {
	
	
	public  HashMap<String, String> xmlParser(String xmlvalue){
		
	 HashMap<String, String> bankResp=new HashMap<String, String>();
	 Document xml = convertStringToDocument(xmlvalue);
     Node user = xml.getFirstChild();
     NodeList childs = user.getChildNodes();
     Node child;
     for (int i = 0; i < childs.getLength(); i++) {
         child = childs.item(i);
         System.out.println(child.getNodeName());
         System.out.println(child.getTextContent());
         bankResp.put(child.getNodeName(), child.getTextContent());
     }
	 
	 return bankResp;
	
	}
	
	 private static Document convertStringToDocument(String xmlStr) {
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder;
	        try {
	            builder = factory.newDocumentBuilder();
	            Document doc = builder.parse(new InputSource(new StringReader(
	                    xmlStr)));
	            return doc;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return null;
	    }
	 
	 
	 public static void main(String[] args) {
		new XmlParser().xmlParser("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><impsTransactionResponse><accountNo> </accountNo><category> </category><custNo>5863713</custNo><errorCode>101</errorCode><errorMessage>Invalid Batch For Branch :: 143 and batch :: DMT2 :: dbdRec read error.</errorMessage><lbrCode>143</lbrCode><mobileNo> </mobileNo><nickNameCredit> </nickNameCredit><nickNameDebit> </nickNameDebit><response>ERROR</response><rrnNo>709690000012</rrnNo><stan>00012</stan><URL> </URL><valid>false</valid></impsTransactionResponse>");
	}
}
