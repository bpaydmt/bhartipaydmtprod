package com.bhartipay.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author ambuj.singh
 *
 */
public class ThreadUtil {
	private static int poolSize=0;
	private static ExecutorService pool=null;
	private ThreadUtil(){
		
	}
	/**
	 * 
	 * @return
	 */
	public static ExecutorService getThreadPool(){
		if(pool==null){
			poolSize=Integer.parseInt(InfrastructureProperties.getInstance().getProperty("thread.poolsize"));
			pool=Executors.newFixedThreadPool(poolSize);
		}
		return pool;
	}
	
	
}
