package com.bhartipay.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import com.sun.jersey.core.util.Base64;

public class AEPSUtil {
	
	public static String calculateHmac(String payload, String secret){
		SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes(),"HmacSHA256");
		Mac mac = null;
		try {
		mac = Mac.getInstance("HmacSHA256");
		mac.init(keySpec);
		byte[] result = mac.doFinal(payload.getBytes());
		return new String(Base64.encode(result));
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
		throw new RuntimeException("Exception hashing payload", e);
		}
		}

}
