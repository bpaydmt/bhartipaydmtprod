package com.bhartipay.util;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.SecretKeySpec;

import com.sun.crypto.provider.SunJCE;

import Decoder.BASE64Decoder;

import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
public class AES128BitNew implements PayoutEncryption {
	//PHP
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static String iv              = "fedcba9876543210";
    public String encryptOpenSSL(String valueToEnc, String secretKey) throws IOException, GeneralSecurityException{
        String encryptedValue = null;
        try {
        	
        	IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            Key key = AES128BitNew.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(1, key,ivspec);
            byte[] encValue = c.doFinal(valueToEnc.getBytes("UTF-8"));
            encryptedValue = new String(BhartiPayBase64CoderNew.encode((byte[])encValue));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return encryptedValue;
    }

    public String decryptOpenSSL(String secretKey, String encryptedValue) throws IOException, GeneralSecurityException{
        String decryptedValue = null;
        try {
        	IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            Key key = AES128BitNew.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(2, key,ivspec);
            byte[] decordedValue = BhartiPayBase64CoderNew.decode((String)encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);
            decryptedValue = new String(decValue);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return decryptedValue;
    }

    private static Key generateKeyFromString(String secretKey) throws Exception {
       
        byte[] keyValue=BhartiPayBase64CoderNew.decode((String)secretKey);//Arrays.copyOf(BhartiPayBase64CoderNew.decode((String)secretKey), 32);
        if(keyValue.length > 32)
         keyValue=Arrays.copyOf(BhartiPayBase64CoderNew.decode((String)secretKey), 32);
        
        SecretKeySpec key = new SecretKeySpec(keyValue, "AES");
        return key;
    }

    public static String generateNewKey() {
        String newKey = null;
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128);
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();
            newKey = new String(BhartiPayBase64CoderNew.encode((byte[])raw));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return newKey;
    }
    private static Cipher getCipher(int mode,String salt) throws Exception {
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding", new SunJCE());

        //a random Init. Vector. just for testing
        byte[] iv = "fedcba9876543210".getBytes("UTF-8");

        c.init(mode, generateKey(salt), new IvParameterSpec(iv));
        return c;
    }

    public static String Decrypt(String encrypted,String keysalt,String tech) throws Exception {

        byte[] decodedValue = new BASE64Decoder().decodeBuffer(encrypted);

        Cipher c = getCipher(Cipher.DECRYPT_MODE,keysalt);
        byte[] decValue = c.doFinal(decodedValue);

        return new String(decValue);
    }

    private static Key generateKey(String keysalt) throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        char[] password = "Pass@word1".toCharArray();
        byte[] salt = keysalt.getBytes("UTF-8");

        KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        return new SecretKeySpec(encoded, "AES");

    }

    public static void main(String[] args) throws IOException, GeneralSecurityException {
    
    	//System.out.println(AES128BitNew.generateNewKey());
    	
    	AES128BitNew abc =new AES128BitNew();
//    	System.out.println("encrypt :- "+abc.encryptOpenSSL("{\"beneName\":\"Rohit Singh\",\"accountNo\":\"914010013588220\",\"ifscCode\":\"utib0000278\",\"bankName\":\"Axis Bank\",\"amount\":\"50\",\"transferType\":\"NEFT\",\"merchantTransId\":\"321212123\"}","EgPJMRv9UgaCl2UW5ZV6Mg=="));
//    	System.out.println("decrypt :- "+abc.decryptOpenSSL("G4hq1i17OyFjWUeCY8jzyw==", "RWg4cmFObnR4dnNJU1EzY3MrYW15a0xiVEVndkRWaVpNRnh0UEVHc3piODFSdmFXZmFSMGJ1V01WRURnNlJiQmRKVkdSS05ab0t4eWJjYUNFVHB5V05GUytVck1yM3cwMnFQSjBLUUFGa2RUbThDeDNqNXg5NThTeGlDRTFUNVd4VGFmMVVFTm5DeEtxVVlDQTNTM2VycGhJbGpzZG1iY09YWFEvd29uWWV5S3Raa3FlWXhEOHR1YlZreCtBQWNnd2pTSDJOa0lFSjVDUjNlMVFSbFVOV0tzbHVhdkJsdmZEMmRkNWdvOEdYd3h6Y3FzTUhkdlF3ZXY4dXRQNFB0Yw=="));
    System.out.println(abc.generateNewKey());	
    }
}
