package com.bhartipay.util;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class SmsAndMailUtility extends Thread{

	public static final Logger LOGGER = Logger.getLogger(SmsAndMailUtility.class);

	public String emailMsg;
	public String smsMsg;
	public String mailSub;
	public String mobileNo;
	public String email;
	public String flag;
	public String aggId;
	public String walletid;
	public String sendername;
	public String type;
	
	CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	SMSSendDetails sMSSendDetails=new SMSSendDetails();
	
	public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId,String walletid,String sendername,String type) {
				this.emailMsg = emailMsg;
				this.smsMsg = smsMsg;
				this.mailSub = mailSub;
				this.mobileNo = mobileNo;
				this.email = email;
				this.flag = flag;
				this.aggId = aggId;
				this.walletid = walletid;
				this.sendername = sendername;
				this.type = type;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","SmsAndMailUtility()" 						);
				//LOGGER.debug("******************************************************aggId**********************************************"+aggId);
				
	}


	public void run() {

		if (flag.equalsIgnoreCase("Both")) {
			/*sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);*/
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","thread started for sending sms on "+ mobileNo );
			
			//LOGGER.debug("******************************************************thread started for sending sms on "+ mobileNo + "**********************************************");
			//new CommUtility().sendSms(mobileNo, smsMsg,aggId,walletid,sendername,type);
			
			new CommUtility().sendSmsKaleyra(mobileNo, smsMsg,aggId,walletid,sendername,type);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sms sent on " + mobileNo + "thread started for sending mail on "+ email);
			
			//LOGGER.debug("******************************************************sms sent on " + mobileNo+ "**********************************************");
			//LOGGER.debug("******************************************************thread started for sending mail on "+ email + "**********************************************");
			// SendMail.sendEmail(email,emailMsg);
			new CommUtility().sendMail(email, mailSub, emailMsg, null,aggId,walletid,sendername);
			//LOGGER.debug("******************************************************email sent on " + email+ "**********************************************");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", "email sent on "+ email);
			
		} else if (flag.equalsIgnoreCase("Mail")) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", "thread started for sending mail on "+ email);
			
			//LOGGER.debug("******************************************************thread started for sending mail on "+ email + "**********************************************");
			// SendMail.sendEmail(email,emailMsg);
			new CommUtility().sendMail(email, mailSub, emailMsg, null,aggId,walletid,sendername);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","email sent on " + email);
			
			//LOGGER.debug("******************************************************email sent on " + email+ "**********************************************");
		} else if (flag.equalsIgnoreCase("SMS")) {
			/*sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);*/
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", "thread started for sendingsms on "+ mobileNo);
			
			//LOGGER.debug("******************************************************thread started for sending sms on "+ mobileNo + "**********************************************");
			//new CommUtility().sendSms(mobileNo, smsMsg,aggId,walletid,sendername);
			new CommUtility().sendSmsKaleyra(mobileNo, smsMsg,aggId,walletid,sendername,type);
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", "sms sent on " + mobileNo);
			
			//LOGGER.debug("******************************************************sms sent on " + mobileNo+ "**********************************************");
		}
	}


}
