package com.bhartipay.util;
import com.bhartipay.util.AppnitBase64Coder;
import com.sun.org.apache.bcel.internal.generic.NEW;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;
public class AES128Bit implements PayoutEncryption{

    public String encryptOpenSSL(String valueToEnc, String secretKey) throws IOException, GeneralSecurityException{
        String encryptedValue = null;
        try {
        	
        
            Key key = AES128Bit.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance("AES");
            c.init(1, key);
            byte[] encValue = c.doFinal(valueToEnc.getBytes());
            encryptedValue = new String(AppnitBase64Coder.encode((byte[])encValue));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return encryptedValue;
    }

    public String decryptOpenSSL(String secretKey, String encryptedValue) throws IOException, GeneralSecurityException {
        String decryptedValue = null;
        try {
        	
            Key key = AES128Bit.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance("AES");
            c.init(2, key);
            byte[] decordedValue = AppnitBase64Coder.decode((String)encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);
            decryptedValue = new String(decValue);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return decryptedValue;
    }

    private static Key generateKeyFromString(String secretKey) throws Exception {
        byte[] keyValue = AppnitBase64Coder.decode((String)secretKey);
        byte[] keyValue1=Arrays.copyOf(keyValue, 32);
        SecretKeySpec key = new SecretKeySpec(keyValue1, "AES");
        return key;
    }

    public static String generateNewKey() {
        String newKey = null;
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128);
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();
            newKey = new String(AppnitBase64Coder.encode((byte[])raw));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return newKey;
    }

    public static void main(String[] args) throws IOException, GeneralSecurityException {
  // for DOTNET 
    	System.out.println((new AES128Bit().generateNewKey()));
    	
//    	AES128Bit a=new AES128Bit();
//    	String str = a.decryptOpenSSL("1u6yOop35Ydljtvn3b+Bng==","rb4oZO3yN22/mZFAGCfDrqCrLCIe/C9zHA767Bxcm7P+VVlgkVHWlj/Z3Ds1xzg8m+UD/ofOo1JCdcoq8Fp4LBUnVSW3iB9dPO+RNX1rMS9iL/H+acLeT4UKrbCYT6b5M5t+jJjHuCFu4P0bfYGWdPuMcI2v/dRBl1aN94mCSi8QH/fxTtHZrZfj3lVhFLTSwZEpnsEshfRn3USj8Kl4yPwi/eYDr6KABDloau/ZMOE=");
//    
//    	System.out.println(str);
    }

}
