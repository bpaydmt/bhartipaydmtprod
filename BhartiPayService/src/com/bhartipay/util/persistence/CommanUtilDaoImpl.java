package com.bhartipay.util.persistence;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cme.bean.CMERequestBean;
import com.bhartipay.mudra.bean.BlockedBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.rechargeService.bean.RechargeCircleCode;
import com.bhartipay.rechargeService.bean.RechargeOperatorMasterBean;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.bean.AgentDetailsBean;
import com.bhartipay.util.bean.CashDepositBankDetails;
import com.bhartipay.util.bean.InstallationDetails;
import com.bhartipay.util.bean.InstallationDetailsBean;
import com.bhartipay.util.bean.MailConfigMast;
import com.bhartipay.util.bean.MailSendDetails;
import com.bhartipay.util.bean.MerchantOffersMast;
import com.bhartipay.util.bean.PartnerPrefundMastBean;
import com.bhartipay.util.bean.PaymentGatewayURL;
import com.bhartipay.util.bean.SMSConfigMast;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.bean.SupportTicketBean;
import com.bhartipay.util.bean.UserMenuMapping;
import com.bhartipay.util.bean.UserRoleMapping;
import com.bhartipay.util.bean.VersionControl;
import com.bhartipay.util.bean.VersionStatusBean;
import com.bhartipay.util.bean.WalletAggreatorTxnMast;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.recharge.plan.bean.ISDPlans;
import com.bhartipay.util.recharge.plan.bean.LocalPlans;
import com.bhartipay.util.recharge.plan.bean.SMSPlans;
import com.bhartipay.util.recharge.plan.bean.STDPlans;
import com.bhartipay.util.recharge.plan.bean.TariffPlans;
import com.bhartipay.util.recharge.plan.bean.TopupPlans;

public class CommanUtilDaoImpl implements CommanUtilDao {

	private static final Logger logger = Logger.getLogger(CommanUtilDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;

	public CommanUtilDaoImpl() {
	}

	/**
	 * configureNewWallet method used to create New Wallet Group. Returns Status
	 * Code
	 * 
	 * @param WalletConfiguration object of WalletConfiguration
	 * @return Status Code
	 */

	/*
	 * public String configureNewWallet(WalletConfiguration walletConfiguration){
	 * logger.
	 * info("Start excution ===================== method getWalletConfiguration()");
	 * factory = DBUtil.getSessionFactory(); session = factory.openSession();
	 * Transaction transaction=null; String statusCode = "1001"; try{
	 * transaction=session.beginTransaction(); Criteria
	 * cr=session.createCriteria(WalletConfiguration.class);
	 * cr.add(Restrictions.eq("country", walletConfiguration.getCountry())); List
	 * <WalletConfiguration> results = cr.list(); if(results != null &&
	 * results.size() == 0){ session.save(walletConfiguration);
	 * transaction.commit(); statusCode = "1000"; }else{ statusCode = "7014"; }
	 * }catch (Exception e) { e.printStackTrace(); if (transaction != null)
	 * transaction.rollback();
	 * logger.debug("problem in configureNewWallet==================" +
	 * e.getMessage(), e); } finally { session.close(); }
	 * logger.info("return ===================== method getWalletConfiguration()"
	 * +statusCode); return statusCode; }
	 */

	/**
	 * configureWalletupdate method used to update configuration of Wallet Group.
	 * Returns Status Code
	 * 
	 * @param WalletConfiguration object of WalletConfiguration
	 * @return Status Code
	 */

	public String configureWalletupdate(WalletConfiguration walletConfiguration) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "configureWalletupdate()");

		// logger.info("Start excution ===================== method
		// getWalletConfiguration()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		String statusCode = "1001";
		try {
			transaction = session.beginTransaction();
			session.update(walletConfiguration);
			transaction.commit();
			statusCode = "1000";
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in configureNewWallet" + e.getMessage() + " " + e);
			// logger.debug("problem in configureNewWallet==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"configureWalletupdate" + statusCode);

		// logger.info("return ===================== method
		// configureWalletupdate()"+statusCode);
		return statusCode;
	}

	public WalletConfiguration getWalletConfiguration(String aggreatorid) {

		System.out.println(
				"_______Inside CommonUtilDaoImpl.java___________WalletConfiguration getWalletConfiguration(String aggreatorid)"
						+ aggreatorid);

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getWalletConfiguration()");

		// logger.info("Start excution ===================== method
		// getWalletConfiguration()"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		WalletConfiguration walletConfiguration = new WalletConfiguration();
		String[] txnType;
		try {
			Query query = session.createQuery("from WalletConfiguration where aggreatorid=:aggreatorid");
			query.setString("aggreatorid", aggreatorid);

			List<WalletConfiguration> list = query.list();

			if (list != null && list.size() != 0) {
				walletConfiguration = list.get(0);

				query = session.createQuery("from WalletAggreatorTxnMast where aggreatorId=:aggreatorid");
				query.setString("aggreatorid", aggreatorid);

				List<WalletAggreatorTxnMast> txnCode = query.list();

				System.out.println("__________Aggreatorid________" + aggreatorid);

				txnType = new String[txnCode.size()];

				for (int i = 0; i < txnCode.size(); i++) {
					txnType[i] = txnCode.get(i).getTxnCode();
				}
				walletConfiguration.setTxnType(txnType);

				System.out.println("________txnType____________" + walletConfiguration.getTxnType());
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getWalletConfiguration" + e.getMessage() + " " + e);
	
		} finally {
			 session.close();
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getWalletConfiguration" + walletConfiguration);
		// logger.info("return ===================== method
		// getWalletConfiguration()"+walletConfiguration);

//		walletConfiguration.setAgent("");
//		walletConfiguration.setDomainName("");
//		walletConfiguration.setGcmAppKey("");
//		walletConfiguration.setIpiemi("");
//		walletConfiguration.setPgAgId("");
//		walletConfiguration.setPgCallBackUrl("");
//		walletConfiguration.setPgEncKey("");
//		walletConfiguration.setPgMid("");
//		walletConfiguration.setPgUrl("");

		return walletConfiguration;

	}
	
	
	
	public WalletConfiguration getAepsUpStatus(String aggreatorid) {
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		WalletConfiguration configuration = new WalletConfiguration();
		try {
			Query query = session.createQuery("from WalletConfiguration where aggreatorid=:aggreatorid");
			query.setString("aggreatorid", aggreatorid);

			List<WalletConfiguration> list = query.list();

			if (list != null && list.size() != 0) {
				WalletConfiguration walletConfiguration = list.get(0);
				configuration.setFinoAeps(walletConfiguration.getFinoAeps());
				configuration.setYesAeps(walletConfiguration.getYesAeps());
				configuration.setIciciAeps(walletConfiguration.getIciciAeps());
			}
			return configuration;

		} catch (Exception e) {
		
		} finally {
			 session.close();
		}
		return configuration;

	}

	public WalletMastBean getUserDetailsById(WalletMastBean configbean) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			Criteria criteria = session.createCriteria(WalletMastBean.class);
			criteria.add(Restrictions.eq("id", configbean.getId()));
			criteria.add(Restrictions.eq("aggreatorid", configbean.getAggreatorid()));
			List<WalletMastBean> list = criteria.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}

		} finally {
			session.close();
		}
		return null;
	}

	public String getAggIdByDomain(String domain) {

		System.out.println(
				"-------******------- CommanUtilDaoImpl.java------method name----getAggIdByDomain(String domain)--- "
						+ domain);

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAggIdByDomain()" + "|domain" + domain);
		// logger.info("Start excution ===================== method
		// getAggIdByDomain()"+domain);

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String aggId = "error";
		try {
			String hqlQuery = "from WalletConfiguration where domainName=:domain";
			/*
			 * Query
			 * query=session.createQuery("from WalletConfiguration where domainName=:domain"
			 * );
			 */

			Query query = session.createQuery(hqlQuery);

			query.setString("domain", domain);

			List<WalletConfiguration> list = query.list();

			if (list != null && list.size() != 0) {
				aggId = list.get(0).getAggreatorid();
				System.out.println("----******----aggId----**********---" + aggId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"getAggIdByDomain()" + "|problem in getAggIdByDomai" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggIdByDomain==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAggIdByDomain()" + "|aggIdbydomain" + aggId);
		// logger.info("Start excution ===================== method
		// getAggIdByDomain()"+aggId);
		return aggId;
	}

	public WalletConfiguration saveWalletConfiguration(WalletConfiguration walletConfiguration) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletConfiguration.getAggreatorid(), "", "", "",
				"", "saveWalletConfiguration()" + "|getBannerpic" + walletConfiguration.getBannerpic());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletConfiguration.getAggreatorid(), "", "", "",
				"", "saveWalletConfiguration()" + "| getLogopic" + walletConfiguration.getLogopic());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletConfiguration.getAggreatorid(), "", "", "",
				"", "saveWalletConfiguration()" + "|getTxnType" + walletConfiguration.getTxnType());
		/*
		 * logger.info("Start excution ===================== method getBannerpic"
		 * +walletConfiguration.getBannerpic());
		 * logger.info("Start excution ===================== method getLogopic"
		 * +walletConfiguration.getLogopic()); logger.
		 * info("Start excution ===================== method getTxnType----------"
		 * +walletConfiguration.getTxnType());
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		walletConfiguration.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		WalletAggreatorTxnMast walletAggreatorTxnMast = null;
		try {
			if (walletConfiguration.getDomainName() == null || walletConfiguration.getDomainName().isEmpty()) {
				walletConfiguration.setActionResult("1001");
			}

			session.saveOrUpdate(walletConfiguration);

			if (walletConfiguration.getTxnType().length > 0) {

				String hql = "delete from WalletAggreatorTxnMast where aggreatorId= :aggreatorId";
				session.createQuery(hql).setString("aggreatorId", walletConfiguration.getAggreatorid()).executeUpdate();

				String[] txnCode = walletConfiguration.getTxnType();
				for (int i = 0; i < walletConfiguration.getTxnType().length; i++) {
					walletAggreatorTxnMast = new WalletAggreatorTxnMast();
					walletAggreatorTxnMast.setAggreatorId(walletConfiguration.getAggreatorid());
					walletAggreatorTxnMast.setTxnCode(txnCode[i]);
					session.saveOrUpdate(walletAggreatorTxnMast);
				}

			}
			transaction.commit();
			walletConfiguration.setActionResult("1000");
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletConfiguration.getAggreatorid(), "", "",
					"", "",
					"saveWalletConfiguration)_" + "|problem in saveWalletConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in saveWalletConfiguration==================" +
			// e.getMessage(), e);
			walletConfiguration.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletConfiguration.getAggreatorid(), "", "", "",
				"", "saveWalletConfiguration()" + "|saveWalletConfiguration" + walletConfiguration);
		// logger.info("return ===================== method
		// saveWalletConfiguration()"+walletConfiguration);
		return walletConfiguration;

	}

	public UserMenuMapping saveUserMenuMapping(UserMenuMapping userMenuMapping) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"saveUserMenuMapping()" + "|getUserId" + userMenuMapping.getUserId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"saveUserMenuMapping()" + "|getMenuType" + userMenuMapping.getMenuType());
		// logger.info("Start excution ===================== method
		// getUserId"+userMenuMapping.getUserId());
		// logger.info("Start excution ===================== method
		// getMenuType"+userMenuMapping.getMenuType());
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		userMenuMapping.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		try {
			if (userMenuMapping.getUserId() == null || userMenuMapping.getUserId().isEmpty()) {
				userMenuMapping.setActionResult("1001");
			}
			if (userMenuMapping.getMenuType().length > 0) {
				String[] menuCode = userMenuMapping.getMenuType();
				StringBuilder assignMenu = new StringBuilder();
				for (int i = 0; i < userMenuMapping.getMenuType().length; i++) {
					if (i == 0) {
						assignMenu.append(menuCode[i]);
					} else {
						assignMenu.append("," + menuCode[i]);
					}
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
						"saveUserMenuMapping()" + "|assignMenu" + assignMenu);
				// logger.info("Start excution ===================== method
				// assignMenu"+assignMenu);
				userMenuMapping.setMenuId(assignMenu.toString());
				session.saveOrUpdate(userMenuMapping);
				transaction.commit();
				userMenuMapping.setActionResult("1000");
			} else {
				userMenuMapping.setActionResult("1001");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|problem in saveWalletConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in saveWalletConfiguration==================" +
			// e.getMessage(), e);
			userMenuMapping.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|saveWalletConfiguration" + userMenuMapping);
		// logger.info("return ===================== method
		// saveWalletConfiguration()"+userMenuMapping);
		return userMenuMapping;

	}

	public UserMenuMapping getUserMenuMapping(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "getUserMenuMapping()");

		// logger.info("Start excution ===================== method
		// getUserMenuMapping()"+userId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		UserMenuMapping userMenuMapping = new UserMenuMapping();
		String[] menuType;
		try {
			Query query = session.createQuery("from UserMenuMapping where userId=:userId");
			query.setString("userId", userId);
			List<UserMenuMapping> list = query.list();
			if (list != null && list.size() != 0) {
				userMenuMapping = list.get(0);
				menuType = userMenuMapping.getMenuId().split(",");
				userMenuMapping.setMenuType(menuType);
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|problem in getWalletConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in getWalletConfiguration==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|getWalletConfiguration()" + userMenuMapping);
		// logger.info("return ===================== method
		// getWalletConfiguration()"+userMenuMapping);
		return userMenuMapping;

	}

	public MailConfigMast getMailConfiguration(String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getMailConfiguration()");
		// logger.info("Start excution ===================== method
		// getMailConfiguration()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MailConfigMast mailConfigMast = new MailConfigMast();
		try {
			Query query = session.createQuery("from MailConfigMast where aggreatorid=:aggreatorid");
			query.setString("aggreatorid", aggreatorid);
			List<MailConfigMast> list = query.list();
			if (list != null && list.size() != 0) {
				mailConfigMast = list.get(0);
				mailConfigMast.setPassword(EncryptionDecryption.getdecrypted(mailConfigMast.getPassword(),
						"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getMailConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in getMailConfiguration==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getMailConfiguration" + mailConfigMast);
		// logger.info("return ===================== method
		// getMailConfiguration()"+mailConfigMast);
		return mailConfigMast;

	}

	public MailConfigMast saveMailConfiguration(MailConfigMast mailConfigMast) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mailConfigMast.getAggreatorid(), "", "", "", "",
				"saveMailConfiguration()");
		// logger.info("Start excution ===================== method
		// getAggreatorid"+mailConfigMast.getAggreatorid());

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mailConfigMast.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		try {
			mailConfigMast.setPassword(EncryptionDecryption.getEncrypted(mailConfigMast.getPassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			session.saveOrUpdate(mailConfigMast);
			transaction.commit();
			mailConfigMast.setActionResult("1000");
			mailConfigMast.setPassword(EncryptionDecryption.getdecrypted(mailConfigMast.getPassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mailConfigMast.getAggreatorid(), "", "", "",
					"", "problem in saveMailConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in saveMailConfiguration==================" +
			// e.getMessage(), e);
			mailConfigMast.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mailConfigMast.getAggreatorid(), "", "", "", "",
				"saveMailConfiguration" + mailConfigMast);
		// logger.info("return ===================== method
		// saveMailConfiguration()"+mailConfigMast);
		return mailConfigMast;

	}

	public SMSConfigMast getSMSConfiguration(String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getSMSConfiguration()");
		// logger.info("Start excution ===================== method
		// getSMSConfiguration()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		SMSConfigMast smsConfigMast = new SMSConfigMast();
		try {
			Query query = session.createQuery("from SMSConfigMast where aggreatorid=:aggreatorid");
			query.setString("aggreatorid", aggreatorid);
			List<SMSConfigMast> list = query.list();
			if (list != null && list.size() != 0) {
				smsConfigMast = list.get(0);
				smsConfigMast.setSmspassword(EncryptionDecryption.getdecrypted(smsConfigMast.getSmspassword(),
						"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getSMSConfiguration" + e.getMessage() + " " + e);
			// logger.debug("problem in getSMSConfiguration==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getMailConfiguration" + smsConfigMast);
		// logger.info("return ===================== method
		// getMailConfiguration()"+smsConfigMast);
		return smsConfigMast;

	}

	public SMSConfigMast saveSMSConfiguration(SMSConfigMast smsConfigMast) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), smsConfigMast.getAggreatorid(), "", "", "", "",
				"saveSMSConfiguration()");
		// logger.info("Start excution ===================== method
		// getAggreatorid"+smsConfigMast.getAggreatorid());

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		smsConfigMast.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		try {

			smsConfigMast.setSmspassword(EncryptionDecryption.getEncrypted(smsConfigMast.getSmspassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));

			session.saveOrUpdate(smsConfigMast);
			transaction.commit();
			smsConfigMast.setActionResult("1000");
			smsConfigMast.setSmspassword(EncryptionDecryption.getdecrypted(smsConfigMast.getSmspassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), smsConfigMast.getAggreatorid(), "", "", "",
					"", "problem in saveSMSConfigurati" + e.getMessage() + " " + e);
			logger.debug("problem in saveSMSConfiguration==================" + e.getMessage(), e);
			smsConfigMast.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), smsConfigMast.getAggreatorid(), "", "", "", "",
				"saveSMSConfiguration" + smsConfigMast);
		// logger.info("return ===================== method
		// saveSMSConfiguration()"+smsConfigMast);
		return smsConfigMast;

	}

	public String getTrxId(String name, String aggreatorid) {

		System.out.println("_______________Inside CommonutilDao___________getTrxId()_______________" + aggreatorid);

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getTrxId()" + aggreatorid);
		// logger.info("start excution ===================== method getTrxId(name)");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		String id = "";
		try {
			Query query = session.createSQLQuery("select nextval('" + name + "','" + aggreatorid + "')");

			List list = query.list();
			id = (String) list.get(0);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getTrxId" + e.getMessage() + " " + e);
			// logger.debug("problem in getTrxId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getTrxId(name)" + id);
		// logger.info("return ===================== method getTrxId(name)" + id);
		return id;
	}

	public int checkLoginStatus(String mobile, String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "checkLoginStatus()");
		// logger.info("start excution ===================== method
		// checkLoginStatus(mobile)");
		int loginSts = 0;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {
			Query query = session.createQuery(
					" FROM WalletMastBean WHERE mobileno=:mobile and userstatus='A' and aggreatorid=:aggId");
			query.setString("mobile", mobile);
			query.setString("aggId", aggId);
			List<WalletMastBean> list = query.list();
			if (list != null && list.size() != 0) {
				loginSts = list.get(0).getLoginStatus();
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in checkLoginStatus" + e.getMessage() + " " + e);
			// logger.debug("problem in checkLoginStatus==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"checkLoginStatus" + loginSts);
		// logger.info("return ===================== method
		// checkLoginStatus()"+loginSts);
		return loginSts;

	}

	public String createPassword(int len) {
		final String passwordString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklm!@#$^*nopqrstuvwxyz";
		final String numString = "0123456789";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(passwordString.charAt(rnd.nextInt(passwordString.length())));
		sb.append(passwordString.charAt(rnd.nextInt(numString.length())));
		return sb.toString();
	}

	public String creatempin() {
		final String passwordString = "0123456789";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(4);
		for (int i = 0; i < 4; i++)
			sb.append(passwordString.charAt(rnd.nextInt(passwordString.length())));

		return sb.toString();
	}

	public MailConfigMast getmailDtl(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getmailDtl()");
		// logger.info("Start excution ===================== method getmailDtl()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MailConfigMast mailConfigMast = null;
		try {
			Query query = session.createQuery("from MailConfigMast where aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List list = query.list();

			if (list != null && list.size() > 0) {
				mailConfigMast = (MailConfigMast) list.get(0);
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getmailDtl" + e.getMessage());
			// logger.debug("problem in getmailDtl==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getmailDtl" + mailConfigMast);
		// logger.info("return ===================== method
		// getmailDtl()"+mailConfigMast);
		return mailConfigMast;
	}

	public SMSConfigMast getSMSDtl(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getSMSDtl()" + "|aggId" + aggId);
		logger.info("Start excution ===================== method getSMSDtl()" + aggId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		SMSConfigMast sMSConfigMast = null;
		try {
			Query query = session.createQuery("from SMSConfigMast where aggreatorid=:aggId");
			query.setString("aggId", aggId);
			List list = query.list();
			if (list != null && list.size() > 0) {
				sMSConfigMast = (SMSConfigMast) list.get(0);
			}

		} catch (Exception e) {

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getSMSDt" + e.getMessage() + " " + e);
			logger.debug("problem in getSMSDtl==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getSMSDtl" + sMSConfigMast);
		// logger.info("return ===================== method getSMSDtl()"+sMSConfigMast);
		return sMSConfigMast;
	}

	public String getNotificationTemplet(String tampName) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getNotificationTemplet()");
		// logger.info("Start excution ===================== method
		// getNotificationTemplet()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String notificationtemplet = null;
		try {
			notificationtemplet = (String) session
					.createSQLQuery("select description from notificationmast where notificationid='" + tampName + "'")
					.uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|problem in getNotificationTemplet" + e.getMessage() + " " + e);
			// logger.debug("problem in getNotificationTemplet==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|getNotificationTemplet" + notificationtemplet);
		// logger.info("return ===================== method
		// getNotificationTemplet()"+notificationtemplet);
		return notificationtemplet;
	}

	public String getmailTemplet(String tampName, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getmailTemplet()" + "|" + tampName);
		// logger.info("Start excution ===================== method
		// getmailTemplet()"+tampName);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String mailtemplet = null;
		try {
			mailtemplet = (String) session.createSQLQuery("select description from mailtemplates where tempnaration='"
					+ tampName + "'and aggreatorid='" + aggreatorid + "'").uniqueResult();
			if (mailtemplet == null || mailtemplet.isEmpty()) {
				mailtemplet = (String) session
						.createSQLQuery("select description from mailtemplates where tempnaration='" + tampName
								+ "'and aggreatorid='admin'")
						.uniqueResult();
			}
		} catch (Exception e) {
			// System.out.println(e);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getmailTemplet" + e.getMessage() + " " + e);
			// logger.debug("problem in getmailTemplet==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getmailTemplet" + mailtemplet);
		// logger.info("return ===================== method
		// getmailTemplet()"+mailtemplet);
		return mailtemplet;
	}

	public String getsmsTemplet(String tampName, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getsmsTemplet()" + "|" + tampName);
		// logger.info("Start excution ===================== method
		// getsmsTemplet()"+tampName);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String smstemplet = null;
		try {
			smstemplet = (String) session.createSQLQuery("select description from smstemplates where smsnaration='"
					+ tampName + "' and aggreatorid='" + aggreatorid + "'").uniqueResult();
			if (smstemplet == null || smstemplet.isEmpty()) {
				smstemplet = (String) session.createSQLQuery("select description from smstemplates where smsnaration='"
						+ tampName + "' and aggreatorid='admin'").uniqueResult();
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getsmsTemplet" + e.getMessage() + " " + e);
			// logger.debug("problem in getsmsTemplet==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getmailTemplet" + smstemplet);
		// logger.info("return ===================== method
		// getsmsTemplet()"+smstemplet);
		return smstemplet;
	}

	public synchronized String generateWalletID(String mobileno) {
		String walletId = null;
		Calendar date = Calendar.getInstance();
		date.setTime(new Date());
		Format f1 = new SimpleDateFormat("ddMMyyyyHHmmss");
		walletId = mobileno + f1.format(date.getTime());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", walletId, "", "", "", "generateWalletID");
		// logger.info("Inside ====================generateWalletID=== return====" +
		// walletId);
		return walletId;
	}

	public Map<String, String> getCountry() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getCountry()");
		// logger.info("Start excution ===================== method getCountry()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> country = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT id,NAME FROM countrycurrencymast WHERE STATUS=1");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				country.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getCountry" + e.getMessage() + " " + e);
			// logger.debug("problem in getCountry==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "Country " + country);
		// logger.info("result ===================== method getCountry()"+country);
		return sortByValue(country);

	}

	public Map<String, String> getCountryCode() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getCountryCode()");
		// logger.info("Start excution ===================== method getCountryCode()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> country = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT countryid,NAME FROM countrycurrencymast WHERE STATUS=1");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				country.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getCountryCode" + e.getMessage() + " " + e);
			// logger.debug("problem in getCountryCode==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "CountryCode" + country);
		// logger.info("result ===================== method getCountryCode()"+country);
		return sortByValue(country);

	}

	public String getcurrencyByCountryId(int countryId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getcurrencyByCountryId()" + "|countryId " + countryId);
		// logger.info("Start excution ===================== method
		// getcurrencyByCountryId()"+countryId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String currency = null;
		try {
			currency = (String) session.createSQLQuery("SELECT currency FROM countrycurrencymast WHERE id=:id")
					.setInteger("id", countryId).uniqueResult();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"getcurrencyByCountryId()" + "|currency " + currency);
			// logger.info("
			// currency=====================================================================================
			// "+currency);
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getcurrencyByCountryId " + e.getMessage() + " " + e);
			// logger.debug("problem in getcurrencyByCountryId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getcurrencyByCountryId " + currency);
		// logger.info("result ===================== method
		// getcurrencyByCountryId()"+currency);
		return currency;

	}

	public String getcurrencyByCountryCode(String countryId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getcurrencyByCountryCode()" + "|countryId " + countryId);
		// logger.info("Start excution ===================== method
		// getcurrencyByCountryCode()"+countryId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String currency = null;
		try {
			currency = (String) session.createSQLQuery("SELECT currency FROM countrycurrencymast WHERE countryid=:id")
					.setString("id", countryId).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"getcurrencyByCountryCode()" + "|currency " + currency);
			// logger.info("
			// currency=====================================================================================
			// "+currency);
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|problem in getcurrencyByCountryCode " + e.getMessage() + " " + e);
			// logger.debug("problem in getcurrencyByCountryCode==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|currencyByCountryCode" + currency);
		// logger.info("result ===================== method
		// getcurrencyByCountryCode()"+currency);
		return currency;

	}

	public Map<String, String> getKyc() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getKyc()");
		// logger.info("Start excution ===================== method getKyc()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> kyc = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT kycid,NAME FROM kycmast WHERE STATUS=1 AND TYPE IN (1,2)");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				kyc.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getKyc" + e.getMessage() + " " + e);
			// logger.debug("problem in getKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"Total Number of KYC" + kyc.size());
		// logger.info("Total Number of KYC ===================== method
		// getKyc()"+kyc.size());

		return sortByValue(kyc);
	}

	public Map<String, String> getKycType() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getKyc()");
		// logger.info("Start excution ===================== method getKyc()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> kyc = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT kycid,NAME FROM kycmast WHERE STATUS=1 AND TYPE IN (1,2)");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				kyc.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getKyc" + e.getMessage() + " " + e);
			// logger.debug("problem in getKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"Total Number of KYC" + kyc.size());
		// logger.info("Total Number of KYC ===================== method
		// getKyc()"+kyc.size());

		return sortByValue(kyc);
	}

	public Map<String, String> getIdProofKyc() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getIdProofKyc()");
		// logger.info("Start excution ===================== method getKyc()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> idProofKyc = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT kycid,NAME FROM kycmast WHERE STATUS=1 AND TYPE IN (1,3)");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				idProofKyc.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getKyc" + e.getMessage() + " " + e);
			// logger.debug("problem in getKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"Total Number of KYC" + idProofKyc.size());
		// logger.info("Total Number of KYC ===================== method
		// getKyc()"+idProofKyc.size());

		return sortByValue(idProofKyc);

	}

	// Function for Shorted by value
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	public InstallationDetailsBean saveInstallationDetails(InstallationDetailsBean installationDetailsBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "saveInstallationDetails()");
		// logger.info("Start excution ===================== method
		// saveInstallationDetails(installationDetailsBean)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		installationDetailsBean.setStatus("1001");
		try {
			if (installationDetailsBean.getImei() == null || installationDetailsBean.getImei().isEmpty()) {
				installationDetailsBean.setStatus("7037");
				return installationDetailsBean;
			}
			InstallationDetailsBean iDetails = (InstallationDetailsBean) session.get(InstallationDetailsBean.class,
					installationDetailsBean.getImei());

			if (iDetails != null && !(iDetails.getImei().isEmpty())) {
				installationDetailsBean.setCount(iDetails.getCount() + 1);
				session.update(installationDetailsBean);
				transaction.commit();
				installationDetailsBean.setStatus("7039");
			} else {
				installationDetailsBean.setCount(1);
				session.save(installationDetailsBean);
				transaction.commit();
				installationDetailsBean.setStatus("7038");
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in saveInstallationDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in saveInstallationDetails==================" +
			// e.getMessage(), e);
			installationDetailsBean.setStatus("7000");
		} finally {
			session.close();
		}
		return installationDetailsBean;
	}

	public String getOperatorCircle(String numberSeries) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getOperatorCircle()");
		// logger.info("Start excution ===================== method
		// getOperatorCircle()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String operatorCircle = "FAIL|FAIL";
		try {

			SQLQuery query = session
					.createSQLQuery("SELECT operator_name,circle FROM operator_series WHERE series=:numberSeries");
			query.setParameter("numberSeries", numberSeries);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				operatorCircle = row[0].toString() + "|" + row[1].toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getOperatorCircle" + e.getMessage() + " " + e);
			// logger.debug("problem in getOperatorCircle==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return operatorCircle;

	}
	
	//*******************developed by Ankit Budhiraja***********************
	
	public String getPostPaidBillerDetails(RechargeTxnBean rechargeTxnBean) {
		 String jsonResult = "";
			System.out.println("________________________CommonUtilDaoImpl getBillerDetails()_________");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "getBillerDetails()");
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			try {
	
				List list=new ArrayList<>();
				Query query = session.createQuery("from RechargeOperatorMasterBean where name=:name and flag=1 ");
				query.setString("name", rechargeTxnBean.getRechargeOperator());
			    list = query.list();
	            for(int i=0; i<list.size(); i++)
	            {
	              RechargeOperatorMasterBean rechargeOp = (RechargeOperatorMasterBean)list.get(i); 
	              rechargeTxnBean.setRechargeOperator(rechargeOp.getOperatorCode());
	              //rechargeTxnBean.setRechargeOperator("PAT");
	            }
				
				try {
					
//String urlString="https://www.mplan.in/api/Bsnl.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel=9810137521&operator=Airtel";
										
					//String urlString = "https://myrc.in/plan_api/electricity?username=501811&token=a31f4943a2ae535f1b729fa3de2fcb4c&number="+rechargeTxnBean.getRechargeNumber()+"&opcode="+rechargeOperatorMasterBean.getCode();
					String urlString="https://www.mplan.in/api/Bsnl.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&operator="+rechargeTxnBean.getRechargeOperator();
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "postpaid mobile()"+urlString);
					
					URL url = new URL(urlString);
			        HttpsURLConnection urlconnection = (HttpsURLConnection) url.openConnection();
			        urlconnection.setRequestMethod("POST");
			        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			        urlconnection.setDoOutput(true);
			        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
			       // outa.write(postData);
			        outa.close();
			        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
			   
			        StringBuilder sb = new StringBuilder();
			        
			    	if (in != null) {
						int cp;
						while ((cp = in.read()) != -1) {
							sb.append((char) cp);
						}
						in.close();
					}
			    	in.close();
			    	org.json.JSONObject json =new org.json.JSONObject(sb.toString());
			    	jsonResult = json.toString();
				System.out.println("response for PostPaid biller details:  "+jsonResult);	
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in getBillerDetail"+ e.getMessage()+" "+e);
				
				System.out.println("------****** Exception Message ----"+e.getMessage());
			} finally {
				session.close();
			}
			return jsonResult;
		
	 }

	public String getGasBillerDetails(RechargeTxnBean rechargeTxnBean) {
		 String jsonResult = "";
		 String operator=null;
			System.out.println("________________________CommonUtilDaoImpl getGasBillerDetails()_________");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "getGasBillerDetails()");
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			try {
	
				
				try {
					
					String getOperator=rechargeTxnBean.getRechargeOperator();
					
				
					
				/*	
					if(getOperator.equals("Indraprastha Gas")) {
						operator="IPGL";
						
					}
					
					if(getOperator.equals("Gujarat Gas")) {
						operator="GJGL";
						
					}
					
					if(getOperator.equals("Adani Gas")) {
						operator="ADGL";
						
					}
					
					if(getOperator.equals("Mahanagar Gas")) {
						operator="MHGL";
						
					}
				*/
					
				/*
				 * if(getOperator.equals("Municipal Corporation of Gurugram")) {
				 * operator="GJGL";
				 * 
				 * }
				 */
					List list=new ArrayList<>();
					Query query = session.createQuery("from RechargeOperatorMasterBean where name=:name and flag=1 ");
					query.setString("name", rechargeTxnBean.getRechargeOperator());
				    list = query.list();
		            for(int i=0; i<list.size(); i++)
		            {
		              RechargeOperatorMasterBean rechargeOp = (RechargeOperatorMasterBean)list.get(i); 
		              rechargeTxnBean.setRechargeOperator(rechargeOp.getOperatorCode());
		              //rechargeTxnBean.setRechargeOperator("PAT");
		            }
					
 String urlString="https://www.mplan.in/api/Gas.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&operator="+rechargeTxnBean.getRechargeOperator();
					            
		            
		            //String urlString = "https://myrc.in/plan_api/electricity?username=501811&token=a31f4943a2ae535f1b729fa3de2fcb4c&number="+rechargeTxnBean.getRechargeNumber()+"&opcode="+rechargeOperatorMasterBean.getCode();
					//String urlString="https://www.mplan.in/api/Gas.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&operator="+operator;
					URL url = new URL(urlString);
			        HttpsURLConnection urlconnection = (HttpsURLConnection) url.openConnection();
			        urlconnection.setRequestMethod("POST");
			        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			        urlconnection.setDoOutput(true);
			        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
			       // outa.write(postData);
			        outa.close();
			        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
			   
			        StringBuilder sb = new StringBuilder();
			        
			    	if (in != null) {
						int cp;
						while ((cp = in.read()) != -1) {
							sb.append((char) cp);
						}
						in.close();
					}
			    	in.close();
			    	org.json.JSONObject json =new org.json.JSONObject(sb.toString());
			    	jsonResult = json.toString();
				System.out.println("response for Gas biller details:  "+jsonResult);	
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in getGasBillerDetail"+ e.getMessage()+" "+e);
				
				System.out.println("------****** Exception Message ----"+e.getMessage());
			} finally {
				session.close();
			}
			return jsonResult;
		
	 }

	public String getInsuranceBillerDetails(RechargeTxnBean rechargeTxnBean) {
		 String jsonResult = "";
		 String operator=null;
			System.out.println("________________________CommonUtilDaoImpl getGasBillerDetails()_________");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "getInsuranceBillerDetails()");
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			try {
	
				
				try {
					System.out.println("DOB for policy is :"+rechargeTxnBean.getOther());
					
					String getOperator=rechargeTxnBean.getRechargeOperator();
					
					if(getOperator.equals("Tata AIA Insurance")) {
						operator="TALI";
						
					}
					
					if(getOperator.equals("ICICI Prudential Insurance")) {
						operator="ICIC";
						
					}
				
				List list=new ArrayList<>();
				Query query = session.createQuery("from RechargeOperatorMasterBean where name=:name and flag=1 ");
				query.setString("name", rechargeTxnBean.getRechargeOperator());
			    list = query.list();
	            for(int i=0; i<list.size(); i++)
	            {
	              RechargeOperatorMasterBean rechargeOp = (RechargeOperatorMasterBean)list.get(i); 
	              rechargeTxnBean.setRechargeOperator(rechargeOp.getOperatorCode());
	              //rechargeTxnBean.setRechargeOperator("PAT");
	            }
	        	
String urlString="https://www.mplan.in/api/insurance.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&mob="+rechargeTxnBean.getMobileNumber()+"&operator="+rechargeTxnBean.getRechargeOperator()+"&dob="+rechargeTxnBean.getOther();
					            
	            
					//String urlString = "https://myrc.in/plan_api/electricity?username=501811&token=a31f4943a2ae535f1b729fa3de2fcb4c&number="+rechargeTxnBean.getRechargeNumber()+"&opcode="+rechargeOperatorMasterBean.getCode();
				//	String urlString="https://www.mplan.in/api/insurance.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&mob="+rechargeTxnBean.getMobileNumber()+"&operator="+operator+"&dob="+rechargeTxnBean.getOther();
					URL url = new URL(urlString);
			        HttpsURLConnection urlconnection = (HttpsURLConnection) url.openConnection();
			        urlconnection.setRequestMethod("POST");
			        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			        urlconnection.setDoOutput(true);
			        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
			       // outa.write(postData);
			        outa.close();
			        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
			   
			        StringBuilder sb = new StringBuilder();
			        
			    	if (in != null) {
						int cp;
						while ((cp = in.read()) != -1) {
							sb.append((char) cp);
						}
						in.close();
					}
			    	in.close();
			    	org.json.JSONObject json =new org.json.JSONObject(sb.toString());
			    	jsonResult = json.toString();
				System.out.println("response for Insurance Biller details:  "+jsonResult);	
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in getGasBillerDetail"+ e.getMessage()+" "+e);
				
				System.out.println("------****** Exception Message ----"+e.getMessage());
			} finally {
				session.close();
			}
			return jsonResult;
		
	 }
	
	public String getBillerDetails(RechargeTxnBean rechargeTxnBean) {
		 String jsonResult = "";
			System.out.println("________________________CommonUtilDaoImpl getBillerDetails()_________");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "getBillerDetails()");
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			try {
//				SQLQuery query = session.createSQLQuery(
//						"SELECT code FROM rechargeoperatormaster WHERE name='"+rechargeTxnBean.getOperatorName()+"' AND flag=1");
//				List<Object[]> rows = query.list();
//				if(rows!=null && rows.size()>0) {
//				Object[] obj =	rows.get(0);
//					
//				}
			/*
			 * RechargeOperatorMasterBean rechargeOperatorMasterBean = null; Criteria
			 * criteria = session.createCriteria(RechargeOperatorMasterBean.class);
			 * criteria.add(Restrictions.eq("name", rechargeTxnBean.getRechargeOperator()));
			 * criteria.add(Restrictions.eq("flag", 1)); List<RechargeOperatorMasterBean>
			 * rMaster = criteria.list(); if(rMaster != null) { rechargeOperatorMasterBean =
			 * rMaster.get(0);
			 * 
			 * }
			 */
				
				
				try {
					
					String getOperator=rechargeTxnBean.getRechargeOperator();
					String operatorName=null;
					//if(getOperator.equals("Jaipur Vidyut Vitran Nigam RAJASTHAN")) {
					//	operatorName="";
					//}
					
				//	if(getOperator.equals("Madhya Kshetra Vitaran MADHYA PRADESH")){
					//	operatorName="";	
					//}
					//if(getOperator.equals("MSEDC MAHARASHTRA")) {
						//operatorName="";
					//}
				//	if(getOperator.equals("Noida Power NOIDA")) {
					//	operatorName="";
					//}
/*					
					if(getOperator.equals("Paschim Kshetra Vitaran MADHYA PRADESH")) {
						operatorName="MPPKVVCL";
					}
					//if(getOperator.equals("Southern Power ANDHRA PRADESH")) {
					//	operatorName="";
					//}
					//if(getOperator.equals("Southern Power TELANGANA")) {
					//	operatorName="";
					//}
					if(getOperator.equals("Torrent Power")) {
						operatorName="TORRENTAHME";
					}
					if(getOperator.equals("DGVCL GUJARAT")) {
						operatorName="DGVCL";
					}
					if(getOperator.equals("UPPCL URBAN UTTAR PRADESH")) {
						operatorName="UPPCL";
					}
					if(getOperator.equals("UPPCLR RURAL UTTAR PRADESH")) {
						operatorName="UPPCLR";
					}
					//if(getOperator.equals("CSEB CHHATTISGARH")) {
						//operatorName="";
				//	}
					if(getOperator.equals("CESC WEST BENGAL")) {
						operatorName="CESC";
					}
					if(getOperator.equals("BSES Rajdhani Power Limited Delhi")) {
						operatorName="BSESR";
					}
					if(getOperator.equals("BSES Yamuna Power Limited Delhi")) {
						operatorName="BSESY";
					}
					if(getOperator.equals("Tata Power Delhi Limited Delhi")) {
						operatorName="TATA";
					}
					if(getOperator.equals("Reliance Energy Limited")) {
						operatorName="REL IANCE";
					}
					//if(getOperator.equals("North Bihar Electricity")) {
						//operatorName="";
					//}

*/
					List list=new ArrayList<>();
					Query query = session.createQuery("from RechargeOperatorMasterBean where name=:name and flag=1 ");
					query.setString("name", rechargeTxnBean.getRechargeOperator());
				    list = query.list();
		            for(int i=0; i<list.size(); i++)
		            {
		              RechargeOperatorMasterBean rechargeOp = (RechargeOperatorMasterBean)list.get(i); 
		              rechargeTxnBean.setRechargeOperator(rechargeOp.getOperatorCode());
		              //rechargeTxnBean.setRechargeOperator("PAT");
		            }
		            Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "List RechargeOperatorMasterBean "+rechargeTxnBean+"   size "+list.size());
					     
String urlString="https://www.mplan.in/api/electricinfo.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&operator="+rechargeTxnBean.getRechargeOperator();
					
          Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "Request data "+urlString, "", "",  "List RechargeOperatorMasterBean "+rechargeTxnBean+"   size "+list.size());
        
//String urlString="https://www.mplan.in/api/electricinfo.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel=101168544&operator=BSESY";					
					
					//String urlString = "https://myrc.in/plan_api/electricity?username=501811&token=a31f4943a2ae535f1b729fa3de2fcb4c&number="+rechargeTxnBean.getRechargeNumber()+"&opcode="+rechargeOperatorMasterBean.getCode();
					//String urlString="https://www.mplan.in/api/electricinfo.php?apikey=777b0a4cb9f867fc1e9394efebe9d1d2&offer=roffer&tel="+rechargeTxnBean.getRechargeNumber()+"&operator="+operatorName;
					URL url = new URL(urlString);
			        HttpsURLConnection urlconnection = (HttpsURLConnection) url.openConnection();
			        urlconnection.setRequestMethod("POST");
			        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			        urlconnection.setDoOutput(true);
			        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
			       // outa.write(postData);
			        outa.close();
			        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
			   
			        StringBuilder sb = new StringBuilder();
			        
			    	if (in != null) {
						int cp;
						while ((cp = in.read()) != -1) {
							sb.append((char) cp);
						}
						in.close();
					}
			    	//{"tel":"151422191","operator":"BSESY","records":[{"CustomerName":"ANURAG MISHRA","BillNumber":"151422191","Billdate":"01-01-1990","Billamount":"9220.00","Duedate":"28-12-2020","status":1}],"status":1}
			    	in.close();
			    	org.json.JSONObject json =new org.json.JSONObject(sb.toString());
			    	jsonResult = json.toString();
				System.out.println("response for electricity biller details Response Data :  "+jsonResult);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "success Biller Detail"+jsonResult+"        "+sb);
					
				
				}catch (Exception e) {
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in catch block"+ e.getMessage());
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in getBillerDetail"+ e.getMessage()+" "+e);
				
				System.out.println("------****** Exception Message ----"+e.getMessage());
			} finally {
				session.close();
			}
			return jsonResult;
		
	 }	
	
	//***********************End  by Ankit Budhiraja**********************************

	public HashMap<String, HashMap> getRechargeOperator() {

		System.out.println(
				"________________________CommonUtilDaoImpl.java_____________________getRechargeOperator()_________");

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getRechargeOperator()");
		// logger.info("Start excution ===================== method
		// getRechargeOperator()");

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		HashMap<String, HashMap> operator = new HashMap<String, HashMap>();

		HashMap<String, String> prePaid = new HashMap<String, String>();
		HashMap<String, String> postPaid = new HashMap<String, String>();
		HashMap<String, String> dth = new HashMap<String, String>();
		HashMap<String, String> landLine = new HashMap<String, String>();
		HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
		HashMap<String, String> postPaidDataCard = new HashMap<String, String>();
		HashMap<String, String> electricity = new HashMap<String, String>();
		HashMap<String, String> gas = new HashMap<String, String>();
		HashMap<String, String> insurance = new HashMap<String, String>();
		HashMap<String, String> circle = new HashMap<String, String>();

		try {
			RechargeOperatorMasterBean rechargeOp=null;
			RechargeCircleCode circles=null;
			List list=new ArrayList<>();
			Query query = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
			query.setString("serviceType", "PREPAID");
		    list = query.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              prePaid.put(rechargeOp.getName(), rechargeOp.getName());
            }
 
            Query posrQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            posrQuery.setString("serviceType", "POSTPAID");
		    list = posrQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              postPaid.put(rechargeOp.getName(), rechargeOp.getName());
            }
            
            Query dthQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            dthQuery.setString("serviceType", "DTH");
		    list = dthQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              dth.put(rechargeOp.getName(), rechargeOp.getName());
            }
            
            Query landLineQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            landLineQuery.setString("serviceType", "LANDLINE");
		    list = landLineQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              landLine.put(rechargeOp.getName(), rechargeOp.getName());
            }
            
            Query prePaidDataCardQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            prePaidDataCardQuery.setString("serviceType", "Datacard");
		    list = prePaidDataCardQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              prePaidDataCard.put(rechargeOp.getName(), rechargeOp.getName());
            } 
            
            Query postPaidDataCardQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            postPaidDataCardQuery.setString("serviceType", "Postpaid-Datacard");
		    list = postPaidDataCardQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              postPaidDataCard.put(rechargeOp.getName(), rechargeOp.getName());
            } 

            Query electricityQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            electricityQuery.setString("serviceType", "Electricity");
		    list = electricityQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              electricity.put(rechargeOp.getName(), rechargeOp.getName());
            }

            Query gasQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            gasQuery.setString("serviceType", "Gas");
		    list = gasQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              gas.put(rechargeOp.getName(), rechargeOp.getName());
            }
            
            Query insuranceQuery = session.createQuery("from RechargeOperatorMasterBean where serviceType=:serviceType and flag=1 ");
            insuranceQuery.setString("serviceType", "Insurance");
		    list = insuranceQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              rechargeOp=(RechargeOperatorMasterBean)list.get(i);
              insurance.put(rechargeOp.getName(), rechargeOp.getName());
            } 
            
            Query circleQuery = session.createQuery("from RechargeCircleCode");
            list = circleQuery.list();
            for(int i=0; i<list.size(); i++)
            {
              circles=(RechargeCircleCode)list.get(i);
              circle.put(circles.getCircleName(), circles.getCircleName());
            } 

			

			operator.put("PREPAID", prePaid);
			operator.put("POSTPAID", postPaid);
			operator.put("DTH", dth);
			operator.put("LANDLINE", landLine);
			operator.put("PREPAIDDATACARD", prePaidDataCard);
			operator.put("POSTPAIDDATACARD", postPaidDataCard);
			operator.put("ELECTRICITY", electricity);
			operator.put("GAS", gas);
			operator.put("INSURANCE", insurance);

			operator.put("CIRCLE", circle);

			Iterator<String> itr = operator.keySet().iterator();
			while (itr.hasNext()) {

				System.out.println("________________operator____________" + itr.next());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getRechargeOperator" + e.getMessage() + " " + e);

			System.out.println("------****** Exception Message ----" + e.getMessage());
			// logger.debug("problem in getRechargeOperator==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}

		return operator;
	}

	/*
	 * public String getAggByUserId(String userId){ logger.
	 * info("start excution*********************************************************** method getAggByUserId(userId)"
	 * +userId); factory=DBUtil.getSessionFactory(); Session session =
	 * factory.openSession(); String aggId="0"; try{ Query query=session.
	 * createQuery("from WalletMastBean where (id=:id OR mobileno=:id OR emailid=:id)"
	 * ); query.setString("id", userId); List <WalletMastBean> list=query.list();
	 * aggId=list.get(0).getAggreatorid(); if(aggId==null ||aggId.equals("-1"))
	 * aggId="0";
	 * 
	 * }catch(Exception e){ logger.
	 * debug("problem in getAggByUserId***********************************************************"
	 * + e.getMessage(), e); e.printStackTrace(); transaction.rollback(); aggId="0";
	 * } finally { session.close(); } logger.
	 * info(" *****************************Return Status******************************"
	 * +aggId); return aggId;
	 * 
	 * }
	 */

	public void saveSMSSendDetails(SMSSendDetails sMSSendDetails) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"saveSMSSendDetails()" + "|" + sMSSendDetails.getRecipientno());
		// logger.info("start
		// excution*********************************************************** method
		// saveSMSSendDetails(sMSSendDetails)"+sMSSendDetails.getRecipientno());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			sMSSendDetails.setEntryon(new Date());
			session.save(sMSSendDetails);
			transaction.commit();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in saveSMSSendDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// saveSMSSendDetails***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();

		} finally {
			session.close();
		}
	}

	public void saveMailSendDetails(MailSendDetails mailSendDetails) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"saveMailSendDetails()" + "|" + mailSendDetails.getEmailId());

		// logger.info("start
		// excution*********************************************************** method
		// saveMailSendDetails(sMSSendDetails)"+mailSendDetails.getEmailId());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			mailSendDetails.setEntryon(new Date());
			session.save(mailSendDetails);
			transaction.commit();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in saveMailSendDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// saveMailSendDetails***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();

		} finally {
			session.close();
		}
	}

	public SupportTicketBean saveUpdateSupportTicket(SupportTicketBean supportTicketBean, String ipiemi, String agent) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), supportTicketBean.getAggreatorId(), "",
				supportTicketBean.getUserId(), "", "", "saveUpdateSupportTicket()");
		// logger.info("start
		// excution*********************************************************** method
		// saveUpdateSupportTicket(sMSSendDetails)"+supportTicketBean.getAggreatorId());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		supportTicketBean.setStatusCode("1001");
		Transaction transaction = session.beginTransaction();
		try {
			supportTicketBean.setIpiemi(ipiemi);
			supportTicketBean.setUserAgent(agent);
			supportTicketBean.setStatus("Pending");
			session.save(supportTicketBean);
			transaction.commit();
			supportTicketBean.setStatusCode("1000");
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), supportTicketBean.getAggreatorId(), "",
					supportTicketBean.getUserId(), "", "",
					"problem in saveUpdateSupportTicket" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// saveUpdateSupportTicket***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
			supportTicketBean.setStatusCode("7000");
		} finally {
			session.close();
		}

		return supportTicketBean;
	}

	public List<SupportTicketBean> getSupportTicket(String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getSupportTicket()");
		// logger.info("start
		// excution*********************************************************** method
		// getSupportTicket(aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<SupportTicketBean> ticket = new ArrayList<SupportTicketBean>();
		try {
			Criteria cr = session.createCriteria(SupportTicketBean.class);
			cr.add(Restrictions.eq("aggreatorId", aggreatorid));
			cr.add(Restrictions.eq("status", "Pending"));
			ticket = cr.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getSupportTicket" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getSupportTicket***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return ticket;
	}

	public List<SupportTicketBean> getSupportTicketList(String aggreatorid, String userId, String stDate,
			String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getSupportTicketList()");
		// logger.info("start
		// excution*********************************************************** method
		// getSupportTicket(aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<SupportTicketBean> ticket = new ArrayList<SupportTicketBean>();
		try {

			StringBuffer query = new StringBuffer("from SupportTicketBean where aggreatorId=:aggreatorId ");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			if (userId != null && !userId.isEmpty()) {
				query.append(" and userId=:userId");
			}

			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				// query.append(" limit 100");
			} else {
				query.append(
						" and DATE(entryon) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");

			}
			query.append(" order by date(entryon) desc");

			Query hql = session.createQuery(query.toString());
			hql.setParameter("aggreatorId", aggreatorid);
			if (userId != null && !userId.isEmpty()) {
				hql.setParameter("userId", userId);
			}
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				hql.setFetchSize(100);
			}

			ticket = hql.list();

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getSupportTicketList" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getSupportTicket***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return ticket;
	}

	public SupportTicketBean updateSupportTicket(int ticketId, String remarks) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"updateSupportTicket()" + "|ticketId " + ticketId);
		// logger.info("start
		// excution*********************************************************** method
		// updateSupportTicket(ticketId)"+ticketId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		SupportTicketBean supportTicketBean = new SupportTicketBean();
		Transaction transaction = session.beginTransaction();
		supportTicketBean.setStatusCode("1001");
		try {
			supportTicketBean = (SupportTicketBean) session.get(SupportTicketBean.class, ticketId);
			supportTicketBean.setStatus("Closed");
			supportTicketBean.setRemarks(remarks);
			session.update(supportTicketBean);
			transaction.commit();
			supportTicketBean.setStatusCode("1000");
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in updateSupportTicket" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// updateSupportTicket***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
			supportTicketBean.setStatusCode("7000");
		} finally {
			session.close();
		}
		return supportTicketBean;
	}

	public VersionControl getVersionControl(String aggId, int versionId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getVersionControl()" + "|aggId " + aggId + "|versionId " + versionId);
		// logger.info("start
		// excution*********************************************aggId**************
		// method getVersionControl( aggId,versionId)"+aggId);
		// logger.info("start
		// excution*********************************************versionId**************
		// method getVersionControl( aggId,versionId)"+versionId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String vesrionStatus;
		VersionControl versionControl = new VersionControl();
		versionControl.setStatusCode("1001");
		versionControl.setStatusMsg("Failed");
		try {
			Criteria cr = session.createCriteria(VersionControl.class);
			cr.add(Restrictions.eq("aggreatorId", aggId));
			cr.add(Restrictions.eq("versionId", versionId));
			List<VersionControl> list = cr.list();
			if (list != null && list.size() > 0) {
				vesrionStatus = list.get(0).getVersionStatus();
				versionControl.setVersionStatus(vesrionStatus);
				versionControl.setVersionId(list.get(0).getVersionId());
				versionControl.setStatusCode("300");
				versionControl.setStatusMsg("Success");
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getVersionControl" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getVersionControl***********************************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
			versionControl.setStatusCode("7000");
			versionControl.setStatusMsg("Some server Error, please try later.");
		} finally {
			session.close();
		}
		return versionControl;
	}

	public HashMap<Integer, String> getMenu() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getMenu()");
		// logger.info("Start excution ===================== method getMenu()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<Integer, String> menuMap = new HashMap<Integer, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT CODE,menudesc FROM menumastercode ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				// System.out.println(row[0].toString());
				menuMap.put((Integer) row[0], row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getMenu" + e.getMessage() + " " + e);
			// logger.debug("problem in getMenu==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return menuMap;
	}

	public HashMap<Integer, String> getRole() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getRole()");
		// logger.info("Start excution ===================== method getRole()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<Integer, String> roleMap = new HashMap<Integer, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT CODE,roledesc FROM rolemastercode order by roledesc");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				roleMap.put((Integer) row[0], row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getRole" + e.getMessage() + " " + e);
			// logger.debug("problem in getRole==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return roleMap;
	}

	public List<MerchantOffersMast> getMerchantOffer(String aggreatorId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getMerchantOffer()");
		// logger.info("Start excution ===================== method
		// getMerchantOffer()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<MerchantOffersMast> merchantOfferList = new ArrayList<MerchantOffersMast>();
		try {
			merchantOfferList = session.createCriteria(MerchantOffersMast.class)
					.add(Restrictions.eq("aggreatorId", aggreatorId)).list();
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getMerchantOffer" + e.getMessage() + " " + e);
			// logger.debug("problem in getMerchantOffer==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return merchantOfferList;
	}

	public String uploadMerchantOffer(String aggreatorId, List offerList) {
		;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "uploadMerchantOffer()");
		// logger.info("Start excution ===================== method
		// insertMerchantOffer()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String status = "1001";
		try {
			if (offerList == null || offerList.size() == 0) {
				return status;
			}

			session.beginTransaction();
			List<MerchantOffersMast> instances = session.createCriteria(MerchantOffersMast.class)
					.add(Restrictions.eq("aggreatorId", aggreatorId)).list();
			for (Object obj : instances) {
				session.delete(obj);
			}
			try {
				for (int i = 0; i < offerList.size(); i++) {
					session.save((MerchantOffersMast) offerList.get(i));
					if (i % 100 == 0) { // Same as the JDBC batch size
						session.flush();
						session.clear();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			session.getTransaction().commit();
			status = "1000";
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in uploadMerchantOffer" + e.getMessage() + " " + e);
			// logger.debug("problem in uploadMerchantOffer==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return status;

	}

	public HashMap<String, String> getSubAggreator(String aggreator) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreator, "", "", "", "", "getSubAggreator()");
		// logger.info("Start excution ===================== method
		// getSubAggreator()"+aggreator);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<String, String> subAggreatorMap = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT id,NAME,mobileno FROM walletmast WHERE  aggreatorid=:aggreator AND usertype in (4,6,3,7) ");
			query.setString("aggreator", aggreator);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				// System.out.println(row[0].toString());
				subAggreatorMap.put(row[0].toString(), row[1].toString() + "-" + row[2].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreator, "", "", "", "",
					"problem in getMenu" + e.getMessage() + " " + e);
			// logger.debug("problem in getMenu==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return subAggreatorMap;
	}

	public UserRoleMapping saveUserRoleMapping(UserRoleMapping userRoleMapping) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "",
				"saveUserRoleMapping()" + "|RoleType " + userRoleMapping.getRoleType()+"  RoleId "+userRoleMapping.getRoleId());
		// logger.info("Start excution ===================== method
		// getUserId"+userRoleMapping.getUserId());
		// logger.info("Start excution ===================== method
		// getRoleType"+userRoleMapping.getRoleType());
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		userRoleMapping.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		try {
			if (userRoleMapping.getUserId() == null || userRoleMapping.getUserId().isEmpty()) {
				userRoleMapping.setActionResult("1001");
			}
			if (userRoleMapping.getRoleType().length > 0) {
				String[] menuCode = userRoleMapping.getRoleType();

				StringBuilder assignMenu = new StringBuilder();
				for (int i = 0; i < userRoleMapping.getRoleType().length; i++) {
					if (i == 0) {
						assignMenu.append(menuCode[i]);
					} else {
						assignMenu.append("," + menuCode[i]);
					}
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "",
						"", "assignMenu" + assignMenu);
				// logger.info("Start excution ===================== method
				// saveUserRoleMapping"+assignMenu);
				userRoleMapping.setRoleId(assignMenu.toString());
				session.saveOrUpdate(userRoleMapping);
//				transaction.commit();
				List<String> menuCodeList = Arrays.asList(menuCode);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "",
						"", "menuCodeList" + menuCodeList.contains("9004"));
				// logger.info("Start excution ===================== method
				// menuCodeList.contains(\"9004\")"+menuCodeList.contains("9004"));
				if (menuCodeList.contains("9004")) {

					new WalletUserDaoImpl().setMpin(userRoleMapping.getUserId());
				}

				/***********************************************
				 * CME default credit
				 *******************************************/

				if (menuCodeList != null && menuCodeList.contains("9004") && userRoleMapping != null
						&& userRoleMapping.getAmount() > 0) {
					WalletUserDao userDao = new WalletUserDaoImpl();
					WalletMastBean wMast = userDao.showUserProfile(userRoleMapping.getUserId());
					CMERequestBean cMERequestBean = new CMERequestBean();
					cMERequestBean.setAggreatorId(wMast.getAggreatorid());
					cMERequestBean.setUserId(wMast.getId());
					cMERequestBean.setWalletId(wMast.getWalletid());
					cMERequestBean.setBankName("NA");
					cMERequestBean.setBranchName("NA");
					cMERequestBean.setDocType("Default Limit");
					cMERequestBean.setTxnRefNo("NA");
					cMERequestBean.setAmount(userRoleMapping.getAmount());
					cMERequestBean.setRemarks("Default Limit assigned");

					cMERequestBean.setCmeRequestid(getTrxId("CMEREQ", wMast.getAggreatorid()));
					cMERequestBean.setStatus("PENDING");
					cMERequestBean.setCheckerStatus("PENDING");
					cMERequestBean.setApproverStatus("PENDING");

					// for changes

					WalletMastBean wBean = (WalletMastBean) session.get(WalletMastBean.class,
							cMERequestBean.getUserId());
					CashDepositMast cMast = new CashDepositMast();

					cMast.setDepositId(cMERequestBean.getCmeRequestid());
					if (wBean != null && wBean.getSuperdistributerid() != null
							&& !wBean.getSuperdistributerid().isEmpty()
							&& !wBean.getSuperdistributerid().equalsIgnoreCase("-1")) {
						cMast.setAggreatorId(wBean.getSuperdistributerid());
					} else {
						cMast.setAggreatorId(cMERequestBean.getAggreatorId());
					}
					cMast.setUserId(cMERequestBean.getUserId());
					cMast.setWalletId(cMERequestBean.getWalletId());
					cMast.setType(cMERequestBean.getDocType());
					cMast.setAmount(cMERequestBean.getAmount());
					cMast.setNeftRefNo(cMERequestBean.getTxnRefNo());
					cMast.setReciptPic(cMERequestBean.getReciptDoc());
					cMast.setStatus(cMERequestBean.getStatus());
					cMast.setIpIemi(cMERequestBean.getIpIemi());
					cMast.setAgent(cMERequestBean.getAgent());
//	SimpleDateFormat df=null;
//	if(cMERequestBean.getDepositeDate()!=null&&cMERequestBean.getDepositeDate().indexOf("-")>=0){
//	df=new SimpleDateFormat("dd-MMM-yyyy");
//	}else{
//	df=new SimpleDateFormat("dd/MM/yyyy");	
//	}

		            Timestamp ts=new Timestamp(new Date().getTime()); 
					Date depositDate = new Date();
					cMast.setDepositDate(new java.sql.Date(depositDate.getTime()));
					cMast.setBankName(cMERequestBean.getBankName());
					cMast.setBranchName(cMERequestBean.getBranchName());
					cMast.setCheckerStatus(cMERequestBean.getCheckerStatus());
					cMast.setApproverStatus(cMERequestBean.getApproverStatus());
					cMast.setRequestdate(ts);
					session.save(cMast);

				}
				/***********************************************
				 * CME default credit
				 *******************************************/
				transaction.commit();
				userRoleMapping.setActionResult("1000");
			} else {
				userRoleMapping.setActionResult("1001");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "",
					"problem in saveUserRoleMapping" + e.getMessage() + " " + e);
			logger.debug("problem in saveUserRoleMapping==================" + e.getMessage(), e);
			userRoleMapping.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "",
				"return" + userRoleMapping);
		// logger.info("return ===================== method
		// saveUserRoleMapping()"+userRoleMapping);
		return userRoleMapping;

	}

	public UserRoleMapping getUserRoleMapping(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "getUserRoleMapping()");
		// logger.info("Start excution ===================== method
		// getUserRoleMapping()"+userId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		UserRoleMapping userRoleMapping = new UserRoleMapping();
		String[] roleType;
		try {
			Query query = session.createQuery("from UserRoleMapping where userId=:userId");
			query.setString("userId", userId);
			List<UserRoleMapping> list = query.list();
			if (list != null && list.size() != 0) {
				userRoleMapping = list.get(0);
				roleType = userRoleMapping.getRoleId().split(",");
				userRoleMapping.setRoleType(roleType);
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
					"problem in getUserRoleMapping" + e.getMessage() + " " + e);
			// logger.debug("problem in getUserRoleMapping==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
				"return RoleType " + userRoleMapping.getRoleType());
		// logger.info("return ===================== method
		// getUserRoleMapping()"+userRoleMapping.getRoleType());
		return userRoleMapping;

	}

	public Boolean validatePan(String panNumber, String aggreatorId, int userType) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
				"validatePan()" + "|panNumber " + panNumber);
		// logger.info("Start excution ===================== method
		// validatePan()*panNumber"+panNumber);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag = false;
		try {
			Query query = session.createQuery(
					"select id from WalletMastBean where pan= :panNumber and aggreatorid=:aggreatorid and usertype=:userType");
			query.setString("panNumber",
					EncryptionDecryption.getEncrypted(panNumber, "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			query.setString("aggreatorid", aggreatorId);
			query.setInteger("userType", userType);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
						"flag" + flag);
				// logger.info("=====validatePan=========flag=============================="+flag);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
					"problem in validatePan" + e.getMessage() + " " + e);
			// logger.debug("problem in validatePan==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public List<AgentDetailsBean> getAgentDtlsByDistId(String aggreatorId, String distributerId, String serverName) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
				"getAgentDtlsByDistId()" + "|distributerId " + distributerId);
		/*
		 * logger.info(serverName+"*****"+
		 * distributerId+"*******Start excution ===================== getAgentDtlsByDistId"
		 * ); logger.info(serverName+"*****"+
		 * distributerId+"*******distributerId ===================== getAgentDtlsByDistId"
		 * +distributerId); logger.info(serverName+"*****"+
		 * distributerId+"*******aggreatorId ===================== getAgentDtlsByDistId"
		 * +aggreatorId);
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentDetailsBean> agentList = new ArrayList<AgentDetailsBean>();

		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT id,walletid,name,shopname,agenttype,usertype,distributerid,userstatus,mobileno FROM walletmast WHERE usertype=2  and approvalrequired='A' AND distributerid=:distributerId AND aggreatorid=:aggreatorId");
			query.setParameter("aggreatorId", aggreatorId);
			query.setParameter("distributerId", distributerId);
			query.setResultTransformer(Transformers.aliasToBean(AgentDetailsBean.class));
			agentList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", serverName
					+ "*****" + distributerId + "problem in getAgentDtlsByDistId" + e.getMessage() + " " + e);
			// logger.debug(serverName+"*****"+distributerId+"problem in
			// getAgentDtlsByDistId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return agentList;
	}

	@SuppressWarnings("unchecked")
	public List<AgentDetailsBean> getAgentDtlsBySupDistId(String aggreatorId, String superDistributorid,
			String distributerId, String serverName) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
				"getAgentDtlsBySupDistId()" + "|distributerId " + distributerId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
				"superDistributorid " + superDistributorid);
		/*
		 * logger.info(serverName+"*****"+
		 * distributerId+"*******Start excution ===================== getAgentDtlsBySupDistId"
		 * ); logger.info(serverName+"*****"+
		 * distributerId+"*******distributerId ===================== getAgentDtlsBySupDistId"
		 * +distributerId); logger.info(serverName+"*****"+
		 * distributerId+"*******superdistributorid ===================== getAgentDtlsBySupDistId"
		 * +superDistributorid); logger.info(serverName+"*****"+
		 * distributerId+"*******aggreatorId ===================== getAgentDtlsBySupDistId"
		 * +aggreatorId);
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentDetailsBean> agentList = new ArrayList<AgentDetailsBean>();

		try {
			StringBuffer sqlquery = new StringBuffer(
					"SELECT id,walletid,name,shopname,agenttype,usertype,distributerid,userstatus,mobileno FROM walletmast WHERE usertype in (2,3) AND aggreatorid=:aggreatorId");
			if (superDistributorid != null && !superDistributorid.isEmpty()) {
				sqlquery.append(" AND superdistributerid=:superdistributerid");
			}
//		else{
//			sqlquery.append("AND distributerid=:distributerId");
//		}
			SQLQuery query = session.createSQLQuery(sqlquery.toString());
			if (superDistributorid != null && !superDistributorid.isEmpty()) {
				query.setParameter("superdistributerid", superDistributorid);
			}
//		else{
//			query.setParameter("distributerId", distributerId);
//		}
			query.setParameter("aggreatorId", aggreatorId);

			query.setResultTransformer(Transformers.aliasToBean(AgentDetailsBean.class));
			agentList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", serverName
					+ "*****" + distributerId + "problem in getAgentDtlsByDistId" + e.getMessage() + " " + e);
			// logger.debug(serverName+"*****"+distributerId+"problem in
			// getAgentDtlsByDistId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return agentList;

	}

	public String getVersionStatus(int version_number) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getVersionStatus()");
		// logger.info("*****************Start Execution
		// ***********getVersionStatus*******");
		VersionStatusBean update_type = null;
		String updateType = null;
		try {
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			update_type = (VersionStatusBean) session.get(VersionStatusBean.class, version_number);
			if (update_type != null) {
				updateType = update_type.getUpdate_type();
			}
		} catch (Exception ex) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getVersionStatus" + ex.getMessage() + " " + ex);
			// logger.info("************problem in getVersionStatus**********e.message
			// "+ex.getMessage());
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return updateType;

	}

	public HashMap<String, String> getPartnerList() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getPartnerList()");
		// logger.info("Start excution ===================== method getPartnerList()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<String, String> partnerMap = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT name,name FROM partnermast where status=1 ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				partnerMap.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getPartnerLis" + e.getMessage() + " " + e);
			// logger.debug("problem in getPartnerList==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return partnerMap;
	}

	public HashMap<String, String> getTxnSourceList() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getTxnSourceList()");
		// logger.info("Start excution ===================== method
		// getTxnSourceList()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<String, String> txnSourceMap = new HashMap<String, String>();
		try {
			SQLQuery query = session.createSQLQuery("SELECT source,source FROM txnsourcemast where status=1 ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				txnSourceMap.put(row[0].toString(), row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in gettxnSourceList" + e.getMessage() + " " + e);
			// logger.debug("problem in gettxnSourceList==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return txnSourceMap;
	}

	public PartnerPrefundMastBean savePartnerPrefund(PartnerPrefundMastBean partnerPrefundMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "savePartnerPrefund()");
		// logger.info("Start excution ===================== method
		// gettxnSourceList()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		partnerPrefundMastBean.setStatusCode("1");
		partnerPrefundMastBean.setStatusMsg("Some server Error, please try later.");
		try {
			transaction = session.beginTransaction();

			String stDate = partnerPrefundMastBean.getStDate();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			Date date = formatter.parse(stDate);
			partnerPrefundMastBean.setTxnDate(date);

			session.save(partnerPrefundMastBean);
			transaction.commit();
			partnerPrefundMastBean.setStatusCode("300");
			partnerPrefundMastBean.setStatusMsg("Partner Prefund save succesfully.");

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in partnerPrefundMastBean" + e.getMessage() + " " + e);
			// logger.debug("problem in partnerPrefundMastBean==================" +
			// e.getMessage(), e);
			partnerPrefundMastBean.setStatusCode("1");
			partnerPrefundMastBean.setStatusMsg("Some server Error, please try later.");
		} finally {
			session.close();
		}
		return partnerPrefundMastBean;

	}

	public TariffPlans getPlans(String serviceOperator, String circle) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getPlans()");
		// logger.info("Start excution ===================== method getPlans()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		TariffPlans tariffPlans = new TariffPlans();
		try {

			int circleCode = (int) session
					.createSQLQuery("SELECT sno FROM rechargecirclecode WHERE  circlename=:circle")
					.setString("circle", circle).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					circle + " getting circle  " + circleCode);
			// logger.info("*************************************"+circle+" getting circle
			// "+circleCode+"****************************************************");
			int operatorCode = (int) session
					.createSQLQuery("SELECT operatorcode FROM operatormaster WHERE  operatorname=:serviceOperator")
					.setString("serviceOperator", serviceOperator).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					serviceOperator + "  getting recharge operator  " + operatorCode);
			// logger.info("****************************************"+serviceOperator+"
			// getting recharge operator "+operatorCode+"********************************");

			Query query2 = session.createSQLQuery(
					"select id,circlecode,operatorcode,price,validity,talktime,benefits from topupplans where operatorcode='"
							+ operatorCode + "' and circlecode='" + circleCode + "' order by price asc");
			query2.setResultTransformer(Transformers.aliasToBean(TopupPlans.class));
			List<TopupPlans> topupList = query2.list();
			tariffPlans.setTopupPlans(topupList);

			Query smsquery = session.createSQLQuery(
					"select id,circlecode,operatorcode,price,validity,talktime,benefits from smsplans where operatorcode='"
							+ operatorCode + "' and circlecode='" + circleCode + "' order by price asc");
			smsquery.setResultTransformer(Transformers.aliasToBean(SMSPlans.class));
			List<SMSPlans> smsList = smsquery.list();
			tariffPlans.setSmsPlans(smsList);

			Query localquery = session.createSQLQuery(
					"select id,circlecode,operatorcode,price,validity,talktime,benefits from localplans where operatorcode='"
							+ operatorCode + "' and circlecode='" + circleCode + "' order by price asc");
			localquery.setResultTransformer(Transformers.aliasToBean(LocalPlans.class));
			List<LocalPlans> localList = localquery.list();
			tariffPlans.setLocalPlans(localList);

			Query stdquery = session.createSQLQuery(
					"select id,circlecode,operatorcode,price,validity,talktime,benefits from stdplans where operatorcode='"
							+ operatorCode + "' and circlecode='" + circleCode + "' order by price asc");
			stdquery.setResultTransformer(Transformers.aliasToBean(STDPlans.class));
			List<STDPlans> stdList = stdquery.list();
			tariffPlans.setStdPlans(stdList);

			Query isdquery = session.createSQLQuery(
					"select id,circlecode,operatorcode,price,validity,talktime,benefits from isdplans where operatorcode='"
							+ operatorCode + "' and circlecode='" + circleCode + "' order by price asc");
			isdquery.setResultTransformer(Transformers.aliasToBean(ISDPlans.class));
			List<ISDPlans> isdList = isdquery.list();
			tariffPlans.setIsdlPlans(isdList);

			tariffPlans.setStatusCode("300");
			tariffPlans.setStatusMsg("Success.");

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getPlans" + e.getMessage() + " " + e);
			// logger.debug("problem in getPlans==================" + e.getMessage(), e);
			tariffPlans.setStatusCode("1");
			tariffPlans.setStatusMsg("Some server Error, please try later.");
		} finally {
			session.close();
		}

		return tariffPlans;
	}

	public InstallationDetails saveInstallationDetails(InstallationDetails installationDetails) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "saveInstallationDetails()");
		// logger.info("Start excution ===================== method
		// saveInstallationDetails()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {
			transaction = session.beginTransaction();
			InstallationDetails iDetails = (InstallationDetails) session.get(InstallationDetails.class,
					installationDetails.getImei());
			session.evict(iDetails);
			if (iDetails != null && iDetails.getImei() > 0) {
				iDetails.setCount(iDetails.getCount() + 1);
				if (installationDetails != null) {
					if (installationDetails.getVersionNumber() != 0)
						iDetails.setVersionNumber(installationDetails.getVersionNumber());
					if (installationDetails.getVersionName() != null && !installationDetails.getVersionName().isEmpty())
						iDetails.setVersionName(installationDetails.getVersionName());
					if (installationDetails.getDeviceName() != null && !installationDetails.getDeviceName().isEmpty())
						iDetails.setDeviceName(installationDetails.getDeviceName());
					if (installationDetails.getDeviceVersion() != null
							&& !installationDetails.getDeviceVersion().isEmpty())
						iDetails.setDeviceVersion(installationDetails.getDeviceVersion());
					if (installationDetails.getDeviceMan() != null && !installationDetails.getDeviceMan().isEmpty())
						iDetails.setDeviceMan(installationDetails.getDeviceMan());
					if (installationDetails.getDevice() != null && !installationDetails.getDevice().isEmpty())
						iDetails.setDevice(installationDetails.getDevice());
					if (installationDetails.getDeviceBrand() != null && !installationDetails.getDeviceBrand().isEmpty())
						iDetails.setDeviceBrand(installationDetails.getDeviceBrand());
				}
				session.update(iDetails);
				installationDetails.setCount(iDetails.getCount());
			} else {

				installationDetails.setCount(1);
				session.saveOrUpdate(installationDetails);
			}

			transaction.commit();
			installationDetails.setStatusCode("300");
			installationDetails.setStatusMsg("Success");

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in saveInstallationDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in saveInstallationDetails==================" +
			// e.getMessage(), e);
			installationDetails.setStatusCode("1001");
			installationDetails.setStatusMsg("Some server Error, please try later.");
		} finally {
			session.close();
		}

		return installationDetails;
	}

	public PaymentGatewayURL savePaymentGatewayURL(PaymentGatewayURL paymentGatewayURL) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "savePaymentGatewayURL()");
		// logger.info("Start excution ===================== method
		// savePaymentGatewayURL()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		PaymentGatewayURL paymentGatewayURLr = new PaymentGatewayURL();
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(paymentGatewayURL);
			transaction.commit();
			paymentGatewayURLr.setStatusCode("300");
			paymentGatewayURLr.setStatusMsg("Success.");

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getPlans" + e.getMessage() + " " + e);
			// logger.debug("problem in getPlans==================" + e.getMessage(), e);
			paymentGatewayURLr.setStatusCode("1");
			paymentGatewayURLr.setStatusMsg("Some server Error, please try later.");
		} finally {
			session.close();
		}

		return paymentGatewayURLr;
	}

	public String getPublicIP() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getPublicIP()");
		// logger.info("**********Start Execution*********getPublicIp()*****");
		// HashMap<String,String> respJson = new HashMap<String,String>();
		String res = "https://api.ipify.org/?format=json";
		/*
		 * try { URL url = new URL("http://www.ip-api.com/json"); HttpURLConnection conn
		 * = (HttpURLConnection)url.openConnection(); conn.setDoOutput(true);
		 * conn.setRequestProperty("Content-Type", "application/json");
		 * conn.setRequestMethod("POST"); int responseCode = conn.getResponseCode();
		 * 
		 * logger.info("responseCode********** : " + responseCode); if(responseCode ==
		 * 200) { BufferedReader in = new BufferedReader( new
		 * InputStreamReader(conn.getInputStream())); String inputLine; StringBuffer
		 * response = new StringBuffer();
		 * 
		 * while ((inputLine = in.readLine()) != null) { response.append(inputLine); }
		 * HashMap<String,Object> resp = new HashMap<String,Object>(); resp = new
		 * com.fasterxml.jackson.databind.ObjectMapper().readValue(response.toString(),
		 * HashMap.class); respJson.put("query", (String)resp.get("query"));
		 * respJson.put("status", "300"); } else { respJson.put("query", "");
		 * respJson.put("status", "301"); }
		 * 
		 * res = new
		 * com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(respJson);
		 * 
		 * } catch(Exception e) {
		 * logger.info("**********problem in*****getPublicIp()*****e.message*"+e.
		 * getMessage()); e.printStackTrace(); respJson.put("query", "");
		 * respJson.put("status", "301"); }
		 */

		JSONObject jobj = new JSONObject();
		try {
			jobj.put("URL", res);
		} catch (JSONException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in PublicIp" + " " + e);
			e.printStackTrace();
		}
		return jobj.toString();
	}

	public String insertTransDtl(String dtl) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|Method Name :insertBeneDtl| Detail : " + dtl);
		String resp = "FAIL";
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		try {
			String result = getTransDtlFlag(dtl, "ALL");

			if (result != null && result.equalsIgnoreCase("success")) {
				transaction = session.beginTransaction();
				SQLQuery insertQuery = session.createSQLQuery("insert into benedtl(details) VALUES (?)");
				insertQuery.setParameter(0, dtl);
				insertQuery.executeUpdate();
				transaction.commit();
				return "SUCCESS";
			} else {
				return result;
			}
		} catch (ConstraintViolationException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :insertBeneDtl| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
			// You have already processed transaction with same details please wait for 10
			// minutes.
			resp = "You have already processed transaction with same details please wait for 10 minutes";
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :insertBeneDtl| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
			if (transaction != null && transaction.isActive())
				transaction.rollback();
		} finally {
			session.close();
		}
		return resp;
	}

	public String insertTransDtl(String dtl, String narr) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|Method Name :insertBeneDtl| Detail : " + dtl);
		String resp = "FAIL";
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		try {
			String result = "SUCCESS";
			if (!narr.equalsIgnoreCase("NEFT")) {
				result = getTransDtlFlag(dtl, "ALL");
			}
			if (result != null && result.equalsIgnoreCase("success")) {
				transaction = session.beginTransaction();
				SQLQuery insertQuery = session.createSQLQuery("insert into benedtl(details) VALUES (?)");
				insertQuery.setParameter(0, dtl);
				insertQuery.executeUpdate();
				transaction.commit();
				return "SUCCESS";
			} else {
				return result;
			}
		} catch (ConstraintViolationException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :insertBeneDtl| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
			// You have already processed transaction with same details please wait for 10
			// minutes.
			resp = "You have already processed transaction with same details please wait for 10 minutes";
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :insertBeneDtl| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
			if (transaction != null && transaction.isActive())
				transaction.rollback();
		} finally {
			session.close();
		}
		return resp;
	}

	public String deleteTransDtl(String dtl) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|Method Name :deleteTransDtl| Detail : " + dtl);
		String resp = "FAIL";
		factory = DBUtil.getSessionFactory();
		try {
			session = factory.openSession();
			transaction = session.beginTransaction();
//				SQLQuery query = session.createSQLQuery( "DELETE  FROM benedtl WHERE details like '"+dtl+"%'");
//				query.executeUpdate();
//				transaction.commit();
//				transaction = session.beginTransaction();
			SQLQuery query1 = session.createSQLQuery(
					"DELETE  FROM benedtl WHERE creationtime  < DATE_SUB(NOW(), INTERVAL 10 MINUTE) and details like '"
							+ dtl + "%'");
			query1.executeUpdate();
			transaction.commit();
			return "SUCCESS";
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :deleteTransDtl| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return resp;
	}

	public String getTransDtlFlag(String dtl, String txnType) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"|Method Name :getTransDtlFlag| Detail : " + dtl);
		String resp = "FAIL";
		factory = DBUtil.getSessionFactory();
		try {
			session = factory.openSession();
			String agentId = "";
			String beneficiaryId = "";
			// agentId=dtl.substring(0,dtl.indexOf('|'));

			StringTokenizer tokenizer = new StringTokenizer(dtl, "|");
			if (tokenizer != null && tokenizer.hasMoreTokens())
				agentId = tokenizer.nextToken();
			if (tokenizer != null && tokenizer.hasMoreTokens()) {
				tokenizer.nextToken();
			}
			if (tokenizer != null && tokenizer.hasMoreTokens())
				beneficiaryId = tokenizer.nextToken();

			WalletMastBean agentDetails = (WalletMastBean) session.get(WalletMastBean.class, agentId);
			if (txnType.equalsIgnoreCase("bank") && agentDetails.getBank().equalsIgnoreCase("1")
					&& beneficiaryId != null) {
				MudraBeneficiaryBank beneDetailsBank = (MudraBeneficiaryBank) session.get(MudraBeneficiaryBank.class,
						beneficiaryId);
				if (beneDetailsBank != null && beneDetailsBank.getBankName() != null) {
					Criteria crt = session.createCriteria(BlockedBank.class)
							.add(Restrictions.eq("bankName", beneDetailsBank.getBankName()))
							.add(Restrictions.eq("isBank", 1));
					BlockedBank blockedBank = (BlockedBank) crt.uniqueResult();
					if (blockedBank != null) {
						return (blockedBank.getBankName() != null ? blockedBank.getBankName() : "This bank")
								+ " is currently down. please try after sometime.";
					}
				}
			} else if (txnType.equalsIgnoreCase("wallet") && agentDetails.getWallet().equalsIgnoreCase("1")
					&& beneficiaryId != null) {
				MudraBeneficiaryWallet beneDetailsBank = (MudraBeneficiaryWallet) session
						.get(MudraBeneficiaryWallet.class, beneficiaryId);
				if (beneDetailsBank != null && beneDetailsBank.getBankName() != null) {
					Criteria crt = session.createCriteria(BlockedBank.class)
							.add(Restrictions.eq("bankName", beneDetailsBank.getBankName()))
							.add(Restrictions.eq("isBank", 0));
					BlockedBank blockedBank = (BlockedBank) crt.uniqueResult();
					if (blockedBank != null) {
						return (blockedBank.getBankName() != null ? blockedBank.getBankName() : "This bank")
								+ " is currently down. please try after sometime.";
					}
				}
			} else if (txnType.equalsIgnoreCase("ALL") && beneficiaryId != null) {
				String bankName = null;
				MudraBeneficiaryWallet beneDetailswallet = (MudraBeneficiaryWallet) session
						.get(MudraBeneficiaryWallet.class, beneficiaryId);
				if (beneDetailswallet != null && beneDetailswallet.getBankName() != null) {
					bankName = beneDetailswallet.getBankName();
				} else {
					MudraBeneficiaryBank beneDetailsBank = (MudraBeneficiaryBank) session
							.get(MudraBeneficiaryBank.class, beneficiaryId);
					if (beneDetailsBank != null && beneDetailsBank.getBankName() != null) {
						bankName = beneDetailsBank.getBankName();
					}
				}
				if (bankName != null) {
					Criteria crt = session.createCriteria(BlockedBank.class).add(Restrictions.eq("bankName", bankName))
							.add(Restrictions.eq("isBank", 2));
					BlockedBank blockedBank = (BlockedBank) crt.uniqueResult();
					if (blockedBank != null) {
						return (blockedBank.getBankName() != null ? blockedBank.getBankName() : "This bank")
								+ " is currently down. please try after sometime.";
					}
				}
			}

			if (agentDetails != null && txnType != null
					&& ((txnType.equalsIgnoreCase("bank") && agentDetails.getBank().equalsIgnoreCase("1"))
							|| (txnType.equalsIgnoreCase("wallet") && agentDetails.getWallet().equalsIgnoreCase("1"))
							|| (txnType.equalsIgnoreCase("ALL")))) {
				SQLQuery query = session.createSQLQuery("select count(*) from benedtl where details='" + dtl
						+ "' and creationtime  > DATE_SUB(NOW(), INTERVAL 10 MINUTE) ");
				int count = ((BigInteger) query.uniqueResult()).intValue();
				if (count > 0) {
					resp = "You have already processed transaction with same details please wait for 10 minutes.";
				} else {
					resp = "SUCCESS";
				}
			} else {
				resp = agentDetails.getPortalMessage() != null ? agentDetails.getPortalMessage()
						: "Portal is down for " + txnType + " transactions please try after sometime.";
			}

			return resp;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"|Method Name :getTransDtlFlag| Detail : " + dtl + "problem found" + e.getMessage() + " " + e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		return resp;
	}

	@Override
	public List<String> getListOfCashdepositBankByAggregator(String aggregatorId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", aggregatorId, "", "", "",
				"getListOfCashdepositBankByAggregator()");
		// logger.info("Start excution ===================== method getPartnerList()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<String> result = new ArrayList<String>();
		try {
			Criteria cri = session.createCriteria(CashDepositBankDetails.class)
					.add(Restrictions.eq("aggregatorId", aggregatorId));
			List<CashDepositBankDetails> list = cri.list();
			for (CashDepositBankDetails bank : list) {
				String val = bank.getBankName() + " - " + bank.getAccountNumber();
				result.add(val);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getListOfCashdepositBankByAggregator" + e.getMessage() + " " + e);
			// logger.debug("problem in getPartnerList==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}

		return result;
	}


		
		
		
		public Boolean validateEmail(String email){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "validatePan()"+"|panNumber "+email );
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			boolean flag=false;
			try{
				Query query = session.createQuery("select id from WalletMastBean where emailid= :email");
				query.setString("email", email);
				List list = query.list();
				if (list != null && list.size() != 0)
				 {
				 flag=true;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "flag"+flag);
			     }
				}catch (Exception e) {				
				e.printStackTrace();
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in validate email"+e.getMessage()+" "+e);
			} finally {
				session.close();
			}
			return flag;
		}
		
		
    public Boolean validateMobile(String mobile){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "validateMobile()"+"| mobileNumber "+mobile );
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			boolean flag=false;
			try{
				Query query = session.createQuery("select id from WalletMastBean where mobileno= :mobile");
				query.setString("mobile", mobile);
				List list = query.list();
				if (list != null && list.size() != 0)
				 {
				 flag=true;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "flag"+flag);
			     }
				}catch (Exception e) {				
				e.printStackTrace();
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in validate email"+e.getMessage()+" "+e);
			} finally {
				session.close();
			}
			return flag;
		}
		
	
    public Boolean validateAadhaar(String adhar, String aggid, int userType){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "validateAadhaar()"+"| Aadhaar "+adhar );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag=false;
		try{
			Query query = session.createQuery("select id from WalletMastBean where adhar= :adhar and aggreatorid=:aggreatorid and usertype=:userType");
			query.setString("adhar", EncryptionDecryption.getEncrypted(adhar, "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			query.setString("aggreatorid", aggid);
			query.setInteger("userType", userType);
			List list = query.list();
			if (list != null && list.size() != 0)
			 {
			 flag=true;
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "flag"+flag);
		     }
			}catch (Exception e) {				
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in validate email"+e.getMessage()+" "+e);
		} finally {
			session.close();
		}
		return flag;
	}

    
	@Override
	public List<RechargeTxnBean> markPending(String str) {
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "markPending()"+str );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		List<RechargeTxnBean> list=new ArrayList<RechargeTxnBean>();
		try {
			if(!str.isEmpty())
			{
			String[] splitData=str.split(" ");
			for(String data:splitData)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","data "+data, "", "", "",  ""+str);
				 
				if(!data.isEmpty() || !data.equals(" "))
				{
				RechargeTxnBean rc = (RechargeTxnBean)session.get(RechargeTxnBean.class, data.trim());
				 if(rc!=null)
					{
					 rc.setStatus("PENDING");
					 session.saveOrUpdate(rc);
					 
					list.add(rc);
				    System.out.println("Size  "+list.size()); 
					}
				}
			}
			transaction.commit();
			}
			
		}catch(Exception e)
		{
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in mark pending recharge"+e.getMessage()+" "+e);
		 transaction.rollback();
		}finally {
			session.close();
		}
		
		return list;
	}
	
    
	
	@Override
	public List<MudraMoneyTransactionBean> markDmtPending(String str) {
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "markDmtPending()"+str );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		List<MudraMoneyTransactionBean> list=new ArrayList<MudraMoneyTransactionBean>();
		try {

			if(!str.isEmpty() || str!=null)
			{
			String[] splitData=str.split(" ");
			for(String data:splitData)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","data "+data, "", "", "markDmtPending",  ""+str);
				
				if(!data.isEmpty() || !data.equals(" "))
				{
				MudraMoneyTransactionBean rc = (MudraMoneyTransactionBean)session.get(MudraMoneyTransactionBean.class, data.trim());
				 if(rc!=null)
					{
					 rc.setStatus("PENDING");
					 rc.setBankResp("PENDING");
					 rc.setRemark("PENDING");
					 session.saveOrUpdate(rc);
					 
					list.add(rc);
				    System.out.println("Size  "+list.size()); 
					}
				}
			}
			transaction.commit();
			}
			
		}catch(Exception e)
		{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in mark pending DMT"+e.getMessage()+" "+e);
		transaction.rollback();
			
		}finally {
			session.close();
		}
		
		return list;
	}
	
	
    
    
	
	
	public UserRoleMapping saveUserRoleMappingNew(UserRoleMapping userRoleMapping) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "","saveUserRoleMapping()" + "|RoleType " + userRoleMapping.getRoleType()+"  RoleId "+userRoleMapping.getRoleId());
		 
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		userRoleMapping.setActionResult("1001");
		Transaction transaction = session.beginTransaction();
		try {
			if (userRoleMapping.getUserId() == null || userRoleMapping.getUserId().isEmpty()) {
				userRoleMapping.setActionResult("1001");
			}
			if (userRoleMapping.getRoleType().length > 0) {
				String[] menuCode = userRoleMapping.getRoleType();

				StringBuilder assignMenu = new StringBuilder();
				for (int i = 0; i < userRoleMapping.getRoleType().length; i++) {
					if (i == 0) {
						assignMenu.append(menuCode[i]);
					} else {
						assignMenu.append("," + menuCode[i]);
					}
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "","", "assignMenu" + assignMenu);
				 
				userRoleMapping.setRoleId(assignMenu.toString());
				session.saveOrUpdate(userRoleMapping);
 
				List<String> menuCodeList = Arrays.asList(menuCode);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "",
						"", "menuCodeList" + menuCodeList.contains("9004"));
 /*
				if (menuCodeList.contains("9004")) {

					new WalletUserDaoImpl().setMpin(userRoleMapping.getUserId());
				}
*/
				/***********************************************
				 * CME default credit
				 *******************************************/

				if (menuCodeList != null && menuCodeList.contains("9004") && userRoleMapping != null
						&& userRoleMapping.getAmount() > 0) {
					WalletUserDao userDao = new WalletUserDaoImpl();
					WalletMastBean wMast = userDao.showUserProfile(userRoleMapping.getUserId());
					CMERequestBean cMERequestBean = new CMERequestBean();
					cMERequestBean.setAggreatorId(wMast.getAggreatorid());
					cMERequestBean.setUserId(wMast.getId());
					cMERequestBean.setWalletId(wMast.getWalletid());
					cMERequestBean.setBankName("NA");
					cMERequestBean.setBranchName("NA");
					cMERequestBean.setDocType("Default Limit");
					cMERequestBean.setTxnRefNo("NA");
					cMERequestBean.setAmount(userRoleMapping.getAmount());
					cMERequestBean.setRemarks("Default Limit assigned");

					cMERequestBean.setCmeRequestid(getTrxId("CMEREQ", wMast.getAggreatorid()));
					cMERequestBean.setStatus("PENDING");
					cMERequestBean.setCheckerStatus("PENDING");
					cMERequestBean.setApproverStatus("PENDING");

					// for changes

					WalletMastBean wBean = (WalletMastBean) session.get(WalletMastBean.class,
							cMERequestBean.getUserId());
					CashDepositMast cMast = new CashDepositMast();

					cMast.setDepositId(cMERequestBean.getCmeRequestid());
					if (wBean != null && wBean.getSuperdistributerid() != null
							&& !wBean.getSuperdistributerid().isEmpty()
							&& !wBean.getSuperdistributerid().equalsIgnoreCase("-1")) {
						cMast.setAggreatorId(wBean.getSuperdistributerid());
					} else {
						cMast.setAggreatorId(cMERequestBean.getAggreatorId());
					}
					cMast.setUserId(cMERequestBean.getUserId());
					cMast.setWalletId(cMERequestBean.getWalletId());
					cMast.setType(cMERequestBean.getDocType());
					cMast.setAmount(cMERequestBean.getAmount());
					cMast.setNeftRefNo(cMERequestBean.getTxnRefNo());
					cMast.setReciptPic(cMERequestBean.getReciptDoc());
					cMast.setStatus(cMERequestBean.getStatus());
					cMast.setIpIemi(cMERequestBean.getIpIemi());
					cMast.setAgent(cMERequestBean.getAgent());
 
					Timestamp ts=new Timestamp(new Date().getTime()); 
					Date depositDate = new Date();
					cMast.setDepositDate(new java.sql.Date(depositDate.getTime()));
					cMast.setBankName(cMERequestBean.getBankName());
					cMast.setBranchName(cMERequestBean.getBranchName());
					cMast.setCheckerStatus(cMERequestBean.getCheckerStatus());
					cMast.setApproverStatus(cMERequestBean.getApproverStatus());
					cMast.setRequestdate(ts);
					session.save(cMast);

				}
				 
				transaction.commit();
				userRoleMapping.setActionResult("1000");
			} else {
				userRoleMapping.setActionResult("1001");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "","problem in saveUserRoleMapping" + e.getMessage() + " " + e);
			logger.debug("problem in saveUserRoleMapping==================" + e.getMessage(), e);
			userRoleMapping.setActionResult("7000");
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userRoleMapping.getUserId(), "", "","return" + userRoleMapping);
		return userRoleMapping;

	}

	
	public List<AgentDetailsBean> getAgentDtlsBySo(String aggreatorId,String id,String type, String serverName) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", "getAgentDtlsBySo()" + "UserId " + id); 
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentDetailsBean> agentList = new ArrayList<AgentDetailsBean>();
		 SQLQuery query =null;
        try {
			StringBuffer sqlquery = new StringBuffer(
					"SELECT id,walletid,name,shopname,agenttype,usertype,userstatus,mobileno FROM walletmast WHERE aggreatorid=:aggreatorId");
			if (type.equalsIgnoreCase("8.0")) {
				sqlquery.append("  AND usertype in (2,3,7) AND  salesId=:salesId");
			  query = session.createSQLQuery(sqlquery.toString());	
			  query.setParameter("salesId",id);
			}
			
			if (type.equalsIgnoreCase("9.0")) {
				sqlquery.append(" AND usertype in (2,3,7,8) AND managerId=:managerId");
			  query = session.createSQLQuery(sqlquery.toString());	
			  query.setParameter("managerId",id);
			}
			 
            query.setParameter("aggreatorId", aggreatorId);
            query.setResultTransformer(Transformers.aliasToBean(AgentDetailsBean.class));
			agentList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", serverName+ "*****" + "problem in getAgentDtlsBySo" + e.getMessage() + " " + e);
 		} finally {
			session.close();
		}
		return agentList;

	}

	public List<AgentDetailsBean> getAgentDtlsBySoList(String aggreatorId,String id,String type, String serverName,String user) {

		System.out.println("getAgentDtlsBySoList  "+aggreatorId+"  id "+id+"  type"+type+"   USER "+user);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", "getAgentDtlsBySo()" + "UserId " + id); 
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentDetailsBean> agentList = new ArrayList<AgentDetailsBean>();
		SQLQuery query =null;
        int users = Integer.parseInt(user);
		try {
			StringBuffer sqlquery = new StringBuffer(
					"SELECT id,walletid,name,shopname,agenttype,usertype,userstatus,mobileno FROM walletmast WHERE aggreatorid=:aggreatorId");
			if (type.equalsIgnoreCase("8.0")) {
				sqlquery.append("  AND usertype=:userType AND  salesId=:salesId");
			  query = session.createSQLQuery(sqlquery.toString());	
			  query.setParameter("salesId",id);
			  query.setParameter("userType",users);
			}
			
			if (type.equalsIgnoreCase("9.0")) {
				sqlquery.append(" AND usertype=:userType AND managerId=:managerId");
			  query = session.createSQLQuery(sqlquery.toString());	
			  query.setParameter("managerId",id);
			  query.setParameter("userType",users);
			}
			 
            query.setParameter("aggreatorId", aggreatorId);
            query.setResultTransformer(Transformers.aliasToBean(AgentDetailsBean.class));
			agentList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", serverName+ "*****" + "problem in getAgentDtlsBySo" + e.getMessage() + " " + e);
 		} finally {
			session.close();
		}
		return agentList;

	}

	
	@Override
	public List<WalletMastBean> enablePg(RechargeTxnBean txn ) {
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "markPending()"+txn.getRechargeId() );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		List<WalletMastBean> list=new ArrayList<WalletMastBean>();
		try {
			if(!txn.getRechargeId().isEmpty())
			{
			String[] splitData=txn.getRechargeId().split(" ");
			for(String data:splitData)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","data "+data, "", "", "",  ""+txn.getRechargeId());
				 
				if(!data.isEmpty() || !data.equals(" "))
				{
				 WalletMastBean rc = (WalletMastBean)session.get(WalletMastBean.class, data.trim());
				 if(rc!=null)
					{
					 if("pg".equalsIgnoreCase(txn.getStatus()))
					 	rc.setOnlineMoney(1);
					 if("bank3".equalsIgnoreCase(txn.getStatus()))
						rc.setBank3("1");
					 
					 session.saveOrUpdate(rc);
					 
					list.add(rc);
				    System.out.println("Size  "+list.size()); 
					}
				}
			}
			transaction.commit();
			}
			
		}catch(Exception e)
		{
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in enable bank and Pg"+e.getMessage()+" "+e);
		 transaction.rollback();
		}finally {
			session.close();
		}
		
		return list;
	}
	
	
    
		 
	public static void main(String[] args) {
		// CommanUtilDaoImpl commanUtilDaoImpl=new CommanUtilDaoImpl();
		// WalletConfiguration
		// walletConfiguration=commanUtilDaoImpl.getWalletConfiguration();
		// System.out.println(commanUtilDaoImpl.getmailTemplet("welcome"));
		// System.out.println(commanUtilDaoImpl.getCountry());

		/*
		 * WalletConfiguration walletConfiguration=new WalletConfiguration();
		 * walletConfiguration.setCountryid(91);
		 * //walletConfiguration.setCountry("INDIA");
		 * walletConfiguration.setEmailvalid(0); walletConfiguration.setKycvalid(0);
		 * walletConfiguration.setOtpsendtomail(0);
		 * walletConfiguration.setCountrycurrency("INR");
		 * walletConfiguration.setStatus(0);
		 * //commanUtilDaoImpl.configureNewWallet(walletConfiguration);
		 * 
		 * commanUtilDaoImpl.getRechargeOperator();
		 */

		/*
		 * List<MerchantOffersMast> offerList=new ArrayList<MerchantOffersMast>();
		 * offerList.add(new MerchantOffersMast("AGGR001035", "abc", "123", "345", 10)
		 * ); offerList.add(new MerchantOffersMast("AGGR001035", "abc", "1231", "345",
		 * 11) ); offerList.add(new MerchantOffersMast("AGGR001035", "abc", "1232",
		 * "345", 12) ); offerList.add(new MerchantOffersMast("AGGR001035", "abc",
		 * "1233", "345", 13) );
		 */

		// commanUtilDaoImpl.uploadMerchantOffer(offerList);

		// commanUtilDaoImpl.getPartnerList();

		// commanUtilDaoImpl.getPlans("AIRTEL", "DELHI");

		// System.out.println(commanUtilDaoImpl.getOperatorCircle("19350"));
		// System.out.println("OXMA001074|MSEN055678|MBEN094729|19913.0".substring(0,"OXMA001074|MSEN055678|MBEN094729|19913.0".indexOf('|')));
		String dtl = "OXMA001074|MSEN055678|MBEN001047|19913.0";
		String res = new CommanUtilDaoImpl().getTransDtlFlag(dtl, "wallet");
		System.out.println(res);

	}



//	@Override
//	public int changeServiceStatus(ServiceMasterRequestBean serviceMasterBean) {
//		// TODO Auto-generated method stub
//		
//		int count = 0;
//
//		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),serviceMasterBean.getAggreatorid(),"", "", "", "",  "changeServiceStatus()");
//		//logger.info("Start excution ===================== method getPartnerList()");
//		factory = DBUtil.getSessionFactory();
//		session = factory.openSession();
//		Transaction trn = session.beginTransaction();
//		try {
//			
//			String updateQueryServiceMaster = "update ServiceMasterRequestBean sm set sm.status=:sts where sm.aggregatorId=:aggId and sm.servicesname=:sname and sm.type=:typ";
//			
//			Query qry = session.createQuery(updateQueryServiceMaster);
//			
//			if(serviceMasterBean.getStatus().toUpperCase().equals("UP")) {
//				qry.setParameter("sts", ""+1);
//			}
//			if(serviceMasterBean.getStatus().toUpperCase().equals("DOWN")) {
//				qry.setParameter("sts", ""+0);
//			}
//			
//			qry.setParameter("aggId", serviceMasterBean.getAggreatorid());
//			qry.setParameter("sname", serviceMasterBean.getServicesname());
//			qry.setParameter("typ", serviceMasterBean.getType());
//	        count = qry.executeUpdate();
//	        trn.commit();
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in changeServiceStatus"+ e.getMessage()+" "+e);
//		} finally {
//			session.close();
//		}
//		return count;
//	}
//
//
//
//	@Override
//	public ServiceMasterRequestBean getServiceStatusByAggId(ServiceMasterRequestBean serviceMasterBean) {
//		// TODO Auto-generated method stub
//		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),serviceMasterBean.getAggreatorid(),"", "", "", "",  "getServiceStatusByAggId()");
//		//logger.info("Start excution ===================== method getPartnerList()");
//		factory = DBUtil.getSessionFactory();
//		session = factory.openSession();
//		ServiceMasterRequestBean serviceReqBean;
//		try {
//			
//			Query query=session.createQuery("select status from ServiceMasterRequestBean sm where sm.aggregatorId=:aggId and sm.servicesname=:sname and sm.type=:typ");
//			query.setParameter("aggId", serviceMasterBean.getAggreatorid());
//			query.setParameter("sname", serviceMasterBean.getServicesname());
//			query.setParameter("typ", serviceMasterBean.getType());
//
//			String status = (String) query.uniqueResult();
//			
//			if(status.equals("1"))
//				serviceMasterBean.setStatus("UP");
//			if(status.equals("0"))
//				serviceMasterBean.setStatus("DOWN");
//			
//			
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "problem in getServiceStatusByAggId "+ e.getMessage()+" "+e);
//		} finally {
//			session.close();
//		}
//		return serviceMasterBean;
//	}
}
