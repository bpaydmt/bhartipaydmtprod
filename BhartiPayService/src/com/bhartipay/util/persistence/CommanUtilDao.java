package com.bhartipay.util.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.bean.AgentDetailsBean;
import com.bhartipay.util.bean.InstallationDetails;
import com.bhartipay.util.bean.InstallationDetailsBean;
import com.bhartipay.util.bean.MailConfigMast;
import com.bhartipay.util.bean.MailSendDetails;
import com.bhartipay.util.bean.MerchantOffersMast;
import com.bhartipay.util.bean.PartnerPrefundMastBean;
import com.bhartipay.util.bean.PaymentGatewayURL;
import com.bhartipay.util.bean.SMSConfigMast;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.bean.SupportTicketBean;
import com.bhartipay.util.bean.UserMenuMapping;
import com.bhartipay.util.bean.UserRoleMapping;
import com.bhartipay.util.bean.VersionControl;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.recharge.plan.bean.TariffPlans; 

public interface CommanUtilDao {
	
	//public WalletConfiguration getWalletConfiguration(int id);
	public WalletConfiguration getWalletConfiguration(String aggreatorid);
	public WalletConfiguration getAepsUpStatus(String aggreatorid);
	public WalletMastBean getUserDetailsById(WalletMastBean configBean);
	public WalletConfiguration saveWalletConfiguration(WalletConfiguration walletConfiguration);
	public String getTrxId(String name,String aggreatorid);
	public String createPassword(int len);
	public String generateWalletID(String mobileno);
	public String getsmsTemplet(String tampName,String aggreatorid);
	public String getmailTemplet(String tampName,String aggreatorid);
	public String getNotificationTemplet(String tampName);
	public Map<String,String> getCountry();
	public int checkLoginStatus(String mobile,String aggId);
	public Map<String,String> getKyc();
	public Map<String,String> getKycType();
	public InstallationDetailsBean saveInstallationDetails(InstallationDetailsBean installationDetailsBean);
	 public HashMap<String, HashMap> getRechargeOperator();
	 public String getBillerDetails(RechargeTxnBean rechargeTxnBean);
	 public MailConfigMast getmailDtl(String aggId);
	 public SMSConfigMast getSMSDtl(String aggId);
	 //public String getAggByUserId(String userId);
	 
	 public MailConfigMast getMailConfiguration(String aggreatorid);
	 public MailConfigMast saveMailConfiguration(MailConfigMast mailConfigMast);
	 
	 public SMSConfigMast getSMSConfiguration(String aggreatorid);
	 public SMSConfigMast saveSMSConfiguration(SMSConfigMast smsConfigMast);
	 
	 public String getcurrencyByCountryId(int countryId);
	 public String getAggIdByDomain(String domain);
	 public void saveSMSSendDetails(SMSSendDetails sMSSendDetails) ;
	 public void saveMailSendDetails(MailSendDetails mailSendDetails);
	 public SupportTicketBean saveUpdateSupportTicket(SupportTicketBean supportTicketBean,String ipiemi,String agent);
	 public List <SupportTicketBean> getSupportTicket(String aggreatorid);
	 public List <SupportTicketBean> getSupportTicketList(String aggreatorid,String userId,String stDate,String endDate);
	 public SupportTicketBean updateSupportTicket(int ticketId,String remarks);
	 public VersionControl getVersionControl(String aggId,int versionId );
	 public HashMap<Integer, String> getMenu();
	 public HashMap<String, String> getSubAggreator(String aggreator) ;
	 
	 public UserMenuMapping saveUserMenuMapping(UserMenuMapping userMenuMapping);
	 public UserMenuMapping getUserMenuMapping(String userId);
	 public Map<String,String> getIdProofKyc();
	 
	 
	 
	 public String getcurrencyByCountryCode(String countryId);
	 public Map<String,String> getCountryCode();
	 
	 public String uploadMerchantOffer(String aggreatorId,List<MerchantOffersMast> offerList);
	 public List<MerchantOffersMast> getMerchantOffer(String aggreatorId);
	 
	 
	 /**
	  * 
	  * @return
	  */
	 public HashMap<Integer, String> getRole();
	 public String getOperatorCircle(String numberSeries);
	 public UserRoleMapping saveUserRoleMapping(UserRoleMapping userRoleMapping);
	 
	 public UserRoleMapping saveUserRoleMappingNew(UserRoleMapping userRoleMapping);	 
	 
	 public UserRoleMapping getUserRoleMapping(String userId);
	 
	 public Boolean validatePan(String panNumber,String aggreatorId,int userType );
	 
	 
	 public List <AgentDetailsBean> getAgentDtlsByDistId(String aggreatorId,String distributerId,String serverName);
	 
	 public List <AgentDetailsBean> getAgentDtlsBySupDistId(String aggreatorId,String superDistributorid,String distributerId,String serverName);
	 
	 public List<AgentDetailsBean> getAgentDtlsBySo(String aggreatorId,String id,String idType, String serverName);
	 public List<AgentDetailsBean> getAgentDtlsBySoList(String aggreatorId,String id,String idType, String serverName,String uType); 
	 public String getVersionStatus(int version_number);
	 
	 public HashMap<String, String> getPartnerList();
	 	 
	 public HashMap<String, String> getTxnSourceList() ;
	 
	 public PartnerPrefundMastBean savePartnerPrefund(PartnerPrefundMastBean partnerPrefundMastBean);
	 
	 public TariffPlans getPlans(String serviceOperator, String circle);
	 
	 public InstallationDetails saveInstallationDetails(InstallationDetails installationDetails);
	 public PaymentGatewayURL savePaymentGatewayURL(PaymentGatewayURL paymentGatewayURL);
	 public String getPublicIP();
	 public String insertTransDtl(String dtl);
		
	 public String deleteTransDtl(String dtl);
		
	 public String getTransDtlFlag(String dtl,String txnType);
	 List<String> getListOfCashdepositBankByAggregator(String aggregatorId);
	public String getPostPaidBillerDetails(RechargeTxnBean rechargeTxnBean);
	public String getGasBillerDetails(RechargeTxnBean rechargeTxnBean);
	public String getInsuranceBillerDetails(RechargeTxnBean rechargeTxnBean);
	 
//	 public int changeServiceStatus(ServiceMasterRequestBean serviceMasterBean);
//	 public ServiceMasterRequestBean getServiceStatusByAggId(ServiceMasterRequestBean serviceMasterBean);

     public Boolean validateEmail(String email);
     public Boolean validateMobile(String mob);
     public Boolean validateAadhaar(String adhar, String aggid, int userType);
     
     public List<RechargeTxnBean> markPending(String str);
     public List<MudraMoneyTransactionBean> markDmtPending(String str);
     public List<WalletMastBean> enablePg(RechargeTxnBean txn );
     
     
}
