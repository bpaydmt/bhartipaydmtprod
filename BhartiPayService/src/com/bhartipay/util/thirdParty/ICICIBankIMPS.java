package com.bhartipay.util.thirdParty;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import com.bhartipay.util.XmlParser;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;

public class ICICIBankIMPS 
{
/*	//for test environment of ICICI
	final String URL="http://203.199.32.92:7474/imps-web-bc/api/transaction/ppi/IBCGoo00083/p2a?";
	final String URLVM="http://203.199.32.92:7474/imps-web-bc/api/transaction/ppi/IBCGoo00083/p2a-vm?";
	*/
	//for test environment
		final String URL="http://203.122.47.170:9080/ICICIIMPSService/rest/icici/service";
		final String URLVM="http://203.122.47.170:9080/FinoIMPS/rest/IMPS/impsrequest";
	
	//For production
	/*final String URL="https://impsbc.icicibank.co.in:7474/imps-web-bc/api/transaction/ppi/IBCApn00339/p2a?";
	final String URLVM="https://impsbc.icicibank.co.in:7474/imps-web-bc/api/transaction/ppi/IBCApn00339/p2a-vm?";*/
	
	 private static final Logger logger = Logger.getLogger(ICICIBankIMPS.class.getName());
	 
	 private static SSLSocketFactory createSslSocketFactory() throws Exception {
	        TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
	            public X509Certificate[] getAcceptedIssuers() {
	                return new X509Certificate[0];
	            }
	            public void checkClientTrusted(X509Certificate[] chain, String authType) {
	            }
	            public void checkServerTrusted(X509Certificate[] chain, String authType) {
	            }
	        } };
	        SSLContext sslContext = SSLContext.getInstance("TLS");
	       sslContext.init(null, byPassTrustManagers, new SecureRandom());
	        return sslContext.getSocketFactory();
	    }
	 
	 public String callIMPSServicePOSTSecured(String impsInput){
		String responseString="Fail";
		 try {
			 
			 // SSLContext sc = SSLContext.getInstance("TLS");
			  System.out.println("impsInput:"+impsInput);
			  
			  
			  SSLSocketFactory sslSocketFactory = createSslSocketFactory();
			  
			  HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
			  
			  URL url = new URL(URL);
			  HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
		
				 httpsCon.setHostnameVerifier(new HostnameVerifier() {
					
					@Override
					public boolean verify(String hostname, SSLSession session) {
						// TODO Auto-generated method stub
						return true;
					}
				});
				 httpsCon.setRequestMethod("POST");
				 httpsCon.setDoOutput(true);
				 httpsCon.setRequestProperty("Content-Type", "text/xml");
				 DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
				 if(impsInput!=null && impsInput.length()>1){
						wr.writeBytes(impsInput);	
					}
				 logger.info("\nSending 'POST' request to URL : " + url);
				 logger.info("impsInput********** : " + impsInput);
				 wr.flush();
				 wr.close();
				int responseCode = httpsCon.getResponseCode();
				logger.info("responseCode********** : " + responseCode);
				 
				 BufferedReader in = new BufferedReader(
					        new InputStreamReader(httpsCon.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					logger.info("swiftcore Response************************************"+response.toString());
					responseString =response.toString();
						  
			} catch (Exception e) 
		 		{
			    	e.printStackTrace();
		 		}
		 
		 return responseString;
		 
		 
		 
		 
	 }
	 
	 public String callIMPSServicePOST(String impsInput){
			String responseString="Fail";
			  HttpURLConnection httpsCon  = null;
			 try {
				 
				 // SSLContext sc = SSLContext.getInstance("TLS");
				  System.out.println("impsInput : "+impsInput);
	
				  
				  URL url = new URL(URL);
				  httpsCon = (HttpURLConnection) url.openConnection();
			
					 httpsCon.setRequestMethod("POST");
					 httpsCon.setDoOutput(true);
					 httpsCon.setRequestProperty("Content-Type", "text/xml");
					 DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
					 wr.writeBytes(impsInput);
					 wr.flush();
					 wr.close();

					int responseCode = httpsCon.getResponseCode();
					logger.info("\nSending 'POST' request to URL : " + url);
					logger.info("Post parameters : " + impsInput);
					logger.info("Response Code : " + responseCode);

						if(responseCode == 200)
						{
							BufferedReader in = new BufferedReader(
							        new InputStreamReader(httpsCon.getInputStream()));
							String inputLine;
							StringBuffer response = new StringBuffer();
	
							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
							}
							responseString = response.toString();
							in.close();
	
							//print result
							logger.info("Response : "+response.toString());
							System.out.println("response string "+response.toString());
						}
							  
				} catch (Exception e) 
			 		{
				    	e.printStackTrace();
			 		}
			 finally
			 {
				 logger.info("Closing http connection in icici in finally block");
				 httpsCon.disconnect();
			 }
			 return responseString;
	 }
	 
	 
	 public String callIMPSVMServicePOST(String impsInput){
			String responseString="Fail";
			 try {
				 
				  System.out.println("impsInput : "+impsInput);
	
				  
				  URL url = new URL(URLVM);
				  HttpURLConnection httpsCon = (HttpURLConnection) url.openConnection();
			
					 httpsCon.setRequestMethod("POST");
					 httpsCon.setDoOutput(true);
					 httpsCon.setRequestProperty("Content-Type", "text/xml");
						DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
						wr.writeBytes(impsInput);
						wr.flush();
						wr.close();

						int responseCode = httpsCon.getResponseCode();
						logger.info("\nSending 'POST' request to URLVM : " + url);
						logger.info("Post parameters : " + impsInput);
						logger.info("Response Code : " + responseCode);

						BufferedReader in = new BufferedReader(
						        new InputStreamReader(httpsCon.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						responseString = response.toString();
						in.close();

						//print result
						logger.info("Response : "+response.toString());
						System.out.println("response string "+response.toString());
							  
				} catch (Exception e) 
			 		{
				    	e.printStackTrace();
			 		}
			 
			 return responseString;
	 }
	 
	 
	 
	 public String callIMPSService(String impsInput){
		  logger.info("*********************ICICI Service calling***********************");
		  String responseString="Fail";
		  try {
			String txnStatus="http://203.199.32.92:7474/imps-web-bc/api/transaction/ppi/IBCMPu00085/p2a?"+impsInput ; 
			 SSLSocketFactory sslSocketFactory = createSslSocketFactory();
			  
			  HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);

			  URL url = new URL(txnStatus);
			  logger.info("*********************"+txnStatus+"***********************");
			  
			 HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
			 
			 httpsCon.setHostnameVerifier(new HostnameVerifier() {
				
				@Override
				public boolean verify(String hostname, SSLSession session) {
					// TODO Auto-generated method stub
					return true;
				}
			});
			  
			 httpsCon.setRequestMethod("GET");
			// httpsCon.setDoOutput(true);
			// DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
			
			int responseCode = httpsCon.getResponseCode();
			logger.info("responseCode********** : " + responseCode);
			 
			 BufferedReader in = new BufferedReader(
				        new InputStreamReader(httpsCon.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				logger.info(" Response************************************"+response.toString());
				responseString =response.toString();
			 
			 
			 
		  } catch (Exception e) {
			   e.printStackTrace() ;
			   System.out.println(e.getMessage());
			}
		 return responseString; 
		 }

	 
	 public String executeURL(String input) {
		String url=URL+input;
		logger.info("***************************************************executeURL***************************"+url);
	    HttpClient client = new HttpClient();
	    client.getParams().setParameter("http.useragent", "Test Client");

	    BufferedReader br = null;
	    String responseString="";
	    logger.info("URL:-"+url+"\n");
	    PostMethod method = new PostMethod(url);
	   try{
	      int returnCode = client.executeMethod(method);

	      if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
	        responseString= method.getResponseBodyAsString();
	      } else {
	        br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
	        responseString=br.readLine();
	        logger.info("returnCode:-"+returnCode);
	        logger.info("\n"+"response string :-"+responseString);
	      }
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	logger.info("*******************************************************************"+e);
	    } finally {
	      method.releaseConnection();
	      if(br != null) try { br.close(); } catch (Exception fe) {}
	    }
	    return responseString;

	  }
	 
	 
	public static void main(String[] args) {
		
		try
		{
		Date date = new Date();
		DateFormat dfor = new SimpleDateFormat("yyyyMMddHHmmss");
		IMPSIciciResponseBean l_impsResponse = new IMPSIciciResponseBean();
	System.out.println(dfor.format(date));
		
		
		
		String a="BeneAccNo=1119876543217&BeneIFSC=ICIC00HSBLW&Amount=10&TranRefNo=123737&PaymentRef=FTTransferP2A&RemName=RelianceRetailLimited&RemMobile=9860001122&RemMMID=8839123&PassCode=a764f85c68cd435fbfe6125032dc30ed&Timestamp=";
	//String a="BeneAccNo=1119876543217&BeneIFSC=ICIC0000001&Amount=10&TranRefNo=123733&PaymentRef=FTTransferP2A&RemName=RelianceRetailLimited&RemMobile=9860001122&RemMMID=8839123&PassCode=a764f85c68cd435fbfe6125032dc30ed&Timestamp=";
		String b="&DeliveryChannel=Internet";//ICIC00HSBLW
		 String input=a+dfor.format(date)+b;
		 //String input="BeneAccNo=1119876543217&BeneIFSC=ICIC9229154&Amount=1&TranRefNo=123456&PaymentRef=FTTransferP2A&RemName=RelianceRetailLimited&RemMobile=9860001122&RemMMID=8839123&PassCode=e5ca038b346f400e81b2d4ecbc3db1e3&Timestamp="+"20171115162500"+"&DeliveryChannel=Internet";
		 //String input="BeneAccNo=1119876543217&BeneIFSC=ICIC00HSBLW&Amount=1&TranRefNo=123456&PaymentRef=FTTransferP2A&RemName=RelianceRetailLimited&RemMobile=9860001122&RemMMID=8839123&PassCode=72e90f4143db494da8a997100e9ec628&Timestamp="+"201801041213500"+"&DeliveryChannel=Internet";
		 //String input="Amount=71.88&RemMobile=9741460755&TransactionDate=20161103140642&PassCode=1292fc028bb946279ba05b463bbaf799&TranRefNo=2016110314043455556176&PaymentRef=FTTransferP2A&RetailerCode=ibcngp00069&RemName=Reliance&BeneAccNo=654223594548796236&BeneIFSC=IDIB000A001";
		// new ICICIBankIMPS().callIMPSServicePOST(input);
		 
		 String bankRespXml= new ICICIBankIMPS().callIMPSServicePOST(input);

			
			
			logger.info("********************bankRespXml**for money transfer : IMPS ICICI **:"+bankRespXml);
			HashMap<String, String> bankResp=new XmlParser().xmlParser(bankRespXml);
			
			HashMap<String,String> errorResp =null;
			
			if(bankResp.get("ActCode").trim().equals("0"))
			{
				
			}
			else
			{
				errorResp=l_impsResponse.returnResposeToUser(bankResp);
				l_impsResponse.setActCode(Integer.parseInt(errorResp.get("ResponseCode").trim()));
				l_impsResponse.setResponseCode(1);
				l_impsResponse.setResponseString(errorResp.get("ResponseString"));
			}

			HashMap<String,String> errorRespVM =null;
			
			
			int count=0;
			
			while(((errorResp != null && (errorResp.get("ResponseCode").trim().equals("30") || errorResp.get("ResponseCode").trim().equals("11"))) || (errorRespVM != null &&(errorRespVM.get("ResponseCode").trim().equals("30") || errorRespVM.get("ResponseCode").trim().equals("11")  || errorRespVM.get("ResponseCode").trim().equals("91")))) && count <3)
			{
				
				Date date1 = new Date();				
				String input1=a+dfor.format(date1)+b;
				errorResp = null;
				errorRespVM=null;
				count ++;
				System.out.println(count);
				
				String bankRespVM = new ICICIBankIMPS().callIMPSVMServicePOST(input1);
				
				HashMap<String, String> bankResp1VM=new XmlParser().xmlParser(bankRespVM);
				
				//errorRespVM = new HashMap<String,String>();
				
				if(bankResp1VM.get("ActCode").trim().equals("0"))
				{}
				else
				{
					errorRespVM=l_impsResponse.returnResposeToUser(bankResp1VM);
					l_impsResponse.setActCode(Integer.parseInt(errorRespVM.get("ResponseCode").trim()));
					l_impsResponse.setResponse(errorRespVM.get("Response"));
					l_impsResponse.setResponseString(errorRespVM.get("ResponseString"));
				}
				//counter = CommanUtil.time(33);
				
			}
		//new ICICIBankIMPS().executeURL(input);
	
			
		
		/*
			 String input="BeneAccNo=1119876543217&BeneIFSC=ICIC00HSBLW&Amount=10&TranRefNo=123498&PaymentRef=FTTransferP2A&RemName=RelianceRetailLimited&RemMobile=9860001122&RemMMID=8839123&PassCode=3761b43b1d974588b76d27aae1e25b1b&Timestamp="+"20180108171200"+"&DeliveryChannel=Internet";
			String bankRespVM = new ICICIBankIMPS().callIMPSVMServicePOST(input);
		*/
		
		
		} 
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	}
	
}