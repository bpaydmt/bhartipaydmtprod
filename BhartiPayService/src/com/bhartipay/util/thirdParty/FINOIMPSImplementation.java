package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.util.EncryptionByEnc256;
import com.bhartipay.util.thirdParty.bean.FINOCredentials;
import com.bhartipay.util.thirdParty.bean.FINORequestBean;
import com.bhartipay.util.thirdParty.bean.FINORespData;
import com.bhartipay.util.thirdParty.bean.FINOResponseBean;
import com.google.gson.Gson;

public class FINOIMPSImplementation 
{
//	//final static String IMPS_URL="http://10.15.20.131:8061/Uiservice.svc/FinoMoneyTransactionApi/impsrequest";
//	final static String IMPS_URL="http://localhost:8080/FinoIMPS/rest/IMPS/impsrequest";
//	final static String GETSTATUS_URL="http://localhost:8080/FinoIMPS/rest/IMPS/impsrequest";
//	final static String NEFT_URL="http://localhost:8080/FinoIMPS/rest/IMPS/impsrequest";
//	//final String TEST_URL="http://203.199.32.92:7474/imps-web-bc/api/transaction/ppi/IBCSpi00062/p2a?";//IBCMPu00062
//	//final String TEST_URL="http://203.199.32.92:7474/imps-web-bc/api/transaction/ppi/IBCPay00015/p2a?";//IBCMPu00085
//	
//	private static final String HKEY="982b0d01-b262-4ece-a2a2-45be82212ba1";
//	private static final String PKEY="21d805f7-a299-4eac-804c-1f9c5649990c";
	 	 
	 private static final Logger logger = Logger.getLogger(FINOIMPSImplementation.class.getName());
	
	 
//	 final static String IMPS_URL="https://fpbs.finopaymentbank.in/FinoMoneyTransferService/UIService.svc/FinoMoneyTransactionApi/IMPSrequest";
//	 final static String NEFT_URL="https://fpbs.finopaymentbank.in/FinoMoneyTransferService/UIService.svc/FinoMoneyTransactionApi/NEFTrequest";
//	 final static String GETSTATUS_URL="https://fpbs.finopaymentbank.in/FinoMoneyTransferService/UIService.svc/FinoMoneyTransactionApi/TxnstatusRequest";
//	 private static final String HKEY="982b0d01-b262-4ece-a2a2-45be82212ba1";
//	 private static final String PKEY="d1f01fcd-9b79-40ff-b93b-2c10b5ef856d";
	 

	 private String getAuthentication(String headerData)
	 {
		 String header="";
		 try
		 {
		 	header=EncryptionByEnc256.encryptOpenSSL(headerData, FINOCredentials.FINO_HEADER_ENCRYPTION_KEY);
		       System.out.println("header :"+header);
                 }
		 catch(Exception e)
		 {
			 logger.info("*****problem in setAuthentication****e.message*****"+e.getMessage());
			 e.printStackTrace();
		 }
		 return header;
	 }
	 
	 public String callIMPSServicePOST(String impsInput,String headerData){
			String responseString="Fail";
			 HttpURLConnection httpCon = null;
			 try {
				 
				 // SSLContext sc = SSLContext.getInstance("TLS");
				  //System.out.println("impsInput : "+impsInput);
				  String encrptedReq = EncryptionByEnc256.encryptOpenSSL(impsInput,FINOCredentials.FINO_BODY_ENCRYPTION_KEY);
				  
				  URL url = new URL(FINOCredentials.FINO_IMPS_URL);
				  httpCon = (HttpURLConnection) url.openConnection();
			
					 httpCon.setRequestMethod("POST");
					 httpCon.setDoOutput(true);
					 
					 httpCon.setRequestProperty("Content-Type", "application/json");
					 httpCon.setRequestProperty("Authentication",getAuthentication(headerData));
					 DataOutputStream wr = new DataOutputStream(httpCon.getOutputStream());
					 logger.info("encrptedReq :"+encrptedReq);					
					 wr.writeBytes("\""+encrptedReq+"\"");
//wr.writeBytes("\"U2FsdGVkX1+yaukquYaIPh29eHYzfXfrIURUSZEcjrIQ1/mAm402x2zAKbSuT0p+/sBb74FOB4C3cc+4jzVjQelFm4QCXGnn4TOQrWhbvHXMl3obmLJFyt1nCt3lfZoLKE/ge48F/PwnCsK5hlRXZiV7xl0/rVRXa5cVzyVTdye2eC8kT5MfC2oG4b/k/vXTYbrz0C+/YRMwqXSGoP/5ec+jzgor8Pbw9TqK212+1opDKOc/L7/73S37NOuriRqKhKm4D+5OkLgNl754ShpoSjQav5DToHGo4kCxGeVc31W6woZVRkFxBeHUkKORcUVvqdTUJDrg7Z2RQINLKEt9VcwGbd5kmQH4Su6vLslArJGRVMPQ5FdRnq2gWE1DNG5U1SCKHPd8+O5axOXjvaWQlg==\"");

//wr.writeBytes("\"U2FsdGVkX1+56NLjRZ1Q+BQ7gybcyqHO2FzHZkGz/rRvoD0FLMt9YhKeX0YTBSiZDfeyUskr0CVyXUe19Tihr/rInNm601fjmd6j6mfFP6tIj/Q6muPKzXm+V9AT7U0D4RNK/1+pSHGLePuc8jbEF/NtgI2Gbl6YZ+CmLBwVUC7Pm6OReRqzH3oUbeA4JH1SinnGEjpQrxHk8lyliLz6WSp5SGz0cAPFJ+GwfkeHy0iacJdNBqYfRV7ki5xJvPqFcyq4g+6pEhPO3/7fWLMvR6wslNDJBzNG4k9fAYiQn5Qfp5Owc/2rHllpWj3Jssy0khl+r+/XOnGm+k7Evhf55UFiVP2quBB5v1ON6ifLDXaYBv4nZmQWgPIOVt71uueVd7uDS7X7g9C2ftJfCjrWaw==\"");
						wr.flush();
						wr.close();

						int responseCode = httpCon.getResponseCode();
						logger.info("\nSending 'POST' request to URL : " + url);
						logger.info("Post parameters : " + encrptedReq);
						logger.info("Response Code : " + responseCode);
						if(responseCode != 200)
						{
							logger.info("ClientUniqueId :"+impsInput+"Response Code : " + responseCode);
							return responseString;
						}
						BufferedReader in = new BufferedReader(
						        new InputStreamReader(httpCon.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();

						//print result
						logger.info("Response : "+response.toString());
						responseString=response.toString();
							  
				} catch (Exception e) 
			 		{
				    	e.printStackTrace();
			 		}
			 finally
			 {
				 httpCon.disconnect();
			 }
			 return responseString;
	 }
	 
	 
	 public String callNEFTServicePOST(String impsInput,String headerData){
			String responseString="Fail";
			HttpURLConnection httpCon  = null;
			 try {
				 
				 // SSLContext sc = SSLContext.getInstance("TLS");
				  System.out.println("impsInput : "+impsInput);
				  String encrptedReq = EncryptionByEnc256.encryptOpenSSL(impsInput,FINOCredentials.FINO_BODY_ENCRYPTION_KEY);
				  
				  URL url = new URL(FINOCredentials.FINO_NEFT_URL);
				  httpCon = (HttpURLConnection) url.openConnection();
			
					 httpCon.setRequestMethod("POST");
					 httpCon.setDoOutput(true);
					 
					 httpCon.setRequestProperty("Content-Type", "application/json");
					 httpCon.setRequestProperty("Authentication",getAuthentication(headerData));
						DataOutputStream wr = new DataOutputStream(httpCon.getOutputStream());
						wr.writeBytes("\""+encrptedReq+"\"");
						//wr.writeBytes(encrptedReq);
						wr.flush();
						wr.close();

						int responseCode = httpCon.getResponseCode();
						logger.info("\nSending 'POST' request to URL : " + url);
						logger.info("Post parameters : " + encrptedReq);
						logger.info("Response Code : " + responseCode);
						if(responseCode != 200)
						{
							return "Fail";
						}
						BufferedReader in = new BufferedReader(
						        new InputStreamReader(httpCon.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();

						//print result
						logger.info("Response : "+response.toString());
						responseString=response.toString();
							  
				} catch (Exception e) 
			 		{
				    	e.printStackTrace();
			 		}
			 finally
			 {
				 httpCon.disconnect();
			 }
			 return responseString;
	 }
	
	 public String callGetStatusServicePOST(String clientId,String headerData){
			String responseString="Fail";
			 HttpURLConnection httpCon = null;
			 try {

				 JSONObject jsonObject = new JSONObject();
				 jsonObject.put("ClientUniqueID", clientId);
				 // SSLContext sc = SSLContext.getInstance("TLS");
				  System.out.println("impsInput : "+jsonObject.toString());
				  String encrptedReq = EncryptionByEnc256.encryptOpenSSL(jsonObject.toString(),FINOCredentials.FINO_BODY_ENCRYPTION_KEY);
				  
				  URL url = new URL(FINOCredentials.FINO_STATUS_URL);
				  httpCon = (HttpURLConnection) url.openConnection();
			
					 httpCon.setRequestMethod("POST");
					 httpCon.setDoOutput(true);
					 
					 httpCon.setRequestProperty("Content-Type", "application/json");
					 httpCon.setRequestProperty("Authentication",getAuthentication(headerData));
						DataOutputStream wr = new DataOutputStream(httpCon.getOutputStream());
						wr.writeBytes("\""+encrptedReq+"\"");
						//wr.writeBytes(encrptedReq);
						wr.flush();
						wr.close();

						int responseCode = httpCon.getResponseCode();
						logger.info("\nSending 'POST' request to URL : " + url);
						logger.info("Post parameters : " + encrptedReq);
						logger.info("Response Code : " + responseCode);
						if(responseCode != 200)
						{
							return "Fail";
						}
						BufferedReader in = new BufferedReader(
						        new InputStreamReader(httpCon.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();

						//print result
						logger.info("Response : "+response.toString());
						responseString=response.toString();
							  
				} catch (Exception e) 
			 		{
				    	e.printStackTrace();
			 		}
			 finally
			 {
				 httpCon.disconnect();
			 }
			 
			 return responseString;
	 }
	
	 
		 
	 
	 
	 
	public static void main(String[] args) {

		
		
		
		try
		{
			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			Format f1 = new SimpleDateFormat("ddMMyyyyHHmmss");
			String clientid = f1.format(date.getTime());

			FINOCredentials.FINO_CLIENT_ID="79";
			FINOCredentials.FINO_AUTH_KEY="6e5fef67-3dae-4925-8271-cb0ca4a3fa11";
			FINOCredentials.FINO_HEADER_ENCRYPTION_KEY="982b0d01-b262-4ece-a2a2-45be82212ba1";
			FINOCredentials.FINO_BODY_ENCRYPTION_KEY="d1f01fcd-9b79-40ff-b93b-2c10b5ef856d";
			FINOCredentials.FINO_IMPS_URL="https://fpbs.finopaymentbank.in/FinoMoneyTransferService/UIService.svc/FinoMoneyTransactionApi/IMPSrequest";

			try {
				File file = new File("C:\\Users\\Ambuj Kumar Singh\\Desktop\\impsverification.txt");
				FileWriter writer = new FileWriter("C:\\Users\\Ambuj Kumar Singh\\Desktop\\output.txt");
				FileReader fr=new FileReader(file);   //reads the file  
				BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream  
				StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters  
				String line;  
				int count = 1;
				while((line=br.readLine())!=null)  
				{  
				String [] impsData = line.split(",");
				System.out.println();
				System.out.println();//appends line to string buffer  
		     
				String transactionRef ="PAY"+clientid+count++;
				
				FINORequestBean req = new FINORequestBean();

				req.setBeneAccountNo(impsData[0].trim());
				req.setBeneIFSCCode(impsData[1].trim());
				req.setBeneName("Akhand");
				req.setCustomerMobileNo("7424004004");
				req.setCustomerName("BhartiPay");
				req.setClientUniqueID(transactionRef);
				req.setrFU1("null");
				req.setAmount(1);
				req.setrFU2("String content");
				req.setrFU3("String content");
				req.setProductCode("String content");

				String parm = new Gson().toJson(req);
				System.out.println(parm);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("ClientId", "79");
				jsonObject.put("AuthKey", "6e5fef67-3dae-4925-8271-cb0ca4a3fa11");
				System.out.println("heaFINOCredentials.FINO_HEADER_ENCRYPTION_KEY:" + jsonObject.toString());
				System.out.println("body :" + parm);
				FINOIMPSImplementation fino = new FINOIMPSImplementation();
				try {
					String response = fino.callIMPSServicePOST(parm, jsonObject.toString());
	            	//String response="{\"ResponseCode\":0,\"MessageString\":\"Transaction Successful\",\"DisplayMessage\":\"Transaction Successful\",\"RequestID\":\"ECO_SYS_USR5_0915202013102741\",\"ClientUniqueID\":\"15092020131015\",\"ResponseData\":\"U2FsdGVkX183mv5xArCum2+K0w2raJx0sj1t+8RcGpBRVnyS0AUG\\/T9IMWW3Ubt+MYmCe9psI+i4ReVTy6MWznLbeut3K7qE3QCDPKYZF6saMjulIjWJAmXg\\/wBHdE0V+uenUNPC6kICfqchm\\/tsCLZVbTJSOtBLIZEZI7wSg7atXZ7VWslxIEBynnn+lXq8XUHuTg0lVmvUVHS4Tv1AswqCI5pSA5qYq9hWCBnwuf1t3nAcrwJyfQCWK4l7no3QObaALN0eFmPf0hMua6fx9Ly\\/QUc5q03OqZNRMzGsQgvK0GOqVVMM8MQ3gTd21NNhSrvYuvNbGZCjp7QLp\\/PvKhMqo5yQoRglP9HUiYAbAK6HdjeeB8dFSMGflGIKXmEx\"}\r\n";//fino.callIMPSServicePOST(parm,jsonObject.toString());
	               System.out.println("IMPS Response:"+response);
	               FINOResponseBean finores = new Gson().fromJson(response, FINOResponseBean.class);
	               String encData = finores.getResponseData().replace("\\", "");
	               String decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY,encData);
	               FINORespData res = new Gson().fromJson(decData, FINORespData.class);
	              
	               System.out.println(req.getBeneAccountNo()+","+req.getBeneIFSCCode()+","+res.getBeneName()+","+res.getTxnID()+","+finores.getDisplayMessage());
	               writer.write(req.getBeneAccountNo()+","+req.getBeneIFSCCode()+","+res.getBeneName()+","+res.getTxnID()+","+finores.getDisplayMessage()+","+transactionRef);
	               writer.write("\n");
				}catch (Exception e) {
				System.out.println(req.getBeneAccountNo()+"--"+e.getMessage());
				
				}

				}  
				writer.close();
				fr.close();    //closes the stream and release the resources  
				System.out.println("Contents of File: ");  
				System.out.println(sb.toString());   //returns a string that textually represents the object  
				}  
				catch(Exception e)  
				{  
				e.printStackTrace();  
				}
				
		} 
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}

