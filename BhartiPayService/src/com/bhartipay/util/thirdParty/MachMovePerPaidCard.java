package com.bhartipay.util.thirdParty;

import java.security.Security;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.bhartipay.matchmove.mmpay.api.Connection;
import com.bhartipay.matchmove.mmpay.api.HttpRequest;
import com.bhartipay.user.bean.SmartCardBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.thirdParty.bean.MatchMoveBean;
import com.google.gson.Gson;


public class MachMovePerPaidCard {
	private static final Logger logger = Logger.getLogger(MachMovePerPaidCard.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	//String host = "https://beta-api.mmvpay.com/indemo/v1";//devlopment
	String host = "https://beta-api.mmvpay.com/inpayg/v1";//sandbox
	String key = "tOnrLTwMiO6lpNRJXvDJFoL6qy4qDKvr";
	String secret = "TWlN7LirS2IQGAWn96wj7QiSJEnNqbY1unpgejAW2hKndhFP906sIByZpMY0oC7H";
	
	
	
	public MatchMoveBean fetchingCardType(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			Connection anoConnection = new Connection(host, key, secret);	
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			JSONObject user = anoConnection.consume("users/wallets/cards/types");
			System.out.println(user);
			if(smartCardBean.getCardType().equalsIgnoreCase("VC")){
			matchMoveBean.setDescription(user.getJSONArray("types").getJSONObject(0).getString("description"));
			matchMoveBean.setCardName(user.getJSONArray("types").getJSONObject(0).getString("name"));
			matchMoveBean.setCode(user.getJSONArray("types").getJSONObject(0).getString("code"));
			}else{
				matchMoveBean.setDescription(user.getJSONArray("types").getJSONObject(1).getString("description"));
				matchMoveBean.setCardName(user.getJSONArray("types").getJSONObject(1).getString("name"));
				matchMoveBean.setCode(user.getJSONArray("types").getJSONObject(1).getString("code"));
			}
			
			
			String cardId =smartCardBean.getPrePaidId();
			JSONObject userWallet = anoConnection.consume("users/wallets/cards/" + cardId);
			
			matchMoveBean.setCardNumber( userWallet.getString("number"));
			matchMoveBean.setHolderName(userWallet.getJSONObject("holder").getString("name"));
			matchMoveBean.setExpiry(userWallet.getJSONObject("date").getString("expiry"));
			
			
			
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in fetchingCardType==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	
	
	public MatchMoveBean fetchingCardTypeCode(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);	
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			JSONObject cardTypes = anoConnection.consume("users/wallets/cards/types");
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			String cardType = cardTypes.getJSONArray("types").getJSONObject(0).getString("code");
			logger.info("problem in cardType======**************====**************************======="+cardType);
			JSONObject user = anoConnection.consume("users/wallets/cards/" + cardType, HttpRequest.METHOD_POST);
			logger.info("fetchingCardTypeCode response**********************************"+user);
			matchMoveBean=gson.fromJson(user.toString(), MatchMoveBean.class);
	
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in fetchingCardTypeCode==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
   public SmartCardBean updatePrePaidCard(SmartCardBean smartCardBeanin){
	MatchMoveBean matchMoveBean=new MatchMoveBean();
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	logger.info("problem in updatePrePaidCard======**************===========");
	try{
	SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
	Gson gson = new Gson();
	Security.addProvider(new BouncyCastleProvider());
	Connection anoConnection = new Connection(host, key, secret);
	
	Map<String, String> userData = new HashMap<String, String>();
	
	if(smartCardBeanin!=null && smartCardBeanin.getEmailId()!=null && !smartCardBeanin.getEmailId().isEmpty()&&!(smartCardBean.getEmailId().equalsIgnoreCase(smartCardBeanin.getEmailId())))
			userData.put("email", smartCardBeanin.getEmailId());
	
	if(smartCardBeanin!=null && smartCardBeanin.getTitle()!=null && !smartCardBeanin.getTitle().isEmpty())
		userData.put("title", smartCardBeanin.getTitle());
		
	if(smartCardBeanin.getCountryCode()>0)
		userData.put("mobile_country_code", Integer.toString(smartCardBeanin.getCountryCode()));
	
	if(smartCardBeanin!=null && smartCardBeanin.getMobileNo()!=null && !smartCardBeanin.getMobileNo().isEmpty())
		userData.put("mobile", smartCardBeanin.getMobileNo());
	
	if(smartCardBeanin!=null && smartCardBeanin.getIdType()!=null && !smartCardBeanin.getIdType().isEmpty())
		userData.put("id_type", smartCardBeanin.getIdType());
	
	if(smartCardBeanin!=null && smartCardBeanin.getIdNumber()!=null && !smartCardBeanin.getIdNumber().isEmpty())
		userData.put("id_number", smartCardBeanin.getIdNumber());
	
	if(smartCardBeanin!=null && smartCardBeanin.getCountryOfIssue()!=null && !smartCardBeanin.getCountryOfIssue().isEmpty())
		userData.put("country_of_issue", smartCardBeanin.getCountryOfIssue());
	
	if(smartCardBeanin!=null && smartCardBeanin.getNationality()!=null && !smartCardBeanin.getNationality().isEmpty())
		userData.put("nationality", smartCardBeanin.getNationality());
	
	if(smartCardBeanin!=null && smartCardBeanin.getGender()!=null && !smartCardBeanin.getGender().isEmpty())
		userData.put("gender", smartCardBeanin.getGender());
	
		
	anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
	JSONObject user = anoConnection.consume("users", HttpRequest.METHOD_PUT, userData);
	logger.info("updatePrePaidCard response**********************************"+user);
	matchMoveBean=gson.fromJson(user.toString(), MatchMoveBean.class);
	
	
	if(!matchMoveBean.getId().isEmpty()){
		transaction=session.beginTransaction();
		if(smartCardBeanin!=null && smartCardBeanin.getEmailId()!=null && !smartCardBeanin.getEmailId().isEmpty())
			smartCardBean.setEmailId( smartCardBeanin.getEmailId());
		if(smartCardBeanin!=null && smartCardBeanin.getMobileNo()!=null && !smartCardBeanin.getMobileNo().isEmpty())
			smartCardBean.setMobileNo(smartCardBeanin.getMobileNo());
		session.update(smartCardBean);
		transaction.commit();
		smartCardBeanin.setStatusCode("1000");
		smartCardBeanin.setStatusDesc("Profile Updated.");
	}	
	
	}catch (Exception e) {
		smartCardBeanin.setStatusCode("7000");
		smartCardBeanin.setStatusDesc(e.getMessage());
		e.printStackTrace();
		logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
	} finally {
		
		session.close();
	}	
	
	return smartCardBeanin;
		
	}
	
	
	
	public MatchMoveBean reqPrePaidCard(String reqId){
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			// Initialize
				Connection anoConnection = new Connection(host, key, secret);
				
				//System.out.println("users/wallets/cards/types~GET: " + anoConnection.consume("users/wallets/cards/types"));
				anoConnection.setSession(new PrePaidSession());
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("email", smartCardBean.getEmailId());
				userData.put("password", smartCardBean.getPassowrd());
				userData.put("first_name", smartCardBean.getName());
				userData.put("last_name", smartCardBean.getLastName());
				userData.put("preferred_name", smartCardBean.getPreferredName());
				userData.put("mobile_country_code", Integer.toString(smartCardBean.getCountryCode()));
				userData.put("mobile", smartCardBean.getMobileNo());
				matchMoveBean=gson.fromJson(anoConnection.consume("users", HttpRequest.METHOD_POST, userData).toString(), MatchMoveBean.class);
				logger.info("users/wallets/funds~POST*******************************: " + matchMoveBean.getId());
				if(!matchMoveBean.getId().isEmpty()){
					anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
					JSONObject response = anoConnection.consume("users/wallets", HttpRequest.METHOD_POST);
					logger.info("reqPrePaidCard response**********************************"+response);
					
				}
				
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in reqPrePaidCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;	
	}
	
	
	
	
	
	
	
	
	public MatchMoveBean fetchingCreatedUserdetail(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			Connection anoConnection = new Connection(host, key, secret);	
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			JSONObject user = anoConnection.consume("users");
			System.out.println(user);
			matchMoveBean=gson.fromJson(user.toString(), MatchMoveBean.class);
	
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in fetchingCreatedUserdetail==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	
	public MatchMoveBean fetchingWalletdetail(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			Connection anoConnection = new Connection(host, key, secret);	
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			JSONObject user = anoConnection.consume("users/wallets");
			System.out.println(user);
			matchMoveBean=gson.fromJson(user.toString(), MatchMoveBean.class);
	
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in fetchingWalletdetail==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	
	
	public MatchMoveBean attachedPrePaidCard(String reqId,String prePaidCardNumber,String prePaidCardPin){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			// Initialize
				Connection anoConnection = new Connection(host, key, secret);
				
				JSONObject cardTypes = anoConnection.consume("users/wallets/cards/types", "GET", null);
				anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
				String cardType = cardTypes.getJSONArray("types").getJSONObject(1).getString("code");
				
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("assoc_number","PY"+prePaidCardPin );
				JSONObject response = anoConnection.consume("users/wallets/cards/" + cardType, HttpRequest.METHOD_POST,userData);
				matchMoveBean=gson.fromJson(response.toString(), MatchMoveBean.class);
				logger.info("attachedPrePaidCard Response**********************************"+response);
			}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in attachedPrePaidCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	
	
	public MatchMoveBean attachedVirtualPrePaidCard(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			
			// Initialize
				Connection anoConnection = new Connection(host, key, secret);
				
				JSONObject cardTypes = anoConnection.consume("users/wallets/cards/types", "GET", null);
				anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
				String cardType = cardTypes.getJSONArray("types").getJSONObject(0).getString("code");
				JSONObject response = anoConnection.consume("users/wallets/cards/" + cardType, HttpRequest.METHOD_POST);
				matchMoveBean=gson.fromJson(response.toString(), MatchMoveBean.class);
				System.out.println(response);
			}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in acceptSmartCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	public  MatchMoveBean activatePhysicalCard (String reqId ,String activation_code){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			// Initialize
			Connection anoConnection = new Connection(host, key, secret);
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			//JSONObject userWallet = anoConnection.consume("users/wallets");
			//String cardId = userWallet.getJSONArray("cards").getJSONObject(0).getString("id");
			String cardId =smartCardBean.getPrePaidId();
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("activation_code", activation_code);
			JSONObject cardTokens = anoConnection.consume("users/wallets/cards/" + cardId, HttpRequest.METHOD_PUT, userData);
			System.out.println(cardTokens);
			matchMoveBean=gson.fromJson(cardTokens.toString(), MatchMoveBean.class);
			System.out.println(cardTokens);
			}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in activatePhysicalCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;	
	}
	

	public  MatchMoveBean terminatePrePaidCard(String reqId ,String type){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);
			
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			//JSONObject userWallet = anoConnection.consume("users/wallets");
			//String cardId = userWallet.getJSONArray("cards").getJSONObject(0).getString("id");
			String cardId =smartCardBean.getPrePaidId();
			JSONObject userWallet = anoConnection.consume("users/wallets/cards/" + cardId);
		
			String cardNumber = userWallet.getString("number");
			//logger.info(walletMastBean.getPrePaidCardNumber()+"***********************card_number******************"+cardNumber);
			//if(cardNumber.contentEquals(walletMastBean.getPrePaidCardNumber())){
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("type",type);
				JSONObject response = anoConnection.consume("users/wallets/cards/" +  cardId, HttpRequest.METHOD_DELETE, userData);
				//cardTokens.getString("id")
				System.out.println(response);
				matchMoveBean.setId(response.getString("id"));
				matchMoveBean.setBlockedStatus(response.getString("status"));
				matchMoveBean.setNumber(response.getString("number"));
			//}

		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in acceptSmartCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
		
	}
	
	
	public  MatchMoveBean suspendPrePaidCard(String reqId ){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);
			
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			//JSONObject userWallet = anoConnection.consume("users/wallets");
			//String cardId = userWallet.getJSONArray("cards").getJSONObject(0).getString("id");
			String cardId =smartCardBean.getPrePaidId();
			JSONObject userWallet = anoConnection.consume("users/wallets/cards/" + cardId);
			String cardNumber = userWallet.getString("number");
			//logger.info(walletMastBean.getPrePaidCardNumber()+"***********************card_number******************"+cardNumber);
			//if(cardNumber.contentEquals(walletMastBean.getPrePaidCardNumber())){
				JSONObject response = anoConnection.consume("users/wallets/cards/" +  walletMastBean.getPrePaidCardId(), HttpRequest.METHOD_DELETE);
				System.out.println(response);
				matchMoveBean.setId(response.getString("id"));
				matchMoveBean.setBlockedStatus(response.getString("status"));
				matchMoveBean.setNumber(response.getString("number"));
			//}

		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in acceptSmartCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
		
	}
	
	
	public MatchMoveBean resumePrePaidCard(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);
			JSONObject cardTypes = anoConnection.consume("users/wallets/cards/types", "GET", null);
				anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
				String cardType = cardTypes.getJSONArray("types").getJSONObject(0).getString("code");
				String cardId =smartCardBean.getPrePaidId();
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("id", cardId);
				JSONObject response = anoConnection.consume("users/wallets/cards", HttpRequest.METHOD_POST,userData);
				logger.info("resumePrePaidCard response**********************************"+response);
				matchMoveBean.setId(response.getString("id"));
				matchMoveBean.setBlockedStatus(response.getString("status"));
				matchMoveBean.setNumber(response.getString("number"));
				
			}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in resumePrePaidCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
	
	
	
	
	
	
	public MatchMoveBean retrieveUserCard(String reqId){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			
			Gson gson = new Gson();
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);
			
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			//JSONObject userWallet = anoConnection.consume("users/wallets");
			//String cardId = userWallet.getJSONArray("cards").getJSONObject(0).getString("id");
			String cardId =smartCardBean.getPrePaidId();
			JSONObject userWallet = anoConnection.consume("users/wallets/cards/" + cardId);
			String cardNumber = userWallet.getString("number");
			matchMoveBean=gson.fromJson(userWallet.toString(), MatchMoveBean.class);
			System.out.println(userWallet);
		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in acceptSmartCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
	}
		
	
	
	
	public SmartCardBean updateAddress(SmartCardBean smartCardBeanin){
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try{
			logger.info("value for smartCardBeanin.getType()==================" + smartCardBeanin.getType());
			logger.info("value for smartCardBeanin.getCountry()=====*********============" +smartCardBeanin.getCountry());
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		
		Map<String, String> userData = new HashMap<String, String>();
		
		if(smartCardBeanin!=null && smartCardBeanin.getAddress_1()!=null && !smartCardBeanin.getAddress_1().isEmpty())
				userData.put("address_1", smartCardBeanin.getAddress_1());
		
		if(smartCardBeanin!=null && smartCardBeanin.getAddress_2()!=null && !smartCardBeanin.getAddress_2().isEmpty())
			userData.put("address_2", smartCardBeanin.getAddress_2());
			
		if(smartCardBeanin!=null && smartCardBeanin.getCity()!=null && !smartCardBeanin.getCity().isEmpty())
			userData.put("city", smartCardBeanin.getCity());
		
		if(smartCardBeanin!=null && smartCardBeanin.getState()!=null && !smartCardBeanin.getState().isEmpty())
			userData.put("state", smartCardBeanin.getState());
		
		if(smartCardBeanin!=null && smartCardBeanin.getCountry()!=null && !smartCardBeanin.getCountry().isEmpty())
			userData.put("country", smartCardBeanin.getCountry());
		
		if(smartCardBeanin!=null && smartCardBeanin.getZipcode()!=null && !smartCardBeanin.getZipcode().isEmpty())
			userData.put("zipcode", smartCardBeanin.getZipcode());
		
		anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
		JSONObject address = anoConnection.consume("users/addresses/"+smartCardBeanin.getType(), HttpRequest.METHOD_PUT, userData);
		System.out.println(address);
		matchMoveBean=gson.fromJson(address.toString(), MatchMoveBean.class);
		smartCardBeanin.setStatusCode("1000");
		smartCardBeanin.setStatusDesc("Address Updated.");
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
			
		}
		
	
	public SmartCardBean updateKYCStatus(SmartCardBean smartCardBeanin){
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try{
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		
		Map<String, String> userData = new HashMap<String, String>();
		
		userData.put("status", smartCardBeanin.getKycStatus());
		anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
		matchMoveBean=gson.fromJson(anoConnection.consume("users/authentications/statuses", HttpRequest.METHOD_PUT, userData).toString(), MatchMoveBean.class);
		if(!matchMoveBean.getId().isEmpty()){
			smartCardBeanin.setStatusCode("1000");
			smartCardBeanin.setStatusDesc("KYC Status Updated.");
		}	
		
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
		
	}
	
	public SmartCardBean updatePassword(SmartCardBean smartCardBeanin){	
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		logger.info("Start excution ************************************ method updatePassword(aggreatorId)"+smartCardBeanin.getReqId());
		logger.info("Start excution ************************************ method updatePassword(aggreatorId)"+smartCardBeanin.getPassowrd());
		try{
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		Map<String, String> userData = new HashMap<String, String>();
		userData.put("email", smartCardBeanin.getEmailId());
		userData.put("password", smartCardBeanin.getPassowrd());
		JSONObject response=anoConnection.consume("oauth/password", HttpRequest.METHOD_POST, userData);
		logger.info("updatePassword response**********************************"+response);
		
		String status = response.getString("status");
		
		if(status.equalsIgnoreCase("success")){
			transaction=session.beginTransaction();
			smartCardBean.setPassowrd(smartCardBeanin.getPassowrd());
			session.update(smartCardBean);
			transaction.commit();
			smartCardBeanin.setStatusCode("1000");
			smartCardBeanin.setStatusDesc(status);
		}else{
			smartCardBeanin.setStatusCode("1001");
			smartCardBeanin.setStatusDesc(status);
		}
		
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
	}
	
	public SmartCardBean pinReset(SmartCardBean smartCardBeanin){	
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try{
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		String cardId =smartCardBean.getPrePaidId();
		Map<String, String> userData = new HashMap<String, String>();
		userData.put("mode", smartCardBeanin.getChangePinMode());
		JSONObject response = anoConnection.consume("users/wallets/cards/" + cardId + "/pins/reset", HttpRequest.METHOD_GET, userData);
		System.out.println(response);
		logger.info("pinReset response**********************************"+response);
		String status = response.getString("status");
		
		if(status.equalsIgnoreCase("success")){
			smartCardBeanin.setStatusCode("1000");
			smartCardBeanin.setStatusDesc(status);
		}else{
			smartCardBeanin.setStatusCode("1001");
			smartCardBeanin.setStatusDesc(status);
		}
		
		
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
	}
	
	
	public SmartCardBean generateCVV(SmartCardBean smartCardBeanin){	
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		smartCardBeanin.setStatusCode("1001");
		try{
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
		JSONObject userWallet = anoConnection.consume("users/wallets");
		String cardId =smartCardBean.getPrePaidId();
		JSONObject response = anoConnection.consume("users/wallets/cards/" + cardId + "/securities/tokens");
		logger.info("generateCVV response**********************************"+response);
		
		String cvv=response.getString("value");
		transaction=session.beginTransaction();
		smartCardBean.setCvv(cvv);
		session.update(smartCardBean);
		transaction.commit();
		smartCardBeanin.setCvv(cvv);
		smartCardBeanin.setStatusCode("1000");
		smartCardBeanin.setStatusDesc("Success");
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in generateCVV==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
	}
	
	
	
	
	public  MatchMoveBean deleteUser(String reqId ,String state){
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		try{
			SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			Security.addProvider(new BouncyCastleProvider());
			Connection anoConnection = new Connection(host, key, secret);
			anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
			String userId =smartCardBean.getPrePaidUserId();
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("state",state);
				JSONObject user = anoConnection.consume("users/"+userId, HttpRequest.METHOD_DELETE, userData);
				System.out.println(user);
				matchMoveBean.setId(user.getString("id"));
				matchMoveBean.setBlockedStatus(user.getString("status"));
				
			

		}catch (Exception e) {
			matchMoveBean.setError(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in deleteCard==================" + e.getMessage(), e);
		} finally {
			session.close();
		}	
		
	return matchMoveBean;
		
	}
	
	
	
	
	public SmartCardBean deleteCard(SmartCardBean smartCardBeanin){	
		MatchMoveBean matchMoveBean=new MatchMoveBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try{
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, smartCardBeanin.getReqId());
		Gson gson = new Gson();
		Security.addProvider(new BouncyCastleProvider());
		Connection anoConnection = new Connection(host, key, secret);
		anoConnection.authenticate(smartCardBean.getEmailId(), smartCardBean.getPassowrd());
		JSONObject userWallet = anoConnection.consume("users/wallets");
		String cardId =smartCardBean.getPrePaidId();
		JSONObject transaction = anoConnection.consume("users/wallets/cards/" + cardId + "/securities/tokens");
		System.out.println(transaction);
		
		}catch (Exception e) {
			smartCardBeanin.setStatusCode("7000");
			smartCardBeanin.setStatusDesc(e.getMessage());
			e.printStackTrace();
			logger.debug("problem in updatePrePaidCard==================" + e.getMessage(), e);
		} finally {
			
			session.close();
		}	
		
		return smartCardBeanin;
	}
	
	
	
	
	
	
public static void main(String[] args) {
	SmartCardBean smartCardBeaninaa=new SmartCardBean();
	//new MachMovePerPaidCard().terminatePrePaidCard("SCRD001010","lost");
	//new MachMovePerPaidCard().activatePhysicalCard("SCRD001010","123545");
	new MachMovePerPaidCard().fetchingCardType("SCRD001023");
	/*;
	smartCardBeaninaa.setReqId("SCRD001013");
	new MachMovePerPaidCard().pinReset(smartCardBeaninaa);*/
	//new MachMovePerPaidCard().attachedVirtualPrePaidCard("SCRD001013");
	//smartCardBeaninaa.setReqId("SCRD001013");
	//new MachMovePerPaidCard().generateCVV(smartCardBeaninaa);
	
	
	/*smartCardBeaninaa.setReqId("SCRD001018");
	smartCardBeaninaa.setEmailId("demo@demo.com");
	smartCardBeaninaa.setPassowrd("Demo@1234");
	new MachMovePerPaidCard().updatePassword(smartCardBeaninaa);*/
}
}
