package com.bhartipay.util.thirdParty;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class EncryptionByEnc256
{

 public EncryptionByEnc256()
 {
 }

 public static String encrypt(String textToEncrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(1, makeKey(key), makeIv());
         return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 public static String decrypt(String textToDecrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(2, makeKey(key), makeIv());
         return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 private static AlgorithmParameterSpec makeIv()
 {
     try
     {
         return new IvParameterSpec("0123456789abcdef".getBytes("UTF-8"));
     }
     catch(UnsupportedEncodingException e)
     {
         e.printStackTrace();
     }
     return null;
 }

 private static Key makeKey(String encryptionKey)
 {
     try
     {
         byte key[] = Base64.decodeBase64(encryptionKey.getBytes());
         return new SecretKeySpec(key, "AES");
     }
     catch(Exception e)
     {
         e.printStackTrace();
     }
     return null;
 }

 public static String generateMerchantKey()
 {
     String newKey = null;
     try
     {
         KeyGenerator kgen = KeyGenerator.getInstance("AES");
         kgen.init(256);
         SecretKey skey = kgen.generateKey();
         byte raw[] = skey.getEncoded();
         newKey = new String(Base64.encodeBase64(raw));
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
     return newKey;
 }

 private static final String ENCRYPTION_IV = "0123456789abcdef";
 private static final String PADDING = "AES/CBC/PKCS5Padding";
 private static final String ALGORITHM = "AES";
 private static final String CHARTSET = "UTF-8";

 static 
 {
     try
     {
         Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
         field.setAccessible(true);
         field.set(null, Boolean.FALSE);
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
 }
 public static void main(String[] args) {
	String encString=encrypt("powerpay technologies", "3JchfNzgbdr03Zwxu9BYsMRXhB7woyTqCVFxmU2RsOs=");
	System.out.println(encString);
	System.out.println(decrypt("rTCNtGAM/BHGwaH4B/6t9QKpzhZkWOo4rYiJrsxyqxoUhEvL9QF22eAGnbR2a4pA/bK7U+8G1Me9cQvJcN6MJwKG1XH8bC1PM2g7WBpYU111VaImWq82crb2srLczFgM6Y9i+Avdi/ILB2dDIeBGYTr3/ZiTDZGlCvFsq+0xg0VUpBw6mI3k8/HSUGBLF0E7oygUV3NQoT9Sp4RtgDFFwHZDootpNky9Q00s6/kyczM=","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("U+kBzE9gY1eHZrtJ8RqL0A==","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("gyqLPVCNBuyjchp5Nrm04g==","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("UgUTQLM9Sg40mGdUInpvRZTrq5nHcm9jq3ZAVZ3iuxbSnh88TaTaq5O9FalJ8BkUWou3P0r4x9xywayF60H2eQXAFE40czQqOKHFoqvFeeA=","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("5qwtvE6AIFyYOp2s7ti9PrsqrKc2uwmgw6S3oYkavHV/AEvcWH0Rb6A+NVqDtaVK","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("5n9IgY16ET/BTn6XF5MbFQ==","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	//System.out.println(decrypt("5n9IgY16ET/BTn6XF5MbFQ==","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	System.out.println(decrypt("gyqLPVCNBuyjchp5Nrm04g==","U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI="));
	
	
	
	
 }
}


