package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.XML;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;
import com.bhartipay.payword.bean.DirectParameters;
import com.bhartipay.payword.bean.Parameter;
import com.bhartipay.payword.bean.PayworldJsonRequest;
import com.bhartipay.payword.bean.PayworldRequest;
import com.bhartipay.payword.bean.PayworldResponseData;
import com.bhartipay.payworld.masterdata.Product;
import com.bhartipay.recharge.request.AgentDeviceInfo;
import com.bhartipay.recharge.request.AmountInfo;
import com.bhartipay.recharge.request.BillPaymentRequest;
import com.bhartipay.recharge.request.CustomerInfo;
import com.bhartipay.recharge.request.Info;
import com.bhartipay.recharge.request.Input;
import com.bhartipay.recharge.request.InputParams;
import com.bhartipay.recharge.request.PaymentInfo;
import com.bhartipay.recharge.request.PaymentMethod;
import com.bhartipay.recharge.response.ExtBillPayResponse;
import com.bhartipay.recharge.response.Result;
import com.bhartipay.rechargeService.bean.RechargeOperatorMasterBean;
import com.bhartipay.transaction.bean.MoneyWaysCredentials;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.DBUtil;
import com.google.gson.Gson;
import com.itextpdf.text.log.SysoLogger;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import io.jsonwebtoken.Claims;


public class EzyPayIntegration {
	private static final Logger logger = Logger.getLogger(EzyPayIntegration.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	String url = "";
	
public String rechargeUsingEazyPay(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	String status="Fail";

	 logger.info("Start excution *********************************************************** name" + name);
	 logger.info("Start excution *********************************************************** servicetype" + servicetype);
	 logger.info("Start excution *********************************************************** rechargetype" + rechargetype);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargeTxnId);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String rest=null;
	 String storeId="BAPPATELECOM002165EZPY";
	 try {
		 	
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
			Query query=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			query.setString("name", name);
			query.setString("servicetype", servicetype);
			query.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = query.list();
			 logger.info("Start excution ***********************************************************listr" + list.size());
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
			rechargeOperatorMasterBean=list.get(0);
			String reqPara=null;
			 logger.info("Start excution ***********************************************************listr" + servicetype);
			if(servicetype.equalsIgnoreCase("LANDLINE"))
				reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&Circle="+circle.replace(" ","%20")+"&AcountNo="+accNumber+"&StdCode="+stdCode;	
			else
				reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&StoreID="+storeId;	
		    
			logger.info("Start excution **************** reqPara" + reqPara);	
		    logger.info("Start excution **************** URL" + rechargeOperatorMasterBean.getUrl());
		    logger.info("Start excution **************** code" + rechargeOperatorMasterBean.getCode());
		
		    String finalUrl=	rechargeOperatorMasterBean.getUrl()+reqPara;
			logger.info("Start excution **************** finalUrl" + finalUrl);
		    
		    URL url = new URL(finalUrl);
	        HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
	        urlconnection.setRequestMethod("POST");
	        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
	        urlconnection.setDoOutput(true);
	        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
	       // outa.write(postData);
	        outa.close();
	        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
	        String decodedString;
	        while ((decodedString = in.readLine()) != null) {
	            System.out.println("decodedString~~~~~~~~```"+decodedString);
	            rest=decodedString;
	      }
	        in.close();
	        status=rest;
		   System.out.println(status); 
					
			}
	 } catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;
}
	

/*public String rechargeUsingMoneyWays(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	String status="Fail";

	 logger.info("Start excution *********************************************************** name" + name);
	 logger.info("Start excution *********************************************************** servicetype" + servicetype);
	 logger.info("Start excution *********************************************************** rechargetype" + rechargetype);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargeTxnId);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String rest=null;
	 String storeId="BAPPATELECOM002165EZPY";
	 String token="xcNgWpMX0vf5mLYylmCw0y5yzXE8BPFO";
	 String userId="AU97136";
	 String password="6J65OI";
	 String agentId="121234567890";
	// String url="https://www.moneyways.co.in/mwa/apiadmin_TransactionApi.jsp?token=xcNgWpMX0vf5mLYylmCw0y5yzXE8BPFO&userid=AU97136&password=6J65OI&spkey=VFP&agentid=123456789012&account=8930345253&amount=1&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";
	 try {
		 	
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
			Query query=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			query.setString("name", name);
			query.setString("servicetype", servicetype);
			query.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = query.list();
			 logger.info("Start excution ***********************************************************listr" + list.size());
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
			rechargeOperatorMasterBean=list.get(0);
			String reqPara=null;
			 logger.info("Start excution ***********************************************************listr" + servicetype);
			if(servicetype.equalsIgnoreCase("LANDLINE"))
				reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&Circle="+circle.replace(" ","%20")+"&AcountNo="+accNumber+"&StdCode="+stdCode;	
			else
				//reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&StoreID="+storeId;	
				reqPara="?token="+token+"&userid="+userId+"&password="+password+"&spkey="+rechargeOperatorMasterBean.getCode()+"&agentid="+agentId+"&account="+rechargenumber+"&amount="+amount+"&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";
		    
			logger.info("Start excution **************** reqPara" + reqPara);	
		    logger.info("Start excution **************** URL" + rechargeOperatorMasterBean.getUrl());
		    logger.info("Start excution **************** code" + rechargeOperatorMasterBean.getCode());
		
		    String finalUrl=	rechargeOperatorMasterBean.getUrl()+reqPara;
			logger.info("Start excution **************** finalUrl" + finalUrl);
		    
		    URL url = new URL(finalUrl);
	        HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
	        urlconnection.setRequestMethod("POST");
	        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
	        urlconnection.setDoOutput(true);
	        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
	       // outa.write(postData);
	        outa.close();
	        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
	        String decodedString;
	        while ((decodedString = in.readLine()) != null) {
	            System.out.println("decodedString~~~~~~~~```"+decodedString);
	            rest=decodedString;
	      }
	        in.close();
	        status=rest;
		   System.out.println(status); 
					
			}
	 } catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;
}
*/



public String rechargeBillAPI(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle) {

	String status="Fail";
	amount = amount.split("\\.")[0];
	 logger.info("Start excution *********************************************************** name" + name);
	 logger.info("Start excution *********************************************************** servicetype" + servicetype);
	 logger.info("Start excution *********************************************************** rechargetype" + rechargetype);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargeTxnId);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 try {
		 	
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
			Query query=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			query.setString("name", name);
			query.setString("servicetype", servicetype);
			query.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = query.list();
			 logger.info("Start excution ***********************************************************listr" + list.size());
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
			rechargeOperatorMasterBean=list.get(0);
			String reqPara=null;
			 logger.info("Start excution ***********************************************************listr" + servicetype);
			 
          /*
			 if(servicetype.equalsIgnoreCase("LANDLINE"))
			reqPara="?username=500009&pwd=LKBhartipay@123&circlecode="+circle+"&operatorcode="+rechargeOperatorMasterBean.getCode()+"&number="+rechargenumber+"&amount="+amount+"&orderid="+rechargeTxnId+"&format=json";	
			 
			 else if(servicetype.equalsIgnoreCase("PREPAID"))
			 reqPara="?username=500009&pwd=LKBhartipay@123&circlecode="+circle+"&operatorcode="+rechargeOperatorMasterBean.getCode()+"&number="+rechargenumber+"&amount="+amount+"&orderid="+rechargeTxnId+"&format=json";	
	
			 else if(servicetype.equalsIgnoreCase("DTH"))
			 reqPara="?username=500009&pwd=LKBhartipay@123&circlecode="+circle+"&operatorcode="+rechargeOperatorMasterBean.getCode()+"&number="+rechargenumber+"&amount="+amount+"&orderid="+rechargeTxnId+"&format=json";	
			 else {
					 reqPara="?username=500009&pwd=LKBhartipay@123&circlecode="+circle+"&operatorcode="+rechargeOperatorMasterBean.getCode()+"&number="+rechargenumber+"&amount="+amount+"&orderid="+rechargeTxnId+"&format=json";	
				} 
          */
			/*
			if(servicetype.equalsIgnoreCase("LANDLINE"))
				reqPara="?apiToken=b0ec03abc26a423f8fad0ce8b5916592&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1="+accNumber+"&field2="; 
			 
			else if(servicetype.equalsIgnoreCase("PREPAID"))
				reqPara="?apiToken=b0ec03abc26a423f8fad0ce8b5916592&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1="+accNumber+"&field2=";
			 
			else if(servicetype.equalsIgnoreCase("DATACARD"))
				reqPara="?apiToken=b0ec03abc26a423f8fad0ce8b5916592&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1="+accNumber+"&field2=";
			 
			else if(servicetype.equalsIgnoreCase("DTH"))
				reqPara="?apiToken=b0ec03abc26a423f8fad0ce8b5916592&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1="+accNumber+"&field2=";
			*/
			// b5c63372351e4649ab5314751c82dbc7
			if(servicetype.equalsIgnoreCase("LANDLINE"))
				reqPara="?apiToken=b5c63372351e4649ab5314751c82dbc7&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1="+accNumber+"&field2="; 
			else {
				 reqPara="?apiToken=b5c63372351e4649ab5314751c82dbc7&mn="+rechargenumber+"&op="+rechargeOperatorMasterBean.getCode()+"&amt="+amount+"&reqid="+rechargeTxnId+"&field1=&field2=";
				} 
			 
	 	    logger.info("Start excution **************** reqPara" + reqPara);	
		    logger.info("Start excution **************** URL" + rechargeOperatorMasterBean.getUrl());
		    logger.info("Start excution **************** code" + rechargeOperatorMasterBean.getCode());
		
		    String finalUrl=	rechargeOperatorMasterBean.getUrl()+reqPara;
			logger.info("Start excution **************** finalUrl" + finalUrl);
			
			Result result=new Result();
			String baseUrl = rechargeOperatorMasterBean.getUrl().trim();
			String finalURL=baseUrl+reqPara;
			DefaultHttpClient httpClient = new DefaultHttpClient();
		    try
		    {
		        HttpGet getRequest = new HttpGet(finalURL);
		        getRequest.addHeader("accept", "application/xml");
		        HttpResponse response = httpClient.execute(getRequest);
		        int statusCode = response.getStatusLine().getStatusCode();
		        if (statusCode != 200) 
		        {
		            throw new RuntimeException("Failed with HTTP error code : " + statusCode);
		        }
		        HttpEntity httpEntity = response.getEntity();
		        String apiOutput = EntityUtils.toString(httpEntity);
		        
		        System.out.println(apiOutput);
		        org.json.JSONObject jsonObj = XML.toJSONObject(apiOutput); 
		        org.json.JSONObject json = jsonObj.getJSONObject("Result");
		        System.out.println("MOB "+json.get("mn"));
		       
		        result.setAmt(json.get("amt").toString());
		        result.setBalance(json.get("balance").toString());
		        result.setMn(json.get("mn").toString());
		        result.setStatus(json.get("status").toString());
		        result.setEc(json.get("ec").toString());
		        result.setReqid(json.get("reqid").toString());
		        result.setApirefid(json.get("apirefid").toString());
		        result.setField1(json.get("field1").toString());
		        result.setRemark(json.get("remark").toString());
		        
		        System.out.println(result.toString());
		       
		        Gson gson = new Gson();
		  	    status=gson.toJson(result);
		        
		        return status;
		    
		    }finally
		    {
		        httpClient.getConnectionManager().shutdown();
		    }
			
			
			
			/*
		    URL url = new URL(finalUrl);
	        HttpsURLConnection urlconnection = (HttpsURLConnection) url.openConnection();
	        //urlconnection.setRequestMethod("POST");
	        //urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
	        
	        urlconnection.setRequestMethod("GET"); 
	        urlconnection.setRequestProperty("Content-Type", "text/plain"); 
	        //urlconnection.setRequestProperty("charset", "utf-8");
	        urlconnection.setDoOutput(true);
	        
	        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
	       // outa.write(postData);
	        outa.close();
	        
	    	String responseStatus = urlconnection.getResponseMessage();
			System.out.println("responseStatus :" + responseStatus);
			int responseCode = urlconnection.getResponseCode();
			System.out.println("responseCode :" + responseCode);
			
	        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
	   
	        StringBuilder sb = new StringBuilder();
	        
	    	if (in != null) {
				int cp;
				while ((cp = in.read()) != -1) {
					sb.append((char) cp);
				}
				in.close();
			}
	    	in.close();
	    	
	    	System.out.println("sb.toString()  "+sb.toString());
	    	
	    	org.json.JSONObject xmlJSONObj = new org.json.JSONObject(sb.toString());
//	    	{"txid":"5804","status":"Success","opid":"RJ0317161622000269New","number":"980000000","amount":"10","orderid":"485668"}
	    	status = sb.toString();
			*/
			
			
			
			}
	 } catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;

}



public String rechargeUsingMoneyWays(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	String status="Fail";
	amount = amount.split("\\.")[0];
	 logger.info("Start excution *********************************************************** name" + name);
	 logger.info("Start excution *********************************************************** servicetype" + servicetype);
	 logger.info("Start excution *********************************************************** rechargetype" + rechargetype);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution *********************************************************** rechargenumber" + rechargeTxnId);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String rest=null;
	 String storeId="BAPPATELECOM002165EZPY";
	 /*String token="xcNgWpMX0vf5mLYylmCw0y5yzXE8BPFO";
	 String userId="AU97136";
	 String password="6J65OI";*/
	 
	 MoneyWaysCredentials  moneyWaysCredentials=(MoneyWaysCredentials)session.get(MoneyWaysCredentials.class, "1");
	 
	 String token=moneyWaysCredentials.getToken();
	 String userId=moneyWaysCredentials.getUserid();
	 String password=moneyWaysCredentials.getPassword();
	 
	// String url="https://www.moneyways.co.in/mwa/apiadmin_TransactionApi.jsp?token=xcNgWpMX0vf5mLYylmCw0y5yzXE8BPFO&userid=AU97136&password=6J65OI&spkey=VFP&agentid=123456789012&account=8930345253&amount=1&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";
	 try {
		 	
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
			Query query=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			query.setString("name", name);
			query.setString("servicetype", servicetype);
			query.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = query.list();
			 logger.info("Start excution ***********************************************************listr" + list.size());
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
			rechargeOperatorMasterBean=list.get(0);
			String reqPara=null;
			 logger.info("Start excution ***********************************************************listr" + servicetype);
			 
			 
			 
			/*if(servicetype.equalsIgnoreCase("LANDLINE"))
				reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&Circle="+circle.replace(" ","%20")+"&AcountNo="+accNumber+"&StdCode="+stdCode;	
			else
				//reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&StoreID="+storeId;	
				reqPara="?token="+token+"&userid="+userId+"&password="+password+"&spkey="+rechargeOperatorMasterBean.getCode()+"&agentid="+rechargeTxnId+"&account="+rechargenumber+"&amount="+amount+"&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";
		    */
			 
			 if(servicetype.equalsIgnoreCase("LANDLINE"))
					reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&Circle="+circle.replace(" ","%20")+"&AcountNo="+accNumber+"&StdCode="+stdCode;	
				else if(servicetype.equalsIgnoreCase("PREPAID"))
					//reqPara="?AuthorisationCode=e13ed0c588b14ab2a2&product="+rechargeOperatorMasterBean.getCode()+"&MobileNumber="+rechargenumber+"&Amount="+amount+"&RequestId="+rechargeTxnId+"&StoreID="+storeId;	
					reqPara="?token="+token+"&userid="+userId+"&password="+password+"&spkey="+rechargeOperatorMasterBean.getCode()+"&agentid="+rechargeTxnId+"&account="+rechargenumber+"&amount="+amount+"&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";
				else if(servicetype.equalsIgnoreCase("DTH"))
					reqPara="?token="+token+"&userid="+userId+"&password="+password+"&spkey="+rechargeOperatorMasterBean.getCode()+"&agentid="+rechargeTxnId+"&account="+rechargenumber+"&amount="+amount+"&optional1=value&optional2=value&optional3=value&optional4=value&serv=dth&format=value";
				else {
					
					reqPara="?token="+token+"&userid="+userId+"&password="+password+"&spkey="+rechargeOperatorMasterBean.getCode()+"&agentid="+rechargeTxnId+"&account="+rechargenumber+"&amount="+amount+"&optional1=value&optional2=value&optional3=value&optional4=value&serv=mobile&format=value";

				} 
			logger.info("Start excution **************** reqPara" + reqPara);	
		    logger.info("Start excution **************** URL" + rechargeOperatorMasterBean.getUrl());
		    logger.info("Start excution **************** code" + rechargeOperatorMasterBean.getCode());
		
		    String finalUrl=	rechargeOperatorMasterBean.getUrl()+reqPara;
			logger.info("Start excution **************** finalUrl" + finalUrl);
		    
		    URL url = new URL(finalUrl);
	        HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
	        urlconnection.setRequestMethod("POST");
	        urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
	        urlconnection.setDoOutput(true);
	        OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
	       // outa.write(postData);
	        outa.close();
	        BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
	       /* String decodedString;
	        while ((decodedString = in.readLine()) != null) {
	            System.out.println("decodedString~~~~~~~~```"+decodedString);
	            rest=decodedString;
	      }
	        in.close();
	        status=rest;*/
	        
	        StringBuilder sb = new StringBuilder();
	        
	    	if (in != null) {
				int cp;
				while ((cp = in.read()) != -1) {
					sb.append((char) cp);
				}
				in.close();
			}
	    	in.close();
	    	String xmlString=sb.toString();
	    	
	    	org.json.JSONObject xmlJSONObj = XML.toJSONObject(xmlString);
            String jsonPrettyPrintString = xmlJSONObj.toString(4);
            System.out.println(jsonPrettyPrintString);
            
            org.json.JSONObject a1 =  (org.json.JSONObject) xmlJSONObj.get("API_Status");
            
            org.json.JSONObject a2 =  (org.json.JSONObject) a1.get("Recharge_Status");
            
            System.out.println("----------------");
            
            System.out.println(a2.get("Status"));
          //  System.out.println(a2.get("Agent id"));
            System.out.println(a2.get("MobNo"));
            System.out.println(a2.get("opr_id"));
            System.out.println(a2.get("recharge_amount"));
            System.out.println(a2.get("Refid"));
            
            String reqstatus=(String) a2.get("Status");
            String mobNo=String.valueOf(a2.get("MobNo"));
            String oprId=String.valueOf(a2.get("opr_id"));
            String rechargeAmount= String.valueOf(a2.get("recharge_amount"));
            String refId=String.valueOf(a2.get("Refid"));
	    	rest=reqstatus+"~"+mobNo+"~"+oprId+"~"+rechargeAmount+"~"+refId;
	    	status=rest;
	    	
		   System.out.println(status); 
					
			}
	 } catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;
}

public ExtBillPayResponse rechargeUsingBillAvenue(String agentId, String billerid,String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle,String ip,String userAgent) {
	BillerBbpsServiceImpl prepaidRecharge = new BillerBbpsServiceImpl();
	BillPaymentRequest prepaidRequest =  new BillPaymentRequest();
	AgentDeviceInfo agentDeviceInfo= new AgentDeviceInfo();
	agentDeviceInfo.setIp(ip);
	agentDeviceInfo.setMac("ed:37:8d:10:5a:f8");
	agentDeviceInfo.setInitChannel("AGT");
	prepaidRequest.setAgentDeviceInfo(agentDeviceInfo);
	prepaidRequest.setAgentId(agentId);//"CC01CC01513515340681"
	AmountInfo amountInfo = new AmountInfo();
	int intAmount = (int)(Double.parseDouble(amount)*100);
	amountInfo.setAmount(""+intAmount);
	amountInfo.setCurrency("356");
	amountInfo.setCustConvFee("0");
	amountInfo.setAmountTags("");
	prepaidRequest.setAmountInfo(amountInfo);
	prepaidRequest.setBillerAdhoc("true");
	prepaidRequest.setBillerId(billerid);
	CustomerInfo customerInfo = new CustomerInfo();
	customerInfo.setCustomerMobile(rechargenumber);
	prepaidRequest.setCustomerInfo(customerInfo);
	InputParams inputParams = new InputParams();
	List<Input> input = new ArrayList<Input>();
	Input input1 = new Input();
	input1.setParamName("Mobile Number");
	input1.setParamValue(rechargenumber);
	Input input2 = new Input();
	input2.setParamName("Location");
	input2.setParamValue(circle);
	input.add(input1);
	input.add(input2);
	inputParams.setInput(input);
	prepaidRequest.setInputParams(inputParams);
	PaymentInfo paymentInfo = new PaymentInfo();
	Info info=new Info();
	info.setInfoName("Remarks");
	info.setInfoValue("Recieved");
	paymentInfo.setInfo(info);
	prepaidRequest.setPaymentInfo(paymentInfo);
	PaymentMethod paymentMethod = new PaymentMethod();
	
	paymentMethod.setPaymentMode("Cash");
	paymentMethod.setQuickPay("Y");
	paymentMethod.setSplitPay("N");
	prepaidRequest.setPaymentMethod(paymentMethod);
	
	return prepaidRecharge.prepaidBillPayment(prepaidRequest);
}

public String rechargeUsingPayWorldOld(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle,String aggreagatorId){
	String status="Fail";
	//rechargeUsingEazyPay(rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "", rechargeTxnBean.getRechargeNumber(), rechargeId, Double.toString(rechargeTxnBean.getRechargeAmt()),rechargeTxnBean.getStdCode(),rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getRechargeCircle());
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** name" + name);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** servicetype" + servicetype);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargetype" + rechargetype);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargenumber" + rechargeTxnId);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** amount" + amount);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** stdCode" + stdCode);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** accNumber" + accNumber);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** circle" + circle);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 //String rest=null;
	 // storeId="BAPPATELECOM002165EZPY";
	/*String agentid="156";
	 String retailerid="156";
	 String loginstatus="LIVE";*/
	 
	 try {
		 	
	 SQLQuery queryRechargeCredentials = session.createSQLQuery("SELECT agentid,retailerid,loginstatus FROM rechargecredentials where apiname= :apiname and aggreagatorid=:aggId");
		 queryRechargeCredentials.setString("apiname", "payworld");
		 queryRechargeCredentials.setString("aggId",aggreagatorId);
		 List rechargeCredentialsList2 = queryRechargeCredentials.list();
		 Object [] detls=(Object[]) rechargeCredentialsList2.get(0);
		 
		 String agentid=(String)detls[0];
		 String retailerid=(String)detls[1];
		 String loginstatus=(String)detls[2];
		 
		 logger.info("==========agentid==========================================+++++  "+ agentid);
		 logger.info("==========retailerid==========================================+++++  "+ retailerid);
		 logger.info("==========loginstatus==========================================+++++  "+ loginstatus);
		 
		 if (servicetype.equalsIgnoreCase("DTH"))
			 circle="Delhi";
		 
		 SQLQuery query = session.createSQLQuery("SELECT payworldcode FROM rechargecirclecode where circlename= :cname");
		 query.setParameter("cname", circle);
		 List list2 = query.list();
		 int circleCode = (Integer) list2.get(0);
		 
		 
		 logger.info("==========rechargeUsingPayWorld==========================================+++++  "+ list2.get(0));
		 String newParam="";
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
		Query rechargeOperatorquery=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
		rechargeOperatorquery.setString("name", name);
		rechargeOperatorquery.setString("servicetype", servicetype);
		rechargeOperatorquery.setString("rechargetype", rechargetype);
		List <RechargeOperatorMasterBean> list = rechargeOperatorquery.list();
		RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
		if(list==null || list.size()>0){
			//int amountFinal=Double.valueOf(amount).intValue();
			
			rechargeOperatorMasterBean=list.get(0); 
		 if(servicetype.equalsIgnoreCase("DTH")){
			 logger.info("##########################################################################");
				newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode=*"
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&denomination=0&appver=3.40";
				logger.info("##########################################################################"+url);
			}
		 else if(name.equalsIgnoreCase("BSNL")){
			 newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&product=Topup&denomination=0&appver=3.40";
				}
		 else if(name.equalsIgnoreCase("Tata DOCOMO")){
			 newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&product=GFLX&denomination=0&appver=3.40";
				}
			else{
				newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&denomination=0&appver=3.40";
			}
		 logger.info("******rechargeUsingPayWorld*********************************************newParam***************************"+newParam);
		 url = rechargeOperatorMasterBean.getUrl().concat(newParam);
		 logger.info("***************************************************Payword URL Callingg url***************************"+url);
		 String responseMsg = executeURL(url.trim());
		 logger.info("*****rechargeUsingPayWorld***************************************************responseMsg for******"+rechargeTxnId+" --- "+ responseMsg);
		
		 status=responseMsg;
		
		} 
		 
		   System.out.println(status); 
					
			
	 } catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in rechargeUsingPayWorld==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;
}



public PayworldResponseData rechargeUsingPayWorld(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle,String aggreagatorId){
	String status="Fail";
	PayworldResponseData data=new PayworldResponseData();
	String key="D9dNyt63p6";
	String version="1.0.0";

	data.setResponseCode("1001");
	data.setResponseMessage("Failed");
	//rechargeUsingEazyPay(rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "", rechargeTxnBean.getRechargeNumber(), rechargeId, Double.toString(rechargeTxnBean.getRechargeAmt()),rechargeTxnBean.getStdCode(),rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getRechargeCircle());
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** name" + name);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** servicetype" + servicetype);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargetype" + rechargetype);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** rechargenumber" + rechargeTxnId);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** amount" + amount);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** stdCode" + stdCode);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** accNumber" + accNumber);
	 logger.info("Start excution ********************rechargeUsingPayWorld*************************************** circle" + circle);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();

	 
	 try {
		 	
		 SQLQuery queryRechargeCredentials = session.createSQLQuery("SELECT agentid,retailerid,loginstatus FROM rechargecredentials where apiname= :apiname and aggreagatorid=:aggId");
		 queryRechargeCredentials.setString("apiname", "payworld");
		 queryRechargeCredentials.setString("aggId",aggreagatorId);
		 List rechargeCredentialsList2 = queryRechargeCredentials.list();
		 Object [] detls=(Object[]) rechargeCredentialsList2.get(0);
		 
		 String agentid=(String)detls[0];
		 String retailerid=(String)detls[1];
		 String loginstatus=(String)detls[2];
		 
		 logger.info("==========agentid==========================================+++++  "+ agentid);
		 logger.info("==========retailerid==========================================+++++  "+ retailerid);
		 logger.info("==========loginstatus==========================================+++++  "+ loginstatus);
		 
		 if(servicetype.equalsIgnoreCase("DTH"))
			 circle="Delhi";
		 
		 SQLQuery query = session.createSQLQuery("SELECT payworldcode FROM rechargecirclecode where circlename= :cname");
		 query.setParameter("cname", circle);
		 List list2 = query.list();
		 int circleCode = (Integer) list2.get(0);
		 
		 
		 logger.info("==========rechargeUsingPayWorld==========================================+++++  "+ list2.get(0));
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 
		Query rechargeOperatorquery=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
		rechargeOperatorquery.setString("name", name);
		rechargeOperatorquery.setString("servicetype", servicetype);
		rechargeOperatorquery.setString("rechargetype", rechargetype);
		List <RechargeOperatorMasterBean> list = rechargeOperatorquery.list();
		RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
		if(list==null || list.size()>0){
		
		rechargeOperatorMasterBean=list.get(0); 
	/**********************************************   start impl for new payworld ******************************************/		
		PayworldRequest payworldRequest=new PayworldRequest();
		Parameter parameter=new Parameter();
		Product product=new Product();
		DirectParameters directParameters=new DirectParameters();
		directParameters.setAccountCode("");
		directParameters.setPartnerCode("");
		directParameters.setAccountCode("");
		directParameters.setPlanCircleCode("");
		directParameters.setPlanId("");
		directParameters.setPlanName("");
		directParameters.setSubscriptionType("");
		directParameters.setPartyCode("");
			
		
		parameter.setChannel(loginstatus);
		parameter.setMerchantUserId(retailerid);
		parameter.setMerchantUserIds(retailerid);
		parameter.setConvenienceCharge(0);
		parameter.setProduct(product);
		parameter.setDevice("WINDOWS");
		parameter.setCustomerMobile(rechargenumber);
		parameter.setCustomerEmailid("abc@appnittech.com");
		parameter.setReferenceNumber(rechargeTxnId);
		parameter.setDirect("NO");
		parameter.setDirectParameters(directParameters);
		
		com.bhartipay.payworld.masterdata.Product p=new com.bhartipay.payworld.masterdata.Product();
		if(rechargeOperatorMasterBean.getCode().equalsIgnoreCase("7")||rechargeOperatorMasterBean.getCode().equalsIgnoreCase("10")){
		if(rechargetype!=null&&rechargetype.equalsIgnoreCase("Special")){
				p.setRtopup("Special Recharge");
			}
			else{
				p.setTopup("Talktime Topup");
			}
			
		}
		else if(rechargeOperatorMasterBean.getCode().equalsIgnoreCase("15")||rechargeOperatorMasterBean.getCode().equalsIgnoreCase("16")){
			if(rechargetype!=null&&rechargetype.equalsIgnoreCase("Special")){
				p.setGspl("Special Recharge");
			}
			else{
				p.setGflx("Talktime Topup");
			}
			
		}
		else if(rechargeOperatorMasterBean.getCode().equalsIgnoreCase("17")){
			if(rechargetype!=null&&rechargetype.equalsIgnoreCase("Special")){
				p.setSTV("Special Recharge");
			}
			else{
				p.setTOPUP("Talktime Topup");
			}
		}
		parameter.setProduct(p);
		
		parameter.setOperatorCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
		
		parameter.setDeviceNumber(rechargenumber);
		parameter.setAmount((int)Double.parseDouble(amount));
		
		if(servicetype!=null&&servicetype.equalsIgnoreCase("DTH")){
			parameter.setCircleCode("*");	
			parameter.setServiceCode(Integer.parseInt("10"));
		}
		else if(servicetype!=null&&servicetype.equalsIgnoreCase("PREPAID")){
			parameter.setCircleCode(""+circleCode);
			parameter.setServiceCode(Integer.parseInt("8"));
		}
		else if(servicetype!=null&&servicetype.equalsIgnoreCase("POSTPAID")){
			parameter.setCircleCode(""+circleCode);
			parameter.setServiceCode(Integer.parseInt("3"));
		}
		else if(servicetype!=null&&servicetype.equalsIgnoreCase("Datacard")){
			parameter.setCircleCode(""+circleCode);
			parameter.setServiceCode(Integer.parseInt("9"));
		}
		
		
		
/*		 if(servicetype.equalsIgnoreCase("DTH")){
			 logger.info("##########################################################################");
				newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode=*"
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&denomination=0&appver=3.40";
				parameter.setOperatorCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				parameter.setCircleCode("*");
				parameter.setDeviceNumber(rechargenumber);
				parameter.setAmount((int)Double.parseDouble(amount));
				parameter.setServiceCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				logger.info("##########################################################################"+url);
			}
		 else if(name.equalsIgnoreCase("BSNL")){
			 newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&product=Topup&denomination=0&appver=3.40";
			 	parameter.setOperatorCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				parameter.setCircleCode(""+circleCode);
				parameter.setDeviceNumber(rechargenumber);
				parameter.setAmount((int)Double.parseDouble(amount));
				parameter.setServiceCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				}
		 else if(name.equalsIgnoreCase("Tata DOCOMO")){
			 newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&product=GFLX&denomination=0&appver=3.40";
			 	parameter.setOperatorCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				parameter.setCircleCode(""+circleCode);
				parameter.setDeviceNumber(rechargenumber);
				parameter.setAmount((int)Double.parseDouble(amount));
				parameter.setServiceCode(Integer.parseInt(rechargeOperatorMasterBean.getCode()));
				}
			else{
				newParam = "?loginstatus="+loginstatus+"&agentid="+agentid+"&retailerid="+retailerid+"&transid="
						+ rechargeTxnId
						+ "&operatorcode="
						+ rechargeOperatorMasterBean.getCode()
						+ "&circode="
						+ circleCode
						+ "&mobileno="
						+ rechargenumber
						+ "&recharge="
						+ Double.valueOf(amount).intValue() +"&denomination=0&appver=3.40";
				parameter.setOperatorCode(Integer.parseInt("5"));
				parameter.setCircleCode("5");
				parameter.setDeviceNumber(rechargenumber);
				parameter.setAmount((int)Double.parseDouble(amount));
				parameter.setServiceCode(Integer.parseInt("8"));
			}*/
		 
		 	BPJWTSignUtil oxyJwtUtil=new BPJWTSignUtil();
			Gson gson=new Gson();
		
			/**********************************************   start impl for getCircleandOperator ******************************************/		
			
			 /*PayworldRequest circleandOperator=new PayworldRequest();
			 circleandOperator.setActionName("getCircleandOperator");
			 List<Parameter> parameters1=new ArrayList<Parameter>();
			 Parameter parameter2=new  Parameter();
			 parameter2.setMobileNo(parameter.getDeviceNumber());
			 parameters1.add(parameter2);
			 circleandOperator.setParameters(parameters1);
			String jsonString1=gson.toJson(circleandOperator);
			HashMap<String,Object> hashMap1=gson.fromJson(jsonString1,ABC.class);
			String token1= oxyJwtUtil.generateToken(hashMap1,key);
			PayworldJsonRequest jsonRequest1=new PayworldJsonRequest();	
			jsonRequest1.setRequestToken(token1);
			String str1=postJwtRequest(gson.toJson(jsonRequest1),version,agentid,rechargeOperatorMasterBean.getUrl());
			HashMap<String,Object> respMap1=gson.fromJson(str1,ABC.class);
			oxyJwtUtil.parseToken(respMap1.get("data").toString(),key);*/
			
			
			
			
			
			
			/**********************************************   start impl for getMasterData ******************************************/		
	
			
		 /*PayworldRequest masterData=new PayworldRequest();
		 masterData.setActionName("getMasterData");
		 List<Parameter> parameters2=new ArrayList<Parameter>();
		 Parameter parameter2=new  Parameter();
		 parameter2=new  Parameter();
		 parameters2.add(parameter2);
		 masterData.setParameters(parameters2);
		String jsonString2=gson.toJson(masterData);
		HashMap<String,Object> hashMap1=gson.fromJson(jsonString2,ABC.class);
		String token2= oxyJwtUtil.generateToken(hashMap1,key);
		PayworldJsonRequest jsonRequest2=new PayworldJsonRequest();	
		jsonRequest2.setRequestToken(token2);
		String str2=postJwtRequest(gson.toJson(jsonRequest2),version,agentid,rechargeOperatorMasterBean.getUrl());
		HashMap<String,Object> respMap2=gson.fromJson(str2,ABC.class);
		Claims mstData=oxyJwtUtil.parseToken(respMap2.get("data").toString(),key);
		mstData.get("services");
		
		Data m=gson.fromJson(gson.toJson(mstData),Data.class);
		
		List<Service> listService= m.getServices();
		 for (Service service : listService) {
			List<Operator> operator=service.getOperators();
			for (Operator operator2 : operator) {
				
			List<com.appnit.bhartipay.payworld.masterdata.Product>	ms=operator2.getProducts();
				if(ms!=null&&ms.size()>0){	
					for (com.appnit.bhartipay.payworld.masterdata.Product product2 : ms) {
						System.out.println(operator2.getOperatorName()+","+operator2.getOperatorCode()+"===="+gson.toJson(product2));
					}
				
				}
			}
		}
		 
		List<Circle> listCircle= m.getServices().get(0).getOperators().get(0).getCircle();
	
				for (Circle circle2 : listCircle) {
					System.out.println(circle2.getCircleName()+","+circle2.getCircleCode());
				}
				
		List<Operator> listOperator= m.getServices().get(0).getOperators();
		for (Operator operator2 : listOperator) {
			System.out.println(operator2.getOperatorName()+","+operator2.getOperatorCode());
		}
		 
		
		List<Service> listService1= m.getServices();
		for (Service ser : listService1) {
			System.out.println(ser.getServiceName()+","+ser.getServiceCode());
		}*/
		
		
		 
		/******************************************** doTransaction *********************************************/
		payworldRequest.setActionName("doTransaction");
		List<Parameter> parameters=new ArrayList<Parameter>();
		parameters.add(parameter);
		payworldRequest.setParameters(parameters);
				 
	
		String jsonString=gson.toJson(payworldRequest);
		HashMap<String,Object> hashMap=gson.fromJson(jsonString,ABC.class);
		String token= oxyJwtUtil.generateToken(hashMap,key);
		PayworldJsonRequest jsonRequest=new PayworldJsonRequest();	
		jsonRequest.setRequestToken(token);
		String str=postJwtRequest(gson.toJson(jsonRequest),version,agentid,rechargeOperatorMasterBean.getUrl());
		
		
		HashMap<String,Object> respMap=gson.fromJson(str,ABC.class);
		Claims rsp=oxyJwtUtil.parseToken(respMap.get("data").toString(),key);
		
		if(respMap!=null&&respMap.size()>0){
		data.setResponseCode(respMap.get("responseCode").toString());
		data.setResponseMessage(respMap.get("responseMessage").toString());
		
		 logger.info("******rechargeUsingPayWorld*********************************************setResponseCode***************************"+data.getResponseCode());
		 logger.info("******rechargeUsingPayWorld*********************************************setResponseMessage***************************"+data.getResponseMessage());

		if(rsp!=null&&rsp.size()>0){
			logger.info("******rechargeUsingPayWorld*********************************************response***************************"+rsp.toString());
			data.setStatus(rsp.get("status").toString());
			data.setReferenceNumber(rsp.get("referenceNumber").toString());
			data.setMerchantTransno(rsp.get("merchantTransno").toString());
			data.setTransDateTime(rsp.get("transDateTime").toString());
			data.setMessage(rsp.get("message").toString());
			data.setDeviceNumber(rsp.get("deviceNumber").toString());
			data.setOperatorName(rsp.get("operatorName").toString());
			
		}
		
	}else{
		//(Double.parseDouble(payworldRes.getResponseCode())==0&&payworldRes.getStatus()!=null&&payworldRes.getStatus().toUpperCase().indexOf("INITIATED")>-1){
		data.setResponseCode("0");
		data.setStatus("INITIATED");
	}
		 logger.info("******rechargeUsingPayWorld end ************************************************************************");
		} 
	 } catch (Exception e) {
		 status = "1001";
		 data.setResponseCode("1001");
		data.setResponseCode("0");
		data.setStatus("INITIATED");
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in rechargeUsingPayWorld==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	data;
}



public String rechargeUsingJOLO(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	
	String status="Fail";
	
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** name" + name);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** servicetype" + servicetype);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** rechargetype" + rechargetype);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** rechargenumber" + rechargeTxnId);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** amount" + amount);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** stdCode" + stdCode);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** accNumber" + accNumber);
	 logger.info("Start excution ********************rechargeUsingJOLO*************************************** circle" + circle);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	try{
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
	 Query rechargeOperatorquery=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			rechargeOperatorquery.setString("name", name);
			rechargeOperatorquery.setString("servicetype", servicetype);
			rechargeOperatorquery.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = rechargeOperatorquery.list();
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
				rechargeOperatorMasterBean=list.get(0);
				if(servicetype.equalsIgnoreCase("LANDLINE")||servicetype.equalsIgnoreCase("POSTPAID")){
					String param = "?mode=0&userid=Powerpay&key=252155908572208&operator="
							+ rechargeOperatorMasterBean.getCode()
							+ "&service="
							+ rechargenumber
							+ "&amount="
							+ amount
							+ "&std="
							+ stdCode
							+ "&ca="
							+ accNumber
							+ "&orderid=" + rechargeTxnId;
					 logger.info("***************************************************newParam***************************"+param);
					 url = rechargeOperatorMasterBean.getUrl().concat(param);
					 logger.info("***************************************************JOLO URL Callingg url***************************"+url);
					 String responseMsg = executeURL(url.trim());
					 logger.info("====================================================+++++ url --- "+ responseMsg);
					 status=responseMsg;
				}else{
				  String param = "?mode=0&userid=Powerpay&key=252155908572208&operator="
						+ rechargeOperatorMasterBean.getCode()
						+ "&service="
						+ rechargenumber
						+ "&amount="
						+ amount + "&orderid=" + rechargeTxnId;	
				  logger.info("***************************************************newParam***************************"+param);
					 url = rechargeOperatorMasterBean.getUrl().concat(param);
					 logger.info("***************************************************JOLO URL Callingg url***************************"+url);
					 String responseMsg = executeURL(url.trim());
					 logger.info("====================================================+++++ url --- "+ responseMsg);
					 status=responseMsg;
				}	
		 
			}
		
	}catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	return 	status;
	
	
	
	
/*
 * 
 * 
				String param = "?mode=0&userid=yourusername&key=yourapikey&operator="
						+ operatorCode
						+ "&service="
						+ mobileNo
						+ "&amount="
						+ amount + "&orderid=" + trxId;
				url = url.concat(param);
				LOGGER.debug("**********************************************JOLO URL Callingg url="
						+ url + "   ***************************************");

				// calling the service provider service
				// URL.****************************************************
				String responseMsg = URLCall.executeURL(url);
				String respMsg[] = responseMsg.split(",");
				if (respMsg.length > 2) {
					// 0 Our order id,
					// 1 status,
					// 2 operator,
					// 3 service,
					// 4 amount,
					// 5 your website orderid,
					// 6 errorcode,
					// 7 operatorid,
					// 8 balance,
					// 9 margin,
					// 10 time
					respTrxId = respMsg[0];
					respStatus = respMsg[1];
					respErrorCode = respMsg[6];
					operator_ref = respMsg[7];
					respTime = respMsg[10];
					rechargeStatus = respStatus;
				} else {
					respStatus = respMsg[0];
					respErrorCode = respMsg[1];
				}
				// if (respStatus.equalsIgnoreCase("FAILED")) {
				// amount = 0;
				// }
				System.out.println(respTrxId);
				System.out.println(respStatus);
				System.out.println(respErrorCode);
				System.out.println(operator_ref);
				System.out.println(respTime);
			*/
	
	
	
	
}


public String rechargeUsingAUREL(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	String status="Fail";
	String api_key="b264af35413e3ee28ed68fa82f82954a";
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** name" + name);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** servicetype" + servicetype);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** rechargetype" + rechargetype);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** rechargenumber" + rechargenumber);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** rechargenumber" + rechargeTxnId);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** amount" + amount);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** stdCode" + stdCode);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** accNumber" + accNumber);
	 logger.info("Start excution ********************rechargeUsingAUREL*************************************** circle" + circle);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	try{
		 if(name.contains("Special")){
			 rechargetype="Special"; 
		 }else{
			 rechargetype="Top Up"; 
		 }
		 Query rechargeOperatorquery=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			rechargeOperatorquery.setString("name", name);
			rechargeOperatorquery.setString("servicetype", servicetype);
			rechargeOperatorquery.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = rechargeOperatorquery.list();
			RechargeOperatorMasterBean rechargeOperatorMasterBean=null;
			if(list==null || list.size()>0){
				rechargeOperatorMasterBean=list.get(0);
			if(servicetype.equalsIgnoreCase("DTH")){
			 
					JSONObject   requestPayload=new JSONObject ();    
			    	requestPayload.put("api_key",api_key);   
					requestPayload.put("operator_id",rechargeOperatorMasterBean.getCode());
					requestPayload.put("txn_amt",amount);
					requestPayload.put("dth_acc",rechargenumber);
					requestPayload.put("order_id",rechargeTxnId);
					String param = JSONValue.toJSONString(requestPayload);
				  		
				logger.info("***************************************************newParam***************************"+param);
				logger.info("***************************************************AUREL URL Callingg url***************************"+rechargeOperatorMasterBean.getUrl());
				status = executePostURL(rechargeOperatorMasterBean.getUrl().trim(),param);
				 logger.info("*******************************************************************AUREL URL Return ************************** "+ status);	  
					
				
			}
				
			}
	
	}catch (Exception e) {
		 status = "1001";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
			logger.debug("problem in rechargeUsingAUREL==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 logger.info("Start excution ***********************************************************status" +status);
	
	return 	status;
}




public static String executePostURL(String url,String param) {

	HttpsURLConnection con=null;
	String responseString="Fail";
    try{
    	
    	SSLContext ssl_ctx = SSLContext.getInstance("TLS");
        TrustManager[ ] trust_mgr = get_trust_mgr();
        ssl_ctx.init(null,                // key manager
                     trust_mgr,           // trust manager
                     new SecureRandom()); // random number generator
        HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
    	
    	URL obj = new URL(url);
		con = (HttpsURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		 
		/*con.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String host, SSLSession sess) {
                if (host.equals("localhost")) return true;
                else return false;
            }
        });*/

		
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(param);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		logger.info("\nSending 'POST' request to URL : " + url);
		logger.info("Post parameters : " + param);
		logger.info("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		logger.debug("getOfflineStores Response************************************"+response.toString());
		responseString =response.toString();
	  } catch (Exception e) {
		  e.printStackTrace();
      System.err.println(e);
    } finally {
    	con.disconnect();
    }
    return responseString;

  }	

private static TrustManager[ ] get_trust_mgr() {
    TrustManager[ ] certs = new TrustManager[ ] {
       new X509TrustManager() {
          public X509Certificate[ ] getAcceptedIssuers() { return null; }
          public void checkClientTrusted(X509Certificate[ ] certs, String t) { }
          public void checkServerTrusted(X509Certificate[ ] certs, String t) { }
        }
     };
     return certs;
 }


public static String executeURL(String url) {
	 logger.info("***************************************************executeURL***************************"+url);
    HttpClient client = new HttpClient();
    client.getParams().setParameter("http.useragent", "Test Client");

    BufferedReader br = null;
    String responseString="";
    PostMethod method = new PostMethod(url);
   try{
      int returnCode = client.executeMethod(method);

      if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
    	logger.info("******************************The Post method is not implemented by this URI*********************************");
        responseString= method.getResponseBodyAsString();
      } else {
        br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
        responseString=br.readLine();
        logger.info("returnCode*****************************************************************************"+returnCode);
        logger.info("responseString**************************************************************************"+responseString);
      }
    } catch (Exception e) {
    	logger.info("*******************************************************************"+e);
    } finally {
      method.releaseConnection();
      if(br != null) try { br.close(); } catch (Exception fe) {}
    }
    return responseString;

  }




public String postJwtRequest(String request, String version, String merchantid,String URL) {
		logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		
		
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource	webResource = client.resource(URL);
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("accessToken","").header("accessToken","").header("merchantId",merchantid).header("version", version).post(ClientResponse.class,request);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling deleteBeneficiary service response***********************"+output);
		//MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return output;
}



/*public static String executeURL(String url) {
	 logger.info("***************************************************executeURL***************************"+url);

	
    HttpClient client = new HttpClient();
    client.getParams().setParameter("http.useragent", "Test Client");

    BufferedReader br = null;
    String responseString="";
    PostMethod method = new PostMethod(url);
   // method.addParameter("p", "");
    try{
      int returnCode = client.executeMethod(method);

      if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
        System.err.println("The Post method is not implemented by this URI");
        // still consume the response body
       responseString= method.getResponseBodyAsString();
      } else {
        br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
        responseString=br.readLine();
//        while(((readLine = br.readLine()) != null)) {
//          System.err.println(readLine);
//      }
        System.out.println(returnCode);
        System.out.println(responseString);
      }
    } catch (Exception e) {
      System.err.println(e);
    } finally {
      method.releaseConnection();
      if(br != null) try { br.close(); } catch (Exception fe) {}
    }
    return responseString;

  }	*/


	
public static void main(String[] args) {
	EzyPayIntegration eazyPayIntegration=new EzyPayIntegration();
	//System.out.println(eazyPayIntegration.rechargeUsingJOLO("VODAFONE", "POSTPAID", "POSTPAID", "9935041287", "SANsdsw241", "10","","","Delhi"));
	//public String rechargeUsingPayWorld(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	//http://220.226.204.104/mainlinkpos/purchase/pw_etrans.php3?loginstatus=LIVE&agentid=156&retailerid=156&transid=RDEV001206&operatorcode=7&circode=5&mobileno=9910311643&recharge=10.0&denomination=0&appver=3.40
	System.out.println(eazyPayIntegration.rechargeUsingPayWorld("JIO", "PREPAID", "PREPAID", "7011605528", "RECHJIO241", "10","","","Delhi",""));
	
	//public String rechargeUsingJOLO(String name,String servicetype,String rechargetype,String rechargenumber,String rechargeTxnId,String amount,String stdCode,String accNumber,String circle){
	
	//System.out.println(eazyPayIntegration.executeURL("http://220.226.204.189/mainlinkpos/purchase/pw_etrans.php3?loginstatus=TEST&agentid=1&retailerid=1&transid=SANsdsw241&operatorcode=7&circode=5&mobileno=9910311643&recharge=10&denomination=0&appver=3.40"));
	
}

}

class ABC extends HashMap<String, Object>{
	
}