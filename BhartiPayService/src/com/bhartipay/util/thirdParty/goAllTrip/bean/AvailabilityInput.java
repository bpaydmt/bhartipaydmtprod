package com.bhartipay.util.thirdParty.goAllTrip.bean;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailabilityInput {

	@SerializedName("BookingType")
	@Expose
	private String bookingType;
	@SerializedName("JourneyDetails")
	@Expose
	private List<JourneyDetail> journeyDetails = null;
	@SerializedName("ClassType")
	@Expose
	private String classType;
	@SerializedName("AirlineCode")
	@Expose
	private String airlineCode;
	@SerializedName("AdultCount")
	@Expose
	private String adultCount;
	@SerializedName("ChildCount")
	@Expose
	private String childCount;
	@SerializedName("InfantCount")
	@Expose
	private String infantCount;
	@SerializedName("ResidentofIndia")
	@Expose
	private String residentofIndia;
	@SerializedName("Optional1")
	@Expose
	private String optional1;
	@SerializedName("Optional2")
	@Expose
	private String optional2;
	@SerializedName("Optional3")
	@Expose
	private String optional3;
	
	
	public String getBookingType() {
		return bookingType;
		}

		public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
		}

		public List<JourneyDetail> getJourneyDetails() {
		return journeyDetails;
		}

		public void setJourneyDetails(List<JourneyDetail> journeyDetails) {
		this.journeyDetails = journeyDetails;
		}

		public String getClassType() {
		return classType;
		}

		public void setClassType(String classType) {
		this.classType = classType;
		}

		public String getAirlineCode() {
		return airlineCode;
		}

		public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
		}

		public String getAdultCount() {
		return adultCount;
		}

		public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
		}

		public String getChildCount() {
		return childCount;
		}

		public void setChildCount(String childCount) {
		this.childCount = childCount;
		}

		public String getInfantCount() {
		return infantCount;
		}

		public void setInfantCount(String infantCount) {
		this.infantCount = infantCount;
		}

		public String getResidentofIndia() {
		return residentofIndia;
		}

		public void setResidentofIndia(String residentofIndia) {
		this.residentofIndia = residentofIndia;
		}

		public String getOptional1() {
		return optional1;
		}

		public void setOptional1(String optional1) {
		this.optional1 = optional1;
		}

		public String getOptional2() {
		return optional2;
		}

		public void setOptional2(String optional2) {
		this.optional2 = optional2;
		}

		public String getOptional3() {
		return optional3;
		}

		public void setOptional3(String optional3) {
		this.optional3 = optional3;
		}

}
