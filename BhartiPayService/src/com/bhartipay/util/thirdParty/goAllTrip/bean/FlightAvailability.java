package com.bhartipay.util.thirdParty.goAllTrip.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightAvailability {
	@SerializedName("AvailabilityInput")
	@Expose
	private AvailabilityInput availabilityInput;

	public AvailabilityInput getAvailabilityInput() {
	return availabilityInput;
	}

	public void setAvailabilityInput(AvailabilityInput availabilityInput) {
	this.availabilityInput = availabilityInput;
	}
	

}
