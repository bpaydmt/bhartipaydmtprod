package com.bhartipay.util.thirdParty.goAllTrip.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JourneyDetail {

	@SerializedName("Origin")
	@Expose
	private String origin;
	@SerializedName("Destination")
	@Expose
	private String destination;
	
	@SerializedName("TravelDate")
	@Expose
	private String travelDate;

	public String getOrigin() {
	return origin;
	}

	public void setOrigin(String origin) {
	this.origin = origin;
	}

	public String getDestination() {
	return destination;
	}

	public void setDestination(String destination) {
	this.destination = destination;
	}

	public String getTravelDate() {
	return travelDate;
	}

	public void setTravelDate(String travelDate) {
	this.travelDate = travelDate;
	}

	
}
