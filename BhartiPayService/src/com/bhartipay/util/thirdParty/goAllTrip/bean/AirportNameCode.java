package com.bhartipay.util.thirdParty.goAllTrip.bean;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirportNameCode implements Serializable{
	
	@SerializedName("id")
	@Expose
	private String id;
	
	@SerializedName("fcity")
	@Expose
	private String fcity;

	public String getId() {
	return id;
	}

	public void setId(String id) {
	this.id = id;
	}

	public String getFcity() {
	return fcity;
	}

	public void setFcity(String fcity) {
	this.fcity = fcity;
	}
	
	
	

}
