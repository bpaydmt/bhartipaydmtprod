package com.bhartipay.util.thirdParty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AadharShilaDMTResponse {
@SerializedName("errorMsg")
@Expose
private String errorMsg;
@SerializedName("errorCode")
@Expose
private String errorCode;
@SerializedName("Reason")
@Expose
private String Reason;
@SerializedName("data")
@Expose
private Data data;

public String getErrorMsg() {
return errorMsg;
}

public void setErrorMsg(String errorMsg) {
this.errorMsg = errorMsg;
}

public String getErrorCode() {
return errorCode;
}

public void setErrorCode(String errorCode) {
this.errorCode = errorCode;
}

public String getReason() {
	return Reason;
}

public void setReason(String reason) {
	Reason = reason;
}

public Data getData() {
return data;
}

public void setData(Data data) {
this.data = data;
}

@Override
public String toString() {
	return "AadharShilaDMTResponse [errorMsg=" + errorMsg + ", errorCode=" + errorCode + ", Reason=" + Reason
			+ ", data=" + data + "]";
}



}
