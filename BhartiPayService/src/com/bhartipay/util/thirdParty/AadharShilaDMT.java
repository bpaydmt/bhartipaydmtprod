package com.bhartipay.util.thirdParty;

import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import java.io.*;
import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.util.thirdParty.bean.AadharShilaCredentials;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class AadharShilaDMT {

	public AadharShilaDMT() {
		AadharShilaCredentials.setAadharShilaCredentials();
	}
	
//	public final static String SENDER_REGISTRATION_OTP_URL = "https://services.bankit.in:8443/DMRV1.1/generic/otp";
//	public final static String SENDER_REGISTRATION_URL = "https://services.bankit.in:8443/DMRV1.1/customer/create";
//	public final static String FETCH_SENDER = "https://services.bankit.in:8443/DMRV1.1/customer/fetch";
//	public final static String BENEFICIARY_REGISTRATION_URL="https://services.bankit.in:8443/DMRV1.1/recipient/add";
//	public final static String FETCH_BANK_LIST="https://services.bankit.in:8443/DMRV1.1/generic/bankList";
//	public final static String IMPS_REMIT_URL="https://services.bankit.in:8443/DMRV1.1/transact/IMPS/remit";
//	public final static String NEFT_REMIT_URL="https://services.bankit.in:8443/DMRV1.1/transact/NEFT/remit";
//	public final static String ACCOUNT_VERIFICATION="https://services.bankit.in:8443/DMRV1.1/transact/IMPS/accountverification";
	public final static String CLIENT_ID="BHARTIPAY SERVICES PRIVATE LIMITED-ALI222714";
	public final static String CLIENT_KEY="vqnz7kgvtb";
	
	//public final static String SENDER_REGISTRATION_OTP_URL = "http://125.63.96.115:9012/DMR/generic/otp";
	//public final static String SENDER_REGISTRATION_URL = "http://125.63.96.115:9012/DMR/customer/create";
	//public final static String FETCH_SENDER = "http://125.63.96.115:9012/DMR/customer/fetch";
	//public final static String BENEFICIARY_REGISTRATION_URL="http://125.63.96.115:9012/DMR/recipient/add";
	//public final static String FETCH_BANK_LIST="http://125.63.96.115:9012/DMR/generic/bankList";
	//public final static String IMPS_REMIT_URL="http://125.63.96.115:9012/DMR/transact/IMPS/remit";
	//public final static String NEFT_REMIT_URL="http://125.63.96.115:9012/DMR/transact/NEFT/remit";
	//public final static String ACCOUNT_VERIFICATION="http://125.63.96.115:9012/DMR/transact/IMPS/accountverification";
	//public final static String CLIENT_ID="Lokesh264542";
	//public final static String CLIENT_KEY="m0c0dejt9n";

	public final static String SENDER_REGISTRATION_OTP_URL = "/generic/otp";
	public final static String SENDER_REGISTRATION_URL = "/customer/create";
	public final static String FETCH_SENDER = "/customer/fetch";
	public final static String BENEFICIARY_REGISTRATION_URL="/recipient/add";
	public final static String FETCH_BANK_LIST="/generic/bankList";
	public final static String IMPS_REMIT_URL="/transact/IMPS/remit";
	public final static String NEFT_REMIT_URL="/transact/NEFT/remit";
	public final static String ACCOUNT_VERIFICATION="/transact/IMPS/accountverification";
	public final static String STATUS_QUERY = "/transact/searchtxn";
	public final static String banklist = "https://services.bankit.in:8443/DMRV1.1/generic/bankList";

	public AadharShilaDMTResponse getBankList() {

		JSONObject jsonText = new JSONObject();
		jsonText.put("", "");

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter("BHARTIPAY SERVICES PRIVATE LIMITED-ALI222714", "vqnz7kgvtb"));
		WebResource webResource = client.resource(banklist);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);

		return resp;

	}

	
	public AadharShilaDMTResponse senderRegistrationOTP(String mobileNo,String agentId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "Mobile number :-"+mobileNo+ "|agentId :-"+agentId+"--senderRegistrationOTP");
		
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentId);
		jsonText.put("customerId", mobileNo);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+SENDER_REGISTRATION_OTP_URL);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);

		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
	}

	public AadharShilaDMTResponse senderRegistration(MudraSenderBank sender,String agentId,String dob,String address) {

//		{"agentCode":"1","customerId": "9243000200","name": "Test","address":"Airoli, Navi mumbai","dateOfBirth":"1997-09-26","otp":"179503"}
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentId);
		jsonText.put("customerId", sender.getMobileNo());
		jsonText.put("name", sender.getFirstName());
		jsonText.put("address", address);
		jsonText.put("dateOfBirth", dob);
		jsonText.put("otp", sender.getOtp());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--senderRegistration");

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+SENDER_REGISTRATION_URL);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
	}

	public AadharShilaDMTResponse fetchSender(String mobileNo,String agentId) {

		// {"agentCode":"1","customerId" : "9243000200"}
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentId);
		jsonText.put("customerId", mobileNo);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--fetchSender");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+FETCH_SENDER);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
	}
	

	public AadharShilaDMTResponse registerBeneficiary(MudraBeneficiaryBank mudraBank,String mobileNo, String agentId) {

		// {"agentCode":"1""bankName":"1229","customerId": "9243000200","accountNo": "11111234212451","ifsc": "SBIN0003625","mobileNo": "9243000200","recipientName":"vikash",}
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentId);
		jsonText.put("customerId", mobileNo);
		jsonText.put("bankName", mudraBank.getBankCode());
		jsonText.put("accountNo", mudraBank.getAccountNo());
		jsonText.put("ifsc", mudraBank.getIfscCode());
		jsonText.put("mobileNo", mobileNo);
		jsonText.put("recipientName",mudraBank.getName());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--registerBeneficiary");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+BENEFICIARY_REGISTRATION_URL);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
	}
	
	public AadharShilaDMTResponse fetchBankList() {
		
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+FETCH_BANK_LIST);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		System.out.println(resp.getData().getBankList().toString());
		return resp;
	}
	
	
public AadharShilaDMTResponse IMPSRemit(String agentCode,String customerId,String recipientId,String amount,String clientRefId) {
		
	//{"agentCode":"1","recipientId":"1587879","customerId":"9243000200","amount":"1.0","clientRefId":"10029012309876543454"} 
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentCode);
		jsonText.put("customerId", customerId);
		jsonText.put("recipientId", recipientId);
		jsonText.put("amount", amount);
		jsonText.put("clientRefId", clientRefId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--IMPSRemit");

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+IMPS_REMIT_URL);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
		
	}
	
	
	public AadharShilaDMTResponse NEFTRemit(String agentCode,String customerId,String recipientId,String amount,String clientRefId) {
	
	//{"agentCode":"1","recipientId":"1587879","customerId":"9243000200","amount":"1.0","clientRefId":"10029012309876543454"} 
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentCode);
		jsonText.put("customerId", customerId);
		jsonText.put("recipientId", recipientId);
		jsonText.put("amount", amount);
		jsonText.put("clientRefId", clientRefId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--NEFTRemit");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+NEFT_REMIT_URL);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		return resp;
		
	}
	
	
	public AadharShilaDMTResponse statusQuery(String clientRefId) {
		
		JSONObject jsonText = new JSONObject();
		jsonText.put("clientRefId", clientRefId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--statusQuery");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+STATUS_QUERY);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		System.out.println(resp.getData().getBankList().toString());
		return resp;
		
	}
	
	
	
	public AadharShilaDMTResponse accuntVerification(String agentCode,String customerId,String amount,String clientRefId,String accontNo,String ifsc) {
		
	//{"agentCode":"1","customerId":"9243000200","amount":"1.0","clientRefId":"100290123098 76543454","udf1":"11111234212","udf2":"SBIN000625"} 
		JSONObject jsonText = new JSONObject();
		jsonText.put("agentCode", agentCode);
		jsonText.put("customerId", customerId);
		jsonText.put("amount", amount);
		jsonText.put("clientRefId", clientRefId);
		jsonText.put("udf1", accontNo);
		jsonText.put("udf2", ifsc);
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","start calling" + "with details :-"+jsonText.toString()+"--accuntVerification");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		client.addFilter(new HTTPBasicAuthFilter(AadharShilaCredentials.CLIENT_ID,AadharShilaCredentials.CLIENT_KEY));
		WebResource webResource = client.resource(AadharShilaCredentials.BASE_URL+ACCOUNT_VERIFICATION);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","getting response " +output);
		AadharShilaDMTResponse resp = gson.fromJson(output, AadharShilaDMTResponse.class);
		
		return resp;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) throws FileNotFoundException {
		AadharShilaDMT aadharShilaDMT = new AadharShilaDMT();

//		imps remit
		// Creating a File object that represents the disk file.
		// PrintStream o = new PrintStream(new File("D:\\A.txt"));

		// Store current System.out before assigning a new value
		PrintStream console = System.out;
//{"agentCode":"BPA001063","accountNo":"3539861637","customerId":"9971463210","recipientName":"Mr PANKAJ  KUMAR","bankName":"1147","mobileNo":"9971463210","ifsc":"CBIN0282643"}
		// Assign o to output stream
//        System.setOut(o); 
		 System.out.println(aadharShilaDMT.getBankList());
		// aadharShilaDMT.statusQuery("AS1PROI00102226");
		// "agentCode":"BPA001041","customerId":"9873013205",bankcode:1229,accountno:61274955476,,ifsccode:SBIN0001537,
		// recipientName:Mr ARVIND KUMAR
//		MudraBeneficiaryBank mudraBank = new MudraBeneficiaryBank();
//		mudraBank.setAccountNo("3539861637");
//		mudraBank.setBankCode("1147");
//		mudraBank.setIfscCode("CBIN0282643");
//		mudraBank.setName("Mr PANKAJ  KUMAR");
//		AadharShilaDMTResponse response = aadharShilaDMT.registerBeneficiary(mudraBank, "9971463210", "BPA001063");
//		System.out.println(response);

	}
}
