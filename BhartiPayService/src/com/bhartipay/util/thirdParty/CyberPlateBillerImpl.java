package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.CyberPlat.IPriv;
import org.CyberPlat.IPrivKey;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.biller.bean.BillerURLDetailsBean;
import com.bhartipay.util.DBUtil;






public class CyberPlateBillerImpl {
	public static final Logger LOGGER = Logger.getLogger(CyberPlateBillerImpl.class.getName());
	
	/*private static final String ENC = "windows-1251";
	private static final String SD = "1003829";
	private static final String AP = "1003851";
	private static final String OP = "1003852";
	private static final String KEYS = "./keys";
	private static final String PASS = "3333333333";*/
	
	/* private static final String ENC = "windows-1251";
     private static final String SD = "326587";
     private static final String AP = "327322";
     private static final String OP = "327323";
     private static final String KEYS = "/home/sandeep/iprivpg/src/java/test/pdtest/KEYS";
     private static final String PASS = "Appnit@123";*/
     
     
     
  /*   private static final String ENC = "windows-1251";
     private static final String SD = "1003829";
     private static final String AP = "1003851";
     private static final String OP = "1003852";
     private static final String KEYS = "/home/support/data1/LinuxJava/keys";
     private static final String PASS = "3333333333";*/

     
   //production setup
     private static final String ENC = "windows-1251";
     private static final String SD = "326585";
     private static final String AP = "327322";
     private static final String OP = "348145";//"327323";
     private static final String KEYS = "/appnit/resource/cyberplat";
     private static final String PASS = "bhartipay@123";
     

	private IPrivKey sec = null;
	private IPrivKey pub = null;
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	String res="0000053801SM000001850000018500000217"
			+"\ninist_in            00000040"
			+"\n                    00000000"
			+"\nBEGIN"
			+"\nDATE=18.07.2016 17:01:49"
			+"\nSESSION=aacctt66"
			+"\nERROR=0"
			+"\nRESULT=0"
			+"\nTRANSID=1000927570523"
			+"\nADDINFO=%3CNA%3E%20%3CNA%3E%20%3C20160726%3E%20%3C860.00%20%3E%20%3CN%3E%20%3CNA%21NA%3E"
			+"\nPRICE=860.00"
			+"\nERRMSG=Invalid amount inputed. You have to pay exactly bill amount"
			+"\n"
			+"\nEND"
			+"\nBEGIN SIGNATURE"
			+"\niQCRAwkBAAAAKFap8kkBARrzBACmlmUYg1qsRBJIh0H0dumBrXNWe6aHL6YrR20X"
			+"\nks/bct/Z66WLE/OqnQs2WKJ8Bk3x7H6qOY+ndT4arpo5nNjEv2Lr92M2PKEG/nt5"
			+"\ntxB1UpuaZsay1VtGksx7U1cUsfIc+gM5nv9XB65CGy/dB0qqd0Wr3VzUwUrU76C1"
			+"\npU5fMLABxw=="
			+"\n=/Mam"
			+"\nEND SIGNATURE";
	
	
	String payres="0000053801SM000001850000018500000217"
			+"\ninist_in            00000040"
			+"\n                    00000000"
			+"\nBEGIN"
			+"\nDATE=18.07.2016 17:01:49"
			+"\nSESSION=aacctt66"
			+"\nERROR=0"
			+"\nRESULT=0"
			+"\nTRANSID=1000927570523"
			+"\nAUTHCODE=CYB1000930628946"
			+"\nTRNXSTATUS=7"
			+"\nERRMSG=Success"
			+"\n"
			+"\nEND"
			+"\nBEGIN SIGNATURE"
			+"\niQCRAwkBAAAAKFap8kkBARrzBACmlmUYg1qsRBJIh0H0dumBrXNWe6aHL6YrR20X"
			+"\nks/bct/Z66WLE/OqnQs2WKJ8Bk3x7H6qOY+ndT4arpo5nNjEv2Lr92M2PKEG/nt5"
			+"\ntxB1UpuaZsay1VtGksx7U1cUsfIc+gM5nv9XB65CGy/dB0qqd0Wr3VzUwUrU76C1"
			+"\npU5fMLABxw=="
			+"\n=/Mam"
			+"\nEND SIGNATURE";
	
	
	public String elBillVerification(String billerId,String consumerID,String sessionId,Double amount,String account,String additionalInfo3){
		
		LOGGER.debug("********elBillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********elBillVerification**********billerId: " + billerId);
		LOGGER.debug("********elBillVerification**********consumerID: " + consumerID);
		LOGGER.debug("********elBillVerification**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getVerificationURL(billerId);
		String reqparams=getRequestParams(billerId,sessionId,consumerID,amount,account,additionalInfo3);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			LOGGER.debug(sessionId+"********secret.key**********path: " + KEYS + "/secret.key");
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parseValidationResponse(apiResp);
		LOGGER.debug(sessionId+"********billVerification**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in elBillVerification**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
//	return parseValidationResponse(res);
	}
	
	
	
	public String elBillPayment(String billerId,String consumerID,String sessionId,Double amount,String account,String additionalInfo3){
		
		LOGGER.debug("********elBillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********elBillVerification**********billerId: " + billerId);
		LOGGER.debug("********elBillVerification**********consumerID: " + consumerID);
		LOGGER.debug("********elBillVerification**********amount: " + amount);
		
		String apiResp=null;
		String paymentURL=getPaymentURL(billerId);
		String reqparams=getRequestParams(billerId,sessionId,consumerID,amount,account,additionalInfo3);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			LOGGER.debug(sessionId+"********secret.key**********path: " + KEYS + "/secret.key");
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(paymentURL,reqparams);
		 apiResp =parsePaymentResponse(apiResp);
		LOGGER.info(sessionId+"********elBillPayment**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in elBillPayment**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
//		return parseValidationResponse(payres);
	}

	
	public String elBillStatus(String billerId,String consumerID,String sessionId,Double amount,String account,String additionalInfo3){
		
		LOGGER.debug("********elBillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********elBillVerification**********billerId: " + billerId);
		LOGGER.debug("********elBillVerification**********consumerID: " + consumerID);
		LOGGER.debug("********elBillVerification**********amount: " + amount);
		
		String apiResp=null;
		String paymentURL=getStatusURL(billerId);
		String reqparams=getRequestParams(billerId,sessionId,consumerID,amount,account,additionalInfo3);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			LOGGER.debug(sessionId+"********secret.key**********path: " + KEYS + "/secret.key");
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
			apiResp = cyberPlateBillerImpl.sendRequest(paymentURL,reqparams);
			apiResp =parseStatusResponse(apiResp);
			LOGGER.info(sessionId+"********elBillPayment**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in elBillStatus**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
		
	}
	
	
	
	private String getRequestParams(String billerId,String sessionId,String consumerID,double amount,String account,String additionalInfo3){
		String comment="bill";
		//ACCOUNT
		if(Integer.parseInt(billerId)==235  ||Integer.parseInt(billerId)==332){
			String reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + consumerID + "\r\n"+"ACCOUNT=" + account + "\r\n" +"AMOUNT=" + amount + "\r\n" +"AMOUNT_ALL=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
			LOGGER.debug(sessionId+"******************reqparams: " + reqparams);
			return reqparams;
			
		}else if(Integer.parseInt(billerId)==342){
			//Authenticator3
			String reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + consumerID + "\r\n"+"ACCOUNT=" + account+ "\r\n"+"Authenticator3=" + additionalInfo3 + "\r\n" +"AMOUNT=" + amount + "\r\n" +"AMOUNT_ALL=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
			LOGGER.debug(sessionId+"******************reqparams: " + reqparams);
			return reqparams;	
			
		}else{
			String reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + consumerID + "\r\n" +"AMOUNT=" + amount + "\r\n" +"AMOUNT_ALL=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
			LOGGER.debug(sessionId+"******************reqparams: " + reqparams);
			return reqparams;	
		}
		
	}
	
	
	
	
	public String gasbillVerification(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********gasbillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********gasbillVerification**********billerId: " + billerId);
		LOGGER.debug("********gasbillVerification**********consumerID: " + consumerID);
		LOGGER.debug("********gasbillVerification**********account: " + account);
		LOGGER.debug("********gasbillVerification**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getVerificationURL(billerId);
		String reqparams=getGasRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parseValidationResponse(apiResp);
		LOGGER.debug(sessionId+"********gasbillVerification**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in gasbillVerification**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	
	public String gasbillPayment(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********gasbillPayment**********sessionId: " + sessionId);
		LOGGER.debug("********gasbillPayment**********billerId: " + billerId);
		LOGGER.debug("********gasbillPayment**********consumerID: " + consumerID);
		LOGGER.debug("********gasbillPayment**********account: " + account);
		LOGGER.debug("********gasbillPayment**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getPaymentURL(billerId);
		String reqparams=getGasRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parsePaymentResponse(apiResp);
		LOGGER.debug(sessionId+"********gasbillPayment**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in gasbillPayment**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	public String gasbillStatus(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********gasbillStatus**********sessionId: " + sessionId);
		LOGGER.debug("********gasbillStatus**********billerId: " + billerId);
		LOGGER.debug("********gasbillStatus**********consumerID: " + consumerID);
		LOGGER.debug("********gasbillStatus**********account: " + account);
		LOGGER.debug("********gasbillStatus**********amount: " + amount);
		LOGGER.debug("********gasbillStatus**********sessionId: " + sessionId);
		String apiResp=null;
		String statusURL=getStatusURL(billerId);
		String reqparams=getGasRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(statusURL,reqparams);
		 apiResp =parseStatusResponse(apiResp);
		LOGGER.debug(sessionId+"********gasbillStatus**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in gasbillStatus**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	
	
	public String insurancebillVerification(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********insurancebillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********insurancebillVerification**********billerId: " + billerId);
		LOGGER.debug("********insurancebillVerification**********consumerID: " + consumerID);
		LOGGER.debug("********insurancebillVerification**********account: " + account);
		LOGGER.debug("********insurancebillVerification**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getVerificationURL(billerId);
		String reqparams=getInsuranceRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parseValidationResponse(apiResp);
		LOGGER.debug(sessionId+"********insurancebillVerification**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in insurancebillVerification**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	
	public String insurancebillPayment(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********insurancebillPayment**********sessionId: " + sessionId);
		LOGGER.debug("********insurancebillPayment**********billerId: " + billerId);
		LOGGER.debug("********insurancebillPayment**********consumerID: " + consumerID);
		LOGGER.debug("********insurancebillPayment**********account: " + account);
		LOGGER.debug("********insurancebillPayment**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getPaymentURL(billerId);
		String reqparams=getInsuranceRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parsePaymentResponse(apiResp);
		LOGGER.debug(sessionId+"********insurancebillPayment**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in insurancebillPayment**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	
	public String insurancebillStatus(String billerId,String consumerID,String account,String sessionId,Double amount){
		LOGGER.debug("********insurancebillStatus**********sessionId: " + sessionId);
		LOGGER.debug("********insurancebillStatus**********billerId: " + billerId);
		LOGGER.debug("********insurancebillStatus**********consumerID: " + consumerID);
		LOGGER.debug("********insurancebillStatus**********account: " + account);
		LOGGER.debug("********insurancebillStatus**********amount: " + amount);
		
		String apiResp=null;
		String verificationURL=getStatusURL(billerId);
		String reqparams=getInsuranceRequestParams(billerId,consumerID,account,sessionId,amount);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parseStatusResponse(apiResp);
		LOGGER.debug(sessionId+"********insurancebillVerification**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in insurancebillStatus**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
	}
	
	
	
	private String getInsuranceRequestParams(String billerId,String consumerID,String account,String sessionId,Double amount){
		String comment="Insurance";
		String reqparams;
		reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" +account+ "\r\n" +"ACCOUNT=" + consumerID + "\r\n" +"AMOUNT=" + amount + "\r\n" +"AMOUNT_ALL=" + amount + "\r\n" +"TERM_ID=" + AP + "\r\n" +"COMMENT=" + comment + "\r\n";
		LOGGER.debug("**********getInsuranceRequestParams********reqparams: " + reqparams);
		return reqparams;
	}
	
	
	private String sendRequest (String url,String requestparm) throws Exception {
		
		String req =requestparm;
		req = "inputmessage=" + URLEncoder.encode(sec.signText(req));
		LOGGER.debug("******************REQUEST: " + req);
		URL u = new URL(url);
		URLConnection con = u.openConnection();
		con.setDoOutput(true);
		con.getOutputStream().write(req.getBytes());
		con.getOutputStream().close();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), ENC));
		char[] raw_resp = new char[1024];
		int raw_resp_len = in.read(raw_resp);
		StringBuffer s = new StringBuffer();
		s.append(raw_resp, 0, raw_resp_len);
		String resp = s.toString();
		LOGGER.debug("******************RESPONSE: " + resp);
		return resp;
		
	}
	
	private void done()
	{
		if (sec != null)
			sec.closeKey();
		
	}
	
	private  String getVerificationURL(String billerId) {
		String verificationURL=null;
		factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		 transaction = session.beginTransaction();
		try {
			BillerURLDetailsBean billerURLDetailsBean=(BillerURLDetailsBean)session.get(BillerURLDetailsBean.class , new Integer(billerId));
			verificationURL=billerURLDetailsBean.getVerificationUrl();
			LOGGER.debug(billerId+"******************getVerificationURL: " + verificationURL);
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			LOGGER.debug(billerId+"problem in featching  Verification URL info for biller "+ e.getMessage(), e);
		} finally {
			session.close();
		}
		return verificationURL;
	}
	
	
	private  String getPaymentURL(String billerId) {
		String paymentURL=null;
		factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		 transaction = session.beginTransaction();
		try {
			BillerURLDetailsBean billerURLDetailsBean=(BillerURLDetailsBean)session.get(BillerURLDetailsBean.class , new Integer(billerId));
			paymentURL=billerURLDetailsBean.getPaymentUrl();
			LOGGER.debug(billerId+"******************paymentURL: " + paymentURL);
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			LOGGER.debug(billerId+"problem in featching  payment URL info for biller "+ e.getMessage(), e);
		} finally {
			session.close();
		}
		return paymentURL;
	}
	
	
	private String getStatusURL(String billerId) {
		String statusURL=null;
		factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		 transaction = session.beginTransaction();
		try {
			BillerURLDetailsBean billerURLDetailsBean=(BillerURLDetailsBean)session.get(BillerURLDetailsBean.class , new Integer(billerId));
			statusURL=billerURLDetailsBean.getStatusUrl();
			LOGGER.debug(billerId+"******************statusURL: " + statusURL);
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			LOGGER.debug(billerId+"problem in featching  status URL info for biller "+ e.getMessage(), e);
		} finally {
			session.close();
		}
		return statusURL;
	}
	
	

	
	
	
	private String getGasRequestParams(String billerId,String consumerID,String account,String sessionId,Double amount){
		String comment="Gas";
		String reqparams;
		if(Integer.parseInt(billerId)==241){
		 reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + consumerID + "\r\n" +"ACCOUNT=" + account + "\r\n" +"AMOUNT=" + amount + "\r\n" +"AMOUNT_ALL=" + amount + "\r\n" +"TERM_ID=" + AP + "\r\n" +"COMMENT=" + comment + "\r\n";
		}else{
			 reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + consumerID + "\r\n" +"AMOUNT=" + amount + "\r\n"  +"COMMENT=" + comment + "\r\n";
		}
		LOGGER.debug(sessionId+"**********getGasRequestParams********reqparams: " + reqparams);
		return reqparams;
	}
	
	
	
	
	
	
	
	public String landLineBillVerification(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId){
		
		LOGGER.debug("********landLineBillVerification**********sessionId: " + sessionId);
		LOGGER.debug("********landLineBillVerification**********billerId: " + billerId);
		LOGGER.debug("********landLineBillVerification**********phoneNumber: " + phoneNumber);
		LOGGER.debug("********landLineBillVerification**********accountNumber: " + accountNumber);
		LOGGER.debug("********landLineBillVerification**********amount: " + amount);
		LOGGER.debug("********landLineBillVerification**********authenticator3: " + authenticator3);
		LOGGER.debug("********landLineBillVerification**********termId: " + termId);
	
		String apiResp=null;
		String verificationURL=getVerificationURL(billerId);
		String reqparams=getLandLineRequestParams(billerId,sessionId,phoneNumber,accountNumber,amount,authenticator3,termId);
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(verificationURL,reqparams);
		 apiResp =parseValidationResponse(apiResp);
		LOGGER.debug(sessionId+"********landLineBillVerification**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in landLineBillVerification**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
		//return parseValidationResponse(res);
	}
	
	
	
	
	
	public String landLineBillPayment(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId){
		
		LOGGER.debug("********landLineBillPayment**********sessionId: " + sessionId);
		LOGGER.debug("********landLineBillPayment**********billerId: " + billerId);
		LOGGER.debug("********landLineBillPayment**********phoneNumber: " + phoneNumber);
		LOGGER.debug("********landLineBillPayment**********accountNumber: " + accountNumber);
		LOGGER.debug("********landLineBillPayment**********amount: " + amount);
		LOGGER.debug("********landLineBillPayment**********authenticator3: " + authenticator3);
		LOGGER.debug("********landLineBillPayment**********termId: " + termId);
		
		
		String apiResp=null;
		String paymentURL=getPaymentURL(billerId);
		String reqparams=getLandLineRequestParams(billerId,sessionId,phoneNumber,accountNumber,amount,authenticator3,termId);;
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(paymentURL,reqparams);
		 apiResp =parsePaymentResponse(apiResp);
		LOGGER.info(sessionId+"********landLineBillPayment**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in landLineBillPayment**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
		//return parseValidationResponse(payres);
	}
	


	public String landLineBillStatus(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId){
		
		LOGGER.debug("********landLineBillStatus**********sessionId: " + sessionId);
		LOGGER.debug("********landLineBillStatus**********billerId: " + billerId);
		LOGGER.debug("********landLineBillStatus**********phoneNumber: " + phoneNumber);
		LOGGER.debug("********landLineBillStatus**********accountNumber: " + accountNumber);
		LOGGER.debug("********landLineBillStatus**********amount: " + amount);
		LOGGER.debug("********landLineBillStatus**********authenticator3: " + authenticator3);
		LOGGER.debug("********landLineBillStatus**********termId: " + termId);
		
		String apiResp=null;
		String paymentURL=getStatusURL(billerId);
		String reqparams=getLandLineRequestParams(billerId,sessionId,phoneNumber,accountNumber,amount,authenticator3,termId);;
		CyberPlateBillerImpl cyberPlateBillerImpl=new CyberPlateBillerImpl();
		IPriv.setCodePage(ENC);
		try{
			cyberPlateBillerImpl.sec = IPriv.openSecretKey(KEYS + "/secret.key", PASS);
		 apiResp = cyberPlateBillerImpl.sendRequest(paymentURL,reqparams);
		 apiResp =parseStatusResponse(apiResp);
		LOGGER.info(sessionId+"********landLineBillStatus**********apiResp: " + apiResp);
		}catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.debug(sessionId+"******************problem in landLineBillStatus**********: " +  e.getMessage(), e);
		}
		cyberPlateBillerImpl.done();
		return apiResp;
		//return parseValidationResponse(payres);
	}
	
	
	
	
	
	
	
	private String getLandLineRequestParams(String billerId,String sessionId,String phoneNumber,String accountNumber,double amount,String authenticator3,String termId){
		String comment="LandLine";
		String reqparams="";
		if(Integer.parseInt(billerId)==240){
			 reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + phoneNumber + "\r\n" +"ACCOUNT=" + accountNumber + "\r\n"+"AMOUNT=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
		}else if(Integer.parseInt(billerId)==344){
			 reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + phoneNumber + "\r\n" +"ACCOUNT=" + accountNumber + "\r\n" +"Authenticator3=" + authenticator3 + "\r\n"+"AMOUNT=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
		}else if(Integer.parseInt(billerId)==239){
			 reqparams ="SD=" + SD + "\r\n" +"AP=" + AP + "\r\n" +"OP=" + OP + "\r\n" +"SESSION=" + sessionId + "\r\n" +"NUMBER=" + phoneNumber + "\r\n" +"TERM_ID=" + termId + "\r\n"+"AMOUNT=" + amount + "\r\n" +"COMMENT=" + comment + "\r\n";
		}
		LOGGER.debug(sessionId+"******************reqparams: " + reqparams);
		return reqparams;
		
	}	
	
	
	
	
	
	
	
	private String parseValidationResponse(String apiResp){
		StringBuilder apiRespRest=new StringBuilder() ;
		LOGGER.debug("******************apiResp: " + apiResp);
		String[] str=apiResp.substring(apiResp.indexOf("BEGIN")+5, apiResp.indexOf("END")).split("\\r?\\n");
		for (String string : str) {
			if(string.contains("="))
				apiRespRest.append(string+"|");
				
		}
		LOGGER.debug("******************apiRespRest: " + apiRespRest.subSequence(0, apiRespRest.length()-1));
		return apiRespRest.subSequence(0, apiRespRest.length()-1).toString();
		
	}
	
	
	
	
	private String parsePaymentResponse(String apiResp){
		StringBuilder apiResppayment=new StringBuilder() ;
		LOGGER.debug("******************apiResp: " + apiResp);
		String[] str=apiResp.substring(apiResp.indexOf("BEGIN")+5, apiResp.indexOf("END")).split("\\r?\\n");
		for (String string : str) {
			if(string.contains("="))
					apiResppayment.append(string+"|");
				}
			//cResponse.put(string.substring(0, string.indexOf('=')),string.substring(string.indexOf('=') + 1));
		
		LOGGER.debug("******************apiRespRest: " + apiResppayment.subSequence(0, apiResppayment.length()-1));
		return apiResppayment.subSequence(0, apiResppayment.length()-1).toString();
	}
	
	
	private String parseStatusResponse(String apiResp){
		StringBuilder apiRespStatus=new StringBuilder() ;
		LOGGER.debug("******************apiResp: " + apiResp);
		String[] str=apiResp.substring(apiResp.indexOf("BEGIN")+5, apiResp.indexOf("END")).split("\\r?\\n");
		for (String string : str) {
			if(string.contains("="))				
					apiRespStatus.append(string+"|");			
		}
		LOGGER.debug("******************apiRespRest: " + apiRespStatus.subSequence(0, apiRespStatus.length()-1));
		return apiRespStatus.subSequence(0, apiRespStatus.length()-1).toString();
	}
	
	
	

}
