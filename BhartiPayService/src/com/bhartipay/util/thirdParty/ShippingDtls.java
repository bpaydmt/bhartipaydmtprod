package com.bhartipay.util.thirdParty;

import java.io.Serializable;



public class ShippingDtls extends ValueObject implements Serializable{
	
	private String deliveryName;
	private String deliveryAddress;
	private String deliveryCity;
	private String deliveryState;
	private String deliveryPinCode;
	private String deliveryCountry;
	private String deliveryPhNo1;
	private String deliveryPhNo2;
	private String deliveryPhNo3;
	private String deliveryMobileNo;
	
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getDeliveryCity() {
		return deliveryCity;
	}
	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}
	public String getDeliveryState() {
		return deliveryState;
	}
	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}
	public String getDeliveryPinCode() {
		return deliveryPinCode;
	}
	public void setDeliveryPinCode(String deliveryPinCode) {
		this.deliveryPinCode = deliveryPinCode;
	}
	public String getDeliveryCountry() {
		return deliveryCountry;
	}
	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}
	public String getDeliveryPhNo1() {
		return deliveryPhNo1;
	}
	public void setDeliveryPhNo1(String deliveryPhNo1) {
		this.deliveryPhNo1 = deliveryPhNo1;
	}
	public String getDeliveryPhNo2() {
		return deliveryPhNo2;
	}
	public void setDeliveryPhNo2(String deliveryPhNo2) {
		this.deliveryPhNo2 = deliveryPhNo2;
	}
	public String getDeliveryPhNo3() {
		return deliveryPhNo3;
	}
	public void setDeliveryPhNo3(String deliveryPhNo3) {
		this.deliveryPhNo3 = deliveryPhNo3;
	}
	public String getDeliveryMobileNo() {
		return deliveryMobileNo;
	}
	public void setDeliveryMobileNo(String deliveryMobileNo) {
		this.deliveryMobileNo = deliveryMobileNo;
	}
	
	
}
