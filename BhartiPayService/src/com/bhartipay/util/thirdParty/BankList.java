package com.bhartipay.util.thirdParty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankList {

@SerializedName("bankCode")
@Expose
private String bankCode;
@SerializedName("bankName")
@Expose
private String bankName;
@SerializedName("channelsSupported")
@Expose
private String channelsSupported;
@SerializedName("accVerAvailabe")
@Expose
private String accVerAvailabe;
@SerializedName("ifsc")
@Expose
private String ifsc;
@SerializedName("ifscStatus")
@Expose
private String ifscStatus;

public String getBankCode() {
return bankCode;
}

public void setBankCode(String bankCode) {
this.bankCode = bankCode;
}

public String getBankName() {
return bankName;
}

public void setBankName(String bankName) {
this.bankName = bankName;
}

public String getChannelsSupported() {
return channelsSupported;
}

public void setChannelsSupported(String channelsSupported) {
this.channelsSupported = channelsSupported;
}

public String getAccVerAvailabe() {
return accVerAvailabe;
}

public void setAccVerAvailabe(String accVerAvailabe) {
this.accVerAvailabe = accVerAvailabe;
}

public String getIfsc() {
return ifsc;
}

public void setIfsc(String ifsc) {
this.ifsc = ifsc;
}

public String getIfscStatus() {
return ifscStatus;
}

public void setIfscStatus(String ifscStatus) {
this.ifscStatus = ifscStatus;
}

@Override
public String toString() {
	return "BankList [bankCode=" + bankCode + ", bankName=" + bankName + ", channelsSupported=" + channelsSupported
			+ ", accVerAvailabe=" + accVerAvailabe + ", ifsc=" + ifsc + ", ifscStatus=" + ifscStatus + "]\n";
}



}