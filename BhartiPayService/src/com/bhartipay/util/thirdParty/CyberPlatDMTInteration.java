package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.CyberPlat.IPriv;
import org.CyberPlat.IPrivKey;
import org.apache.log4j.Logger;

import com.bhartipay.util.thirdParty.bean.DMTBean;

public class CyberPlatDMTInteration {
	public static final Logger LOGGER = Logger.getLogger(CyberPlatDMTInteration.class.getName());
	
	 private static final String ENC = "windows-1251";
     private static final String SD = "326587";
     private static final String AP = "327322";
     private static final String OP = "327323";
     private static final String KEYS = "/home/sandeep/iprivpg/src/java/test/pdtest/KEYS";
    // private static final String KEYS = "./KEYS";
    private static final String PASS = "powerpay@123";
     
     private static final String ACCEPT_KEYS="40";
     private static final String NO_ROUTE="0";
     

	private IPrivKey sec = null;
	private IPrivKey pub = null;
	
	
	public String DMTVerification(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay_check.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 LOGGER.debug("********DMTVerification***********************************************apiResp:\r\n" + apiResp);
			}catch (Exception e)
		{
			LOGGER.debug("******************problem in DMTVerification****************************: " +  e.getMessage(), e);
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	
	
	
	public String DMTPayment(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 LOGGER.debug("********DMTPayment*************************************************************apiResp:\r\n " + apiResp);
			}catch (Exception e)
		{
			LOGGER.debug("******************problem in DMTPayment********************************************: " +  e.getMessage(), e);
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	
	
	public String DMTStatus(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay_status.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 LOGGER.debug("********DMTStatus********************************************************apiResp:\r\n " + apiResp);
			}catch (Exception e)
		{
			LOGGER.debug("******************problem in DMTStatus**********************************************: " +  e.getMessage(), e);
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	
			
	
	private String getRequestParams(DMTBean dMTBean){
		String reqparams ="SD=" + SD + "\r\n" 
						+"AP=" + AP + "\r\n"
						+"OP=" + OP + "\r\n" 
						+"ACCEPT_KEYS="+ACCEPT_KEYS+"\r\n"
						+"NO_ROUTE="+NO_ROUTE+"\r\n"
						+"SESSION="+dMTBean.getSession()+"\r\n" 
						+"pin="+dMTBean.getPin()+"\r\n" 
						+"otc="+dMTBean.getOtc()+"\r\n" 
						+"fName="+dMTBean.getfName()+"\r\n"
						+"routingType="+dMTBean.getRoutingType()+"\r\n" 
						+"mothersMaidenName="+dMTBean.getMothersMaidenName()+"\r\n" 
						+"state="+dMTBean.getState()+"\r\n" 
						+"benAccount="+dMTBean.getBenAccount()+"\r\n" 
						+"TERM_ID="+OP+"\r\n" 
						+"benMobile="+dMTBean.getBenMobile()+"\r\n" 
						+"address="+dMTBean.getAddress()+"\r\n" 
						+"birthday="+dMTBean.getBirthday()+"\r\n" 
						+"NUMBER="+dMTBean.getNumber()+"\r\n" 
						+"gender="+dMTBean.getGender()+"\r\n" 
						+"otcRefCode="+dMTBean.getOtcRefCode()+"\r\n" 
						+"benNick="+dMTBean.getBenNick()+"\r\n" 
						+"benIFSC="+dMTBean.getBenIFSC()+"\r\n" 
						+"lName="+dMTBean.getlName()+"\r\n" 
						+"benName="+dMTBean.getBenName()+"\r\n" 
						+"benCode="+dMTBean.getBenCode()+"\r\n" 
						+"Type="+dMTBean.getType()+"\r\n" 
						+"ACCOUNT="+dMTBean.getAccount()+"\r\n" 
						+"AMOUNT="+dMTBean.getAmount()+"\r\n" 
						+"AMOUNT_ALL="+dMTBean.getAmountAll()+"\r\n" 
						+"COMMENT="+dMTBean.getComment()+"\r\n" ;
				
		System.out.println("**********************************************************reqparams: " + reqparams);
		return reqparams;
		
	}
	
	
	private void done()
	{
		if (sec != null)
			sec.closeKey();
		
	}
	
	
private String sendRequest (String url,String requestparm) throws Exception {
		
		String req =requestparm;
		req = "inputmessage=" + URLEncoder.encode(sec.signText(req));
		LOGGER.debug("***************************************************************************REQUEST:\r\n" + req);
		URL u = new URL(url);
		URLConnection con = u.openConnection();
		con.setDoOutput(true);
		con.getOutputStream().write(req.getBytes());
		con.getOutputStream().close();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), ENC));
		char[] raw_resp = new char[1024];
		int raw_resp_len = in.read(raw_resp);
		StringBuffer s = new StringBuffer();
		s.append(raw_resp, 0, raw_resp_len);
		String resp = s.toString();
		LOGGER.debug("**********************************************************RESPONSE: \r\n" + resp);
		return resp;
		
	}
	
	
	
	
private String parseValidationResponse(String apiResp){
	StringBuilder apiRespRest=new StringBuilder() ;
	LOGGER.debug("************parseValidationResponse******apiResp:\r\n " + apiResp);
	String[] str=apiResp.substring(apiResp.indexOf("BEGIN")+5, apiResp.indexOf("END")).split("\\r?\\n");
	for (String string : str) {
		if(string.contains("="))
			apiRespRest.append(string+"|");
			
	}
	LOGGER.debug("**********parseValidationResponse********apiRespRest: \r\n" + apiRespRest.subSequence(0, apiRespRest.length()-1));
	return apiRespRest.subSequence(0, apiRespRest.length()-1).toString();
	
}	


public static void main(String[] args) {
	CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
	DMTBean dmTBean =new DMTBean ();
	dmTBean.setSession("user123");
	dmTBean.setTermId(OP);
	dmTBean.setNumber("9935041287");
	dmTBean.setType("5");
	dmTBean.setAmount(1.0);
	dmTBean.setAmountAll(1.0);
	cyberPlatDMTInteration.DMTVerification(dmTBean);
}


	
}


