package com.bhartipay.util.thirdParty;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.bhartipay.rechargeService.bean.RechargeOperatorMasterBean;
import com.bhartipay.util.XmlParser;


public class MyPayStoreImpl 
{
	private final static Logger LOGGER = Logger.getLogger(MyPayStoreImpl.class);
	private final String userName="7011605528";
	private final String password="PAT106";
	private final List<String> failList = new ArrayList<>(Arrays.asList("-1","0","10000","10001","10002","10003","10004","10005","10006","10007","10009","10010","10011"
			,"10013","10014","10015","10016","10017","10018"));
	private final List<String> pendingList = new ArrayList<>(Arrays.asList("10012","10019","10020","10021"));
	//<?xml version='1.0' encoding='ISO-8859-1'?><_ApiResponse><statusCode>10008</statusCode><statusDescription>REQUEST ACCEPTED.</statusDescription><availableBalance>950</availableBalance><requestID>117745449584480</requestID></_ApiResponse>
	public HashMap<String,String> goForRecharge(RechargeOperatorMasterBean recharge,String accountNo,double amount)
	{
		LOGGER.info("start execution of goForRecharge having field as "+recharge.getName()+" "+accountNo+" "+recharge.getClient());
		HashMap<String,String> result = new HashMap<>();
		result.put("statusString", "INCOMPLETE");
		try
		{
			String url = recharge.getUrl();
			int requestType=0;
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("MM.dd.yyyy%20hh:mm:ss");
			DecimalFormat decFor = new DecimalFormat("#");
			if(recharge.getServiceType().toUpperCase().contains("DTH"))
			{
				requestType = 3;
			}
			String requestString = password+"|"+accountNo+"|"+decFor.format(amount)+"|"+recharge.getCode()+"|"+requestType+"|"+df.format(date);//"_prcsr=7011605528&_urlenc=(PAT106)|(9810471689)|(10)|(1)|(0)|("+df.format(date)+")&_encuse=(0)";
			String response = new MyPayStoreService().requestRecharge(userName,requestString,0, url, accountNo);
			if(!response.equals("FAIL"))
			{
				//{statusDescription=REQUEST ACCEPTED., requestID=117745449584480, statusCode=10008, availableBalance=950}
				HashMap<String,String> responseMap = new XmlParser().xmlParser(response);
				String statusCode=responseMap.get("statusCode");
				result.putAll(responseMap);
				if(statusCode.equals("10008"))
				{
					result.put("statusString", "SUBMITTED");
				}
				else if(failList.contains(statusCode))
				{
					result.put("statusString", "FAILED");
				}
				else if(pendingList.contains(statusCode))
				{
					result.put("statusString", "PENDING");
				}
			}
		}
		catch(Exception ex)
		{
			LOGGER.info("problem occured in  goForRecharge having field as "+recharge.getName()+" "+accountNo+" "+recharge.getClient()+" with message "+ex.getMessage());
			ex.printStackTrace();
		}
		return result;
	}
	
	public static void main(String[] args) {
		String xmlvalue = "<?xml version='1.0' encoding='ISO-8859-1'?><_ApiResponse><statusCode>10008</statusCode><statusDescription>REQUEST ACCEPTED.</statusDescription><availableBalance>950</availableBalance><requestID>117745449584480</requestID></_ApiResponse>";
		HashMap<String,String> map = new XmlParser().xmlParser(xmlvalue);
		System.out.println(map);
	}
}
