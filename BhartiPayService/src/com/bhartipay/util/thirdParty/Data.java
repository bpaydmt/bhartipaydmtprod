package com.bhartipay.util.thirdParty;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data{


@SerializedName("customerId")
@Expose
private String customerId;
@SerializedName("recipientId")
@Expose
private String recipientId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("kycstatus")
@Expose
private Integer kycstatus;
@SerializedName("walletbal")
@Expose
private Integer walletbal;
@SerializedName("dateOfBirth")
@Expose
private String dateOfBirth;
@SerializedName("recipientName")
@Expose
private String recipientName;
@SerializedName("mobileNo")
@Expose
private String mobileNo;
@SerializedName("bankName")
@Expose
private String bankName;
@SerializedName("clientRefId")
@Expose
private String clientRefId;
@SerializedName("txnId")
@Expose
private String txnId;
@SerializedName("impsRespCode")
@Expose
private String impsRespCode;
@SerializedName("accountNumber")
@Expose
private String accountNumber;
@SerializedName("ifscCode")
@Expose
private String ifscCode;
@SerializedName("txnStatus")
@Expose
private String txnStatus;

@SerializedName("bankList")
@Expose
private List<BankList> bankList = null;


public List<BankList> getBankList() {
return bankList;
}

public void setBankList(List<BankList> bankList) {
this.bankList = bankList;
}

public String getCustomerId() {
return customerId;
}

public void setCustomerId(String customerId) {
this.customerId = customerId;
}

public String getRecipientId() {
return recipientId;
}

public void setRecipientId(String recipientId) {
this.recipientId = recipientId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getKycstatus() {
return kycstatus;
}

public void setKycstatus(Integer kycstatus) {
this.kycstatus = kycstatus;
}

public Integer getWalletbal() {
return walletbal;
}

public void setWalletbal(Integer walletbal) {
this.walletbal = walletbal;
}

public String getDateOfBirth() {
return dateOfBirth;
}

public void setDateOfBirth(String dateOfBirth) {
this.dateOfBirth = dateOfBirth;
}

public String getRecipientName() {
return recipientName;
}

public void setRecipientName(String recipientName) {
this.recipientName = recipientName;
}

public String getMobileNo() {
return mobileNo;
}

public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

public String getBankName() {
	return bankName;
}

public void setBankName(String bankName) {
	this.bankName = bankName;
}

public String getClientRefId() {
	return clientRefId;
}

public void setClientRefId(String clientRefId) {
	this.clientRefId = clientRefId;
}

public String getTxnId() {
	return txnId;
}

public void setTxnId(String txnId) {
	this.txnId = txnId;
}

public String getImpsRespCode() {
	return impsRespCode;
}

public void setImpsRespCode(String impsRespCode) {
	this.impsRespCode = impsRespCode;
}

public String getAccountNumber() {
	return accountNumber;
}

public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
}

public String getIfscCode() {
	return ifscCode;
}

public void setIfscCode(String ifscCode) {
	this.ifscCode = ifscCode;
}

public String getTxnStatus() {
	return txnStatus;
}

public void setTxnStatus(String txnStatus) {
	this.txnStatus = txnStatus;
}

@Override
public String toString() {
	return "Data [customerId=" + customerId + ", recipientId=" + recipientId + ", name=" + name + ", kycstatus="
			+ kycstatus + ", walletbal=" + walletbal + ", dateOfBirth=" + dateOfBirth + ", recipientName="
			+ recipientName + ", mobileNo=" + mobileNo + ", bankName=" + bankName + ", clientRefId=" + clientRefId
			+ ", txnId=" + txnId + ", impsRespCode=" + impsRespCode + ", accountNumber=" + accountNumber + ", ifscCode="
			+ ifscCode + ", txnStatus=" + txnStatus + ", bankList=" + bankList + "]";
}

}

