package com.bhartipay.util.thirdParty;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import com.bhartipay.util.thirdParty.goAllTrip.bean.AvailabilityInput;
import com.bhartipay.util.thirdParty.goAllTrip.bean.FlightAvailability;
import com.bhartipay.util.thirdParty.goAllTrip.bean.JourneyDetail;
import com.google.gson.Gson;


public class GoalltripTravelImplementation {
	
	private static final Logger logger = Logger.getLogger(GoalltripTravelImplementation.class.getName());
	String apiGatkey="powerpayindia5238823389@in55sanapatel";
	private String getAirportNameCodeURL="https://www.goalltrip.com/api/test/Flight/" ;
	
	public String getAirportNameCode(){
		logger.info("Start excution ********************getAirportNameCode*************************************** " );
		String airportName = executeGetURL(getAirportNameCodeURL.trim()+"get_airport?gatkey="+apiGatkey,"");
		logger.info("end excution ********************getAirportNameCode*************************************** "+airportName );
		return airportName;
		}
	
	
	public String getFlightName(){
		logger.info("Start excution ********************getFlightName*************************************** " );
		String flightName = executeGetURL(getAirportNameCodeURL.trim()+"get_flight_name_images?gatkey="+apiGatkey,"");
		logger.info("end excution ********************getFlightName*************************************** "+flightName );
		return flightName;
		}
	
	public String getFlightCommission(){
		logger.info("Start excution ********************getFlightCommission*************************************** " );
		String flightCommission = executeGetURL(getAirportNameCodeURL.trim()+"get_flight_commission?gatkey="+apiGatkey,"");
		logger.info("end excution ********************getFlightCommission*************************************** "+flightCommission );
		return flightCommission;
		}
	
	
	
	public String getFlightAvailability(FlightAvailability flightAvailability){
		logger.info("Start excution ********************getFlightAvailability*************************************** " );
		Gson gson = new Gson();
		String aa= gson.toJson(flightAvailability).toString();
		logger.info("Start excution ********************aa*************************************** "+aa );
		String flightAvailabilitys = executeGetURL(getAirportNameCodeURL.trim()+"availability?gatkey="+apiGatkey,aa);
		logger.info("Start excution ********************flightAvailability*************************************** "+flightAvailabilitys );
		
		return null;
	}
	
	
	
	
	
	public static String executeGetURL(String url,String jsonData) {

		HttpsURLConnection con=null; 
		String responseString="Fail";
	    try{
	    		    	
	    	URL obj = new URL(url);
			con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setDoOutput(true);
					
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			
			if(jsonData!=null && jsonData.length()>1){
				wr.writeBytes(jsonData);	
			}
			
			logger.info("\nSending 'Get' request to URL : " + url);
			logger.info("get jsonData : " + jsonData);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			
			logger.info("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			logger.debug("getOfflineStores Response************************************"+response.toString());
			responseString =response.toString();
		  } catch (Exception e) {
			  e.printStackTrace();
	      System.err.println(e);
	    } finally {
	    	con.disconnect();
	    }
	    return responseString;

	  }	


	
	public static void main(String[] args) {
		new GoalltripTravelImplementation().getAirportNameCode();
		
		FlightAvailability flightAvailability=new FlightAvailability();
		AvailabilityInput availabilityInput=new AvailabilityInput();
		JourneyDetail journeyDetail=null;
		availabilityInput.setBookingType("O");
		journeyDetail=new JourneyDetail();
		journeyDetail.setOrigin("BLR");
		journeyDetail.setDestination("GOI");
		journeyDetail.setTravelDate("04/23/2017");
		List <JourneyDetail> journeyDetails=new ArrayList <JourneyDetail>();
		journeyDetails.add(journeyDetail);
		availabilityInput.setJourneyDetails(journeyDetails);
		availabilityInput.setClassType("Economy");
		availabilityInput.setAirlineCode("");
		availabilityInput.setAdultCount("1");
		availabilityInput.setChildCount("0");
		availabilityInput.setInfantCount("0");
		availabilityInput.setResidentofIndia("1");
		availabilityInput.setOptional1("0");
		availabilityInput.setOptional2("0");
		availabilityInput.setOptional3("3");
		flightAvailability.setAvailabilityInput(availabilityInput);
		new GoalltripTravelImplementation().getFlightAvailability(flightAvailability);
		
		
		
		
		
		
	}

}

