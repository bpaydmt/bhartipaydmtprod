package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class MyPayStoreService 
{
	Logger LOGGER = Logger.getLogger(this.getClass());
	
	
	public String requestRecharge(String userName,String _urlenc,int encrpted,String url,String mobile)
	{
		String responseString = "FAIL";
		LOGGER.info("starting requestRecharge execution with mobile "+mobile+" has  URL : "+url);
		HttpURLConnection httpsCon   = null;
		String requestString = createUrl(userName, _urlenc, encrpted);
		String endPoint = url+"?"+requestString;
		LOGGER.info("starting requestRecharge execution with mobile "+mobile+" has end URL : "+endPoint);
		try
		{
			LOGGER.info("starting requestRecharge execution with mobile "+mobile+" has request string : "+requestString);
			URL urlPt = new URL(endPoint);
		    httpsCon = (HttpURLConnection) urlPt.openConnection();
	
			httpsCon.setRequestMethod("GET");
			httpsCon.setDoOutput(true);
			httpsCon.setRequestProperty("Content-Type", "text/xml");

			int responseCode = httpsCon.getResponseCode();
			LOGGER.info("requestRecharge  mobile "+mobile+" has  ResponseCode : "+responseCode);
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(httpsCon.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			responseString = response.toString();
			in.close();

			//print result
			LOGGER.info("requestRecharge  mobile "+mobile+" has  Response String  : "+responseString);
			
		}
		catch(Exception ex)
		{
			LOGGER.info("Error occured while executing requestRecharge "+ ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			httpsCon.disconnect();
		}
		return responseString;
	}
	
	private String createUrl(String userName,String _urlenc,int encrpted)
	{
		String queryString ="_prcsr="+userName+"&_urlenc="+_urlenc+"&_encuse=0";
		return queryString;
	}
	
	public static void main(String[] args) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("MM.dd.yyyy%20hh:mm:ss");
		String url = "http://www.mypaystore.in/_API/_APIPROCS.ASPX";
		String messsageString = "PAT106|9717885863|10|1|0|"+df.format(date);//"_prcsr=7011605528&_urlenc=(PAT106)|(9810471689)|(10)|(1)|(0)|("+df.format(date)+")&_encuse=(0)";
		new MyPayStoreService().requestRecharge("7011605528",messsageString,0, url, "9810471689");
		
	}
}
