package com.bhartipay.util.thirdParty.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="finoreq")
public class FINORequestBean 
{
	@Id
	@Column(name="reqid",length=15)
	private String reqId;
	
	@Column(name="clientuniqueid")
	@SerializedName("ClientUniqueID")
	private String clientUniqueID;
	
	@Column(name="customermobileno")
	@SerializedName("CustomerMobileNo")
	private String customerMobileNo;
	
	@Column(name="beneifsccode")
	@SerializedName("BeneIFSCCode")
	private String beneIFSCCode;
	
	@Column(name="beneaccountno")
	@SerializedName("BeneAccountNo")
	private String beneAccountNo;
	
	@Column(name="benename")
	@SerializedName("BeneName")
	private String beneName;
	
	@Column(name="amount")
	@SerializedName("Amount")
	private double amount;
	
	@Column(name="customername")
	@SerializedName("CustomerName")
    private String customerName;
	
	@Column(name="rfu1")
	@SerializedName("RFU1")
	private String rFU1;
	
	@Column(name="rfu2")
	@SerializedName("RFU2")
	private String rFU2;
	
	@Column(name="rfu3")
	@SerializedName("RFU3")
	private String rFU3;

	@Column(name="productcode")
	@SerializedName("ProductCode")
    private String productCode;


	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getClientUniqueID() {
		return clientUniqueID;
	}

	public void setClientUniqueID(String clientUniqueID) {
		this.clientUniqueID = clientUniqueID;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBeneIFSCCode() {
		return beneIFSCCode;
	}

	public void setBeneIFSCCode(String beneIFSCCode) {
		this.beneIFSCCode = beneIFSCCode;
	}

	public String getBeneAccountNo() {
		return beneAccountNo;
	}

	public void setBeneAccountNo(String beneAccountNo) {
		this.beneAccountNo = beneAccountNo;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getrFU1() {
		return rFU1;
	}

	public void setrFU1(String rFU1) {
		this.rFU1 = rFU1;
	}

	public String getrFU2() {
		return rFU2;
	}

	public void setrFU2(String rFU2) {
		this.rFU2 = rFU2;
	}

	public String getrFU3() {
		return rFU3;
	}

	public void setrFU3(String rFU3) {
		this.rFU3 = rFU3;
	}
	public String getProductCode() {
           return productCode;
        }
        public void setProductCode(String productCode) {
           this.productCode=productCode;
        }	
	
}
