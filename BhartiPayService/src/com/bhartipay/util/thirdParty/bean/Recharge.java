package com.bhartipay.util.thirdParty.bean;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recharge {

	@SerializedName("AgentId")
	@Expose
	private String agentId;
	@SerializedName("PaycTransId")
	@Expose
	private String paycTransId;
	@SerializedName("TransDate")
	@Expose
	private String transDate;
	@SerializedName("Amount")
	@Expose
	private String amount;
	@SerializedName("TopupCharge")
	@Expose
	private String topupCharge;
	@SerializedName("Product")
	@Expose
	private String product;
	@SerializedName("Status")
	@Expose
	private String status;
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getPaycTransId() {
		return paycTransId;
	}
	public void setPaycTransId(String paycTransId) {
		this.paycTransId = paycTransId;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTopupCharge() {
		return topupCharge;
	}
	public void setTopupCharge(String topupCharge) {
		this.topupCharge = topupCharge;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
