
package com.bhartipay.util.thirdParty.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

	@SerializedName("CardExists")
	@Expose
	private String cardExists;

	@SerializedName("RequestNo")
	@Expose
	private String RequestNo;

	@SerializedName("CardDetail")
	@Expose
	private CardDetail cardDetail;
	@SerializedName("Beneficiary")
	@Expose
	private Beneficiary beneficiary;
	@SerializedName("Response")
	@Expose
	private String response;
	@SerializedName("Message")
	@Expose
	private String message;
	@SerializedName("Code")
	@Expose
	private String code;

	@SerializedName("MobileNo")
	@Expose
	private String MobileNo;

	@SerializedName("Balance")
	@Expose
	private double Balance;
	
	@SerializedName("TransDetails")
	@Expose
	private List<TransDetail> transDetails = new ArrayList<TransDetail>();
	
	@SerializedName("Count")
	@Expose
	private int Count;
	
	
	@SerializedName("MoneyRemittance")
	@Expose
	private MoneyRemittance moneyRemittance;

	@SerializedName("BankDetail")
	@Expose
	private BankDetail bankDetail;
	
	@SerializedName("Recharge")
	@Expose
	private Recharge recharge;
	
	@SerializedName("AgentTransId")
	@Expose
	private String agentTransId;
	
	
	@SerializedName("Status")
	@Expose
	private String Status;
	
	@SerializedName("Amount")
	@Expose
	private String Amount;
	
	@SerializedName("TransDateTime")
	@Expose
	private String TransDateTime;
	
	
	
	
	
	
	
	
	
	

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getTransDateTime() {
		return TransDateTime;
	}

	public void setTransDateTime(String transDateTime) {
		TransDateTime = transDateTime;
	}

	public String getCardExists() {
		return cardExists;
	}

	public void setCardExists(String cardExists) {
		this.cardExists = cardExists;
	}

	public String getRequestNo() {
		return RequestNo;
	}

	public void setRequestNo(String requestNo) {
		RequestNo = requestNo;
	}

	public CardDetail getCardDetail() {
		return cardDetail;
	}

	public void setCardDetail(CardDetail cardDetail) {
		this.cardDetail = cardDetail;
	}

	public Beneficiary getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(Beneficiary beneficiary) {
		this.beneficiary = beneficiary;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public double getBalance() {
		return Balance;
	}

	public void setBalance(double balance) {
		Balance = balance;
	}

	public List<TransDetail> getTransDetails() {
		return transDetails;
	}

	public void setTransDetails(List<TransDetail> transDetails) {
		this.transDetails = transDetails;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public MoneyRemittance getMoneyRemittance() {
		return moneyRemittance;
	}

	public void setMoneyRemittance(MoneyRemittance moneyRemittance) {
		this.moneyRemittance = moneyRemittance;
	}

	public BankDetail getBankDetail() {
		return bankDetail;
	}

	public void setBankDetail(BankDetail bankDetail) {
		this.bankDetail = bankDetail;
	}

	public Recharge getRecharge() {
		return recharge;
	}

	public void setRecharge(Recharge recharge) {
		this.recharge = recharge;
	}

	public String getAgentTransId() {
		return agentTransId;
	}

	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}
	
	
	




}
