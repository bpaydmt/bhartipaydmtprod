package com.bhartipay.util.thirdParty.bean;

public class WalletExist {
	
	
	  private String RequestNo;
	  private String Response;// STRING Y SUCCESS/ERROR
	  private String CardExists;// STRING Y Y If wallet exist else N
	  private String  Message;// STRING Y Response message
	  private String Code;// SUCCESS: 300 FAILED: 1
	  private String Balance;
	  private String RemitLimitAvailable;
	  private String Mobileno;
	  private String  OtpVerify;
	 
	public String getRequestNo() {
		return RequestNo;
	}
	public void setRequestNo(String requestNo) {
		RequestNo = requestNo;
	}
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public String getCardExists() {
		return CardExists;
	}
	public void setCardExists(String cardExists) {
		CardExists = cardExists;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getBalance() {
		return Balance;
	}
	public void setBalance(String balance) {
		Balance = balance;
	}
	public String getRemitLimitAvailable() {
		return RemitLimitAvailable;
	}
	public void setRemitLimitAvailable(String remitLimitAvailable) {
		RemitLimitAvailable = remitLimitAvailable;
	}
	public String getMobileno() {
		return Mobileno;
	}
	public void setMobileno(String mobileno) {
		Mobileno = mobileno;
	}
	public String getOtpVerify() {
		return OtpVerify;
	}
	public void setOtpVerify(String otpVerify) {
		OtpVerify = otpVerify;
	}
	
	  
	  
	  

}
