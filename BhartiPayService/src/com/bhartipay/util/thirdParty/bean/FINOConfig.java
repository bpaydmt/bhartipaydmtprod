package com.bhartipay.util.thirdParty.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="finoconfig")
public class FINOConfig
{
	@Id
	@Column(name="id",length=20)
	private String id;
	
	@Column(name="clientid",length=20)
	private String clientId;
	
	@Column(name="authkey",length=20)
	private String authKey;
	
	@Column(name="headerkey",length=20)
	private String headerEncKey;
	
	@Column(name="bodykey",length=20)
	private String bodyEncKey;
	
	
	@Column(name="impsurl",length=20)
	private String impsUrl;
	
	
	@Column(name="nefturl",length=20)
	private String neftUrl;
	
	@Column(name="statusurl",length=20)
	private String statusEnquiryUrl;
	
	@Column(name="balenqurl",length=20)
	private String balanceEnquiryUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getHeaderEncKey() {
		return headerEncKey;
	}

	public void setHeaderEncKey(String headerEncKey) {
		this.headerEncKey = headerEncKey;
	}

	public String getBodyEncKey() {
		return bodyEncKey;
	}

	public void setBodyEncKey(String bodyEncKey) {
		this.bodyEncKey = bodyEncKey;
	}

	public String getImpsUrl() {
		return impsUrl;
	}

	public void setImpsUrl(String impsUrl) {
		this.impsUrl = impsUrl;
	}

	public String getNeftUrl() {
		return neftUrl;
	}

	public void setNeftUrl(String neftUrl) {
		this.neftUrl = neftUrl;
	}

	public String getStatusEnquiryUrl() {
		return statusEnquiryUrl;
	}

	public void setStatusEnquiryUrl(String statusEnquiryUrl) {
		this.statusEnquiryUrl = statusEnquiryUrl;
	}

	public String getBalanceEnquiryUrl() {
		return balanceEnquiryUrl;
	}

	public void setBalanceEnquiryUrl(String balanceEnquiryUrl) {
		this.balanceEnquiryUrl = balanceEnquiryUrl;
	}
	
	
}
