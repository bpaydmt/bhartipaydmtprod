package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

@SerializedName("text")
@Expose
private String text;
@SerializedName("is_active")
@Expose
private Boolean isActive;

/**
* 
* @return
* The text
*/
public String getText() {
return text;
}

/**
* 
* @param text
* The text
*/
public void setText(String text) {
this.text = text;
}

/**
* 
* @return
* The isActive
*/
public Boolean getIsActive() {
return isActive;
}

/**
* 
* @param isActive
* The is_active
*/
public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

}