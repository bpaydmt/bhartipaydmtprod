package com.bhartipay.util.thirdParty.bean;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MatchMoveBean {
	
	@SerializedName("id")
	@Expose
	private String id;
	
	@SerializedName("status")
	@Expose
	private Status status;
	
	@SerializedName("blockedStatus")
	@Expose
	private String blockedStatus;
	
	
	@SerializedName("email")
	@Expose
	private String email;
	
	@SerializedName("name")
	@Expose
	private Name name;
	
	@SerializedName("date")
	@Expose
	private Date date;
	@SerializedName("mobile")
	@Expose
	private Mobile mobile;
	
	@SerializedName("error")
	@Expose
	private String error;

	

	@SerializedName("holder")
	@Expose
	private Holder holder;
	@SerializedName("funds")
	@Expose
	private Funds funds;
	
	@SerializedName("image")
	@Expose
	private Image image;
	@SerializedName("links")
	@Expose
	private List<Link> links = null;
	@SerializedName("number")
	@Expose
	private String number;

	@SerializedName("type")
	@Expose
	private Type type;
	
	
	
	
	
	@SerializedName("description")
	@Expose
	private String description;

	
	@SerializedName("cardname")
	@Expose
	private String cardName;
	
	
	@SerializedName("cardNumber")
	@Expose
	private String cardNumber;
	
	@SerializedName("holderName")
	@Expose
	private String holderName;
	
	@SerializedName("expiry")
	@Expose
	private String expiry;
	
	
	
	@SerializedName("code")
	@Expose
	private String code;
	
	
	@SerializedName("state")
	@Expose
	private String state;
	
	@SerializedName("suspendStatus")
	@Expose
	private String suspendStatus;
	
	
	
	
	
	
	
	
	
	
	
	
	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSuspendStatus() {
		return suspendStatus;
	}

	public void setSuspendStatus(String suspendStatus) {
		this.suspendStatus = suspendStatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBlockedStatus() {
		return blockedStatus;
	}

	public void setBlockedStatus(String blockedStatus) {
		this.blockedStatus = blockedStatus;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Funds getFunds() {
		return funds;
	}

	public void setFunds(Funds funds) {
		this.funds = funds;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	* 
	* @return
	* The id
	*/
	public String getId() {
	return id;
	}

	/**
	* 
	* @param id
	* The id
	*/
	public void setId(String id) {
	this.id = id;
	}

	/**
	* 
	* @return
	* The status
	*/
	public Status getStatus() {
	return status;
	}

	/**
	* 
	* @param status
	* The status
	*/
	public void setStatus(Status status) {
	this.status = status;
	}

	/**
	* 
	* @return
	* The email
	*/
	public String getEmail() {
	return email;
	}

	/**
	* 
	* @param email
	* The email
	*/
	public void setEmail(String email) {
	this.email = email;
	}

	/**
	* 
	* @return
	* The name
	*/
	public Name getName() {
	return name;
	}

	/**
	* 
	* @param name
	* The name
	*/
	public void setName(Name name) {
	this.name = name;
	}

	/**
	* 
	* @return
	* The date
	*/
	public Date getDate() {
	return date;
	}

	/**
	* 
	* @param date
	* The date
	*/
	public void setDate(Date date) {
	this.date = date;
	}

	/**
	* 
	* @return
	* The mobile
	*/
	public Mobile getMobile() {
	return mobile;
	}

	/**
	* 
	* @param mobile
	* The mobile
	*/
	public void setMobile(Mobile mobile) {
	this.mobile = mobile;
	}

	/**
	 * 
	 * @return
	 * The error
	 */
	public String getError() {
		return error;
	}

	/**
	 * 
	 * @param error
	 * @return
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	
	
}
