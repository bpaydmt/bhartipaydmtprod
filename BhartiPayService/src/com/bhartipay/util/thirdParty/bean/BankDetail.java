package com.bhartipay.util.thirdParty.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class BankDetail {

	@SerializedName("BankName")
	@Expose
	private String bankName;
	@SerializedName("BranchName")
	@Expose
	private String branchName;
	@SerializedName("Address")
	@Expose
	private String address;
	@SerializedName("State")
	@Expose
	private String state;
	@SerializedName("City")
	@Expose
	private String city;
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	
	
}
