package com.bhartipay.util.thirdParty.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date {

	@SerializedName("registration")
	@Expose
	private String registration;

	
	@SerializedName("issued")
	@Expose
	private String issued;
	@SerializedName("expiry")
	@Expose
	private String expiry;
	
	/**
	* 
	* @return
	* The issued
	*/
	public String getIssued() {
	return issued;
	}

	/**
	* 
	* @param issued
	* The issued
	*/
	public void setIssued(String issued) {
	this.issued = issued;
	}

	/**
	* 
	* @return
	* The expiry
	*/
	public String getExpiry() {
	return expiry;
	}

	/**
	* 
	* @param expiry
	* The expiry
	*/
	public void setExpiry(String expiry) {
	this.expiry = expiry;
	}
	
	/**
	* 
	* @return
	* The registration
	*/
	public String getRegistration() {
	return registration;
	}

	/**
	* 
	* @param registration
	* The registration
	*/
	public void setRegistration(String registration) {
	this.registration = registration;
	}
}
