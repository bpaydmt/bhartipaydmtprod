package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Name {

@SerializedName("last")
@Expose
private String last;
@SerializedName("preferred")
@Expose
private String preferred;
@SerializedName("first")
@Expose
private String first;

/**
* 
* @return
* The last
*/
public String getLast() {
return last;
}

/**
* 
* @param last
* The last
*/
public void setLast(String last) {
this.last = last;
}

/**
* 
* @return
* The preferred
*/
public String getPreferred() {
return preferred;
}

/**
* 
* @param preferred
* The preferred
*/
public void setPreferred(String preferred) {
this.preferred = preferred;
}

/**
* 
* @return
* The first
*/
public String getFirst() {
return first;
}

/**
* 
* @param first
* The first
*/
public void setFirst(String first) {
this.first = first;
}

}