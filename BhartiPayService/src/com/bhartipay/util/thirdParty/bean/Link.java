package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Link {

@SerializedName("method")
@Expose
private String method;
@SerializedName("rel")
@Expose
private String rel;
@SerializedName("href")
@Expose
private String href;

/**
* 
* @return
* The method
*/
public String getMethod() {
return method;
}

/**
* 
* @param method
* The method
*/
public void setMethod(String method) {
this.method = method;
}

/**
* 
* @return
* The rel
*/
public String getRel() {
return rel;
}

/**
* 
* @param rel
* The rel
*/
public void setRel(String rel) {
this.rel = rel;
}

/**
* 
* @return
* The href
*/
public String getHref() {
return href;
}

/**
* 
* @param href
* The href
*/
public void setHref(String href) {
this.href = href;
}

}