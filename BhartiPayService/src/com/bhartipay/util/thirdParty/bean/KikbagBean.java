package com.bhartipay.util.thirdParty.bean;

import java.io.Serializable;

public class KikbagBean implements Serializable {
	
	
	private String cat_name;
	private String city;
	private String retail_id;
	
	
	
	
	public String getCat_name() {
		return cat_name;
	}
	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRetail_id() {
		return retail_id;
	}
	public void setRetail_id(String retail_id) {
		this.retail_id = retail_id;
	}
	
	
	
	

}
