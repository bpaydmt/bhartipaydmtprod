package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Funds {

@SerializedName("withholding")
@Expose
private Withholding withholding;
@SerializedName("available")
@Expose
private Available available;

/**
* 
* @return
* The withholding
*/
public Withholding getWithholding() {
return withholding;
}

/**
* 
* @param withholding
* The withholding
*/
public void setWithholding(Withholding withholding) {
this.withholding = withholding;
}

/**
* 
* @return
* The available
*/
public Available getAvailable() {
return available;
}

/**
* 
* @param available
* The available
*/
public void setAvailable(Available available) {
this.available = available;
}

}