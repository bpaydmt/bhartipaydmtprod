package com.bhartipay.util.thirdParty.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;

import com.bhartipay.mudra.persistence.MudraDaoImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Entity
@Table(name="icici_imps")
public class IMPSParameterBean implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 12345678765432L;

	private static final Logger logger = Logger.getLogger(MudraDaoImpl.class.getName());
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="bene_acc_no",length=50)
	private String beneAccNo;
	
	@Column(name="transaction_date",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date transactionDate;
	
	@Column(name="bene_ifsc",length=50)
	private String beneIFSC;
	
	@Column(name="bene_mobile_no",length=20)
	private String beneMobileNo;

	@Column(name="bene_mmid",length=50)
	private String beneMMID;
	
	@Column(name="tran_ref_no",length=50)
	private String tranRefNo;
	
	@Column(name="amount")
	private double Amount; 
	
	@Column(name="payment_ref",length=50)
	private String paymentRef;
	
	@Column(name="retailer_code",length=50)
	private String retailerCode;
	
	@Column(name="rem_name",length=100)
	private String remName;
	
	@Column(name="rem_mobile",length=20)
	private String remMobile;
	
	@Column(name="rem_mmid",length=50)
	private String remMMID;
	
	@Column(name="timestamp",length=50)
	private String timestamp;
	
	@Column(name="passcode",length=50)
	private String passCode;
	
	@Column(name="delivery_channel",length=50)
	private String deliveryChannel;

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getBeneAccNo() {
		return beneAccNo;
	}

	
	public String getRetailerCode() {
		return retailerCode;
	}


	public void setRetailerCode(String retailerCode) {
		this.retailerCode = retailerCode;
	}


	public String getTranRefNo() {
		return tranRefNo;
	}


	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}


	public void setBeneAccNo(String beneAccNo) {
		this.beneAccNo = beneAccNo;
	}

	public String getBeneIFSC() {
		return beneIFSC;
	}

	public void setBeneIFSC(String beneIFSC) {
		this.beneIFSC = beneIFSC;
	}
	
	
	public String getBeneMobileNo() {
		return beneMobileNo;
	}

	public void setBeneMobileNo(String beneMobileNo) {
		this.beneMobileNo = beneMobileNo;
	}

	public String getBeneMMID() {
		return beneMMID;
	}

	public void setBeneMMID(String beneMMID) {
		this.beneMMID = beneMMID;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}


	public String getPaymentRef() {
		return paymentRef;
	}

	public void setPaymentRef(String paymentRef) {
		this.paymentRef = paymentRef;
	}

	public String getRemName() {
		return remName;
	}

	public void setRemName(String remName) {
		this.remName = remName;
	}

	public String getRemMobile() {
		return remMobile;
	}

	public void setRemMobile(String remMobile) {
		this.remMobile = remMobile;
	}

	public String getRemMMID() {
		return remMMID;
	}

	public void setRemMMID(String remMMID) {
		this.remMMID = remMMID;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public String getPassCode() {
		return passCode;
	}

	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	public String getDeliveryChannel() {
		return deliveryChannel;
	}

	public void setDeliveryChannel(String deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}
	
	public HashMap<String,String> getRequestForFundTransfer(IMPSParameterBean impsParameter)
	{
		String xmlInput = "";
		HashMap<String,String> resp = new HashMap<String,String>();
		try
		{
			
			
			if(impsParameter.getAmount() == 0 )
			{
				resp.put("RespCode","103");
				resp.put("RespString","Amount");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"Amount="+impsParameter.getAmount();
			}
			if(impsParameter.getRemMobile() == null || impsParameter.getRemMobile().trim().equals("") || impsParameter.getRemMobile().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Remitter Mobile Number");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"RemMobile="+impsParameter.getRemMobile();
			}
			if(impsParameter.getPassCode() == null ||impsParameter.getPassCode().trim().equals("") || impsParameter.getPassCode().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Passcode");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"PassCode="+impsParameter.getPassCode();
			}
			if(impsParameter.getTranRefNo() == null ||impsParameter.getTranRefNo().trim().equals("") || impsParameter.getTranRefNo().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Transfer Reference Number");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"TranRefNo="+impsParameter.getTranRefNo();
			}
			if(impsParameter.getPaymentRef() == null ||impsParameter.getPaymentRef().trim().equals("") || impsParameter.getPaymentRef().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Payment Reference");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"PaymentRef="+impsParameter.getPaymentRef();
			}
			/*if(impsParameter.getRetailerCode() == null ||impsParameter.getRetailerCode().trim().equals("") || impsParameter.getRetailerCode().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Retailer Code");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"RetailerCode="+impsParameter.getRetailerCode();
			}*/
			if(impsParameter.getRemName() == null ||impsParameter.getRemName().trim().equals("") || impsParameter.getRemName().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Remitter Name");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"RemName="+impsParameter.getRemName();
			}	
			if(impsParameter.getRemMMID() == null ||impsParameter.getRemMMID().trim().equals("") || impsParameter.getRemMMID().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Remitter MMID");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"RemMMID="+impsParameter.getRemMMID();
			}
			if(impsParameter.getDeliveryChannel() == null ||impsParameter.getDeliveryChannel().trim().equals("") || impsParameter.getDeliveryChannel().isEmpty())
			{
				resp.put("RespCode","103");
				resp.put("RespString","Delivery Channel");
				return resp;
			}
			else
			{
				xmlInput = xmlInput+"&"+"DeliveryChannel="+impsParameter.getDeliveryChannel();
			}
			if(!(impsParameter.getBeneAccNo().trim().equals("") || impsParameter.getBeneAccNo().isEmpty()))
			{
				xmlInput = xmlInput+"&"+"BeneAccNo="+impsParameter.getBeneAccNo();
				if(!(impsParameter.getBeneIFSC().trim().equals("") || impsParameter.getBeneIFSC().isEmpty()))
				{
					xmlInput = xmlInput+"&"+"BeneIFSC="+impsParameter.getBeneIFSC();
				}
				else
				{
					resp.put("RespCode","103");
					resp.put("RespString","Combination of Beneficiary Account Number & Beneficiary IFSC Code");
					return resp;
				}
			}
			else if(!(impsParameter.getBeneMobileNo().trim().equals("") || impsParameter.getBeneMobileNo().isEmpty()))
			{
				xmlInput = "BeneMobileNo="+impsParameter.getBeneMobileNo();
				if(!(impsParameter.getBeneMMID().trim().equals("") || impsParameter.getBeneMMID().isEmpty()))
				{
					xmlInput = xmlInput+"&"+"BeneMMID="+impsParameter.getBeneMMID();	
				}
				else
				{
					resp.put("RespCode","103");
					resp.put("RespString","Combination of Beneficiary Mobile Number & Beneficiary MMID");
					return resp;
				}
			}
			else
			{
				resp.put("RespCode","103");
				resp.put("RespString","Beneficiary Account Number/Beneficiary Mobile Number");
				return resp;
			}
			if(impsParameter.getTimestamp() == null || impsParameter.getTimestamp().trim().isEmpty())
			{
				
				//add date via tomcat server
				Date tomcatDate = new Date();
			   
			    DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			   
			    String formattedDate= dateFormat.format(tomcatDate);
			    
			    //System.out.println("Current time of the day using Date - 12 hour format: " + formattedDate);
				xmlInput = xmlInput+"&"+"Timestamp="+formattedDate;
			}
			else
			{
				xmlInput = xmlInput+"&"+"Timestamp="+impsParameter.getTimestamp();
			}
			resp.put("RespCode","100");
			resp.put("RespString",xmlInput);
			logger.info("ICICI(IMPS) Request string is : "+xmlInput);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("RespCode","103");
			resp.put("RespString","Data not Found");
		}
		return resp;
	}
	
	
}
