package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mobile {

@SerializedName("number")
@Expose
private String number;
@SerializedName("country_code")
@Expose
private String countryCode;

/**
* 
* @return
* The number
*/
public String getNumber() {
return number;
}

/**
* 
* @param number
* The number
*/
public void setNumber(String number) {
this.number = number;
}

/**
* 
* @return
* The countryCode
*/
public String getCountryCode() {
return countryCode;
}

/**
* 
* @param countryCode
* The country_code
*/
public void setCountryCode(String countryCode) {
this.countryCode = countryCode;
}

}