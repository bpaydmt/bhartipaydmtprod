package com.bhartipay.util.thirdParty.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="aadharshilareqresp")
public class AadharShilaRequestResponse 
{
	@Id
	@Column(name="txnid",length=20)
	private String txnId;
	
	@Column(name="requestjson",length=1000)
 	@SerializedName("requestjson")
	private String requestJson;
	
	@Column(name="responsejson",length=1000)
	@SerializedName("responsejson")
	private String responseJson;
	
	@Column(name="errormsg",length=200)
	@SerializedName("errormsg")
	private String errorMsg;
	
	@Column(name="errorcode",length=50)
	@SerializedName("errorcode")
	private String errorCode;

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	
}
