
package com.bhartipay.util.thirdParty.bean;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Beneficiary {

   
    private List<TAG0> tAG0;
    
    @SerializedName("BeneficiaryCode")
    @Expose
    private String beneficiaryCode;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("AccountNo")
    @Expose
    private String accountNo;
    @SerializedName("AccountType")
    @Expose
    private String accountType;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    
    
    
    
    
    
    
    

	public String getBeneficiaryCode() {
		return beneficiaryCode;
	}

	public void setBeneficiaryCode(String beneficiaryCode) {
		this.beneficiaryCode = beneficiaryCode;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getiFSC() {
		return iFSC;
	}

	public void setiFSC(String iFSC) {
		this.iFSC = iFSC;
	}

	public List<TAG0> gettAG0() {
		return tAG0;
	}

	public void settAG0(List<TAG0> tAG0) {
		this.tAG0 = tAG0;
	}

   

}
