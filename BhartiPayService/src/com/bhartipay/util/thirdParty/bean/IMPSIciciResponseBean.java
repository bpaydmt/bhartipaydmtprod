package com.bhartipay.util.thirdParty.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name="icici_imps")
@XmlRootElement(name="ImpsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class IMPSIciciResponseBean 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Transient
	private String bankRespXml;
	
	@Column(name="actcode",length=4)
	@XmlElement(name="ActCode")
	private int actCode;
	
	@Column(name="response")
	@XmlElement(name="Response")
	private String response;
	
	@Transient
	private int responseCode;
	
	@Transient
	private String responseString;

	@Column(name="bene_name",length=50)
	@XmlElement(name="BeneName")
	private String beneName;
	
	@Transient
	private String tranRefNo;
	
	@Column(name="bank_rrn",length=50)
	@XmlElement(name="BankRRN")
	private String bankRRN;
	
	@Transient
	private String paymentRef;
	
	@Transient
	private Date tranDateTime;
	
	@Column(name="tran_amount")
	@XmlElement(name="TranAmount")
	private BigDecimal tranAmount;
	
	@Column(name="bene_mmid",length=50)
	@XmlElement(name="BeneMMID")
	private String beneMMID;
	
	@Column(name="bene_mobile",length=20)
	@XmlElement(name="BeneMobile")
	private String beneMobile;
	
	@Transient
	private String beneAccNo;
	
	@Transient
	private String beneIFSC;
	
	@Transient
	private String remName;
	
	@Transient
	private String remMobile;
	
	@Column(name="retailer_code",length=50)
	@XmlElement(name="RetailerCode")
	private String retailerCode;
	
	@Column(name="validation_summary",length=50)
	@XmlElement(name="ValidationSummary")
	private String validationSummary;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValidationSummary() {
		return validationSummary;
	}

	
	public void setValidationSummary(String validationSummary) {
		this.validationSummary = validationSummary;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getBankRespXml() {
		return bankRespXml;
	}

	public void setBankRespXml(String bankRespXml) {
		this.bankRespXml = bankRespXml;
	}

	public int getActCode() {
		return actCode;
	}

    
	public void setActCode(int actCode) {
		this.actCode = actCode;
	}

	public String getResponse() {
		return response;
	}

	
	public void setResponse(String response) {
		this.response = response;
	}
	
	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}
	
	public String getBeneName() {
		return beneName;
	}

	
	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getTranRefNo() {
		return tranRefNo;
	}

	
	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}

	public String getBankRRN() {
		return bankRRN;
	}

	
	public void setBankRRN(String bankRRN) {
		this.bankRRN = bankRRN;
	}

	public String getPaymentRef() {
		return paymentRef;
	}

	
	public void setPaymentRef(String paymentRef) {
		this.paymentRef = paymentRef;
	}

	public Date getTranDateTime() {
		return tranDateTime;
	}

	public void setTranDateTime(Date tranDateTime) {
		this.tranDateTime = tranDateTime;
	}

	public BigDecimal getTranAmount() {
		return tranAmount;
	}

	
	public void setTranAmount(BigDecimal tranAmount) {
		this.tranAmount = tranAmount;
	}

	public String getBeneMMID() {
		return beneMMID;
	}

	
	public void setBeneMMID(String beneMMID) {
		this.beneMMID = beneMMID;
	}

	public String getBeneMobile() {
		return beneMobile;
	}

	
	public void setBeneMobile(String beneMobile) {
		this.beneMobile = beneMobile;
	}

	public String getBeneAccNo() {
		return beneAccNo;
	}

	
	public void setBeneAccNo(String beneAccNo) {
		this.beneAccNo = beneAccNo;
	}

	public String getBeneIFSC() {
		return beneIFSC;
	}

	
	public void setBeneIFSC(String beneIFSC) {
		this.beneIFSC = beneIFSC;
	}

	public String getRemName() {
		return remName;
	}

	
	public void setRemName(String remName) {
		this.remName = remName;
	}

	public String getRemMobile() {
		return remMobile;
	}

	
	public void setRemMobile(String remMobile) {
		this.remMobile = remMobile;
	}

	public String getRetailerCode() {
		return retailerCode;
	}

	
	public void setRetailerCode(String retailerCode) {
		this.retailerCode = retailerCode;
	}
	
	public HashMap<String,String> returnResposeToUser(HashMap<String,String> responseXML)
	{
		HashMap<String,String> returnResp = new HashMap<String,String>();
			int actCode = Integer.parseInt(responseXML.get("ActCode").trim());
			returnResp.put("ActCode", Integer.toString(actCode));
			switch(actCode)
			{
			case 1 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Invalid Account Number");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 2 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Amount limit exceeded for the customer");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 3 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Frozen Account");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 4 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","NRE Account");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 5 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Closed Account");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 6 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","NET Debit cap exceeded for member bank");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 7 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Transaction not permitted for this account type");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 8 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Transaction limit exceeded for this account type");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 9 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Transaction not allowed as this is non-reloadable card");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 10 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Invalid Bank IFSC/NBIN not registered");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 11 :
				returnResp.put("Response","Original transaction has been Timeout � PPI to initiate VM transaction max 3 times if RC 11/30 is been received otherwise omit VM request. Check final transaction status by initiating query transaction");
				returnResp.put("ResponseString","Time out at NPCI");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 12 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Bank not live on IMPS account / IFSC based fund transfer");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 13 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Invalid amount");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 14 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Duplicate transaction");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 15 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Beneficiary is Merchant");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 16 :
				returnResp.put("Response","Transaction Rejected / Check Validation Summary to know the reason of invalid input parameter value");
				returnResp.put("ResponseString","Format Error");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 17 :
				returnResp.put("Response","Financial P2P/P2A Transaction not found in IMPS");
				returnResp.put("ResponseString","Transaction not found");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 18 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","NPCI/Issuing bank is not connected or down");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 21 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Decline on verification");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 30 :
				returnResp.put("Response","Original transaction has been Timeout � PPI to initiate VM transaction max 3 times if RC 11/30 is been received otherwise omit VM request. Check final transaction status by initiating query transaction");
				returnResp.put("ResponseString","No transaction response from NPCI");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 31 :
				returnResp.put("Response","Original transaction has been Timeout � PPI to perform Query transaction to check final transaction status");
				returnResp.put("ResponseString","No transaction response from CDCI");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 32 :
				returnResp.put("Response","Original transaction has been Timeout � PPI to perform Query transaction to check final transaction status");
				returnResp.put("ResponseString","Time out at ICICI CBS");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 41 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Transaction not processed as client txn date expired");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 51 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Transaction declined by beneficiary bank due to insufficient funds");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 60 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Reason Unknown");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 61 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Reason Unknown");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 62 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Reason Unknown");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 63 :
				returnResp.put("Response","Original transaction has been Timeout � PPI to perform Query transaction to check final transaction status");
				returnResp.put("ResponseString","No response from IMPS switch");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 80 :
				returnResp.put("Response","Duplicate transaction request received from the client (Omit processing). � PPI to perform Query transaction to check final transaction status");
				returnResp.put("ResponseString","Initial request is already been processed");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			case 101 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","IMPS Switch Not reachable");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 403 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Unauthorized API Access");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 901 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Invalid PPI");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 902 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Authentication failed");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			case 903 :
				returnResp.put("Response","Transaction Rejected");
				returnResp.put("ResponseString","Invalid PPI Retailer");
				returnResp.put("ResponseCode", "1");
				return returnResp;
			default :
				returnResp.put("Response","Transaction is in pending");
				returnResp.put("ResponseString","Transaction is in pending");
				returnResp.put("ResponseCode", "0");
				return returnResp;
			}	
	}
}
