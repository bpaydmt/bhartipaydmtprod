package com.bhartipay.util.thirdParty.bean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bhartipay.util.DBUtil;

public class FINOCredentials {

	private static FINOCredentials credentials=null;

	
	public static  String FINO_CLIENT_ID;
	public static  String FINO_AUTH_KEY;
	public static  String FINO_HEADER_ENCRYPTION_KEY;
	public static  String FINO_BODY_ENCRYPTION_KEY;
	public static  String FINO_IMPS_URL;
	public static  String FINO_NEFT_URL;
	public static  String FINO_STATUS_URL;
	public static  String FINO_BALANCE_URL;
	
	private FINOCredentials(){
	  
	}
	public static void setFinoCredentials() {
		if(credentials==null) {
			SessionFactory factory = DBUtil.getSessionFactory();
			Session session=null;
			try {
				session=factory.openSession();
				FINOConfig config=(FINOConfig)session.get(FINOConfig.class,"fino");
				if(config!=null) {
					FINO_CLIENT_ID=config.getClientId();
					FINO_AUTH_KEY=config.getAuthKey();
					FINO_HEADER_ENCRYPTION_KEY=config.getHeaderEncKey();
					FINO_BODY_ENCRYPTION_KEY=config.getBodyEncKey();
					FINO_IMPS_URL=config.getImpsUrl();
					FINO_NEFT_URL=config.getNeftUrl();
					FINO_STATUS_URL=config.getStatusEnquiryUrl();
					FINO_BALANCE_URL=config.getBalanceEnquiryUrl();
				}else {
					System.out.println("______________FINO CREDENTIALS NOT FOUND_______________");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if(session!=null)
				session.close();
			}
		}
	}
}
