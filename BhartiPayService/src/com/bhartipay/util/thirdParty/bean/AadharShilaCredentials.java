package com.bhartipay.util.thirdParty.bean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bhartipay.util.DBUtil;

public class AadharShilaCredentials {

	public static AadharShilaCredentials credentials=null;
	public static String BASE_URL;
	public static String CLIENT_ID;
	public static String CLIENT_KEY;
	
	private AadharShilaCredentials(){
		  
	}
	public static void setAadharShilaCredentials() {
		if(credentials==null) {
			SessionFactory factory = DBUtil.getSessionFactory();
			Session session=null;
			try {
				session=factory.openSession();
				AadharShilaConfig config=(AadharShilaConfig)session.get(AadharShilaConfig.class,"aadharshila");
				if(config!=null) {
					BASE_URL=config.getBaseUrl();
					CLIENT_ID=config.getClientId();
					CLIENT_KEY=config.getClientKey();
				}else {
					System.out.println("______________AADHARSHILA IMPS CREDENTIALS NOT FOUND_______________");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if(session!=null)
				session.close();
			}
		}
	}
	
	
	
}
