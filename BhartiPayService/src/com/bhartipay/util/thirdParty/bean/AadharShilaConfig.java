package com.bhartipay.util.thirdParty.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="aadharshilaconfig")
public class AadharShilaConfig {

	
		@Id
		@Column(name="id",length=15)
		private String id;
		
		@Column(name="baseurl",length=200)
		private String baseUrl;
		
		@Column(name="clientid",length=100)
		private String clientId;
		
		@Column(name="clientkey",length=100)
		private String clientKey;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getBaseUrl() {
			return baseUrl;
		}

		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}

		
		public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public String getClientKey() {
			return clientKey;
		}

		public void setClientKey(String clientKey) {
			this.clientKey = clientKey;
		}
		
		
}
