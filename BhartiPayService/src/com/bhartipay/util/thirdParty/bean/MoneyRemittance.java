package com.bhartipay.util.thirdParty.bean;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class MoneyRemittance {

	@SerializedName("Amount")
	@Expose
	private Integer amount;
	@SerializedName("Charges")
	@Expose
	private String charges;
	@SerializedName("Recharge")
	@Expose
	private Integer recharge;
	@SerializedName("Description")
	@Expose
	private String description;
	@SerializedName("selected_transfertype")
	@Expose
	private String selectedTransfertype;
	@SerializedName("FundTransno")
	@Expose
	private String fundTransno;
	@SerializedName("PaymentId")
	@Expose
	private String paymentId;
	@SerializedName("PaymentStatus")
	@Expose
	private String paymentStatus;
	@SerializedName("IMPSREFNO")
	@Expose
	private String iMPSREFNO;
	@SerializedName("BankTransId")
	@Expose
	private String bankTransId;
	@SerializedName("TransferStatus")
	@Expose
	private String transferStatus;
	@SerializedName("Message")
	@Expose
	private String message;
	@SerializedName("BenefName")
	@Expose
	private String benefName;
	@SerializedName("Remarks")
	@Expose
	private String remarks;
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public Integer getRecharge() {
		return recharge;
	}
	public void setRecharge(Integer recharge) {
		this.recharge = recharge;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSelectedTransfertype() {
		return selectedTransfertype;
	}
	public void setSelectedTransfertype(String selectedTransfertype) {
		this.selectedTransfertype = selectedTransfertype;
	}
	public String getFundTransno() {
		return fundTransno;
	}
	public void setFundTransno(String fundTransno) {
		this.fundTransno = fundTransno;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getiMPSREFNO() {
		return iMPSREFNO;
	}
	public void setiMPSREFNO(String iMPSREFNO) {
		this.iMPSREFNO = iMPSREFNO;
	}
	public String getBankTransId() {
		return bankTransId;
	}
	public void setBankTransId(String bankTransId) {
		this.bankTransId = bankTransId;
	}
	public String getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBenefName() {
		return benefName;
	}
	public void setBenefName(String benefName) {
		this.benefName = benefName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	
	
}
