package com.bhartipay.util.thirdParty.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Holder {

@SerializedName("name")
@Expose
private String name;

/**
* 
* @return
* The name
*/
public String getName() {
return name;
}

/**
* 
* @param name
* The name
*/
public void setName(String name) {
this.name = name;
}

}