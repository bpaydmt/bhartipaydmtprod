package com.bhartipay.util.thirdParty.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="finoimps")
public class FINORespData
{
	@Id
	@Column(name="respid",length=20)
	private String respId;
	
	@Column(name="actcode",length=10)
	@SerializedName("ActCode")
	private String actCode;
	
	@Column(name="txnid",length=12)
	@SerializedName("TxnID")
	private String txnID;
	
	@Column(name="amountrequested",length=10)
	@SerializedName("AmountRequested")
	private double amountRequested;
	
	@Column(name="chargesdeducted",length=10)
	@SerializedName("ChargesDeducted")
	private double chargesDeducted;
	
	@Column(name="totalamount",length=10)
	@SerializedName("TotalAmount")
	private double totalAmount;
	
	@Column(name="benename",length=100)
	@SerializedName("BeneName")
	private String beneName;
	
	@Column(name="rfu1",length=200)
	@SerializedName("Rfu1")
	private String rfu1;
	
	@Column(name="rfu2",length=200)
	@SerializedName("Rfu2")
	private String rfu2;
	
	@Column(name="rfu3",length=200)
	@SerializedName("Rfu3")
	private String rfu3;
	
	@Column(name="transactiondatetime")
	@SerializedName("TransactionDatetime")
	private String transactionDatetime;
	
	@Column(name="txndescription",length=10)
	@SerializedName("TxnDescription")
	private String txnDescription;

	
	public String getRespId() {
		return respId;
	}

	public void setRespId(String respId) {
		this.respId = respId;
	}

	public String getActCode() {
		return actCode;
	}

	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

	public double getAmountRequested() {
		return amountRequested;
	}

	public void setAmountRequested(double amountRequested) {
		this.amountRequested = amountRequested;
	}

	public double getChargesDeducted() {
		return chargesDeducted;
	}

	public void setChargesDeducted(double chargesDeducted) {
		this.chargesDeducted = chargesDeducted;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getRfu1() {
		return rfu1;
	}

	public void setRfu1(String rfu1) {
		this.rfu1 = rfu1;
	}

	public String getRfu2() {
		return rfu2;
	}

	public void setRfu2(String rfu2) {
		this.rfu2 = rfu2;
	}

	public String getRfu3() {
		return rfu3;
	}

	public void setRfu3(String rfu3) {
		this.rfu3 = rfu3;
	}

	public String getTransactionDatetime() {
		return transactionDatetime;
	}

	public void setTransactionDatetime(String transactionDatetime) {
		this.transactionDatetime = transactionDatetime;
	}

	public String getTxnDescription() {
		return txnDescription;
	}

	public void setTxnDescription(String txnDescription) {
		this.txnDescription = txnDescription;
	}

	
}
