package com.bhartipay.util.thirdParty.bean;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class TransDetail {

	/*@SerializedName("AgentTransId")
	@Expose
	private String agentTransId;
	@SerializedName("MrTransId")
	@Expose
	private String mrTransId;
	@SerializedName("TopupTransId")
	@Expose
	private String topupTransId;
	@SerializedName("Status")
	@Expose
	private String status;*/
	
	
	@SerializedName("AgentTransId")
	@Expose
	private String agentTransId;
	@SerializedName("MrTransId")
	@Expose
	private String mrTransId;
	@SerializedName("TopupTransId")
	@Expose
	private String topupTransId;
	@SerializedName("TransDateTime")
	@Expose
	private String transDateTime;
	@SerializedName("Amount")
	@Expose
	private String amount;
	@SerializedName("Status")
	@Expose
	private String status;
	@SerializedName("Reinitiate")
	@Expose
	private String reinitiate;
	@SerializedName("BenefAccNo")
	@Expose
	private String benefAccNo;
	@SerializedName("OriginalTransId")
	@Expose
	private String originalTransId;
	@SerializedName("Remark")
	@Expose
	private String remark;
	
	
	
	public String getAgentTransId() {
		return agentTransId;
	}
	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}
	public String getMrTransId() {
		return mrTransId;
	}
	public void setMrTransId(String mrTransId) {
		this.mrTransId = mrTransId;
	}
	public String getTopupTransId() {
		return topupTransId;
	}
	public void setTopupTransId(String topupTransId) {
		this.topupTransId = topupTransId;
	}
	public String getTransDateTime() {
		return transDateTime;
	}
	public void setTransDateTime(String transDateTime) {
		this.transDateTime = transDateTime;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReinitiate() {
		return reinitiate;
	}
	public void setReinitiate(String reinitiate) {
		this.reinitiate = reinitiate;
	}
	public String getBenefAccNo() {
		return benefAccNo;
	}
	public void setBenefAccNo(String benefAccNo) {
		this.benefAccNo = benefAccNo;
	}
	public String getOriginalTransId() {
		return originalTransId;
	}
	public void setOriginalTransId(String originalTransId) {
		this.originalTransId = originalTransId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

	
	
}
