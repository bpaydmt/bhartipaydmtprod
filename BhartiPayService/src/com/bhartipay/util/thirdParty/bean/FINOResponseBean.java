package com.bhartipay.util.thirdParty.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="finoimps")
public class FINOResponseBean 
{
	@Id
	@Column(name="respid",length=20)
	private String respId;
	
	@Column(name="responsecode",length=5)
 	@SerializedName("ResponseCode")
	private String responseCode;
	
	@Column(name="messagestring",length=200)
	@SerializedName("MessageString")
	private String messageString;
	
	@Column(name="displaymessage",length=200)
	@SerializedName("DisplayMessage")
	private String displayMessage;
	
	@Column(name="requestid",length=50)
	@SerializedName("RequestID")
	private String requestID;
	
	@Column(name="clientuniqueid",length=50)
	@SerializedName("ClientUniqueID")
	private String clientUniqueID;
	
	@Column(name="responsedata",length=2000)
	@SerializedName("ResponseData")
	private String responseData;

	
	public String getRespId() {
		return respId;
	}

	public void setRespId(String respId) {
		this.respId = respId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getMessageString() {
		return messageString;
	}

	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getClientUniqueID() {
		return clientUniqueID;
	}

	public void setClientUniqueID(String clientUniqueID) {
		this.clientUniqueID = clientUniqueID;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}
	
}
