package com.bhartipay.util.thirdParty.bean;

public class DMTBean {
	
	private String	session="";
	private String	pin="";
	private String	otc="";
	private String	fName="";
	private String	routingType="";
	private String	mothersMaidenName="";
	private String	state="";
	private String	benAccount="";
	private String	termId="";
	private String	benMobile="";
	private String	address="";
	private String	birthday="";
	private String	number="";
	private String	gender="";
	private String	otcRefCode="";
	private String	benNick="";
	private String	benIFSC="";
	private String	lName="";
	private String	benName="";
	private String	benCode="";
	private String	type="";
	private String	account="";
	private double	amount;
	private double	amountAll;
	private String  comment="PowerPay";
	
	
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getOtc() {
		return otc;
	}
	public void setOtc(String otc) {
		this.otc = otc;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getRoutingType() {
		return routingType;
	}
	public void setRoutingType(String routingType) {
		this.routingType = routingType;
	}
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getBenAccount() {
		return benAccount;
	}
	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getBenMobile() {
		return benMobile;
	}
	public void setBenMobile(String benMobile) {
		this.benMobile = benMobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOtcRefCode() {
		return otcRefCode;
	}
	public void setOtcRefCode(String otcRefCode) {
		this.otcRefCode = otcRefCode;
	}
	public String getBenNick() {
		return benNick;
	}
	public void setBenNick(String benNick) {
		this.benNick = benNick;
	}
	public String getBenIFSC() {
		return benIFSC;
	}
	public void setBenIFSC(String benIFSC) {
		this.benIFSC = benIFSC;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getBenName() {
		return benName;
	}
	public void setBenName(String benName) {
		this.benName = benName;
	}
	public String getBenCode() {
		return benCode;
	}
	public void setBenCode(String benCode) {
		this.benCode = benCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmountAll() {
		return amountAll;
	}
	public void setAmountAll(double amountAll) {
		this.amountAll = amountAll;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
