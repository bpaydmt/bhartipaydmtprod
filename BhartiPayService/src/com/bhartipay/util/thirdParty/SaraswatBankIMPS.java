package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import com.bhartipay.mudra.persistence.MudraDaoImpl;

public class SaraswatBankIMPS {
	 //final String TEST_URL="https://125.16.108.22/swiftcore/faces/services/Transaction/dmtP2ATransfer/";
	final String TEST_URL="https://125.16.108.22/swiftcore/faces/services/DMT/dmtP2ATransfer/";
	 private static final Logger logger = Logger.getLogger(SaraswatBankIMPS.class.getName());
	 
	 public String callIMPSService(String impsInput){
		 String responseString="Fail";
		 try {
			 
			  SSLContext sc = SSLContext.getInstance("TLS");
			  sc.init(null, trustAllCerts, new SecureRandom());
			  HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			  URL url = new URL(TEST_URL);
				 HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
				 httpsCon.setHostnameVerifier(new HostnameVerifier() {
					
					@Override
					public boolean verify(String hostname, SSLSession session) {
						// TODO Auto-generated method stub
						return true;
					}
				});
				 httpsCon.setRequestMethod("POST");
				 httpsCon.setDoOutput(true);
				 httpsCon.setRequestProperty("Content-Type", "text/xml");
				 DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
				 if(impsInput!=null && impsInput.length()>1){
						wr.writeBytes(impsInput);	
					}
				 logger.info("\nSending 'POST' request to URL : " + url);
				 logger.info("impsInput********** : " + impsInput);
				 wr.flush();
				 wr.close();
				int responseCode = httpsCon.getResponseCode();
				logger.info("responseCode********** : " + responseCode);
				 
				 BufferedReader in = new BufferedReader(
					        new InputStreamReader(httpsCon.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					logger.info("swiftcore Response************************************"+response.toString());
					responseString =response.toString();
						  
			} catch (Exception e) {
			    ;
			}
		 
		 return responseString;
		 
		 
	 }
	 
	  TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
		    public X509Certificate[] getAcceptedIssuers(){return null;}
		    public void checkClientTrusted(X509Certificate[] certs, String authType){}
		    public void checkServerTrusted(X509Certificate[] certs, String authType){}
		}};

	  
	  
	  public String getTransactionStatus(String bankRRN){
		  logger.info("*********************bankRRN***********************"+bankRRN);
		  String responseString="Fail";
		  try {
			String txnStatus="https://125.16.108.22/swiftcore/faces/services/DMT/dmtStatusDetails?param1="+bankRRN+"&param2=DMT" ; 
			  
		  SSLContext sc = SSLContext.getInstance("TLS");
		  sc.init(null, trustAllCerts, new SecureRandom());
		  HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		  URL url = new URL(txnStatus);
			 HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
			 httpsCon.setHostnameVerifier(new HostnameVerifier() {
				
				@Override
				public boolean verify(String hostname, SSLSession session) {
					// TODO Auto-generated method stub
					return true;
				}
			});
			 httpsCon.setRequestMethod("GET");
			 httpsCon.setDoOutput(true);
			 DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
			
			int responseCode = httpsCon.getResponseCode();
			logger.info("responseCode********** : " + responseCode);
			 
			 BufferedReader in = new BufferedReader(
				        new InputStreamReader(httpsCon.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				logger.info("swiftcore Response************************************"+response.toString());
				responseString =response.toString();
			 
			 
			 
		  } catch (Exception e) {
			    ;
			}
		 return responseString; 
	  }
	  
	  
	  
	  public static void main(String[] args) {
		System.out.println("main"+new SaraswatBankIMPS().callIMPSService("<impsTransactionRequest><remAccountNo>143200100201152</remAccountNo><custNo>5863713</custNo><accountNo>7211406164</accountNo><ifscCode>KKBK0000631</ifscCode><transAmt>12</transAmt><source>B</source><benefName>TEST</benefName><benefMobileNo>8983389108</benefMobileNo><narration>DMT test</narration><channel>DMT</channel></impsTransactionRequest>"));
	}
	  
}
