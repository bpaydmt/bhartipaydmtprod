package com.bhartipay.util.thirdParty;


import java.math.BigDecimal;
import java.util.HashMap;


import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.bhartipay.transaction.bean.P2MTransactionBean;
import com.bhartipay.util.XmlParser;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;
import com.bhartipay.util.thirdParty.bean.IMPSParameterBean;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;


public class IMPSImplementation {
	private static final Logger logger = Logger.getLogger(IMPSImplementation.class.getName());
	
	public HashMap callImps(P2MTransactionBean p2MTransactionBean){
		
		logger.info("start excution ===================== method callImps(p2MTransactionBean)"+p2MTransactionBean.impsTxnId);
		HashMap <String, String> impsRespnnce=null;
		try{
			ClientConfig config = new DefaultClientConfig();
			Client client=Client.create(config);
			client.addFilter(new LoggingFilter());
			client.addFilter(new HTTPBasicAuthFilter("testclient", "test@123"));
			WebResource webResource=client.resource("https://uatsky.yesbank.in/app/uat/ssl/Services/json/IMPS/P2M");
		
			JSONObject   requestPayload=new JSONObject ();    
			requestPayload.put("BenefMobileNo",p2MTransactionBean.getBenefMobileNo());   
			requestPayload.put("BenefMMID",p2MTransactionBean.getBenefMMID());
			requestPayload.put("RemitterMobileNo",p2MTransactionBean.getRemitterMobileNo());
			requestPayload.put("RemitterMMID",p2MTransactionBean.getRemitterMMID());
			requestPayload.put("Narration",p2MTransactionBean.getNarration());
			requestPayload.put("TransactionAmount",p2MTransactionBean.getAmount());
			requestPayload.put("OTP",p2MTransactionBean.getOtp());
			
			JSONObject  header=new JSONObject ();    
			header.put("Version","1");    
			header.put("Channel","DigitalWallet");    
			header.put("ExtUniqueRefId",p2MTransactionBean.getImpsTxnId()); 
		
			JSONObject  finalmap=new JSONObject (); 
			finalmap.put("Header", header);
			finalmap.put("RequestPayload", requestPayload);
			String jsonText = JSONValue.toJSONString(finalmap);
			logger.info("start excution ********************************final json string*******************"+jsonText);
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("X-IBM-Client-Id", "d705a9af-a814-4406-b0e0-ba3b42cd81c0").header("X-IBM-Client-Secret", "N8pR2lV6cA7xR3gE0aQ2pD3tH6jN2uR4bS4fG0kG4gQ8hL0yG1").post(ClientResponse.class, jsonText);
			logger.info("start excution ******************************************************response************** "+response.getStatus());
			
			if (response.getStatus() != 200) {
				   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			String output = response.getEntity(String.class);
			logger.info("start excution ******************************************************output************** "+output);
			impsRespnnce=IMPSImplementation.parseResponse(output);
	}catch (Exception e) {
			e.printStackTrace();
		}
	return impsRespnnce;	

}
	
	public static HashMap parseResponse(String output){
		HashMap <String, String> mapResponse=new HashMap<String, String> ();
		 JsonParser parser = new JsonParser();
		  JsonObject jo = (JsonObject) parser.parse(output);
		  JsonElement headerRes = jo.get("Header");
		  JsonElement bodyRes = jo.get("ResponseBody");
		  JsonElement faultRes = jo.get("Fault");
		   System.out.println(headerRes);
		   System.out.println(bodyRes);
		 System.out.println(faultRes);
		   JsonObject headerResObject = headerRes.getAsJsonObject();
		     mapResponse.put("ExtUniqueRefId", headerResObject.get("ExtUniqueRefId").getAsString());
		   if(!(bodyRes==null)){
			   JsonObject bodyResObject = bodyRes.getAsJsonObject();
			   mapResponse.put("Status",  bodyResObject.get("Status").getAsString());
			   mapResponse.put("ReferenceNumber", bodyResObject.get("ReferenceNumber").getAsString());
			   mapResponse.put("ErrorCode", bodyResObject.get("ErrorCode").getAsString());
			}else{
				mapResponse.put("Status",  "Failed");
				mapResponse.put("ReferenceNumber", "");
				JsonObject faultResObject = faultRes.getAsJsonObject();
				String errorType=faultResObject.get("Type").getAsString();
				if(errorType.equals("Server Error")){
					 mapResponse.put("ErrorCode",  faultResObject.get("Code").getAsString());
				 }else{
					 mapResponse.put("ErrorCode",  faultResObject.get("ErrorCode").getAsString());
				}
			}
		  return mapResponse;
	}
	
	HashMap<String,Object> responsemap= new HashMap<String,Object>();
	
	public IMPSIciciResponseBean fundTransferICICIBankIMPS(IMPSParameterBean impsParameter)
	{
		IMPSParameterBean l_impsParameter= new IMPSParameterBean();
		IMPSIciciResponseBean l_impsResponse = new IMPSIciciResponseBean();
		l_impsResponse.setActCode(301);
		l_impsResponse.setResponse("Error");
		l_impsResponse.setResponseString("Transaction Failed");
		try
		{
			HashMap<String,String> request= l_impsParameter.getRequestForFundTransfer(impsParameter);
			if(request.get("RespCode").trim().equals("103"))
			{
				l_impsResponse.setActCode(301);
				l_impsResponse.setResponse("Format Error");
				l_impsResponse.setResponseString("Insufficient input - "+request.get("RespString"));
				logger.info("********************bankRespXml**for money transfer : IMPS ICICI **: "+"Insufficient input - "+request.get("RespString"));
				return l_impsResponse;
			}
			else if(request.get("RespCode").trim().equals("100"))
			{
				//String bankRespXml = "<ImpsResponse> <ActCode>1</ActCode> <Response>An error</Response> <BeneName>Shweta</BeneName > <TranRefNo>abcd123</TranRefNo> <BankRRN>shwe123</BankRRN> </ImpsResponse>"; 
				
				String bankRespXml=new ICICIBankIMPS().callIMPSServicePOST(request.get("RespString"));
				
				l_impsResponse.setBankRespXml(bankRespXml);
				logger.info("********************bankRespXml**for money transfer : IMPS ICICI **:"+bankRespXml);
				HashMap<String, String> bankResp=new XmlParser().xmlParser(bankRespXml);
				l_impsResponse.setBankRespXml(bankRespXml);
				
				HashMap<String,String> errorResp = new HashMap<String,String>();
				
				if(bankResp.get("ActCode").trim().equals("0"))
				{
					/*l_impsResponse.setBankRRN(bankResp.get());
					l_impsResponse.setRemName(bankResp.get());*/
					l_impsResponse.setTranRefNo(bankResp.get(""));
					l_impsResponse.setActCode(0);
					l_impsResponse.setResponse("Success");
					l_impsResponse.setResponseString("Transaction Successful");					
				}
				else
				{
					errorResp=l_impsResponse.returnResposeToUser(bankResp);
					l_impsResponse.setActCode(Integer.parseInt(errorResp.get("ResponseCode").trim()));
					l_impsResponse.setResponse(errorResp.get("Response"));
					l_impsResponse.setResponseString(errorResp.get("ResponseString"));
				}
				logger.info("********************bankRespXml**for money transfer : IMPS ICICI **:"+l_impsResponse.getActCode()+"\n"+l_impsResponse.getResponse()+"\n"+l_impsResponse.getResponseString());
				l_impsResponse.setBeneName(bankResp.get("BeneName"));
				l_impsResponse.setTranRefNo(bankResp.get("TranRefNo"));
				l_impsResponse.setBankRRN(bankResp.get("BankRRN"));
				return l_impsResponse;
			}
		}
		catch(Exception e)
		{
			logger.info("**********************Problem in fund transfer : IMPS ICICI **:"+e.getMessage());
			l_impsResponse.setActCode(301);
			l_impsResponse.setResponse("Transaction Rejected");
			l_impsResponse.setResponseString("");
			e.printStackTrace();
		}
		return l_impsResponse;
		
	}
	
	public static void main(String arg[])
	{
		IMPSParameterBean inputObj= new IMPSParameterBean();
		inputObj.setBeneAccNo("111234567890");
		inputObj.setBeneIFSC("DLXB0000092");
		inputObj.setAmount(71.88);
		inputObj.setTranRefNo("2016110314043455556156");
		inputObj.setPaymentRef("FTTransferP2A");
		inputObj.setRemName("Reliance Retail Limited");
		inputObj.setRetailerCode("ibcngp00069");
		inputObj.setRemMobile("9860001122");
		inputObj.setRemMMID("8305000");
		inputObj.setPassCode("3761b43b1d974588b76d27aae1e25b1b");
		inputObj.setDeliveryChannel("Internet");
		new IMPSImplementation().fundTransferICICIBankIMPS(inputObj);
		
	}

	
}
