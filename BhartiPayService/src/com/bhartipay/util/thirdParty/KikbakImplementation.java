package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import com.bhartipay.util.thirdParty.bean.KikbagBean;
import com.google.gson.JsonObject;

import java.net.HttpURLConnection;

public class KikbakImplementation {
	
	private static final Logger logger = Logger.getLogger(KikbakImplementation.class.getName());
	private String cityURL="http://dhanushshetty.com/kikbakmobileapplication/getcity.php" ;
	private String categoryURL="http://dhanushshetty.com/kikbakmobileapplication/getcategory.php" ;
	private String offlineStoresURL=" http://dhanushshetty.com/kikbakmobileapplication/getofflinestores.php" ;
	private String retailidURL="http://dhanushshetty.com/kikbakmobileapplication/getaddress.php" ;

	
	
	public String getCityCategory(){
		logger.info("Start excution ********************getCityCategory*************************************** " );
		String city = executeURL(cityURL.trim());
		logger.info(" ********************city*************************************** "+city );
		String Category = executeURL(categoryURL.trim());
		logger.info(" ********************Category*************************************** "+Category );
		JsonObject json=new JsonObject();
		json.addProperty("city", city);
		json.addProperty("Category", Category);
		return json.toString();
		
	}
		
	
	/*public String getOfflineStores(KikbagBean kikbagBean){
		logger.info("Start excution ********************getOfflineStores********************City Name******************* " +kikbagBean.getCity());
		logger.info("Start excution ********************getOfflineStores********************Cat_name******************* " +kikbagBean.getCat_name());
		JsonObject json=new JsonObject();
		String param = "?cat_name="+kikbagBean.getCat_name()+"&City="+kikbagBean.getCity();
		offlineStoresURL = offlineStoresURL.concat(param);
		 logger.info("Start excution ********************getOfflineStores***************offlineStoresURL******** " +offlineStoresURL);
		 URL url;
		try {
			url = new URL(offlineStoresURL);
			HttpURLConnection conn =(HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.connect();
					BufferedReader rd = new BufferedReader(new
					InputStreamReader(conn.getInputStream()));
					String line;
					StringBuffer buffer = new StringBuffer();
					while ((line = rd.readLine()) != null){
					buffer.append(line).append("\n");
					}
					logger.debug("getOfflineStores LOG************************************"+buffer.toString());
					String offlineStores =buffer.toString();
					rd.close();
					conn.disconnect();
					 json.addProperty("offlineStores", offlineStores);
		} catch (Exception e) {
			logger.debug("problem in getOfflineStores=========KikbakImplementation=========" + e.getMessage(), e);
		}
		 return json.toString();
	}*/

	
	
	
	public String getOfflineStores(KikbagBean kikbagBean){
		
		logger.info("Start excution ********************getOfflineStores********************City Name******************* " +kikbagBean.getCity());
		logger.info("Start excution ********************getOfflineStores********************Cat_name******************* " +kikbagBean.getCat_name());
		JsonObject json=new JsonObject();
		
		try {
			String param = "cat_name="+URLEncoder.encode(kikbagBean.getCat_name(), "UTF-8")+"&City="+kikbagBean.getCity();
			logger.info("Start excution ********************getOfflineStores***************offlineStoresURL******** " +offlineStoresURL);
			URL obj = new URL(offlineStoresURL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(param);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			logger.info("\nSending 'POST' request to URL : " + offlineStoresURL);
			logger.info("Post parameters : " + param);
			logger.info("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			logger.debug("getOfflineStores Response************************************"+response.toString());
			String offlineStores =response.toString();
			con.disconnect();
			
			json.addProperty("offlineStores", offlineStores);
			json.addProperty("Category", kikbagBean.getCat_name());
			
		} catch (Exception e) {
			logger.debug("problem in getOfflineStores=========KikbakImplementation=========" + e.getMessage(), e);
		}
		 return json.toString();
	}
	
	
	public String getretailAddress(KikbagBean kikbagBean){
		logger.info("Start excution ********************getretailAddress********************retail Id******************* " +kikbagBean.getRetail_id());
		JsonObject json=new JsonObject();
		
		try {
			String param = "retailid="+URLEncoder.encode(kikbagBean.getRetail_id(), "UTF-8");
			logger.info("Start excution ********************getretailAddress***************retailidURL******** " +retailidURL);
			URL obj = new URL(retailidURL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(param);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			logger.info("\nSending 'POST' request to URL : " + retailidURL);
			logger.info("Post parameters : " + param);
			logger.info("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug("getretailAddress Response************************************"+response.toString());
			String retailAddress =response.toString();
			con.disconnect();
			json.addProperty("retailAddress", retailAddress);
			
		} catch (Exception e) {
			logger.debug("problem in getOfflineStores=========KikbakImplementation=========" + e.getMessage(), e);
		}
		 return json.toString();
	}
	
	

		public static String executeURL(String url) {
		    HttpClient client = new HttpClient();
		    client.getParams().setParameter("http.useragent", "Test Client");
		    BufferedReader br = null;
		    String responseString="";
		    PostMethod method = new PostMethod(url);
	    try{
		      int returnCode = client.executeMethod(method);
		      if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
		        logger.info("The Post method is not implemented by this URI");
		        responseString= method.getResponseBodyAsString();
		      } else {
		        br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
		        responseString=br.readLine();
		        logger.info("***********************************************************returnCode*******"+returnCode);
		        logger.info("***********************************************************responseString*******"+responseString);
		       		      }
		    } catch (Exception e) {
		    	logger.debug("problem in executeURL=========KikbakImplementation=========" + e.getMessage(), e);
		    } finally {
		      method.releaseConnection();
		      if(br != null) try { br.close(); } catch (Exception fe) {}
		    }
		    return responseString;
		  }	
		
	public static void main(String[] args) {
		//new KikbakImplementation().getCityCategory();
		KikbagBean kikbagBean=new KikbagBean ();
		kikbagBean.setCat_name("Cafe & Restaurant");
		kikbagBean.setCity("3");
		kikbagBean.setRetail_id("2198");
		new KikbakImplementation().getretailAddress(kikbagBean);
	}	

}
