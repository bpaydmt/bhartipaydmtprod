package com.bhartipay.util.thirdParty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;

import org.apache.log4j.Logger;
import org.apache.log4j.chainsaw.Main;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.bean.CouponsBean;
import com.google.gson.Gson;
//import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;

public class CouponsImplementation {
	private static final Logger logger = Logger.getLogger(CouponsImplementation.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	
	public String getCoupons(){
		
		Gson gson = new Gson();
		String rest="";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String result="";
		 try{
			 
		 URL url = new URL("https://tools.vcommission.com/api/coupons.php?apikey=93f099e30a2d1dfe6ee5ab3517493855cf188557ffbf7524b0992f31b24f8df8");
		 HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
		 urlconnection.setRequestMethod("POST");
		 urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		 urlconnection.setDoOutput(true);
		 OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
		   // outa.write(postData);
		 outa.close();
		 BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
		 String decodedString;
		 while ((decodedString = in.readLine()) != null) {
			 logger.info("decoded CouponsString~~~*******************************~~~~```"+decodedString);
		 rest=decodedString;
		  }
		    in.close();
		    String output=rest;
		    //String output = response.getEntity(String.class);  
		 List<CouponsBean> list = gson.fromJson(output,OTOList6.class);
		 if(!list.isEmpty()){
			 Transaction  transaction=session.beginTransaction();
			 Query delQuery= session.createQuery("delete CouponsBean");
			 int delQueryResult = delQuery.executeUpdate();
			 logger.info("***************************coupon data deleted ***************"+delQueryResult);
			 Iterator<CouponsBean> iterator = list.iterator();
				while (iterator.hasNext()) {
					session.save(iterator.next());
					}
				
				SQLQuery query = session.createSQLQuery("UPDATE couponsmast SET category=REPLACE(category, '&', '-')");
				query.executeUpdate();
				transaction.commit(); 
		 }
		 
		 result="Fetching Coupon completed From Coupon Provider"; 
	 }catch (Exception e) {
		 result=" Problem In fetching Coupon From Coupon Provider"; 
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}
	
	public HashMap<String, Object> getCouponsCategory() {
		logger.info("Start excution ===================== method getCouponsCategory()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		HashMap<String, Object>  categoryCoupons = new HashMap<String, Object>();
		HashMap<String, String> category = new HashMap<String, String>();
		
		try {

			SQLQuery query = session.createSQLQuery(
					"SELECT   category, category FROM couponsmast WHERE category !='' GROUP BY category ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				//System.out.println(row[0].toString());
				category.put(row[0].toString(), row[1].toString());
			}
			Query couQoery=session.createQuery("from CouponsBean");
			couQoery.setMaxResults(30);
			List<CouponsBean>  couponList=couQoery.list();
			
			categoryCoupons.put("category", category);
			categoryCoupons.put("couponList", couponList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("problem in getCouponsCategory==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return categoryCoupons;
		
	}
	
	
	
	
	
	public List<CouponsBean>getCouponsByCategory(String category){
		logger.info("Start excution ===================== method getCouponsByCategory()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<CouponsBean> couponsList=new ArrayList<CouponsBean>();
		try {
			Query catQoery=session.createQuery("from CouponsBean where category=:category ");
			catQoery.setString("category", category);
			couponsList=catQoery.list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("problem in getCouponsByCategory==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return couponsList;
	}
	
	
	
	public String mailCoupan(String userId, String  ids,String rechargeMobile,String rechargeAmount){
		logger.info("Start excution ===================== method mailCoupan(coupanId)"+ids);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String result="";
		List<CouponsBean> couponsList=new ArrayList<CouponsBean>();
		CouponsBean CouponsBeanmail=null;
		try {
			String [] aa=ids.split(",");
			ArrayList<Integer> numbers = new ArrayList<Integer>();
			for(int i = 0;i < aa.length;i++)
			{
				numbers.add( Integer.parseInt(aa[i]));
			}
			Query catQoery=session.createQuery("from CouponsBean where id IN (:ids) ");
			catQoery.setParameterList("ids", numbers);
			couponsList=catQoery.list();
			//StringBuilder mailString=new StringBuilder("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head><body style='text-align: center;margin: auto'><table width='600px' style='margin: auto;text-align: center;'><tr>	<td style='padding: 10px 0'><img src='http://bhartipay.com/wp-content/uploads/2016/09/logo.png' width='150' height='64' /></td></tr>");
			StringBuilder mailString=new StringBuilder("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head><body style='text-align: center;margin: auto'><table width='600px' style='margin: auto;text-align: center;'><tr>	<td style='padding: 10px 0;text-align:center;font-size:25px'>Coupons Are Ready !</td></tr>");
			
			
			Iterator<CouponsBean> iterator = couponsList.iterator();
			while (iterator.hasNext()) {
				CouponsBeanmail=iterator.next();
				mailString.append("<tr><td style=' padding: 20px 0;   border-bottom: 1px solid #ddd;'><table width='100%' align='center' style='text-align:center;'><tr>	<td style='padding: 10px 0'>");
				mailString.append("<img src='"+CouponsBeanmail.getStoreImage()+"' width='100'>");
				mailString.append("</td></tr><tr><td style='font-size: 20px'>");
				mailString.append(CouponsBeanmail.getCouponTitle());
				mailString.append("</td></tr><tr><td style='font-size:14px; padding:10px 0 0 0;color:#f36f2e;text-align:center;'>");
				mailString.append("Expiry Date :"+CouponsBeanmail.getCouponExpiry());
				mailString.append("</td></tr><tr><td style='font-size: 20px; padding:2px 0 10px 0;text-align:center;color:#f36f2e'>");
				mailString.append("Code :"+CouponsBeanmail.getCouponCode());
				mailString.append("</td></tr></Table></td></tr>");
				
			}
			mailString.append("</table></body></html>");
			WalletUserDao walletUserDao=new WalletUserDaoImpl();
			WalletMastBean walletMastBean=walletUserDao.showUserProfile(userId);
			//String AggId=walletMastBean.getAggreatorid();
			String subject="Recharge of Rs "+rechargeAmount+" for "+rechargeMobile+" is successful.";
			
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailString.toString(),subject, "", "", "Mail",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
//			Thread t = new Thread(smsAndMailUtility);
//			t.start();
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			 result="Coupon Mail Sent."; 
			
			
		} catch (Exception e) {
			result="Error While Sending Mail.";
			e.printStackTrace();
			logger.debug("problem in mailCoupan==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return result;
		
	}
	
	
	public static void main(String[] args) {
		new CouponsImplementation().getCoupons();
		//new CouponsImplementation().mailCoupan("CUST001014","1803,1804,1805,1806","9935041287","20");
	}

}
class OTOList6 extends ArrayList<CouponsBean>{
	  
}
