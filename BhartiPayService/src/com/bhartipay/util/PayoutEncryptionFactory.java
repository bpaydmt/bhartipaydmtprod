package com.bhartipay.util;

public class PayoutEncryptionFactory {
	
	public static PayoutEncryption getEncryptionObject(String encType) {
		if("AES128".equalsIgnoreCase(encType)) {
			return new AES128Bit();
		}else if("AES256".equalsIgnoreCase(encType)){
			return new Encryption256ForPayout();
		}else if("AES128NEW".equalsIgnoreCase(encType)){
			return new AES128BitNew();
		}else {
			return new Encryption256ForPayout();
		}
	}

}
