package com.bhartipay.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.bhartipay.util.CommUtility;

public class OTPGeneration {

	public static String key;
	public static String time;

	public OTPGeneration() {
		try {
			Properties prop = new Properties();
			InputStream in = CommUtility.class.getResourceAsStream("comm.properties");

			prop.load(in);
			in.close();
			key = prop.getProperty("otpKey");
			time = prop.getProperty("otpTime");
			
			System.out.println("___________key________________"+key+"___________time___________"+time);
			
			System.out.println("~~~~~~~~~************~~~~~~~~~~~~" + key);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public  String generateOTP() {
		long l = System.currentTimeMillis();
		String otp1 = OTPGenUtility.generateTOTP(key.getBytes(), (l / 1000), 6, "HmacSHA1");
		return otp1;
	}

	public  boolean validateOTP(String otp) {
		boolean b = false;
		try {
			b = OTPValidateUtility.checkCode(key, otp, 1, Integer.parseInt(time));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

}
