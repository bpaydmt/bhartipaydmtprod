package com.bhartipay.util;

import java.io.InputStream;
import java.util.Properties;


public class SmsTemplates {



	
	public static String regOtp(){
		String regOtp="";
		try{
		Properties prop = new Properties();
		InputStream in = SmsTemplates.class.getResourceAsStream("smstemplates.properties");
		prop.load(in);
		in.close();
		regOtp = prop.getProperty("regOtp");
		}catch(Exception e){
			System.out.println(e);
		}	
		return regOtp;
	}
	
	public static String askMoney(){
		String askmoney="";
		try{
		Properties prop = new Properties();
		InputStream in = SmsTemplates.class.getResourceAsStream("smstemplates.properties");
		prop.load(in);
		in.close();
		askmoney = prop.getProperty("askmoney");
		}catch(Exception e){
			System.out.println(e);
		}	
		return askmoney;
	}
	
	
	public static String forgetPassword(){
		String forgetPassword="";
		try{
		Properties prop = new Properties();
		InputStream in = SmsTemplates.class.getResourceAsStream("smstemplates.properties");
		prop.load(in);
		in.close();
		forgetPassword = prop.getProperty("forgetPassword");
		}catch(Exception e){
			System.out.println(e);
		}	
		return forgetPassword;
	}
	
	
	public static String getKycURL(){
		String kycimageurl="";
		try{
		Properties prop = new Properties();
		InputStream in = SmsTemplates.class.getResourceAsStream("Infrastructure.properties");
		prop.load(in);
		in.close();
		kycimageurl = prop.getProperty("kycimageurl");
		}catch(Exception e){
			System.out.println(e);
		}	
		return kycimageurl;
	}
	

}
