package com.bhartipay.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {
	
	private Pattern pattern;
	private Matcher matcher;

	//private static final String EMAIL_PATTERN ="(^(?=.*[a-z])(?=.*\\d)[A-Za-z\\d$@$!%*?&]{6,}$)";
	//private static final String PASSWORD_PATTERN ="(^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{6,20}$)";
	private static final String PASSWORD_PATTERN ="(^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[!@#$^*]){1,})(?!.*\\s).{8,15}$)";
	
	
	

	public PasswordValidator() {
		pattern = Pattern.compile(PASSWORD_PATTERN);
	}

	public boolean validate(final String hex) {

		matcher = pattern.matcher(hex);
		return matcher.matches();

	}
	
	
public static void main(String[] args) {
	System.out.println(new PasswordValidator().validate("TTTTT12"));
	System.out.println(new PasswordValidator().validate("Ta@a12"));
	System.out.println(new PasswordValidator().validate("aaaadsfsf"));
	System.out.println(new PasswordValidator().validate("aaa"));
	System.out.println(new PasswordValidator().validate("aaa1A@1q"));
}	

}
