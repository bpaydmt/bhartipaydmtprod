package com.bhartipay.util;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class MailContextListener implements ServletContextListener{

	Logger logger = Logger.getLogger(MailContextListener.class.getName());
	Timer t = new Timer();
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		t.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("****************************MailContextListener context Listener start*********************** ");
		long delay=60*1000;
		long period = 1000*60*60*4;
		MailSchedular scheduler = new MailSchedular(period);
		t.scheduleAtFixedRate(scheduler, delay, period);
		logger.info("****************************MailContextListener context Listener Stop*********************** ");
	}
	

}
