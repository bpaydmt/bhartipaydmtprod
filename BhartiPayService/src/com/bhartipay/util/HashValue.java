package com.bhartipay.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;

public class HashValue {


	
	public static void main(String[] args) {
		System.out.println(hashValue(""+123456));
		System.out.println(hashValue(""+654321));
		System.out.println(hashValue(""+123456));
	}
	

	/**
	 * @param args
	 */
	
	public static String hashValue( String arg )   
	{   
		
		String returnValue = null;
	
		
		if (arg == null || StringUtils.isBlank(arg)){
			return null;
		}
		
		MessageDigest md = null;
		
		try {
			md = MessageDigest.getInstance("SHA");
		}
		catch (NoSuchAlgorithmException nsae) {
			returnValue = null;
		}
		
		if (md == null) {
			returnValue = null;
		}
		else {
			try {
				byte[] bytesOfMessage = arg.getBytes("UTF-8");
				md.reset();
				md.update(bytesOfMessage);
			    byte messageDigest[] = md.digest();
			    
			    StringBuffer hexString = new StringBuffer();
			    	for (int i=0;i<messageDigest.length;i++) {
			    		String hex = Integer.toHexString(0xFF & messageDigest[i]); 
			    		if(hex.length()==1){
			    			hexString.append('0');
			    		}
			    		hexString.append(hex);
			    	}
			    returnValue	 = hexString.toString();
			}
			catch (UnsupportedEncodingException uee) {
				returnValue = null;
			}
		}
		return returnValue;
	}

}
