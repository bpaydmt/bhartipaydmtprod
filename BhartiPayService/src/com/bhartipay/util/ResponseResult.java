package com.bhartipay.util;

public class ResponseResult {

	public enum ResponseStatus {
		Success,Fail,Pending,Error;
	}
	public enum StatusCode {
	    SUCCESS(200), 
	    ERROR(422), 
	    NOT_FOUND(404);

	    private int numVal;

	    StatusCode(int numVal) {
	        this.numVal = numVal;
	    }

	    public int getNumVal() {
	        return numVal;
	    }
	}
	
	private StatusCode code;
    private ResponseStatus resStaus;
    private Object data;
	private int status;
	private String message;

	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public StatusCode getCode() {
		return code;
	}
	public void setCode(StatusCode code) {
		this.code = code;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public ResponseStatus getResStaus() {
		return resStaus;
	}
	public void setResStaus(ResponseStatus resStaus) {
		this.resStaus = resStaus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}