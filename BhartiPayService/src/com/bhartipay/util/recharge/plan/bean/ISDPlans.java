package com.bhartipay.util.recharge.plan.bean;

import java.io.Serializable;

public class ISDPlans implements Serializable{
	
	private int id;
	private int circlecode;
	private int operatorcode;
	private int price;
	private String validity;
	private String talktime;
	private String benefits;
	public String planType="ISDPlans";
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(int circlecode) {
		this.circlecode = circlecode;
	}
	public int getOperatorcode() {
		return operatorcode;
	}
	public void setOperatorcode(int operatorcode) {
		this.operatorcode = operatorcode;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getTalktime() {
		return talktime;
	}
	public void setTalktime(String talktime) {
		this.talktime = talktime;
	}
	public String getBenefits() {
		return benefits;
	}
	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	
	
	

}
