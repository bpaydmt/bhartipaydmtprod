package com.bhartipay.util.recharge.plan.bean;

import java.io.Serializable;
import java.util.List;



public class TariffPlans implements Serializable{
	
	private String statusCode;
	private String statusMsg;
	private  List<TopupPlans> topupPlans;
	private  List<SMSPlans> smsPlans;
	private  List<LocalPlans> localPlans;
	
	private  List<STDPlans> stdPlans;
	private  List<ISDPlans> isdlPlans;
	
	
	
	
	
	
	
	
	
	
	
	  
	 public List<STDPlans> getStdPlans() {
		return stdPlans;
	}

	public void setStdPlans(List<STDPlans> stdPlans) {
		this.stdPlans = stdPlans;
	}

	public List<ISDPlans> getIsdlPlans() {
		return isdlPlans;
	}

	public void setIsdlPlans(List<ISDPlans> isdlPlans) {
		this.isdlPlans = isdlPlans;
	}

	public List<LocalPlans> getLocalPlans() {
		return localPlans;
	}

	public void setLocalPlans(List<LocalPlans> localPlans) {
		this.localPlans = localPlans;
	}

	public List<SMSPlans> getSmsPlans() {
		return smsPlans;
	}

	public void setSmsPlans(List<SMSPlans> smsPlans) {
		this.smsPlans = smsPlans;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public List<TopupPlans> getTopupPlans() {
		return topupPlans;
	}

	public void setTopupPlans(List<TopupPlans> topupPlans) {
		this.topupPlans = topupPlans;
	}
	  
	  
	  
	  
	  
	  
	  
}
