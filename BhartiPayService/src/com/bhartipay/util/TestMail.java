package com.bhartipay.util;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;

import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.util.bean.MailConfigMast;

public class TestMail {
	
	
	public static void main(String[] args) {
		
	

	 
	 try {
	 
		// MailConfigMast mailConfigMast=commanUtilDao.getmailDtl(aggId);
		 
		 final String username ="b2bsupport@bhartipay.com";
		 final String password = "Support@123";
		 final String mailHost = "mail.go2cloud.in";
		 final String mailPort = "465";
		 final String mailFrom = "b2bsupport@bhartipay.com";
		 final String mailFromName = "bhartipay";
		 
		 
	 
		
     Properties props = new Properties();
		props.put("mail.smtp.host", mailHost); 
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.sender",mailFrom);
		props.put("mail.debug", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtps.starttls.enable","true");
		props.put("mail.smtp.starttls.required", "true");
		 props.put("mail.smtp.ssl.trust", mailHost);
		 props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	 
	  System.out.println("STep 1 set Parameter");
	  
	  Session session = Session.getInstance(props,
		         new javax.mail.Authenticator() {
		            protected PasswordAuthentication getPasswordAuthentication() {
		               return new PasswordAuthentication(username, password);
		            }
		         });
	  
	    Message message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(mailFrom,mailFromName));
       message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("santosh.kumar@appnittech.com"));
       message.setSubject("test");
       System.out.println("STep 2 set Parameter");
       BodyPart messageBodyPart = new MimeBodyPart();
       messageBodyPart.setContent("test", "text/html");
       Multipart multipart = new MimeMultipart();
       multipart.addBodyPart(messageBodyPart);
       messageBodyPart = new MimeBodyPart();
       System.out.println("STep 3 set Parameter");
     /* if(  StringUtils.isNotBlank("")){
   	   
        String filename = attach;
        DataSource source = new FileDataSource(filename);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);
        multipart.addBodyPart(messageBodyPart);
      }*/
       
        message.setContent(multipart);
        message.saveChanges();
       // Transport.
        Transport.send(message);
       
      System.out.println("Sent message successfully....to");
     // res= " Success";
		  
	  } catch(MessagingException me){
		  me.printStackTrace();
		 
		}catch (Exception e) {
			 e.printStackTrace();
	         
	      }finally {
	    	  
	    	 
		}
	 
	}


}
