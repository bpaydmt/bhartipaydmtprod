package com.bhartipay.util;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletSecurityBean;

import appnit.com.crypto.CheckSumHelper;

public class WalletSecurityUtility {
	static SessionFactory factory;
	private static final Logger logger = Logger.getLogger(WalletSecurityUtility.class.getName());
	public static int checkSecurity(TreeMap<String, String> treeMap) {

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		int returnStatus=1000;
		
		//8001=User not active.
		//8002=Token security expire.
		//1000=Password not changed.

		
		try {
			WalletSecurityBean walletBean = (WalletSecurityBean) session.get(WalletSecurityBean.class,treeMap.get("userId"));
			if (walletBean != null) {
				if (!walletBean.getUserStatus().equalsIgnoreCase("A")) {
					return 8001;
				}
				treeMap.put("token", walletBean.getToken());
				System.out.println("____________________Token___________________"+walletBean.getToken());
				System.out.println("____________________TokenSecurity___________________"+walletBean.getTokenSecurityKey());

				boolean status = CheckSumHelper.getCheckSumHelper().verifycheckSum(walletBean.getTokenSecurityKey(),treeMap, treeMap.get("CHECKSUMHASH"));
				logger.debug("Map for checking:"+ treeMap);
				logger.info("Map for checking:"+ treeMap);
				if (!status) {
					return 8002;
				}
			}else{
				return 8002;
			}

		} catch (Exception e) {
			returnStatus=1001;
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		return returnStatus;
	}
}
