package com.bhartipay.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class DBUtil {
	
	private static final SessionFactory sessionFactory = buildSessionFactory();
	
	
public static SessionFactory buildSessionFactory() {
		
		try {
			return  ((AnnotationConfiguration) new AnnotationConfiguration().configure()).buildSessionFactory();
		} catch (Throwable ex) {
			ex.printStackTrace();
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
 public static SessionFactory getSessionFactory() {
		return sessionFactory;

	}

 
 
 
 
 
 
 
 
 
 
}
