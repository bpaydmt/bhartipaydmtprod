package com.bhartipay.util;

import java.io.InputStream;
import java.util.Properties;


/**
 * Class defined to load all the configurable items defined in under
 * Infrastructure.properties file.
 * 
 * @author ambuj
 * 
 */
public class InfrastructureProperties{

	private static InfrastructureProperties instance = null;
	private static Properties properties = new Properties();;
	private InfrastructureProperties(){

		//Properties prop = new Properties();
		InputStream in = PasswordValidator.class.getResourceAsStream("Infrastructure.properties");
		                                                            
		try {
			properties.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
	}
	public static synchronized InfrastructureProperties getInstance(){
		
		if (instance == null) {
					instance = new InfrastructureProperties();
		}

		return instance;

	}

	
	public String getProperty(String propertyName) {
		
		if (properties.isEmpty()){
			return null;
		}else {
			return (String) properties.getProperty(propertyName);
		}
	}


}
