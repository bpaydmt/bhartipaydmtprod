package com.bhartipay.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONUtil {

	public static String GETJSONObjectValue(String jsonData,String jsonObjName, String objKey) {
		String value="";
		//System.out.println(jsonData);
		try {
			if(!jsonData.isEmpty()) {
				JsonObject jsonMainObject = new JsonParser().parse(jsonData).getAsJsonObject();
		        if(jsonMainObject.has(jsonObjName)){
					JsonObject jsonChildObj	= jsonMainObject.getAsJsonObject(jsonObjName);
			        if(jsonChildObj.has(objKey)){
			        	value	= jsonChildObj.get(objKey).getAsString();
			        }else {
			        	System.out.println("Key - "+objKey+" not exist in JSON-Object ");
			        }
		        }else {
		        	System.out.println("JSON-Object - "+jsonObjName+" not exist in JSON-Object ");
		        }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value ;
	}
	public static String GETJSONArrayValue(String jsonData,String jsonArrayName, String objKey,int postion) {
		String value="";
		//System.out.println(jsonData);
		try {
			if(!jsonData.isEmpty()) {
				JsonObject jsonMainObject = new JsonParser().parse(jsonData).getAsJsonObject();
				if(jsonMainObject.has(jsonArrayName)){
		        	JsonArray jsonArrayObj = jsonMainObject.getAsJsonArray(jsonArrayName);
		        	//System.out.println(jsonArrayObj.size());
		        	if(jsonArrayObj.size() > 0) {
		        		if(postion < jsonArrayObj.size()) {
		        			JsonObject jsonChildObj	= (JsonObject) jsonArrayObj.get(postion);
				        	 if(jsonChildObj.has(objKey)){
					        	value	= jsonChildObj.get(objKey).getAsString();
	  				         }else {
					        	System.out.println("Key - "+objKey+" not exist in JSON-Object ");
					         }
		        		}else {
				        	System.out.println("Array Postion - "+postion+" not exist in JSON-Array ,Actual Size :  "+jsonArrayObj.size());
		        		}
		        	}else {
			        	System.out.println("JSON-Array - "+jsonArrayName+" not exist in JSON-Object ");
		        	}
		        }else {
		        	System.out.println("JSON-Object - "+jsonArrayName+" not exist in JSON-Object ");
		        }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value ;
	}
	
	
	public static String GETJSONArrayJSONObjectValue(String jsonData,String jsonObjectName ,String jsonArrayName) {
		String value="";
		List<String> arrayList = new ArrayList<>();
		
		try {
			if(!jsonData.isEmpty()) {
				JsonObject jsonMainObject = new JsonParser().parse(jsonData).getAsJsonObject();
				if(jsonMainObject.has(jsonObjectName)){
					
					JsonObject jsonChildObj	= jsonMainObject.getAsJsonObject(jsonObjectName);
					if(jsonChildObj.has(jsonArrayName)){
						JsonArray jsonArrayObj = jsonChildObj.getAsJsonArray(jsonArrayName);
						for (int i = 0; i < jsonArrayObj.size(); i++) {
							arrayList.add(jsonArrayObj.get(i).getAsString());
						}
					}else {
			        	System.out.println("JSON-Array - "+jsonArrayName+" not exist in JSON-Object ");
					}
		        }else {
		        	System.out.println("JSON-Object - "+jsonObjectName+" not exist in JSON-Object ");
		        }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("ArrayList : "+arrayList);
		if(arrayList.size() >0) {
			value = String.join(", ", arrayList);
		}
		
		return value ;
	}
	
	
	public static void main(String[] args) {

		String jsonData ="{\"sales_information\":{\"aggregator_application_number\":\"1805202011111\",\"application_date\":\"2021-05-18\",\"aggreement_date\":\"2021-05-25\"},\"company_information\":{\"legal_name\":\"Test Merchant 18052020\",\"brand_name\":\"Test Merchant 18052020\",\"registered_address\":\"test plan 1\",\"registered_pincode\":\"5e81b1c262b7d36af649e65d\",\"pan\":\"CVCPS5805G\",\"gstin\":\"1234567890\",\"business_type\":\"individual\",\"established_year\":\"2020-05-18\",\"business_nature\":\"Grocery Stores, Supermarkets\",\"merchant_category_code_id\":\"5e81b1d062b7d36af64a11be\",\"merchant_type_id\":\"5e97e6179f535a7fc56dc12f\",\"contact_name\":\"sreekanth\",\"contact_mobile\":\"8888888888\",\"contact_alternate_mobile\":\"2222222222\",\"contact_email\":\"test2@gmail.com\"},\"personal_information\":[{\"title\":\"Mr.\",\"first_name\":\"k\",\"last_name\":\"sreeeeeee\",\"address\":\"testplan1\",\"pincode\":\"5e81b1c262b7d36af649e65d\",\"mobile\":\"9393883838\",\"email\":\"test@gmail.com\",\"nationality\":\"Indian\",\"passport_expiry_date\":\"2020-05-18\",\"is_own_house\":false,\"dob\":\"1978-05-18\",\"pan\":\"CVCPS5805G\"}]}";
        JsonObject jsonObject = new JsonParser().parse(jsonData).getAsJsonObject();
       // String aggregator_application_number = jsonObject.getAsJsonObject("sales_information").get("aggregator_application_number").getAsString();
        String aggregator_application_number = GETJSONObjectValue(jsonData,"sales_information", "aggregator_application_number") ;
        
        System.out.println(aggregator_application_number);
        
	}

}