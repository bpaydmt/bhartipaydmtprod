package com.bhartipay.util;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimerTask;

import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class MailSchedular extends TimerTask{
	
	Long period = new Long(24*60*60*1000L);
	 Properties prop = new Properties();
	
	MailSchedular(Long period)
	{
		this.period = period;
		try
		{
	    	 String filename = "Balance.properties";
	    	 InputStream input1 = MailSchedular.class.getResourceAsStream("Balance.properties");
			if (input1 == null) 
			{
				System.out.println("Sorry, unable to find " + filename);		
			}
			prop.load(input1);
			input1.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	TransactionDao DaoObject=new TransactionDaoImpl();
	WalletUserDao walletObject=new WalletUserDaoImpl();
	WalletMastBean walletmst=new WalletMastBean();
	CommUtility sendMessage=new CommUtility();
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();	
	public synchronized void run()
	{

		String avgBalanceId="";
		String finalBalanceCompare="";
		double mainBalance=0.0;
		double avgBalance=0.0;
		double finalBalancePookCity=0.0;
		double finalBalancetranspay=0.0;
	
		
         try {
			avgBalanceId = prop.getProperty("avgBalanceCompare");
			finalBalanceCompare = prop.getProperty("finalBalanceCompare");
			mainBalance=Double.parseDouble(prop.getProperty("mainBalance"));
			avgBalance=DaoObject.getAverageBalance(avgBalanceId);
			finalBalancePookCity=DaoObject.getCashDepositWalletBalance(avgBalanceId);
			finalBalancetranspay=DaoObject.getCashDepositWalletBalance(finalBalanceCompare);
			{
				sendPendingRechargeMail(avgBalanceId,avgBalanceId);
			}
		if(finalBalancePookCity<=avgBalance)
		{
			walletmst=walletObject.getUserByMobileNo(avgBalanceId,avgBalanceId,"");

			String emailto=walletmst.getEmailid();
			String aggid=walletmst.getAggreatorid();
			String walletid=walletmst.getWalletid();
			String senderName=walletmst.getName();
		   String template =commanUtilDao.getmailTemplet("lowBalanceMailer", aggid).replaceAll("<<name>>", senderName).replaceAll("<<amt>>", Double.toString(finalBalancePookCity))
				   .replaceAll("<<appName>>", "BhartiPay");
		   new CommUtility().sendMail(emailto, "BhartiPay :Low Wallet Balance",template , "", "OAGG001050", walletid, senderName);

		}
		if(finalBalancetranspay<=mainBalance)
		{			
			walletmst=walletObject.getUserByMobileNo(finalBalanceCompare,finalBalanceCompare,"");

			String emailto=walletmst.getEmailid();
			String subject="walletBalance";
			String aggid=walletmst.getAggreatorid();
			String walletid=walletmst.getWalletid();
			String senderName=walletmst.getName();
			String template =commanUtilDao.getmailTemplet("lowBalanceMailer", aggid).replaceAll("<<name>>", senderName).replaceAll("<<amt>>", Double.toString(finalBalancetranspay))
					   .replaceAll("<<appName>>", "BhartiPay");;

			 new CommUtility().sendMail(emailto, "BhartiPay :Low Wallet Balance",template , "", "OAGG001050", walletid, senderName);
		
		}
	}catch(Exception e)
         {
		e.printStackTrace();
         }
	}

 void sendPendingRechargeMail(String mobile,String aggregatorId)
 {
	 try
	 {
		walletmst=walletObject.getUserByMobileNo(mobile,aggregatorId,"");

		String emailto=walletmst.getEmailid();
		String subject="walletBalance";
		String aggid=walletmst.getAggreatorid();
		String walletid=walletmst.getWalletid();
		String senderName=walletmst.getName();
		List<RechargeTxnBean> list = DaoObject.getRechargePendingStatus(aggregatorId, period) ;
		if(!list.isEmpty() && list.size()>0)
		{
			String mailTemplate = prop.getProperty("mailHeader");
			Iterator<RechargeTxnBean> itr = list.iterator();
			String body = prop.getProperty("mailSegment");
			while(itr.hasNext())
			{
				RechargeTxnBean itrVal= itr.next();
				mailTemplate += body;
				mailTemplate.replaceAll("<<rechargeRef>>",itrVal.getRechargeRespId()).replaceAll("<<txnId>>", itrVal.getRechargeId())
				.replaceAll("<<accountNo>>",  itrVal.getRechargeNumber()).replaceAll("<<amount>>", Double.toString(itrVal.getRechargeAmt()))
				.replaceAll("<<status>>", itrVal.getStatus());
			}
			mailTemplate += prop.getProperty("mailTail");
			new CommUtility().sendMail(emailto, "BhartiPay : Pending Recharges Status",mailTemplate , "", "OAGG001050", walletid, senderName);
		}
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
	 }
 }
}
