package com.bhartipay.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.util.bean.MailConfigMast;
import com.bhartipay.util.bean.MailSendDetails;
import com.bhartipay.util.bean.SMSConfigMast;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class CommUtility {

	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
	// CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	SMSSendDetails sMSSendDetails = new SMSSendDetails();
	MailSendDetails mailSendDetails = new MailSendDetails();

	public String sendMail(String to, String sub, String msg, String attach, String aggId, String walletid,
			String sendername) {
		String res = null;

		try {

			MailConfigMast mailConfigMast = commanUtilDao.getmailDtl(aggId);

			final String username = mailConfigMast.getUsername();
//			final String password = EncryptionDecryption.getdecrypted(mailConfigMast.getPassword(),
//					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
			final String password = mailConfigMast.getPassword();
			final String mailHost = mailConfigMast.getHosturl();
			final String mailPort = mailConfigMast.getPort();
			final String mailFrom = mailConfigMast.getFrom_mail();
			final String mailFromName = mailConfigMast.getFromname();
			

			Properties props = new Properties();

			props.put("mail.smtp.host", mailHost);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.sender", mailHost);
			props.put("mail.debug", "true");
			props.put("mail.smtp.port", mailPort);
			props.put("mail.smtps.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			props.put("mail.smtp.ssl.trust", mailHost);
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailFrom, mailFromName));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(sub);

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(msg, "text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			if (StringUtils.isNotBlank(attach)) {

				String filename = attach;
				DataSource source = new FileDataSource(filename);
				messageBodyPart.setDataHandler(new DataHandler(source));
				if(filename.contains("Bhartipay_Ticket_"))
				{
					String fName=filename.substring(filename.length()-30,filename.length());
					messageBodyPart.setFileName(fName);
				}
				else if(filename.contains("/")) {
					String fName=filename.substring(filename.lastIndexOf("/")+1);
					messageBodyPart.setFileName(fName);
				}else{
				messageBodyPart.setFileName(filename);
				}
				multipart.addBodyPart(messageBodyPart);
			}

			message.setContent(multipart);
			message.saveChanges();
			Transport.send(message);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", "sendMail()"+
					"Sent message successfully to -" + to);
			//System.out.println("Sent message successfully....to" + to);
			res = " Success";

		} catch (MessagingException me) {
			me.printStackTrace();
			res = me.getMessage();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", 
					"problem in sendmail" + me.getMessage()+" "+me);
			//logger.debug("problem in sendmail******************" + me.getMessage(), me);
		} catch (Exception e) {
			e.printStackTrace();
			res = e.getMessage();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", 
					"problem in sendmail" + e.getMessage()+" "+e);
			//logger.debug("problem in sendmail******************" + e.getMessage(), e);
		} finally {

			String toArray[] = to.split(",");
			for (String email : toArray) {
			mailSendDetails.setAggreatorid(aggId);
			mailSendDetails.setWalletid(walletid);
			mailSendDetails.setSendername(sendername);
			mailSendDetails.setEmailId(email);
			mailSendDetails.setSubject(sub);
			mailSendDetails.setStatus(res);
			commanUtilDao.saveMailSendDetails(mailSendDetails);
			}
		}

		return res;
	}

	// public String sendSms(String mobileNo, String msg,String aggId, String
	// walletid,String sendername) {
	public String sendSms(String mobileNo, String msg, String aggId, String walletid, String sendername) {
		String responseString = "";
		String status = "success";
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSms()"
					);
			//logger.info("Start excution ===================== method sendSms()" + aggId);

			HttpClient client = new HttpClient();
			client.getParams().setParameter("http.useragent", "Test Client");
			BufferedReader br = null;

			Properties prop = new Properties();
			InputStream in = CommUtility.class.getResourceAsStream("comm.properties");
			prop.load(in);
			in.close();

			SMSConfigMast sMSConfigMast = commanUtilDao.getSMSDtl(aggId);
			String url = sMSConfigMast.getSmsurl();
			String smsUsername = sMSConfigMast.getSmsusername();
			String smsPassword = EncryptionDecryption.getdecrypted(sMSConfigMast.getSmspassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
			String smsFeedid = sMSConfigMast.getSmsfeedid();
			String smsSenderid = sMSConfigMast.getSmssenderid();

			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			Format f1 = new SimpleDateFormat("yyyyMMddHHmm");
			f1.format(date.getTime());

			
			
			
			HttpMethod method = new GetMethod(url);
			method.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
			if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001057")){
				//ToMobile=9999215888&smsText=Hello%20Test%20SMS%20Api&Tokenkey=3259SWJODOEJ&senderid=EZEEJN
			NameValuePair[] query = { 
						new NameValuePair("ToMobile", mobileNo),
						new NameValuePair("smsText", msg),
						new NameValuePair("Tokenkey", smsPassword), 
						new NameValuePair("senderid", smsSenderid) 
						};
			method.setQueryString(query);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","parameter"+ query
					);
			//logger.debug("in sendSms********parameter**********" + query);
			}else{
			NameValuePair[] query = { new NameValuePair("feedid", smsFeedid),
						new NameValuePair("username", smsUsername), new NameValuePair("password", smsPassword),
						new NameValuePair("To", "91" + mobileNo), new NameValuePair("Text", msg),
						new NameValuePair("time", f1.format(date.getTime())), new NameValuePair("senderid", smsSenderid) };
			method.setQueryString(query);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","parameter"+ query
					);
			//logger.debug("in sendSms********parameter**********" + query);
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","url" + url
					);
			//logger.debug("in sendSms********url**********" + url);
			int returnCode = client.executeMethod(method);
			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","The Post method is not implemented by this URI"
						);
				//logger.info("The Post method is not implemented by this URI");
				responseString = method.getResponseBodyAsString();
			} else {
				br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
				responseString = br.readLine();
				System.out.println(returnCode);
				System.out.println(responseString);
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","problem in sendSms"+ e.getMessage()+" "+e
					);
			//logger.debug("problem in sendSms******************" + e.getMessage(), e);
			status = e.getMessage();
		} finally {
			sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			sMSSendDetails.setStatus(status);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);
		}
		return responseString;

	}

	public String sendSmsOLD(String mobileNo, String msg, String aggId, String walletid, String sendername) {
		String responseString = "";
		String status = "success";
		HttpURLConnection conn=null;
		try {
			HttpClient client = new HttpClient();
			client.getParams().setParameter("http.useragent", "Test Client");
			//BufferedReader br = null;

			Properties prop = new Properties();
			InputStream in = CommUtility.class.getResourceAsStream("comm.properties");
			prop.load(in);
			in.close();

			SMSConfigMast sMSConfigMast = commanUtilDao.getSMSDtl(aggId);
			String durl = sMSConfigMast.getSmsurl();
			String smsUsername = sMSConfigMast.getSmsusername();
			String smsPassword = EncryptionDecryption.getdecrypted(sMSConfigMast.getSmspassword(),
					"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");

			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			Format f1 = new SimpleDateFormat("yyyyMMddHHmm");
			f1.format(date.getTime());

			//Date mydate = new Date(System.currentTimeMillis());
			String data = "";
			data += "?method=sendMessage";
			data += "&userid=" + smsUsername; // your loginId
			data += "&password=" + URLEncoder.encode(smsPassword, "UTF-8"); // your
																			// password
			data += "&msg=" + URLEncoder.encode(msg.toString(), "UTF-8");
			data += "&send_to=" + URLEncoder.encode("91" + mobileNo, "UTF-8"); 
			data += "&v=1.1";
			data += "&msg_type=TEXT"; // Can by "FLASH" or "UNICODE_TEXT" or
										// �BINARY�
			data += "&auth_scheme=PLAIN";
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsOLD()"+ durl + data
					);
			//System.out.println("SMS URL******************************" + durl + data);
			URL url = new URL(durl + data);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.connect();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuffer buffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				buffer.append(line).append("\n");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","SMS LOG"+ buffer.toString()
					);
			//logger.debug("SMS LOG************************************" + buffer.toString());
			responseString = buffer.toString();
			rd.close();
		

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","problem in sendSms"+ e.getMessage()+" "+e
					);
			//logger.debug("problem in sendSms******************" + e.getMessage(), e);
			status = e.getMessage();
		} finally {
			sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			sMSSendDetails.setStatus(status);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);
			if(conn!=null){
			conn.disconnect();
			}
		}
		return responseString;

	}

	org.apache.http.client.HttpClient http4Client=PoolingManager.INSTANCE.getHttpClient();
	
	public String sendSmsKaleyra (String to, String message, String aggId, String walletid, String sendername,String type) 
	{
			
//		if("OAGG001050".equalsIgnoreCase(aggId) && "OTP".equalsIgnoreCase(type)) {
//		new CommUtility().orionteleSMS (to,message,  aggId,  walletid,  sendername, type) ;
//		}else {
					
				HttpPost httpPost = null;
		 		SMSConfigMast sMSConfigMast = commanUtilDao.getSMSDtl(aggId);
		 		List<org.apache.http.NameValuePair> param=new LinkedList<>();

				String endpoint=sMSConfigMast.getSmsurl();
				String api_key=sMSConfigMast.getSmspassword();
 				String method="sms";
				String sender=sMSConfigMast.getSmssenderid();
  				

					
					param.add(new BasicNameValuePair("api_key",api_key));
					param.add(new BasicNameValuePair("method",method));
					param.add(new BasicNameValuePair("message",message));
					param.add(new BasicNameValuePair("to",to));
					param.add(new BasicNameValuePair("sender",sender));
					
				
					
				String queryString = URLEncodedUtils.format(param, "UTF-8");
				String finalEndpoint= endpoint+"?"+queryString;
				HttpResponse httpResponse = null;
				try 
				{
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsKaleyra() Executing " + finalEndpoint	);
					httpPost  = new HttpPost(finalEndpoint);
					httpResponse = http4Client.execute(httpPost);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsKaleyra() Executed " + finalEndpoint + httpResponse	);
					boolean is2xx = httpResponse.getStatusLine().getStatusCode()>=200 && httpResponse.getStatusLine().getStatusCode() < 300;
					if (is2xx)
					{
						String result=IOUtils.toString(httpResponse.getEntity().getContent(),"UTF-8");
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsKaleyra() Executed " + finalEndpoint +" : " + result	);
 						return result;
					}
				}
				catch (Exception e)
				{
					Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsKaleyra() Exception occured while executing " + finalEndpoint + e.getMessage()	);

				}
				finally
				{
					
					if (httpResponse!=null && httpResponse.getEntity()!=null)
					{
						EntityUtils.consumeQuietly(httpResponse.getEntity());
					}
					try 
					{
						httpPost.releaseConnection();
 					} catch (Exception e2) 
					{
 						Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsKaleyra() Exception occured while executing "  + e2.getMessage()	);
					}
					try {
						sMSSendDetails.setRecipientno(to);
						sMSSendDetails.setAggreatorid(aggId);
						sMSSendDetails.setWalletid(walletid);
						sMSSendDetails.setSendername(sendername);
						sMSSendDetails.setStatus("Success");
						commanUtilDao.saveSMSSendDetails(sMSSendDetails);
					}catch (Exception e) {
						e.printStackTrace();
					}
 				}
 			
//		}
				return "";
	}
		
	
	
	
	public String sendSmsVideocon_backup (String mobileNo, String msg, String aggId, String walletid, String sendername,String type) 
	{
				HttpGet httpGet = null;
		 		SMSConfigMast sMSConfigMast = commanUtilDao.getSMSDtl(aggId);
		 		List<org.apache.http.NameValuePair> param=new LinkedList<>();
		 		List<org.apache.http.NameValuePair> rawParam=new LinkedList<>();

				String endpoint="";
 				String smsPassword;
				String senderId;
  				
				//ToMobile=9999215888&smsText=Hello%20Test%20SMS%20Api&Tokenkey=3259SWJODOEJ&senderid=EZEEJN
				if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001057"))
				{
					try 
					{
	 					smsPassword=EncryptionDecryption.getdecrypted(sMSConfigMast.getSmspassword(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
 					} catch (Exception e) 
					{
 						Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Exception decrypting token " + e.getMessage()	);
 						return "";
					}
					senderId=sMSConfigMast.getSmssenderid();
					endpoint = sMSConfigMast.getSmsurl();
 					param.add(new BasicNameValuePair("ToMobile",mobileNo));
					param.add(new BasicNameValuePair("smsText",msg));
					rawParam.add(new BasicNameValuePair("Tokenkey",smsPassword));
					rawParam.add(new BasicNameValuePair("senderid",senderId));
  				}
				else
				{
					endpoint = "https://bulksmsapi.vispl.in/";
					if (type.equalsIgnoreCase("OTP")) 
					{
					
						rawParam.add(new BasicNameValuePair("username","appnitotp"));
						rawParam.add(new BasicNameValuePair("password","appnitotp@123"));

		 				if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001054"))
						{
							senderId = "TRANSP";
						}
		 				else
		 				{
							senderId = "OXYMNY";
						}
		 				rawParam.add(new BasicNameValuePair("senderId",senderId));
	  				}
					else 
					{
						rawParam.add(new BasicNameValuePair("username","appnittrn"));
						rawParam.add(new BasicNameValuePair("password","appnittrn@123"));
						if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001054"))
							senderId = "TRANSP";
						else
							senderId = "OXYMNY";
						rawParam.add(new BasicNameValuePair("senderId",senderId));
					}

					param.add(new BasicNameValuePair("messageType","text"));
					param.add(new BasicNameValuePair("mobile",mobileNo));
					param.add(new BasicNameValuePair("message",msg));
					
				}
					
				String queryString = URLEncodedUtils.format(param, "UTF-8");
				String rawQueryString =toString(rawParam);
				String finalEndpoint= endpoint+"?"+rawQueryString+queryString;
				HttpResponse httpResponse = null;
				try 
				{
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Executing " + finalEndpoint	);
					httpGet  = new HttpGet(finalEndpoint);
					httpResponse = http4Client.execute(httpGet);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Executed " + finalEndpoint + httpResponse	);
					boolean is2xx = httpResponse.getStatusLine().getStatusCode()>=200 && httpResponse.getStatusLine().getStatusCode() < 300;
					if (is2xx)
					{
						String result=IOUtils.toString(httpResponse.getEntity().getContent(),"UTF-8");
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Executed " + finalEndpoint +" : " + result	);
 						return result;
					}
				}
				catch (Exception e)
				{
					Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Exception occured while executing " + finalEndpoint + e.getMessage()	);

				}
				finally
				{
					if (httpResponse!=null && httpResponse.getEntity()!=null)
					{
						EntityUtils.consumeQuietly(httpResponse.getEntity());
					}
					try 
					{
						httpGet.releaseConnection();
 					} catch (Exception e2) 
					{
 						Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon() Exception occured while executing "  + e2.getMessage()	);
					}
 				}
				return "";
	}
		
	
	
	
	
	public String toString(List<org.apache.http.NameValuePair> pairs)
	{
		StringBuilder builder = new StringBuilder();
		for (org.apache.http.NameValuePair pair : pairs)
		{
			builder.append(pair.getName()+"="+pair.getValue()+"&");
		}
		String result= builder.toString();
	 
		return result;
		
		
		
	}
	 
	public String _sendSmsVideocon (String mobileNo, String msg, String aggId, String walletid, String sendername,String type) {
		String responseString = "";
		String status = "success";
		HttpsURLConnection conn=null;
		try {
			HttpClient client = new HttpClient();
			client.getParams().setParameter("http.useragent", "Test Client");
			//BufferedReader br = null;

			Properties prop = new Properties();
			InputStream in = CommUtility.class.getResourceAsStream("comm.properties");
			prop.load(in);
			in.close();

			 SMSConfigMast sMSConfigMast = commanUtilDao.getSMSDtl(aggId);
			
			// String durl = sMSConfigMast.getSmsurl(); 
			// String smsUsername =sMSConfigMast.getSmsusername();
			// String smsPassword =EncryptionDecryption.getdecrypted(sMSConfigMast.getSmspassword(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
			// String senderId =sMSConfigMast.getSmssenderid();
			// String durl = sMSConfigMast.getSmsurl();

			String smsUsername;
			String smsPassword;
			String senderId;
			String data = "";
			String durl = "https://bulksmsapi.vispl.in/";
			
			//ToMobile=9999215888&smsText=Hello%20Test%20SMS%20Api&Tokenkey=3259SWJODOEJ&senderid=EZEEJN
			if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001057")){
				smsPassword=EncryptionDecryption.getdecrypted(sMSConfigMast.getSmspassword(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
				senderId=sMSConfigMast.getSmssenderid();
				durl = sMSConfigMast.getSmsurl();
				data += "?ToMobile="+URLEncoder.encode(mobileNo, "UTF-8");
				data += "&smsText=" + URLEncoder.encode(msg, "UTF-8"); // your // password
				data += "&Tokenkey=" +smsPassword;
				data += "&senderid=" + senderId;
				
				
			}else{
			durl = "https://bulksmsapi.vispl.in/";
			if (type.equalsIgnoreCase("OTP")) {
				smsUsername = "appnitotp";
				smsPassword = "appnitotp@123";
				if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001054")){
					senderId = "TRANSP";
				}else{
					senderId = "OXYMNY";
				}

			} else {
				smsUsername = "appnittrn";
				smsPassword = "appnittrn@123";
				if(aggId!=null&&aggId.equalsIgnoreCase("OAGG001054"))
					senderId = "TRANSP";
				else
					senderId = "OXYMNY";
			}
			
			data += "?username=" + smsUsername;
			data += "&password=" + smsPassword; // your // password
			data += "&messageType=text";
			data += "&mobile=" + URLEncoder.encode(mobileNo, "UTF-8"); // a valid 10 digit phone no.
			data += "&senderId=" + senderId;
			data += "&message=" + URLEncoder.encode(msg, "UTF-8"); // Can by "FLASH" or "UNICODE_TEXT" or  �BINARY�
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","sendSmsVideocon()" + durl + data
					);
			//System.out.println("SMS URL***********sendSmsVideocon*******************" + durl + data);
			
			}
			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			Format f1 = new SimpleDateFormat("yyyyMMddHHmm");
			f1.format(date.getTime());
			//Date mydate = new Date(System.currentTimeMillis());

			// https://bulksmsapi.videoconsolutions.com/?username=appnitotp&password=appnitotp@123&messageType=text&mobile=<MSISDN>&senderId=OXYMNY&message=TEST
			// https://bulksmsapi.videoconsolutions.com/?username=appnitotp&password=appnitotp@123&messageType=text&mobile=9999215888&senderId=OXYMNY&message=test
			
			
			

			// Create a context that doesn't check certificates.
			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, // key manager
					trust_mgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(durl + data);

			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(30000);
			conn.connect();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuffer buffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				buffer.append(line).append("\n");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", buffer.toString()
					);
			//logger.debug("SMS LOG*********************sendSmsVideocon***************" + buffer.toString());
			responseString = buffer.toString();
			rd.close();
			

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","problem in sendSms" + e.getMessage()+" "+e
					);
			//logger.debug("problem in sendSms*******sendSmsVideocon***********" + e.getMessage(), e);
			status = e.getMessage();
		} finally {
			sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			sMSSendDetails.setStatus(status);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);
			if(conn!=null){
				conn.disconnect();
			}
		}
		return responseString;

	}

	
	
	
	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

	public String orionteleSMS(String mobileNo, String msg, String aggId, String walletid, String sendername,String type) {
		String responseString = "";
		String status = "success";
		HttpURLConnection conn=null;
		try {
			HttpClient client = new HttpClient();
			client.getParams().setParameter("http.useragent", "Test Client");
			
			
				String user="bhartipaynew";
				String password="bharti1234";
				String senderid="BHARTI";
				String channel="Trans";
				String DCS="0";
				String flashsms="0";
				String route="07";

			String durl = "http://103.16.142.249/api/mt/SendSMS";
			String data="";
			data += "?user=" + user;
			data += "&password=" + password; // your // password
			data += "&senderid="+senderid;
			data += "&channel="+channel;
			data += "&DCS=" + DCS;
			data += "&flashsms=" + flashsms;
			data += "&number=" + URLEncoder.encode("91"+mobileNo, "UTF-8"); // a valid 10 digit phone no.
			data += "&text="+ URLEncoder.encode(msg, "UTF-8"); // Can by "FLASH" or "UNICODE_TEXT" or  �BINARY�
			data += "&route=" + route;
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","orionteleSMS()" + durl + data
					);

			
			//HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(durl + data);

			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(30000);
			conn.connect();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuffer buffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				buffer.append(line).append("\n");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "", buffer.toString()
					);
			//logger.debug("SMS LOG*********************sendSmsVideocon***************" + buffer.toString());
			responseString = buffer.toString();
			rd.close();
			

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,walletid,"", "", "","problem in sendSms" + e.getMessage()+" "+e
					);
			//logger.debug("problem in sendSms*******sendSmsVideocon***********" + e.getMessage(), e);
			status = e.getMessage();
		} finally {
			sMSSendDetails.setRecipientno(mobileNo);
			sMSSendDetails.setAggreatorid(aggId);
			sMSSendDetails.setWalletid(walletid);
			sMSSendDetails.setSendername(sendername);
			sMSSendDetails.setStatus(status);
			commanUtilDao.saveSMSSendDetails(sMSSendDetails);
			if(conn!=null){
				conn.disconnect();
			}
		}
		return responseString;

	}
	
	
	public static void main(String[] args) throws IOException, Exception { 
		
		new CommUtility().sendSmsKaleyra ("9999215888", "Your OTP for Bhartipay wallet creation and beneficiary addition is 111790. OTP is confidential, please enter it yourself & do not disclose to anyone",  "OAGG001050", "", "","Txn") ;
		
	}

}
