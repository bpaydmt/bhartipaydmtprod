package com.bhartipay.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class BPJWTSignUtil {

	public static Logger logger=Logger.getLogger(BPJWTSignUtil.class);
	
    public Claims parseToken(String token,String key) {
        try {
        	String str = new String(DatatypeConverter.printBase64Binary(new String(key).getBytes()));
            System.out.println("Token :"+token);
        	Claims body = Jwts.parser()
                    .setSigningKey(str)
                    .parseClaimsJws(token)
                    .getBody();

          // logger.info("********"+Jwts.parser().setSigningKey(str).parse(token).getBody().toString().substring(1, Jwts.parser().setSigningKey(str).parse(token).getBody().toString().length())); 
        	return body;

        } catch (JwtException | ClassCastException e) {
        	logger.debug(e);
            return null;
        }
		
    }
	
    public JSONObject parseTokenNew(String token,String key) {
        try {
        	if(token!=null&&token.length()>0){
        	byte[] s=org.apache.commons.codec.binary.Base64.decodeBase64(token.trim().split("\\.")[1].getBytes("UTF-8"));
        	
        	String plainToken=new String(s).replaceAll("\u0000","").trim();
        	
        	if(plainToken.endsWith("\"")){
        		plainToken=plainToken+"}]";
        	}else if(plainToken.endsWith("}")){
        		plainToken=plainToken+"]";
        	}
        	
        	System.out.println(new Gson().toJson(new String(s)));
//        	plainToken=plainToken.trim().lastIndexOf("}]")>-1?plainToken:plainToken.trim()+"}]";
//        	plainToken=plainToken.trim().lastIndexOf("}]")>-1?plainToken:plainToken.trim()+"}]";
        	JSONObject json=new JSONArray(plainToken).getJSONObject(0);

            logger.info("*************************** json "+json);
        	return json;
        	}else{
        		return null;
        	}

        } catch (Exception e) {
        	logger.debug(e);
            return null;
        }
		
    }
    
    
   public String generateToken(HashMap<String, Object> valueObj,String key ) {
        
    	Claims claims = Jwts.claims();
        
        //Jwts.header();
        Set set = valueObj.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           logger.info("********key is: "+ mentry.getKey() + " & Value is: ");
           logger.info("********"+mentry.getValue());
           claims.put((String) mentry.getKey(), mentry.getValue());
        }

        String str = new String(DatatypeConverter.printBase64Binary(new String(key).getBytes()));
		return Jwts.builder()
		    .setHeaderParam("typ", "JWT").setHeaderParam("alg", "HS256")
		    .setClaims(claims).signWith(SignatureAlgorithm.HS256, str)   
		    .compact();
                
    }
    
    
   public static void main(String args[]){
	   BPJWTSignUtil sv=new BPJWTSignUtil();
		HashMap<String, Object> aa=new HashMap<String, Object>();
		aa.put("billerType", "Electricity");
		
		System.out.println(sv.generateToken(aa,"e391ab57590132714ad32da9acf3013eb88c"));
		//Claims claimss=sv.parseToken(sv.generateToken(aa,"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
		//Claims claimss=sv.parseToken("eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJtb2JpbGVObyI6Ijk4MjgwNzc4NzEifQ.","km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
		
		//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2JpbGVObyI6Ijk4MjgwNzc4NzEifQ.h41n7UJhiXYqWMdFDajdvFjxE60FdbF6YpbQ7oUvLdw
		
		//System.out.println(claimss.get("mobileNo").toString());
		//  System.out.println(sv.parseToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJNb2JpbGVObyI6IjExMTExMTExMTEifQ.Z_4k91rQX_QUqeUZTZT81yZqLZMpWt-EkId6oIPd0pg","e391ab57590132714ad32da9acf3013eb88c"));
		  
	  }  
    
}
