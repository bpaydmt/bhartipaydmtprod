package com.bhartipay.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;


public class CommanUtil extends Thread{
	
	long second = 0;
	public static String SHAHashing256(String otp) throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(otp.getBytes());
        byte byteData[] = md.digest();
      //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
	    return sb.toString();
	}

	public CommanUtil()
	{
		
	}
	
	public CommanUtil(long isecond)
	{
			second = isecond;
			
	}
	
	
	public void run()
	{
		int counter = 0;
		try
		{
		while(counter < second)
		{
		   counter++;
		   Thread.sleep(1000);
		    System.out.println(counter);
		  }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public boolean time(int time)
	{
		boolean flag = false;
		try
		{
			CommanUtil commanUtil = new CommanUtil(time);
			commanUtil.start();
			return true;
		}
		catch(Exception e)
		{
			
		}
		return flag;
	}
	public String getErrorMsg(String errorCode){
		  
		  switch (errorCode) {
		  case "7013":
		   return "New password must not be same as previous 5 passwords.";
		   
		   
		  case "7014":
		   return "Your transfer amount exceed from limit of per transaction.";
		   
		  case "7017":
		   return "Your per day total transfer amount exceed.please try next day.";
		   
		  case "7018":
		   return "Your per week total transfer amount exceed.please try next week.";
		   
		  case "7019":
		   return "Your per month total transfer amount exceed.please try next month.";
		   
		  case "7020":
		   return "Your per day number of transaction exceed.please try next day.";
		   
		  case "7021":
		   return "Your per week number of transaction exceed.please try next week.";
		   
		  case "7022":
		   return "Your per month number of transaction exceed.please try next month.";
		   
		  case "7045":
		   return "Your maximum transaction per quarter have been exceed.please try next quarter.";
		   
		  case "7046":
		   return "Your maximum transaction per half yearly have been exceed.please try next half yearly.";
		   
		  case "7047":
		   return "Your maximum transaction per year have been exceed.please try next year.";
		  
		  case "7048":
		   return "Insufficient Balance.";
		   
		  case "7049":
		   return "Your request aggregator ID is Invalid.";
		   
		  case "7050":
		   return "Your are a suspended user.Please contact to aggregator.";
		   
		  case "7032":
		   return "Txn Amount should not be Zero.";
		   
		  case "7024":
		   return "Other problem than velocity.";
		   
		  case "1001":
		   return "Transaction has not done.";
		  
		  default:
		   return "Velocity Check Failed";
		  
		  }
	}
	
	
	 public void htmlToPdfGeneration(String filename , String html)
	 {
	  try {
	         OutputStream file = new FileOutputStream(new File(filename));
	         Document document = new Document();
	         PdfWriter.getInstance(document, file);
	         document.open();
	         //XMLWorkerHelper.getInstance().parseXHtml(new XHtmlElementHandler(document), new StringReader(text));
	         HTMLWorker htmlWorker = new HTMLWorker(document);
	         htmlWorker.parse(new StringReader(html));
	         System.out.println("*****************Done***********");
	         document.close();
	         file.close();
	     } catch (Exception e) {
	         e.printStackTrace();
	         e.getMessage();
	     }
	 }
	
	public static void main(String[] args) {
		try {
			System.out.println(SHAHashing256("1234"));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
