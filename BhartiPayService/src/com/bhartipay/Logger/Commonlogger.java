package com.bhartipay.Logger;

import org.apache.log4j.Logger;



public class Commonlogger {

	public static final int FATAL = 5;
	public static final int ERROR = 3;
	public static final int WARN = 4;
	public static final int INFO = 1;
	public static final int DEBUG = 7;
	public static final int TRACE = 9;
	
	static Logger logger = Logger.getLogger(Commonlogger.class);
	
	

	public static void log(int level, String ClassName, String aggregatorid,String walletid,String userid,String senderid,String txnid, String message) {
		
		/*System.out.println("----------*********-------logger-----*************---"+logger);
		
		System.out.println("-______________________________________________________________________");
		
		System.out.println("----------*********-------level-----*************---"+level);
		
		System.out.println("----------*********-------ClassName-----*************---"+ClassName);
		
		System.out.println("----------*********-------aggregatorid-----*************---"+aggregatorid);
		
		System.out.println("----------*********-------walletid-----*************---"+walletid);
		
		System.out.println("----------*********-------userid-----*************---"+userid);
		
		System.out.println("----------*********-------senderid-----*************---"+senderid);
		
		System.out.println("----------*********-------txnid-----*************---"+txnid);
		
		System.out.println("----------*********-------message-----*************---"+message);
		
		*/
		
		//PropertyConfigurator.configure("/jhub/_prod/dataImporter/properties/dataImporter.properties");
//		PropertyConfigurator.configure("E:/B! workspace/log4j.properties");
		//logger = logger.getLogger("");

		switch(level) {

		case 1: logger.info("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message); break;
		case 2:
		case 3: logger.error("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message); break;
		case 4: logger.warn("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message); break;
		case 5: logger.fatal("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message); break;
		case 6:
		case 7: logger.debug("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message); break;
		case 8:
	//  case 9:  logger.trace(message); break;
		default: logger.info("| "+ClassName+" | "+aggregatorid+" | "+walletid+" | "+userid+" | "+senderid+" | "+txnid+" | --> "+message);

		}
	}
}
