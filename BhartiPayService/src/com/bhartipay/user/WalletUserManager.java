package com.bhartipay.user;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.DmtPricing;
import com.bhartipay.lean.bean.NewRegs;
import com.bhartipay.lean.bean.OnboardStatusResponse;
import com.bhartipay.lean.bean.Test;
import com.bhartipay.report.bean.ReportBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.user.bean.AgentDetailsView;
import com.bhartipay.user.bean.LoginResponse;
import com.bhartipay.user.bean.ProfileImgBean;
import com.bhartipay.user.bean.SmartCardBean;
import com.bhartipay.user.bean.TapAndPayBean;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.user.bean.UserBlockedBean;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.UserWalletConfigBean;
import com.bhartipay.user.bean.UserWishListBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletTapAndPay;
import com.bhartipay.user.persistence.WalletTapAndPayImpl;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.thirdParty.MachMovePerPaidCard;
import com.bhartipay.util.thirdParty.bean.MatchMoveBean;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.sun.jersey.core.header.FormDataContentDisposition;
//import com.sun.jersey.multipart.FormDataParam;

import appnit.com.crypto.RSAEncryptionWithAES;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * 
 * @author santosh.kumar
 * This class have the user related activity. 
 */

@Path("/WalletUser")

public class WalletUserManager {
	private static final Logger logger = Logger.getLogger(WalletUserManager.class.getName());
	WalletUserDao walletUserDao=new WalletUserDaoImpl();
	WalletTapAndPay walletTapAndPay=new WalletTapAndPayImpl();
	
/**
 * use for mobile validation
 * @param mobileno
 * @return String type code where  1000 for success  other for error
 */
@POST
@Path("/mobileValidation")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)

public String mobileValidation(@FormParam("mobileno") String mobileno){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|mobileValidation()|"
			+ mobileno);
	//logger.info("Start excution ===================== method mobileValidation(mobileno)");
	return walletUserDao.mobileValidation(mobileno);
}

/**
 * validate OTP use for validation the OTP
 * @param walletBean
 * @return Sting
 */
@POST
@Path("/validateOTP")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String validateOTP(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|validateOTP()"
			);
	//logger.info("Start excution ===================== method validateOTP(mobileno,otp)");
	return ""+walletUserDao.validateOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp());
}

@POST
@Path("/validateOTPFirstTime")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String validateOTPFirstTime(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|validateOTP()"
			);
	//logger.info("Start excution ===================== method validateOTP(mobileno,otp)");
	return ""+walletUserDao.validateOTPFirstTime(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp());
}

/**
 * customerValidateOTP use for merchant otp validation
 * @param walletBean
 * @return Sting
 */

@POST
@Path("/customerValidateOTP")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public LoginResponse customerValidateOTP(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|customerValidateOTP()"
			);
	//logger.info("Start excution ===================== method customerValidateOTP(mobileno,otp)");
	return walletUserDao.customerValidateOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp());
}


/**
 * verifyOTP use the validate OTP while reset password. 
 * @param walletBean
 * @return true or false
 */
@POST
@Path("/verifyOTP")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String verifyOTP(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|verifyOTP()"
			);
	//logger.info("Start excution ===================== method verifyOTP(mobileno,otp)");
	return ""+walletUserDao.verifyOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp());
}



/**
 * verifyOTP use the validate OTP while reset password. 
 * @param walletBean
 * @return true or false
 */
@POST
@Path("/verifyRefundOTP")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String verifyRefundOTP(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|verifyOTP()"
			);
	return ""+walletUserDao.verifyRefundOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp());
}


/**
 * 
 * otpResend use for resend the OTP
 * @param walletBean
 * @return true or false
 */
@POST
@Path("/otpResend")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String otpResend(WalletBean walletBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|otpResend()"
			);
	//logger.info("Start excution ===================== method otpResend(walletBean)"+walletBean.getUserId());
	//logger.info("Start excution ===================== method otpResend(walletBean)"+walletBean.getAggreatorid());
	return ""+walletUserDao.otpResend(walletBean.getUserId(),walletBean.getAggreatorid());
}

/**
 * signUpUserByUpload use for bulk user creation
 * @param imei
 * @param agent
 * @param walletMastBean
 *  @return String type code where  1000 for success  other for error
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/signUpUserByUpload")
public String signUpUserByUpload(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUserByUpload()"
			+agent+"|"+imei);
	/*logger.info("Start excution ===================== method signUpUser(walletMastBean)");
	logger.info("******************************agent***********************************"+agent);
	logger.info("******************************agent***********************************"+imei);*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.signUpUserByUpload(walletMastBean);
}


/**
 * signUpUser use for create the new user/aggreator
 * @param imei
 * @param agent
 * @param walletMastBean
 * @return String type code where  1000 for success  other for error
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/signup")
public String signUpUser(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
	
	
	//logger.info("Start excution ===================== method signUpUser(walletMastBean)");
	String descText="";
	try {
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletMastBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
	
	Gson gson=new Gson();
	WalletMastBean wBean=gson.fromJson(descText,WalletMastBean.class);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUser()|agent"
			+agent+"|imei "+imei);
	//logger.info("******************************agent***********************************"+agent);
	//logger.info("******************************agent***********************************"+imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.signUpUser(wBean);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "7000";
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/saveSignUpNewMember")
public String saveSignUpNewMember(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
	String descText="";
	try {
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletMastBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
	
	Gson gson=new Gson();
	WalletMastBean wBean=gson.fromJson(descText,WalletMastBean.class);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUser()|agent"+agent+"|imei "+imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.saveSignUpNewMember(wBean);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "7000";
}



/**
 * signUpUser use for Agent on board in B2B
 * @param imei
 * @param agent
 * @param walletMastBean
 * @return walletMastBean JSON object
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentOnBoard")
public WalletMastBean agentOnBoard(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoard()"+walletMastBean.getApplicationFee()+"|"
			+agent+"|"+imei);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+walletMastBean.getApplicationFee());
	/*logger.info("Start excution ===================== method agentOnBoad(walletMastBean)");
	logger.info("******************************agentOnBoad***********************************"+agent);
	logger.info("******************************agentOnBoad***********************************"+imei);
	logger.info("******************************agentOnBoad***********************************"+walletMastBean.getApplicationFee());*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.agentOnBoad(walletMastBean);
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateAgentOnBoard")
public WalletMastBean updateAgentOnBoard(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoard()"+walletMastBean.getApplicationFee()+"|"
			+agent+"|"+imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.updateAgentDetails(walletMastBean);
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/saveSelfAgentOnBoard")
public WalletMastBean saveSelfAgentOnBoard(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoard()"+walletMastBean.getApplicationFee()+"|"
			+agent+"|"+imei);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+walletMastBean.getApplicationFee());
	/*logger.info("Start excution ===================== method agentOnBoad(walletMastBean)");
	logger.info("******************************agentOnBoad***********************************"+agent);
	logger.info("******************************agentOnBoad***********************************"+imei);
	logger.info("******************************agentOnBoad***********************************"+walletMastBean.getApplicationFee());*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.saveSelfAgentOnBoard(walletMastBean);
}

/**
 * updateAgentOnBoad use for upload the agent document 
 * @param imei
 * @param agent
 * @param walletMastBean
 * @return walletMastBean JSON object
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateAgentOnBoad")
public WalletMastBean updateAgentOnBoad(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|updateAgentOnBoad()|"
			+agent+"|"+imei);
	/*logger.info("Start excution ===================== method agentOnBoad(walletMastBean)");
	logger.info("******************************agentOnBoad***********************************"+agent);
	logger.info("******************************agentOnBoad***********************************"+imei);*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.updateAgentOnBoad(walletMastBean);
}

/**
 * agentdetailsview use to get the agent details in B2B
 * @param walletMastBean
 * @return AgentDetailsView
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentdetailsview")
public AgentDetailsView getAgentDetails(WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentDetails()|"
			+"ID "+walletMastBean.getId());
	/*logger.info("Start excution ===================== method getAgentDetails(walletMastBean)");
	logger.info("******************************getAgentDetails***********************************"+walletMastBean.getId());
	logger.info("******************************getAgentDetails***********************************"+walletMastBean.getAggreatorid());*/
	return walletUserDao.getAgentDetails(walletMastBean.getId(),walletMastBean.getAggreatorid());
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/newBpayAgentdetailsview")
public AgentDetailsView newBpayAgentdetailsview(WalletMastBean walletMastBean){
	 
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentDetails()|"+"ID "+walletMastBean.getId());
	return walletUserDao.newBpayAgentdetailsview(walletMastBean.getId(),walletMastBean.getAggreatorid());
}



/**
 * editProfile use for update the customer and aggreator details
 * @param WalletMastBean
 * @return WalletMastBean
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/editProfile")
public WalletMastBean updateUserProfile(WalletMastBean WalletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),WalletMastBean.getAggreatorid(),WalletMastBean.getWalletid(),"", "", "", "|updateUserProfile()"
			);
	//logger.info("Start excution ===================== method updateUserProfile(walletDetailsBean)");
	return walletUserDao.updateUserProfile(WalletMastBean);
}

/**
 * showUserProfile use to get the customer details
 * @param walletBean
 * @return WalletMastBean
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/showUserProfile")
public WalletMastBean showUserProfile(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|showUserProfile()"
			);
	//logger.info("Start excution ===================== method showUserProfile(walletBean)");
	return walletUserDao.showUserProfile(walletBean.getUserId());
}


/**
 * updateUserWalletConfig use to update the velocity check parameter for customer/agent
 * @param userWalletConfigBean
 * @return UserWalletConfigBean
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/updateUserWalletConfig")
public UserWalletConfigBean updateUserWalletConfig(UserWalletConfigBean userWalletConfigBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|updateUserWalletConfig()"
			);
	//logger.info("Start excution ===================== method updateUserWalletConfig(walletDetailsBean)");
	return walletUserDao.updateUserWalletConfig(userWalletConfigBean);
}

/**
 * getUserWalletConfig use to get the velocity check parameter for customer/agent
 * @param userId
 * @return UserWalletConfigBean
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getUserWalletConfig")
public UserWalletConfigBean getUserWalletConfig(String userId){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getUserWalletConfig()"
			);
	//logger.info("Start excution ===================== method getUserWalletConfig(userId)");
	return walletUserDao.getUserWalletConfig(userId);
}

/**
 * login use for sign in the customer/agent/Aggreator
 * @param imei
 * @param agent
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/login")
public LoginResponse login( @HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|login()"
			);
	//logger.info("Start excution ===================== method login(walletBean)");
	String descText="";
	try {
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
	} catch (Exception e) {
		e.printStackTrace();
	}
	Gson gson=new Gson();
	WalletBean wBean=gson.fromJson(descText,WalletBean.class);
	
	return walletUserDao.login(/*wBean.getHceToken(),*/wBean.getUserId(),wBean.getPassword(),wBean.getAggreatorid(),imei,agent);
}

/**
 * ProfilebyloginId use the get the user details after sign in
 * @param walletBean
 * @return UserProfileBean
 */

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })

@Path("/ProfilebyloginId")
public UserProfileBean showUserProfilebymobile(WalletBean walletBean){
	
	System.out.println("_______________Inside WalletManager_______/profilebyLoginId_________");
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|showUserProfilebymobile()|"
			);
	//logger.info("Start excution ===================== method showUserProfilebymobile(walletBean)"+walletBean.getUserId());
	//logger.info("Start excution ===================== method showUserProfilebymobile(walletBean)"+walletBean.getAggreatorid());
	
	System.out.println("______getUserId__________"+walletBean.getUserId());
	System.out.println("______getAggreatorid__________"+walletBean.getAggreatorid());

	return walletUserDao.showUserProfilebymobile(walletBean.getUserId(),walletBean.getAggreatorid());
}




@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getDashBoardDMTData")
public String getDashBoardDMTData(WalletBean walletBean){
	
	System.out.println("--------------------inside getdashbordDMTdata_______________");
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|getDashBoardDMTData()|");
	Gson gson=new Gson();
	return gson.toJson(walletUserDao.getDashBoardDMTData(walletBean.getUserId(),walletBean.getAggreatorid()));
}


/**
 * 
 * @param imei
 * @param agent
 * @param request
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveKyc")
public String saveKyc(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request,WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|saveKyc()"
			+request.getContextPath().toString());
	//logger.info("Start excution ===================== method saveKyc(walletBean)");
	//logger.info("Start excution ===================== method saveKyc(request)"+request.getContextPath().toString());
	return walletUserDao.saveKyc(request.getSession().getServletContext().getRealPath("/"),walletBean.getUserId(),walletBean.getAddpkycid(),walletBean.getAddpdesc(),walletBean.getAddpkycpic(),walletBean.getIdpkycid(),walletBean.getIdpdesc(),walletBean.getIdpkycpic(),walletBean.getUserName(),walletBean.getMobileNo(),walletBean.getEmailId(),imei,agent);
	
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveSenderKyc")
public String saveSenderKyc(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request,UploadSenderKyc senderKyc){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"",senderKyc.getSenderEmailId(), "", "", "|saveSenderKyc()"
			+request.getContextPath().toString());
	return walletUserDao.saveSenderKyc(senderKyc,imei,agent);
	
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveSenderPhysicalKyc")
public String saveSenderKycFromApp(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request,UploadSenderKyc senderKyc){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"",senderKyc.getSenderEmailId(), "", "", "|saveSenderKyc()"
			+request.getContextPath().toString());
	return walletUserDao.saveSenderKycFromApp(senderKyc,imei,agent);
	
}

/**
 * changePassword use for change/update password for customer/agent
 * @param imei
 * @param httpHeaders
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/changePassword")
//public String changePassword(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpHeaders httpHeaders,WalletBean walletBean){
public String changePassword(@HeaderParam("IPIMEI") String imei,@Context HttpHeaders httpHeaders,WalletBean walletBean){
	
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changePassword()|imei"
			+imei+"|httpHeaders "+httpHeaders.getRequestHeader("user-agent"));
	//System.out.println("IMEI~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+imei);
	//System.out.println("IMEI~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+agent);
	//System.out.println("IMEI~~~~~~httpHeaders~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+httpHeaders.getRequestHeader("user-agent"));
	return walletUserDao.changePassword(walletBean.getUserId(),walletBean.getOldpassword(),walletBean.getPassword(),walletBean.getImeiIP());
}


/**
 * saveProfilePic use to save the user image
 * @param imei
 * @param agent
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveProfilePic")
public String saveProfilePic(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletBean walletBean ) {
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|saveProfilePic()|imei"
			+imei);
	return ""+walletUserDao.saveProfilePic(walletBean.getUserId(),walletBean.getProfilePic(),imei,agent);	
}

/**
 * getProfilePic use to get the user profile image 
 * @param profileImgBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getProfilePic")


public String getProfilePic(ProfileImgBean profileImgBean) {
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",profileImgBean.getUserId(), "", "", "|getProfilePic()");
	return walletUserDao.getProfilePic(profileImgBean.getUserId());
}

/**
 * userLogOut use for log off the user 
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/userLogOut")
public String userLogOut(WalletBean walletBean) {
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|userLogOut()"
			);
	return ""+walletUserDao.userLogOut(walletBean.getUserId());

}
/**
 * getCustomerByAgentId use for get the user under agent
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getCustomerByAgentId")
public String getCustomerByAgentId(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|getCustomerByAgentId()");
	//logger.info("Start excution ===================== method getCustomerByAgentId()");
	GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    return gson.toJson(walletUserDao.getCustomerByAgentId(walletBean.getUserId(),walletBean.getUserType()));
}

/**
 * getWalletConfigByUserId use for get the wallet configuration for specified user.
 * @param walletBean
 * @return UserWalletConfigBean
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getWalletConfigByUserId")
public UserWalletConfigBean getWalletConfigByUserId(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|getWalletConfigByUserId()"
			);
	//logger.info("Start excution ===================== method getWalletConfigByUserId()");
	 return walletUserDao.getWalletConfigByUserId(walletBean.getId(),walletBean.getTxnType(),walletBean.getWalletid());
}

/**
 * saveUserWalletConfig use for save or update the wallet configuration for specified user. 
 * @param imei
 * @param agent
 * @param userWalletConfigBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveUserWalletConfig")
public UserWalletConfigBean saveUserWalletConfig(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,UserWalletConfigBean userWalletConfigBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|saveUserWalletConfig()"
			);
	//logger.info("Start excution ===================== method getWalletConfigByUserId()");
	userWalletConfigBean.setIpiemi(imei);
	userWalletConfigBean.setAgent(agent);
	return walletUserDao.saveUserWalletConfig(userWalletConfigBean);
	
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveWishList")
public UserWishListBean saveWishList(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,UserWishListBean userWishListBean ){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|saveWishList()|Agnet "+agent
			);
	//logger.info("Start excution ===================== method saveWishList(userWishListBean)");
	userWishListBean.setIpiemi(imei);
	userWishListBean.setAgent(agent);
	return walletUserDao.saveWishList(userWishListBean);
	
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getWishList")
public String getWishList(UserWishListBean userWishListBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|getWishList()");
	//logger.info("Start excution ===================== method getWishList(userWishListBean)");
	GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    return gson.toJson(walletUserDao.getWishList(userWishListBean.getUserId()));
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/deleteWishList")
public String deleteWishList(UserWishListBean userWishListBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|deleteWishList()"
			);
	//logger.info("Start excution ===================== method deleteWishList(userWishListBean)");
	return ""+walletUserDao.deleteWishList(userWishListBean.getWishId());
}

/**
 * forgotPassword use to validate the user  and send OTP while forgot password. 
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/forgotPassword")
public String forgotPassword(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|forgotPassword()"
			);
	//logger.info("Start excution ===================== method forgotPassword( loginId)"+walletBean.getUserId());
	//logger.info("Start excution ===================== method forgotPassword( loginId)"+walletBean.getAggreatorid());
	 return walletUserDao.forgotPassword(walletBean.getUserId(),walletBean.getAggreatorid());
}

/**
 * setForgotPassword use to set the updated password
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/setForgotPassword")
public String setForgotPassword(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|setForgotPassword()"
			);
	//logger.info("Start excution ===================== method  loginId"+walletBean.getUserId());
//	logger.info("Start excution ===================== method password"+walletBean.getPassword());
	 return walletUserDao.setForgotPassword(walletBean.getUserId(),walletBean.getPassword(),walletBean.getAggreatorid());
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/forgotPasswordConfirm")
public String forgotPasswordConfirm(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|setForgotPassword()"
			);
	//logger.info("Start excution ===================== method  loginId"+walletBean.getUserId());
//	logger.info("Start excution ===================== method password"+walletBean.getPassword());
	 return walletUserDao.setForgotPassword(walletBean.getOtp(),walletBean.getUserId(),walletBean.getPassword(),walletBean.getAggreatorid());
}

/**
 * firstTimePasswordChange use to change the password while first time login in portal
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/firstTimePasswordChange")
public String firstTimePasswordChange(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|firstTimePasswordChange()"
			);
	//logger.info("Start excution **************************  firstTimePasswordChange **************Aggreatorid"+walletBean.getAggreatorid());
	//logger.info("Start excution **************************  firstTimePasswordChange **************UserId"+walletBean.getUserId());
	//logger.info("Start excution **************************  firstTimePasswordChange **************Oldpassword"+walletBean.getOldpassword());
	//logger.info("Start excution **************************  firstTimePasswordChange **************Password"+walletBean.getPassword());
	return walletUserDao.firstTimePasswordChange(walletBean.getAggreatorid(),walletBean.getUserId(),walletBean.getOldpassword(),walletBean.getPassword());
}



/**
 * getPendingAgentByAggId return the pending agent by aggreator
 * @param reportBean
 * @return String
 */
@POST
@Path("/getPendingAgentByAggId")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String getAgentByAggId(ReportBean reportBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "|getAgentByAggId()"
			);
	logger.info("Start excution ===================== method getAgentByAggId(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getAgentDetail(reportBean.getAggId()));
}



@POST
@Path("/newBpayAgent")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String newBpayAgent(ReportBean reportBean){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "|getAgentByAggId()");
	logger.info("Start excution ===================== method newBpayAgent(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.newBpayAgent(reportBean.getAggId()));
}




/**
 * acceptAgentByAgg use to accept the on board agent
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/acceptAgentByAgg")
public String acceptAgentByAgg(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|acceptAgentByAgg()|Id "+
			walletBean.getId()+"|UtrNo "+walletBean.getUtrNo());
	//logger.info("Start excution ===================== method  acceptAgentByAgg"+walletBean.getId());
	//logger.info("Start excution ===================== method  acceptAgentByAgg"+walletBean.getUtrNo());
	return ""+walletUserDao.acceptAgentByAgg(walletBean.getId(), walletBean.getUtrNo());
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/acceptNewAgentByAgg")
public String acceptNewAgentByAgg(WalletBean walletBean){
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "walletBean.getOtp() "+walletBean.getOtp(), "", "|acceptAgentByAgg()|Id "+walletBean.getId()+"|UtrNo "+walletBean.getUtrNo());
	
	return ""+walletUserDao.acceptNewAgentByAgg(walletBean.getId(), walletBean.getUtrNo(),walletBean.getOtp());
}

/**
 * rejectAgentByAgg use to reject the on board agent
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/rejectAgentByAgg")
public String rejectAgentByAgg(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|rejectAgentByAgg()"+"|rejectAgentByAgg "+walletBean.getDeclinedComment()+"|getDeclinedComment"+walletBean.getDeclinedComment());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|rejectAgentByAgg "+walletBean.getDeclinedComment());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|getDeclinedComment"+walletBean.getDeclinedComment());
	//logger.info("Start excution ===================== method  rejectAgentByAgg"+walletBean.getUserId());
	//logger.info("Start excution ===================== method  getDeclinedComment"+walletBean.getDeclinedComment());
	return ""+walletUserDao.rejectAgentByAgg(walletBean.getCreatedBy(), walletBean.getUserId(),walletBean.getDeclinedComment());
}

/**
 * validateChangeMobile use to validate the mobile while changing
 * @param walletBean
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/validateChangeMobile")
public String validateChangeMobile(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|validateChangeMobile()|MobileNo "+walletBean.getMobileNo());
	
	//logger.info("Start excution ===================== method  validateChangeMobile"+walletBean.getUserId());
	return walletUserDao.validateChangeMobile(walletBean.getUserId(), walletBean.getAggreatorid(), walletBean.getMobileNo());
}


/**
 * changeOtpResend use to send the otp
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/changeOtpResend")
public String changeOtpResend(WalletBean walletBean) {
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changeOtpResend()|MobileNo "+walletBean.getMobileNo());
	
	//logger.info("Start excution ===================== method  validateChangeMobile"+walletBean.getUserId());
	return ""+walletUserDao.changeOtpResend(walletBean.getUserId(), walletBean.getAggreatorid(), walletBean.getMobileNo());
	
}

/**
 * changeMobileNo use for update the mobile no
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/changeMobileNo")
public String changeMobileNo(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changeMobileNo()|MobileNo "+walletBean.getMobileNo());
	
	//logger.info("Start excution ===================== method  changeMobileNo"+walletBean.getUserId());
	return  ""+walletUserDao.changeMobileNo(walletBean.getUserId(), walletBean.getAggreatorid(), walletBean.getMobileNo(),walletBean.getOtp());
}

/**
 * validateChangeEmail use for validate the mail id while updating the email
 * @param walletBeana
 * @return String
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/validateChangeEmail")
public String validateChangeEmail(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|validateChangeEmail()|MobileNo "+walletBean.getMobileNo());
	
	//logger.info("Start excution ===================== method  validateChangeEmail"+walletBean.getUserId());
	return walletUserDao.validateChangeEmail(walletBean.getUserId(),walletBean.getAggreatorid(),walletBean.getEmailId());
}

/**
 * changeEmailId use for update the email id
 * @param walletBean
 * @return
 */
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/changeEmailId")
public String changeEmailId(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changeEmailId()|MobileNo "+walletBean.getMobileNo());
	
	//logger.info("Start excution ===================== method  changeEmailId"+walletBean.getUserId());
	return ""+ walletUserDao.changeEmailId(walletBean.getUserId(),walletBean.getAggreatorid(),walletBean.getEmailId(),walletBean.getOtp());
}

/**
 * 
 * @param imei
 * @param agent
 * @param smartCardBean
 * @return
 */
@POST
@Path("/generateSmartCardReq")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
 public SmartCardBean generateSmartCardReq(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|generateSmartCardReq()");
	
	//logger.info("Start excution ************************************ method generateSmartCardReq(smartCardBean)"+smartCardBean.getWalletId());
	 smartCardBean.setIpiemi(imei);
	 smartCardBean.setAgent(agent);
	 return walletUserDao.generateSmartCardReq(smartCardBean); 
 }

/**
 * 
 * @param smartCardBean
 * @return
 */
@POST
@Path("/getSmartCardReq")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getSmartCardReq(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|getSmartCardReq()");
	
	//logger.info("Start excution ************************************ method getSmartCardReq(aggreatorId)"+smartCardBean.getAggreatorId());
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson( walletUserDao.getSmartCardReq(smartCardBean.getAggreatorId())); 
}


@POST
@Path("/rejecttSmartCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String rejecttSmartCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|rejecttSmartCard()| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ****************************************************** method rejecttSmartCard()"+smartCardBean.getReqId());
	return ""+walletUserDao.rejecttSmartCard(smartCardBean.getReqId());
	
}

@POST
@Path("/acceptSmartCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public SmartCardBean acceptSmartCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|acceptSmartCard()| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ****************************************************** method acceptSmartCard()"+smartCardBean.getReqId());
	return walletUserDao.acceptSmartCard(smartCardBean.getReqId());
}


@POST
@Path("/getdispatchCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getdispatchCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|getdispatchCard| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ************************************ method getSmartCardReq(aggreatorId)"+smartCardBean.getAggreatorId());
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson( walletUserDao.getDispatchCard(smartCardBean.getAggreatorId())); 
}

@POST
@Path("/dispatchCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean dispatchCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|dispatchCard()| "+smartCardBean.getReqId()+ smartCardBean.getPrePaidCardNumber()+smartCardBean.getPrePaidCardPin());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", smartCardBean.getPrePaidCardNumber());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", smartCardBean.getPrePaidCardPin());
	//logger.info("Start excution ****************************************************** method DispatchCard()"+smartCardBean.getReqId());
	//logger.info("start excution*********************************************************** method getDispatchCard(aggreatorId)"+smartCardBean.getPrePaidCardNumber());
	//logger.info("start excution*********************************************************** method getDispatchCard(aggreatorId)"+smartCardBean.getPrePaidCardPin());
	return walletUserDao.dispatchCard(smartCardBean.getReqId(),smartCardBean.getPrePaidCardNumber(),smartCardBean.getPrePaidCardPin());
}

@POST
@Path("/getPrePaidCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean getPrePaidCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|getPrePaidCard()");
	
	//logger.info("start excution*********************************************************** method getPrePaidCard(aggreatorId)"+smartCardBean.getUserId());
	return walletUserDao.getPrePaidCard(smartCardBean.getUserId());
}

@POST
@Path("/linkCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean linkCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|linkCard()| "+smartCardBean.getReqId()+smartCardBean.getPrePaidCardNumber()+smartCardBean.getPrePaidCardPin());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", smartCardBean.getPrePaidCardNumber());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", smartCardBean.getPrePaidCardPin());
	
	//logger.info("Start excution ****************************************************** method linkCard()"+smartCardBean.getReqId());
	//logger.info("start excution*********************************************************** method linkCard(aggreatorId)"+smartCardBean.getPrePaidCardNumber());
	//logger.info("start excution*********************************************************** method linkCard(aggreatorId)"+smartCardBean.getPrePaidCardPin());
	return walletUserDao.linkCard(smartCardBean.getReqId(),smartCardBean.getPrePaidCardNumber(),smartCardBean.getPrePaidCardPin());
}


@POST
@Path("/linkVirtualCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean linkVirtualCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|linkVirtualCard()| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ****************************************************** method linkVirtualCard()"+smartCardBean.getReqId());
	return walletUserDao.linkVirtualCard(smartCardBean.getReqId());
}



@POST
@Path("/resumePrePaidCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean resumePrePaidCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|resumePrePaidCard()| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ****************************************************** method linkCard()"+smartCardBean.getReqId());
	return walletUserDao.resumePrePaidCard(smartCardBean.getReqId());
}


//@DELETE
@POST
@Path("/linkCardBlocked")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean linkCardBlocked(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|linkCardBlocked()| "+smartCardBean.getReqId()+smartCardBean.getBlockedType());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+smartCardBean.getBlockedType());
	
	//logger.info("Start excution ****************************************************** method linkCardBlocked()"+smartCardBean.getReqId());
	//logger.info("start excution*********************************************************** method linkCardBlocked(blockedType)"+smartCardBean.getBlockedType());
	return walletUserDao.linkCardBlocked(smartCardBean.getReqId(),smartCardBean.getBlockedType());
	}


@POST
@Path("/deleteUser")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean deleteUser(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|deleteUser()| "+smartCardBean.getReqId()+"|"+smartCardBean.getState());
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+smartCardBean.getState());
	
	//logger.info("Start excution ****************************************************** method deleteUser()"+smartCardBean.getReqId());
	//logger.info("start excution*********************************************************** method deleteUser(State)"+smartCardBean.getState());
	return walletUserDao.deleteUser(smartCardBean.getReqId(),smartCardBean.getState());
	}


@POST
@Path("/suspendedCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean suspendedCard(SmartCardBean smartCardBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|suspendedCard()| "+smartCardBean.getReqId());
	
	//logger.info("Start excution ****************************************************** method suspendedCardBlocked()"+smartCardBean.getReqId());
	return walletUserDao.suspendedCard(smartCardBean.getReqId());
	}



@POST
@Path("/tapAndPayReq")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public TapAndPayBean getTapAndPayReq(TapAndPayBean tapAndPayBean) throws NoSuchAlgorithmException{
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),tapAndPayBean.getAggreatorId(),"",tapAndPayBean.getUserId(), "", "", "|getTapAndPayReq()| "+tapAndPayBean.getRequest());
	
	//logger.info("Start excution ****************************************************** method getTapAndPayReq(tapAndPayBean)"+tapAndPayBean.getAggreatorId());
	//logger.info("start excution*********************************************************** method getTapAndPayReq(tapAndPayBean)"+tapAndPayBean.getRequest());
	return walletTapAndPay.getTapAndPayReq(tapAndPayBean.getAggreatorId(), tapAndPayBean.getRequest());
}

@POST
@Path("/getUserCardId")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public TapAndPayBean getUserCardId(TapAndPayBean tapAndPayBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),tapAndPayBean.getAggreatorId(),"",tapAndPayBean.getUserId(), "", "", "|getUserCardId()| TokenId"+tapAndPayBean.getTokenId());
	
	//logger.info("Start excution ****************************************************** method getUserCardId(tapAndPayBean)"+tapAndPayBean.getTokenId());
	//logger.info("start excution*********************************************************** method getUserCardId(tapAndPayBean)"+tapAndPayBean.getUserId());
	return walletTapAndPay.getUserCardId(tapAndPayBean.getTokenId());
}


/**
 * 
 * @return
 */
@POST
@Path("/getUnAuthorizedAggreator")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getUnAuthorizedAggreator(){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|getUnAuthorizedAggreator()");
	
	logger.info("Start excution ************************************ method getUnAuthorizedAggreator");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson( walletUserDao.getUnAuthorizedAggreator()); 
}


@POST
@Path("/authorizedAggreator")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String authorizedAggreator(WalletBean walletBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|authorizedAggreator()");
	
	//logger.info("Start excution ************************************ method AuthorizedAggreator(aggreatorId)"+walletBean.getUserId());	
	return walletUserDao.authorizedAggreator(walletBean.getUserId());
}





@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/signupios")
public String signupios(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signupios()|agent  "+agent+"|imei  "+imei);
	
	//logger.info("Start excution ===================== method signupios(walletMastBean)");
	//logger.info("******************************agent*****************signupios******************"+agent);
	//logger.info("******************************agent******************signupios*****************"+imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	
	String descText="";
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletMastBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletMastBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletMastBean.getAggreatorid()));
	
	Gson gson=new Gson();
	WalletMastBean wBean=gson.fromJson(descText,WalletMastBean.class);
	
		json.put("status", walletUserDao.signUpUser(wBean));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
	
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/loginios")
public String loginios( @HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|loginios()");
	
	//logger.info("Start excution ===================== method loginios(walletBean)");
	String descText="";
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
	
	Gson gson=new Gson();
	WalletBean wBean=gson.fromJson(descText,WalletBean.class);
	json.put("status",  walletUserDao.login(wBean.getUserId(),wBean.getPassword(),wBean.getAggreatorid(),imei,agent));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
}



@POST
@Path("/validateOTPios")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String validateOTPios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|validateOTPios()");
	
	//logger.info("Start excution ===================== method validateOTPios(mobileno,otp)");
	json.put("status",  ""+walletUserDao.validateOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
	
}


@POST
@Path("/otpResendios")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String otpResendios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|otpResendios()");
	
	//logger.info("Start excution ===================== method otpResendios(walletBean)"+walletBean.getUserId());
	//logger.info("Start excution ===================== method otpResendios(walletBean)"+walletBean.getAggreatorid());
	json.put("status",  walletUserDao.otpResend(walletBean.getUserId(),walletBean.getAggreatorid()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/firstTimePasswordChangeios")
public String firstTimePasswordChangeios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|firstTimePasswordChangeios()");
	
	//logger.info("Start excution **************************  firstTimePasswordChangeios **************Aggreatorid"+walletBean.getAggreatorid());
	//logger.info("Start excution **************************  firstTimePasswordChangeios **************UserId"+walletBean.getUserId());
	//logger.info("Start excution **************************  firstTimePasswordChangeios **************Oldpassword"+walletBean.getOldpassword());
	//logger.info("Start excution **************************  firstTimePasswordChangeios **************Password"+walletBean.getPassword());
	json.put("status",  walletUserDao.firstTimePasswordChange(walletBean.getAggreatorid(),walletBean.getUserId(),walletBean.getOldpassword(),walletBean.getPassword()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
	
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getProfilePicios")
public String getProfilePicios(ProfileImgBean profileImgBean) {
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",profileImgBean.getUserId(), "", "", "|getProfilePicios()");
	
	//logger.info("Start excution **************************  getProfilePicios **************getUserId"+profileImgBean.getUserId());
	json.put("status", walletUserDao.getProfilePic(profileImgBean.getUserId()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/saveProfilePicios")
public String saveProfilePicios(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletBean walletBean ) {
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|saveProfilePicios()");
	
	//logger.info("Start excution **************************  saveProfilePicios **************getUserId"+walletBean.getUserId());
	json.put("status", walletUserDao.saveProfilePic(walletBean.getUserId(),walletBean.getProfilePic(),imei,agent));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/userLogOutios")
public String userLogOutios(WalletBean walletBean) {
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|userLogOutios()");
	
	//logger.info("Start excution **************************  userLogOutios **************getUserId"+walletBean.getUserId());
	json.put("status", walletUserDao.userLogOut(walletBean.getUserId()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/changePasswordios")
public String changePasswordios(@HeaderParam("IPIMEI") String imei,@Context HttpHeaders httpHeaders,WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changePasswordios()|imei"+imei+"|httpHeaders"+httpHeaders.getRequestHeader("user-agent"));
	
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|"+methodname+"|httpHeaders"+httpHeaders.getRequestHeader("user-agent"));
	
	//System.out.println("IMEI~~~~~~~~~~~~~~~~~~changePasswordios~~~~~~~~~~~~~~~~~`"+imei);
	//System.out.println("IMEI~~~~~~httpHeaders~~~~~~~~~~changePasswordios~~~~~~~~~~~~~~~~~~~`"+httpHeaders.getRequestHeader("user-agent"));
	json.put("status", walletUserDao.changePassword(walletBean.getUserId(),walletBean.getOldpassword(),walletBean.getPassword(),walletBean.getImeiIP()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString();
	
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/forgotPasswordios")
public String forgotPasswordios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|forgotPasswordios()");
	
	//logger.info("Start excution ===================== method forgotPasswordios( loginId)"+walletBean.getUserId());
	//logger.info("Start excution ===================== method forgotPasswordios( loginId)"+walletBean.getAggreatorid());
	json.put("status", walletUserDao.forgotPassword(walletBean.getUserId(),walletBean.getAggreatorid()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString(); 
}



@POST
@Path("/verifyOTPios")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String verifyOTPios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|verifyOTPios()");
	
	//logger.info("Start excution ===================== method verifyOTPios(mobileno,otp)");
	json.put("status", walletUserDao.verifyOTP(walletBean.getUserId(),walletBean.getAggreatorid(), walletBean.getOtp()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString(); 
	
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/setForgotPasswordios")
public String setForgotPasswordios(WalletBean walletBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|setForgotPasswordios()");
	
	//logger.info("Start excution =============setForgotPasswordios======== method  loginId"+walletBean.getUserId());
	//logger.info("Start excution =============setForgotPasswordios======== method password"+walletBean.getPassword());
	json.put("status", walletUserDao.setForgotPassword(walletBean.getUserId(),walletBean.getPassword(),walletBean.getAggreatorid()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString(); 
	 
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/deleteWishListios")
public String deleteWishListios(UserWishListBean userWishListBean){
	JSONObject json=new JSONObject();
	try {
	json.put("status", "1001");
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),"", "", "", "|deleteWishListios()");
	
	//logger.info("Start excution ===============deleteWishListios====== method deleteWishListios(userWishListBean)");
	json.put("status", walletUserDao.deleteWishList(userWishListBean.getWishId()));
	} catch (Exception e) {
	e.printStackTrace();
	}
	return json.toString(); 
	
	
}



@POST
@Path("/getSmartCardList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getSmartCardList(SmartCardBean smartCardBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|getSmartCardList()");
	
	//logger.info("Start excution ************************************ method getSmartCardList(aggreatorId)"+smartCardBean.getAggreatorId());
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson( walletUserDao.getSmartCardList(smartCardBean.getAggreatorId())); 
}



@POST
@Path("/fetchingCreatedUserdetail")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String fetchingCreatedUserdetail(SmartCardBean smartCardBean){
	JSONObject json=new JSONObject();
	try {
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|fetchingCreatedUserdetail()|"+smartCardBean.getReqId());
		
	//logger.info("Start excution ************************************ method fetchingCreatedUserdetail(aggreatorId)"+smartCardBean.getReqId());
	MatchMoveBean matchMoveBean=new MachMovePerPaidCard().fetchingCreatedUserdetail(smartCardBean.getReqId());
	
	json.put("firstName", matchMoveBean.getName().getFirst());
	json.put("lastName", matchMoveBean.getName().getLast());
	json.put("preferredName", matchMoveBean.getName().getPreferred());
	json.put("mobileNumber", matchMoveBean.getMobile().getNumber());
	json.put("email", matchMoveBean.getEmail());
	json.put("activeDate", matchMoveBean.getDate().getRegistration());
	json.put("status", matchMoveBean.getStatus().getText());
		
	} catch (Exception e) {
	e.printStackTrace();
	}
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+json.toString());
	
	//logger.info("matchMoveBean ********************fetchingCreatedUserdetail*************** method "+json.toString());
	return json.toString(); 
	
}


@POST
@Path("/updatePrePaidCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean updatePrePaidCard(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|updatePrePaidCard()|"+smartCardBeanin.getReqId());
	
	//logger.info("Start excution ************************************ method updatePrePaidCard(smartCardBeanin)"+smartCardBeanin.getReqId());
	if(smartCardBeanin.getReqId()==null ||smartCardBeanin.getReqId().isEmpty()){
		smartCardBeanin.setStatusDesc("Request Id is empty.");
		smartCardBeanin.setStatusCode("7777");
		return smartCardBeanin;
	}
	return new MachMovePerPaidCard().updatePrePaidCard(smartCardBeanin);
}


@POST
@Path("/updateAddress")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean updateAddress(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|updateAddress()|"+smartCardBeanin.getReqId());
	
	//logger.info("Start excution ************************************ method updateAddress(smartCardBeanin)"+smartCardBeanin.getReqId());
	if(smartCardBeanin.getReqId()==null ||smartCardBeanin.getReqId().isEmpty()){
		smartCardBeanin.setStatusDesc("Request Id is empty.");
		smartCardBeanin.setStatusCode("7777");
		return smartCardBeanin;
	}
	return new MachMovePerPaidCard().updateAddress(smartCardBeanin);
}


@POST
@Path("/updateKYCStatus")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean updateKYCStatus(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|updateKYCStatus()|"+smartCardBeanin.getReqId());
	
	//logger.info("Start excution ************************************ method updateKYCStatus(smartCardBeanin)"+smartCardBeanin.getReqId());
	if(smartCardBeanin.getReqId()==null ||smartCardBeanin.getReqId().isEmpty()){
		smartCardBeanin.setStatusDesc("Request Id is empty.");
		smartCardBeanin.setStatusCode("7777");
		return smartCardBeanin;
	}
	return new MachMovePerPaidCard().updateKYCStatus(smartCardBeanin);
}




@POST
@Path("/fetchingWalletdetail")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String fetchingWalletdetail(SmartCardBean smartCardBean){
	JSONObject json=new JSONObject();
	try {
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|fetchingWalletdetail()|"+smartCardBean.getReqId());
		
	//logger.info("Start excution ************************************ method fetchingWalletdetail(aggreatorId)"+smartCardBean.getReqId());
	MatchMoveBean matchMoveBean=new MachMovePerPaidCard().fetchingWalletdetail(smartCardBean.getReqId());
	json.put("holderName", matchMoveBean.getHolder().getName());
	json.put("cardNumber", matchMoveBean.getNumber());
	json.put("fundAvailable", matchMoveBean.getFunds().getAvailable().getAmount()+" "+matchMoveBean.getFunds().getAvailable().getCurrency());
	json.put("fundWithholding", matchMoveBean.getFunds().getWithholding().getAmount()+" "+matchMoveBean.getFunds().getWithholding().getCurrency());
	json.put("status", matchMoveBean.getStatus().getText());
	} catch (Exception e) {
	e.printStackTrace();
	}
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+json.toString());
	
	//logger.info("matchMoveBean ********************fetchingWalletdetail*************** method "+json.toString());
	return json.toString(); 
	
}

@POST
@Path("/fetchingCardType")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String fetchingCardType(SmartCardBean smartCardBean){
	JSONObject json=new JSONObject();
	
    
	try {
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|fetchingCardType()|"+smartCardBean.getReqId());
		
	//logger.info("Start excution ************************************ method fetchingCardType(aggreatorId)"+smartCardBean.getReqId());
	MatchMoveBean matchMoveBean=new MachMovePerPaidCard().fetchingCardType(smartCardBean.getReqId());
	json.put("cardCodeName", matchMoveBean.getCode());
	json.put("cardName", matchMoveBean.getCardName());
	json.put("description", matchMoveBean.getDescription());
	json.put("holdaerName", matchMoveBean.getHolderName());
	json.put("cardNumber", matchMoveBean.getCardNumber());
	json.put("expiry", matchMoveBean.getExpiry());
	
	json.put("reqId", smartCardBean.getReqId());	
	
	
	
	} catch (Exception e) {
		e.printStackTrace();
		}
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+json.toString());
	
		//logger.info("matchMoveBean ********************fetchingCardType*************** method "+json.toString());
		return json.toString(); 
}



@POST
@Path("/fetchingCardTypeCode")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String fetchingCardTypeCode(SmartCardBean smartCardBean){
	JSONObject json=new JSONObject();
	
    
	try {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|fetchingCardTypeCode()|"+smartCardBean.getReqId());
		
	//logger.info("Start excution ************************************ method fetchingCardTypeCode(aggreatorId)"+smartCardBean.getReqId());
	MatchMoveBean matchMoveBean=new MachMovePerPaidCard().fetchingCardTypeCode(smartCardBean.getReqId());
	json.put("cardCodeName", matchMoveBean.getType().getType());
	json.put("cardName", matchMoveBean.getType().getName());
	json.put("description", matchMoveBean.getType().getDescription());
	json.put("cardNumber", matchMoveBean.getNumber());
	json.put("status", matchMoveBean.getStatus().getText());
	json.put("reqId", smartCardBean.getReqId());	
	} catch (Exception e) {
		e.printStackTrace();
	}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+json.toString());
		
	//logger.info("matchMoveBean ********************fetchingCardTypeCode*************** method "+json.toString());
	return json.toString(); 
}

@POST
@Path("/updatePassword")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean updatePassword(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|updatePassword()|"+smartCardBeanin.getReqId());
	
	//logger.info("Start excution ************************************ method updatePassword(aggreatorId)"+smartCardBeanin.getReqId());
	//logger.info("Start excution ************************************ method updatePassword(aggreatorId)"+smartCardBeanin.getPassowrd());
	return new MachMovePerPaidCard().updatePassword(smartCardBeanin);
}


@POST
@Path("/generateCVV")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean generateCVV(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|generateCVV()|"+smartCardBeanin.getReqId());
	
	//logger.info("Start excution ************************************ method generateCVV(getReqId)"+smartCardBeanin.getReqId());
	return new MachMovePerPaidCard().generateCVV(smartCardBeanin);
	
}

@POST
@Path("/pinReset")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public SmartCardBean pinReset(SmartCardBean smartCardBeanin){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|pinReset()|"+smartCardBeanin.getReqId()+"|"+smartCardBeanin.getChangePinMode());
   // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBeanin.getAggreatorId(),smartCardBeanin.getWalletId(),smartCardBeanin.getUserId(), "", "", "|"+methodname+"|"+smartCardBeanin.getChangePinMode());
	
	//logger.info("Start excution ************************************ method pinReset(getReqId)"+smartCardBeanin.getReqId());
	//logger.info("Start excution ************************************ method pinReset(getReqId)"+smartCardBeanin.getChangePinMode());
	return new MachMovePerPaidCard().pinReset(smartCardBeanin);
}


public  String activatePhysicalCard (SmartCardBean smartCardBean){
	JSONObject json=new JSONObject();
	
    
	try {
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|activatePhysicalCard()|"+smartCardBean.getReqId()+"|"+smartCardBean.getReqId());
	   // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+methodname+"|"+smartCardBean.getReqId());
	    
	//logger.info("Start excution ************************************ method activatePhysicalCard(aggreatorId)"+smartCardBean.getReqId());
	MatchMoveBean matchMoveBean=new MachMovePerPaidCard().activatePhysicalCard(smartCardBean.getReqId(),smartCardBean.getActivationCode());
	
	} catch (Exception e) {
		e.printStackTrace();
	}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|"+json.toString());
	    
	//logger.info("matchMoveBean ********************activatePhysicalCard*************** method "+json.toString());
	return json.toString(); 
	
}


@POST
@Path("/getRejectAgentDetail")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getRejectAgentDetail(ReportBean reportBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "|getRejectAgentDetail()");
	   
	//logger.info("Start excution ===================== method getRejectAgentDetail(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getRejectAgentDetail(reportBean.getAggId(),reportBean.getCreatedBy()));
}

@POST
@Path("/editAgent")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public WalletMastBean editAgent(WalletMastBean walletMastBean) {
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|editAgent()|"+walletMastBean.getId());
	 
	//logger.info("Start excution ************************************ method editAgent*******"+walletMastBean.getId());
	return walletUserDao.editAgent(walletMastBean);

}

@POST
@Path("/editAgentPending")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public WalletMastBean editAgentPending(WalletMastBean walletMastBean) {
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|editAgent()|"+walletMastBean.getId());
	 
	//logger.info("Start excution ************************************ method editAgent*******"+walletMastBean.getId());
	return walletUserDao.editAgentPending(walletMastBean);

}

@POST
@Path("/getAgentList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getAgentList(WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentList()");
	 
	//logger.info("Start excution ===================== method agentList(walletMastBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getAgentList(walletMastBean));
}


@POST
@Path("/getApprovedAgentList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getApprovedAgentList(WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getApprovedAgentList()");
	 
	//logger.info("Start excution ===================== method getApprovedAgentList(walletMastBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getApprovedAgentList(walletMastBean));
}


@POST
@Path("/getRejectAgentList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getRejectAgentList(WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getRejectAgentList()");
	 
	//logger.info("Start excution ===================== method getRejectAgentList(walletMastBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getRejectAgentList(walletMastBean));
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/blockUnblockUser")
public String blockUnblockUser(UserBlockedBean userBlockedBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|blockUnblockUser()|AgentId "+userBlockedBean.getAgentId()+"|Status "+userBlockedBean.getStatus());
   // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|"+methodname+"|Status "+userBlockedBean.getStatus());
	 
	//logger.info("Start excution ========getUserId============= method blockUnblockUser( )"+userBlockedBean.getAgentId());
	//logger.info("Start excution ========getUserId============= method blockUnblockUser( )"+userBlockedBean.getStatus());
	 return walletUserDao.blockUnblockUser(userBlockedBean);
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/distributerOnBoad")
public String distributerOnBoad(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad()|agent"+agent+"|imei"+imei);
	 
/*	logger.info("Start excution ===================== method signUpUser(walletMastBean)");
	logger.info("******************************agent***********************************"+agent);
	logger.info("******************************agent***********************************"+imei);*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.distributerOnBoad(walletMastBean);
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/userOnBoad")
public String userOnBoad(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad()|agent"+agent+"|imei"+imei);
	 
/*	logger.info("Start excution ===================== method signUpUser(walletMastBean)");
	logger.info("******************************agent***********************************"+agent);
	logger.info("******************************agent***********************************"+imei);*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.userOnBoad(walletMastBean);
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/allUserOnBoad")
public String allUserOnBoad(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|allUserOnBoad()|agent"+agent+"|imei"+imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.allUserOnBoad(walletMastBean);
}


@POST
@Consumes(value = { MediaType.APPLICATION_JSON })
@Produces(value = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/allUserOnBoad1")
public String allUserOnBoardDeep(@HeaderParam("IPIMEI") String imei, @HeaderParam("AGENT") String agent,
		WalletMastBean walletMastBean, @Context HttpServletRequest request) {

	
	String filePath = request.getServletContext().getRealPath("/").concat("AGENTDOC");
	System.out.println("FILE PATH ----   " + filePath);

	File dir = new File(filePath, walletMastBean.getPan());
	boolean mkdir = dir.mkdir();
	filePath = filePath.concat("/" + walletMastBean.getPan());

	try {
		byte b1[] = Base64.decodeBase64(walletMastBean.getBase64_1());

		File fileToCreate1 = new File(filePath, walletMastBean.getFile1FileName());
		// FileUtils.copyFile(walletBean.getFile2(), fileToCreate1);
		FileOutputStream f1 = new FileOutputStream(fileToCreate1);
		f1.write(b1);
		f1.flush();
		f1.close();
		walletMastBean
				.setIdLoc("./BhartiPayService/AGENTDOC/" + walletMastBean.getPan() + "/" + walletMastBean.getFile1FileName());
		Logger.getLogger("deep log address location").warn(walletMastBean.getAddressLoc());
	} catch (Exception e) { // TODO Auto-generated catch block
		e.printStackTrace();
	}

	try {
		byte b2[] = Base64.decodeBase64(walletMastBean.getBase64_2());

		File fileToCreate2 = new File(filePath, walletMastBean.getFile2FileName());
		// FileUtils.copyFile(walletBean.getFile2(), fileToCreate1);
		FileOutputStream f2 = new FileOutputStream(fileToCreate2);
		f2.write(b2);
		f2.flush();
		f2.close();
		walletMastBean
				.setAddressLoc("./BhartiPayService/AGENTDOC/" + walletMastBean.getPan() + "/" + walletMastBean.getFile2FileName());
		Logger.getLogger("deep log address location").warn(walletMastBean.getAddressLoc());
	} catch (Exception e) { // TODO Auto-generated catch block
		e.printStackTrace();
	}

	try {
		byte b3[] = Base64.decodeBase64(walletMastBean.getBase64_3());

		File fileToCreate3 = new File(filePath, walletMastBean.getFile3FileName());
		// FileUtils.copyFile(walletBean.getFile2(), fileToCreate1);
		FileOutputStream f3 = new FileOutputStream(fileToCreate3);
		f3.write(b3);
		f3.flush();
		f3.close();
		walletMastBean
				.setPassbookLoc("./BhartiPayService/AGENTDOC/" + walletMastBean.getPan() + "/" + walletMastBean.getFile3FileName());
		Logger.getLogger("deep log address location").warn(walletMastBean.getAddressLoc());
	} catch (Exception e) { // TODO Auto-generated catch block
		e.printStackTrace();
	}

	try {
		byte b4[] = Base64.decodeBase64(walletMastBean.getBase64_4());

		File fileToCreate4 = new File(filePath, walletMastBean.getFile4FileName());
		// FileUtils.copyFile(walletBean.getFile2(), fileToCreate1);
		FileOutputStream f4 = new FileOutputStream(fileToCreate4);
		f4.write(b4);
		f4.flush();
		f4.close();
		walletMastBean
				.setPaymentLoc("./BhartiPayService/AGENTDOC/" + walletMastBean.getPan() + "/" + walletMastBean.getFile4FileName());
		Logger.getLogger("deep log address location").warn(walletMastBean.getAddressLoc());
	} catch (Exception e) { // TODO Auto-generated catch block
		e.printStackTrace();
	}

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
			walletMastBean.getWalletid(), "", "", "", "|allUserOnBoad()|agent" + agent + "|imei" + imei);
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.allUserOnBoad(walletMastBean);
}




@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/distributerOnBoardList")
public List<WalletMastBean> distributerOnBoardList(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletMastBean walletMastBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoardList()|agent"+agent+"|imei"+imei);
	 
	/*logger.info("Start excution ===================== method signUpUser(walletMastBean)");
	logger.info("******************************agent***********************************"+agent);
	logger.info("******************************agent***********************************"+imei);*/
	walletMastBean.setIpiemi(imei);
	walletMastBean.setAgent(agent);
	return walletUserDao.distributerOnBoardList(walletMastBean);
}




@POST
@Path("/getAgentDetailForApprove")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String getAgentDetailForApprove(ReportBean reportBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "|getAgentDetailForApprove()");
	 
	//logger.info("Start excution ===================== method getAgentByAggId(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getAgentDetailForApprove(reportBean.getAggId()));
}

@POST
@Path("/wLCashApprover")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String wLCashApprover(WalletMastBean walletBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|wLCashApprover()");
	 
	//logger.info("Start excution ===================== method wLCashApprover(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.wLCashApprover(walletBean));
}

@POST
@Path("/updateCashRequestStatus")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String updateCashRequestStatus(WalletMastBean walletBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|updateCashRequestStatus()");
	 
	//logger.info("Start excution ===================== method updateCashRequestStatus(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.updateCashRequestStatus(walletBean));
}
@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/approvedAgentByAgg")
public String approvedAgentByAgg(WalletBean walletBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|approvedAgentByAgg()|"+walletBean.getId()+"|"+walletBean.getUtrNo());
    //Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|"+methodname+"|"+walletBean.getUtrNo());
	//logger.info("Start excution ===================== method  acceptAgentByAgg"+walletBean.getId());
	//logger.info("Start excution ===================== method  acceptAgentByAgg"+walletBean.getUtrNo());
	return ""+walletUserDao.approvedAgentByAgg(walletBean.getId(), walletBean.getUtrNo());
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/getUserByMobileNo")
public WalletMastBean getUserByMobileNo(WalletMastBean req){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),req.getAggreatorid(),req.getWalletid(),"", "", "", "|getUserByMobileNo()|"+req.getMobileno());
   
	//logger.info("Start excution ===================== method  getUserByMobileNo"+req.getMobileno());
	//logger.info("Start excution ===================== method  getUserByMobileNo"+ req.getAggreatorid() );
	return walletUserDao.getUserByMobileNo(req.getMobileno() , req.getAggreatorid() ,"");
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/loginMerchant")
public LoginResponse loginMerchant( @HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,WalletBean walletBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|loginMerchant()");
   
	//logger.info("Start excution ===================== method login(walletBean)");
	String descText="";
	try {
		String encKey=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncKey(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
		descText=RSAEncryptionWithAES.decryptTextUsingAES(walletBean.getEncText(), encKey);
		//descText=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
	} catch (Exception e) {
		e.printStackTrace();
	}
	Gson gson=new Gson();
	WalletBean wBean=gson.fromJson(descText,WalletBean.class);
	
	return walletUserDao.login(wBean.getUserId(),wBean.getAggreatorid(),imei,agent);
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces(MediaType.APPLICATION_JSON)
@Path("/getSessionId")
public String getSessionId(String userId)
{
	return walletUserDao.getSessionId(userId);
}

@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/requestRevoke")
public String requestRevoke( String dtl,@HeaderParam("IPIMEI") String ipimei,@HeaderParam("AGENT") String agent)
{
	try{
		JSONObject jObject = new JSONObject(dtl);
		return walletUserDao.requestRevoke(jObject.getString("userId"),jObject.getString("revokeUserId"),jObject.getString("remark"),
				jObject.getDouble("amount"),ipimei,agent);
	}
	catch(Exception e)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","", "Problem in requestRevoke | e.getMessage "+e.getMessage());
		e.printStackTrace();
		return "Unable to raise revoke request. Please Contact Admin";
	}
}

@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/getRevokeList")
public String getRevokeList(String id)
{
	return new Gson().toJson(walletUserDao.getRevokeList(id));
}

@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/rejectAcceptRevoke")
public String rejectAcceptRevoke(String dtl)
{
	try{
		JSONObject jObject = new JSONObject(dtl);
		return walletUserDao.rejectAcceptRevoke(jObject.getString("revokeType"),jObject.getString("remark"),jObject.getString("revokeId"),jObject.getString("aggregatorId"),jObject.getString("requesterId"),jObject.getString("revokeUserId"),
				jObject.getDouble("amount"));
	}
	catch(Exception e)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","", "Problem in requestRevoke | e.getMessage "+e.getMessage());
		e.printStackTrace();
		return "Unable to raise revoke request. Please Contact Admin";
	}
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/getAepsDetailsView")
public AEPSLedger getAepsDetailsView(String txnId){
	
  logger.info("******************************getAepsDetails***********************************"+txnId);
	
  return walletUserDao.getAepsDetailsView(txnId);
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/onboardStatus")
public OnboardStatusResponse onboardStatus(String agentId){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.onboardStatus(agentId);
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/saveDmtPricing")
public DmtPricing saveDmtPricing(DmtPricing pricing){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.saveDmtPricing(pricing);
}



@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/deletePricing")
public DmtPricing deletePricing(DmtPricing pricing){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.deletePricing(pricing);
}

@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/getPlan")
public WalletMastBean getPlan(String agentId){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.getPlan(agentId);
}

@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentPlan")
public WalletMastBean agentPlan(String agentId){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.agentPlan(agentId);
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateAgentPlan")
public WalletMastBean updateAgentPlan(WalletMastBean mast){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.updateAgentPlan(mast.getId(),mast.getPlanId());
}

@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateNewPlan")
public CommPercMaster updateNewPlan(CommPercMaster mast){
	
  logger.info("******************************updateNewPlan  Check***********************************");
	
  return walletUserDao.updateNewPlan(mast.getDistributorperc(),mast.getSuperdistperc(),mast.getCommid());
}

@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/addNewPlan")
public List<CommPercMaster> addNewPlan(String id){
	
  logger.info("******************************addNewPlan  Check***********************************");
	
  return walletUserDao.addNewPlan(id);
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentPricingDetail")
public String agentPricingDetail(String agentId){
	
  logger.info("******************************Agent pricing list***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.agentPricingDetail(agentId));
}



@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateAgentAccount")
public String updateAgentAccount(AddBankAccount bank){
	
  logger.info("******************************Agent pricing list***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.updateAgentAccount(bank));
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/distributorDetails")
public String distributorDetails(String distId){
	
  logger.info("******************************Agent pricing list***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.distributorDetails(distId));
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/saveAccount")
public AddBankAccount saveAccount(AddBankAccount account){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.saveAccount(account);
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentAccountDetails")
public String agentAccountDetails(String agentId){
	
  logger.info("******************************Agent pricing list***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.agentAccountDetails(agentId));
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/deleteUserAccount")
public AddBankAccount deleteUserAccount(AddBankAccount account){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.deleteUserAccount(account);
}


@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateUserAccount")
public AddBankAccount updateUserAccount(AddBankAccount account){
	
  logger.info("******************************Onboard Status Check***********************************");
	
  return walletUserDao.updateUserAccount(account);
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/agentAccounts")
public String agentAccounts(WalletMastBean account){
	
  logger.info("******************************Agent agentAccounts list***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.agentAccounts(account.getAccountNumber(),account.getId()));
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/getFlag")
public String getFlag(String aggId){
	
  logger.info("******************************Agent getFlag ***********************************");
  GsonBuilder builder = new GsonBuilder();
  Gson gson = builder.create();
  return gson.toJson(walletUserDao.getFlag(aggId));
}

@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/updateFlag")
public String updateFlag(String statusRequest){
	
logger.info("******************************Agent updateFlag list***********************************");
	 
return walletUserDao.updateFlag(statusRequest);
}


@POST
@Produces(MediaType.APPLICATION_JSON)
@Path("/getServicesStatus")
public String getServicesStatus(String serviceRequest){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()" );
	
	
	return walletUserDao.getServicesStatus(serviceRequest);		
}

@POST
@Produces(MediaType.APPLICATION_JSON)
@Path("/saveNewRegs")
public String saveNewRegs(NewRegs serviceRequest){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()" );
	
	
	return walletUserDao.saveNewRegs(serviceRequest);		
}


@POST
@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
@Path("/getPanAndAdhar")
public WalletMastBean getPanAndAdhar(RechargeTxnBean mast){
	
  logger.info("******************************Agent getPanAndAdhar***********************************");
  //GsonBuilder builder = new GsonBuilder();
  //Gson gson = builder.create();
  //return gson.toJson(walletUserDao.getPanAndAdhar(agentId));
 /*
  public WalletMastBean getPanAndAdhar(@HeaderParam("IPIMEI") String ipiemi,
		  @HeaderParam("AGENT") String agent,RechargeTxnBean mast){
    mast.setIpIemi(ipiemi);
	mast.setTxnAgent(agent);
  */
  return walletUserDao.getPanAndAdhar(mast);
}

@POST
@Consumes({MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/rejectAgentByMuser")
public String rejectAgentByMuser(WalletBean walletBean){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|rejectAgentByAgg()"+"|rejectAgentByAgg "+walletBean.getDeclinedComment()+"|getDeclinedComment"+walletBean.getDeclinedComment());
	
	return ""+walletUserDao.rejectAgentByMuser(walletBean.getAggreatorid(),walletBean.getCreatedBy(), walletBean.getUserId(),walletBean.getDeclinedComment());
}


@POST
@Path("/declinedNewAgentList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String declinedNewAgentList(WalletMastBean reportBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",""+reportBean.getUsertype(), "", "", "|getRejectAgentDetail()");
	   
	//logger.info("Start excution ===================== method getRejectAgentDetail(reportBean)");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.declinedNewAgentList(reportBean.getAggreatorid(),reportBean.getCreatedby(),reportBean.getUsertype()));
}

@POST
@Path("/getAgentListNew")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getAgentListNew(WalletMastBean walletMastBean){
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentList()");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getAgentListNew(walletMastBean));
}




public static void main(String s[])
{
	try {
		String encKey=RSAEncryptionWithAES.decryptAESKey("dzoybj4eZixI4fwhWepOBWWPnq+qpgcC9aHcyzX0zEQN5ncALLnimXfcrnZBQBLvN0J1J4fEjrTAqG9lH1idWlaKZp+gQ6l7uToeRR2DRWUOTPl9WS3MOW/3FJg2nCjo/UdgbipiNLCJezR12ZyPdjVN14zqXTEntahywgW61u4F2gmfaSBoFi614vi88Sjt5NabEYOpzITKK8m9eOteEFwPKLSuDfhoUNVdPgOMq48xieJQY2HJMfJDUfidbUtRWDg8ES25x2cLphFPs1/REsyfLxjSsxaMn2N/fLNKWxKDWqEsXuL6dyP3SLbujQzKqgyld4JptfSXp84M14jZUA==", new WalletUserDaoImpl().getPrivateKeyByAggId("AGGR001035"));
		String descText=RSAEncryptionWithAES.decryptTextUsingAES(encKey, "+fmv/vB9Fk2wCCNoKpECsUGpu2njmqtxMtxNbuoz9MUCH9goXK4gfqJnEqXEY8FiIOucHmexs5PYXZE3COjNhLOLC2omo2RT5nwFiUVbzZ4zuFN7aSIltb0kqHnj4b//");
		//descText=RSAEncryptionWithAES.decryptAESKey(walletBean.getEncText(), walletUserDao.getPrivateKeyByAggId(walletBean.getAggreatorid()));
		System.out.println(descText);
	} catch (Exception e) {
		e.printStackTrace();
	}
}



@POST
@Path("/getUserCount")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getUserCount(WalletMastBean walletMastBean){
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "getUserCount()");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getUserCount(walletMastBean));
}

@POST
@Path("/getUserManagement")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public String getUserManagement(WalletMastBean walletMastBean){
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "getUserManagement()");
	GsonBuilder builder = new GsonBuilder();
	Gson gson = builder.create();
	return gson.toJson(walletUserDao.getUserManagement(walletMastBean));
}





@POST
@Path("/onboardFileUpload") 
@Consumes(MediaType.MULTIPART_FORM_DATA)  
public String testUpload(@FormDataParam("userId")  String userId, 
		@FormDataParam("file") InputStream uploadedInputStream,
        @FormDataParam("file") FormDataContentDisposition fileDetail) {

        String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();
	   //String uploadedFileLocation = "/home/mani/AGENTDOC" + fileDetail.getFileName();
	
        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);

        String output = "File uploaded to : " + uploadedFileLocation;
	
     return output+""+userId;
}


// save uploaded file to new location
private void writeToFile(InputStream uploadedInputStream,
    String uploadedFileLocation) {

    try {
        OutputStream out = new FileOutputStream(new File(
                uploadedFileLocation));
        int read = 0;
        byte[] bytes = new byte[1024];

        out = new FileOutputStream(new File(uploadedFileLocation));
        while ((read = uploadedInputStream.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        out.flush();
        out.close();
    } catch (IOException e) {

        e.printStackTrace();
    }

}

}