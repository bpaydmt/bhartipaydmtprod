package com.bhartipay.user.bean;

import java.io.Serializable;

public class TapAndPayBean implements Serializable{
	
	private String cardHolderName;
	private String cardnumber;
	private String expiryNo   ;
	private String mobileNumber;
	private String imei;
	private String tokenId;
	
	private String statusCode;
	private String statusDesc;
	
	
	
	private String prePaidUserId;
	private String prePaidCardId;
	private String userId;
	
	
	
	private String  aggreatorId; 
	private String  request; 
	private String  response;
	
	
	
	
	
	
	

	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPrePaidUserId() {
		return prePaidUserId;
	}
	public void setPrePaidUserId(String prePaidUserId) {
		this.prePaidUserId = prePaidUserId;
	}
	public String getPrePaidCardId() {
		return prePaidCardId;
	}
	public void setPrePaidCardId(String prePaidCardId) {
		this.prePaidCardId = prePaidCardId;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
	public String getExpiryNo() {
		return expiryNo;
	}
	public void setExpiryNo(String expiryNo) {
		this.expiryNo = expiryNo;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	
}
