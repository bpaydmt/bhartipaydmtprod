package com.bhartipay.user.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;



@NamedNativeQueries({
	@NamedNativeQuery(
	name = "callOTPMANAGE",
	query = "CALL otpmanage(:mobile,:aggreatorid,:otp)",
	resultClass = WalletOtpBean.class
	)
})







@Entity
@Table(name="walletotpmast")
public class WalletOtpBean implements Serializable{
	
	@Id
	@Column(name = "mobileno", nullable = false)
	private String mobileno; 
	
	
	@Column(name = "aggreatorid", nullable = false)
	private String aggreatorId;
		
	@Column(name = "otp", nullable = false)
	private String otp;
	
	
	
	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}	
	
	
	

}
