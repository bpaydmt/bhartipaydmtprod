package com.bhartipay.user.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usertypemast")
public class UserTypeMast implements Serializable{
	@Id
	@Column(name="id",length =30)
	private int id;
	
	@Column(name="naration",length =100,nullable = true)
	private String naration;   
	
	@Column(name="description",length =100,nullable = true)
	private String description;
	
	
	public int getId() {
		return id;
	}
	public String getNaration() {
		return naration;
	}
	public String getDescription() {
		return description;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setNaration(String naration) {
		this.naration = naration;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
	
}
