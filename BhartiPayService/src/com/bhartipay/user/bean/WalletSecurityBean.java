package com.bhartipay.user.bean;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="walletsecurity")
public class WalletSecurityBean {
	
@Id
@Column(name="userId")
private String userId;
@Column(name="walletId")
private String walletId;
@Column(name="aggreagatorId")
private String aggreagatorId;
@Column(name="token")
private String token;
@Column(name="tokenSecurityKey")
private String tokenSecurityKey;
@Column(name="appVersion")
private String appVersion;
@Column(name="userStatus")
private String userStatus;
@Temporal(TemporalType.TIMESTAMP)
@Column(name="date",insertable=false)
private Date date;




public String getWalletId() {
	return walletId;
}
public void setWalletId(String walletId) {
	this.walletId = walletId;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getAggreagatorId() {
	return aggreagatorId;
}
public void setAggreagatorId(String aggreagatorId) {
	this.aggreagatorId = aggreagatorId;
}
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getTokenSecurityKey() {
	return tokenSecurityKey;
}
public void setTokenSecurityKey(String tokenSecurityKey) {
	this.tokenSecurityKey = tokenSecurityKey;
}
public String getAppVersion() {
	return appVersion;
}
public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
}
public String getUserStatus() {
	return userStatus;
}
public void setUserStatus(String userStatus) {
	this.userStatus = userStatus;
}
}
