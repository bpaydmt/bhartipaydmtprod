package com.bhartipay.user.bean;

public class UserProfileBean {
	
	
	private String id;
	private String walletid;
	private String  mobileno;
	private String emailid;
	private String name;
	private double finalBalance;
	private int usertype;
	private String aggreatorid;
	private String distributerid; 
	private String agentid;
	private String subAgentId;
	private String addressProofType;
	private String kycStatus;
	private double cashBackFinalBalance;
	
	private boolean cme;
	private String token;
	private String tokenSecurityKey;
	
	private String isimps;
	private String wallet;
	
	private String bank;
	private String whiteLabel;
	private int miniKycStatus;
	private String superdistributerid;
	private String portalMessage;
	private String agentCode;
	private String otpVerificationRequired;
	private String asAgentCode;
	private String aepsChannel;
	private String asCode;
	private String shopName;
	
	private String bank1;
	private String bank2;
	private String bank3;
	private String bank3_via;

	private int onlineMoney;
	private int recharge;
	private int bbps;

	

	
	public int getOnlineMoney() {
		return onlineMoney;
	}


	public void setOnlineMoney(int onlineMoney) {
		this.onlineMoney = onlineMoney;
	}


	public int getRecharge() {
		return recharge;
	}


	public void setRecharge(int recharge) {
		this.recharge = recharge;
	}


	public int getBbps() {
		return bbps;
	}


	public void setBbps(int bbps) {
		this.bbps = bbps;
	}


	public String getBank1() {
		return bank1;
	}


	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}


	public String getBank2() {
		return bank2;
	}


	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}


	public String getBank3() {
		return bank3;
	}


	public void setBank3(String bank3) {
		this.bank3 = bank3;
	}


	public String getBank3_via() {
		return bank3_via;
	}


	public void setBank3_via(String bank3_via) {
		this.bank3_via = bank3_via;
	}


	public String getAsCode() {
		return asCode;
	}


	public void setAsCode(String asCode) {
		this.asCode = asCode;
	}


	public String getShopName() {
		return shopName;
	}


	public void setShopName(String shopName) {
		this.shopName = shopName;
	}


	public String getAsAgentCode() {
		return asAgentCode;
	}


	public void setAsAgentCode(String asAgentCode) {
		this.asAgentCode = asAgentCode;
	}


	public String getAepsChannel() {
		return aepsChannel;
	}


	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}

	
	
	public String getOtpVerificationRequired() {
		return otpVerificationRequired;
	}
	public void setOtpVerificationRequired(String otpVerificationRequired) {
		this.otpVerificationRequired = otpVerificationRequired;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getPortalMessage() {
		return portalMessage;
	}
	public void setPortalMessage(String portalMessage) {
		this.portalMessage = portalMessage;
	}
	public String getSuperdistributerid() {
		return superdistributerid;
	}
	public void setSuperdistributerid(String superdistributerid) {
		this.superdistributerid = superdistributerid;
	}
	public int getMiniKycStatus() {
		return miniKycStatus;
	}
	public void setMiniKycStatus(int miniKycStatus) {
		this.miniKycStatus = miniKycStatus;
	}
	public String getWhiteLabel() {
		return whiteLabel;
	}
	public void setWhiteLabel(String whiteLabel) {
		this.whiteLabel = whiteLabel;
	}
	public String getWallet() {
		return wallet;
	}
	public void setWallet(String wallet) {
		this.wallet = wallet;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getIsimps() {
		return isimps;
	}
	public void setIsimps(String isimps) {
		this.isimps = isimps;
	}
	public String getKycStatus() {
		return kycStatus;
	}
	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenSecurityKey() {
		return tokenSecurityKey;
	}
	public void setTokenSecurityKey(String tokenSecurityKey) {
		this.tokenSecurityKey = tokenSecurityKey;
	}
	public boolean isCme() {
	  return cme;
	 }
	 public void setCme(boolean cme) {
	  this.cme = cme;
	 }
	
	
	public double getCashBackFinalBalance() {
		return cashBackFinalBalance;
	}
	public void setCashBackFinalBalance(double cashBackFinalBalance) {
		this.cashBackFinalBalance = cashBackFinalBalance;
	}
	public String getAddressProofType() {
		return addressProofType;
	}
	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getDistributerid() {
		return distributerid;
	}
	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getSubAgentId() {
		return subAgentId;
	}
	public void setSubAgentId(String subAgentId) {
		this.subAgentId = subAgentId;
	}
	private String country;
	private String countrycurrency;
	
	
	
	
	
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(double finalBalance) {
		this.finalBalance = finalBalance;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountrycurrency() {
		return countrycurrency;
	}
	public void setCountrycurrency(String countrycurrency) {
		this.countrycurrency = countrycurrency;
	}
	
	
	
	

}
