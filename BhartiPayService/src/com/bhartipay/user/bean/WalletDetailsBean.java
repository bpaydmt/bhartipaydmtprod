package com.bhartipay.user.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="walletuserdtl")

public class WalletDetailsBean {
	
	@Id
	@Column(name="id",length =50)
	private String id;
	
	@Column(name="address1",length =50,nullable = true)
	private String address1;
	
	@Column(name="address2",length =50,nullable = true)
	private String address2;
	
	@Column(name="city",length =50,nullable = true)
	private String city;
	
	@Column(name="state",length =50,nullable = true)
	private String state;
	
	@Column(name="pin",length =50,nullable = true)
	private String pin;
	
	@Column(name="pan",length =20,nullable = true)
	private String pan;
	
	@Column(name="adhar",length =20,nullable = true)
	private String adhar;
	
	@Column(name="kycstatus",length =20,nullable = true)
	private String kycstatus;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAdhar() {
		return adhar;
	}

	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}

	public String getKycstatus() {
		return kycstatus;
	}

	public void setKycstatus(String kycstatus) {
		this.kycstatus = kycstatus;
	}
	
	
	

}
