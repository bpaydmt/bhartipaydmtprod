package com.bhartipay.user.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="wishlistmast")
public class UserWishListBean {

	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column( name ="wishid", length=5, nullable = false)
	private int wishId;
	
	@Column( name = "userid", length=100, nullable = false)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;

	@Column(updatable = false, name = "rechargenumber", nullable = false, length=25)
	private String rechargeNumber;
	
	@Column(updatable = false, name = "rechargetype", nullable = false, length=50)
	private String rechargeType;
	
	@Column(updatable = false, name = "rechargecircle", nullable = false, length=50)
	private String rechargeCircle;
	
	@Column(updatable = false, name = "rechargeoperator", nullable = false, length=50)
	private String rechargeOperator;
	
	@Column(name = "rechargeamt", nullable = false, length=10)
	private Double rechargeAmt;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	

	@Transient
	public String status;
	
	
	
	
	
	
	
	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getWishId() {
		return wishId;
	}

	public void setWishId(int wishId) {
		this.wishId = wishId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getRechargeNumber() {
		return rechargeNumber;
	}

	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeCircle() {
		return rechargeCircle;
	}

	public void setRechargeCircle(String rechargeCircle) {
		this.rechargeCircle = rechargeCircle;
	}

	public String getRechargeOperator() {
		return rechargeOperator;
	}

	public void setRechargeOperator(String rechargeOperator) {
		this.rechargeOperator = rechargeOperator;
	}

	public Double getRechargeAmt() {
		return rechargeAmt;
	}

	public void setRechargeAmt(Double rechargeAmt) {
		this.rechargeAmt = rechargeAmt;
	}
	
	
	
	
	
	
}
