package com.bhartipay.user.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="walletsecuritymast")
public class WalletSecurityMast {
	@Id
	@Column(name="aggreagatorid")
	private String aggreagatorid;   
	@Column(name="publickey")
	private String publickey;
	@Column(name="privatekey")
	private String privatekey;
	
	public String getAggreagatorid() {
		return aggreagatorid;
	}
	public void setAggreagatorid(String aggreagatorid) {
		this.aggreagatorid = aggreagatorid;
	}
	public String getPublickey() {
		return publickey;
	}
	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}
	public String getPrivatekey() {
		return privatekey;
	}
	public void setPrivatekey(String privatekey) {
		this.privatekey = privatekey;
	}
	
	
	
	
}
