package com.bhartipay.user.bean;

import java.io.Serializable;
import java.sql.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="walletmast")


public class WalletMastBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8889359917898893779L;

	@Id
	@Column(name="id",length =30)
	private String id;
	
	@Column(name="walletid",length =50,nullable = false,updatable=false)
	private String walletid;
	
	@Column(name = "mobileno", unique = true, nullable = false, length = 12)
	private String mobileno;
	
	@Column(name = "emailid", unique = true, nullable = false, length = 100)
	private String emailid;
	
	@Column(name = "password", nullable = false, length = 100)
	private String password;
	
	@Column(name = "name", nullable = false, length = 100)
	private String name;
	
	@Column(name="pan",length =100,nullable = true)
	private String pan;
	
	@Column(name="addressprooftype",length =20,nullable = true)
	private String addressProofType;
	
	@Column(name="adhar",length =100,nullable = true)
	private String adhar;
	

	@Column(name = "usertype", nullable = false, length = 2 ,columnDefinition = "int default 0")
	private int usertype;
	
	@Column(name = "userstatus", nullable = false, length = 2)
	private String userstatus;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date creationDate;
	
	@Column(name = "aggreatorid", nullable = true, length = 100)
	private String aggreatorid;
	
	@Column(name = "distributerid", nullable = true, length = 100)
	private String distributerid;
	
	@Column(name = "superdistributerid", nullable = true, length = 100)
	private String superdistributerid;
	
	@Column(name = "agentid", nullable = true, length = 100)
	private String agentid;
	
	@Column(name = "subagentid", nullable = true, length = 100)
	private String subAgentId;
	
	@Column(name = "createdby", nullable = true, length = 100)
	private String createdby;

	@Column(name = "loginStatus", nullable = false, length = 2 ,columnDefinition = "int default 0")
	private int loginStatus;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date lastlogintime;
	
	
	@Column(name="address1",length =50,nullable = true)
	private String address1;
	
	@Column(name="address2",length =50,nullable = true)
	private String address2;
	
	@Column(name="city",length =50,nullable = true)
	private String city;
	
	@Column(name="state",length =50,nullable = true)
	private String state;
	
	@Column(name="pin",length =50,nullable = true)
	private String pin;
	
	
	
	@Column(name = "dob", nullable = true, length = 12)
	private String dob;
	
	@Column(name="agenttype",length =50,nullable = true)
	private String agentType;
	
	@Column(name="shopname",length =100,nullable = true)
	private String shopName;
		
	@Column(name="shopaddress1",length =50,nullable = true)
	private String shopAddress1;
	
	@Column(name="shopaddress2",length =50,nullable = true)
	private String shopAddress2;
	
	@Column(name="shopcity",length =50,nullable = true)
	private String shopCity;
	
	@Column(name="shopstate",length =50,nullable = true)
	private String shopState;
	
	@Column(name="shoppin",length =8,nullable = true)
	private String shopPin;
	
	
	@Column(name="kycstatus",length =20,nullable = true)
	private String kycstatus;
	
	@Column(name = "approvalrequired", nullable = true, length = 2)
	private String approvalRequired;
	
	
	@Column(name = "planid", nullable = true, length = 20 ,columnDefinition = "0")
	private String planId;
	
	
	@Column(name = "prepaiduserid",  length = 100 )
	private String prePaidUserId;
	
	@Column(name = "prepaidcardid",  length = 100 )
	private String prePaidCardId;
	
	/*@Column(name = "prepaidcardnumber",  length = 20 )
	private String prePaidCardNumber;
	*/
	@Column(name = "prepaidcardstatus",  length = 20 )
	private String prePaidCardStatus;
	
	
	@Column(name = "hceToken",  length = 100 )
	private String hceToken;
		

	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	@Column(name = "loginipiemi",  length = 20 )
	private String loginipiemi;
	
	@Column(name = "loginagent",  length = 300 )
	private String loginagent;
	
//	@Column(name = "loginmac",  length = 100 )
//	private String loginMac;
//	
//	@Column(name = "loginfrom",  length = 20 )
//	private String loginFrom;
	
	@Column(name = "oldpassword",  length = 1000 )
	private String oldPassword;
	
	
	@Column(name = "formlocation",  length = 150 )
	private String formLoc;
	
	@Column(name = "idlocation",  length = 150 )
	private String idLoc;
	
	@Column(name = "addresslocation",  length = 150 )
	private String addressLoc;
	
	
	@Column(name = "paymentamount",  columnDefinition="Decimal(10,2)" )
	private double applicationFee;
	
	@Column(name = "territory",  length = 50, columnDefinition="default 'N/A'")
	private String territory;
	
	@Column(name = "managername",  length = 40,columnDefinition="default 'N/A'" )
	private String managerName;
	
	@Column(name = "soname",  length = 40,columnDefinition="default 'N/A'" )
	private String soName;
	
	@Column(name = "distributorname",  length = 40,columnDefinition="default 'N/A'" )
	private String distributorName;
	
	@Column(name = "distributormobileno",  length = 12 ,columnDefinition="default 'N/A'")
	private String distributorMobileNo;
	
	@Column(name = "accountnumber",  length = 25,columnDefinition="default 'N/A'" )
	private String accountNumber;
	
	@Column(name = "ifsccode",  length = 15 ,columnDefinition="default 'N/A'")
	private String ifscCode;
	
	@Column(name = "paymentdate",  length = 15 )
	private String paymentDate;
	
	@Column(name = "paymentmode",  length = 50 )
	private String paymentMode;
	
	@Column(name = "bankname",  length = 100 )
	private String bankName;
	
	@Column(name = "utrno",  length = 100 )
	private String utrNo;
	
	
	@Column(name = "mpin",  length = 100 )
	private String mpin;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date approveDate;
	
	@Column(name="isimps" , columnDefinition="int default 0")
	private String isimps;
	
	@Column(name="wallet" , columnDefinition="int default 0")
	private String wallet;
	
	@Column(name="bank" , columnDefinition="int default 0")
	private String bank;
	
	@Column(name="whitelabel")
	private String whiteLabel;
	
	@Column(name="documentId" , length=100)
	private String documentId;	
	
	@Column(name="documentType" , length=100)
	private String documentType;
	
	@Column(name="minikycstatus",length=2,columnDefinition = "int default 0")
	private int miniKycStatus;
	
	@Column(name="otp_verification_required",length=2, columnDefinition="int default 0")
	private String otpVerificationRequired;
	
	@Transient
	private String statusCode;
	
	@Transient
	private String stDate;
	
	@Transient
	private String endDate;
	
	@Transient
	private String encText;
	
	@Transient
	private String encKey;
	
	@Transient
	private String remark;
	
	@Transient
	private String bankType;
	
	@Transient
	private String txnStatus;
	
	
	@Column(name="cashdepositewallet", length=30)
	private String cashDepositeWallet;
	
	@Column(name = "portalmessage",  length = 500 )
	private String portalMessage;
	
	@Column(name = "agentcode",  length = 100 )
	private String agentCode;
	
	@Column(name = "ascode",  length = 100 )
	private String asCode;
	
	@Column(name = "asagentcode",  length = 100 )
	private String asAgentCode;
	
	@Column(name = "aepschannel",  length = 100 )
	private String aepsChannel;
	
	@Column(name = "NEFTBatch", nullable = false, length = 1 ,columnDefinition = "int default 0")
	private int NEFTBatch;
	
	@Column(name = "gender",  length = 100 )
	private String gender;
	
	
	@Column(name="bank1")
	private String bank1;
	
	@Column(name="bank2")
	private String bank2;
	
	@Column(name="bank3")
	private String bank3;
	
	@Column(name="bank3_via")
	private String bank3_via;
	
	@Column(name="online_money", nullable = false, length = 1 ,columnDefinition = "int default 0")
	private int onlineMoney;
	
	@Column(name="recharge", nullable = false, length = 1 ,columnDefinition = "int default 0")
	private int recharge;
	
	@Column(name="bbps", nullable = false, length = 1 ,columnDefinition = "int default 0")
	private int bbps;
	
	
	@Column(name = "passbooklocation",  length = 150 )
	private String passbookLoc;
	
	@Column(name = "paymentlocation",  length = 150 )
	private String paymentLoc;
	
	@Column(name="managerid")
	private String managerId;
	
	@Column(name="salesid")
	private String salesId;
	
	@Column(name="subaggregatorid")
	private String subAggregatorId;
	
	
	@Transient
	private String base64_1;
	@Transient
	private String file1FileName;
	@Transient
	private String file1ContentType;
	@Transient
	private String base64_2;
	@Transient
	private String file2FileName;
	@Transient
	private String file2ContentType;
	@Transient
	private String base64_3;
	@Transient
	private String file3FileName;
	@Transient
	private String file3ContentType;
	@Transient
	private String base64_4;
	@Transient
	private String file4FileName;
	@Transient
	private String file4ContentType;
	
	
	
	
	
	public String getBase64_1() {
		return base64_1;
	}

	public void setBase64_1(String base64_1) {
		this.base64_1 = base64_1;
	}

	public String getFile1FileName() {
		return file1FileName;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public String getFile1ContentType() {
		return file1ContentType;
	}

	public void setFile1ContentType(String file1ContentType) {
		this.file1ContentType = file1ContentType;
	}

	public String getBase64_2() {
		return base64_2;
	}

	public void setBase64_2(String base64_2) {
		this.base64_2 = base64_2;
	}

	public String getFile2FileName() {
		return file2FileName;
	}

	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	public String getFile2ContentType() {
		return file2ContentType;
	}

	public void setFile2ContentType(String file2ContentType) {
		this.file2ContentType = file2ContentType;
	}

	public String getBase64_3() {
		return base64_3;
	}

	public void setBase64_3(String base64_3) {
		this.base64_3 = base64_3;
	}

	public String getFile3FileName() {
		return file3FileName;
	}

	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	public String getFile3ContentType() {
		return file3ContentType;
	}

	public void setFile3ContentType(String file3ContentType) {
		this.file3ContentType = file3ContentType;
	}

	public String getBase64_4() {
		return base64_4;
	}

	public void setBase64_4(String base64_4) {
		this.base64_4 = base64_4;
	}

	public String getFile4FileName() {
		return file4FileName;
	}

	public void setFile4FileName(String file4FileName) {
		this.file4FileName = file4FileName;
	}

	public String getFile4ContentType() {
		return file4ContentType;
	}

	public void setFile4ContentType(String file4ContentType) {
		this.file4ContentType = file4ContentType;
	}

	public int getOnlineMoney() {
		return onlineMoney;
	}

	public void setOnlineMoney(int onlineMoney) {
		this.onlineMoney = onlineMoney;
	}

	public int getRecharge() {
		return recharge;
	}

	public void setRecharge(int recharge) {
		this.recharge = recharge;
	}

	public int getBbps() {
		return bbps;
	}

	public void setBbps(int bbps) {
		this.bbps = bbps;
	}

	public String getBank1() {
		return bank1;
	}

	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}

	public String getBank2() {
		return bank2;
	}

	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}

	public String getBank3() {
		return bank3;
	}

	public void setBank3(String bank3) {
		this.bank3 = bank3;
	}

	public String getBank3_via() {
		return bank3_via;
	}

	public void setBank3_via(String bank3_via) {
		this.bank3_via = bank3_via;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	
	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getSoName() {
		return soName;
	}

	public void setSoName(String soName) {
		this.soName = soName;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getDistributorMobileNo() {
		return distributorMobileNo;
	}

	public void setDistributorMobileNo(String distributorMobileNo) {
		this.distributorMobileNo = distributorMobileNo;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAsAgentCode() {
		return asAgentCode;
	}

	public void setAsAgentCode(String asAgentCode) {
		this.asAgentCode = asAgentCode;
	}

	public String getAepsChannel() {
		return aepsChannel;
	}

	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getOtpVerificationRequired() {
		return otpVerificationRequired;
	}

	public void setOtpVerificationRequired(String otpVerificationRequired) {
		this.otpVerificationRequired = otpVerificationRequired;
	}

	public int getNEFTBatch() {
		return NEFTBatch;
	}

	public void setNEFTBatch(int nEFTBatch) {
		NEFTBatch = nEFTBatch;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}


	public String getPortalMessage() {
		return portalMessage;
	}


	public void setPortalMessage(String portalMessage) {
		this.portalMessage = portalMessage;
	}
	
	public String getCashDepositeWallet() {
		return cashDepositeWallet;
	}

	public void setCashDepositeWallet(String cashDepositeWallet) {
		this.cashDepositeWallet = cashDepositeWallet;
	}
	
	
	public String getSuperdistributerid() {
		return superdistributerid;
	}

	public void setSuperdistributerid(String superdistributerid) {
		this.superdistributerid = superdistributerid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public int getMiniKycStatus() {
		return miniKycStatus;
	}

	public void setMiniKycStatus(int miniKycStatus) {
		this.miniKycStatus = miniKycStatus;
	}

	public String getWhiteLabel() {
		return whiteLabel;
	}

	public void setWhiteLabel(String whiteLabel) {
		this.whiteLabel = whiteLabel;
	}

	public String getIsimps() {
		return isimps;
	}

	public void setIsimps(String isimps) {
		this.isimps = isimps;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getEncKey() {
		return encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	public String getEncText() {
		return encText;
	}

	public void setEncText(String encText) {
		this.encText = encText;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getUtrNo() {
		return utrNo;
	}

	public void setUtrNo(String utrNo) {
		this.utrNo = utrNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getAddressProofType() {
		return addressProofType;
	}

	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getShopAddress1() {
		return shopAddress1;
	}

	public void setShopAddress1(String shopAddress1) {
		this.shopAddress1 = shopAddress1;
	}

	public String getShopAddress2() {
		return shopAddress2;
	}

	public void setShopAddress2(String shopAddress2) {
		this.shopAddress2 = shopAddress2;
	}

	public String getShopCity() {
		return shopCity;
	}

	public void setShopCity(String shopCity) {
		this.shopCity = shopCity;
	}

	public String getShopState() {
		return shopState;
	}

	public void setShopState(String shopState) {
		this.shopState = shopState;
	}

	public String getShopPin() {
		return shopPin;
	}

	public void setShopPin(String shopPin) {
		this.shopPin = shopPin;
	}

	public double getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(double applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getFormLoc() {
		return formLoc;
	}

	public void setFormLoc(String formLoc) {
		this.formLoc = formLoc;
	}

	public String getIdLoc() {
		return idLoc;
	}

	public void setIdLoc(String idLoc) {
		this.idLoc = idLoc;
	}

	public String getAddressLoc() {
		return addressLoc;
	}

	public void setAddressLoc(String addressLoc) {
		this.addressLoc = addressLoc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getHceToken() {
		return hceToken;
	}

	public void setHceToken(String hceToken) {
		this.hceToken = hceToken;
	}

	public String getPrePaidCardStatus() {
		return prePaidCardStatus;
	}

	public void setPrePaidCardStatus(String prePaidCardStatus) {
		this.prePaidCardStatus = prePaidCardStatus;
	}

	public String getLoginipiemi() {
		return loginipiemi;
	}

	public void setLoginipiemi(String loginipiemi) {
		this.loginipiemi = loginipiemi;
	}

	public String getLoginagent() {
		return loginagent;
	}

	public void setLoginagent(String loginagent) {
		this.loginagent = loginagent;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getPrePaidUserId() {
		return prePaidUserId;
	}

	public void setPrePaidUserId(String prePaidUserId) {
		this.prePaidUserId = prePaidUserId;
	}

	public String getPrePaidCardId() {
		return prePaidCardId;
	}

	public void setPrePaidCardId(String prePaidCardId) {
		this.prePaidCardId = prePaidCardId;
	}

/*	public String getPrePaidCardNumber() {
		return prePaidCardNumber;
	}

	public void setPrePaidCardNumber(String prePaidCardNumber) {
		this.prePaidCardNumber = prePaidCardNumber;
	}*/

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getSubAgentId() {
		return subAgentId;
	}

	public void setSubAgentId(String subAgentId) {
		this.subAgentId = subAgentId;
	}

	public String getApprovalRequired() {
		return approvalRequired;
	}

	public void setApprovalRequired(String approvalRequired) {
		this.approvalRequired = approvalRequired;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}



	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUsertype() {
		return usertype;
	}

	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}



	public String getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getDistributerid() {
		return distributerid;
	}

	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public Date getLastlogintime() {
		return lastlogintime;
	}

	public void setLastlogintime(Date lastlogintime) {
		this.lastlogintime = lastlogintime;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAdhar() {
		return adhar;
	}

	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}

	public String getKycstatus() {
		return kycstatus;
	}

	public void setKycstatus(String kycstatus) {
		this.kycstatus = kycstatus;
	}

	public String getAsCode() {
		return asCode;
	}

	public void setAsCode(String asCode) {
		this.asCode = asCode;
	}

	public String getPassbookLoc() {
		return passbookLoc;
	}

	public void setPassbookLoc(String passbookLoc) {
		this.passbookLoc = passbookLoc;
	}

	public String getPaymentLoc() {
		return paymentLoc;
	}

	public void setPaymentLoc(String paymentLoc) {
		this.paymentLoc = paymentLoc;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getSubAggregatorId() {
		return subAggregatorId;
	}

	public void setSubAggregatorId(String subAggregatorId) {
		this.subAggregatorId = subAggregatorId;
	}


 
	
	
}
