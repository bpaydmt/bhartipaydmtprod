package com.bhartipay.user.bean;

public class LoginResponse {
	
	private String token;
	private String tokenSecurityKey;
	private String statusCode;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenSecurityKey() {
		return tokenSecurityKey;
	}
	public void setTokenSecurityKey(String tokenSecurityKey) {
		this.tokenSecurityKey = tokenSecurityKey;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	
	
}
