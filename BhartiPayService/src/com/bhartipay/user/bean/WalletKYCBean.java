package com.bhartipay.user.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="walletkycmast")

public class WalletKYCBean {
	
	
	@Id
	@Column(name="refid",length =30)
	private String refid;
	
	@Column(name="userid",length =50,nullable = false,updatable=false)
	private String UserId;
	
	@Column(name="username",length =100,nullable = false,updatable=false)
	private String userName ;
	
	@Column(name="mobileno",length =20,nullable = false,updatable=false)
	private String mobileNo ;
	
	@Column(name="emailid",length =100,nullable = false,updatable=false)
	private String emailId ;
	
	
	@Column(name="addprofkycid",length =5,nullable = false,updatable=false)
	private int addprofkycid;
	
	
	@Column(name="addprofdesc",length =50,nullable = false,updatable=false)
	private String addprofdesc;
	
	
	@Column(name = "addprofkycpic", unique = false, nullable = false, length = 100000)
    private String addprofkycpic;
	
	
	@Column(name="idprofkycid",length =5,nullable = false,updatable=false)
	private int idprofkycid;
	
	
	@Column(name="idprofdesc",length =50,nullable = false,updatable=false)
	private String idprofdesc;
	
	
	@Column(name = "idprofkycpic", unique = false, nullable = false, length = 100000)
    private String idprofkycpic;
	
	
	@Column(name="status",length =50,nullable = false)
	private String status;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date loaddate;


	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	
	
	
	
	
	
	
	
	
	public String getIpiemi() {
		return ipiemi;
	}


	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}


	public String getAgent() {
		return agent;
	}


	public void setAgent(String agent) {
		this.agent = agent;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getRefid() {
		return refid;
	}


	public void setRefid(String refid) {
		this.refid = refid;
	}


	


	public String getUserId() {
		return UserId;
	}


	public void setUserId(String userId) {
		UserId = userId;
	}


	public int getAddprofkycid() {
		return addprofkycid;
	}


	public void setAddprofkycid(int addprofkycid) {
		this.addprofkycid = addprofkycid;
	}


	public String getAddprofdesc() {
		return addprofdesc;
	}


	public void setAddprofdesc(String addprofdesc) {
		this.addprofdesc = addprofdesc;
	}


	public int getIdprofkycid() {
		return idprofkycid;
	}


	public void setIdprofkycid(int idprofkycid) {
		this.idprofkycid = idprofkycid;
	}


	public String getIdprofdesc() {
		return idprofdesc;
	}


	public void setIdprofdesc(String idprofdesc) {
		this.idprofdesc = idprofdesc;
	}




	public String getAddprofkycpic() {
		return addprofkycpic;
	}


	public void setAddprofkycpic(String addprofkycpic) {
		this.addprofkycpic = addprofkycpic;
	}


	public String getIdprofkycpic() {
		return idprofkycpic;
	}


	public void setIdprofkycpic(String idprofkycpic) {
		this.idprofkycpic = idprofkycpic;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getLoaddate() {
		return loaddate;
	}


	public void setLoaddate(Date loaddate) {
		this.loaddate = loaddate;
	}


		
	
}
