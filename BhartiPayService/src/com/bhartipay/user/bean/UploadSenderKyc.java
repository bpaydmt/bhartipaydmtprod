package com.bhartipay.user.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="senderkycdetails")
public class UploadSenderKyc {

	@Id
	@Column(name="kycid")
	private String kycId;

	@Column(name="senderId")
	private String senderId;
	@Column(name="agentId")
	private String agentId;
	@Column(name="aggregatorId")
	private String aggreagatorId;
	
	@Column(name="addressProofType")
	private String addressProofType;
	@Column(name="addressProof")
	private String addressProof;
	@Column(name="addressProofUrl")
	private String addressProofUrl;
//	@Column(name="myFile1")
//	private File myFile1;
	@Column(name="myFile1FileName")
	private String myFile1FileName;
	@Column(name="myFile1ContentType")
	private String myFile1ContentType;
	
	
	@Column(name="idProofType")
	private String idProofType;
	@Column(name="idProof")
	private String idProof;
	@Column(name="idProofUrl")
	private String idProofUrl;
//	@Column(name="myFile2")
//	private File myFile2;
	@Column(name="myFile2FileName")
	private String myFile2FileName;
	@Column(name="myFile2ContentType")
	private String myFile2ContentType;
	
	@Column(name="addressDesc")
	 private String  addressDesc;
	@Column(name="idDesc")
	 private String  idDesc;
	 
	 
	@Column(name="senderName")
	private String senderName;
	@Column(name="senderAddress")
	private String senderAddress;
	@Column(name="senderMobileNo")
	private String senderMobileNo;
	@Column(name="senderEmailId")
	private String senderEmailId;
	
	@Column(name="checkarstatus")
	private String checkarStatus;
	@Column(name="approvarstatus")
	private String approvarStatus;
	@Column(name="finalstatus")
	private String finalStatus;
	
	@Column(name="requestdate")
	private Date requestDate;
	@Column(name="approvedate")
	private Date approveDate;
	@Column(name="remark")
	private String remark;
	@Transient
	private String statusCode;
	@Transient
	private String statusMessage;
	@Transient
	private String stDate;
	@Transient
	private String endDate;
	
	
	
	
	
	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getKycId() {
		return kycId;
	}

	public String getSenderId() {
		return senderId;
	}
	public String getAgentId() {
		return agentId;
	}
	public String getAggreagatorId() {
		return aggreagatorId;
	}
	public String getAddressProofType() {
		return addressProofType;
	}
	public String getAddressProof() {
		return addressProof;
	}
	public String getAddressProofUrl() {
		return addressProofUrl;
	}
	public String getMyFile1FileName() {
		return myFile1FileName;
	}
	public String getMyFile1ContentType() {
		return myFile1ContentType;
	}
	public String getIdProofType() {
		return idProofType;
	}
	public String getIdProof() {
		return idProof;
	}
	public String getIdProofUrl() {
		return idProofUrl;
	}

	public String getMyFile2FileName() {
		return myFile2FileName;
	}
	public String getMyFile2ContentType() {
		return myFile2ContentType;
	}
	public String getAddressDesc() {
		return addressDesc;
	}
	public String getIdDesc() {
		return idDesc;
	}
	public String getSenderName() {
		return senderName;
	}
	public String getSenderEmailId() {
		return senderEmailId;
	}
	public String getCheckarStatus() {
		return checkarStatus;
	}
	public String getApprovarStatus() {
		return approvarStatus;
	}
	public String getFinalStatus() {
		return finalStatus;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public String getStDate() {
		return stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setKycId(String kycId) {
		this.kycId = kycId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public void setAggreagatorId(String aggreagatorId) {
		this.aggreagatorId = aggreagatorId;
	}
	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}
	public void setAddressProof(String addressProof) {
		this.addressProof = addressProof;
	}
	public void setAddressProofUrl(String addressProofUrl) {
		this.addressProofUrl = addressProofUrl;
	}
	public void setMyFile1FileName(String myFile1FileName) {
		this.myFile1FileName = myFile1FileName;
	}
	public void setMyFile1ContentType(String myFile1ContentType) {
		this.myFile1ContentType = myFile1ContentType;
	}
	public void setIdProofType(String idProofType) {
		this.idProofType = idProofType;
	}
	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}
	public void setIdProofUrl(String idProofUrl) {
		this.idProofUrl = idProofUrl;
	}

	public void setMyFile2FileName(String myFile2FileName) {
		this.myFile2FileName = myFile2FileName;
	}
	public void setMyFile2ContentType(String myFile2ContentType) {
		this.myFile2ContentType = myFile2ContentType;
	}
	public void setAddressDesc(String addressDesc) {
		this.addressDesc = addressDesc;
	}
	public void setIdDesc(String idDesc) {
		this.idDesc = idDesc;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}
	public void setCheckarStatus(String checkarStatus) {
		this.checkarStatus = checkarStatus;
	}
	public void setApprovarStatus(String approvarStatus) {
		this.approvarStatus = approvarStatus;
	}
	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}
	
}
