package com.bhartipay.user.bean;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="aepssettlement")
public class AepsSettlementBean {

	
	@Id
	@Column(name="resptxnid",length =50)
	private String respTxnId;
	
	@Column(name="amount",length =9,columnDefinition = "double default 0.0")
	private double amount;
	
	@Column(name="charges",length =9,columnDefinition = "double default 0.0")
	private double charges;
	
	@Column(name="agentid",length =50)
	private String agentId;
	
	@Column(name="agentname",length =50)
	private String agentName;
	
	@Column(name="agentwalletid",length =50)
	private String agentWalletId;
	
	@Column(name="accountnumber",length =50)
	private String accountNumber;
	
	@Column(name="ifsccode",length =50)
	private String ifscCode;
	
	
	@Column(name="status",length =20)
	private String status;
	
	@Column(name="others",length =50)
	private String others;
	
	@Column(name="rrn",length =50)
	private String rrn;
	
	@Column(name="paymentmode",length =50)
	private String paymentMode;
	
	@Column(name="aggregatorid",length =50)
	private String aggregatorId;
	
	@Column(name="banktransactionid",length =50)
	private String bankTransactionId;
	
	
	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getUtr() {
		return utr;
	}

	public void setUtr(String utr) {
		this.utr = utr;
	}

	@Column(name="paymentstatus",length =50)
	private String paymentStatus;
	
	
	@Column(name="utr",length =50)
	private String utr;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "settlementdate", nullable = false, updatable=false)
	private Date settlementDate;

	
	
	
	
	public double getCharges() {
		return charges;
	}

	public void setCharges(double charges) {
		this.charges = charges;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRespTxnId() {
		return respTxnId;
	}

	public void setRespTxnId(String respTxnId) {
		this.respTxnId = respTxnId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentWalletId() {
		return agentWalletId;
	}

	public void setAgentWalletId(String agentWalletId) {
		this.agentWalletId = agentWalletId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	
	
	
}
