package com.bhartipay.user.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="revokeuserdtl")
public class RevokeUserDtl {

	@Id
	@Column(name="id",length = 15)
	private String id;
	
	@Column(name="aggreatorid",length=11)
	private String aggreatorId;
	
	@Column(name="walletid",length=30)
	private String walletId;
	
	@Column(name="userid", length=11)
	private String userId;
	
	@Column(name="requestedby",length=11)
	private String requestedBy;
	
	@Column(name="amount" , columnDefinition="Decimal(10,2) default '00.00'")
	private double amount;
	
	@Column(name="approverid",length = 11)
	private String approverId;
	
	@Column(name="requesterremark",length = 150)
	private String requesterRemark;
	
	@Column(name="approverremark",length = 150)
	private String approverRemark;
	
	@Column(name="requestdate",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date requestDate;
	
	@Column(name="approvedate",columnDefinition="TIMESTAMP" )
	private Date approveDate;
	
	@Column(name="status",length=20)
	private String status;

	public String getRequesterRemark() {
		return requesterRemark;
	}

	public void setRequesterRemark(String requesterRemark) {
		this.requesterRemark = requesterRemark;
	}

	public String getApproverRemark() {
		return approverRemark;
	}

	public void setApproverRemark(String approverRemark) {
		this.approverRemark = approverRemark;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getApproverId() {
		return approverId;
	}

	public void setApproverId(String approverId) {
		this.approverId = approverId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
