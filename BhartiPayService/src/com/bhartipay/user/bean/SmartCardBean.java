package com.bhartipay.user.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="smartcardmast")
public class SmartCardBean {
	
	@Id
	@Column(name = "reqid",length = 30)
	private String reqId;
	
	@Column(name="userid" ,length = 30)
	private String userId;
	
	@Column(name="walletid",length = 30)
	private String walletId;
	
	@Column(name="aggreatorid",length = 30)
	private String aggreatorId;
	
	@Column(name="mobileno",length = 12)
	private String mobileNo;
	
	@Column(name="emailid",length = 100)
	private String emailId;
	
	@Column(name="name",length = 100)
	private String name;
	
	@Column(name="lastname",length = 100)
	private String lastName;
	
	
	@Column(name="preferredname",length = 25)
	private String preferredName;
	
	@Column(name="passowrd",length = 25)
	private String passowrd;
	
	@Column(name="cardtype",length = 25)
	private String cardType;
	
	
	
	@Column(name="countrycode",length = 25)
	private int countryCode;
	
	
	@Column(name="status",length = 30)
	private String status;	
		
	@Column(name="prepaidid",length = 100)
	private String prePaidId;
	
	@Column(name="prepaiduserid",length = 100)
	private String prePaidUserId;
	
	@Column(name="prepaidstatus",length = 100)
	private String prePaidStatus;
	
	@Column(name="prepaiderror",length = 1000)
	private String prePaidError;
	
	@Column(name="prepaidcardnumber",length = 20)
	private String prePaidCardNumber;
	
	@Column(name="prepaidcardpin",length = 20)
	private String prePaidCardPin;
	
	@Column(name="cvv",length = 20)
	private String cvv;
	
	/*@Column(name="expirydate",length = 20)
	private String expDate;*/
	
	
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date reqDate;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	
	@Transient 
	private String idType;
	
	@Transient 
	private String idNumber;
	
	@Transient 
	private String countryOfIssue;
	
	@Transient 
	private String nationality;
	
	@Transient 
	private String gender;
	
	@Transient 
	private String title;

	@Transient 
	private String statusCode;
	
	@Transient 
	private String statusDesc;
	
	@Transient 
	private String blockedType;
	
	@Transient 
	private String activationCode;
	
	@Transient 
	private String type;
	
	@Transient 
	private String address_1;
	
	@Transient 
	private String address_2;
	
	@Transient 
	private String city;
	
	@Transient 
	private String state;
	
	@Transient 
	private String country;
	
	@Transient 
	private String zipcode;
	
	@Transient 
	private String kycStatus;
	
	
	@Transient 
	private String changePinMode;
	
	
	
	
	
	
	
	

		
/*	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}*/

	public String getChangePinMode() {
		return changePinMode;
	}

	public void setChangePinMode(String changePinMode) {
		this.changePinMode = changePinMode;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getPrePaidUserId() {
		return prePaidUserId;
	}

	public void setPrePaidUserId(String prePaidUserId) {
		this.prePaidUserId = prePaidUserId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress_1() {
		return address_1;
	}

	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}

	public String getAddress_2() {
		return address_2;
	}

	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getBlockedType() {
		return blockedType;
	}

	public void setBlockedType(String blockedType) {
		this.blockedType = blockedType;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getPrePaidCardNumber() {
		return prePaidCardNumber;
	}

	public void setPrePaidCardNumber(String prePaidCardNumber) {
		this.prePaidCardNumber = prePaidCardNumber;
	}

	public String getPrePaidCardPin() {
		return prePaidCardPin;
	}

	public void setPrePaidCardPin(String prePaidCardPin) {
		this.prePaidCardPin = prePaidCardPin;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrePaidError() {
		return prePaidError;
	}

	public void setPrePaidError(String prePaidError) {
		this.prePaidError = prePaidError;
	}

	public String getPrePaidStatus() {
		return prePaidStatus;
	}

	public void setPrePaidStatus(String prePaidStatus) {
		this.prePaidStatus = prePaidStatus;
	}

	public String getPrePaidId() {
		return prePaidId;
	}

	public void setPrePaidId(String prePaidId) {
		this.prePaidId = prePaidId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getReqDate() {
		return reqDate;
	}

	public void setReqDate(Date reqDate) {
		this.reqDate = reqDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public String getPassowrd() {
		return passowrd;
	}

	public void setPassowrd(String passowrd) {
		this.passowrd = passowrd;
	}

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
	
	

}
