package com.bhartipay.user.bean;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="userwalletconfig")

public class UserWalletConfigBean implements Serializable{
	
	@EmbeddedId
	private UserWalletConfigKey userWalletConfigKey;
	
	/*@Column(name="id",length =50)
	private String id;
	
	@Column(name="walletid",length =50,nullable = false)
	private String walletid;
	
	@Column(name="txntype",length =4,nullable = false,columnDefinition = "double default 0")
	private int txntype;*/
	
	@Column(name="aggreatorid",length =50,nullable = false)
	private String aggreatorid;
	
	@Column(name="minbal",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double minbal;
	
	@Column(name="maxamtpertx",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtpertx;
	
	@Column(name="maxamtperday",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperday;
	
	@Column(name="maxamtperweek",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperweek;
	
	@Column(name="maxamtpermonth",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtpermonth;
	
	@Column(name="maxtxperday",length =3,nullable = false,columnDefinition = "int default 0")
	private int maxtxperday;
	
	@Column(name="maxtxperweek",length =3,nullable = false,columnDefinition = "int default 0")
	private int maxtxperweek;
	
	@Column(name="maxtxpermonth",length =3,nullable = false,columnDefinition = "int default 0")
	private int maxtxpermonth;
	
	@Column(name="maxamtperacc",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperacc;
	
	
	@Column(name="maxamtperquarter",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperquarter;
	
	@Column(name="maxamtperhalfyearly",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperhalfyearly;
	
	@Column(name="maxamtperyear",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double maxamtperyear;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	
	
	@Column(name="mtpertxn",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double mtpertxn;
	
	@Column(name="mtpermonth",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double mtpermonth;
	
	@Column(name="mtperday",length =8,nullable = false,columnDefinition = "double default 0.0")
	private double mtperday;
	
	
	@Column(name="mtnotxnperday",length =3,nullable = false,columnDefinition = "int default 0")
	private int mtnotxnperday;
	
	@Column(name="mtnotxnpermonth",length =3,nullable = false,columnDefinition = "int default 0")
	private int mtnotxnpermonth;
	
	

	
	
	
	
	@Transient
	private String status;
	
	@Transient
	private String id;
	
	@Transient
	private String walletid;
	
	@Transient
	private int txnType;
	
	
	
	
	
	
	
		
	public double getMtpertxn() {
		return mtpertxn;
	}

	public void setMtpertxn(double mtpertxn) {
		this.mtpertxn = mtpertxn;
	}

	public double getMtpermonth() {
		return mtpermonth;
	}

	public void setMtpermonth(double mtpermonth) {
		this.mtpermonth = mtpermonth;
	}

	public double getMtperday() {
		return mtperday;
	}

	public void setMtperday(double mtperday) {
		this.mtperday = mtperday;
	}

	public int getMtnotxnperday() {
		return mtnotxnperday;
	}

	public void setMtnotxnperday(int mtnotxnperday) {
		this.mtnotxnperday = mtnotxnperday;
	}

	public int getMtnotxnpermonth() {
		return mtnotxnpermonth;
	}

	public void setMtnotxnpermonth(int mtnotxnpermonth) {
		this.mtnotxnpermonth = mtnotxnpermonth;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}



	public int getTxnType() {
		return txnType;
	}

	public void setTxnType(int txnType) {
		this.txnType = txnType;
	}

	public UserWalletConfigKey getUserWalletConfigKey() {
		return userWalletConfigKey;
	}

	public void setUserWalletConfigKey(UserWalletConfigKey userWalletConfigKey) {
		this.userWalletConfigKey = userWalletConfigKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public double getMinbal() {
		return minbal;
	}

	public void setMinbal(double minbal) {
		this.minbal = minbal;
	}

	public double getMaxamtpertx() {
		return maxamtpertx;
	}

	public void setMaxamtpertx(double maxamtpertx) {
		this.maxamtpertx = maxamtpertx;
	}

	public double getMaxamtperday() {
		return maxamtperday;
	}

	public void setMaxamtperday(double maxamtperday) {
		this.maxamtperday = maxamtperday;
	}

	public double getMaxamtperweek() {
		return maxamtperweek;
	}

	public void setMaxamtperweek(double maxamtperweek) {
		this.maxamtperweek = maxamtperweek;
	}

	public double getMaxamtpermonth() {
		return maxamtpermonth;
	}

	public void setMaxamtpermonth(double maxamtpermonth) {
		this.maxamtpermonth = maxamtpermonth;
	}

	public int getMaxtxperday() {
		return maxtxperday;
	}

	public void setMaxtxperday(int maxtxperday) {
		this.maxtxperday = maxtxperday;
	}

	public int getMaxtxperweek() {
		return maxtxperweek;
	}

	public void setMaxtxperweek(int maxtxperweek) {
		this.maxtxperweek = maxtxperweek;
	}

	public int getMaxtxpermonth() {
		return maxtxpermonth;
	}

	public void setMaxtxpermonth(int maxtxpermonth) {
		this.maxtxpermonth = maxtxpermonth;
	}

	public double getMaxamtperacc() {
		return maxamtperacc;
	}

	public void setMaxamtperacc(double maxamtperacc) {
		this.maxamtperacc = maxamtperacc;
	}

/*	public int getTxntype() {
		return txntype;
	}

	public void setTxntype(int txntype) {
		this.txntype = txntype;
	}*/

	public double getMaxamtperquarter() {
		return maxamtperquarter;
	}

	public void setMaxamtperquarter(double maxamtperquarter) {
		this.maxamtperquarter = maxamtperquarter;
	}

	public double getMaxamtperhalfyearly() {
		return maxamtperhalfyearly;
	}

	public void setMaxamtperhalfyearly(double maxamtperhalfyearly) {
		this.maxamtperhalfyearly = maxamtperhalfyearly;
	}

	public double getMaxamtperyear() {
		return maxamtperyear;
	}

	public void setMaxamtperyear(double maxamtperyear) {
		this.maxamtperyear = maxamtperyear;
	}
	
	
	
	
	

}
