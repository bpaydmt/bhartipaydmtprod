package com.bhartipay.user.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="cashbackwalletbalance")
public class CashBackBalanceBean {
	
	
	
	@Id
	@Column(name="walletid",length =50)
	private String walletid;
		
	@Column(name="credit",length =9,columnDefinition = "double default 0.0")
	private double credit;
	
	@Column(name="debit",length =9,columnDefinition = "double default 0.0")
	private double debit;
	
	@Column(name="balance",length =9,columnDefinition = "double default 0.0")
	private double finalBalance;
	
	@Column(name="lasttxnid",length =20)
	private String lasttxnid;
	
	

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public double getFinalBalance() {
		return finalBalance;
	}

	public void setFinalBalance(double finalBalance) {
		this.finalBalance = finalBalance;
	}

	public String getLasttxnid() {
		return lasttxnid;
	}

	public void setLasttxnid(String lasttxnid) {
		this.lasttxnid = lasttxnid;
	}
	
	
	
	
	
	

}
