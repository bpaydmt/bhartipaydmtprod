package com.bhartipay.user.bean;

import java.sql.Date;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="agentdeclinedcomment")
public class AgentDeclinedComment implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length =10)
	private String id;
	
	@Column(name="agentid",length =50)
	private String agentId;
	
	@Column(name="comment",length =300)
	private String declinedComment;
	
	@Column(name="status",length =10)
	private String status;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date declineddate;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public String getDeclinedComment() {
		return declinedComment;
	}


	public void setDeclinedComment(String declinedComment) {
		this.declinedComment = declinedComment;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getDeclineddate() {
		return declineddate;
	}


	public void setDeclineddate(Date declineddate) {
		this.declineddate = declineddate;
	}



	
	
	

}
