package com.bhartipay.user.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userblockmast")
public class UserBlockedBean{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length =10)
	private String id;
	
	@Column(name="agentid",length =50)
	private String agentId;
	
	@Column(name="blockby",length =50)
	private String blockBy;
	
	@Column(name="comment",length =300)
	private String declinedComment;
	
	@Column(name="status",length =10)
	private String status;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date blockeddate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBlockBy() {
		return blockBy;
	}

	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}

	public String getDeclinedComment() {
		return declinedComment;
	}

	public void setDeclinedComment(String declinedComment) {
		this.declinedComment = declinedComment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getBlockeddate() {
		return blockeddate;
	}

	public void setBlockeddate(Date blockeddate) {
		this.blockeddate = blockeddate;
	}

	
	
	
	
	

}
