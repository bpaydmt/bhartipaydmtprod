package com.bhartipay.user.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserWalletConfigKey implements Serializable{
	
	
	@Column(name="id",length =50)
	private String id;
	
	@Column(name="walletid",length =50,nullable = false)
	private String walletid;
	
	@Column(name="txncode",length =4,nullable = false,columnDefinition = "double default 0")
	private int txncode;
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public int getTxncode() {
		return txncode;
	}

	public void setTxncode(int txncode) {
		this.txncode = txncode;
	}

	
	
	
	
	
	

}
