package com.bhartipay.user.persistence;

import java.security.NoSuchAlgorithmException;

import com.bhartipay.user.bean.TapAndPayBean;

public interface WalletTapAndPay {

	public TapAndPayBean getTapAndPayReq(String aggreatorId,String payLoad) throws NoSuchAlgorithmException;
	public TapAndPayBean getUserCardId(String tokenId);
}
