package com.bhartipay.user.persistence;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;

import com.bhartipay.dmt.persistence.JWTSignVerify;
import com.bhartipay.user.bean.TapAndPayBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;

public class WalletTapAndPayImpl  implements WalletTapAndPay{
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	private static final Logger logger = Logger.getLogger(WalletTapAndPayImpl.class.getName());
	@SuppressWarnings("unchecked")
	public TapAndPayBean getTapAndPayReq(String aggreatorId,String payLoad) throws NoSuchAlgorithmException{
		JWTSignVerify jwts=new JWTSignVerify();
		Gson gson = new Gson();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		TapAndPayBean tapAndPayBeanR=new TapAndPayBean();
		TapAndPayBean tapAndPayBean=new TapAndPayBean();
		try{
		Claims claim=jwts.parseToken(payLoad);
		JSONObject cc = new JSONObject(claim);
		tapAndPayBean=gson.fromJson(cc.toString(), TapAndPayBean.class);
		Criteria criteriaQuery=session.createCriteria(WalletMastBean.class);
		criteriaQuery.add(Restrictions.eq("mobileno", tapAndPayBean.getMobileNumber()));
		criteriaQuery.add(Restrictions.eq("aggreatorid", aggreatorId));
		WalletMastBean walletMastBean=(WalletMastBean)criteriaQuery.uniqueResult();
		
		if(walletMastBean==null|| walletMastBean.getId()==null ||walletMastBean.getId().isEmpty() || !walletMastBean.getUserstatus().equalsIgnoreCase("A")){
			tapAndPayBean.setStatusCode("5501");
			tapAndPayBean.setStatusDesc("You don't have a valid user.");
		//}else if(walletMastBean.getPrePaidCardNumber()==null || walletMastBean.getPrePaidCardNumber().isEmpty()||!walletMastBean.getPrePaidCardStatus().equalsIgnoreCase("Active")|| !walletMastBean.getPrePaidCardNumber().equalsIgnoreCase(tapAndPayBean.getCardnumber())){
		}else if(!walletMastBean.getPrePaidCardStatus().equalsIgnoreCase("Active")){
			tapAndPayBean.setStatusCode("5502");
			tapAndPayBean.setStatusDesc("You don't have have a valid card");
		}else{
			String hceToken=CommanUtil.SHAHashing256(walletMastBean.getId());
			System.out.println("hceToken Token********************************"+hceToken);
			Transaction transactions = session.beginTransaction();
			//walletMastBean.setHceToken(token);
			Query query=session.createQuery("update WalletMastBean set hceToken=:hceToken where mobileno=:mobileno and aggreatorid=:aggreatorid");
			query.setString("hceToken", hceToken);
			query.setString("mobileno", tapAndPayBean.getMobileNumber());
			query.setString("aggreatorid", tapAndPayBean.getAggreatorId());
			query.executeUpdate();
			transactions.commit();
			tapAndPayBean.setTokenId(hceToken);
			tapAndPayBean.setStatusCode("5000");
			tapAndPayBean.setStatusDesc("Success");
		}
		HashMap<String, String> tapAndPayBeanMap =gson.fromJson(gson.toJson(tapAndPayBean),HashMap.class);
		
		tapAndPayBeanR.setAggreatorId(aggreatorId);
		tapAndPayBeanR.setResponse(jwts.generateToken(tapAndPayBeanMap));
		} catch (Exception e) {
			tapAndPayBean.setStatusCode("5501");
			tapAndPayBean.setStatusDesc("You don't have a valid user.");
			HashMap<String, String> tapAndPayBeanMap =gson.fromJson(gson.toJson(tapAndPayBean),HashMap.class);
			tapAndPayBeanR.setAggreatorId(aggreatorId);
			tapAndPayBeanR.setResponse(jwts.generateToken(tapAndPayBeanMap));
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return 	tapAndPayBeanR;
	}
	
	
	
	
	public TapAndPayBean getUserCardId(String tokenId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		TapAndPayBean tapAndPayBean=new TapAndPayBean();
		try {
			logger.info("Start excution ********************************************* method getUserCardId(tokenId, userId)" +tokenId);
			
			 SQLQuery  query=session.createSQLQuery("SELECT prepaiduserid,prepaidcardid FROM walletmast WHERE  hceToken=:tokenId AND prepaidcardstatus='Active' ");
			 
			 query.setString("tokenId", tokenId);
			 List<Object[]> rows=query.list();
			 for(Object[] row : rows){
				 tapAndPayBean.setPrePaidUserId(row[0].toString()); 
				 tapAndPayBean.setPrePaidCardId(row[1].toString());
			 }
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			logger.debug("problem in getUserCardId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return tapAndPayBean;
		
	}
	
	
	
	
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		HashMap<String, String> hmap = new HashMap<String, String>();
		
		WalletTapAndPayImpl walletTapAndPay=new WalletTapAndPayImpl();
		/*hmap.put("cardHolderName", "santosh");
		hmap.put("cardnumber", "9898987656787654");
		hmap.put("expiryNo", "98765");
		hmap.put("mobileNumber", "8010097530");
		hmap.put("imei", "876789987678876");
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		walletTapAndPay.getTapAndPayReq("AGGR001035", token);*/
		
		walletTapAndPay.getUserCardId("ce15b35a5fe1b79eb0c6e81554e22eaabd0f586d9f951670c818ddf3318b0ea1");
		
		
		
	}
	
	

}
