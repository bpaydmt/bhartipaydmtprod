package com.bhartipay.user.persistence;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cme.bean.CMERespBean;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.AssignedUnit;
import com.bhartipay.lean.bean.DistributorIdAllot;
import com.bhartipay.lean.bean.DmtPricing;
import com.bhartipay.lean.bean.IdCreationCharge;
import com.bhartipay.lean.bean.NewRegs;
import com.bhartipay.lean.bean.OnboardStatusResponse;
import com.bhartipay.lean.bean.ServiceMast;
import com.bhartipay.lean.bean.Test;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.payoutapi.cashfree.persistnace.PayoutClientConfig;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.transaction.bean.MoneyRequestBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.notification.persistence.TransNotificationDao;
import com.bhartipay.transaction.notification.persistence.TransNotificationDaoImpl;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.AgentDeclinedComment;
import com.bhartipay.user.bean.AgentDetailsView;
import com.bhartipay.user.bean.DeclinedListBean;
import com.bhartipay.user.bean.LoginResponse;
import com.bhartipay.user.bean.ProfileImgBean;
import com.bhartipay.user.bean.RevokeUserDtl;
import com.bhartipay.user.bean.SmartCardBean;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.user.bean.UserBlockedBean;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.UserWalletConfigBean;
import com.bhartipay.user.bean.UserWalletConfigKey;
import com.bhartipay.user.bean.UserWishListBean;
import com.bhartipay.user.bean.WalletBalanceBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletKYCBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.bean.WalletSecurityBean;
import com.bhartipay.user.bean.WalletSecurityMast;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.EmailValidator;
import com.bhartipay.util.InfrastructureProperties;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.PasswordValidator;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.SmsTemplates;
import com.bhartipay.util.StringEncryption;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.bean.MailConfigMast;
import com.bhartipay.util.bean.SMSConfigMast;
import com.bhartipay.util.bean.UserRoleMapping;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.MachMovePerPaidCard;
import com.bhartipay.util.thirdParty.bean.MatchMoveBean;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.bean.WalletApiRequestLog;
import com.cashfree.lib.http.ObjectMapper;
import com.google.gson.Gson;

import appnit.com.crypto.RSAEncryptionWithAES;




public class WalletUserDaoImpl implements WalletUserDao {

	private static final Logger logger = Logger.getLogger(WalletUserDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	OTPGeneration oTPGeneration = new OTPGeneration();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	WalletConfiguration walletConfiguration = null;
	UserWalletConfigKey userWalletConfigKey=null;
	
	
	
	private static final long twepoch = 1288834974657L;
	private static final long sequenceBits = 17;
	private static final long sequenceMax = 65536;
	private static volatile long lastTimestamp = -1L;
	private static volatile long sequence = 0L;
	
	
	/**
	 * 	
	 * @param mobileno
	 * @return String type code where  1000 for success  other for error
	 */
	public String mobileValidation(String mobileno) {
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|"+"mobileValidation()"+"|MobileNO "+mobileno);
	   
		//logger.info("Start excution ********************************************* method mobileValidation(mobileno)");
		String statusCode = "1001";
		// 1001 -ISE
		String newOtp = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		WalletMastBean walletMastBean = new WalletMastBean();
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from WalletMastBean where mobileno=:mobileno");
			query.setString("mobileno", "9074270586");
			List<WalletMastBean> walletMastlist = query.list();
			if (walletMastlist != null && walletMastlist.size() != 0) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|Inside mobileValidation Mobile Number allready signup with us.");
				   
				//logger.info("Inside ====================mobileValidation====Mobile Number allready signup with us.");
				statusCode = "7002";
				// 7002-Mobile Number allready signup with us
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|Inside account wallet send otp for validate mobile on" + mobileno);
				
				//logger.info("Inside *********************************************account wallet====send otp for validate mobile on" + mobileno);

				newOtp = oTPGeneration.generateOTP();
				
				
				
				
				if (!(newOtp == null)) {
					
					//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", newOtp);
					//SHA-256
					Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					
					insertOtp.executeUpdate();
				}
				transaction.commit();
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|Inside otp send on mobile" + mobileno + "otp"
							+ newOtp);
					
					/*logger.info("Inside ====================otp send on mobile==========" + mobileno + "===otp==="
							+ newOtp);*/
					statusCode = "101";
					// 101- otp send successfully
					/*SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,SmsTemplates.regOtp().replace("<<OTP>>", newOtp), "SMS","0");
					Thread t = new Thread(smsAndMailUtility);
					t.start();*/
				}
			}

		} catch (Exception e) {
			statusCode = "1001";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in mobileValidation"+e.getMessage()+" "+e);
			
			//logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return statusCode;
	}

/*	public Boolean validateOTP(String mobileno, String otp) {
		logger.info("Start excution ********************************************* method validateOTP(mobileno,otp)");
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {
			Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp ");
			queryOTP.setString("mobile", mobileno);
			queryOTP.setString("otp", otp);
			List list = queryOTP.list();
			if (list != null && list.size() != 0) {
				new OTPGeneration();
				flag = oTPGeneration.validateOTP(otp);
				if (flag) {
					flag = true;
				}

			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			logger.debug("problem in validateOTP==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}*/

/*	public String signUpUser(String name, String email, String mobile, String password) {

		logger.info("Start excution ********************************************* method signUpUser(name, email, mobile, password)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		Session sessions = factory.openSession();

		WalletMastBean walletMastBean = new WalletMastBean();
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		try {
			transaction = sessions.beginTransaction();
			if (validateEailId(email)) {
				logger.info("Inside *********************************************Email register with us" + email);
				// 2001-Email allready register with Us
				return "2001";
			}
			walletConfiguration = commanUtilDao.getWalletConfiguration();
			String walletid = generateWalletID(mobile);
			walletMastBean.setWalletid(walletid);
			walletMastBean.setMobileno(mobile);
			walletMastBean.setEmailid(email);
			walletMastBean.setName(name);
			walletMastBean.setPassword(HashValue.hashValue(password));

			if (walletConfiguration.getEmailvalied() == 1) {
				walletMastBean.setUserstatus("D");
			} else {
				walletMastBean.setUserstatus("A");
			}
			walletBalanceBean.setWalletid(walletid);
			sessions.save(walletMastBean);
			sessions.merge(walletMastBean);
			sessions.save(walletBalanceBean);
			transaction.commit();
			statusCode = "101";

		} catch (Exception e) {
			statusCode = "1001";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return statusCode;

	}*/

	public Boolean validateEailId(String emailid) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|"+"validateEailId() "+"|"
				+ emailid);
		//logger.info("Start excution ********************************************* method validateEailId(String email)");
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from WalletMastBean where emailid=:emailid");
			query.setString("emailid", emailid);
			List<WalletMastBean> walletMastlist = query.list();
			if (walletMastlist != null && walletMastlist.size() != 0) {
				flag = true;
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in validateEailId"+e.getMessage()+" "+e
					);
		//	logger.debug("problem in validateEailId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}

	public synchronized String generateWalletID(String mobileno) {
		String walletId = null;
		// getting current date and time
		Calendar date = Calendar.getInstance();
		date.setTime(new Date());
		Format f1 = new SimpleDateFormat("ddMMyyyyHHmmss");
		walletId = mobileno + f1.format(date.getTime());
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", "|generateWalletID()|"
				+ mobileno);
		//logger.info("Inside ====================generateWalletID=== return====" + walletId);
		return walletId;
	}

	
	
	
	public String signUpUserByUpload(WalletMastBean walletMastBean) {
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUserByUpload()"
				);
		//logger.info("Start excution ********************************************* method signUpUserByUpload(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
	 try {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid()+"|Agent" + walletMastBean.getAgentid()+"|Sub" + walletMastBean.getSubAgentId()
					);
	/*	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|Agent" + walletMastBean.getAgentid()
					);
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|Sub" + walletMastBean.getSubAgentId()
					);*/
			/*logger.info("Inside *********************************************Aggg" + walletMastBean.getAggreatorid());
			logger.info("Inside *********************************************Dist" + walletMastBean.getDistributerid());
			logger.info("Inside *********************************************Agent" + walletMastBean.getAgentid());
			logger.info("Inside *********************************************Sub" + walletMastBean.getSubAgentId());*/
			transaction = session.beginTransaction();
					
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile NO" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty" 
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				return "7049";
			}
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid email number" + walletMastBean.getEmailid() 
						);
				//logger.info("Inside *********************************************invalid email number" + walletMastBean.getEmailid());
				return "7041";
			}
			
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Email register with us" + walletMastBean.getEmailid() 
						);
				//logger.info("Inside *********************************************Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
		
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|mobile register with us" +walletMastBean.getMobileno() 
						);
				//logger.info("Inside *********************************************mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}
			
		/*	if(walletMastBean.getAggreatorid().equals("-1") && walletMastBean.getDistributerid().equals("-1") && walletMastBean.getAgentid().equals("-1") && walletMastBean.getSubAgentId().equals("-1")){
			}else if (!(walletMastBean.getAggreatorid().equals("-1")) && !(walletMastBean.getDistributerid().equals("-1")) && !(walletMastBean.getAgentid().equals("-1")) && !(walletMastBean.getSubAgentId().equals("-1"))){
				walletMastBean.setUsertype(1);
			}else if(!(walletMastBean.getAggreatorid().equals("-1")) && !(walletMastBean.getDistributerid().equals("-1")) && !(walletMastBean.getAgentid().equals("-1")) && walletMastBean.getSubAgentId().equals("-1")){
				walletMastBean.setUsertype(5);
			}else if(!(walletMastBean.getAggreatorid().equals("-1")) && !(walletMastBean.getDistributerid().equals("-1")) && walletMastBean.getAgentid().equals("-1") && walletMastBean.getSubAgentId().equals("-1")){
				walletMastBean.setUsertype(2);
			}else if(!(walletMastBean.getAggreatorid().equals("-1")) && walletMastBean.getDistributerid().equals("-1") && walletMastBean.getAgentid().equals("-1") && walletMastBean.getSubAgentId().equals("-1")){
				walletMastBean.setUsertype(3);
			}*/
			
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D'");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|mobile register with us" +walletMastBean.getMobileno() 
					);
			//System.out.println("*****************************Mobileno***********************************" + walletMastBean.getMobileno());
			
			fstpassword = commanUtilDao.createPassword(7);
			walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));
		
			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			}else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			//logger.info("*******************************************walletid*************************************************" + walletid);
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			walletMastBean.setApprovalRequired("N");
			walletMastBean.setUserstatus("D");
			walletMastBean.setPlanId("0");
			/*if(walletMastBean.getAggreatorid().equalsIgnoreCase("admin")){
				walletMastBean.setAggreatorid(agentid);
			}*/
			walletBalanceBean.setWalletid(walletid);
			
		userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			/*userWalletConfigBean.setId(agentid);
			userWalletConfigBean.setWalletid(walletid);*/
			
			session.save(walletMastBean);
			session.save(walletBalanceBean);
			session.save(userWalletConfigBean);
			transaction.commit();
			
			
			statusCode = "1000";
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()) 
					);
				
				//	logger.info("*************************************walletid********************************" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid())
							.replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|mailtemplet"+mailtemplet 
							);
					//logger.info("*************************************mailtemplet********************************" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			
			
		
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in mobileValidatiot"+e.getMessage()+" "+e 
					);
				//logger.debug("problem in mobileValidation*******************************************" + e.getMessage(), e);
		} finally {
			session.close();
		}

return statusCode;
		
	}
	
	
	
	/**
	 * signUpUser method used to create wallet user
	 * Returns an Status Code define in status mast table 
	 * 
	 * @param  WalletMastBean  object of WalletMastBean 
	 * @return  status code
	 */
	
	
	public String signUpUser(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUser()" 
				);
		//logger.info("Start excution ********************************************* method signUpUser(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		//WalletDetailsBean walletDetailsBean = new WalletDetailsBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid() +"|Sub" + walletMastBean.getSubAgentId()
					);
		
			transaction = session.beginTransaction();
			
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty" 
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				return "7049";
			}
			
			
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
				
			}
			
	
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside Email register with us" + walletMastBean.getEmailid()
						);
				//logger.info("Inside *********************************************Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Insidemobile register with us" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}

	
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile Number" + walletMastBean.getMobileno()
					);
			//logger.info("*****************************************************************************" + walletMastBean.getMobileno());
			
			/*if(walletMastBean.getAggreatorid().equals("-1")){
			walletConfiguration = commanUtilDao.getWalletConfiguration("0");
			}else{
				walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			}*/
			
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Invalid Aggreator" + walletConfiguration.getAppname()
						);
				//logger.info("************************************Invalid Aggreator********************************" + walletConfiguration.getAppname());
				return "7049";	
			}
			
			if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(7);
			//	walletMastBean.setPassword(HashValue.hashValue(fstpassword));
				walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));
		
			}else{
				if(new PasswordValidator().validate(walletMastBean.getPassword())){
					//walletMastBean.setPassword(HashValue.hashValue(walletMastBean.getPassword()));
					walletMastBean.setPassword(CommanUtil.SHAHashing256(walletMastBean.getPassword()));
				}else{
					return "7038";
				}
			}
	

			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
			}else if (walletMastBean.getUsertype() == 7) {
				agentid = commanUtilDao.getTrxId("SUPERDIST",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid(agentid);
			}
			else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid("-1");
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "",""
					);
			//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + walletid);
			
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			
			if(walletConfiguration.getReqAgentApproval()==1){
				if(walletMastBean.getUsertype()==2||walletMastBean.getUsertype()==5)
					walletMastBean.setApprovalRequired("Y");
				else
					walletMastBean.setApprovalRequired("N");
			}else{
					walletMastBean.setApprovalRequired("N");
			}
			if(walletMastBean.getAggreatorid().equalsIgnoreCase("admin")){
				walletMastBean.setAggreatorid(agentid);
			}
			
			//For authorized the Aggrigator
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}

			if( walletMastBean.getPan()==null ||walletMastBean.getPan().isEmpty()){
			    walletMastBean.setPan("");
			   }
			   
			   if( walletMastBean.getAdhar()==null ||walletMastBean.getAdhar().isEmpty()){
			    walletMastBean.setAdhar("");
			}
			walletMastBean.setPlanId("0");
			walletMastBean.setAddressProofType("Aadhar");
			
			if(walletMastBean.getDocumentId() != null && !walletMastBean.getDocumentId().trim().isEmpty() && walletMastBean.getDocumentType() != null && !walletMastBean.getDocumentType().trim().isEmpty())
			{
				walletMastBean.setMiniKycStatus(1);
			}
			else
			{
				walletMastBean.setMiniKycStatus(0);
			}
			
			walletBalanceBean.setWalletid(walletid);
			
			//walletDetailsBean.setId(agentid);
			
			
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
			WalletSecurityMast walletSecurityMast=new WalletSecurityMast();
			Map<String,String> map=RSAEncryptionWithAES.getRSAKeys();
			walletSecurityMast.setAggreagatorid(walletMastBean.getAggreatorid());
			walletSecurityMast.setPrivatekey(map.get("private"));
			walletSecurityMast.setPublickey(map.get("public"));
			/*userWalletConfigBean.setId(agentid);
			userWalletConfigBean.setWalletid(walletid);*/

			session.save(walletMastBean);
			session.save(walletBalanceBean);
			//session.save(walletDetailsBean);
			session.save(userWalletConfigBean);
			if(aggId.equalsIgnoreCase("Admin")){
			session.save(walletSecurityMast);
			}
			transaction.commit();
			statusCode = "1000";
			if (statusCode.equals("1000")) {
				
				if(aggId.equalsIgnoreCase("Admin")){
					 aggId="admin";
				}else{
					aggId=walletMastBean.getAggreatorid();
				}
				
				/*if(walletMastBean.getUsertype()==1){
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()).replaceAll("<<NAME>>", walletMastBean.getName());
					logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid());
					//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome to Bhartipay", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
					Thread t = new Thread(smsAndMailUtility);
					t.start();
					
				}*/
				
				
				
				
				
				if(walletConfiguration.getEmailvalid()==1 && !(walletMastBean.getUsertype()==4)){
					String url=walletConfiguration.getProtocol()+"://"+walletConfiguration.getDomainName();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","walletid" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid())
							);
					//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet
							);
					//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid().replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword)));
					//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}else{
					/*if(walletConfiguration.getSmssend()!=0){
						String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid());
						logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", walletMastBean.getMobileno(), smstemplet, "SMS",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
						Thread t = new Thread(smsAndMailUtility);
						t.start();
					}*/
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|walletid " + commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid())
							);
					//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()).replaceAll("<<NAME>>", walletMastBean.getName());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet
							);
					//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
					//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
			}
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|problem in mobileValidation" + e.getMessage() +" "+e
					);
			//logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return statusCode;
	}
	
	

	public String saveSignUpNewMember(WalletMastBean walletMastBean) {
			      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUser()" );
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		//WalletDetailsBean walletDetailsBean = new WalletDetailsBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid() +"|Sub" + walletMastBean.getSubAgentId());		
			transaction = session.beginTransaction();
			String aggId=walletMastBean.getAggreatorid();
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno()); 
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty" );
				return "7049";
			}
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
			}
	
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Insidemobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}
	
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile Number" + walletMastBean.getMobileno());
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Invalid Aggreator" + walletConfiguration.getAppname());
				return "7049";	
			}
			
			if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(7);
	 			walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));
	 		}else{
				if(new PasswordValidator().validate(walletMastBean.getPassword())){
	 				walletMastBean.setPassword(CommanUtil.SHAHashing256(walletMastBean.getPassword()));
				}else{
					return "7038";
				}
			}
	

			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 8) {
				agentid = commanUtilDao.getTrxId("SALES",walletMastBean.getAggreatorid());
				walletMastBean.setSalesId(agentid);
				walletMastBean.setSoName(walletMastBean.getName());
			} else if (walletMastBean.getUsertype() == 9) {
				agentid = commanUtilDao.getTrxId("MANAGER",walletMastBean.getAggreatorid());
				walletMastBean.setManagerId(agentid);
				walletMastBean.setManagerName(walletMastBean.getName());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
				walletMastBean.setSubAggregatorId(agentid);
			}
			else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid("-1");
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","");
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			
			
			if(walletConfiguration.getReqAgentApproval()==1){
				if(walletMastBean.getUsertype()==2||walletMastBean.getUsertype()==5)
					walletMastBean.setApprovalRequired("Y");
				else
					walletMastBean.setApprovalRequired("N");
			}else{
					walletMastBean.setApprovalRequired("N");
			}
			
			if(walletMastBean.getAggreatorid().equalsIgnoreCase("admin")){
				walletMastBean.setAggreatorid(agentid);
			}
			
			if(walletMastBean.getUsertype()==8)
			{
				WalletMastBean mmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getManagerId());
		        if(mmast!=null) {
		        	walletMastBean.setManagerId(mmast.getManagerId());
		        	walletMastBean.setManagerName(mmast.getManagerName());
		        }
			}
			//For authorized the Aggrigator
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}

			if( walletMastBean.getPan()==null ||walletMastBean.getPan().isEmpty()){
			    walletMastBean.setPan("");
			   }
			   
			   if( walletMastBean.getAdhar()==null ||walletMastBean.getAdhar().isEmpty()){
			    walletMastBean.setAdhar("");
			}
			walletMastBean.setPlanId("0");
			walletMastBean.setAddressProofType("Aadhar");
			
			if(walletMastBean.getDocumentId() != null && !walletMastBean.getDocumentId().trim().isEmpty() && walletMastBean.getDocumentType() != null && !walletMastBean.getDocumentType().trim().isEmpty())
			{
				walletMastBean.setMiniKycStatus(1);
			}
			else
			{
			   walletMastBean.setMiniKycStatus(0);
			}
			walletBalanceBean.setWalletid(walletid);
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
			WalletSecurityMast walletSecurityMast=new WalletSecurityMast();
			Map<String,String> map=RSAEncryptionWithAES.getRSAKeys();
			walletSecurityMast.setAggreagatorid(walletMastBean.getAggreatorid());
			walletSecurityMast.setPrivatekey(map.get("private"));
			walletSecurityMast.setPublickey(map.get("public"));

			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
			
			walletMastBean.setWallet("0");
			walletMastBean.setIsimps("0");
			walletMastBean.setBank("0");
			
			session.save(walletMastBean);
			session.save(walletBalanceBean);
			session.save(userWalletConfigBean);
			if(aggId.equalsIgnoreCase("Admin")){
			session.save(walletSecurityMast);
			}
			transaction.commit();
			statusCode = "1000";
			if (statusCode.equals("1000")) {
				
				if(aggId.equalsIgnoreCase("Admin")){
					 aggId="admin";
				}else{
					aggId=walletMastBean.getAggreatorid();
				}
				 
				if(walletConfiguration.getEmailvalid()==1 && !(walletMastBean.getUsertype()==4)){
					String url=walletConfiguration.getProtocol()+"://"+walletConfiguration.getDomainName();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","walletid" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid().replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword)));
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
 					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}else{
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|walletid " + commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()));
 					String mailtemplet = commanUtilDao.getmailTemplet("signUp",walletMastBean.getAggreatorid()).replaceAll("<<NAME>>", walletMastBean.getName());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
 					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
			}
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|problem in mobileValidation" + e.getMessage() +" "+e);
		} finally {
			session.close();
		}

		return statusCode+"|"+agentid;
	}

	
	
	
	public String distributerOnBoad(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","distributerOnBoad()"
				);
		logger.info("Start excution ********************************************* method distributerOnBoad(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		UserRoleMapping userRoleMapping= new UserRoleMapping();
		try {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid() +"|Sub" + walletMastBean.getSubAgentId()
					);
	
			transaction = session.beginTransaction();
			
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno()
						);
				logger.info("Inside *****************distributerOnBoad****************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Aggreator Id is empty"
						);
				//logger.info("Inside *****************distributerOnBoad****************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				return "7049";
			}
			
			
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno()
						);
				//logger.info("Inside ****************distributerOnBoad*****************************invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
				
			}
			
	
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Email register with us" + walletMastBean.getEmailid()
						);
				//logger.info("Inside ******************distributerOnBoad***************************Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad mobile register with us" + walletMastBean.getMobileno()
						);
				//logger.info("Inside ********************distributerOnBoad*************************mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}
			if (checkPanCard(walletMastBean.getPan(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad pan register with us" + walletMastBean.getPan()
						);
				//logger.info("Inside ******************distributerOnBoad***************************pan register with us" + walletMastBean.getPan());
				return "7003";
			}
			if (checkAadhar(walletMastBean.getAdhar(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad adhar register with us" + walletMastBean.getAdhar()
						);
				//logger.info("Inside ********************distributerOnBoad*************************adhar register with us" + walletMastBean.getAdhar());
				return "7004";
			}
	
			Query query = session.createQuery("delete FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile No "   + walletMastBean.getMobileno()
					);
			//logger.info("*********************************distributerOnBoad********************************************" + walletMastBean.getMobileno());
			
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad Invalid Aggreator " + walletConfiguration.getAppname()
						);
				//logger.info("*************************distributerOnBoad***********Invalid Aggreator********************************" + walletConfiguration.getAppname());
				return "7049";	
			}
			
				fstpassword = commanUtilDao.createPassword(7);
				walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));	
		
			

			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
				walletMastBean.setDistributerid(agentid);
				String rType[]= {"9004"};
				
				userRoleMapping.setUserId(agentid);
				userRoleMapping.setRoleId("9004");
				userRoleMapping.setIpiemi(walletMastBean.getIpiemi());
				userRoleMapping.setAgent(walletMastBean.getAgent());
				userRoleMapping.setRoleType(rType);
				
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
			}else if (walletMastBean.getUsertype() == 7) {
				agentid = commanUtilDao.getTrxId("SUPERDIST",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid(agentid);
			}
			else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid("-1");
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "",""
					);
		
			
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			walletMastBean.setApprovalRequired("N");
			
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAddressProofType("Aadhar");
			
			walletBalanceBean.setWalletid(walletid);
						
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
		
			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
			
			session.save(walletMastBean);
			session.save(walletBalanceBean);
			session.save(userWalletConfigBean);
			//session.saveOrUpdate(userRoleMapping);
			
			transaction.commit();
			statusCode = "1000";
			
		   if (walletMastBean.getUsertype() == 3)
		   {
			 if(agentid!=null || !agentid.equalsIgnoreCase(""))
			 {
				UserRoleMapping dRole = (UserRoleMapping)session.get(UserRoleMapping.class, agentid);
				if(dRole==null) {
			    CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
			    commanUtilDao.saveUserRoleMapping(userRoleMapping);
				 }
			 }
		    }
			
			if (statusCode.equals("1000")) {
				
			
					
					String mailtemplet = commanUtilDao.getmailTemplet("agentOnBoard",walletMastBean.getAggreatorid())
							.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet
							);	
					//logger.info("~~~~~~~~~~~~~~~~~~distributerOnBoad~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
						String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid())
								.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|smstemplet "+smstemplet
										);
								//logger.info("~~~~~~~~~~~~~~~~~distributerOnBoad~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
								SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
								
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					
				}
			
			return statusCode+"|"+agentid;
			
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|problem in distributerOnBoad " + e.getMessage() +" "+e
					);
			//logger.debug("problem in distributerOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return statusCode;
	}
	
	
	public String userOnBoad(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","userOnBoad()");
		logger.info("Start excution ********************************************* method userOnBoad(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		UserRoleMapping userRoleMapping= new UserRoleMapping();
		try {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid() +"|Sub" + walletMastBean.getSubAgentId());
	
			transaction = session.beginTransaction();
			
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno());
				logger.info("Inside *****************distributerOnBoad****************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Aggreator Id is empty");
				return "7049";
			}
			 
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
			 }
			 
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}
			if (checkPanCard(walletMastBean.getPan(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad pan register with us" + walletMastBean.getPan());
				return "7003";
			}
			if (checkAadhar(walletMastBean.getAdhar(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad adhar register with us" + walletMastBean.getAdhar());
				return "7004";
			}
	
			Query query = session.createQuery("delete FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile No "   + walletMastBean.getMobileno());
			
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad Invalid Aggreator " + walletConfiguration.getAppname());
				return "7049";	
			}
			
			fstpassword = commanUtilDao.createPassword(7);
			walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));	
	 
			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
				walletMastBean.setDistributerid(agentid);
				String rType[]= {"9004"};
				
				userRoleMapping.setUserId(agentid);
				userRoleMapping.setRoleId("9004");
				userRoleMapping.setIpiemi(walletMastBean.getIpiemi());
				userRoleMapping.setAgent(walletMastBean.getAgent());
				userRoleMapping.setRoleType(rType);
				
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
			}else if (walletMastBean.getUsertype() == 7) {
				agentid = commanUtilDao.getTrxId("SUPERDIST",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid(agentid);
			}
			else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid("-1");
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","");
		 
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			walletMastBean.setApprovalRequired("N");
			
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAddressProofType("Aadhar");
			
			walletBalanceBean.setWalletid(walletid);
						
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
		
			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
			
			session.save(walletMastBean);
			session.save(walletBalanceBean);
			session.save(userWalletConfigBean);
			//session.saveOrUpdate(userRoleMapping);
			
			transaction.commit();
			statusCode = "1000";
			
		   if (walletMastBean.getUsertype() == 3)
		   {
			 if(agentid!=null || !agentid.equalsIgnoreCase(""))
			 {
				UserRoleMapping dRole = (UserRoleMapping)session.get(UserRoleMapping.class, agentid);
				if(dRole==null) {
			    CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
			    commanUtilDao.saveUserRoleMapping(userRoleMapping);
				 }
			 }
		    }
			
			if (statusCode.equals("1000")) {
		 			
				String mailtemplet = commanUtilDao.getmailTemplet("agentOnBoard",walletMastBean.getAggreatorid())
						.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet
						);	
				//logger.info("~~~~~~~~~~~~~~~~~~distributerOnBoad~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid())
							.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|smstemplet "+smstemplet
									);
							//logger.info("~~~~~~~~~~~~~~~~~distributerOnBoad~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
							SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
	//						Thread t = new Thread(smsAndMailUtility);
	//						t.start();
							
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					
				}
			
			return statusCode+"|"+agentid;
			
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|problem in userOnBoad " + e.getMessage() +" "+e
					);
			//logger.debug("problem in distributerOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return statusCode;
	}
	
	// for all User onboard
	
  public String allUserOnBoad(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","userOnBoad()");
		logger.info("Start excution ********************************************* method allUserOnBoad(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		UserRoleMapping userRoleMapping= new UserRoleMapping();
		
		try {
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid() +"|Sub" + walletMastBean.getSubAgentId());
	      transaction = session.beginTransaction();
		  String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno());
				logger.info("Inside *****************distributerOnBoad****************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Aggreator Id is empty");
				return "7049";
			}
			 
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
			 }
			/* 
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			*/
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}
			if (checkPanCard(walletMastBean.getPan(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad pan register with us" + walletMastBean.getPan());
				return "7003";
			}
			if (checkAadhar(walletMastBean.getAdhar(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoad adhar register with us" + walletMastBean.getAdhar());
				return "7004";
			}
	
			Query query = session.createQuery("delete FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile No "   + walletMastBean.getMobileno());
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad Invalid Aggreator " + walletConfiguration.getAppname());
				return "7049";	
			}
			
			fstpassword = commanUtilDao.createPassword(7);
			walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));	
	 
			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
				walletMastBean.setAgentType("Retailer");
				walletMastBean.setAsCode(agentid);
				walletMastBean.setAgentCode(walletMastBean.getMobileno());
				walletMastBean.setAepsChannel("2");
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
				walletMastBean.setDistributerid(agentid);
				walletMastBean.setAgentType("Distributor");
				String rType[]= {"9004"};
				
				userRoleMapping.setUserId(agentid);
				userRoleMapping.setRoleId("9004");
				userRoleMapping.setIpiemi(walletMastBean.getIpiemi());
				userRoleMapping.setAgent(walletMastBean.getAgent());
				userRoleMapping.setRoleType(rType);
				
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
			}else if (walletMastBean.getUsertype() == 7) {
				agentid = commanUtilDao.getTrxId("SUPERDIST",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid(agentid);
				walletMastBean.setAgentType("Super Distributor");
				
                String rType[]= {"9004"};
				
				userRoleMapping.setUserId(agentid);
				userRoleMapping.setRoleId("9004");
				userRoleMapping.setIpiemi(walletMastBean.getIpiemi());
				userRoleMapping.setAgent(walletMastBean.getAgent());
				userRoleMapping.setRoleType(rType);
				
			}
			else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
				walletMastBean.setSuperdistributerid("-1");
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","");
		 
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			walletMastBean.setApprovalRequired("Y");
			
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAddressProofType("Aadhar");
			
			walletBalanceBean.setWalletid(walletid);
				
			if("OAGG001050".equalsIgnoreCase(walletMastBean.getAggreatorid()))
			walletMastBean.setWhiteLabel("0");
			else
			{
			walletMastBean.setWhiteLabel("1");	
			walletMastBean.setOnlineMoney(1);
			}
			
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
		
			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
			
	        if (walletMastBean.getUsertype() == 7) {
	         
	        	if ( !"-1".equalsIgnoreCase(walletMastBean.getSalesId())) {
	        		 WalletMastBean mmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getSalesId());
				        if(mmast!=null) {
				        	walletMastBean.setManagerId(mmast.getManagerId());
				        	walletMastBean.setManagerName(mmast.getManagerName());
				        	walletMastBean.setSoName(mmast.getSoName());
				        	//walletMastBean.setSuperdistributerid("-1");
				     } 
	        	}else if ( "-1".equalsIgnoreCase(walletMastBean.getManagerId())  && "-1".equalsIgnoreCase(walletMastBean.getSalesId()) ) {  
	        		walletMastBean.setManagerId("-1");
		        	walletMastBean.setManagerName("");
		        	walletMastBean.setSoName(""); 
		        	walletMastBean.setSalesId("-1");
	        	
	        	}else if ( !"-1".equalsIgnoreCase(walletMastBean.getManagerId())  && "-1".equalsIgnoreCase(walletMastBean.getSalesId()) ) {
		        		 WalletMastBean mmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getManagerId());
					        if(mmast!=null) {
					        	walletMastBean.setManagerId(mmast.getManagerId());
					        	walletMastBean.setManagerName(mmast.getManagerName());
					        	//walletMastBean.setSoName(mmast.getSoName());
					        	//walletMastBean.setSuperdistributerid("-1");
					     } 
		        	}
	         
	        }
	        
	        if (walletMastBean.getUsertype() == 3) {
		        if ( !"-1".equalsIgnoreCase(walletMastBean.getSuperdistributerid())) {
		        	WalletMastBean smast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getSuperdistributerid());
		            if(smast!=null)
		            {
		            walletMastBean.setSuperdistributerid(smast.getSuperdistributerid());
		            walletMastBean.setSalesId(smast.getSalesId());
		            walletMastBean.setManagerId(smast.getManagerId());
		            walletMastBean.setManagerName(smast.getManagerName());
		            walletMastBean.setSoName(smast.getSoName());
		            }
		         }else if ( "-1".equalsIgnoreCase(walletMastBean.getManagerId())  && "-1".equalsIgnoreCase(walletMastBean.getSalesId()) && "-1".equalsIgnoreCase(walletMastBean.getSuperdistributerid())) {  
		        		walletMastBean.setManagerId("-1");
			        	walletMastBean.setManagerName("");
			        	walletMastBean.setSoName(""); 
			        	walletMastBean.setSalesId("-1");
			        	walletMastBean.setSuperdistributerid("-1");
		         } else  if ( !"-1".equalsIgnoreCase(walletMastBean.getSalesId()) && "-1".equalsIgnoreCase(walletMastBean.getSuperdistributerid())  ) {
			        WalletMastBean mmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getSalesId());
			        if(mmast!=null) {
			        	walletMastBean.setManagerId(mmast.getManagerId());
			        	walletMastBean.setManagerName(mmast.getManagerName());
			        	walletMastBean.setSoName(mmast.getSoName());
			        	walletMastBean.setSuperdistributerid("-1");
			        }
			      }else  if ( !"-1".equalsIgnoreCase(walletMastBean.getSalesId()) && !"-1".equalsIgnoreCase(walletMastBean.getSuperdistributerid())  ) {
				        WalletMastBean mmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getSuperdistributerid());
				        if(mmast!=null) {
				        	walletMastBean.setManagerId(mmast.getManagerId());
				        	walletMastBean.setSalesId(mmast.getSalesId());
				        	walletMastBean.setManagerName(mmast.getManagerName());
				        	walletMastBean.setSoName(mmast.getSoName());
				        	walletMastBean.setSuperdistributerid("-1");
				        }
				      }
		     }
	        
	       
	        
	        if (walletMastBean.getUsertype() == 2) {
	          if(!walletMastBean.getDistributerid().equalsIgnoreCase("-1") || !walletMastBean.getDistributerid().equalsIgnoreCase(""))
		        {
			        WalletMastBean dmast = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getDistributerid());
			        if(dmast!=null)
			        {
			        	walletMastBean.setDistributorName(dmast.getDistributorName());
			        	walletMastBean.setDistributorMobileNo(dmast.getDistributorMobileNo());
			        	walletMastBean.setSuperdistributerid(dmast.getSuperdistributerid());
			        	walletMastBean.setSalesId(dmast.getSalesId());
			        	walletMastBean.setManagerId(dmast.getManagerId());
			        	walletMastBean.setSoName(dmast.getSoName());
			        	walletMastBean.setManagerName(dmast.getManagerName());
			        }
			    }
	        }
	        
	        
	        walletMastBean.setIsimps("0");
	        walletMastBean.setKycstatus("0");
	        walletMastBean.setWallet("0");
	        walletMastBean.setBank("0");
	        
			session.save(walletMastBean);
			session.save(walletBalanceBean);
			session.save(userWalletConfigBean);
			 
			transaction.commit();
			statusCode = "1000";
			
		   if (walletMastBean.getUsertype() == 3 || walletMastBean.getUsertype() == 7)
		   {
			 if(agentid!=null || !agentid.equalsIgnoreCase(""))
			 {
				UserRoleMapping dRole = (UserRoleMapping)session.get(UserRoleMapping.class, agentid);
				if(dRole==null) {
			    CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
			    commanUtilDao.saveUserRoleMappingNew(userRoleMapping);
				 }
			 }
		    }
			
		 /*  
			if (statusCode.equals("1000")) {
		 			
				String mailtemplet = commanUtilDao.getmailTemplet("agentOnBoard",walletMastBean.getAggreatorid())
						.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|mailtemplet "+mailtemplet);	
				String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid())
							.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|smstemplet "+smstemplet);
							 
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
	 			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			} 
		 */
			
			return statusCode+"|"+agentid;
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "","|problem in userOnBoad " + e.getMessage() +" "+e);
		} finally {
			session.close();
		}

		return statusCode;
	}


	
	
	
	
	public List<WalletMastBean> distributerOnBoardList(WalletMastBean walletMastBean){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoardList()"
				);
		//logger.info("Start excution ********************************************* method distributerOnBoardList(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<WalletMastBean> wmBean=new ArrayList<WalletMastBean>();
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside distributerOnBoardList Aggg"
					);
			
			//logger.info("Inside ******************distributerOnBoardList***************************Aggg" + walletMastBean.getAggreatorid());
			transaction = session.beginTransaction();
			
			//String aggId=walletMastBean.getAggreatorid();
						
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|distributerOnBoad Aggreator Id is empty"
						);
				//logger.info("Inside *****************distributerOnBoad****************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				//return "7049";
			}
			

//			Criteria criteria=session.createCriteria(WalletMastBean.class);
//			criteria.add(Restrictions.eq("aggreatorid",walletMastBean.getAggreatorid()));
//			if(walletMastBean.getUsertype()==7){
//			criteria.add(Restrictions.eq("superdistributerid",walletMastBean.getSuperdistributerid()));	
//			}
//		
			
			
			
			
			
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			 StringBuilder serchQuery=new StringBuilder();
			 serchQuery.append(" from WalletMastBean where aggreatorid='"+walletMastBean.getAggreatorid()+"' ");
			 if(walletMastBean.getUsertype()==7){
					serchQuery.append(" and superdistributerid='"+walletMastBean.getSuperdistributerid()+"'");	
			 }
			 
			 if(walletMastBean.getUsertype()==6){
				 if(!walletMastBean.getSuperdistributerid().equalsIgnoreCase("BPMU001000"))
					serchQuery.append(" and createdby='"+walletMastBean.getSuperdistributerid()+"'");	
			 }
			 
			 if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
				 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
			 }else{
				 serchQuery.append(" and DATE(creationDate)=NOW()");
			 }
			 serchQuery.append(" and userType in(3,7) order by creationDate DESC");
			 System.out.println("serchQuery~~~~"+ serchQuery);
			 Query query=session.createQuery(serchQuery.toString());
			 wmBean = query.list();
			 
			 
			 
			
			
			
			
			
			
			
//			criteria.add(Restrictions.in("usertype",new Integer[]{3,7}));
//			wmBean=criteria.list();
			
			return wmBean;
			
	
			
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in distributerOnBoad" + e.getMessage()+" "+e
					);
			//logger.debug("problem in distributerOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return wmBean;
	}
	
	
	
	
	public WalletMastBean agentOnBoad(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoad()"+"|method getCreatedby "+walletMastBean.getCreatedby()
				);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|method getCreatedby "+walletMastBean.getCreatedby()
				);*/
		//logger.info("Start excution ********************************************* method signUpUser(walletMastBean)");
		//logger.info("Start excution ********************************************* method getCreatedby"+walletMastBean.getCreatedby());
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		
		Query queryMob = session.createQuery("FROM WalletMastBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
		queryMob.setString("mobile", walletMastBean.getMobileno().trim());
		queryMob.setString("aggreatorid", walletMastBean.getAggreatorid().trim());
		List onboardList = queryMob.list(); 
		System.out.println("Onboard List size "+onboardList.size());
		if(onboardList.size()==0)
		{
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid()+"|Sub" + walletMastBean.getSubAgentId()
					);
			/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|Agent" + walletMastBean.getAgentid() 
					);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname +"|Sub" + walletMastBean.getSubAgentId()
					);*/
		/*	logger.info("Inside *********************************************Aggg" + walletMastBean.getAggreatorid());
			logger.info("Inside *********************************************Dist" + walletMastBean.getDistributerid());
			logger.info("Inside *********************************************Agent" + walletMastBean.getAgentid());
			logger.info("Inside *********************************************Sub" + walletMastBean.getSubAgentId());
*/			
			
			transaction = session.beginTransaction();
			
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number " + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				walletMastBean.setStatusCode("7040");
				return walletMastBean;
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty " 
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			
			
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number " + walletMastBean.getMobileno() +"|"+walletMastBean.getEmailid()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				walletMastBean.setStatusCode("7041");
				return walletMastBean;
				
				
			}
			
	    
			/*
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Email register with us" + walletMastBean.getEmailid()
						);
				//logger.info("Inside *********************************************Email register with us" + walletMastBean.getEmailid());
				walletMastBean.setStatusCode("7001");
				return walletMastBean;
				
			}
			*/
			
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|mobile register with us" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************mobile register with us" + walletMastBean.getMobileno());
				walletMastBean.setStatusCode("7002");
				return walletMastBean;
				
			}

	
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "| mobile number " +  walletMastBean.getMobileno()
					);
			//logger.info("*****************************************************************************" + walletMastBean.getMobileno());
			
			/*if(walletMastBean.getAggreatorid().equals("-1")){
			walletConfiguration = commanUtilDao.getWalletConfiguration("0");
			}else{
				walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			}*/
			
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Invalid Aggreator " + walletConfiguration.getAppname()
						);
				//logger.info("************************************Invalid Aggreator********************************" + walletConfiguration.getAppname());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				}
			
			//if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(7);
			//	walletMastBean.setPassword(HashValue.hashValue(fstpassword));
				walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));
		
		/*	}else{
				if(new PasswordValidator().validate(walletMastBean.getPassword())){
					//walletMastBean.setPassword(HashValue.hashValue(walletMastBean.getPassword()));
					walletMastBean.setPassword(CommanUtil.SHAHashing256(walletMastBean.getPassword()));
				}else{
					walletMastBean.setStatusCode("7038");
					return walletMastBean;
					
				}
			}*/
	

			if (walletMastBean.getUsertype() == 4) {
				agentid = commanUtilDao.getTrxId("AGGREATOR",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 2) {
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
				walletMastBean.setAgentCode(walletMastBean.getMobileno());
				walletMastBean.setAepsChannel("2");
				walletMastBean.setAsCode(agentid);
			} else if (walletMastBean.getUsertype() == 3) {
				agentid = commanUtilDao.getTrxId("DISTRIBUTER",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 5) {
				agentid = commanUtilDao.getTrxId("SUBAGENT",walletMastBean.getAggreatorid());
			} else if (walletMastBean.getUsertype() == 6) {
				agentid = commanUtilDao.getTrxId("AGGREATORADMIN",walletMastBean.getAggreatorid());
			}else{
				agentid = commanUtilDao.getTrxId("CUSTOMER",walletMastBean.getAggreatorid());
			}
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", ""
					);
			//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + walletid);
			
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			/*if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(6);
			walletMastBean.setPassword(HashValue.hashValue(fstpassword));
			}else{
				walletMastBean.setPassword(HashValue.hashValue(walletMastBean.getPassword()));
			}*/
			
			//For maker Checker
			if(walletConfiguration.getReqAgentApproval()==1){
				if(walletMastBean.getUsertype()==2||walletMastBean.getUsertype()==5)
					walletMastBean.setApprovalRequired("Y");
				else
					walletMastBean.setApprovalRequired("N");
			}else{
					walletMastBean.setApprovalRequired("N");
			}
			if(walletMastBean.getAggreatorid().equalsIgnoreCase("admin")){
				walletMastBean.setAggreatorid(agentid);
			}
			
			//For authorized the Aggrigator
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			
			if(walletMastBean.getAgentid()==null||walletMastBean.getAgentid().isEmpty())
				walletMastBean.setAgentid("-1");
			
			if(walletMastBean.getSubAgentId()==null||walletMastBean.getSubAgentId().isEmpty())
				walletMastBean.setSubAgentId("-1");
			
			
			
			
			walletBalanceBean.setWalletid(walletid);
			
			//walletDetailsBean.setId(agentid);
			
			
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
	      
	        
			/*userWalletConfigBean.setId(agentid);
			userWalletConfigBean.setWalletid(walletid);*/

			session.save(walletMastBean);
			session.save(walletBalanceBean);
			//session.save(walletDetailsBean);
			session.save(userWalletConfigBean);
			transaction.commit();
			statusCode = "1000";
			walletMastBean.setPan(EncryptionDecryption.getdecrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getdecrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", "problem in agentOnBoad" + e.getMessage() +" "+e
					);
			//logger.debug("problem in agentOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		walletMastBean.setStatusCode(statusCode);
		
		}else
		walletMastBean.setStatusCode(statusCode);
			
		return walletMastBean;
		//return statusCode;
	}
	
	
public WalletMastBean updateAgentDetails(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoad()"+"|method getCreatedby "+walletMastBean.getCreatedby()
				);
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid()+"|Sub" + walletMastBean.getSubAgentId()
					);
			
			transaction = session.beginTransaction();
			String aggId=walletMastBean.getAggreatorid();
			
			
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			

			Query query = session.createQuery("update WalletMastBean set name =:name, emailid=:emailid, mobileno=:mobileno, dob=:dob,"
					+ "pan=:pan, adhar=:adhar, address1=:address1, address2=:address2, city=:city, state=:state, pin=:pin, shopName=:shopName,"
					+ "shopAddress1=:shopAddress1, shopAddress2=:shopAddress2, shopCity=:shopCity, shopState=:shopState, shopPin=:shopPin,"
					+ "territory=:territory, managerName=:managerName, soName=:soName, distributerid=:distributerid, distributorName=:distributorName,"
					+ "distributorMobileNo=:distributorMobileNo, bankName=:bankName, accountNumber=:accountNumber, ifscCode=:ifscCode where id=:id");
			
			query.setParameter("name", walletMastBean.getName());
			query.setParameter("emailid", walletMastBean.getEmailid());
			query.setParameter("mobileno", walletMastBean.getMobileno());
			query.setParameter("dob", walletMastBean.getDob());
			query.setParameter("pan", walletMastBean.getPan());
			query.setParameter("adhar", walletMastBean.getAdhar());
			
			query.setParameter("address1", walletMastBean.getAddress1());
			query.setParameter("address2", walletMastBean.getAddress2());
			query.setParameter("city", walletMastBean.getCity());
			query.setParameter("state", walletMastBean.getState());
			query.setParameter("pin", walletMastBean.getPin());
			
			
			query.setParameter("shopName", walletMastBean.getShopName());
			query.setParameter("shopAddress1", walletMastBean.getShopAddress1());
			query.setParameter("shopAddress2", walletMastBean.getShopAddress2());
			query.setParameter("shopCity", walletMastBean.getShopCity());
			query.setParameter("shopState", walletMastBean.getShopState());
			query.setParameter("shopPin", walletMastBean.getShopPin());
			
			
			query.setParameter("territory", walletMastBean.getTerritory());
			query.setParameter("managerName", walletMastBean.getManagerName());
			query.setParameter("soName", walletMastBean.getSoName());
			
			query.setParameter("distributerid", walletMastBean.getDistributerid());
			query.setParameter("distributorName", walletMastBean.getDistributorName());
			query.setParameter("distributorMobileNo", walletMastBean.getDistributorMobileNo());
			
			query.setParameter("bankName", walletMastBean.getBankName());
			query.setParameter("accountNumber", walletMastBean.getAccountNumber());
			query.setParameter("ifscCode", walletMastBean.getIfscCode());
			
			query.setParameter("id", walletMastBean.getId());
			
			int i = query.executeUpdate();
			
//			userWalletConfigKey.setId(agentid);
//			userWalletConfigKey.setWalletid(walletid);
//			userWalletConfigKey.setTxncode(0);
//			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
//			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
//			session.save(walletBalanceBean);
//			//session.save(walletDetailsBean);
//			session.save(userWalletConfigBean);
			transaction.commit();
			if(i>0) {
				statusCode = "1000";
			}
			
			walletMastBean.setPan(EncryptionDecryption.getdecrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getdecrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", "problem in agentOnBoad" + e.getMessage() +" "+e
					);
			//logger.debug("problem in agentOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		walletMastBean.setStatusCode(statusCode);
		
		
		return walletMastBean;
		//return statusCode;
	}
	
		
	
	
	public WalletMastBean saveSelfAgentOnBoard(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|agentOnBoad()"+"|method getCreatedby "+walletMastBean.getCreatedby()
				);

		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid()+"|Sub" + walletMastBean.getSubAgentId()
					);
			transaction = session.beginTransaction();
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number " + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				walletMastBean.setStatusCode("7040");
				return walletMastBean;
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty " 
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			
			
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number " + walletMastBean.getMobileno() +"|"+walletMastBean.getEmailid()
						);
				walletMastBean.setStatusCode("7041");
				return walletMastBean;
				
				
			}
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Email register with us" + walletMastBean.getEmailid()
						);
				walletMastBean.setStatusCode("7001");
				return walletMastBean;
				
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|mobile register with us" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************mobile register with us" + walletMastBean.getMobileno());
				walletMastBean.setStatusCode("7002");
				return walletMastBean;
				
			}

	
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "| mobile number " +  walletMastBean.getMobileno()
					);
			
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Invalid Aggreator " + walletConfiguration.getAppname()
						);
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				}
			
				fstpassword = commanUtilDao.createPassword(7);
				walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));	
			
				agentid = commanUtilDao.getTrxId("AGENT",walletMastBean.getAggreatorid());
			
			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", ""
					);
			
			walletMastBean.setId(agentid);
			walletMastBean.setWalletid(walletid);
			
			walletMastBean.setUsertype(2);
			walletMastBean.setAgentCode(walletMastBean.getMobileno());
			walletMastBean.setAepsChannel("2");
			walletMastBean.setAsCode(agentid);
			/*if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(6);
			walletMastBean.setPassword(HashValue.hashValue(fstpassword));
			}else{
				walletMastBean.setPassword(HashValue.hashValue(walletMastBean.getPassword()));
			}*/
			
			//For maker Checker
			if(walletConfiguration.getReqAgentApproval()==1){
				if(walletMastBean.getUsertype()==2||walletMastBean.getUsertype()==5)
					walletMastBean.setApprovalRequired("Y");
				else
					walletMastBean.setApprovalRequired("N");
			}else{
					walletMastBean.setApprovalRequired("N");
			}
			if(walletMastBean.getAggreatorid().equalsIgnoreCase("admin")){
				walletMastBean.setAggreatorid(agentid);
			}
			
			//For authorized the Aggrigator
			if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}
			
			walletMastBean.setPlanId("0");
			walletMastBean.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			
			if(walletMastBean.getAgentid()==null||walletMastBean.getAgentid().isEmpty())
				walletMastBean.setAgentid("-1");
			
			if(walletMastBean.getSubAgentId()==null||walletMastBean.getSubAgentId().isEmpty())
				walletMastBean.setSubAgentId("-1");
			
			
			
			
			walletBalanceBean.setWalletid(walletid);
			
			//walletDetailsBean.setId(agentid);
			
			
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
			walletMastBean.setCreationDate(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setLastlogintime(new java.sql.Date(new java.util.Date().getTime()));
	        walletMastBean.setApproveDate(new java.sql.Date(new java.util.Date().getTime()));
			
			/*userWalletConfigBean.setId(agentid);
			userWalletConfigBean.setWalletid(walletid);*/

			session.save(walletMastBean);
			session.save(walletBalanceBean);
			//session.save(walletDetailsBean);
			session.save(userWalletConfigBean);
			transaction.commit();
			statusCode = "1000";
			walletMastBean.setPan(EncryptionDecryption.getdecrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBean.setAdhar(EncryptionDecryption.getdecrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", "problem in agentOnBoad" + e.getMessage() +" "+e
					);
			//logger.debug("problem in agentOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		walletMastBean.setStatusCode(statusCode);
		
		
		return walletMastBean;
		//return statusCode;
	}
	public WalletMastBean updateAgentOnBoad(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|updateAgentOnBoad() |Id"+walletMastBean.getId()+"|IdLoc"+walletMastBean.getIdLoc()+"|AddressLoc"+walletMastBean.getAddressLoc()+"|FormLoc"+walletMastBean.getFormLoc()
				);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|IdLoc"+walletMastBean.getIdLoc()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|AddressLoc"+walletMastBean.getAddressLoc()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|FormLoc"+walletMastBean.getFormLoc()
				);*/
		/*logger.info("Start excution ********************************************* method updateAgentOnBoad(walletMastBean)");
		logger.info("Start excution ***********************************updateAgentOnBoad********** method getCreatedby"+walletMastBean.getId());
		logger.info("Start excution ***********************************updateAgentOnBoad********** method getCreatedby"+walletMastBean.getIdLoc());
		logger.info("Start excution ***********************************updateAgentOnBoad********** method getCreatedby"+walletMastBean.getAddressLoc());
		logger.info("Start excution ***********************************updateAgentOnBoad********** method getCreatedby"+walletMastBean.getFormLoc());*/
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try{
		
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside Aggreator Id is empty"
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			if( walletMastBean.getId()==null ||walletMastBean.getId().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Inside Aggreator Id is empty" + walletMastBean.getId()
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getId());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			/*if( walletMastBean.getIdLoc()==null ||walletMastBean.getIdLoc().isEmpty()){
				logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getIdLoc());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			
			if( walletMastBean.getAddressLoc()==null ||walletMastBean.getAddressLoc().isEmpty()){
				logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAddressLoc());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}
			if( walletMastBean.getFormLoc()==null ||walletMastBean.getFormLoc().isEmpty()){
				logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getFormLoc());
				walletMastBean.setStatusCode("7049");
				return walletMastBean;
				
			}*/
			
			transaction=session.beginTransaction();
			WalletMastBean walletMastObj=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());	
			//walletMastObj.setApplicationFee(walletMastBean.getApplicationFee());
			
			if( walletMastBean.getIdLoc()!=null ){
			walletMastObj.setIdLoc(walletMastBean.getIdLoc());
			}
			if( walletMastBean.getAddressLoc()!=null ){
			walletMastObj.setAddressLoc(walletMastBean.getAddressLoc());
			}
			if( walletMastBean.getFormLoc()!=null ){
			walletMastObj.setFormLoc(walletMastBean.getFormLoc());
			}
			
			session.update(walletMastObj);
			transaction.commit();
			statusCode = "1000";
		
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in updateAgentOnBoad" + e.getMessage()+" "+e
					);
			//logger.debug("problem in updateAgentOnBoad==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		walletMastBean.setStatusCode(statusCode);
		return walletMastBean;
	}
	
	
	
	
	
	
	
	
	public String signUpUserDMT(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|signUpUserDMT()"
				);
		
		//logger.info("Start excution ********************************************* method signUpUser(walletMastBean)");
		String statusCode = "1001";
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String agentid = null;
		String fstpassword = null;
		String walletid = null;
		WalletBalanceBean walletBalanceBean = new WalletBalanceBean();
		UserWalletConfigBean userWalletConfigBean = new UserWalletConfigBean();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Dist" + walletMastBean.getDistributerid() +"|Agent" + walletMastBean.getAgentid()+"|Sub" + walletMastBean.getSubAgentId()
					);
/*			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname+"|Agent" + walletMastBean.getAgentid() 
					);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|"+methodname +"|Sub" + walletMastBean.getSubAgentId()
					);*/
			/*logger.info("Inside *********************************************Aggg" + walletMastBean.getAggreatorid());
			logger.info("Inside *********************************************Dist" + walletMastBean.getDistributerid());
			logger.info("Inside *********************************************Agent" + walletMastBean.getAgentid());
			logger.info("Inside *********************************************Sub" + walletMastBean.getSubAgentId());*/
			
			
			transaction = session.beginTransaction();
			
			String aggId=walletMastBean.getAggreatorid();
			
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", walletMastBean.getMobileno()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				return "7040";
			}
			
			if( walletMastBean.getAggreatorid()==null ||walletMastBean.getAggreatorid().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Aggreator Id is empty"
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + walletMastBean.getAggreatorid());
				return "7049";
			}
			
			
			
			if(!(new EmailValidator().validate(walletMastBean.getEmailid()))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalid mobile number" + walletMastBean.getMobileno()
						);
			//	logger.info("Inside *********************************************invalid mobile number" + walletMastBean.getMobileno());
				return "7041";
				
			}
			
	
			if (checkEailId(walletMastBean.getEmailid(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Email register with us" + walletMastBean.getEmailid()
						);
				//logger.info("Inside *********************************************Email register with us" + walletMastBean.getEmailid());
				return "7001";
			}
			if (checkMobileNo(walletMastBean.getMobileno(),walletMastBean.getAggreatorid())) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "||mobile register with us" + walletMastBean.getMobileno()
						);
				//logger.info("Inside *********************************************mobile register with us" + walletMastBean.getMobileno());
				return "7002";
			}

	
			Query query = session.createQuery(
					" delete  FROM WalletMastBean WHERE (mobileno=:mobile or  emailid=:emailNo )and userstatus= 'D' and aggreatorid=:aggreatorid");
			query.setString("mobile", walletMastBean.getMobileno());
			query.setString("emailNo", walletMastBean.getEmailid());
			query.setString("aggreatorid", walletMastBean.getAggreatorid());
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Mobile No"+ walletMastBean.getMobileno()
					);
			//logger.info("*****************************************************************************" + walletMastBean.getMobileno());
			
						
			walletConfiguration = commanUtilDao.getWalletConfiguration(walletMastBean.getAggreatorid());
			if(walletConfiguration.getCountrycurrency()==null ||walletConfiguration.getCountrycurrency().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Invalid Aggreator" + walletConfiguration.getAppname()
						);
				//logger.info("************************************Invalid Aggreator********************************" + walletConfiguration.getAppname());
				return "7049";	
			}
			if(new PasswordValidator().validate(walletMastBean.getPassword())){
					walletMastBean.setPassword(CommanUtil.SHAHashing256(walletMastBean.getPassword()));
			}else{
					return "7038";
			}
	
	


			walletid = commanUtilDao.generateWalletID(walletMastBean.getMobileno());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletid,"", "", "", ""
					);
			//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + walletid);
			agentid=walletMastBean.getId();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|Agent" + agentid
					);
			//logger.info("~~~~~~~~~~~~~~~~~~~~~agentid~~~~~~~~~~~~~~~~~~~````" + agentid);
			walletMastBean.setWalletid(walletid);
			/*if(walletConfiguration.getEmailvalid()==1){
				fstpassword = commanUtilDao.createPassword(6);
			walletMastBean.setPassword(HashValue.hashValue(fstpassword));
			}else{
				walletMastBean.setPassword(HashValue.hashValue(walletMastBean.getPassword()));
			}*/
			
			//For maker Checker
			/*if(walletConfiguration.getReqAgentApproval()==1){
				if(walletMastBean.getUsertype()==2||walletMastBean.getUsertype()==5)
					walletMastBean.setApprovalRequired("Y");
				else
					walletMastBean.setApprovalRequired("N");
			}else{
					walletMastBean.setApprovalRequired("N");
			}*/
			
			walletMastBean.setApprovalRequired("N");
		
			
			//For authorized the Aggrigator
		/*	if(walletMastBean.getUsertype()==4){
			walletMastBean.setUserstatus("N");
			}else{
			walletMastBean.setUserstatus("D");
			}*/
			
			walletMastBean.setUserstatus("A");
			
			walletMastBean.setPlanId("0");
			walletMastBean.setAgentType("SELF");
			walletBalanceBean.setWalletid(walletid);
			walletMastBean.setAddressProofType("Aadhar");
			
			if("OAGG001050".equalsIgnoreCase(walletMastBean.getAggreatorid())){
				walletMastBean.setCreatedby("BPMU001095");
			}else if("OAGG001054".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
				walletMastBean.setCreatedby("TPMU001096");
			}else if("OAGG001057".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
				walletMastBean.setCreatedby("PROM001098");
			}else if("OAGG001060".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
				walletMastBean.setCreatedby("TURM001003");
			}
			
			
			//walletDetailsBean.setId(agentid);
			
			userWalletConfigKey.setId(agentid);
			userWalletConfigKey.setWalletid(walletid);
			userWalletConfigKey.setTxncode(0);
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			userWalletConfigBean.setAggreatorid(walletMastBean.getAggreatorid());
			
			/*userWalletConfigBean.setId(agentid);
			userWalletConfigBean.setWalletid(walletid);*/

			session.save(walletMastBean);
			session.save(walletBalanceBean);
			//session.save(walletDetailsBean);
			session.save(userWalletConfigBean);
			transaction.commit();
			statusCode = "1000";
/*			if (statusCode.equals("1000")) {
				
				if(aggId.equalsIgnoreCase("Admin")){
					 aggId="admin";
				}else{
					aggId=walletMastBean.getAggreatorid();
				}
				if(walletConfiguration.getEmailvalid()==1 && !(walletMastBean.getUsertype()==4)){
					String url=walletConfiguration.getProtocol()+"://"+walletConfiguration.getDomainName();
					logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
					String mailtemplet = commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<MOBILE>>", walletMastBean.getMobileno())	.replaceAll("<<EMAIL>>", walletMastBean.getEmailid()).replaceAll("<<PASSWORD>>", fstpassword).replaceAll("<<URL>>", url);
					logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
					String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid());
					//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",aggId,walletMastBean.getWalletid(),walletMastBean.getName());
					Thread t = new Thread(smsAndMailUtility);
					t.start();
				}else{
					if(walletConfiguration.getSmssend()!=0){
						String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid());
						logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", walletMastBean.getMobileno(), smstemplet, "SMS",aggId,walletMastBean.getWalletid(),walletMastBean.getName());
						Thread t = new Thread(smsAndMailUtility);
						t.start();
					}
				}
			}*/
			
		} catch (Exception e) {
			statusCode = "7000";
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in mobileValidation " + e.getMessage()+" "+e
					);
			//logger.debug("problem in mobileValidation==================" + e.getMessage(), e);
		} finally {
			session.close();
		}

		return statusCode;
	}
	
	
	
	
	
	
	
	
	
	

	/**
	 * checkEailId method used to validate mail for active user
	 * Returns True- if mail is assign with active user 
	 * Returns False- if mail is assign with not active user 
	 * 
	 * @param  email  email of user 
	 * @param  aggreatorid  aggreator id of user
	 * @return  Boolean 
	 */
	
	private Boolean checkEailId(String email,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkEailId()"+"|emailid :"+email
				);
		//logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + email);
		//logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE emailid=:emailid and userstatus in ('A','S') and aggreatorid=:aggreatorid");
			query.setString("emailid", email);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkEailId"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}
	
	
	private Boolean checkPanCard(String panCard,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkPanCard() |pancard :"
				+ panCard);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + panCard);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE pan=:pan and userstatus in ('A','S') and aggreatorid=:aggreatorid");
			query.setString("pan", panCard);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkPanCard"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}
	
	private Boolean checkAadhar(String aAdhar,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkAadhar()"+"|Aadhar :"
				+aAdhar);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aAdhar);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE adhar=:adhar and userstatus in ('A','S') and aggreatorid=:aggreatorid");
			query.setString("adhar", aAdhar);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkAadhar"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	
	/**
	 * checkMobileNo method used to validate mobile for active user
	 * Returns True- if mobile is assign with active user 
	 * Returns False- if mobile is assign with not active user 
	 * 
	 * @param  mobile  mobile of user 
	 * @return  Boolean 
	 */
	public Boolean checkMobileNo(String mobile,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkMobileNo()"+"|Mobile No :"
				+mobile);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + mobile);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:mobile and userstatus in ('A','S') and aggreatorid=:aggreatorid");
			query.setString("mobile", mobile);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkMobileNo"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	public Boolean checkActiveUser(String mobile,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkActiveUser()| Mobile No "
				+mobile);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + mobile);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:mobile and userstatus in ('D') and aggreatorid=:aggreatorid");
			query.setString("mobile", mobile);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkActiveUser"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}
	
	/**
	 * checkEailIdWhileLogin method used to check  email is register with us.
	 * Returns True- if email  register with us 
	 * Returns False- if email not register with us 
	 * 
	 * @param  email  mobile of user 
	 * @return  Boolean 
	 */
	private Boolean checkEailIdWhileLogin(String email,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkEailIdWhileLogin()|email: "
				+email);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + email);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE emailid=:emailid and aggreatorid=:aggreatorid");
			query.setString("emailid", email);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkEailIdWhileLogin"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	
	/**
	 * checkMobileNoWhileLogin method used to check  mobile is register with us.
	 * Returns True- if mobile  register with us 
	 * Returns False- if mobile not register with us 
	 * 
	 * @param  mobile  mobile of user 
	 * @return  Boolean 
	 */
	private Boolean checkMobileNoWhileLogin(String mobile,String aggreatorid) {
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checkMobileNoWhileLogin()|"
				+mobile);
		/*logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + mobile);
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`" + aggreatorid);*/
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid");
			query.setString("mobile", mobile);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				flag = true;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Problem in checkMobileNoWhileLogin"+e.getMessage()+ " "+e
					);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	
	
	
	
	public WalletMastBean updateUserProfile(WalletMastBean walletMastBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|updateUserProfile()|Userstatus"
				+walletMastBean.getName());
		//logger.info("Start excution ********************************************* method updateUserProfile( walletMastBean)--getUserstatus"+walletMastBean.getName());
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {
			if (walletMastBean.getId() !=null ) {
				
				
				
				Query query = session.createQuery(
						"update WalletMastBean set name=:username,address1=:address1,address2= :address2,city=:userCity,state=:userState,pin=:userPin,pan=:pan,adhar=:userAdh,userstatus=:userstatus where id= :id");

				query.setString("username", walletMastBean.getName());
				query.setString("address1", walletMastBean.getAddress1());
				query.setString("address2", walletMastBean.getAddress2());
				query.setString("userCity", walletMastBean.getCity());
				query.setString("userState", walletMastBean.getState());
				query.setString("userPin", walletMastBean.getPin());
				try{
				if(walletMastBean!=null&&walletMastBean.getPan()!=null&!walletMastBean.getPan().isEmpty())
				query.setString("pan",EncryptionDecryption.getEncrypted(walletMastBean.getPan(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				else
				query.setString("pan","");
				if(walletMastBean!=null&&walletMastBean.getAdhar()!=null&!walletMastBean.getAdhar().isEmpty())
				query.setString("userAdh", EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				else
				query.setString("userAdh","");
				}catch(Exception e){
					e.printStackTrace();
				}
				query.setString("userstatus", walletMastBean.getUserstatus());
				query.setString("id", walletMastBean.getId());
				
				query.executeUpdate();
				transaction.commit();
				walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());
				
					try{
					if(walletMastBean!=null&&walletMastBean.getPan()!=null&!walletMastBean.getPan().isEmpty())
						walletMastBean.setPan(EncryptionDecryption.getdecrypted(walletMastBean.getPan(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
					if(walletMastBean!=null&&walletMastBean.getAdhar()!=null&!walletMastBean.getAdhar().isEmpty())
						walletMastBean.setAdhar(EncryptionDecryption.getdecrypted(walletMastBean.getAdhar(),"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
					}catch(Exception e){
						e.printStackTrace();
					}
				return walletMastBean;
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|invalied user id"
						+walletMastBean.getName());
				//logger.info("Start excution ********************************************* invalied user id");
				return walletMastBean;
			}
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in updateUserProfile " + e.getMessage()+" "+e
					+walletMastBean.getName());
			//logger.debug("problem in updateUserProfile==================" + e.getMessage(), e);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			return walletMastBean;
		} finally {
			session.close();
		}
	}

	
	
	@Override
	public AgentDetailsView getAgentDetails(String agentId,String aggreatorId) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|getAgentDetails()|agentid"
				+agentId);
		//logger.info("Start excution ************************************* getAgentDetails method**:" + agentId);
		//logger.info("Start excution ************************************* getAgentDetails method**aggreatorid:" + aggreatorId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		AgentDetailsView agentdetailsview = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT * FROM agentdetailsview WHERE (id=:agentId or mobileno=:mobileno) and aggreatorid=:aggreatorId");
			query.setParameter("agentId", agentId);
			query.setParameter("mobileno", agentId);
			query.setParameter("aggreatorId", aggreatorId);
			query.setResultTransformer(Transformers.aliasToBean(AgentDetailsView.class));
			List list = query.list();
			if (list != null && list.size() != 0) {
				agentdetailsview = (AgentDetailsView)query.list().get(0);
				if(agentdetailsview.getPan()!=null&&!agentdetailsview.getPan().isEmpty())
				agentdetailsview.setPan(EncryptionDecryption.getdecrypted(agentdetailsview.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				if(agentdetailsview.getAdhar()!=null&&!agentdetailsview.getAdhar().isEmpty())
				agentdetailsview.setAdhar(EncryptionDecryption.getdecrypted(agentdetailsview.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|problem in getAgentDetails" + e.getMessage()+" "+e
					);
			//logger.debug("problem in getAgentDetails*****************************************" + e.getMessage(), e);

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|return");

		return agentdetailsview;

	}
	

	
	@Override
	public AgentDetailsView newBpayAgentdetailsview(String agentId,String aggreatorId) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|getAgentDetails()|agentid"
				+agentId);
		//logger.info("Start excution ************************************* getAgentDetails method**:" + agentId);
		//logger.info("Start excution ************************************* getAgentDetails method**aggreatorid:" + aggreatorId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		AgentDetailsView agentdetailsview = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT * FROM agentdetailsview WHERE (id=:agentId or mobileno=:mobileno) and aggreatorid=:aggreatorId");
			query.setParameter("agentId", agentId);
			query.setParameter("mobileno", agentId);
			query.setParameter("aggreatorId", aggreatorId);
			query.setResultTransformer(Transformers.aliasToBean(AgentDetailsView.class));
			List list = query.list();
			if (list != null && list.size() != 0) {
				agentdetailsview = (AgentDetailsView)query.list().get(0);
				if(agentdetailsview.getPan()!=null&&!agentdetailsview.getPan().isEmpty())
				agentdetailsview.setPan(EncryptionDecryption.getdecrypted(agentdetailsview.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				if(agentdetailsview.getAdhar()!=null&&!agentdetailsview.getAdhar().isEmpty())
				agentdetailsview.setAdhar(EncryptionDecryption.getdecrypted(agentdetailsview.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|problem in getAgentDetails" + e.getMessage()+" "+e);
 		e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|return");

		return agentdetailsview;

	}

	
	
	
	
	
	/*public UserProfileBean showUserProfilebymobile(String emailMobile,String aggreatorid) {
		logger.info("Start excution ********************************************* method showUserProfilebymobile( emailMobile,aggreatorid)"+emailMobile);
		logger.info("Start excution ********************************************* method showUserProfilebymobile( emailMobile,aggreatorid)"+aggreatorid);
		Session session = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		UserProfileBean walletMastBean = new UserProfileBean();
		try {
			//Query query = session.createSQLQuery("SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,a.usertype,b.finalBalance,a.aggreatorid,a.distributerid,a.agentid,a.subagentid,a.addressprooftype FROM walletmast a,walletbalance b WHERE a.walletid=b.walletid AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
//			Query query = session.createSQLQuery("SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,a.usertype,b.finalBalance,a.aggreatorid,a.distributerid,a.agentid,a.subagentid,a.addressprooftype,c.balance,d.token,d.tokenSecurityKey FROM walletmast a,walletbalance b, cashbackwalletbalance c, walletsecurity d WHERE a.walletid=b.walletid  AND a.walletid=c.walletid and a.walletid=d.walletId AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
			Query query = session.createSQLQuery("SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,a.usertype,b.finalBalance,a.aggreatorid,a.distributerid,a.agentid,a.subagentid,a.addressprooftype,c.balance,a.kycstatus FROM walletmast a,walletbalance b, cashbackwalletbalance c WHERE a.walletid=b.walletid  AND a.walletid=c.walletid AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
			//Query query = session.createSQLQuery(" SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,b.finalBalance  FROM walletmast a,walletbalance b WHERE a.walletid=b.walletid AND (mobileno=:userid OR emailid=:userid)");
			query.setString("userid", emailMobile);
			query.setString("aggreatorid", aggreatorid);
			List<Object[]> rows=query.list();
			
			for(Object[] row : rows){
				//System.out.println(row[0].toString());
				walletMastBean.setId(row[0].toString());
				walletMastBean.setWalletid(row[1].toString());
				walletMastBean.setMobileno(row[2].toString());
				walletMastBean.setEmailid(row[3].toString());
				walletMastBean.setName(row[4].toString());
				walletMastBean.setUsertype((int)row[5]);
				walletMastBean.setFinalBalance((double)row[6]);
				walletMastBean.setAggreatorid(row[7].toString());
				walletMastBean.setDistributerid(row[8].toString());
				walletMastBean.setAgentid(row[9].toString());
				walletMastBean.setSubAgentId(row[10].toString());
				walletMastBean.setAddressProofType(row[11].toString());
				walletMastBean.setCashBackFinalBalance((double)row[12]);
				if(row[13]!=null)
				walletMastBean.setKycStatus(row[13].toString());
//				walletMastBean.setToken(row[13].toString());
//				walletMastBean.setTokenSecurityKey(row[14].toString());
				
				 *//**************** start change by ambuj singh*******************//*
			    try{
			     if (walletMastBean.getId() != null && !walletMastBean.getId().isEmpty()) {
			      UserRoleMapping urm = (UserRoleMapping) session.get(UserRoleMapping.class,
			        walletMastBean.getId());
			      if (urm != null && urm.getRoleId() != null && !urm.getRoleId().isEmpty()) {
			       String roleId = urm.getRoleId();
			       String stArr[] = roleId.split(",");
			       List<String> typeList = Arrays.asList(stArr);
			       if (typeList.contains("9004")) {
			        walletMastBean.setCme(true);
			       } else {
			        walletMastBean.setCme(false);
			       }
			      } else {
			       walletMastBean.setCme(false);
			      }
			     }
			   }catch(Exception e){
			    logger.debug("gettting error in finding ISCME "+e);
			   }
			    *//**************** end change by ambuj singh*******************//*
			
			}
		} catch (HibernateException e) {
			logger.debug("problem in showUserProfile==================" + e.getMessage(), e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		logger.info("showUserProfilebymobile =========walletMastBean============ method getId"+walletMastBean.getId());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getWalletid"+walletMastBean.getWalletid());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getMobileno"+walletMastBean.getMobileno());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getEmailid"+walletMastBean.getEmailid());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getName"+walletMastBean.getName());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getUsertype"+walletMastBean.getUsertype());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getFinalBalance"+walletMastBean.getFinalBalance());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getAggreatorid"+walletMastBean.getAggreatorid());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getDistributerid"+walletMastBean.getDistributerid());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getAgentid"+walletMastBean.getAgentid());
		logger.info("showUserProfilebymobile =========walletMastBean============ method getSubAgentId"+walletMastBean.getSubAgentId());

		return walletMastBean;
	}
	
	*/
	
	
	public UserProfileBean showUserProfilebymobile(String emailMobile,String aggreatorid) {
		
		System.out.println("_________emailMobile______"+emailMobile+"____________aggreatorid_________"+aggreatorid);
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|showUserProfilebymobile()|"+emailMobile);
		
		
	 //logger.info("Start excution ********************************************* method showUserProfilebymobile( emailMobile,aggreatorid)"+emailMobile);
	 // logger.info("Start excution ********************************************* method showUserProfilebymobile( emailMobile,aggreatorid)"+aggreatorid);
		
	  Session session = null;
	  factory = DBUtil.getSessionFactory();
	  session = factory.openSession();
	  UserProfileBean walletMastBean = new UserProfileBean();
	  
	  try {
	   //Query query = session.createSQLQuery("SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,a.usertype,b.finalBalance,a.aggreatorid,a.distributerid,a.agentid,a.subagentid,a.addressprooftype FROM walletmast a,walletbalance b WHERE a.walletid=b.walletid AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
	   // Query query = session.createSQLQuery("SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,a.usertype,b.finalBalance,a.aggreatorid,a.distributerid,a.agentid,a.subagentid,a.addressprooftype,c.balance,d.token,d.tokenSecurityKey FROM walletmast a,walletbalance b, cashbackwalletbalance c, walletsecurity d WHERE a.walletid=b.walletid  AND a.walletid=c.walletid and a.walletid=d.walletId AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
	  
		  
		  Query query = session.createSQLQuery("SELECT "
			  		+ "a.id,a.walletid,a.mobileno,"
			  		+ "a.emailid,a.name,a.usertype,"
			  		+ "b.finalBalance,a.aggreatorid,"
			  		+ "a.distributerid,a.agentid,"
			  		+ "a.subagentid,a.addressprooftype,"
			  		+ "case when a.id like 'OAGG%' then getDepositBalance(a.id) else c.balance end as balance,a.kycstatus,a.isimps,"
			  		+ "a.wallet,a.bank,a.whitelabel,"
			  		+ "a.minikycstatus,a.superdistributerid,"
			  		+ "a.portalmessage,a.agentcode,"
			  		+ "a.otp_verification_required,"
			  		+ "a.asagentcode,a.aepschannel,a.shopname,a.ascode, "
			  		+ "a.bank1,a.bank2,a.bank3,a.bank3_via, "
			  		+ "a.online_money,a.recharge,a.bbps "
			  		+ "FROM walletmast a,walletbalance b, cashbackwalletbalance c "
			  		+ "WHERE a.walletid=b.walletid  AND a.walletid=c.walletid AND  (mobileno=:userid OR emailid=:userid OR id=:userid) and aggreatorid=:aggreatorid");
		   


		  
		  //Query query = session.createSQLQuery(" SELECT a.id,a.walletid,a.mobileno,a.emailid,a.name,b.finalBalance  FROM walletmast a,walletbalance b WHERE a.walletid=b.walletid AND (mobileno=:userid OR emailid=:userid)");
	   query.setString("userid", emailMobile);
	   query.setString("aggreatorid", aggreatorid);
	   
	   
	   List<Object[]> rows=query.list();
	   
	   for(Object[] row : rows){
		   
	    //System.out.println(row[0].toString());
	    walletMastBean.setId(row[0].toString());
	    walletMastBean.setWalletid(row[1].toString());
	    walletMastBean.setMobileno(row[2].toString());
	    walletMastBean.setEmailid(row[3].toString());
	    walletMastBean.setName(row[4].toString());
	    walletMastBean.setUsertype((int)row[5]);
	    walletMastBean.setFinalBalance((double)row[6]);
	    walletMastBean.setAggreatorid(row[7].toString());
	    walletMastBean.setDistributerid(row[8].toString());
	    walletMastBean.setAgentid(row[9].toString());
	    walletMastBean.setSubAgentId(row[10].toString());
	    walletMastBean.setAddressProofType(row[11].toString());
	    walletMastBean.setCashBackFinalBalance((double)row[12]);
	    if(row[13]!=null)
	    walletMastBean.setKycStatus(row[13].toString());
	    if(row[14]!=null)
	    walletMastBean.setIsimps(row[14].toString());
	    if(row[15]!=null)
	    walletMastBean.setWallet(row[15].toString());
	    if(row[16]!=null)
	    walletMastBean.setBank(row[16].toString());
	    if(row[17]!=null)
		walletMastBean.setWhiteLabel(row[17].toString());
	    if(row[18]!=null)
	    walletMastBean.setMiniKycStatus(Integer.parseInt(row[18].toString()));
	    if(row[19]!=null)
	    walletMastBean.setSuperdistributerid(row[19].toString());
	    if(row[20]!=null)
		    walletMastBean.setPortalMessage(row[20].toString());
	    if(row[21]!=null)
		    walletMastBean.setAgentCode(row[21].toString());
	    if(row[22]!=null)
		    walletMastBean.setOtpVerificationRequired(row[22].toString());
	    else
	    	 walletMastBean.setOtpVerificationRequired("0");
	    if(row[23]!=null)
	    	walletMastBean.setAsAgentCode(row[23].toString());
	    if(row[24]!=null)
	    	walletMastBean.setAepsChannel(row[24].toString());
	    if(row[25]!=null)
	    	walletMastBean.setShopName(row[25].toString());
	    if(row[26]!=null)
	    	walletMastBean.setAsCode(row[26].toString());
	    if(row[27]!=null)
	    	walletMastBean.setBank1(row[27].toString());
	    if(row[28]!=null)
	    	walletMastBean.setBank2(row[28].toString());
	    if(row[29]!=null)
	    	walletMastBean.setBank3(row[29].toString());
	    if(row[30]!=null)
	    	walletMastBean.setBank3_via(row[30].toString());
	    if(row[31]!=null)
	    	walletMastBean.setOnlineMoney(Integer.parseInt(row[31].toString()));
	    if(row[32]!=null)
	    	walletMastBean.setRecharge(Integer.parseInt(row[32].toString()));
	    if(row[33]!=null)
	    	walletMastBean.setBbps(Integer.parseInt(row[33].toString()));
//	    walletMastBean.setToken(row[13].toString());
//	    walletMastBean.setTokenSecurityKey(row[14].toString());
	    try {
	    WalletSecurityBean   securityBean = (WalletSecurityBean)session.get(WalletSecurityBean.class, walletMastBean.getId());
	    walletMastBean.setToken(securityBean.getToken());
	    walletMastBean.setTokenSecurityKey(securityBean.getTokenSecurityKey());
	   // walletMastBean.setToken("");
	   // walletMastBean.setTokenSecurityKey("");
	    
	    }catch (Exception e) {
			e.printStackTrace();
		}
	    
	     /**************** start change by ambuj singh*******************/
	       try{
	        if (walletMastBean.getId() != null && !walletMastBean.getId().isEmpty()) 
	        {
	         UserRoleMapping urm = (UserRoleMapping) session.get(UserRoleMapping.class,walletMastBean.getId());
	         
	         if (urm != null && urm.getRoleId() != null && !urm.getRoleId().isEmpty()) 
	         {
	          String roleId = urm.getRoleId();
	          
	          System.out.println("________________roleId______________"+roleId);
	          
	          String stArr[] = roleId.split(",");
	          
	          List<String> typeList = Arrays.asList(stArr);
	          
	          for(String list:typeList)
	          {
	        	  System.out.println("_________list___________"+list);
	          }
	          
	          if (typeList.contains("9004")) 
	          {
	           walletMastBean.setCme(true);
	          } else {
	           walletMastBean.setCme(false);
	          }
	         } else {
	          walletMastBean.setCme(false);
	         }
	        }
	      }catch(Exception e){
	    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|gettting error in finding ISCME "+e.getMessage()+" "+e
	  			);
	      // logger.debug("gettting error in finding ISCME "+e);
	      }
	       /**************** end change by ambuj singh*******************/
	   
	   }
	  } catch (HibernateException e) {
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|problem in showUserProfile" + e.getMessage()+" "+e
					);
	  // logger.debug("problem in showUserProfile==================" + e.getMessage(), e);
	   e.printStackTrace();
	  } finally {
	   session.close();
	  }
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|getId"+walletMastBean.getId() + "|getMobileno"+walletMastBean.getMobileno()+"|getEmailid"+walletMastBean.getEmailid()+"|getName"+walletMastBean.getName()+"|getUsertype"+walletMastBean.getUsertype()+"| getFinalBalance"+walletMastBean.getFinalBalance()+"|getDistributerid"+walletMastBean.getDistributerid()+ "|getAgentid"+walletMastBean.getAgentid()+"|getSubAgentId"+walletMastBean.getSubAgentId()
				);

	  return walletMastBean;
	  }
	
	public Map<String,String> getDashBoardDMTData(String emailMobile,String aggreatorid){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|getDashBoardDMTData()|"
				+emailMobile);
	  Session session = null;
	  factory = DBUtil.getSessionFactory();
	  session = factory.openSession();
	  HashMap<String,String> hashSet=new HashMap<String,String>();
	  
	  try {
		  
	   WalletMastBean wBean=(WalletMastBean)session.get(WalletMastBean.class,emailMobile);
		  
   if(wBean!=null&&wBean.getUsertype()==2){
	   
	     SQLQuery query = session.createSQLQuery("SELECT sum(txndebit)-sum(txncredit) as txnAmt from wallettransaction where date(txndate)=CURDATE() and userid=:userid and aggreatorid=:aggreatorid and txncode in (4,25)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows1=query.list();
	     if(rows1!=null&&rows1.size()>0)
			   hashSet.put("MoneyTrans",""+(rows1.get(0)==null?"0.0":rows1.get(0)));
	     else
	    	 hashSet.put("MoneyTrans","0.0");
	     
	     query = session.createSQLQuery("SELECT sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and walletid in (select walletid from walletmast where id=:userid) and aggreatorid=:aggreatorid and txncode in (13,24,92)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows2=query.list();
		 if(rows2!=null&&rows2.size()>0)
			   hashSet.put("Deposit",""+(rows2.get(0)==null?"0.0":rows2.get(0)));
		 else
	    	 hashSet.put("Deposit","0.0");
	    
		 query = session.createSQLQuery("SELECT sum(txndebit)-sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and userid=:userid and aggreatorid=:aggreatorid and txncode in (5,6)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows3=query.list();
		 if(rows3!=null&&rows3.size()>0)
			   hashSet.put("Recharge",""+(rows3.get(0)==null?"0.0":rows3.get(0)));
		 else
	    	 hashSet.put("Recharge","0.0");
	    
		 
		 query = session.createSQLQuery("SELECT sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and userid=:userid and aggreatorid=:aggreatorid and txncode in (88)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows4=query.list();
		 if(rows4!=null&&rows4.size()>0)
			   hashSet.put("MATM",""+(rows4.get(0)==null?"0.0":rows4.get(0)));
		 else
	    	 hashSet.put("MATM","0.0");
	    
		 
		 
		 query = session.createSQLQuery("SELECT sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and userid=:userid and aggreatorid=:aggreatorid and txncode in (80)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows5=query.list();
		 if(rows5!=null&&rows5.size()>0)
			   hashSet.put("AEPS",""+(rows5.get(0)==null?"0.0":rows5.get(0)));
		 else
	    	 hashSet.put("AEPS","0.0");
	    
		
	   
	}else if(wBean!=null&&wBean.getUsertype()==3){

		 SQLQuery query = session.createSQLQuery("SELECT sum(txndebit)-sum(txncredit) as txnAmt from wallettransaction where date(txndate)=CURDATE() and userid  in (select id from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (4,25)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows1=query.list();
	     if(rows1!=null&&rows1.size()>0)
			   hashSet.put("MoneyTrans",""+(rows1.get(0)==null?"0.0":rows1.get(0)));
	     else
	    	 hashSet.put("MoneyTrans","0.0");
	     
	     
	     query = session.createSQLQuery("SELECT sum(txncredit)-sum(txndebit) as txnAmt from wallettransaction where date(txndate)=CURDATE() and userid in (select id from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (13,92)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows2=query.list();
		 if(rows2!=null&&rows2.size()>0)
			   hashSet.put("Deposit",""+(rows2.get(0)==null?"0.0":rows2.get(0)));
		 else
	    	 hashSet.put("Deposit","0.0");
	    
		 
		 query = session.createSQLQuery("SELECT sum(txndebit)-sum(txncredit) as txnAmt from wallettransaction where date(txndate)=CURDATE() and userid in (select id from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (5,6)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows3=query.list();
		 if(rows3!=null&&rows3.size()>0)
			   hashSet.put("Recharge",""+(rows3.get(0)==null?"0.0":rows3.get(0)));
		
		 else
	    	 hashSet.put("Recharge","0.0");
	    
		 query = session.createSQLQuery("SELECT sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and userid in (select id from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (88)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows4=query.list();
		 if(rows4!=null&&rows4.size()>0)
			   hashSet.put("MATM",""+(rows4.get(0)==null?"0.0":rows4.get(0)));
		 else
	    	 hashSet.put("MATM","0.0");
	    
		 
		 query = session.createSQLQuery("SELECT sum(txncredit) as credit from wallettransaction where date(txndate)=CURDATE() and userid  in (select id from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (80)  order by txncode");
	     query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows5=query.list();
		 if(rows5!=null&&rows5.size()>0)
			   hashSet.put("AEPS",""+(rows5.get(0)==null?"0.0":rows5.get(0)));
		 else
	    	 hashSet.put("AEPS","0.0");
	    
		 
		 query = session.createSQLQuery("SELECT count(distinct(walletid)) as cnt from wallettransaction where date(txndate)=CURDATE() and walletid  in (select walletid from walletmast where distributerid=:userid) and aggreatorid=:aggreatorid and txncode in (80,88,5,4,13,24) ");
	     
		 query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows6=query.list();
	     
         query = session.createSQLQuery("SELECT count(*) as cnt from walletmast where  distributerid=:userid and aggreatorid=:aggreatorid  ");
	     
		 query.setString("userid", emailMobile);
	     query.setString("aggreatorid", aggreatorid);
	     List<Object[]> rows7=query.list();
	     
	     if(rows6!=null&&rows6.size()>0&&rows7!=null&&rows7.size()>0)
			   hashSet.put("AGENT",""+(rows6.get(0)==null && rows7.get(0)==null?"0.0":rows6.get(0)+" / "+rows7.get(0)));
		 else
	    	 hashSet.put("AGENT","0/0"); 
	    
		 
	   }else if(wBean!=null&&wBean.getUsertype()==7){

		   Query query = session.createSQLQuery("select sum(txndebit) from wallettransaction where txndate >=DATE_ADD(LAST_DAY(now() - INTERVAL 2 MONTH), INTERVAL 1 DAY) and txndate<= LAST_DAY(now() - INTERVAL 1 MONTH) and userid in (select id from walletmast where superdistributerid=:userid) and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows1=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txndebit) from wallettransaction where txndate>=DATE_ADD(LAST_DAY(now() - INTERVAL 1 MONTH), INTERVAL 1 DAY) and txndate<=now() and userid in (select id from walletmast where superdistributerid=:userid) and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows2=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txndebit) from wallettransaction where date(txndate)=CURDATE() and userid in (select id from walletmast where superdistributerid=:userid) and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows3=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txncredit) from wallettransaction where date(txndate)=CURDATE() and walletid=:userid and aggreatorid=:aggreatorid and txncode in(1,13,24)");
		   query.setString("userid", wBean.getWalletid());
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows4=query.list();
		   
		   if(rows1!=null&&rows1.size()>0)
			   hashSet.put("lastMonth",""+(rows1.get(0)==null?"0.0":rows1.get(0)));
		   if(rows2!=null&&rows2.size()>0)
			   hashSet.put("Month",""+(rows2.get(0)==null?"0.0":rows2.get(0)));
		   if(rows3!=null&&rows3.size()>0)
			   hashSet.put("Day",""+(rows3.get(0)==null?"0.0":rows3.get(0)));
		   if(rows4!=null&&rows4.size()>0)
			   hashSet.put("Credit",""+(rows4.get(0)==null?"0.0":rows4.get(0)));
		   
	   }
	   else if(wBean!=null&&wBean.getUsertype()==7){

		   Query query = session.createSQLQuery("select sum(txndebit) from wallettransaction where txndate >=DATE_ADD(LAST_DAY(now() - INTERVAL 2 MONTH), INTERVAL 1 DAY) and txndate<= LAST_DAY(now() - INTERVAL 1 MONTH) and userid=:userid and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows1=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txndebit) from wallettransaction where txndate>=DATE_ADD(LAST_DAY(now() - INTERVAL 1 MONTH), INTERVAL 1 DAY) and txndate<=now() and userid=:userid and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows2=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txndebit) from wallettransaction where date(txndate)=CURDATE() and userid=:userid and aggreatorid=:aggreatorid and txncode=4");
		   query.setString("userid", emailMobile);
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows3=query.list();
		   
		   query = session.createSQLQuery("SELECT sum(txncredit) from wallettransaction where date(txndate)=CURDATE() and walletid=:userid and aggreatorid=:aggreatorid and txncode in(13,24)");
		   query.setString("userid", wBean.getWalletid());
		   query.setString("aggreatorid", aggreatorid);
		   List<Object[]> rows4=query.list();
		   
		   if(rows1!=null&&rows1.size()>0)
			   hashSet.put("lastMonth",""+(rows1.get(0)==null?"0.0":rows1.get(0)));
		   if(rows2!=null&&rows2.size()>0)
			   hashSet.put("Month",""+(rows2.get(0)==null?"0.0":rows2.get(0)));
		   if(rows3!=null&&rows3.size()>0)
			   hashSet.put("Day",""+(rows3.get(0)==null?"0.0":rows3.get(0)));
		   if(rows4!=null&&rows4.size()>0)
			   hashSet.put("Credit",""+(rows4.get(0)==null?"0.0":rows4.get(0)));
		   
	   }
	   
	  }catch(Exception e){
		 e.printStackTrace(); 
	  }finally {
		session.close();
	}
	  return hashSet;
		
	}
	
	
	/**
	 * userId-customer /agent id
	 */
	public WalletMastBean showUserProfile(String userId) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|showUserProfile()|"
				+userId);
		//logger.info("Start excution ********************************************* method showUserProfile( userid)"+userId);
		Session session = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		WalletMastBean walletMastBean = null;
		try {
			walletMastBean = (WalletMastBean) session.get(WalletMastBean.class, userId);
			
			if(walletMastBean.getPan()!=null){
			walletMastBean.setPan(EncryptionDecryption.getdecrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}
			if(walletMastBean.getAdhar()!=null){
			walletMastBean.setAdhar(EncryptionDecryption.getdecrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in showUserProfile" + e.getMessage()+" "+e
					+userId);
			//logger.debug("problem in showUserProfile==================" + e.getMessage(), e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		
		
		return walletMastBean;
	}
	
	public String checkUserKycStatus(String userId){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|checkUserKycStatus()|"
				+userId);
		//logger.info("Start excution ********************************************* method showUserProfile( userid)"+userId);
		Session session = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		WalletMastBean walletMastBean = null;
		try {
			//walletMastBean = (WalletMastBean) session.get(WalletMastBean.class, userId);
		    Criteria criteria=session.createCriteria(WalletMastBean.class);
			criteria.add(Restrictions.eq("mobileno",userId));
			List<WalletMastBean> list=criteria.list();
			logger.info("mno-"+userId+"**********getting list -"+list);
			if(list!=null&&list.size()>0){
			walletMastBean=list.get(0);
			if(walletMastBean!=null&&walletMastBean.getKycstatus()!=null&&walletMastBean.getKycstatus().equalsIgnoreCase("1")){
			return "active";
			}
			}
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "||problem in checkUserKycStatus" + e.getMessage()+" "+e
					+userId);
			//logger.debug("problem in showUserProfile==================" + e.getMessage(), e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		
		
		return "inactive";
	}

	public UserWalletConfigBean updateUserWalletConfig(UserWalletConfigBean userWalletConfigBean) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|updateUserWalletConfig()|"
				);
		//logger.info("Start excution ********************************************* method updateUserWalletConfig( userWalletConfigBean)");
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
		try {
			/*if (!(userWalletConfigBean.getId() == "")) {
				session.saveOrUpdate(userWalletConfigBean);
				transaction.commit();
				UserWalletConfigBean userWalletConfigBeanupate = (UserWalletConfigBean) session
						.get(UserWalletConfigBean.class, userWalletConfigBean.getId());
				return userWalletConfigBeanupate;
			} else {
				logger.info("Start excution ********************************************* invalied user id");
				return userWalletConfigBean;
			}*/
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|problem in updateUserWalletConfig" + e.getMessage()+" "+e
					);
			//logger.debug("problem in updateUserProfile==================" + e.getMessage(), e);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			return userWalletConfigBean;
		} finally {
			session.close();
		}
		return userWalletConfigBean;
	}

	public UserWalletConfigBean getUserWalletConfig(String userId) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getUserWalletConfig()|"
				);
		//logger.info("Start excution ********************************************* method getUserWalletConfig( userid)");
		Session session = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		UserWalletConfigBean userWalletConfigBean = null;
		try {
			userWalletConfigBean = (UserWalletConfigBean) session.get(UserWalletConfigBean.class, userId);
		} catch (HibernateException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|problem in getUserWalletConfig" + e.getMessage()+" "+e
					);
			//logger.debug("problem in getUserWalletConfig==================" + e.getMessage(), e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		return userWalletConfigBean;
	}

/*	public LoginResponse login(String hcetoken,String userId, String password,String aggreatorid, String imeiIP,String agent) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|login()|agent" +agent
				);
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)");
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)"+password);
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		Query query = null;
		String newOtp = null;
		WalletMastBean walletMastBean=new WalletMastBean();
		LoginResponse loginResponse=new LoginResponse();
		logger.info("userId ********************************************* " + userId);
		try {
			walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			transaction=session.beginTransaction();
			
			
			if (new EmailValidator().validate(userId)) {
				if (checkEailIdWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent +"|Email register with us"
							);
					//logger.info("Inside *********************************************Email register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE emailid=:userId and password=:password and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|Email id not register with us"
							);
					//logger.info("Inside *********************************************Email id not register with us" + userId);
					loginResponse.setStatusCode("7010");
					return loginResponse;
				}
			} else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", userId))){
				if (checkMobileNoWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "agent" +agent+"|mobile register with us"
							);
					//logger.info("Inside *********************************************mobile register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:userId and password=:password and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|Mobile Number not register with us"
							);
					//logger.info("Inside *********************************************Mobile Number not register with us" + userId);
					loginResponse.setStatusCode("7011");
					return loginResponse;
					
				}
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|login with id"
						);
				//logger.info("Inside *********************************************login with id" + userId);
				query = session.createQuery(" FROM WalletMastBean WHERE id=:userId and password=:password and aggreatorid=:aggreatorid  ");
				
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|query"+query
					);
			//logger.info("Inside *********************************************query" + query);
			query.setString("userId", userId);
			//query.setString("password", HashValue.hashValue(password));
			query.setString("password", CommanUtil.SHAHashing256(password));
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean> list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getApprovalRequired().equals("A")||walletMastBean.getApprovalRequired().equals("N"))){
					loginResponse.setStatusCode("7039");
					return loginResponse;
				}
				
				if(walletMastBean.getUserstatus().equals("S") ||walletMastBean.getUserstatus().equals("B")){
					
					loginResponse.setStatusCode("7050");
					return loginResponse;
				}
				
				
				
			if(walletMastBean.getUserstatus().equalsIgnoreCase("D")){
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|user account login send otp for activate user and its wallet" + "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
						);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
						);
					//logger.info("Inside *********************************************user account login====send otp for activate user and its wallet");
					//logger.info("Inside *********************************************account wallet====send otp for validate mobile on" + walletMastBean.getMobileno());
					newOtp = oTPGeneration.generateOTP();
					//logger.info("Inside *********************************************"+newOtp);
					if (!(newOtp == null)) {
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", walletMastBean.getMobileno()).setParameter("otp", newOtp);
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", newOtp);
						//SHA-256
						Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  walletMastBean.getMobileno()).setParameter("aggreatorid", walletMastBean.getAggreatorid()).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
						insertOtp.executeUpdate();
					}
					transaction.commit();
					if (!(newOtp == null)) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on mobile" + walletMastBean.getMobileno() + "otp"+ newOtp
								);
						//logger.info("Inside ====================otp send on mobile==========" + walletMastBean.getMobileno() + "===otp==="+ newOtp);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", walletMastBean.getMobileno(),commanUtilDao.getsmsTemplet("regOtp",walletMastBean.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}
					
					loginResponse.setStatusCode("1010");
					return loginResponse;
				}else{
					//change to check
					
					Query queryLogin = session.createQuery(	" update WalletMastBean set hcetoken=:hcetoken,loginStatus=1,loginipiemi=:loginipiemi,loginagent=:loginagent WHERE mobileno=:mobileno  and aggreatorid=:aggreatorid");
					queryLogin.setString("hcetoken", hcetoken);
					queryLogin.setString("loginipiemi", imeiIP);
					queryLogin.setString("loginagent", agent);
					queryLogin.setString("mobileno", walletMastBean.getMobileno());
					queryLogin.setString("aggreatorid", walletMastBean.getAggreatorid());
					int result = queryLogin.executeUpdate();
					transaction.commit();
					if (result >= 1) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Login status updated"
								);
						//logger.info("Inside ====================Login status updated......");
					}
					WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
					if(bean!=null){
					loginResponse.setToken(bean.getToken());
					loginResponse.setTokenSecurityKey(bean.getTokenSecurityKey());
					if(walletMastBean.getUsertype() == 2)
					{
						new CommanUtilDaoImpl().deleteTransDtl(walletMastBean.getId());
					}
					}
					loginResponse.setStatusCode("1011");
					return loginResponse;
				}
		
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|incorrect Password provided by user"
						);
				//logger.info("Inside ===================incorrect Password provided by user=======");
			
				loginResponse.setStatusCode("7012");
				return loginResponse;
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
					);
			e.printStackTrace();
			if (transaction != null&&transaction.isActive())
				transaction.rollback();
			e.printStackTrace();
			loginResponse.setStatusCode("7000");
			return loginResponse;
		} finally {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Login status updated -2"
					);
			//logger.info("Inside ==================== Login status updated......2");
			session.close();
		}

	}

	*/
	public LoginResponse login(String userId, String password,String aggreatorid, String imeiIP,String agent) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|login()|agent" +agent
				);
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)");
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)"+password);
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)"+aggreatorid);
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		Query query = null;
		String newOtp = null;
		WalletMastBean walletMastBean=new WalletMastBean();
		LoginResponse loginResponse=new LoginResponse();
		logger.info("userId ********************************************* " + userId);
		try {
			walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			transaction=session.beginTransaction();
			
			/*
			if (new EmailValidator().validate(userId)) {
				if (checkEailIdWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent +"|Email register with us"
							);
					//logger.info("Inside *********************************************Email register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE emailid=:userId and password=:password and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|Email id not register with us"
							);
					//logger.info("Inside *********************************************Email id not register with us" + userId);
					loginResponse.setStatusCode("7010");
					return loginResponse;
				}
			} else
			*/	
				if((Pattern.matches("^([6-9]{1})([0-9]{9})$", userId))){
				if (checkMobileNoWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "agent" +agent+"|mobile register with us"
							);
					//logger.info("Inside *********************************************mobile register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:userId and password=:password and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|Mobile Number not register with us"
							);
					//logger.info("Inside *********************************************Mobile Number not register with us" + userId);
					loginResponse.setStatusCode("7011");
					return loginResponse;
					
				}
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|login with id"
						);
				//logger.info("Inside *********************************************login with id" + userId);
				query = session.createQuery(" FROM WalletMastBean WHERE id=:userId and password=:password and aggreatorid=:aggreatorid  ");
				
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|agent" +agent+"|query"+query
					);
			//logger.info("Inside *********************************************query" + query);
			query.setString("userId", userId);
			//query.setString("password", HashValue.hashValue(password));
			query.setString("password", CommanUtil.SHAHashing256(password));
			query.setString("aggreatorid", aggreatorid);
			
			List<WalletMastBean> list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getApprovalRequired().equals("A")||walletMastBean.getApprovalRequired().equals("N"))){
					loginResponse.setStatusCode("7039");
					return loginResponse;
				}
				
				if(walletMastBean.getUserstatus().equals("S") ||walletMastBean.getUserstatus().equals("B")){
					
					loginResponse.setStatusCode("7050");
					return loginResponse;
				}
				
				
				
			if(walletMastBean.getUserstatus().equalsIgnoreCase("D")){
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|user account login send otp for activate user and its wallet" + "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
						);
				/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
						);*/
					//logger.info("Inside *********************************************user account login====send otp for activate user and its wallet");
					//logger.info("Inside *********************************************account wallet====send otp for validate mobile on" + walletMastBean.getMobileno());
					
				newOtp = oTPGeneration.generateOTP();
				//newOtp="123456";
					
					//logger.info("Inside *********************************************"+newOtp);
					System.out.println("---------------------------------newOtp---------------------->>>>>>>>>>>>>>>>>>>>>>"+newOtp);
					if (!(newOtp == null)) {
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", walletMastBean.getMobileno()).setParameter("otp", newOtp);
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", newOtp);
						//SHA-256
						Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  walletMastBean.getMobileno()).setParameter("aggreatorid", walletMastBean.getAggreatorid()).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
						insertOtp.executeUpdate();
					}
					transaction.commit();
					if (!(newOtp == null)) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on mobile" + walletMastBean.getMobileno() + "otp"+ newOtp
								);
						//logger.info("Inside ====================otp send on mobile==========" + walletMastBean.getMobileno() + "===otp==="+ newOtp);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", walletMastBean.getMobileno(),commanUtilDao.getsmsTemplet("regOtp",walletMastBean.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}
					
					loginResponse.setStatusCode("1010");
					return loginResponse;
				}else{
					//change to check
					
					Query queryLogin = session.createQuery(	" update WalletMastBean set loginStatus=1,loginipiemi=:loginipiemi,loginagent=:loginagent WHERE mobileno=:mobileno  and aggreatorid=:aggreatorid");
					queryLogin.setString("loginipiemi", imeiIP);
					queryLogin.setString("loginagent", agent);
					queryLogin.setString("mobileno", walletMastBean.getMobileno());
					queryLogin.setString("aggreatorid", walletMastBean.getAggreatorid());
					int result = queryLogin.executeUpdate();
					transaction.commit();
					if (result >= 1) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Login status updated"
								);
						//logger.info("Inside ====================Login status updated......");
					}
					WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
					if(bean!=null){
					loginResponse.setToken(bean.getToken());
					loginResponse.setTokenSecurityKey(bean.getTokenSecurityKey());
					if(walletMastBean.getUsertype() == 2)
					{
						new CommanUtilDaoImpl().deleteTransDtl(walletMastBean.getId());
					}
					}
					loginResponse.setStatusCode("1011");
					return loginResponse;
				}
		
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|incorrect Password provided by user"
						);
				//logger.info("Inside ===================incorrect Password provided by user=======");
			
				loginResponse.setStatusCode("7012");
				return loginResponse;
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|account wallet send otp for validate mobile on" + walletMastBean.getMobileno()
					);
			e.printStackTrace();
			if (transaction != null&&transaction.isActive())
				transaction.rollback();
			e.printStackTrace();
			loginResponse.setStatusCode("7000");
			return loginResponse;
		} finally {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Login status updated -2"
					);
			//logger.info("Inside ==================== Login status updated......2");
			session.close();
		}

	}

	public String getPrivateKeyByAggId(String aggId){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "", "", "|getPrivateKeyByAggId()|"
				);
	    //  logger.info("Start excution ********************************************* method getPrivateKeyByAggId");
		  boolean saveImage = false;
		  SessionFactory factory = null;
		  Session session = null;
		  Transaction transaction = null;
		  factory = DBUtil.getSessionFactory();
		  session = factory.openSession();
		
		  try {
			WalletSecurityMast wsMast=(WalletSecurityMast)session.get(WalletSecurityMast.class, aggId);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "", "", "|getPrivateKeyByAggId"
					);
		  // logger.info(" *********************************************getPrivateKeyByAggId*********************************************=");
		   return wsMast.getPrivatekey();
		  } catch (Exception e) {
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "", "", "|problem in Save Profile Pic"+ e.getMessage()+" "+e
						);
			 // logger.debug("problem in Save Profile Pic=============="+ e.getMessage(), e);
		   e.printStackTrace();
		  } finally {
		   session.close();
		  }
		  return "";
		 
	}
	
	public LoginResponse customerValidateOTP(String userid,String aggreatorid, String otp) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|customerValidateOTP()|otp"+otp
				);
		//logger.info("Start excution ********************************************* method customerValidateOTP(userid,otp)"+otp);
		//logger.info("Start excution ********************************************* aggreatorid)"+aggreatorid);
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		String uMobile = null;
		String id;
		LoginResponse loginResponse=new LoginResponse();
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE (emailid=:userid or mobileno=:userid or id=:userid) and aggreatorid=:aggreatorid");
			query.setString("userid", userid);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getMobileno() == null)){
					id=walletMastBean.getId();
					uMobile=walletMastBean.getMobileno();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Mobile No."+uMobile
							);
					//logger.info("Start excution ********************************************* mobile number"+uMobile);
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", uMobile);
					//queryOTP.setString("otp", otp);
					//SHA-256
					queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
					queryOTP.setString("aggreatorid", aggreatorid);
					
					list = queryOTP.list();
					if (list != null && list.size() != 0) {
							flag = new OTPGeneration().validateOTP(otp);
							if(flag){
								Query queryStatus = session.createQuery("update WalletMastBean set  userstatus= 'A',loginStatus=1 where id=:id and userstatus= 'D'");
								queryStatus.setString("id", walletMastBean.getId());
								queryStatus.executeUpdate();
								
								
								
								WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
								
								if(bean==null){
								String token=StringEncryption.generateNewKey();
								String tokenSecurityKey=StringEncryption.generateNewKey();
								WalletSecurityBean wsb=new WalletSecurityBean();
								wsb.setAggreagatorId(aggreatorid);
								wsb.setUserId(walletMastBean.getId());
								wsb.setWalletId(walletMastBean.getWalletid());
								wsb.setToken(token);
								wsb.setTokenSecurityKey(tokenSecurityKey);
								wsb.setUserStatus("A");
								loginResponse.setToken(token);
								loginResponse.setTokenSecurityKey(tokenSecurityKey);
								session.save(wsb);
								
								}
								transaction.commit();
								//WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
								if(bean!=null){
								loginResponse.setToken(bean.getToken());
								loginResponse.setTokenSecurityKey(bean.getTokenSecurityKey());
								}
								
							}
				
					}else{
						flag =false;
					}
					
				}else{
					flag = false;
				}
			}else{
				flag = false;
			}
			
			
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in customerValidateOTP otp" + e.getMessage()+" "+e
					);
			//logger.debug("problem in customerValidateOTP otp==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside customerValidateOTP return flag and end" + flag
				);
		//logger.info("Inside ====================customerValidateOTP return flag=== and end=======" + flag);
		if(flag){
			loginResponse.setStatusCode("1000");
		}else{
			loginResponse.setStatusCode("1001");
		}
		return loginResponse;
	}
	
	
	
	
	
	
	public Boolean validateOTP(String userid,String aggreatorid, String otp) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|validateOTP()|otp"+otp
				);
		//logger.info("Start excution ********************************************* method validateOTP(userid,otp)"+otp);
		//logger.info("Start excution ********************************************* aggreatorid)"+aggreatorid);
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		String uMobile = null;
		String id;
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE (emailid=:userid or mobileno=:userid or id=:userid) and aggreatorid=:aggreatorid");
			query.setString("userid", userid);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getMobileno() == null)){
					id=walletMastBean.getId();
					uMobile=walletMastBean.getMobileno();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "||Mobile"+uMobile
							);
					//logger.info("Start excution ********************************************* mobile number"+uMobile);
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", uMobile);
					//queryOTP.setString("otp", otp);
					//SHA-256
					queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
					queryOTP.setString("aggreatorid", aggreatorid);
					
					list = queryOTP.list();
					if (list != null && list.size() != 0) {
							flag = new OTPGeneration().validateOTP(otp);
							/*if(flag){
								Query queryStatus = session.createQuery("update WalletMastBean set  userstatus= 'A',loginStatus=1 where id=:id and userstatus= 'D'");
								queryStatus.setString("id", walletMastBean.getId());
								queryStatus.executeUpdate();
								transaction.commit();
							}*/
				
					}else{
						flag =false;
					}
					
				}else{
					flag = false;
				}
			}else{
				flag = false;
			}
			
			
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in validate otp" + e.getMessage()+" "+e
					);
			//logger.debug("problem in validate otp==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside validateOTP return flag and end" + flag
				);
		//logger.info("Inside ====================validateOTP return flag=== and end=======" + flag);
		return flag;
	}
	
	
public Boolean validateOTPFirstTime(String userid,String aggreatorid, String otp) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|validateOTP()|otp"+otp
				);
		//logger.info("Start excution ********************************************* method validateOTP(userid,otp)"+otp);
		//logger.info("Start excution ********************************************* aggreatorid)"+aggreatorid);
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		String uMobile = null;
		String id;
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE (emailid=:userid or mobileno=:userid or id=:userid) and aggreatorid=:aggreatorid");
			query.setString("userid", userid);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getMobileno() == null)){
					id=walletMastBean.getId();
					uMobile=walletMastBean.getMobileno();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "||Mobile"+uMobile
							);
					//logger.info("Start excution ********************************************* mobile number"+uMobile);
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", uMobile);
					//queryOTP.setString("otp", otp);
					//SHA-256
					queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
					queryOTP.setString("aggreatorid", aggreatorid);
					
					list = queryOTP.list();
					if (list != null && list.size() != 0) {
							flag = new OTPGeneration().validateOTP(otp);
							/*if(flag){
								Query queryStatus = session.createQuery("update WalletMastBean set  userstatus= 'A',loginStatus=1 where id=:id and userstatus= 'D'");
								queryStatus.setString("id", walletMastBean.getId());
								queryStatus.executeUpdate();
								transaction.commit();
							}*/
							transaction = session.beginTransaction();
						if (walletMastBean != null) {
							Query queryStatus = session.createQuery(
									"update WalletMastBean set  userstatus= 'A' , loginStatus =1 where id=:id and userstatus= 'D'");
							queryStatus.setString("id", walletMastBean.getId());
							queryStatus.executeUpdate();
							WalletSecurityBean bean = (WalletSecurityBean) session.get(WalletSecurityBean.class,
									walletMastBean.getId());
							if (bean == null) {
								String token = StringEncryption.generateNewKey();
								String tokenSecurityKey = StringEncryption.generateNewKey();
								WalletSecurityBean wsb = new WalletSecurityBean();
								wsb.setAggreagatorId(aggreatorid);
								wsb.setUserId(walletMastBean.getId());
								wsb.setWalletId(walletMastBean.getWalletid());
								wsb.setToken(token);
								wsb.setTokenSecurityKey(tokenSecurityKey);
								wsb.setUserStatus("A");
								session.save(wsb);
							}

						}
							transaction.commit();
				
					}else{
						flag =false;
					}
					
				}else{
					flag = false;
				}
			}else{
				flag = false;
			}
			
			
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in validate otp" + e.getMessage()+" "+e
					);
			//logger.debug("problem in validate otp==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside validateOTP return flag and end" + flag
				);
		//logger.info("Inside ====================validateOTP return flag=== and end=======" + flag);
		return flag;
	}
	
	
	
	public Boolean verifyOTP(String userid,String aggreatorid, String otp) {  
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|verifyOTP()"
				);
		//logger.info("Start excution ********************************************* method verifyOTP(userid,otp)");
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		String uMobile = null;
		String id;
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE (emailid=:userid or mobileno=:userid or id=:userid) and aggreatorid=:aggreatorid ");
			query.setString("userid", userid);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getMobileno() == null)){
					id=walletMastBean.getId();
					uMobile=walletMastBean.getMobileno();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Mobile"+uMobile
							);
					
					//logger.info("Start excution ********************************************* mobile number"+uMobile);
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", uMobile);
					queryOTP.setString("aggreatorid", aggreatorid);
					//queryOTP.setString("otp", otp);
					//SHA-256
					queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
					
					list = queryOTP.list();
					if (list != null && list.size() != 0) {
							flag = new OTPGeneration().validateOTP(otp);
						
					}else{
						flag =false;
					}
					
				}else{
					flag = false;
				}
			}else{
				flag = false;
			}
			
			
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in verifyOTP " + e.getMessage()+" "+e
					);
			//logger.debug("problem in verifyOTP ****************************************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|InsideverifyOTP return flag" + flag
				);
		//logger.info("InsideverifyOTP return flag******************************************************" + flag);
		return flag;
	}
	
	
		
	
	public Boolean verifyRefundOTP(String userid,String aggreatorid, String otp) {  
		
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {
			
					
					//logger.info("Start excution ********************************************* mobile number"+uMobile);
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", userid);
					queryOTP.setString("aggreatorid", aggreatorid);
					queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
					
					List list = queryOTP.list();
					if (list != null && list.size() != 0) {
							flag = new OTPGeneration().validateOTP(otp);
						
					}else{
						flag =false;
					}
					
			
			
			
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in verifyOTP " + e.getMessage()+" "+e
					);
			//logger.debug("problem in verifyOTP ****************************************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}
	
	
	public Boolean otpResend(String userid,String aggreatorid ) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|otpResend()|"
				);
		//logger.info("Start excution ********************************************* method otpResend(mobileno)");
		Boolean flag = false;
		boolean mobileFlag = false;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query1 = null;
		try {
			
			if (new EmailValidator().validate(userid)) {
				query1 = session.createQuery(" FROM WalletMastBean WHERE emailid=:emailMobileNo and aggreatorid=:aggreatorid");
				flag = false;
			} else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", userid))){
				query1 = session.createQuery(" FROM WalletMastBean WHERE mobileno=:emailMobileNo and aggreatorid=:aggreatorid ");
				flag = false;
				mobileFlag = true;
			}else{
				query1 = session.createQuery(" FROM WalletMastBean WHERE id=:emailMobileNo and aggreatorid=:aggreatorid ");
				flag = false;
			}
			
			
			query1.setString("emailMobileNo", userid);
			query1.setString("aggreatorid", aggreatorid);
			
			
			System.out.println("_________________________userId______________"+userid);
			System.out.println("_________________________aggreatorid______________"+aggreatorid);

			
			List<WalletMastBean> list = query1.list();
			
			if ((list != null && list.size() != 0) || mobileFlag) {
				String mobileno;
				String name;
			if(list != null && list.size() != 0) {
				WalletMastBean	walletUserBean=list.get(0);
				mobileno=walletUserBean.getMobileno();
				name = walletUserBean.getName();
			}else {
				mobileno = userid;
				name = userid;
			}
			System.out.println("_____________mobileNo_____________"+mobileno);
			
			Query query = session.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileno);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside Delete old otp from otpmast"
					);
			new OTPGeneration();
			walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			String newOtp = new OTPGeneration().generateOTP();
			System.out.println("_______________________________________________________________________________________newOTP____________________"+newOtp);
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|"+CommanUtil.SHAHashing256(newOtp)
						);
					Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  mobileno).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					
					insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside otp send on mobile" +mobileno + "otp"+ newOtp
							);
					smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,commanUtilDao.getsmsTemplet("regOtp",aggreatorid)!=null?commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp):"", "SMS",aggreatorid,"",name,"OTP");
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				flag = true;
			}
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|problem in resend otp" + e.getMessage()+" "+e
					);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "", "|Inside return flag and end" + flag
				);
		return flag;

	}
	
	
	
	
	
	private String matchPassword(String id,String oldpassword, String newpassword) {
		String flag = "9901";
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|matchPassword()|"
				+id +"|"+oldpassword+"|"+newpassword);
		//logger.info("Start excution ********************************************* method matchPassword( agentId,  password,  imeiIP)"+id +"|"+oldpassword+"|"+newpassword);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			
			String currentPass =walletMastBean.getPassword();
			String _oldPassword =walletMastBean.getOldPassword();
			//System.out.println("~~~~~~~~oldPass~~~~~~~~~~~~~~~~~~~~~~~`"+currentPass);
			if(currentPass.equals(CommanUtil.SHAHashing256(oldpassword))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|old password same"
						);
			//logger.info(" ********************************************* old password same*********************************************=========");
			if (currentPass.equals(CommanUtil.SHAHashing256(newpassword))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|old password and new password   same"
						);
			//logger.info(" ********************************************* old password and new password   same*********************************************=========");
			flag = "7702";
			}else{
					
				if(_oldPassword==null ||_oldPassword.isEmpty()){
					walletMastBean.setOldPassword(currentPass);
					session.save(walletMastBean);
					transaction.commit();
					flag = "1000";
				}else{
					String [] __oldPassword=_oldPassword.split(",");
					
					List<String> passList = new LinkedList<String>(Arrays.asList(__oldPassword));
					
					if(passList.contains(CommanUtil.SHAHashing256(newpassword))){
						flag = "7703";
					}else{
						if(passList.size()==4){
							passList.remove(0);
						}
						passList.add(currentPass);
						_oldPassword=StringUtils.join(passList, ',');
						walletMastBean.setOldPassword(_oldPassword);
						session.save(walletMastBean);
						transaction.commit();
						flag = "1000";
					}
						
				}
				}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|old password and new password not  same"
					);
			//logger.info(" ********************************************* old password and new password not same*********************************************=========");
			}else{
				flag = "7013";
			}
		} catch (Exception e) {
			transaction.rollback();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in matchPassword" + e.getMessage()+" "+e
					+id +"|"+oldpassword+"|"+newpassword);
			//logger.debug("problem in matchPassword==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|flag"+flag
				);
		//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + flag);
		return flag;
	}
	
	
	
	
	public String firstTimePasswordChange(String aggId,String userId, String oldpassword,String newpassword){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"",userId, "", "", "|firstTimePasswordChange()|"
			 +"|"+oldpassword+"|"+newpassword);
		//logger.info("Start excution ********************************************* method firstTimePasswordChange(aggId,userId,oldpassword,  password)");
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		String status="7000";
		try {
			if(!new PasswordValidator().validate(newpassword)){
				return "7038";
			}
			
			String id;
			Query query = session.createQuery(" FROM WalletMastBean WHERE (emailid=:userid or mobileno=:userid or id=:userid) and aggreatorid=:aggId");
			query.setString("userid", userId);
			query.setString("aggId", aggId);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				String flag=matchPassword(walletMastBean.getId(),oldpassword, newpassword);
				if(flag.equalsIgnoreCase("1000")){
				query = session.createQuery("update WalletMastBean set password= :password where id= :id and userstatus= 'D'");		
				//query.setString("password", HashValue.hashValue(newpassword));
				query.setString("password", CommanUtil.SHAHashing256(newpassword));
				query.setString("id", walletMastBean.getId());
				int result = query.executeUpdate();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"",userId, "", "", "|Change user Password"
						);
				//logger.info(" ********************************************* Change user Password*********************************************=========");
				if (result == 1) {
					Query queryStatus = session.createQuery("update WalletMastBean set  userstatus= 'A' where id=:id and userstatus= 'D'");
					queryStatus.setString("id", walletMastBean.getId());
					result = queryStatus.executeUpdate();
					
					WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
				
					
					if(bean==null){
					String token=StringEncryption.generateNewKey();
					String tokenSecurityKey=StringEncryption.generateNewKey();
					WalletSecurityBean wsb=new WalletSecurityBean();
					wsb.setAggreagatorId(aggId);
					wsb.setUserId(walletMastBean.getId());
					wsb.setWalletId(walletMastBean.getWalletid());
					wsb.setToken(token);
					wsb.setTokenSecurityKey(tokenSecurityKey);
					wsb.setUserStatus("A");
					session.save(wsb);
					}
					
					transaction.commit();
					status="1000";
				}
				}else{
					status=flag;
				}
			}else{
				status="7001";
			}
		}catch (Exception e) {
			if (transaction != null)
				transaction.commit();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"",userId, "", "", "|problem in firstTimePasswordChange" + e.getMessage()+" "+e
					);
			//logger.debug("problem in firstTimePasswordChange==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return status;		
	}
	
	
	public String changePassword(String id,String oldpassword, String newpassword, String imeiIP){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|changePassword()|Id"+id+"|imeiIP"+imeiIP
				);
		/*logger.info("Start excution ********************************************* method changePassword(userId,  oldpassword,newpassword,  imeiIP)");
		logger.info("Start excution ********************************************* methodid"+id);
		//logger.info("Start excution ********************************************* method oldpassword"+oldpassword);
		//logger.info("Start excution ********************************************* method newpassword"+newpassword);
		logger.info("Start excution ********************************************* method imeiIP"+imeiIP);
*/		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		String status="1001";
		try {
			if(!new PasswordValidator().validate(newpassword)){
				return "7038";
			}
			
			String flag=matchPassword(id, oldpassword,newpassword);
				if(flag.equalsIgnoreCase("1000")){
				Query query = session.createQuery("update WalletMastBean set password= :password where id= :id and userstatus= 'A'");		
				//query.setString("password", HashValue.hashValue(newpassword));
				query.setString("password", CommanUtil.SHAHashing256(newpassword));
				
				query.setString("id", id);
				int result = query.executeUpdate();
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|successfully Change user Password"
						);
				//logger.info(" ********************************************* successfully Change user Password*********************************************=========");
				status="1000";
				}else{
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|matchPassword return false"
							);
					//logger.info("=============matchPassword return false*********************************************");
					status=flag;
				}
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			status="7000";
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|problem in changePassword" + e.getMessage()+" "+e
					);
			//logger.debug("problem in changePassword==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|password change status"+status
				);
		//logger.info("=============password change status*********************************************===" + status);
		return status;		
		
	}
	
	/**
	 * 
	 */
	
	
	public String changePassword(WalletBean walletBean){
		
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|changePassword()|"+ "|imeiIP"+walletBean.getImeiIP()+"|Mobile"+walletBean.getMobileno()
				);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|imeiIP"+walletBean.getImeiIP()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|Mobile"+walletBean.getMobileno()
				);*/
		/*logger.info("Start excution ********************************************* method changePassword(userId,  oldpassword,newpassword,  imeiIP)");
		logger.info("Start excution ********************************************* methodid"+walletBean.getUserId());
		//logger.info("Start excution ********************************************* method oldpassword"+walletBean.getOldpassword());
		//logger.info("Start excution ********************************************* method newpassword"+walletBean.getPassword());
		logger.info("Start excution ********************************************* method imeiIP"+walletBean.getImeiIP());
		logger.info("Start excution ********************************************* method Mobile"+walletBean.getMobileno());
		//logger.info("Start excution ********************************************* method OTP"+walletBean.getOtp());
		logger.info("Start excution ********************************************* method Agg"+walletBean.getAggreatorid());
*/		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		String status="1001";
		try {
			if(!new PasswordValidator().validate(walletBean.getPassword())){
				return "7038";
			}
			if(verifyOTP(walletBean.getMobileno(),walletBean.getAggreatorid(), walletBean.getOtp())){
				
			String flag=matchPassword(walletBean.getUserId(), walletBean.getOldpassword(),walletBean.getOldpassword());
				if(flag.equalsIgnoreCase("1000")){
				Query query = session.createQuery("update WalletMastBean set password= :password where id= :id and userstatus= 'A'");		
				//query.setString("password", HashValue.hashValue(newpassword));
				query.setString("password", CommanUtil.SHAHashing256(walletBean.getOldpassword()));
				
				query.setString("id", walletBean.getUserId());
				int result = query.executeUpdate();
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|successfully Change user Password"
						);
				//logger.info(" ********************************************* successfully Change user Password*********************************************=========");
				status="1000";
				}else{
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|matchPassword return false"
							);
					//logger.info("=============matchPassword return false*********************************************");
					status=flag;
				}
			}else{
				status="7039";
			}
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			status="7000";
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|problem in changePassword" + e.getMessage()+" "+e
					);
			//logger.debug("problem in changePassword==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "", "|password change status"+status
				);
		//logger.info("=============password change status*********************************************===" + status);
		return status;		
		
	}
	
	
	/**
	 * 
	 * @param userId
	 * @param kycid
	 * @param desc
	 * @param kycpic
	 * @return
	 */
	public String saveKyc(String contextPath,String userId,int addkycid,String adddesc,String addkycpic,int idkycid,String iddesc,String idkycpic,String userName,String mobileNo,String emailId,String ipiemi,String agent){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "saveKyc()|Agent"+agent+"|contextPath "+contextPath +"|idkycpic"+addkycpic+"|addkycid"+addkycid+"|adddesc"+adddesc+"|idkycid"+idkycid
				);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|walletid"+contextPath +"|idkycpic"+addkycpic+"|addkycid"+addkycid+"|adddesc"+adddesc+"|idkycid"+idkycid
				);*/
		/*logger.info("Start excution ********************************************* method walletid"+contextPath);
		logger.info("Start excution ********************************************* method walletid"+userId);
		logger.info("Start excution ********************************************* method idkycpic"+addkycpic);
		logger.info("Start excution ********************************************* method addkycid"+addkycid);
		logger.info("Start excution ********************************************* method adddesc"+adddesc);
		logger.info("Start excution ********************************************* method idkycid"+idkycid);*/
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		String status="7000";
		try {
			String kycRefNo=commanUtilDao.getTrxId("KYCREF","OAGG001050");
			WalletKYCBean walletKYCBean=new WalletKYCBean();
			
			String filePath = contextPath.concat("/userkyc");                 
	        System.out.println("Image Location:" + filePath);
	        File dir=new File(filePath,userId);
	        boolean mkdir=dir.mkdir();
	        filePath=filePath.concat("/"+userId);
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|filePath"+filePath
					);
	    	//logger.info("Start excution *********************************************filePath"+filePath); 
	       	String getKycURL=SmsTemplates.getKycURL();
	     	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getKycURL"+getKycURL
					);
	    	//logger.info("Start excution *********************************************getKycURL"+getKycURL); 
			
			walletKYCBean.setRefid(kycRefNo);
			walletKYCBean.setUserId(userId);
			
			walletKYCBean.setAddprofkycid(addkycid);
			walletKYCBean.setAddprofdesc(adddesc);
			byte[] imageDataBytes = Base64.decodeBase64(addkycpic.getBytes());
			InputStream in = new ByteArrayInputStream(imageDataBytes);
			BufferedImage bImageFromConvert = ImageIO.read(in);
			ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+userId+"-address.png"));
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getKycURL"+getKycURL+"|userkyc"+userId+"|"+userId+"-address.png"
					);
			//logger.info("Start excution ************************2323*********************getKycURL"+getKycURL+"userkyc/"+userId+"/"+userId+"-address.png");
			walletKYCBean.setAddprofkycpic(getKycURL+"userkyc/"+userId+"/"+userId+"-address.png");
			walletKYCBean.setIdprofkycid(idkycid);
			walletKYCBean.setIdprofdesc(iddesc);
			imageDataBytes = Base64.decodeBase64(idkycpic.getBytes());
			in = new ByteArrayInputStream(imageDataBytes);
			bImageFromConvert = ImageIO.read(in);
			ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+userId+"-Id.png"));
			walletKYCBean.setIdprofkycpic(getKycURL+"userkyc/"+userId+"/"+userId+"-Id.png");
			
			walletKYCBean.setStatus("Pending");
			walletKYCBean.setUserName(userName);
			walletKYCBean.setMobileNo(mobileNo);
			walletKYCBean.setEmailId(emailId);
			walletKYCBean.setIpiemi(ipiemi);
			walletKYCBean.setAgent(agent);
			
			session.save(walletKYCBean);
			transaction.commit();
			status="1000";
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			status="7001";
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|problem in saveKyc" + e.getMessage()+" "+e
					);
			//logger.debug("problem in saveKyc==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return status;
	}
	
	
	public String saveSenderKyc(UploadSenderKyc senderKyc,String ipiemi,String agent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",senderKyc.getSenderId(), "", "", "saveKyc()|Agent"+agent+"|contextPath|idkycpic|addkycid|adddes");
		
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		String status="7000";
		try {
			String kycRefNo=commanUtilDao.getTrxId("KYCREF",senderKyc.getAggreagatorId());
			senderKyc.setKycId(kycRefNo);
			
/*			String filePath = contextPath.concat("/userkyc");                 
	        System.out.println("Image Location:" + filePath);
	        File dir=new File(filePath,userId);
	        boolean mkdir=dir.mkdir();
	        filePath=filePath.concat("/"+userId);
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|filePath"+filePath
					);
	    	//logger.info("Start excution *********************************************filePath"+filePath); 
	       	String getKycURL=SmsTemplates.getKycURL();
	     	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getKycURL"+getKycURL
					);
	    	//logger.info("Start excution *********************************************getKycURL"+getKycURL); 
			
			walletKYCBean.setRefid(kycRefNo);
			walletKYCBean.setUserId(userId);
			
			walletKYCBean.setAddprofkycid(addkycid);
			walletKYCBean.setAddprofdesc(adddesc);
			byte[] imageDataBytes = Base64.decodeBase64(addkycpic.getBytes());
			InputStream in = new ByteArrayInputStream(imageDataBytes);
			BufferedImage bImageFromConvert = ImageIO.read(in);
			ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+userId+"-address.png"));
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getKycURL"+getKycURL+"|userkyc"+userId+"|"+userId+"-address.png"
					);
			//logger.info("Start excution ************************2323*********************getKycURL"+getKycURL+"userkyc/"+userId+"/"+userId+"-address.png");
			walletKYCBean.setAddprofkycpic(getKycURL+"userkyc/"+userId+"/"+userId+"-address.png");
			walletKYCBean.setIdprofkycid(idkycid);
			walletKYCBean.setIdprofdesc(iddesc);
			imageDataBytes = Base64.decodeBase64(idkycpic.getBytes());
			in = new ByteArrayInputStream(imageDataBytes);
			bImageFromConvert = ImageIO.read(in);
			ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+userId+"-Id.png"));
			walletKYCBean.setIdprofkycpic(getKycURL+"userkyc/"+userId+"/"+userId+"-Id.png");
			
			walletKYCBean.setStatus("Pending");
			walletKYCBean.setUserName(userName);
			walletKYCBean.setMobileNo(mobileNo);
			walletKYCBean.setEmailId(emailId);
			walletKYCBean.setIpiemi(ipiemi);
			walletKYCBean.setAgent(agent);*/
			
			
			MudraSenderWallet senderWallet=(MudraSenderWallet)session.get(MudraSenderWallet.class,senderKyc.getSenderId());
			
			if(senderWallet!=null){
				senderKyc.setSenderName(senderWallet.getFirstName());
				senderKyc.setSenderMobileNo(senderWallet.getMobileNo());
			}
			
			senderKyc.setCheckarStatus("pending");
			senderKyc.setApprovarStatus("N/A");
			senderKyc.setFinalStatus("pending");
			senderKyc.setRequestDate(new Date());
		
			session.save(senderKyc);
			transaction.commit();
			status="1000";
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			status="7001";
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",senderKyc.getSenderId(), "", "", "|problem in saveKyc" + e.getMessage()+" "+e
					);
			//logger.debug("problem in saveKyc==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return status;
	}
	
	
	public String saveSenderKycFromApp(UploadSenderKyc senderKyc,String ipiemi,String agent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",senderKyc.getSenderId(), "", "", "saveKyc()|Agent"+agent+"|contextPath|idkycpic|addkycid|adddes");
		
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		String status="7000";
		try {
			String kycRefNo=commanUtilDao.getTrxId("KYCREF",senderKyc.getAggreagatorId());
			senderKyc.setKycId(kycRefNo);
			String filePath=InfrastructureProperties.getInstance().getProperty("senderkyc");
			 System.out.println("Image Location:" + filePath);
			 filePath=filePath.concat("/SENDERKYC");
			 File dir=new File(filePath,senderKyc.getSenderId());
			 boolean mkdir=dir.mkdir();
		     filePath=filePath.concat("/"+senderKyc.getSenderId());
		     
		     	byte[] imageDataBytes = Base64.decodeBase64(senderKyc.getAddressDesc().getBytes());
				InputStream in = new ByteArrayInputStream(imageDataBytes);
				BufferedImage bImageFromConvert = ImageIO.read(in);
				ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+senderKyc.getSenderId()+"-address.png"));
			    senderKyc.setAddressProofUrl("./SENDERKYC/"+senderKyc.getSenderId()+"/"+senderKyc.getSenderId()+"-address.png");
			    senderKyc.setAddressDesc("");
			    byte[] imageDataBytes1 = Base64.decodeBase64(senderKyc.getIdDesc().getBytes());
				InputStream in1 = new ByteArrayInputStream(imageDataBytes1);
				BufferedImage bImageFromConvert1 = ImageIO.read(in1);
				ImageIO.write(bImageFromConvert1, "png", new File(filePath+"/"+senderKyc.getSenderId()+"-id.png"));
			    senderKyc.setIdProofUrl("./SENDERKYC/"+senderKyc.getSenderId()+"/"+senderKyc.getSenderId()+"-id.png");
				senderKyc.setIdDesc("");			
			
			MudraSenderWallet senderWallet=(MudraSenderWallet)session.get(MudraSenderWallet.class,senderKyc.getSenderId());
			
			if(senderWallet!=null){
				senderKyc.setSenderName(senderWallet.getFirstName());
				senderKyc.setSenderMobileNo(senderWallet.getMobileNo());
			}
			
			senderKyc.setCheckarStatus("pending");
			senderKyc.setApprovarStatus("N/A");
			senderKyc.setFinalStatus("pending");
			senderKyc.setRequestDate(new Date());
		
			session.save(senderKyc);
			transaction.commit();
			status="1000";
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			status="7001";
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",senderKyc.getSenderId(), "", "", "|problem in saveKyc" + e.getMessage()+" "+e
					);
			//logger.debug("problem in saveKyc==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return status;
	}
	
	/**
	 * 
	 * @param walletId
	 * @param profilepic
	 * @return
	 */
	
	public Boolean saveProfilePic(String userId,String profilepic,String ipiemi,String agent) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|Agent"+agent+"|profilepic"+profilepic+"|ipiemi"+ipiemi
				);
	     /* logger.info("Start excution ********************************************* method saveProfilePic(userid,profilePic)"+userId);
	      logger.info("Start excution ********************************************* method saveProfilePic(userid,profilePic)"+profilepic);
	      logger.info("Start excution ********************************************* ipiemi "+ipiemi);
	      logger.info("Start excution ********************************************* agent"+agent);*/
		  boolean saveImage = false;
		  SessionFactory factory = null;
		  Session session = null;
		  Transaction transaction = null;
		  factory = DBUtil.getSessionFactory();
		  session = factory.openSession();
		  transaction = session.beginTransaction();
		  try {
			  if(userId==null ||userId.isEmpty()||profilepic==null||profilepic.isEmpty()){
				  return false;
			  }
			  ProfileImgBean profileImgBean=new ProfileImgBean();
			  profileImgBean.setUserId(userId);
			  profileImgBean.setProfilePic(profilepic.getBytes());
			  profileImgBean.setIpiemi(ipiemi);
			  profileImgBean.setAgent(agent);
			  session.saveOrUpdate(profileImgBean);
		   saveImage = true;
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "Save Profile Pic"
					);
		  // logger.info(" *********************************************Save Profile Pic*********************************************=");
		   transaction.commit();
		  } catch (Exception e) {
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|problem in Save Profile Pic"+ e.getMessage()+" "+e
						);
			 // logger.debug("problem in Save Profile Pic=============="+ e.getMessage(), e);
		   if (transaction != null)
		    transaction.rollback();
		   e.printStackTrace();
		  } finally {
		   session.close();
		  }
		  return saveImage;
		 }
	
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	public String getProfilePic(String userId) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getProfilePic()|"
				);
		//logger.info("Start excution ********************************************* method getProfilePic(walletId)"+userId);
		  SessionFactory factory = null;
		  Session session = null;
		  byte[] profilepic = null;
		  String profilepics = "";
		  factory = DBUtil.getSessionFactory();
		  session = factory.openSession();
		  try {
			  ProfileImgBean dp = (ProfileImgBean) session.get( ProfileImgBean.class, userId);
			  if (dp!=null)
				  profilepic = dp.getProfilePic();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|get Profile Pic");
		  // logger.info(" *********************************************get Profile Pic*********************************************=");
		   if(profilepic==null || profilepic.length==0){
			    dp = (ProfileImgBean) session.get( ProfileImgBean.class, "DEFAULTIMAGE");
			   profilepic = dp.getProfilePic(); 
		   }
		   
		   
		  } catch (Exception e) {
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|problem in get Profile Pic" + e.getMessage()+" "+e
						);
			 // logger.debug("problem in get Profile Pic==============" + e.getMessage(),e);
			  profilepics= "";
		  } finally {
		   session.close();
		  }
		  
		  if(profilepic!=null){
			  profilepics= new String(profilepic);
		  }
		  else{
			  profilepics= "";
		  }
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|get Profile Pic profilepics"
					);
		  //logger.info(" *********************************************get Profile Pic===========profilepics===========");
		  return profilepics;
		 }
	
	
	
	public Boolean userLogOut(String userId) {
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "userLogOut()|logout status updated"
					);
		//logger.info("Inside ====================logout status updated......"+userId);
		Session session = null;
		Transaction transaction = null;
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		TransNotificationDao transNotificationDao=new TransNotificationDaoImpl();
		try {

			Query query = session.createQuery(
					" update WalletMastBean set loginStatus=0 WHERE id=:id ");
			query.setString("id", userId);

			int result = query.executeUpdate();
			if (result >= 1) {
				//if (transNotificationDao.removeDeviceId(userId))
					flag = true;
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|logout status updated"
								);
				//logger.info("Inside ====================logout status updated......");
			}
			transaction.commit();
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|Problem in userLogout"+" "+e
						);
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			flag = false;
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|logout status updated" + flag
				);
		//logger.info("Inside ====================logout status updated......" + flag);
		return flag;
	}
	
	/**
	 * 
	 * @param agentId
	 * @return
	 */
	public Map<String,String> getCustomerByAgentId(String agentId,int userType){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "getCustomerByAgentId()|Agent" + agentId+"|userType"+userType
				);
		//logger.info("Start excution ********************************************* method getCustomerByAgentId(String agentId)"+agentId);
		//logger.info("Start excution ********************************************* method getCustomerByAgentId(String agentId)"+userType);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String,String> customerMap =new HashMap<String, String>();
		try{
			SQLQuery query=null;
			
			if(userType==99){
				 query=session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  userstatus='A' and usertype!=99");
			}else if(userType==5){
				query=session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE agentid=:agentId AND userstatus='A'");
			}else if(userType==4){
				 query=session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE aggreatorid=:agentId AND userstatus='A' UNION SELECT  id,NAME FROM walletmast WHERE distributerid IN (SELECT id FROM walletmast WHERE aggreatorid=:agentId AND userstatus='A') AND userstatus='A' UNION SELECT  id,NAME FROM walletmast WHERE agentid IN (SELECT  id FROM walletmast WHERE distributerid IN (SELECT id FROM walletmast WHERE aggreatorid=:agentId AND userstatus='A') AND userstatus='A')AND userstatus='A'");
			}else if(userType==3){
				 query=session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE distributerid=:agentId AND userstatus='A' UNION SELECT  id,NAME FROM walletmast WHERE agentid IN (SELECT id FROM walletmast WHERE distributerid=:agentId AND userstatus='A') AND userstatus='A'");
			}else if(userType==2){
				query=session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE agentid=:agentId AND userstatus='A'");
			}
			
			if(userType!=99)
			query.setString("agentId", agentId);
			
			List<Object[]> rows=query.list();
			for(Object[] row : rows){
				System.out.println(row[0].toString());
				customerMap.put(row[0].toString(), row[1].toString());
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in getKyc" + e.getMessage()+" "+e
					);
			//logger.debug("problem in getKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(customerMap);
	}
	
	
	/**
	 * 
	 */
	
	public UserWalletConfigBean getWalletConfigByUserId(String id,int txnType,String walletid){
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "", "getWalletConfigByUserId()|id" +id+"|txnType"+txnType 
				);
		/*logger.info("Start excution ********************************************* method getWalletConfigByUserId(userId,txnType,walletid)"+id);
		logger.info("Start excution ********************************************* method getWalletConfigByUserId(userId,txnType,walletid)"+txnType);
		logger.info("Start excution ********************************************* method getWalletConfigByUserId(userId,txnType,walletid)"+walletid);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		UserWalletConfigBean userWalletConfigBean=new UserWalletConfigBean();
		try{
			
			 Criteria critCt = session.createCriteria(UserWalletConfigBean.class);
			 UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
			 userWalletConfigKey.setId(id);
			 userWalletConfigKey.setTxncode(txnType);
			 userWalletConfigKey.setWalletid(walletid);
			 critCt.add(Restrictions.eq("userWalletConfigKey", userWalletConfigKey));
			List <UserWalletConfigBean> list=critCt.list();
			if (list != null && list.size() != 0) {
				userWalletConfigBean=list.get(0);
				userWalletConfigKey=userWalletConfigBean.getUserWalletConfigKey();
				userWalletConfigBean.setId(userWalletConfigKey.getId());
				userWalletConfigBean.setTxnType(userWalletConfigKey.getTxncode());
				userWalletConfigBean.setWalletid(userWalletConfigKey.getWalletid());
				userWalletConfigBean.setWalletid(userWalletConfigKey.getWalletid());
			}else{
				userWalletConfigBean.setId(id);
				userWalletConfigBean.setTxnType(txnType);
				userWalletConfigBean.setWalletid(walletid);
			}
		}catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "", "problem in getWalletConfigByUserId" + e.getMessage()+" "+e 
					);
			//logger.debug("problem in getWalletConfigByUserId==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return userWalletConfigBean;
	}
	
	
	
	/**
	 * 
	 * @param userWalletConfigBean
	 * @return
	 */
	
public UserWalletConfigBean saveUserWalletConfig(UserWalletConfigBean userWalletConfigBean){
	
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|saveUserWalletConfig()|"
			);
		//logger.info("Start excution ********************************************* method saveUserWalletconfig(String agentId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction  transaction=session.beginTransaction();
		String status="1001";
		try{
			
			UserWalletConfigKey userWalletConfigKey=new UserWalletConfigKey();
			userWalletConfigKey.setId(userWalletConfigBean.getId());
			userWalletConfigKey.setWalletid(userWalletConfigBean.getWalletid());
			userWalletConfigKey.setTxncode(userWalletConfigBean.getTxnType());
			userWalletConfigBean.setUserWalletConfigKey(userWalletConfigKey);
			
			session.saveOrUpdate(userWalletConfigBean);
			transaction.commit();
			
			status="1000";
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userWalletConfigBean.getAggreatorid(),userWalletConfigBean.getWalletid(),"", "", "", "|problem in saveUserWalletconfig" + e.getMessage()+" "+e
					);
			//logger.debug("problem in saveUserWalletconfig==================" + e.getMessage(), e);
			status="7000";
		} finally {
			session.close();
		}
		userWalletConfigBean.setStatus(status);
		return userWalletConfigBean;
	}
	
	
	
	public UserWishListBean saveWishList(UserWishListBean userWishListBean ){
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|saveWishList()|getRechargeNumber"+userWishListBean.getRechargeNumber()+"|getRechargeOperator"+userWishListBean.getRechargeOperator()+"|getRechargeCircle"+userWishListBean.getRechargeCircle()+"|getRechargeType"+userWishListBean.getRechargeType());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|getRechargeOperator"+userWishListBean.getRechargeOperator());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|getRechargeCircle"+userWishListBean.getRechargeCircle());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|getRechargeType"+userWishListBean.getRechargeType());
		/*logger.info("Start excution **********************12121212********************** method saveWishList(userWishListBean)"+userWishListBean.getUserId());
		logger.info("Start excution ********************************************* walletId"+userWishListBean.getWalletId());
		logger.info("Start excution ********************************************* getRechargeNumber"+userWishListBean.getRechargeNumber());
		logger.info("Start excution ********************************************* getRechargeOperator"+userWishListBean.getRechargeOperator());
		logger.info("Start excution ********************************************* getRechargeCircle"+userWishListBean.getRechargeCircle());
		logger.info("Start excution ********************************************* getRechargeType"+userWishListBean.getRechargeType());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction  transaction=session.beginTransaction();
		userWishListBean.setStatus("1001");
		try{
			
			Criteria cr=session.createCriteria(UserWishListBean.class);
			cr.add(Restrictions.eq("rechargeNumber", userWishListBean.getRechargeNumber()));
			cr.add(Restrictions.eq("rechargeAmt", userWishListBean.getRechargeAmt()));
			List wishList=cr.list();
			if(wishList==null || wishList.size()==0){
				session.save(userWishListBean);
				transaction.commit();
				userWishListBean.setStatus("1000");	
			}
			
		}catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userWishListBean.getWalletId(),userWishListBean.getUserId(), "", "", "|problem in saveWishList" + e.getMessage()+" "+e);
			
			logger.debug("problem in saveWishList*********************************************" + e.getMessage(), e);
			userWishListBean.setStatus("7000");
		} finally {
			session.close();
		}
		return userWishListBean;
	}
	
	
 public List <UserWishListBean> getWishList(String userId){
	 
	    
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|getWishList()");
		
		//logger.info("Start excution ********************************************* method getWishList(userId)"+userId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List <UserWishListBean> wishList=new ArrayList<UserWishListBean>() ;
		try{
			Criteria cr=session.createCriteria(UserWishListBean.class);
			cr.add(Restrictions.eq("userId", userId));
			cr.addOrder(Order.asc("wishId"));
			wishList=cr.list();
		}catch (Exception e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "|problem in getWishList" + e.getMessage()+" "+e);
				//logger.debug("problem in getWishList*********************************************" + e.getMessage(), e);
				} finally {
				session.close();
			}
			return wishList;
}
	
	
public boolean deleteWishList(int wishId){
	
    
 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|deleteWishList()|wishId"+wishId);
		//logger.info("Start excution ********************************************* method deleteWishList(wishId)"+wishId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Boolean flag=false;
		try{
			Transaction  transaction=session.beginTransaction();
			Object persistentInstance = session.load(UserWishListBean.class, wishId);
			if (persistentInstance != null) {
			    session.delete(persistentInstance);
			    transaction.commit();
			    flag=true;
			}
		}catch (Exception e) {
				flag=false;
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in deleteWishList" + e.getMessage()+" "+e);
				//logger.debug("problem in deleteWishList*********************************************" + e.getMessage(), e);
				} finally {
				session.close();
			}
			return flag;
		}
		
/**
 *  
 */

 public String forgotPassword(String loginId,String aggreatorid) {
	 
	 
	    
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|forgotPassword()|loginId"+loginId);
	// logger.info("Start excution *********************************************************** method forgotPassword( loginId)" + loginId);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String status="1001";
	
	 Query query1 = null;
	 try{
		 if(loginId==null ||loginId.isEmpty()){
			 return "7036";
		 }
		 if (new EmailValidator().validate(loginId)) {
			// logger.info("Inside ********************************************************==");
				query1 = session.createQuery(" FROM WalletMastBean WHERE emailid=:emailMobileNo and aggreatorid=:aggreatorid");
				status="7010";
			} else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", loginId))){
				query1 = session.createQuery(" FROM WalletMastBean WHERE mobileno=:emailMobileNo and aggreatorid=:aggreatorid");
				status="7011";
		}else{
			query1 = session.createQuery(" FROM WalletMastBean WHERE id=:emailMobileNo and aggreatorid=:aggreatorid");
			status="7011";
		}
		 
		 query1.setString("emailMobileNo", loginId);
		 query1.setString("aggreatorid", aggreatorid);
			List<WalletMastBean> list = query1.list();
			if (list != null && list.size() != 0) { 
				WalletMastBean	walletUserBean=list.get(0);
				String mobileno=walletUserBean.getMobileno();
				String email=walletUserBean.getEmailid();
				String aggId=walletUserBean.getAggreatorid();
				if(aggId.equalsIgnoreCase("-1")){
					aggId="0";
				}
				
				
				Query query = session.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
				query.setString("mobile", mobileno);
				query.setString("aggreatorid", aggreatorid);
				query.executeUpdate();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|Delete old otp from otpmast");
				
				//logger.info("Inside ********************************************************Delete old otp from otpmast==");
				new OTPGeneration();
				String newOtp = new OTPGeneration().generateOTP();
				if (!(newOtp == null)) {
					walletConfiguration = commanUtilDao.getWalletConfiguration(aggId);
					 Transaction transaction = session.beginTransaction();
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", newOtp);
						//SHA-256
						Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  mobileno).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
						
						
						
						insertOtp.executeUpdate();
					SmsAndMailUtility smsAndMailUtility;
					transaction.commit();
					if (!(newOtp == null)) {
						if(walletConfiguration.getOtpsendtomail()==1){
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|otp send on mobile :" +mobileno +"and Email"+email+ "otp");
							
							//logger.info("Inside *****************************otp send on mobile :" +mobileno +"******and Email"+email+ "===otp===");
							String mailtemplet = commanUtilDao.getmailTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp);
							smsAndMailUtility = new SmsAndMailUtility(email, mailtemplet, "One Time Password", mobileno,commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replaceAll("<<APPNAME>>", walletConfiguration.getAppname()).replaceAll("<<OTP>>", newOtp), "BOTH",aggreatorid,walletUserBean.getWalletid(),walletUserBean.getName(),"OTP");
							
						}else{
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|otp send on mobile" +mobileno + "otp");
							
							//logger.info("Inside *******************otp send on mobile*****************" +mobileno + "********otp************");
							smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replaceAll("<<APPNAME>>", walletConfiguration.getAppname()).replaceAll("<<OTP>>", newOtp), "SMS",aggreatorid,walletUserBean.getWalletid(),walletUserBean.getName(),"OTP");
						}
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}
			}
				status="1000";
	}
	 }catch (Exception e) {
		if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|problem in forgotPassword"+ e.getMessage()+" "+e);
			
		//logger.debug("problem in forgotPassword*********************************************" + e.getMessage(), e);
		status="7000";
	} finally {
		session.close();
	}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|return Statu"+status);
		
		//logger.info("************************************************return Status************************************"+status);
	 return status;
 }
 

 public String setForgotPassword(String loginId,String password,String aggreatorid){
	 
	    
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","setForgotPassword()|loginId"+loginId+"|password"+password);
		
	 /*logger.info("Start excution *********************************************************** method setForgotPassword( loginId,password)" );
	 logger.info("Start excution *********************************************************** loginId" + loginId);
	 logger.info("Start excution *********************************************************** password" + password);
	 logger.info("Start excution *********************************************************** aggreatorid" + aggreatorid);*/
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String status="1001";
	 Transaction transaction = session.beginTransaction();
	 Query query1 = null;
	 try{
		 
		 if(loginId==null ||loginId.isEmpty()){
			 return "7036";
		 }
		 if(password==null ||password.isEmpty()){
			 return "7037";
		 }
		 
		 if(!new PasswordValidator().validate(password)){
				return "7038";
			}
		 
		if( matchForgotPassword(new TransactionDaoImpl().getIdByUserId(loginId,aggreatorid),password)){
		 
		 
		 if (new EmailValidator().validate(loginId)) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","Delete old otp from otpmast");
				
			 logger.info("Inside ********************************************************Delete old otp from otpmast==");
				query1 = session.createQuery(" update WalletMastBean set password= :password where emailid=:emailMobileNo and aggreatorid=:aggreatorid");
		 } else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", loginId))){
				query1 = session.createQuery(" update WalletMastBean set password= :password  WHERE mobileno=:emailMobileNo and aggreatorid=:aggreatorid");
		}else{
			query1 = session.createQuery(" update WalletMastBean set password= :password  WHERE id=:emailMobileNo and aggreatorid=:aggreatorid");
		}
		//query1.setString("password", HashValue.hashValue(password));
		 query1.setString("password", CommanUtil.SHAHashing256(password));
		
		
		query1.setString("emailMobileNo", loginId);
		query1.setString("aggreatorid", aggreatorid);
		int result = query1.executeUpdate();
		transaction.commit();
		if (result >= 1) {
						status="1000";
					}
		
		}else{
			status="7013";
		}
		
	 }catch (Exception e) {
			if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|problem in setForgotPassword" + e.getMessage()+" "+e);
				
			//logger.debug("problem in setForgotPassword*********************************************" + e.getMessage(), e);
			status="7000";
		} finally {
			session.close();
		}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|return Status"+status);
		
	 //logger.info("************************************************return Status************************************"+status);
	 return status;
 }

 
 
 public String setForgotPassword(String otp,String loginId,String password,String aggreatorid){
	 
	    
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","setForgotPassword()|loginId"+loginId+"|password"+password);
		
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 String status="1001";
	 Transaction transaction = session.beginTransaction();
	 Query query1 = null;
	 Query query2 = null;
	 try{
		 
		 if(loginId==null ||loginId.isEmpty()){
			 return "7036";
		 }
		 if(password==null ||password.isEmpty()){
			 return "7037";
		 }
		 
		 if(!new PasswordValidator().validate(password)){
				return "7038";
			}
		 if(validateOTP(loginId,aggreatorid, otp))
		 {	 
		if( matchForgotPassword(new TransactionDaoImpl().getIdByUserId(loginId,aggreatorid),password)){
		 
		 
		 if (new EmailValidator().validate(loginId)) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","Delete old otp from otpmast");
				
			 logger.info("Inside ********************************************************Delete old otp from otpmast==");
				query1 = session.createQuery(" update WalletMastBean set password= :password where emailid=:emailMobileNo and aggreatorid=:aggreatorid");
				query2 = session.createQuery("from WalletMastBean where emailid=:emailMobileNo and aggreatorid=:aggreatorid");
		 } else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", loginId))){
				query1 = session.createQuery(" update WalletMastBean set password= :password  WHERE mobileno=:emailMobileNo and aggreatorid=:aggreatorid");
				query2 = session.createQuery("from WalletMastBean where mobileno=:emailMobileNo and aggreatorid=:aggreatorid");
		 }else{
			query1 = session.createQuery(" update WalletMastBean set password= :password  WHERE id=:emailMobileNo and aggreatorid=:aggreatorid");
			query2 = session.createQuery("from WalletMastBean where id=:emailMobileNo and aggreatorid=:aggreatorid");

		 }
		//query1.setString("password", HashValue.hashValue(password));
		 query1.setString("password", CommanUtil.SHAHashing256(password));
		
		
		query1.setString("emailMobileNo", loginId);
		query1.setString("aggreatorid", aggreatorid);
		
		query2.setString("emailMobileNo", loginId);
		query2.setString("aggreatorid", aggreatorid);
		int result = query1.executeUpdate();
		List<WalletMastBean> list = query2.list();
		transaction.commit();
		if (result >= 1) {
						status="1000";
						transaction = session.beginTransaction();
						WalletMastBean walletMastBean = null;
						if(list!=null) {
							walletMastBean = list.get(0);
						}
						if(walletMastBean != null) {
						Query queryStatus = session.createQuery("update WalletMastBean set  userstatus= 'A' where id=:id and userstatus= 'D'");
						queryStatus.setString("id", walletMastBean.getId());
						result = queryStatus.executeUpdate();
						
						WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
					
						
						
						if(bean==null){
						String token=StringEncryption.generateNewKey();
						String tokenSecurityKey=StringEncryption.generateNewKey();
						WalletSecurityBean wsb=new WalletSecurityBean();
						wsb.setAggreagatorId(aggreatorid);
						wsb.setUserId(walletMastBean.getId());
						wsb.setWalletId(walletMastBean.getWalletid());
						wsb.setToken(token);
						wsb.setTokenSecurityKey(tokenSecurityKey);
						wsb.setUserStatus("A");
						session.save(wsb);
						}
						
						
						status="1000";
						}
						transaction.commit();
					}
		
		}else{
			status="7013";
		}}
		 else
		 {
			 status="8804";
		 }
		
	 }catch (Exception e) {
			if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|problem in setForgotPassword" + e.getMessage()+" "+e);
				
			//logger.debug("problem in setForgotPassword*********************************************" + e.getMessage(), e);
			status="7000";
		} finally {
			session.close();
		}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|return Status"+status);
		
	 //logger.info("************************************************return Status************************************"+status);
	 return status;
 }

 
 
 private Boolean matchForgotPassword(String id,String newpassword) {
		Boolean flag = false;
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|Id:"+id +"|newpassword "+newpassword);
		//logger.info("Start excution ********************************************* method matchForgotPassword( agentId,  password,  imeiIP)"+id +"|"+newpassword);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			
			String currentPass =walletMastBean.getPassword();
			String _oldPassword =walletMastBean.getOldPassword();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|old password "+currentPass);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|old password same");
			
			//logger.info("~~~~~~~~oldPass~~~~~~~~~~~~~~~~~~~~~~~`"+currentPass);
			//logger.info( "********************************************* old password same*********************************************=========");
			if (currentPass.equals(CommanUtil.SHAHashing256(newpassword))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|old password and new password  sam");
				
				//logger.info(" ********************************************* old password and new password   same*********************************************=========");
				flag = false;
			}else{
					
				if(_oldPassword==null ||_oldPassword.isEmpty()){
					walletMastBean.setOldPassword(currentPass);
					session.save(walletMastBean);
					transaction.commit();
					flag = true;
				}else{
					String [] __oldPassword=_oldPassword.split(",");
				
				List<String> passList = new LinkedList<String>(Arrays.asList(__oldPassword));
				
				if(passList.contains(CommanUtil.SHAHashing256(newpassword))){
					flag = false;
				}else{
					if(passList.size()==4){
						passList.remove(0);
					}
					passList.add(currentPass);
					_oldPassword=StringUtils.join(passList, ',');
					walletMastBean.setOldPassword(_oldPassword);
					session.save(walletMastBean);
					transaction.commit();
					flag = true;
				}
				}
				}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","old password and new password not same");
			
			//logger.info(" ********************************************* old password and new password not same*********************************************=========");
			//}
		} catch (Exception e) {
			transaction.rollback();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|problem in matchForgotPassword" + e.getMessage()+" "+e);
			
			//logger.debug("problem in matchForgotPassword==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|flag:"+flag );
		
		//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + flag);
		return flag;
	}
	
 
 
 
 
 
 
 
	public List<WalletMastBean> getAgentDetail(String aggreatorid){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","getAgentDetail()");
		
		//logger.info("start excution ******************************************************** method getAgentDetail()");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<WalletMastBean> results=null;
		try{
			Query  query=session.createQuery("from WalletMastBean  WHERE  aggreatorid=:aggreatorid and approvalRequired='Y'");
			query.setString("aggreatorid", aggreatorid);
			results=query.list();
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|problem in getAgentDetail"+e.getMessage()+" "+e);
			
	    	//logger.debug("problem in getAgentDetail*****************************************" + e.getMessage(), e);
	    	
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|results size"+results.size());
		
		//logger.info(" **********************return **********************************************************"+results.size());
		return results;
	 }
	
	
public List<WalletMastBean> newBpayAgent(String aggreatorid){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","getAgentDetail()");
	 factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<WalletMastBean> results=null;
		try{
			Query  query=session.createQuery("from WalletMastBean  WHERE  aggreatorid=:aggreatorid and approvalRequired='Y'");
			query.setString("aggreatorid", aggreatorid);
			results=query.list();
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|problem in getAgentDetail"+e.getMessage()+" "+e);
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "","|results size"+results.size());
	 	return results;
	 }
	
	
	
	
	
	
	
	
	public List<DeclinedListBean> getRejectAgentDetail(String aggreatorId,String createdBy){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","getRejectAgentDetail()");
		
		//logger.info("start excution ******************************************************** method getRejectAgentDetail()");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<DeclinedListBean> results=null;
		try{
			
			SQLQuery query = session.createSQLQuery(
					"SELECT a.id AS agentid,mobileno,emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND createdby=:createdBy AND a.id=b.agentid and b.status='OPEN'");
			query.setParameter("createdBy", createdBy);
			query.setString("aggreatorId", aggreatorId);
			query.addScalar("agentid").addScalar("commentid").addScalar("mobileno")
			.addScalar("emailid")
			.addScalar("name")
			.addScalar("comment")
			.setResultTransformer(Transformers.aliasToBean(DeclinedListBean.class));
			results = query.list();
			}catch(Exception e){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","problem in getRejectAgentDetail" + e.getMessage()+" "+e);
				
				//logger.debug("problem in getRejectAgentDetail*****************************************" + e.getMessage(), e);
	    	
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","getRejectAgentDetail" +results.size());
		
		//logger.info(" **********************return ***********getRejectAgentDetail***********************************************"+results.size());
		return results;
	 }
	
	
 
	
	public boolean acceptAgentByAgg(String id,String utrNo){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","acceptAgentByAgg()"+"|Id"+id);
		
		//logger.info("start excution ******************************************************** method acceptAgentByAgg()"+id);
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		boolean flag=false;
		try{
			if(id==null ||id.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","is is null or empty");
				
				//logger.info(" *************************is is null or empty******************************* method acceptAgentByAgg()");
				return false;
			}
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			if(!(walletMastBean.getName()==null)){
				transaction=session.beginTransaction();
				Query queryLogin = session.createQuery(	" update WalletMastBean set approvalRequired=:approvalRequired,utrNo=:utrNo,approveDate=now() WHERE id=:userid");
//				queryLogin.setInteger("kycstatus",1);
				queryLogin.setString("approvalRequired", "C");
				queryLogin.setString("utrNo", utrNo);
				queryLogin.setString("userid", id);
				int result = queryLogin.executeUpdate();
				
				AddBankAccount bank=new AddBankAccount(walletMastBean.getId(),walletMastBean.getWalletid(),walletMastBean.getAggreatorid(),
						walletMastBean.getName(),walletMastBean.getBankName(),walletMastBean.getAccountNumber(),walletMastBean.getIfscCode(),
						walletMastBean.getMobileno(),true);
				session.save(bank);
				
				transaction.commit();
				/*String mailtemplet=null;
				String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid().replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", walletMastBean.getPassword())));
				//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "SMS",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				*/
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","invalid Id");
				
				//logger.info(" *************************invalid Id******************************* method acceptAgentByAgg()");
				return false;
			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in acceptAgentByAgg" + e.getMessage()+" "+e);
			
	    	//logger.debug("problem in acceptAgentByAgg*****************************************" + e.getMessage(), e);
	    	e.printStackTrace();
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return"+flag);
		
		//logger.info(" **********************return **********************************************************"+flag);
		return flag;
	}
	
	
	
  public boolean acceptNewAgentByAgg(String id,String utrNo,String token){
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","utrNo  "+utrNo ,"  token "+token, "", "","acceptNewAgentByAgg()"+"|Id"+id);
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction=session.beginTransaction();
		boolean flag=false;
		try{
			if(id==null ||id.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","is is null or empty");
			 return false;
			}
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			if(!(walletMastBean.getName()==null)){
				
				if(walletMastBean.getUsertype()==2 || walletMastBean.getUsertype()==3 || walletMastBean.getUsertype()==7)
				{
					if(walletMastBean.getUsertype()==2) {
					String disId = walletMastBean.getDistributerid();
					DistributorIdAllot data = (DistributorIdAllot)session.get(DistributorIdAllot.class, disId);
					int all = data.getAllToken();
					int consume = data.getConsumeToken();
					int rem = data.getRemainingToken();
					if(rem>0) {
					data.setUserId(data.getUserId());
					//data.setAllToken(all-1);
					data.setConsumeToken(consume+1);
					data.setRemainingToken(rem-1);
					session.saveOrUpdate(data);
					
					IdCreationCharge idCharge = new IdCreationCharge(walletMastBean.getId(),walletMastBean.getWalletid(),250, walletMastBean.getAggreatorid(),186 ,"0","Distributer "+disId);
					session.save(idCharge);  
					
				    }else
				     {
				     
			    	IdCreationCharge idCharge = new IdCreationCharge(walletMastBean.getId(),walletMastBean.getWalletid(),250, walletMastBean.getAggreatorid(),113 ,"1","SELF");
				    session.save(idCharge);
			    	
				    //	return false;
					 }
					}
				
				
				if(walletMastBean.getUsertype()==2)
				{
					Query queryLogin = session.createQuery(	" update WalletMastBean set approvalRequired=:approvalRequired,utrNo=:utrNo,approveDate=now() WHERE id=:userid");
//					queryLogin.setInteger("kycstatus",1);
					queryLogin.setString("approvalRequired", "C");
					queryLogin.setString("utrNo", utrNo);
					queryLogin.setString("userid", id);
					int result = queryLogin.executeUpdate();
				}else
				{
				String fstpassword = commanUtilDao.createPassword(7);
				Query queryLogin = session.createQuery(	" update WalletMastBean set approvalRequired=:approvalRequired, password=:pass  ,userstatus=:userstatus, utrNo=:utrNo,approveDate=now() WHERE id=:userid");
//				queryLogin.setInteger("kycstatus",1);
				queryLogin.setString("approvalRequired", "A");
				
				queryLogin.setString("pass",CommanUtil.SHAHashing256(fstpassword) );
				
				queryLogin.setString("userstatus", "A");
				queryLogin.setString("utrNo", utrNo);
				queryLogin.setString("userid", id);
				
				int result = queryLogin.executeUpdate();
				 
	               if(result>0) {
					 
	            	   String mailtemplet = commanUtilDao.getmailTemplet("agentOnBoard",walletMastBean.getAggreatorid())
								.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","|mailtemplet "+mailtemplet);	
					 
						String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid())
									.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","|smstemplet "+smstemplet);
	 					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
	                    ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					 }
				 
				}
				AddBankAccount bank=new AddBankAccount(walletMastBean.getId(),walletMastBean.getWalletid(),walletMastBean.getAggreatorid(),
						walletMastBean.getName(),walletMastBean.getBankName(),walletMastBean.getAccountNumber(),walletMastBean.getIfscCode(),
						walletMastBean.getMobileno(),true);
				bank.setComment("Approved");
				bank.setStatusCode(1);
				session.save(bank);
				
				if(walletMastBean.getUsertype()==3 )
				 {
					DistributorIdAllot data = (DistributorIdAllot)session.get(DistributorIdAllot.class, id);
					if(data==null) {
					DistributorIdAllot allotId=new DistributorIdAllot(id,Integer.parseInt(token) ,0,Integer.parseInt(token),walletMastBean.getAggreatorid());
					session.save(allotId);
					}
				 }else if(walletMastBean.getUsertype()==7 ) {
					 DistributorIdAllot data = (DistributorIdAllot)session.get(DistributorIdAllot.class, id);
						if(data==null) {
						DistributorIdAllot allotId=new DistributorIdAllot(id,Integer.parseInt(token) ,0,Integer.parseInt(token),walletMastBean.getAggreatorid());
						session.save(allotId);
						}
					 
				 }
				  
				transaction.commit();
				
				if(walletMastBean.getUsertype()!=2)
				{
					setMpin(id);	
				}
				
				  return true;
				}else
					return false;
				
		 	}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","invalid Id");
				return false;
			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in acceptAgentByAgg" + e.getMessage()+" "+e);
	    	e.printStackTrace();
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return"+flag);
	 	return flag;
	}
	
	
	
	
	
 
	
	
	
	
	public boolean rejectAgentByAgg(String createdBy, String id,String declinedComment){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","rejectAgentByAgg()"+"|Id"+id);
		
		//logger.info("start excution ******************************************************** method acceptAgentByAgg()"+id);
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		boolean flag=false;
		try{
			if(id==null ||id.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","is is null or empty");
				
				//logger.info(" *************************is is null or empty******************************* method acceptAgentByAgg()");
				return false;
			}
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			if(!(walletMastBean.getName()==null)){
				transaction=session.beginTransaction();
				if(null ==  walletMastBean.getCreatedby() || "SELF".equalsIgnoreCase(walletMastBean.getAgentType())) {
					walletMastBean.setCreatedby(createdBy);
//					BPMU001095 for OAGG001050
//					TPMU001096 for OAGG001054
//					PROM001098 for OAGG001057
//					TURM001003 for OAGG001060
					if("OAGG001050".equalsIgnoreCase(walletMastBean.getAggreatorid())){
						walletMastBean.setCreatedby("BPMU001103");
					}else if("OAGG001054".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
						walletMastBean.setCreatedby("TPMU001096");
					}else if("OAGG001057".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
						walletMastBean.setCreatedby("PROM001098");
					}else if("OAGG001060".equalsIgnoreCase(walletMastBean.getAggreatorid())) {
						walletMastBean.setCreatedby("TURM001003");
					}
				}
				walletMastBean.setApprovalRequired("R");
				session.save(walletMastBean);
				AgentDeclinedComment agentDeclinedComment=new AgentDeclinedComment();
				agentDeclinedComment.setAgentId(id);
				agentDeclinedComment.setDeclinedComment(declinedComment);
				agentDeclinedComment.setStatus("OPEN");
				session.save(agentDeclinedComment);
				
				
				
				
				
				
				transaction.commit();
				return true;
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","invalid Id");
				
				//logger.info(" *************************invalid Id******************************* method acceptAgentByAgg()");
				return false;
			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in rejectAgentByAgg" + e.getMessage()+" "+e);
			
	    	//logger.debug("problem in rejectAgentByAgg*****************************************" + e.getMessage(), e);
	    	e.printStackTrace();
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return"+flag);
		
		//logger.info(" **********************return **********************************************************"+flag);
		return flag;
	}
	
	
	public String validateChangeMobile(String userId,String aggreatorid,String mobileNo){
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|validateChangeMobile()|mobileNo "+mobileNo
				);
		/*logger.info("start excution ******************************************************** method validateChangeMobile()"+userId);
		logger.info("start excution ******************************************************** method validateChangeMobile()"+aggreatorid);
		logger.info("start excution ******************************************************** method validateChangeMobile()"+mobileNo);*/
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String status="1001";
		try{
			if( !(Pattern.matches("^([6-9]{1})([0-9]{9})$", mobileNo))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|invalid mobile number" + mobileNo
						);
				//logger.info("Inside *********************************************invalid mobile number" + mobileNo);
				return "7040";
				}
					
			if( aggreatorid==null ||aggreatorid.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Aggreator Id is empty"
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + aggreatorid);
				return "7049";
			}
							
			if (checkMobileNo(mobileNo,aggreatorid)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|mobile register with us"+mobileNo
						);
				//logger.info("Inside *********************************************mobile register with us" + mobileNo);
				return "7002";
			}	
			
			WalletMastBean walletMastBean=showUserProfile(userId) ;
			if( walletMastBean.getMobileno()==null ||walletMastBean.getMobileno().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Invalid User Id"+userId
						);
				//logger.info("Inside *********************************************Invalid User Id" + userId);
				return "7034";
			}
			
			if( walletMastBean.getMobileno().equals(mobileNo)){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|mobileNo "+mobileNo
						);
				logger.info("Inside *********************************************Old and new Mobile No is same" + mobileNo);
				return "7051";
			}
				
			Query query = session.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Delete old otp from otpmast"
					);
			//logger.info("Inside ********************************************************Delete old otp from otpmast==");
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				Transaction transaction = session.beginTransaction();
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  mobileNo).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on mobile" +mobileNo + "otp"+ newOtp
						);
				//logger.info("Inside *******************otp send on mobile*****************" +mobileNo + "********otp************"+ newOtp);
				smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileNo,commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",aggreatorid,walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
		
				status="1000";
		 }catch (Exception e) {
				if (transaction != null)
						transaction.rollback();
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|problem in validateChangeMobile"+e.getMessage()+" "+e
							);
				//logger.debug("problem in validateChangeMobile*********************************************" + e.getMessage(), e);
				status="7000";
			} finally {
				session.close();
			}
		 return status;
	}
	
	
	
	
	public String validateChangeEmail(String userId,String aggreatorid,String email){
		
	    
		
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|validateChangeEmail()|email "+email
				);
		/*logger.info("start excution ******************************************************** method validateChangeMobile()"+userId);
		logger.info("start excution ******************************************************** method validateChangeMobile()"+aggreatorid);
		logger.info("start excution ******************************************************** method validateChangeMobile()"+email);*/
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String status="1001";
		try{
			
					
			if( aggreatorid==null ||aggreatorid.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|"+"Aggreator Id is empty"
						);
				//logger.info("Inside *********************************************Aggreator Id is empty" + aggreatorid);
				return "7049";
			}
							
			if(!(new EmailValidator().validate(email))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|invalid mobile number" + email
						);
				//logger.info("Inside *********************************************invalid mobile number" + email);
				return "7041";
				
			}
			
	
			if (checkEailId(email,aggreatorid)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|"+"Email register with us"+email
						);
				//logger.info("Inside *********************************************Email register with us" + email);
				return "7001";
			}
			
			WalletMastBean walletMastBean=showUserProfile(userId) ;
			if( walletMastBean.getMobileno()==null ||walletMastBean.getMobileno().isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Invalid User Id"+userId
						);
				//logger.info("Inside *********************************************Invalid User Id" + userId);
				return "7034";
			}
			
			if( walletMastBean.getEmailid().equals(email)){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Old and new Email Id is same"+email
						);
				//logger.info("Inside *********************************************Old and new Email Id is same" + email);
				return "7052";
			}
				
			Query query = session.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", email);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Delete old otp from otpmast"
					);
			//logger.info("Inside ********************************************************Delete old otp from otpmast==");
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				Transaction transaction = session.beginTransaction();
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  email).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on email" +email + "otp"+ newOtp
						);
				//logger.info("Inside *******************otp send on email*****************" +email + "********otp************"+ newOtp);
				String eSub="Your one time Email verification code"+newOtp;
				smsAndMailUtility = new SmsAndMailUtility(email, eSub, commanUtilDao.getmailTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp),"" ,"" , "Mail",aggreatorid,walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
		
				status="1000";
		 }catch (Exception e) {
				if (transaction != null)
						transaction.rollback();
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|problem in validateChangeMobile"+e.getMessage()+" "+e
							);
				//logger.debug("problem in validateChangeMobile*********************************************" + e.getMessage(), e);
				status="7000";
			} finally {
				session.close();
			}
		 return status;
	}
	
	
	
	
	
	
	public Boolean changeOtpResend(String userId,String aggreatorid,String mobileNo) {
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|changeOtpResend()|mobileNo "+mobileNo
				);
		//logger.info("Start excution ********************************************* method changeOtpResend(mobileno)"+mobileNo);
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query1 = null;
		try {
			query1 = session.createQuery(" FROM WalletMastBean WHERE id=:userId and aggreatorid=:aggreatorid ");
			query1.setString("userId", userId);
			query1.setString("aggreatorid", aggreatorid);
			List<WalletMastBean> list = query1.list();
			if (list != null && list.size() != 0) {
				WalletMastBean	walletUserBean=list.get(0);
			Query query = session.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Delete old otp from otpmast"
					);
			//logger.info("Inside ==============Delete old otp from otpmast==");
			//logger.info("Inside ********************************************************Delete old otp from otpmast==");
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  mobileNo).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on mobile" +mobileNo + "otp"+ newOtp
						);
				//logger.info("Inside *******************otp send on mobile*****************" +mobileNo + "********otp************"+ newOtp);
				if( (Pattern.matches("^([6-9]{1})([0-9]{9})$", mobileNo))){
				smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileNo,commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",aggreatorid,walletUserBean.getWalletid(),walletUserBean.getName(),"OTP");
				}else{
					String eSub="Your one time Email verification code"+newOtp;
					smsAndMailUtility = new SmsAndMailUtility(mobileNo, eSub, commanUtilDao.getmailTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp),"" ,"" , "Mail",aggreatorid,walletUserBean.getWalletid(),walletUserBean.getName(),"OTP");
					
				}
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				flag = true;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|otp send on mobile" + mobileNo + "otp" + newOtp
						);
				//logger.info("Inside ==============otp send on mobile========" + mobileNo + "===otp===" + newOtp);
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|problem in resend otp" + e.getMessage()+" "+e
					);
			//logger.debug("problem in resend otp==============" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|return flag and end" + flag
				);
		//logger.info("Inside ====================return flag=== and end=======" + flag);
		return flag;

	}
	
	
	public Boolean changeMobileNo(String userId,String aggreatorid,String mobileNo,String otp){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|changeMobileNo()"
				);
		//logger.info("Start excution ********************************************* method changeMobileNo(userid,otp)");
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE id=:userid and aggreatorid=:aggreatorid ");
			query.setString("userid", userId);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|mobileNo "+mobileNo
						);
				//logger.info("Start excution ********************************************* mobile number"+mobileNo);
				Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
				queryOTP.setString("mobile", mobileNo);
				queryOTP.setString("aggreatorid", aggreatorid);
				queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
				
				list = queryOTP.list();
				if (list != null && list.size() != 0) {
					flag = new OTPGeneration().validateOTP(otp);
					if(flag){
						flag =false;
						 query = session.createQuery(
								" delete  FROM WalletMastBean WHERE mobileno=:mobile and userstatus= 'D' and aggreatorid=:aggreatorid");
						query.setString("mobile", mobileNo);
						query.setString("aggreatorid", aggreatorid);
						query.executeUpdate();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|mobileNo "+walletMastBean.getMobileno()
								);
						//logger.info("*****************************************************************************" + walletMastBean.getMobileno());	
					
						Query queryLogin = session.createQuery(	" update WalletMastBean set mobileno=:mobileNo WHERE id=:userid  and aggreatorid=:aggreatorid and userstatus= 'A'");
						queryLogin.setString("mobileNo", mobileNo);
						queryLogin.setString("userid", userId);
						queryLogin.setString("aggreatorid", aggreatorid);
						
						
						int result = queryLogin.executeUpdate();
						transaction.commit();
						
						if (result >= 1) {
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Mobile  updated"
									);
							//logger.info("Inside ===================Mobile  updated......");
							//flag =true;
						}
						}
					}else{
					flag =false;
				}
		
			}else{
				flag = false;
			}
		
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|problem in changeMobileNo "+e.getMessage()+" "+e
					);
			//logger.debug("problem in changeMobileNo ****************************************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|Inside changeMobileNo return flag "+flag
				);
		//logger.info("Inside changeMobileNo return flag******************************************************" + flag);
		
	return flag;	
	}
	
	
	
	
	
	public Boolean changeEmailId(String userId,String aggreatorid,String email,String otp){
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "changeEmailId()|email "+email+"|otp"+otp
				);
		//logger.info("Start excution ********************************************* method changeEmailId(userid,otp)"+email);
		//logger.info("Start excution ****************************otp***************** method changeEmailId(userid,otp)"+otp);
		Boolean flag = false;
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		WalletMastBean walletMastBean=new WalletMastBean();
		
		try {
			Query query = session.createQuery(" FROM WalletMastBean WHERE id=:userid and aggreatorid=:aggreatorid ");
			query.setString("userid", userId);
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean>list = query.list();
			if (list != null && list.size() != 0) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","|email "+email
						);
				//logger.info("Start excution ********************************************* emailId"+email);
				Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:email and otp=:otp and aggreatorid=:aggreatorid");
				queryOTP.setString("email", email);
				queryOTP.setString("aggreatorid", aggreatorid);
				queryOTP.setString("otp", CommanUtil.SHAHashing256(otp));
				
				list = queryOTP.list();
				if (list != null && list.size() != 0) {
					flag = new OTPGeneration().validateOTP(otp);
					if(flag){
						flag =false;
						 query = session.createQuery(
								" delete  FROM WalletMastBean WHERE emailid=:email and userstatus='D' and aggreatorid=:aggreatorid");
						query.setString("email", email);
						query.setString("aggreatorid", aggreatorid);
						query.executeUpdate();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","|MobileNo "+walletMastBean.getMobileno()
								);
						//logger.info("*****************************************************************************" + walletMastBean.getMobileno());	
					
						Query queryLogin = session.createQuery(	" update WalletMastBean set emailid=:email WHERE id=:userid  and aggreatorid=:aggreatorid and userstatus= 'A'");
						queryLogin.setString("email", email);
						queryLogin.setString("userid", userId);
						queryLogin.setString("aggreatorid", aggreatorid);
						
						
						int result = queryLogin.executeUpdate();
						transaction.commit();
						
						if (result >= 1) {
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","|email  updated"
									);
							//logger.info("Inside ===================email  updated......");
							flag =true;
						}
						}
					}else{
					flag =false;
				}
		
			}else{
				flag = false;
			}
		
		}catch (Exception e) {
			flag = false;
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","|problem in changeEmailId "+e.getMessage()+" "+e
					);
			//logger.debug("problem in changeEmailId ****************************************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","|Inside changeEmailId return flag "+flag
				);
		//logger.info("Inside changeEmailId return flag******************************************************" + flag);
		
	return flag;	
	}
	
	
	

public SmartCardBean generateSmartCardReq(SmartCardBean smartCardBean){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|generateSmartCardReq()");
	
	//logger.info("start excution*********************************************************** method generateSmartCardReq(smartCardBean)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	smartCardBean.setStatusCode("1001");
	try{
	Query query = session.createQuery("from SmartCardBean where userId=:userId and aggreatorId=:aggreatorId AND status IN ('Pending','Accepted','Card Dispatch','Card Linked')");
		query.setString("userId", smartCardBean.getUserId());
		query.setString("aggreatorId", smartCardBean.getAggreatorId());
		
		List <SmartCardBean> smartCardList=query.list();
		if (smartCardList != null && smartCardList.size() != 0) {
			if(smartCardList.get(0).getStatus().equals("Pending")){
				smartCardBean.setStatusCode("5001");
			}else{
				smartCardBean.setStatusCode("5002");
			}
		}else{
			smartCardBean.setReqId(commanUtilDao.getTrxId("SMARTCARD",smartCardBean.getAggreatorId()));
			smartCardBean.setStatus("Pending");
			smartCardBean.setPrePaidStatus("Pending");
			session.save(smartCardBean);
			transaction.commit(); 
			smartCardBean.setStatusCode("1000");
		}
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smartCardBean.getAggreatorId(),smartCardBean.getWalletId(),smartCardBean.getUserId(), "", "", "|problem in generateSmartCardReq"+e.getMessage()+" "+e);
		
    	//logger.debug("problem in generateSmartCardReq***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		smartCardBean.setStatusCode("7000");
   	} finally {
   	session.close();
   	}
	return smartCardBean;
}


public List<SmartCardBean> getSmartCardReq(String aggreatorId){
	
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|getSmartCardReq()");
	
	//logger.info("start excution*********************************************************** method getSmartCardReq(aggreatorId)"+aggreatorId);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List<SmartCardBean> smartCardReqList=new ArrayList<SmartCardBean>();
	try{
		Query query = session.createQuery(" FROM  SmartCardBean  WHERE  status='Pending' and aggreatorId=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		smartCardReqList=query.list();
	}catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|problem in getSmartCardReq"+e.getMessage()+" "+e);
			
		//logger.debug("problem in getSmartCardReq***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   	} finally {
   	session.close();
   	}
	
	return smartCardReqList;	
}










public boolean rejecttSmartCard(String reqId){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "rejecttSmartCard()|reqId"+reqId);
	
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		SmartCardBean smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
		smartCardBean.setStatus("Rejected");
		session.saveOrUpdate(smartCardBean);
		transaction.commit();
		flag=true;
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in rejecttSmartCard"+e.getMessage()+" "+e);
		
		//logger.debug("problem in rejecttSmartCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return flag;
}


public SmartCardBean acceptSmartCard(String reqId){
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=null;
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "acceptSmartCard()|reqId"+reqId);
	
	try{
		
		
		MatchMoveBean matchMoveBean=new MachMovePerPaidCard().reqPrePaidCard(reqId);
		
		
		if(matchMoveBean.getId()==null ||matchMoveBean.getId().isEmpty()){
			 smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);	
			smartCardBean.setPrePaidError(matchMoveBean.getError());
			smartCardBean.setStatus("Rejected");
			smartCardBean.setPrePaidStatus("Rejected");
			session.saveOrUpdate(smartCardBean);
			smartCardBean.setStatusCode("1001");
		}else{
			smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			walletMastBean.setPrePaidUserId(matchMoveBean.getId());
			session.update(walletMastBean);
				
		//	smartCardBean.setPrePaidId(matchMoveBean.getId());
			smartCardBean.setPrePaidUserId(matchMoveBean.getId());
			smartCardBean.setStatus("Accepted");
			smartCardBean.setPrePaidStatus("WalletCreate");
			session.saveOrUpdate(smartCardBean);
			session.beginTransaction().commit();
			if(smartCardBean.getCardType().equalsIgnoreCase("PC")){
				smartCardBean.setStatusCode("10000");
			}else{
				  matchMoveBean=new MachMovePerPaidCard().attachedVirtualPrePaidCard(reqId);
				 if(matchMoveBean.getId()==null ||matchMoveBean.getId().isEmpty()){
					 
					 smartCardBean.setPrePaidError(matchMoveBean.getError());
					 smartCardBean.setStatusCode("6789");
					 smartCardBean.setPassowrd("");
					 //smartCardBean.setPrePaidCardNumber("");
					 smartCardBean.setPrePaidCardPin("");
				 }else{
					 Transaction transaction=session.beginTransaction();
					 walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
					 walletMastBean.setPrePaidCardId(matchMoveBean.getId());
					// walletMastBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 walletMastBean.setPrePaidCardStatus("Active");
					 session.update(walletMastBean);
					 smartCardBean.setPrePaidId(matchMoveBean.getId());
					 smartCardBean.setStatus("Card Linked");
					 smartCardBean.setPrePaidStatus("Card Linked");
					 session.saveOrUpdate(smartCardBean);
					 //smartCardBean.setExpDate(matchMoveBean.getDate().getExpiry());
					 //smartCardBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 smartCardBean.setStatusCode("1000");
					 transaction.commit();
					 smartCardBean.setPassowrd("");
					 smartCardBean.setPrePaidCardPin("");
					
				 }
			}
		}
		
		
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in acceptSmartCard"+e.getMessage()+" "+e);
		
		//logger.debug("problem in acceptSmartCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return smartCardBean;
}

public List<SmartCardBean> getDispatchCard(String aggreatorId){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "getDispatchCard()");
	
	//logger.info("start excution*********************************************************** method getDispatchCard(aggreatorId)"+aggreatorId);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List<SmartCardBean> smartCardReqList=new ArrayList<SmartCardBean>();
	try{
		Query query = session.createQuery(" FROM  SmartCardBean  WHERE  (status='Accepted' or status='Card Linked')  and (prePaidStatus='WalletCreate' or prePaidStatus='Card Linked' or prePaidStatus='CradBlocked' )and aggreatorId=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		smartCardReqList=query.list();
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "problem in getDispatchCard"+e.getMessage()+" "+e);
		
    	//logger.debug("problem in getDispatchCard***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   	} finally {
   	session.close();
   	}
	return smartCardReqList;	
}
 
public SmartCardBean dispatchCard(String reqId,String prePaidCardNumber,String prePaidCardPin){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "dispatchCard()|reqid"+reqId+"|prePaidCardNumber"+prePaidCardNumber+"|prePaidCardPin"+prePaidCardPin);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|prePaidCardNumber"+prePaidCardNumber);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|prePaidCardPin"+prePaidCardPin);
	
	
	//logger.info("start excution*********************************************************** method getDispatchCard(aggreatorId)"+prePaidCardNumber);
	//logger.info("start excution*********************************************************** method getDispatchCard(aggreatorId)"+prePaidCardPin);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=new SmartCardBean();
	 smartCardBean.setStatusCode("1001");
	
	try{
		/*if(prePaidCardNumber.isEmpty() ||prePaidCardPin.isEmpty()){
			return smartCardBean;
		}*/
		
	     Transaction transaction=session.beginTransaction();
		 smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);	
		// smartCardBean.setPrePaidCardNumber(prePaidCardNumber);
		 smartCardBean.setPrePaidCardPin(prePaidCardPin);
		 smartCardBean.setStatus("Card Dispatch");
    	 session.saveOrUpdate(smartCardBean);
    	 smartCardBean.setStatusCode("1000");
		 transaction.commit();
		
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in DispatchCard"+e.getMessage()+" "+e);
		
		//logger.debug("problem in DispatchCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return smartCardBean; 
}   



public SmartCardBean getPrePaidCard(String userId){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "","getPrePaidCard()");
	
	//logger.info("start excution*********************************************************** method getPrePaidCard(userId)"+userId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=new SmartCardBean();
	 smartCardBean.setStatusCode("1001");
	try{
		if(userId.isEmpty()){
			return smartCardBean;
		}
		Query query = session.createQuery(" FROM  SmartCardBean  WHERE  status in('Accepted','Pending','Card Dispatch','Card Linked')  and prePaidStatus in ('Pending','WalletCreate','Card Linked') and userId=:userId");
		query.setString("userId", userId);
		List<SmartCardBean> smartCardReqList=query.list();
		if (smartCardReqList != null && smartCardReqList.size() != 0) {
			smartCardBean=smartCardReqList.get(0);
			smartCardBean.setStatusCode("1000");
			
			if(smartCardBean.getStatus().equals("Card Linked")){
				smartCardBean.setPassowrd("");
			    smartCardBean.setPrePaidCardPin("");
			}else{
				 smartCardBean.setPassowrd("");
				// smartCardBean.setPrePaidCardNumber("");
				 smartCardBean.setPrePaidCardPin("");
			}
		}
	
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "","problem in getPrePaidCard"+e.getMessage()+ " "+e);
		
		//logger.debug("problem in getPrePaidCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return smartCardBean;
}

public SmartCardBean resumePrePaidCard(String reqId){
	
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","resumePrePaidCard()|reqId"+reqId);
	
	//logger.info("start excution*********************************************************** method resumePrePaidCard(aggreatorId)"+reqId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
	     Transaction transaction=session.beginTransaction();
		 smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);	
		 MatchMoveBean matchMoveBean=new MachMovePerPaidCard().resumePrePaidCard(reqId);
				 if(matchMoveBean.getBlockedStatus().equalsIgnoreCase("active")){
				 WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
				 walletMastBean.setPrePaidCardId(matchMoveBean.getId());
				// walletMastBean.setPrePaidCardNumber(matchMoveBean.getNumber());
				 walletMastBean.setPrePaidCardStatus("Active");
				 session.update(walletMastBean);
				 smartCardBean.setStatusDesc("Card Resume");
				 smartCardBean.setStatusCode("1000");
				 transaction.commit();
				 smartCardBean.setPassowrd("");
				 smartCardBean.setPrePaidCardPin("");
		 }else{
			 smartCardBean.setPrePaidError(matchMoveBean.getError());
			 smartCardBean.setStatusDesc("Card Resume Failed");
			 smartCardBean.setStatusCode("1001");
			 smartCardBean.setPassowrd("");
			 //smartCardBean.setPrePaidCardNumber("");
			 smartCardBean.setPrePaidCardPin("");
			
		 }
		 
	}catch (Exception e) {
		smartCardBean.setStatusDesc(e.getMessage());
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in resumePrePaidCard"+e.getMessage()+" "+e);
		
		//logger.debug("problem in resumePrePaidCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return in resumePrePaidCard" + smartCardBean.getStatusCode());
	
	//logger.debug("return in resumePrePaidCard=======*******************************===========" + smartCardBean.getStatusCode());
	return smartCardBean; 
	 
}


public SmartCardBean linkCard(String reqId,String prePaidCardNumber,String prePaidCardPin){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","linkCard()|reqId"+reqId+"linkCardNumber"+prePaidCardNumber+"linkCardPin"+prePaidCardPin);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","linkCardNumber"+prePaidCardNumber);
	
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","linkCardPin"+prePaidCardPin);
	
	/*logger.info("start excution*********************************************************** method linkCard(aggreatorId)"+reqId);
	logger.info("start excution*********************************************************** method linkCard(aggreatorId)"+prePaidCardNumber);
	logger.info("start excution*********************************************************** method linkCard(aggreatorId)"+prePaidCardPin);*/
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
	     Transaction transaction=session.beginTransaction();
		 smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);	
		
		 	
		 
		// if(smartCardBean.getPrePaidCardNumber().equals(prePaidCardNumber)){
			 if(smartCardBean.getPrePaidCardPin().equals(prePaidCardPin)){
				 MatchMoveBean matchMoveBean=new MachMovePerPaidCard().attachedPrePaidCard(reqId,prePaidCardNumber,prePaidCardPin);
				 if(matchMoveBean.getId()==null ||matchMoveBean.getId().isEmpty()){
					 smartCardBean.setPrePaidError(matchMoveBean.getError());
					 smartCardBean.setStatusDesc(matchMoveBean.getError());
					 smartCardBean.setStatusCode("1001");
					 smartCardBean.setPassowrd("");
					 //smartCardBean.setPrePaidCardNumber("");
					 smartCardBean.setPrePaidCardPin("");
				 }else{
					 WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
					 walletMastBean.setPrePaidCardId(matchMoveBean.getId());
					// walletMastBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 walletMastBean.setPrePaidCardStatus("Active");
					 session.update(walletMastBean);
					 smartCardBean.setPrePaidId(matchMoveBean.getId());
					 smartCardBean.setStatus("Card Linked");
					 smartCardBean.setPrePaidStatus("Card Linked");
					// smartCardBean.setExpDate(matchMoveBean.getDate().getExpiry());
					 session.saveOrUpdate(smartCardBean);
					// smartCardBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 smartCardBean.setStatusCode("1000");
					 transaction.commit();
					 smartCardBean.setPassowrd("");
					 smartCardBean.setPrePaidCardPin("");
					
				 }
			 }else{
				 smartCardBean.setPassowrd("");
				// smartCardBean.setPrePaidCardNumber("");
				 smartCardBean.setPrePaidCardPin("");
				 smartCardBean.setStatusCode("9901"); 
			 }
			 /*}else{
				 smartCardBean.setPassowrd("");
				 smartCardBean.setPrePaidCardNumber("");
				 smartCardBean.setPrePaidCardPin("");
				 smartCardBean.setStatusCode("9902"); 
		 }*/
	
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		smartCardBean.setStatusDesc(e.getMessage());
		smartCardBean.setPrePaidError(e.getMessage());
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in linkCard" + e.getMessage()+" "+e);
		
		//logger.debug("problem in linkCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return in linkCard"+smartCardBean.getStatusCode());
	
	//logger.debug("return in linkCard=======*******************************===========" + smartCardBean.getStatusCode());
	return smartCardBean;
}




public SmartCardBean linkVirtualCard(String reqId){
	
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","linkVirtualCard()|reqId"+reqId);
	
	//logger.info("start excution*********************************************************** method linkVirtualCard(aggreatorId)"+reqId);
	
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
	     Transaction transaction=session.beginTransaction();
		 smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);	
			 
				 MatchMoveBean matchMoveBean=new MachMovePerPaidCard().attachedVirtualPrePaidCard(reqId);
				 
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|matchMoveBean getId"+matchMoveBean.getId());
					
				 //logger.info("*********************************************************"+matchMoveBean.getId());
				 if(matchMoveBean.getId()==null ||matchMoveBean.getId().isEmpty()){
					 smartCardBean.setPrePaidError(matchMoveBean.getError());
					 smartCardBean.setStatusCode("1001");
					 smartCardBean.setPassowrd("");
					 //smartCardBean.setPrePaidCardNumber("");
					 smartCardBean.setPrePaidCardPin("");
				 }else{
					 WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
					 walletMastBean.setPrePaidCardId(matchMoveBean.getId());
					 //walletMastBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 walletMastBean.setPrePaidCardStatus("Active");
					 session.update(walletMastBean);
					 
					 smartCardBean.setPrePaidId(matchMoveBean.getId());
					 smartCardBean.setStatus("Card Linked");
					 smartCardBean.setPrePaidStatus("Card Linked");
					// smartCardBean.setExpDate(matchMoveBean.getDate().getExpiry());
					 session.saveOrUpdate(smartCardBean);
					// smartCardBean.setPrePaidCardNumber(matchMoveBean.getNumber());
					 smartCardBean.setStatusCode("1000");
					 transaction.commit();
					 smartCardBean.setPassowrd("");
					 smartCardBean.setPrePaidCardPin("");
					
				 }
	
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		smartCardBean.setPrePaidError(e.getMessage());
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|problem in linkVirtualCard"+ e.getMessage()+" "+e);
		
		//logger.debug("problem in linkVirtualCard==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|return in linkVirtualCard"+smartCardBean.getStatusCode());
	
	//logger.debug("return in linkVirtualCard=======*******************************===========" + smartCardBean.getStatusCode());
	return smartCardBean;
}







public SmartCardBean linkCardBlocked(String reqId,String blockedType){
	
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","linkCardBlocked()|reqId"+reqId);
	
	//logger.info("start excution*********************************************************** method linkCardBlocked(reqId)"+reqId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
		MatchMoveBean matchMoveBean=new MachMovePerPaidCard().terminatePrePaidCard(reqId,blockedType);
		if(matchMoveBean.getBlockedStatus()!=null && matchMoveBean.getBlockedStatus().equalsIgnoreCase("blocked")){
			Transaction transaction=session.beginTransaction();
			smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);		
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			 walletMastBean.setPrePaidCardStatus("Blocked");
			 session.update(walletMastBean);
			 //smartCardBean.setStatus("Card Linked");
			// smartCardBean.setPrePaidStatus("Card Linked");
			 if(smartCardBean.getCardType().equalsIgnoreCase("PC")){
			smartCardBean.setStatus("Accepted");
			smartCardBean.setPrePaidStatus("WalletCreate");
			 }else{
				 smartCardBean.setStatus("Accepted");
				 smartCardBean.setPrePaidStatus("CradBlocked"); 
			 }
			
			
			session.saveOrUpdate(smartCardBean);
			smartCardBean.setStatusCode("1000");
			transaction.commit();
			smartCardBean.setPassowrd("");
			smartCardBean.setPrePaidCardPin("");
		}
		
			
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in linkCardBlocked"+e.getMessage()+" "+e);
		
		//logger.debug("problem in linkCardBlocked******************************************************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|return in linkCardBlocked"+smartCardBean.getStatusCode());
	
	//logger.debug("return in linkCardBlocked***********************************************************" + smartCardBean.getStatusCode());
	return smartCardBean;
}



public SmartCardBean suspendedCard(String reqId){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","suspendedCard()|reqId"+reqId);
	
	//logger.info("start excution*********************************************************** method suspendedCardBlocked(reqId)"+reqId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
		MatchMoveBean matchMoveBean=new MachMovePerPaidCard().suspendPrePaidCard(reqId);
		if(matchMoveBean.getBlockedStatus().equalsIgnoreCase("locked")){
			Transaction transaction=session.beginTransaction();
			smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);		
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			 walletMastBean.setPrePaidCardStatus("Suspended");
			 session.update(walletMastBean);
			// smartCardBean.setStatus("Card Linked");
			//smartCardBean.setPrePaidStatus("Card Linked");
			//smartCardBean.setStatus("Accepted");
			//smartCardBean.setPrePaidStatus("WalletCreate");
			session.saveOrUpdate(smartCardBean);
			smartCardBean.setStatusCode("1000");
			transaction.commit();
			smartCardBean.setPassowrd("");
			smartCardBean.setPrePaidCardPin("");
		}
		
			
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in suspendedCardBlocked"+e.getMessage()+" "+e);
		
		//logger.debug("problem in suspendedCardBlocked******************************************************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return in suspendedCardBlocked"+smartCardBean.getStatusCode());
	
	//logger.debug("return in suspendedCardBlocked***********************************************************" + smartCardBean.getStatusCode());
	return smartCardBean;
}






public SmartCardBean deleteUser(String reqId,String state){
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","deleteUser()|reqId"+reqId);
	
	//logger.info("start excution*********************************************************** method deleteUser(reqId)"+reqId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SmartCardBean smartCardBean=new SmartCardBean();
	smartCardBean.setStatusCode("1001"); 
	try{
		MatchMoveBean matchMoveBean=new MachMovePerPaidCard().deleteUser(reqId,state);
		if(matchMoveBean.getState()!=null ){
			Transaction transaction=session.beginTransaction();
			smartCardBean=(SmartCardBean)session.get(SmartCardBean.class, reqId);		
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, smartCardBean.getUserId());
			 walletMastBean.setPrePaidCardStatus(matchMoveBean.getState());
			 session.update(walletMastBean);
			 smartCardBean.setStatus(matchMoveBean.getState());
			 smartCardBean.setPrePaidStatus(matchMoveBean.getState());
			 session.saveOrUpdate(smartCardBean);
			 smartCardBean.setStatusCode("1000");
			 transaction.commit();
			 smartCardBean.setPassowrd("");
			 smartCardBean.setPrePaidCardPin("");
		}
	}catch (Exception e) {
		smartCardBean.setStatusCode("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in deleteUse"+e.getMessage()+" "+e);
		
		//logger.debug("problem in deleteUser******************************************************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return in deleteUser"+smartCardBean.getStatusCode());
	
	//logger.debug("return in deleteUser***********************************************************" + smartCardBean.getStatusCode());
	return smartCardBean;
}








public List<SmartCardBean> getSmartCardList(String aggreatorId){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","getSmartCardList()");
	
	//logger.info("start excution*********************************************************** method getSmartCardList(aggreatorId)"+aggreatorId);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List<SmartCardBean> smartCardReqList=new ArrayList<SmartCardBean>();
	try{
		Query query = session.createQuery(" FROM  SmartCardBean  WHERE  prepaidstatus in ('WalletCreate','Card Linked') and aggreatorId=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		smartCardReqList=query.list();
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","problem in getSmartCardReq"+e.getMessage()+" "+e);
		
    	//logger.debug("problem in getSmartCardReq***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   	} finally {
   	session.close();
   	}
	
	return smartCardReqList;	
}







/*public  SmartCardBean terminatePrePaidCard(String reqId ,String type){
	logger.info("start excution*********************************************************** method terminatePrePaidCard(reqId)"+reqId);
	logger.info("start excution*********************************************************** method terminatePrePaidCard(type)"+type);
	
	

}*/




public String authorizedAggreator(String aggreatorId){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","authorizedAggreator()");
	
	//logger.info("start excution*********************************************************** method AuthorizedAggreator(aggreatorId)"+aggreatorId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();	
	String status="1001";
	List<WalletConfiguration> walletConfigurationlist=new ArrayList<WalletConfiguration>();
	List<MailConfigMast> MailConfigMastlist=new ArrayList<MailConfigMast>();
	List<SMSConfigMast> SMSConfigMastlist=new ArrayList<SMSConfigMast>();
	
	try{
	
		if(aggreatorId==null||aggreatorId.isEmpty()){
			return "6000";
		}
		
		Query query=session.createQuery("from WalletConfiguration where aggreatorid=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		walletConfigurationlist=query.list();
		if (walletConfigurationlist == null || walletConfigurationlist.size() == 0) {
			return "6001";
		}
	
		query=session.createQuery("from MailConfigMast where aggreatorid=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		MailConfigMastlist=query.list();
		if (MailConfigMastlist == null || MailConfigMastlist.size() == 0) {
			return "6002";
		}
		
		query=session.createQuery("from SMSConfigMast where aggreatorid=:aggreatorId");
		query.setString("aggreatorId", aggreatorId);
		SMSConfigMastlist=query.list();
		if (SMSConfigMastlist == null || SMSConfigMastlist.size() == 0) {
			return "6003";
		}
		
		Transaction transaction=session.beginTransaction();
		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, aggreatorId);
		String fstpassword = commanUtilDao.createPassword(7);
		walletMastBean.setPassword(CommanUtil.SHAHashing256(fstpassword));
		walletMastBean.setUserstatus("D");
		session.update(walletMastBean);
		transaction.commit();
		String url=walletConfigurationlist.get(0).getProtocol()+"://"+walletConfigurationlist.get(0).getDomainName();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","walletid"+commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
		
		//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid()));
		String mailtemplet = commanUtilDao.getmailTemplet("welcome",walletMastBean.getAggreatorid().replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword));
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","walletid"+mailtemplet);
		
		//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
		String smstemplet = commanUtilDao.getsmsTemplet("welcome",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
		//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
		SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"NOTP");
//		Thread t = new Thread(smsAndMailUtility);
//		t.start();
		ThreadUtil.getThreadPool().execute(smsAndMailUtility);
		return "1000";
		
	}catch (Exception e) {
		status="7000";
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","problem in AuthorizedAggreator"+e.getMessage()+" "+e);
		
		//logger.debug("problem in AuthorizedAggreator******************************************************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","return in AuthorizedAggreator"+status);
	
	//logger.debug("return in AuthorizedAggreator***********************************************************" + status);
	return status;
	}


public List <WalletMastBean> getUnAuthorizedAggreator(){
	
    
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUnAuthorizedAggreator()");
	
	
	logger.info("start excution*********************************************************** method getUnAuthorizedAggreator()");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List <WalletMastBean> UnAuthorizedAggreatorList=new ArrayList<WalletMastBean>();
	try{
		Criteria cr=session.createCriteria(WalletMastBean.class);
		cr.add(Restrictions.eq("userstatus", "N"));
		UnAuthorizedAggreatorList=cr.list();
	}catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in getUnAuthorizedAggreator"+e.getMessage()+" "+e);
			
    	//logger.debug("problem in getUnAuthorizedAggreator***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		} finally {
   	session.close();
   	}
	return UnAuthorizedAggreatorList;
}


public WalletMastBean editAgent(WalletMastBean walletMastBean) {
	
    
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|editAgent()|Id  "+walletMastBean.getId());
	
	//logger.info("Start excution ********************************************* method editAgent( walletMastBean)--getUserstatus"+walletMastBean.getId());
	Session session = null;
	Transaction transaction = null;
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	transaction = session.beginTransaction();
	try {
		if (walletMastBean.getId() !=null ) {
			WalletMastBean walletMastBeanOld=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());
			walletMastBeanOld.setAgentType(walletMastBean.getAgentType());
			walletMastBeanOld.setName(walletMastBean.getName());
			walletMastBeanOld.setEmailid(walletMastBean.getEmailid());
			walletMastBeanOld.setMobileno(walletMastBean.getMobileno());
			walletMastBeanOld.setDob(walletMastBean.getDob());
			walletMastBeanOld.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBeanOld.setAddressProofType(walletMastBean.getAddressProofType());
			walletMastBeanOld.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBeanOld.setAddress1(walletMastBean.getAddress1());
			walletMastBeanOld.setAddress2(walletMastBean.getAddress2());
			walletMastBeanOld.setCity(walletMastBean.getCity());
			walletMastBeanOld.setState(walletMastBean.getState());
			walletMastBeanOld.setPin(walletMastBean.getPin());
			walletMastBeanOld.setShopName(walletMastBean.getShopName());
			walletMastBeanOld.setShopAddress1(walletMastBean.getShopAddress1());
			walletMastBeanOld.setShopAddress2(walletMastBean.getShopAddress2());
			walletMastBeanOld.setShopCity(walletMastBean.getShopCity());
			walletMastBeanOld.setShopState(walletMastBean.getShopState());
			walletMastBeanOld.setShopPin(walletMastBean.getShopPin());
			walletMastBeanOld.setDistributerid(walletMastBean.getDistributerid());
			walletMastBeanOld.setPaymentMode(walletMastBean.getPaymentMode());
			walletMastBeanOld.setBankName(walletMastBean.getBankName());
			walletMastBeanOld.setApplicationFee(walletMastBean.getApplicationFee());
			walletMastBeanOld.setPaymentDate(walletMastBean.getPaymentDate());
			//walletMastBeanOld.setApprovalRequired("Y");
			walletMastBeanOld.setApprovalRequired("C");
			
			if(walletMastBean.getIdLoc()!=null )
				  walletMastBeanOld.setIdLoc(walletMastBean.getIdLoc());
				
			if(walletMastBean.getAddressLoc()!=null )
			  walletMastBeanOld.setAddressLoc(walletMastBean.getAddressLoc());
		
			if(walletMastBean.getPassbookLoc()!=null  )
			  walletMastBeanOld.setPassbookLoc(walletMastBean.getPassbookLoc());
			
			if(walletMastBean.getPaymentLoc()!=null )
			  walletMastBeanOld.setPaymentLoc(walletMastBean.getPaymentLoc());
			
			
			
			session.update(walletMastBeanOld);
			
			Query query = session.createQuery(
					"update AgentDeclinedComment set status='CLOSED' where agentId=:id and status='OPEN'");
			query.setString("id", walletMastBean.getId());
			query.executeUpdate();
			
			transaction.commit();
			walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());
			
			walletMastBean.setStatusCode("1000");
			return walletMastBean;
		} else {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "| invalied user id");
			
			//logger.info("Start excution ********************************************* invalied user id");
			return walletMastBean;
		}
	} catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in updateUserProfile" + e.getMessage()+" "+e);
		
		//logger.debug("problem in updateUserProfile==================" + e.getMessage(), e);
		if (transaction != null)
			transaction.rollback();
		e.printStackTrace();
		walletMastBean.setStatusCode("7001");
		return walletMastBean;
	} finally {
		session.close();
	}
}



public WalletMastBean editAgentPending(WalletMastBean walletMastBean) {
	
    
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|editAgent()|Id  "+walletMastBean.getId());
	
	//logger.info("Start excution ********************************************* method editAgent( walletMastBean)--getUserstatus"+walletMastBean.getId());
	Session session = null;
	Transaction transaction = null;
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	transaction = session.beginTransaction();
	try {
		if (walletMastBean.getId() !=null ) {
			WalletMastBean walletMastBeanOld=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());
			walletMastBeanOld.setAgentType(walletMastBean.getAgentType());
			walletMastBeanOld.setName(walletMastBean.getName());
			walletMastBeanOld.setEmailid(walletMastBean.getEmailid());
			walletMastBeanOld.setMobileno(walletMastBean.getMobileno());
			walletMastBeanOld.setDob(walletMastBean.getDob());
			walletMastBeanOld.setPan(EncryptionDecryption.getEncrypted(walletMastBean.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBeanOld.setAddressProofType(walletMastBean.getAddressProofType());
			walletMastBeanOld.setAdhar(EncryptionDecryption.getEncrypted(walletMastBean.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			walletMastBeanOld.setAddress1(walletMastBean.getAddress1());
			walletMastBeanOld.setAddress2(walletMastBean.getAddress2());
			walletMastBeanOld.setCity(walletMastBean.getCity());
			walletMastBeanOld.setState(walletMastBean.getState());
			walletMastBeanOld.setPin(walletMastBean.getPin());
			walletMastBeanOld.setShopName(walletMastBean.getShopName());
			walletMastBeanOld.setShopAddress1(walletMastBean.getShopAddress1());
			walletMastBeanOld.setShopAddress2(walletMastBean.getShopAddress2());
			walletMastBeanOld.setShopCity(walletMastBean.getShopCity());
			walletMastBeanOld.setShopState(walletMastBean.getShopState());
			walletMastBeanOld.setShopPin(walletMastBean.getShopPin());
			walletMastBeanOld.setDistributerid(walletMastBean.getDistributerid());
			walletMastBeanOld.setPaymentMode(walletMastBean.getPaymentMode());
			walletMastBeanOld.setBankName(walletMastBean.getBankName());
			walletMastBeanOld.setApplicationFee(walletMastBean.getApplicationFee());
			walletMastBeanOld.setPaymentDate(walletMastBean.getPaymentDate());
			walletMastBeanOld.setApprovalRequired("Y");
			//walletMastBeanOld.setApprovalRequired("C");
			
			if(walletMastBean.getIdLoc()!=null )
				  walletMastBeanOld.setIdLoc(walletMastBean.getIdLoc());
				
			if(walletMastBean.getAddressLoc()!=null )
			  walletMastBeanOld.setAddressLoc(walletMastBean.getAddressLoc());
		
			if(walletMastBean.getPassbookLoc()!=null  )
			  walletMastBeanOld.setPassbookLoc(walletMastBean.getPassbookLoc());
			
			if(walletMastBean.getPaymentLoc()!=null )
			  walletMastBeanOld.setPaymentLoc(walletMastBean.getPaymentLoc());
			 
			session.update(walletMastBeanOld);
			
			Query query = session.createQuery(
					"update AgentDeclinedComment set status='CLOSED' where agentId=:id and status='OPEN'");
			query.setString("id", walletMastBean.getId());
			query.executeUpdate();
			
			transaction.commit();
			walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getId());
			
			walletMastBean.setStatusCode("1000");
			return walletMastBean;
		} else {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "| invalied user id");
			 return walletMastBean;
		}
	} catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in updateUserProfile" + e.getMessage()+" "+e);
		if (transaction != null)
			transaction.rollback();
		e.printStackTrace();
		walletMastBean.setStatusCode("7001");
		return walletMastBean;
	} finally {
		session.close();
	}
}


public List<WalletMastBean> getAgentList(WalletMastBean walletMastBean) {
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentList()|Id  "+walletMastBean.getId());
	
	// logger.info("start excution ===================== method agentList(userId)"+walletMastBean.getId());
	 factory=DBUtil.getSessionFactory();
	 Session session = factory.openSession();
	  List<WalletMastBean> results=new ArrayList<WalletMastBean>();
	 Criteria cr = session.createCriteria(WalletMastBean.class);
	 try{
		 
		 if(walletMastBean.getUsertype()==7&&walletMastBean.getCreatedby()!=null){
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			 StringBuilder serchQuery=new StringBuilder();
			 serchQuery.append(" from WalletMastBean where createdby in(:id) and usertype in (2,3) ");
			
			List<WalletMastBean> wList=session.createQuery("select id from WalletMastBean where superdistributerid=:sd").setParameter("sd",walletMastBean.getId()).list();
			 serchQuery.append(" order by creationDate DESC");
	        System.out.println("serchQuery~~~~"+ serchQuery);
			 Query query=session.createQuery(serchQuery.toString());
			 query.setParameterList("id", wList);
			 results=query.list(); 
		 }
		 
		 else if(!(walletMastBean.getId()=="")){
	
		 
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		 StringBuilder serchQuery=new StringBuilder();
		 
		WalletMastBean agentMast = (WalletMastBean) session.get(WalletMastBean.class, walletMastBean.getId());
		  if(agentMast!=null & (agentMast.getUsertype()==6 || agentMast.getUsertype()==4) )
		     serchQuery.append(" from WalletMastBean where usertype in (2,3,7,8,9) and aggreatorid=:aggreatorid ");		
			else
		     serchQuery.append(" from WalletMastBean where createdby=:id and usertype in (2,3,7) and aggreatorid=:aggreatorid ");
		     
		 if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
			 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
		 }else{
			 serchQuery.append(" and DATE(creationDate)=NOW()");
		 }
		 
		 serchQuery.append(" order by creationDate DESC");
       
		 System.out.println("serchQuery~~~~"+ serchQuery);
		 
		 Query query=session.createQuery(serchQuery.toString());
		 if(agentMast.getUsertype()==6 || agentMast.getUsertype()==4)
		 {}else {
		 query.setString("id", walletMastBean.getId());
		 }
		 query.setString("aggreatorid", agentMast.getAggreatorid());
		 
		 results=query.list();
	 }	 
		 
	 }catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "problem in agentList" + e.getMessage()+" "+e);
			
			//logger.debug("problem in agentList==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "method agentList Result "+results.size());
		
	 //logger.info("start excution ===================== method agentList Result "+results.size());
	 return results;
	
	
	
	
}



public List<WalletMastBean> getApprovedAgentList(WalletMastBean walletMastBean) {
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getApprovedAgentList()|Id  "+walletMastBean.getId());
	
	// logger.info("start excution ===================== method getApprovedAgentList(userId)"+walletMastBean.getId());
	 factory=DBUtil.getSessionFactory();
	 Session session = factory.openSession();
	  List<WalletMastBean> results=new ArrayList<WalletMastBean>();
	 try{
	 if(!(walletMastBean.getId()=="")){
	
		 
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		 StringBuilder serchQuery=new StringBuilder();
//		 approvalRequired='A' and
		 serchQuery.append(" from WalletMastBean where aggreatorid=:id and usertype in (2,3,7) ");
		 if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
			 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
		 }else{
			 //serchQuery.append(" and DATE(creationDate) >= DATE_SUB(CURDATE(),INTERVAL 1 DAY)");
			 
			 serchQuery.append(" and DATE(creationDate)=CURDATE() ");
		 }
		 serchQuery.append(" order by creationDate DESC");
       System.out.println("~~~~"+ serchQuery);
		 Query query=session.createQuery(serchQuery.toString());
		 query.setString("id", walletMastBean.getAggreatorid());
		 results=query.list();
	 }	 
		 
	 }catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "problem in getApprovedAgentList"+e.getMessage()+" "+e);
			
			logger.debug("problem in getApprovedAgentList==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getApprovedAgentList Result"+results.size());
		
	// logger.info("start excution ===================== method getApprovedAgentList Result "+results.size());
	 return results;

}




public List<DeclinedListBean> getRejectAgentList(WalletMastBean walletMastBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getRejectAgentList()|Id  "+walletMastBean.getId());
	
	//logger.info("start excution ******************************************************** method getRejectAgentDetail()");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	 List<DeclinedListBean> results=null;
	try{
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		 StringBuilder serchQuery=new StringBuilder();
		 serchQuery.append("SELECT a.id AS agentid, createdby,soName,usertype,   mobileno,emailid,name,comment,b.id AS commentid, declineddate FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId   AND a.id=b.agentid and b.status='OPEN'" );
		 if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
			 serchQuery.append(" and DATE(declineddate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
		 }
		 SQLQuery query = session.createSQLQuery(serchQuery.toString());
		query.setString("aggreatorId", walletMastBean.getAggreatorid());
		query.addScalar("agentid").addScalar("commentid").addScalar("mobileno")
		
		.addScalar("createdby")
		.addScalar("soName")
		.addScalar("usertype")
		.addScalar("declineddate")
		
		.addScalar("emailid")
		.addScalar("name")
		.addScalar("comment")
		.setResultTransformer(Transformers.aliasToBean(DeclinedListBean.class));
		results = query.list();
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "problem in getRejectAgentList"+e.getMessage()+" "+e);
    	//logger.debug("problem in getRejectAgentList*****************************************" + e.getMessage(), e);
    	
   		  e.printStackTrace();
   			 transaction.rollback();
   	} finally {
   				session.close();
   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "getRejectAgentLis"+results.size());
	//logger.info(" **********************return ***********getRejectAgentList***********************************************"+results.size());
	return results;
 }




public CMERespBean setUpdateMpin(String mobileNo,String mpin,String aggreatorid){
	
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|setUpdateMpin()|MobileNo " + mobileNo +"|mpin"+mpin);
	
	/* logger.info("Start excution *********************************************************** method setUpdateMpin( loginId,password)" );
	 logger.info("Start excution *********************************************************** loginId" + mobileNo);
	 logger.info("Start excution *********************************************************** password" + mpin);
	 logger.info("Start excution *********************************************************** aggreatorid" + aggreatorid);*/
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 CMERespBean cMERespBean=new CMERespBean();
	 cMERespBean.setStatusCode("0");
	 cMERespBean.setStatusMsg("FAILED");
	// String status="1001";
	 Transaction transaction = session.beginTransaction();
	 Query query1 = null;
	 try{
		 
		 if(mobileNo==null ||mobileNo.isEmpty() ||!(Pattern.matches("^([6-9]{1})([0-9]{9})$", mobileNo))){
			 cMERespBean.setStatusCode("0");
			 cMERespBean.setStatusMsg("Invalid Mobile Number");
			 return cMERespBean;
		 }
		 
		 if(mpin==null ||mpin.isEmpty()||!(Pattern.matches("^([0-9]{4})$", mpin))){
			 cMERespBean.setStatusCode("0");
			 cMERespBean.setStatusMsg("Invalid MPIN.");
			 return cMERespBean;
		 }
		
		query1 = session.createQuery(" update WalletMastBean set mpin= :mpin  WHERE mobileno=:mobileNo and aggreatorid=:aggreatorid");
		query1.setString("mpin", CommanUtil.SHAHashing256(mpin));
		query1.setString("mobileNo", mobileNo);
		query1.setString("aggreatorid", aggreatorid);
		int result = query1.executeUpdate();
		transaction.commit();
		if (result >= 1) {
			cMERespBean.setStatusCode("1000");
			 cMERespBean.setStatusMsg("Update MPIN Successfully.");
					}
	 }catch (Exception e) {
			if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|problem in setUpdateMpin"+ e.getMessage()+" "+e);
				
			//logger.debug("problem in setUpdateMpin*********************************************" + e.getMessage(), e);
			 cMERespBean.setStatusCode("0");
			 cMERespBean.setStatusMsg("FAILED");
		} finally {
			session.close();
		}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|setUpdateMpin**return Status"+cMERespBean.getStatusMsg());
		
	// logger.info("**********************************************setUpdateMpin**return Status************************************"+cMERespBean.getStatusMsg());
	 return cMERespBean;
}


public String getMpin(String mobileNo,String aggreatorid){
	String mpin="";
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|getMpin()|MobileNo " + mobileNo );
	
	 /*logger.info("Start excution *********************************************************** method getMpin( loginId,aggreatorid)" );
	 logger.info("Start excution *********************************************************** loginId" + mobileNo);
	 logger.info("Start excution *********************************************************** aggreatorid" + aggreatorid);*/
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 try {
			Query query = session.createQuery("select mpin from WalletMastBean where mobileno= :mobileNo  and  aggreatorid=:aggreatorid");
			query.setString("mobileNo", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				mpin=	(String) list.get(0);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|mpin"+mpin);
				
				//logger.info("==============Wallet Id=============================="+mpin);
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checking id fail from getMpin"+e.getMessage()+" "+e);
			
			logger.debug("checking id fail from getMpin********************************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
	return mpin;
}

public String changeMpin(String mobileNo,String aggreatorid,String newmpin){
	String mpinStatus="";
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|changeMpin()|MobileNo " + mobileNo );
	
	/* logger.info("Start excution *********************************************************** method changeMpin( loginId,aggreatorid)" );
	 logger.info("Start excution *********************************************************** loginId" + mobileNo);
	 logger.info("Start excution *********************************************************** aggreatorid" + aggreatorid);*/
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 try {
		 	transaction=session.beginTransaction();
			Query query = session.createQuery("update WalletMastBean set mpin=:mpin where mobileno= :mobileNo  and  aggreatorid=:aggreatorid");
			query.setString("mobileNo", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			query.setString("mpin",newmpin);
			int result = query.executeUpdate();
			transaction.commit();
			if (result>0){
				mpinStatus="1000";
			}else{
				mpinStatus="1001";
			}
		} catch (Exception e) {
			mpinStatus="7000";
			transaction.rollback();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|checking id fail from changeMpin" + e.getMessage()+" "+e );
			
			//logger.debug("checking id fail from changeMpin********************************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
	return mpinStatus;
}


public void setMpin(String id){
	String mpin="";
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|setMpin()|Id " + id );
	
	 //logger.info("Start excution *********************************************setMpin************** id" + id);
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 transaction=session.beginTransaction();
	 try {
		 Query query = session.createQuery("select mpin from WalletMastBean where id=:id");
			query.setString("id", id);
			List list = query.list();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|list size"+list.size() );
			
			logger.info("==============list.size()============================="+list.size());
		/*	if (list != null && list.size() != 0) {
				mpin=	(String) list.get(0);
				logger.info("==============Wallet Id=============================="+mpin);
			}else{*/
				
			mpin = commanUtilDao.creatempin();
			
			WalletMastBean walletMastObj=(WalletMastBean)session.get(WalletMastBean.class, id);	
			walletMastObj.setMpin(CommanUtil.SHAHashing256(mpin));
			session.update(walletMastObj);
			transaction.commit();
			
			String smstemplet = commanUtilDao.getsmsTemplet("CMEMPIN",walletMastObj.getAggreatorid());

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|mailtemplet"+smstemplet);
			
			//logger.info("*************************************mailtemplet********************************" + smstemplet);
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", walletMastObj.getMobileno(), smstemplet.replaceAll("<<MPIN>>", mpin).replaceAll("<<USERID>>", id), "SMS",walletMastObj.getAggreatorid(),walletMastObj.getWalletid(),walletMastObj.getName(),"OTPT");
//			Thread t = new Thread(smsAndMailUtility);
//			t.start();
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);	
			//}
		 
	 } catch (Exception e) {
		 e.printStackTrace();
			transaction.rollback();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|checking id fail from setMpin"+e.getMessage()+" "+e);
			
			//logger.debug("checking id fail from setMpin********************************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
	
	
}




public String blockUnblockUser(UserBlockedBean userBlockedBean){
	
    
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "blockUnblockUser()|Agentid"+userBlockedBean.getAgentId()+"|blockStatus"+userBlockedBean.getStatus());
	
	/*logger.info("Start excution ********************************************* method blockUnblockUser(userId,  blockStatus)");
	logger.info("Start excution **************************id******************* methodid"+userBlockedBean.getAgentId());
	logger.info("Start excution **************************blockStatus******************* method imeiIP"+userBlockedBean.getStatus());*/
	Session session = null;
	Transaction transaction = null;
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	transaction = session.beginTransaction();
	String status="1001";
	try {
			Query query = session.createQuery("update WalletMastBean set userstatus= :userstatus where id= :id");		
			if(userBlockedBean.getStatus().equalsIgnoreCase("Block")){
				query.setString("userstatus", "B");
				status="Un-Block";
			}else{
				query.setString("userstatus", "A");
				status="Block";
			}
			
			query.setString("id", userBlockedBean.getAgentId());
			int result = query.executeUpdate();
			
			session.save(userBlockedBean);
			transaction.commit();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|successfully blockUnblockUser");
			
			//logger.info(" ********************************************* successfully blockUnblockUser*********************************************=========");
			
			
	}catch (Exception e) {
		if (transaction != null)
			transaction.rollback();
		e.printStackTrace();
		status="7000";
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "problem in blockUnblockUser"+e.getMessage()+" "+e);
		
		//logger.debug("problem in blockUnblockUser==============" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "blockUnblockUser status"+status);
	
	//logger.info("=============blockUnblockUser status*********************************************===" + status);
	return status;		
	
}











private static synchronized Long generateLongId() {
	   long timestamp = System.currentTimeMillis();
	   if (lastTimestamp == timestamp) {
	       sequence = (sequence + 1) % sequenceMax;
	       if (sequence == 0) {
	           timestamp = tilNextMillis(lastTimestamp);
	       }
	   } else {
	       sequence = 0;
	   }
	   lastTimestamp = timestamp;
	   Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
	   return id;
	}

	private static long tilNextMillis(long lastTimestamp) {
	   long timestamp = System.currentTimeMillis();
	   while (timestamp <= lastTimestamp) {
	       timestamp = System.currentTimeMillis();
	   }
	   return timestamp;
	}

	public String saveWalletApiRequest(String aggreatorId ,String request , String serverName , String apiName)
	{
		
	    
		
	    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|RequestValue"+request+"|Server Name "+serverName);
		
		//logger.info(serverName+"**Start Execuion ************saveWalletApiRequest****RequestValue***"+request);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String requestId = "";
		try
		{
			transaction = session.beginTransaction();
			WalletApiRequestLog walletApiRequestLog = new WalletApiRequestLog();
			
			walletApiRequestLog.setRequestId(Long.toString(generateLongId())+commanUtilDao.getTrxId("WalletAPILog",aggreatorId));
			walletApiRequestLog.setAggregatorId(aggreatorId);
			walletApiRequestLog.setRequestValue(request);
			walletApiRequestLog.setApiName(apiName);
			session.save(walletApiRequestLog);
			transaction.commit();
			requestId=walletApiRequestLog.getRequestId();
			logger.info(serverName+"***Saved data ***********saveWalletApiRequest****RequestId***"+requestId);		
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|RequestValue"+request+"|Server Name "+serverName+"problem in saveWalletApiRequest"+ex.getMessage()+" "+ex);
			
			//logger.info(serverName+"****problem in saveWalletApiRequest *****e.message()****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return requestId;
		
	}

	public WalletMastBean getUserByWalletId(String walletId ,String serverName)
	
	{
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", "|getUserByWalletId()|serverName " + serverName );
		
		logger.info(serverName+"****Start Execution****getUserByWalletId******getUserByWalletId******walletId******"+walletId);
		List<WalletMastBean> walletMastBeanList = null;
		WalletMastBean walletMastBean = new WalletMastBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try
		{
			walletMastBeanList = session.createCriteria(WalletMastBean.class).add(Restrictions.eq("walletid", walletId)).add(Restrictions.in("userstatus", Arrays.asList("A","S"))).list();
			 if(walletMastBeanList.size() < 1)
			 {
				 walletMastBean=null;
			 }
			 else
			 {
				 walletMastBean =walletMastBeanList.get(0);
			 }
			 return walletMastBean;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", "|Problem in getUserByWalletId"+ex.getMessage()+" "+ex);
			
			logger.info(serverName+"*****Problem in getUserByWalletId*******e.message****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
		return walletMastBean;
	}

	public WalletMastBean getUserByMobileNo(String mobileNo,String aggreatorId ,String serverName)
	
	{
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|getUserByMobileNo()|MobileNo " + mobileNo +"|ServerName"+serverName);
		
		//logger.info(serverName+"****Start Execution****getUserByWalletId******getUserByMobileNo******mobileNo******"+mobileNo+"*****AggreatorId***"+aggreatorId);
		List<WalletMastBean> walletMastBeanList = null;
		WalletMastBean walletMastBean = new WalletMastBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try
		{
			walletMastBeanList = session.createCriteria(WalletMastBean.class).add(Restrictions.or(Restrictions.eq("mobileno", mobileNo),Restrictions.or(Restrictions.eq("emailid", mobileNo),Restrictions.eq("id", mobileNo)))).add(Restrictions.eq("aggreatorid", aggreatorId)).add(Restrictions.in("userstatus", Arrays.asList("A","S"))).list();
			 if(walletMastBeanList.size() < 1)
			 {
				 walletMastBean=null;	
			 }
			 else
			 {
				 walletMastBean =walletMastBeanList.get(0);
			 }
			 return walletMastBean;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|Problem in getUserByWalletId"+ex.getMessage()+" "+ex);
			
			//logger.info(serverName+"*****Problem in getUserByWalletId*******e.message****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
		return walletMastBean;
	}
	
	public WalletMastBean getUserByMobileNoWithoutAgg(String mobileNo,String serverName)
	{

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "getUserByMobileNoWithoutAgg()|mobileNo "+mobileNo);
		
		//logger.info(serverName+"****Start Execution****getUserByWalletId******getUserByMobileNo******mobileNo******"+mobileNo);
		List<WalletMastBean> walletMastBeanList = null;
		WalletMastBean walletMastBean = new WalletMastBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try
		{
			walletMastBeanList = session.createCriteria(WalletMastBean.class).add(Restrictions.or(Restrictions.eq("mobileno", mobileNo),Restrictions.or(Restrictions.eq("emailid", mobileNo),Restrictions.eq("id", mobileNo)))).add(Restrictions.in("userstatus", Arrays.asList("A","S"))).list();
			 if(walletMastBeanList.size() < 1)
			 {
				 walletMastBean=null;	
			 }
			 else
			 {
				 walletMastBean =walletMastBeanList.get(0);
			 }
			 return walletMastBean;
		}
		catch(Exception ex)
		
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "Problem in getUserByWalletId "+ex.getMessage()+" "+ex);
			
			//logger.info(serverName+"*****Problem in getUserByWalletId*******e.message****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
		return walletMastBean;
	}
	
	public WalletMastBean updateUserAsActive(String mobileNo,String aggreatorId ,String serverName)
	{
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|updateUserAsActive()|MobileNo " + mobileNo +"|serverName"+serverName);
		
		//logger.info(serverName+"****Start Execution****getUserByWalletId******updateUserAsActive******mobileNo******"+mobileNo+"*****AggreatorId***"+aggreatorId);
		List<WalletMastBean> walletMastBeanList = null;
		WalletMastBean walletMastBean = new WalletMastBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try
		{
			walletMastBeanList = session.createCriteria(WalletMastBean.class).add(Restrictions.eq("mobileno", mobileNo)).add(Restrictions.eq("aggreatorid", aggreatorId)).add(Restrictions.eq("userstatus","D")).list();
			 if(walletMastBeanList.size() < 1 )
			 {
				 walletMastBean=null;	
			 }
			 else
			 {
				 walletMastBean =walletMastBeanList.get(0);
				 walletMastBean.setUserstatus("A");
				 session.save(walletMastBean);
				 transaction.commit();
			 }
			 return walletMastBean;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|Problem in updateUserAsActive"+ex.getMessage()+" "+ex);
			
			//logger.info(serverName+"*****Problem in updateUserAsActive*******e.message****"+ex.getMessage());
			ex.printStackTrace();
			walletMastBean=null;	
		}
		finally {
			session.close();
		}
		return walletMastBean;
	}
	public int verifyMobileNoAggId(String mobileNo ,String aggreatorId,String serverName)
	{
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|verifyMobileNoAggId()|MobileNo " + mobileNo +"|serverName"+serverName);
		
		//logger.info(serverName+"*********start execution of method verifyMobileNoAggId ******mobileNo:-"+mobileNo+"*****aggreatorId:-"+aggreatorId);
		int count = -1;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try
		{
			Query query = session.createSQLQuery("SELECT COUNT(1) FROM walletmast WHERE mobileno='"+mobileNo+"' AND userstatus='A' AND aggreatorid='"+aggreatorId+"' ");
			count =(Integer)(query.uniqueResult());
			return count;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", "|problem in method verifyMobileNoAggId"+ex.getMessage()+" "+ex);
			
			//logger.info(serverName+"*********problem in method verifyMobileNoAggId ******e.message*****"+ex.getMessage());
			ex.printStackTrace();
			return count;
		}finally {
			session.close();
		}
		
	}


	
	public List<WalletMastBean> getAgentDetailForApprove (String aggreatorid){
		
	    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|getAgentDetailForApprove()");
		
		//logger.info("start excution ******************************************************** method getAgentDetailForApprove");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<WalletMastBean> results=null;
		try{
			Query query=null;
			if(aggreatorid!=null&&aggreatorid.equalsIgnoreCase("All")){
				query=session.createQuery("from WalletMastBean  WHERE approvalRequired='C'");
			}else{
				query=session.createQuery("from WalletMastBean  WHERE  aggreatorid=:aggreatorid and approvalRequired='C'");
				query.setString("aggreatorid", aggreatorid);
			}
			
			results=query.list();
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|problem in getAgentDetailForApprove"+e.getMessage()+" "+e);
			
	    	//logger.debug("problem in getAgentDetailForApprove*****************************************" + e.getMessage(), e);
	    	
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", "|getAgentDetailForApprove"+results.size());
		
		//logger.info(" **********************getAgentDetailForApprove **********************************************************"+results.size());
		return results;
	 }
	
	public List<MoneyRequestBean> wLCashApprover(WalletMastBean walletMastBean){
		
	    
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|wLCashApprover()");
		
		
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		 
		//logger.info("start excution ******************************************************** method wLCashApprover");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<MoneyRequestBean> results=null;
		try{
//			Query  query=session.createQuery("from MoneyRequestBean WHERE requestStatus=0");
			
			
//			Criteria criteria=session.createCriteria(MoneyRequestBean.class);
			 StringBuilder rRevenuequery=new StringBuilder("select id,bank,txnType,amount,utrNo,aggreatorid,walletId,senderName,ipImei,userAgent,requestStatus,DATE_FORMAT(requestDate, '%d/%m/%Y %T') AS requestDate,remark from dmtmoneyrequest");
			 if(walletMastBean.getStDate()==null ||walletMastBean.getStDate().isEmpty() ||walletMastBean.getEndDate()==null ||walletMastBean.getEndDate().isEmpty()){
				 rRevenuequery.append(" where date(requestDate)=CURDATE()");
				 //criteria.add(Restrictions.eq("requestDate",new Date()));
			 }else{
				 rRevenuequery.append(" WHERE DATE(requestDate) BETWEEN STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				/// criteria.add(Restrictions.between("requestDate",formatter.parse(walletMastBean.getStDate()),formatter.parse(walletMastBean.getEndDate())));
			 }

			 if(walletMastBean.getAggreatorid()!=null&&!walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
				 rRevenuequery.append(" and aggreatorid='"+walletMastBean.getAggreatorid()+"'");
				// criteria.add(Restrictions.eq("aggId",walletMastBean.getAggreatorid()));
			 
//			results=criteria.list();
			 SQLQuery  query=session.createSQLQuery(rRevenuequery.toString());
			 query.setResultTransformer(Transformers.aliasToBean(MoneyRequestBean.class));
			 results=query.list();
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|problem in wLCashApprove"+e.getMessage()+" "+e);
				
	    	//logger.debug("problem in wLCashApprover*****************************************" + e.getMessage(), e);
	    	
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|wLCashApprover"+results.size());
			
		//logger.info(" **********************wLCashApprover **********************************************************"+results.size());
		
		
		return results;
	 }
	
	
	public String updateCashRequestStatus(WalletMastBean walletBean){
		
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|updateCashRequestStatus()");
		 
		//logger.info("start excution ******************************************************** method getAgentDetailForApprove");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=session.beginTransaction();
		String status="";
		try{
		SQLQuery query=session.createSQLQuery("update dmtmoneyrequest set requestStatus=:status,remark=:remark where id=:id");
			query.setParameter("id",walletBean.getId());
			if(walletBean.getUserstatus()!=null){
				query.setParameter("status",walletBean.getUserstatus());
			}
			if(walletBean.getRemark()!=null){
				query.setParameter("remark",walletBean.getRemark());
			}else{
				query.setParameter("remark","-");
			}
			int i=query.executeUpdate();
			transaction.commit();
			if(i>0){
				status="1000";
			}
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|problem in updateCashRequestStatus"+e.getMessage()+" "+e);
				
	    	//logger.debug("problem in updateCashRequestStatus*****************************************" + e.getMessage(), e);
	    	status="7000";
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", "|updateCashRequestStatus"+status);
			
		//logger.info(" **********************updateCashRequestStatus **********************************************************"+status);
		return status;
	
	}
	
	
	
	
	
	
	public boolean approvedAgentByAgg(String id,String utrNo){
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|approvedAgentByAgg()|Id "+id);
		 
		//logger.info("start excution ******************************************************** method acceptAgentByAgg()"+id);
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		boolean flag=false;
		try{
			if(id==null ||id.isEmpty()){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|is is null or empty");
				 
				//logger.info(" *************************is is null or empty******************************* method acceptAgentByAgg()");
				return false;
			}
			
			String fstpassword = commanUtilDao.createPassword(7);
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			if(!(walletMastBean.getName()==null)){
				transaction=session.beginTransaction();
				Query queryLogin = session.createQuery(	" update WalletMastBean set kycstatus=:kycstatus,approvalRequired=:approvalRequired,utrNo=:utrNo,approveDate=now(),password=:password,bank=:bank,isimps=:isimps,wallet=:wallet WHERE id=:userid");
				queryLogin.setString("approvalRequired", "A");
				queryLogin.setString("utrNo", utrNo);
				queryLogin.setString("password", CommanUtil.SHAHashing256(fstpassword));
				queryLogin.setString("userid", id);
				queryLogin.setString("isimps","1");
				queryLogin.setString("bank", "1");
				queryLogin.setString("wallet","1");
				queryLogin.setInteger("kycstatus",1);
				int result = queryLogin.executeUpdate();
				
				
				transaction.commit();
				/*
				String mailtemplet = commanUtilDao.getmailTemplet(" 	",walletMastBean.getAggreatorid())
					.replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|mailtemplet"+mailtemplet);
				 
				//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + mailtemplet);
				String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId().replaceAll("<<PASSWORD>>", fstpassword));
						 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|smstemplet"+smstemplet);
						 
						//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "BOTH",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
						
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			*/
				String mailtemplet=null;
				String smstemplet = commanUtilDao.getsmsTemplet("agentOnBoard",walletMastBean.getAggreatorid()).replaceAll("<<USERID>>", walletMastBean.getId()).replaceAll("<<PASSWORD>>", fstpassword);
				//public SmsAndMailUtility(String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId)
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailtemplet,"Welcome", walletMastBean.getMobileno(), smstemplet, "SMS",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
				/*
				 * Thread t = new Thread(smsAndMailUtility); t.start();
				 */
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				
				
				return true;
			}else{
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|invalid Id");
				 
				//logger.info(" *************************invalid Id******************************* method acceptAgentByAgg()");
				return false;
			}
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|problem in acceptAgentByAgg"+e.getMessage()+" "+e);
			 
			//logger.debug("problem in acceptAgentByAgg*****************************************" + e.getMessage(), e);
	    	e.printStackTrace();
	    	if(transaction!=null&&transaction.isActive())
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "|return"+flag);
		 
		//logger.info(" **********************return **********************************************************"+flag);
		return flag;
	}
	
	public LoginResponse login(String userId,String aggreatorid, String imeiIP,String agent) {
		
	    
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "|login()|Agent "+agent);
		 
		/*logger.info("Start excution ********************************************* method login( emailMobileNo, imeiIP)");
		//logger.info("Start excution ********************************************* method login( emailMobileNo,  password, imeiIP)"+password);
		logger.info("Start excution ********************************************* method login( emailMobileNo, imeiIP)"+aggreatorid);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		Query query = null;
		String newOtp = null;
		WalletMastBean walletMastBean=new WalletMastBean();
		LoginResponse loginResponse=new LoginResponse();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "");
		
		//logger.info("userId ********************************************* " + userId);
		try {
			walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			transaction=session.beginTransaction();
			
			
			if (new EmailValidator().validate(userId)) {
				if (checkEailIdWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Email register with us");
					
					//logger.info("Inside *********************************************Email register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE emailid=:userId and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Email id not register with us");
					
					//logger.info("Inside *********************************************Email id not register with us" + userId);
					loginResponse.setStatusCode("7010");
					return loginResponse;
				}
			} else if((Pattern.matches("^([6-9]{1})([0-9]{9})$", userId))){
				if (checkMobileNoWhileLogin(userId,aggreatorid)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "mobile register with  us");
					
					//logger.info("Inside *********************************************mobile register with us" + userId);
					query = session.createQuery(" FROM WalletMastBean WHERE mobileno=:userId  and aggreatorid=:aggreatorid  ");
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Mobile Number not register with us");
					
					//logger.info("Inside *********************************************Mobile Number not register with us" + userId);
					loginResponse.setStatusCode("7011");
					return loginResponse;
					
				}
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "login with id userid");
				
			//	logger.info("Inside *********************************************login with id" + userId);
				query = session.createQuery(" FROM WalletMastBean WHERE id=:userId and aggreatorid=:aggreatorid  ");
				
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "query"+query);
			
			//logger.info("Inside *********************************************query" + query);
			query.setString("userId", userId);
			//query.setString("password", HashValue.hashValue(password));
			query.setString("aggreatorid", aggreatorid);
			List<WalletMastBean> list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=list.get(0);
				if(!(walletMastBean.getApprovalRequired().equals("A")||walletMastBean.getApprovalRequired().equals("N"))){
					loginResponse.setStatusCode("7039");
					return loginResponse;
				}
				
				if(walletMastBean.getUserstatus().equals("S") ||walletMastBean.getUserstatus().equals("B")){
					
					loginResponse.setStatusCode("7050");
					return loginResponse;
				}
				
				
				
			if(walletMastBean.getUserstatus().equalsIgnoreCase("D")){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "user account login send otp for activate user and its wallet");
				
					//logger.info("Inside *********************************************user account login====send otp for activate user and its wallet");
					//logger.info("Inside *********************************************account wallet====send otp for validate mobile on" + walletMastBean.getMobileno());
					/*newOtp = oTPGeneration.generateOTP();
					//logger.info("Inside *********************************************"+newOtp);
					if (!(newOtp == null)) {
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", walletMastBean.getMobileno()).setParameter("otp", newOtp);
						//Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno).setParameter("otp", newOtp);
						//SHA-256
						Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  walletMastBean.getMobileno()).setParameter("aggreatorid", walletMastBean.getAggreatorid()).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
						insertOtp.executeUpdate();
					}
					transaction.commit();
					if (!(newOtp == null)) {
						logger.info("Inside ====================otp send on mobile==========" + walletMastBean.getMobileno() + "===otp==="+ newOtp);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", walletMastBean.getMobileno(),commanUtilDao.getsmsTemplet("regOtp",walletMastBean.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),walletMastBean.getName(),"OTP");
						Thread t = new Thread(smsAndMailUtility);
						t.start();
					}*/
					
					loginResponse.setStatusCode("1010");
					return loginResponse;
				}else{
					Query queryLogin = session.createQuery(	" update WalletMastBean set loginStatus=1,loginipiemi=:loginipiemi,loginagent=:loginagent WHERE mobileno=:mobileno  and aggreatorid=:aggreatorid");
					
					queryLogin.setString("loginipiemi", imeiIP);
					queryLogin.setString("loginagent", agent);
					queryLogin.setString("mobileno", walletMastBean.getMobileno());
					queryLogin.setString("aggreatorid", walletMastBean.getAggreatorid());
					int result = queryLogin.executeUpdate();
					transaction.commit();
					if (result >= 1) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Login status updated");
						
						//logger.info("Inside ====================Login status updated......");
					}
					WalletSecurityBean bean=(WalletSecurityBean)session.get(WalletSecurityBean.class,walletMastBean.getId());
					if(bean!=null){
						if(bean.getUserStatus().equalsIgnoreCase("A"))
						{
							loginResponse.setToken(bean.getToken());
							loginResponse.setTokenSecurityKey(bean.getTokenSecurityKey());
							loginResponse.setStatusCode("1011");
						}
						else
						{
							loginResponse.setStatusCode("8001");
						}
					}
					else
					{
						loginResponse.setStatusCode("1010");
					}
					
					return loginResponse;
				}
		
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "User Id not registered with u");
				
				//logger.info("Inside ===================User Id not registered with us=======");
			
				loginResponse.setStatusCode("7011");
				return loginResponse;
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			loginResponse.setStatusCode("7000");
			return loginResponse;
		} finally {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Login status updated-2");
			
			//logger.info("Inside ==================== Login status updated......2");
			session.close();
		}

	}

	public String getSessionId(String userId)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), userId, "", userId, "", "", "Start Execution");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String sessionId = "0";
		try
		{
			SQLQuery query = session.createSQLQuery("select hcetoken from walletmast where id='"+userId+"'");
			sessionId = (String)query.uniqueResult();
			if(sessionId == null)
				sessionId = "0";
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "Problem in getSessionId |e.getMessage:"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "Login status updated-2");
			session.close();
		}
		return sessionId;
	}
	
	
	public String requestRevoke(String userId,String revokeUserId,String remark,double amount,String ipimei,String agent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), userId, "", userId, "", "", "Start Execution of requestRevoke");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		RevokeUserDtl revokedtl = new RevokeUserDtl();
		Transaction transaction = session.beginTransaction();
		String message = "7000";
		try
		{
			WalletMastBean walletuser = (WalletMastBean)session.get(WalletMastBean.class, userId);
			WalletMastBean revokeUser = (WalletMastBean)session.get(WalletMastBean.class, revokeUserId);
			if((revokeUser.getUsertype() == 2||revokeUser.getUsertype()==3) && walletuser.getAggreatorid().equals(revokeUser.getAggreatorid())){
				if(walletuser.getUsertype() ==  3)
				{
					if(!revokeUser.getDistributerid().equals(walletuser.getId())){
						return message;
					}
				}
				else if(walletuser.getUsertype() == 7&&revokeUser.getUsertype()!=3){
					if(!revokeUser.getSuperdistributerid().equals(walletuser.getId())){
						return message;
					}
					return message;
				}
				String txnId=commanUtilDao.getTrxId("REVO",walletuser.getAggreatorid());
				String flag = new TransactionDaoImpl().revokeRequest(revokeUserId,revokeUser.getWalletid(),amount,ipimei,userId,txnId,revokeUser.getAggreatorid(),agent);
				if(flag.equals("1000")){
					
					revokedtl.setId(txnId);
					revokedtl.setAggreatorId(walletuser.getAggreatorid());
					revokedtl.setAmount(amount);
					revokedtl.setRequesterRemark(remark);
					revokedtl.setRequestedBy(userId);
					revokedtl.setStatus("REQUESTED");
					revokedtl.setUserId(revokeUserId);
					revokedtl.setWalletId(revokeUser.getWalletid());
					session.save(revokedtl);
					transaction.commit();
					message = "1000";
				}
				else{
					return flag;
				}
			}
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "Problem in requestRevoke |e.getMessage:"+e.getMessage());
			e.printStackTrace();
			if(transaction.isActive())
				transaction.rollback();
		}
		finally
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", "requestRevoke finally block");
			session.close();
		}
		return message;
	}
	
	public List<RevokeUserDtl> getRevokeList(String id)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",id, "", "", "start execution getRevokeList");
		factory= DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<RevokeUserDtl> revokeList = new ArrayList<RevokeUserDtl>();
		try
		{
			revokeList = session.createCriteria(RevokeUserDtl.class).add(Restrictions.eq("aggreatorId", id))/*.add(Restrictions.like("status", "REVOKE_REQUESTED"))*/.list();
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",id, "", "", "problem in getRevokeList e.getMessage() "+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return revokeList;
	}
	
	public String rejectAcceptRevoke(String revokeType,String remark,String revokeId,String aggreatorId, String requesterId, String revokeUserId,double amount)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,revokeId,requesterId, revokeUserId, "", "start execution rejectRevoke");
		factory= DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String message = "1001";
		try
		{
			transaction = session.beginTransaction();
			SQLQuery insertQueryt = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
			insertQueryt.setParameter(0, revokeUserId);
			insertQueryt.executeUpdate();
			transaction.commit();
			transaction = session.beginTransaction();
			
			RevokeUserDtl revokeDtl = (RevokeUserDtl)session.createCriteria(RevokeUserDtl.class).add(Restrictions.idEq(revokeId)).
					add(Restrictions.eq("aggreatorId", aggreatorId)).add(Restrictions.eq("requestedBy", requesterId)).add(Restrictions.eq("userId", revokeUserId)).
					add(Restrictions.eq("amount", amount)).add(Restrictions.like("status", "REQUESTED")).uniqueResult();
			if(revokeDtl != null)
			{
				TransactionBean transactionBean=new TransactionBean();
				String txnId=commanUtilDao.getTrxId("AMIW",aggreatorId);
				transactionBean.setTxnid(txnId);
				if(revokeType.equals("1"))
				{
					transactionBean.setUserid(revokeUserId);
					transactionBean.setWalletid(revokeDtl.getWalletId());
					 revokeDtl.setStatus("REJECTED");
						transactionBean.setPayeedtl(revokeDtl.getRequestedBy());
				}else if(revokeType.equals("0")){
					transactionBean.setUserid(requesterId);
					transactionBean.setWalletid(new TransactionDaoImpl().getWalletIdByUserId(requesterId, aggreatorId));
					transactionBean.setPayeedtl(revokeUserId);
					 revokeDtl.setStatus("APPROVED");
				}
				transactionBean.setTxncode(51);
				transactionBean.setTxncredit(amount);
				transactionBean.setTxndebit(0);
				transactionBean.setResult("Success");
				transactionBean.setResptxnid(revokeId);
				transactionBean.setAggreatorid(aggreatorId);
				transactionBean.setWalletuserip("");
				transactionBean.setWalletuseragent("");
				session.save(transactionBean); 
				
				 revokeDtl.setApproverRemark(remark);
				 revokeDtl.setApproverId(aggreatorId);
				 revokeDtl.setApproveDate(new Date());
				 session.save(revokeDtl);
				 transaction.commit();
				 return "1000";
			}
				
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,revokeId,requesterId, revokeUserId, "", "problem in rejectRevoke e.getMessage() "+e.getMessage());
			e.printStackTrace();
		}
		finally {
	  		transaction=session.beginTransaction();
			SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+revokeUserId+"'");
			delete.executeUpdate();
			transaction.commit();
			session.close();
		}
		return message;
	}
	
	public PayoutClientConfig getPayoutClientDetails(String agentId, String transferApp) {

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			List<PayoutClientConfig> clientConfig = session.createCriteria(PayoutClientConfig.class)
					.add(Restrictions.eq("agentId", agentId))
					.add(Restrictions.eq("transferApp", transferApp))
					.add(Restrictions.eq("status", "A")).list();
			if (clientConfig.size() >= 1) {
				return clientConfig.get(0);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	
	@Override
	public AEPSLedger getAepsDetailsView(String txnId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		AEPSLedger agentdetailsview = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT * FROM aepsledger WHERE txnId=:txnId");
			List list = query.list();
			if (list != null && list.size() != 0) {
				agentdetailsview = (AEPSLedger)query.list().get(0);
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return agentdetailsview;

	}
	
	@Override
	public OnboardStatusResponse onboardStatus(String agentId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		OnboardStatusResponse onboardDetail = null;
		try {
                DefaultHttpClient httpClient = new DefaultHttpClient(); 
		        HttpGet getRequest = new HttpGet("https://services.bankit.in:8443/AEPSAPI/customer/onboarddetails");
		        //getRequest.addHeader("accept", "application/json");
		        getRequest.addHeader("Authorization", "Basic QkhBUlRJUEFZIFNFUlZJQ0VTIFBSSVZBVEUgTElNSVRFRC1BTEkyMjI3MTQ6dnFuejdrZ3Z0Yg==");
		        getRequest.addHeader("Content-Type", "application/json");
		        getRequest.addHeader("agent_id", agentId);
		        HttpResponse response = httpClient.execute(getRequest);
		        int statusCode = response.getStatusLine().getStatusCode();
			/*
			 * if (statusCode != 200) { throw new
			 * RuntimeException("Failed with HTTP error code : " + statusCode); }
			 */
		        HttpEntity httpEntity = response.getEntity();
		        String apiOutput = EntityUtils.toString(httpEntity);
		        System.out.println(apiOutput); 
		        
		        ObjectMapper mapper = new ObjectMapper();
		        onboardDetail = mapper.readValue(apiOutput, OnboardStatusResponse.class);
		        
		        if(!onboardDetail.getStatus().equalsIgnoreCase("A0001")) {
		       if(onboardDetail.getFinoonboardstatus().equalsIgnoreCase("1") )
		    	   onboardDetail.setFinoonboardstatus("YES");   
		       else
		    	   onboardDetail.setFinoonboardstatus("NO");
			   if(onboardDetail.getYesonboardstatus().equalsIgnoreCase("1"))
		           onboardDetail.setYesonboardstatus("YES");
			   else
				   onboardDetail.setYesonboardstatus("NO");
		       if(onboardDetail.getIcicionboardstatus().equalsIgnoreCase("1"))
			       onboardDetail.setIcicionboardstatus("YES");
		       else
		    	   onboardDetail.setIcicionboardstatus("NO");
		        }
		} catch (Exception e) {
			
			e.printStackTrace();
			transaction.rollback();
			//return onboardDetail;
		} finally {
			session.close();
		}
		return onboardDetail;

	}

	@Override
	public DmtPricing saveDmtPricing(DmtPricing pricing) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		boolean status=false;
		try {
            Integer to=pricing.getRangeTo();
			Integer from = pricing.getRangeFrom();
			String sqlquery="select rangefrom,rangeto from dmtpricing where userid='"+pricing.getUserId()+"'  ";
			Query query=session.createSQLQuery(sqlquery);
			List<Object[]> list = query.list();
			System.out.println("LIST  "+list.size());
			
			String sql="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangefrom= "+pricing.getRangeFrom()+" and rangeto= "+pricing.getRangeTo()+" and aggregatorId ='"+pricing.getAggregatorId()+"' ";
			Query querySql=session.createSQLQuery(sql);
			List pList = querySql.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",pricing.getUserId(),"", "","saveDmtPricing()","pricing save return"+list.size());
			if(!pList.isEmpty() && pList.size()>0)
			{
				pricing.setStatus(false);
				pricing.setAggregatorId("Record already exist");
				System.out.println("Record already exist");
				status=true;
				return pricing;
			}else
			{
			
            if(list.size()>0) {
			for (Object[] row : list) {
				int min = Integer.parseInt(row[0].toString());
				int max = Integer.parseInt(row[1].toString());
				if((from>=min && from<=max) && (to>=min && to<=max) )
				{
					System.out.println(row[0].toString() + "   -    " + row[1].toString());
					System.out.println("Record exist");
					status=true;
					pricing.setAggregatorId("Pricing details Exist");
					return pricing;
				}else if((from>=min && from<=max) && !(to>=min && to<=max))
				{
					System.out.println(row[0].toString() + "   -    " + row[1].toString());
					System.out.println(row[0].toString() + "   -    " + row[1].toString());
					String sqlquery2="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangeTo in ('"+from+"')  and rangeTo not in ('"+to+"') and rangefrom not in ('"+to+"')  "; 
				    Query query2=session.createSQLQuery(sqlquery2);
					List list2 = query2.list();
					
					String sqlquery3="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangefrom in ('"+from+"')  and rangefrom not in ('"+to+"') and rangeto not in ('"+to+"')  "; 
				    Query query3=session.createSQLQuery(sqlquery3);
					List list3 = query3.list();
					 
					 if(list2.size()==1 && list3.size()==0)
						{
							
						pricing.setStatus(true);
						session.save(pricing);
						transaction.commit();
						pricing.setAggregatorId("Pricing details saved");
						System.out.println("Record saved");	
						return pricing;
						}else
						{
						System.out.println("Invalid range");
						status=false;	
						pricing.setAggregatorId("Invalid Range");
						return pricing;
						}
				}else if(!(from>=min && from<=max) && (to>=min && to<=max))
				{
					System.out.println(row[0].toString() + "   -    " + row[1].toString());
					String sqlquery2="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangeTo in ('"+from+"')  and rangeTo not in ('"+to+"') and rangefrom not in ('"+to+"')  "; 
				    Query query2=session.createSQLQuery(sqlquery2);
					List list2 = query2.list();
					
					String sqlquery3="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangefrom in ('"+from+"')  and rangefrom not in ('"+to+"') and rangeto not in ('"+to+"')  "; 
				    Query query3=session.createSQLQuery(sqlquery3);
					List list3 = query3.list();
					 
					 if(list2.size()==1 && list3.size()==0)
						{
							
						pricing.setStatus(true);
						session.save(pricing);
						transaction.commit();
						pricing.setAggregatorId("Pricing details saved");
						System.out.println("Record saved");	
						return pricing;
						}else
						{
						
						System.out.println("Invalid range");
						status=false;
						pricing.setAggregatorId("Invalid Range");
						return pricing;
						}
				}
				else if(!(from>=min && from<=max) && !(to>=min && to<=max) )
				{
					System.out.println(row[0].toString() + "   -    " + row[1].toString());
					String sqlquery2="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangeTo in ('"+from+"')  and rangeTo not in ('"+to+"') and rangefrom not in ('"+to+"')  "; 
				    Query query2=session.createSQLQuery(sqlquery2);
					List list2 = query2.list();
					
					String sqlquery3="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangefrom in ('"+from+"')  and rangefrom not in ('"+to+"') and rangeto not in ('"+to+"')  "; 
				    Query query3=session.createSQLQuery(sqlquery3);
					List list3 = query3.list();
					 
					 if(list2.size()==1 && list3.size()==0)
						{
					/*	 String sqlquery2="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangeTo in ('"+from+"')  and rangeTo not in ('"+to+"') and rangefrom not in ('"+to+"')  "; 
						 Query query2=session.createSQLQuery(sqlquery2);
						 List<Object[]> obj = query2.list();
						 for (Object[] rows : obj) {
							 System.out.println("mmmmm  "+rows[0]);
							int minx = Integer.parseInt(rows[0].toString());
							int maxx = Integer.parseInt(rows[3].toString());
						 if(to>=minx && to<=maxx)
						 {
							System.out.println("Invalid range");
							status=false;
							pricing.setAggregatorId("Invalid Range");
							return pricing; 
						  }else {*/
							 pricing.setStatus(true);
								session.save(pricing);
								transaction.commit();
								pricing.setAggregatorId("Pricing details saved");
								System.out.println("Record saved");	
								return pricing; 
						    // }
						 
					      //}
						 
							
						}else  if(list2.size()==0 && list3.size()==0)
						{
							
							pricing.setStatus(true);
							session.save(pricing);
							transaction.commit();
							pricing.setAggregatorId("Pricing details saved");
							System.out.println("Record saved");	
							return pricing;
						}
					
					//System.out.println("Record save");
					//status=false;
					//pricing.setAggregatorId("Record Saved");
					//return pricing;
				}
				 
			 }
				
            }else
            {
             	System.out.println("Record save");
            	pricing.setStatus(true);
				session.save(pricing);
				transaction.commit();
            	status=false;
				pricing.setAggregatorId("Record Saved");
				return pricing;	
            }
		 }	
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return pricing;
  }	

	
	
	@Override
	public DmtPricing deletePricing(DmtPricing pricing) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		try {
           
			String sqlquery="select * from dmtpricing where userid= '"+pricing.getUserId()+"' and rangefrom= "+pricing.getRangeFrom()+" and rangeto= "+pricing.getRangeTo()+" and aggregatorId ='"+pricing.getAggregatorId()+"' ";
			Query query=session.createSQLQuery(sqlquery);
			List list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",pricing.getUserId(),"", "","saveDmtPricing()","pricing save return"+list.size());
			if(!list.isEmpty() && list.size()>0)
			{
				 Object[] obj = (Object[])list.get(0);
				 DmtPricing dmtPricing = new DmtPricing();
				 dmtPricing.setId(Integer.parseInt((""+obj[0]).trim()));
				 dmtPricing.setRangeTo(pricing.getRangeTo());
				 dmtPricing.setDistributorPercentage(pricing.getDistributorPercentage()); 
				 dmtPricing.setSuperDistributorPercentage(pricing.getSuperDistributorPercentage());
				 pricing.setStatus(true);
				 session.delete(dmtPricing);
				 transaction.commit();
				 System.out.println("Record deleted");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return pricing;
  }
	
	
	@Override
	public WalletMastBean getPlan(String agentId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		List<WalletMastBean> results = new ArrayList<WalletMastBean>();
		WalletMastBean walletMast=null;
		try{
			Query query1 = session.createQuery(" FROM WalletMastBean WHERE (mobileno=:userId or  emailid=:userId or id=:userId )");
			query1.setString("userId",agentId);
		    results =  query1.list();
		    transaction.commit();
			if(results.size()>0) {
		    walletMast=results.get(0);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
			
		return walletMast;
  }
	
	
	@Override
	public List<CommPercMaster> addNewPlan(String agggId) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "addNewPlan(String id)","addNewPlan    agggId " + agggId); 
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		List<CommPercMaster> commn=new ArrayList<>();
		try {
			
			SQLQuery nextQuery = null;
		/*	Query queryPlan = session.createSQLQuery("select nextval('COMMISSION',agggId)");
			List listPlan = queryPlan.list();        // select nextval('COMMISSION','OAGG001050')
			String newPlan = (String) listPlan.get(0);
		*/
			String queryNext = "SELECT nextval(:ad,:aggregatorid)";
			nextQuery = session.createSQLQuery(queryNext);
			nextQuery.setParameter("aggregatorid", agggId);
			nextQuery.setParameter("ad", "COMMISSION");

			Object nextObj = nextQuery.uniqueResult();
			String newPlanIdd =""+nextObj;
			String newPlan="";
			if("OAGG001057".equalsIgnoreCase(agggId))
              newPlan=newPlanIdd.replace("COMM","COMMW");
			else
				newPlan=""+nextObj;
				
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "nextObj pId "+nextObj, "", "addNewPlan()","  New Plan " + newPlan); 
			
			System.out.println("newPlanId " + newPlan);
            if(newPlan==null)
            	return commn;
			
			Query queryList = session
					.createSQLQuery("select  1  from commpercmaster  where planid='" + newPlan + "' limit 1");
			List listList = queryList.list();
			if (listList.size() == 0) {
				Query queryInsert = session.createSQLQuery(
						"insert into commpercmaster(planid,txnid,servicetax,description,aggregatorperc,distributorperc,agentperc,subagentperc,commperc,fixedcharge,maxfixedcharge,ismaxfixedapply,sbcess,kycess,txntype,rangefrom,rangeto,wlaggreatorchg,bankrefid,ipiemi,agent,wlaggreatorservicechg,superdistperc,fixedorperc,agentcommission,agretrunAmt,tdsApply) select '"+newPlan+"',txnid,servicetax,description,aggregatorperc,distributorperc,agentperc,subagentperc,commperc,fixedcharge,maxfixedcharge,ismaxfixedapply,sbcess,kycess,txntype,rangefrom,rangeto,wlaggreatorchg,bankrefid,ipiemi,agent,wlaggreatorservicechg,superdistperc,fixedorperc,agentcommission,agretrunAmt,tdsApply from commpercmaster where planid='COMMW02008' ");
				int listInsert = queryInsert.executeUpdate();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "addNewPlan()",
						"addNewPlan Number of rows affect " + listInsert + "   New Plan " + newPlan);
				transaction.commit();
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "addNewPlan()",
					"addNewPlan    New Plan " + newPlan);
			Query query = session.createSQLQuery(
					"select rangefrom,rangeto,commperc,distributorperc,superdistperc,commid from commpercmaster where planid=:planId and txntype in ('IMPS') order by rangefrom ");
			query.setString("planId", newPlan);
			List results = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "addNewPlan()",
					"addNewPlan List Table rows  " + results.size() + "   New Plan " + newPlan);

			if (results.size() > 0) {
				for (int i = 0; i < results.size(); i++) {
					CommPercMaster comm = new CommPercMaster();
					Object[] obj1 = (Object[]) results.get(i);
					comm.setRangeFrom(Double.parseDouble("" + (obj1[0])));
					comm.setRangeTo(Double.parseDouble("" + obj1[1]));
					// comm.setCommperc(Double.parseDouble(""+obj1[2]) );
					comm.setDistributorperc(Double.parseDouble("" + obj1[3]));
					comm.setSuperdistperc(Double.parseDouble("" + obj1[4]));
					comm.setCommid(Integer.parseInt("" + obj1[5]));
					commn.add(comm);
			 		}
              }
			// transaction.commit();
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","addNewPlan()","addNewPlan exception  New Plan "+e.getMessage());
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
			
		return commn;
  }
	
	

	@Override
	public WalletMastBean agentPlan(String agentId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		Map<String, String> aggMap = new HashMap<String, String>();
		//List<WalletMastBean> results = new ArrayList<WalletMastBean>();
		WalletMastBean walletMast=new WalletMastBean();
		try{
			
			Query query = session.createSQLQuery("select planid,aggreatorid from walletmast where id=:userId ");
			query.setString("userId",agentId);
		    List results = query.list();
		    transaction.commit();
		    Object[] obj1=(Object[])results.get(0);
		    
		    String plan = String.valueOf(obj1[0]);
		    String aggregatorId = String.valueOf(obj1[1]);	
		    walletMast.setPlanId(plan);
		    walletMast.setAggreatorid(aggregatorId);
		
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
			
		return walletMast;
    }
	
	 
	 @Override
	 public CommPercMaster updateNewPlan(double dist,double supDist,int row) {
			
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		CommPercMaster comm=new CommPercMaster(); 
		int rowId = 0 , update =0;
		 try {
			  Query queryList = session.createSQLQuery("select commid from commpercmaster  where concat(concat(rangefrom,rangeto),planid) in (select concat(concat(rangefrom,rangeto),planid) from commpercmaster where commid=:rowId)");
			  queryList.setInteger("rowId",row);
			  List rowList = queryList.list();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","updateNewPlan()","updateNewPlan Distributor % "+dist+"  Super Distributor % "+supDist+"  Row   "+row+"   List "+rowList.size());
			  for(int i=0;i<rowList.size();i++)
			  {
				Object obj = rowList.get(i);  
				rowId=Integer.parseInt(""+obj);  
			    Query query = session.createSQLQuery("update commpercmaster set distributorperc=:dist , superdistperc=:supDist where commid=:rowId ");
				query.setDouble("dist",dist);
				query.setDouble("supDist",supDist);
				query.setInteger("rowId",rowId);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","updateNewPlan()","updateNewPlan   Row   "+rowId+"   List "+rowList.size());
				update = query.executeUpdate();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","updateNewPlan()","updateNewPlan Execute Update "+update+"  Row   "+rowId);
				  
			  }	
			  transaction.commit();
			  if(update>0)
				{
				  comm.setAgent("Plan updated Successfully");
				  comm.setCommid(1);
				}else
				{
					comm.setAgent("Plan not updated ");
					comm.setCommid(0);	
				}
			
			}catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
			} finally {
				session.close();
			}
				
			return comm;
	    }
	 
	 
	 
	
	@Override
	public WalletMastBean updateAgentPlan(String agentId,String planId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();  
		WalletMastBean mast=new WalletMastBean();
	 try {
			Query query = session.createSQLQuery("update walletmast set planid=:planId where id=:userId ");
			query.setString("userId",agentId);
			query.setString("planId",planId);
		    int update = query.executeUpdate();
		    transaction.commit();
			if(update>0)
			{
			  mast.setAgent("Plan Updated Successfully");
			  mast.setAgentCode("1");
			  mast.setPlanId(planId);
			}else
			{
			  mast.setAgent("Plan not Updated");
			  mast.setAgentCode("0");	
			}
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
			
		return mast;
    }
	
	@Override
	public Map<String, String> aggPlan(String aggId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		Map<String, String> aggMap = new HashMap<String, String>();
		String queryStr="";
		try {
			//Query query = session.createSQLQuery("select distributorperc,superdistperc,planid from commpercmaster where (planid in (select distinct(planid) from walletmast where aggreatorid='"+aggId+"') or planid not in (select distinct(planid) from walletmast)) and rangefrom=0 and txntype in ('IMPS') "); // ('IMPS','NEFT')
			if("OAGG001050".equalsIgnoreCase(aggId))
			queryStr="select distributorperc,superdistperc,planid from commpercmaster where (planid in (select distinct(planid) from walletmast where aggreatorid='"+aggId+"') or planid not in (select distinct(planid) from walletmast)) and rangefrom=0 and txntype in ('IMPS') ";  
			else
			queryStr="select distributorperc,superdistperc,planid from commpercmaster where (planid in (select distinct(planid) from walletmast where aggreatorid='"+aggId+"') or planid not in (select distinct(planid) from walletmast)) and rangefrom=0 and txntype in ('IMPS') and planid like 'COMMW%'  ";  
				
			Query query = session.createSQLQuery(queryStr);
			
			List results = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","Agent aggPlan ()","checking plan list "+results.size());
			
		    for(int i=0;i<results.size();i++)
		    {
		    	Object[] obj2=(Object[])results.get(i);
		    	 
		    	if(obj2[2]!=null && !"null".equalsIgnoreCase(String.valueOf(obj2[2])) )  
		    	aggMap.put(String.valueOf(obj2[2]), "Plan:"+" - "+String.valueOf(obj2[2])+"   Distributor % :"+" - "+ String.valueOf(obj2[0])+"  SuperDistributor % :"+" - "+String.valueOf(obj2[1]));
		    }
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","Agent aggPlan ()","checking after loop  list size "+results.size());
			
		    transaction.commit();
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
	return aggMap;
   }

	
	
	@Override
	public List<DmtPricing> agentPricingDetail(String agentId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		List<DmtPricing> list = new ArrayList<DmtPricing>();
		
		try {
            Query query = session.createQuery("from DmtPricing where userId=:agentId");
            query.setString("agentId", agentId);
            list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","Agent Pricing ()","checking Agent Pricing list "+list.size());
			 
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","()","checking  error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return list;
  }	
	
	
	@Override
	public AddBankAccount updateAgentAccount(AddBankAccount bank) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		List<DmtPricing> list = new ArrayList<DmtPricing>();
		
		try {
			String queryStr = "update WalletMastBean  set bankName='"+bank.getBankName().trim()+"' , accountNumber='"+bank.getAccountNumber().trim()+"'  , ifscCode='"+bank.getIfscCode().trim()+"'   where id=:userId";
	        Query query = session.createQuery(queryStr);
	        query.setParameter("userId", bank.getUserId().trim());

	        
	        int count = query.executeUpdate();
	        transaction.commit();
			System.out.println("COUNT  "+count);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",bank.getUserId(),"", "","updateLeanAmount()","excution updateLeanAmount "+count);
    	 
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","()","checking  error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return bank;
  }	
	

	
	@Override
	public List<AddBankAccount> agentAccountDetails(String agentId) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		List<AddBankAccount> list = new ArrayList<AddBankAccount>();
		
		try {
            Query query = session.createQuery("from AddBankAccount where userId=:agentId");
            query.setString("agentId", agentId);
            list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","Agent AddBankAccount ()","checking Agent AddBankAccount list "+list.size());
			 
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","()","checking  error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return list;
  }	
	
	
	
	
	
	public static void main(String[] args) {
		WalletUserDaoImpl walletUserDaoImpl = new WalletUserDaoImpl();
		// System.out.println(walletUserDaoImpl.mobileValidation("9935041287"));
		// System.out.println(walletUserDaoImpl.validateOTP("9935041287",
		// "607105"));
		/*WalletMastBean walletMastBean = new WalletMastBean();
		walletMastBean.setUserstatus("1");
		walletMastBean.setAggreatorid("AGGR001035");
		walletMastBean.setEmailid("shweta.mriu@gmail.com");
		walletMastBean.setWalletid("ASddddddddddd");
		walletMastBean.setId("Sds");
		walletMastBean.setMobileno("9879879872");
		walletMastBean.setPassword("Shwet@1212");
		walletUserDaoImpl.signUpUser( walletMastBean) ;*/

		/*WalletMastBean wallet = new WalletMastBean();
		wallet.setMobileno("9935041287");
		
		wallet.setName("santosh");
		wallet.setUsertype(1);*/

		//System.out.println(walletUserDaoImpl.signUpUser(wallet));
		
		//System.out.println(walletUserDaoImpl.firstTimePasswordChange("9935041287", "123456", "aa"));
		
		
		//walletUserDaoImpl.getWalletConfigByUserId("DIST001004",3,"999921400016092016183721");
		//walletUserDaoImpl.getSmartCardReq("AGGR001035");
		//walletUserDaoImpl.acceptSmartCard("SCRD001006");
		//walletUserDaoImpl.linkCard("SCRD001010","8765987623457623","2345");
		//walletUserDaoImpl.linkCardBlocked("SCRD001010","stolen");
		
		//walletUserDaoImpl.getRejectAgentDetail("AGGR001035","ADMA001038");
		//walletUserDaoImpl.setUpdateMpin("9898989898","1234","AGGR001035");
		
		try {
			//System.out.println(CommanUtil.SHAHashing256("c6b43f95488c1ca3ece8a18e27912a8ffd1c7fc33505515856c46fc11d1596d5"));
			System.out.println("encrp :- "+EncryptionDecryption.getEncrypted("ERYPS2421A", "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	 public Map<String,String> distributorDetails(String customerId){

	        System.out.println("CustomerID "+customerId);
	  List agentList=new ArrayList();
	  Map<String,String> detailMap=new HashMap<String ,String>();
	  factory=DBUtil.getSessionFactory();
	  Session session = factory.openSession();
	        Transaction transaction = session.beginTransaction();
	  try {
	   String sqlquery="select id,name,mobileno,aggreatorid  from walletmast where id='"+customerId.trim()+"'";
	   Query query=session.createSQLQuery(sqlquery);
	      agentList = query.list();
	      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","","","" ,"distribuotrDetails()"+agentList.size());
	   Object[] obj=(Object[])agentList.get(0);
	   
	    detailMap.put("agentId",String.valueOf(obj[0]) );
	    detailMap.put("userName",String.valueOf(obj[1]));
	    detailMap.put("mobileNo",String.valueOf(obj[2]));
	    detailMap.put("aggregatorId",String.valueOf(obj[3]));
	    }
	   
	  catch(Exception e){
	   e.printStackTrace();
	     transaction.rollback();
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","distribuotrDetails()","checking distribuotrDetails error"+ e.getMessage()+" "+e);
	     logger.debug("*****************error************** " + e.getMessage(), e);
	    }
	    finally{
	     session.close();
	   }
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","distribuotrDetails()","stop excution distribuotrDetails return"+agentList.size());
	  return detailMap;
	    }
	
	
	
	@Override
	public AddBankAccount saveAccount(AddBankAccount account) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		String userId = account.getUserId();
		try {
		    String sqlquery="select * from addbankaccount where userId= '"+userId+"'";
			Query query=session.createSQLQuery(sqlquery);
			List list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","saveAccount()","stop excution saveAccount return"+list.size());
		   if(list.size()>5)
			{
				System.out.println("You have added maximum account");
				account.setStatus(false);
				account.setAccountNumber("5");
			}else
			{
				 String sqlqueryAc="select * from addbankaccount where userId= '"+userId+"' and accountnumber= '"+account.getAccountNumber()+"' ";
				 Query queryAc=session.createSQLQuery(sqlqueryAc);
				 List listAc = queryAc.list();
				if(listAc.size()>0) 
				{ 
					System.out.println("Account already exist");
					account.setStatus(false);
					account.setAccountNumber("0");
				}else {	
				account.setComment("Accepted");
				account.setStatusCode(1);
				account.setStatus(true);
				session.save(account);
				transaction.commit();
				System.out.println("Record saved");
			    }
		  }
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return account;
    }		
		
	
	 @Override
	 public AddBankAccount deleteUserAccount(AddBankAccount account)
	    {
	    	
	    	factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	    Transaction transaction = session.beginTransaction();
	  	    try
	    	{
	  	    	String queryStr = "delete from AddBankAccount where id=:id";
	  	        Query query = session.createQuery(queryStr);
	  	        query.setParameter("id", account.getId());
	  	        int count = query.executeUpdate();
	  	        transaction.commit();
	  	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","deleteAccount()","stop excution deleteAccount "+count);
	  	        account.setStatus(true);
	        	System.out.println("COUNT "+count);
	    	}catch(Exception e)
	    	{
	    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","deleteAccount()","stop excution deleteAccount return"+e.getMessage()+"   "+e);
	    		logger.debug("deleteRecord*****************error************** " + e.getMessage(), e);  
	    		e.printStackTrace();
	    		transaction.rollback();
	    	}finally
	    	{
	    		session.close();
	    	}
	  	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","deleteAccount()","stop excution deleteAccount ");
			
	    	return account;
	    }
	    
	
	    @Override
		public AddBankAccount updateUserAccount(AddBankAccount account) {
			
		    factory = DBUtil.getSessionFactory();
			Session session = factory.openSession();
			transaction = session.beginTransaction();
			//AddBankAccount bank=new AddBankAccount();
			try {
				AddBankAccount bank = (AddBankAccount)session.get(AddBankAccount.class, account.getId());
				
				if(bank!=null)
				{
					bank.setId(account.getId());
					bank.setName(account.getName());
					bank.setBankName(account.getBankName());
					bank.setAccountNumber(account.getAccountNumber());
					bank.setIfscCode(account.getIfscCode());
					session.save(bank);
					transaction.commit();
					System.out.println("Record updated");
					account.setStatus(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
			} finally {
				session.close();
			}
			return account;
	    }		
		
	 @Override
	 public Map<String,String> agentAccounts(String account,String id){

		  List agentList=new ArrayList();
		  Map<String,String> detailMap=new HashMap<String ,String>();
		  factory=DBUtil.getSessionFactory();
		  Session session = factory.openSession();
		  Transaction transaction = session.beginTransaction();
		  try {
			  
			 String sqlString="select accountnumber  from addbankaccount where accountNumber='"+account+"' and userId='"+id+"' ";
			 Query queryQ=session.createSQLQuery(sqlString);
			 List agentId = queryQ.list(); 
			 System.out.println("IDDDD   "+agentId.size());
			 Object userIdObj = agentId.get(0); 
			 String accounts = String.valueOf(""+userIdObj);
			 
			 
			 
		     String sqlquery="select userid,mobileno,name,bankname,accountnumber,ifsccode, walletid from addbankaccount where accountNumber='"+accounts+"' and userId='"+id+"' ";
			 Query query=session.createSQLQuery(sqlquery);
		     agentList = query.list();
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),account,"","","","" ,"agentAccounts()"+agentList.size());
		     Object[] obj=(Object[])agentList.get(0);
		   
		    detailMap.put("userId",String.valueOf(obj[0]) );
		    detailMap.put("mobileNo",String.valueOf(obj[1]));
		    detailMap.put("name",String.valueOf(obj[2]));
		    detailMap.put("bankName",String.valueOf(obj[3]));
		    detailMap.put("accountNumber",String.valueOf(obj[4]));
		    detailMap.put("ifscCode",String.valueOf(obj[5]));
		    detailMap.put("walletId",String.valueOf(obj[6]));
		  }catch(Exception e){
		     e.printStackTrace();
		     transaction.rollback();
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",account,"", "","agentAccounts()","checking agentAccounts error"+ e.getMessage()+" "+e);
		     logger.debug("*****************error************** " + e.getMessage(), e);
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",account,"", "","agentAccounts()","stop excution agentAccounts return"+agentList.size());
		  }
		    finally{
		     session.close();
		   }
		 return detailMap;
		}
		
	
	 @Override
	 public Map<String,String> getFlag(String id){

		  List list=new ArrayList();
		  Map<String,String> detailMap=new HashMap<String ,String>();
		  factory=DBUtil.getSessionFactory();
		  Session session = factory.openSession();
		  Transaction transaction = session.beginTransaction();
		  try {
			  
		     String sqlquery="select fimps,fneft,asimps,asneft, pimps, pneft,pmode,finoaeps,yesaeps, iciciaeps, matm  from walletconfig where aggreatorid='"+id+"'  ";
			 Query query=session.createSQLQuery(sqlquery);
		     list = query.list();
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),id,"","","","" ,"getFlag()"+list.size());
		     Object[] obj=(Object[])list.get(0);
		   
		    detailMap.put("fImps",String.valueOf(obj[0])  );
		    detailMap.put("fNeft",String.valueOf(obj[1]) );
		    detailMap.put("asImps",String.valueOf(obj[2]) );
		    detailMap.put("asNeft",String.valueOf(obj[3]) );
		    detailMap.put("pImps",String.valueOf(obj[4]) );
		    detailMap.put("pNeft",String.valueOf(obj[5]) );
		    detailMap.put("pMode",String.valueOf(obj[6]) );
		    
		    detailMap.put("finoAeps",String.valueOf(obj[7]) );
		    detailMap.put("yesAeps",String.valueOf(obj[8]) );
		    detailMap.put("iciciAeps",String.valueOf(obj[9]) );
		    detailMap.put("matm",String.valueOf(obj[10]) ); 
		    
		  }catch(Exception e){
		     e.printStackTrace();
		     transaction.rollback();
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",id,"", "","getFlag()","checking getFlag error"+ e.getMessage()+" "+e);
		     logger.debug("*****************error************** " + e.getMessage(), e);
		    }
		    finally{
		     session.close();
		   }
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",id,"", "","getFlag()","stop excution getFlag return"+list.size());
		  return detailMap;
		}

	 
	@Override
	public WalletMastBean getPanAndAdhar(RechargeTxnBean rechargeTxnBean) {

	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	WalletMastBean wallet=new WalletMastBean();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"AgentId",rechargeTxnBean.getUserId(),"AggregatorId", rechargeTxnBean.getAggreatorid(),"getPanAndAdhar()","");
	String jsonText =null;
	try {
		/*
		String token="aKk5ehF6VpLlxgrVJToOIg==";
		String key="Ib0buhlDB3AIn505e3kJag==";
		TreeMap<String,String>  jsonObj=new TreeMap<String,String>();
        jsonObj.put("userId",rechargeTxnBean.getUserId());
        jsonObj.put("aggreatorid",rechargeTxnBean.getAggreatorid());
        jsonObj.put("token",token); 
        String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(key,jsonObj);
		System.out.println("checkSumString   "+checkSumString);
		rechargeTxnBean.setCHECKSUMHASH(checkSumString);
		*/
			TreeMap<String, String> map = new TreeMap<String, String>(); 
			map.put("userId", rechargeTxnBean.getUserId());
			map.put("aggreatorid", rechargeTxnBean.getAggreatorid());
			
            map.put("CHECKSUMHASH", rechargeTxnBean.getCHECKSUMHASH());
			
			int statusCode = WalletSecurityUtility.checkSecurity(map);

			System.out.println("______statusCode________" + statusCode);
			if (statusCode == 1000) {
				
				List<WalletMastBean> list = new ArrayList<WalletMastBean>();
				Criteria cr=session.createCriteria(WalletMastBean.class);
				cr.add(Restrictions.eq("id", rechargeTxnBean.getUserId()));
				cr.add(Restrictions.eq("aggreatorid", rechargeTxnBean.getAggreatorid()));
				list=cr.list();
				if(list.size()>0)
				{
				 WalletMastBean data = list.get(0);
				 wallet.setPan(EncryptionDecryption.getdecrypted(data.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				 wallet.setAdhar(EncryptionDecryption.getdecrypted(data.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=" ));
				 wallet.setMobileno(data.getMobileno());
				 /*
				 JSONObject jObject = new JSONObject();
				 jObject.put("adhar",EncryptionDecryption.getdecrypted(data.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=" ));
				 jObject.put("pan",EncryptionDecryption.getdecrypted(data.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				 jObject.put("mobile",data.getMobileno());
				 jsonText = jObject.toJSONString();
				*/
				}
			}
			
	}catch(Exception e)
	{
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",rechargeTxnBean.getUserId(),"", "","getPanAndAdhar()","stop excution getPanAndAdhar "+e.getMessage());
		e.printStackTrace();
	}finally{
	     session.close();
	   }
	
		return wallet;
	}
		
	 
	public String updateFlag(String statusRequest)
	{
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    List<Object[]> rows=null;
  	    String res="";
		try {
			Gson g = new Gson();
			ServiceMast serviceMasterBean = g.fromJson(statusRequest, ServiceMast.class);
			String serviceType = serviceMasterBean.getServiceType().trim();
			String status = serviceMasterBean.getStatus().trim();
			
			if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
				
				SQLQuery query = null;
				query = session.createSQLQuery("SELECT aggreatorid FROM walletconfig WHERE   aggreatorid like 'OAGG%' ");
				 rows = query.list();
			}
			
			if(serviceType.equalsIgnoreCase("mATM")) {
			if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
			   if(rows.size()>0)
			   {
				   
				for (Object row : rows) {
					String agg = row.toString();
					updateSerStatus(agg,serviceType, status);
				   }
			   }
				
				res="All Aggregator service have been changed"; 
			}else{
			WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
				if(config!=null)
				{
				 if(status.equalsIgnoreCase("up")) {
			     config.setMatm("1");
				 }else if(status.equalsIgnoreCase("down")) {
				 config.setMatm("0"); 
				 }
				 session.saveOrUpdate(config);
				 transaction.commit();
				 res=" Aggregator Service has been changed.";
				}
			}
			
			}else if(serviceType.equalsIgnoreCase("Aeps")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						  for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed"; 
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setYesAeps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setYesAeps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("FinoAeps")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						  for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				 
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setFinoAeps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setFinoAeps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("IciciAeps")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						  for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setIciciAeps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setIciciAeps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Fino-NEFT")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setfNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setfNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Fino-IMPS")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				 
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setfImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setfImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Aadharshila-NEFT")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				 
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setAsNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setAsNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Aadharshila-IMPS")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				 
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setAsImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setAsImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Paytm-NEFT")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setpNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setpNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}else if(serviceType.equalsIgnoreCase("Paytm-IMPS")) {
				if(serviceMasterBean.getAggregatorId().equalsIgnoreCase("ALL")) {
					if(rows.size()>0)
					   {
						for (Object row : rows) {
							String agg = row.toString();
							updateSerStatus(agg,serviceType, status);
						   }
					   }
					res="All Aggregator service have been changed";	
				
				}else{
				WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, serviceMasterBean.getAggregatorId());	 
					if(config!=null)
					{
					 if(status.equalsIgnoreCase("up")) {
				     config.setpImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setpImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
					 res=" Aggregator Service has been changed.";
					}
				}
			}
			
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","changeServiceStatus()| Exception "+e.getMessage() );
		    e.printStackTrace();
		}finally {
		session.close();	
		}
		return res;
	}

	@Override
	public String getServicesStatus(String serviceRequest) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()" );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String strResponse = null;
		ServiceMast con=new ServiceMast();
		try {
			Gson g = new Gson();
			ServiceMast serviceMasterBean = g.fromJson(serviceRequest, ServiceMast.class);
			String serviceType = serviceMasterBean.getServiceType().trim();
			String status = serviceMasterBean.getStatus().trim();
			String aggregator = serviceMasterBean.getAggregatorId();
			WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, aggregator);	 
			
			if(serviceType.equalsIgnoreCase("Aeps")) {
			String yesAeps = config.getYesAeps()==null?"DOWN":config.getYesAeps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(yesAeps);
			}else if(serviceType.equalsIgnoreCase("FinoAeps")) {
			String finoAeps = config.getFinoAeps()==null?"DOWN":config.getFinoAeps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(finoAeps);
			}else if(serviceType.equalsIgnoreCase("IciciAeps")) {
			String iciciAeps = config.getIciciAeps()==null?"DOWN":config.getIciciAeps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(iciciAeps);
			}else if(serviceType.equalsIgnoreCase("mATM")) {
			String mAtm = config.getMatm()==null?"DOWN":config.getMatm().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(mAtm);
			}else if(serviceType.equalsIgnoreCase("Fino-IMPS")) {
			String finoImps = config.getfImps()==null?"DOWN":config.getfImps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(finoImps);
			}else if(serviceType.equalsIgnoreCase("Fino-NEFT")) {
			String finoNeft = config.getfNeft()==null?"DOWN":config.getfNeft().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(finoNeft);
			}else if(serviceType.equalsIgnoreCase("Aadharshila-IMPS")) {
			String asImps = config.getAsImps()==null?"DOWN":config.getAsImps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(asImps);
			}else if(serviceType.equalsIgnoreCase("Aadharshila-NEFT")) {
			String asNeft = config.getAsNeft()==null?"DOWN":config.getAsNeft().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(asNeft);
			}else if(serviceType.equalsIgnoreCase("Paytm-IMPS")) {
			String paytmImps = config.getpImps()==null?"DOWN":config.getpImps().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(paytmImps);
			}else if(serviceType.equalsIgnoreCase("Paytm-NEFT")) {
			String paytmNeft = config.getpNeft()==null?"DOWN":config.getpNeft().equalsIgnoreCase("0")?"DOWN":"UP";
			con.setStatus(paytmNeft);
			}
			
			Gson gson = new Gson();
			strResponse = gson.toJson(con);
			
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()| Exception "+e.getMessage() );
		}finally {
			session.close();	
		}
		return strResponse;
		
	}
	 
	public String updateSerStatus(String id,String type,String status)
	{
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try {
		WalletConfiguration config = (WalletConfiguration)session.get(WalletConfiguration.class, id);
		 if(config!=null)
		 {
			 if(type.equalsIgnoreCase("Aeps")) {
			     if(status.equalsIgnoreCase("up")) {
			     config.setYesAeps("1");
				 }else if(status.equalsIgnoreCase("down")) {
				 config.setYesAeps("0"); 
				 }
				 session.saveOrUpdate(config);
				 transaction.commit();
				 
			 }else  if(type.equalsIgnoreCase("FinoAeps")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setFinoAeps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setFinoAeps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
				 
			 }else  if(type.equalsIgnoreCase("IciciAeps")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setIciciAeps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setIciciAeps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
				 
			 }else  if(type.equalsIgnoreCase("mATM")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setMatm("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setMatm("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Fino-IMPS")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setfImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setfImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Fino-NEFT")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setfNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setfNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Aadharshila-IMPS")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setAsImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setAsImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Aadharshila-NEFT")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setAsNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setAsNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Paytm-IMPS")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setpImps("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setpImps("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }else  if(type.equalsIgnoreCase("Paytm-NEFT")) {
				 if(status.equalsIgnoreCase("up")) {
				     config.setpNeft("1");
					 }else if(status.equalsIgnoreCase("down")) {
					 config.setpNeft("0"); 
					 }
					 session.saveOrUpdate(config);
					 transaction.commit();
			 }
		 
		 
		 
		 }
		
		
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return "All Aggregator Service have been changed. ";
	}

	@Override
	public String saveNewRegs(NewRegs reg) 
	{
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    String result="fail";
  	    String mob = reg.getMobile();
	  	if(mob!=null)
	    {
	      try {
		       
	          session.save(reg);
	          transaction.commit();
	          result="success";	
		       
	        }catch(Exception e)
	        {
	        	e.printStackTrace();
	        	transaction.rollback();
	        }
	    }else {
	    	
	     }
	  	
		return result;
	 }
	 
	
   public boolean rejectAgentByMuser(String aggId,String createdBy, String id,String declinedComment){
		  
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","rejectAgentByMuser()"+"|Id"+id);
		
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		boolean flag=false;
		try{
			if(id==null ||id.isEmpty()){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","is is null or empty");
				return false;
			}
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, id);
			if(!(walletMastBean.getName()==null)){
				transaction=session.beginTransaction();
				
				walletMastBean.setApprovalRequired("R");
				session.save(walletMastBean);
				AgentDeclinedComment agentDeclinedComment=new AgentDeclinedComment();
				agentDeclinedComment.setAgentId(id);
				agentDeclinedComment.setDeclinedComment(declinedComment);
				agentDeclinedComment.setStatus("OPEN");
				agentDeclinedComment.setDeclineddate(new java.sql.Date(System.currentTimeMillis()));
				session.save(agentDeclinedComment);
				 
				
				transaction.commit();
				return true;
			}else{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","invalid Id");
				
				//logger.info(" *************************invalid Id******************************* method acceptAgentByAgg()");
				return false;
			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in rejectAgentByAgg" + e.getMessage()+" "+e);
			
	    	//logger.debug("problem in rejectAgentByAgg*****************************************" + e.getMessage(), e);
	    	e.printStackTrace();
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","return"+flag);
		
		//logger.info(" **********************return **********************************************************"+flag);
		return flag;
	}
	
	
   public List<DeclinedListBean> declinedNewAgentList(String aggreatorId,String createdBy, int type){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","getRejectAgentDetail()");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		 List<DeclinedListBean> results=null;
		 String stringQuery=null;
		 SQLQuery query =null;
		try{
			if(type==6)
			{
		    stringQuery="SELECT  a.id AS agentid,mobileno, usertype,createdby,soName,managerId,salesId,   emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId   AND a.id=b.agentid and b.status='OPEN'";
		    query = session.createSQLQuery(stringQuery);
		    } else if (type==3){
				stringQuery="SELECT a.id AS agentid,mobileno, usertype ,createdby,soName,managerId,salesId,  emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND distributerid=:createdBy AND a.id=b.agentid and b.status='OPEN'";
				query = session.createSQLQuery(stringQuery);
				query.setParameter("createdBy", createdBy);
			} else if (type==7){
				stringQuery="SELECT a.id AS agentid,mobileno, usertype ,createdby,soName,managerId,salesId,  emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND superdistributerid=:createdBy AND a.id=b.agentid and b.status='OPEN'";
				query = session.createSQLQuery(stringQuery);
				query.setParameter("createdBy", createdBy);
			} else if (type==8){
				stringQuery="SELECT a.id AS agentid,mobileno, usertype ,createdby,soName,managerId,salesId,  emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND salesId=:createdBy AND a.id=b.agentid and b.status='OPEN'";
				query = session.createSQLQuery(stringQuery);
				query.setParameter("createdBy", createdBy);
			} else if (type==9){
				stringQuery="SELECT a.id AS agentid,mobileno, usertype ,createdby,soName,managerId,salesId,  emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND managerId=:createdBy AND a.id=b.agentid and b.status='OPEN'";
				query = session.createSQLQuery(stringQuery);
				query.setParameter("createdBy", createdBy);
			}
			else {
			stringQuery="SELECT a.id AS agentid,mobileno, usertype ,createdby,soName,managerId,salesId,  emailid,name,comment,b.id AS commentid FROM walletmast a ,agentdeclinedcomment b WHERE approvalrequired='R' AND aggreatorid=:aggreatorId  AND createdby=:createdBy AND a.id=b.agentid and b.status='OPEN'";
			query = session.createSQLQuery(stringQuery);
			query.setParameter("createdBy", createdBy);
			}
			
			
			query.setString("aggreatorId", aggreatorId);
			query.addScalar("agentid").addScalar("commentid").addScalar("mobileno")
			.addScalar("emailid")
			.addScalar("createdby")
			.addScalar("soName")
			.addScalar("managerId")
			.addScalar("salesId")
			.addScalar("usertype")
			.addScalar("name")
			.addScalar("comment")
			.setResultTransformer(Transformers.aliasToBean(DeclinedListBean.class));
			results = query.list();
			}catch(Exception e){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","problem in declinedNewAgentList" + e.getMessage()+" "+e);
				 
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "","getRejectAgentDetail" +results.size());
		
		//logger.info(" **********************return ***********getRejectAgentDetail***********************************************"+results.size());
		return results;
	 }

   
   public List<WalletMastBean> getAgentListNew(WalletMastBean walletMastBean) {
		 
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "|getAgentList()|Id  "+walletMastBean.getId());
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 List<WalletMastBean> results=new ArrayList<WalletMastBean>();
		 Criteria cr = session.createCriteria(WalletMastBean.class);
		 try{
			 WalletMastBean agentMast = (WalletMastBean) session.get(WalletMastBean.class, walletMastBean.getId());
			 if(agentMast!=null) {
			 if(walletMastBean.getUsertype()==3){
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				 StringBuilder serchQuery=new StringBuilder();
				 serchQuery.append(" from WalletMastBean where (createdby in(:id) or distributerid=:dist) and usertype in (2) ");
				
				List<WalletMastBean> wList=session.createQuery("select id from WalletMastBean where distributerid=:sd").setParameter("sd",walletMastBean.getId()).list();
				
				if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
					 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				 }else{
					 serchQuery.append(" and DATE(creationDate)=NOW()");
				 }
				
				serchQuery.append(" order by creationDate DESC");
		        System.out.println("serchQuery~~~~"+ serchQuery);
				 Query query=session.createQuery(serchQuery.toString());
				 query.setParameterList("id", wList);
				 query.setString("dist", walletMastBean.getId());
				 results=query.list(); 
			
			 } else if(walletMastBean.getUsertype()==7){
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				 StringBuilder serchQuery=new StringBuilder();
				 serchQuery.append(" from WalletMastBean where (createdby in(:id) or superdistributerid=:superDist) and usertype in (2,3) ");
				
				List<WalletMastBean> wList=session.createQuery("select id from WalletMastBean where superdistributerid=:sd").setParameter("sd",walletMastBean.getId()).list();
				
				if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
					 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				 }else{
					 serchQuery.append(" and DATE(creationDate)=NOW()");
				 }
				 serchQuery.append(" order by creationDate DESC");
		         System.out.println("serchQuery~~~~"+ serchQuery);
				 Query query=session.createQuery(serchQuery.toString());
				 query.setParameterList("id", wList);
				 query.setString("superDist", walletMastBean.getId());
				 results=query.list(); 
			
			 }else if(walletMastBean.getUsertype()==8){
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				 StringBuilder serchQuery=new StringBuilder();
				 serchQuery.append(" from WalletMastBean where (createdby in(:id) or salesId=:salesId) and usertype in (2,3,7) ");
				
				List<WalletMastBean> wList=session.createQuery("select id from WalletMastBean where salesId=:sd").setParameter("sd",walletMastBean.getId()).list();
				
				if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
					 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				 }else{
					 serchQuery.append(" and DATE(creationDate)=NOW()");
				 }
				 serchQuery.append(" order by creationDate DESC");
		         System.out.println("serchQuery~~~~"+ serchQuery);
				 Query query=session.createQuery(serchQuery.toString());
				 query.setParameterList("id", wList);
				 query.setString("salesId", walletMastBean.getId());
				 results=query.list(); 
			
			 }else if(walletMastBean.getUsertype()==9){
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				 StringBuilder serchQuery=new StringBuilder();
				 serchQuery.append(" from WalletMastBean where (createdby in(:id) or managerId=:managerId) and usertype in (2,3,7,8) ");
				
				List<WalletMastBean> wList=session.createQuery("select id from WalletMastBean where managerId=:sd").setParameter("sd",walletMastBean.getId()).list();
				if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
					 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				 }else{
					 serchQuery.append(" and DATE(creationDate)=NOW()");
				 }
				
				 serchQuery.append(" order by creationDate DESC");
				 System.out.println("serchQuery~~~~"+ serchQuery);
				 Query query=session.createQuery(serchQuery.toString());
				 query.setParameterList("id", wList);
				 query.setString("managerId", walletMastBean.getId());
				 results=query.list(); 
			
			 }else if(walletMastBean.getUsertype()==4 || walletMastBean.getUsertype()==6){
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				 StringBuilder serchQuery=new StringBuilder();
				 serchQuery.append(" from WalletMastBean where usertype in (2,3,7,8,9) and aggreatorid=:aggreatorid ");		
					   
				 if(walletMastBean.getStDate()!=null&&!walletMastBean.getStDate().isEmpty() &&walletMastBean.getEndDate()!=null&&!walletMastBean.getEndDate().isEmpty()){
					 serchQuery.append(" and DATE(creationDate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"', '/', '-'),'%d-%m-%Y')"); 
				 }else{
					 serchQuery.append(" and DATE(creationDate)=NOW()");
				 }
				 serchQuery.append(" order by creationDate DESC");
		         
				 System.out.println("serchQuery~~~~"+ serchQuery);
				 Query query=session.createQuery(serchQuery.toString());
				 query.setString("aggreatorid", agentMast.getAggreatorid());
				 results=query.list();
			   }
			 }   
			 
		 }catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "problem in agentList" + e.getMessage()+" "+e);
		 	} finally {
				session.close();
			}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "", "method agentList Result "+results.size());
		  
		 return results;
	}

   
   public List<AssignedUnit> getUserCount(WalletMastBean walletMastBean)
   {
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUserCount()");
		factory=DBUtil.getSessionFactory();
		session = factory.openSession();
		 List<AssignedUnit> results=null;
		 String stringQuery=null;
		 SQLQuery query =null;
		try{
			if(walletMastBean.getUsertype()==6)
			{
			  stringQuery="select usertype,userId, managerId,salesId,allToken,consumeToken,remainingToken  from walletmast a, distributoridallot b where a.id=b.userId and b.aggregatorId=:aggId ";		
			  query = session.createSQLQuery(stringQuery);
			  query.setString("aggId", walletMastBean.getAggreatorid());
			  query.addScalar("usertype")
					.addScalar("userId")
					.addScalar("managerId")
					.addScalar("salesId")
					.addScalar("allToken")
					.addScalar("consumeToken")
					.addScalar("remainingToken")
					.setResultTransformer(Transformers.aliasToBean(AssignedUnit.class));
				results = query.list();
			  
			}
		}catch(Exception e)
		{
			
		}finally {
		session.close();	
		}
		return results;
   }
   
   
   
   
   
   
   
@Override
public Test testUpload(Test walletBean) {
    
		  
		//String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("AGENTDOC");
		String filePath = "D://temp/"+walletBean.getUserId()+"/";
		System.out.println("FILE PATH ----   "+filePath+"     "+walletBean.getFile()+"   "+walletBean.getUserId());
		
		File dir=new File(filePath,walletBean.getPan());
	    boolean mkdir=dir.mkdir();
	    filePath=filePath.concat("/"+walletBean.getPan());
	/*		
		if(walletBean.getFile1()!=null||walletBean.getFile1FileName()!=null||walletBean.getFile1ContentType()!=null){
		try{
		
	      if((walletBean.getFile1ContentType().equalsIgnoreCase("image/png")
	    		  ||walletBean.getFile1ContentType().equalsIgnoreCase("image/jpeg")
	    		  ||walletBean.getFile1ContentType().equalsIgnoreCase("image/jpg")
	    		  ||walletBean.getFile1ContentType().equalsIgnoreCase("image/gif")
	    		  ||walletBean.getFile1ContentType().equalsIgnoreCase("application/pdf"))
	    		  &&(walletBean.getFile1FileName().toLowerCase().endsWith(".png")
	    				  ||walletBean.getFile1FileName().toLowerCase().endsWith(".jpeg")
	    				  ||walletBean.getFile1FileName().toLowerCase().endsWith(".jpg")
	    				  ||walletBean.getFile1FileName().toLowerCase().endsWith(".gif")
	    				  ||walletBean.getFile1FileName().toLowerCase().endsWith(".pfd"))
	    		  &&walletBean.getFile1().length()<=2000000){
	        File fileToCreate1 = new File(filePath,walletBean.getFile1FileName());  
	        FileUtils.copyFile(walletBean.getFile1(), fileToCreate1);		        
	        //walletBean.setIdLoc("./AGENTDOC/"+walletBean.getPan()+"/"+walletBean.getFile1FileName());
		
	      } 
		}catch(Exception e){
			e.printStackTrace();
		}
	    }
		*/
		walletBean.setUserId(filePath);
		
		return walletBean;
	
	}

@Override
public List<WalletMastBean> getUserManagement(WalletMastBean walletMastBean) {
	 
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUserManagement()");
	 factory=DBUtil.getSessionFactory();
	 session = factory.openSession(); 
	 List<WalletMastBean> results =new ArrayList<WalletMastBean>();
	  try{
	
		  if(walletMastBean.getUsertype()==6)
			{
			  Query query = session.createQuery("from WalletMastBean where aggreatorid=:aggreatorid and usertype=2");
			  query.setString("aggreatorid", walletMastBean.getAggreatorid());
			  List<WalletMastBean> walletMast = query.list(); 
			  
			  if(walletMast.size()>0)
			  {
				for(int i=0;i<=walletMast.size()-1;i++) {
				 WalletMastBean mast = (WalletMastBean) walletMast.get(i);  
				 WalletMastBean map=new WalletMastBean();
				 map.setUsertype(mast.getUsertype());
				 map.setId(mast.getId()+" - "+mast.getName());
			     map.setDistributerid(mast.getDistributerid());
				 map.setSuperdistributerid(mast.getSuperdistributerid());
				 map.setSalesId(mast.getSalesId()+" - "+mast.getSoName());
				 map.setManagerId(mast.getManagerId());
				 map.setApproveDate(mast.getApproveDate());
				 map.setUserstatus(mast.getUserstatus());
				 map.setApprovalRequired(mast.getApprovalRequired());
				 
				 results.add(map);
				}
			  }
			  
			}
		
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","problem in getUserManagement"+e.getMessage()+" "+e);
			  e.printStackTrace();
	   		transaction.rollback();
	   	 } finally {
	   	session.close();
	   	}
	 
	return results;
}
   
   
	
	
}
