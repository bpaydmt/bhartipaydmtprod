package com.bhartipay.user.persistence;

import java.util.List;
import java.util.Map;

import com.bhartipay.cme.bean.CMERespBean;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.AssignedUnit;
import com.bhartipay.lean.bean.DmtPricing;
import com.bhartipay.lean.bean.MappingReport;
import com.bhartipay.lean.bean.NewRegs;
import com.bhartipay.lean.bean.OnboardStatusResponse;
import com.bhartipay.lean.bean.Test;
import com.bhartipay.payoutapi.cashfree.persistnace.PayoutClientConfig;
import com.bhartipay.transaction.bean.MoneyRequestBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.user.bean.AgentDetailsView;
import com.bhartipay.user.bean.DeclinedListBean;
import com.bhartipay.user.bean.LoginResponse;
import com.bhartipay.user.bean.RevokeUserDtl;
import com.bhartipay.user.bean.SmartCardBean;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.user.bean.UserBlockedBean;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.UserWalletConfigBean;
import com.bhartipay.user.bean.UserWishListBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.wallet.aeps.AEPSLedger;

public interface WalletUserDao {
	public String mobileValidation(String mobileno);
	//public Boolean validateOTP(String mobileno, String otp);
	public Boolean validateOTP(String userid,String aggreatorid, String otp);
	public Boolean validateOTPFirstTime(String userid,String aggreatorid, String otp);
	//public String signUpUser(String name, String email, String mobile, String password);
	public Boolean validateEailId(String email);
	
	
	public String signUpUser(WalletMastBean walletMastBean);
	public String saveSignUpNewMember(WalletMastBean walletMastBean);
	
	public WalletMastBean updateUserProfile(WalletMastBean walletMastBean);
	public WalletMastBean showUserProfile(String userId);
	public String checkUserKycStatus(String userId);
	public UserWalletConfigBean updateUserWalletConfig(UserWalletConfigBean userWalletConfigBean);
	public UserWalletConfigBean getUserWalletConfig(String userId);
	//public String login(String userId, String password, String imeiIP);
	//////public LoginResponse login(String hceToken,String userId, String password,String aggreatorid, String imeiIP,String agent);
	public LoginResponse login(String userId, String password,String aggreatorid, String imeiIP,String agent);
	public String getPrivateKeyByAggId(String aggId);
	//public String firstTimePasswordChange(String userId, String password, String imeiIP);
	//public String firstTimePasswordChange(String userId, String oldpassword,String newpassword, String imeiIP);
	public String firstTimePasswordChange(String aggId,String userId, String oldpassword,String newpassword);
	public LoginResponse login(String userId,String aggreatorid, String imeiIP,String agent);
	
	//public String changePassword(String id, String password, String imeiIP);
	public String changePassword(String id,String oldpassword, String newpassword, String imeiIP);
	//public Boolean otpResend(String userid) ;
	public Boolean otpResend(String userid,String aggreatorid );
	//public UserProfileBean showUserProfilebymobile(String emailMobile);
	public UserProfileBean showUserProfilebymobile(String emailMobile,String aggreatorid);
	public Map<String,String> getDashBoardDMTData(String emailMobile,String aggreatorid);
	public String saveKyc(String contextPath,String userId,int addkycid,String adddesc,String addkycpic,int idkycid,String iddesc,String idkycpic,String userName,String mobileNo,String emailId,String ipiemi,String agent);
	public String saveSenderKyc(UploadSenderKyc senderKyc,String ipiemi,String agent);
	
	public String saveSenderKycFromApp(UploadSenderKyc senderKyc,String ipiemi,String agent);
	public Boolean saveProfilePic(String userId,String profilepic,String ipiemi,String agent);
	public String getProfilePic(String userId) ;
	public Boolean userLogOut(String userId) ;
	public Map<String,String> getCustomerByAgentId(String agentId,int userType);
	public UserWalletConfigBean getWalletConfigByUserId(String id,int txnType,String walletid);
	public UserWalletConfigBean saveUserWalletConfig(UserWalletConfigBean userWalletConfigBean);
	public UserWishListBean saveWishList(UserWishListBean userWishListBean );
	public List <UserWishListBean> getWishList(String userId);
	public boolean deleteWishList(int wishId);
	//public String forgotPassword(String loginId) ;
	 public String forgotPassword(String loginId,String aggreatorid);
	//public String setForgotPassword(String loginId,String password);
	 public String setForgotPassword(String loginId,String password,String aggreatorid);
	 public String setForgotPassword(String otp,String loginId,String password,String aggreatorid);
	//public Boolean verifyOTP(String userid, String otp);
	public Boolean verifyOTP(String userid,String aggreatorid, String otp);
	public Boolean verifyRefundOTP(String userid,String aggreatorid, String otp);
	public List<WalletMastBean> getAgentDetail(String aggreatorid);
	
	public List<WalletMastBean> newBpayAgent(String aggreatorid);
	
	public boolean acceptAgentByAgg(String id,String utrNo);
	
	public boolean acceptNewAgentByAgg(String id,String utrNo,String token);
	
	public boolean rejectAgentByAgg(String createdBy,String id,String declinedComment);
	public boolean rejectAgentByMuser(String aggId,String subAggid, String agentId,String declinedComment );
	
	public String signUpUserByUpload(WalletMastBean walletMastBean) ;
	public String validateChangeMobile(String userId,String aggreatorid,String mobileNo);
	public Boolean changeOtpResend(String userId,String aggreatorid,String mobileNo) ;
	public Boolean changeMobileNo(String userId,String aggreatorid,String mobileNo,String otp);
	
	public String validateChangeEmail(String userId,String aggreatorid,String email);
	public Boolean changeEmailId(String userId,String aggreatorid,String email,String otp);
	public SmartCardBean generateSmartCardReq(SmartCardBean smartCardBean);
	
	public List<SmartCardBean> getSmartCardReq(String aggreatorId);
	 
	public boolean rejecttSmartCard(String reqId);
	public SmartCardBean acceptSmartCard(String reqId);
	public List<SmartCardBean> getDispatchCard(String aggreatorId);
	public SmartCardBean dispatchCard(String reqId,String prePaidCardNumber,String prePaidCardPin);
	public SmartCardBean getPrePaidCard(String userId);
	public SmartCardBean linkCard(String reqId,String prePaidCardNumber,String prePaidCardPin);
	
	public SmartCardBean linkCardBlocked(String reqId,String blockedType);
	public List <WalletMastBean> getUnAuthorizedAggreator();
	public String authorizedAggreator(String aggreatorId);
	
	public List<SmartCardBean> getSmartCardList(String aggreatorId);
	
	public SmartCardBean suspendedCard(String reqId);
	public SmartCardBean resumePrePaidCard(String reqId);
	public SmartCardBean deleteUser(String reqId,String state);
	public SmartCardBean linkVirtualCard(String reqId);
		
	public String changePassword(WalletBean walletBean);
	
	
	
	public String signUpUserDMT(WalletMastBean walletMastBean) ;
	
	public WalletMastBean agentOnBoad(WalletMastBean walletMastBean);
	
	public WalletMastBean saveSelfAgentOnBoard(WalletMastBean walletMastBean);
	public WalletMastBean updateAgentOnBoad(WalletMastBean walletMastBean);
	public WalletMastBean updateAgentDetails(WalletMastBean walletMastBean);
	public AgentDetailsView getAgentDetails(String agentId,String aggreatorId);
	
	public AgentDetailsView newBpayAgentdetailsview(String agentId,String aggreatorId);
	
	public List<DeclinedListBean> getRejectAgentDetail(String aggreatorId,String createdBy);
	public List<DeclinedListBean> declinedNewAgentList(String aggreatorId,String createdBy, int type);
	
	public WalletMastBean editAgent(WalletMastBean walletMastBean) ;
	public WalletMastBean editAgentPending(WalletMastBean walletMastBean) ;
	public List<WalletMastBean> getAgentList(WalletMastBean walletMastBean);
	public List<WalletMastBean> getAgentListNew(WalletMastBean walletMastBean);
	
	public List<WalletMastBean> getApprovedAgentList(WalletMastBean walletMastBean);
	
	public List<DeclinedListBean> getRejectAgentList(WalletMastBean walletMastBean);
	
	public CMERespBean setUpdateMpin(String mobileNo,String mpin,String aggreatorid);
	
	public String getMpin(String mobileNo,String aggreatorid);
	
	
	public LoginResponse customerValidateOTP(String userid,String aggreatorid, String otp);
	
	public String blockUnblockUser(UserBlockedBean userBlockedBean);
	
	public String distributerOnBoad(WalletMastBean walletMastBean);
	
	public String userOnBoad(WalletMastBean walletMastBean);
	public String allUserOnBoad(WalletMastBean walletMastBean);
	
	public List<WalletMastBean> distributerOnBoardList(WalletMastBean walletMastBean);
	
	//added for WalletApi
	 public String saveWalletApiRequest(String aggreatorid ,String request , String serverName , String apiName);
	 public WalletMastBean getUserByWalletId(String walletId ,String serverName);
	 public WalletMastBean getUserByMobileNo(String mobileNo,String aggreatorId ,String serverName);
	 public WalletMastBean updateUserAsActive(String mobileNo,String aggreatorId ,String serverName);
	 public int verifyMobileNoAggId(String mobileNo ,String aggreatorId,String serverName);
	 public PayoutClientConfig getPayoutClientDetails(String agentId, String transferApp);
	 
	 public List<WalletMastBean> getAgentDetailForApprove (String aggreatorid);
	 public List<MoneyRequestBean> wLCashApprover(WalletMastBean walletBean);
	 public String updateCashRequestStatus(WalletMastBean walletBean);
	 public boolean approvedAgentByAgg(String id,String utrNo);
	 public WalletMastBean getUserByMobileNoWithoutAgg(String mobileNo,String serverName);
	 
	 public String getSessionId(String userId);
	 
	 public String requestRevoke(String userId,String revokeUserId,String remark,double amount,String ipimei,String agent);
	 
	 public List<RevokeUserDtl> getRevokeList(String id);

	 public String rejectAcceptRevoke(String revokeType,String remark,String revokeId,String aggreatorId, String requesterId, String revokeUserId,double amount);

	 public AEPSLedger getAepsDetailsView(String txnId);
	 
	 public OnboardStatusResponse onboardStatus(String agentId);
     public DmtPricing saveDmtPricing(DmtPricing pricing);
     public DmtPricing deletePricing(DmtPricing pricing);
     public WalletMastBean getPlan(String agentId);
     public WalletMastBean agentPlan(String agentId);
     public WalletMastBean updateAgentPlan(String agentId,String planId);
     public Map<String,String> aggPlan(String aggId);
     public List<CommPercMaster> addNewPlan(String id); 
     public CommPercMaster updateNewPlan(double dist,double supDist,int row);
     
     public List<DmtPricing> agentPricingDetail(String agentId);
     public AddBankAccount updateAgentAccount(AddBankAccount bank);
     public Map<String,String> distributorDetails(String distId);
     public AddBankAccount saveAccount(AddBankAccount account);
     public List<AddBankAccount> agentAccountDetails(String agentId);
     public AddBankAccount deleteUserAccount(AddBankAccount account);
     public AddBankAccount updateUserAccount(AddBankAccount account);
     public Map<String,String> agentAccounts(String account,String id);
     public Map<String,String> getFlag(String id);
     public String updateFlag(String id);
     public String getServicesStatus(String id);
     public WalletMastBean getPanAndAdhar(RechargeTxnBean mast);
     public String saveNewRegs(NewRegs reg);
     
     public List<AssignedUnit> getUserCount(WalletMastBean walletMastBean);
     public List<WalletMastBean> getUserManagement(WalletMastBean walletMastBean);
     public Test testUpload(Test test);
     
}
