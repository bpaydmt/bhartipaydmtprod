package com.bhartipay.remittence.service.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bankverificationmast")
public class BankVerificationBean {
	
	@Id
	@Column(name="agentid")
	private String agentId;
	
	@Column(name="bcsessionid")
	private String bcSessionId;
	
	@Column(name="sessionstatus")
	private int sessionStatus;
	
	@Column(name="methodcall")
	private String methodCall;
	
	@Column(name = "bcverificationdate" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date bcVerificationDate;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBcSessionId() {
		return bcSessionId;
	}

	public void setBcSessionId(String bcSessionId) {
		this.bcSessionId = bcSessionId;
	}

	public int getSessionStatus() {
		return sessionStatus;
	}

	public void setSessionStatus(int sessionStatus) {
		this.sessionStatus = sessionStatus;
	}

	public String getMethodCall() {
		return methodCall;
	}

	public void setMethodCall(String methodCall) {
		this.methodCall = methodCall;
	}

	public Date getBcVerificationDate() {
		return bcVerificationDate;
	}

	public void setBcVerificationDate(Date bcVerificationDate) {
		this.bcVerificationDate = bcVerificationDate;
	}
	
	
	
	
	
	
	
	
	

}
