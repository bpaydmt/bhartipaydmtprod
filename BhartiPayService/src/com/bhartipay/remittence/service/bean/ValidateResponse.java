package com.bhartipay.remittence.service.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "VerficationRes")
public class ValidateResponse {

	private String bCAgentId;
	private String sessionid;
	private String description;
	private String status;

	public ValidateResponse() {

	}

	/**
	 * @param bCAgentId
	 * @param sessionid
	 * @param description
	 * @param status
	 */
	public ValidateResponse(String bCAgentId, String sessionid, String description, String status) {

		this.bCAgentId = bCAgentId;
		this.sessionid = sessionid;
		this.description = description;
		this.status = status;
	}

	@XmlElement(name = "BCAgentId")
	public String getbCAgentId() {
		return bCAgentId;
	}

	public void setbCAgentId(String bCAgentId) {
		this.bCAgentId = bCAgentId;
	}

	@XmlElement(name = "Sessionid")
	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	@XmlElement(name = "Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name = "Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
