package com.bhartipay.remittence.service.validator;

import org.apache.log4j.Logger;

import com.bhartipay.remittence.service.bean.BankVerificationBean;
import com.bhartipay.remittence.service.bean.ValidateResponse;
import com.bhartipay.remittence.service.dao.RemittenceDao;
import com.bhartipay.remittence.service.dao.RemittenceDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;


public class RemittenceValidator {
	public static final Logger LOGGER = Logger.getLogger(RemittenceValidator.class.getName());
	
	RemittenceDao remittenceDao=new RemittenceDaoImpl();
	WalletUserDao walletUserDao=new WalletUserDaoImpl();
	
	public ValidateResponse Verification(String bCAgentId,String sessionid,String remarks){
		
		ValidateResponse validateResponse=new ValidateResponse();
		LOGGER.info("===================================================Verification===============================================");
		LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~bCAgentId~~~~~~~"+bCAgentId);
		LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~sessionid~~~~~~~"+sessionid);
		LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~remarks~~~~~~~"+remarks);
		if(bCAgentId.length()>=10){	
				BankVerificationBean varificationMastBean=	remittenceDao.getVarificationDtl(bCAgentId);
				if(varificationMastBean.getBcSessionId()==null){
					remittenceDao.updateOperation(bCAgentId, "Validate",sessionid);
					validateResponse.setbCAgentId(bCAgentId);
					validateResponse.setSessionid(sessionid);
					validateResponse.setStatus("0");
					validateResponse.setDescription("Verification Fail");
					LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Verification Fail");
				}else{
					if(varificationMastBean.getBcSessionId().equals(sessionid)){
						WalletMastBean walletMastBean=	walletUserDao.showUserProfile(bCAgentId);
						LOGGER.info("PAssword~~~~~~~~~~~~~~~~~"+walletMastBean.getPassword());
						if(walletMastBean.getPassword().equals(remarks)){
							//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
							remittenceDao.updateOperation(bCAgentId, "Validate",sessionid);
							validateResponse.setbCAgentId(bCAgentId);
							validateResponse.setSessionid(sessionid);
							validateResponse.setStatus("1");
							validateResponse.setDescription("Verification Success");
							LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Verification Success");
						}else{
							//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
							remittenceDao.updateOperation(bCAgentId, "Validate",sessionid);
							validateResponse.setbCAgentId(bCAgentId);
							validateResponse.setSessionid(sessionid);
							validateResponse.setStatus("-1");
							validateResponse.setDescription("Invalid Password");
							LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Invalid Password");
						}			
					}else{
						//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
						remittenceDao.updateOperation(bCAgentId, "Validate",sessionid);
						validateResponse.setbCAgentId(bCAgentId);
						validateResponse.setSessionid(sessionid);
						validateResponse.setStatus("-1");
						validateResponse.setDescription("Session Expire");
						LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Session Expire");
					}
			}
		}else{
			remittenceDao.updateOperation(bCAgentId, "Validate",sessionid);
			validateResponse.setbCAgentId(bCAgentId);
			validateResponse.setSessionid(sessionid);
			validateResponse.setStatus("0");
			validateResponse.setDescription("Invalid AgentId");
			LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Invalid AgentId");
		}
		LOGGER.info("===================================================Verification==============================================="+validateResponse.getSessionid());
		return validateResponse;
	}
	
}

	
	/*if(bCAgentId.length()>=10){			
			YesBankVerificationMastBean varificationMastBean=	moneyTransferDao.getVarificationDtl(bCAgentId);
		
			if(varificationMastBean.getBcSessionId()==null){
				//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
				moneyTransferDao.updateOperation(bCAgentId, "Validate",sessionid);
				validateResponse.setbCAgentId(bCAgentId);
				validateResponse.setSessionid(sessionid);
				validateResponse.setStatus("0");
				validateResponse.setDescription("Verification Fail");
				LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Verification Fail");
			}else{
				if(varificationMastBean.getBcSessionId().equals(sessionid)){
					WalletAgentBean walletAgentBean=	porttype.getAgentDtl(bCAgentId);
					LOGGER.info("PAssword~~~~~~~~~~~~~~~~~"+walletAgentBean.getAgentPassword().getValue());
					if(walletAgentBean.getAgentPassword().getValue().equals(remarks)){
						//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
						moneyTransferDao.updateOperation(bCAgentId, "Validate",sessionid);
						validateResponse.setbCAgentId(bCAgentId);
						validateResponse.setSessionid(sessionid);
						validateResponse.setStatus("1");
						validateResponse.setDescription("Verification Success");
						LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Verification Success");
					}else{
						//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
						moneyTransferDao.updateOperation(bCAgentId, "Validate",sessionid);
						validateResponse.setbCAgentId(bCAgentId);
						validateResponse.setSessionid(sessionid);
						validateResponse.setStatus("-1");
						validateResponse.setDescription("Invalid Password");
						LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Invalid Password");
					}			
				}else{
					//moneyTransferDao.updateVarificationDtl(bCAgentId,"", 1,sessionid);
					moneyTransferDao.updateOperation(bCAgentId, "Validate",sessionid);
					validateResponse.setbCAgentId(bCAgentId);
					validateResponse.setSessionid(sessionid);
					validateResponse.setStatus("-1");
					validateResponse.setDescription("Session Expire");
					LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Session Expire");
				}
			}
			}else{
			moneyTransferDao.updateOperation(bCAgentId, "Validate",sessionid);
			validateResponse.setbCAgentId(bCAgentId);
			validateResponse.setSessionid(sessionid);
			validateResponse.setStatus("0");
			validateResponse.setDescription("Invalid AgentId");
			LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Verification~~~~~~~~~~~~~~~~~~Invalid AgentId");
		}
		LOGGER.info("===================================================Verification==============================================="+validateResponse.getSessionid());
		return validateResponse;*/