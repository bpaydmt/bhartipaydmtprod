package com.bhartipay.remittence.service.manager;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.remittence.service.bean.ValidateResponse;
import com.bhartipay.remittence.service.validator.RemittenceValidator;






@Path("/RemittenceManager")
public class RemittenceManager {
	
public static final Logger LOGGER = Logger.getLogger(RemittenceManager.class.getName());


@POST
@Path("/ValidateReq")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public ValidateResponse ValidateReq(@FormParam("bCAgentId") String bCAgentId, @FormParam("sessionid") String sessionid,@FormParam("remarks") String remarks){
	LOGGER.info("===================================================ValidateReq=====================================bCAgentId=========="+bCAgentId);
	LOGGER.info("===================================================ValidateReq=====================================sessionid=========="+sessionid);
	LOGGER.info("===================================================ValidateReq=====================================remarks=========="+remarks);
	return new RemittenceValidator().Verification( bCAgentId, sessionid, remarks);	
}






}
