package com.bhartipay.remittence.service.dao;

import com.bhartipay.remittence.service.bean.BankVerificationBean;

public interface RemittenceDao {
	public BankVerificationBean getVarificationDtl(String agentId);
	public int updateOperation(String agentId,String method,String  sessionId);

}
