package com.bhartipay.remittence.service.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.remittence.service.bean.BankVerificationBean;
import com.bhartipay.util.DBUtil;


public class RemittenceDaoImpl implements RemittenceDao{
	
	public static final Logger LOGGER = Logger.getLogger(RemittenceDaoImpl.class.getName());
	SessionFactory factory; 
		
	@Override
	public BankVerificationBean getVarificationDtl(String agentId){
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=null;
		BankVerificationBean varificationMastBean=new BankVerificationBean();
		LOGGER.info("===================================================getVarificationDtl===============================================");
		try{
			Query query=session.createQuery("FROM BankVerificationBean where agentId=:agentId and bcsessionid=0");
			query.setString("agentId", agentId);
			List <BankVerificationBean> list=query.list();
			 if (list != null && list.size() != 0) {
				 varificationMastBean=list.get(0); 
			 }
		}catch(HibernateException e) {			
			e.printStackTrace();
         if (transaction!=null)transaction.rollback();
         e.printStackTrace(); 
         LOGGER.debug("**********************************************Error While getVarificationDtl***************************************"+e.getMessage());
      } finally {
    	 session.close(); 
      }
		return varificationMastBean;	
	}
	
	
	@Override
	public int updateOperation(String agentId,String method,String  sessionId){
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=null;
		int updStatus=0;
		LOGGER.info("===================================================updateOperation===============================================");
		try{
			transaction = session.beginTransaction();
			//Query query=session.createQuery("UPDATE VarificationMastBean set BANK_DESCRIPTION=:desc,BC_Status=:status where BC_AGENT_ID=:agentId and BC_Status=0");
			Query query=session.createQuery("UPDATE BankVerificationBean set methodcall=:method where agentId=:agentId and sessionstatus=0 and bcsessionid=:sessionId ");
			
			query.setString("method", method);
			query.setString("agentId", agentId);
			query.setString("sessionId", sessionId);
			updStatus=query.executeUpdate();
			transaction.commit();
			LOGGER.info("===================================================updateOperation===========================commit===================="+method);
		}catch(HibernateException e) {			
			e.printStackTrace();
         if (transaction!=null)transaction.rollback();
         e.printStackTrace(); 
         LOGGER.debug("**********************************************  Error While updateOperation***************************************"+e.getMessage());
      } finally {
    	 session.close(); 
      }
		return updStatus;
		}
	
	
	

}
