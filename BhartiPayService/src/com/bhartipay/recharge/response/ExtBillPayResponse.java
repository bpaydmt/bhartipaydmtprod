package com.bhartipay.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ExtBillPayResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtBillPayResponse
{
    private InputParams inputParams;

    private String RespCustomerName;

    private String RespDueDate;

    private String RespBillDate;

    private String RespAmount;

    private String responseCode;

    private String txnRespType;

    private String responseReason;

    private String RespBillNumber;

    private String txnRefId;

    private String approvalRefNumber;

    private String RespBillPeriod;

    private String CustConvFee;
    
    private ErrorInfo errorInfo;


    public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public InputParams getInputParams ()
    {
        return inputParams;
    }

    public void setInputParams (InputParams inputParams)
    {
        this.inputParams = inputParams;
    }

    public String getRespCustomerName ()
    {
        return RespCustomerName;
    }

    public void setRespCustomerName (String RespCustomerName)
    {
        this.RespCustomerName = RespCustomerName;
    }

    public String getRespDueDate ()
    {
        return RespDueDate;
    }

    public void setRespDueDate (String RespDueDate)
    {
        this.RespDueDate = RespDueDate;
    }

    public String getRespBillDate ()
    {
        return RespBillDate;
    }

    public void setRespBillDate (String RespBillDate)
    {
        this.RespBillDate = RespBillDate;
    }

    public String getRespAmount ()
    {
        return RespAmount;
    }

    public void setRespAmount (String RespAmount)
    {
        this.RespAmount = RespAmount;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getTxnRespType ()
    {
        return txnRespType;
    }

    public void setTxnRespType (String txnRespType)
    {
        this.txnRespType = txnRespType;
    }

    public String getResponseReason ()
    {
        return responseReason;
    }

    public void setResponseReason (String responseReason)
    {
        this.responseReason = responseReason;
    }

    public String getRespBillNumber ()
    {
        return RespBillNumber;
    }

    public void setRespBillNumber (String RespBillNumber)
    {
        this.RespBillNumber = RespBillNumber;
    }

    public String getTxnRefId ()
    {
        return txnRefId;
    }

    public void setTxnRefId (String txnRefId)
    {
        this.txnRefId = txnRefId;
    }

    public String getApprovalRefNumber ()
    {
        return approvalRefNumber;
    }

    public void setApprovalRefNumber (String approvalRefNumber)
    {
        this.approvalRefNumber = approvalRefNumber;
    }

    public String getRespBillPeriod ()
    {
        return RespBillPeriod;
    }

    public void setRespBillPeriod (String RespBillPeriod)
    {
        this.RespBillPeriod = RespBillPeriod;
    }

    public String getCustConvFee ()
    {
        return CustConvFee;
    }

    public void setCustConvFee (String CustConvFee)
    {
        this.CustConvFee = CustConvFee;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [inputParams = "+inputParams+", RespCustomerName = "+RespCustomerName+", RespDueDate = "+RespDueDate+", RespBillDate = "+RespBillDate+", RespAmount = "+RespAmount+", responseCode = "+responseCode+", txnRespType = "+txnRespType+", responseReason = "+responseReason+", RespBillNumber = "+RespBillNumber+", txnRefId = "+txnRefId+", approvalRefNumber = "+approvalRefNumber+", RespBillPeriod = "+RespBillPeriod+", CustConvFee = "+CustConvFee+"]";
    }
}