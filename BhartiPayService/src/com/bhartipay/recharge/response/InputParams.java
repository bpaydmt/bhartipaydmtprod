package com.bhartipay.recharge.response;

public class InputParams
{
    private Input[] input;

    public Input[] getInput ()
    {
        return input;
    }

    public void setInput (Input[] input)
    {
        this.input = input;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [input = "+input+"]";
    }
}
			
	