package com.bhartipay.recharge.response;

public class ParamInfo
{
    private String dataType;

    private String minLength;

    private String paramName;

    private String isOptional;

    private String maxLength;

    public String getDataType ()
    {
        return dataType;
    }

    public void setDataType (String dataType)
    {
        this.dataType = dataType;
    }

    public String getMinLength ()
    {
        return minLength;
    }

    public void setMinLength (String minLength)
    {
        this.minLength = minLength;
    }

    public String getParamName ()
    {
        return paramName;
    }

    public void setParamName (String paramName)
    {
        this.paramName = paramName;
    }

    public String getIsOptional ()
    {
        return isOptional;
    }

    public void setIsOptional (String isOptional)
    {
        this.isOptional = isOptional;
    }

    public String getMaxLength ()
    {
        return maxLength;
    }

    public void setMaxLength (String maxLength)
    {
        this.maxLength = maxLength;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [dataType = "+dataType+", minLength = "+minLength+", paramName = "+paramName+", isOptional = "+isOptional+", maxLength = "+maxLength+"]";
    }
}
			