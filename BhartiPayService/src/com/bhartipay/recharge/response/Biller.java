package com.bhartipay.recharge.response;

public class Biller {
	private String billerId;

    private String billerDescription;

    private String billerSupportBillValidation;

    private String billerPaymentExactness;

    private String billerFetchRequiremet;

    private String billerName;

    private String billerAdhoc;

    private String billerCategory;

    private BillerInputParams billerInputParams;

    private String billerAmountOptions;

    private String billerPaymentModes;

    private String billerCoverage;

    private String rechargeAmountInValidationRequest;

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    public String getBillerDescription ()
    {
        return billerDescription;
    }

    public void setBillerDescription (String billerDescription)
    {
        this.billerDescription = billerDescription;
    }

    public String getBillerSupportBillValidation ()
    {
        return billerSupportBillValidation;
    }

    public void setBillerSupportBillValidation (String billerSupportBillValidation)
    {
        this.billerSupportBillValidation = billerSupportBillValidation;
    }

    public String getBillerPaymentExactness ()
    {
        return billerPaymentExactness;
    }

    public void setBillerPaymentExactness (String billerPaymentExactness)
    {
        this.billerPaymentExactness = billerPaymentExactness;
    }

    public String getBillerFetchRequiremet ()
    {
        return billerFetchRequiremet;
    }

    public void setBillerFetchRequiremet (String billerFetchRequiremet)
    {
        this.billerFetchRequiremet = billerFetchRequiremet;
    }

    public String getBillerName ()
    {
        return billerName;
    }

    public void setBillerName (String billerName)
    {
        this.billerName = billerName;
    }

    public String getBillerAdhoc ()
    {
        return billerAdhoc;
    }

    public void setBillerAdhoc (String billerAdhoc)
    {
        this.billerAdhoc = billerAdhoc;
    }

    public String getBillerCategory ()
    {
        return billerCategory;
    }

    public void setBillerCategory (String billerCategory)
    {
        this.billerCategory = billerCategory;
    }

    public BillerInputParams getBillerInputParams ()
    {
        return billerInputParams;
    }

    public void setBillerInputParams (BillerInputParams billerInputParams)
    {
        this.billerInputParams = billerInputParams;
    }

    public String getBillerAmountOptions ()
    {
        return billerAmountOptions;
    }

    public void setBillerAmountOptions (String billerAmountOptions)
    {
        this.billerAmountOptions = billerAmountOptions;
    }

    public String getBillerPaymentModes ()
    {
        return billerPaymentModes;
    }

    public void setBillerPaymentModes (String billerPaymentModes)
    {
        this.billerPaymentModes = billerPaymentModes;
    }

    public String getBillerCoverage ()
    {
        return billerCoverage;
    }

    public void setBillerCoverage (String billerCoverage)
    {
        this.billerCoverage = billerCoverage;
    }

    public String getRechargeAmountInValidationRequest ()
    {
        return rechargeAmountInValidationRequest;
    }

    public void setRechargeAmountInValidationRequest (String rechargeAmountInValidationRequest)
    {
        this.rechargeAmountInValidationRequest = rechargeAmountInValidationRequest;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [billerId = "+billerId+", billerDescription = "+billerDescription+", billerSupportBillValidation = "+billerSupportBillValidation+", billerPaymentExactness = "+billerPaymentExactness+", billerFetchRequiremet = "+billerFetchRequiremet+", billerName = "+billerName+", billerAdhoc = "+billerAdhoc+", billerCategory = "+billerCategory+", billerInputParams = "+billerInputParams+", billerAmountOptions = "+billerAmountOptions+", billerPaymentModes = "+billerPaymentModes+", billerCoverage = "+billerCoverage+", rechargeAmountInValidationRequest = "+rechargeAmountInValidationRequest+"]";
    }
}
