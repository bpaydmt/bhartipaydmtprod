package com.bhartipay.recharge.response;

public class BillerInputParams
{
    private ParamInfo[] paramInfo;

    public ParamInfo[] getParamInfo ()
    {
        return paramInfo;
    }

    public void setParamInfo (ParamInfo[] paramInfo)
    {
        this.paramInfo = paramInfo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [paramInfo = "+paramInfo+"]";
    }
}
			
	
