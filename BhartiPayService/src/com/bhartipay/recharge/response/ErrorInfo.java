package com.bhartipay.recharge.response;


public class ErrorInfo
{
private Error error;

public Error getError ()
{
return error;
}

public void setError (Error error)
{
this.error = error;
}

@Override
public String toString()
{
return "ClassPojo [error = "+error+"]";
}
}