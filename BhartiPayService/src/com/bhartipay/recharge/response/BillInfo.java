package com.bhartipay.recharge.response;

public class BillInfo {
	private BillerInfoResponse billerInfoResponse;

    public BillerInfoResponse getBillerInfoResponse ()
    {
        return billerInfoResponse;
    }

    public void setBillerInfoResponse (BillerInfoResponse billerInfoResponse)
    {
        this.billerInfoResponse = billerInfoResponse;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [billerInfoResponse = "+billerInfoResponse+"]";
    }
}
