package com.bhartipay.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="billerInfoResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillPaymentResponse {
	@XmlElement(name="ExtBillPayResponse")
	private ExtBillPayResponse ExtBillPayResponse;

    public ExtBillPayResponse getExtBillPayResponse ()
    {
        return ExtBillPayResponse;
    }

    public void setExtBillPayResponse (ExtBillPayResponse ExtBillPayResponse)
    {
        this.ExtBillPayResponse = ExtBillPayResponse;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ExtBillPayResponse = "+ExtBillPayResponse+"]";
    }
}
