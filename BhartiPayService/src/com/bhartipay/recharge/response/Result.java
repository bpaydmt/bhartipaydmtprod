package com.bhartipay.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name="Result")
//@XmlAccessorType(XmlAccessType.FIELD)
public class Result {

	//@XmlElement(name = "reqid")
	private String reqid;

	//@XmlElement(name = "status")
    private String status;

	//@XmlElement(name = "remark")
    private String remark;

	//@XmlElement(name = "mn")
    private String mn;
    
	//@XmlElement(name = "amt")
    private String amt;
    
	//@XmlElement(name = "balance")
    private String balance;

	//@XmlElement(name = "field1")
    private String field1;
    
	//@XmlElement(name = "ec")
    private String ec;

	//@XmlElement(name = "apirefid")
    private String apirefid;

	public String getReqid() {
		return reqid;
	}

	public void setReqid(String reqid) {
		this.reqid = reqid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMn() {
		return mn;
	}

	public void setMn(String mn) {
		this.mn = mn;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getEc() {
		return ec;
	}

	public void setEc(String ec) {
		this.ec = ec;
	}

	public String getApirefid() {
		return apirefid;
	}

	public void setApirefid(String apirefid) {
		this.apirefid = apirefid;
	}
    
	
	
	
}
