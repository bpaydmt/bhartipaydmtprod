package com.bhartipay.recharge.request;

public class AmountInfo
{
    private String amount;

    private String amountTags;

    private String custConvFee;

    private String currency;

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getAmountTags ()
    {
        return amountTags;
    }

    public void setAmountTags (String amountTags)
    {
        this.amountTags = amountTags;
    }

    public String getCustConvFee ()
    {
        return custConvFee;
    }

    public void setCustConvFee (String custConvFee)
    {
        this.custConvFee = custConvFee;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", amountTags = "+amountTags+", custConvFee = "+custConvFee+", currency = "+currency+"]";
    }
}
		