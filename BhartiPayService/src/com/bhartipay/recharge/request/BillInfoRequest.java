package com.bhartipay.recharge.request;

public class BillInfoRequest {
	 private BillerInfoRequest billerInfoRequest;

	    public BillerInfoRequest getBillerInfoRequest ()
	    {
	        return billerInfoRequest;
	    }

	    public void setBillerInfoRequest (BillerInfoRequest billerInfoRequest)
	    {
	        this.billerInfoRequest = billerInfoRequest;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [billerInfoRequest = "+billerInfoRequest+"]";
	    }
}
