package com.bhartipay.recharge.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BillerInfoRequest
{
    private String billerId;

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [billerId = "+billerId+"]";
    }
}
			
		