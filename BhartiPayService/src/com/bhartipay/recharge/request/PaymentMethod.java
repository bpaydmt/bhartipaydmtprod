package com.bhartipay.recharge.request;

public class PaymentMethod
{
    private String quickPay;

    private String splitPay;

    private String paymentMode;

    public String getQuickPay ()
    {
        return quickPay;
    }

    public void setQuickPay (String quickPay)
    {
        this.quickPay = quickPay;
    }

    public String getSplitPay ()
    {
        return splitPay;
    }

    public void setSplitPay (String splitPay)
    {
        this.splitPay = splitPay;
    }

    public String getPaymentMode ()
    {
        return paymentMode;
    }

    public void setPaymentMode (String paymentMode)
    {
        this.paymentMode = paymentMode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [quickPay = "+quickPay+", splitPay = "+splitPay+", paymentMode = "+paymentMode+"]";
    }
}