package com.bhartipay.matchmove.mmpay.api;

public interface ConnectionSession {
	
	public boolean set (String key, String value);
	
	public boolean set (String key, String value, long expires);
	
	public String get (String key);

}
