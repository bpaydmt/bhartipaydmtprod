package com.bhartipay.extremmitance.bean;

public class ExtRemittanceRequestBean {
	private String aggregatorID;
	private String request;
	private String txn_mode;
	public String getAggregatorID() {
		return aggregatorID;
	}
	public void setAggregatorID(String aggregatorID) {
		this.aggregatorID = aggregatorID;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getTxn_mode() {
		return txn_mode;
	}
	public void setTxn_mode(String txn_mode) {
		this.txn_mode = txn_mode;
	}
	
	

}
