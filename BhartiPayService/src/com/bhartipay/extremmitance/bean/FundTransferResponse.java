package com.bhartipay.extremmitance.bean;

import java.util.ArrayList;
import java.util.List;

import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;

public class FundTransferResponse 
{
	private String senderMobile;
	private String beneficiaryName;
	private String beneficiaryIFSC;
	private String beneficiaryAccNo;
	private double amount;
	private String charges;
	private String mode;
	private List<MudraMoneyTransactionBean> mudraMoneyTransactionBean=new ArrayList<MudraMoneyTransactionBean>();
	private String statusCode;
	private String statusDesc;
	private double agentWalletAmount;
	private double senderLimit;
	private String verified;
	private String accHolderName;
	private String verificationDesc;
	private String beneficiaryId;
	private String bankRRN;
	
	public String getBankRRN() {
		return bankRRN;
	}
	public void setBankRRN(String bankRRN) {
		this.bankRRN = bankRRN;
	}
	public String getAccHolderName() {
		return accHolderName;
	}
	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}
	public String getVerificationDesc() {
		return verificationDesc;
	}
	public void setVerificationDesc(String verificationDesc) {
		this.verificationDesc = verificationDesc;
	}
	public String getBeneficiaryId() {
		return beneficiaryId;
	}
	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}
	public String getVerified() {
		return verified;
	}
	public void setVerified(String verified) {
		this.verified = verified;
	}
	public double getSenderLimit() {
		return senderLimit;
	}
	public void setSenderLimit(double senderLimit) {
		this.senderLimit = senderLimit;
	}
	public double getAgentWalletAmount() {
		return agentWalletAmount;
	}
	public void setAgentWalletAmount(double agentWalletAmount) {
		this.agentWalletAmount = agentWalletAmount;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryIFSC() {
		return beneficiaryIFSC;
	}
	public void setBeneficiaryIFSC(String beneficiaryIFSC) {
		this.beneficiaryIFSC = beneficiaryIFSC;
	}
	public String getBeneficiaryAccNo() {
		return beneficiaryAccNo;
	}
	public void setBeneficiaryAccNo(String beneficiaryAccNo) {
		this.beneficiaryAccNo = beneficiaryAccNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public List<MudraMoneyTransactionBean> getMudraMoneyTransactionBean() {
		return mudraMoneyTransactionBean;
	}
	public void setMudraMoneyTransactionBean(List<MudraMoneyTransactionBean> mudraMoneyTransactionBean) {
		this.mudraMoneyTransactionBean = mudraMoneyTransactionBean;
	}
}
