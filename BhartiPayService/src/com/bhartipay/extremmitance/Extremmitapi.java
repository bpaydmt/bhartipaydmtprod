
	package com.bhartipay.extremmitance;
	

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.extremmitance.bean.ExtRemittanceRequestBean;
import com.bhartipay.extremmitance.bean.ExtRemittanceResponseBean;
import com.bhartipay.extremmitance.bean.FundTransferResponse;
import com.bhartipay.extremmitance.persistence.ExtremmitapiDao;
import com.bhartipay.extremmitance.persistence.ExtremmitapiDaoImpl;
import com.bhartipay.merchant.persistence.MerchantDaoImpl;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.FundTransferBean;
import com.bhartipay.mudra.bean.MerchantDmtTransBean;
import com.bhartipay.mudra.bean.MerchantRequestBean;
import com.bhartipay.mudra.bean.MerchantResponseBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.SenderLedgerBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.EncryptionByEnc256;
import com.bhartipay.util.BPJWTSignUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import antlr.CommonAST;
import appnit.com.crypto.RSAEncryptionWithAES;
import io.jsonwebtoken.Claims;
import net.pg.appnit.utility.AES128Bit;

@Path("/extremmitapi")
public class Extremmitapi {
		

		private static final Logger logger = Logger.getLogger(Extremmitapi.class.getName());
		Gson gson = new Gson();
		EncryptionByEnc256 encrypt256 = new EncryptionByEnc256();
		BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
		MudraDao mudraDaoWallet=new MudraDaoImpl();
		MudraDaoBank mudraDao=new MudraDaoBankImpl();
		ExtremmitapiDao remmiDao = new ExtremmitapiDaoImpl();
		TransactionDao transactionDao = new TransactionDaoImpl();
		WalletUserDao walletUserDao = new WalletUserDaoImpl();
		HashMap<String,Object> responsemap= new HashMap<String,Object>();
		private static String mKey="e391ab57590132714ad32da9acf3013eb88c";
		
	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/checkWalletExistRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean checkWalletExistRequest(ExtRemittanceRequestBean remittanceRequestBean,@Context HttpServletRequest request){
		ExtRemittanceResponseBean ResponseBean=new ExtRemittanceResponseBean();
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		MudraSenderBank mudraSenderbank=new MudraSenderBank();
		MudraSenderWallet mudraSenderWallet=new MudraSenderWallet();
		String statusCode,statusdesc;
		
		String requestId=remmiDao.saveRemitApiRequest(remittanceRequestBean.getAggregatorID(),remittanceRequestBean.getRequest(),serverName,"checkWalletExistRequest");
		
		logger.info(serverName +"*****RequestId******"+requestId+"**************************************Merchantid*****"+remittanceRequestBean.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+remittanceRequestBean.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		
		ResponseBean.setRequestId(requestId);
		
		if(remittanceRequestBean.getAggregatorID()==null ||remittanceRequestBean.getAggregatorID().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid AggregatorID.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		if(remittanceRequestBean.getTxn_mode()==null ||remittanceRequestBean.getTxn_mode().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		if(remittanceRequestBean.getRequest()==null ||remittanceRequestBean.getRequest().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(remittanceRequestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"**************************************mKey*****"+mKey);
		
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid AggregatorId");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		 try{
		 Claims claim=oxyJWTSignUtil.parseToken(remittanceRequestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				//ResponseBean.setRequestId("");
				ResponseBean.setResponse(gson.toJson(responsemap));
				return ResponseBean;
			}
		 
		 String mobileNo=claim.get("mobileNo").toString();
		 
		 if(!mobileValidation(mobileNo)){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "INVALID MOBILE NUMBER.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean;
		 }
		 if(remittanceRequestBean.getTxn_mode().equals("W"))
		 {
			 mudraSenderWallet.setMobileNo(mobileNo);
			 mudraSenderWallet.setAggreatorId(remittanceRequestBean.getAggregatorID());
			 mudraSenderWallet=remmiDao.getSenderDtlWallet(mudraSenderWallet,serverName,requestId);
			 responsemap.put("mobileNo", mudraSenderWallet.getMobileNo());
			 statusCode=mudraSenderWallet.getStatusCode();
			 statusdesc=mudraSenderWallet.getStatusDesc();
		 }
		 else
		 {
			mudraSenderbank.setMobileNo(mobileNo);
			mudraSenderbank.setAggreatorId(remittanceRequestBean.getAggregatorID());
			mudraSenderbank=remmiDao.getSenderDtlbank(mudraSenderbank,serverName,requestId);
			responsemap.put("mobileNo", mudraSenderbank.getMobileNo());
			statusCode=mudraSenderbank.getStatusCode();
			statusdesc=mudraSenderbank.getStatusDesc();
		 }
		 logger.info(serverName+"*****RequestId******"+requestId+"************************************StatusCode*****"+statusCode);
		 
		 if(statusCode.equalsIgnoreCase("1001")||statusCode.equalsIgnoreCase("7000")){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Please try after sometime.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			//ResponseBean.setRequestId("");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return ResponseBean;
		 }
		 if(statusCode.equalsIgnoreCase("1104")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Mobile Number.");
				//ResponseBean.setRequestId("");
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 if(statusCode.equalsIgnoreCase("1000")){
			 	responsemap.put("resp_code", "300");
				responsemap.put("userexists",statusdesc);
				if(statusdesc.equalsIgnoreCase("Y"))
				{
					responsemap.put("message", "SENDER EXISTS.");
				}
				else
				{
					responsemap.put("message", "SENDER DOES NOT EXISTS.");
				}
				responsemap.put("response", "SUCCESS.");
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		
		 
		 
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//ResponseBean.setRequestId("");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return ResponseBean;
		}
		 
		 
		return ResponseBean;
	}

	@POST
	@Path("/createWalletRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean createWalletRequest(ExtRemittanceRequestBean remitRequest,@Context HttpServletRequest request){
		ExtRemittanceResponseBean ResponseBean=new ExtRemittanceResponseBean();
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
		MudraSenderBank mudraSenderMastBank=new MudraSenderBank();
		String responseStatus;
		String senderId;
		
		String requestId=remmiDao.saveRemitApiRequest(remitRequest.getAggregatorID(),remitRequest.getRequest(),serverName,"createWalletRequest");
		
		logger.info(serverName +"*****RequestId******"+requestId+"**************************************Aggregator*****"+remitRequest.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+remitRequest.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		
		ResponseBean.setRequestId(requestId);
		
		if(remitRequest.getAggregatorID()==null ||remitRequest.getAggregatorID().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		if(remitRequest.getTxn_mode()==null ||remitRequest.getTxn_mode().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		if(remitRequest.getRequest()==null ||remitRequest.getRequest().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(remitRequest.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"**************************************mKey*****"+mKey);
		
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Merchant. ");
			//ResponseBean.setRequestId(requestId);
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		 try{
		 Claims claim=oxyJWTSignUtil.parseToken(remitRequest.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				//ResponseBean.setRequestId("");
				ResponseBean.setResponse(gson.toJson(responsemap));
				return ResponseBean;
			}
		 
		 String mobileNo=claim.get("mobileNo").toString();
		 
		 if(!mobileValidation(mobileNo)){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "INVALID MOBILE NUMBER.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean;
		 }
		 if(remitRequest.getTxn_mode().equals("W"))
		 {
			 mudraSenderMastBean.setMobileNo(mobileNo);
			 mudraSenderMastBean.setAggreatorId(remitRequest.getAggregatorID());
			 mudraSenderMastBean=mudraDaoWallet.validateSender(mudraSenderMastBean,serverName,requestId);
			 responseStatus = mudraSenderMastBean.getStatusCode();
			 senderId = mudraSenderMastBean.getId();
		 }
		 else
		 {
			 mudraSenderMastBank.setMobileNo(mobileNo);
		 	 mudraSenderMastBank.setAggreatorId(remitRequest.getAggregatorID());
		 	 mudraSenderMastBank=mudraDao.validateSender(mudraSenderMastBank,serverName,requestId);
		 	 responseStatus = mudraSenderMastBank.getStatusCode();
		 	senderId=mudraSenderMastBank.getId();
		 }
		 
		 logger.info(serverName+"*****RequestId******"+requestId+"************************************StatusCode*****"+responseStatus);
		 if(responseStatus.equalsIgnoreCase("1001")||responseStatus.equalsIgnoreCase("7000")){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Aggregator ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			//ResponseBean.setRequestId("");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return ResponseBean;
		 }
		 responsemap.put("mobileno", mobileNo);
		 if(responseStatus.equalsIgnoreCase("1104")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Mobile Number.");
				//ResponseBean.setRequestId("");
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 if(responseStatus.equalsIgnoreCase("2000")){
				responsemap.put("resp_code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("userexists", "N");
				responsemap.put("message", "OTP has been sent on Mobile Number.");
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 if(responseStatus.equalsIgnoreCase("1000")){
				responsemap.put("resp_code", "300");
				responsemap.put("message", "Sender already exists.");
				responsemap.put("userexists", "Y");
				responsemap.put("response", "SUCCESS.");
				responsemap.put("senderId", senderId);
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		
		 
		 
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//ResponseBean.setRequestId("");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return ResponseBean;
		}
		 
		 
		return ResponseBean;
	}

	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/createWalletConfirm")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean createWalletConfirm(ExtRemittanceRequestBean remitRequest,@Context HttpServletRequest request){
		MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
		MudraSenderBank mudraSenderMastbank=new MudraSenderBank();
		String responseStatus;
		String senderId;
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=remmiDao.saveRemitApiRequest(remitRequest.getAggregatorID(),remitRequest.getRequest(),serverName,"createWalletConfirm");
		logger.info(serverName +"****RequestId*****"+requestId+"**************************************Merchantid*****"+remitRequest.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"**************************************Request*****"+remitRequest.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean ResponseBean=new ExtRemittanceResponseBean();
		ResponseBean.setRequestId(requestId);
		
		
		if(remitRequest.getAggregatorID()==null ||remitRequest.getAggregatorID().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		if(remitRequest.getTxn_mode()==null ||remitRequest.getTxn_mode().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		if(remitRequest.getRequest()==null ||remitRequest.getRequest().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(remitRequest.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"**************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(remitRequest.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				ResponseBean.setResponse(gson.toJson(responsemap));
				return ResponseBean;
			}
		 
		 
		 String mobileNo=claim.get("mobileNo").toString();
		 if(!mobileValidation(mobileNo)){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "INVALID MOBILE NUMBER.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean;
		 }
		 
		 if(!nameValidation(claim.get("name").toString())){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Only alphabets are allowed in name.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean; 
		 }
		 
		 if(!mpinValidation(claim.get("mpin").toString())){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean; 
		 }
		 if(remitRequest.getTxn_mode().equalsIgnoreCase("W"))
		{
			 if(claim.get("ovd_name") ==null || claim.get("ovd_name").toString().isEmpty()){
					responsemap.put("resp_code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Document Type.");
					oxyJWTSignUtil.generateToken(responsemap, mKey);
					 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
					 return ResponseBean;
				}
			 if(claim.get("ovd_number")==null ||claim.get("ovd_number").toString().isEmpty()){
					responsemap.put("resp_code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Document ID.");
					oxyJWTSignUtil.generateToken(responsemap, mKey);
					 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
					 return ResponseBean;
				}
			 mudraSenderMastBean.setMobileNo(mobileNo);
			 mudraSenderMastBean.setAggreatorId(remitRequest.getAggregatorID());
			 mudraSenderMastBean.setAgentId(remitRequest.getAggregatorID());
			 mudraSenderMastBean.setFirstName(claim.get("name").toString());
			 mudraSenderMastBean.setMpin(claim.get("mpin").toString());
			 mudraSenderMastBean.setOtp(claim.get("otp").toString());
			 mudraSenderMastBean=mudraDaoWallet.registerSender(mudraSenderMastBean,serverName,requestId);
			 responseStatus = mudraSenderMastBean.getStatusCode();
			 senderId=mudraSenderMastBean.getId();
		}
		else
		{

			 mudraSenderMastbank.setMobileNo(mobileNo);
			 mudraSenderMastbank.setAggreatorId(remitRequest.getAggregatorID());
			 mudraSenderMastbank.setAgentId(remitRequest.getAggregatorID());
			 mudraSenderMastbank.setFirstName(claim.get("name").toString());
			 mudraSenderMastbank.setMpin(claim.get("mpin").toString());
			 mudraSenderMastbank.setOtp(claim.get("otp").toString());
			 mudraSenderMastbank=mudraDao.registerSender(mudraSenderMastbank,serverName,requestId);
			 responseStatus = mudraSenderMastbank.getStatusCode();
			 senderId=mudraSenderMastbank.getId();
		}
		 
		 responsemap.put("mobileno", mobileNo);
		 logger.info(serverName +"*****RequestId******"+requestId+"**********************************StatusCode*****"+responseStatus);
		 if(responseStatus.equalsIgnoreCase("1001")||responseStatus.equalsIgnoreCase("7000")){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return ResponseBean;
		 }
		 if(responseStatus.equalsIgnoreCase("1104")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Mobile Number.");
				
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 
		 if(responseStatus.equalsIgnoreCase("1101")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Name can't be empty.");
				
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 
		 if(responseStatus.equalsIgnoreCase("1102")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid MPIN or MPIN empty.");
				
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 
		 if(responseStatus.equalsIgnoreCase("1103")||responseStatus.equalsIgnoreCase("1105")||responseStatus.equalsIgnoreCase("1106")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid OTP.");
				
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 
		 
		 
		 if(responseStatus.equalsIgnoreCase("11206")){
				responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Remitter (Sender) Mobile Number already registered with us.");
				responsemap.put("userexists", "Y");//senderId
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }
		 
		 
		 
		 
		 if(responseStatus.equalsIgnoreCase("1000")){
				responsemap.put("resp_code", "300");
				responsemap.put("message", "Sender has been registered successfully.");
				responsemap.put("userexists", "Y");
				responsemap.put("response", "SUCCESS.");
				responsemap.put("senderId", senderId);	
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean;
		 }else{
			 responsemap.put("resp_code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "INVALID MESSAGE STRING.");
				
				ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return ResponseBean; 
		 }
		
	}catch (Exception e) {
		logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//ResponseBean.setRequestId("");
		ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return ResponseBean;
	}
		 
		
	}

	/**
	* 	
	* @param merchantRequestBean
	* @param request
	* @return
	*/
	@POST
	@Path("/getUserDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getUserDetails(ExtRemittanceRequestBean remitRequest,@Context HttpServletRequest request){
	ExtRemittanceResponseBean ResponseBean=new ExtRemittanceResponseBean();
	String serverName=request.getServletContext().getInitParameter("serverName");
	MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	MudraSenderBank mudraSenderMastBank=new MudraSenderBank();
	String responseStatus,senderId,statusDesc;
	double remainLimit,balance;
	List beneList = null;

	//String requestId=remmiDao.saveRemitApiRequest(remitRequest.getAggregatorID(),remitRequest.getRequest(),serverName,"getUserDetails");
	String requestId=remmiDao.saveRemitApiRequest(remitRequest.getAggregatorID(),remitRequest.getRequest(),serverName,"getUserDetails");
	logger.info(serverName +"*******RequestId*********"+requestId+"**************************************Aggregator*****"+remitRequest.getAggregatorID());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+remitRequest.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	ResponseBean.setRequestId(requestId);
///change
	if(remitRequest.getAggregatorID()==null ||remitRequest.getAggregatorID().isEmpty()){
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return ResponseBean;
	}
	if(remitRequest.getTxn_mode()==null ||remitRequest.getTxn_mode().isEmpty()){
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Transaction Mode.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return ResponseBean;
	}
	if(remitRequest.getRequest()==null ||remitRequest.getRequest().isEmpty()){
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		ResponseBean.setResponse(gson.toJson(responsemap));
		return ResponseBean;		
	}
	//String mKey=new MerchantDaoImpl().getKeyByMerchantid(remitRequest.getAggregatorID());
	logger.info(serverName+"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Merchant. ");
		ResponseBean.setResponse(gson.toJson(responsemap));
		return ResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(remitRequest.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
	 
	 String mobileNo=claim.get("mobileNo").toString();
	 if(!mobileValidation(mobileNo)){
		 responsemap.put("resp_code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "INVALID MOBILE NUMBER.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return ResponseBean;
	 }
	 if(remitRequest.getTxn_mode().equals("W"))
	 {
		 mudraSenderMastBean.setMobileNo(mobileNo);
		 mudraSenderMastBean.setAggreatorId(remitRequest.getAggregatorID());
		 mudraSenderMastBean=remmiDao.getSenderDtlWallet(mudraSenderMastBean,serverName,requestId);
		 responseStatus = mudraSenderMastBean.getStatusCode();
		 senderId = mudraSenderMastBean.getId();
		 statusDesc= mudraSenderMastBean.getStatusDesc();
		 beneList = remmiDao.getBeneficiaryListWallet(mudraSenderMastBean.getBeneficiaryList());
		 balance = mudraSenderMastBean.getWalletBalance();
		 remainLimit=mudraSenderMastBean.getTransferLimit();
	 }
	 else
	 {
		 mudraSenderMastBank.setMobileNo(mobileNo);
	 	 mudraSenderMastBank.setAggreatorId(remitRequest.getAggregatorID());
	 	 mudraSenderMastBank=remmiDao.getSenderDtlbank(mudraSenderMastBank,serverName,requestId);
	 	 responseStatus = mudraSenderMastBank.getStatusCode();
	 	 senderId=mudraSenderMastBank.getId();
	 	 statusDesc= mudraSenderMastBank.getStatusDesc();
	 	 beneList = remmiDao.getBeneficiaryListBank(mudraSenderMastBank.getBeneficiaryList());
	 	 balance = mudraSenderMastBank.getWalletBalance();
		 remainLimit=mudraSenderMastBank.getTransferLimit();
	 }
	 
	 logger.info(serverName+"*****RequestId******"+requestId+"************************************StatusCode*****"+responseStatus);
	 if(responseStatus.equalsIgnoreCase("1001")||responseStatus.equalsIgnoreCase("7000")){
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Aggregator ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return ResponseBean;
	 }
	 responsemap.put("mobileno", mobileNo);
	 if(responseStatus.equalsIgnoreCase("1104")){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Mobile Number.");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return ResponseBean;
	 }
	 
	 if(responseStatus.equalsIgnoreCase("1000")){
		 if(statusDesc.equalsIgnoreCase("Y"))
		 {
			 HashMap<String,Object> userdtl = new HashMap<String,Object>();
			 userdtl.put("mobileno", mobileNo);
			 userdtl.put("Balance", balance);
			 userdtl.put("remitlimitavailable", remainLimit);
			 responsemap.put("userdetails", userdtl);
			 responsemap.put("userexits", statusDesc);
			 responsemap.put("beneficiarylist", beneList);
			 responsemap.put("resp_code", "300");
			 responsemap.put("message", "Sender exists.");
			 responsemap.put("senderId", senderId);
		 }
		 else{
			responsemap.put("resp_code", "1");
			responsemap.put("message", "Sender does not exists.");
			responsemap.put("userexists", statusDesc);
			responsemap.put("response", "FAILED.");
		 }
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return ResponseBean;
	 }
	
	}catch (Exception e) {
		logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return ResponseBean;
	}
	return ResponseBean;
	}





	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/resendOtp")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean resendOtp(ExtRemittanceRequestBean remitRequest,@Context HttpServletRequest request){
		ExtRemittanceResponseBean ResponseBean=new ExtRemittanceResponseBean();
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=remmiDao.saveRemitApiRequest(remitRequest.getAggregatorID(),remitRequest.getRequest(),serverName,"resendOtp");
		logger.info(serverName +"******RequestId******"+requestId+"**************************************Merchantid*****"+remitRequest.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"**************************************Request*****"+remitRequest.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ResponseBean.setRequestId(requestId);

		
		if(remitRequest.getAggregatorID()==null ||remitRequest.getAggregatorID().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		if(remitRequest.getRequest()==null ||remitRequest.getRequest().isEmpty()){
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;		
		}
		
		
	//	String mKey=new MerchantDaoImpl().getKeyByMerchantid(remitRequest.getAggregatorID());
		 logger.info(serverName+"*****RequestId******"+requestId+"**********************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			ResponseBean.setResponse(gson.toJson(responsemap));
			return ResponseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(remitRequest.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				ResponseBean.setResponse(gson.toJson(responsemap));
				return ResponseBean;
			}
		 String mobileNo=claim.get("mobileNo").toString();
		 
		 if(!mobileValidation(mobileNo)){
			 responsemap.put("resp_code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "INVALID MOBILE NUMBER.");
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return ResponseBean;
		 }
		 Boolean flag=mudraDao.otpSenderResend(mobileNo,remitRequest.getAggregatorID(),serverName,requestId);
		 
		if(flag){
			responsemap.put("resp_code", "300");
			responsemap.put("response", "SUCCESS");
			responsemap.put("message", "OTP has been sent to sender mobile number.");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));;
			return ResponseBean;
		}else{
			responsemap.put("resp_code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "FAILED.");
			ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return ResponseBean;
		}
	}catch (Exception e) {
		logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("resp_code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		ResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return ResponseBean;
	}
		
	}


	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/addBeneficiaryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean addBeneficiaryRequest(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"addBeneficiaryRequest");

		logger.info(serverName +"****RequestId***"+requestId+"**************************************Merchantid*****"+requestBean.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+requestBean.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		MudraBeneficiaryWallet mudraBeneficiaryWallet=new MudraBeneficiaryWallet();
		MudraBeneficiaryBank mudraBeneficiaryBank = new MudraBeneficiaryBank();
		String respDesc;
		responseBean.setRequestId(requestId);
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		///String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		 
		 
		 //added for defect id 6.1
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("senderid") == null || claim.get("senderid").toString().trim().equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;			 
		 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderid").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderid").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderid").toString()).trim();
		 }
		if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 }
		 
		 	if (claim.get("name").toString() == null || claim.get("name").toString().isEmpty() || !nameValidation(claim.get("name").toString())) {
		 		 responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid Name.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				 return responseBean;
			   }

			   if (claim.get("bankName").toString() == null || claim.get("bankName").toString().isEmpty() || !nameValidation(claim.get("bankName").toString())) {
				   responsemap.put("code", "1");
					 responsemap.put("response", "ERROR");
					 responsemap.put("message", "Invalid Bank Name.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					 return responseBean;
			   }
			   if (claim.get("accountNo").toString() == null ||claim.get("accountNo").toString().isEmpty() || !accountValidation(claim.get("accountNo").toString())) {
				   responsemap.put("code", "1");
					 responsemap.put("response", "ERROR");
					 responsemap.put("message", "Invalid Account Number.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					 return responseBean;
				}

				if (claim.get("ifsccode").toString() == null || claim.get("ifsccode").toString().isEmpty() || !iFSCValidation(claim.get("ifsccode").toString())) {
					responsemap.put("code", "1");
					 responsemap.put("response", "ERROR");
					 responsemap.put("message", "Invalid IFSC Code.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					 return responseBean;
				}

				if (claim.get("transferType").toString() == null || claim.get("transferType").toString().isEmpty() || !(claim.get("transferType").toString().trim().equalsIgnoreCase("IMPS") || claim.get("transferType").toString().trim().equalsIgnoreCase("NEFT"))) {
					responsemap.put("code", "1");
					 responsemap.put("response", "ERROR");
					 responsemap.put("message", "Invalid Transfer Type.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					 return responseBean;
				}
				
				if (claim.get("mpin").toString() == null || claim.get("mpin").toString().isEmpty() || !mpinValidation(claim.get("mpin").toString())) {
					responsemap.put("code", "1");
					 responsemap.put("response", "ERROR");
					 responsemap.put("message", "Invalid MPIN.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					 return responseBean;
				}
		if(requestBean.getTxn_mode().equalsIgnoreCase("W"))
		{
			 mudraBeneficiaryWallet.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryWallet.setSenderId(claim.get("senderId").toString());
			 mudraBeneficiaryWallet.setName(claim.get("name").toString());
			 mudraBeneficiaryWallet.setBankName(claim.get("bankName").toString());
			 mudraBeneficiaryWallet.setAccountNo(claim.get("accountNo").toString());
			 mudraBeneficiaryWallet.setIfscCode(claim.get("ifscCode").toString());
			 mudraBeneficiaryWallet.setTransferType(claim.get("transferType").toString());
			 mudraBeneficiaryWallet.setMpin(claim.get("mpin").toString());	 
			 mudraBeneficiaryWallet=mudraDaoWallet.registerBeneficiary(mudraBeneficiaryWallet,serverName,requestId);
			 respDesc =mudraBeneficiaryWallet.getStatusCode();
		}
		else
		{
			 mudraBeneficiaryBank.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryBank.setSenderId(claim.get("senderid").toString());
			 mudraBeneficiaryBank.setName(claim.get("name").toString());
			 mudraBeneficiaryBank.setBankName(claim.get("bankName").toString());
			 mudraBeneficiaryBank.setAccountNo(claim.get("accountNo").toString());
			 mudraBeneficiaryBank.setIfscCode(claim.get("ifsccode").toString());
			 mudraBeneficiaryBank.setTransferType(claim.get("transferType").toString());
			 mudraBeneficiaryBank.setMpin(claim.get("mpin").toString());	 
			 mudraBeneficiaryBank=mudraDao.registerBeneficiary(mudraBeneficiaryBank,serverName,requestId);
			 respDesc=mudraBeneficiaryBank.getStatusCode();
		}
		
		 
		 logger.info(serverName+"*****RequestId******"+requestId+"********************************StatusCode*****"+respDesc);
		 switch(respDesc)
		 {
		 case "1001"  :
		 case "7000":
		    {
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid wallet Agent ID.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
			
		 case "1110" :
		    {
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Sender Id.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 //added for defect id 6.4
		 case "1101" :
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Beneficiary name.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			//added for defect id 6.4
		 case "1107" :
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid bank name.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1108" :
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid account number.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1109" :
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid IFSC code.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1111":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Transfer Type.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1102":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1124":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Provided account number is already added with sender.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1123" :
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1000":
		 {
				 responsemap.put("code", "300");
				 //added on 2nd Aug
					responsemap.put("response", "SUCCESS");
					responsemap.put("message", "Beneficiary has been registered successfully.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					//responseBean.setResponse(gson.toJson(responsemap));
					return responseBean;
			 }
		}
	}catch (Exception e) {
		logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		System.out.println(e.getMessage());
		e.printStackTrace();
		//responseBean.setRequestId("");
		responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return responseBean;
	}
		 return responseBean; 
		 
	}

	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/deactiveBeneficiaryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean deactiveBeneficiaryRequest(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		 MudraBeneficiaryWallet mudraBeneficiaryWallet=new MudraBeneficiaryWallet();
		 MudraSenderWallet mudraSenderWallet=new MudraSenderWallet();
		 MudraBeneficiaryBank mudraBeneficiaryBank=new MudraBeneficiaryBank();
		 MudraSenderBank mudraSenderBank=new MudraSenderBank();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 String respDesc;
		 List beneList;
		 
		
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"deactiveBeneficiaryRequest");

		logger.info(serverName +"***RequestId***"+requestId+"************Merchantid*****"+requestBean.getAggregatorID());
		logger.info(serverName+"*****RequestId******"+requestId+"********Request*****"+requestBean.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
		
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		 
		// added for defect id 7.1
		 
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("senderid") == null || claim.get("senderid").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid SenderId.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("beneficiaryid") == null || claim.get("beneficiaryid").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid beneficiary Id.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderid").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderid").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderid").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 }
		 if(requestBean.getTxn_mode().equalsIgnoreCase("W"))
		 {
			 mudraBeneficiaryWallet.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryWallet.setSenderId(claim.get("senderId").toString());
			 mudraBeneficiaryWallet.setId(claim.get("beneficiaryId").toString());
			 mudraSenderWallet=mudraDaoWallet.deActiveBeneficiary(mudraBeneficiaryWallet,serverName,requestId);
			 respDesc = mudraSenderWallet.getStatusCode();
			 beneList = remmiDao.getBeneficiaryListWallet(mudraSenderWallet.getBeneficiaryList());
		 }
		 else
		 {
			 mudraBeneficiaryBank.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryBank.setSenderId(claim.get("senderId").toString());
			 mudraBeneficiaryBank.setId(claim.get("beneficiaryId").toString());
			 mudraSenderBank=mudraDao.deActiveBeneficiary(mudraBeneficiaryBank,serverName,requestId); 
			 respDesc = mudraSenderBank.getStatusCode();
			 beneList = remmiDao.getBeneficiaryListBank(mudraSenderBank.getBeneficiaryList());
		 }
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+respDesc);
		 //added for defect id 7.1
		 if(respDesc.equalsIgnoreCase("1130")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(respDesc.equalsIgnoreCase("1001")||respDesc.equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return responseBean;
		 }
		 //changed on 21 july ,2017
		 if(respDesc.equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Provided account number is already added with sender.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		 
		 if(respDesc.equalsIgnoreCase("1110")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		 
		 if(respDesc.equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		 
		 if(respDesc.equalsIgnoreCase("1000")){
				responsemap.put("code", "300");
				responsemap.put("message", "Beneficiary Deactivated SUCCESSFULLY.");
				//added for defect id 7.6
				//responsemap.put("cardExists", "Y");
				responsemap.put("response", "SUCCESS.");
				responsemap.put("beneficiary",beneList);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}
		 return responseBean; 
		 
		 
	}

	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/activeBeneficiaryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean activeBeneficiaryRequest(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		MudraBeneficiaryWallet mudraBeneficiaryWallet=new MudraBeneficiaryWallet();
		MudraSenderWallet mudraSenderWallet=new MudraSenderWallet();
		MudraBeneficiaryBank mudraBeneficiaryBank=new MudraBeneficiaryBank();
		MudraSenderBank mudraSenderBank=new MudraSenderBank();
		String statusDesc;
		List beneList;
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"activeBeneficiaryRequest");

		logger.info(serverName +"********RequestId****"+requestId+"*********AggregatorId*****"+requestBean.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*******************Request*****"+requestBean.getRequest());
		
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
		
		
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		 //added for defect id 8.1
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("senderId") == null || claim.get("senderId").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("beneficiaryId") == null || claim.get("beneficiaryId").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 }
		 if(requestBean.getTxn_mode().equalsIgnoreCase("W"))
		 {
			 mudraBeneficiaryWallet.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryWallet.setSenderId(claim.get("senderId").toString());
			 mudraBeneficiaryWallet.setId(claim.get("beneficiaryId").toString());
			 mudraSenderWallet=mudraDaoWallet.activeBeneficiary(mudraBeneficiaryWallet,serverName,requestId);
			 statusDesc=mudraSenderWallet.getStatusCode();
			 beneList = remmiDao.getBeneficiaryListWallet(mudraSenderWallet.getBeneficiaryList());
		 }
		 else
		 {
			 mudraBeneficiaryBank.setAggreatorId(requestBean.getAggregatorID());
			 mudraBeneficiaryBank.setSenderId(claim.get("senderId").toString());
			 mudraBeneficiaryBank.setId(claim.get("beneficiaryId").toString());
			 mudraSenderBank=mudraDao.activeBeneficiary(mudraBeneficiaryBank,serverName,requestId);	
			 statusDesc=mudraSenderBank.getStatusCode();
			 beneList = remmiDao.getBeneficiaryListBank(mudraSenderBank.getBeneficiaryList());
		 }
		
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+statusDesc);
		 //added for defect id 8.2
		 switch(statusDesc)
		 {
		 case "1130" :
	 		{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 case "1001":
		 case "7000":
		 	{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid wallet Agent ID.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
			 
		 case "1110":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Sender Id.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1116":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Beneficiary Id.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
			 
		 case "1123":
		 	{
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
		 case "1000" :
		 	{
					responsemap.put("code", "300");
					responsemap.put("message", "Beneficiary activated SUCCESSFULLY.");
					//added for defect id 8.6
					//responsemap.put("cardExists", "Y");
					responsemap.put("response", "SUCCESS.");
					responsemap.put("beneficiary", beneList);
					responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
					return responseBean;
			 }
		}
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}
		 return responseBean; 
		 
		 
	}

	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/forgotMPINRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean forgotMPINRequest(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		 MudraSenderBank mudraSenderBank=new MudraSenderBank();
		 MudraSenderWallet mudraSenderWallet = new MudraSenderWallet();
		 String statusDesc;
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"forgotMPINRequest");

		logger.info(serverName +"****RequestId*******"+requestId+"***************Aggregator Id****"+requestBean.getAggregatorID());
		logger.info(serverName+"*****RequestId******"+requestId+"*************************************Request*****"+requestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
			
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		//added for defect id 9.2
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("senderId") == null || claim.get("senderId").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 }
		 //changed on 2nd Aug
		 if(requestBean.getTxn_mode().equalsIgnoreCase("W"))
		 {

			 mudraSenderWallet.setAggreatorId(requestBean.getAggregatorID());
			 mudraSenderWallet.setId(claim.get("senderId").toString());
			 mudraSenderWallet.setMobileNo(claim.get("mobileNo").toString());
			 mudraSenderWallet=mudraDaoWallet.forgotMPIN(mudraSenderWallet,serverName,requestId); 
			 statusDesc=mudraSenderWallet.getStatusCode();
		 }
		 else
		 {
			 mudraSenderBank.setAggreatorId(requestBean.getAggregatorID());
			 mudraSenderBank.setId(claim.get("senderId").toString());
			 mudraSenderBank.setMobileNo(claim.get("mobileNo").toString());
			 mudraSenderBank=mudraDao.forgotMPIN(mudraSenderBank,serverName,requestId);
			 statusDesc=mudraSenderBank.getStatusCode();
		 }
		
		 logger.info(serverName +"*****RequestId******"+requestId+"***********************************StatusCode*****"+statusDesc);
		 if(statusDesc.equalsIgnoreCase("1001")||statusDesc.equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return responseBean;
		 }

		 if(statusDesc.equalsIgnoreCase("1000")){
				responsemap.put("code", "300");
				responsemap.put("message", "OTP has been sent on registered Mobile Number.");
				responsemap.put("response", "SUCCESS.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		} 
		 return responseBean;

	}

	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/updateMPINRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean updateMPINRequest(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		MudraSenderWallet mudraSenderWallet=new MudraSenderWallet();
		MudraSenderBank mudraSenderBank=new MudraSenderBank();
		String statusCode;
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=mudraDao.saveDMTApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"updateMPINRequest");

		logger.info(serverName +"********RequestID****"+requestId+"****************************Aggregator Id*****"+requestBean.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+requestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName+"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		 
		//added for defect id 10.1
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 if(claim.get("senderId") == null || claim.get("senderId").toString().trim().equalsIgnoreCase("")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return responseBean;
		 }
		 //added on 21 July
		 if(claim.get("mpin").toString() == null || claim.get("mpin").toString().isEmpty() || !mpinValidation(claim.get("mpin").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		 //changed on 2nd Aug
		 //mudraSenderMastBean.setAggreatorId(requestBean.getAgentId());
		 if(requestBean.getTxn_mode().equalsIgnoreCase("W"))
		 {
			 mudraSenderWallet.setAggreatorId(requestBean.getAggregatorID());
			 mudraSenderWallet.setId(claim.get("senderId").toString());
			 mudraSenderWallet.setMobileNo(claim.get("mobileNo").toString());
			 mudraSenderWallet.setMpin(claim.get("mpin").toString());
			 mudraSenderWallet.setOtp(claim.get("otp").toString());
			 mudraSenderWallet=mudraDaoWallet.updateMPIN(mudraSenderWallet,serverName,requestId);
			 statusCode = mudraSenderWallet.getStatusCode();
		 }
		 else
		 {
			 mudraSenderBank.setAggreatorId(requestBean.getAggregatorID());
			 mudraSenderBank.setId(claim.get("senderId").toString());
			 mudraSenderBank.setMobileNo(claim.get("mobileNo").toString());
			 mudraSenderWallet.setMpin(claim.get("mpin").toString());
			 mudraSenderBank.setOtp(claim.get("otp").toString());
			 mudraSenderBank=mudraDao.updateMPIN(mudraSenderBank,serverName,requestId);
			 statusCode= mudraSenderBank.getStatusCode();
		 }
		 
		
		 
		 logger.info(serverName+"*****RequestId******"+requestId+"*************************************StatusCode*****"+statusCode);
		 if(statusCode.equalsIgnoreCase("1001")||statusCode.equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return responseBean;
		 }
		 
		 if(statusCode.equalsIgnoreCase("1103")||statusCode.equalsIgnoreCase("1105")||statusCode.equalsIgnoreCase("1106")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid OTP.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		 
		 if(statusCode.equalsIgnoreCase("1102")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }

		 if(statusCode.equalsIgnoreCase("1000")){
				responsemap.put("code", "300");
				responsemap.put("message", "MPIN has been changed successfully.");
				responsemap.put("response", "SUCCESS.");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}
		 return responseBean;
	}





	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @return
	 *//*
	@POST
	@Path("/mrTransferRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean mrTransferRequest(ExtRemittanceRequestBean remRequestBean,@Context HttpServletRequest request){
		MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
		ExtRemittanceResponseBean mrTransferResponse=new ExtRemittanceResponseBean();
		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		String serverName=request.getServletContext().getInitParameter("serverName");
		String operationType ;
		
		String requestId=remmiDao.saveRemitApiRequest(remRequestBean.getAggregatorID(),remRequestBean.getRequest(),serverName,"mrTransferRequest");

		logger.info(serverName +"****RequestId****"+requestId+"**************************************Merchantid*****"+remRequestBean.getAggregatorID());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+remRequestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		mrTransferResponse.setRequestId(requestId);
		
		
		if(remRequestBean.getAggregatorID()==null ||remRequestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(remRequestBean.getTxn_mode()==null ||remRequestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(remRequestBean.getRequest()==null ||remRequestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(remRequestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(remRequestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
			}	 
		 if(claim.get("operationType").toString().trim().isEmpty() || !nameValidation(claim.get("operationType").toString()))
		 {
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary Name is not Valid.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 } 
		 else
		 {
			 operationType=claim.get("operationType").toString();
		 }
		//added for defect id 14.4
		 if(claim.get("beneficiaryName").toString().trim().isEmpty() || !nameValidation(claim.get("beneficiaryName").toString()))
		 {
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary Name is not Valid.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 } 
		 if(claim.get("bankName").toString().trim().isEmpty() || !nameValidation(claim.get("bankName").toString()))
		 {
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Bank Name is not Valid.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 } 

			if(claim.get("accountNo").toString().trim().equalsIgnoreCase("") || !accountValidation(claim.get("accountNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Account Number");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return mrTransferResponse;
			 }
			
			if(claim.get("ifsccode").toString().trim().equalsIgnoreCase("") || !iFSCValidation(claim.get("ifsccode").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid IFSC Code");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return mrTransferResponse;
			 }
			 
		 if(Double.parseDouble(claim.get("amount").toString())>5000){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Your max transaction limit is 5000.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 
		 
		 WalletMastBean walletMast=new WalletUserDaoImpl().showUserProfile(remRequestBean.getAggregatorID());
		 	
		 mudraMoneyTransactionBean.setUserId(remRequestBean.getAggregatorID());
		 mudraMoneyTransactionBean.setSenderId(claim.get("senderid").toString());
		 mudraMoneyTransactionBean.setBeneficiaryId(claim.get("beneficiaryId").toString());
		 
		 if(claim.get("transferType").toString().equalsIgnoreCase("NEFT")||claim.get("transferType").toString().equalsIgnoreCase("IMPS")){
		  mudraMoneyTransactionBean.setNarrartion(claim.get("transferType").toString());
		 }else{
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid transfer Type :"+claim.get("transferType").toString());
			 oxyJWTSignUtil.generateToken(responsemap, mKey);
			 mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			 return mrTransferResponse; 
		 }
		 //added for defect id 6.1
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return mrTransferResponse;
			 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderid").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderid").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return mrTransferResponse;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderid").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return mrTransferResponse;
		 }
		 mudraMoneyTransactionBean.setTxnAmount(Double.parseDouble(claim.get("amount").toString()));
		 mudraMoneyTransactionBean.setTransType(claim.get("transferType").toString());
		// mudraMoneyTransactionBean.setSurChargeAmount(mudraDao.calculateSurCharge(mudraMoneyTransactionBean, serverName,requestId).getSurChargeAmount());
		 mudraMoneyTransactionBean.setWalletId(walletMast.getWalletid());
		 mudraMoneyTransactionBean.setMerchantTransId(claim.get("merchantTransId").toString());
		 mudraMoneyTransactionBean.setAgentid(remRequestBean.getAggregatorID());
		 if(remRequestBean.getTxn_mode().equalsIgnoreCase("W"))
		 {
			
		 }
		 else
		 {
			 fundTransactionSummaryBean=remmiDao.fundTransferBank(mudraMoneyTransactionBean,operationType ,serverName,requestId);
		 }

		 logger.info(serverName +"*****RequestId******"+requestId+"**********************************StatusCode*****"+fundTransactionSummaryBean.getStatusCode());
		 
		//added for defect id 14.7
			
			if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1130")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return mrTransferResponse;
			 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1001")||fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1110")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1111")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Transfer Type.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1170"))
		 {
			   responsemap.put("code", "1");
			   responsemap.put("response", "ERROR");
			   responsemap.put("message", "IMPS transfer channel not open. ");
			   oxyJWTSignUtil.generateToken(responsemap, mKey);
			   mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			   return mrTransferResponse;  
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1120")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Amount can't be zero.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1122")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transfer Limit exceed.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1000")){
		
			  				
				MudraMoneyTransactionBean mResult=new MudraMoneyTransactionBean();
				
				List <MudraMoneyTransactionBean> results=fundTransactionSummaryBean.getMudraMoneyTransactionBean();
				Iterator<MudraMoneyTransactionBean> iterator = results.iterator();
				
				while (iterator.hasNext()) {
					mResult=iterator.next();
					if(mResult.getCrAmount()==0){
						break;
					}
					}
				HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
				moneyRemittance.put("amount", fundTransactionSummaryBean.getAmount());
				moneyRemittance.put("paymentId", mResult.getTxnId());
				moneyRemittance.put("bankTransId", mResult.getBankRrn());
				moneyRemittance.put("transferStatus", mResult.getStatus());
				moneyRemittance.put("transDate", mResult.getPtytransdt());
				HashMap<String,Object> beneficiary= new HashMap<String,Object>();
				MudraBeneficiaryBank bResult=mudraDao.getBeneficiary(mResult.getBeneficiaryId(), serverName);
				beneficiary.put("beneficiaryId", bResult.getId());
				if(!operationType.equalsIgnoreCase("verify"))
				{
					moneyRemittance.put("charges", mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("totalAmount", fundTransactionSummaryBean.getAmount()+mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("transferType", mResult.getNarrartion());
					moneyRemittance.put("transId", mResult.getId());
					responsemap.put("moneyRemittance", moneyRemittance);					
					beneficiary.put("beneficiaryName", bResult.getName());
					beneficiary.put("accountNo", bResult.getAccountNo());
					//changed on 28th july
					beneficiary.put("ifscCode", bResult.getIfscCode() );
					responsemap.put("beneficiary", beneficiary);
					HashMap<String,Object> bankDetail= new HashMap<String,Object>();
					bankDetail.put("bankName", bResult.getBankName());
					bankDetail.put("branchName", bResult.getBranchName());
					bankDetail.put("address", bResult.getAddress());
					bankDetail.put("city", bResult.getCity());
					responsemap.put("bankDetail", bankDetail);
					
					responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
					//added for defect id  14.5
					//responsemap.put("mobileNo", mudraMoneyTransactionBean.getSenderId());
					
				}
				else{
					responsemap.put("mt_dtl", moneyRemittance);
					beneficiary.put("verified", fundTransactionSummaryBean.getVerified());
					beneficiary.put("accholdername", fundTransactionSummaryBean.getAccHolderName());
					beneficiary.put("verificationdesc", fundTransactionSummaryBean.getVerificationDesc());
					responsemap.put("verification",beneficiary);
					responsemap.put("transid",mResult.getMerchantTransId());
				}
				responsemap.put("mobileNo", mobileNo);
				responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
				responsemap.put("code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("message", "SUCCESS.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 
		 }else{
			 responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity check error.");
				mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return mrTransferResponse;
		 }
		}//added on 3rd Aug
		 catch (NumberFormatException e) 
		 {
		  logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		  e.printStackTrace();
		  responsemap.put("code", "1");
		  responsemap.put("response", "ERROR");
		  responsemap.put("message", "Invalid amount");
		  mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		  return mrTransferResponse;
		 }catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//mrTransferResponse.setRequestId("");
			mrTransferResponse.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return mrTransferResponse;
		} 
	}


*/

	@POST
	@Path("/mrTransferRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean mrTransferRequest(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,String encryptedRequest,@Context HttpServletRequest request)
	{
		MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
		MudraSenderBank mudraSender = new MudraSenderBank();
		MudraBeneficiaryBank mudraBeneficiary = new MudraBeneficiaryBank();
		FundTransferBean fundTransferBean = new FundTransferBean();
		ExtRemittanceResponseBean mrTransferResponse=new ExtRemittanceResponseBean();
		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		String serverName=request.getServletContext().getInitParameter("serverName");
		String aggregatorId=null;
		String privateKey = null;
		InputStream in =null;
		String operationType="";
		String requestId = null;
		JSONObject requestJson =null;
		try{

			requestJson = new JSONObject(encryptedRequest);
			aggregatorId = requestJson.getString("aggregatorId");
			Properties prop = new Properties();
			in = this.getClass().getResourceAsStream("remittanceKey.properties");
			prop.load(in);
			privateKey = prop.getProperty(aggregatorId+"PrivateKey");
			in.close();
			String ips=prop.getProperty("whiteListIP");
			//List<Student> participantJsonList = mapper.readValue(jsonString, new TypeReference<List<Student>>(){})
			//List<String> ipList = new ObjectMapper().readValue(ips, new TypeReference<List<String>>(){});
			List<String> ipList = new ArrayList<String>(Arrays.asList(ips.split(","))); 
			if(ipiemi == null)
			{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Authentication Problem.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
			}
			Iterator<String> itrIP = ipList.iterator();
			int isIPValid=0;
			while(itrIP.hasNext())
			{
				if(ipiemi.equals(itrIP.next()))
				{
					isIPValid=1;
					break;
				}
			}
			if(isIPValid == 0)
			{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Authentication Problem.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
			}
			String decryptionKey = RSAEncryptionWithAES.decryptAESKey(requestJson.getString("Key"), privateKey);
			requestId=remmiDao.saveRemitApiRequest(aggregatorId,encryptedRequest,serverName,"mrTransferRequest");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "Inside mrTransferRequest|RequestId:"+requestId);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", requestId ,"", "", "Inside mrTransferRequest|Request : "+encryptedRequest);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "Inside mrTransferRequest|Request:"+requestJson.getString("request"));
			mrTransferResponse.setRequestId(requestId);
			String decryptJson = RSAEncryptionWithAES.decryptTextUsingAES(requestJson.getString("request"), decryptionKey);
			fundTransferBean = gson.fromJson(decryptJson, FundTransferBean.class);
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "problem while getting property file|e.getMessage"+e.getMessage());
			e.printStackTrace();

			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Authentication Problem.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		
		}

		if(encryptedRequest == null || encryptedRequest.isEmpty())
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		try{
		
		fundTransferBean.setAggregatorId(aggregatorId);
		if(fundTransferBean.getAggregatorId()==null ||fundTransferBean.getAggregatorId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getMerchantTransId()==null ||fundTransferBean.getMerchantTransId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Merchant Transaction ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}			
		int count = mudraDao.gettxndetailsCount(fundTransferBean.getMerchantTransId(),aggregatorId, serverName,requestId);
		if(count > 0)
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Duplicate Merchant Transaction ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getSenderMobileNo() == null || !mobileValidation(fundTransferBean.getSenderMobileNo()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Mobile Number.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getSenderFName() == null || fundTransferBean.getSenderFName().isEmpty() || !nameValidation(fundTransferBean.getSenderFName()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender First name.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getSenderLName() == null || fundTransferBean.getSenderLName().isEmpty() || !nameValidation(fundTransferBean.getSenderLName()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender last name.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getBeneName() == null || fundTransferBean.getBeneName().isEmpty() || !nameValidation(fundTransferBean.getBeneName()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary name.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getBankName() == null || fundTransferBean.getBankName().isEmpty() || !nameValidation(fundTransferBean.getBankName()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid bank name.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getAccountNo() == null || fundTransferBean.getAccountNo().isEmpty() || !accountValidation(fundTransferBean.getAccountNo()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid beneficiary account number.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getIfscCode() == null || fundTransferBean.getIfscCode().isEmpty() || !iFSCValidation(fundTransferBean.getIfscCode()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid beneficiary IFSC.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getTransferType() == null || (!fundTransferBean.getTransferType().equals("NEFT") && !fundTransferBean.getTransferType().equals("IMPS")))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid transfer type.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		if(fundTransferBean.getAmount() < 1 )
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Transaction Amount cannot be less than Re 1.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		 if(fundTransferBean.getAmount() > 5000)
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Transaction Amount cannot be greater than Rs 5000.");
			 mrTransferResponse.setResponse(gson.toJson(responsemap));
			 return mrTransferResponse;
		 }
		 mudraSender.setMobileNo(fundTransferBean.getSenderMobileNo());
		 mudraSender.setFirstName(fundTransferBean.getSenderFName());
		 mudraSender.setLastName(fundTransferBean.getSenderLName());
		 mudraSender.setAggreatorId(fundTransferBean.getAggregatorId());
		 mudraSender.setAgent(agent);
		 mudraSender.setIpiemi(ipiemi);
		 mudraSender.setAgentId(fundTransferBean.getAggregatorId());
		 mudraSender = remmiDao.createSenderBankIfNotExt(mudraSender,serverName,requestId);
		 
		 mudraBeneficiary.setAccountNo(fundTransferBean.getAccountNo());
		 mudraBeneficiary.setSenderId(mudraSender.getId());
		 mudraBeneficiary.setName(fundTransferBean.getBeneName());
		 mudraBeneficiary.setBankName(fundTransferBean.getBankName());
		 mudraBeneficiary.setIfscCode(fundTransferBean.getIfscCode());
		 mudraBeneficiary.setTransferType(fundTransferBean.getTransferType());
		 mudraBeneficiary.setAgent(agent);
		 mudraBeneficiary.setIpiemi(ipiemi);
		 mudraBeneficiary.setAggreatorId(fundTransferBean.getAggregatorId());
		 mudraBeneficiary = remmiDao.createBeneBankIfNotExt(mudraBeneficiary,serverName,requestId);
		 
		 mudraMoneyTransactionBean.setSenderId(mudraSender.getId());
		 mudraMoneyTransactionBean.setBeneficiaryId(mudraBeneficiary.getId());
		 mudraMoneyTransactionBean.setNarrartion(fundTransferBean.getTransferType());
		 mudraMoneyTransactionBean.setTxnAmount(fundTransferBean.getAmount());
		 mudraMoneyTransactionBean.setTransType(fundTransferBean.getTransferType());
		 mudraMoneyTransactionBean.setAccHolderName(mudraBeneficiary.getName());
		 mudraMoneyTransactionBean.setMerchantTransId(fundTransferBean.getMerchantTransId());
		 mudraMoneyTransactionBean.setIpiemi(ipiemi);
		 mudraMoneyTransactionBean.setAgent(agent);
		 mudraMoneyTransactionBean.setWalletId("-1");
		fundTransactionSummaryBean=remmiDao.fundTransferBank(aggregatorId,mudraMoneyTransactionBean,operationType ,serverName,requestId);

		 logger.info(serverName +"*****RequestId******"+requestId+"**********StatusCode*****"+fundTransactionSummaryBean.getStatusCode());
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7024")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity Check error.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1333")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Insufficient Aggregator Wallet Balance.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
			if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1130")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
				mrTransferResponse.setResponse(gson.toJson(responsemap)); 
				return mrTransferResponse;
			 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1001")||fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap)); 
			return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1110")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1111")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Transfer Type.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1170"))
		 {
			   responsemap.put("code", "1");
			   responsemap.put("response", "ERROR");
			   responsemap.put("message", "IMPS transfer channel not open. ");
			   mrTransferResponse.setResponse(gson.toJson(responsemap)); 
			   return mrTransferResponse;  
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1120")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Amount can't be zero.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1122")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transfer Limit exceed.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1000")){
		
			  				
				MudraMoneyTransactionBean mResult=new MudraMoneyTransactionBean();
				
				List <MudraMoneyTransactionBean> results=fundTransactionSummaryBean.getMudraMoneyTransactionBean();
				Iterator<MudraMoneyTransactionBean> iterator = results.iterator();
				
				while (iterator.hasNext()) {
					mResult=iterator.next();
					if(mResult.getCrAmount()==0){
						break;
					}
					}
				HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
				moneyRemittance.put("amount", fundTransactionSummaryBean.getAmount());
				moneyRemittance.put("paymentId", mResult.getTxnId());
				moneyRemittance.put("bankTransId", mResult.getBankRrn());
				moneyRemittance.put("transferStatus", mResult.getStatus());
				moneyRemittance.put("transDate", mResult.getPtytransdt());
				HashMap<String,Object> beneficiary= new HashMap<String,Object>();
				MudraBeneficiaryBank bResult=mudraDao.getBeneficiary(mResult.getBeneficiaryId(), serverName);
				beneficiary.put("beneficiaryId", bResult.getId());
				if(!operationType.equalsIgnoreCase("verify"))
				{
					moneyRemittance.put("charges", mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("totalAmount", fundTransactionSummaryBean.getAmount()+mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("transferType", mResult.getNarrartion());
					moneyRemittance.put("transId", mResult.getId());
					responsemap.put("moneyRemittance", moneyRemittance);					
					beneficiary.put("beneficiaryName", bResult.getName());
					beneficiary.put("accountNo", bResult.getAccountNo());
					//changed on 28th july
					beneficiary.put("ifscCode", bResult.getIfscCode() );
					responsemap.put("beneficiary", beneficiary);
					HashMap<String,Object> bankDetail= new HashMap<String,Object>();
					bankDetail.put("bankName", bResult.getBankName());
					bankDetail.put("branchName", bResult.getBranchName());
					bankDetail.put("address", bResult.getAddress());
					bankDetail.put("city", bResult.getCity());
					responsemap.put("bankDetail", bankDetail);
					
					responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
					//added for defect id  14.5
					//responsemap.put("mobileNo", mudraMoneyTransactionBean.getSenderId());
					
				}
				else{
					responsemap.put("mt_dtl", moneyRemittance);
					beneficiary.put("verified", fundTransactionSummaryBean.getVerified());
					beneficiary.put("accholdername", fundTransactionSummaryBean.getAccHolderName());
					beneficiary.put("verificationdesc", fundTransactionSummaryBean.getVerificationDesc());
					responsemap.put("verification",beneficiary);
					responsemap.put("transid",mResult.getMerchantTransId());
				}
				responsemap.put("mobileNo", fundTransferBean.getSenderMobileNo());
				responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
				responsemap.put("code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("message", "SUCCESS.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 
		 }else{
			 responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity check error.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		}//added on 3rd Aug
		 catch (NumberFormatException e) 
		 {
		  logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		  e.printStackTrace();
		  responsemap.put("code", "1");
		  responsemap.put("response", "ERROR");
		  responsemap.put("message", "Invalid amount");
		  mrTransferResponse.setResponse(gson.toJson(responsemap));
		  return mrTransferResponse;
		 }catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		} 
	}





	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/accountVerificationRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public MerchantResponseBean accountVerificationRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	//public MerchantResponseBean accountVerificationRequest(MerchantRequestBean merchantRequestBean){
		MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
		String serverName=request.getServletContext().getInitParameter("serverName");
		//String serverName="bcd";
		
		String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"accountVerificationRequest");

		logger.info(serverName +"******RequestId***"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
		logger.info(serverName +"**************************************Request*****"+merchantRequestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
		merchantResponseBean.setRequestId(requestId);
		
		
		if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
		if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;		
		}
		String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		 logger.info(serverName+"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				merchantResponseBean.setResponse(gson.toJson(responsemap));
				return merchantResponseBean;
			}
		 
		 
		
		 
		//IMPS BLOCKED
		  
		  if(claim.get("transferType").toString().equalsIgnoreCase("IMPS")){
		   responsemap.put("code", "1");
		   responsemap.put("response", "ERROR");
		   responsemap.put("message", "IMPS transfer channel not open. ");
		   oxyJWTSignUtil.generateToken(responsemap, mKey);
		   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		   return merchantResponseBean;  
		  }
		 //END
		  
		  //added for defect id 13.3
			
			if(claim.get("beneficiaryName").toString().trim().equalsIgnoreCase("") || !nameValidation(claim.get("beneficiaryName").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Name");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
			
			if(claim.get("bankName").toString().trim().equalsIgnoreCase("") || !nameValidation(claim.get("bankName").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Bank Name");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
		   
			if(claim.get("accountNo").toString().trim().equalsIgnoreCase("") || !accountValidation(claim.get("accountNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Account Number");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
			
			if(claim.get("ifscCode").toString().trim().equalsIgnoreCase("") || !iFSCValidation(claim.get("ifscCode").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid IFSC Code");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
		 
		 
		 if(Double.parseDouble(claim.get("amount").toString())>5000){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Your max transaction limit is 5000.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 //added for defect id 13.2
			 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Mobile Number.");
					oxyJWTSignUtil.generateToken(responsemap, mKey);
					merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
					return merchantResponseBean;
				 }
		 String mobileNo = "";
		 
		 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		 } 
		 else
		 {
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
		 }
		 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		 }
		 WalletMastBean walletMast=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
		 	
		 mudraMoneyTransactionBean.setUserId(merchantRequestBean.getAgentId());
		 mudraMoneyTransactionBean.setSenderId(claim.get("senderId").toString());
		 mudraMoneyTransactionBean.setBeneficiaryId(claim.get("beneficiaryId").toString());
		 mudraMoneyTransactionBean.setNarrartion("IMPS");
		 mudraMoneyTransactionBean.setTxnAmount(1);
		 mudraMoneyTransactionBean.setVerificationAmount(1);
		 
		 mudraMoneyTransactionBean.setSurChargeAmount(mudraDao.calculateSurCharge(mudraMoneyTransactionBean, serverName,requestId).getSurChargeAmount());
		 mudraMoneyTransactionBean.setWalletId(walletMast.getWalletid());
		 mudraMoneyTransactionBean.setMerchantTransId(claim.get("merchantTransId").toString());
		
		 
		 MudraMoneyTransactionBean mudraMoneyTransactionBeanres=mudraDao.verifyAccount(mudraMoneyTransactionBean, serverName,requestId);
		 
		
		 
		 logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+mudraMoneyTransactionBeanres.getStatusCode());
		 
		 //added for defect id 13.3
		
		if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1130")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1001")||mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		 
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1110")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1111")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Transfer Type.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1120")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Amount can't be zero.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1122")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transfer Limit exceed.");
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 }
		 
		 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1000")){
		

				HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
				moneyRemittance.put("amount", mudraMoneyTransactionBeanres.getTxnAmount());
				moneyRemittance.put("transId", mudraMoneyTransactionBeanres.getTxnId());
				moneyRemittance.put("paymentId", mudraMoneyTransactionBeanres.getTxnId());
				moneyRemittance.put("bankTransId", mudraMoneyTransactionBeanres.getBankRrn());
				moneyRemittance.put("transferStatus", mudraMoneyTransactionBeanres.getStatusDesc());
				moneyRemittance.put("transDate", mudraMoneyTransactionBeanres.getPtytransdt());
			
				responsemap.put("moneyRemittance", moneyRemittance);
				HashMap<String,Object> verification= new HashMap<String,Object>();
				verification.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
				verification.put("verified", mudraMoneyTransactionBeanres.getVerified());
				verification.put("accHolderName", mudraMoneyTransactionBeanres.getAccHolderName());
				verification.put("verificationDesc", mudraMoneyTransactionBeanres.getVerificationDesc());
				responsemap.put("verification", verification);
				responsemap.put("code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("message", "SUCCESS.");
				//changed on 28th july
				responsemap.put("mobileNo", claim.get("mobileNo").toString());
				responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
				responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
		 
		 }else{
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Velocity check error.");
			 responsemap.put("mobileNo",claim.get("mobileNo").toString());
			 responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
			 responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
		} 
	}


	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/getTransHistoryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public MerchantResponseBean getTransHistoryRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getTransHistoryRequest");

		logger.info(serverName +"******RequestId*****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
		logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
		merchantResponseBean.setRequestId(requestId);
		
		
		
		if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
		if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		 logger.info(serverName+"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				//merchantResponseBean.setResponse(gson.toJson(responsemap));
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return merchantResponseBean;
			}	
		//added on 2nd Aug 
			 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
					responsemap.put("code", "1");
					responsemap.put("response", "ERROR");
					responsemap.put("message", "Invalid Mobile Number.");
					oxyJWTSignUtil.generateToken(responsemap, mKey);
					merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
					return merchantResponseBean;
				 }
		String mobileNo = "";

		if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
		{
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid senderId");
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		} 
		else
		{
			 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
		}
		if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
		{
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		}
		 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
		// mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
		 
		 	List<MerchantDmtTransBean> gettransDtl=mudraDao.gettransDtl(walletMastBean.getAggreatorid(), claim.get("mobileNo").toString(), serverName);
		 	responsemap.put("count", gettransDtl.size());
		 	responsemap.put("mobileNo", claim.get("mobileNo").toString());
		 	responsemap.put("transDetails", gettransDtl);
		 	responsemap.put("response", "SUCCESS");
			responsemap.put("message", "SUCCESS");
			responsemap.put("code", "300");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
		}
	}







	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/getTransStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getTransStatus(String requestBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();

		try{
		 JSONObject claim = new JSONObject(requestBean);
			
		if(claim.getString("aggregatorId") ==null ||claim.getString("aggregatorId").isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		 String aggreatorid=claim.getString("aggregatorId");
			
		 String requestId=remmiDao.saveRemitApiRequest(aggreatorid,requestBean,serverName,"getTransStatus");
			responseBean.setRequestId(requestId);
			logger.info(serverName +"********RequestId***"+requestId+"********"+requestBean);
		
		 if(claim.get("txnId")==null || claim.toString().isEmpty()){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}
		 SenderLedgerBean gettransDtl=mudraDao.gettxndetails(claim.getString("txnId"),aggreatorid,  serverName, requestId);
		 //if(gettransDtl==null && gettransDtl.size()<=0){
		 if(gettransDtl == null || gettransDtl.getStatus() == null || gettransDtl.getStatus().isEmpty()){
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid Agent Transaction Id. ");
			 responsemap.put("agentTransId", claim.get("agentTransId") );
			 responseBean.setResponse(gson.toJson(responsemap));
			 return responseBean;
		 }else{
			 responsemap.put("code", "300");
			 responsemap.put("response", "SUCCESS");
			 responsemap.put("message", "SUCCESS. ");
			 responsemap.put("transId", claim.get("txnId") );
			 responsemap.put("status", gettransDtl.getStatus() );
			 responsemap.put("BankRRN", gettransDtl.getBankrrn());
			 responsemap.put("amount", gettransDtl.getDramount() );
			 responsemap.put("transDateTime", gettransDtl.getPtytransdt() );
			responseBean.setResponse(gson.toJson(responsemap));
			 return responseBean; 
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId*****************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
	}
	
	
	@POST
	@Path("/getUserBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getUserBalance(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"getUserBalance");

		logger.info(serverName+"***RequestId*****"+requestId +"**************************************Merchantid*****"+requestBean.getAggregatorID());
		logger.info(serverName+"*****RequestId******"+requestId+"************************************Request*****"+requestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
		
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getTxn_mode()==null ||requestBean.getTxn_mode().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Mode.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid wallet Agent ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}	
		 
	//added on 2nd Aug 
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
	String mobileNo = "";
	double userBalance=0.0;

	if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return responseBean;
	} 
	else
	{
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	}
	if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return responseBean;
	}
	 	userBalance=mudraDaoWallet.getUserBalance(requestBean.getAggregatorID(), claim.get("senderId").toString(), serverName);

	 	responsemap.put("code", "300");
		responsemap.put("response", "SUCCESS");
		responsemap.put("message", "SUCCESS.");
		responsemap.put("mobileNo", claim.get("mobileNo").toString());
		responsemap.put("senderId", claim.get("senderId").toString());
		responsemap.put("balance", userBalance);
		//responseBean.setResponse(gson.toJson(responsemap));
		responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return responseBean;
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}
	}


	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */

	/*@POST
	@Path("/getUserBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getUserBalance(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=remmiDao.saveRemitApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"getUserBalance");

		logger.info(serverName+"***RequestId*****"+requestId +"**************************************Merchantid*****"+requestBean.getAggregatorID());
		logger.info(serverName+"*****RequestId******"+requestId+"************************************Request*****"+requestBean.getRequest());
		logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		responseBean.setRequestId(requestId);
		
		
		
		if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;		
		}
		//String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
		 logger.info(serverName +"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
		if(mKey==null ||mKey.isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(gson.toJson(responsemap));
			return responseBean;
		}
		try{
		 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
		 if(claim==null){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "JWT signature does not match. ");
				responseBean.setResponse(gson.toJson(responsemap));
				return responseBean;
			}	
		 
	//added on 2nd Aug 
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return responseBean;
			 }
	String mobileNo = "";

	if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return responseBean;
	} 
	else
	{
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	}
	if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return responseBean;
	}
	
			WalletMastBean walletAgg = new WalletUserDaoImpl().getUserByMobileNo(claim.get("mobileNo").toString(),requestBean.getAggregatorID(),serverName);
			if(!walletAgg.getWhiteLabel().equals("1"))
			{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Not a Valid Aggregator.");
				//responseBean.setRequestId("");
				responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				return responseBean;
			}
		 	//double userBalance=mudraDao.getUserBalance(requestBean.getAgentId(), claim.get("senderId").toString(), serverName);
		 	double userBalance = new TransactionDaoImpl().getWalletBalancebyId(walletAgg.getCashDepositeWallet());
		 	responsemap.put("code", "300");
			responsemap.put("response", "SUCCESS");
			responsemap.put("message", "SUCCESS.");
			responsemap.put("mobileNo", claim.get("mobileNo").toString());
			responsemap.put("senderId", claim.get("senderId").toString());
			responsemap.put("balance", userBalance);
			//responseBean.setResponse(gson.toJson(responsemap));
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			//responseBean.setRequestId("");
			responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return responseBean;
		}
	}

*/	
	
@POST
@Path("/showPassbook")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public ExtRemittanceResponseBean showPassbook(TxnInputBean txnInputBean,@Context HttpServletRequest request){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showPassbook()");
    TransactionDao transdao = new  TransactionDaoImpl();
    ExtRemittanceResponseBean mrTransferResponse=new ExtRemittanceResponseBean();
    HashMap<String,String> responsemap = new HashMap<String,String>();
    try
    {
    	String serverName=request.getServletContext().getInitParameter("serverName");
    	String requestId=remmiDao.saveRemitApiRequest(txnInputBean.getAggreatorid(),gson.toJson(txnInputBean),serverName,"showPassbook");
    	mrTransferResponse.setRequestId(requestId);
    	if(txnInputBean.getAggreatorid() == null || txnInputBean.getAggreatorid().isEmpty())
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
    	WalletMastBean user =new WalletUserDaoImpl().getUserByMobileNo(txnInputBean.getAggreatorid(),txnInputBean.getAggreatorid(),serverName);
    	if(user.getCashDepositeWallet() == null || user.getCashDepositeWallet().isEmpty())
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Please Contact Customer-Care.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
    	List passbook =transdao.showPassbook( transdao.getWalletIdByUserId(user.getCashDepositeWallet(), txnInputBean.getAggreatorid()),txnInputBean.getStDate(),txnInputBean.getEndDate());
    	if(passbook == null || passbook.size() <1)
    	{
    		responsemap.put("message","No Record Found.");
        	responsemap.put("code", "1");
    		responsemap.put("response", "ERROR");
    		mrTransferResponse.setResponse(gson.toJson(responsemap));
    		return mrTransferResponse;
    	}
    	responsemap.put("passbook", gson.toJson(passbook));
    	responsemap.put("message","Success.");
    	responsemap.put("code", "1000");
		responsemap.put("response", "SUCCESS");
		mrTransferResponse.setResponse(gson.toJson(responsemap));
		return mrTransferResponse;    	
    }
    catch(Exception ex)
    {
    	responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		mrTransferResponse.setResponse(gson.toJson(responsemap));
    	ex.printStackTrace();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),"", "","" ,"problem in showPassbook()|e.message:"+ex.getMessage());
    }
    return mrTransferResponse;   
}


public  boolean mobileValidation(String mobile) {
	//added on 2nd Aug
	    Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
	    Matcher matcher = pattern.matcher(mobile);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	public  boolean accountValidation(String account) {

	    Pattern pattern = Pattern.compile("^[0-9]{9,26}$");
	    Matcher matcher = pattern.matcher(account);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	public  boolean iFSCValidation(String ifsc) {

	    Pattern pattern = Pattern.compile("^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$");
	    Matcher matcher = pattern.matcher(ifsc);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	public  boolean nameValidation(String mobile) {

	    Pattern pattern = Pattern.compile("^[a-zA-Z ]+$");
	    Matcher matcher = pattern.matcher(mobile);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	public  boolean mpinValidation(String mobile) {

	    Pattern pattern = Pattern.compile("^[0-9]{4}+$");
	    Matcher matcher = pattern.matcher(mobile);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}



	//changed on 27th july
	/**
	* 
	* @param merchantRequestBean
	* @param request
	* @return
	*/
	@POST
	@Path("/getBankDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML }) 
	public ExtRemittanceResponseBean getBankDetails(ExtRemittanceRequestBean requestBean,@Context HttpServletRequest request){
	 String serverName=request.getServletContext().getInitParameter("serverName");
	 
	 String requestId=mudraDao.saveDMTApiRequest(requestBean.getAggregatorID(),requestBean.getRequest(),serverName,"getBankDetails");
	 
	 logger.info(serverName+"***RequestId*****"+requestId +"**************************************Merchantid*****"+requestBean.getAggregatorID());
	 logger.info(serverName +"***RequestId*****"+requestId +"**************************************Request*****"+requestBean.getRequest());
	 logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
	 ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
	 responseBean.setRequestId(requestId);
	 
	 
	 
	 if(requestBean.getAggregatorID()==null ||requestBean.getAggregatorID().isEmpty()){
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "Invalid wallet Agent ID.");
	 responseBean.setResponse(gson.toJson(responsemap));
	 return responseBean;
	 }
	 if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "Invalid Request.");
	 responseBean.setResponse(gson.toJson(responsemap));
	 return responseBean;  
	 }
	// String mKey=new MerchantDaoImpl().getKeyByMerchantid(requestBean.getAggregatorID());
	 logger.info(serverName +"***RequestId*****"+requestId +"***************************************mKey*****"+mKey);
	 if(mKey==null ||mKey.isEmpty()){
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "Invalid wallet Agent ID.");
	 responseBean.setResponse(gson.toJson(responsemap));
	 return responseBean;
	 }
	 try{
	 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
	 if(claim==null){
	  responsemap.put("code", "1");
	  responsemap.put("response", "ERROR");
	  responsemap.put("message", "JWT signature does not match. ");
	  responseBean.setResponse(gson.toJson(responsemap));
	  return responseBean;
	 } 
	 int errorCount = 0;
	 String bankName , branchName , ifscCode;
	 if(claim.get("bankName") == null || claim.get("bankName").toString().isEmpty())
	 {
	   errorCount++;
	   bankName="";
	 }
	 else
	 {
	  if( !nameValidation(claim.get("bankName").toString()))
	  {
	   responsemap.put("code", "1");
	   responsemap.put("response", "Error");
	   responsemap.put("message", "Invalid Bank Name");
	   responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	   return responseBean;
	  }
	  else
	  {
	   bankName=claim.get("bankName").toString();
	  }
	 }
	 if(claim.get("branchName") == null || claim.get("branchName").toString().isEmpty() )
	 {
	  errorCount++;
	  branchName="";
	 }
	 else
	 {
	  if( !nameValidation(claim.get("branchName").toString()))
	  {
	   responsemap.put("code", "1");
	   responsemap.put("response", "Error");
	   responsemap.put("message", "Invalid Branch Name");
	   responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	   return responseBean;
	  }
	  else
	  {
	   branchName=claim.get("branchName").toString();
	  }
	 }
	 if(claim.get("ifscCode") == null || claim.get("ifscCode").toString().isEmpty())
	 {
	  errorCount++;
	  ifscCode="";
	 }
	 else
	 {
	  if(!iFSCValidation(claim.get("ifscCode").toString()))
	  {
	   responsemap.put("code", "1");
	   responsemap.put("response", "Error");
	   responsemap.put("message", "Invalid IFSC");
	   //responseBean.setResponse(gson.toJson(responsemap));
	   responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	   return responseBean;
	  }
	  else
	  {
	   ifscCode=claim.get("ifscCode").toString();
	  }
	 }
	 if(errorCount == 3)
	 {
	  responsemap.put("code", "1");
	  responsemap.put("response", "Error");
	  responsemap.put("message", "Atleast one request Parameter should come Bank Name/ Branch Name / IFSC");
	 // responseBean.setResponse(gson.toJson(responsemap));
	  responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	  return responseBean;
	 }
	  BankDetailsBean bankDetails= new BankDetailsBean();
	  List<BankDetailsBean> bankSDetailsList=mudraDao.getBankDetails(bankName,ifscCode,branchName, serverName);
	  bankDetails.setBankDetailsList(bankSDetailsList);
	  responsemap.put("code", "300");
	 responsemap.put("response", "SUCCESS");
	 responsemap.put("message", "SUCCESS.");
	 responsemap.put("bankBranchDetails ", bankDetails.getBankDetailsList());
	 responsemap.put("count", bankSDetailsList.size());
	 //responseBean.setResponse(gson.toJson(responsemap));
	 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	 return responseBean;
	 }catch (Exception e) {
	  logger.info(serverName +"***RequestId*****"+requestId +"********************Exception*******************e.getMessage()*****"+e.getMessage());
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "INVALID MESSAGE STRING");
	 //responseBean.setRequestId("");
	 responseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	 return responseBean;
	 }
	}
/*
	@POST
	@Path("/agentBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getAgentBalance(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,String encryptedRequest,@Context HttpServletRequest request){
		ExtRemittanceResponseBean mrTransferResponse=new ExtRemittanceResponseBean();
		String serverName=request.getServletContext().getInitParameter("serverName");
		String aggregatorId=null;
		String privateKey = null;
		InputStream in =null;
		String encAggregatorId = null;
		String agentId=null;
		JSONObject requestJson =null;
		
		try{
			if(encryptedRequest == null || encryptedRequest.isEmpty())
			{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Request.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
			}	
			requestJson = new JSONObject(encryptedRequest);
			Properties prop = new Properties();
			in = this.getClass().getResourceAsStream("remittanceKey.properties");
			prop.load(in);
			privateKey = prop.getProperty("OAGG001057PrivateKey");
			in.close();
			String decryptionKey = RSAEncryptionWithAES.decryptAESKey(requestJson.getString("Key"), privateKey);
			aggregatorId = requestJson.getString("aggregatorId");
			String decryptJson = RSAEncryptionWithAES.decryptTextUsingAES(requestJson.getString("request"), decryptionKey);
			requestJson = new JSONObject(decryptJson);
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "problem while getting property file|e.getMessage"+e.getMessage());
			e.printStackTrace();

			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Authentication Problem.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		
		}
		try{
		if(requestJson.get("aggregatorId") == null || !requestJson.getString("aggregatorId").equals(aggregatorId)){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		
		if(requestJson.get("agentId") == null || requestJson.getString("agentId").isEmpty() ){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Agent ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		agentId=  requestJson.getString("agentId");
		if(requestJson.get("amount") == null || requestJson.getDouble("amount") < 1 ){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transaction Amount.");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		}
		WalletMastBean agentWalletMast = walletUserDao.getUserByMobileNo(agentId, aggregatorId, serverName);
		if(agentWalletMast.getAggreatorid().equals(aggregatorId))
		{
			
		}
		
		 logger.info(serverName +"*****RequestId******"+requestId+"**********StatusCode*****"+fundTransactionSummaryBean.getStatusCode());
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7024")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity Check error.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1333")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Insufficient Aggregator Wallet Balance.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
			if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1130")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
				mrTransferResponse.setResponse(gson.toJson(responsemap)); 
				return mrTransferResponse;
			 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1001")||fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(gson.toJson(responsemap)); 
			return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1110")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Sender Id.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1111")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Transfer Type.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1170"))
		 {
			   responsemap.put("code", "1");
			   responsemap.put("response", "ERROR");
			   responsemap.put("message", "IMPS transfer channel not open. ");
			   mrTransferResponse.setResponse(gson.toJson(responsemap)); 
			   return mrTransferResponse;  
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1120")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Amount can't be zero.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1122")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transfer Limit exceed.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1000")){
		
			  				
				MudraMoneyTransactionBean mResult=new MudraMoneyTransactionBean();
				
				List <MudraMoneyTransactionBean> results=fundTransactionSummaryBean.getMudraMoneyTransactionBean();
				Iterator<MudraMoneyTransactionBean> iterator = results.iterator();
				
				while (iterator.hasNext()) {
					mResult=iterator.next();
					if(mResult.getCrAmount()==0){
						break;
					}
					}
				HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
				moneyRemittance.put("amount", fundTransactionSummaryBean.getAmount());
				moneyRemittance.put("paymentId", mResult.getTxnId());
				moneyRemittance.put("bankTransId", mResult.getBankRrn());
				moneyRemittance.put("transferStatus", mResult.getStatus());
				moneyRemittance.put("transDate", mResult.getPtytransdt());
				HashMap<String,Object> beneficiary= new HashMap<String,Object>();
				MudraBeneficiaryBank bResult=mudraDao.getBeneficiary(mResult.getBeneficiaryId(), serverName);
				beneficiary.put("beneficiaryId", bResult.getId());
				if(!operationType.equalsIgnoreCase("verify"))
				{
					moneyRemittance.put("charges", mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("totalAmount", fundTransactionSummaryBean.getAmount()+mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("transferType", mResult.getNarrartion());
					moneyRemittance.put("transId", mResult.getId());
					responsemap.put("moneyRemittance", moneyRemittance);					
					beneficiary.put("beneficiaryName", bResult.getName());
					beneficiary.put("accountNo", bResult.getAccountNo());
					//changed on 28th july
					beneficiary.put("ifscCode", bResult.getIfscCode() );
					responsemap.put("beneficiary", beneficiary);
					HashMap<String,Object> bankDetail= new HashMap<String,Object>();
					bankDetail.put("bankName", bResult.getBankName());
					bankDetail.put("branchName", bResult.getBranchName());
					bankDetail.put("address", bResult.getAddress());
					bankDetail.put("city", bResult.getCity());
					responsemap.put("bankDetail", bankDetail);
					
					responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
					//added for defect id  14.5
					//responsemap.put("mobileNo", mudraMoneyTransactionBean.getSenderId());
					
				}
				else{
					responsemap.put("mt_dtl", moneyRemittance);
					beneficiary.put("verified", fundTransactionSummaryBean.getVerified());
					beneficiary.put("accholdername", fundTransactionSummaryBean.getAccHolderName());
					beneficiary.put("verificationdesc", fundTransactionSummaryBean.getVerificationDesc());
					responsemap.put("verification",beneficiary);
					responsemap.put("transid",mResult.getMerchantTransId());
				}
				responsemap.put("mobileNo", fundTransferBean.getSenderMobileNo());
				responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
				responsemap.put("code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("message", "SUCCESS.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 
		 }else{
			 responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity check error.");
				mrTransferResponse.setResponse(gson.toJson(responsemap));
				return mrTransferResponse;
		 }
		}//added on 3rd Aug
		 catch (NumberFormatException e) 
		 {
		  logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		  e.printStackTrace();
		  responsemap.put("code", "1");
		  responsemap.put("response", "ERROR");
		  responsemap.put("message", "Invalid amount");
		  mrTransferResponse.setResponse(gson.toJson(responsemap));
		  return mrTransferResponse;
		 }catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			mrTransferResponse.setResponse(gson.toJson(responsemap));
			return mrTransferResponse;
		} 
	}


*/




	public static void main(String[] args) {
			FundTransferBean fundTransferBean = new FundTransferBean();
			fundTransferBean.setAccountNo("914010021082501");
			fundTransferBean.setAggregatorId("OAGG001062");
			fundTransferBean.setAmount(5);
			fundTransferBean.setBankName("Axis Bank");
			fundTransferBean.setBeneName("Sweta Roy");
			fundTransferBean.setBranchName("Faridabad");
			fundTransferBean.setIfscCode("UTIB0000140");

			fundTransferBean.setMerchantTransId("Paymonk3");

			fundTransferBean.setSenderFName("Riya");
			fundTransferBean.setSenderLName("Kumari");
			fundTransferBean.setSenderMobileNo("9912121212");
			fundTransferBean.setTransferType("IMPS");
			HashMap<String,String> map = new HashMap<String,String>();
			map.put("agentTransId", "Paymonk3");
			String jsonObj = new Gson().toJson(fundTransferBean);
			try{
				
				String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
				 String encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonObj, aesKey);
				String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoM6VPLX+q+Ux+E4OEk5xTnQfQFau79HGy/MatUe7lPaf1mpCfYUb1N1zM1lg8+NG0N7xqfbEoU6VJlirfCkJI9J/fuZzV0EctZpGSmhEPuoxExrlncM1shzGF/3VLYWE5mBJdzAnwPn9gVMgHrVLHqBtIC2/tZ39zcRY5mXIuWZZ5apH59jG6FcFj/n/r5hAEMxVfGAbIRH2qNiaIv0OzHqkY8dhOYR/tJyCJDGOkTi5oReCGRrS9K2mzUHjQeDfCkvNhvwSiOXzWRPxDObfF4bHLlnCgTPTE1alHuOMVTgfYiYBPMGTjkVV/pj8EHDpbwZfMDHrw+gVYx4ZM98qRQIDAQAB");
			//String encry = RSAEncryptionWithAES.encryptAESKey(jsonObj, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoM6VPLX+q+Ux+E4OEk5xTnQfQFau79HGy/MatUe7lPaf1mpCfYUb1N1zM1lg8+NG0N7xqfbEoU6VJlirfCkJI9J/fuZzV0EctZpGSmhEPuoxExrlncM1shzGF/3VLYWE5mBJdzAnwPn9gVMgHrVLHqBtIC2/tZ39zcRY5mXIuWZZ5apH59jG6FcFj/n/r5hAEMxVfGAbIRH2qNiaIv0OzHqkY8dhOYR/tJyCJDGOkTi5oReCGRrS9K2mzUHjQeDfCkvNhvwSiOXzWRPxDObfF4bHLlnCgTPTE1alHuOMVTgfYiYBPMGTjkVV/pj8EHDpbwZfMDHrw+gVYx4ZM98qRQIDAQAB");
			//System.out.println("encrpt---"+encry);
			String request="vU/k+qAH0VFCy6xmMmZ4vslOElvVuPvBAVKD+m7xThEeq89x2seouDiWFqhZOzfaBDwvPk4obq56sqqVGqTFm9XP4A2hIQrrLA7SBo7sxIstG+YYvvUxNn1DXr3xXYnYo/AErBelJZe0iKPYCOZFIE6aFNKxx5N5mVhrn1ZEvK5R8plHQSLkMUi0+4GXsq59KHWli1330Sn7NF4je6zOFnJQcYEmcFW/27/Zc83vfmj0YGSBc7sX1mweioVHlSE4y9rpKVu34sVsSy//cI+TdF4SagSX4afIzNVtMzoXFGYmKieP+mBjNJsTxM53vXV82es1adun1qb3IyHaXYRd3LZYN3wi03NbAAhovWt2HzD5wA8IqrVRuiIkOxKwTtTJdYRd/UI51JyEuEmJ/+l06lRvvnTHjiI0jW5PgymHwho\u003d";
            String Key = "jny5jjHCzanWXlhpGDoFgqs0F1gUbR0Oeet5pQezmJZh5hfm8fXuPp8dOiX7lxkQOYdYmWUx16VuC8jAsm74yy9hTZLlkTEWblOwGDbaB7nr4hzhkU4qOuHvrQPbygAH+TwRqFP2bGXvx9yP7OD4/74k+OYaR9vxPsAF2dhO5VmF4aw5qFW2ADoPCIR6k9E67ICyGAlGeSpNSnIG3aN6t6hC/vU/VHCeTUU7s5WPp4SBjgJmOeR9l3MFu95v62ft7ukBClZML28GclIGFZtElY6c4KAJMnV4cwaWWckgBJbxRfr0M1/fANN4BSn+UkOJvK4u/An3kMoj7W1KVJK4EA\u003d\u003d";
            String descKey=RSAEncryptionWithAES.decryptAESKey(Key, "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCgzpU8tf6r5TH4Tg4STnFOdB9AVq7v0cbL8xq1R7uU9p/WakJ9hRvU3XMzWWDz40bQ3vGp9sShTpUmWKt8KQkj0n9+5nNXQRy1mkZKaEQ+6jETGuWdwzWyHMYX/dUthYTmYEl3MCfA+f2BUyAetUseoG0gLb+1nf3NxFjmZci5Zlnlqkfn2MboVwWP+f+vmEAQzFV8YBshEfao2Joi/Q7MeqRjx2E5hH+0nIIkMY6ROLmhF4IZGtL0rabNQeNB4N8KS82G/BKI5fNZE/EM5t8XhscuWcKBM9MTVqUe44xVOB9iJgE8wZOORVX+mPwQcOlvBl8wMevD6BVjHhkz3ypFAgMBAAECggEABWLF5tGlpPY5NTrQB9buvpTvEwWUOF4LTXtn8phjdPrJobHPZkrWAswj9p1QvPpovcgpyeqE7W4nWrBvlen972A2evaW+F4q6v4uyoExf/XyDNTCygTmwH9sNxN+V1b4e4rQCxjmy/TTg8hX9A9YY781yHUfzaJsBL9FxvmzvlncosL5qh/3DQ2JpMRAktZkFCZT2N7QfwWIPK3hJmC6S7kPQJTZFqhza/omBPdkvFKlu9tYmkqWkSVnLnk4CIPSB5plhE7y+uiVjvaf+uArm/Ut3/0P6VwgXg1n0LJRrL4xXFsEeI8oJ2CymvqKiYF9fla5hplkJ3nKqswz2axkRQKBgQDPYp9O2Yw3cCOX4Zq3nOzJoE+X8qiH6bA1wOeO0N5dMVviOLWQpiWV57FKHnTOkp0FQ+XWuEVCMBR4bu0r4OPoqFAkpoNaRQdzkiLN1ejcG7Rp9YZxcWsrWH0B4iI2FPkhGdlikVA997DDg5UQvXFVKuR5Ti4OXKEj4oZTfEWqwwKBgQDGgMFydGRy5LarzwpWDtM+KUvfkgmFtN77i6y4QEo7/dz7l2fs24BCgUbjr7vaCS1AJJ6p54SkRqSSkSJj51yI0anjk1NdbY5v9+eUotszIurCUcqUxhHuCUBJTgAoDwtQju6jJqViWuI/tj7figs9+xju8S+++pXiY3Ns3E82VwKBgATeuQT+ANfwbzWK27pCGiFl/ViBH0oJx9SLUiS9bOtdVF4MnN7nt+w8Q6KM8otDUCKmhIg1TZMPU/u8Bx/HqTJ2eWNS8bS1+EXTgbO5uT0p/WObm2ugkTa0XkMkD0+b0HnhhIRXCz5ZSbJdrat192pbln/uwRAfRr1z/VmKioxHAoGAXicCGnSklG5xSoq4f1smzSwvRcrx406IGU4FzZsI2u2tnFQTQiBW9LR22FYpRuDlarAz5ajfnD+5nZ9jbXl2xZqU91GQ0Ba/4dH3MOQSDm44ZZT//pL4PCx9JoftwRJprnstK4uQFFfBSrD5n3PNo0dvax5RJEedQjUjsXXJyJ8CgYAWHrw+B7QvgHlCQHfSyxicDzxHsVS20qc3N4CV1sbbhdh1tFoVqOt5Te8m2Lv/f8u5tWI/AZIsCm7r2Y6XwoCC196OPAAQzX/EVlqJePW7C2U/UnRvyvkw9C/DHgweuW7FEbSCMaPb/yLoYKeGO09YW8ct8OfMhZ2tErkxEjHflg==");
			System.out.println("decrpt....."+descKey);
			HttpServletRequest httpRequest = null;
			System.out.println("value:----"+RSAEncryptionWithAES.decryptTextUsingAES(request,descKey));
			System.out.println("Request-------"+new Gson().toJson(fundTransferBean));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			MudraDao mudraDao = new MudraDaoImpl();
			String mobileNo = mudraDao.getSenderMobileNo("MSEN001019").trim();
			 if(!("9935041287".trim().equals(mobileNo)))
			 {
				System.out.println(true);		 }
			 else
			 {
				 System.out.println(false);
			 }
		}

	}

	

