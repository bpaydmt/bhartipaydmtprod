package com.bhartipay.extremmitance.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.extremmitance.bean.ExtRemittanceAPILog;
import com.bhartipay.extremmitance.bean.FundTransferResponse;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;


public class ExtremmitapiDaoImpl implements ExtremmitapiDao {
	
	private static final Logger logger = Logger.getLogger(ExtremmitapiDaoImpl.class.getName());
	SessionFactory factory;

	//Transaction transaction = null;

	OTPGeneration oTPGeneration = new OTPGeneration();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	MudraDaoBank mudraDao = new MudraDaoBankImpl();
	MudraDao mudraDaoWallet = new MudraDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
	
	public MudraSenderBank getSenderDtlbank(MudraSenderBank mudraSenderBank, String serverName, String requestId) {

		logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Start excution ********************************* validateSender method**:"
				+ mudraSenderBank.getMobileNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()
					|| mudraSenderBank.getMobileNo().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo())).list();
			
			if (senderList.size() > 0) {
				mudraSenderBank = senderList.get(0);
				mudraSenderBank.setStatusCode("1000");
			mudraSenderBank.setStatusDesc("Y");
			List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
					.add(Restrictions.ne("status", "Deleted"))
					.addOrder(Order.asc("status")).list();
			mudraSenderBank.setBeneficiaryList(benfList );

			} else{ 
				mudraSenderBank.setStatusCode("1000");
			mudraSenderBank.setStatusDesc("N");
			}

		} catch (Exception e) {
			logger.debug(serverName + "******RequestId******" + requestId
					+ "*******problem in validateSender=========================" + e.getMessage(), e);
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;
	}

	public MudraSenderWallet getSenderDtlWallet(MudraSenderWallet mudraSenderwallet, String serverName, String requestId) {

		logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Start excution ********************************* validateSender method**:"
				+ mudraSenderwallet.getMobileNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		mudraSenderwallet.setStatusCode("1001");
		mudraSenderwallet.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderwallet.getMobileNo() == null || mudraSenderwallet.getMobileNo().isEmpty()
					|| mudraSenderwallet.getMobileNo().length() != 10) {
				mudraSenderwallet.setStatusCode("1104");
				mudraSenderwallet.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderwallet;
			}

			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderwallet.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderwallet.getAggreatorId())).list();
			if (senderList.size() > 0) {
				mudraSenderwallet = (MudraSenderWallet)senderList.get(0);
				mudraSenderwallet.setStatusCode("1000");
				mudraSenderwallet.setStatusDesc("Y");
				
				
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraSenderwallet.getId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				
				mudraSenderwallet.setBeneficiaryList(benfList);
				
				
			} else{ 
				mudraSenderwallet.setStatusCode("1000");
				mudraSenderwallet.setStatusDesc("N");
			}

		} catch (Exception e) {
			logger.debug(serverName + "******RequestId******" + requestId
					+ "*******problem in validateSender=========================" + e.getMessage(), e);
			mudraSenderwallet.setStatusCode("7000");
			mudraSenderwallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderwallet;
	}
	
	
	public FundTransferResponse fundTransferBank(String aggreatorId,MudraMoneyTransactionBean mudraMoneyTransactionBean,String opertaionType,String serverName, String requestId) 
	{
		logger.info(serverName + "****RequestId****" + requestId+ "***Start excution *** fundTransferBank method******:");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");
		try {
			logger.info(serverName + "****RequestId****" + requestId + "****fundTransferBank******"+ mudraMoneyTransactionBean.getSenderId());
			logger.info(serverName + "****RequestId****" + requestId + "****fundTransferBank******"+ mudraMoneyTransactionBean.getBeneficiaryId());
			logger.info(serverName + "****RequestId****" + requestId + "****fundTransferBank******"+ mudraMoneyTransactionBean.getNarrartion());
			logger.info(serverName + "****RequestId****" + requestId + "****fundTransferBank******"+ mudraMoneyTransactionBean.getTxnAmount());
		
			/*TreeMap<String, String> treeMap = new TreeMap<String, String>();
			treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(treeMap);
			if (statusCode != 1000) {
				fundTransactionSummaryBean.setStatusCode("" + statusCode);
				fundTransactionSummaryBean.setStatusDesc("Security error.");
				return fundTransactionSummaryBean;
			}*/

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null || mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}


			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount();
			double wlSurcharge = 0;//while label Aggregator Surcharge
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn = null;

			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,mudraMoneyTransactionBean.getSenderId());
			WalletMastBean walletmastAgg = (WalletMastBean) session.get(WalletMastBean.class,aggreatorId);
			MudraBeneficiaryBank mudraBeneficiaryBank = (MudraBeneficiaryBank) session.get(MudraBeneficiaryBank.class,mudraMoneyTransactionBean.getBeneficiaryId());
			
			// logic needs to be changed for verifing beneficiary and setting V in below try and catch inside if
			if (mudraBeneficiaryBank != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
				//if(!mudraBeneficiaryBank.getAccountNo().equals(mudraMoneyTransactionBean.getA))
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			// monthly Transfer Limit validation
			if (mudraSenderBank.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
				return fundTransactionSummaryBean;
			}

			// Yearly Transfer Limit validation
		/*	if (mudraSenderBank.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderBank.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}*/

			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {
				ppiTxnIdfundTransfer = commanUtilDao.getTrxId("FBTX",mudraMoneyTransactionBean.getAggreatorid());
			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();

				
				if (mudraMoneyTransactionBean != null && mudraMoneyTransactionBean.getAccHolderName() != null && !mudraMoneyTransactionBean.getAccHolderName().isEmpty()) {
					try {
						System.out.println(mudraMoneyTransactionBean.getAccHolderName());
						mudraBeneficiaryBank.setVerified("V");
						mudraBeneficiaryBank.setName(mudraMoneyTransactionBean.getAccHolderName());
						session.update(mudraBeneficiaryBank);
					} catch (Exception e) {
						logger.debug("********** problem in update bene details "+ mudraBeneficiaryBank.getName());
					}
				}
			}
			logger.info(serverName + "**RequestId**" + requestId + "****" + mudraMoneyTransactionBean.getSenderId()+ "****fundTransferBank ID****:" + ppiTxnIdfundTransfer);
			String walletAggTxnCode = "1000";
			String walletId=transactionDao.getWalletIdByUserId(walletmastAgg.getCashDepositeWallet(),aggreatorId);
			SurchargeBean surchargeBean = new SurchargeBean();
			surchargeBean.setAgentId(aggreatorId);
			surchargeBean.setAmount(agnWalletTxnAmount);
			if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
				surchargeBean.setTransType("NEFT");
			} else {
				surchargeBean.setTransType("IMPS");
			}
			surchargeBean = mudraDao.calculateSurcharge(surchargeBean, serverName, requestId);

			if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
				wlSurcharge=surchargeBean.getCreditSurcharge();
			} else {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
				return fundTransactionSummaryBean;
			}

			if (walletmastAgg.getWhiteLabel().equalsIgnoreCase("1") )
			{
					if((agnWalletTxnAmount +wlSurcharge) > new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())) 
					{
						fundTransactionSummaryBean.setStatusCode("1333");
						fundTransactionSummaryBean.setStatusDesc("Insufficient Aggregator Wallet Balance.");
						return fundTransactionSummaryBean;
					} 
					else 
					{		

						walletAggTxnCode = transactionDao.walletToAggregatorTransferExt(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),agnWalletTxnAmount,wlSurcharge, mudraMoneyTransactionBean.getIpiemi(),

								aggreatorId, mudraMoneyTransactionBean.getAgent());
					}
				}				
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				mudraMoneyTransactionBeanSaveTxn.setIsBank(1);
				mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setWalletId(walletId);
				mudraMoneyTransactionBeanSaveTxn.setAgentid(aggreatorId);
				mudraMoneyTransactionBeanSaveTxn.setStatus("Pending");
				mudraMoneyTransactionBeanSaveTxn.setRemark("Pending");
				session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();
				transaction = session.beginTransaction();
				logger.info(serverName + "****RequestId****" + requestId + "*" + mudraMoneyTransactionBean.getSenderId()+ "**fundTransferBank***:" + ppiTxnIdfundTransfer);
					if (walletAggTxnCode.equalsIgnoreCase("1000")) {
						mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();
					} else {
						logger.info(serverName + "****RequestId****" + requestId + "*"+ mudraMoneyTransactionBean.getSenderId()+ "****velocity error** fundtransfer method*"+ walletAggTxnCode);
						mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
						mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();

					}

			if (walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				String responseCode = null;
				String actCode=null;
				List<String> respList = new ArrayList<String>();
				String beneName = new String();
				MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				fundSaveTxn.setIsBank(1);
				transaction = session.beginTransaction();
				fundSaveTxn.setId("TB1"+ ppiTxnIdfundTransfer);
				fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				fundSaveTxn.setAgentid(aggreatorId);
				fundSaveTxn.setWalletId(walletId);
				if(!opertaionType.equalsIgnoreCase("verify"))
				{
					if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
						fundSaveTxn.setStatus("NEFT INITIATED");
					} else {
						fundSaveTxn.setStatus("PENDING");
					}
					fundSaveTxn.setDrAmount(mudraMoneyTransactionBean.getTxnAmount());
					session.save(fundSaveTxn);
					
					respList=mudraDao.finoImplementation(mudraMoneyTransactionBean.getNarrartion(),fundSaveTxn.getTxnAmount(),mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getName(),fundSaveTxn.getId(),mudraSenderBank.getMobileNo(),mudraSenderBank.getFirstName());
					
					responseCode = respList.get(0);
					actCode = respList.get(1);
				}
				else
				{
					fundSaveTxn.setStatus("PENDING(VERIFICATION)");
					if (mudraBeneficiaryBank != null && mudraBeneficiaryBank.getVerified() != null
							&& !mudraBeneficiaryBank.getVerified().equalsIgnoreCase("V")) {
						System.out.println("++++++++++++++++++++++++++++++++calling finoimps for verify account++++++++++++++++++++++");

						respList=mudraDao.finoImplementation(mudraMoneyTransactionBean.getNarrartion(),mudraMoneyTransactionBean.getVerificationAmount(),mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getName(),mudraMoneyTransactionBean.getId(),mudraSenderBank.getMobileNo(),mudraSenderBank.getFirstName());
						System.out.println("++++++++++++++++++++++++++++++++getting result from fino imps after verifying account ++++++++++++++++++++++");
						System.out.println("++++++++++++++++++++++++++++++++response is +++++++++++++++++++++"+respList);

						responseCode = respList.get(0);
						actCode=respList.get(1);
						if(!respList.get(3).equals("null"))
							beneName=respList.get(3);
						else
						{	
							beneName = "null";
						}
						transaction = session.beginTransaction();
		
						} 
					else {
						String finotrxId = commanUtilDao.getTrxId("FINO",mudraMoneyTransactionBean.getAggreatorid());
						actCode="0";
						responseCode="0";
						beneName = mudraBeneficiaryBank.getName();
						
					}
				}
				transaction = session.beginTransaction();
				if (!responseCode.equals("0") && !(actCode.equals("0") ||actCode.equalsIgnoreCase("S") || actCode.equals("26") || actCode.equals("11"))) {
					if (responseCode.equals("0")) {
						agnWalletTxnAmount = agnWalletTxnAmount - fundSaveTxn.getDrAmount();
						fundSaveTxn.setStatus("Confirmation Awaited");
						fundSaveTxn.setRemark("Transaction Pending");
						if(!respList.get(2).equals("null"))
							fundSaveTxn.setBankRrn(respList.get(2));
						fundSaveTxn.setBankResp("Confirmation Awaited");
					} else {
						fundSaveTxn.setStatus("FAILED");
						fundSaveTxn.setRemark("Transaction FAILED");
						if(!respList.get(2).equals("null"))
							fundSaveTxn.setBankRrn(respList.get(2));
						fundSaveTxn.setBankResp("FAILED");
					}
					if ( agnWalletTxnAmount  > 0) {

						MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
						mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxnRef.setIsBank(1);
						mudraMoneyTransactionBeanSaveTxnRef.setAgentid(aggreatorId);
						mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxnRef.setWalletId(walletId);
						mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
						mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
						mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(agnWalletTxnAmount);
						if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("NEFT");
						} else {
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("IMPS");
						}
						mudraMoneyTransactionBeanSaveTxnRef.setStatus("SUCCESS");
						mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To Aggregator)");
						session.save(mudraMoneyTransactionBeanSaveTxnRef);

						if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
						{

							String AggRefundCode =  transactionDao.walletToAggregatorRefundExt(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),agnWalletTxnAmount,wlSurcharge,mudraMoneyTransactionBean.getIpiemi()
									,aggreatorId, mudraMoneyTransactionBean.getAgent());

						}	
					}
					txnAmount = 0;

				}
				else 
				{
					if (actCode.equals("0") || actCode.equalsIgnoreCase("S")
							|| actCode.equals("26") || actCode.equals("11")) {
						fundSaveTxn.setStatus("SUCCESS");
						fundSaveTxn.setRemark("Transaction SUCCESSFUL");
						if(!respList.get(2).equals("null"))
						{
							fundSaveTxn.setBankRrn(respList.get(2));
						}
						fundSaveTxn.setBankResp("SUCCESS");
						if(opertaionType.equalsIgnoreCase("verify")){
							if (beneName.trim().equalsIgnoreCase(mudraBeneficiaryBank.getName().trim())) {
								mudraBeneficiaryBank.setVerified("V");
								session.update(mudraBeneficiaryBank);
								fundTransactionSummaryBean.setVerified("V");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(V)");
								fundTransactionSummaryBean.setVerificationDesc("Match");	
							} else {
								mudraBeneficiaryBank.setVerified("NV");
								session.update(mudraBeneficiaryBank);
								fundTransactionSummaryBean.setVerified("NV");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(NV)");
								fundTransactionSummaryBean.setVerificationDesc("Not Matched");
							}
						}
					} else {
						fundSaveTxn.setStatus("Confirmation Awaited");
						fundSaveTxn.setRemark("Transaction Pending");
						if(!respList.get(2).equals("null"))
							fundSaveTxn.setBankRrn(respList.get(2));
						fundSaveTxn.setBankResp("Confirmation Awaited");
					}
				}
				session.update(fundSaveTxn);
				transaction.commit();
				logger.info(serverName + "**RequestId****" + requestId + "**" + mudraMoneyTransactionBean.getSenderId());
				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderBank.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryBank.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryBank.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryBank.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("Min INR 15.00 Max upto 2%");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());

				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));

				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				MudraSenderBank mudraSenderNew = (MudraSenderBank) session.get(MudraSenderBank.class,mudraSenderBank.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());

			} else {
				logger.info(serverName + "****RequestId****" + requestId + "**" + mudraMoneyTransactionBean.getSenderId()+ "**velocity error**:" + walletAggTxnCode);
				
				fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			logger.debug(serverName + "**RequestId**" + requestId + "****" + mudraMoneyTransactionBean.getSenderId()+"problem in fundTransferBank=======" + e.getMessage(), e);
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(transaction.isActive())
				transaction.commit();
			session.close();
		}

		return fundTransactionSummaryBean;

	}


	public FundTransferResponse fundTransferWallet(MudraMoneyTransactionBean mudraMoneyTransactionBean,String operationType,String serverName, String requestId) {
		logger.info(serverName + "****RequestId****" + requestId+ "*Start excution *** fundTransfer method***:");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");

		try {
			logger.info(serverName + "**RequestId*" + requestId + "**** fundTransferWallet method**:"+ mudraMoneyTransactionBean.getSenderId());
			logger.info(serverName + "****RequestId****" + requestId + "**getBeneficiaryId()*** fundTransferWallet method****:"+ mudraMoneyTransactionBean.getBeneficiaryId());
			logger.info(serverName + "****RequestId****" + requestId + "** fundTransferWallet method*"+ mudraMoneyTransactionBean.getNarrartion());
			logger.info(serverName + "****RequestId****" + requestId + "**getTxnAmount()*** fundTransferWallet method****:"+ mudraMoneyTransactionBean.getTxnAmount());
			logger.info(serverName + "****RequestId****" + requestId + "** fundTransferWallet method***"+ mudraMoneyTransactionBean.getWalletId());

			TreeMap<String, String> treeMap = new TreeMap<String, String>();
			treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(treeMap);
			if (statusCode != 1000) {
				fundTransactionSummaryBean.setStatusCode("" + statusCode);
				fundTransactionSummaryBean.setStatusDesc("Security error.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null || mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}
			if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("IMPS")) {
				fundTransactionSummaryBean.setStatusCode("1170");
				fundTransactionSummaryBean.setStatusDesc("IMPS transfer channel not open. ");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}
			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Wallet Id can't be empty.");
				return fundTransactionSummaryBean;
			}

			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double agnWalletTxnAmount = 0;
			double surChargeAmount = 0;
			String walletAggTxnCode="1000";
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn=null;
			
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,mudraMoneyTransactionBean.getSenderId());
			WalletMastBean walletmastAgg = (WalletMastBean)session.get(WalletMastBean.class, mudraSenderWallet.getAggreatorId());
			MudraBeneficiaryWallet mudraBeneficiaryMastBean = (MudraBeneficiaryWallet) session.get(MudraBeneficiaryWallet.class, mudraMoneyTransactionBean.getBeneficiaryId());
			
			if (mudraBeneficiaryMastBean != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim()
						.equals(mudraBeneficiaryMastBean.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}
			if (mudraSenderWallet.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
				return fundTransactionSummaryBean;
			}
			if (mudraSenderWallet.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderWallet.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {
				ppiTxnIdfundTransfer = commanUtilDao.getTrxId("MMTXN",mudraMoneyTransactionBean.getAggreatorid());
			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();
			}
			logger.info(serverName + "****RequestId****" + requestId + "*****fundTransferWallet start with fundtxnid*******:" + ppiTxnIdfundTransfer);
				agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount() ;
				surChargeAmount = 0;
				SurchargeBean surchargeBean = new SurchargeBean();
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(agnWalletTxnAmount);
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					surchargeBean.setTransType("NEFT");
				} else {
					surchargeBean.setTransType("IMPS");
				}
				surchargeBean = mudraDao.calculateSurcharge(surchargeBean, serverName, requestId);

				if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
					surChargeAmount = surchargeBean.getSurchargeAmount();
				} else {
					fundTransactionSummaryBean.setStatusCode("1122");
					fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return fundTransactionSummaryBean;
				}

			
				if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1")&&(surchargeBean.getCreditSurcharge()+agnWalletTxnAmount)>new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())){
				     fundTransactionSummaryBean.setStatusCode("7024");
				     fundTransactionSummaryBean.setStatusDesc("Insufficient Aggregator Wallet Balance.");
				     return fundTransactionSummaryBean;
				}
				else if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
				{
					
					walletAggTxnCode = transactionDao.walletToAggregatorTransfer(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),(agnWalletTxnAmount),surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),mudraSenderWallet.getAggreatorId(),mudraMoneyTransactionBean.getAgent(),0,0);
			
				}
				
				logger.info(serverName + "****RequestId****" + requestId + "*** method****:" + ppiTxnIdfundTransfer);

				transaction = session.beginTransaction();
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				if (walletAggTxnCode.equalsIgnoreCase("1000")) {
					mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
					mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
				} else {
					logger.info(serverName + "****RequestId****" + requestId + "***********velocity error******* findtransferwallet method****:"+ walletAggTxnCode);
					mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
					mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
				}

			if ( walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				String beneName = new String();
				MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				transaction = session.beginTransaction();
				fundSaveTxn.setId("TB"+ ppiTxnIdfundTransfer);
				fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					fundSaveTxn.setStatus("NEFT INITIATED");
				} else {
					fundSaveTxn.setStatus("PENDING");
				}
				session.save(fundSaveTxn);

				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					logger.info(serverName + "****RequestId****" + requestId + "****start insert into daily_neft_icici with txnid*******"+ fundSaveTxn.getId());
					SQLQuery insertQuery = session.createSQLQuery(""
							+ "insert into daily_neft_icici(mode,txnid,debitacc,creditacc,benname,amount,ifsc,status)VALUES(?,?,?,?,?,?,?,?)");
					insertQuery.setParameter(0, "N");
					insertQuery.setParameter(1, fundSaveTxn.getId());
					insertQuery.setParameter(2, "003105031195");
					insertQuery.setParameter(3, mudraBeneficiaryMastBean.getAccountNo());
					insertQuery.setParameter(4, mudraBeneficiaryMastBean.getName());
					insertQuery.setParameter(5, fundSaveTxn.getDrAmount());
					insertQuery.setParameter(6, mudraBeneficiaryMastBean.getIfscCode());
					insertQuery.setParameter(7, 0);
					insertQuery.executeUpdate();
					session.getTransaction().commit();
					logger.info(serverName + "****RequestId****" + requestId + "*****completed insert into daily_neft_icici with txnid***"+ fundSaveTxn.getId());
				} else { 
					IMPSIciciResponseBean respBean = new IMPSIciciResponseBean();
					if(!operationType.equalsIgnoreCase("verify"))
					{
						respBean =mudraDaoWallet.fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),fundSaveTxn.getDrAmount(),fundSaveTxn.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());
					}
					else
					{
						if (mudraBeneficiaryMastBean != null && mudraBeneficiaryMastBean.getVerified() != null
								&& !mudraBeneficiaryMastBean.getVerified().equalsIgnoreCase("V")) {
							System.out.println("++++++++++++++++++++++++++++++++calling finoimps for verify account++++++++++++++++++++++");

							//List<String> respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),mudraMoneyTransactionBean.getVerificationAmount(),mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraBeneficiaryMastBean.getName(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getMobileNo(),mudraSenderWallet.getFirstName());
							respBean =mudraDaoWallet.fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraMoneyTransactionBean.getVerificationAmount(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());
							
							System.out.println("++++++++++++++++++++++++++++++++getting result from fino imps after verifying account ++++++++++++++++++++++");
							System.out.println("++++++++++++++++++++++++++++++++response is +++++++++++++++++++++"+respBean);
							
							transaction = session.beginTransaction();
							} 
						else {
							String finotrxId = commanUtilDao.getTrxId("ICICI",mudraMoneyTransactionBean.getAggreatorid());
							respBean.setActCode(200);
							beneName = mudraBeneficiaryMastBean.getName();
							
						}
					}
					transaction = session.beginTransaction();
					
					if(respBean.getActCode()!=200) {
						fundSaveTxn.setStatus("FAILED");
						fundSaveTxn.setRemark("Transaction FAILED");
						fundSaveTxn.setBankRrn(respBean.getBankRRN());
						MudraMoneyTransactionBean wallRefundTxnRef = new MudraMoneyTransactionBean();
						if (agnWalletTxnAmount > 0) {
							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
							mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(agnWalletTxnAmount);
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND");
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("SUCCESS");
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To Aggregator)");
							session.save(mudraMoneyTransactionBeanSaveTxnRef);
							if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
							{
								String AggRefundCode =  transactionDao.walletToAggregatorRefund(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),agnWalletTxnAmount,surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),mudraSenderWallet.getAggreatorId(), mudraMoneyTransactionBean.getAgent(),0,0);
							}
						}
							

						txnAmount = 0;

					} else {
						fundSaveTxn.setStatus("SUCCESS");
						fundSaveTxn.setRemark("Transaction SUCCESSFUL");
						fundSaveTxn.setBankRrn(respBean.getBankRRN());
						if(operationType.equalsIgnoreCase("verify")){
							if (beneName.trim().equalsIgnoreCase(mudraBeneficiaryMastBean.getName().trim())) {
								mudraBeneficiaryMastBean.setVerified("V");
								session.update(mudraBeneficiaryMastBean);
								fundTransactionSummaryBean.setVerified("V");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(V)");
								fundTransactionSummaryBean.setVerificationDesc("Match");	
							} else {
								mudraBeneficiaryMastBean.setVerified("NV");
								session.update(mudraBeneficiaryMastBean);
								fundTransactionSummaryBean.setVerified("NV");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(NV)");
								fundTransactionSummaryBean.setVerificationDesc("Not Matched");
							}
						}
					}
					fundSaveTxn.setBankResp(respBean.getBankRRN());
					session.update(fundSaveTxn);
					transaction.commit();
				}
				
				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderWallet.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryMastBean.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryMastBean.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryMastBean.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("Min INR 15.00 Max upto 2%");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());

				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));

				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				try
				{
				MudraSenderWallet mudraSenderNew=(MudraSenderWallet)session.get(MudraSenderWallet.class, mudraSenderWallet.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());
				String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer", mudraSenderWallet.getAggreatorId());
				logger.info(serverName + "****RequestId****" + requestId +  smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()));
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderWallet.getMobileNo(),
						smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()),
						"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "NOTP");

				ThreadUtil.getThreadPool().execute(smsAndMailUtility);


				}
				catch(Exception ex)
				{
					logger.info("*****problem in fundTransferWallet while sending message******e.message**"+ex.getMessage());
					ex.printStackTrace();
				}
			} else {
				logger.info(serverName + "****RequestId****" + requestId + "******" + mudraMoneyTransactionBean.getSenderId()+"**velocity error****  method****:" + walletAggTxnCode);
				fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			logger.debug(serverName + "****RequestId****" + requestId + "*fundTransferWallet====" + e.getMessage(), e);
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return fundTransactionSummaryBean;

	}


	public String saveRemitApiRequest(String aggregatorId, String reqVal, String serverName, String apiName) {

		logger.info(serverName + "Start excution ************************************* saveRemitApiRequest agentId**:"
				+ aggregatorId);
		logger.info(serverName + "Start excution ************************************* saveRemitApiRequest reqVal**:"
				+ reqVal);

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		String requestId = "";
		try {
			transaction = session.beginTransaction();
			ExtRemittanceAPILog remApi = new ExtRemittanceAPILog();

			remApi.setRequestId(Long.toString(generateLongId()) + commanUtilDao.getTrxId("REMMAPI",aggregatorId));
			remApi.setAggregatorId(aggregatorId);;
			remApi.setRequestValue(reqVal);
			remApi.setApiName(apiName);
			session.save(remApi);
			transaction.commit();
			requestId = remApi.getRequestId();
			logger.info(
					serverName + "Start excution ************************************* saveRemitApiRequest requestId**:"
							+ requestId);
		} catch (Exception e) {
			logger.debug(serverName + "problem in saveRemitApiRequest=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return requestId;
	}

	
	private static final long twepoch = 1288834974657L;
	private static final long sequenceBits = 17;
	private static final long sequenceMax = 65536;
	private static volatile long lastTimestamp = -1L;
	private static volatile long sequence = 0L;

	private static synchronized Long generateLongId() {
		long timestamp = System.currentTimeMillis();
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) % sequenceMax;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimestamp);
			}
		} else {
			sequence = 0;
		}
		lastTimestamp = timestamp;
		Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
		return id;
	}

	private static long tilNextMillis(long lastTimestamp) {
		long timestamp = System.currentTimeMillis();
		while (timestamp <= lastTimestamp) {
			timestamp = System.currentTimeMillis();
		}
		return timestamp;
	}
	
	public List getBeneficiaryListBank(List<MudraBeneficiaryBank> beneList)
	{
		List respList = new ArrayList();
		Iterator<MudraBeneficiaryBank> itr = beneList.iterator();
		while(itr.hasNext())
		{
			MudraBeneficiaryBank benebank= itr.next();
			HashMap<String,Object> beneficiary = new HashMap<String,Object>();
			beneficiary.put("Id", benebank.getId());
			beneficiary.put("name", benebank.getName());
			beneficiary.put("transfertype", benebank.getTransferType());
			beneficiary.put("accountno", benebank.getAccountNo());
			beneficiary.put("ifsccode", benebank.getIfscCode());
			beneficiary.put("status", benebank.getStatus());
			respList.add(beneficiary);
		}
		return respList;
	}
	public List getBeneficiaryListWallet(List<MudraBeneficiaryWallet> beneList)
	{
		List respList = new ArrayList();
		Iterator<MudraBeneficiaryWallet> itr = beneList.iterator();
		while(itr.hasNext())
		{
			MudraBeneficiaryWallet benewallet= itr.next();
			HashMap<String,Object> beneficiary = new HashMap<String,Object>();
			beneficiary.put("Id", benewallet.getId());
			beneficiary.put("name", benewallet.getName());
			beneficiary.put("transfertype", benewallet.getTransferType());
			beneficiary.put("accountno", benewallet.getAccountNo());
			beneficiary.put("ifsccode", benewallet.getIfscCode());
			beneficiary.put("status", benewallet.getStatus());
			respList.add(beneficiary);
		}
		return respList;
	}
	
	public MudraSenderBank createSenderBankIfNotExt(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "", "", "Inside createSenderBankIfNotExt method");
		try {
			mudraSenderBank = getSenderDtlbank(mudraSenderBank, serverName, requestId);
			
			if (mudraSenderBank.getStatusDesc().equalsIgnoreCase("Y")) {
				return mudraSenderBank;
			}else{
				mudraSenderBank.setMpin("0000");
				mudraSenderBank = registerSender(mudraSenderBank, serverName, requestId);
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "", "", "problem in"
					+ " createSenderBankIfNotExt method|e.message+"+e.getMessage());
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
		}

		return mudraSenderBank;
	}

	
	public MudraSenderBank registerSender(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*****RequestId*******" + requestId
				+"|registerSender()|"
				+ mudraSenderBank.getMobileNo());
	
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		Transaction transaction = null;
		String nmpin = "";
		try {
			if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()
					|| mudraSenderBank.getMobileNo().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}
			if (mudraSenderBank.getFirstName() == null || mudraSenderBank.getFirstName().isEmpty()) {
				mudraSenderBank.setStatusCode("1101");
				mudraSenderBank.setStatusDesc("First Name can't be empty.");
				return mudraSenderBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo())).list();
			if (senderList.size() == 0) {
						transaction = session.beginTransaction();
						mudraSenderBank.setId(commanUtilDao.getTrxId("MSENDER",mudraSenderBank.getAggreatorId()));
						mudraSenderBank.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.getMpin()));
						mudraSenderBank.setStatus("ACTIVE");
						mudraSenderBank.setKycStatus("MIN KYC");
						mudraSenderBank.setTransferLimit(25000.00);
						mudraSenderBank.setFavourite("");
						session.save(mudraSenderBank);
						transaction.commit();
						List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
								.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
								.add(Restrictions.ne("status", "Deleted"))
								.addOrder(Order.asc("status"))
								.list();
						mudraSenderBank.setBeneficiaryList(benfList);
						mudraSenderBank.setStatusCode("1000");
						mudraSenderBank.setStatusDesc("Sender has been registered successfully.");

					
			} else {
				mudraSenderBank.setStatusCode("11206");
				mudraSenderBank.setStatusDesc("Mobile Number Register With Us.");
				return mudraSenderBank;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*****RequestId*******" + requestId
					+ "problem in registerSender" + e.getMessage()+" "+e);
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;

	}
	
	public MudraBeneficiaryBank getBeneficiaryByAccountNo(String accountNo,String ifscCode,String senderId, String serverName) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName
				+ "|accountNo|"
				+ accountNo+"|senderId|"+senderId);
			factory = DBUtil.getSessionFactory();
			Session session = factory.openSession();

			Transaction transaction = null;

		MudraBeneficiaryBank bResult =null;
		try {
			bResult = (MudraBeneficiaryBank) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("accountNo", accountNo)).add(Restrictions.eq("senderId", senderId)).add(Restrictions.eq("ifscCode", ifscCode)).uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "problem in getBeneficiary" + e.getMessage()+" "+e);
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bResult;
	}
	
	public MudraBeneficiaryBank createBeneBankIfNotExt(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraBeneficiaryBank.getAggreatorId(), "", "", "", "", "Inside createBeneBankIfNotExt method|RequestId|"+requestId);
		Session session = null;

		Transaction transaction = null;

		try {
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			MudraBeneficiaryBank mudraBeneficiaryBankGet = getBeneficiaryByAccountNo(mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getSenderId(),serverName);
			if(mudraBeneficiaryBankGet != null )
			{
				mudraBeneficiaryBank = (MudraBeneficiaryBank)mudraBeneficiaryBankGet.cloneMe();
			}
			if(mudraBeneficiaryBankGet == null || mudraBeneficiaryBank.getStatus().equals("Deactive"))
			{
				if(!session.isConnected())
				{
					factory = DBUtil.getSessionFactory();
					session = factory.openSession();
				}
				int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
						+ "Number ofactive Beneficiary :" + activeCount);
				
				if (activeCount >= 15) {
					transaction = session.beginTransaction();
					MudraBeneficiaryBank bened = (MudraBeneficiaryBank)session.createCriteria(MudraBeneficiaryBank.class).add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
								.add(Restrictions.eq("status", "Active")).list().get(0);
					bened.setStatus("Deactive");
					session.update(bened);
					transaction.commit();
						
				}
				if(mudraBeneficiaryBankGet == null){
					mudraBeneficiaryBank = registerBeneficiary(mudraBeneficiaryBank,serverName,requestId);
				}else if(mudraBeneficiaryBank.getStatus().equals("Deactive"))
				{
					transaction = session.beginTransaction();
					mudraBeneficiaryBank.setStatus("Active");
					session.update(mudraBeneficiaryBank);
					transaction.commit();
				}
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraBeneficiaryBank.getAggreatorId(), "", "", "", "", "problem in"
					+ " createBeneBankIfNotExt method|e.message+"+e.getMessage());
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(session.isConnected())
			{session.close();}
			
		}

		return mudraBeneficiaryBank;
	}
	
	public MudraBeneficiaryBank registerBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
				+ "|registerBeneficiary()|"
				+ mudraBeneficiaryBank.getAccountNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1110");
				mudraBeneficiaryBank.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getName() == null || mudraBeneficiaryBank.getName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1101");
				mudraBeneficiaryBank.setStatusDesc("Invalid Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getBankName() == null || mudraBeneficiaryBank.getBankName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1107");
				mudraBeneficiaryBank.setStatusDesc("Invalid Bank Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getAccountNo() == null || mudraBeneficiaryBank.getAccountNo().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1108");
				mudraBeneficiaryBank.setStatusDesc("Invalid Account Number.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getIfscCode() == null || mudraBeneficiaryBank.getIfscCode().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1109");
				mudraBeneficiaryBank.setStatusDesc("Invalid IFSC Code.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getTransferType() == null || mudraBeneficiaryBank.getTransferType().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1111");
				mudraBeneficiaryBank.setStatusDesc("Invalid Transfer Type.");
				return mudraBeneficiaryBank;
			}

			List<MudraBeneficiaryBank> benList = session.createCriteria(MudraBeneficiaryBank.class)
				     .add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
				     .add(Restrictions.eq("accountNo", mudraBeneficiaryBank.getAccountNo()))
				     .add(Restrictions.ne("status","Deleted")).list();
			
			if (benList.size() > 0) {
				mudraBeneficiaryBank.setStatusCode("1124");
				mudraBeneficiaryBank.setStatusDesc("Your provided account number is already added with sender.");
				return mudraBeneficiaryBank;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			if (activeCount >= 15) {
				mudraBeneficiaryBank.setStatusCode("1123");
				mudraBeneficiaryBank.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
				return mudraBeneficiaryBank;
			}

			transaction = session.beginTransaction();
			mudraBeneficiaryBank.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryBank.getAggreatorId()));
			mudraBeneficiaryBank.setStatus("Active");
			mudraBeneficiaryBank.setVerified("NV");
			session.save(mudraBeneficiaryBank);
			transaction.commit();
			mudraBeneficiaryBank.setStatusCode("1000");
			mudraBeneficiaryBank.setStatusDesc("Beneficiary has been registered successfully.");

			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraBeneficiaryBank.getSenderId());


		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "problem in registerBeneficiary" + e.getMessage()+" "+e);
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");

			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}
	
	public static void main(String s[])
	{
		MudraSenderBank bk = new MudraSenderBank();
		bk.setMobileNo("9999215888");
		bk.setAggreatorId("OAGG001062");
		new ExtremmitapiDaoImpl().getSenderDtlbank(bk,"","");
	}
}
