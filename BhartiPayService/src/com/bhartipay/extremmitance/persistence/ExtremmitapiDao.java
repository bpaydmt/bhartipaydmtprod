package com.bhartipay.extremmitance.persistence;

import java.util.List;

import org.json.JSONException;

import com.bhartipay.dmt.bean.DMTInputBean;
import com.bhartipay.extremmitance.bean.FundTransferResponse;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.util.thirdParty.bean.WalletExist;

public interface ExtremmitapiDao {

	public MudraSenderBank getSenderDtlbank(MudraSenderBank mudraSenderBank, String serverName, String requestId);
	public MudraSenderWallet getSenderDtlWallet(MudraSenderWallet mudraSenderwallet, String serverName, String requestId);
	public FundTransferResponse fundTransferBank(String aggreatodId,MudraMoneyTransactionBean mudraMoneyTransactionBean,String operationType,String serverName, String requestId);
	public FundTransferResponse fundTransferWallet(MudraMoneyTransactionBean mudraMoneyTransactionBean,String operationType,String serverName, String requestId) ;
	public List getBeneficiaryListWallet(List<MudraBeneficiaryWallet> beneList);
	public List getBeneficiaryListBank(List<MudraBeneficiaryBank> beneList);
	public String saveRemitApiRequest(String aggregatorId,String reqVal,String serverName,String apiName);
	public MudraSenderBank createSenderBankIfNotExt(MudraSenderBank mudraSenderBank, String serverName, String requestId);
	public MudraBeneficiaryBank createBeneBankIfNotExt(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId);
}
