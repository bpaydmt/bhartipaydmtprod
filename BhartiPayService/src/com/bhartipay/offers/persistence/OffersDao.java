package com.bhartipay.offers.persistence;

import com.bhartipay.offers.bean.OffersCouponBean;

public interface OffersDao {
	
	/**
	 * 
	 * @param offersCouponBean
	 * @return
	 */
	public OffersCouponBean createOffers(OffersCouponBean offersCouponBean);

}
