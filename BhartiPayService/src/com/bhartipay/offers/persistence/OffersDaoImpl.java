package com.bhartipay.offers.persistence;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.offers.bean.OffersCouponBean;
import com.bhartipay.util.DBUtil;

public class OffersDaoImpl implements OffersDao{
	
	private static final Logger logger = Logger.getLogger(OffersDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	public OffersCouponBean createOffers(OffersCouponBean offersCouponBean){
		logger.info("start excution ===================== method createOffers(offersCouponBean)"+offersCouponBean.getCouponCode());
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 Transaction transaction = session.beginTransaction();
		 try{
			 
			 if(offersCouponBean.getCouponCode()==null ||offersCouponBean.getCouponCode().isEmpty()){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Coupon Code is empty."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getCouponType()==null ||offersCouponBean.getCouponType().isEmpty()){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Coupon type is empty."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getRate()<=0 ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Offer Rate not define."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getMaxOfferLimit()<=0 ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Max Offer Limit not define."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getValidFrom()==null ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Valid From not define."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getValidTo()==null ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Valid To not define."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getMinTxnAmount()<=0 ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Min transaction amount not define."); 
		         return offersCouponBean;
			 }
			 
			 if(offersCouponBean.getAllowed()<=0 ){
				 offersCouponBean.setStatusCode("1000");
		         offersCouponBean.setStatusMsg("Number of offer per user not define."); 
		         return offersCouponBean;
			 }
			 
			 session.save(offersCouponBean);
			 offersCouponBean.setStatusCode("300");
	         offersCouponBean.setStatusMsg("SUCCESS");
	         
		 }catch (HibernateException e) {
	         if (transaction!=null)transaction.rollback();
	         e.printStackTrace();
	         logger.debug("problem in createOffers***************************" + e.getMessage(), e);
	         offersCouponBean.setStatusCode("1000");
	         offersCouponBean.setStatusMsg("FAILED");
	         
	         
	      }finally {
	    	  session.close(); 
	      }
		 logger.info("Inside ****************************createOffers***** return===="+offersCouponBean.getStatusCode());
		
		
		
	return offersCouponBean;	
	}
	

}
