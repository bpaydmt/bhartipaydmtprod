package com.bhartipay.offers;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.offers.bean.OffersCouponBean;
import com.bhartipay.offers.persistence.OffersDao;
import com.bhartipay.offers.persistence.OffersDaoImpl;


public class OffersManager {
	private static final Logger logger = Logger.getLogger(OffersManager.class.getName());

	OffersDao offersDao=new OffersDaoImpl();
	
	
	@POST
	@Path("/createOffers")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public OffersCouponBean createOffers(OffersCouponBean offersCouponBean){
		 logger.info("Start excution ===================== method createOffers(offersCouponBean)"+offersCouponBean.getCouponCode()); 
		 return offersDao.createOffers(offersCouponBean);
	 }
	
	
	
	
}
