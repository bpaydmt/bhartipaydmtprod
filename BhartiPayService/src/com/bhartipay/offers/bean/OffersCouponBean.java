package com.bhartipay.offers.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name="offerscouponmast")
public class OffersCouponBean {
	
		
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable = false,length=5)
	private int id ;
	
	@Column(name="couponcode", nullable = false,length=5,unique=true)
	private String couponCode ;
	
	@Column(name="aggreatorid", nullable = false,length=20)
	private String aggreatorid ;
	
	@Column(name="type", nullable = false,length=20)
	private String couponType ;
	
	@Column(name="rate", nullable = false,length=20)
	private int rate ;
	
	@Column(name="maxofferlimit", nullable = false,length=20)
	private int maxOfferLimit ;
	
	@Temporal(TemporalType.DATE)
	@Column(name="validfrom")
	private Date validFrom;
	
	@Temporal(TemporalType.DATE)
	@Column(name="validto")
	private Date validTo;
	
	@Column(name="mintxnamount",nullable = false, columnDefinition="Decimal(12,2)")
	private double minTxnAmount;
	
	@Column(name="allowed", nullable = false,length=2)
	private int allowed ;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createddate;
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMsg;
	

	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	
		

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getMaxOfferLimit() {
		return maxOfferLimit;
	}

	public void setMaxOfferLimit(int maxOfferLimit) {
		this.maxOfferLimit = maxOfferLimit;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public double getMinTxnAmount() {
		return minTxnAmount;
	}

	public void setMinTxnAmount(double minTxnAmount) {
		this.minTxnAmount = minTxnAmount;
	}

	public int getAllowed() {
		return allowed;
	}

	public void setAllowed(int allowed) {
		this.allowed = allowed;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	

}
