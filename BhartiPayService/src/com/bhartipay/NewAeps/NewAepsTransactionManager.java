package com.bhartipay.NewAeps;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.NewAeps.bean.AepsRedirection;
import com.bhartipay.NewAeps.bean.UpiQrRequest;
import com.bhartipay.NewAeps.service.AepsTransactionDao;
import com.bhartipay.NewAeps.service.AepsTransactionDaoImpl;
import com.bhartipay.wallet.aeps.AEPSLedger;

@Path("/AepsApi")
public class NewAepsTransactionManager {

	public static Logger logger=Logger.getLogger(NewAepsTransactionManager.class);	
	AepsTransactionDao transactionDao =new AepsTransactionDaoImpl(); 
	
	
	@POST
	@Path("/aepsApiRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AepsRedirection saveNewAepsRequest(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	  System.out.println("=======================================");    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),"",aepsLedger.getAgentId(), "","" ,"saveAepsRequest()|"+agent);
		
		return transactionDao.saveNewAepsRequest(aepsLedger, imei, agent);
		
	}
	
	
	@POST
	@Path("/aepsApiEnquiry")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AepsRedirection saveNewAepsEnquiryRequest(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	  System.out.println("===============Balanced Enquiry========================");    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),"",aepsLedger.getAgentId(), "","" ,"saveAepsRequest()|"+agent);
		
		return transactionDao.saveNewAepsEnquiryRequest(aepsLedger, imei, agent);
		
	}
	
	@POST
	@Path("/makeAepsTxnStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AepsRedirection makeAepsTxnStatus(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	  System.out.println("===============Balanced Enquiry========================");    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),"",aepsLedger.getAgentId(), "","" ,"saveAepsRequest()|"+agent);
		
		return transactionDao.makeAepsTxnStatus(aepsLedger, imei, agent);
		
	}
	 
	
	@POST
	@Path("/upiRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String upiRequest(UpiQrRequest upi){
		 
	  System.out.println("===============upiRequest========================");    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"");
		
		return transactionDao.upiQrRequest(upi);
		
	}
	
	
	
	
	
}
