package com.bhartipay.NewAeps.bean;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.security.GeneralSecurityException;



import org.apache.commons.ssl.OpenSSL;
import java.io.*;

public class EncryptionByEnc256 {


Logger logger=Logger.getLogger(EncryptionByEnc256.class);
 public EncryptionByEnc256()
 {
 }

 
/* public static String encryptOpenSSL(String key,String data) throws IOException, GeneralSecurityException{
	   
	   OpenSSL opensll=new OpenSSL();
	   InputStream  is=OpenSSL.encrypt("AES256", key.getBytes(), new ByteArrayInputStream(data.getBytes()));
	   Base64 encode=new Base64();
	   
	    
	   BufferedReader in = new BufferedReader(new InputStreamReader(is));
	   String inputLine;
	  StringBuffer response = new StringBuffer();

	  while ((inputLine = in.readLine()) != null) {
	   response.append(inputLine);
	  }
	  in.close();
	     System.out.println(response);
	     return encode.encode(response.toString().getBytes());
	  }*/
	  
	  public static String decryptOpenSSL(String key,String data) throws IOException, GeneralSecurityException{
	   
	    OpenSSL opensll=new OpenSSL();
	    InputStream  is=OpenSSL.decrypt("AES256", key.getBytes(), new ByteArrayInputStream(data.getBytes()));
	    Base64 encode=new Base64();
	    
	     
	    BufferedReader in = new BufferedReader(new InputStreamReader(is));
	    String inputLine;
	   StringBuffer response = new StringBuffer();

	   while ((inputLine = in.readLine()) != null) {
	    response.append(inputLine);
	   }
	   in.close();
	      System.out.println(response);
	      //return encode.encode(response.toString().getBytes()).toString();
	      return response.toString();
	   }
public static String encryptOpenSSL(String data,String key) throws IOException, GeneralSecurityException{
	  
  	OpenSSL opensll=new OpenSSL();
  	InputStream  is=OpenSSL.encrypt("AES256", key.getBytes(), new ByteArrayInputStream(data.getBytes()), true);
  	BufferedReader in = new BufferedReader(new InputStreamReader(is));
  	String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
     System.out.println(response);
     return response.toString();
  }


 public static String encrypt(String textToEncrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(1, makeKey(key), makeIv());
         return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 public static String decrypt(String textToDecrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(2, makeKey(key), makeIv());
         return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 private static AlgorithmParameterSpec makeIv()
 {
     try
     {
         return new IvParameterSpec("0123456789abcdef".getBytes("UTF-8"));
     }
     catch(UnsupportedEncodingException e)
     {
         e.printStackTrace();
     }
     return null;
 }

 private static Key makeKey(String encryptionKey)
 {
     try
     {
         byte key[] = Base64.decodeBase64(encryptionKey.getBytes());
         return new SecretKeySpec(key, "AES");
     }
     catch(Exception e)
     {
         e.printStackTrace();
     }
     return null;
 }

 public static String generateMerchantKey()
 {
     String newKey = null;
     try
     {
         KeyGenerator kgen = KeyGenerator.getInstance("AES");
         kgen.init(256);
         SecretKey skey = kgen.generateKey();
         byte raw[] = skey.getEncoded();
         newKey = new String(Base64.encodeBase64(raw));
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
     return newKey;
 }

 private static final String ENCRYPTION_IV = "0123456789abcdef";
 private static final String PADDING = "AES/CBC/PKCS5Padding";
 private static final String ALGORITHM = "AES";
 private static final String CHARTSET = "UTF-8";

 static 
 {
     try
     {
         Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
         field.setAccessible(true);
         field.set(null, Boolean.FALSE);
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
 }
 
 public String decryptSBI(String encData,String merchantCode) {
     logger.info("decrypt(String encData, String merchantCode, String delimiter) method begin");
     String decdata=null;
      
     String path =merchantCode + ".key";

         byte[] key = null;
         try {
             key = returnbyte(path);
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
      
     try{
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding","SunJCE");
         logger.info("Provider Info " + cipher.getProvider().getInfo());
         byte[] keyBytes= new byte[16];
         //byte[] b= key.getBytes("UTF-8");
         int len= key.length;
         if (len > keyBytes.length) len = keyBytes.length;
         System.arraycopy(key, 0, keyBytes, 0, len);
         SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
         logger.info("After SecretKeySpec");
         IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
         cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);
         BASE64Decoder decoder = new BASE64Decoder();
         logger.info("before final");
         byte [] results= decoder.decodeBuffer(encData);
         byte [] ciphertext=cipher.doFinal(results);
         logger.info("after");
         decdata=new String(ciphertext,"UTF-8");
         System.out.println(decdata); 
     }catch(Exception ex){
         logger.info("Exception occured :" + ex);
         ex.printStackTrace();
     }
     logger.info("decrypt(String encData, String merchantCode, String delimiter) method end");
     return decdata;
 }

public String encryptSBI(String data,String merchantCode) {

     String path = merchantCode + ".key";

     byte[] key = null;
     try {
         key = returnbyte(path);
     } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
     }
  
     String encData=null;
     try{
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding","SunJCE");
         logger.info("Provider Info " + cipher.getProvider().getInfo());
         byte[] keyBytes= new byte[16];
         //byte[] b= path.getBytes("UTF-8");
         int len= key.length;
         if (len > keyBytes.length) len = keyBytes.length;
         System.arraycopy(key, 0, keyBytes, 0, len);
         SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
         IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
         cipher.init(Cipher.ENCRYPT_MODE,keySpec, ivSpec);
         byte[] results = cipher.doFinal(data.getBytes("UTF-8"));
         BASE64Encoder encoder = new BASE64Encoder();
         encData= encoder.encode(results);
         System.out.println(encData); 
          
     }catch(Exception ex){
         logger.info("Exception occured :" + ex);
     }
     logger.info("encrypt(String data,String merchantCode) method end");
     return encData;
 }

private byte[] returnbyte(String filepath) throws IOException {
Path path = Paths.get(filepath);
byte[] data = java.nio.file.Files.readAllBytes(path);
return data;
}
 
 public static void main(String[] args) {

	 try{
//	 String val = "hello";
//	 String encr = new EncryptionByEnc256().encryptOpenSSL( val,"db868108-5b65-4477-ab79-618d724eb3b8");
//	 System.out.println("encr "+encr );

	 System.out.println("dec "+ new EncryptionByEnc256().decryptOpenSSL("d1f01fcd-9b79-40ff-b93b-2c10b5ef856d","U2FsdGVkX19so+L8Ss+LXGqQxQGNbh/c/JFyLA785eTt7Q55LgAIzebTdH/bzExIUk1nv362snU1ZZ9npFp/8O7pPZKwTB/Av48URWdzgDPjXh8cBrbxUxPB8eb9+AxJGK/jPc3FlxRQNKiZMjqq/8AMg+HV6yTVUTrw5RSIvVEdv6VBkL12NcUOKZYe/5wf7L16HzqhpatW8h0srxyckTpx0kNooFCkG0CdkacEvd+Hu4igETZDmDw4glw2j5NE") );
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
	 }
 }
}
