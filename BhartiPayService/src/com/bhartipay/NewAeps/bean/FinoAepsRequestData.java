package com.bhartipay.NewAeps.bean;

public class FinoAepsRequestData {


	private String MerchantId;
	private String SERVICEID;
	private String RETURNURL;
	private String Version;
	private String Amount;
	private String ClientRefID;
	
	public String getMerchantId() {
		return MerchantId;
	}
	public void setMerchantId(String merchantId) {
		MerchantId = merchantId;
	}
	public String getSERVICEID() {
		return SERVICEID;
	}
	public void setSERVICEID(String sERVICEID) {
		SERVICEID = sERVICEID;
	}
	public String getRETURNURL() {
		return RETURNURL;
	}
	public void setRETURNURL(String rETURNURL) {
		RETURNURL = rETURNURL;
	}
	public String getVersion() {
		return Version;
	}
	public void setVersion(String version) {
		Version = version;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getClientRefID() {
		return ClientRefID;
	}
	public void setClientRefID(String clientRefID) {
		ClientRefID = clientRefID;
	}
	
	
	
}
