package com.bhartipay.NewAeps.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.NewAeps.bean.AepsRedirection;
import com.bhartipay.NewAeps.bean.EncryptionByEnc256;
import com.bhartipay.NewAeps.bean.FinoAepsHeader;
import com.bhartipay.NewAeps.bean.FinoAepsRequestData;
import com.bhartipay.NewAeps.bean.UpiQrRequest;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aeps.AEPSConfig;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.google.gson.Gson;

public class AepsTransactionDaoImpl implements AepsTransactionDao {

	private static final Logger logger = Logger.getLogger(TransactionDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();

	@Override
	public AepsRedirection saveNewAepsRequest(AEPSLedger aepsLedger, String ipimei, String userAgent) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
				aepsLedger.getAgentId(), "", " ipimei " + ipimei, " userAgent " + userAgent,
				"" + "saveNewAepsRequest()   --- " + aepsLedger);

		//aepsLedger.setStatus("1001");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		AepsRedirection aepsRed = new AepsRedirection();
		FinoAepsHeader finoHeader = new FinoAepsHeader();
		FinoAepsRequestData aepsRequest = new FinoAepsRequestData();
		AEPSConfig aepsconfig = null;
		WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, aepsLedger.getAgentId());
		if(mast!=null)
		{
		aepsLedger.setAggregator(mast.getAggreatorid());	
		if (mast.getAgentCode() == null || aepsLedger.getAgentCode().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(001);
			aepsRed.setMessage("Invalid Agent Code ");
			return aepsRed;
		}

		if (aepsLedger.getAmount() < 100) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(002);
			aepsRed.setMessage("Please enter amount between 100 to 10,000.");
			return aepsRed;
		}
/*
		if (aepsLedger.getAgentId() == null || aepsLedger.getAgentId().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(003);
			aepsRed.setMessage("Agent ID INVALID.");
			return aepsRed;
		}
*/
		try {
			String txn = "151";
			long ptyTrandDt = System.currentTimeMillis();
			aepsLedger.setPtyTransDt("" + ptyTrandDt);
			Timestamp timestamp = new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));
			aepsLedger.setTxnDate(timestamp);
			aepsLedger.setResponseHash("");
			aepsLedger.setType("AEPS");
			aepsLedger.setAepsChannel("2");
			aepsLedger.setTxnType("CASH WITHDRAWAL");
			aepsLedger.setStatus("Pending");
			String aepstxnid = commanUtilDao.getTrxId("AEPSAPI", aepsLedger.getAggregator());

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", " aepstxnid " + aepstxnid,
					"" + " saveNewAepsRequest()  --- " + aepsLedger);

			aepsLedger.setTxnId(aepstxnid);
			session.save(aepsLedger);
			transaction.commit();

			try {
				Criteria criteria = session.createCriteria(AEPSConfig.class);
				criteria.add(Restrictions.eq("aggregatorId", aepsLedger.getAggregator()));
				criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
				criteria.add(Restrictions.eq("type", aepsLedger.getType() != null ? aepsLedger.getType() : "2"));

				List<AEPSConfig> aepsConfigList = criteria.list();
				if (aepsConfigList != null && aepsConfigList.size() > 0) {
					aepsconfig = (aepsConfigList.get(0));
					finoHeader.setAuthKey(aepsconfig.getSecretKey());
					finoHeader.setClientId(aepsconfig.getMid());
					aepsRequest.setRETURNURL(aepsconfig.getAepsCallbackURL());

				}

				Gson gson = new Gson();
				String jsonHeader = gson.toJson(finoHeader);// Plain Header JSON Format

				String encryptedJsonHeader = EncryptionByEnc256.encryptOpenSSL(jsonHeader,
						"982b0d01-b262-4ece-a2a2-45be82212ba1"); // Encrypted Header JSON Format

				int amount = (int) aepsLedger.getAmount();
				aepsRequest.setAmount("" + amount);

				aepsRequest.setClientRefID(aepsLedger.getTxnId());
				aepsRequest.setMerchantId(aepsLedger.getAgentCode());
				aepsRequest.setSERVICEID(txn);
				aepsRequest.setVersion("1000");

				String jsonText = gson.toJson(aepsRequest);// Plain aepsRequest JSON Format
				if (aepsconfig != null) {
					String encryptedJsonText = EncryptionByEnc256.encryptOpenSSL(jsonText,
							aepsconfig.getAgentAuthPassword());

					aepsRed.setEncData(encryptedJsonText);
					aepsRed.setAuthentication(encryptedJsonHeader);
					aepsRed.setRedirectionUrl(aepsconfig.getRedirectionUrl());
					aepsRed.setStatus("SUCCESS");
					aepsRed.setStatusCode(000);
					aepsRed.setMessage("SUCCESS");
					return aepsRed;
				} else {
					aepsRed.setStatus("ERROR");
					aepsRed.setStatusCode(004);
					aepsRed.setMessage("Please try later");
					return aepsRed;
				}

			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
						aepsLedger.getAgentId(), "", "", "",
						"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

				aepsRed.setStatus("ERROR");
				aepsRed.setStatusCode(005);
				aepsRed.setMessage("Something went wrong");
				return aepsRed;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", "",
					"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(005);
			aepsRed.setMessage("Something went wrong");
			transaction.rollback();
		} finally {
			session.close();
		}
		}else
		{
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(003);
			aepsRed.setMessage("Agent ID INVALID.");
			return aepsRed;	
		}
		return aepsRed;
	}

	@Override
	public AepsRedirection saveNewAepsEnquiryRequest(AEPSLedger aepsLedger, String ipimei, String userAgent) {


		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
				aepsLedger.getAgentId(), "", " ipimei " + ipimei, " userAgent " + userAgent,
				"" + "saveNewAepsRequest()   --- " + aepsLedger);

		//aepsLedger.setStatus("1001");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		AepsRedirection aepsRed = new AepsRedirection();
		FinoAepsHeader finoHeader = new FinoAepsHeader();
		FinoAepsRequestData aepsRequest = new FinoAepsRequestData();
		AEPSConfig aepsconfig = null;
		WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, aepsLedger.getAgentId());
		if(mast!=null)
		{
		aepsLedger.setAggregator(mast.getAggreatorid());	
		if (aepsLedger.getAgentCode() == null || aepsLedger.getAgentCode().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(001);
			aepsRed.setMessage("Invalid Agent Code ");
			return aepsRed;
		}
 
/*
		if (aepsLedger.getAggregator() == null || aepsLedger.getAggregator().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(003);
			aepsRed.setMessage("Agent ID INVALID.");
			return aepsRed;
		}
*/
		try {
			String txn = "152";
			long ptyTrandDt = System.currentTimeMillis();
			aepsLedger.setPtyTransDt("" + ptyTrandDt);
			Timestamp timestamp = new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));
			aepsLedger.setTxnDate(timestamp);
			aepsLedger.setResponseHash("");
			aepsLedger.setType("AEPS");
			aepsLedger.setAepsChannel("2");
			aepsLedger.setTxnType("BALANCE ENQUIRY");
			aepsLedger.setStatus("Pending");
			String aepstxnid = commanUtilDao.getTrxId("AEPSAPI", aepsLedger.getAggregator());

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", " aepstxnid " + aepstxnid,
					"" + " saveNewAepsRequest()  --- " + aepsLedger);

			aepsLedger.setTxnId(aepstxnid);
			session.save(aepsLedger);
			transaction.commit();

			try {
				Criteria criteria = session.createCriteria(AEPSConfig.class);
				criteria.add(Restrictions.eq("aggregatorId", aepsLedger.getAggregator()));
				criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
				criteria.add(Restrictions.eq("type", aepsLedger.getType() != null ? aepsLedger.getType() : "2"));

				List<AEPSConfig> aepsConfigList = criteria.list();
				if (aepsConfigList != null && aepsConfigList.size() > 0) {
					aepsconfig = (aepsConfigList.get(0));
					finoHeader.setAuthKey(aepsconfig.getSecretKey());
					finoHeader.setClientId(aepsconfig.getMid());
					aepsRequest.setRETURNURL(aepsconfig.getAepsCallbackURL());

				}

				Gson gson = new Gson();
				String jsonHeader = gson.toJson(finoHeader);// Plain Header JSON Format

				String encryptedJsonHeader = EncryptionByEnc256.encryptOpenSSL(jsonHeader,
						"982b0d01-b262-4ece-a2a2-45be82212ba1"); // Encrypted Header JSON Format

				int amount = (int) aepsLedger.getAmount();
				aepsRequest.setAmount("" + amount);

				aepsRequest.setClientRefID(aepsLedger.getTxnId());
				aepsRequest.setMerchantId(aepsLedger.getAgentCode());
				aepsRequest.setSERVICEID(txn);
				aepsRequest.setVersion("1000");

				String jsonText = gson.toJson(aepsRequest);// Plain aepsRequest JSON Format
				if (aepsconfig != null) {
					String encryptedJsonText = EncryptionByEnc256.encryptOpenSSL(jsonText,
							aepsconfig.getAgentAuthPassword());

					aepsRed.setEncData(encryptedJsonText);
					aepsRed.setAuthentication(encryptedJsonHeader);
					aepsRed.setRedirectionUrl(aepsconfig.getRedirectionUrl());
					aepsRed.setStatus("SUCCESS");
					aepsRed.setStatusCode(000);
					aepsRed.setMessage("SUCCESS");
					return aepsRed;
				} else {
					aepsRed.setStatus("ERROR");
					aepsRed.setStatusCode(004);
					aepsRed.setMessage("Please try later");
					return aepsRed;
				}

			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
						aepsLedger.getAgentId(), "", "", "",
						"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

				aepsRed.setStatus("ERROR");
				aepsRed.setStatusCode(005);
				aepsRed.setMessage("Something went wrong");
				return aepsRed;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", "",
					"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(005);
			aepsRed.setMessage("Something went wrong");
			transaction.rollback();
		} finally {
			session.close();
		}
		}else
		{
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(003);
			aepsRed.setMessage("Agent ID INVALID.");
			return aepsRed;
		}
		return aepsRed;
		
	}

	@Override
	public AepsRedirection makeAepsTxnStatus(AEPSLedger aepsLedger, String ipimei, String userAgent) {


		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
				aepsLedger.getAgentId(), "", " ipimei " + ipimei, " userAgent " + userAgent,
				"" + "makeAepsTxnStatus()   --- " + aepsLedger);

		//aepsLedger.setStatus("1001");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		AepsRedirection aepsRed = new AepsRedirection();
		FinoAepsHeader finoHeader = new FinoAepsHeader();
		FinoAepsRequestData aepsRequest = new FinoAepsRequestData();
		AEPSConfig aepsconfig = null;
		WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, aepsLedger.getAgentId());
		if(mast!=null)
		{
		aepsLedger.setAggregator(mast.getAggreatorid());	
		if (aepsLedger.getAgentCode() == null || aepsLedger.getAgentCode().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(001);
			aepsRed.setMessage("Invalid Agent Code ");
			return aepsRed;
		}
 

		if (aepsLedger.getTxnId() == null || aepsLedger.getTxnId().isEmpty()) {
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(006);
			aepsRed.setMessage("INVALID Transaction ID.");
			return aepsRed;
		}

		try {
			String txn = "154";
			long ptyTrandDt = System.currentTimeMillis();
			aepsLedger.setPtyTransDt("" + ptyTrandDt);
			Timestamp timestamp = new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));
			aepsLedger.setTxnDate(timestamp);
			aepsLedger.setResponseHash("");
			aepsLedger.setType("AEPS");
			aepsLedger.setAepsChannel("2");
			aepsLedger.setTxnType("STATUS ENQUIRY");
			aepsLedger.setStatus("Pending");
			String aepstxnid = commanUtilDao.getTrxId("AEPSAPI", aepsLedger.getAggregator());

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", " aepstxnid " + aepstxnid,
					"" + " saveNewAepsRequest()  --- " + aepsLedger);

			aepsLedger.setTxnId(aepstxnid);
			session.save(aepsLedger);
			transaction.commit();

			try {
				Criteria criteria = session.createCriteria(AEPSConfig.class);
				criteria.add(Restrictions.eq("aggregatorId", aepsLedger.getAggregator()));
				criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
				criteria.add(Restrictions.eq("type", aepsLedger.getType() != null ? aepsLedger.getType() : "2"));

				List<AEPSConfig> aepsConfigList = criteria.list();
				if (aepsConfigList != null && aepsConfigList.size() > 0) {
					aepsconfig = (aepsConfigList.get(0));
					finoHeader.setAuthKey(aepsconfig.getSecretKey());
					finoHeader.setClientId(aepsconfig.getMid());
					aepsRequest.setRETURNURL(aepsconfig.getAepsCallbackURL());

				}

				Gson gson = new Gson();
				String jsonHeader = gson.toJson(finoHeader);// Plain Header JSON Format

				String encryptedJsonHeader = EncryptionByEnc256.encryptOpenSSL(jsonHeader,
						"982b0d01-b262-4ece-a2a2-45be82212ba1"); // Encrypted Header JSON Format

				int amount = (int) aepsLedger.getAmount();
				aepsRequest.setAmount("" + amount);

				aepsRequest.setClientRefID(aepsLedger.getTxnId());
				aepsRequest.setMerchantId(aepsLedger.getAgentCode());
				aepsRequest.setSERVICEID(txn);
				aepsRequest.setVersion("1000");

				String jsonText = gson.toJson(aepsRequest);// Plain aepsRequest JSON Format
				if (aepsconfig != null) {
					String encryptedJsonText = EncryptionByEnc256.encryptOpenSSL(jsonText,
							aepsconfig.getAgentAuthPassword());

					aepsRed.setEncData(encryptedJsonText);
					aepsRed.setAuthentication(encryptedJsonHeader);
					aepsRed.setRedirectionUrl(aepsconfig.getRedirectionUrl());
					aepsRed.setStatus("SUCCESS");
					aepsRed.setStatusCode(000);
					aepsRed.setMessage("SUCCESS");
					return aepsRed;
				} else {
					aepsRed.setStatus("ERROR");
					aepsRed.setStatusCode(004);
					aepsRed.setMessage("Please try later");
					return aepsRed;
				}

			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
						aepsLedger.getAgentId(), "", "", "",
						"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

				aepsRed.setStatus("ERROR");
				aepsRed.setStatusCode(005);
				aepsRed.setMessage("Something went wrong");
				return aepsRed;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aepsLedger.getAggregator(),
					aepsLedger.getAgentId(), "", "", "",
					"saveNewAepsRequest()" + "|problem in Add  aeps Txn" + e.getMessage() + " " + e);

			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(005);
			aepsRed.setMessage("Something went wrong");
			transaction.rollback();
		} finally {
			session.close();
		}
		}else
		{
			aepsRed.setStatus("ERROR");
			aepsRed.setStatusCode(003);
			aepsRed.setMessage("Aggreator ID INVALID.");
			return aepsRed;
		}
		return aepsRed;
		
	}

	@Override
	public String upiQrRequest(UpiQrRequest req) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","VPA  "+req.getVpa(), " BILL No  "+req.getBillno()," AMOUNT "+req.getAmount() ," MODE "+req.getPaymode());
		
		String resp=null;
		try {
			
			URL url= new URL("https://www.upiqrcode.com/upi-qr-code-api/v01/?apikey=cwxkqo&seckey=brtpay&vpa="+req.getVpa()+"&billno="+req.getBillno()+"&amount="+req.getAmount()+"&paymode="+req.getPaymode() );
			
			String readLine=null;
			HttpURLConnection connection =(HttpURLConnection) url.openConnection(); 
			connection.setRequestMethod("GET");
			int responseCode=connection.getResponseCode();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", " responseCode "+responseCode, "", "", "url Request " , ""+url);	
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
				StringBuffer response=new StringBuffer();
				while((readLine = in.readLine()) != null) {
					response.append(readLine);
					
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", " responseCode "+responseCode, "", "", "readLine " , ""+readLine);	
				} in.close();

               resp= response.toString();
           	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "responseCode "+responseCode, "", "", " " , "resp "+resp);
			}
			 
		} catch (IOException e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "resp "+resp, "", " " , ""+e.getMessage());
			e.printStackTrace();
		}
		
		return resp;
	}

	
	
	
	
	
}
