package com.bhartipay.NewAeps.service;

import com.bhartipay.NewAeps.bean.AepsRedirection;
import com.bhartipay.NewAeps.bean.UpiQrRequest;
import com.bhartipay.wallet.aeps.AEPSLedger;

public interface AepsTransactionDao {

	public AepsRedirection saveNewAepsRequest(AEPSLedger aepsLedger,String ipimei,String userAgent);
	
	public AepsRedirection saveNewAepsEnquiryRequest(AEPSLedger aepsLedger,String ipimei,String userAgent);
	
	public AepsRedirection makeAepsTxnStatus(AEPSLedger aepsLedger,String ipimei,String userAgent);
	
	public String  upiQrRequest(UpiQrRequest req);
	
}
