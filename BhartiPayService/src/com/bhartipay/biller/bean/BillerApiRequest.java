package com.bhartipay.biller.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "billerapirequestlog")
public class BillerApiRequest {

	
	@Id
	@Column(name="requestid",length =50)
	private String requestId;
	
	@Column(name="aggreatorid",length =50)
	private String aggreatorId;
	
	@Column(name="requestvalue",length =500)
	private String requestValue;
	
	@Column(name="apiname",length =500)
	private String apiName;
	
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date requestdate;



	public String getRequestId() {
		return requestId;
	}



	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}



	public String getAggreatorId() {
		return aggreatorId;
	}



	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}



	public String getRequestValue() {
		return requestValue;
	}



	public void setRequestValue(String requestValue) {
		this.requestValue = requestValue;
	}



	public String getApiName() {
		return apiName;
	}



	public void setApiName(String apiName) {
		this.apiName = apiName;
	}



	public Date getRequestdate() {
		return requestdate;
	}



	public void setRequestdate(Date requestdate) {
		this.requestdate = requestdate;
	}
	
	
	
	
	
	
	
	
}
