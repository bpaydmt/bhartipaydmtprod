package com.bhartipay.biller.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "billerdetails")
public class BillerURLDetailsBean {
	
	@Id
	@Column(name="billerid",length =50)	
	private int billerId;
	
	@Column(name="billername",length =500)
	private String billerName;
	
	@Column(name="billertype",length =20)
	private String billerType;
	
	@Column(name="billerstatus",length =5)	
	private int billerStatus;
	
	
	@Column(name="billerclient",length =200)
	private String billerClient;

	@Column(name="verificationurl",length =300)
	private String verificationUrl;
	
	@Column(name="paymenturl",length =300)	
	private String paymentUrl;
	
	@Column(name="statusurl",length =300)
	private String statusUrl;
	
	@Column(name="servicecharge")
	private Double serviceCharge;
	
	@Column(name="billericon",length =300)
	private String BillerIcon;
	
	
	
	
	

	public int getBillerId() {
		return billerId;
	}

	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}

	public String getBillerName() {
		return billerName;
	}

	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}

	public String getBillerType() {
		return billerType;
	}

	public void setBillerType(String billerType) {
		this.billerType = billerType;
	}

	public int getBillerStatus() {
		return billerStatus;
	}

	public void setBillerStatus(int billerStatus) {
		this.billerStatus = billerStatus;
	}

	public String getBillerClient() {
		return billerClient;
	}

	public void setBillerClient(String billerClient) {
		this.billerClient = billerClient;
	}

	public String getVerificationUrl() {
		return verificationUrl;
	}

	public void setVerificationUrl(String verificationUrl) {
		this.verificationUrl = verificationUrl;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getStatusUrl() {
		return statusUrl;
	}

	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}

	public Double getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(Double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public String getBillerIcon() {
		return BillerIcon;
	}

	public void setBillerIcon(String billerIcon) {
		BillerIcon = billerIcon;
	}
		

	
	


}
