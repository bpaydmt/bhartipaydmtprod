package com.bhartipay.biller.bean;

public class BillerDetailsBean {
	
	private int billerId;
	private String billerName;
	private String billerIcon;
	
	
	
	
	
	public int getBillerId() {
		return billerId;
	}
	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}
	public String getBillerName() {
		return billerName;
	}
	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}
	public String getBillerIcon() {
		return billerIcon;
	}
	public void setBillerIcon(String billerIcon) {
		this.billerIcon = billerIcon;
	}
	
	
	
	
	
}
