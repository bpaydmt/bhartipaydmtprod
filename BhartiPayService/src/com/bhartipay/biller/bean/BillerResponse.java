package com.bhartipay.biller.bean;





public class BillerResponse {
	
	private String statusCode;
	private String status;
	private String statusMsg;
	private PayLoadBean resPayload=new PayLoadBean();
	
	
	
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public PayLoadBean getResPayload() {
		return resPayload;
	}
	public void setResPayload(PayLoadBean resPayload) {
		this.resPayload = resPayload;
	}
	
	
	
	
	
	

}
