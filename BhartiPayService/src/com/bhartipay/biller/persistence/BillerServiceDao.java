package com.bhartipay.biller.persistence;

import java.util.List;

import com.bhartipay.biller.bean.BillerDetailsBean;

public interface BillerServiceDao {
	
	/**
	 * 
	 * @param aaggreatorId
	 * @param reqVal
	 * @param serverName
	 * @param apiName
	 * @return
	 */
	public String saveBillerApiRequest(String aaggreatorId,String reqVal,String serverName,String apiName);
	
	/**
	 * 
	 * @param billerType
	 * @param serverName
	 * @param requsetId
	 * @return
	 */
	public List<BillerDetailsBean> getBillerDetails(String billerType,String serverName,String requsetId);
	

}
