package com.bhartipay.biller.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.biller.bean.BillerApiRequest;
import com.bhartipay.biller.bean.BillerDetailsBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class BillerServiceDaoImpl implements BillerServiceDao {
	
	private static final Logger logger = Logger.getLogger(BillerServiceDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	
	
public String saveBillerApiRequest(String aaggreatorId,String reqVal,String serverName,String apiName){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aaggreatorId,"","", "","", serverName +  "|saveBillerApiRequest() |reqVal"+reqVal
			);
		//logger.info(serverName+"Start excution ************************************* saveBillerApiRequest agentId**:"	+ aaggreatorId);
		//logger.info(serverName+"Start excution ************************************* saveBillerApiRequest reqVal**:"	+ reqVal);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String requestId="";
		try{
			transaction=session.beginTransaction();
			BillerApiRequest billerApiRequest=new BillerApiRequest();
			
			
			billerApiRequest.setRequestId(Long.toString(generateLongId())+commanUtilDao.getTrxId("BILLERREQ",aaggreatorId));
			billerApiRequest.setAggreatorId(aaggreatorId);
			billerApiRequest.setRequestValue(reqVal);
			billerApiRequest.setApiName(apiName);
			
			
			session.save(billerApiRequest);
			transaction.commit();
			requestId=billerApiRequest.getRequestId();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aaggreatorId,"","", "","", serverName +  "|saveBillerApiRequest requestId"+requestId
					);
			//logger.info(serverName+" excution ************************************* saveBillerApiRequest requestId**:"	+ requestId);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aaggreatorId,"","", "","", serverName +  "|problem in saveBillerApiRequest"+e.getMessage()+" "+e
					);
			//logger.debug(serverName+"problem in saveBillerApiRequest=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
			} finally {
			session.close();
			}
		return requestId;
	}
	
	
	
	
	
	
	
	public List<BillerDetailsBean> getBillerDetails(String billerType,String serverName,String requsetId){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", serverName +"|requsetId" +requsetId+ "|getBillerDetails()"
				);
		//logger.info(serverName+"*****"+requsetId+"start excution******************** method getBillerDetails");
		  List<BillerDetailsBean> billerList=new ArrayList<BillerDetailsBean>();
		  factory=DBUtil.getSessionFactory();
		  Session session = factory.openSession();
		  try{
			  SQLQuery query;
			  if(billerType.equalsIgnoreCase("Electricity")){
				  query=session.createSQLQuery("SELECT billerid billerId,billername billerName,billericon billerIcon FROM billerdetails WHERE billertype='Electricity' AND billerstatus=1");
			  }else if(billerType.equalsIgnoreCase("Gas")){
				  query=session.createSQLQuery("SELECT billerid billerId,billername billerName,billericon billerIcon FROM billerdetails WHERE billertype='Gas' AND billerstatus=1"); 
			  }else if(billerType.equalsIgnoreCase("Insurance")){
				  query=session.createSQLQuery("SELECT billerid billerId,billername billerName,billericon billerIcon FROM billerdetails WHERE billertype='Insurance' AND billerstatus=1"); 
			  }else if(billerType.equalsIgnoreCase("Landline")){
				  query=session.createSQLQuery("SELECT billerid billerId,billername billerName,billericon billerIcon FROM billerdetails WHERE billertype='Landline' AND billerstatus=1"); 
			  }else{
				  return billerList;
			  }
			  query.setResultTransformer(Transformers.aliasToBean(BillerDetailsBean.class));
			  query.addScalar("billerId").addScalar("billerName").addScalar("billerIcon");
			   billerList=query.list();
		  }catch(Exception e){
			   e.printStackTrace();
			     transaction.rollback();
			 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", serverName +"|requsetId" +requsetId+ "|problem in getBillerDetaisl"+e.getMessage()+" "+e
						);
			   //  logger.debug(serverName+"*****"+requsetId+"problem in getBillerDetaisl*******************************" + e.getMessage(), e);
			    }
			    finally{
			     session.close();
			   }
		  return billerList;
	}
	
	
	
	
	

	
	
	
	 private static final long twepoch = 1288834974657L;
	 private static final long sequenceBits = 17;
	 private static final long sequenceMax = 65536;
	 private static volatile long lastTimestamp = -1L;
	 private static volatile long sequence = 0L;
	
	
	private static synchronized Long generateLongId() {
        long timestamp = System.currentTimeMillis();
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) % sequenceMax;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0;
        }
        lastTimestamp = timestamp;
        Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
        return id;
    }

    private static long tilNextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }
    
    
    
    public static void main(String[] args) {
		new BillerServiceDaoImpl().getBillerDetails("Electricity", "aaaaa", "11111");
	}
	
}
