package com.bhartipay.biller.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.json.XML;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.DBUtil;


public class UpdateBillStatus extends TimerTask{
	public static final Logger logger=Logger.getLogger(UpdateBillStatus.class);
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	TransactionDao transactionDao=new TransactionDaoImpl();

	@Override
	public synchronized void run() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular started"
				);

		updateRechargeBillStatus();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular finished his task"
				);
	}
	

	
	public void updateRechargeBillStatus() {

		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction txn = session.beginTransaction();
		   
		 try {
				SQLQuery query1 = session.createSQLQuery(
						"DELETE  FROM benedtl WHERE creationtime  < DATE_SUB(NOW(), INTERVAL 10 MINUTE) ");
				query1.executeUpdate(); 
				txn.commit();
			}catch(Exception e)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in Deleting BeneDtl "+e.getMessage()+" "+e);
				e.printStackTrace();
				txn.rollback();
			}
		
		
		try {
			Query query = session .createQuery("from RechargeTxnBean where status in('PENDING','In process') and rechargeType in ('PREPAID','DTH','LANDLINE','POSTPAID','Electricity','Gas','Insurance','Datacard') and rechargeDate < :rechargeDate");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new java.util.Date());
			cal.add(Calendar.MINUTE, -10);
			Timestamp timestamp = new Timestamp(cal.getTime().getTime());
			query.setTimestamp("rechargeDate", timestamp);
//			query.setString("status", "PENDING");
			List<RechargeTxnBean> list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," total pending checkMobileDTHStatus Bill "+list.size()
					);
			logger.info("total pending checkMobileDTHStatus Bill ***************************************:"+list.size());
			Iterator<RechargeTxnBean> ite = list.iterator();
			while (ite.hasNext()) {
				RechargeTxnBean rechargeTxnBean=ite.next();
				String trxId =rechargeTxnBean.getRechargeId();
				transaction=session.beginTransaction();
				try {
				
					  String url="https://www.rechargebill.in/apiservice.asmx/GetRechargeStatus?apiToken=b5c63372351e4649ab5314751c82dbc7&reqid="+trxId;
							
					  logger.info("Start excution **************** reqPara" + url);	
					  DefaultHttpClient httpClient = new DefaultHttpClient();
					  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"getRecharge Status()"+trxId);
					 
				     HttpGet getRequest = new HttpGet(url);
				     getRequest.addHeader("accept", "application/xml");
				     HttpResponse response = httpClient.execute(getRequest);
				     int statusCode = response.getStatusLine().getStatusCode();
				     if (statusCode != 200) 
				     {
				         throw new RuntimeException("Failed with HTTP error code : " + statusCode);
				     }
				     HttpEntity httpEntity = response.getEntity();
				     String apiOutput = EntityUtils.toString(httpEntity);
				     
				     System.out.println(apiOutput);
				     org.json.JSONObject jsonObject = XML.toJSONObject(apiOutput); 
				     org.json.JSONObject jsonObj = jsonObject.getJSONObject("Result");
 
					
					String status="";
					String txid = "";
					String orderid = "";
					if(jsonObj != null)
					 {
						 if(jsonObj.has("reqid") && jsonObj.get("reqid")!=null) 
							 txid = jsonObj.get("reqid").toString();
						  
						  if(jsonObj.has("status") && jsonObj.get("status")!=null)
							  status =jsonObj.get("status").toString(); 
						  
						  if(jsonObj.has("apirefid") && jsonObj.get("apirefid")!=null) 
							  orderid = jsonObj.get("apirefid").toString();
						
					 }
					 logger.info("Api Response status **************** txid " + txid+" status   "+status+" orderid  "+orderid);	
						
						if ("Success".equalsIgnoreCase(status)) {
							rechargeTxnBean.setStatus("SUCCESS");
					    	rechargeTxnBean.setOther(rechargeTxnBean.getOther() + "|"+status);
					    	rechargeTxnBean.setOperatorTxnId(txid);
					    	
						}
						 
						else if("Failure".equalsIgnoreCase(status) || "FAILED".equalsIgnoreCase(status) || "REFUND".equalsIgnoreCase(status) || "NOTFOUND".equalsIgnoreCase(status)){
							Criteria rechCriteria = session.createCriteria(TransactionBean.class);
							rechCriteria.add(Restrictions.eq("resptxnid", rechargeTxnBean.getRechargeId()));
							List<TransactionBean> summarylist = (List<TransactionBean>)rechCriteria.list();
							
						int txn_5 = 0;
						int txn_6 = 0;
						int txn_93 = 0;
						int txn_94 = 0;
						
						int txn_119 = 0;
						int txn_120 = 0;
						int txn_145 = 0;
						int txn_146 = 0;
						
						
							
						TransactionBean txBean = null;
						for (TransactionBean txnBean : summarylist) {
							if (txnBean.getTxncode() == 5) {
								txn_5 = 5;
							}
							if (txnBean.getTxncode() == 6) {
								txn_6 = 6;
							}
							if (txnBean.getTxncode() == 93) {
								txn_93 = 93;
								txBean = txnBean;
							}
							if (txnBean.getTxncode() == 94) {
								txn_94 = 94;
							}
							
							if (txnBean.getTxncode() == 119) {
								txn_119 = 119;
							}
							if (txnBean.getTxncode() == 120) {
								txn_120 = 120;
							}
							if (txnBean.getTxncode() == 145) {
								txn_145 = 145;
							}
							if (txnBean.getTxncode() == 146) {
								txn_146 = 146;
							}
						}
						
						if(txn_5 == 5 && txn_6 == 0 ) {
							transactionDao.settlement(rechargeTxnBean.getUserId(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getRechargeAmt(),rechargeTxnBean.getRechargeId(),rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getUseragent());
							if(txn_93 == 93 && txn_94 == 0) {
								transactionDao.wlRechargeSettlement(txBean.getUserid(), txBean.getWalletid(), txBean.getTxndebit(), txBean.getResptxnid(), rechargeTxnBean.getIpIemi(), rechargeTxnBean.getRechargeNumber(), txBean.getAggreatorid(), rechargeTxnBean.getUseragent());
							}
							try {
								new TransactionDaoImpl().processRechargeCommission(rechargeTxnBean.getRechargeId(), rechargeTxnBean.getUserId(), rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeAmt(), "debit");
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
						
					    	rechargeTxnBean.setStatus("FAILED");
					    	rechargeTxnBean.setOperatorTxnId(txid);
					    	rechargeTxnBean.setOther(rechargeTxnBean.getOther() + "|"+status);
						}else if("Pending".equalsIgnoreCase(status)) {
							rechargeTxnBean.setStatus("In process");
					    	//rechargeTxnBean.setOperatorTxnId(txid);
					    	//rechargeTxnBean.setOther(rechargeTxnBean.getOther() + "|"+status);
						}
	  
				 
				    session.saveOrUpdate(rechargeTxnBean);
	 				transaction.commit();
	 				
				}catch (Exception e) {
					e.printStackTrace();
					if (transaction != null)
						transaction.rollback();
				}
			}
			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in checkMobileDTHStatus"+e.getMessage()+" "+e);
			e.printStackTrace();
			
		} finally {
			session.close();
		}
	
	}
	
	//public static void main(String[] args) {
	//   new	UpdateBillStatus().updateRechargeBillStatus();
 	//}

}

