package com.bhartipay.biller.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;
import com.bhartipay.lean.bean.BbpsStatusUpdateResponse;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.DBUtil;

public class UpdateBbpsBillStatus extends TimerTask 
{
public static final Logger logger=Logger.getLogger(UpdateBbpsBillStatus.class);
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	TransactionDao transactionDao=new TransactionDaoImpl();

	@Override
	public synchronized void run() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular started"
				);

		updateBbpsStatus();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular finished his task");
	}
	
	
	public void updateBbpsStatus() 
	{		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {
			//Query query = session .createQuery("from BbpsPayment where status= :status ");
			
			Query query = session .createQuery("from BbpsPayment where status= :status and requestdate < :requestdate");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new java.util.Date());
			cal.add(Calendar.MINUTE, -10);
			Timestamp timestamp = new Timestamp(cal.getTime().getTime());
			query.setTimestamp("requestdate", timestamp);
			query.setString("status", "PENDING");
			List<BbpsPayment> list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," total pending BBPS Bill  "+list.size());
			logger.info("total pending BBPS Bill ***************************************:"+list.size());
			Iterator<BbpsPayment> ite = list.iterator();
			while (ite.hasNext()) {
				BbpsPayment bbpsRecharge=ite.next();
				String reqId =bbpsRecharge.getRequestid();
				if(reqId!=null && !reqId.equalsIgnoreCase("")) {
				transaction=session.beginTransaction();
			 try {
			    BbpsStatusUpdateResponse bbpsStatus = new BillerBbpsServiceImpl().bbpsStatusUpdateByReqId(reqId);
			
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS status check response ");
				if(bbpsStatus!=null) {	
				String status = bbpsStatus.getStatus();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS status check response "+status);
				
				if ("Success".equalsIgnoreCase(status)) {
					String txnid = bbpsStatus.getTxnId();
			    	bbpsRecharge.setTxnid(txnid);
					bbpsRecharge.setStatus("Successful");
			    	bbpsRecharge.setRemark("Successful");
			    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS status check SUCCESS response ");
			    }
				else if("Failure".equalsIgnoreCase(status) || "FAILED".equalsIgnoreCase(status) ){
					Criteria rechCriteria = session.createCriteria(TransactionBean.class);
					rechCriteria.add(Restrictions.eq("resptxnid",bbpsRecharge.getResptxnid() ));
					List<TransactionBean> summarylist = (List<TransactionBean>)rechCriteria.list();
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",bbpsRecharge.getResptxnid(),"  BBPS status Failed list "+summarylist.size());
				
				int txn_28=0;
				int txn_29=0;
				int txn_134=0;
				int txn_135=0;
				
				TransactionBean txBean = null;
				for (TransactionBean txnBean : summarylist) {
					if (txnBean.getTxncode() == 28) {
						txn_28 = 28;
					}
					if (txnBean.getTxncode() == 29) {
						txn_29 = 29;
					}
					if (txnBean.getTxncode() == 134) {
						txn_134 = 134;
					}
					if (txnBean.getTxncode() == 135) {
						txn_135 = 135;
					}
				  }	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS txn code"+txn_28+" "+txn_29+"  "+txn_134+"  "+txn_135);
					
				if(txn_134==134 && txn_135 == 0)
				{
					String queryWalletId = "select walletid from walletmast where id='"+bbpsRecharge.getAggreatorid()+"' AND whitelabel=1 ";
					SQLQuery creditWalletQueryWalletId = session.createSQLQuery(queryWalletId);
					Object WalletIdObj = creditWalletQueryWalletId.uniqueResult();
					String queryStr = "call creditwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS whitelabel aggregator"+bbpsRecharge.getAggreatorid()+"  WalletId "+WalletIdObj);
					       
					if(WalletIdObj!=null)
					{
					if(txn_28==28 && txn_29 == 0)
					{
					    SQLQuery creditWalletQueryAgent = session.createSQLQuery(queryStr);
						creditWalletQueryAgent.setParameter("aggregatorid", bbpsRecharge.getAggreatorid());
						creditWalletQueryAgent.setParameter("walletid", bbpsRecharge.getWalletid());
						creditWalletQueryAgent.setParameter("amount", bbpsRecharge.getTxnamount());
						creditWalletQueryAgent.setParameter("narration", "bbps recharge refund");
						creditWalletQueryAgent.setParameter("resptxnid", bbpsRecharge.getResptxnid());
						creditWalletQueryAgent.setParameter("txncode", 135);
						
						int i = creditWalletQueryAgent.executeUpdate();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS whitelabel aggregator and agent "+i);
						
						SQLQuery creditWalletQueryAgg = session.createSQLQuery(queryStr);
						creditWalletQueryAgg.setParameter("aggregatorid", bbpsRecharge.getAggreatorid());
						creditWalletQueryAgg.setParameter("walletid", WalletIdObj);
						creditWalletQueryAgg.setParameter("amount", bbpsRecharge.getTxnamount());
						creditWalletQueryAgg.setParameter("narration", "bbps recharge refund");
						creditWalletQueryAgg.setParameter("resptxnid", bbpsRecharge.getResptxnid());
						creditWalletQueryAgg.setParameter("txncode", 29);
						int j = creditWalletQueryAgg.executeUpdate();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS whitelabel aggregator and agent "+j);
					    bbpsRecharge.setStatus("FAILED");
					    bbpsRecharge.setRemark("FAILED");
						}
					  }else
						{
						SQLQuery creditWalletQueryAgent = session.createSQLQuery(queryStr);
						creditWalletQueryAgent.setParameter("aggregatorid", bbpsRecharge.getAggreatorid());
						creditWalletQueryAgent.setParameter("walletid", bbpsRecharge.getWalletid());
						creditWalletQueryAgent.setParameter("amount", bbpsRecharge.getTxnamount());
						creditWalletQueryAgent.setParameter("narration", "bbps recharge refund");
						creditWalletQueryAgent.setParameter("resptxnid", bbpsRecharge.getResptxnid());
						creditWalletQueryAgent.setParameter("txncode", 135);
						int k = creditWalletQueryAgent.executeUpdate();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS bhartipay and agent "+k);
					    bbpsRecharge.setStatus("FAILED");
					    bbpsRecharge.setRemark("FAILED");
						 }
				 	}

				  }
			     session.saveOrUpdate(bbpsRecharge);
 				 
		      }else {
		    	 bbpsRecharge.setStatus("FAILED");
				 bbpsRecharge.setRemark("FAILED"); 
				 session.saveOrUpdate(bbpsRecharge);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  BBPS status check not found response ");
		      }
			  transaction.commit();
			 }catch (Exception e) {
				 if(transaction!=null) {
					 transaction.rollback();
				 }
				e.printStackTrace();
			 }
		   }
		  }
		 }catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in checkMobileDTHStatus"+e.getMessage()+" "+e);
	    	e.printStackTrace();
	     } finally {
		  session.close();
	     }
			
	 }
/*
	public static void main(String[] args) {
		new UpdateBbpsBillStatus().updateBbpsStatus();;
	}
*/	
}
