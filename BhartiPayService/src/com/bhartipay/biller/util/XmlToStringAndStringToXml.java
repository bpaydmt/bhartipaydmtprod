package com.bhartipay.biller.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlToStringAndStringToXml {
	
	private Document convertXMLFileToXMLDocument(String filePath) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document xmlDocument = builder.parse(new File(filePath));
             
            return xmlDocument;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }
     
	private String writeXmlDocumentToXmlFile(Document xmlDocument)
    {
    	String xmlString = null;
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
             
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
            xmlString = writer.getBuffer().toString();   
        } 
        catch (TransformerException e) 
        {
            e.printStackTrace();
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return xmlString;
    }
    
    
    private static Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException{
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document document = docBuilder.parse(new InputSource(new StringReader(str)));
       
        return document;
   }

    public String getBillersListFromXml(String filePath) {
    	return writeXmlDocumentToXmlFile(convertXMLFileToXMLDocument(filePath));
    }
    
    public boolean setBillersListToXml(String xmlData, String fileName) {
    	
    	boolean isXmlWrite = false;
    	
    	try {
			Document xmlDocument = toXmlDocument(xmlData);
			
			TransformerFactory tf = TransformerFactory.newInstance();
	        Transformer transformer;
	        try {
	            transformer = tf.newTransformer();
	             
	            StringWriter writer = new StringWriter();
	            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
	             
	            //Write XML to file
	            FileOutputStream outStream = new FileOutputStream(new File(fileName)); 
	            transformer.transform(new DOMSource(xmlDocument), new StreamResult(outStream));
	            isXmlWrite = true;
	        } 
	        catch (TransformerException e) 
	        {
	            e.printStackTrace();
	        }
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return isXmlWrite;
    }
}
