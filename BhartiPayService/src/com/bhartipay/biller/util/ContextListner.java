package com.bhartipay.biller.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cache.CacheFactory;
import com.bhartipay.cache.SchedularThread;
import com.bhartipay.lean.bean.ActivationCharge;

public class ContextListner implements ServletContextListener {
	public static final Logger logger = Logger.getLogger(ContextListner.class.getName());

	Timer t = new Timer();

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		t.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		CacheFactory.getCache();

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "contextInitialized()");
		

		ActivationCharge activationCharge = new ActivationCharge(); 
		int delay11 = 1000 * 60;
		int interval11 = 1000 * 60 * 1;
		 
		try {
			t.scheduleAtFixedRate(activationCharge, delay11, interval11);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		UpdateBillStatus updateBillStatus = new UpdateBillStatus();
		SchedularThread schedularThread = new SchedularThread();
		int delay = 1000 * 60;
		int interval = 1000 * 60 * 5;
		
		
		try {
			t.scheduleAtFixedRate(updateBillStatus, delay, interval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		UpdateBbpsBillStatus updateBbpsStatus=new UpdateBbpsBillStatus();
		try {
			t.scheduleAtFixedRate(updateBbpsStatus, delay, interval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			t.scheduleAtFixedRate(schedularThread, delay, interval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ReportEmailer reportEmailer = new ReportEmailer();
		int interval1 = 1000 * 60 * 60 * 24;
		
		
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date myDate = format.parse(format.format(new Date()));
			myDate = DateUtils.addDays(myDate, 1);
			myDate = DateUtils.addMinutes(myDate, 5);
			t.schedule(reportEmailer, myDate, interval1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		
		try {
			LowBalanceAlert balanceAlert = new LowBalanceAlert();
			t.scheduleAtFixedRate(balanceAlert, delay, interval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "context Listener Stop");

	}

}
