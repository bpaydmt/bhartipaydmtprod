package com.bhartipay.biller.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.bhartipay.user.bean.WalletBalanceBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;

public class LowBalanceAlert extends TimerTask{

	
	public static final Logger logger = Logger.getLogger(LowBalanceAlert.class);

	@Override
	public void run() {
		new LowBalanceAlert().sendAlert();
	}
	
	public void sendAlert() {

		SessionFactory factory;
		Transaction transaction = null;
		Session session = null;

			factory = DBUtil.getSessionFactory();
			session = factory.openSession();

			try {
				Criteria criteria = session.createCriteria(LowBalanceNotificationDetails.class);
				criteria.add(Restrictions.eq("flag","1"));
				List<LowBalanceNotificationDetails> list = criteria.list();
				Iterator<LowBalanceNotificationDetails> ite = list.iterator();
				
				 
				SimpleDateFormat formater = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
				Calendar cal  = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				String dateString = formater.format(new Date(cal.getTimeInMillis()));
				
				while (ite.hasNext()) {
					LowBalanceNotificationDetails lowBalanceNotificationDetails = ite.next();
					WalletBalanceBean walletBalance =(WalletBalanceBean) session.get(WalletBalanceBean.class, lowBalanceNotificationDetails.getCashWalletId());
					
					if(walletBalance != null && "0".equalsIgnoreCase(lowBalanceNotificationDetails.getSmsSentFlag()) && walletBalance.getFinalBalance() <= lowBalanceNotificationDetails.getLowAmount()) {
						String mobileNos = lowBalanceNotificationDetails.getMobileNo();
						String mobileNoArr[] = mobileNos.split(",");
						WalletMastBean walletBean = (WalletMastBean)session.get(WalletMastBean.class, lowBalanceNotificationDetails.getAggregatorId());
						String smsTemplate = "ALERT : Low Balance! You have only Rs.<<final balance>> in your <<name>> account on <<date>>.".replace("<<final balance>>", ""+walletBalance.getFinalBalance()).replace("<<name>>", walletBean.getName()).replace("<<date>>", dateString);
						for (String mobile : mobileNoArr) {
							SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
									mobile,
									smsTemplate,
									"SMS",walletBean.getId(), "", "", "OTP");
							ThreadUtil.getThreadPool().execute(smsAndMailUtility);
						}
						
						lowBalanceNotificationDetails.setSmsSentFlag("1");
						transaction = session.beginTransaction();
						session.saveOrUpdate(lowBalanceNotificationDetails);
						transaction.commit();
						
					}
				}
			}catch (Exception e) {
				if(transaction!=null) {
					transaction.rollback();
				}
				e.printStackTrace();
			}finally {
				session.close();
			}
	}
	
	public static void main(String[] args) {
		new LowBalanceAlert().sendAlert();
	}
}
