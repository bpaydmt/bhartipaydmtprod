package com.bhartipay.biller.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.report.persistence.ReportDao;
import com.bhartipay.report.persistence.ReportDaoImpl;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.CommUtility;

public class ReportEmailer extends TimerTask {

	public static final String FILE_LOCATION = "/home/ambuj/";
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	
	@Override
	public void run() {
		
		SimpleDateFormat formater = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal  = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String startDate = formater.format(new Date(cal.getTimeInMillis()));
		
		ReportEmailer rmp= new ReportEmailer();
		try {
			rmp.generateWalletTransactionReport();
		}catch (Exception e) {
			e.printStackTrace();
		}
		try {
			new CommUtility().sendMail("akhil.sharma@bhartipay.com,akhand.pratap@bhartipay.com,vikas.kumar@bhartipay.com,sachin.mittal@bhartipay.com", "Wallet Transaction Report_"+startDate, "Dear Team, This is auto generated email. please do not reply.", FILE_LOCATION+"WalletReportZIP.zip", "OAGG001050", "", "Bhartipay");
		}catch (Exception e) {
			e.printStackTrace();
		}
		try {
		rmp.generateCashDepositReport();
		}catch (Exception e) {
			e.printStackTrace();
		}
		try {
		new CommUtility().sendMail("akhil.sharma@bhartipay.com,akhand.pratap@bhartipay.com,vikas.kumar@bhartipay.com,sachin.mittal@bhartipay.com", "Cash Deposit Report_"+startDate, "Dear Team, This is auto generated email. please do not reply.", FILE_LOCATION+"CashDepositReportZIP.zip", "OAGG001050", "", "Bhartipay");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void generateWalletTransactionReport() {
		
		ReportDao reportDao = new ReportDaoImpl();
		SimpleDateFormat formater = new SimpleDateFormat("dd-MMM-yyyy");
	
		
		Calendar cal  = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String startDate = formater.format(new Date(cal.getTimeInMillis()));
		List<WalletTxnDetailsRpt> report = reportDao.getWalletTxnDetailsReport(startDate,startDate,"All"); 
		
		File dirFile = new File(FILE_LOCATION+"WalletReport");
		
		if (!dirFile.exists()){
			dirFile.mkdirs();
		}else {
			
			File[] files = dirFile.listFiles();
	        for (int i = 0; i < files.length; i++) {
	                files[i].delete();
	        }
			
			boolean deleteStatus = dirFile.delete();
			System.out.println(deleteStatus);
			if (!dirFile.exists()){
				dirFile.mkdirs();
			}
		}
		
		File file = new File(FILE_LOCATION+"WalletReport/Wallet_Transaction_Report_"+startDate+".csv");
		try {
			file.setReadable(true);
			file.createNewFile();
			file.setReadable(true);
			
			FileOutputStream fos = new FileOutputStream(file);
			BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append("Id,Name,Date,Txn ID,Resp Txn ID,Credit,Debit,Final Balance,Description");
			bufferedWriter.write(strBuilder.toString());
			bufferedWriter.newLine();
			
			for (WalletTxnDetailsRpt rpt : report) {
				strBuilder = new StringBuilder();
				
				strBuilder.append(rpt.getId());
				strBuilder.append(","+(rpt.getName()));
				strBuilder.append(","+rpt.getTxndate());
				strBuilder.append(","+rpt.getTxnid());
				strBuilder.append(","+rpt.getResptxnid());
				strBuilder.append(","+rpt.getTxncredit());
				strBuilder.append(","+rpt.getTxndebit());
				strBuilder.append(","+rpt.getClosingbal());
				strBuilder.append(","+rpt.getTxndesc());
				
				
				bufferedWriter.write(strBuilder.toString());
				bufferedWriter.newLine();
			}
			
			bufferedWriter.flush();
			bufferedWriter.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ZipHelper zipHelper = new ZipHelper();
		try {
			zipHelper.zipDir(FILE_LOCATION+"WalletReport", FILE_LOCATION+"WalletReportZIP.zip");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void generateCashDepositReport() {
		
		TransactionDao taDao = new TransactionDaoImpl();
		SimpleDateFormat formater = new SimpleDateFormat("dd-MMM-yyyy");
	
		
		Calendar cal  = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String startDate = formater.format(new Date(cal.getTimeInMillis()));
		List<CashDepositMast> report = taDao.getCashDepositReportByAggId("OAGG001050",startDate,startDate,"0");
		File dirFile = new File(FILE_LOCATION+"CashDepositReport");
		
		if (!dirFile.exists()){
			dirFile.mkdirs();
		}else {
			
			File[] files = dirFile.listFiles();
	        for (int i = 0; i < files.length; i++) {
	                files[i].delete();
	        }
			
			boolean deleteStatus = dirFile.delete();
			System.out.println(deleteStatus);
			if (!dirFile.exists()){
				dirFile.mkdirs();
			}
		}
		
		File file = new File(FILE_LOCATION+"CashDepositReport/Cash_Deposit_Report_"+startDate+".csv");
		try {
			file.setReadable(true);
			file.createNewFile();
			file.setReadable(true);
			
			FileOutputStream fos = new FileOutputStream(file);
			BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(
					"User Type,User Name,Id,User Id,Type,Amount,Reference No.,Bank Name,Branch Name,Status,Remark,Request Date,Approve Date");
			bufferedWriter.write(strBuilder.toString());
			bufferedWriter.newLine();
			
			for (CashDepositMast tdo : report) {
				strBuilder = new StringBuilder();
				
				strBuilder.append((tdo.getUserId().contains("OAGG")==true?"Aggregator":tdo.getUserId().contains("A")==true?"Agent":tdo.getUserId().contains("M")==true?"CME":tdo.getUserId().contains("D")==true?"Distributor":tdo.getUserId().contains("OXSD")==true?"Super-Distributor":"-"));
				strBuilder.append(","+tdo.getAgentname());
				strBuilder.append(","+tdo.getDepositId());
				strBuilder.append(","+tdo.getUserId());
				strBuilder.append(","+tdo.getType());
				strBuilder.append(","+tdo.getAmount());
				strBuilder.append(","+tdo.getNeftRefNo());
				strBuilder.append(","+tdo.getBankName());
				strBuilder.append(","+tdo.getBranchName());
				strBuilder.append(","+tdo.getStatus());
				strBuilder.append(","+tdo.getRemark());
				strBuilder.append(","+tdo.getRequestdate());
				strBuilder.append(","+tdo.getApproveDate());
				
	
				bufferedWriter.write(strBuilder.toString());
				bufferedWriter.newLine();
			}
			
			bufferedWriter.flush();
			bufferedWriter.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ZipHelper zipHelper = new ZipHelper();
		try {
			zipHelper.zipDir(FILE_LOCATION+"CashDepositReport", FILE_LOCATION+"CashDepositReportZIP.zip");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args) throws ParseException {
//		ReportEmailer rmp= new ReportEmailer();
//		rmp.generateCashDepositReport();
//		new CommUtility().sendMail("ambuj.singh@bhartipay.com,akhand.pratap@bhartipay.com", "Testmail", "ambuj singh", FILE_LOCATION+"CashDepositReportZIP.zip", "OAGG001050", "", "ambuj");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date myDate = format.parse(format.format(new Date()));
		myDate = DateUtils.addDays(myDate, 1);
		myDate = DateUtils.addMinutes(myDate, 5);
		System.out.println(myDate);
	}
}
