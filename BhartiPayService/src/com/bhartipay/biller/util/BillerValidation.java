package com.bhartipay.biller.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;



import org.apache.log4j.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.biller.bean.BillerDetailsBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.CyberPlateBillerImpl;




public class BillerValidation {
	
	private static final Logger logger = Logger.getLogger(BillerValidation.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	Properties billVerificationProp=null;
	Properties billPaymentProp=null;
	TransactionDao transactionDao=new TransactionDaoImpl();
	
	public boolean billerIdVerification(String billerType,int billerId,String serverName,String requestId){
		boolean isBillerId=false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", serverName +"|requsetId" +requestId+ "|billerIdVerification()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method billerIdVerification");
		  List<BillerDetailsBean> billerList=new ArrayList<BillerDetailsBean>();
		  factory=DBUtil.getSessionFactory();
		  Session session = factory.openSession();
		  String billerName = new String();
		  try{
			  billerName =(String)session.createQuery("SELECT billerName from BillerURLDetailsBean  where billerId = :billerId and billerType=:billerType").setInteger("billerId", billerId).setString("billerType", billerType).uniqueResult();
			  if(!(billerName==null ||billerName.isEmpty())){
				  isBillerId=true;  
			  }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", serverName +"|requsetId" +requestId+ "|problem in billerIdVerification"+e.getMessage()+" "+e
				);
	      //logger.debug(serverName+"*****"+requestId+"problem in billerIdVerification=========================" + e.getMessage(), e);
	      e.printStackTrace();
	      transaction.rollback();
	      } finally {
	      session.close();
	      }
return isBillerId;
	}
	
	
	
	public Properties getElBillVerification(String userId,String aggreatorId,int billerId,String billerNumber,String serverName,String requestId,String ipIemi,String userAgent,String billerAccount,String additionalInfo)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getElBillVerification()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getElBillVerification");
		billVerificationProp=new Properties();
		String verifyresp =new CyberPlateBillerImpl().elBillVerification( Integer.toString(billerId), billerNumber, commanUtilDao.getTrxId("BILLPAY",aggreatorId), 1.79,billerAccount,additionalInfo);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getElBillVerification resp first time"+verifyresp
				);
		//logger.info(serverName+"*****"+requestId+"***********getElBillVerification resp first time*******************`"+verifyresp );
		StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
		while (st.hasMoreTokens()) {  
			 String tok = (String) st.nextToken();
			 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
			     tok.substring(tok.indexOf('=') + 1)); 
	     } 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("ERROR") 
				);	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("RESULT") 
						);
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("ERROR") );
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("RESULT") );
			
		if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==7 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==1){
			 factory=DBUtil.getSessionFactory();
			 Session session = factory.openSession();
			try{
			//if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==0 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==0){
			if(Double.parseDouble(billVerificationProp.getProperty("PRICE"))>0){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.getProperty("PRICE")
						);
				//logger.info(serverName+"*****"+requestId+"***************************"+billVerificationProp.getProperty("PRICE"));
		   	
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
		    RechargeTxnBean rechargeTxnBean=new RechargeTxnBean();	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|save initial biller txn"
					);
		    //logger.info(serverName+"*****"+requestId+"*******************save initial biller txn********");
		   // transaction=session.beginTransaction()
		    rechargeTxnBean.setUserId(userId);
		    rechargeTxnBean.setWalletId(walletMastBean.getWalletid());
		    rechargeTxnBean.setAggreatorid(aggreatorId);
		    rechargeTxnBean.setRechargeNumber(billerNumber);
		    if(billerId==235 || billerId==342 ||billerId==332){
		    rechargeTxnBean.setAccountNumber(billerAccount);
		    }else{
		     rechargeTxnBean.setAccountNumber(billerNumber);
		    }
		    
		    if(billerId==342){
			    rechargeTxnBean.setProcessingCycle(additionalInfo);
			}else{
			     rechargeTxnBean.setProcessingCycle("NA");
			}
		    
		    
		    rechargeTxnBean.setRechargeType("Electricity");
		    rechargeTxnBean.setRechargeCircle("India");
		    rechargeTxnBean.setRechargeOperator(Integer.toString(billerId));
		    rechargeTxnBean.setRechargeAmt(Double.parseDouble(billVerificationProp.getProperty("PRICE")));
		    rechargeTxnBean.setStatus("Cancel");
		    rechargeTxnBean.setIpIemi(ipIemi);
		    rechargeTxnBean.setUseragent(userAgent);
		    String rechargeId=commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid()); 
		    rechargeTxnBean.setRechargeId(rechargeId);
		    session.save(rechargeTxnBean);
		    session.beginTransaction().commit();
		    
			verifyresp =new CyberPlateBillerImpl().elBillVerification(Integer.toString(billerId), billerNumber, rechargeId, Double.parseDouble(billVerificationProp.getProperty("PRICE")),billerAccount,additionalInfo);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getElBillVerification resp first time"+verifyresp
					);
			//logger.info(serverName+"*****"+requestId+"***********getElBillVerification resp first time*******************`"+verifyresp );
			billVerificationProp.clear();
			billVerificationProp.put("id",rechargeId);
			  st=new StringTokenizer(verifyresp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     } 
				
		}
		}catch(Exception e){
		e.printStackTrace();	
		}finally {
			session.close();
		}
			
			}
		return billVerificationProp;
		
	}
	
	
	
	
	public Properties getElBillPayment(String userId,String aggreatorId,String txnId, double billerAmount,String serverName,String requestId,String ipIemi,String userAgent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getElBillPayment()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getElBillPayment");
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 billPaymentProp=new Properties();
		 try{
			 RechargeTxnBean rechargeTxnBean=(RechargeTxnBean) session.get(RechargeTxnBean.class, txnId);
			 if(rechargeTxnBean!=null &&rechargeTxnBean.getStatus().equalsIgnoreCase("Cancel")){
				 	if(billerAmount==rechargeTxnBean.getRechargeAmt()){
				 		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
				 	
				 		String walletResp;
				 		double cashBackWalletBalance=transactionDao.getCashBackWalletBalance(walletMastBean.getWalletid());
				 		
				 		if(cashBackWalletBalance >= rechargeTxnBean.getRechargeAmt()){
				 			walletResp= transactionDao.billPaymentFromCashBack(userId, walletMastBean.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), txnId, ipIemi, aggreatorId, userAgent, 28);
				 			if(walletResp.equals("1000")){
				 				transaction=session.beginTransaction();
				 				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 				rechargeTxnBean.setUseCashBack("Y");
				 				session.update(rechargeTxnBean);
				 				transaction.commit();
				 			}
				 		}else{
				 			walletResp= transactionDao.billPayment(userId, walletMastBean.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), txnId, ipIemi, aggreatorId, userAgent, 28);
				 		}
				 		
				 		
				 		if(walletResp.equals("1000")){
				 			String verifyresp =new CyberPlateBillerImpl().elBillPayment( rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeNumber(), txnId,billerAmount,rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getProcessingCycle());	
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getElBillPayment resp first time"+verifyresp
				 					);
				 			//logger.info(serverName+"*****"+requestId+"***********getElBillPayment resp first time*******************`"+verifyresp );
				 			StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
				 			
				 			while (st.hasMoreTokens()) {  
				 				String tok = (String) st.nextToken();
				 				billPaymentProp.put(tok.substring(0, tok.indexOf('=')),
				 				tok.substring(tok.indexOf('=') + 1)); 
				 		     } 
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("ERROR")
				 					);
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("RESULT")
				 					);
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("ERROR"));
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("RESULT"));
				 			
				 			if( Integer.parseInt(billPaymentProp.getProperty("ERROR").toString())==0 &&Integer.parseInt(billPaymentProp.getProperty("RESULT").toString())==0){
				 				transaction=session.beginTransaction();
					 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
					 			int result=Integer.parseInt(billPaymentProp.getProperty("TRNXSTATUS").toString());
				 				if(result==3){
				 					rechargeTxnBean.setStatus("PENDING");	
				 				}else if(result==7){
				 					rechargeTxnBean.setStatus("SUCCESS");
				 				}
				 				rechargeTxnBean.setOther(verifyresp);
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 					rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 				
				 			}else{
				 				transactionDao.settlement(userId,walletMastBean.getWalletid(),rechargeTxnBean.getRechargeAmt(),txnId,ipIemi,rechargeTxnBean.getRechargeNumber(),aggreatorId,userAgent);
				 				
				 				transaction = session.beginTransaction();
				 				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 				rechargeTxnBean.setStatus("FAILED");
				 				rechargeTxnBean.setOther("["+billPaymentProp.getProperty("ERROR")+"]["+verifyresp+"]");
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 				rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 			}
					 			
				 		}else{
				 			transaction=session.beginTransaction();
				 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 			rechargeTxnBean.setStatus("Failed");
				 			rechargeTxnBean.setWallettxnStatus(walletResp);
				 			session.update(rechargeTxnBean);
				 			transaction.commit();
				 			billPaymentProp.put("ERROR", "9999"); 
					 		billPaymentProp.put("RESULT", "1");
				 			}
				 	}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
				 		}}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
			 }
			 
		 }catch (Exception e) {				
				e.printStackTrace();
				billPaymentProp.put("ERROR", "9999"); 
		 		billPaymentProp.put("RESULT", "1");
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|problem in getElBillPayment"+e.getMessage()+" "+e
						);
				//logger.debug(serverName+"*****"+requestId+"problem in getElBillPayment==================" + e.getMessage(), e);
				
			} finally {
				session.close();
			}
		 
		return billPaymentProp;
		
	}
	
	
	public Properties getGasBillVerification(String userId,String aggreatorId,int billerId,String billerConsumerID,String accountNo,String serverName,String requestId,String ipIemi,String userAgent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getGasBillVerification()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getGasBillVerification");
		billVerificationProp=new Properties();
		String verifyresp =new CyberPlateBillerImpl().gasbillVerification( Integer.toString(billerId),billerConsumerID, accountNo, commanUtilDao.getTrxId("BILLPAY",aggreatorId), 1.79);
		//	public String gasbillVerification(String billerId,String consumerID,String account,String sessionId,Double amount)
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getGasBillVerification resp first time"+verifyresp
				);
		//logger.info(serverName+"*****"+requestId+"***********getGasBillVerification resp first time*******************`"+verifyresp );
		StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
		while (st.hasMoreTokens()) {  
			 String tok = (String) st.nextToken();
			 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
			     tok.substring(tok.indexOf('=') + 1)); 
	     } 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("ERROR") 
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("RESULT") 
				);
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("ERROR") );
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("RESULT") );
		
		if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==7 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==1){
			if(Double.parseDouble(billVerificationProp.getProperty("PRICE"))>0){
				 factory=DBUtil.getSessionFactory();
				    Session session = factory.openSession();	
				    try{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.getProperty("PRICE")
						);
				//logger.info(serverName+"*****"+requestId+"***************************"+billVerificationProp.getProperty("PRICE"));
		   	
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
		    RechargeTxnBean rechargeTxnBean=new RechargeTxnBean();	
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|save initial biller txn"
					);
		    //logger.info(serverName+"*****"+requestId+"*******************save initial biller txn********");
		    
		    rechargeTxnBean.setUserId(userId);
		    rechargeTxnBean.setWalletId(walletMastBean.getWalletid());
		    rechargeTxnBean.setAggreatorid(aggreatorId);
		    rechargeTxnBean.setRechargeNumber(billerConsumerID);
		    rechargeTxnBean.setConsumerId(billerConsumerID);
		    rechargeTxnBean.setAccountNumber(accountNo);
		    rechargeTxnBean.setRechargeType("Gas");
		    rechargeTxnBean.setRechargeCircle("India");
		    rechargeTxnBean.setRechargeOperator(Integer.toString(billerId));
		    rechargeTxnBean.setRechargeAmt(Double.parseDouble(billVerificationProp.getProperty("PRICE")));
		    rechargeTxnBean.setStatus("Cancel");
		    rechargeTxnBean.setIpIemi(ipIemi);
		    rechargeTxnBean.setUseragent(userAgent);
		    String rechargeId=commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid()); 
		    rechargeTxnBean.setRechargeId(rechargeId);
		    session.save(rechargeTxnBean);
		    session.beginTransaction().commit();
			verifyresp =new CyberPlateBillerImpl().gasbillVerification(Integer.toString(billerId),billerConsumerID, accountNo, rechargeId, Double.parseDouble(billVerificationProp.getProperty("PRICE")));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getGasBillVerification resp first time"+verifyresp
					);
			//logger.info(serverName+"*****"+requestId+"***********getGasBillVerification resp first time*******************`"+verifyresp );
			billVerificationProp.clear();
			  st=new StringTokenizer(verifyresp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     } 
				billVerificationProp.put("id",rechargeId);
		}
				    catch (Exception e) {
						e.printStackTrace();
					}finally {
						session.close();
					}	    
			}
			
		}
		return billVerificationProp;
		
	}

	
	public Properties getGasBillPayment(String userId,String aggreatorId,String txnId, double billerAmount,String serverName,String requestId,String ipIemi,String userAgent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getGasBillPayment()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getGasBillPayment");
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 billPaymentProp=new Properties();
		 try{
			 RechargeTxnBean rechargeTxnBean=(RechargeTxnBean) session.get(RechargeTxnBean.class, txnId);
			 if(rechargeTxnBean!=null &&rechargeTxnBean.getStatus().equalsIgnoreCase("Cancel")){
				 	if(billerAmount==rechargeTxnBean.getRechargeAmt()){
				 		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
				 		String walletResp= transactionDao.billPayment(userId, walletMastBean.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), txnId, ipIemi, aggreatorId, userAgent, 29);
				 		if(walletResp.equals("1000")){
				 			String verifyresp =new CyberPlateBillerImpl().gasbillPayment( rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAccountNumber(), txnId,billerAmount);	
				 			//public String gasbillPayment(String billerId,String consumerID,String account,String sessionId,Double amount)
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getGasBillPayment resp first time"+verifyresp
				 					);
				 			
				 			//logger.info(serverName+"*****"+requestId+"***********getGasBillPayment resp first time*******************`"+verifyresp );
				 			StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
				 			
				 			while (st.hasMoreTokens()) {  
				 				String tok = (String) st.nextToken();
				 				billPaymentProp.put(tok.substring(0, tok.indexOf('=')),
				 				tok.substring(tok.indexOf('=') + 1)); 
				 		     } 
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("ERROR") 
				 					);
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("RESULT") 
				 					);
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("ERROR") );
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("RESULT") );
				 			
				 			if( Integer.parseInt(billPaymentProp.getProperty("ERROR").toString())==0 &&Integer.parseInt(billPaymentProp.getProperty("RESULT").toString())==0){
				 				transaction=session.beginTransaction();
					 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
					 			int result=Integer.parseInt(billPaymentProp.getProperty("TRNXSTATUS").toString());
				 				if(result==3){
				 					rechargeTxnBean.setStatus("PENDING");	
				 				}else if(result==7){
				 					rechargeTxnBean.setStatus("SUCCESS");
				 				}
				 				rechargeTxnBean.setOther(verifyresp);
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 					rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 				
				 			}else{
				 				transactionDao.settlement(userId,walletMastBean.getWalletid(),rechargeTxnBean.getRechargeAmt(),txnId,ipIemi,rechargeTxnBean.getRechargeNumber(),aggreatorId,userAgent);
				 				
				 				transaction = session.beginTransaction();
				 				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 				rechargeTxnBean.setStatus("FAILED");
				 				rechargeTxnBean.setOther("["+billPaymentProp.getProperty("ERROR")+"]["+verifyresp+"]");
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 				rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 			}
					 			
				 		}else{
				 			transaction=session.beginTransaction();
				 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 			rechargeTxnBean.setStatus("Failed");
				 			rechargeTxnBean.setWallettxnStatus(walletResp);
				 			session.update(rechargeTxnBean);
				 			transaction.commit();
				 			billPaymentProp.put("ERROR", "9999"); 
					 		billPaymentProp.put("RESULT", "1");
				 			}
				 	}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
				 		}}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
			 }
			 
		 }catch (Exception e) {				
				e.printStackTrace();
				billPaymentProp.put("ERROR", "9999"); 
		 		billPaymentProp.put("RESULT", "1");
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|problem in getElBillPayment"+e.getMessage()+" "+e
	 					);
				//logger.debug(serverName+"*****"+requestId+"problem in getElBillPayment==================" + e.getMessage(), e);
				
			} finally {
				session.close();
			}
		 
		return billPaymentProp;
		
	}
	
	
	
	public Properties getInsuranceBillVerification(String userId,String aggreatorId,int billerId,String billGrpNumber,String accountNo,String serverName,String requestId,String ipIemi,String userAgent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getInsuranceBillVerification()"
					);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getInsuranceBillVerification");
		billVerificationProp=new Properties();
		String verifyresp =new CyberPlateBillerImpl().insurancebillVerification( Integer.toString(billerId), accountNo,billGrpNumber, commanUtilDao.getTrxId("BILLPAY",aggreatorId), 1.79);
		//public String insurancebillVerification(String billerId,String consumerID,String account,String sessionId,Double amount){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getInsuranceBillVerification resp first time"+verifyresp
				);
		//logger.info(serverName+"*****"+requestId+"***********getInsuranceBillVerification resp first time*******************`"+verifyresp );
		StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
		while (st.hasMoreTokens()) {  
			 String tok = (String) st.nextToken();
			 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
			     tok.substring(tok.indexOf('=') + 1)); 
	     } 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("ERROR") 
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("RESULT") 
				);
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("ERROR") );
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("RESULT") );
		
		if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==7 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==1){
				
			
			if(Double.parseDouble(billVerificationProp.getProperty("PRICE"))>0){
				 factory=DBUtil.getSessionFactory();
				    Session session = factory.openSession();
				    try{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.getProperty("PRICE") 
						);
				//logger.info(serverName+"*****"+requestId+"***************************"+billVerificationProp.getProperty("PRICE"));
		   	
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
		    RechargeTxnBean rechargeTxnBean=new RechargeTxnBean();	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|save initial biller txn"
					);
		    //logger.info(serverName+"*****"+requestId+"*******************save initial biller txn********");
		    
		    rechargeTxnBean.setUserId(userId);
		    rechargeTxnBean.setWalletId(walletMastBean.getWalletid());
		    rechargeTxnBean.setAggreatorid(aggreatorId);
		    rechargeTxnBean.setRechargeNumber(accountNo);
		    rechargeTxnBean.setConsumerId(billGrpNumber);
		    rechargeTxnBean.setAccountNumber(accountNo);
		    rechargeTxnBean.setRechargeType("Insurance");
		    rechargeTxnBean.setRechargeCircle("India");
		    rechargeTxnBean.setRechargeOperator(Integer.toString(billerId));
		    rechargeTxnBean.setRechargeAmt(Double.parseDouble(billVerificationProp.getProperty("PRICE")));
		    rechargeTxnBean.setStatus("Cancel");
		    rechargeTxnBean.setIpIemi(ipIemi);
		    rechargeTxnBean.setUseragent(userAgent);
		    String rechargeId=commanUtilDao.getTrxId("RECHARGE",aggreatorId); 
		    rechargeTxnBean.setRechargeId(rechargeId);
		    session.save(rechargeTxnBean);
		    session.beginTransaction().commit();
			verifyresp =new CyberPlateBillerImpl().insurancebillVerification(Integer.toString(billerId), accountNo,billGrpNumber, rechargeId, Double.parseDouble(billVerificationProp.getProperty("PRICE")));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getInsuranceBillVerification resp first time"+verifyresp
					);
			//logger.info(serverName+"*****"+requestId+"***********getInsuranceBillVerification resp first time*******************`"+verifyresp );
			billVerificationProp.clear();
			  st=new StringTokenizer(verifyresp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     } 
				billVerificationProp.put("id",rechargeId);
		
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				session.close();
			}
			}
			
		}
		return billVerificationProp;
		
	}
	
	
	public Properties getInsuranceBillPayment(String userId,String aggreatorId,String txnId, double billerAmount,String serverName,String requestId,String ipIemi,String userAgent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getInsuranceBillPayment()"
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getInsuranceBillPayment");
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 billPaymentProp=new Properties();
		 try{
			 RechargeTxnBean rechargeTxnBean=(RechargeTxnBean) session.get(RechargeTxnBean.class, txnId);
			 if(rechargeTxnBean!=null &&rechargeTxnBean.getStatus().equalsIgnoreCase("Cancel")){
				 	if(billerAmount==rechargeTxnBean.getRechargeAmt()){
				 		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
				 		String walletResp= transactionDao.billPayment(userId, walletMastBean.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), txnId, ipIemi, aggreatorId, userAgent, 30);
				 		if(walletResp.equals("1000")){
				 			String verifyresp =new CyberPlateBillerImpl().insurancebillPayment( rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getConsumerId(), txnId,billerAmount);	
				 			//public String insurancebillPayment(String billerId,String consumerID,String account,String sessionId,Double amount)
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getInsuranceBillPayment resp first time"+verifyresp
				 					);
				 			//logger.info(serverName+"*****"+requestId+"***********getInsuranceBillPayment resp first time*******************`"+verifyresp );
				 			StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
				 			
				 			while (st.hasMoreTokens()) {  
				 				String tok = (String) st.nextToken();
				 				billPaymentProp.put(tok.substring(0, tok.indexOf('=')),
				 				tok.substring(tok.indexOf('=') + 1)); 
				 		     } 
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("ERROR") 
				 					);
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("RESULT") 
				 					);
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("ERROR") );
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("RESULT") );
				 			
				 			if( Integer.parseInt(billPaymentProp.getProperty("ERROR").toString())==0 &&Integer.parseInt(billPaymentProp.getProperty("RESULT").toString())==0){
				 				transaction=session.beginTransaction();
					 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
					 			int result=Integer.parseInt(billPaymentProp.getProperty("TRNXSTATUS").toString());
				 				if(result==3){
				 					rechargeTxnBean.setStatus("PENDING");	
				 				}else if(result==7){
				 					rechargeTxnBean.setStatus("SUCCESS");
				 				}
				 				rechargeTxnBean.setOther(verifyresp);
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 					rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 				
				 			}else{
				 				transactionDao.settlement(userId,walletMastBean.getWalletid(),rechargeTxnBean.getRechargeAmt(),txnId,ipIemi,rechargeTxnBean.getRechargeNumber(),aggreatorId,userAgent);
				 				
				 				transaction = session.beginTransaction();
				 				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 				rechargeTxnBean.setStatus("FAILED");
				 				rechargeTxnBean.setOther("["+billPaymentProp.getProperty("ERROR")+"]["+verifyresp+"]");
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 				rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 			}
					 			
				 		}else{
				 			transaction=session.beginTransaction();
				 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 			rechargeTxnBean.setStatus("Failed");
				 			rechargeTxnBean.setWallettxnStatus(walletResp);
				 			session.update(rechargeTxnBean);
				 			transaction.commit();
				 			billPaymentProp.put("ERROR", walletResp); 
					 		billPaymentProp.put("RESULT", "1");
				 			}
				 	}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
				 		}}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
			 }
			 
		 }catch (Exception e) {				
				e.printStackTrace();
				billPaymentProp.put("ERROR", "9999"); 
		 		billPaymentProp.put("RESULT", "1");
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|problem in getInsuranceBillPayment"+e.getMessage()+" "+e 
	 					);
				//logger.debug(serverName+"*****"+requestId+"problem in getInsuranceBillPayment==================" + e.getMessage(), e);
				
			} finally {
				session.close();
			}
		 
		return billPaymentProp;
		
	}
	
	
	
	
	
	
	
	public Properties getLandLineBillVerification(String userId,String aggreatorId,int billerId,String phoneNumber,String accountNo,String authenticator3,String termId,String serverName,String requestId,String ipIemi,String userAgent,double amount){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getLandLineBillVerification()" 
					);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getLandLineBillVerification");
		billVerificationProp=new Properties();
		String verifyresp =new CyberPlateBillerImpl().landLineBillVerification( Integer.toString(billerId),phoneNumber,accountNo,commanUtilDao.getTrxId("BILLPAY",aggreatorId), 1.79,authenticator3,termId);
		//public String landLineBillVerification(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId)
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getLandLineBillVerification resp first time"+ verifyresp
				);
		//logger.info(serverName+"*****"+requestId+"***********getLandLineBillVerification resp first time*******************`"+verifyresp );
		StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
		while (st.hasMoreTokens()) {  
			 String tok = (String) st.nextToken();
			 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
			     tok.substring(tok.indexOf('=') + 1)); 
	     } 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("ERROR") 
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.get("RESULT") 
				);
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("ERROR") );
		//logger.info(serverName+"*****"+requestId+"******************"+billVerificationProp.get("RESULT") );
		
		if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==7 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==1){
		//if(Integer.parseInt(billVerificationProp.get("ERROR").toString())==0 && Integer.parseInt(billVerificationProp.get("RESULT").toString())==0){
		//	if(Double.parseDouble(billVerificationProp.getProperty("PRICE"))>0){
			if(billerId==344 ||billerId==240){
				amount=Double.parseDouble(billVerificationProp.getProperty("PRICE"));
			}
			
			if(amount>0){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billVerificationProp.getProperty("PRICE") 
						);
			//logger.info(serverName+"*****"+requestId+"***************************"+billVerificationProp.getProperty("PRICE"));
		    factory=DBUtil.getSessionFactory();
		    Session session = factory.openSession();
		    try{
			WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
		    RechargeTxnBean rechargeTxnBean=new RechargeTxnBean();
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "save initial biller txn" 
					);
		    //logger.info(serverName+"*****"+requestId+"*******************save initial biller txn********");
		   // transaction=session.beginTransaction()
		    rechargeTxnBean.setUserId(userId);
		    rechargeTxnBean.setWalletId(walletMastBean.getWalletid());
		    rechargeTxnBean.setAggreatorid(aggreatorId);
		    rechargeTxnBean.setRechargeNumber(phoneNumber);
		    rechargeTxnBean.setAccountNumber(accountNo);
		    rechargeTxnBean.setRechargeType("Landline");
		    rechargeTxnBean.setRechargeCircle("India");
		    rechargeTxnBean.setRechargeOperator(Integer.toString(billerId));
		    rechargeTxnBean.setRechargeAmt(amount);
		    rechargeTxnBean.setStatus("Cancel");
		    rechargeTxnBean.setIpIemi(ipIemi);
		    rechargeTxnBean.setUseragent(userAgent);
		    String rechargeId=commanUtilDao.getTrxId("RECHARGE",aggreatorId); 
		    rechargeTxnBean.setRechargeId(rechargeId);
		    session.save(rechargeTxnBean);
		    session.beginTransaction().commit();
			verifyresp =new CyberPlateBillerImpl().landLineBillVerification(Integer.toString(billerId),phoneNumber, accountNo, rechargeId,amount,authenticator3,termId);
			//public String landLineBillVerification(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId)
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "getLandLineBillVerification resp first time"+verifyresp
					);
			//logger.info(serverName+"*****"+requestId+"***********getLandLineBillVerification resp first time*******************`"+verifyresp );
			billVerificationProp.clear();
			billVerificationProp.put("id",rechargeId);
			  st=new StringTokenizer(verifyresp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 billVerificationProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     } 
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				session.close();
			}
		}
			
			}
		return billVerificationProp;
		
	}
	
	
	
	
	public Properties getLandlineBillPayment(String userId,String aggreatorId,String txnId, double billerAmount,String serverName,String requestId,String ipIemi,String userAgent,String authenticator3,String termId){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getLandlineBillPayment()" +txnId
				);
		//logger.info(serverName+"*****"+requestId+"start excution******************** method getLandlineBillPayment"+txnId);
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 billPaymentProp=new Properties();
		 try{
			 
			 RechargeTxnBean rechargeTxnBean=(RechargeTxnBean) session.get(RechargeTxnBean.class, txnId);
			 if(rechargeTxnBean!=null &&rechargeTxnBean.getStatus().equalsIgnoreCase("Cancel")){
				 	if(billerAmount==rechargeTxnBean.getRechargeAmt()){
				 		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, userId)	;
				 		String walletResp= transactionDao.billPayment(userId, walletMastBean.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), txnId, ipIemi, aggreatorId, userAgent, 31);
				 		if(walletResp.equals("1000")){
				 			String verifyresp =new CyberPlateBillerImpl().landLineBillPayment( rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAccountNumber(), txnId,billerAmount,authenticator3,termId);	
				 			//public String landLineBillPayment(String billerId,String phoneNumber,String accountNumber,String sessionId,Double amount,String authenticator3,String termId){
				 			
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "|getLandlineBillPayment resp first time" +verifyresp
				 					);
				 			//logger.info(serverName+"*****"+requestId+"***********getLandlineBillPayment resp first time*******************`"+verifyresp );
				 			StringTokenizer st=new StringTokenizer(verifyresp,"|"); 
				 			
				 			while (st.hasMoreTokens()) {  
				 				String tok = (String) st.nextToken();
				 				billPaymentProp.put(tok.substring(0, tok.indexOf('=')),
				 				tok.substring(tok.indexOf('=') + 1)); 
				 		     } 
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("ERROR") 
				 					);
				 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ billPaymentProp.get("RESULT") 
				 					);
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("ERROR") );
				 			//logger.info(serverName+"*****"+requestId+"******************"+billPaymentProp.get("RESULT") );
				 			
				 			if( Integer.parseInt(billPaymentProp.getProperty("ERROR").toString())==0 &&Integer.parseInt(billPaymentProp.getProperty("RESULT").toString())==0){
				 				transaction=session.beginTransaction();
					 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
					 			int result=Integer.parseInt(billPaymentProp.getProperty("TRNXSTATUS").toString());
				 				if(result==3){
				 					rechargeTxnBean.setStatus("PENDING");	
				 				}else if(result==7){
				 					rechargeTxnBean.setStatus("SUCCESS");
				 				}
				 				rechargeTxnBean.setOther(verifyresp);
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 					rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 				
				 			}else{
				 				transactionDao.settlement(userId,walletMastBean.getWalletid(),rechargeTxnBean.getRechargeAmt(),txnId,ipIemi,rechargeTxnBean.getRechargeNumber(),aggreatorId,userAgent);
				 				
				 				transaction = session.beginTransaction();
				 				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 				rechargeTxnBean.setStatus("FAILED");
				 				rechargeTxnBean.setOther("["+billPaymentProp.getProperty("ERROR")+"]["+verifyresp+"]");
				 				if(!(billPaymentProp.getProperty("TRANSID").toString()==null))
				 				rechargeTxnBean.setOperatorTxnId(billPaymentProp.getProperty("TRANSID"));
				 				session.saveOrUpdate(rechargeTxnBean);
				 				transaction.commit();
				 			}
					 			
				 		}else{
				 			transaction=session.beginTransaction();
				 			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, txnId);
				 			rechargeTxnBean.setStatus("Failed");
				 			rechargeTxnBean.setWallettxnStatus(walletResp);
				 			session.update(rechargeTxnBean);
				 			transaction.commit();
				 			billPaymentProp.put("ERROR", "9999"); 
					 		billPaymentProp.put("RESULT", "1");
				 			}
				 	}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
				 		}}else{
				 		billPaymentProp.put("ERROR", "9999"); 
				 		billPaymentProp.put("RESULT", "1");
			 }
			 
		 }catch (Exception e) {				
				e.printStackTrace();
				billPaymentProp.put("ERROR", "9999"); 
		 		billPaymentProp.put("RESULT", "1");
				if (transaction != null)
					transaction.rollback();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "","", serverName +"|requsetId" +requestId+ "problem in getLandlineBillPayment"+e.getMessage()+" "+e 
	 					);
				//logger.debug(serverName+"*****"+requestId+"problem in getLandlineBillPayment==================" + e.getMessage(), e);
				
			} finally {
				session.close();
			}
		 
		return billPaymentProp;
		
	}
	
	
	
	
	
	
	
	public void updateBillAmount(String rechargeId, double billAmount, String serverName, String requestId) {
		 factory=DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			RechargeTxnBean rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
			rechargeTxnBean.setRechargeAmt(billAmount);
			session.update(rechargeTxnBean);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", serverName +"|requsetId" +requestId+ "problem in updateBillAmoun"+e.getMessage()+" "+e 
 					);
			/*logger.debug(
					serverName + "*****" + requestId + "problem in updateBillAmount*****************=" + e.getMessage(),
					e);*/

		} finally {
			session.close();
		}

	}
	
	
	
	
}
