package com.bhartipay.biller.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.EncryptionByEnc256;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.google.gson.Gson;

public class FinoAepsStatusUpdater extends TimerTask {
	public static final Logger logger = Logger.getLogger(UpdateBillStatus.class);

	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	TransactionDao transactionDao = new TransactionDaoImpl();

	@Override
	public synchronized void run() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "schedular started");

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"schedular finished his task");
		// logger.debug("*****************************************schedular finished his
		// task*************************************************");
	}

	public void updateRechargeBillStatus() {

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {
			Query query = session.createQuery("from AEPSLedger where status= :status");
			query.setString("status", "Pending");
			List<AEPSLedger> list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					" total pending aeps transaction " + list.size());
			logger.info("total pending aeps transaction ***************************************:" + list.size());
			Iterator<AEPSLedger> ite = list.iterator();
			while (ite.hasNext()) {
				AEPSLedger aepsLedger = ite.next();
				String txnId = aepsLedger.getTxnId();
				transaction = session.beginTransaction();
				try {
					
					Gson gson = new Gson();
					String reqPara = "https://partner.finopaymentbank.in/PaymentBankBCAPI/UIService.svc/AEPSTransactionEnquiry";
					URL url = new URL(reqPara);

					FinoHeader finoHeader = new FinoHeader();
					finoHeader.setAuthKey("6e5fef67-3dae-4925-8271-cb0ca4a3fa11");
					finoHeader.setClientId("79");
					String jsonHeader = gson.toJson(finoHeader);// Plain Header JSON Format
					String encryptedJsonHeader = EncryptionByEnc256.encryptOpenSSL(jsonHeader,
							"982b0d01-b262-4ece-a2a2-45be82212ba1"); // Encrypted Header JSON Forma

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("SERVICEID", "159");
					jsonObject.put("Version", "1");
					jsonObject.put("ClientRefID", txnId);
					String encryptedRequest = EncryptionByEnc256.encryptOpenSSL(jsonObject.toString(),
							"d1f01fcd-9b79-40ff-b93b-2c10b5ef856d");
					
					
					HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
					urlconnection.setRequestMethod("POST");
					urlconnection.setRequestProperty("Content-Type", "application/json");
					urlconnection.setRequestProperty("Authentication", encryptedJsonHeader);
					urlconnection.setDoOutput(true);
					OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
					outa.write(encryptedRequest);
					outa.flush();
					outa.close();
					BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));

					StringBuilder sb = new StringBuilder();

					if (in != null) {
						int cp;
						while ((cp = in.read()) != -1) {
							sb.append((char) cp);
						}
						in.close();
					}
					in.close();

					org.json.JSONObject jsonObj = new org.json.JSONObject(sb.toString());

					// {"txid":"37404","status":"Failure","opid":"282121793477","number":"9151334124","amount":"149","orderid":"PROR01003505"}

					String status = null;
					String txid = null;
					String orderid = null;
					if (jsonObj != null) {
						if (jsonObj.has("txid") && jsonObj.get("txid") != null)
							txid = jsonObj.get("txid").toString();

						if (jsonObj.has("status") && jsonObj.get("status") != null)
							status = jsonObj.get("status").toString();
						if (jsonObj.has("orderid") && jsonObj.get("orderid") != null)
							orderid = jsonObj.get("orderid").toString();

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				finally {
					session.close();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void main(String[] args) {
		new FinoAepsStatusUpdater().updateRechargeBillStatus();
	}
}
