package com.bhartipay.biller.util;


import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.util.AEPSUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsStatusResponse;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class UpdateAepsStatus {
	public static final Logger logger=Logger.getLogger(UpdateAepsStatus.class);
	
	static final String MID="400f243b35b3477c";
	static final String SECRET_KEY="5cbee8373a874432a300751479149fee";
	static final String URL="https://shreedhan.paymonk.com/megatron/partner/aepsCheckStatus/";
	
	
	public void checkAepsStatus() {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," checkAepsStatus()");
		
		SessionFactory factory=DBUtil.getSessionFactory();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=null;
		

		
		try {
			Criteria criteria=session.createCriteria(AEPSLedger.class);
			criteria.add(Restrictions.eq("status","Pending"));
			List<AEPSLedger> list=criteria.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," total pending checkAepsStatus  "+list.size());
			logger.info("total pending checkAepsStatus  ***************************************:"+list.size());
			Iterator<AEPSLedger> ite = list.iterator();
			while (ite.hasNext()) {
				AEPSLedger aepsLedger=ite.next();
				transaction=session.beginTransaction();
				
				String txnId =aepsLedger.getTxnId();
				String checkSum=MID+"|"+aepsLedger.getAgentCode()+"|"+aepsLedger.getPtyTransDt();
				
				String payload=AEPSUtil.calculateHmac(checkSum,SECRET_KEY);
				AepsStatusResponse aepsStatusResponse=postRequest(aepsLedger.getAgentCode(),aepsLedger.getPtyTransDt(),payload, MID,URL+txnId);
				
				if(aepsStatusResponse!=null&&aepsStatusResponse.getResult()!=null&&aepsStatusResponse.getResult().getResponseCode()==0&&aepsStatusResponse.getResult().getStatus()!=null&&aepsStatusResponse.getResult().getStatus().equalsIgnoreCase("SUCCESS")){
					 /*"orderId":20,
			         "ownerId":9,
			         "ownerType":"RETAILER",
			         "ownerOrderId":"123456",
			         "consumerNo":"8826025599",
			         "orderAmount":100,
			         "paymentAmount":100,
			         "orderStatus":"FAILURE",
			         "paymentStatus":"FAILURE",
			         "fulfilmentStatus":"FAILURE",
			         "responseCode":"DD",
			         "responseMessage":"FAILURE",
			         "paymentMode":"ppi",
			         "processingCode":"CASHWITHDRAWAL",
			         "stan":"000020",
			         "rrn":"816215000020",
			         "bankResponseCode":"DD",
			         "categoryName":"AEPS PAYMENT",
			         "subCategoryName":"Cash Withdrawal",
			         "updatedDate":1528710748000,
			         "createdDate":1528710748000,
			         "aadharNumber":"7778"*/
					if(aepsStatusResponse.getResult().getPayload()!=null&&aepsStatusResponse.getResult().getPayload().getOrderStatus().equalsIgnoreCase("SUCCESS")){
						
					}
				}
				
			}

			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in checkAepsStatus"+e.getMessage()+" "+e);
			e.printStackTrace();
			if(transaction!=null)
			transaction.rollback();
		} finally {
			if(session!=null)
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," end excution" );
		
	}	

	
	public AepsStatusResponse postRequest(String ac,String ts,String cs,String mid,String URL) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," calling aepsstatus service");
		//logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		
		
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource	webResource = client.resource(URL);
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("ac",ac).header("ts",ts).header("cs",cs).header("mid", mid).get(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," calling aepsstatus postrequest service response"+output
				);
		AepsStatusResponse aepsStatusResponse=gson.fromJson(output,AepsStatusResponse.class);
		return aepsStatusResponse;
}	

}
