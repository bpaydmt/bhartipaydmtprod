package com.bhartipay.biller.util;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lowbalance")
public class LowBalanceNotificationDetails {
	
	@Id
	@Column(name="aggregatorid",length =30)
	private String aggregatorId;
	
	@Column(name="cashwalletid",length =30)
	private String cashWalletId;
	
	@Column(name="walletid",length =30)
	private String walletId;
	
	@Column(name="flag",length =30)
	private String flag;
	
	@Column(name="lowamount")
	private double lowAmount;
	
	@Column(name="finalbalance")
	private double finalBalance;
	
	@Column(name="smssentflag",length =30)
	private String smsSentFlag;
	
	@Column(name="mobileno")
	private String mobileNo;
	
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getCashWalletId() {
		return cashWalletId;
	}

	public void setCashWalletId(String cashWalletId) {
		this.cashWalletId = cashWalletId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public double getLowAmount() {
		return lowAmount;
	}

	public void setLowAmount(double lowAmount) {
		this.lowAmount = lowAmount;
	}

	public double getFinalBalance() {
		return finalBalance;
	}

	public void setFinalBalance(double finalBalance) {
		this.finalBalance = finalBalance;
	}

	public String getSmsSentFlag() {
		return smsSentFlag;
	}

	public void setSmsSentFlag(String smsSentFlag) {
		this.smsSentFlag = smsSentFlag;
	}

}
