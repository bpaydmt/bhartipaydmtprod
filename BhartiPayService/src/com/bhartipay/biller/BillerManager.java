package com.bhartipay.biller;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.billerinfo.bean.BillerBbpsResponseBean;
import com.bhartipay.bbps.service.BbpsBillerService;
import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;
import com.bhartipay.biller.bean.BillerDetailsBean;
import com.bhartipay.biller.bean.BillerRequest;
import com.bhartipay.biller.bean.BillerResponse;
import com.bhartipay.biller.bean.BillerResponseBean;
import com.bhartipay.biller.bean.PayLoadBean;
import com.bhartipay.biller.persistence.BillerServiceDao;
import com.bhartipay.biller.persistence.BillerServiceDaoImpl;
import com.bhartipay.biller.util.BillerValidation;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;




@Path("/BillerManager")
public class BillerManager {
	
	private static final Logger logger = Logger.getLogger(BillerManager.class.getName());
	BillerServiceDao billerDao=new BillerServiceDaoImpl();
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	Gson gson = new Gson();
	String mKey="e391ab57590132714ad32da9acf3013eb88c";
	ObjectMapper oMapper = new ObjectMapper();
	 String authenticator3="LLI";
	  String termId="327322";
	
	
	@POST
	@Path("/getBillerDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BillerResponseBean getBillerDetails(BillerRequest billerRequest,@Context HttpServletRequest request){
		BillerResponseBean billerResponseBean=new BillerResponseBean();
		BillerResponse billerResponse=new BillerResponse();
		PayLoadBean payLoadBean=new PayLoadBean();
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		//logger.info(serverName +"**************************************AggreatorId*****"+billerRequest.getAggreatorId());
		//logger.info(serverName +"*************************************Request*****"+billerRequest.getRequest());
		String requestId=billerDao.saveBillerApiRequest(billerRequest.getAggreatorId(),billerRequest.getRequest(),serverName,"getBillerDetaisl");
		//logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|getBillerDetails()"+billerRequest.getRequest()
				);
		billerResponseBean.setRequestId(requestId);
		
		if(billerRequest.getAggreatorId()==null ||billerRequest.getAggreatorId().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid wallet Aggreator ID.");
			//billerResponseBean.setResponse(gson.toJson(billerResponse));
			 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey));
			return billerResponseBean;
		}
		
		if(billerRequest.getRequest()==null ||billerRequest.getRequest().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid Request.");
			//billerResponseBean.setResponse(gson.toJson(billerResponse));
			 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey));
			return billerResponseBean;
		}
		
		 try{
			 Claims claim=oxyJWTSignUtil.parseToken(billerRequest.getRequest(),mKey); 
			 
			 if(claim==null){
				 billerResponse.setStatusCode("1");
					billerResponse.setStatus("ERROR");
					billerResponse.setStatusMsg("JWT signature does not match.");
					//billerResponseBean.setResponse(gson.toJson(billerResponse));
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey));
					return billerResponseBean;
				 }
			 String billerType=claim.get("billerType").toString();
			 
			 if(billerType==null ||billerType.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Biller Type.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
		 	 ArrayList<BillerDetailsBean> billerList=(ArrayList)billerDao.getBillerDetails(billerType, serverName, requestId);
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|billerList"+billerList.size()
					);
		 	// logger.info(serverName +"***********************billerList ***************requestId*****"+billerList.size());
		 	if(billerList.size()>0){
		 	 payLoadBean.setBillerList(billerList);
		 	 billerResponse.setStatusCode("300");
			 billerResponse.setStatus("SUCCESS");
			 billerResponse.setStatusMsg("SUCCESS.");
			 billerResponse.setResPayload(payLoadBean);
			 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
			 return billerResponseBean;
		 	}else{
		 		 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Biller Type.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
		 	}
			
		 }catch (Exception e) {
			 	e.printStackTrace();
			 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|Exception Occurred"+e.getMessage()+" "+e
						);
				//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
				billerResponse.setStatusCode("1");
				billerResponse.setStatus("ERROR");
				billerResponse.setStatusMsg("INVALID MESSAGE STRING.");
				//billerResponseBean.setResponse(gson.toJson(billerResponse));
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey));
				return billerResponseBean;
			}
		 
		 return billerResponseBean;	
	}
	
	
	
	@POST
	@Path("/getBillerverification")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BillerResponseBean getBillerverification(BillerRequest billerRequest,@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent){
		BillerResponseBean billerResponseBean=new BillerResponseBean();
		BillerResponse billerResponse=new BillerResponse();
		PayLoadBean payLoadBean=new PayLoadBean();
		String billerNumber="";
		String billerConsumerID="";
		 String phoneNumber="";
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		//logger.info(serverName +"**************************************AggreatorId*****"+billerRequest.getAggreatorId());
		//logger.info(serverName +"*************************************Request*****"+billerRequest.getRequest());
		String requestId=billerDao.saveBillerApiRequest(billerRequest.getAggreatorId(),billerRequest.getRequest(),serverName,"getBillerverification");
		//logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|getBillerverification()"+billerRequest.getRequest()
				);
		billerResponseBean.setRequestId(requestId);
		
		if(billerRequest.getAggreatorId()==null ||billerRequest.getAggreatorId().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid wallet Aggreator ID.");
			billerResponseBean.setResponse(gson.toJson(billerResponse));
			return billerResponseBean;
		}
		
		if(billerRequest.getRequest()==null ||billerRequest.getRequest().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid Request.");
			billerResponseBean.setResponse(gson.toJson(billerResponse));
			return billerResponseBean;
		}
		
		 try{
			 Claims claim=oxyJWTSignUtil.parseToken(billerRequest.getRequest(),mKey); 
			 
			 if(claim==null){
				 billerResponse.setStatusCode("1");
					billerResponse.setStatus("ERROR");
					billerResponse.setStatusMsg("JWT signature does not match.");
					billerResponseBean.setResponse(gson.toJson(billerResponse));
					return billerResponseBean;
				 }
			 
			 // billerType
			 // walletUserId
			 //billerId
			 //billerAccount
			 //billerAmount
			 //billerConsumerId
			 //billGrpNumber
			 //phoneNumber
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +claim.get("billerType").toString()
						);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +claim.get("walletUserId").toString()
						);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +claim.get("billerId").toString()
						);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId + claim.get("billerAccount").toString()
						);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +claim.get("additionalInfo").toString()
						);
			 
/*			 logger.info(serverName+"*****"+requestId+"*************billerType******* value***`" + claim.get("billerType").toString());
			 logger.info(serverName+"*****"+requestId+"*************walletUserId******* value***`" + claim.get("walletUserId").toString());
			 logger.info(serverName+"*****"+requestId+"*************billerId******* value***`" + claim.get("billerId").toString());
			 
			 logger.info(serverName+"*****"+requestId+"*************billerAccount******* value***`" + claim.get("billerAccount").toString());
			 logger.info(serverName+"*****"+requestId+"*************additionalInfo******* value***`" + claim.get("additionalInfo").toString());*/
			 
			 String billerType=claim.get("billerType").toString();
			 String walletUserId=claim.get("walletUserId").toString();
			 int billerId= Double.valueOf(claim.get("billerId").toString()).intValue(); 
			 /*if(claim.get("billerId").toString().contains(".")){
				 logger.info(serverName+"*****"+requestId+"*************billerId******* value***`" + claim.get("billerId").toString().substring(0, claim.get("billerId").toString().indexOf(".")-1));
				 billerId=Integer.parseInt(claim.get("billerId").toString().substring(0, claim.get("billerId").toString().indexOf(".")-1));
			 }else{
				 billerId=Integer.parseInt(claim.get("billerId").toString()); 
			 }*/
			 
			 String billerAccount=claim.get("billerAccount").toString();
			 String additionalInfo= claim.get("additionalInfo").toString();
			 
			 if(billerType==null ||billerType.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Biller Type.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 if(walletUserId==null ||walletUserId.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Wallet User Id.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 if(!(new BillerValidation().billerIdVerification(billerType,billerId,serverName,requestId))){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Biller Id.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean; 
				 
			 }
			
			 Properties pverificationProp=null;
			 if(billerType.equalsIgnoreCase("Electricity")){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"billerNumber value" + claim.get("billerNumber").toString()
							);
				// logger.info(serverName+"*****"+requestId+"*************billerNumber******* value***`" + claim.get("billerNumber").toString()); 
				 billerNumber=claim.get("billerNumber").toString();
				 
				 
				 if(billerNumber==null ||billerNumber.isEmpty()){
					 billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Invalid Biller Number.");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
				 }
				 
				 if(billerId==235 || billerId==342 ||billerId==332){
					 if(billerAccount==null ||billerAccount.isEmpty()){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid Biller Account Number.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				 }
				 
				 
				 
				 
				 
				 pverificationProp= new BillerValidation().getElBillVerification(walletUserId,billerRequest.getAggreatorId(),billerId,billerNumber,serverName,requestId,ipiemi,agent,billerAccount,additionalInfo);
			
			 
			 
			 }else if(billerType.equalsIgnoreCase("Gas")){
				   billerConsumerID=claim.get("billerConsumerId").toString();
				  
				  if(billerConsumerID==null ||billerConsumerID.isEmpty()){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid Biller Consumer Number.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				  if(billerId==241){
					  if(billerAccount==null ||billerAccount.isEmpty()){
							 billerResponse.setStatusCode("1");
							 billerResponse.setStatus("ERROR");
							 billerResponse.setStatusMsg("Invalid Biller Account Number.");
							 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
							 return billerResponseBean;
						 }
				  }
				  
				  pverificationProp= new BillerValidation().getGasBillVerification(walletUserId,billerRequest.getAggreatorId(),billerId,billerConsumerID,billerAccount,serverName,requestId,ipiemi,agent);  
				   
			  }else if(billerType.equalsIgnoreCase("Insurance")){
				  String billGrpNumber=claim.get("billGrpNumber").toString();
				  if(billGrpNumber==null ||billGrpNumber.isEmpty()){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid DOB.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				  
				  pverificationProp= new BillerValidation().getInsuranceBillVerification(walletUserId,billerRequest.getAggreatorId(),billerId,billerAccount,billGrpNumber, serverName, requestId,ipiemi,agent);
				  
			  }else if(billerType.equalsIgnoreCase("Landline")){
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"amount value" + claim.get("amount").toString()
							);
				 // logger.info(serverName+"*****"+requestId+"*************amount******* value***`" + claim.get("amount").toString());
				  double amount= Double.valueOf(claim.get("amount").toString()); 
				  
				  
				  
				   phoneNumber=claim.get("phoneNumber").toString();
				  if(billerId==239)
					  billerAccount="327322";
				  
				  if(billerId==344)
					  authenticator3= claim.get("additionalInfo").toString();
				 
				  if(billerAccount==null ||billerAccount.isEmpty()){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid Biller Account Number.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				  
				  if(phoneNumber==null ||phoneNumber.isEmpty()){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid phoneNumber.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				  
				  if(billerId==239){
				  if(amount<0){
						 billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Invalid Bill Amount.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					 }
				  }
				  
				  
				  pverificationProp= new BillerValidation().getLandLineBillVerification(walletUserId,billerRequest.getAggreatorId(),billerId,phoneNumber,billerAccount,authenticator3,termId, serverName, requestId,ipiemi,agent,amount);
				  
			  }  else{
				     billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Invalid Biller Type.");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
			  }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"pverificationProp value" + pverificationProp
						);
			 	//logger.info(serverName+"*****"+requestId+"***************pverificationProp value**************" + pverificationProp);
				int error = Integer.parseInt(pverificationProp.get("ERROR").toString());
				int result = Integer.parseInt(pverificationProp.get("RESULT").toString());
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"|error" + error
							);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"|result" + result
							);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"|pverificationProp.getProperty(\"id\")" + pverificationProp.getProperty("id")
							);
				 
				/*logger.info(serverName+"*****"+requestId+"*************error************`" + error);
				logger.info(serverName+"*****"+requestId+"~~**************result******************`" + result);
				logger.info(serverName+"*****"+requestId+"~~**************pverificationProp.getProperty(\"id\")******************`" + pverificationProp.getProperty("id"));*/
			 
				if (error == 0 && result == 0) {
					if(pverificationProp.get("PRICE").toString().contentEquals("NA")){
						billerResponse.setStatusCode("1");
						 billerResponse.setStatus("ERROR");
						 billerResponse.setStatusMsg("Sorry! No bill found. Please enter valid details.");
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
					}else{
						new BillerValidation().updateBillAmount(pverificationProp.getProperty("id"),Double.parseDouble(pverificationProp.getProperty("PRICE")),serverName,requestId);
						payLoadBean.setAggreatorId(billerRequest.getAggreatorId());
						payLoadBean.setWalletUserId(walletUserId);
						payLoadBean.setBillerId(billerId);
						
						 if(billerType.equalsIgnoreCase("Electricity")){
						payLoadBean.setBillerAccount(billerNumber);
						 }else  if(billerType.equalsIgnoreCase("Gas")){
							 payLoadBean.setBillerAccount(billerConsumerID); 
						 }else  if(billerType.equalsIgnoreCase("Insurance")){
							 payLoadBean.setBillerAccount(billerAccount); 
						 }else  if(billerType.equalsIgnoreCase("Landline")){
							 payLoadBean.setBillerAccount(phoneNumber); 
						 }
						 
						 
						payLoadBean.setBillerType(billerType);
						
						payLoadBean.setBillerAmount(Double.parseDouble(pverificationProp.getProperty("PRICE")));
						payLoadBean.setTxnId(pverificationProp.getProperty("id"));
						billerResponse.setStatusCode("300");
						 billerResponse.setStatus("SUCCESS");
						 billerResponse.setStatusMsg("SUCCESS.");
						 billerResponse.setResPayload(payLoadBean);
						 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
						 return billerResponseBean;
						
					}
					
				}else if((error == 7 ||error == 8) && result == 1){
					billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg(pverificationProp.getProperty("ERRMSG"));
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
			 
				}else if(error == 23 && result == 1){
					 billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("No dues pending or due date has passed. Thanks for visiting us!");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
					
				}else{
					 billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Sorry! We're unable to process your request at this time. Please try again later.");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
					
				}
			 
		 }catch (Exception e) {
			 	e.printStackTrace();
			 	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId +"|Exception Occurred"+e.getMessage()+" "+e
							);
				logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
				billerResponse.setStatusCode("1");
				billerResponse.setStatus("ERROR");
				billerResponse.setStatusMsg("INVALID MESSAGE STRING.");
				billerResponseBean.setResponse(gson.toJson(billerResponse));
				return billerResponseBean;
			}
		
		
	}
	
	
	
	
	@POST
	@Path("/getBillerPayment")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BillerResponseBean getBillerPayment(BillerRequest billerRequest,@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent){
	//public BillerResponseBean getBillerPayment(BillerRequest billerRequest){
		
		BillerResponseBean billerResponseBean=new BillerResponseBean();
		BillerResponse billerResponse=new BillerResponse();
		PayLoadBean payLoadBean=new PayLoadBean();

		
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		/*String serverName="test";
		String ipiemi="test";
		String agent="test";*/
	
		//logger.info(serverName +"**************************************AggreatorId*****"+billerRequest.getAggreatorId());
		//logger.info(serverName +"*************************************Request*****"+billerRequest.getRequest());
		String requestId=billerDao.saveBillerApiRequest(billerRequest.getAggreatorId(),billerRequest.getRequest(),serverName,"getBillerPayment");
		//logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|getBillerPayment()"+billerRequest.getRequest()
				);
		billerResponseBean.setRequestId(requestId);
		
		if(billerRequest.getAggreatorId()==null ||billerRequest.getAggreatorId().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid wallet Aggreator ID.");
			billerResponseBean.setResponse(gson.toJson(billerResponse));
			return billerResponseBean;
		}
		
		if(billerRequest.getRequest()==null ||billerRequest.getRequest().isEmpty()){
			billerResponse.setStatusCode("1");
			billerResponse.setStatus("ERROR");
			billerResponse.setStatusMsg("Invalid Request.");
			billerResponseBean.setResponse(gson.toJson(billerResponse));
			return billerResponseBean;
		}
		
		 try{
			 Claims claim=oxyJWTSignUtil.parseToken(billerRequest.getRequest(),mKey); 
			 
			 if(claim==null){
				 billerResponse.setStatusCode("1");
					billerResponse.setStatus("ERROR");
					billerResponse.setStatusMsg("JWT signature does not match.");
					billerResponseBean.setResponse(gson.toJson(billerResponse));
					return billerResponseBean;
				 }
			 
			 // billerType//
			 // walletUserId
			 //billerId
			 //billerAccount
			 //billerAmount
			 //billerConsumerId
			 //billGrpNumber
			 
			 //txnId
			 String billerType=claim.get("billerType").toString();
			 String walletUserId=claim.get("walletUserId").toString();
			 String txnId=claim.get("txnId").toString();
			 String CHECKSUMHASH=claim.get("CHECKSUMHASH").toString();
			 double billerAmount=Double.valueOf(claim.get("billerAmount").toString());
			 
			 TreeMap<String,String> treeMap=new TreeMap<String,String>();
				
				treeMap.put("userId",walletUserId);
				treeMap.put("txnId",txnId);
				treeMap.put("billerAmount",""+billerAmount);
				treeMap.put("billerType",billerType);
				treeMap.put("CHECKSUMHASH",CHECKSUMHASH);
				
				int statusCode= WalletSecurityUtility.checkSecurity(treeMap);
				 if(statusCode!=1000){
					 billerResponse.setStatusCode(""+statusCode);
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Security error.");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
			     }
				 
			 if(txnId==null ||txnId.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Bill Txn Id.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 
			 if(billerAmount<=0){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Bill Aount.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 
		 if(billerType==null ||billerType.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Biller Type.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 if(walletUserId==null ||walletUserId.isEmpty()){
				 billerResponse.setStatusCode("1");
				 billerResponse.setStatus("ERROR");
				 billerResponse.setStatusMsg("Invalid Wallet User Id.");
				 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
				 return billerResponseBean;
			 }
			 
			 Properties pverificationProp=null;
			 if(billerType.equalsIgnoreCase("Electricity")){
				 pverificationProp= new BillerValidation().getElBillPayment(walletUserId,billerRequest.getAggreatorId(),txnId,billerAmount,serverName,requestId,ipiemi,agent);
			  }else if(billerType.equalsIgnoreCase("Gas")){
				  pverificationProp= new BillerValidation().getGasBillPayment(walletUserId,billerRequest.getAggreatorId(),txnId,billerAmount,serverName,requestId,ipiemi,agent);
			  }else if(billerType.equalsIgnoreCase("Insurance")){
				  pverificationProp= new BillerValidation().getInsuranceBillPayment(walletUserId,billerRequest.getAggreatorId(),txnId,billerAmount,serverName,requestId,ipiemi,agent);
			  }else if(billerType.equalsIgnoreCase("Landline")){
				  
				  	  pverificationProp= new BillerValidation().getLandlineBillPayment(walletUserId,billerRequest.getAggreatorId(),txnId,billerAmount,serverName,requestId,ipiemi,agent,authenticator3,termId);
			  }else{
				     billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Invalid Biller Type.");
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
			  }
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|pverificationProp value" + pverificationProp
						);
			 	//logger.info(serverName+"*****"+requestId+"***************pverificationProp value**************" + pverificationProp);
				int error = Integer.parseInt(pverificationProp.get("ERROR").toString());
				int result = Integer.parseInt(pverificationProp.get("RESULT").toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|error" + error
						);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|result" + result
						);
				//logger.info(serverName+"*****"+requestId+"*************error************`" + error);
				//logger.info(serverName+"*****"+requestId+"~~**************result******************`" + result);
				if (error == 0 && result == 0) {
					billerResponse.setStatusCode("300");
					int TxnStatusResult=Integer.parseInt(pverificationProp.getProperty("TRNXSTATUS").toString());
	 				if(TxnStatusResult==3){
	 					 billerResponse.setStatus("PENDING");	
	 				}else if(TxnStatusResult==7){
	 					 billerResponse.setStatus("SUCCESS");
	 				}
	 				billerResponse.setStatusMsg("SUCCESS.");
	 				payLoadBean.setTxnId(txnId);
	 				 billerResponse.setResPayload(payLoadBean);
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
					
				}else if(error == 7 && result == 1){
					billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg(pverificationProp.getProperty("ERRMSG"));
					 payLoadBean.setTxnId(txnId);
	 				 billerResponse.setResPayload(payLoadBean);
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
			 
				}else if(error == 23 && result == 1){
					 billerResponse.setStatusCode("1");
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("No dues pending or due date has passed. Thanks for visiting us!");
					 payLoadBean.setTxnId(txnId);
	 				 billerResponse.setResPayload(payLoadBean);
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
					
				}else{
					 billerResponse.setStatusCode(Integer.toString(error));
					 billerResponse.setStatus("ERROR");
					 billerResponse.setStatusMsg("Sorry! We're unable to process your request at this time. Please try again later.");
					 payLoadBean.setTxnId(txnId);
	 				 billerResponse.setResPayload(payLoadBean);
					 billerResponseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(billerResponse, HashMap.class), mKey)); 
					 return billerResponseBean;
				}
			 
		 }catch (Exception e) {
			 	e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),billerRequest.getAggreatorId(),"","", "","", serverName + "|"+ "requestId"+requestId+"|Exception Occurred"+e.getMessage()+" "+e
						);
				//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
				billerResponse.setStatusCode("1");
				billerResponse.setStatus("ERROR");
				billerResponse.setStatusMsg("INVALID MESSAGE STRING.");
				billerResponseBean.setResponse(gson.toJson(billerResponse));
				return billerResponseBean;
			}
	}
	
	
	/*public static void main(String[] args) {
		BillerRequest billerRequest=new BillerRequest();
		billerRequest.setAggreatorId(aggreatorId);
		billerRequest.setRequest(request);
		
	}*/
}
