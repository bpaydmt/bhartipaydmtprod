
package com.bhartipay.payword.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayworldRequest {

    @SerializedName("actionName")
    @Expose
    private String actionName;
    @SerializedName("parameters")
    @Expose
    private List<Parameter> parameters = null;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

}
