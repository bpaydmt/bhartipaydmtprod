package com.bhartipay.payword.bean;

public class PayworldResponseData {
	
		private String responseCode;//":0,"
		private String responseMessage;
	
		private String status;//=SUCCESS,
		private String referenceNumber;//RDEV001573,
		private String merchantTransno;//M18021417AAAF,
		private String transDateTime;//2018-02-14 17:09:19,
		private String message;//Transaction Successful,
		private String deviceNumber;//9999215888,
		private String operatorName;//Airtel
		
		
		
		
		public String getStatus() {
			return status;
		}
		public String getReferenceNumber() {
			return referenceNumber;
		}
		public String getMerchantTransno() {
			return merchantTransno;
		}
		public String getTransDateTime() {
			return transDateTime;
		}
		public String getMessage() {
			return message;
		}
		public String getDeviceNumber() {
			return deviceNumber;
		}
		public String getOperatorName() {
			return operatorName;
		}
		public String getResponseCode() {
			return responseCode;
		}
		public String getResponseMessage() {
			return responseMessage;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public void setReferenceNumber(String referenceNumber) {
			this.referenceNumber = referenceNumber;
		}
		public void setMerchantTransno(String merchantTransno) {
			this.merchantTransno = merchantTransno;
		}
		public void setTransDateTime(String transDateTime) {
			this.transDateTime = transDateTime;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public void setDeviceNumber(String deviceNumber) {
			this.deviceNumber = deviceNumber;
		}
		public void setOperatorName(String operatorName) {
			this.operatorName = operatorName;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}
		
		
}
