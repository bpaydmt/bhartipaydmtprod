
package com.bhartipay.payword.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DirectParameters {

    @SerializedName("planId")
    @Expose
    private String planId;
    @SerializedName("planCircleCode")
    @Expose
    private String planCircleCode;
    @SerializedName("planName")
    @Expose
    private String planName;
    @SerializedName("subscriptionType")
    @Expose
    private String subscriptionType;
    @SerializedName("accountCode")
    @Expose
    private String accountCode;
    @SerializedName("partyCode")
    @Expose
    private String partyCode;
    @SerializedName("partnerCode")
    @Expose
    private String partnerCode;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getPlanCircleCode() {
        return planCircleCode;
    }

    public void setPlanCircleCode(String planCircleCode) {
        this.planCircleCode = planCircleCode;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

}
