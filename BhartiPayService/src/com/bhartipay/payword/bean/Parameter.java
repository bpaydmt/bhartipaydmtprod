
package com.bhartipay.payword.bean;

import com.bhartipay.payworld.masterdata.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Parameter {

    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("operatorCode")
    @Expose
    private Integer operatorCode;
    @SerializedName("circleCode")
    @Expose
    private String circleCode;
    @SerializedName("merchantUserId")
    @Expose
    private String merchantUserId;
    @SerializedName("merchantUserIds")
    @Expose
    private String merchantUserIds;
    @SerializedName("convenienceCharge")
    @Expose
    private Integer convenienceCharge;
    @SerializedName("deviceNumber")
    @Expose
    private String deviceNumber;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("serviceCode")
    @Expose
    private Integer serviceCode;
    @SerializedName("device")
    @Expose
    private String device;
    @SerializedName("customerMobile")
    @Expose
    private String customerMobile;
    @SerializedName("customerEmailid")
    @Expose
    private String customerEmailid;
    @SerializedName("referenceNumber")
    @Expose
    private String referenceNumber;
    @SerializedName("direct")
    @Expose
    private String direct;
    @SerializedName("directParameters")
    @Expose
    private DirectParameters directParameters;
    
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    
    @SerializedName("checkOperator")
    @Expose
    private boolean checkOperator;
    
    
    
    
   
	public boolean isCheckOperator() {
		return checkOperator;
	}

	public void setCheckOperator(boolean checkOperator) {
		this.checkOperator = checkOperator;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(Integer operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(String merchantUserId) {
        this.merchantUserId = merchantUserId;
    }

    public String getMerchantUserIds() {
        return merchantUserIds;
    }

    public void setMerchantUserIds(String merchantUserIds) {
        this.merchantUserIds = merchantUserIds;
    }

    public Integer getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(Integer convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerEmailid() {
        return customerEmailid;
    }

    public void setCustomerEmailid(String customerEmailid) {
        this.customerEmailid = customerEmailid;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public DirectParameters getDirectParameters() {
        return directParameters;
    }

    public void setDirectParameters(DirectParameters directParameters) {
        this.directParameters = directParameters;
    }

}
