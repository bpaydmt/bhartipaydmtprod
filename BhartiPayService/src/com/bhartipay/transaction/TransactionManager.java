package com.bhartipay.transaction;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.airTravel.bean.RequestBean;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.CmsBean;
import com.bhartipay.lean.bean.CmsEnquiryResponse;
import com.bhartipay.lean.bean.CmsResponse;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.lean.bean.PgReturnResponse;
import com.bhartipay.matchmove.mmpay.api.HttpRequest;
import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.request.BillRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;
import com.bhartipay.recharge.response.Result;
import com.bhartipay.transaction.bean.AppResponse;
import com.bhartipay.transaction.bean.AskMoneyBean;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.bean.MoneyRequestBean;
import com.bhartipay.transaction.bean.P2MTransactionBean;
import com.bhartipay.transaction.bean.PGAggregatorBean;
import com.bhartipay.transaction.bean.PGPayeeBean;
import com.bhartipay.transaction.bean.PrePaidCardResponse;
import com.bhartipay.transaction.bean.PrePaidCardTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.RefundMastBean;
import com.bhartipay.transaction.bean.RefundTransactionBean;
import com.bhartipay.transaction.bean.RemainingLimitBean;
import com.bhartipay.transaction.bean.Response;
import com.bhartipay.transaction.bean.SMSCallBackBean;
import com.bhartipay.transaction.bean.SurchargeBean;
import com.bhartipay.transaction.bean.TerminalOnBoardingCallBackBean;
import com.bhartipay.transaction.bean.TerminalStatusCallBackBean;
import com.bhartipay.transaction.bean.TransactionNotificationCallBackBean;
import com.bhartipay.transaction.bean.TransactionResponse;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.ResponseResult;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsResponseData;
import com.bhartipay.wallet.aeps.transaction.AEPSDao;
import com.bhartipay.wallet.aeps.transaction.AEPSDaoImpl;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.matm.MatmResponseData;
import com.bhartipay.wallet.matm.transaction.MATMDao;
import com.bhartipay.wallet.matm.transaction.MATMDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.ClientResponse;

import io.jsonwebtoken.Claims;
import javassist.bytecode.stackmap.BasicBlock.Catch;
import net.pg.appnit.utility.AES128Bit;

@Path("/TransactionManager")
public class TransactionManager {

	public static Logger logger=Logger.getLogger(TransactionManager.class);	
	TransactionDao transactionDao=new TransactionDaoImpl();
	ObjectMapper oMapper = new ObjectMapper();
	Gson gson = new Gson();
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	
	
	private static final String SMS_URL ="/sms";							//DELETE
	private static final String MERCHANT_ONBOARDING_URL ="/merchantonboarding";
	private static final String TERMINAL_ONBOARDING_URL ="/terminalonboarding";
	private static final String TERMINAL_STATUS_URL ="/terminalstatus";
	private static final String TRANSACTION_NOTIFICATION_URL ="/transactionnotification";
	ResponseResult rbean = new ResponseResult();

	//================================ CALLBACK API ================================================

	/*
	 * Create By :  Niraj
	 * Date : 25-July-2021
	 *  API Name : SMS Callback API
	 *  API Return : STATUS 
	 */  

	@POST
	@Path("/getMerchantMid")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getMerchantMid(AppResponse res ){
		 
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",res.getUserid(), "","" ,"getMid()");
		
		return transactionDao.getMerchantMid(res);
	 }
	
	
	@POST
	@Path(SMS_URL)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseResult saveSMSResponse(SMSCallBackBean smsCallBackBean ){

		logger.info("--------------------Start  execution of : SMS_URL CallBack----------------------");
		logger.info("saveSMSResponse Input Paramter getMobile > : "+smsCallBackBean.getMobile());
		logger.info("saveSMSResponse Input Paramter getMessage > : "+smsCallBackBean.getMessage());
		logger.info("saveSMSResponse >> GSON DATA ::: "+new Gson().toJson(smsCallBackBean));    

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"mobile -"+smsCallBackBean.getMobile()+"|  message : "+smsCallBackBean.getMessage());
		rbean= transactionDao.saveSMSResponseApi(smsCallBackBean);
		logger.info("saveSMSResponse Response Status : > : "+rbean.getStatus());
		logger.info("--------------------End execution of : SMS_URL CallBack----------------------");
		return rbean;
	}
	
	@POST
	@Path(TRANSACTION_NOTIFICATION_URL)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseResult saveTransactionNotificationResponse(TransactionNotificationCallBackBean transCallBackBean ){

		logger.info("--------------------Start  execution of : TRANSACTION_NOTIFICATION_URL CallBack----------------------");
		
		
		logger.info("saveTransactionNotificationResponse Input Paramter getTransaction_id > : "+transCallBackBean.getTransaction_id());
		logger.info("saveTransactionNotificationResponse Input Paramter isIs_void > : "+transCallBackBean.isIs_void());
		logger.info("saveTransactionNotificationResponse Input Paramter isIs_reversed > : "+transCallBackBean.isIs_reversed());
		logger.info("saveTransactionNotificationResponse Input Paramter isIs_disputed > : "+transCallBackBean.isIs_disputed());
		logger.info("saveTransactionNotificationResponse Input Paramter getAmount > : "+transCallBackBean.getAmount());
		logger.info("saveTransactionNotificationResponse Input Paramter getCard_type > : "+transCallBackBean.getCard_type());
		logger.info("saveTransactionNotificationResponse Input Paramter getCard_payment_type > : "+transCallBackBean.getCard_payment_type());
		logger.info("saveTransactionNotificationResponse Input Paramter getCard_usage_type > : "+transCallBackBean.getCard_usage_type());
		logger.info("saveTransactionNotificationResponse Input Paramter getMasked_pan > : "+transCallBackBean.getMasked_pan());
		logger.info("saveTransactionNotificationResponse Input Paramter getNetwork > : "+transCallBackBean.getNetwork());
		logger.info("saveTransactionNotificationResponse Input Paramter getPayment_method > : "+transCallBackBean.getPayment_method());
		logger.info("saveTransactionNotificationResponse Input Paramter getTid > : "+transCallBackBean.getTid());
		logger.info("saveTransactionNotificationResponse Input Paramter getTransaction_type > : "+transCallBackBean.getTransaction_type());
		logger.info("saveTransactionNotificationResponse Input Paramter getMerchant_id > : "+transCallBackBean.getMerchant_id());
		logger.info("saveTransactionNotificationResponse Input Paramter getCreated_at > : "+transCallBackBean.getCreated_at());
		logger.info("saveTransactionNotificationResponse Input Paramter getRrn > : "+transCallBackBean.getRrn());
		logger.info("saveTransactionNotificationResponse Input Paramter getStatus > : "+transCallBackBean.getStatus());

		
		logger.info("saveTransactionNotificationResponse >> GSON DATA ::: "+new Gson().toJson(transCallBackBean));    
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"JSON DATA : "+new Gson().toJson(transCallBackBean));
		rbean= transactionDao.saveTransactionNotificationResponseApi(transCallBackBean);
		logger.info("saveTransactionNotificationResponse Response Status : > : "+rbean.getStatus());
		logger.info("--------------------End execution of : TRANSACTION_NOTIFICATION_URL CallBack----------------------");
		return rbean;
	}
	
	
	@POST
	@Path(TERMINAL_ONBOARDING_URL)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseResult saveTerminalonboardingResponse(@HeaderParam("Authorization") String authorization,
														TerminalOnBoardingCallBackBean transOnboardBean ){

		logger.info("--------------------Start  execution of : TERMINAL_ONBOARDING_URL CallBack----------------------");
		logger.info("saveTerminalonboardingResponse Input Paramter Authorization Token > : "+authorization);

		logger.info("saveTerminalonboardingResponse Input Paramter getMerchant_id > : "+transOnboardBean.getMerchant_id());
		logger.info("saveTerminalonboardingResponse Input Paramter getLocation > : "+transOnboardBean.getLocation());
		logger.info("saveTerminalonboardingResponse Input Paramter getAddress > : "+transOnboardBean.getAddress());
		logger.info("saveTerminalonboardingResponse Input Paramter getPincode > : "+transOnboardBean.getPincode());
		logger.info("saveTerminalonboardingResponse Input Paramter getSim_number > : "+transOnboardBean.getSim_number());
		logger.info("saveTerminalonboardingResponse Input Paramter getTerminal_type > : "+transOnboardBean.getTerminal_type());
		logger.info("saveTerminalonboardingResponse Input Paramter getDevice_model > : "+transOnboardBean.getDevice_model());
		logger.info("saveTerminalonboardingResponse Input Paramter getMax_usage_daily > : "+transOnboardBean.getMax_usage_daily());
		logger.info("saveTerminalonboardingResponse Input Paramter getMax_usage_weekly > : "+transOnboardBean.getMax_usage_weekly());
		logger.info("saveTerminalonboardingResponse Input Paramter getMax_usage_montly > : "+transOnboardBean.getMax_usage_montly());
		logger.info("saveTerminalonboardingResponse Input Paramter getVelocity_check_minutes > : "+transOnboardBean.getVelocity_check_minutes());
		logger.info("saveTerminalonboardingResponse Input Paramter getVelocity_check_count > : "+transOnboardBean.getVelocity_check_count());
		
		logger.info("saveTerminalonboardingResponse >> GSON DATA ::: "+new Gson().toJson(transOnboardBean));    
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"JSON DATA : "+new Gson().toJson(transOnboardBean));
		rbean= transactionDao.saveTerminalonboardingResponseApi(transOnboardBean,authorization);
		logger.info("saveTerminalonboardingResponse Response Status : > : "+rbean.getStatus());
		logger.info("--------------------End execution of : TERMINAL_ONBOARDING_URL CallBack----------------------");
		return rbean;
	}
	
	@GET
	@Path(TERMINAL_STATUS_URL) 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ResponseResult saveTerminalstatusResponse(@QueryParam("id") String status_id_check, 
													@HeaderParam("Authorization") String authorization,
													TerminalStatusCallBackBean terminalStatusBean){
		
		
		logger.info("--------------------Start  execution of : TERMINAL_STATUS_URL CallBack----------------------");
		
		
		logger.info("saveTerminalstatusResponse Input Paramter status_id_check > : "+status_id_check);
		logger.info("saveTerminalstatusResponse Input Paramter authorization > : "+authorization);
		logger.info("saveTerminalstatusResponse >> GSON DATA ::: "+new Gson().toJson(terminalStatusBean));    

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"JSON DATA : "+new Gson().toJson(terminalStatusBean));
		rbean= transactionDao.saveTerminalstatusResponseApi(terminalStatusBean,status_id_check,authorization);
		logger.info("saveTerminalstatusResponse Response Status : > : "+rbean.getStatus());
		logger.info("--------------------End execution of : TERMINAL_STATUS_URL CallBack----------------------");
		return rbean;
	}
	
	
	@POST
	@Path(MERCHANT_ONBOARDING_URL) 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ResponseResult saveMerchantOnboardingResponse(@HeaderParam("Authorization") String authorization,
														String  jsonData){
		
		logger.info("--------------------Start  execution of : MERCHANT_ONBOARDING_URL CallBack----------------------");
		
		
		logger.info("saveMerchantOnboardingResponse Input Paramter jsonData > : "+jsonData);
		logger.info("saveMerchantOnboardingResponse Input Paramter authorization > : "+authorization);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"JSON DATA : "+jsonData);
		
		rbean= transactionDao.saveMerchantOnboardingResponseApi(jsonData,authorization);
		logger.info("saveMerchantOnboardingResponse Response Status : > : "+rbean.getStatus());
		logger.info("--------------------End execution of : MERCHANT_ONBOARDING_URL CallBack----------------------");
		return rbean;
	}
	
	
	
	//================================ CALLBACK API ================================================

	
	
	/*@POST
	@Path("/savePGTrx")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePGTrx(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("_____________agent_____________"+agent);
		System.out.println("____________txnInputBean_______"+txnInputBean);
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"savePGTrx()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.savePGTrx(txnInputBean.getTrxAmount(), txnInputBean.getWalletId(), txnInputBean.getMobileNo(), txnInputBean.getTrxReasion(),txnInputBean.getPayTxnid(),txnInputBean.getAggreatorid(),imei,agent);
		
	}*/
	
	@POST
	@Path("/savePGTrx")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePGTrx(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("_____________agent_____________"+agent);
		System.out.println("____________txnInputBean_______"+txnInputBean);
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"savePGTrx()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.savePGTrx(txnInputBean.getTrxAmount(), txnInputBean.getWalletId(), txnInputBean.getMobileNo(), txnInputBean.getTrxReasion(),txnInputBean.getPayTxnid(),txnInputBean.getAggreatorid(),imei,agent);
		
	}
	
	
	/*@POST
	@Path("/updatePGTrx")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String updatePGTrx(TxnInputBean txnInputBean ){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"updatePGTrx()");
		
		//logger.info("Start excution ===================== method updatePGTrx(txnInputBean)");
		return transactionDao.updatePGTrx(txnInputBean.getHash(),txnInputBean.getResponseParam(),txnInputBean.getUserId(), txnInputBean.getTxnId(),txnInputBean.getPgtxn(), txnInputBean.getTrxAmount(), txnInputBean.getStatus(),txnInputBean.getIpImei());
				
	}*/
	
	@POST
	@Path("/updatePGTrx")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String updatePGTrx(TxnInputBean txnInputBean ){
		 
		  
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"updatePGTrx()");
		
		//logger.info("Start excution ===================== method updatePGTrx(txnInputBean)");
		return transactionDao.updatePGTrx(txnInputBean.getHash(),txnInputBean.getResponseParam(),txnInputBean.getUserId(), txnInputBean.getTxnId(),txnInputBean.getPgtxn(), txnInputBean.getTrxAmount(), txnInputBean.getStatus(),txnInputBean.getIpImei());
			
}

	@POST
	@Path("/updatePGTrxTest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String updatePGTrxTest(TxnInputBean txnInputBean ){
		 
		String response = new Gson().toJson(txnInputBean);	  
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"updatePGTrx() String "+response);
		
		logger.info("Start excution ===================== method updatePGTrx(txnInputBean)"+" Transaction Mode "+txnInputBean.getPaymentMode());
		
		return transactionDao.updatePGTrxTest(txnInputBean.getHash(),txnInputBean.getResponseParam(),txnInputBean.getUserId(), txnInputBean.getTxnId(),txnInputBean.getPgtxn(), txnInputBean.getTrxAmount(), txnInputBean.getStatus(),txnInputBean.getIpImei(),txnInputBean.getPaymentMode(),txnInputBean.getCardMask());
			
    }
	
	
	@POST
	@Path("/savePgReturnResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePgReturnResponse(PgReturnResponse res ){
		
	 String response = new Gson().toJson(res);
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),res.getAgentId(),res.getPgOrderId(),res.getPgTxnType(), "",res.getPgAmount() ,"savePgReturnResponse() String "+response);
		
	logger.info("Start excution ===================== method savePgReturnResponse(PgReturnResponse res)"+" Transaction Mode "+res.getPgPaymentType());
		
	return transactionDao.savePgReturnResponse(res);
	}
	
	
	
	@POST
	@Path("/getPGAGGId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public PGAggregatorBean getPGAGGId(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("_____________agent_____________"+agent);
		System.out.println("____________txnInputBean_______"+txnInputBean);
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"savePGTrx()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.getPGAGGId(txnInputBean.getAggreatorid());
		
	}
	
	@POST
	@Path("/saveAepsRequestApi")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AEPSLedger saveAepsRequestApi(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getWalletId(),aepsLedger.getAgentId(), "",aepsLedger.getTxnId() ,"saveAepsRequest()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.saveAepsRequestApi(aepsLedger, imei, agent);
		
	}
	
	@POST
	@Path("/saveAepsRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AEPSLedger saveAepsRequest(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getWalletId(),aepsLedger.getAgentId(), "",aepsLedger.getTxnId() ,"saveAepsRequest()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.saveAepsRequest(aepsLedger, imei, agent);
		
	}
	@POST
	@Path("/getAepsLedger")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AEPSLedger getAepsLedger(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getWalletId(),aepsLedger.getAgentId(), "",aepsLedger.getTxnId() ,"saveAepsRequest()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.getAepsLedger(aepsLedger, imei, agent);
		
	}
	
	@POST
	@Path("/saveMatmRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MATMLedger saveMatmRequest(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MATMLedger matmLedger){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),matmLedger.getAggregator(),matmLedger.getWalletId(),matmLedger.getAgentId(), "",matmLedger.getTxnId() ,"saveMatmRequest()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.saveMatmRequest(matmLedger, imei, agent);
		
	}

	
	
	
	@POST
	@Path("/updateAepsRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AEPSLedger updateAepsRequest(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,AEPSLedger aepsLedger){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getWalletId(),aepsLedger.getAgentId(), "",aepsLedger.getTxnId() ,"saveAepsRequest()");
		
		//logger.info("Start excution ===================== method updatePGTrx(txnInputBean)");
		return transactionDao.updateAepsRequest(aepsLedger, imei, agent);
				
	}
	
	
	
	public static void main(String[] args)throws Exception {
//		new TransactionManager().AepsResponse("{\"payload\":{\"metadata\":\"{\\\"refno\\\":\\\"OXAP001035\\\",\\\"name\\\":\\\"AMARDEEP SINGH\\\",\\\"redirectionUrl\\\":\\\"http:\\\\/\\\\/testpg.payplutus.in:9080\\\\/AppnitPgClient\\\\/paymentSuccess.jsp\\\"}\",\"aeps\":{\"payerId\":null,\"payertype\":null,\"payeeId\":null,\"payeetype\":null,\"txnType\":null,\"orderId\":\"102982\",\"amount\":0,\"txnId\":null,\"balance\":null,\"orderStatus\":\"SUCCESS\",\"paymentStatus\":\"SUCCESS\",\"requestId\":\"939619\",\"stan\":\"019541\",\"rrn\":\"827415019541\",\"bankAuth\":\"87e2c89c5c114e0998deb36ab06fd9c3\",\"processingCode\":\"310000\",\"accountBalance\":\"30839.95\",\"bankResponseCode\":null,\"bankResponseMsg\":\"Approved or completed successfully\",\"terminalId\":\"RPM01366\",\"agentId\":\"Act1731446\",\"aadharNumber\":\"********4698\",\"dateTime\":1538387243497,\"statusCode\":\"00\",\"statusMessage\":null,\"commissionAmt\":0.00,\"gstAmt\":0.00,\"tdsAmt\":0.00,\"walletMessage\":null,\"isWalletFailed\":false,\"bcname\":\"PAYMONK\",\"bcaddress\":\"C-16 Sector 6, Noida\"}}}");
		
		String json= "{\"payload\":{\"metadata\":{\"refno\":\"BPAP074181\",\"name\":\"N/A\",\"redirectionUrl\":\"N/A\"},\"aeps\":{\"dateTime\":1585731185000,\"agentId\":\"9999999999\",\"orderId\":\"BPAP074181\",\"processingCode\":\"N/A\",\"orderStatus\":\"Success\",\"terminalId\":\"N/A\",\"bcaddress\":\"N/A\",\"balance\":\"25243.53\",\"walletMessage\":\"N/A\",\"requestId\":\"BPAP074181\",\"payeeId\":\"N/A\",\"paymentStatus\":\"Success\",\"amount\":2010,\"bankAuth\":\"N/A\",\"commissionAmt\":0,\"bcname\":\"UNION BANK OF INDIA\",\"gstAmt\":0,\"tdsAmt\":0,\"statusMessage\":\"AEPS Transaction Success\",\"rrn\":\"009214010397\",\"payeetype\":\"N/A\",\"aadharNumber\":\"XXXXXXXX2647\",\"stan\":\"N/A\",\"bankResponseMsg\":\"Success\",\"isWalletFailed\":false,\"accountBalance\":\"25243.53\",\"bankResponseCode\":\"N/A\",\"statusCode\":\"0\",\"txnId\":\"BPAP074181\"}}}";
//		new TransactionManager().MatmResponse("{\"payload\":{\"metadata\":\"{\\\"refno\\\":\\\"OXAP001035\\\",\\\"name\\\":\\\"AMARDEEP SINGH\\\",\\\"redirectionUrl\\\":\\\"http:\\\\/\\\\/testpg.payplutus.in:9080\\\\/AppnitPgClient\\\\/paymentSuccess.jsp\\\"}\",\"aeps\":{\"payerId\":null,\"payertype\":null,\"payeeId\":null,\"payeetype\":null,\"txnType\":null,\"orderId\":\"102982\",\"amount\":0,\"txnId\":null,\"balance\":null,\"orderStatus\":\"SUCCESS\",\"paymentStatus\":\"SUCCESS\",\"requestId\":\"939619\",\"stan\":\"019541\",\"rrn\":\"827415019541\",\"bankAuth\":\"87e2c89c5c114e0998deb36ab06fd9c3\",\"processingCode\":\"310000\",\"accountBalance\":\"30839.95\",\"bankResponseCode\":null,\"bankResponseMsg\":\"Approved or completed successfully\",\"terminalId\":\"RPM01366\",\"agentId\":\"Act1731446\",\"aadharNumber\":\"********4698\",\"dateTime\":1538387243497,\"statusCode\":\"00\",\"statusMessage\":null,\"commissionAmt\":0.00,\"gstAmt\":0.00,\"tdsAmt\":0.00,\"walletMessage\":null,\"isWalletFailed\":false,\"bcname\":\"PAYMONK\",\"bcaddress\":\"C-16 Sector 6, Noida\"}}}");
		//String jsonNew ="{\"Amount\":705.0,\"AdhaarNo\":\"XXXXXXXX9951\",\"TxnTime\":\"12:21:36\",\"TxnDate\":\"25/12/2019\",\"BankName\":\"HDFC BANK\",\"RRN\":\"935912049176\",\"Status\":\"Success\",\"CustomerMobile\":\"9812564665\",\"AvailableBalance\":\"2162.5\",\"LedgerBalance\":null}";
		new TransactionManager().AepsResponse(json);
	}
	
	@POST
	@Path("/AepsResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String AepsResponse(String aepsLedger2) throws JSONException{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","AepsResponse()--"+aepsLedger2);
		aepsLedger2=aepsLedger2.replaceAll(" ","");
		AepsResponseData data=new AepsResponseData();
		try {
		JSONObject jsonObject=new JSONObject(aepsLedger2);
		JSONObject jsonObject2=new JSONObject(jsonObject.get("payload").toString());
		JSONObject jsonObject3=new JSONObject(jsonObject2.get("metadata").toString());
		JSONObject jsonObject4=new JSONObject(jsonObject2.get("aeps").toString());
		
		
		data.setAadharNumber(jsonObject4.getString("aadharNumber"));
		data.setAccountBalance(jsonObject4.getString("accountBalance"));
		data.setAgentId(jsonObject4.getString("agentId"));
		
		data.setAmount(jsonObject4.getInt("amount"));
		
		data.setBalance(""+jsonObject4.get("balance")!=null&&!jsonObject4.get("balance").toString().equalsIgnoreCase("null")?jsonObject4.get("balance").toString():"0");
		data.setBankAuth(jsonObject4.getString("bankAuth"));
		data.setBankResponseCode(""+jsonObject4.get("bankResponseCode"));
		data.setBankResponseMsg(""+jsonObject4.get("bankResponseMsg"));
		data.setBcaddress(jsonObject4.getString("bcaddress"));
		data.setBcname(jsonObject4.getString("bcname"));
		data.setCommissionAmt(jsonObject4.getDouble("commissionAmt"));
		data.setDateTime(jsonObject4.getInt("dateTime"));
		data.setGstAmt(jsonObject4.getDouble("gstAmt"));
		data.setIsWalletFailed(jsonObject4.getBoolean("isWalletFailed"));
		data.setName(jsonObject3.getString("name"));
		data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderStatus(jsonObject4.getString("orderStatus"));
		data.setPayeeId(""+jsonObject4.get("payeeId"));
		data.setPayeetype(""+jsonObject4.get("payeetype"));
		data.setPaymentStatus(jsonObject4.getString("paymentStatus"));
		data.setProcessingCode(jsonObject4.getString("processingCode"));
		data.setRedirectionUrl(jsonObject3.getString("redirectionUrl"));
		data.setRefno(jsonObject3.getString("refno"));
		data.setRequestId(jsonObject4.getString("requestId"));
		data.setRrn(jsonObject4.getString("rrn"));
		data.setStan(jsonObject4.getString("stan"));
		data.setStatusCode(jsonObject4.getString("statusCode"));
		data.setStatusMessage(""+jsonObject4.getString("statusMessage"));
		data.setTdsAmt(jsonObject4.getDouble("tdsAmt"));
		data.setTerminalId(jsonObject4.getString("terminalId"));
		data.setTxnId(""+jsonObject4.getString("txnId"));
		data.setTxnType(""+jsonObject4.getString("txnType"));
		data.setWalletMessage(""+jsonObject4.getString("walletMessage"));
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("***************************** Inside transactionManger *********************"+data.getOrderStatus());

		logger.debug("***************************** TxnStatus *********************"+data.getOrderStatus());
		logger.debug("***************************** TxnAmt  *********************"+data.getAmount());
		logger.debug("***************************** RRN *********************"+data.getRrn());
		logger.debug("***************************** Aadhar Number  *********************"+data.getAadharNumber());
		logger.debug("***************************** AvailableBalance *********************"+data.getAccountBalance());
		logger.debug("***************************** RefNo  *********************"+data.getRefno());
		
		
		AEPSDao aepsDao=new AEPSDaoImpl();
		
		//aepsDao.persistTransaction(data);
		
		
	     
		return aepsDao.persistTransaction(data).getAgentId();
		
		
				
	}
	
	
	
	@POST
	@Path("/AepsStatusResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AEPSLedger aepsStatusResponse(String aepsLedger2) throws JSONException{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","AepsResponse()--"+aepsLedger2);
		aepsLedger2=aepsLedger2.replaceAll(" ","");
		AepsResponseData data=new AepsResponseData();
		try {
		JSONObject jsonObject=new JSONObject(aepsLedger2);
		JSONObject jsonObject2=new JSONObject(jsonObject.get("payload").toString());
		JSONObject jsonObject3=new JSONObject(jsonObject2.get("metadata").toString());
		JSONObject jsonObject4=new JSONObject(jsonObject2.get("aeps").toString());
		
		data.setAadharNumber(jsonObject4.getString("aadharNumber"));
		data.setAccountBalance(jsonObject4.getString("accountBalance"));
		data.setAgentId(jsonObject4.getString("agentId"));
		try {
			
			int amtInt = (int) Double.parseDouble(jsonObject4.getString("amount"));
			
		    data.setAmount(amtInt);
		}catch(Exception e) {
			
			
		}
		data.setBalance(""+jsonObject4.get("balance")!=null&&!jsonObject4.get("balance").toString().equalsIgnoreCase("null")?jsonObject4.get("balance").toString():"0");
		data.setBankAuth(jsonObject4.getString("bankAuth"));
		data.setBankResponseCode(""+jsonObject4.getString("bankResponseCode"));
		data.setBankResponseMsg(""+jsonObject4.getString("bankResponseMsg"));
		data.setBcaddress(jsonObject4.getString("bcaddress"));
		data.setBcname(jsonObject4.getString("bcname"));
		data.setCommissionAmt(jsonObject4.getDouble("commissionAmt"));
		try {
		data.setDateTime(jsonObject4.getInt("dateTime"));
		}catch(Exception epe) {
			
		}
		data.setGstAmt(jsonObject4.getDouble("gstAmt"));
		data.setIsWalletFailed(jsonObject4.getBoolean("isWalletFailed"));
		data.setName(jsonObject3.getString("name"));
		data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderStatus(jsonObject4.getString("orderStatus"));
		data.setPayeeId(""+jsonObject4.get("payeeId"));
		data.setPayeetype(""+jsonObject4.get("payeetype"));
		data.setPaymentStatus(jsonObject4.getString("paymentStatus"));
		data.setProcessingCode(jsonObject4.getString("processingCode"));
		data.setRedirectionUrl(jsonObject3.getString("redirectionUrl"));
		data.setRefno(jsonObject3.getString("refno"));
		data.setRequestId(jsonObject4.getString("requestId"));
		data.setRrn(jsonObject4.getString("rrn"));
		data.setStan(jsonObject4.getString("stan"));
		data.setStatusCode(jsonObject4.getString("statusCode"));
		data.setStatusMessage(""+jsonObject4.getString("statusMessage"));
		data.setTdsAmt(jsonObject4.getDouble("tdsAmt"));
		data.setTerminalId(jsonObject4.getString("terminalId"));
		data.setTxnId(""+jsonObject4.getString("txnId"));
		data.setTxnType(""+jsonObject4.getString("txnType"));
		data.setWalletMessage(""+jsonObject4.getString("walletMessage"));
	}catch(Exception e){
		e.printStackTrace();
	}
		logger.debug("***************************** Inside transactionManger *********************"+data.getOrderStatus());

		logger.debug("***************************** TxnStatus *********************"+data.getOrderStatus());
		logger.debug("***************************** TxnAmt  *********************"+data.getAmount());
		logger.debug("***************************** RRN *********************"+data.getRrn());
		logger.debug("***************************** Aadhar Number  *********************"+data.getAadharNumber());
		logger.debug("***************************** AvailableBalance *********************"+data.getAccountBalance());
		logger.debug("***************************** RefNo  *********************"+data.getRefno());
		
		
		AEPSDao aepsDao=new AEPSDaoImpl();
		
		aepsDao.persistTransaction(data);
		
		
	     
		return new AEPSLedger();
		
		
				
	}
	
	@POST
	@Path("/MatmStatusResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MATMLedger matmStatusResponse(String matmLedger2) throws JSONException{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","MatmResponse()--"+matmLedger2);
		try {
		matmLedger2=matmLedger2.replaceAll(" ","");
		
		JSONObject jsonObject=new JSONObject(matmLedger2);
		JSONObject jsonObject2=new JSONObject(jsonObject.get("payload").toString());
		JSONObject jsonObject3=new JSONObject(jsonObject2.get("metadata").toString());
		JSONObject jsonObject4=new JSONObject(jsonObject2.get("aeps").toString());
		
	
		
		//AepsResponseData data=new AepsResponseData();
		MatmResponseData data = new MatmResponseData();
		
		
		//data.setAadharNumber(jsonObject4.getString("aadharNumber"));
		data.setAccountBalance(jsonObject4.getString("accountBalance"));
		
		data.setAgentId(jsonObject4.getString("agentId"));
		
		data.setAmount(jsonObject4.getInt("amount"));
//		data.setBalance(""+Integer.parseInt((jsonObject4.get("balance")!=null&&!jsonObject4.get("balance").toString().equalsIgnoreCase("null"))?jsonObject4.get("balance").toString():"0"));
		data.setBankAuth(jsonObject4.getString("bankAuth"));
		data.setBankResponseCode(""+jsonObject4.get("bankResponseCode"));
		data.setBankResponseMsg(""+jsonObject4.get("bankResponseMsg"));
		data.setBcaddress(jsonObject4.getString("bcaddress"));
		data.setBcname(jsonObject4.getString("bcname"));
		data.setCommissionAmt(jsonObject4.getDouble("commissionAmt"));
		data.setDateTime(jsonObject4.getString("dateTime"));
		//data.setGstAmt(jsonObject4.getDouble("gstAmt"));
		//data.setIsWalletFailed(jsonObject4.getBoolean("isWalletFailed"));
		data.setName(jsonObject3.getString("name"));
		data.setOrderId(jsonObject4.getString("orderId"));
		//data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderStatus(jsonObject4.getString("orderStatus"));
		data.setPayeeId(""+jsonObject4.get("payeeId"));
		data.setPayeetype(""+jsonObject4.get("payeetype"));
		data.setPaymentStatus(jsonObject4.getString("paymentStatus"));
		data.setProcessingCode(jsonObject4.getString("processingCode"));
		data.setRedirectionUrl(jsonObject3.getString("redirectionUrl"));
		data.setRefno(jsonObject3.getString("refno"));
		data.setRequestId(jsonObject4.getString("requestId"));
		data.setRrn(jsonObject4.getString("rrn"));
		data.setStan(jsonObject4.getString("stan"));
		data.setStatusCode(jsonObject4.getString("statusCode"));
		data.setStatusMessage(""+jsonObject4.get("statusMessage"));
		//data.setTdsAmt(jsonObject4.getDouble("tdsAmt"));
		data.setTerminalId(jsonObject4.getString("terminalId"));
		data.setTxnId(""+jsonObject4.getString("txnId"));
		data.setTxnType(""+jsonObject4.getString("txnType"));
		data.setCardNumber(jsonObject4.getString("cardnumber"));
		data.setWalletMessage(""+jsonObject4.getString("walletMessage"));
		
		
		logger.debug("***************************** Inside transactionManger *********************"+data.getOrderStatus());

		logger.debug("***************************** TxnStatus *********************"+data.getOrderStatus());
		logger.debug("***************************** TxnAmt  *********************"+data.getAmount());
		logger.debug("***************************** RRN *********************"+data.getRrn());
		logger.debug("***************************** AvailableBalance *********************"+data.getAccountBalance());
		logger.debug("***************************** RefNo  *********************"+data.getRefno());
		logger.debug("***************************** CardNumber  *********************"+data.getCardNumber());
		logger.debug("***************************** TerminalId  *********************"+data.getTerminalId());
		
		
		MATMDao matmDao = new MATMDaoImpl();
		
		//AEPSDao aepsDao=new AEPSDaoImpl();
		
		//aepsDao.persistTransaction(data);
		matmDao.persistTransaction(data);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	     
		return new MATMLedger();
		
		
				
	}
	
	
	@POST
	@Path("/sendSMSAlert")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendSMSAlert(TxnInputBean inputBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"sendSMSAlert()--" + inputBean.getTxnId());
		AEPSDao aepsDao = new AEPSDaoImpl();
		return aepsDao.sendSMSAlert(inputBean);
	}
	
	@POST
	@Path("/MatmResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	public MATMLedger MatmResponse(String matmLedger2) throws JSONException{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","MatmResponse()--"+matmLedger2);
		try {
		matmLedger2=matmLedger2.replaceAll(" ","");
		
		JSONObject jsonObject=new JSONObject(matmLedger2);
		JSONObject jsonObject2=new JSONObject(jsonObject.get("payload").toString());
		JSONObject jsonObject3=new JSONObject(jsonObject2.get("metadata").toString());
		JSONObject jsonObject4=new JSONObject(jsonObject2.get("aeps").toString());
		
	
		
		//AepsResponseData data=new AepsResponseData();
		MatmResponseData data = new MatmResponseData();
		
		
		//data.setAadharNumber(jsonObject4.getString("aadharNumber"));
		data.setAccountBalance(jsonObject4.getString("accountBalance"));
		
		data.setAgentId(jsonObject4.getString("agentId"));
		
		data.setAmount(jsonObject4.getInt("amount"));
//		data.setBalance(""+Integer.parseInt((jsonObject4.get("balance")!=null&&!jsonObject4.get("balance").toString().equalsIgnoreCase("null"))?jsonObject4.get("balance").toString():"0"));
		data.setBankAuth(jsonObject4.getString("bankAuth"));
		data.setBankResponseCode(""+jsonObject4.get("bankResponseCode"));
		data.setBankResponseMsg(""+jsonObject4.get("bankResponseMsg"));
		data.setBcaddress(jsonObject4.getString("bcaddress"));
		data.setBcname(jsonObject4.getString("bcname"));
		data.setCommissionAmt(jsonObject4.getDouble("commissionAmt"));
		data.setDateTime(jsonObject4.getString("dateTime"));
		//data.setGstAmt(jsonObject4.getDouble("gstAmt"));
		//data.setIsWalletFailed(jsonObject4.getBoolean("isWalletFailed"));
		data.setName(jsonObject3.getString("name"));
		data.setOrderId(jsonObject4.getString("orderId"));
		//data.setOrderId(jsonObject4.getString("orderId"));
		data.setOrderStatus(jsonObject4.getString("orderStatus"));
		data.setPayeeId(""+jsonObject4.get("payeeId"));
		data.setPayeetype(""+jsonObject4.get("payeetype"));
		data.setPaymentStatus(jsonObject4.getString("paymentStatus"));
		data.setProcessingCode(jsonObject4.getString("processingCode"));
		data.setRedirectionUrl(jsonObject3.getString("redirectionUrl"));
		data.setRefno(jsonObject3.getString("refno"));
		data.setRequestId(jsonObject4.getString("requestId"));
		data.setRrn(jsonObject4.getString("rrn"));
		data.setStan(jsonObject4.getString("stan"));
		data.setStatusCode(jsonObject4.getString("statusCode"));
		data.setStatusMessage(""+jsonObject4.get("statusMessage"));
		//data.setTdsAmt(jsonObject4.getDouble("tdsAmt"));
		data.setTerminalId(jsonObject4.getString("terminalId"));
		data.setTxnId(""+jsonObject4.getString("txnId"));
		data.setTxnType(""+jsonObject4.get("txnType"));
		data.setCardNumber(jsonObject4.getString("cardnumber"));
		data.setWalletMessage(""+jsonObject4.get("walletMessage"));
		
		
		logger.debug("***************************** Inside transactionManger *********************"+data.getOrderStatus());

		logger.debug("***************************** TxnStatus *********************"+data.getOrderStatus());
		logger.debug("***************************** TxnAmt  *********************"+data.getAmount());
		logger.debug("***************************** RRN *********************"+data.getRrn());
		logger.debug("***************************** AvailableBalance *********************"+data.getAccountBalance());
		logger.debug("***************************** RefNo  *********************"+data.getRefno());
		logger.debug("***************************** CardNumber  *********************"+data.getCardNumber());
		logger.debug("***************************** TerminalId  *********************"+data.getTerminalId());
		
		
		MATMDao matmDao = new MATMDaoImpl();
		
		//AEPSDao aepsDao=new AEPSDaoImpl();
		
		//aepsDao.persistTransaction(data);
		matmDao.persistTransaction(data);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	     
		return new MATMLedger();
		
		
				
	}
	
	
	
	
	@POST
	@Path("/getMatmLedger")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	public MATMLedger getMatmLedger(String txnId) throws JSONException{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","MatmResponse()--"+txnId);
		try {
		
		JSONObject jsonObject=new JSONObject(txnId);
		
		MATMDao matmDao = new MATMDaoImpl();

		return matmDao.getMatmLedger(jsonObject.get("txnId").toString());
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@POST
	@Path("/asAepsResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
		public String asAepsResponse(@HeaderParam("developer_key") String developer_key,String aepsLedger2) throws JSONException{
				
		if(!"Ib0buhlDB3AIn505e3kJag==".equalsIgnoreCase(developer_key.trim())) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response not valid developer_key-"+developer_key);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response not valid json values-"+aepsLedger2);

			return "{\"Status\":\"1\",\"Description\":\"Transaction failed! invalid credintial.\"}";	
		}
//		"rrn":"009221188797","stanNo":"23330650","txnId":"20040121530143356709",
//		"aepstxnId":"677612552256","action":"Credit","device":"Morpho MSO 1300",
//		"status":"0","txnStatus":"Success","bankName":"IDBI Bank","mobileNo":"9874870009",
//		"uId":"XXXXXXXX2624","authCode":"","deviceNo":"1911I012677","balance":"1003.72",
//		"Agent_Id":"7479325191","Service":"Cash Withdrawl","Amount":1000.0;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response  json values--"+aepsLedger2);
//		aepsLedger2=aepsLedger2.replaceAll(" ","");
		JSONObject jsonObject=new JSONObject(aepsLedger2);
		
		String rrn = null;
		String stanNo = null;
		String txnId = null;
		String aepstxnId = null;
		String action = null;
		String device = null;
		String status = null;
		String txnStatus = null;
		String bankName = null;
		String mobileNo = null;
		String uId = null;
		String authCode = null;
		String deviceNo = null;
		String balance = null;
		String agentId = null;
		String service = null;
		double amount = 0; 
		
		if(jsonObject != null ) {
		 if(jsonObject.has("rrn"))
		 rrn = jsonObject.get("rrn")!=null ? jsonObject.getString("rrn") : "-";
		 if(jsonObject.has("stanNo"))
		 stanNo = jsonObject.getString("stanNo")!=null ? jsonObject.getString("stanNo") : "-";
		 if(jsonObject.has("txnId"))
		 txnId = jsonObject.getString("txnId")!=null ? jsonObject.getString("txnId") : "-";
		 if(jsonObject.has("aepstxnId"))
		 aepstxnId = jsonObject.getString("aepstxnId")!=null ? jsonObject.getString("aepstxnId") : "-";
		 if(jsonObject.has("device"))
		 action = jsonObject.getString("action")!=null ? jsonObject.getString("action") : "-";
		 if(jsonObject.has("device"))
		 device = jsonObject.getString("device")!=null ? jsonObject.getString("device") : "-";
		 if(jsonObject.has("status"))
		 status = jsonObject.getString("status")!=null ? jsonObject.getString("status") : "-";
		 if(jsonObject.has("txnStatus"))
		 txnStatus = jsonObject.getString("txnStatus")!=null ? jsonObject.getString("txnStatus") : "-";
		 if(jsonObject.has("bankName"))
		 bankName = jsonObject.getString("bankName")!=null ? jsonObject.getString("bankName") : "-";
		 if(jsonObject.has("mobileNo"))
		 mobileNo = jsonObject.getString("mobileNo")!=null ? jsonObject.getString("mobileNo") : "-";
		 if(jsonObject.has("uId"))
		 uId = jsonObject.getString("uId")!=null ? jsonObject.getString("uId") : "-";
		 if(jsonObject.has("authCode"))
		 authCode = jsonObject.getString("authCode")!=null ? jsonObject.getString("authCode") : "-";
		 if(jsonObject.has("deviceNo"))
		 deviceNo = jsonObject.getString("deviceNo")!=null ? jsonObject.getString("deviceNo") : "-";
		 if(jsonObject.has("balance"))
		 balance = jsonObject.getString("balance")!=null ? jsonObject.getString("balance") : "-";
		 if(jsonObject.has("Agent_Id"))
		 agentId = jsonObject.getString("Agent_Id")!=null ? jsonObject.getString("Agent_Id") : "-";
		 if(jsonObject.has("Service"))
		 service = jsonObject.getString("Service")!=null ? jsonObject.getString("Service") : "-";
		 if(jsonObject.has("Amount"))
		 amount = jsonObject.getDouble("Amount")!= 0 ? jsonObject.getDouble("Amount") : 00 ;
		
		}
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject rrn--"+rrn);;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject stanNo--"+stanNo);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject txnId--"+txnId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject aepstxnId--"+aepstxnId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject action--"+action);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject device--"+device);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject status--"+status);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject txnStatus--"+txnStatus);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject bankName--"+bankName);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject mobileNo--"+mobileNo);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject uId--"+uId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject authCode--"+authCode);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject deviceNo--"+deviceNo);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject balance--"+balance);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject agentId--"+agentId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject service--"+service);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject amount--"+amount); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila aeps response jsonobject --"+jsonObject);
		
		AepsResponseData data=new AepsResponseData();
		data.setAgentId(agentId);
		int amt = (int)amount;
		data.setAmount(amt);
		data.setBankResponseMsg("");
		data.setOrderId(txnId);
		data.setOrderStatus(txnStatus);
		data.setPaymentStatus(txnStatus);
		data.setRefno(aepstxnId);
		data.setRequestId("");
		data.setRrn(rrn);
		data.setStan(stanNo);
		data.setStatusCode(status);
		data.setStatusMessage(txnStatus);
		data.setTxnId(txnId);
		data.setProcessingCode(action);
		data.setTxnType(service);
		data.setBcname(bankName);
		data.setAadharNumber(uId);
		data.setBalance(balance);
		
		AEPSDao aepsDao=new AEPSDaoImpl();
		
		AEPSLedger aepsLedger3 = aepsDao.asPersistTransaction(data);
		
		
		    if(aepsLedger3 != null && "Success".equalsIgnoreCase(aepsLedger3.getPaymentStatus())) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila returning response to aadharshila --txnId"+"{\"Status\":\"0\",\"Description\":\\\"Transaction Success.\"}");
		return "{\"Status\":\"0\",\"Description\":\"Transaction Success.\"}";
		    }
		    else {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," aadharshila returning response to aadharshil --txnId"+"{\"Status\":\"1\",\"Description\":\"Transaction failed.\"}");
		return "{\"Status\":\"1\",\"Description\":\"Transaction failed.\"}";	
		    }
		//	� BALANCE NOT AVAILABLE IN BENEFICIARY BANK ACCOUNT  � DAILY WITHDRAWAL LIMIT EXCEEDED  � BIOMETRIC DATA DID NOT MATCH  � AADHAAR NOT LINKED WITH BANK  � MAXIMUM TRANSACTION LIMIT EXCEEDED
				
			}
	
	
	
	@POST
	@Path("/profilebytxnId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public UserProfileBean profilebytxnId(TxnInputBean txnInputBean){
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"profilebytxnId()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		return transactionDao.profilebytxnId( txnInputBean.getTxnId());
	}
	
	
	@POST
	@Path("/profilebyPGtxnId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public PGPayeeBean profilebyPGtxnId(TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"profilebyPGtxnId()");
		
		// logger.info("Start excution ===================== method profilebyPGtxnId(txnInputBean)");
			return transactionDao.profilebyPGtxnId( txnInputBean.getTxnId());
		 
	 }
	
	
	
	@POST
	@Path("/showPassbook")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String showPassbook(TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showPassbook()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.showPassbook( txnInputBean.getWalletId(),txnInputBean.getStDate(),txnInputBean.getEndDate()));
	}
	
	
	@POST
	@Path("/getAmountPopup")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getAmountPopup(String txnIdPassed){
		
	      List response=null;
		  ClientResponse message=null;
		  GsonBuilder builder = new GsonBuilder();
		    Gson gson = builder.create();
		try {
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		 response=transactionDao.getAmountPopup(txnIdPassed);
		 
		 if (response.size()==0) {
	  	  return "something went wrong.";
		  }
		 
		 //System.out.println("RESPONSE  "+response);
		 
			/*
			 * if(response.equals("success")) { response= "success"; message.setStatus(200);
			 * } else { response="error"; message.setStatus(300); }
			 */
		}catch(Exception e) {
	e.printStackTrace();
	
	}
		return gson.toJson(response);
		
	}
	
	
	
	
	@POST
	@Path("/pgTransactionsUpdate")
	public String pgTransactionsUpdate(TxnInputBean txnInputBean){
		
	      String response=null;
		  ClientResponse message=null;
		 
		try {
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		 response=transactionDao.txnUpdate(txnInputBean);
		 //System.out.println("RESPONSE  "+response);
		 
			/*
			 * if(response.equals("success")) { response= "success"; message.setStatus(200);
			 * } else { response="error"; message.setStatus(300); }
			 */
		}catch(Exception e) {
	e.printStackTrace();
	response=e.toString();
	}
		return response;
		
	}
	
		
	@POST
	@Path("/showOrder")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String showOrder(TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showOrder()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.showOrder(txnInputBean.getWalletId(),txnInputBean.getStDate(),txnInputBean.getEndDate()));
	}
	
	
	
	@POST
	@Path("/reCharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String reCharge(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"reCharge()");
		
		//logger.info("Start excution ===================== method reCharge(userId, payeeWalletid, amount, rechageNumber, rechargeTxnId, ipImei)");
		return transactionDao.reCharge(txnInputBean.getUserId(), txnInputBean.getWalletId(), txnInputBean.getTrxAmount(), txnInputBean.getPayeeNumber(), txnInputBean.getPayeeTxnId(), imei,txnInputBean.getAggreatorid(),agent);
		 
	 }
	
	
	@POST
	@Path("/walletToAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String walletToAccount(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"walletToAccount()");
		
		//logger.info("Start excution ===================== method reCharge(userId, payeeWalletid, amount, rechageNumber, rechargeTxnId, ipImei)");
		return transactionDao.walletToAccount(txnInputBean.getUserId(), txnInputBean.getWalletId(), txnInputBean.getTrxAmount(), txnInputBean.getPayeeNumber(), txnInputBean.getPayeeTxnId(), imei,txnInputBean.getAggreatorid(),agent,txnInputBean.getSurChargeAmount());
	 }
	
	
	
	@POST
	@Path("/walletToWallet")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String walletToWallet(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"walletToWallet()"+"|"+agent);
		
		//logger.info("Start excution ===================== method walletToWalletTransfer(userId,sendWalletid,recMobile,amount,ipImei)");
		//System.out.println("Start excution ===================== method walletToWalletTransfer(userId,sendWalletid,recMobile,amount,ipImei)");
		return transactionDao.walletToWalletTransfer(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), imei,txnInputBean.getAggreatorid(),agent,txnInputBean.getCHECKSUMHASH());
	 }
	
	
	@POST
	@Path("/cashIn")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String cashIn(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"cashIn()"+"|"+agent);
		
		//logger.info("Start excution ===================== method walletToWalletTransfer(userId,sendWalletid,recMobile,amount,ipImei)");
		return transactionDao.cashIn(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), imei,txnInputBean.getAggreatorid(),agent,txnInputBean.getSurChargeAmount(),txnInputBean.getCHECKSUMHASH());
	 }
	
	
	
	
	
		
	@POST
	@Path("/askMoney")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String askMoney(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"askMoney()"+"|"+agent);
		
		//logger.info("Start excution ===================== method askMoney(userid,reqwalletId,resmobile,amount,ipImei)");
		return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei(),txnInputBean.getAggreatorid(),agent);
	}
	
	@POST
	@Path("/askMoneyUpdate")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String askMoneyUpdate(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"askMoneyUpdate()|"+agent);
		
		//logger.info("Start excution ===================== method askMoneyUpdate(userid,trxId,txnstatus,ipImei,msgId)");
		return transactionDao.askMoneyUpdate(txnInputBean.getUserId(), txnInputBean.getTxnId(),txnInputBean.getStatus(),txnInputBean.getIpImei(),txnInputBean.getMsgId(),txnInputBean.getAggreatorid(),agent);
	}
	
		
/*	@POST
	@Path("/tansferInAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public WalletToBankTxnMast tansferInAccount(WalletToBankTxnMast walletToBankTxnMast){
		logger.info("Start excution ===================== method tansferInAccount(walletToBankTxnMast)");
		return transactionDao.tansferInAccount(walletToBankTxnMast);
	 }*/
	
	@POST
	@Path("/goForPayment")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	 public RechargeTxnBean payForPayment(@HeaderParam("IPIMEI") String ipiemi,
			 							  @HeaderParam("AGENT") String agent,
			 							  RechargeTxnBean rechargeTxnBean){
		
		
	      
		  // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"payForPayment()"+"|"+agent);
		
		//logger.info("Start excution =========*************************************************============ method payForPayment(rechargeTxnBean)"+rechargeTxnBean.getRechargeNumber());
		
		rechargeTxnBean.setIpIemi(ipiemi);
		rechargeTxnBean.setTxnAgent(agent);
	
		return transactionDao.payForPayment(rechargeTxnBean);
		 
	 }

	@POST
	@Path("/getRechargeStatusNew")
	public Result getRechargeStatusNew(Result result){
		
		
	      
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"getRecharge Status()");
		
	 logger.info("Start excution =========*************************************************============ method rechargeStatusNew()"+"");
	 
		return transactionDao.getRechargeStatusNew(result);
		 
	 }
	
	@POST
	@Path("/getBillerInfo")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	 public BillerInfoResponse payForPayment(@HeaderParam("IPIMEI") String ipiemi,
			 							  @HeaderParam("AGENT") String agent,
			 							  BillInfoRequest billRequest){
			
		return transactionDao.getBillerInfo(billRequest, ipiemi, agent);
		 
	 }

	
	@POST
	@Path("/moneyRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public MoneyRequestBean moneyRequest(CashDepositMast requestBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getUserId(), "","" ,"moneyRequest()");
		
		//logger.info("Start excution =========*************************************************============ method moneyRequest(requestBean)");
		return transactionDao.moneyRequest(requestBean);
	 }
	
	
	@POST
	@Path("/getWalletBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String getWalletBalance(TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "","" ,"getWalletBalance()");
		
		//logger.info("Start excution ===================== method getWalletBalance(txnInputBean)"+txnInputBean.getWalletId());
		return ""+transactionDao.getWalletBalance(txnInputBean.getWalletId());
	 }
	
	
	@POST
	@Path("/getWalletBalancebyId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String getWalletBalancebyId(TxnInputBean txnInputBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "","" ,"getWalletBalancebyId()");
		
		//logger.info("Start excution ===================== method getWalletBalancebyId(txnInputBean)"+txnInputBean.getWalletId());
		return ""+transactionDao.getWalletBalancebyId(txnInputBean.getWalletId());
	 }
	
	 
	
	
	
	@POST
	@Path("/P2MTransaction")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public P2MTransactionBean P2MTransaction(P2MTransactionBean p2MTransactionBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"P2MTransaction()");
		
		//logger.info("Start excution ************************************ method P2MTransaction(p2MTransactionBean)"+p2MTransactionBean.getWalletId());
		return transactionDao.P2MTransaction(p2MTransactionBean);
	 }
	
	
	@POST
	@Path("/payment/authorization")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public PrePaidCardResponse prePaidCardPaymentAutorization (PrePaidCardTxnBean prePaidCardTxnBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"prePaidCardPaymentAutorization()"+"|"+prePaidCardTxnBean.getCard_id());
		
		//logger.info("Start excution ************************************ method prePaidCardPaymentAutorization(prePaidCardTxnBean)"+prePaidCardTxnBean.getCard_id());
		return transactionDao.prePaidCardPaymentAutorization (prePaidCardTxnBean);
	 }
	
	
	@POST
	@Path("/getCashDepositReqByUserId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCashDepositReqByUserId(CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"getCashDepositReqByUserId()");
		
		
		//logger.info("Start excution ===================== method getCashDepositReqByUserId(cashDepositMast)"+cashDepositMast.getUserId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getCashDepositReqByUserId( cashDepositMast.getUserId(),cashDepositMast.getStDate(),cashDepositMast.getEndDate()));
	}
	
	@POST
	@Path("/getCashDepositReqByUserIdPending")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCashDepositReqByUserIdPending(CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"getCashDepositReqByUserId()");
		
		
		//logger.info("Start excution ===================== method getCashDepositReqByUserId(cashDepositMast)"+cashDepositMast.getUserId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getCashDepositReqByUserIdPending( cashDepositMast.getUserId(),cashDepositMast.getStDate(),cashDepositMast.getEndDate()));
	}
	
	@POST
	@Path("/requsetCashDeposit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CashDepositMast requsetCashDeposit(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"requsetCashDeposit()"+"|"+agent);
		
		//logger.info("Start excution ************************************ method requsetCashDeposit(cashDepositMast)"+cashDepositMast.getUserId());
		cashDepositMast.setIpIemi(ipIemi);
		cashDepositMast.setAgent(agent);
		return transactionDao.requsetCashDeposit(cashDepositMast);
	}
	
	
	@POST
	@Path("/getCashDepositReqByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCashDepositReqByAggId(CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"getCashDepositReqByAggId()");
		
		//logger.info("Start excution ===================== method getCashDepositReqByAggId(cashDepositMast)"+cashDepositMast.getUserId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getCashDepositReqByAggId( cashDepositMast.getAggreatorId(),cashDepositMast.getType()));
	}
	
	
	@POST
	@Path("/rejectCashDeposit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String rejectCashDeposit(CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"rejectCashDeposit()|"+cashDepositMast.getDepositId());
		
		//logger.info("Start excution ************************************ method rejecttCashDeposit(cashDepositMast)"+cashDepositMast.getDepositId());
		return ""+transactionDao.rejectCashDeposit(cashDepositMast.getDepositId(),cashDepositMast.getRemark());
	 }
	
	
	@POST
	@Path("/acceptCashDeposit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String acceptCashDeposit(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"acceptCashDeposit()"+"|"+cashDepositMast.getDepositId());
		
		//logger.info("Start excution ************************************ method acceptCashDeposit(cashDepositMast)"+cashDepositMast.getDepositId());
		return ""+transactionDao.acceptCashDeposit(cashDepositMast.getDepositId(),cashDepositMast.getRemark(),ipIemi,agent);
	 }
	
	
	@POST
	@Path("/acceptCashDepositByDist")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String acceptCashDepositByDist(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CashDepositMast cashDepositMast){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"acceptCashDepositByDist()"+"|"+cashDepositMast.getDepositId());
		
		//logger.info("Start excution ************************************ method acceptCashDepositByDist(cashDepositMast)"+cashDepositMast.getDepositId());
		return ""+transactionDao.acceptCashDepositByDist(cashDepositMast.getDepositId(),cashDepositMast.getRemark(),ipIemi,agent);
	 }
	
	
	@POST
	@Path("/requestRefund")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public RefundMastBean requestRefund(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,RefundMastBean refundMastBean){
		
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "","" ,"requestRefund()");
		
		//logger.info("Start excution ************************************ method requestRefund(refundMastBean)"+refundMastBean.getUserId());
		 refundMastBean.setIpIemi(ipIemi);
		 refundMastBean.setAgent(agent);
		 return transactionDao.requestRefund(refundMastBean);
		 }
	
	
	@POST
	@Path("/getRefundReqByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getRefundReqByAggId(RefundMastBean refundMastBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "","" ,"getRefundReqByAggId()");
		//logger.info("Start excution ===================== method getRefundReqByAggId(refundMastBean)"+refundMastBean.getAggreatorId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getRefundReqByAggId( refundMastBean.getAggreatorId()));
	}
	
	
	@POST
	@Path("/rejectRefundReq")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String rejectRefundReq(RefundMastBean refundMastBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "","" ,"rejectRefundReq()"+"|"+refundMastBean.getRefundId());
		
		//logger.info("Start excution ************************************ method rejectRefundReq(cashDepositMast)"+refundMastBean.getRefundId());
		return ""+transactionDao.rejectRefundReq(refundMastBean.getRefundId());
	 }
	
	
	@POST
	@Path("/acceptRefundReq")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String acceptRefundReq(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,RefundMastBean refundMastBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "","" ,"acceptRefundReq()"+"|"+refundMastBean.getRefundId());
		
		//logger.info("Start excution ************************************ method acceptRefundReq(refundMastBean)"+refundMastBean.getRefundId());
		return ""+transactionDao.acceptRefundReq(refundMastBean.getRefundId(),ipIemi,agent);
	 }
	
	@POST
	@Path("/processRefund")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public List<RefundTransactionBean> processRefund(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,String txnIds){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnIds ,"processRefund()");
		
		//logger.info("Start excution ************************************ method processRefund(txnids+>)"+txnIds);
		return transactionDao.processRefund(ipIemi, agent, txnIds);
	 }
	
	
	@POST
	@Path("/getCashOut")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public AskMoneyBean getCashOut(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"getCashOut()|"+txnInputBean.getSurChargeAmount());
		
		//logger.info("Start excution ===================== method getCashOut(userid,reqwalletId,resmobile,amount,ipImei)");
		//logger.info("Start excution ===================== method getCashOut SurChargeAmount*****"+txnInputBean.getSurChargeAmount());
		return transactionDao.getCashOut(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei(),txnInputBean.getAggreatorid(),agent,txnInputBean.getSurChargeAmount());
	}
	
	
	@POST
	@Path("/cashOutUpdate")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public AskMoneyBean cashOutUpdate(AskMoneyBean askMoneyBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "",askMoneyBean.getTrxid() ,"cashOutUpdate()");
		
		//logger.info("Start excution ===================== method askMoney(userid,reqwalletId,resmobile,amount,ipImei)");
		return transactionDao.cashOutUpdate(askMoneyBean);
	}
	
	@POST
	@Path("/calculateCashinSurcharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean){
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateCashinSurcharge()"+"|"+surchargeBean.getId()+"|"+surchargeBean.getAmount());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,methodname+"|"+surchargeBean.getAmount());
		
		//logger.info("Start excution ===================== method calculateCashinSurcharge(surchargeBean)"+surchargeBean.getId());
		//logger.info("Start excution ===================== method calculateCashinSurcharge(surchargeBean)"+surchargeBean.getAmount());
		return transactionDao.calculateCashinSurcharge(surchargeBean);
	 }
	
	
	@POST
	@Path("/calculateCashOutSurcharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateCashOutSurcharge()"+"|"+surchargeBean.getId()+"|"+surchargeBean.getAmount());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,methodname+"|"+surchargeBean.getAmount());
		//logger.info("Start excution ===================== method calculateCashOutSurcharge(surchargeBean)"+surchargeBean.getId());
		//logger.info("Start excution ===================== method calculateCashOutSurcharge(surchargeBean)"+surchargeBean.getAmount());
		return transactionDao.calculateCashOutSurcharge(surchargeBean);
	 }
	

	@POST
	@Path("/calculateDMTSurcharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public SurchargeBean calculateDMTSurcharge(SurchargeBean surchargeBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateDMTSurcharge()"+"|"+surchargeBean.getId()+"|"+surchargeBean.getAmount());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,methodname+"|"+surchargeBean.getAmount());
		
		
		//logger.info("Start excution ===================== method calculateDMTSurcharge(surchargeBean)"+surchargeBean.getId());
		//logger.info("Start excution ===================== method calculateDMTSurcharge(surchargeBean)"+surchargeBean.getAmount());
		return transactionDao.calculateDMTSurcharge(surchargeBean);
	 }
	
	@POST
	@Path("/payment/reversal")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public PrePaidCardResponse prePaidCardReversal (PrePaidCardTxnBean prePaidCardTxnBean){
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"prePaidCardReversal()"+"|"+prePaidCardTxnBean.getTransaction_ref_no());
		
		
		//logger.info("Start excution ************************************ method prePaidCardReversal(prePaidCardTxnBean)"+prePaidCardTxnBean.getTransaction_ref_no());
		return transactionDao.prePaidCardReversal (prePaidCardTxnBean);
	 }
	

	@POST
	@Path("/payment/authorizationMachMove")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public PrePaidCardResponse authorizationMachMove (String val){
		PrePaidCardTxnBean prePaidCardTxnBean=null;
		try {
			
		      
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"authorizationMachMove()"+"|"+"card_id"+val);
			
		//logger.info("*********************card_id*******1*********************"+val);
		String [] splitval  =val.substring(val.indexOf('{')+1,val.indexOf('}')).split(",");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"|"+"splitval 2"+splitval.length);
		//logger.info("*********************splitval**********2******************"+splitval.length);
		JSONObject json=new JSONObject();
		for(int i=0;i<splitval.length;i++){ 
			String [] _split=splitval[i].split("=");
			if(_split.length>1)
				json.put(_split[0].trim(), _split[1].trim());
			
		}
		
		 prePaidCardTxnBean =  new Gson().fromJson(json.toString(), PrePaidCardTxnBean.class);
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"|"+"*cardAmount 3"+prePaidCardTxnBean.getCard_amount()+"Card id"+prePaidCardTxnBean.getCard_id());
		// Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"|"+"Card id"+prePaidCardTxnBean.getCard_id());
		 //logger.info("*********************cardAmount*********3*******************"+prePaidCardTxnBean.getCard_amount());
		//logger.info("Start excution ************************************ method authorizationMachMove(prePaidCardTxnBean)"+prePaidCardTxnBean.getCard_id());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionDao.prePaidCardPaymentAutorization (prePaidCardTxnBean);
	 }
	
	@POST
	@Path("/payment/reversalMachMove")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	 public PrePaidCardResponse prePaidCardReversalMachMove ( String transactionRefno){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"prePaidCardReversalMachMove()"+"|"+transactionRefno);
		
		//logger.info("*********************transaction_ref_no****************************"+transactionRefno);
		transactionRefno=transactionRefno.substring(transactionRefno.indexOf('{')+1,transactionRefno.indexOf('}'));
		String [] _split=transactionRefno.split("=");
		PrePaidCardTxnBean prePaidCardTxnBean=new PrePaidCardTxnBean();
		prePaidCardTxnBean.setTransaction_ref_no(_split[1].trim());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"|"+prePaidCardTxnBean.getTransaction_ref_no());
		//logger.info("Start excution ************************************ method prePaidCardReversal(prePaidCardTxnBean)"+prePaidCardTxnBean.getTransaction_ref_no());
		return transactionDao.prePaidCardReversal (prePaidCardTxnBean);
	 }
	
	
	
	
	@POST
	@Path("/getWalletBalanceios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String getWalletBalanceios(TxnInputBean txnInputBean){
		JSONObject json=new JSONObject();
		try {
		json.put("status", "1001");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"getWalletBalanceios()");
		
		//logger.info("Start excution ===================== method getWalletBalance(txnInputBean)"+txnInputBean.getWalletId());
		json.put("status", transactionDao.getWalletBalance(txnInputBean.getWalletId()));
		} catch (Exception e) {
		e.printStackTrace();
		}
		return json.toString();
		
	 }
	
	
	@POST
	@Path("/walletToWalletios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String walletToWalletios(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		JSONObject json=new JSONObject();
		try {
		json.put("status", "1001");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"walletToWalletios()"+"|"+agent);
		
		//logger.info("Start excution ===============walletToWalletios====== method walletToWalletios(userId,sendWalletid,recMobile,amount,ipImei)");
		json.put("status", transactionDao.walletToWalletTransfer(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), imei,txnInputBean.getAggreatorid(),agent,txnInputBean.getCHECKSUMHASH()));
		} catch (Exception e) {
		e.printStackTrace();
		}
		return json.toString();
		
	 }
	
	
	@POST
	@Path("/askMoneyios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String askMoneyios(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,TxnInputBean txnInputBean){
		JSONObject json=new JSONObject();
		try {
		json.put("status", "1001");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"askMoneyios()"+"|"+agent);
		
		//logger.info("Start excution ===============askMoneyios====== method askMoneyios(userid,reqwalletId,resmobile,amount,ipImei)");
		json.put("status", transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei(),txnInputBean.getAggreatorid(),agent));
		} catch (Exception e) {
		e.printStackTrace();
		}
		return json.toString();
		
	}
	
	
	
	@POST
	@Path("/b2CMoneyTxn")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public B2CMoneyTxnMast b2CMoneyTxn(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,B2CMoneyTxnMast b2CMoneyTxnMast){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ,"b2CMoneyTxn()"+"|"+agent);
		
		//logger.info("Start excution ===================== method b2CMoneyTxn(b2CMoneyTxnMast)");
		b2CMoneyTxnMast.setUseragent(agent);
		b2CMoneyTxnMast.setIpimei(imei);
		return transactionDao.b2CMoneyTxn( b2CMoneyTxnMast);
	}
	
	 
	
	@POST
	@Path("/getCashDepositReportByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCashDepositReportByAggId(CashDepositMast cashDepositMast){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"getCashDepositReportByAggId()");
		
		//logger.info("Start excution ===================== method getCashDepositReqByAggId(cashDepositMast)"+cashDepositMast.getUserId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getCashDepositReportByAggId( cashDepositMast.getAggreatorId(),cashDepositMast.getStDate(),cashDepositMast.getEndDate(),cashDepositMast.getType()));
	}
	
	
	@POST
	 @Path("/showPassbookById")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String showPassbookById(TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showPassbookById()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
	  GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(transactionDao.showPassbookById( txnInputBean.getUserId(),txnInputBean.getStDate(),txnInputBean.getEndDate(),txnInputBean.getAggreatorid()));
	 }
	
	
	
	@POST
	@Path("/getCashBackWalletBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String getCashBackWalletBalance(TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"getCashBackWalletBalance()");
		
		//logger.info("Start excution ===================== method getCashBackWalletBalance(txnInputBean)"+txnInputBean.getWalletId());
		return ""+transactionDao.getCashBackWalletBalance(txnInputBean.getWalletId());
	 }
	
	
	
	@POST
	@Path("/getCashBackWalletBalancebyId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public String getCashBackWalletBalancebyId(TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"getCashBackWalletBalancebyId()");
		
		//logger.info("Start excution ===================== method getCashBackWalletBalancebyId(txnInputBean)"+txnInputBean.getWalletId());
		return ""+transactionDao.getCashBackWalletBalancebyId(txnInputBean.getWalletId());
	 }
	
	
	
	@POST
	@Path("/showCashBackPassbook")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String showCashBackPassbook(TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showCashBackPassbook()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.showCashBackPassbook( txnInputBean.getWalletId(),txnInputBean.getStDate(),txnInputBean.getEndDate()));
	}
	
	
	
	@POST
	 @Path("/showCashBackPassbookById")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String showCashBackPassbookById(TxnInputBean txnInputBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showCashBackPassbookById()");
		
	  //logger.info("Start excution ===================== method showCashBackPassbookById(txnInputBean)");
	  GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(transactionDao.showCashBackPassbookById( txnInputBean.getUserId(),txnInputBean.getStDate(),txnInputBean.getEndDate()));
	 }
	
	
	
	
	
	
	@POST
	@Path("/getCDAReqByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCDAReqByAggId(CashDepositMast cashDepositMast){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"getCDAReqByAggId()");
		
		//logger.info("Start excution ===================== method getCDAReqByAggId(cashDepositMast)"+cashDepositMast.getUserId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.getCDAReqByAggId( cashDepositMast.getAggreatorId(),cashDepositMast.getType()));
	}
	
	
	@POST
	@Path("/rejectApproverCashDeposit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String rejectApproverCashDeposit(CashDepositMast cashDepositMast){
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"rejectApproverCashDeposit()"+"|"+cashDepositMast.getDepositId());
		
		//logger.info("Start excution ************************************ method rejectApproverCashDepositl(cashDepositMast)"+cashDepositMast.getDepositId());
		return ""+transactionDao.rejectApproverCashDeposit(cashDepositMast.getDepositId(),cashDepositMast.getRemarkApprover());
	 }
	
	
	@POST
	@Path("/approvedCashDeposit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String approvedCashDeposit(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CashDepositMast cashDepositMast){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"approvedCashDeposit()"+"|"+cashDepositMast.getDepositId());
		
		//logger.info("Start excution ************************************ method approvedCashDeposit(cashDepositMast)"+cashDepositMast.getDepositId());
		return ""+transactionDao.approvedCashDeposit(cashDepositMast.getDepositId(),cashDepositMast.getRemarkApprover(),ipIemi,agent);
	 }
	
	
	
	 @POST
	  @Path("/getRemainingLimit")
	  @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	  @Consumes(MediaType.APPLICATION_JSON)
	  public Response getRemainingLimit(RequestBean requestBean ,@Context HttpServletRequest request)
	  {
	   
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),"","", "","" ,"getRemainingLimit()");
		
	   //logger.info("***********Start Execution *******getRemainingLimit*******");
	   TransactionResponse response=new TransactionResponse(); 
	   Response resp = new Response();
	   
	   if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
	    response.setStatus("FAILED");
	    response.setStatusCode("1");
	    response.setStatusMsg("Invalid Aggreator ID."); 
	    resp.setResponse(gson.toJson(response));
	   return resp;
	  }
	   try
	   {
	   String mKey="e391ab57590132714ad32da9acf3013eb88c";
	   Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
	   JSONObject claimJson = new JSONObject(claim);
	   String walletId=claimJson.getString("walletId").toString();
	   
	   RemainingLimitBean remainingLimitBean=transactionDao.getRemainingLimit(walletId);
	   
	   response.setStatus("SUCCESS");
	   response.setStatusCode("300");
	   response.setStatusMsg("SUCCESS");
	   response.setRemainingLimitload(remainingLimitBean);
	   resp.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(response, HashMap.class), mKey));
	   return resp;
	   
	   }
	   catch(Exception ex)
	   {
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),"","", "","" ,"problem ingetRemainingLimit"+ex.getMessage()+" "+ex);
			
	    //logger.info("************problem in **********getRemainingLimit*********");
	    ex.printStackTrace();
	    response.setStatus("FAILED");
	    response.setStatusCode("1");
	    response.setStatusMsg("FAILED");
	    resp.setResponse(gson.toJson(response));
	    return resp;
	   }
	   
	  }
	 
	 @POST
	 @Path("/showCashDeposite")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String showCashDeposite(TxnInputBean txnInputBean){
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txnInputBean.getAggreatorid(),txnInputBean.getWalletId(),txnInputBean.getUserId(), "",txnInputBean.getTxnId() ,"showCashDeposite()");
		
	 // logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
	  GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(transactionDao.showCashDeposite( txnInputBean.getWalletId(),txnInputBean.getStDate(),txnInputBean.getEndDate()));
	 }
	 
 
	@POST
	@Path("/rechargePayment")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public RechargeTxnBean rechargePayment(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,String request){
		JSONObject jsonRequest = null;
		String aggregatorId = null;
		RechargeTxnBean rechargeTxnBean=new RechargeTxnBean();
		rechargeTxnBean.setStatusCode("7000");
		rechargeTxnBean.setStatus("Something went wrong. Please try after sometime.");
		String Key="pYo868VZjHXzKPq0wW7iwtcWHpZ8vjUsU5M9Jjd1LXM=";
		try{	
		jsonRequest = new JSONObject(request);
		aggregatorId = jsonRequest.get("aggreatorid").toString();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "","" ,"rechargePayment()|Request"+request);
		String decryptedData=AES128Bit.decrypt(jsonRequest.get("encryptedValue").toString(),Key);
		//JSONObject jsonObject = new JSONObject(decryptedData);
		rechargeTxnBean = new Gson().fromJson(decryptedData, RechargeTxnBean.class);
		rechargeTxnBean.setAggreatorid(aggregatorId);
		rechargeTxnBean.setIpIemi(ipiemi);
		rechargeTxnBean.setTxnAgent(agent);
		return transactionDao.rechargePayment(rechargeTxnBean);
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "","" ,"rechargePayment()|Exception message:"+ex.getMessage());
			ex.printStackTrace();
		}
		 return rechargeTxnBean;
	 }
	
	
	@POST
	@Path("/getRechargeStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public RechargeTxnBean getRechargeStatus(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,String request){
		JSONObject jsonRequest = null;
		RechargeTxnBean rechargeTxnBeanResp = new RechargeTxnBean();
		rechargeTxnBeanResp.setStatusCode("7000");
		rechargeTxnBeanResp.setStatus("Something went wrong. Please try after sometime.");
		try{	
		jsonRequest = new JSONObject(request);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),jsonRequest.getString("aggreatorid"),"","", "","" ,"rechargePayment()|Request"+request);
		RechargeTxnBean rechargeTxnBean =  transactionDao.getRechargeStatus(jsonRequest.getString("aggreatorid"),jsonRequest.getString("rechargeRespId"));
		if(rechargeTxnBean.getStatusCode() == null || (!rechargeTxnBean.getStatusCode().equals("1001") && rechargeTxnBean.getStatusCode().equals("7000")))
		{
			rechargeTxnBeanResp.setStatusCode("1000");
			rechargeTxnBeanResp.setStatus(rechargeTxnBean.getStatus());
			rechargeTxnBeanResp.setRechargeAmt(rechargeTxnBean.getRechargeAmt());
			rechargeTxnBeanResp.setRechargeRespId(rechargeTxnBean.getRechargeRespId());
			rechargeTxnBeanResp.setRechargeNumber(rechargeTxnBean.getRechargeNumber());
			rechargeTxnBeanResp.setRechargeOperator(rechargeTxnBean.getRechargeOperator());
			rechargeTxnBeanResp.setRechargeType(rechargeTxnBean.getRechargeType());
		}
		else
		{
			rechargeTxnBeanResp.setStatusCode(rechargeTxnBean.getStatusCode());
			rechargeTxnBeanResp.setStatus(rechargeTxnBean.getStatus());
		}
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"rechargePayment()|Exception message:"+ex.getMessage());
			ex.printStackTrace();
		}
		 return rechargeTxnBeanResp;
	 }
	
	 
	 @POST
	 @Path("/insertTransDtl")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String insertTransDtl(TrasactionDTL tDtl){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :insertTransDtl|Details :"+tDtl.getDetails());
			
		// logger.info("checkDMTStatus log");
	     String resp=new CommanUtilDaoImpl().insertTransDtl(tDtl.getDetails());
	     return resp;
	 }
	 
	 @POST
	 @Path("/deleteTransDtl")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String deleteTransDtl(TrasactionDTL tDtl){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :deleteTransDtl|Details :"+tDtl.getDetails());
			
		// logger.info("checkDMTStatus log");
	     String resp=new CommanUtilDaoImpl().deleteTransDtl(tDtl.getDetails());
	     return resp;
	   
	 }
	 
	 @POST
	 @Path("/getTransDtlFlag")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String getTransDtlFlag(TrasactionDTL tDtl){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :getTransDtlFlag|Details :"+tDtl.getDetails());
			
		// logger.info("checkDMTStatus log");
	     String resp=new CommanUtilDaoImpl().getTransDtlFlag(tDtl.getDetails(),tDtl.getTxnType());
	     return resp;
	   
	 }
	
	@GET
	@Path("/recharge/response")
	 public void myPayStoreResp(@QueryParam("txid") String txid,@QueryParam("accountId") String accountID,@QueryParam("Transtype") String Transtype,@Context HttpServletRequest request) 
	 {
		Commonlogger.log(Commonlogger.INFO,"","","","","","","callback from myPayStore with values : "+txid+" "+accountID+" "+Transtype);
		 String userAgent=request.getHeader("User-Agent");
		   
		   String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		   if (ipAddress == null) {
		       ipAddress = request.getRemoteAddr();
		   }
		   Commonlogger.log(Commonlogger.INFO,"","","","","","","ip hitting from myPayStore id : "+ipAddress+ " and user-agent is : "+userAgent);
		 //  if(ipAddress.equals("0:0:0:0:0:0:0:1") || ipAddress.equals("203.122.47.170") || ipAddress.equals("158.69.53.199"))
			   new TransactionDaoImpl().markFinalStatusMark(txid,accountID,Transtype);
	 }
	
	
	@POST
	@Path("/getRechargeTxnBean")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public RechargeTxnBean getRechargeTxnBean(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,RechargeTxnBean rechargeTxnBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("_____________agent_____________"+agent);
		System.out.println("____________txnInputBean_______"+rechargeTxnBean);
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "",rechargeTxnBean.getRechargeId() ,"savePGTrx()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.getRechargeTxnBean(rechargeTxnBean.getRechargeId());
		
	}
	
	@POST
	@Path("/updateRechargeTxnBean")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public RechargeTxnBean updateRechargeTxnBean(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,RechargeTxnBean rechargeTxnBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("_____________agent_____________"+agent);
		System.out.println("____________txnInputBean_______"+rechargeTxnBean);
		 
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "",rechargeTxnBean.getRechargeId() ,"savePGTrx()|"+agent);
		//logger.info("Start excution ===================== method savePGTrx(txnInputBean)");
		//logger.info("******************************agent***********************************"+agent);
		return transactionDao.updateRechargeTxnBean(rechargeTxnBean.getRechargeId(),rechargeTxnBean.getStatus());
		
	}
	
	@POST
	@Path("/subAggregatorUtilityChange")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String subAggregatorUtilityChange(String customerid){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerid,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.subAggregatorUtilityChange( customerid));
	}
	
	
	@POST
	@Path("/superDistributorUtilityChange")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String superDistributorUtilityChange(String customerid){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerid,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.superDistributorUtilityChange( customerid));
	}
	
	
	@POST
	@Path("/distributorList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String distributorList(String aggId){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.distributorList( aggId));
	}
	
	@POST
	@Path("/subAggregatorUtility")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String subAggregatorUtility(String aggId){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.subAggregatorUtility( aggId));
	}
	
	
	
	
	@POST
	@Path("/superDistributerListChange")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String superDistributerListChange(String aggId){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.superDistributerListChange( aggId));
	}
	
	
	
	
	@POST
	@Path("/utilProfileChange")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String utilProfileChange(String aggId){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.utilProfileChange( aggId));
	}
	
	@POST
	@Path("/utilProfileAgentDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String utilProfileAgentDetails(String customerid){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerid,"","","","" ,"utilProfileAgentDetails()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transactionDao.utilProfileAgentDetails( customerid));
	}
	
	@POST
	@Path("/changeDistributor")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String changeDistributor(WalletMastBean walletMastBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getDistributerid(),"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return (transactionDao.changeDistributor( walletMastBean.getId(),walletMastBean.getDistributerid()));
	}
	
	
	@POST
	@Path("/changeSuperDistributor")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String changeSuperDistributor(WalletMastBean walletMastBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getDistributerid(),"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return (transactionDao.changeSuperDistributor( walletMastBean.getId(),walletMastBean.getDistributerid()));
	}
	
	
	
	@POST
	@Path("/utilChangeAgentProfile")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String utilChangeAgentProfile(WalletMastBean walletMastBean){
		
	      
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getDistributerid(),"","","","" ,"subAggregatorUtility()");
		
		//logger.info("Start excution ===================== method profilebytxnId(txnInputBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return (transactionDao.utilChangeAgentProfile( walletMastBean.getId(),walletMastBean.getName(),walletMastBean.getMobileno(),walletMastBean.getEmailid()));
	}
	
	@POST
	@Path("/callCreditWallet")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public void callCreditWallet(@HeaderParam("IPIMEI") String imei, RechargeTxnBean rechargeTxnBean ){
		
		System.out.println("_____________Imei_____________"+imei);
		System.out.println("____________txnInputBean_______"+rechargeTxnBean);
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "",rechargeTxnBean.getRechargeId() ,"callCreditWallet|"+rechargeTxnBean.getAggreatorid());
		transactionDao.callCreditWallet(rechargeTxnBean);
	}
	
	@POST
	@Path("/updateRechargeStatus")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Consumes(MediaType.APPLICATION_JSON)
	public String updateRechargeStatus(@QueryParam("reqid")String reqid, @QueryParam("status")String status, @QueryParam("remark")String remark, @QueryParam("balance")String balance, @QueryParam("mn")String mn,@QueryParam("field1")String field1,@QueryParam("ec")String ec){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reqid,status,remark,mn,ec ,"updateRechargeStatus callback url");
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),status,remark, balance,mn,field1,"updateRechargeStatus callback url");
			
	// template fields: @reqid, @status, @remark, @balance, @mn, @field1, @ec
		transactionDao.updateRechargeStatus(reqid,status,remark,balance,mn,field1,ec);
		return "{\"status\":\"Success\"}";
	}
	
	
	@POST
	@Path("/updateRechargeStatusPrev")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Consumes(MediaType.APPLICATION_JSON)
	public String updateRechargeStatusPrev(@QueryParam("txid")String txid, @QueryParam("status")String status, @QueryParam("opid")String opid){
		
		transactionDao.updateRechargeStatusPrev(txid,status,opid);
		return "{\"tatus\":\"Success\"}";
	}
	
	
	// change by mani
	
	@POST
	@Path("/leanAccountDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String leanAccountDetails(String customerid){
		
	    System.out.println("CUSTOMERID "+customerid);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerid,"","","","" ,"leanAccountDetails()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    logger.debug("leanAccountDetails ______________"+customerid);
	    return gson.toJson(transactionDao.leanAccountDetails(customerid));
	}
	
	
	
	@POST
	@Path("/leanAccountUtility")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String leanAccountUtility(String aggId){
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"leanAccountUtility()");
		
	    System.out.println("---***********---- "+aggId+"   *****");  
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    logger.debug("leanAccountDetails ______________"+aggId);
	    return gson.toJson(transactionDao.leanAccountUtility( aggId));
	}
	
	
	@POST
	@Path("/saveLeanAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public LeanAccount saveLeanAccount(LeanAccount lean)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),lean.getUserId(),"","","","" ,"saveLeanAccount()");
		
		logger.debug("leanAccountDetails ______________"+lean.getUserId());
		return transactionDao.saveLeanAccount(lean);
	}
	
	
	@POST
	@Path("/updateLeanAmount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public LeanAccount updateLeanAmount(LeanAccount leanAccount)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),leanAccount.getUserId(),"","","","" ,"updateLeanAmount()");
		
		logger.debug("leanAccountDetails ______________"+leanAccount.getUserId());
		return transactionDao.updateLeanAmount(leanAccount);
	}
	
	
	
	@POST
	@Path("/deleteLeanRecord")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public LeanAccount deleteRecord(LeanAccount lean)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),lean.getUserId(),"","","","" ,"deleteRecord()");
		
		logger.debug("leanAccountDetails ______________"+lean.getUserId());
		return transactionDao.deleteRecord(lean.getUserId());
	}
	

	@POST
	@Path("/findLeanRecord")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public LeanAccount findLeanRecord(LeanAccount lean)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),lean.getUserId(),"","","","" ,"findLeanRecord()");
		logger.debug("leanAccountDetails ______________"+lean.getUserId());
		return transactionDao.findLeanRecord(lean.getUserId(),lean.getAggregatorId());
	}
	
	@POST
	@Path("/accountDetail")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String accountDetail(String customerid){
		
	    System.out.println("CUSTOMERID "+customerid);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerid,"","","","" ,"accountDetail()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    logger.debug("accountDetail ______________"+customerid);
	    
	 return gson.toJson(transactionDao.accountDetail(customerid));
	}
	
	
	@POST
	@Path("/addAccountSelf")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AddBankAccount addAccountSelf(AddBankAccount account)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),account.getUserId(),"","","","" ,"addAccountSelf()");
		 
		  return transactionDao.addAccountSelf(account);
	}
	
	@POST
	@Path("/getAllAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public List<AddBankAccount> getAllAccount(AddBankAccount account)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),account.getUserId(),"","","","" ,"addAccountSelf()");
		 
		
		return transactionDao.getAllAccount(account);
	}
	
	@GET
	@Path("/getPendingAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public List<AddBankAccount> getPendingAccount()
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"getPendingAccount()");
		 
		
		return transactionDao.getPendingAccount();
	}
	
	@POST
	@Path("/updateAccountSelf")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public AddBankAccount updateAccountSelf(AddBankAccount account)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),account.getUserId(),"","","","" ,"updateAccountSelf()");
		 
		return transactionDao.updateAccountSelf(account);
	}
	

	@POST
	@Path("/preCmsDebitNotification")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CmsResponse preCmsDebitNotification(CmsBean cms)
	{
		Gson gson = new Gson();
		String jsonText=gson.toJson(cms);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"preCmsDebitNotification()"+jsonText);
		 
		return transactionDao.preCmsDebitNotification(cms);
	}
	
	@POST
	@Path("/postCmsDebitNotification")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CmsResponse postCmsDebitNotification(CmsBean cms)
	{
		Gson gson = new Gson();
		String jsonText=gson.toJson(cms);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"postCmsDebitNotification() "+jsonText);
		 
		return transactionDao.postCmsDebitNotification(cms);
	}
	
	
	@POST
	@Path("/searchCmsTxn")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CmsEnquiryResponse searchCmsTxn(CmsEnquiryResponse cms)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"searchCmsTxn()"+cms.getClientTransactionId());
		  
		return transactionDao.searchCmsTxn(cms);
	}
	
	
}


class TrasactionDTL{
	private String details;
	private String txnType;
	public String getDetails() {
		return details;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	
	
}
