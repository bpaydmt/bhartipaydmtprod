package com.bhartipay.transaction.notification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.bhartipay.transaction.notification.bean.FCMDataBean;
import com.bhartipay.transaction.notification.bean.FCMGroupDetails;
import com.bhartipay.transaction.notification.persistence.TransNotificationDao;
import com.bhartipay.transaction.notification.persistence.TransNotificationDaoImpl;

public class NotificationService 
{
	private static final Logger logger = Logger.getLogger(NotificationService.class.getName());
	TransNotificationDao transDao = new TransNotificationDaoImpl();
	GCMServeice gcmService = new GCMServeice();
	
	public String createDeviceIdGroupFCM(String aggreatorid,String senderId,String operation,String groupName,List<String> deviceId)
	{
		logger.info("********Start Execution******addDeviceIdGroup");
		FCMGroupDetails fcmgroup = new FCMGroupDetails();
		String notification_key="";
		try
		{
			notification_key= gcmService.createDeviceIdGroup(senderId,operation,groupName,deviceId.toArray((new String[0])));
			if(notification_key.equalsIgnoreCase("null"))
			{
				return "700";
			}
			fcmgroup.setAggreatorId(aggreatorid);
			fcmgroup.setNotificationKey(notification_key);
			fcmgroup.setGroupName(groupName);
			fcmgroup.setSenderId(senderId);
			Iterator<String> itr = deviceId.iterator();
			while(itr.hasNext())
			{
				String device = itr.next();
				fcmgroup.setId(0);
				fcmgroup.setDeviceId(device);
				fcmgroup.setUserId(transDao.getUserIdByDeviceId(device));
				transDao.saveFCMGroupDetails(fcmgroup);
			}
			return "300";
		}
		catch(Exception e)
		{
			logger.info("*********problem in addDeviceIdGroup*********e.message"+e.getMessage());
			e.printStackTrace();
			return "700";
		}
	}
	
	public String addDeviceIdGroupFCM(String aggreatorid,String notificationKey,String senderId,String groupName,List<String> deviceId)
	{

		logger.info("********Start Execution******addDeviceIdGroupFCM");
		FCMGroupDetails fcmgroup = new FCMGroupDetails();
		String notification_key="";
		try
		{
			notification_key= gcmService.addDeviceIdGroup(senderId,notificationKey,groupName,deviceId.toArray((new String[0])));
			if(notification_key.equalsIgnoreCase("null"))
			{
				return "700";
			}
			fcmgroup.setAggreatorId(aggreatorid);
			fcmgroup.setNotificationKey(notificationKey);
			fcmgroup.setGroupName(groupName);
			fcmgroup.setSenderId(senderId);
			Iterator<String> itr = deviceId.iterator();
			while(itr.hasNext())
			{
				String device = itr.next();
				fcmgroup.setId(0);
				fcmgroup.setDeviceId(device);
				fcmgroup.setUserId(transDao.getUserIdByDeviceId(device));
				transDao.saveFCMGroupDetails(fcmgroup);
			}
			return "300";
		}
		catch(Exception e)
		{
			logger.info("*********problem in addDeviceIdGroupFCM*********e.message"+e.getMessage());
			e.printStackTrace();
			return "700";
		}
	
	}
	
	public String removeDeviceIdGroupFCM(String notificationKey,String senderId,String groupName,List<String> deviceId)
	{

		logger.info("********Start Execution******addDeviceIdGroupFCM");
		FCMGroupDetails fcmgroup = new FCMGroupDetails();
		String notification_key="";
		try
		{
			notification_key= gcmService.removeDeviceIdGroup(senderId,notificationKey,groupName,deviceId.toArray((new String[0])));
			if(notification_key.equalsIgnoreCase("null"))
			{
				return "700";
			}
			Iterator<String> itr = deviceId.iterator();
			while(itr.hasNext())
			{
				String device = itr.next();
				transDao.removeFCMGroupDetails(device,groupName);
			}
			return "300";
		}
		catch(Exception e)
		{
			logger.info("*********problem in addDeviceIdGroupFCM*********e.message"+e.getMessage());
			e.printStackTrace();
			return "700";
		}
	
	}

	public void sendMsgToGroupFCM(String aggreatorid,String notificationKey,String senderId,String groupName,FCMDataBean dataMap)
	{
		logger.info("**********Start Execution*******sendMsgToGroupFCM");
		try
		{
			List<String> failureList =gcmService.pushNotificationFCM(notificationKey,dataMap);
			Iterator<String> itr = failureList.iterator();
			List<String> addDeviceIdGroup = new ArrayList<String>();
			List<String> removeDeviceIdGroup = new ArrayList<String>();
			while(itr.hasNext())
			{
				String l_deviceId = itr.next();
				String userId = transDao.getUserIdByDeviceId(l_deviceId);
				String updatedDeviceId = transDao.getDeviceIdByUserId(userId);
				if(!updatedDeviceId.equals("l_deviceId"))
				{
					addDeviceIdGroup.add(updatedDeviceId);
					removeDeviceIdGroup.add(l_deviceId);
				}
			}
			if(addDeviceIdGroup == null || removeDeviceIdGroup == null)
			{
				addDeviceIdGroupFCM(aggreatorid,notificationKey,senderId,groupName,addDeviceIdGroup);
				removeDeviceIdGroupFCM(notificationKey, senderId, groupName, removeDeviceIdGroup);
			}
		}
		catch(Exception e)
		{
			logger.info("*******Problem in sendMsgToGroupFCM*******e.message"+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String s[])
	{
		List<String> str=new ArrayList<String>();
		FCMDataBean dataMap = new FCMDataBean();
		dataMap.setNotificationType("list");
		dataMap.setNormalTitle("close notification title");
		dataMap.setNormalBody("close noti body");
		dataMap.setRightIconUrl("icon url");
		dataMap.setRedirectUrl("redirectUrl");
		dataMap.setSharingContent("sharingTitle|sharingText");
		dataMap.setBigText("bigText|bigTextTitle|bigTextSummary");
		dataMap.setBigPicture("bigPictureUrl|bigPictureTitle|bigPictureSummary");
		dataMap.setBigList("9,8,7|bigListTitle|bigListSummary");
		str.add("fcbf0_b28HA:APA91bEmhbkYRTJd346tVg6tOuBTpx67SdjtvWdzCCqtbGTf6hoEthBl_kyqtScx5WPygpCIoOIf1-_eG70T3jVgVFBONaR92JvuhzzpqhZjXZ85Txy-sGAv75whImJm7JCmKnLKsCMj");
		str.add("dghEjS6dTqk:APA91bGBpHjDDXptBr8QaS8lMLmHzDvHEntt1riefjnXes0krruW4Ckq9ojndl8RGrcyIRK6KLS9zSdNuJpzehQvjTHfsIprpco5_F-L8YCfCUimls9UfZFpxrWbmb7HLGt2LWgX_sKp");
		//new NotificationService().removeDeviceIdGroupFCM("AGGR001035","APA91bHAQdPJ4LEkb3Weic__fuUSA-q5GWoznPNQ_OBGKFSlREzXi8j-iuQRMHUNeZPygXDfYvpBxaINI65yee851o8DfKDvRy5IyG9P41F_T6SBnD6tXak","462856195609","remove","addedGroup10",str);
		//new NotificationService().createDeviceIdGroupFCM("AGGR001035","462856195609","create","addedGroup12",str);
		//new NotificationService().addDeviceIdGroupFCM("AGGR001035","APA91bElj0XnfGZuZaWANjUWKgIyZNAl1Xf9IaXj_1Z9PL1JNT_EX76iSb3EoPp062SXSD79PfQPBggbUirckzbDGy8p0eC3yInnoGVbE51czknpCgodKTk","462856195609","addedGroup11",str);
		new NotificationService().sendMsgToGroupFCM("AGGR001035","APA91bENrIZ0rEbO7FymVpjJRu4_FVtnZwOrErWwNS3GEyUQgCb_2bL3sfLaLaO3QmHuuuKZmh5T5ZIWen7Fo462JHLWLyeVDg8hnFMYo7Lo8dU3FNbrKog","462856195609","addedGroup12",dataMap);
	}
}
