package com.bhartipay.transaction.notification.persistence;

import java.util.List;

import com.bhartipay.transaction.notification.bean.FCMGroupDetails;
import com.bhartipay.transaction.notification.bean.GCMDeviceBean;
import com.bhartipay.transaction.notification.bean.GCMHistoryMsgBean;

public interface TransNotificationDao {
	//public boolean setDeviceID(String mobileno, String deviceId);
	public boolean setDeviceID(GCMDeviceBean gCMDeviceBean);
	public boolean removeDeviceId(String mobileno);
	public String getGCMAppId(String aggreatorid);
	//public List<GCMHistoryMsgBean> getPendingNotification(String mobileno);
	public List<GCMHistoryMsgBean> getPendingNotification(String userid,String aggreatorid);
	
	public int saveFCMGroupDetails(FCMGroupDetails fcmgroupdetails);
	public int removeFCMGroupDetails(String deviceId,String groupName);
	public String getUserIdByDeviceId(String deviceId);
	public String getDeviceIdByUserId(String userId);
	public List<String> getPromotionalMsg(String AggId , String walletId);
}
