package com.bhartipay.transaction.notification.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.transaction.notification.GCMServeice;
import com.bhartipay.transaction.notification.bean.FCMGroupDetails;
import com.bhartipay.transaction.notification.bean.GCMAppIdBean;
import com.bhartipay.transaction.notification.bean.GCMDeviceBean;
import com.bhartipay.transaction.notification.bean.GCMHistoryMsgBean;
import com.bhartipay.transaction.notification.bean.GCMPendingMsgBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.bean.WalletConfiguration;


public class TransNotificationDaoImpl implements TransNotificationDao{

	private static final Logger logger = Logger.getLogger(TransNotificationDaoImpl.class.getName());
	SessionFactory factory;
	
	@Override
	public boolean setDeviceID(GCMDeviceBean gCMDeviceBean){
		logger.info("Start excution ===================== method setDeviceID(walletId, deviceId)");
		logger.info("deviceid======================================= "+gCMDeviceBean.getDeviceId());
		boolean flag=false;
		factory = DBUtil.getSessionFactory();
		Transaction transaction = null;
		Session session = factory.openSession();
		//GCMDeviceBean gCMDeviceBean=new GCMDeviceBean();
		try {
			if(gCMDeviceBean.getDeviceId() == null || gCMDeviceBean.getDeviceId().isEmpty())
			{
				return false;
			}
			if(gCMDeviceBean.getUserId() == null || gCMDeviceBean.getUserId().isEmpty())
			{
				return false;
			}
			transaction=session.beginTransaction();	
			session.saveOrUpdate(gCMDeviceBean);
		   Query query = session.createQuery(" FROM GCMPendingMsgBean WHERE receivermobile=:mobile and aggreatorid=:aggreatorid");
			query.setString("mobile", gCMDeviceBean.getMobileno());
			query.setString("aggreatorid", gCMDeviceBean.getAggreatorid());
			List<GCMPendingMsgBean> list = query.list();
			if (list != null && list.size() != 0) {
				ListIterator<GCMPendingMsgBean> itr=list.listIterator();
				while(itr.hasNext()){ 
					GCMPendingMsgBean gCMPendingMsgBean=itr.next();
					logger.info("deviceId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMDeviceBean.getDeviceId());
					logger.info("msg~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getMsg());
					logger.info("aggId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getAggreatorid());
					logger.info("msg_type~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getMsgtype());
					logger.info("sender_mobile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getSendermobile());
					logger.info("trxId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getTxnid());
					logger.info("reciver_mobile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMDeviceBean.getMobileno());
					logger.info("msgid~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gCMPendingMsgBean.getTxnid());
					
					//GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), gCMPendingMsgBean.getMsg(),gCMPendingMsgBean.getMsgtype(),gCMPendingMsgBean.getSendermobile(),gCMPendingMsgBean.getTxnid(),gCMDeviceBean.getMobileno(),gCMPendingMsgBean.getTxnid());
				 }
				/*Query queryDe = session.createQuery(" Delete FROM GCMPendingMsgBean WHERE receivermobile=:mobile and aggreatorid=:aggreatorid");
				queryDe.setString("mobile", gCMDeviceBean.getMobileno());
				queryDe.setString("aggreatorid", gCMDeviceBean.getAggreatorid());
				queryDe.executeUpdate();*/
			}
			transaction.commit();	
			flag=true;
		} catch (Exception e) {
			logger.debug("problem in setDeviceID==============" + e.getMessage(), e);
			flag=false;
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		logger.info("Stop excution ===================== method setDeviceID with return==="+flag);
		return flag;	
	}
	
	
	@Override
	public boolean removeDeviceId(String userId) {
		logger.info("Start excution ===================== method removeDeviceId(walletId)");
		logger.info("mobileno======================================= "+userId);
		boolean flag=false;
		factory = DBUtil.getSessionFactory();
		Transaction transaction = null;
		Session session = factory.openSession();
		GCMDeviceBean gCMDeviceBean=new GCMDeviceBean();
		try {
			transaction=session.beginTransaction();
			gCMDeviceBean.setUserId(userId);
			session.delete(gCMDeviceBean);
			transaction.commit();
			flag=true;
		} catch (Exception e) {
			logger.debug("problem in removeDeviceId==============" + e.getMessage(), e);
			flag=false;
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		
		return flag;
		
	}
	
	
	
	
	
	
	
	
	
	@Override
	public List<GCMHistoryMsgBean> getPendingNotification(String userid,String aggreatorid) {
		logger.info("Start excution ===================== method getPendingNotification(userid)");
		logger.info("mobileno======================================= "+userid);
		logger.info("aggreatorid======================================= "+aggreatorid);
		List<GCMHistoryMsgBean> list=new ArrayList<GCMHistoryMsgBean>();
		GCMHistoryMsgBean gCMHistoryMsgBean=new GCMHistoryMsgBean();
		factory = DBUtil.getSessionFactory();
		Transaction transaction = null;
		Session session = factory.openSession();
		try {
			transaction=session.beginTransaction();
			Query query = session.createQuery(" FROM GCMHistoryMsgBean WHERE receiveruserid=:mobile and Status='0' and aggreatorid=:aggreatorid");
			query.setString("mobile", userid);
			query.setString("aggreatorid", aggreatorid);
			list = query.list();
			} catch (Exception e) {
			logger.debug("problem in getPendingNotification==============" + e.getMessage(), e);
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
			return list;
	}


	@Override
	public String getGCMAppId(String aggreatorid) {
		logger.info("Start excution ===================== method getGCMAppId(aggreatorid)");
		logger.info("mobileno======================================= "+aggreatorid);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String gcmAppId="";
		Transaction  transaction=session.beginTransaction();
		try {
			WalletConfiguration walletConfiguration=(WalletConfiguration)session.get(WalletConfiguration.class, aggreatorid);
			return walletConfiguration.getGcmAppKey();
		} catch (Exception e) {
			logger.debug("problem in getGCMAppId==============" + e.getMessage(), e);
			e.printStackTrace();
						
		} finally {
			session.close();
		}
		
		return gcmAppId;
	}
	
	
	public int saveFCMGroupDetails(FCMGroupDetails fcmgroupdetails)
	{
		logger.info("*************start Execution*****saveFCMGroupDetails");
		int id=0;
		Session session=null;
		Transaction trans=null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			trans = session.beginTransaction();
			if(fcmgroupdetails.getId() <= 0)
			{
				session.save(fcmgroupdetails);
			}
			else
			{
				session.update(fcmgroupdetails);
			}
			trans.commit();
		}
		catch(Exception e)
		{
			logger.info("*******problem in saveFCMGroupDetails*****e.message"+e.getMessage());
			e.printStackTrace();
			trans.rollback();
		}
		finally
		{
			session.close();
		}
		return id;
	}

	public int removeFCMGroupDetails(String deviceId,String groupName)
	{

		logger.info("*************start Execution*****removeFCMGroupDetails");
		int count=0;
		Session session=null;
		Transaction trans=null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			trans = session.beginTransaction();
			SQLQuery query = session.createSQLQuery("delete from fcmgroupdetails where device_id='"+deviceId+"' and group_name='"+groupName+"'");
			count=query.executeUpdate();
			trans.commit();
		}
		catch(Exception e)
		{
			logger.info("*******problem in removeFCMGroupDetails*****e.message"+e.getMessage());
			e.printStackTrace();
			trans.rollback();
		}
		finally
		{
			session.close();
		}
		return count;
	}
	
public  String getUserIdByDeviceId(String deviceId)
	{
		logger.info("**************start execution getUserIdByDeviceId******");
		String userId=null;
		Session session=null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			SQLQuery query = session.createSQLQuery("select userid From walletdevicemappingmast where deviceid='"+deviceId+"'");	
			userId = (String)query.uniqueResult();
		}
		catch(Exception e)
		{
			logger.info("****problem in getUserIdByDeviceId******e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return userId;
	}
	
	public String getDeviceIdByUserId(String userId)
	{

		logger.info("**************start execution getUserIdByDeviceId******");
		String deviceId=null;
		Session session=null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			SQLQuery query = session.createSQLQuery("select userid From walletdevicemappingmast where userid='"+userId+"'");	
			deviceId = (String)query.uniqueResult();
		}
		catch(Exception e)
		{
			logger.info("****problem in getUserIdByDeviceId******e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return deviceId;
	
	}
	
	public List<String> getPromotionalMsg(String aggId , String walletId)
	{
		logger.info("*************Start Execution********getNotificationMsg");
		List<String> notificationMsg = new ArrayList<String>();
		Session session=null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			SQLQuery qry = session.createSQLQuery("select promsg from promotionalnotification where aggreatorid='"+aggId+"' and walletid='"+walletId+"'");
			notificationMsg = (List<String>)qry.list();
		}
		catch(Exception e)
		{
			logger.info("*******problem in getNotificationMsg******e.message**"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return notificationMsg;
	}
	
	public static void main(String s[])
	{
		String userid=new TransNotificationDaoImpl().getUserIdByDeviceId("fr66yPMpavw:APA91bEWW2rO_r6lE2wsTfG3DzyfSBRkkgSa1faA34Ea8P5FvjainBfa4BrED1bGdgUdGxSaPD1_2tXsVUPkjjyNbawn1BQ3EoxUzJJOtPWp54p-AhMb9Ucp5x9SC0Gek29FclEvQCUX");
		System.out.println(userid);
	}
}




