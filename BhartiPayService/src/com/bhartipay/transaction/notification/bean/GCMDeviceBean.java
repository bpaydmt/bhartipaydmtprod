package com.bhartipay.transaction.notification.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="walletdevicemappingmast")

public class GCMDeviceBean {
	
	@Id
	@Column(name="userid",nullable=false)
	private String userId;
	
	
	@Column(name="mobileno")
	private String mobileno;
	
	@Column(name = "aggreatorid", nullable = true, length = 100)
	private String aggreatorid;
	
	
	@Column(name="deviceid")
	 private String deviceId;




	
	

	public String getAggreatorid() {
		return aggreatorid;
	}


	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getMobileno() {
		return mobileno;
	}


	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}


	public String getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
	
	
	

}
