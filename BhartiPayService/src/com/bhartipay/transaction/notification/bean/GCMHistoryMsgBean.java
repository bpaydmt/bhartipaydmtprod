package com.bhartipay.transaction.notification.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gcmhistorymsgmast")

public class GCMHistoryMsgBean {
	
	
	@Id
	@Column(name = "gcmid")
	private String gcmid;
	
	@Column(name = "receiveruserid")
	private String receiverUserId;
	
	@Column(name = "receivermobile")
	private String receivermobile;
	
	@Column(name = "aggreatorid", nullable = true, length = 100)
	private String aggreatorid;
	
	@Column(name = "message")
	private String msg;
	
	@Column(name = "messagetype")
	private String msgtype;
	
	@Column(name = "sendermobile")
	private String sendermobile;
	
	@Column(name = "txnid")
	private String txnid;
	
	@Column(name = "status")
	private String status;
	
	
	
	

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getReceiverUserId() {
		return receiverUserId;
	}

	public void setReceiverUserId(String receiverUserId) {
		this.receiverUserId = receiverUserId;
	}

	public String getGcmid() {
		return gcmid;
	}

	public void setGcmid(String gcmid) {
		this.gcmid = gcmid;
	}

	public String getReceivermobile() {
		return receivermobile;
	}

	public void setReceivermobile(String receivermobile) {
		this.receivermobile = receivermobile;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getSendermobile() {
		return sendermobile;
	}

	public void setSendermobile(String sendermobile) {
		this.sendermobile = sendermobile;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
	
	

}
