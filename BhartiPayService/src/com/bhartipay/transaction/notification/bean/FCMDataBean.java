package com.bhartipay.transaction.notification.bean;

public class FCMDataBean 
{
	private String notificationType;
	private String normalTitle;
	private String normalBody;
	private String rightIconUrl;
	private String redirectUrl;
	private String sharingContent;
	private String bigText;
	private String bigPicture;
	private String bigList;
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getNormalTitle() {
		return normalTitle;
	}
	public void setNormalTitle(String normalTitle) {
		this.normalTitle = normalTitle;
	}
	public String getNormalBody() {
		return normalBody;
	}
	public void setNormalBody(String normalBody) {
		this.normalBody = normalBody;
	}
	public String getRightIconUrl() {
		return rightIconUrl;
	}
	public void setRightIconUrl(String rightIconUrl) {
		this.rightIconUrl = rightIconUrl;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getSharingContent() {
		return sharingContent;
	}
	public void setSharingContent(String sharingContent) {
		this.sharingContent = sharingContent;
	}
	public String getBigText() {
		return bigText;
	}
	public void setBigText(String bigText) {
		this.bigText = bigText;
	}
	public String getBigPicture() {
		return bigPicture;
	}
	public void setBigPicture(String bigPicture) {
		this.bigPicture = bigPicture;
	}
	public String getBigList() {
		return bigList;
	}
	public void setBigList(String bigList) {
		this.bigList = bigList;
	}
	
	
}
