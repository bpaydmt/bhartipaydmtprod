package com.bhartipay.transaction.notification.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="walletgcmappmappingmast")

public class GCMAppIdBean {
	
	
	@Id
	@Column(name="aggreatorid",nullable=false)
	private String aggreatorid;
	
	
	@Column(name="gcmappid")
	private String gcmAppId;


	public String getAggreatorid() {
		return aggreatorid;
	}


	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}


	public String getGcmAppId() {
		return gcmAppId;
	}


	public void setGcmAppId(String gcmAppId) {
		this.gcmAppId = gcmAppId;
	}
	
	
	
	
	

}
