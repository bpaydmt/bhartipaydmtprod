package com.bhartipay.transaction.notification.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="wtwnotificationmast")	
public class WTWNotificationBean {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "noticid")
	private long noticid;
	
	@Column(name="aggreatorid")
	private String aggreatorid;
	
	@Column(name="noticwalletid")
	private String noticwalletid;
	
	@Column(name="givenwalletid")
	private String givenwalletid;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="recivertxnid")
	private String recivertxnid;
	
	@Column(name="sendertxnid")
	private String sendertxnid;
	
	
	
	

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public long getNoticid() {
		return noticid;
	}

	public void setNoticid(long noticid) {
		this.noticid = noticid;
	}

	public String getNoticwalletid() {
		return noticwalletid;
	}

	public void setNoticwalletid(String noticwalletid) {
		this.noticwalletid = noticwalletid;
	}

	public String getGivenwalletid() {
		return givenwalletid;
	}

	public void setGivenwalletid(String givenwalletid) {
		this.givenwalletid = givenwalletid;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRecivertxnid() {
		return recivertxnid;
	}

	public void setRecivertxnid(String recivertxnid) {
		this.recivertxnid = recivertxnid;
	}

	public String getSendertxnid() {
		return sendertxnid;
	}

	public void setSendertxnid(String sendertxnid) {
		this.sendertxnid = sendertxnid;
	}
	
	
	
	
	

	
	

}
