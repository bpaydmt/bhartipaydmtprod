package com.bhartipay.transaction.notification;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.httpclient.HttpConnection;
import org.apache.commons.httpclient.HttpsURL;
import org.apache.commons.httpclient.util.HttpURLConnection;
import org.apache.log4j.Logger;
import org.apache.log4j.chainsaw.Main;
import org.json.JSONException;
import org.json.JSONObject;

import com.bhartipay.transaction.notification.bean.FCMDataBean;
import com.bhartipay.transaction.notification.persistence.TransNotificationDao;
import com.bhartipay.transaction.notification.persistence.TransNotificationDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.google.gson.JsonObject;



public class GCMServeice {
	//	static Sender sender = new Sender("AIzaSyCt2kenuc_hZB0wEpApVbhTxGvYrH5WQok");
	//	static Sender sender = new Sender("AIzaSyDDaWu6IRLLY0bcxeJcWgso1MXS2sxwT5U");
	//static Sender sender = new Sender("AIzaSyClcSjo9GKdwX3DW20qPgv8xJXLZ-o_7uU");
	
	private static final Logger logger = Logger.getLogger(GCMServeice.class.getName());
	
	private static final String FCM_URL="https://fcm.googleapis.com/fcm/send";
	
	private static final String FCM_ADDGROUP_URL="https://android.googleapis.com/gcm/notification";
	
	private static final String FCM_SERVER_API_KEY="AAAAa8RhDhk:APA91bEFP2uMchHFLkHJ6gD63VilPtlTfzOqYgCaa958W0rzgagJ6-qA7gbiJxfqnD5_JBtfyx_slehiptp8KzRYTfeJI1P87GpCXHSI0uQS_if4W5FE8FVxwTQIEwRfT6PA9txGyNpc";
	
	public static void pushNotification(String deviceId, String msg,String msg_type,String sender_mobile,String trxId,String receiver_mobile,String msgid,String aggreatorid) {
		
		try {
			
			TransNotificationDao transNotificationDao=new TransNotificationDaoImpl();
			String gcmId=transNotificationDao.getGCMAppId(aggreatorid);
			logger.info("gcmId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+gcmId);
			Sender sender = new Sender(gcmId);
			logger.info("deviceId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+deviceId);
			logger.info("msg~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+msg);
			logger.info("msg_type~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+msg_type);
			logger.info("sender_mobile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+sender_mobile);
			logger.info("trxId~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+trxId);
			logger.info("reciver_mobile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+receiver_mobile);
			logger.info("msgid~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+msgid);
			
			Message message = new Message.Builder()/*.collapseKey("message").timeToLive(3)*/.delayWhileIdle(true)
					.addData("message", msg) 
					.addData("msg_type",msg_type)
					.addData("sender_mobile", sender_mobile)// you can get this message on  client side app
					.addData("trxId", trxId)
					.addData("receiver_mobile", receiver_mobile)
					.addData("msgid", msgid)
					.build();

			// Use this code to send notification message to a single device
			Result result = sender.send(message, deviceId, 1);
			logger.info("Message Result: " + result.toString()); // Print message result on console
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static List<String> pushNotificationFCM(String deviceId,FCMDataBean dataMap)
	{
		int responseCode=-1;
		List<String> failureList = new ArrayList<String>();
		//	map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
		String responseString=null;
		JSONObject jObject =null;
		try
		{
			URL url=new URL(FCM_URL);
			byte[] postDate = getPostData(deviceId,dataMap);
			 HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "key="+FCM_SERVER_API_KEY);
			OutputStream out = conn.getOutputStream();
			out.write(postDate);
			out.close();
			
			responseCode = conn.getResponseCode();
			System.out.println("ResponseCode : "+responseCode);
			if(responseCode == 200)
			{
				responseString = convertStreamToString(conn.getInputStream());
                logger.info("FCM message sent : " + responseString);
                jObject = new JSONObject(responseString);
                if(jObject.getInt("failure") != 0)
                	failureList= Arrays.asList(jObject.getNames("failed_registration_ids"));
			}
            //failure
            else
            {
            	responseString = convertStreamToString(conn.getErrorStream());
                logger.info("Sending FCM request failed for regId: " + deviceId + " response: " + responseString);
                jObject = new JSONObject(responseString);
                if(jObject.getInt("failure") != 0)
                	failureList= Arrays.asList(jObject.getNames("failed_registration_ids"));
            }
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			 logger.info("********Problem in pushNotificationFCM *********e.message"+ex.getMessage());
		}
		return failureList;
	}
	
	public static byte[] getPostData(String registrationId,FCMDataBean dataMap) throws JSONException {
        //HashMap<String, String> dataMap = new HashMap<>();
        JSONObject payloadObject = new JSONObject();
        //FCMDataBean dataMap = new FCMDataBean();
        
        /*dataMap.put("name", "Aniket!");
        dataMap.put("country", "India");
        dataMap.put("image","https://ibin.co/2t1lLdpfS06F.png");
        dataMap.put("message","Firebase Push Message Using API");
        dataMap.put("AnotherActivity","True");*/
       
        JSONObject data = new JSONObject(dataMap);;
        payloadObject.put("data", data);
        payloadObject.put("to", registrationId);
 
        return payloadObject.toString().getBytes();
    }
	
	public static String convertStreamToString (InputStream inStream) throws Exception
    {
        InputStreamReader inputStream = new InputStreamReader(inStream);
        BufferedReader bReader = new BufferedReader(inputStream);
 
        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = bReader.readLine()) != null)
        {
            sb.append(line);
        }
 
        return sb.toString();
    }

	public static String createDeviceIdGroup(String senderId,String operation,String groupName,String[] deviceId)
	{
		String notification_key="";
		try
		{
			URL url = new URL(FCM_ADDGROUP_URL);
			HashMap<String,Object> data=new HashMap<String,Object>();
			data.put("operation",operation);
			data.put("notification_key_name", groupName);
			data.put("registration_ids", deviceId);
			String strData= new ObjectMapper().writeValueAsString(data);
			logger.info("-------Request String--"+strData);
			HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization","key="+FCM_SERVER_API_KEY);
			conn.setRequestProperty("project_id", senderId/*"462856195609"*/);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(strData);
			wr.close();
			int responseCode=conn.getResponseCode();
			String response=null;
			if(responseCode == 200)
			{
				response = convertStreamToString(conn.getInputStream());
                logger.info("FCM message sent : " + response);
                notification_key=response.substring(21, response.length()-2);
			}
            else
            {
            	response = convertStreamToString(conn.getErrorStream());
                logger.info("Sending FCM request failed for regId: " + deviceId + " response: " + response);
                notification_key="null";
            }
		}
		catch(Exception e)
		{
			logger.info("*********problem in addDeviceIdInGroup********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return notification_key;
	}
	
	public static String addDeviceIdGroup(String senderId,String notificationKey,String groupName,String[] deviceId)
	{
		String notification_key="";
		try
		{
			URL url = new URL(FCM_ADDGROUP_URL);
			HashMap<String,Object> data=new HashMap<String,Object>();
			data.put("operation","add");
			data.put("notification_key_name", groupName);
			data.put("notification_key", notificationKey);
			data.put("registration_ids", deviceId);
			String strData= new ObjectMapper().writeValueAsString(data);
			logger.info("-------Request String--"+strData);
			HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization","key="+FCM_SERVER_API_KEY);
			conn.setRequestProperty("project_id", senderId/*"462856195609"*/);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(strData);
			wr.close();
			int responseCode=conn.getResponseCode();
			String response=null;
			if(responseCode == 200)
			{
				response = convertStreamToString(conn.getInputStream());
                logger.info("FCM message sent : " + response);
                notification_key=response.substring(21, response.length()-2);
			}
            else
            {
            	response = convertStreamToString(conn.getErrorStream());
                logger.info("Sending FCM request failed for regId: " + deviceId + " response: " + response);
                notification_key="null";
            }
		}
		catch(Exception e)
		{
			logger.info("*********problem in addDeviceIdGroup********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return notification_key;
	}
	
	public static String removeDeviceIdGroup(String senderId,String notificationKey,String groupName,String[] deviceId)
	{
		logger.info("***********Start Execution removeDeviceIdGroup***************");
		String notification_key="";
		try
		{
			URL url = new URL(FCM_ADDGROUP_URL);
			HashMap<String,Object> data=new HashMap<String,Object>();
			data.put("operation","remove");
			data.put("notification_key_name", groupName);
			data.put("notification_key", notificationKey);
			data.put("registration_ids", deviceId);
			String strData= new ObjectMapper().writeValueAsString(data);
			logger.info("-------Request String--"+strData);
			HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization","key="+FCM_SERVER_API_KEY);
			conn.setRequestProperty("project_id", senderId/*"462856195609"*/);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(strData);
			wr.close();
			int responseCode=conn.getResponseCode();
			String response=null;
			if(responseCode == 200)
			{
				response = convertStreamToString(conn.getInputStream());
                logger.info("FCM message sent : " + response);
                notification_key=response.substring(21, response.length()-2);
			}
            else
            {
            	response = convertStreamToString(conn.getErrorStream());
                logger.info("Sending FCM request failed for regId: " + deviceId + " response: " + response);
                notification_key="null";
            }
		}
		catch(Exception e)
		{
			logger.info("*****problem in removeDeviceIdGroup*******e.message"+e.getMessage());
			e.printStackTrace();
		}
		return notification_key;
	}
	/*	public static void pushBulckNotification(ArrayList<String>devicesList,String msg){
		
		try {
			Message message = new Message.Builder().collapseKey("message").timeToLive(3).delayWhileIdle(true)
					.addData("message", msg) // you can get this message on  client side app
					.build();
			
			 //Use this code for multicast messages   
            MulticastResult multicastResult = sender.send(message, devicesList, 0);
            logger.info("Message Result: "+multicastResult.toString());//Print multicast message result on console
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}*/

public static void main(String[] args) {
	FCMDataBean dataMap = new FCMDataBean();
	dataMap.setNotificationType("picture,list,more,none");
	dataMap.setNormalTitle("close notification title");
	dataMap.setNormalBody("close noti body");
	dataMap.setRightIconUrl("icon url");
	dataMap.setRedirectUrl("redirectUrl");
	dataMap.setSharingContent("sharingTitle|sharingText");
	dataMap.setBigText("bigText|bigTextTitle|bigTextSummary");
	dataMap.setBigPicture("bigPictureUrl|bigPictureTitle|bigPictureSummary");
	dataMap.setBigList("1,2,3|bigListTitle|bigListSummary");
	String[] str={"dlExbCUySVc:APA91bHosVB6NW7zHjLBJPSAgidsb6HCMFoo2MaJ0VkhZGMyMkKydmuvDlK2UOguemphDOqMoeOlP5vS3PzXD4LEcqg2kVLKoIEWrnx7aRZnr1zMjK-UehGpFCxftYQClxfvZkwQLm29","dghEjS6dTqk:APA91bGBpHjDDXptBr8QaS8lMLmHzDvHEntt1riefjnXes0krruW4Ckq9ojndl8RGrcyIRK6KLS9zSdNuJpzehQvjTHfsIprpco5_F-L8YCfCUimls9UfZFpxrWbmb7HLGt2LWgX_sKp"};
	
	//addDeviceIdGroup("462856195609","APA91bHAQdPJ4LEkb3Weic__fuUSA-q5GWoznPNQ_OBGKFSlREzXi8j-iuQRMHUNeZPygXDfYvpBxaINI65yee851o8DfKDvRy5IyG9P41F_T6SBnD6tXak","add","addedGroup4",str);
	List<String> val = pushNotificationFCM("APA91bElj0XnfGZuZaWANjUWKgIyZNAl1Xf9IaXj_1Z9PL1JNT_EX76iSb3EoPp062SXSD79PfQPBggbUirckzbDGy8p0eC3yInnoGVbE51czknpCgodKTk",dataMap);
	System.out.println(val);
	//pushNotification("ej2BG_JQ5J0:APA91bEqhwRZ5dCwZm7U14Nbq-pX2b6WYytZ9TqfvCfKtrKww_L0M3ikAZoo8JIwsSfC4fnU1BP1v8D_zwCGcFcYsI4FzJp3XOGywQxjFt6ef87iAfhUeQYSY8rL93Ht2mR43Z6eddbh", "ABC","Request","9910311643","ABCR123","9935041287","111212");
	
}
}
