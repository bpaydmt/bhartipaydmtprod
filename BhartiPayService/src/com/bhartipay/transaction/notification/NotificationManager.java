package com.bhartipay.transaction.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.transaction.bean.PromotionalNotificationBean;
import com.bhartipay.transaction.notification.bean.GCMDeviceBean;
import com.bhartipay.transaction.notification.bean.GCMHistoryMsgBean;
import com.bhartipay.transaction.notification.persistence.TransNotificationDao;
import com.bhartipay.transaction.notification.persistence.TransNotificationDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/NotificationManager")
public class NotificationManager {
	private static final Logger logger = Logger.getLogger(NotificationManager.class.getName());
	TransNotificationDao transNotificationDao=new TransNotificationDaoImpl();
	
	@POST
	@Path("/setDeviceID")
	@Produces({  MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String setDeviceID(GCMDeviceBean gCMDeviceBean){
		logger.info("deviceid=====================TransNotificationDao================== "+gCMDeviceBean.getDeviceId());
		return ""+transNotificationDao.setDeviceID(gCMDeviceBean);
		
	}
	
	
	@POST
	@Path("/getPendingNotification")
	@Produces({MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getPendingNotification(GCMDeviceBean gCMDeviceBean){
		
		logger.info("deviceid=====================TransNotificationDao================== getPendingNotification(userid)");
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(transNotificationDao.getPendingNotification(gCMDeviceBean.getUserId(),gCMDeviceBean.getAggreatorid()));	
	}
	
	@POST
	@Path("/getPromotionalMsg")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public String getPromotionalMsg(PromotionalNotificationBean prmtnlNotification)
	{
		logger.info("==================TransNotificationDao================== getPromotionalMsg(prmtnlNotification)");
		GsonBuilder builder = new GsonBuilder();
		HashMap<String,Object> response = new HashMap<String,Object>();
		List<String> promotionalMsg=new ArrayList<String>();
		try
		{
			Gson gson =builder.create();
			promotionalMsg = transNotificationDao.getPromotionalMsg(prmtnlNotification.getAggreatorId(), prmtnlNotification.getWalletId());
			if(promotionalMsg == null || promotionalMsg.size() < 1)
			{
				response.put("status", "Failed");
				response.put("statusCode", "301");
				response.put("statusMsg", "No Promotional Message found for given user.");
			}
			else
			{
				response.put("status", "Success");
				response.put("statusCode", "300");
				response.put("statusMsg", "Success.");
				response.put("promotionalMsg", promotionalMsg);
			}	
			return  new ObjectMapper().writeValueAsString(response); 
		}
		catch(Exception e)
		{
			logger.info("===============problem in getPromotionalMsg========e.message"+e.getMessage() );
			e.printStackTrace();
		}
		return new Gson().toJson(response);
	}
	
	public static void main(String s[])
	{
		PromotionalNotificationBean prmtnlNotification = new PromotionalNotificationBean();
		prmtnlNotification.setAggreatorId("AGGR001035");
		prmtnlNotification.setWalletId("1234");
		String json = new NotificationManager().getPromotionalMsg(prmtnlNotification);
	}

}
