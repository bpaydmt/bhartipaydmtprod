package com.bhartipay.transaction.persistence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONObject;
import org.json.XML;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.airTravel.bean.AirFareBean;
import com.bhartipay.airTravel.bean.FlightTxnBean;
import com.bhartipay.airTravel.bean.booking.Response_;
import com.bhartipay.airTravel.persistence.AirTravelDaoImpl;
import com.bhartipay.bbps.service.BbpsBillerService;
import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.CmsBean;
import com.bhartipay.lean.bean.CmsEnquiryResponse;
import com.bhartipay.lean.bean.CmsResponse;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.lean.bean.PgReturnResponse;
import com.bhartipay.lean.bean.RechargeBillRequest;
import com.bhartipay.payword.bean.PayworldResponseData;
import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;
import com.bhartipay.recharge.response.ExtBillPayResponse;
import com.bhartipay.recharge.response.Result;
import com.bhartipay.rechargeService.bean.RechargeOperatorMasterBean;
import com.bhartipay.rechargeService.dao.RechargeDao;
import com.bhartipay.rechargeService.dao.RechargeDaoImpl;
import com.bhartipay.transaction.bean.AppResponse;
import com.bhartipay.transaction.bean.AskMoneyBean;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.CashBackLedgerBean;
import com.bhartipay.transaction.bean.CashBackPassbookBean;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.bean.DisputeBean;
import com.bhartipay.transaction.bean.MOCompanyInformationBean;
import com.bhartipay.transaction.bean.MODocumentsInformationBean;
import com.bhartipay.transaction.bean.MODocumentsInformationURLBean;
import com.bhartipay.transaction.bean.MOPersoanInformationBean;
import com.bhartipay.transaction.bean.MORiskInformationBean;
import com.bhartipay.transaction.bean.MORiskInformationTransLimitBean;
import com.bhartipay.transaction.bean.MOSalesInformationBean;
import com.bhartipay.transaction.bean.MerchantOnboardingCallBackBean;
import com.bhartipay.transaction.bean.MoneyRequestBean;
import com.bhartipay.transaction.bean.P2MTransactionBean;
import com.bhartipay.transaction.bean.PGAggregatorBean;
import com.bhartipay.transaction.bean.PGPayeeBean;
import com.bhartipay.transaction.bean.PassbookBean;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.PrePaidCardResponse;
import com.bhartipay.transaction.bean.PrePaidCardTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.RefundMastBean;
import com.bhartipay.transaction.bean.RefundTransactionBean;
import com.bhartipay.transaction.bean.RemainingLimitBean;
import com.bhartipay.transaction.bean.SMSCallBackBean;
import com.bhartipay.transaction.bean.SurchargeBean;
import com.bhartipay.transaction.bean.TerminalOnBoardingCallBackBean;
import com.bhartipay.transaction.bean.TerminalStatusCallBackBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.bean.TransactionNotificationCallBackBean;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.bean.WalletToBankTxnMast;
import com.bhartipay.transaction.notification.GCMServeice;
import com.bhartipay.transaction.notification.bean.GCMDeviceBean;
import com.bhartipay.transaction.notification.bean.GCMHistoryMsgBean;
import com.bhartipay.transaction.notification.bean.GCMPendingMsgBean;
import com.bhartipay.transaction.notification.bean.WTWNotificationBean;
import com.bhartipay.user.bean.CashBackBalanceBean;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.UserWalletConfigBean;
import com.bhartipay.user.bean.WalletBalanceBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.bean.WalletSecurityBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.JSONUtil;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.ResponseResult;
import com.bhartipay.util.ResponseResult.ResponseStatus;
import com.bhartipay.util.ResponseResult.StatusCode;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.EzyPayIntegration;
import com.bhartipay.util.thirdParty.IMPSImplementation;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.wallet.aeps.AEPSConfig;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.matm.MATMLedger;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;



public class TransactionDaoImpl implements TransactionDao{
	 
	
	private static final Logger logger = Logger.getLogger(TransactionDaoImpl.class.getName());
	SessionFactory factory;
	public static String globalAggregator ="OAGG001050";//production
//	public static String globalAggregator ="AGGR001035"; //local
	Transaction transaction = null;
	Session session = null;
	private HttpServletRequest request;
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
	WalletUserDao walletUserDao=new WalletUserDaoImpl();
	WalletConfiguration walletConfiguration = null;
	WalletMastBean walletMastBean=null;
	OTPGeneration oTPGeneration = new OTPGeneration();
	
	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private java.sql.Date parseDate(String date) {
	    try {
	        return (java.sql.Date) new java.sql.Date(DATE_FORMAT.parse(date).getTime());
	    } catch (Exception e) {
	        throw new IllegalArgumentException(e);
	    }
	}
 	
 	
 	//================================ CALLBACK API ================================================
 	//================================ CALLBACK API ================================================
 
	
	public AppResponse getMerchantMid(AppResponse app)
	{
	 factory = DBUtil.getSessionFactory();
	 session = factory.openSession();
	 List<TerminalOnBoardingCallBackBean> list=new ArrayList<>();
		try {
		 Query query = session.createQuery("From TerminalOnBoardingCallBackBean where userId=:uid");
		 query.setString("uid", app.getUserid());
		 list=query.list();
		 if(list.size()>0) {
		  for(TerminalOnBoardingCallBackBean res:list) 
		  { 
			 if(res!=null)
			   {
				 app.setUserid(res.getUserId());
				 app.setMid(res.getMerchant_id()); 
			     app.setTid(res.getTerminal_type());
			     app.setMessage("SUCCESS");
			  } 
		  }
		  //app.setMessage("FAIL");
		 }
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	 return app;	
	}
	
 		@Override
 		public ResponseResult saveSMSResponseApi(SMSCallBackBean smsCallBackBean) {
 		
 			logger.info("-------------------------------DAO :  saveSMSResponseApi ------------------------------");
 			logger.info("DAO >> SMS BEAN DATA  : "+new Gson().toJson(smsCallBackBean));
 			logger.info("DAO >> mobile : "+smsCallBackBean.getMobile());
 			logger.info("DAO >> message : "+smsCallBackBean.getMessage());
 			
 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"DAO : saveSMSResponseApi() : "+new Gson().toJson(smsCallBackBean));
 			ResponseResult rbean = new ResponseResult();
 			String aggregatorId= "Test";
 			
 			try {
 		  		factory = DBUtil.getSessionFactory();
 				session = factory.openSession();
 				transaction = session.beginTransaction();
 
 				Timestamp timestamp;
 		    	try {
 					timestamp = new Timestamp(System.currentTimeMillis());
 		    	}catch (Exception e) {
 					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
 				}
 				logger.info("DAO >> timestamp : "+timestamp);
 		    	smsCallBackBean.setInsertedtime(timestamp);
 
 				
 					//aggregatorId = commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
 					smsCallBackBean.setAggregatorId(aggregatorId);
 					logger.info("DAO >> aggregatorId : "+aggregatorId);
 
 					session.save(smsCallBackBean);
 					transaction.commit();
 					rbean.setMessage(ResponseStatus.Success.toString());
 		   			rbean.setStatus(StatusCode.SUCCESS.getNumVal());
 
 				} catch (Exception e) {
 					logger.info("Error Occured ! Class : " + this.getClass().getName() + " > Method : "+ Thread.currentThread().getStackTrace()[1].getMethodName() + "()");
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","Error : saveSMSResponseApi()","problem in saveSMSResponseApi() : "+e.getMessage()+" "+e);
 					e.printStackTrace();
 					
 					rbean.setMessage(ResponseStatus.Error.toString());
 		   			rbean.setStatus(StatusCode.ERROR.getNumVal());
 		   			
 		   			
 					transaction.rollback();
 
 				} finally {
 				    if(session != null)
 				        session.close();
 				}
 				
 				logger.info("DAO >> Response Status : > : "+rbean.getStatus());
 				logger.info("-------------------------------XXXXXXXXXX  DAO  XXXXXXXXXX ------------------------------");
 				return rbean;
 				
 			}
 
 		@Override
 		public ResponseResult saveTransactionNotificationResponseApi(TransactionNotificationCallBackBean transCallBackBean) {
 		
 			logger.info("-------------------------------DAO :  saveTransactionNotificationResponseApi  ------------------------------");
 			logger.info("DAO >> Transaction Notification BEAN DATA  : "+new Gson().toJson(transCallBackBean));
 
 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"DAO : saveTransactionNotificationResponseApi() : "+new Gson().toJson(transCallBackBean));
 			ResponseResult rbean = new ResponseResult();
 			String aggregatorId= "Test";
 			
 			try {
 		  		factory = DBUtil.getSessionFactory();
 				session = factory.openSession();
 				transaction = session.beginTransaction();
 
 				Timestamp timestamp;
 		    	try {
 					timestamp = new Timestamp(System.currentTimeMillis());
 		    	}catch (Exception e) {
 					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
 				}
 				logger.info("DAO >> timestamp : "+timestamp);
 		    	transCallBackBean.setInsertedtime(timestamp);
 
 				
 					//aggregatorId = commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
 					transCallBackBean.setAggregatorId(aggregatorId);
 					logger.info("DAO >> aggregatorId : "+aggregatorId);
 
 					session.save(transCallBackBean);
 					transaction.commit();
 					rbean.setMessage(ResponseStatus.Success.toString());
 		   			rbean.setStatus(StatusCode.SUCCESS.getNumVal());
 
 				} catch (Exception e) {
 					logger.info("Error Occured ! Class : " + this.getClass().getName() + " > Method : "+ Thread.currentThread().getStackTrace()[1].getMethodName() + "()");
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","saveTransactionNotificationResponseApi()","problem in saveTransactionNotificationResponseApi() : "+e.getMessage()+" "+e);
 					e.printStackTrace();
 					
 					rbean.setMessage(ResponseStatus.Error.toString());
 		   			rbean.setStatus(StatusCode.ERROR.getNumVal());
 		   			
 					transaction.rollback();
 					
 				} finally {
 				    if(session != null)
 				        session.close();
 				}
 				
 				logger.info("DAO >> Response Status : > : "+rbean.getStatus());
 				logger.info("-------------------------------XXXXXXXXXX  DAO  XXXXXXXXXX ------------------------------");
 				return rbean;
 				
 			}
 		
 		@Override
 		public ResponseResult saveTerminalonboardingResponseApi(TerminalOnBoardingCallBackBean transCallBackBean,String authorization) {
 		
 			logger.info("-------------------------------DAO :  saveTerminalonboardingResponseApi  ------------------------------");
 			logger.info("DAO >> Transaction Onboarding BEAN DATA  : "+new Gson().toJson(transCallBackBean));
 			logger.info("DAO >> Transaction Onboarding authorization  : "+authorization);
 
 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"DAO : saveTerminalonboardingResponseApi() : "+new Gson().toJson(transCallBackBean));
 			ResponseResult rbean = new ResponseResult();
 			String aggregatorId= "Test";
 			
 			try {
 		  		factory = DBUtil.getSessionFactory();
 				session = factory.openSession();
 				transaction = session.beginTransaction();
 
 				Timestamp timestamp;
 		    	try {
 					timestamp = new Timestamp(System.currentTimeMillis());
 		    	}catch (Exception e) {
 					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
 				}
 				logger.info("DAO >> timestamp : "+timestamp);
 		    	transCallBackBean.setInsertedtime(timestamp);
 
 				
 					//aggregatorId = commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
 					transCallBackBean.setAggregatorId(aggregatorId);
 					transCallBackBean.setAuthorization(authorization);
 					
 					logger.info("DAO >> aggregatorId : "+aggregatorId);
 					session.save(transCallBackBean);
 					transaction.commit();
 					rbean.setMessage(ResponseStatus.Success.toString());
 		   			rbean.setStatus(StatusCode.SUCCESS.getNumVal());
 
 				} catch (Exception e) {
 					logger.info("Error Occured ! Class : " + this.getClass().getName() + " > Method : "+ Thread.currentThread().getStackTrace()[1].getMethodName() + "()");
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","saveTerminalonboardingResponseApi()","problem in saveTerminalonboardingResponseApi () : "+e.getMessage()+" "+e);
 					e.printStackTrace();
 					
 					rbean.setMessage(ResponseStatus.Error.toString());
 		   			rbean.setStatus(StatusCode.ERROR.getNumVal());
 		   			
 					transaction.rollback();
 
 				} finally {
 				    if(session != null)
 				        session.close();
 				}
 				
 				logger.info("DAO >> Response Status : > : "+rbean.getStatus());
 				logger.info("-------------------------------XXXXXXXXXX  DAO  XXXXXXXXXX ------------------------------");
 				return rbean;
 				
 			}	
 		@Override
 		public ResponseResult saveTerminalstatusResponseApi(TerminalStatusCallBackBean terminalOnboardingBean,String status_id_check,String authorization) {
 		
 			logger.info("-------------------------------DAO :  saveTerminalstatusResponseApi  ------------------------------");
 			logger.info("DAO >> Transaction Status BEAN DATA  : "+new Gson().toJson(terminalOnboardingBean));
 			logger.info("DAO >> Transaction Status status_id_check  : "+status_id_check);
 			logger.info("DAO >> Transaction Status authorization  : "+authorization);
 
 			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"DAO : saveTerminalstatusResponseApi() : "+new Gson().toJson(terminalOnboardingBean));
 			ResponseResult rbean = new ResponseResult();
 			String aggregatorId= "Test";
 			
 			try {
 		  		factory = DBUtil.getSessionFactory();
 				session = factory.openSession();
 				transaction = session.beginTransaction();
 
 				Timestamp timestamp;
 		    	try {
 					timestamp = new Timestamp(System.currentTimeMillis());
 		    	}catch (Exception e) {
 					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
 				}
 		    	
 					logger.info("DAO >> timestamp : "+timestamp);
 					//aggregatorId = commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
 					terminalOnboardingBean.setAggregatorId(aggregatorId);
 					terminalOnboardingBean.setAuthorization(authorization);
 					terminalOnboardingBean.setInsertedtime(timestamp);
 					terminalOnboardingBean.setStatus_id_check(status_id_check);
 					
 					logger.info("DAO >> aggregatorId : "+aggregatorId);
 					session.save(terminalOnboardingBean);
 					transaction.commit();
 					rbean.setMessage(ResponseStatus.Success.toString());
 		   			rbean.setStatus(StatusCode.SUCCESS.getNumVal());
 
 				} catch (Exception e) {
 					logger.info("Error Occured ! Class : " + this.getClass().getName() + " > Method : "+ Thread.currentThread().getStackTrace()[1].getMethodName() + "()");
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","saveTerminalstatusResponseApi()","problem in saveTerminalstatusResponseApi () : "+e.getMessage()+" "+e);
 					e.printStackTrace();
 					
 					rbean.setMessage(ResponseStatus.Error.toString());
 		   			rbean.setStatus(StatusCode.ERROR.getNumVal());
 		   			
 					transaction.rollback();
 
 				} finally {
 				    if(session != null)
 				        session.close();
 				}
 				
 				logger.info("DAO >> Response Status : > : "+rbean.getStatus());
 				logger.info("-------------------------------XXXXXXXXXX  DAO  XXXXXXXXXX ------------------------------");
 				return rbean;
 				
 			}	
 		
 		
 		
 		
 		
 		@Override
		public ResponseResult saveMerchantOnboardingResponseApi(String jsonData,String authorization) {
		
			logger.info("-------------------------------DAO :  saveTerminalstatusResponseApi  ------------------------------");
			logger.info("DAO >> Merchent Onboarding jsonData  : "+jsonData);
			logger.info("DAO >> Merchent Onboarding authorization  : "+authorization);
			MerchantOnboardingCallBackBean  merchantOnbarding = new MerchantOnboardingCallBackBean();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"DAO : saveTerminalstatusResponseApi() : "+jsonData);
			ResponseResult rbean = new ResponseResult();
			String aggregatorId= "Test";
			
			try {
		  		factory = DBUtil.getSessionFactory();
				session = factory.openSession();
				transaction = session.beginTransaction();

				Timestamp timestamp;
		    	try {
					timestamp = new Timestamp(System.currentTimeMillis());
		    	}catch (Exception e) {
					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
				}
		    	
					logger.info("DAO >> timestamp : "+timestamp);
					//aggregatorId = commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
					merchantOnbarding.setAggregatorId(aggregatorId);
					merchantOnbarding.setAuthorization(authorization);
					merchantOnbarding.setInsertedtime(timestamp);

					//========================= MO- SalesInformationBean ==========================
		           /*
					JsonObject jsonObject = new JsonParser().parse(jsonData).getAsJsonObject();
		            String aggregator_application_number = jsonObject.getAsJsonObject("sales_information").get("aggregator_application_number").getAsString();
		            String application_date = jsonObject.getAsJsonObject("sales_information").get("application_date").getAsString();
		            String aggreement_date = jsonObject.getAsJsonObject("sales_information").get("aggreement_date").getAsString();
		            */
		            
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MOSalesInformationBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					
		            String aggregator_application_number =JSONUtil.GETJSONObjectValue(jsonData,"sales_information", "aggregator_application_number") ;
		            String application_date = JSONUtil.GETJSONObjectValue(jsonData,"sales_information", "application_date") ; 
		            String aggreement_date = JSONUtil.GETJSONObjectValue(jsonData,"sales_information", "aggreement_date") ; 
 		            
		            
		            
					logger.info("DAO >> JSON : sales_information > aggregator_application_number  : "+aggregator_application_number);
					logger.info("DAO >> JSON : sales_information > application_date : "+application_date);
					logger.info("DAO >> JSON : sales_information > aggreement_date  : "+aggreement_date);
					logger.info("DAO >> JSON : sales_information > parseDate application_date : "+parseDate(application_date));
					logger.info("DAO >> JSON : sales_information > parseDate aggreement_date  : "+parseDate(aggreement_date));

					MOSalesInformationBean  mo_salesInformation_Obj = new MOSalesInformationBean(timestamp, 
																							aggregator_application_number,
																							parseDate(application_date), 
																							parseDate(aggreement_date), 
																							merchantOnbarding);
					
							
					/*
					 * mo_Information_Obj.setApplicationDate(parseDate(application_date));
					 * mo_Information_Obj.setAggreementDate(parseDate(aggreement_date));
					 * mo_Information_Obj.setAggregatorApplicationNumber(
					 * aggregator_application_number);
					 * mo_Information_Obj.setMobean(merchantOnbarding);
					 */
							
					//========================= MO- company_information  ==========================
/*
					String legal_name	= jsonObject.getAsJsonObject("company_information").get("legal_name").getAsString();
					String brand_name	= jsonObject.getAsJsonObject("company_information").get("brand_name").getAsString();
					String registered_address	= jsonObject.getAsJsonObject("company_information").get("registered_address").getAsString();
					String registered_pincode	= jsonObject.getAsJsonObject("company_information").get("registered_pincode").getAsString();
					String pan	= jsonObject.getAsJsonObject("company_information").get("pan").getAsString();
					String gstin	= jsonObject.getAsJsonObject("company_information").get("gstin").getAsString();
					String business_type	= jsonObject.getAsJsonObject("company_information").get("business_type").getAsString();
					String established_year	= jsonObject.getAsJsonObject("company_information").get("established_year").getAsString();
					String business_nature	= jsonObject.getAsJsonObject("company_information").get("business_nature").getAsString();
					String merchant_category_code_id	= jsonObject.getAsJsonObject("company_information").get("merchant_category_code_id").getAsString();
					String merchant_type_id	= jsonObject.getAsJsonObject("company_information").get("merchant_type_id").getAsString();
					String contact_name	= jsonObject.getAsJsonObject("company_information").get("contact_name").getAsString();
					String contact_mobile	= jsonObject.getAsJsonObject("company_information").get("contact_mobile").getAsString();
					String contact_alternate_mobile	= jsonObject.getAsJsonObject("company_information").get("contact_alternate_mobile").getAsString();
					String contact_email	= jsonObject.getAsJsonObject("company_information").get("contact_email").getAsString();

					
					

					logger.info("DAO >> JSON : company_information > parseDate legal_name  : "+legal_name);
					logger.info("DAO >> JSON : company_information > parseDate brand_name  : "+brand_name);
					logger.info("DAO >> JSON : company_information > parseDate registered_address  : "+registered_address);
					logger.info("DAO >> JSON : company_information > parseDate registered_pincode  : "+registered_pincode);
					logger.info("DAO >> JSON : company_information > parseDate pan  : "+pan);
					logger.info("DAO >> JSON : company_information > parseDate gstin  : "+gstin);
					logger.info("DAO >> JSON : company_information > parseDate business_type  : "+business_type);
					logger.info("DAO >> JSON : company_information > parseDate established_year  : "+parseDate(established_year));
					logger.info("DAO >> JSON : company_information > parseDate business_nature  : "+business_nature);
					logger.info("DAO >> JSON : company_information > parseDate merchant_category_code_id  : "+merchant_category_code_id);
					logger.info("DAO >> JSON : company_information > parseDate merchant_type_id  : "+merchant_type_id);
					logger.info("DAO >> JSON : company_information > parseDate contact_name  : "+contact_name);
					logger.info("DAO >> JSON : company_information > parseDate contact_mobile  : "+contact_mobile);
					logger.info("DAO >> JSON : company_information > parseDate contact_alternate_mobile  : "+contact_alternate_mobile);
					logger.info("DAO >> JSON : company_information > parseDate contact_email  : "+contact_email);
	*/				
                    logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MOCompanyInformationBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

					String cI_legal_name	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "legal_name") ;  
					String cI_brand_name	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "brand_name") ; 
					String cI_registered_address	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "registered_address") ;
					String cI_registered_pincode	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "registered_pincode") ;
					String cI_pan	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "pan") ;
					String cI_gstin	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "gstin") ;
					String cI_business_type	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "business_type") ;
					String cI_established_year	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "established_year") ;
					String cI_business_nature	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "business_nature") ;
					String cI_merchant_category_code_id	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "merchant_category_code_id") ;
					String cI_merchant_type_id	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "merchant_type_id") ;
					String cI_contact_name	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "contact_name") ;
					String cI_contact_mobile	=JSONUtil.GETJSONObjectValue(jsonData,"company_information", "contact_mobile") ; 
					String cI_contact_alternate_mobile	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "contact_alternate_mobile") ;
					String cI_contact_email	= JSONUtil.GETJSONObjectValue(jsonData,"company_information", "contact_email") ;


					logger.info("DAO >> JSON : company_information > parseDate legal_name  : "+cI_legal_name);
					logger.info("DAO >> JSON : company_information > parseDate brand_name  : "+cI_brand_name);
					logger.info("DAO >> JSON : company_information > parseDate registered_address  : "+cI_registered_address);
					logger.info("DAO >> JSON : company_information > parseDate registered_pincode  : "+cI_registered_pincode);
					logger.info("DAO >> JSON : company_information > parseDate pan  : "+cI_pan);
					logger.info("DAO >> JSON : company_information > parseDate gstin  : "+cI_gstin);
					logger.info("DAO >> JSON : company_information > parseDate business_type  : "+cI_business_type);
					logger.info("DAO >> JSON : company_information > parseDate established_year  : "+parseDate(cI_established_year));
					logger.info("DAO >> JSON : company_information > parseDate business_nature  : "+cI_business_nature);
					logger.info("DAO >> JSON : company_information > parseDate merchant_category_code_id  : "+cI_merchant_category_code_id);
					logger.info("DAO >> JSON : company_information > parseDate merchant_type_id  : "+cI_merchant_type_id);
					logger.info("DAO >> JSON : company_information > parseDate contact_name  : "+cI_contact_name);
					logger.info("DAO >> JSON : company_information > parseDate contact_mobile  : "+cI_contact_mobile);
					logger.info("DAO >> JSON : company_information > parseDate contact_alternate_mobile  : "+cI_contact_alternate_mobile);
					logger.info("DAO >> JSON : company_information > parseDate contact_email  : "+cI_contact_email);
					
					
					MOCompanyInformationBean  mo_companyInformation_Obj = new MOCompanyInformationBean(timestamp, 
							  cI_legal_name, 
							  cI_brand_name,
							  cI_registered_address,
							  cI_registered_pincode,
							  cI_pan, 
							  cI_gstin, 
							  cI_business_type, 
							  parseDate(cI_established_year),
							  cI_business_nature,
							  cI_merchant_category_code_id,
							  cI_merchant_type_id, 
							  cI_contact_name, 
							  cI_contact_mobile, 
							  cI_contact_alternate_mobile,
							  cI_contact_email,
							  merchantOnbarding);
																				
					//========================= SAVE/UPDATE ==========================
					
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MOPersoanInformationBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

					String pI_title	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "title",0);
					String pI_first_name	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "first_name",0);
					String pI_last_name	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "last_name",0);
					String pI_address	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "address",0);
					String pI_pincode	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "pincode",0);
					String pI_mobile	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "mobile",0);
					String pI_email	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "email",0);
					String pI_nationality	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "nationality",0);
					String pI_passport_expiry_date	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "passport_expiry_date",0);
					String pI_is_own_house	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "is_own_house",0);
					String pI_dob	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "dob",0);
					String pI_pan	= JSONUtil.GETJSONArrayValue(jsonData,"personal_information", "pan",0);


					logger.info("DAO >> JSON : personal_information > title  : "+pI_title);
					logger.info("DAO >> JSON : personal_information > first_name  : "+pI_first_name);
					logger.info("DAO >> JSON : personal_information > last_name  : "+pI_last_name);
					logger.info("DAO >> JSON : personal_information > address  : "+pI_address);
					logger.info("DAO >> JSON : personal_information > pincode  : "+pI_pincode);
					logger.info("DAO >> JSON : personal_information > mobile  : "+pI_mobile);
					logger.info("DAO >> JSON : personal_information > email  : "+pI_email);
					logger.info("DAO >> JSON : personal_information > nationality  : "+pI_nationality);
					logger.info("DAO >> JSON : personal_information > passport_expiry_date  : "+parseDate(pI_passport_expiry_date));
					logger.info("DAO >> JSON : personal_information > is_own_house  : "+pI_is_own_house);
					logger.info("DAO >> JSON : personal_information > dob  : "+parseDate(pI_dob));
					logger.info("DAO >> JSON : personal_information > pan  : "+pI_pan);
					
					
					MOPersoanInformationBean  mo_personalInformation_Obj = new MOPersoanInformationBean(timestamp, 
																					pI_title, 
																					pI_first_name, 
																					pI_last_name, 
																					pI_address, 
																					pI_pincode, 
																					pI_mobile,
																					pI_email, 
																					pI_nationality, 
																					parseDate(pI_passport_expiry_date),
																					Boolean.valueOf(pI_is_own_house), 
																					parseDate(pI_passport_expiry_date),
																					pI_pan,
																					merchantOnbarding);
					
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MORiskInformationBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

					String rI_expected_card_business	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "expected_card_business") ;
					String rI_average_bill_amount	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "average_bill_amount") ;
					String rI_velocity_check_minutes	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "velocity_check_minutes") ;
					String rI_velocity_check_count	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "velocity_check_count") ;
					String rI_settlement_days	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "settlement_days") ;
					String rI_merchant_type_code	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "merchant_type_code") ;
					String rI_member_since	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "member_since") ;
					String rI_international_card_acceptance	= JSONUtil.GETJSONObjectValue(jsonData,"risk_information", "international_card_acceptance") ;
					String rI_transaction_sets	= JSONUtil.GETJSONArrayJSONObjectValue(jsonData,"risk_information", "transaction_sets") ;

					
					logger.info("DAO >> JSON : risk_information > expected_card_business  : "+rI_expected_card_business);
					logger.info("DAO >> JSON : risk_information > average_bill_amount  : "+rI_average_bill_amount);
					logger.info("DAO >> JSON : risk_information > velocity_check_minutes  : "+rI_velocity_check_minutes);
					logger.info("DAO >> JSON : risk_information > velocity_check_count  : "+rI_velocity_check_count);
					logger.info("DAO >> JSON : risk_information > settlement_days  : "+rI_settlement_days);
					logger.info("DAO >> JSON : risk_information > merchant_type_code  : "+rI_merchant_type_code);
					logger.info("DAO >> JSON : risk_information > member_since  : "+rI_member_since);
					logger.info("DAO >> JSON : risk_information > international_card_acceptance  : "+rI_international_card_acceptance);
					logger.info("DAO >> JSON : risk_information > transaction_sets  : "+rI_transaction_sets);
			
					MORiskInformationBean  mo_riskInformation_Obj = new MORiskInformationBean(timestamp, 
																			Long.parseLong(rI_expected_card_business),
																			Long.parseLong(rI_average_bill_amount), 
																			Long.parseLong(rI_velocity_check_minutes), 
																			Long.parseLong(rI_velocity_check_count),
																			Long.parseLong(rI_settlement_days),
																			rI_merchant_type_code, 
																			parseDate(rI_member_since),
																			Boolean.valueOf(rI_international_card_acceptance),
																			rI_transaction_sets, 
																			merchantOnbarding);	
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MODocumentsInformationBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					List<MODocumentsInformationBean> docList =  docListGet(jsonData,timestamp,merchantOnbarding) ;
					if(docList.size() > 0) {
						for (MODocumentsInformationBean docBean : docList) {
							session.saveOrUpdate(docBean);
							List<MODocumentsInformationURLBean> urlBeanList = docBean.getUrlBeanList();
							if(urlBeanList.size()>0) {
								for (MODocumentsInformationURLBean urlBean : urlBeanList) {
									 urlBean.setDocinfo(docBean);
									 session.saveOrUpdate(urlBean); 
								}
							}
							
						}
					}
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MODocumentsInformationURLBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

					  /*if(urlList.size()>0) {
						 for (MODocumentsInformationURLBean urlBean :  urlList) {
							 session.saveOrUpdate(urlBean); 
						 }
					  }*/
					
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| SAVE/UPDATE |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
 
					
					
					session.saveOrUpdate(merchantOnbarding);
					session.saveOrUpdate(mo_salesInformation_Obj);
					session.saveOrUpdate(mo_companyInformation_Obj);
					session.saveOrUpdate(mo_personalInformation_Obj);
					session.saveOrUpdate(mo_riskInformation_Obj);
						
					logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~| MORiskInformationTransLimitBean |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

					List<MORiskInformationTransLimitBean> listGet =  listGet(jsonData,timestamp,mo_riskInformation_Obj) ;
					if(listGet.size() > 0) {
						for (MORiskInformationTransLimitBean moRiskInformationTransLimitBean : listGet) {
							session.saveOrUpdate(moRiskInformationTransLimitBean);
						}
					}
					
					
					transaction.commit();
					rbean.setMessage(ResponseStatus.Success.toString());
		   			rbean.setStatus(StatusCode.SUCCESS.getNumVal());

				} catch (Exception e) {
					logger.info("Error Occured ! Class : " + this.getClass().getName() + " > Method : "+ Thread.currentThread().getStackTrace()[1].getMethodName() + "()");
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","saveTerminalstatusResponseApi()","problem in saveTerminalstatusResponseApi () : "+e.getMessage()+" "+e);
					e.printStackTrace();
					
					rbean.setMessage(ResponseStatus.Error.toString());
		   			rbean.setStatus(StatusCode.ERROR.getNumVal());
		   			
					transaction.rollback();

				} finally {
				    if(session != null)
				        session.close();
				}
				
				logger.info("DAO >> Response Status : > : "+rbean.getStatus());
				logger.info("-------------------------------XXXXXXXXXX  DAO  XXXXXXXXXX ------------------------------");
				return rbean;
				
			}
 		
     	
 			public List<MODocumentsInformationBean> docListGet(String jsonData,Timestamp timestamp,MerchantOnboardingCallBackBean mobean){
 	 			
 				List<MODocumentsInformationBean> listGet  = new ArrayList<>();
 				JsonObject jsonMainObject = new JsonParser().parse(jsonData).getAsJsonObject();
 		    	JsonArray jsonArrayObj = jsonMainObject.getAsJsonArray("documents");
 		    	
 		    	System.out.println("***************************************************************");
 				for (int i = 0; i < jsonArrayObj.size(); i++) {
 					
 					MODocumentsInformationBean  docBean = null;
 					
 			    	System.out.println("@@@@@@@@@@@@@@@@@@@ LOOP - "+i+" @@@@@@@@@@@@@@@@@@@@@@@");
 			    	
 					JsonObject jsonChildObjX	= (JsonObject) jsonArrayObj.get(i);
 					String doc_name	= jsonChildObjX.get("name").getAsString();
 					String doc_type_id	= jsonChildObjX.get("type_id").getAsString();
 					
 					List<MODocumentsInformationURLBean> urlList  = new ArrayList<>();

 					//----------------------------------------------------------------
 					JsonArray innerArrayObj = jsonChildObjX.getAsJsonArray("urls");
 			    	for (int j = 0; j < innerArrayObj.size(); j++) {
 				    	
 			    		MODocumentsInformationURLBean  urlBean  = null;
 			    		System.out.println("---- Inner - "+j+" Start --------");
 						JsonObject jsonChildObjXX	= (JsonObject) innerArrayObj.get(j);
 					
 						String url_path	= jsonChildObjXX.get("path").getAsString();
 						String url_filename	= jsonChildObjXX.get("filename").getAsString();
 						String url_mime_type	= jsonChildObjXX.get("mime_type").getAsString();
 						urlBean =new MODocumentsInformationURLBean(timestamp, url_path, url_filename, url_mime_type, docBean);
 						urlList.add(urlBean);
 				    	System.out.println("---- Inner - "+j+" END --------");
 					}
 			    	//----------------------------------------------------------------
 			    	
 					docBean = new MODocumentsInformationBean(timestamp, doc_name, doc_type_id, mobean,urlList);
 					listGet.add(docBean);
 					
 					System.out.println("@@@@@@@@@@@@@@@@@@@ LOOP - "+i+" END  @@@@@@@@@@@@@@@@@@@@@@@");
 				}
 				return listGet;
 			}
 			public List<MORiskInformationTransLimitBean> listGet(String jsonData,Timestamp timestamp,MORiskInformationBean riskInfo){
 	 		 
			List<MORiskInformationTransLimitBean> listGet  = new ArrayList<>();
			
			JsonObject jsonMainObject = new JsonParser().parse(jsonData).getAsJsonObject();
			System.out.println(jsonMainObject.has("risk_information"));

			JsonObject jsonChildObj	= jsonMainObject.getAsJsonObject("risk_information");

			JsonArray jsonArrayObj = jsonChildObj.getAsJsonArray("transaction_limits");
			System.out.println(jsonArrayObj.size());
			
			for (int i = 0; i < jsonArrayObj.size(); i++) {
				
				JsonObject jsonChildObjX	= (JsonObject) jsonArrayObj.get(i);
				MORiskInformationTransLimitBean  bean = null;
				
				String channel			= jsonChildObjX.get("channel").getAsString();
		    	String daily_limit		= jsonChildObjX.get("daily_limit").getAsString();
		    	String weekly_limit		= jsonChildObjX.get("weekly_limit").getAsString();
		    	String monthly_limit	= jsonChildObjX.get("monthly_limit").getAsString();
		    	String minimum_amount	= jsonChildObjX.get("minimum_amount").getAsString();
		    	String maximum_amount	= jsonChildObjX.get("maximum_amount").getAsString();

		    	bean  = new MORiskInformationTransLimitBean(timestamp, 
		    												channel,
		    												Long.parseLong(daily_limit), 
		    												Long.parseLong(weekly_limit),
															Long.parseLong(monthly_limit),
															Long.parseLong(minimum_amount), 
															Long.parseLong(maximum_amount),
		    												riskInfo);
		    	listGet.add(bean);
		    	System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			}
			
			return listGet;
		}

 		
 		
 	//================================ CALLBACK API ================================================

	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	 public double getWalletBalance(String walletId){
		 
	    
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
					+ "getWalletBalance"
					);
		// logger.info("start excution ===================== method getWalletBalance(walletId)"+walletId);
		 double walletBal=0.0;
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 Transaction transaction = session.beginTransaction();
		 try{
		 	 Query query=session.createQuery("from WalletBalanceBean  where walletid= :walletId");
				query.setString("walletId",walletId);
				List<WalletBalanceBean> list=query.list();
				if(list!=null && list.size()>0){
					walletBal=list.get(0).getFinalBalance();
				}
				transaction.commit();
			 
		 }catch (HibernateException e) {
	         if (transaction!=null)transaction.rollback();
	         e.printStackTrace();
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
						+ "getWalletBalance()"+"|problem in getWalletBalance "+e.getMessage()+" "+e
						);
	        // logger.debug("problem in getWalletBalance=====" + e.getMessage(), e);
	      }finally {
	    	  session.close(); 
	      }
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
					+ "getWalletBalance()"+"|Inside getWalletBalance return :"+walletBal
					);
		 //logger.info("Inside ====================getWalletBalance=== return===="+walletBal);
		 return walletBal;
		 
	 }
	 
	 /**
		 * 
		 * @param walletId
		 * @return
		 */
		
		 public double getWalletBalancebyId(String agentId){
			
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
						+ "getWalletBalancebyId()"+"|Agentid :"+agentId
						);
			// logger.info("start excution ===================== method getWalletBalance(walletId)"+agentId);
			 double walletBal=0.0;
			 factory=DBUtil.getSessionFactory();
			 Session session = factory.openSession();
			 Transaction transaction = session.beginTransaction();
			 try{
				 Query qry = session.createSQLQuery("Select walletId from walletmast where id='"+agentId+"';" );
				 String walletId =(String)(qry.uniqueResult());
				 				 
			 	 Query query=session.createQuery("from WalletBalanceBean WBB where walletid= :walletId");
					query.setString("walletId",walletId);
					List<WalletBalanceBean> list=query.list();
					if(list!=null && list.size()>0){
						walletBal=list.get(0).getFinalBalance();
					}
					transaction.commit();
				 
			 }catch (HibernateException e) {
		         if (transaction!=null)transaction.rollback();
		         e.printStackTrace();
		         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
							+ "getWalletBalancebyId()"+"problem in getWalletBalance"+e.getMessage()+" "+e
							);
		        // logger.debug("problem in getWalletBalance=====" + e.getMessage(), e);
		      }finally {
		    	  session.close(); 
		      }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
						+ "getWalletBalancebyId()"+"|WalletBal :"+walletBal
						);
			// logger.info("Inside ====================getWalletBalance=== return===="+walletBal);
			 return walletBal;
			 
		 }
		 /**
			 * 
			 * @param walletId
			 * @return
			 */
			
			 public double getCashBackWalletBalance(String walletId){
				
			      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
							+ "getCashBackWalletBalance()");
			      //logger.info("start excution ===================== method getCashBackWalletBalance(walletId)"+walletId);
				 double walletBal=0.0;
				 factory=DBUtil.getSessionFactory();
				 Session session = factory.openSession();
				 Transaction transaction = session.beginTransaction();
				 try{
				 	 Query query=session.createQuery("from CashBackBalanceBean WBB where walletid= :walletId");
						query.setString("walletId",walletId);
						List<CashBackBalanceBean> list=query.list();
						if(list!=null && list.size()>0){
							walletBal=list.get(0).getFinalBalance();
						}
						transaction.commit();
					 
				 }catch (Exception e) {
			         if (transaction!=null)transaction.rollback();
			         e.printStackTrace();
			         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
								+ "getCashBackWalletBalance()"+"problem in getCashBackWalletBalance"+e.getMessage()+" "+e);
			         //logger.debug("problem in getCashBackWalletBalance=====" + e.getMessage(), e);
			      }finally {
			    	  session.close(); 
			      }
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",walletId, "", "", "" 
							+ "getCashBackWalletBalance()"+"|WalletBal :"+walletBal);
				 //logger.info("Inside ====================getCashBackWalletBalance=== return===="+walletBal);
				 return walletBal;
				 
			 }
			 
			 /**
				 * 
				 * @param walletId
				 * @return
				 */
				
				 public double getCashBackWalletBalancebyId(String agentId){
					
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
								+ "getCashBackWalletBalancebyId()"+"|AgentId :"+agentId);
					 //logger.info("start excution ===================== method getCashBackWalletBalancebyId(walletId)"+agentId);
					 double walletBal=0.0;
					 factory=DBUtil.getSessionFactory();
					 Session session = factory.openSession();
					 Transaction transaction = session.beginTransaction();
					 try{
						 Query qry = session.createSQLQuery("Select walletId from walletmast where id='"+agentId+"';" );
						 String walletId =(String)(qry.uniqueResult());
						 				 
					 	 Query query=session.createQuery("from CashBackBalanceBean WBB where walletid= :walletId");
							query.setString("walletId",walletId);
							List<CashBackBalanceBean> list=query.list();
							if(list!=null && list.size()>0){
								walletBal=list.get(0).getFinalBalance();
							}
							transaction.commit();
						 
					 }catch (HibernateException e) {
				         if (transaction!=null)transaction.rollback();
				         e.printStackTrace();
				         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
									+ "getCashBackWalletBalancebyId()"+"|problem in getCashBackWalletBalancebyId "+e.getMessage()+" "+e);
				        // logger.debug("problem in getCashBackWalletBalancebyId=====" + e.getMessage(), e);
				      }finally {
				    	  session.close(); 
				      }
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "" 
								+ "getCashBackWalletBalancebyId()"+"|WalletBal :"+walletBal);
					 //logger.info("Inside ====================getCashBackWalletBalancebyId=== return===="+walletBal);
					 return walletBal;
					 
				 }
			 
			 
		 
		 
		 
		 
		 
		 
	 
	
	/**
	 * 
	 * @param trxId
	 * @param trxAmount
	 * @param walletId
	 * @param mobileNo
	 * @param trxReasion
	 * @return
	 */
	 
		/*public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion ,
				String payTxnid,String aggreatorid,String ipiemi,String agent){
			
			System.out.println("________Inside Transaction DaoImpl.java_______________");
			System.out.println("_____trxAmount____"+trxAmount);
			System.out.println("_____walletId____"+walletId);
			System.out.println("_____mobileNo____"+mobileNo);
			System.out.println("_____trxReasion____"+trxReasion);
			System.out.println("_____payTxnid____"+payTxnid);
			System.out.println("_____aggreatorid____"+aggreatorid);
			System.out.println("_____ipiemi____"+ipiemi);
			System.out.println("_____agent____"+agent);
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", ""+ "savePGTrx()"+"|agent :"+agent);
			
			//logger.info("start excution ===================== method savePGTrx()"+walletId);
			String status="1001";
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	
		    	String velocityCheckSt=velocityCheck(walletId,trxAmount,1);
  	  			if(velocityCheckSt.equals("1000")){
		    	  	
		    	PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();
		    	
		    	String pgtxnid=commanUtilDao.getTrxId("PGTXN");
		    	
		    	System.out.println("____________pgTxnId_____________"+pgtxnid);
		    	
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
						+ "savePGTrx()"+"|agent :"+agent);
		    	//logger.info(" excution ===================== pgtxnid"+pgtxnid);
		    	
		    	
		    	paymentGatewayTrxBean.setTrxId(pgtxnid);
		    	paymentGatewayTrxBean.setTrxAmount(trxAmount);
		    	paymentGatewayTrxBean.setTrxResult("Cancel");
		    	paymentGatewayTrxBean.setWalletId(walletId);
		    	paymentGatewayTrxBean.setMobileNo(mobileNo);
		    	paymentGatewayTrxBean.setTrxReasion(trxReasion);
		    	paymentGatewayTrxBean.setPayTxnid(payTxnid);
		    	paymentGatewayTrxBean.setAggreatorid(aggreatorid);
		    	paymentGatewayTrxBean.setImeiIP(ipiemi);
		    	paymentGatewayTrxBean.setUseragent(agent);
		    	
		    	System.out.println("_____trxId__________"+paymentGatewayTrxBean.getTrxId());
		    	System.out.println("_____trxAmount__________"+paymentGatewayTrxBean.getTrxAmount());
		    	System.out.println("_____TrxResult__________"+paymentGatewayTrxBean.getTrxResult());
		    	System.out.println("_____WalletId__________"+paymentGatewayTrxBean.getWalletId());
		    	System.out.println("_____MobileNo__________"+paymentGatewayTrxBean.getMobileNo());
		    	System.out.println("_____TrxReasion__________"+paymentGatewayTrxBean.getTrxReasion());
		    	System.out.println("_____PayTxnid__________"+paymentGatewayTrxBean.getPayTxnid());
		    	System.out.println("_____Aggreatorid__________"+paymentGatewayTrxBean.getAggreatorid());
		    	System.out.println("_____ImeiIP__________"+paymentGatewayTrxBean.getImeiIP());
		    	System.out.println("_____Useragent__________"+paymentGatewayTrxBean.getUseragent());

		    	session.save(paymentGatewayTrxBean);
		    	transaction.commit();
		    	status=pgtxnid;
				}else{
  	  			status=velocityCheckSt;
  	  			}
		    	
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
						+ "savePGTrx()"+"problem in Add  PG Txn"+e.getMessage()+" "+e);
		    	status="7000";
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
					+ "savePGTrx()"+"|agent :"+agent+"|status :"+status);
		   // logger.info(" excution ===================== status"+status);
		    return status;
		}*/
				
				 @Override
				 public PGAggregatorBean getPGAGGId(String aggreatorid) {
					// TODO Auto-generated method stubsdsd
					
					PGAggregatorBean bean1=new PGAggregatorBean() ;
					factory=DBUtil.getSessionFactory();
					Session session = factory.openSession();
					Transaction transaction = session.beginTransaction();
				    
				    try{
				    	
				    	bean1=(PGAggregatorBean)session.get(PGAggregatorBean.class, aggreatorid);
				    	/* bean1.setId("OAGG001057");
				    	 bean1.setMid("1903260434111000");
				    	 bean1.setPassword("");
				    	 bean1.setTransactionkey("2cafbc664d074721");
				    	*/
				    	 transaction.commit();
				}
				    
				    catch(Exception e) {
				    	
				    }
				    finally {
						session.close();
					}
				    return bean1 ;
				}
				 
				 
				 public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion ,
							String payTxnid,String aggreatorid,String ipiemi,String agent){
						
						System.out.println("________Inside Transaction DaoImpl.java_______________");
						System.out.println("_____trxAmount____"+trxAmount);
						System.out.println("_____walletId____"+walletId);
						System.out.println("_____mobileNo____"+mobileNo);
						System.out.println("_____trxReasion____"+trxReasion);
						System.out.println("_____payTxnid____"+payTxnid);
						System.out.println("_____aggreatorid____"+aggreatorid);
						System.out.println("_____ipiemi____"+ipiemi);
						System.out.println("_____agent____"+agent);
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", ""+ "savePGTrx()"+"|agent :"+agent);
						
						//logger.info("start excution ===================== method savePGTrx()"+walletId);
						String status="1001";
						factory=DBUtil.getSessionFactory();
						Session session = factory.openSession();
					    Transaction transaction = session.beginTransaction();
					    try{
					    	
					    	String velocityCheckSt=velocityCheck(walletId,trxAmount,1);
			  	  			if(velocityCheckSt.equals("1000")){
					    	  	
					    	PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();

					    	Criteria criteria =  session.createCriteria(WalletMastBean.class);
					    	criteria.add(Restrictions.eq("walletid", walletId));
					    	List<WalletMastBean> walletMast = criteria.list();
					    	
					    	if( walletMast == null || walletMast.get(0).getOnlineMoney()== 0) {
					    		return "7000";
					    	}
							
					    	
					    	String pgtxnid=commanUtilDao.getTrxId("PGTXN",aggreatorid);
					    	
					    	System.out.println("____________pgTxnId_____________"+pgtxnid);
					    	
					    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
									+ "savePGTrx()"+"|agent :"+agent);
					    	//logger.info(" excution ===================== pgtxnid"+pgtxnid);
					    	
					    	
					    	paymentGatewayTrxBean.setTrxId(pgtxnid);
					    	paymentGatewayTrxBean.setTrxAmount(trxAmount);
					    	paymentGatewayTrxBean.setTrxResult("Cancel");
					    	paymentGatewayTrxBean.setWalletId(walletId);
					    	paymentGatewayTrxBean.setMobileNo(mobileNo);
					    	paymentGatewayTrxBean.setTrxReasion(trxReasion);
					    	paymentGatewayTrxBean.setPayTxnid(payTxnid);
					    	paymentGatewayTrxBean.setAggreatorid(aggreatorid);
					    	paymentGatewayTrxBean.setImeiIP(ipiemi);
					    	paymentGatewayTrxBean.setUseragent(agent);
					    	
					    	System.out.println("_____trxId__________"+paymentGatewayTrxBean.getTrxId());
					    	System.out.println("_____trxAmount__________"+paymentGatewayTrxBean.getTrxAmount());
					    	System.out.println("_____TrxResult__________"+paymentGatewayTrxBean.getTrxResult());
					    	System.out.println("_____WalletId__________"+paymentGatewayTrxBean.getWalletId());
					    	System.out.println("_____MobileNo__________"+paymentGatewayTrxBean.getMobileNo());
					    	System.out.println("_____TrxReasion__________"+paymentGatewayTrxBean.getTrxReasion());
					    	System.out.println("_____PayTxnid__________"+paymentGatewayTrxBean.getPayTxnid());
					    	System.out.println("_____Aggreatorid__________"+paymentGatewayTrxBean.getAggreatorid());
					    	System.out.println("_____ImeiIP__________"+paymentGatewayTrxBean.getImeiIP());
					    	System.out.println("_____Useragent__________"+paymentGatewayTrxBean.getUseragent());

					    	
					    	
					    	status=pgtxnid;
							session.save(paymentGatewayTrxBean);
							transaction.commit();
							
							}else{
			  	  			status=velocityCheckSt;
			  	  			}
					    	
					    }catch(Exception e){
					    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
									+ "savePGTrx()"+"problem in Add  PG Txn"+e.getMessage()+" "+e);
					    	status="7000";
					   		  e.printStackTrace();
					   			 transaction.rollback();
					   	} finally {
					   				session.close();
					   	}
					    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",walletId, "", "", "" 
								+ "savePGTrx()"+"|agent :"+agent+"|status :"+status);
					   // logger.info(" excution ===================== status"+status);
					    return status;
					}		 
		
		/**
		 * txnAmount,walletId,mobileNumber,txnReason,emailId,appName,imeiIP
		 */
		public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId,String aggreatorid){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,userId,walletId, "", "", "" 
						+ "savePGTrx()");
			//logger.info("start excution ===================== method savePGTrx()"+walletId);
			String status="1001";
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	WalletMastBean wmBean = (WalletMastBean)session.get(WalletMastBean.class,userId);
		    	if(wmBean == null || wmBean.getOnlineMoney()==0) {
		    		return "7000";
		    	}
		    	String velocityCheckSt=velocityCheck(walletId,trxAmount,1);
  	  			if(velocityCheckSt.equals("1000")){
		     	PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();
		    	String pgtxnid=commanUtilDao.getTrxId("PGTXN",aggreatorid);
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,userId,walletId, "", "", "" 
						+  "savePGTrx()"+"|pgtxnid :"+pgtxnid);
		    	//logger.info(" excution ===================== pgtxnid"+pgtxnid);
		    	paymentGatewayTrxBean.setTrxId(pgtxnid);
		    	paymentGatewayTrxBean.setTrxAmount(trxAmount);
		    	paymentGatewayTrxBean.setTrxResult("Cancel");
		    	paymentGatewayTrxBean.setWalletId(walletId);
		    	paymentGatewayTrxBean.setMobileNo(mobileNo);
		    	paymentGatewayTrxBean.setTrxReasion(trxReason);
		    	paymentGatewayTrxBean.setEmail(emailId);
		    	paymentGatewayTrxBean.setAppName(appName);
		    	paymentGatewayTrxBean.setImeiIP(imeiIP);
		    	paymentGatewayTrxBean.setUserId(userId);
		    	paymentGatewayTrxBean.setAggreatorid(aggreatorid);
		       	session.save(paymentGatewayTrxBean);
		    	transaction.commit();
		    	status="1000|"+pgtxnid;
  	  			}else{
  	  			status=velocityCheckSt;
  	  			}
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,userId,walletId, "", "", "" 
						+  "savePGTrx()"+"|problem in Add  PG Txn"+e.getMessage()+" "+e);
		    	//logger.debug("problem in Add  PG Txn=========================" + e.getMessage(), e);
		    	status="7000";
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,userId,walletId, "", "", "" 
					+  "savePGTrx()"+"|status"+status);
		   // logger.info(" excution ===================== status"+status);
		    return status;
		}
		
		/**
		 * for wallet ID by user mobile /email
		 * @return walletId
		 */
		public AEPSLedger getAepsLedger(AEPSLedger aepsLedger,String ipimei,String userAgent){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
					+ "saveAepsRequest()");

		aepsLedger.setStatus("1001");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	
	    	//Timestamp timestamp=new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));

	    	Criteria criteria=session.createCriteria(AEPSConfig.class);
	    	criteria.add(Restrictions.eq("aggregatorId",aepsLedger.getAggregator()));
	    	criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
	    	criteria.add(Restrictions.eq("type", aepsLedger.getType()));
	    	
	    	List<AEPSConfig> aepsConfigList=criteria.list();
	    	if(aepsConfigList!=null&&aepsConfigList.size()>0)
	    		aepsLedger.setAepsConfig(aepsConfigList.get(0));
	    		aepsLedger.setStatus("1000");
	    		aepsLedger.setTxnDate(null);
	  				return aepsLedger;
	    }catch(Exception e){
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
					+  "saveAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);

	    	aepsLedger.setStatus("7000");
	   		  e.printStackTrace();
	   			 transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
	    return aepsLedger;
	  }
		
		
		  public AEPSLedger saveAepsRequest(AEPSLedger aepsLedger,String ipimei,String userAgent){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+ "saveAepsRequest()   --- "+aepsLedger);
	
			aepsLedger.setStatus("1001");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	
		    	Timestamp timestamp=new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));

		    	aepsLedger.setTxnDate(timestamp);
		    	aepsLedger.setStatus("Pending");
		    	String aepstxnid=commanUtilDao.getTrxId("AEPS",aepsLedger.getAggregator());
		    	
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+ "saveAepsRequest()  --- "+aepsLedger);
		    	
		    	aepsLedger.setTxnId(aepstxnid);
		    	session.save(aepsLedger);
		    	transaction.commit();
		    	
		    	try {
		    	Criteria criteria=session.createCriteria(AEPSConfig.class);
		    	criteria.add(Restrictions.eq("aggregatorId",aepsLedger.getAggregator()));
		    	criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
		    	criteria.add(Restrictions.eq("type", aepsLedger.getType()!=null ? aepsLedger.getType():"2"));
		    	
		    	List<AEPSConfig> aepsConfigList=criteria.list();
		    	if(aepsConfigList!=null&&aepsConfigList.size()>0)
		    		aepsLedger.setAepsConfig(aepsConfigList.get(0));
		    	
		    	}catch (Exception e) {
		    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
							+  "saveAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);
				}
		    		aepsLedger.setStatus("1000");
		    		aepsLedger.setTxnDate(null);
  	  				return aepsLedger;
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+  "saveAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);

		    	aepsLedger.setStatus("7000");
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return aepsLedger;
		  }
		 
		
		  public AEPSLedger saveAepsRequestApi(AEPSLedger aepsLedger,String ipimei,String userAgent){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+ "saveAepsRequest()   --- "+aepsLedger);
	
			aepsLedger.setStatus("1001");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	
		    	Timestamp timestamp=new Timestamp(Long.parseLong(aepsLedger.getPtyTransDt()));

		    	aepsLedger.setTxnDate(timestamp);
		    	aepsLedger.setStatus("Pending");
		    	String aepstxnid=commanUtilDao.getTrxId("AEPSAPI",aepsLedger.getAggregator());
		    	aepsLedger.setTxnId(aepstxnid);
		    	
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", " aepstxnid "+aepstxnid, "" 
						+ "saveAepsRequest()  --- "+aepsLedger);
		    	
		    	
		    	session.save(aepsLedger);
		    	transaction.commit();
		    	
		    	try {
		    	Criteria criteria=session.createCriteria(AEPSConfig.class);
		    	criteria.add(Restrictions.eq("aggregatorId",aepsLedger.getAggregator()));
		    	criteria.add(Restrictions.eq("aepsChannel", aepsLedger.getAepsChannel()));
		    	criteria.add(Restrictions.eq("type", aepsLedger.getType()!=null ? aepsLedger.getType():"2"));
		    	
		    	List<AEPSConfig> aepsConfigList=criteria.list();
		    	if(aepsConfigList!=null&&aepsConfigList.size()>0)
		    		aepsLedger.setAepsConfig(aepsConfigList.get(0));
		    	
		    	}catch (Exception e) {
		    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
							+  "saveAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);
				}
		    		aepsLedger.setStatus("1000");
		    		aepsLedger.setTxnDate(null);
	  				return aepsLedger;
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+  "saveAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);

		    	aepsLedger.setStatus("7000");
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return aepsLedger;
		  }

		  
		  
		  
		  public MATMLedger saveMatmRequest(MATMLedger matmLedger,String ipimei,String userAgent){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),matmLedger.getAggregator(),matmLedger.getAgentId(),matmLedger.getWalletId(), "", "", "" 
						+ "saveMatmRequest()");
	
			matmLedger.setStatus("1001");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	Timestamp timestamp;
		    	try {
		    		timestamp=new Timestamp(Long.parseLong(matmLedger.getPtyTransDt()));
		    	}catch (Exception e) {
					timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
				}
//		    	String velocityCheckSt=velocityCheck(walletId,trxAmount,1);
//	  			if(velocityCheckSt.equals("1000")){
		    	matmLedger.setTxnDate(timestamp);
		    	matmLedger.setStatus("Pending");
		     	PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();
		    	String matmtxnid=commanUtilDao.getTrxId("MATM",matmLedger.getAggregator());
		    	matmLedger.setTxnId(matmtxnid);
		    	session.save(matmLedger);
		    	transaction.commit();
		    	String type=null;
		    	if(matmLedger != null && matmLedger.getType() != null && "STATUS ENQUIRY".equalsIgnoreCase(matmLedger.getType())) {
		    		type = "MATM STATUS ENQUIRY";
		    	}else {
		    		type = "MATM";
		    	}
		    	Criteria criteria=session.createCriteria(AEPSConfig.class);
		    	criteria.add(Restrictions.eq("aggregatorId",matmLedger.getAggregator()));
		    	criteria.add(Restrictions.eq("aepsChannel", matmLedger.getAepsChannel()));
		    	criteria.add(Restrictions.eq("type",type));
		    	
		    	List<AEPSConfig> matmConfigList=criteria.list();
		    	if(matmConfigList!=null&&matmConfigList.size()>0)
		    		matmLedger.setAepsConfig(matmConfigList.get(0));
		    	matmLedger.setStatus("1000");
		    	matmLedger.setTxnDate(null);
	  			return matmLedger;
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),matmLedger.getAggregator(),matmLedger.getAgentId(),matmLedger.getWalletId(), "", "", "" 
						+  "saveMatmRequest()"+"|problem in Add  Matm Txn"+e.getMessage()+" "+e);
		    	//logger.debug("problem in Add  PG Txn=========================" + e.getMessage(), e);
		    	matmLedger.setStatus("7000");
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return matmLedger;
		  }
		  
		  
		  public MATMLedger updateMatmRequest(MATMLedger matmLedger,String ipimei,String userAgent){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),matmLedger.getAggregator(),matmLedger.getAgentId(),matmLedger.getWalletId(), "", "", "" 
						+ "updateMatmRequest()");
	
				matmLedger.setStatus("1001");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	
		    	MATMLedger aeps=(MATMLedger)session.get(MATMLedger.class, matmLedger.getTxnId());
		    	aeps.setStatus(matmLedger.getStatus());
		    	aeps.setStatusCode(matmLedger.getAgentCode());
		    	aeps.setAmount(matmLedger.getAmount());
		    	session.saveOrUpdate(aeps);
		    	transaction.commit();
		   
		    	matmLedger.setStatus("1000");
		    	
	  			return matmLedger;
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),matmLedger.getAggregator(),matmLedger.getAgentId(),matmLedger.getWalletId(), "", "", "" 
						+  "updateMatmRequest()"+"|problem in Add  MATM Txn"+e.getMessage()+" "+e);
		    	//logger.debug("problem in Add  PG Txn=========================" + e.getMessage(), e);
		    	matmLedger.setStatus("7000");
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return matmLedger;
		  }
		  
		  
		  
		  public AEPSLedger updateAepsRequest(AEPSLedger aepsLedger,String ipimei,String userAgent){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+ "updateAepsRequest()");
	
			aepsLedger.setStatus("1001");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    try{
		    	
		    	AEPSLedger aeps=(AEPSLedger)session.get(AEPSLedger.class, aepsLedger.getTxnId());
		    	aeps.setStatus(aepsLedger.getStatus());
		    	aeps.setStatusCode(aepsLedger.getAgentCode());
		    	aeps.setAmount(aepsLedger.getAmount());
		    	session.saveOrUpdate(aeps);
		    	transaction.commit();
		   
		    	aepsLedger.setStatus("1000");
		    	
  	  			return aepsLedger;
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aepsLedger.getAggregator(),aepsLedger.getAgentId(),aepsLedger.getWalletId(), "", "", "" 
						+  "updateAepsRequest()"+"|problem in Add  aeps Txn"+e.getMessage()+" "+e);
		    	//logger.debug("problem in Add  PG Txn=========================" + e.getMessage(), e);
		    	aepsLedger.setStatus("7000");
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return aepsLedger;
		  }
		
	 
	/**
	 * 
	 * @param txnId
	 * @return
	 */
		
		public UserProfileBean profilebytxnId(String txnId){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
						+ "profilebytxnId()");
			//logger.info("start excution ===================== method profilebytxnId(txnId)"+txnId);
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
			PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();
			WalletSecurityBean wsBean=new WalletSecurityBean();
			try{
		   	paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		   
			}catch(Exception e){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
						+ "profilebytxnId()"+"problem occure while excution"+e.getMessage()+" "+e);
				//logger.info("problem occure while excution ===================== method profilebytxnId(txnId)"+txnId);
				e.printStackTrace();
			}
			
			WalletUserDao walletUserDao=new WalletUserDaoImpl();
		   	UserProfileBean upb=walletUserDao.showUserProfilebymobile(paymentGatewayTrxBean.getMobileNo(),paymentGatewayTrxBean.getAggreatorid());
		   	try{
		   		wsBean=(WalletSecurityBean)session.get(WalletSecurityBean.class, upb.getId());
		   	}catch(Exception e){
		   		e.printStackTrace();
		   	}finally {
				session.close();
			}
		   	if(upb!=null&&wsBean!=null){
		   	upb.setToken(wsBean.getToken());
		   	upb.setTokenSecurityKey(wsBean.getTokenSecurityKey());
		   	}
		   	return upb;
		}
		
		
		public PGPayeeBean profilebyPGtxnId(String txnId){

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
					+ "profilebyPGtxnId()");
			//logger.info("start excution ===================== method profilebytxnId(txnId)"+txnId);
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
			Transaction transaction=session.beginTransaction();
			PGPayeeBean gGPayeeBean=null;
			try{
				SQLQuery  query=session.createSQLQuery("SELECT a.txnid AS txnid,txnamount,walletid,mobileno,paytxnid,merchantid,merchantuser,amount,merchanttxnid,callbackurl,a.aggreatorid AS aggreatorid FROM paymentgatewaytxn a ,merchanttxnmast b WHERE a.paytxnid=b.txnid and a.txnid=:txn");
				query.setString("txn", txnId);
				query.setResultTransformer(Transformers.aliasToBean(PGPayeeBean.class));
				gGPayeeBean=(PGPayeeBean)query.list().get(0);
			 }catch(Exception e){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
							+ "profilebyPGtxnId()"+"|problem in profilebyPGtxnId Txn"+e.getMessage()+" "+e);
				// logger.debug("problem in profilebyPGtxnId Txn=========================" + e.getMessage(), e);
		    	
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		   	return gGPayeeBean;
		  	}
		
		
		
		 /**
		  * 
		  * @param txnId
		  * @param pgtxn
		  * @param trxAmount
		  * @param status
		  *
		  */
		/*public String updatePGTrx(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
					+ "updatePGTrx()");
			//logger.info("start excution ===================== method updatePGTrx(userId,txnId,pgtxn,trxAmount,status,ipImei)");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    Boolean flag=false;
		    String txnStatus="1001";
		    try{
		    	PaymentGatewayTxnBean paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		    	WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
		    	responseparam=responseparam+"|"+walletConfiguration.getPgEncKey();
		    	
		    	String newHash=CommanUtil.SHAHashing256(responseparam);
		    	if(newHash.equals(hash)){
		    	
		    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
							+ "updatePGTrx()"+"|"+paymentGatewayTrxBean.getTrxAmount());
		    	//System.out.println("~~~~~~~~~~~"+paymentGatewayTrxBean.getTrxAmount());
		   	 	if(trxAmount==paymentGatewayTrxBean.getTrxAmount()){
		   	 	paymentGatewayTrxBean.setPgId(pgtxn);
		   	 	if (status.contains("success")){
					 paymentGatewayTrxBean.setTrxResult("Success");
					 flag=true;
				
		   	 	}else{
					 paymentGatewayTrxBean.setTrxResult("Fail");
					 txnStatus="7029";
				 }}else{
					 txnStatus="7029";
				 }
				session.saveOrUpdate(paymentGatewayTrxBean);
				transaction.commit();
				if(flag)
					txnStatus=new TransactionDaoImpl().addMoney(userId,paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid(),paymentGatewayTrxBean.getUseragent());
				}
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
						+"updatePGTrx()"+"|problem in updatePGTrx"+ e.getMessage()+" "+e);
		    	//logger.debug("problem in updatePGTrx=======================" + e.getMessage(), e);
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return txnStatus;
		    }*/
		
		
public String updatePGTrx(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
					+ "updatePGTrx()");
			//logger.info("start excution ===================== method updatePGTrx(userId,txnId,pgtxn,trxAmount,status,ipImei)");
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    Boolean flag=false;
		    String txnStatus="1001";
		    try{
		    	PaymentGatewayTxnBean paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		    	WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
		    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
							+ "updatePGTrx()"+"|"+paymentGatewayTrxBean.getTrxAmount());
		   	 	paymentGatewayTrxBean.setPgId(pgtxn);
		   	 	if (status.contains("success")){
					 paymentGatewayTrxBean.setTrxResult("Success");
					 flag=true;
				
		   	 	}else{
					 paymentGatewayTrxBean.setTrxResult("Fail");
					 txnStatus="7029";
				 }
		   	 	
				session.saveOrUpdate(paymentGatewayTrxBean);
				transaction.commit();
				if(flag)
					txnStatus=new TransactionDaoImpl().addMoney(userId,paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid(),paymentGatewayTrxBean.getUseragent());
//				}
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
						+"updatePGTrx()"+"|problem in updatePGTrx"+ e.getMessage()+" "+e);
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return txnStatus;
		    }
	
  
public String updatePGTrxTest(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei,String mode,String card){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId+ "updatePGTrx()"+" Payment mode  "+mode+"  card "+card);
	//logger.info("start excution ===================== method updatePGTrx(userId,txnId,pgtxn,trxAmount,status,ipImei)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
    Transaction transaction = session.beginTransaction();
    Boolean flag=false;
    String txnStatus="1001";
    try{
    	PaymentGatewayTxnBean paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
    	WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
					+ "updatePGTrx()"+"|"+paymentGatewayTrxBean.getTrxAmount());
   	 	paymentGatewayTrxBean.setPgId(pgtxn);
   	    paymentGatewayTrxBean.setPaymentMode(mode);
   	    paymentGatewayTrxBean.setCardMask(card);
   	 	if (status.contains("success")){
			 paymentGatewayTrxBean.setTrxResult("Success");
			 flag=true;
		   	
			 txnStatus=new TransactionDaoImpl().addMoney(userId,paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid(),paymentGatewayTrxBean.getUseragent());
			 if(txnStatus.contains("1000"))
				paymentGatewayTrxBean.setCreditStatus("CREDIT");
			 
   	 	}else{
			 paymentGatewayTrxBean.setTrxResult("Fail");
			 txnStatus="7029";
		 }

		session.saveOrUpdate(paymentGatewayTrxBean);
		transaction.commit();
		/*
		if(flag)
			txnStatus=new TransactionDaoImpl().addMoney(userId,paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid(),paymentGatewayTrxBean.getUseragent());
       */
		//		}
    }catch(Exception e){
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",txnId
				+"updatePGTrx()"+"|problem in updatePGTrx"+ e.getMessage()+" "+e);
   		  e.printStackTrace();
   			 transaction.rollback();
   	} finally {
   				session.close();
   	}
    return txnStatus;
    }


  public String savePgReturnResponse(PgReturnResponse res){
	
   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",res.getAgentId(),res.getPaymentId(), res.getPgPaymentType(),res.getPgOrderId() ,"savePgReturnResponse()");
	//logger.info("start Excute==============================================saveDispute(userId,walletId,txnId,disputeReason) method");
	String status="1001";
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	try {
		String objGson = new Gson().toJson(res);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",res.getAgentId(),""," ",res.getPgPaymentType() ,"String pGReturnResponse "+objGson);
		 session.save(res);
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",res.getAgentId(),res.getPgOrderId()," ",res.getPgPaymentType() ,"save pGReturnResponse ");
		 transaction.commit(); 
		 status="1000";
		 
	} catch (Exception e) {
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",res.getAgentId(),"", "",res.getPgOrderId() ,"Problem in save pgReturnResponse"+e.getMessage()+" "+e);
		logger.debug("Problem in save pgReturnResponse============================================== " + e.getMessage(), e);
		status="7000";
	} finally {
		session.close();
	}
	
	return status;
	
   }


		
		public String updatePGTrx(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status){
	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
					+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn);
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    Boolean flag=false;
		    String txnStatus="1001|FAIL|"+txnId;
		    PaymentGatewayTxnBean paymentGatewayTrxBean=null;
		    try{
		    	paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		    	WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
		    	responseparam=responseparam+"|"+walletConfiguration.getPgEncKey();
		    	
		    	String newHash=CommanUtil.SHAHashing256(responseparam);
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn+"|"+paymentGatewayTrxBean.getTrxAmount());

			   	 	paymentGatewayTrxBean.setPgId(pgtxn);
			   	 	if (status.contains("Success") ||status.contains("Captured")){
						 paymentGatewayTrxBean.setTrxResult("Success");
						 flag=true;
					
			   	 	}else{
						 paymentGatewayTrxBean.setTrxResult("Fail");
						 txnStatus="7029|FAIL|"+txnId;
					 }
					
			   	 	session.update(paymentGatewayTrxBean);
					transaction.commit();
					if(flag)
						txnStatus=new TransactionDaoImpl().addMoneybyApp(paymentGatewayTrxBean.getUserId(),paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid());
//					}
		    	
		    
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
						+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn+"|problem in updatePGTrx"+e.getMessage()+" "+e);
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return txnStatus+"|"+paymentGatewayTrxBean.getTrxAmount()+"|"+paymentGatewayTrxBean.getPgId();
		    }
		

		public String updatePGTrx(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status,String mode,String card){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn+"  | MODE  "+mode);
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		    Boolean flag=false;
		    String txnStatus="1001|FAIL|"+txnId;
		    PaymentGatewayTxnBean paymentGatewayTrxBean=null;
		    try{
		    	paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		    	WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
		    	responseparam=responseparam+"|"+walletConfiguration.getPgEncKey();
		    	
		    	String newHash=CommanUtil.SHAHashing256(responseparam);
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn+"|"+paymentGatewayTrxBean.getTrxAmount());

			   	 	paymentGatewayTrxBean.setPgId(pgtxn);
			   	    paymentGatewayTrxBean.setPaymentMode(mode);
			   	    paymentGatewayTrxBean.setCardMask(card);
			   	 	
			   	    if (status.contains("Success") ||status.contains("Captured")){
						 paymentGatewayTrxBean.setTrxResult("Success");
						 flag=true;
						
						 txnStatus=new TransactionDaoImpl().addMoneybyApp(paymentGatewayTrxBean.getUserId(),paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid());
						if(txnStatus.contains("SUCCESS") || txnStatus.contains("1000"))
							paymentGatewayTrxBean.setCreditStatus("CREDIT");
						 
			   	 	}else{
						 paymentGatewayTrxBean.setTrxResult("Fail");
						 txnStatus="7029|FAIL|"+txnId;
					 }
			   	 
			   	 	
			   	 	session.update(paymentGatewayTrxBean);
					transaction.commit();
					/*
					if(flag)
						txnStatus=new TransactionDaoImpl().addMoneybyApp(paymentGatewayTrxBean.getUserId(),paymentGatewayTrxBean.getWalletId(),paymentGatewayTrxBean.getTrxAmount(),txnId,paymentGatewayTrxBean.getImeiIP(),paymentGatewayTrxBean.getAggreatorid());
//					}
		    	*/
		    
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",txnId
						+ "updatePGTrx()"+"|status"+status+"|pgtxn :"+pgtxn+"|problem in updatePGTrx"+e.getMessage()+" "+e);
		   		  e.printStackTrace();
		   			 transaction.rollback();
		   	} finally {
		   				session.close();
		   	}
		    return txnStatus+"|"+paymentGatewayTrxBean.getTrxAmount()+"|"+paymentGatewayTrxBean.getPgId();
		    }

		
		
	
		/**
		 * 
		 * @param walletId
		 * @param amount
		 * @param pgTxnId
		 * @param ipImei
		 * @return
		 */
		
		private String addMoneybyApp(String userId,String walletId,double amount,String pgTxnId,String ipImei,String aggreatorid){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|pgTxnId :"+pgTxnId);
			//logger.info("start excution ===================== method addMoney(userId,walletId, amount, pgTxnId, ipImei)");
			String status="1001|FAIL|"+pgTxnId;
			TransactionBean transactionBean=new TransactionBean();
			PaymentGatewayTxnBean paymentGatewayTxnBean=new PaymentGatewayTxnBean();
			factory=DBUtil.getSessionFactory();
	   	 	Session session = factory.openSession();
	  	    Transaction transaction = session.beginTransaction();
	  	  try{
	  		  
	  		Query query=session.createQuery( "from PaymentGatewayTxnBean where txnid=:pgTxnId  and txnotherdetails IS NULL");
	  		//Query query=session.createQuery( "from PaymentGatewayTxnBean where txnid=:pgTxnId and txnresult='Success' and txnotherdetails IS NULL");
			query.setString("pgTxnId", pgTxnId);
			 List <PaymentGatewayTxnBean> list = query.list();
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "","addMoneybyApp()"+"|list  :"+list.size());
			 //logger.info("===============================list=======44======================"+list.size());
			 if (list != null && list.size() != 0) {
				 paymentGatewayTxnBean=list.get(0);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|result  :"+paymentGatewayTxnBean.getTrxResult());
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|amoun : "+paymentGatewayTxnBean.getTrxAmount());
				// logger.info("===============================result=======44======================"+paymentGatewayTxnBean.getTrxResult());
				// logger.info("===============================amount=======44======================"+paymentGatewayTxnBean.getTrxAmount());
				 if(amount==paymentGatewayTxnBean.getTrxAmount()){
					//logger.info("============================================================"+request.getHeader("User-Agent"));
					String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
					transactionBean.setTxnid(txnId);
					transactionBean.setUserid(userId);
					transactionBean.setWalletid(walletId);
					transactionBean.setTxncode(92);
					transactionBean.setTxncredit(amount);
					transactionBean.setTxndebit(0);
					transactionBean.setPayeedtl(userId);
					transactionBean.setResult("Success");
					transactionBean.setAggreatorid(aggreatorid);
					transactionBean.setResptxnid(pgTxnId);
					if(ipImei==null){
						 transactionBean.setWalletuserip("");
						  }else{
							  transactionBean.setWalletuserip(ipImei);
					}
				    // transactionBean.setWalletuseragent(request.getHeader("User-Agent"));
					transactionBean.setWalletuseragent("");
					session.save(transactionBean);
					paymentGatewayTxnBean.setTrxOtherDetails("2");
			     	session.update(paymentGatewayTxnBean);
			     	transaction.commit();
			     	status="1000|SUCCESS|"+txnId;
			     	walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			     	walletMastBean=walletUserDao.showUserProfile(userId);
			     	/*
			     	try {
			       	String smstemplet = commanUtilDao.getsmsTemplet("addMoney",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<AMOUNT>>", String.format("%.2f",amount)).replace("<<TXNNO>>", txnId).replace("<<APPNAME>>", walletConfiguration.getAppname());
			     	
			       	//Dear <<NAME>>, you have added Rs. <<AMOUNT>> to your <<APPNAME>> wallet. Transaction ID is <<TXNNO>>.
			        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "","addMoneybyApp()"+"|Add Money SMS  :"+smstemplet);
			     	String mailSub= "Rs. "+String.format("%.2f",amount)+" added successfully.";
			     	String mailContent= commanUtilDao.getmailTemplet("addMoney",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", String.format("%.2f",amount)).replace("<<servicedetail>>", "Online Gateway").replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
			     	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|Add Money MAIL  :"+mailContent);
			       	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",aggreatorid,walletId,walletMastBean.getName(),"NOTP");
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			     	}catch (Exception e) {
						e.printStackTrace();
					}
			     	*/
				 }else{
					 status="7015|FAIL|"+pgTxnId;
				 }
			 }else{
				 status="7015|FAIL|"+pgTxnId;
			 }
	  		  
	  	  }catch(Exception e){
	  		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|problem in Add  monet to wallet  "+ e.getMessage()+" "+e);
			//logger.debug("problem in Add  monet to wallet" + e.getMessage(), e);
			transaction.rollback();
			return "7000|FAIL|"+pgTxnId;
	  	  } finally {
			session.close();
		}
	  	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoneybyApp()"+"|Inside addMoney return Status "+ status);
	  	  //logger.info("Inside ====================addMoney=== return===="+status);
		return status;
			
		}
		
		
	
	/**
	 * 
	 * @param walletId
	 * @param amount
	 * @param pgTxnId
	 * @param ipImei
	 * @return
	 */
	
	private String addMoney(String userId,String walletId,double amount,String pgTxnId,String ipImei,String aggreatorid,String Agent){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "","addMoney()");
		//logger.info("start excution ===================== method addMoney(userId,walletId, amount, pgTxnId, ipImei)");
		String status="1001";
		TransactionBean transactionBean=new TransactionBean();
		PaymentGatewayTxnBean paymentGatewayTxnBean=new PaymentGatewayTxnBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	  try{
  		  
  		Query query=session.createQuery( "from PaymentGatewayTxnBean where txnid=:pgTxnId  and txnotherdetails IS NULL");
  		//Query query=session.createQuery( "from PaymentGatewayTxnBean where txnid=:pgTxnId and txnresult='Success' and txnotherdetails IS NULL");
		query.setString("pgTxnId", pgTxnId);
		 List <PaymentGatewayTxnBean> list = query.list();
		
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "","addMoney()"+"|list "+ list.size());
		 //logger.info("===============================list=======44======================"+list.size());
		 if (list != null && list.size() != 0) {
			 paymentGatewayTxnBean=list.get(0);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoney()"+"|result "+ paymentGatewayTxnBean.getTrxResult());
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoney()"+"|amount "+ paymentGatewayTxnBean.getTrxAmount());
			// logger.info("===============================result=======44======================"+paymentGatewayTxnBean.getTrxResult());
			// logger.info("===============================amount=======44======================"+paymentGatewayTxnBean.getTrxAmount());
			 if(amount==paymentGatewayTxnBean.getTrxAmount()){
				//logger.info("============================================================"+request.getHeader("User-Agent"));
				String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
				transactionBean.setTxnid(txnId);
				transactionBean.setUserid(userId);
				transactionBean.setWalletid(walletId);
				transactionBean.setTxncode(92);
				transactionBean.setTxncredit(amount);
				transactionBean.setTxndebit(0);
				transactionBean.setPayeedtl("1");
				transactionBean.setResult("Success");
				transactionBean.setResptxnid(pgTxnId);
				transactionBean.setAggreatorid(aggreatorid);
				transactionBean.setWalletuserip(ipImei);
				transactionBean.setWalletuseragent(Agent);
				session.save(transactionBean);
				paymentGatewayTxnBean.setTrxOtherDetails("2");
		     	session.update(paymentGatewayTxnBean);
		     	transaction.commit();
		     	status="1000";
		     	walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
		     	walletMastBean=walletUserDao.showUserProfile(userId);
		       	//String smstemplet = commanUtilDao.getsmsTemplet("addMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<TXNNO>>", txnId);
		     	
		     	
		     	/*String smstemplet = commanUtilDao.getsmsTemplet("addMoney",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<TXNNO>>", txnId).replace("<<APPNAME>>", walletConfiguration.getAppname());
		     	
		     	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "walletToMerchent()"+"|Add Money SMS "+ smstemplet);
		       //	logger.info("~~~~~~~~~~~~~~~~~~~~~Add Money SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
		     	String mailSub= "Rs. "+String.format("%.2f",amount)+" added successfully.";
		     //	String mailContent= commanUtilDao.getmailTemplet("addMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<TXNNO>>", txnId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
		     	String mailContent= commanUtilDao.getmailTemplet("addMoney",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", String.format("%.2f",amount)).replace("<<TXNNO>>", txnId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
		     	
		     	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "walletToMerchent()"+"|Add Money MAIL "+ mailContent);
		     	//logger.info("~~~~~~~~~~~~~~~~~~~~~Add Money MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
		       	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",aggreatorid,walletId,walletMastBean.getName(),"NOTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);*/
		     	
			 }else{
				 status="7015";
			 }
		 }else{
			 status="7015";
		 }
  		  
  	  }catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoney()"+"|problem in Add  monet to walle "+  e.getMessage()+" "+e);
		//logger.debug("problem in Add  monet to wallet" + e.getMessage(), e);
		transaction.rollback();
		return "7000";
  	  } finally {
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "", "addMoney()"+"|Inside addMoney return "+ status);
	//logger.info("Inside ====================addMoney=== return===="+status);
	return status;
		
	}
			
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param accNumber
	 * @param trnsTxnId
	 * @param ipImei
	 * @return
	 */
	
	public String walletToMerchent(String userId,String payeeWalletid,double amount,String mid,String trnsTxnId,String ipImei,String aggreatorid,String agent){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToMerchent()"+"|AgentId: "+ agent);
		//logger.info("start excution ===================== method walletToMerchent(userId,payeeWalletid,amount,mid,trnsTxnId,ipImei)");
		String status="Fail|1001";
		TransactionBean transactionBean=new TransactionBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	  try{
			 if(amount<=0){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "", "Txn Amount Not Be Zero");
				 //logger.info("==========================Txn Amount Not Be Zero==================================");
		       	return "Fail|7032";
			  }
			 
			
				SQLQuery insertQuery = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
				insertQuery.setParameter(0, payeeWalletid);
				insertQuery.executeUpdate();
				transaction.commit();
				 transaction = session.beginTransaction();
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToMerchent()"+"|"+ payeeWalletid+"getWalletBalance()"+getWalletBalance(payeeWalletid));
			// logger.info(payeeWalletid+"start excution ===================== method getWalletBalance()"+	getWalletBalance(payeeWalletid));
			 if(getWalletBalance(payeeWalletid)>=amount){
					String velocityCheckSt=velocityCheck(payeeWalletid,amount,7);
	  	  			if(velocityCheckSt.equals("1000")){
	  	  			String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);
	  	  			transactionBean.setTxnid(mpfwTxnId);
	  	  			transactionBean.setUserid(userId);
	  	  			transactionBean.setWalletid(payeeWalletid);
	  	  			transactionBean.setTxncode(7);
	  	  			transactionBean.setTxncredit(0.0);
	  	  			transactionBean.setTxndebit(amount);
	  	  			transactionBean.setPayeedtl(mid);
	  	  			transactionBean.setResult("Success");
	  	  			transactionBean.setResptxnid(trnsTxnId);
	  	  			transactionBean.setAggreatorid(aggreatorid);
	  	  		    if(ipImei==null){
	  	  			 transactionBean.setWalletuserip(ipImei);
					}else{
					   transactionBean.setWalletuserip(ipImei);
					}
	  	  		  	transactionBean.setWalletuseragent(agent);
	  	  		   	session.save(transactionBean);
	  	  		transaction.commit();
	  	  	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","Wallet Payment transactin commit sucesfully");   
	  	  		//logger.info("================Wallet Payment transactin commit sucesfully================"); 
	  	  		   	status= "Success|"+mpfwTxnId;
	  	  		   	
	  	  		/*   	transaction=session.beginTransaction();
	  	  		 walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			     walletMastBean=walletUserDao.showUserProfile(userId);
				 String smstemplet = commanUtilDao.getsmsTemplet("sendMoney").replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", mid).replace("<<TXNNO>>", mpfwTxnId);
			    logger.info("~~~~~~~~~~~~~~~~~~~~~wallet To Merchent Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
			    String mailSub= "Rs. "+Double.toString(amount)+" Send successfully to "+mid+".";
			    String mailContent= commanUtilDao.getmailTemplet("sendMoney").replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", mid).replace("<<TXNNO>>", mpfwTxnId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
			    logger.info("~~~~~~~~~~~~~~~~~~~~~wallet To Merchent Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
			    SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",aggreatorid,payeeWalletid,walletMastBean.getName());
				Thread t = new Thread(smsAndMailUtility);
				t.start();*/
	  	  		   	
	  	  		   	
	  	  		   	
	 			}else{
	  	  			status= "Fail|"+velocityCheckSt;
	  	  			}			
					}else{
	  				status= "Fail|7024";
	  			}
			}catch(Exception e){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToMerchent()"+"problem in paymentFromWallet"+e.getMessage()+" "+e);
				logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
				transaction.rollback();
				return "Fail|7000";
		  	  } finally {
		  		transaction=session.beginTransaction();
				SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+payeeWalletid+"'");
				delete.executeUpdate();
				transaction.commit();
				session.close();
			}
  	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","Inside paymentFromWallet return"+status);	
  	 // logger.info("Inside ====================paymentFromWallet=== return===="+status);
		return status;
	}
	
	
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param rechageNumber
	 * @param rechargeTxnId
	 * @param ipImei
	 * @return
	 */
	public String reCharge(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent){
			return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,5,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
	}
	
	public String reChargeByTxnCode(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent,int txncode){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,txncode,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
	}
	
	public String wlWalletDebitOnRecharge(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,93,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
	}

	public String maintenanceChargeDeduction(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){
			return new TransactionDaoImpl().walletToAggregatorMoneyTransfer(76,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent,0.0,0.0);
}
	
	public String reChargeFromCashBack(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent){
		return paymentFromCashBackWallet(userId,payeeWalletid,5,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
}

	
	
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param rechageNumber
	 * @param rechargeTxnId
	 * @param ipImei
	 * @return
	 */
	public String billPayment(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent,int txncode){
			return paymentFromWallet(userId,payeeWalletid,txncode,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
	}
	
	
	public String billPaymentFromCashBack(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent,int txncode){
		 return paymentFromCashBackWallet(userId,payeeWalletid,txncode,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
}
	
	
	
	
	
	
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param accNumber
	 * @param trnsTxnId
	 * @param ipImei
	 * @return
	 */
	public String walletToAccount(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,4,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge);
	}
	
	public String walletToAccountVerify(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,52,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge);
	}
	
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param accNumber
	 * @param trnsTxnId
	 * @param ipImei
	 * @return
	 */
	public String walletToAccountSurcharge(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,19,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge);
	}
	
	public String walletToAccountSurchargeDist(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distAmt,String distributerId,double supDistSurcharge,String supDistId,double agentRefund, double dtdsApply){
		return new TransactionDaoImpl().paymentFromWalletDist(userId,payeeWalletid,19,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge,distAmt,distributerId,supDistSurcharge,supDistId,agentRefund,dtdsApply);
	}
	
	public String walletToAccountSurchargeVerify(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,53,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge);
	}
	
	public String walletToAccountSurchargeVerifyDist(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distAmt,String distributerId,double supDistSurcharge,String supDistId){
		return new TransactionDaoImpl().paymentFromWalletDist(userId,payeeWalletid,53,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge,distAmt,distributerId,supDistSurcharge,supDistId,0,0);
	}
	
	public String walletToAccountSurchargeVerifyOnly(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distAmt,String distributerId,double supDistSurcharge,String supDistId,double agentRefund){
		return new TransactionDaoImpl().surchargePaymentFromWallet(userId,payeeWalletid,53,amount,accNumber,trnsTxnId,ipImei,aggreatorid,agent,surCharge,distAmt,distributerId,supDistSurcharge,supDistId,agentRefund);
	}
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param amount
	 * @param mercName
	 * @param trnsTxnId
	 * @param ipImei
	 * @return
	 */
	public String prePaidCardTxn(String userId,String payeeWalletid,double amount,String mercName,String trnsTxnId,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,11,amount,mercName,trnsTxnId,ipImei,aggreatorid,agent,0);
	}
	
	/**
	 * 
	 * @param userId
	 * @param payeeWalletid
	 * @param txnCode
	 * @param amount
	 * @param payeedtl
	 * @param reptxnId
	 * @param ipImei
	 * @param aggreatorid
	 * @param agent
	 * @return
	 */
	private String surchargePaymentFromWallet(String userId,String payeeWalletid,int txnCode,double amount,String payeedtl,String reptxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distSurchar,String distId,double supDistSurcharge,String supDistId,double agentRefund){

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist()"+"|"+agent);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist() amount"+amount);
		String status="1001";
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try{
			 if(amount<=0){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()","Txn Amount Not Be Zero");	
				return "7032";
		   		  }
			 else
			 {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()",payeeWalletid+"getWalletBalance()"+getWalletBalance(payeeWalletid));
			
			 String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);	
  	      
  	       if(txnCode==53){
  	           creditMoney(mpfwTxnId,aggreatorid,getWalletIdByUserId(aggreatorid,aggreatorid),(((amount-distSurchar)-supDistSurcharge)-agentRefund),getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,66);
  	           if(distSurchar != 0 && distId != null && !distId.trim().equals("")){
  	          creditMoney("D"+mpfwTxnId,distId,getWalletIdByUserId(distId,aggreatorid),distSurchar,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,67);
  	           }
  	         if(supDistSurcharge!=0&&(supDistId!=null&&!supDistId.trim().isEmpty()&&!supDistId.equalsIgnoreCase("-1"))){
  	  	          creditMoney("SD"+mpfwTxnId,supDistId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,36);
	            }
  	       if(agentRefund != 0 && userId != null && !userId.trim().equals("")){
   	          creditMoney("AG"+mpfwTxnId,userId,getWalletIdByUserId(userId,aggreatorid),agentRefund,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,85);
   	           }
  	             }
  	       else if(txnCode==19 ){
  	            creditMoney(mpfwTxnId,aggreatorid,getWalletIdByUserId(aggreatorid,aggreatorid),(((amount-distSurchar)-supDistSurcharge)-agentRefund),getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,33);
  	            if(distSurchar != 0 && distId != null && !distId.trim().equals(""))
  	            {
  	            	creditMoney("D"+mpfwTxnId,distId,getWalletIdByUserId(distId,aggreatorid),distSurchar,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,54);
  	            }
  	            if(supDistSurcharge!=0&&(supDistId!=null&&!supDistId.trim().isEmpty()&&!supDistId.equalsIgnoreCase("-1"))){
  	            	creditMoney("SD"+mpfwTxnId,supDistId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,73);
  	            }
  	          if(agentRefund != 0 && userId != null && !userId.trim().equals("")){
  	   	          creditMoney("AG"+mpfwTxnId,userId,getWalletIdByUserId(userId,aggreatorid),agentRefund,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,87);
  	   	           }
  	           }
  	       		return "1000";
			 }
  	
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","problem in paymentFromWallet" + e.getMessage()+" "+e);
			//logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
			transaction.rollback();
			return "7000";
	  	  } finally {
			session.close();
		}
		 
	}
	
	public String paymentFromWallet(String userId,String payeeWalletid,int txnCode,double amount,String payeedtl,String reptxnId,String ipImei,String aggreatorid,String agent,double surCharge){
	
			System.out.println("_____________________________Inside TransactionDaoimpl.java_______________________________");
		
			System.out.println("_______________________________Method__________________paymentFromWallet_________________");
			
			System.out.println("userid_____________"+userId);
			System.out.println("payeeWalletid______"+ payeeWalletid);
			System.out.println("txnCode____________"+txnCode);
			System.out.println("amount_____________"+amount);
			System.out.println("payeedtl___________"+payeedtl);
			System.out.println("reptxnId___________"+reptxnId);
			System.out.println("ipImei_____________"+ipImei);
			System.out.println("aggreatorid________"+aggreatorid);
			System.out.println("agent______________"+agent);
			System.out.println("surCharge__________"+surCharge);

		
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWallet()"+"Agent"+agent);
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWallet Amount :"+amount);
		 
		 
		String status="1001";
		String walletTxn=null;
		TransactionBean transactionBean=new TransactionBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try{
			 if(amount<=0){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWallet()","Txn Amount Not Be Zero");	
		         	return "7032";
		   		  }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWallet()",payeeWalletid+"getWalletBalance()"+getWalletBalance(payeeWalletid));

			if(getWalletBalance(payeeWalletid)>=amount){
				String velocityCheckSt=velocityCheck(payeeWalletid,amount,txnCode);
  	  			
				if(velocityCheckSt.equals("1000")){
  	  			String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);
  	  				
  	  			transactionBean.setTxnid(mpfwTxnId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(payeeWalletid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount);
  	  			transactionBean.setSurCharge(surCharge);
  	  			transactionBean.setPayeedtl(payeedtl);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(reptxnId);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setStatus(1);
  	  		    /*if(ipImei==null){
  	  			//   transactionBean.setWalletuserip(request.getRemoteAddr());
  	  		    transactionBean.setWalletuserip("");
				}else{
				   transactionBean.setWalletuserip(ipImei);
				}*/
  	  		   //	transactionBean.setWalletuseragent(request.getHeader("User-Agent"));
  	  			transactionBean.setWalletuseragent(agent);
  	  		   	session.save(transactionBean);
  	  		   	transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWallet()","Wallet Payment transactin commit sucesfully");
  	  		   //	logger.info("================Wallet Payment transactin commit sucesfully================"); 
  	  		   	status= "1000";
  	  		   	
  	  		   	
 	  		if(status.contains("1000") && txnCode==19  ){			     		
	     		creditMoney(mpfwTxnId,userId,getWalletIdByUserId(aggreatorid,aggreatorid),amount,getMobileNumber(payeeWalletid),mpfwTxnId, ipImei,aggreatorid,agent,33);
	     	}

  	  			}else{
  	  			status= velocityCheckSt;
  	  			}			
				}else{
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWallet|Insufficient fund walletId:"+payeeWalletid);
  				status= "7024";
  			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","problem in paymentFromWallet"+ e.getMessage()+" "+e);
			//logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
			transaction.rollback();
			return "7000";
	  	  } finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","Inside paymentFromWallet() return"+status);
		//logger.info("Inside ====================paymentFromWallet=== return===="+status);
		return status;
	}
	
	public String paymentFromWalletDist(String userId,String payeeWalletid,int txnCode,double amount,String payeedtl,String reptxnId,String ipImei,String aggreatorid,String agent,double surCharge){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist()"+"Inside paymentFromWallet return"+agent);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist() amount"+amount);
		//logger.info("start excution ===================== method paymentFromWallet(userId,payeeWalletid,txnCode,amount,paydtl,reptxnId,ipImei)"+userId);
		//logger.info("paymentFromWallet ===================== aount"+amount);
		String status="1001";
		String walletTxn=null;
		TransactionBean transactionBean=new TransactionBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try{
			 if(amount<=0){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()","Txn Amount Not Be Zero");	
				// logger.info("==========================Txn Amount Not Be Zero==================================");
		         	return "7032";
		   		  }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()","getWalletBalance()"+	getWalletBalance(payeeWalletid));
			 //logger.info(payeeWalletid+"start excution ===================== method getWalletBalance()"+	getWalletBalance(payeeWalletid));

			if(getWalletBalance(payeeWalletid)>=amount){
  	  			String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);
  	  				
  	  			transactionBean.setTxnid(mpfwTxnId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(payeeWalletid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount);
  	  			transactionBean.setSurCharge(surCharge);
  	  			transactionBean.setPayeedtl(payeedtl);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(reptxnId);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setStatus(1);
  	  		    /*if(ipImei==null){
  	  			//   transactionBean.setWalletuserip(request.getRemoteAddr());
  	  		    transactionBean.setWalletuserip("");
				}else{
				   transactionBean.setWalletuserip(ipImei);
				}*/
  	  		   //	transactionBean.setWalletuseragent(request.getHeader("User-Agent"));
  	  			transactionBean.setWalletuseragent(agent);
  	  		   	session.save(transactionBean);
  	  		   	transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","Wallet Payment transactin commit sucesfully");
  	  		   	//logger.info("================Wallet Payment transactin commit sucesfully================"); 
  	  		   	status= "1000";
  	  		   	
  	  			   

  	  			}else{
  				status= "7024";
  			}
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","problem in paymentFromWallet"+	e.getMessage()+" "+e);
			//logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
			transaction.rollback();
			return "7000";
	  	  } finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","Inside paymentFromWallet return"+status);
		//logger.info("Inside ====================paymentFromWallet=== return===="+status);
		return status;
	}
	

	
	private String paymentFromWalletDist(String userId,String payeeWalletid,int txnCode,double amount,String payeedtl,String reptxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distSurchar,String distId,double supDistSurcharge,String supDistId,double agentRefund, double dtdsApply){

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist()"+"|"+agent);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromWalletDist() amount"+amount);
		//logger.info("start excution ===================== method paymentFromWallet(userId,payeeWalletid,txnCode,amount,paydtl,reptxnId,ipImei)"+userId);
		//logger.info("paymentFromWallet ===================== aount"+amount);
		String status="1001";
		String walletTxn=null;
		TransactionBean transactionBean=new TransactionBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try{
			 if(amount<=0){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()","Txn Amount Not Be Zero");	
				// logger.info("==========================Txn Amount Not Be Zero==================================");
		         	return "7032";
		   		  }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromWalletDist()",payeeWalletid+"getWalletBalance()"+getWalletBalance(payeeWalletid));
			// logger.info(payeeWalletid+"start excution ===================== method getWalletBalance()"+	getWalletBalance(payeeWalletid));

			if(getWalletBalance(payeeWalletid)>=amount){
				String velocityCheckSt=velocityCheck(payeeWalletid,amount,txnCode);
  	  			if(velocityCheckSt.equals("1000")){
  	  			String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);
  	  				
  	  			transactionBean.setTxnid(mpfwTxnId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(payeeWalletid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount);
  	  			transactionBean.setSurCharge(surCharge);
  	  			transactionBean.setPayeedtl(payeedtl);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(reptxnId);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setStatus(1);
  	  			transactionBean.setWalletuseragent(agent);
	  		   	session.save(transactionBean);

					if (agentRefund > 0) {
						TransactionBean transactionRefund = new TransactionBean();
						mpfwTxnId = commanUtilDao.getTrxId("MPFW",aggreatorid);
						transactionRefund.setTxnid(mpfwTxnId);
						transactionRefund.setUserid(userId);
						transactionRefund.setWalletid(payeeWalletid);
						transactionRefund.setTxncode(78);
						transactionRefund.setTxncredit(agentRefund);
						transactionRefund.setTxndebit(0);
						transactionRefund.setSurCharge(surCharge);
						transactionRefund.setPayeedtl(payeedtl);
						transactionRefund.setResult("Success");
						transactionRefund.setResptxnid(reptxnId);
						transactionRefund.setAggreatorid(aggreatorid);
						transactionRefund.setWalletuserip(ipImei);
						transactionRefund.setStatus(1);
						transactionRefund.setWalletuseragent(agent);
						session.save(transactionRefund);
						
						if(agentRefund > 0 && dtdsApply > 0) {
							double tdsAmount = (agentRefund * dtdsApply)/100;
							if(tdsAmount > 0) {
								TransactionBean transactionRefundTDS = new TransactionBean();
								//mpfwTxnId = commanUtilDao.getTrxId("MPFW",aggreatorid);
								transactionRefundTDS.setTxnid("TC"+mpfwTxnId);
								transactionRefundTDS.setUserid(userId);
								transactionRefundTDS.setWalletid(payeeWalletid);
								transactionRefundTDS.setTxncode(95);
								transactionRefundTDS.setTxncredit(0);
								transactionRefundTDS.setTxndebit(tdsAmount);
								transactionRefundTDS.setSurCharge(surCharge);
								transactionRefundTDS.setPayeedtl(payeedtl);
								transactionRefundTDS.setResult("Success");
								transactionRefundTDS.setResptxnid(reptxnId);
								transactionRefundTDS.setAggreatorid(aggreatorid);
								transactionRefundTDS.setWalletuserip(ipImei);
								transactionRefundTDS.setStatus(1);
								transactionRefundTDS.setWalletuseragent(agent);
								session.save(transactionRefundTDS);
							}
						}
					}
  	  			
  	  			
  	  		   	transaction.commit();
  	  		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","Wallet Payment transactin commit sucesfully");
  	  		   	//logger.info("================Wallet Payment transactin commit sucesfully================"); 
  	  		   	status= "1000";
  	  		   	
  	  			   	
  	  		if(status.contains("1000") && txnCode==22  ){ 
  	          List<AirFareBean> airFareList  = new AirTravelDaoImpl().getFareQuoteByTripId( reptxnId, "");
  	           if(airFareList.get(0).getWalletTxn() != null && !airFareList.get(0).getWalletTxn().trim().equals(""))
  	           {
  	            walletTxn = airFareList.get(0).getWalletTxn()+","+mpfwTxnId;
  	           }
  	           else
  	           {
  	            walletTxn = mpfwTxnId;
  	           }
  	           int flag1=new AirTravelDaoImpl().updateFareQuote(-1,  -1,(airFareList.get(0).getWalletAmt()+amount),-1,-1, "","","","",walletTxn,reptxnId);
  	          }
  
  			   	
  	  		   	
  	  		   	
  	      if(status.contains("1000"))
					{
						if (txnCode == 53) {
							creditMoney(mpfwTxnId, userId, getWalletIdByUserId(aggreatorid, aggreatorid),
									amount - (distSurchar + supDistSurcharge), getMobileNumber(payeeWalletid), reptxnId,
									ipImei, aggreatorid, agent, 66);
							if (dtdsApply > 0) {
								double tdsAmount66 = ((amount - (distSurchar + supDistSurcharge)) * dtdsApply) / 100;
								if (tdsAmount66 > 0) {

								}
							}
							if (distSurchar != 0 && distId != null && !distId.trim().equals("")) {
								creditMoney("D" + mpfwTxnId, userId, getWalletIdByUserId(distId, aggreatorid),
										distSurchar, getMobileNumber(payeeWalletid), reptxnId, ipImei, aggreatorid,
										agent, 67);
								if (dtdsApply > 0) {
									double tdsAmount67 = (distSurchar * dtdsApply) / 100;
									if (tdsAmount67 > 0) {

									}
								}
							}
							if (supDistSurcharge != 0 && (supDistId != null && !supDistId.trim().isEmpty()
									&& !supDistId.equalsIgnoreCase("-1"))) {
								creditMoney("SD" + mpfwTxnId, userId, getWalletIdByUserId(supDistId, aggreatorid),
										supDistSurcharge, getMobileNumber(payeeWalletid), reptxnId, ipImei, aggreatorid,
										agent, 36);
								if (dtdsApply > 0) {
									double tdsAmount36 = (supDistSurcharge * dtdsApply) / 100;
									if (tdsAmount36 > 0) {

									}
								}
							}
						} else if (txnCode == 19) {
							creditMoney(mpfwTxnId, userId, getWalletIdByUserId(aggreatorid, aggreatorid),
									(((amount - distSurchar) - supDistSurcharge) - agentRefund),
									getMobileNumber(payeeWalletid), reptxnId, ipImei, aggreatorid, agent, 33);
							
							if (dtdsApply > 0) {
								double tdsAmount33 = ((((amount - distSurchar) - supDistSurcharge) - agentRefund) * dtdsApply) / 100;
								if (tdsAmount33 > 0) {
									paymentFromWalletDist(userId,getWalletIdByUserId(aggreatorid,aggreatorid),98,tdsAmount33,getMobileNumber(payeeWalletid),reptxnId,ipImei,aggreatorid,agent,0);
								}
							}
							
							if (distSurchar != 0 && distId != null && !distId.trim().equals("")) {
								creditMoney("D" + mpfwTxnId, userId, getWalletIdByUserId(distId, aggreatorid),
										distSurchar, getMobileNumber(payeeWalletid), reptxnId, ipImei, aggreatorid,
										agent, 54);
								if (dtdsApply > 0) {
									double tdsAmount54 = (distSurchar * dtdsApply) / 100;
									if (tdsAmount54 > 0) {
										paymentFromWalletDist(userId,getWalletIdByUserId(distId,aggreatorid),100,tdsAmount54,getMobileNumber(payeeWalletid),reptxnId,ipImei,aggreatorid,agent,0);

									}
								}
							}
							if (supDistSurcharge != 0 && (supDistId != null && !supDistId.trim().isEmpty()
									&& !supDistId.equalsIgnoreCase("-1"))) {
								creditMoney("SD" + mpfwTxnId, userId, getWalletIdByUserId(supDistId, aggreatorid),
										supDistSurcharge, getMobileNumber(payeeWalletid), reptxnId, ipImei, aggreatorid,
										agent, 73);
								if (dtdsApply > 0) {
									double tdsAmount73 = (supDistSurcharge * dtdsApply) / 100;
									if (tdsAmount73 > 0) {
										paymentFromWalletDist(userId,getWalletIdByUserId(supDistId,aggreatorid),102,tdsAmount73,getMobileNumber(payeeWalletid),reptxnId,ipImei,aggreatorid,agent,0);

									}
								}
							}
						}
					}

  	  			}else{
  	  			status= velocityCheckSt;
  	  			}			
				}else{
  				status= "7024";
  			}
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","problem in paymentFromWallet" + e.getMessage()+" "+e);
			//logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
			transaction.rollback();
			return "7000";
	  	  } finally {
			session.close();
		}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromWalletDist()","Inside paymentFromWallet return "+status);
		//logger.info("Inside ====================paymentFromWallet=== return===="+status);
		return status;
	}
	

	
	private String paymentFromCashBackWallet(String userId,String payeeWalletid,int txnCode,double amount,String payeedtl,String reptxnId,String ipImei,String aggreatorid,String agent,double surCharge){

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","paymentFromCashBackWallet()"+"|"+agent);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "paymentFromCashBackWallet()","paymentFromCashBackWallet amount "+amount);
		//logger.info("start excution ===================== method paymentFromCashBackWallet(userId,payeeWalletid,txnCode,amount,paydtl,reptxnId,ipImei)"+userId);
		//logger.info("paymentFromCashBackWallet ===================== aount"+amount);
		String status="1001";
		CashBackLedgerBean cashBackLedgerBean=new CashBackLedgerBean();
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try{
			 if(amount<=0){
				 
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromCashBackWallet()","Txn Amount Not Be Zero");	 
		   			//logger.info("==========================Txn Amount Not Be Zero==================================");
		         	return "7032";
		   		  }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromCashBackWallet()","getCashBackWalletBalance()"+	getCashBackWalletBalance(payeeWalletid));
			//logger.info(payeeWalletid+"start excution ===================== method getCashBackWalletBalance()"+	getCashBackWalletBalance(payeeWalletid));

			if(getCashBackWalletBalance(payeeWalletid)>=amount){
				
  	  			String mpfwTxnId=commanUtilDao.getTrxId("MPFW",aggreatorid);
  	  				
				cashBackLedgerBean.setTxnid(mpfwTxnId);
				cashBackLedgerBean.setUserid(userId);
				cashBackLedgerBean.setWalletid(payeeWalletid);
				cashBackLedgerBean.setTxncode(txnCode);
				cashBackLedgerBean.setTxncredit(0.0);
				cashBackLedgerBean.setTxndebit(amount);
				cashBackLedgerBean.setSurCharge(surCharge);
				cashBackLedgerBean.setPayeedtl(payeedtl);
				cashBackLedgerBean.setResult("Success");
				cashBackLedgerBean.setResptxnid(reptxnId);
				cashBackLedgerBean.setAggreatorid(aggreatorid);
				cashBackLedgerBean.setWalletuserip(ipImei);
				cashBackLedgerBean.setStatus(1);
  	  		  	cashBackLedgerBean.setWalletuseragent(agent);
  	  		   	session.save(cashBackLedgerBean);
  	  		   	transaction.commit();
  	  		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromCashBackWallet()","Wallet Payment transactin commit sucesfully");
  	  		   	logger.info("================Wallet Payment transactin commit sucesfully================"); 
  	  		   	status= "1000";
  	  		
				}else{
  				status= "7024";
  			}
		}catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromCashBackWallet()","problem in paymentFromWallet"+e.getMessage()+" "+e);
			//logger.debug("problem in paymentFromWallet=========================" + e.getMessage(), e);
			transaction.rollback();
			return "7000";
	  	  } finally {
			session.close();
		}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","paymentFromCashBackWallet()","Inside paymentFromWallet return"+status);
		//logger.info("Inside ====================paymentFromWallet=== return===="+status);
		return status;
	}
	
	
	
	public String walletToAggregatorTransferVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().walletToAggregatorMoneyTransfer(58,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent,0.0,0.0);
	}
	
	public String walletToAggregatorTransfer(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent, double aggReturnVal, double dtdsApply){
		return new TransactionDaoImpl().walletToAggregatorMoneyTransfer(59,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent,aggReturnVal,dtdsApply);
	}
	
	public String walletToAggregatorTransferExt(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().walletToAggregatorMoneyTransferExt(59,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent);
	}
	
	public String walletToAggregatorRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent , double aggReturnVal, double dtdsApply ){
		return new TransactionDaoImpl().walletToAggregatorRefundTrans(63,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent,  aggReturnVal, dtdsApply );
	}
	
	public String walletToAggregatorRefundExt(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().walletToAggregatorRefundTransExt(63,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent);
	}
	
	public String walletToAggregatorRefundVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){
		return new TransactionDaoImpl().walletToAggregatorRefundTrans(62,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,agent,0.0,0.0);
	}
	
	
	
/*	 public String walletToSuperDistributorTransfer(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
		 return new TransactionDaoImpl().walletToSuperDistributorTransfer(70,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,superDistributorId,agent);
	 }

	  public String walletToSuperDistributorTransferRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
			 return new TransactionDaoImpl().walletToSuperDistributorTransferRefund(71,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,superDistributorId,agent);

	  }

	  public String walletToSuperDistributorTransferVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
			 return new TransactionDaoImpl().walletToSuperDistributorTransfer(72,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,superDistributorId,agent);
	  }

	  public String walletToSuperDistributorTransferVerifyRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
			 return new TransactionDaoImpl().walletToSuperDistributorTransferRefund(73,respTxnId,userId,amount,surcharge,ipImei,aggreatorid,superDistributorId,agent);

	  }*/

	/**
	 * 
	 * @param userId
	 * @param sendWalletid
	 * @param recMobile
	 * @param amount
	 * @param ipImei
	 * @return
	 */
	
	public String walletToWalletTransfer(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent,String checkSum){
		return new TransactionDaoImpl().walletToWalletMoneyTransfer(2, userId, sendWalletid, recMobile, amount, ipImei, aggreatorid, agent,0.0,checkSum);
	}
	
	/**
	 * 
	 * @param userId
	 * @param sendWalletid
	 * @param recMobile
	 * @param amount
	 * @param ipImei
	 * @return
	 */
	public String cashIn(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent,double surCharge,String checkSum){
		return new TransactionDaoImpl().walletToWalletMoneyTransfer(15, userId, sendWalletid, recMobile, amount, ipImei, aggreatorid, agent,surCharge,checkSum);
	}
	
	
	
	/**
	 * 
	 * @param txnCode
	 * @param userId
	 * @param sendWalletid
	 * @param recMobile
	 * @param amount
	 * @param ipImei
	 * @param aggreatorid
	 * @param agent
	 * @return
	 */
	public String walletToWalletMoneyTransfer(int txnCode,String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent,double surCharge,String checkSum){
		
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToWalletMoneyTransfer()"+"|"+agent);
		//logger.info("Inside ===================== method walletToWalletTransfer(userId,sendWalletid, recMobile, amount, ipImei)");
		
		String status="1001";
		
		TreeMap<String,String> obj=new TreeMap<String,String>();
		 obj.put("userId",userId);
         obj.put("walletId",sendWalletid);
         obj.put("mobileNo",recMobile);
         obj.put("trxAmount",""+amount);
         obj.put("ipImei",ipImei);
         obj.put("aggreatorid",aggreatorid);
         obj.put("CHECKSUMHASH",checkSum);
         
       int statusCode= WalletSecurityUtility.checkSecurity(obj);
       if(statusCode!=1000){
    	   return ""+statusCode;
       }
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		transaction = session.beginTransaction();
		SQLQuery insertQuery = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
		insertQuery.setParameter(0, sendWalletid);
		insertQuery.executeUpdate();
		transaction.commit();
  		  
  		  if(amount<=0){
  			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToWalletMoneyTransfer()","Txn Amount Not Be Zero"+sendWalletid);
  			  //logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
        	return "7032";
  		  }
  		  
  		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(recMobile,aggreatorid);
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToWalletMoneyTransfer()","You can not transfer money to your own wallet.");
  			// logger.info("==========================You can not transfer money to your own wallet.==================================");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToWalletMoneyTransfer()");
  			 // logger.info("==========================Start walletToWalletTransfer excution==================================");
  			if(getWalletBalance(sendWalletid)>=amount+surCharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String velocityCheckSt=velocityCheck(recWalletid,amount,9);
  	  			if(velocityCheckSt.equals("1000")){
  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  		
  	  				
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(sendWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount);
  	  			transactionBean.setSurCharge(surCharge);
  	  			transactionBean.setPayeedtl(recMobile);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(recivetrxId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  		
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
  	  		    transactionBeanr.setUserid(userId);
  	  		    transactionBeanr.setWalletid(recWalletid);
  	  		    transactionBeanr.setAggreatorid(aggreatorid);
  	  		    transactionBeanr.setTxncode(9);
  	  		    transactionBeanr.setTxncredit(amount);
  	  		    transactionBeanr.setTxndebit(0.0);
  	  		    transactionBeanr.setPayeedtl(getMobileNumber(sendWalletid));
  	  		    transactionBeanr.setResult("Success");
  	  		    transactionBeanr.setResptxnid(sendtrxId);
  	  		    transactionBeanr.setWalletuserip(ipImei);
  	  		    transactionBeanr.setWalletuseragent(agent);
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeanr);
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		   String msgId=commanUtilDao.getTrxId("GCM",aggreatorid);
  	  		   	if(!(commanUtilDao.checkLoginStatus(recMobile,aggreatorid)==1)){ 
  	  		   		GCMPendingMsgBean gCMPendingMsgBean=new GCMPendingMsgBean();
  	  		   		gCMPendingMsgBean.setGcmid(msgId);
  	  		   		gCMPendingMsgBean.setReceivermobile(recMobile);
  	  		   	    gCMPendingMsgBean.setMsg(commanUtilDao.getNotificationTemplet("walletTowallet").replace("<<mobile>>", getMobileNumber(sendWalletid)).replace("<<AMOUNT>>", Double.toString(amount)));
  	  		   	    gCMPendingMsgBean.setMsgtype("request");
  	  		    	gCMPendingMsgBean.setSendermobile(getMobileNumber(sendWalletid));
  	  		       gCMPendingMsgBean.setTxnid(recivetrxId);
  	  		       gCMPendingMsgBean.setAggreatorid(aggreatorid);
  	  		       session.saveOrUpdate(gCMPendingMsgBean);
  	  		   	}
  	  		 transaction.commit();
  	  	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToWalletMoneyTransfer()","wallet to Wallet transactin commit sucesfully");
  	  		// logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 try{
			 walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
		     walletMastBean=walletUserDao.showUserProfile(userId);
			    //String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId);
		     String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid));
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","Wallet to Wallet Send SMS"+smstemplet);
		    // logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
		     String mailSub= "You have made payment of Rs. "+String.format("%.2f",amount)+" to "+recMobile+".";
		     //String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
		     String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", String.format("%.2f",amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
		     	
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToWalletMoneyTransfer()","Wallet to Wallet Send  MAIL"+mailContent);
		     	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",aggreatorid,sendWalletid,walletMastBean.getName(),"NOTP");
		     	ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			     smstemplet = commanUtilDao.getsmsTemplet("reciveMoney",aggreatorid).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<Datewithtime>>", getDate("yyyy/MM/dd HH:mm:ss"));
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToWalletMoneyTransfer()","Wallet to Wallet recived SMS"+smstemplet);
			     mailSub= "Rs. "+String.format("%.2f",amount)+" received successfully from "+getMobileNumber(sendWalletid)+".";
			     mailContent= commanUtilDao.getmailTemplet("receivedMoney",aggreatorid).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<AMOUNT>>", String.format("%.2f",amount)).replace("<<NAME>>", walletMastBean.getName()).replace("<<MOBILE>>", getMobileNumber(sendWalletid)).replace("<<TXNNO>>", recivetrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()))).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "walletToWalletMoneyTransfer()", "walletToWalletMoneyTransfer()","Wallet to Wallet recived  MAIL"+mailContent);
			     smsAndMailUtility = new SmsAndMailUtility(getEmailByUserId(recMobile,aggreatorid), mailContent,mailSub, recMobile, smstemplet, "Both",aggreatorid,sendWalletid,getNameByUserId(recMobile,aggreatorid),"NOTP");
				 ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			
			 if(commanUtilDao.checkLoginStatus(recMobile,aggreatorid)==1){  
				  GCMDeviceBean gCMDeviceBean=(GCMDeviceBean)session.get(GCMDeviceBean.class, getIdByUserId(recMobile,aggreatorid));
	  			  GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), commanUtilDao.getNotificationTemplet("walletTowallet").replace("<<mobile>>", getMobileNumber(sendWalletid)).replace("<<AMOUNT>>", Double.toString(amount)),"WTW",getMobileNumber(sendWalletid),recivetrxId,recMobile,msgId,aggreatorid);
				}
			 }catch (Exception e) {
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToWalletMoneyTransfer()","problem in wallet to wallet  Money transfer while SMS Mail And GCM"+e.getMessage()+" "+e);
				// logger.debug("problem in wallet to wallet  Money transfer while SMS Mail And GCM " + e.getMessage(), e);
			}
			 
			 
 	  			}else{
  	  			status= velocityCheckSt;
  	  			}  	  			
  			}else{
  				status= "7024";
  			}
  		  }
  	  }catch(Exception e){
  		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToWalletMoneyTransfer()","problem in wallet to wallet  Money transfer"+e.getMessage()+" "+e);
		transaction.rollback();
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToWalletMoneyTransfer()","Inside"+ status+" " + e.getMessage()+" "+e);
		status="7000";
	} finally {
		
		transaction=session.beginTransaction();
		SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+sendWalletid+"'");
		delete.executeUpdate();
		transaction.commit();
		session.close();
	}
  	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToWalletMoneyTransfer()","Inside walletToWalletTransfer return"+status);
  	 // logger.info("Inside ====================walletToWalletTransfer=== return===="+status);
	return status;
	}
	
	
	public String walletToAggregatorMoneyTransfer(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent, double aggReturnVal, double dtdsApply){

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorMoneyTransfer()"+"|"+agent);
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction =null;
  	    String sendWalletid = null;
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		sendWalletid = getWalletIdByUserId(userId, aggreatorid);
  		transaction = session.beginTransaction();
		SQLQuery insertQuery = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
		insertQuery.setParameter(0, sendWalletid);
		insertQuery.executeUpdate();
		transaction.commit();
  		  
  		  if((amount+surcharge)<=0){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","Txn Amount Not Be Zero"+sendWalletid);
        	return "7032";
  		  }
  		  
  		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(/*"OAGG001050","OAGG001050"*/globalAggregator,globalAggregator);
  		String recWalletidRev=new TransactionDaoImpl().getWalletIdByUserId(/*"OAGG001050","OAGG001050"*/aggreatorid,aggreatorid);
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","You can not transfer money to your own wallet");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","Start walletToAggregatorMoneyTransfer excution");
  			if(getWalletBalance(sendWalletid)>=amount+surcharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeancomm=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  		
  	  				
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(sendWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount);
  	  			transactionBean.setSurCharge(surcharge);
  	  			transactionBean.setPayeedtl(globalAggregator);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(respTxnId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  		
					transactionBeancomm.setTxnid(commanUtilDao.getTrxId("WTWMT",aggreatorid));
					transactionBeancomm.setUserid(userId);
					transactionBeancomm.setWalletid(sendWalletid);
					transactionBeancomm.setAggreatorid(aggreatorid);
					if(txnCode == 58)
						transactionBeancomm.setTxncode(111);
	  	  		    else if(txnCode == 59)
	  	  		    	transactionBeancomm.setTxncode(107);
					
					transactionBeancomm.setTxncredit(0.0);
					transactionBeancomm.setTxndebit(surcharge);
					transactionBeancomm.setSurCharge(surcharge);
		  			transactionBeancomm.setPayeedtl(globalAggregator);
		  			transactionBeancomm.setResult("Success");
		  			transactionBeancomm.setResptxnid(respTxnId);
		  			transactionBeancomm.setWalletuserip(ipImei);
		  			transactionBeancomm.setWalletuseragent(agent);
	  		
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
	  	  		transactionBeanr.setUserid(globalAggregator);
				transactionBeanr.setWalletid(recWalletid);
				transactionBeanr.setAggreatorid(globalAggregator);
  	  		    if(txnCode == 58)
  	  		    	transactionBeanr.setTxncode(60);
  	  		    else if(txnCode == 59)
  	  		    	transactionBeanr.setTxncode(61);
					if (txnCode == 76) {
						transactionBeanr.setTxncode(77);
						transactionBeanr.setTxncredit(amount);
					} else {
						transactionBeanr.setTxncredit(surcharge - aggReturnVal);
					}
  	  		    transactionBeanr.setTxndebit(0.0);
  	  		    transactionBeanr.setPayeedtl(getMobileNumber(sendWalletid));
  	  		    transactionBeanr.setResult("Success");
  	  		    transactionBeanr.setResptxnid(respTxnId);
  	  		    transactionBeanr.setWalletuserip(ipImei);
  	  		    transactionBeanr.setWalletuseragent(agent);
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeancomm);
  	  		   session.save(transactionBeanr);
  	  		   
				if (aggReturnVal > 0) {
					
					String txnId1=commanUtilDao.getTrxId("AMIW",aggreatorid);
					TransactionBean transactionBeanAggReturn=new TransactionBean();
					transactionBeanAggReturn.setTxnid(txnId1);
					transactionBeanAggReturn.setUserid(userId);
					transactionBeanAggReturn.setWalletid(sendWalletid);
					transactionBeanAggReturn.setAggreatorid(aggreatorid/* "AGGR001035" */);
					transactionBeanAggReturn.setTxncode(104);
					transactionBeanAggReturn.setTxncredit(aggReturnVal);
					transactionBeanAggReturn.setTxndebit(0);
					transactionBeanAggReturn.setPayeedtl(globalAggregator);
					transactionBeanAggReturn.setResult("Success");
					transactionBeanAggReturn.setResptxnid(respTxnId);
					transactionBeanAggReturn.setWalletuserip(ipImei);
					transactionBeanAggReturn.setWalletuseragent(agent);
					session.save(transactionBeanAggReturn);
					
					if(aggReturnVal>0 && dtdsApply >0){
						double tdsAmount=(aggReturnVal*dtdsApply)/100;
						if(tdsAmount>0) {
							TransactionBean transactionBeanTDS=new TransactionBean();
							transactionBeanTDS.setTxnid("TC"+txnId1);
							transactionBeanTDS.setUserid(userId);
							transactionBeanTDS.setWalletid(sendWalletid);
							transactionBeanTDS.setAggreatorid(aggreatorid/* "AGGR001035" */);
							transactionBeanTDS.setTxncode(106);
							transactionBeanTDS.setTxncredit(0);
							transactionBeanTDS.setTxndebit(tdsAmount);
							transactionBeanTDS.setPayeedtl(globalAggregator);
							transactionBeanTDS.setResult("Success");
							transactionBeanTDS.setResptxnid(respTxnId);
							transactionBeanTDS.setWalletuserip(ipImei);
							transactionBeanTDS.setWalletuseragent(agent);
							session.save(transactionBeanTDS);
						}
					}
				}
  	  		   
  	  		   
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		 transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","wallet to Wallet transactin commit sucesfully");
  	  		 //logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 
			 
 	  			}  else{
  				status= "7024";
  			}
  	  }}catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","problem in wallet to wallet  Money transfer"+e.getMessage()+" "+e);
  		 // logger.debug("problem in wallet to wallet  Money transfer " + e.getMessage(), e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","Inside "+ status + e.getMessage()+" "+e);
		//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		
		transaction=session.beginTransaction();
		SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+sendWalletid+"'");
		delete.executeUpdate();
		transaction.commit();
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransfer()","InsidewalletToAggregatorMoneyTransfer return "+status);
  	  //logger.info("Inside ====================walletToAggregatorMoneyTransfer=== return===="+status);
	return status;
	}
	
	
	public String walletToAggregatorMoneyTransferExt(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorMoneyTransferExt()"+"|"+agent);
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction =null;
  	    String sendWalletid = null;
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		sendWalletid = getWalletIdByUserId(userId, aggreatorid);
  		transaction = session.beginTransaction();
		SQLQuery insertQuery = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
		insertQuery.setParameter(0, sendWalletid);
		insertQuery.executeUpdate();
		transaction.commit();
  		  
  		  if((amount+surcharge)<=0){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","Txn Amount Not Be Zero"+sendWalletid);
  			  //logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
        	return "7032";
  		  }
  		  
  		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(/*"OAGG001050","OAGG001050"*/globalAggregator,globalAggregator);
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","You can not transfer money to your own wallet");
  			 //logger.info("==========================You can not transfer money to your own wallet.==================================");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","Start walletToAggregatorMoneyTransferExt excution");
  			 // logger.info("==========================Start walletToAggregatorMoneyTransferExt excution==================================");
  			if(getWalletBalance(sendWalletid)>=amount+surcharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  				String sendtrxIdA=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeanA=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  		
  	  				
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(sendWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(surcharge);
  	  			transactionBean.setSurCharge(surcharge);
  	  			transactionBean.setPayeedtl(globalAggregator);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(respTxnId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  			
  	  			transactionBeanA.setTxnid(sendtrxIdA);
	  			transactionBeanA.setUserid(userId);
	  			transactionBeanA.setWalletid(sendWalletid);
	  			transactionBeanA.setAggreatorid(aggreatorid);
	  			transactionBeanA.setTxncode(4);
	  			transactionBeanA.setTxncredit(0.0);
	  			transactionBeanA.setTxndebit(amount);
	  			transactionBeanA.setSurCharge(0.0);
	  			transactionBeanA.setPayeedtl(globalAggregator);
	  			transactionBeanA.setResult("Success");
	  			transactionBeanA.setResptxnid(respTxnId);
	  			transactionBeanA.setWalletuserip(ipImei);
	  			transactionBeanA.setWalletuseragent(agent);
  	  		
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
  	  		    transactionBeanr.setUserid(globalAggregator);
  	  		    transactionBeanr.setWalletid(recWalletid);
  	  		    transactionBeanr.setAggreatorid(globalAggregator);
  	  		    if(txnCode == 58)
  	  		    	transactionBeanr.setTxncode(60);
  	  		    else if(txnCode == 59)
  	  		    	transactionBeanr.setTxncode(61);
  	  		    transactionBeanr.setTxncredit(surcharge);
  	  		    transactionBeanr.setTxndebit(0.0);
  	  		    transactionBeanr.setPayeedtl(getMobileNumber(sendWalletid));
  	  		    transactionBeanr.setResult("Success");
  	  		    transactionBeanr.setResptxnid(respTxnId);
  	  		    transactionBeanr.setWalletuserip(ipImei);
  	  		    transactionBeanr.setWalletuseragent(agent);
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeanA);
  	  		   session.save(transactionBeanr);
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		 transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","wallet to Wallet transactin commit sucesfully");
  	  		 //logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 
			 
 	  			}  else{
  				status= "7024";
  			}
  	  }}catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","problem in wallet to wallet  Money transfer"+e.getMessage()+" "+e);
  		 // logger.debug("problem in wallet to wallet  Money transfer " + e.getMessage(), e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","Inside "+ status + e.getMessage()+" "+e);
		//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		
		transaction=session.beginTransaction();
		SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+sendWalletid+"'");
		delete.executeUpdate();
		transaction.commit();
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorMoneyTransferExt()","InsidewalletToAggregatorMoneyTransferExt return "+status);
  	  //logger.info("Inside ====================walletToAggregatorMoneyTransferExt=== return===="+status);
	return status;
	}
	
	
	public String walletToAggregatorRefundTrans(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent, double aggReturnVal, double dtdsApply ){

	      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorRefundTrans()"+"| "+agent);
	      //logger.info("Inside ===================== method walletToAggregatorRefundTrans(userId,sendWalletid, recMobile, amount, ipImei)");
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction =null;
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		String  recWalletid= getWalletIdByUserId(userId, aggreatorid);
  		String  sendWalletid=new TransactionDaoImpl().getWalletIdByUserId(globalAggregator,globalAggregator);
  		String  sendWalletidRen=new TransactionDaoImpl().getWalletIdByUserId(aggreatorid,aggreatorid);
  		  
  		  if((amount+surcharge)<=0){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","Txn Amount Not Be Zero"+sendWalletid);
  			//logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
        	return "7032";
  		  }
  		  
  		
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","You can not transfer money to your own wallet.");
  			 //logger.info("==========================You can not transfer money to your own wallet.==================================");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorRefundTrans()");
  			  //logger.info("==========================Start walletToAggregatorRefundTrans excution==================================");
  			if(getWalletBalance(sendWalletid)>=amount+surcharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  			TransactionBean transactionBeanSur=new TransactionBean();
  	  		
  	  			
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(recWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(amount);
  	  			transactionBean.setTxndebit(0);
  	  			transactionBean.setSurCharge(surcharge);
  	  			transactionBean.setPayeedtl(getMobileNumber(sendWalletid));
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(respTxnId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  			
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
	  		    transactionBeanr.setUserid(userId);
	  		    transactionBeanr.setWalletid(recWalletid);
	  		    transactionBeanr.setAggreatorid(aggreatorid/*"AGGR001035"*/);
					if (txnCode == 62)
						transactionBeanr.setTxncode(112);
					else if (txnCode == 63)
						transactionBeanr.setTxncode(108);
	  		    
	  		    transactionBeanr.setTxncredit(surcharge);
	  		    transactionBeanr.setTxndebit(0.0);
	  		    transactionBeanr.setPayeedtl(globalAggregator);
	  		    transactionBeanr.setResult("Success");
	  		    transactionBeanr.setResptxnid(respTxnId);
	  		    transactionBeanr.setWalletuserip(ipImei);
	  		    transactionBeanr.setWalletuseragent(agent);
	  		    
  	  				
  	  			
		  	  		transactionBeanSur.setTxnid(commanUtilDao.getTrxId("WTWMT",aggreatorid));
					transactionBeanSur.setUserid(globalAggregator);
					transactionBeanSur.setWalletid(sendWalletid);
					transactionBeanSur.setAggreatorid(globalAggregator/* "AGGR001035" */);
					 if(txnCode == 62)
						transactionBeanSur.setTxncode(64);
			  		 else if(txnCode == 63)
			  			transactionBeanSur.setTxncode(65);
					transactionBeanSur.setTxncredit(0);
					transactionBeanSur.setTxndebit(surcharge-aggReturnVal);
					transactionBeanSur.setPayeedtl(globalAggregator);
					transactionBeanSur.setResult("Success");
					transactionBeanSur.setResptxnid(respTxnId);
					transactionBeanSur.setWalletuserip(ipImei);
					transactionBeanSur.setWalletuseragent(agent);
  	  			
  	  			
  	  		    
	  		    
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeanr);
  	  		   session.save(transactionBeanSur);
					if (aggReturnVal > 0) {
						
						String txnId1=commanUtilDao.getTrxId("AMIW",aggreatorid);
						TransactionBean transactionBeanAggReturn=new TransactionBean();
						transactionBeanAggReturn.setTxnid(txnId1);
						transactionBeanAggReturn.setUserid(userId);
						transactionBeanAggReturn.setWalletid(recWalletid);
						transactionBeanAggReturn.setAggreatorid(aggreatorid/* "AGGR001035" */);
						transactionBeanAggReturn.setTxncode(103);
						transactionBeanAggReturn.setTxncredit(0);
						transactionBeanAggReturn.setTxndebit(aggReturnVal);
						transactionBeanAggReturn.setPayeedtl(globalAggregator);
						transactionBeanAggReturn.setResult("Success");
						transactionBeanAggReturn.setResptxnid(respTxnId);
						transactionBeanAggReturn.setWalletuserip(ipImei);
						transactionBeanAggReturn.setWalletuseragent(agent);
						session.save(transactionBeanAggReturn);
						
						if(aggReturnVal>0 && dtdsApply >0){
							double tdsAmount=(aggReturnVal*dtdsApply)/100;
							if(tdsAmount>0) {
								TransactionBean transactionBeanTDS=new TransactionBean();
								transactionBeanTDS.setTxnid("TC"+txnId1);
								transactionBeanTDS.setUserid(userId);
								transactionBeanTDS.setWalletid(recWalletid);
								transactionBeanTDS.setAggreatorid(aggreatorid/* "AGGR001035" */);
								transactionBeanTDS.setTxncode(105);
								transactionBeanTDS.setTxncredit(tdsAmount);
								transactionBeanTDS.setTxndebit(0);
								transactionBeanTDS.setPayeedtl(globalAggregator);
								transactionBeanTDS.setResult("Success");
								transactionBeanTDS.setResptxnid(respTxnId);
								transactionBeanTDS.setWalletuserip(ipImei);
								transactionBeanTDS.setWalletuseragent(agent);
								session.save(transactionBeanTDS);
							}
						}
					}
  	  		   
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		 transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","wallet to Wallet transactin commit sucesfully");
  	  		// logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 
			 
 	  			}  else{
  				status= "7024";
  			}
  	  }}
  	catch(HibernateException e ){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","problem in walletToAggregatorRefundTrans"+e.getMessage()+" "+e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","Inside "+ status +"|"+ e.getMessage()+" "+e);
		status="7000";
	} 
  	  catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToAggregatorRefundTrans()","problem in walletToAggregatorRefundTrans"+ e.getMessage()+" "+e);
		transaction.rollback();
		logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTrans()","walletToAggregatorRefundTrans return"+status);
	return status;
	}
	
	public String walletToAggregatorRefundTransExt(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent){

	      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorRefundTransExt()"+"| "+agent);
	      //logger.info("Inside ===================== method walletToAggregatorRefundTransExt(userId,sendWalletid, recMobile, amount, ipImei)");
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
 	 	Session session = factory.openSession();
	    Transaction transaction =null;
	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
	  try{
		  
		String  recWalletid= getWalletIdByUserId(userId, aggreatorid);
		String  sendWalletid=new TransactionDaoImpl().getWalletIdByUserId(globalAggregator,globalAggregator);
		  
		  if((amount+surcharge)<=0){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","Txn Amount Not Be Zero"+sendWalletid);
			//logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
      	return "7032";
		  }
		  
		
		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","You can not transfer money to your own wallet.");
			 //logger.info("==========================You can not transfer money to your own wallet.==================================");
       	return "7036";
 		  }
	  		
		if(recWalletid.equals("1001")){
			status= "7023";
		  }else{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToAggregatorRefundTransExt()");
			  //logger.info("==========================Start walletToAggregatorRefundTransExt excution==================================");
			if(getWalletBalance(sendWalletid)>=amount+surcharge){
				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
				String sendtrxIdA=commanUtilDao.getTrxId("WTWMT",aggreatorid);
	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

	  			transaction = session.beginTransaction();
	  				
	  			TransactionBean transactionBean=new TransactionBean();
	  			TransactionBean transactionBeanA=new TransactionBean();
	  			TransactionBean transactionBeanr=new TransactionBean();
	  		
	  				
	  			transactionBean.setTxnid(sendtrxId);
	  			transactionBean.setUserid(globalAggregator);
	  			transactionBean.setWalletid(sendWalletid);
	  			transactionBean.setAggreatorid(globalAggregator);
	  			transactionBean.setTxncode(txnCode);
	  			transactionBean.setTxncredit(0.0);
	  			transactionBean.setTxndebit(surcharge);
	  			transactionBean.setSurCharge(surcharge);
	  			transactionBean.setPayeedtl(getMobileNumber(sendWalletid));
	  			transactionBean.setResult("Success");
	  			transactionBean.setResptxnid(respTxnId);
	  			transactionBean.setWalletuserip(ipImei);
	  			transactionBean.setWalletuseragent(agent);
	  			
	  			transactionBeanA.setTxnid(sendtrxIdA);
	  		    transactionBeanA.setUserid(userId);
	  		    transactionBeanA.setWalletid(recWalletid);
	  		    transactionBeanA.setAggreatorid(aggreatorid/*"AGGR001035"*/);
	  		    transactionBeanA.setTxncode(10);
	  		    transactionBeanA.setTxncredit(amount);
	  		    transactionBeanA.setTxndebit(0.0);
	  		    transactionBeanA.setPayeedtl(globalAggregator);
	  		    transactionBeanA.setResult("Success");
	  		    transactionBeanA.setResptxnid(respTxnId);
	  		    transactionBeanA.setWalletuserip(ipImei);
	  		    transactionBeanA.setWalletuseragent(agent);
	  			transactionBeanA.setSurCharge(0.0);
	  			
	  		
	  			
	  			transactionBeanr.setTxnid(recivetrxId);
	  		    transactionBeanr.setUserid(userId);
	  		    transactionBeanr.setWalletid(recWalletid);
	  		    transactionBeanr.setAggreatorid(aggreatorid/*"AGGR001035"*/);
	  		    if(txnCode == 63)
	  		    	transactionBeanr.setTxncode(65);
	  		    transactionBeanr.setTxncredit(surcharge);
	  		    transactionBeanr.setTxndebit(0.0);
	  		    transactionBeanr.setPayeedtl(globalAggregator);
	  		    transactionBeanr.setResult("Success");
	  		    transactionBeanr.setResptxnid(respTxnId);
	  		    transactionBeanr.setWalletuserip(ipImei);
	  		    transactionBeanr.setWalletuseragent(agent);
	  		// transactionBeanr.setWalletuseragent("");
	  		   session.save(transactionBean);
	  		   session.save(transactionBeanA);
	  		   session.save(transactionBeanr);
	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
	  		   wTWNotificationBean.setAmount(amount);
	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
	  		   session.save(wTWNotificationBean);
	  		   
	  		 transaction.commit();
	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","wallet to Wallet transactin commit sucesfully");
	  		// logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 
			 
	  			}  else{
				status= "7024";
			}
	  }}
	catch(HibernateException e ){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","problem in walletToAggregatorRefundTransExt"+e.getMessage()+" "+e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","Inside "+ status +"|"+ e.getMessage()+" "+e);
		status="7000";
	} 
	  catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToAggregatorRefundTransExt()","problem in walletToAggregatorRefundTransExt"+ e.getMessage()+" "+e);
		transaction.rollback();
		logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToAggregatorRefundTransExt()","walletToAggregatorRefundTransExt return"+status);
	return status;
	}
	
	
	
	
	public String walletToSuperDistributorTransfer(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToSuperDistributorTransfer()"+"|"+agent);
		//logger.info("Inside ===================== method walletToAggregatorMoneyTransfer(userId,sendWalletid, recMobile, amount, ipImei)");
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction =null;
  	    String sendWalletid = null;
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		sendWalletid = getWalletIdByUserId(userId, aggreatorid);
  		transaction = session.beginTransaction();
		SQLQuery insertQuery = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
		insertQuery.setParameter(0, sendWalletid);
		insertQuery.executeUpdate();
		transaction.commit();
  		  
  		  if(amount<=0){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransfer()","Txn Amount Not Be Zero"+sendWalletid);
  			  //logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
        	return "7032";
  		  }
  		  
  		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(/*"OAGG001050","OAGG001050"*/superDistributorId,aggreatorid);
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransfer()","You can not transfer money to your own wallet.");
  			// logger.info("==========================You can not transfer money to your own wallet.==================================");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToSuperDistributorTransfer()");
  			 // logger.info("==========================Start walletToAggregatorMoneyTransfer excution==================================");
  			if(getWalletBalance(sendWalletid)>=amount+surcharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  		
  	  				
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(sendWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(amount+surcharge);
  	  			transactionBean.setSurCharge(surcharge);
  	  			transactionBean.setPayeedtl(superDistributorId);
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(respTxnId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  		
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
  	  		    transactionBeanr.setUserid(userId);
  	  		    transactionBeanr.setWalletid(recWalletid);
  	  		    transactionBeanr.setAggreatorid(aggreatorid);
  	  		    if(txnCode == 58)
  	  		    	transactionBeanr.setTxncode(60);
  	  		    else if(txnCode == 59)
  	  		    	transactionBeanr.setTxncode(61);
  	  		    transactionBeanr.setTxncredit(surcharge);
  	  		    transactionBeanr.setTxndebit(0.0);
  	  		    transactionBeanr.setPayeedtl(getMobileNumber(sendWalletid));
  	  		    transactionBeanr.setResult("Success");
  	  		    transactionBeanr.setResptxnid(respTxnId);
  	  		    transactionBeanr.setWalletuserip(ipImei);
  	  		    transactionBeanr.setWalletuseragent(agent);
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeanr);
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		 transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "","walletToSuperDistributorTransfer()");
  	  		 logger.info("================wallet to Wallet transactin commit sucesfully================"); 
			 status= "1000";
			 
			 
 	  			}  else{
  				status= "7024";
  			}
  	  }}catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransfer()","problem in wallet to wallet  Money transfer"+ e.getMessage()+" "+e);
  		 // logger.debug("problem in wallet to wallet  Money transfer " + e.getMessage(), e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransfer()","Inside" + status+"|"+e.getMessage()+" "+e);
		//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		
		transaction=session.beginTransaction();
		SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+sendWalletid+"'");
		delete.executeUpdate();
		transaction.commit();
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToSuperDistributorTransfer()","Inside walletToAggregatorMoneyTransfer return"+status);
  	  //logger.info("Inside ====================walletToAggregatorMoneyTransfer=== return===="+status);
	return status;
	}
	
	
	public String walletToSuperDistributorTransferRefund(int txnCode,String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "",
				"|"+"walletToSuperDistributorTransferRefund()"+"|"
				+ agent);
		//logger.info("Inside ===================== method walletToSuperDistributorTransferRefund(userId,sendWalletid, recMobile, amount, ipImei)");
		
		String status="1001";
		
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
  	    Transaction transaction =null;
  	  WTWNotificationBean wTWNotificationBean=new WTWNotificationBean();
  	  try{
  		  
  		String  recWalletid= getWalletIdByUserId(userId, aggreatorid);
  		String  sendWalletid=new TransactionDaoImpl().getWalletIdByUserId(superDistributorId,aggreatorid);
  		  
  		  if(amount<=0){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransferRefund()","Txn Amount Not Be Zero"
  					
  					+ sendWalletid);
  			 // logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
        	return "7032";
  		  }
  		  
  		
  		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransferRefund()","You can not transfer money to your own wallet"
  					);
  			 logger.info("==========================         .==================================");
         	return "7036";
   		  }
  	  		
  		if(recWalletid.equals("1001")){
  			status= "7023";
  		  }else{
  			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "",
  					"|"+"walletToSuperDistributorTransferRefund()");
  			  //logger.info("==========================Start walletToSuperDistributorTransferRefund excution==================================");
  			if(getWalletBalance(sendWalletid)>=amount+surcharge){
  				String sendtrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);
  	  			String recivetrxId=commanUtilDao.getTrxId("WTWMT",aggreatorid);

  	  			transaction = session.beginTransaction();
  	  				
  	  			TransactionBean transactionBean=new TransactionBean();
  	  			TransactionBean transactionBeanr=new TransactionBean();
  	  		
  	  				
  	  			transactionBean.setTxnid(sendtrxId);
  	  			transactionBean.setUserid(userId);
  	  			transactionBean.setWalletid(sendWalletid);
  	  			transactionBean.setAggreatorid(aggreatorid);
  	  			transactionBean.setTxncode(txnCode);
  	  			transactionBean.setTxncredit(0.0);
  	  			transactionBean.setTxndebit(surcharge);
  	  			transactionBean.setSurCharge(surcharge);
  	  			transactionBean.setPayeedtl(getMobileNumber(sendWalletid));
  	  			transactionBean.setResult("Success");
  	  			transactionBean.setResptxnid(respTxnId);
  	  			transactionBean.setWalletuserip(ipImei);
  	  			transactionBean.setWalletuseragent(agent);
  	  		
  	  			
  	  			transactionBeanr.setTxnid(recivetrxId);
  	  		    transactionBeanr.setUserid(userId);
  	  		    transactionBeanr.setWalletid(recWalletid);
  	  		    transactionBeanr.setAggreatorid(aggreatorid/*"AGGR001035"*/);
  	  		    if(txnCode == 62)
  	  		    	transactionBeanr.setTxncode(64);
  	  		    else if(txnCode == 63)
  	  		    	transactionBeanr.setTxncode(65);
  	  		    transactionBeanr.setTxncredit(surcharge+amount);
  	  		    transactionBeanr.setTxndebit(0.0);
  	  		    transactionBeanr.setPayeedtl(superDistributorId);
  	  		    transactionBeanr.setResult("Success");
  	  		    transactionBeanr.setResptxnid(respTxnId);
  	  		    transactionBeanr.setWalletuserip(ipImei);
  	  		    transactionBeanr.setWalletuseragent(agent);
  	  		// transactionBeanr.setWalletuseragent("");
  	  		   session.save(transactionBean);
  	  		   session.save(transactionBeanr);
  	  		   wTWNotificationBean.setNoticwalletid(recWalletid);
  	  		   wTWNotificationBean.setGivenwalletid(sendWalletid);
  	  		   wTWNotificationBean.setAmount(amount);
  	  		   wTWNotificationBean.setRecivertxnid(recivetrxId);
  	  		   wTWNotificationBean.setSendertxnid(sendtrxId);
  	  		   wTWNotificationBean.setAggreatorid(aggreatorid);
  	  		   session.save(wTWNotificationBean);
  	  		   
  	  		 transaction.commit();
  	  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransferRefund()",
  					"walletToSuperDistributorTransferRefund commit sucesfully");
  	  		// logger.info("================walletToSuperDistributorTransferRefund commit sucesfully================"); 
			 status= "1000";
			 
			 
 	  			}  else{
  				status= "7024";
  			}
  	  }
  	}
  	  
  	catch(HibernateException e ){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToSuperDistributorTransferRefund()",
				"problem in walletToSuperDistributorTransferRefund |"+e.getMessage()+" "+e
				);
  		//logger.debug("problem in walletToSuperDistributorTransferRefund  " + e.getMessage(), e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransferRefund()",
				"Inside"+ status+"" + e.getMessage()+" "+e);
		//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} 
  	  catch(Exception e){
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToSuperDistributorTransferRefund()",
  				"problem in walletToAggregatorRefundTrans  " + e.getMessage()+" "+e);
  		 // logger.debug("problem in walletToAggregatorRefundTrans  " + e.getMessage(), e);
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "", "walletToSuperDistributorTransferRefund()",
				"Inside "+ status+"" + e.getMessage()+" "+e);
		//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
		status="7000";
	} finally {
		session.close();
	}
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","walletToSuperDistributorTransferRefund()",
  			"Inside walletToSuperDistributorTransferRefund return"+status);
  	  //logger.info("Inside ====================walletToSuperDistributorTransferRefund=== return===="+status);
	return status;
	}
	
	
	
	
	
	
	
	
	public AskMoneyBean getCashOut(String userid,String reqwalletId,String resmobile,double amount,String ipImei,String aggreatorid,String agent,double surCharge){
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "","getCashOut()"+"|"+agent+
	  			"|"+amount);
		//logger.info("Inside ===================== method getCashOut(userId,reqwalletId, resmobile, amount, ipImei)"+amount);
		//String status="1001";
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
   	 	Transaction transaction = session.beginTransaction();
   	 	AskMoneyBean askMoneyBean=new AskMoneyBean();
   	 	askMoneyBean.setStatusCode("1001");
   	    walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
   	    try{
   		 if(amount<=0){
   			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Txn Amount Not Be Zero");
   			// logger.info("==========================Txn Amount Not Be Zero==================================");
          	 askMoneyBean.setStatusCode("7032");
          	 return askMoneyBean;
   		  }
   		 
		 if(getMobileNumber(reqwalletId).equals(resmobile)){
			 askMoneyBean.setStatusCode("7025");
          	 return askMoneyBean;
		 }
		 String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(resmobile,aggreatorid); 
		 if(recWalletid.equals("1001")){
	 		askMoneyBean.setStatusCode("7023");
	 	  }else{

	 		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Start askMoney DAO excution");
	 		  //logger.info("==========================Start askMoney DAO excution==================================");	
 			 String trxId=commanUtilDao.getTrxId("AMBF",aggreatorid);
 			 
 			 askMoneyBean.setTrxid(trxId);
			 askMoneyBean.setReqWalletId(reqwalletId);
			 askMoneyBean.setAggreatorid(aggreatorid);
			 askMoneyBean.setResMobile(resmobile);
			 askMoneyBean.setReqAmount(amount);
			 askMoneyBean.setSurCharge(surCharge);
			 askMoneyBean.setStatus("1");
			 askMoneyBean.setAskUserIp(ipImei);  
			 askMoneyBean.setAskUserAgent(agent); 
			 session.save(askMoneyBean);
				 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Inside user account login send otp for activate user and its wallet");
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Inside account wallet send otp for validate mobile on" + resmobile);
			 //logger.info("Inside *********************************************user account login====send otp for activate user and its wallet");
				//logger.info("Inside *********************************************account wallet====send otp for validate mobile on" + resmobile);
				String newOtp = oTPGeneration.generateOTP();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Inside "+newOtp);
				//logger.info("Inside *********************************************"+newOtp);
				if (!(newOtp == null)) {
					Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile",  resmobile).setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					insertOtp.executeUpdate();
				}
				transaction.commit();
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Inside otp send on mobile" + resmobile + "otp"+ newOtp);
					//logger.info("Inside ====================otp send on mobile==========" + resmobile + "===otp==="+ newOtp);
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", resmobile,commanUtilDao.getsmsTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp), "SMS",aggreatorid,reqwalletId,getName(reqwalletId),"OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				if(!(transaction.wasCommitted()))
	  				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "","wallet to Wallet transactin commit sucesfully");
				//logger.info("================wallet to Wallet transactin commit sucesfully================"); 
				 askMoneyBean.setStatusCode("1000");
						 
	 	  }	 
		 
   	 }catch (HibernateException e) {
         if (transaction.isActive())transaction.rollback();
         e.printStackTrace(); 
         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "getCashOut()","problem in getCashOut through wallet" + e.getMessage()+" "+e);
        // logger.debug("problem in getCashOut through wallet" + e.getMessage(),e);
         askMoneyBean.setStatusCode("7000");
      }catch (Exception ex) {
    	 if (transaction.isActive())transaction.rollback();
	         ex.printStackTrace(); 
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "","getCashOut()"+"|problem in getCashOut through wallet" + ex.getMessage()+" "+ex);
	        // logger.debug("problem in getCashOut through wallet=====" + ex.getMessage(), ex);
	         askMoneyBean.setStatusCode("7000");
	 }finally {
    	  session.close(); 	      
  	  }
   	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","getCashOut()","Inside askMoney return"+askMoneyBean.getStatusCode());
   	   // logger.info("Inside ====================askMoney=== return===="+askMoneyBean.getStatusCode());
	 return askMoneyBean;
		
	}
	
	public AskMoneyBean cashOutUpdate(AskMoneyBean askMoneyBean) {
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "", askMoneyBean.getTrxid(),"|"+"cashOutUpdate()");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|"+askMoneyBean.getResMobile());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|"+askMoneyBean.getOtp());
		/*logger.info("Inside ===================== method cashOutUpdate(askMoneyBean)" + askMoneyBean.getTrxid());
		logger.info("Inside ===================== method cashOutUpdate(askMoneyBean)" + askMoneyBean.getResMobile());
		logger.info("Inside ===================== method cashOutUpdate(askMoneyBean)" + askMoneyBean.getOtp());*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		askMoneyBean.setStatusCode("1001");
		try {
			AskMoneyBean askMoneyBeanOld = (AskMoneyBean) session.load(AskMoneyBean.class, askMoneyBean.getTrxid());
			if (askMoneyBeanOld == null || askMoneyBeanOld.getReqAmount() == 0) {
				askMoneyBean.setStatusCode("8802");
				return askMoneyBean;
			}

			if (new WalletUserDaoImpl().verifyOTP(askMoneyBeanOld.getResMobile(), askMoneyBeanOld.getAggreatorid(),	askMoneyBean.getOtp())) {
				if (getWalletBalance(getWalletIdByUserId(askMoneyBeanOld.getResMobile(),askMoneyBeanOld.getAggreatorid())) >= askMoneyBeanOld.getReqAmount()+askMoneyBeanOld.getSurCharge()) {

					String velocityCheckSt = velocityCheck(
							getWalletIdByUserId(askMoneyBeanOld.getResMobile(), askMoneyBeanOld.getAggreatorid()),
							askMoneyBeanOld.getReqAmount(), 16);
					if (velocityCheckSt.equals("1000")) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","","cashOutUpdate()", askMoneyBean.getTrxid(),"|"+"cashOutUpdate()");
						/*logger.info(
								"==========================Start cashOutUpdate excution==================================");*/
						String accepttrxId = commanUtilDao.getTrxId("AAMBF",askMoneyBeanOld.getAggreatorid());
						TransactionBean transactionBean = new TransactionBean();
						TransactionBean transactionBeanr = new TransactionBean();

						transactionBean.setTxnid(askMoneyBeanOld.getTrxid());
						transactionBean.setUserid(askMoneyBean.getId());
						transactionBean.setWalletid(
								getWalletIdByUserId(askMoneyBeanOld.getResMobile(), askMoneyBeanOld.getAggreatorid()));
						transactionBean.setTxncode(16);
						transactionBean.setTxncredit(0.0);
						transactionBean.setTxndebit(askMoneyBeanOld.getReqAmount());
						transactionBean.setSurCharge(askMoneyBeanOld.getSurCharge());
						transactionBean.setPayeedtl(getMobileNumber(askMoneyBeanOld.getReqWalletId()));
						transactionBean.setAggreatorid(askMoneyBeanOld.getAggreatorid());
						transactionBean.setResult("Success");
						transactionBean.setResptxnid(accepttrxId);
						transactionBean.setWalletuserip(askMoneyBeanOld.getAskUserIp());
						transactionBean.setWalletuseragent(askMoneyBeanOld.getAskUserAgent());

						transactionBeanr.setTxnid(accepttrxId);
						transactionBeanr.setUserid(askMoneyBean.getId());
						transactionBeanr.setWalletid(askMoneyBeanOld.getReqWalletId());
						transactionBeanr.setTxncode(16);
						transactionBeanr.setTxncredit(askMoneyBeanOld.getReqAmount());
						transactionBeanr.setTxndebit(0.0);
						transactionBeanr.setSurCharge(0.0);
						transactionBeanr.setPayeedtl(askMoneyBeanOld.getResMobile());
						transactionBeanr.setResult("Success");
						transactionBeanr.setAggreatorid(askMoneyBeanOld.getAggreatorid());
						transactionBeanr.setResptxnid(askMoneyBeanOld.getTrxid());
						transactionBeanr.setWalletuserip(askMoneyBeanOld.getAskUserIp());
						transactionBeanr.setWalletuseragent(askMoneyBeanOld.getAskUserAgent());

						session.merge(transactionBean);
						session.save(transactionBeanr);
						askMoneyBeanOld.setStatus("2");
						askMoneyBean.setReqAmount(askMoneyBeanOld.getReqAmount());
						askMoneyBean.setStatusCode("1000");

						try {
							walletConfiguration = commanUtilDao
									.getWalletConfiguration(askMoneyBeanOld.getAggreatorid());
							walletMastBean = walletUserDao.showUserProfile(askMoneyBean.getId());

							
							
							/*jh
							String smstemplet = commanUtilDao
									.getsmsTemplet("sendMoney", askMoneyBeanOld.getAggreatorid())
									.replace("<<APPNAME>>", walletConfiguration.getAppname())
									.replace("<<AMOUNT>>", Double.toString(askMoneyBeanOld.getReqAmount()))
									.replace("<<MOBILE>>", getMobileNumber(askMoneyBeanOld.getReqWalletId()))
									.replace("<<TXNNO>>", askMoneyBeanOld.getTrxid());
							logger.info("~~~~~~~~~~~~~~~~~~~~~cashOutUpdate  Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
							String mailSub = "Rs. " + Double.toString(askMoneyBeanOld.getReqAmount())
									+ " Send successfully to " + getMobileNumber(askMoneyBeanOld.getReqWalletId())
									+ ".";
							String mailContent = commanUtilDao
									.getmailTemplet("sendMoney", askMoneyBeanOld.getAggreatorid())
									.replace("<<APPNAME>>", walletConfiguration.getAppname())
									.replace("<<AMOUNT>>", Double.toString(askMoneyBeanOld.getReqAmount()))
									.replace("<<MOBILE>>", getMobileNumber(askMoneyBeanOld.getReqWalletId()))
									.replace("<<TXNNO>>", askMoneyBeanOld.getTrxid())
									.replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));*/
						
							
							
							String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",askMoneyBeanOld.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(askMoneyBeanOld.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBeanOld.getReqWalletId())).replace("<<TXNNO>>", askMoneyBeanOld.getTrxid()).replace("<<BENE NAME>>", getNameByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()), askMoneyBeanOld.getAggreatorid()));
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|cashOutUpdate Send SMS " + smstemplet);
							//logger.info("~~~~~~~~~~~~~~~~~~~~~cashOutUpdate  Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						     String mailSub = "You have made payment of Rs. " + String.format("%.2f",askMoneyBeanOld.getReqAmount())
								+ " to " + getMobileNumber(askMoneyBeanOld.getReqWalletId())
								+ ".";
						     //String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
						     String mailContent= commanUtilDao.getmailTemplet("sendMoney",askMoneyBeanOld.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>",String.format("%.2f", askMoneyBeanOld.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBeanOld.getReqWalletId())).replace("<<TXNNO>>", askMoneyBeanOld.getTrxid()).replace("<<BENE NAME>>", getNameByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()), askMoneyBeanOld.getAggreatorid())).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
						    
							
						
							
						     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|cashOutUpdate Send  MAIL" + mailContent);
							/*logger.info(
									"~~~~~~~~~~~~~~~~~~~~~cashOutUpdate Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);*/
							SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(),
									mailContent, mailSub, askMoneyBeanOld.getResMobile(), smstemplet, "Both",
									askMoneyBeanOld.getAggreatorid(),
									getWalletIdByUserId(askMoneyBeanOld.getResMobile(),
											askMoneyBeanOld.getAggreatorid()),
									walletMastBean.getName(),"NOTP");
//							Thread t = new Thread(smsAndMailUtility);
//							t.start();
							
							ThreadUtil.getThreadPool().execute(smsAndMailUtility);

							smstemplet = commanUtilDao.getsmsTemplet("askMoneyAccept", askMoneyBeanOld.getAggreatorid())
									.replace("<<NAME>>", getNameByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()), askMoneyBeanOld.getAggreatorid()))
									.replace("<<mobile>>", askMoneyBeanOld.getResMobile())
									.replace("<<AMOUNT>>", String.format("%.2f",askMoneyBeanOld.getReqAmount()))
									.replace("<<TXNNO>>", accepttrxId);
							
							
							
							
							
							
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|cashOutUpdate  recived SMS"+ smstemplet);
							/*logger.info(
									"~~~~~~~~~~~~~~~~~~~~~cashOutUpdate  recived SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
							mailSub = "Rs. " + String.format("%.2f",askMoneyBeanOld.getReqAmount())
									+ " recived successfully to " + askMoneyBeanOld.getResMobile() + ".";
							mailContent = commanUtilDao
									.getmailTemplet("askMoneyAccept", askMoneyBeanOld.getAggreatorid())
									.replace("<<NAME>>", getNameByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()), askMoneyBeanOld.getAggreatorid()))
									.replace("<<AMOUNT>>", String.format("%.2f",askMoneyBeanOld.getReqAmount()))
									.replace("<<MOBILE>>", askMoneyBeanOld.getResMobile())
									.replace("<<TXNNO>>", accepttrxId)
									.replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"))
									.replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","","cashOutUpdate()", askMoneyBean.getTrxid(),"|cashOutUpdate Send  MAIL" + mailContent);
						/*	logger.info(
									"~~~~~~~~~~~~~~~~~~~~~cashOutUpdate Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);*/
							smsAndMailUtility = new SmsAndMailUtility(
									getEmailByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()),
											askMoneyBeanOld.getAggreatorid()),
									mailContent, mailSub, getMobileNumber(askMoneyBeanOld.getReqWalletId()), smstemplet,
									"Both", askMoneyBeanOld.getAggreatorid(),
									getWalletIdByUserId(askMoneyBeanOld.getResMobile(),
											askMoneyBeanOld.getAggreatorid()),
									getNameByUserId(getMobileNumber(askMoneyBeanOld.getReqWalletId()),
											askMoneyBeanOld.getAggreatorid()),"NOTP");
//							t = new Thread(smsAndMailUtility);
//							t.start();
							
							ThreadUtil.getThreadPool().execute(smsAndMailUtility);

						} catch (Exception e) {
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "cashOutUpdate()", askMoneyBean.getTrxid(),"|problem in cashOutUpdate transfer while SMS Mail And GCM"+ e.getMessage()+" "+e);
							//logger.debug("problem in cashOutUpdate transfer while SMS Mail And GCM " + e.getMessage(),	e);
						}

					} else {// status= velocityCheckSt;
						askMoneyBeanOld.setStatus("2");
						askMoneyBean.setStatusCode(velocityCheckSt);
					}

				} else {
					askMoneyBeanOld.setStatus("9");
					askMoneyBean.setStatusCode("7024");
				}

			} else {
				askMoneyBean.setStatusCode("8801");
			}
			transaction.commit();

		} catch (Exception ex) {
			if (transaction.isActive())
				transaction.rollback();
			ex.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "", askMoneyBean.getTrxid(),"|problem in cashOutUpdate through wallet"+ ex.getMessage()+" "+ex);
			//logger.debug("problem in cashOutUpdate through wallet=====" + ex.getMessage(), ex);
			askMoneyBean.setStatusCode("7000");
		} finally {
			session.close();
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),askMoneyBean.getAggreatorid(),"","", "", askMoneyBean.getTrxid(),"|Inside cashOutUpdate return" + askMoneyBean.getStatusCode());
		//logger.info("Inside ====================cashOutUpdate=== return====" + askMoneyBean.getStatusCode());
		return askMoneyBean;

	}
	
	
	
	/**
	 * 
	 * @param userid
	 * @param reqwalletId
	 * @param resmobile
	 * @param amount
	 * @param ipImei
	 * @return
	 */
	
    public String askMoney(String userid,String reqwalletId,String resmobile,double amount,String ipImei,String aggreatorid,String agent){
    
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "","askMoney()"+"|"+agent);
    	logger.info("Inside ===================== method askMoney(userId,reqwalletId, resmobile, amount, ipImei)");
		String status="1001";
		factory=DBUtil.getSessionFactory();
   	 	Session session = factory.openSession();
   	 	Transaction transaction = session.beginTransaction();
   	 	AskMoneyBean askMoneyBean=new AskMoneyBean();
   	    walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
   	  
   	  try{
   		 
   		 if(amount<=0){
   			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "askMoney()","Txn Amount Not Be Zero");
   			 //logger.info("==========================Txn Amount Not Be Zero==================================");
         	return "7032";
   		  }
   		 
		 if(getMobileNumber(reqwalletId).equals(resmobile)){
			 return "7025";
		 }
		 
		 String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(resmobile,aggreatorid);
 		  if(recWalletid.equals("1001")){
 			status= "7023";
 		  }else{
 			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "","askMoney()");
 			 // logger.info("==========================Start askMoney DAO excution==================================");	
 			 String trxId=commanUtilDao.getTrxId("AMBF",aggreatorid);
 			 
 			 askMoneyBean.setTrxid(trxId);
			 askMoneyBean.setReqWalletId(reqwalletId);
			 askMoneyBean.setAggreatorid(aggreatorid);
			 askMoneyBean.setResMobile(resmobile);
			 askMoneyBean.setReqAmount(amount);
			 askMoneyBean.setStatus("1");
			 askMoneyBean.setAskUserIp(ipImei);  
			 askMoneyBean.setAskUserAgent(agent); 
			 session.save(askMoneyBean);
				 
			 String msgId=commanUtilDao.getTrxId("GCM",aggreatorid);
	  		   	if(!(commanUtilDao.checkLoginStatus(resmobile,aggreatorid)==1)){ 
	  		   		GCMPendingMsgBean gCMPendingMsgBean=new GCMPendingMsgBean();
	  		   		gCMPendingMsgBean.setGcmid(msgId);
	  		   		gCMPendingMsgBean.setReceivermobile(resmobile);
	  		   	    gCMPendingMsgBean.setMsg(commanUtilDao.getNotificationTemplet("askMoney").replace("<<mobile>>", getMobileNumber(reqwalletId)).replace("<<AMOUNT>>", Double.toString(amount)));
	  		   	    gCMPendingMsgBean.setMsgtype("request");
	  		    	gCMPendingMsgBean.setSendermobile(getMobileNumber(reqwalletId));
	  		        gCMPendingMsgBean.setTxnid(trxId);
	  		        gCMPendingMsgBean.setAggreatorid(aggreatorid);
	  		       session.saveOrUpdate(gCMPendingMsgBean);
	  		   	}
			 
	  		  GCMHistoryMsgBean gCMHistoryMsgBean=new GCMHistoryMsgBean();
				gCMHistoryMsgBean.setGcmid(msgId);
				gCMHistoryMsgBean.setReceiverUserId(getIdByUserId(resmobile,aggreatorid));
				gCMHistoryMsgBean.setReceivermobile(resmobile);
				gCMHistoryMsgBean.setMsg(commanUtilDao.getNotificationTemplet("askMoney").replace("<<mobile>>", getMobileNumber(reqwalletId)).replace("<<AMOUNT>>", Double.toString(amount)));
				gCMHistoryMsgBean.setMsgtype("request");
				gCMHistoryMsgBean.setSendermobile(getMobileNumber(reqwalletId));
				gCMHistoryMsgBean.setTxnid(trxId);
				gCMHistoryMsgBean.setStatus("0");
				gCMHistoryMsgBean.setAggreatorid(aggreatorid);
				session.saveOrUpdate(gCMHistoryMsgBean);
				if(!(transaction.wasCommitted()))
	  				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","askMoney()","wallet to Wallet transactin commit sucesfully");
				//logger.info("================wallet to Wallet transactin commit sucesfully================"); 
				 status= "1000";
				 try{
				 if(commanUtilDao.checkLoginStatus(resmobile,aggreatorid)==1){  
					  GCMDeviceBean gCMDeviceBean=(GCMDeviceBean)session.get(GCMDeviceBean.class, getIdByUserId(resmobile,aggreatorid));
		  			  GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), commanUtilDao.getNotificationTemplet("askMoney").replace("<<mobile>>", getMobileNumber(reqwalletId)).replace("<<AMOUNT>>", Double.toString(amount)),"Request",getMobileNumber(reqwalletId),trxId,resmobile,msgId,aggreatorid);
				 }}catch(Exception ex){
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","askMoney()","wallet not have device Id");
					// logger.info("===============wallet not have device Id===============");  
				 }
				// if(walletConfiguration.getSmssend()!=0){
						String smstemplet = commanUtilDao.getsmsTemplet("askmoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<NAME>>", getName(recWalletid)).replace("<<REQ NAME>>", getName(reqwalletId)).replace("<<AMOUNT>>", Double.toString(amount));
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "","askMoney()","walletid"+ smstemplet);
						//logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", resmobile, smstemplet, "SMS",aggreatorid,getWalletIdByUserId(resmobile,aggreatorid),getNameByUserId(resmobile,aggreatorid),"NOTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					//}
				
 		  } 
 		 }catch (HibernateException e) {
	         if (transaction.isActive())transaction.rollback();
	         e.printStackTrace(); 
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "askMoney()","problem in askMoney through wallet" + e.getMessage()+" "+e);
	        // logger.debug("problem in askMoney through wallet=====" + e.getMessage(), e);
	         status="7000";
	      }catch (Exception ex) {
	    	 if (transaction.isActive())transaction.rollback();
		         ex.printStackTrace(); 
		         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "askMoney()","problem in askMoney through wallet" + ex.getMessage()+" "+ ex);
		        // logger.debug("problem in askMoney through wallet=====" + ex.getMessage(), ex);
		         status="7000";
		 }finally {
	    	  session.close(); 	      
	  	  }
   	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userid, "", "askMoney()","Inside askMoney return"+status); 
   	 // logger.info("Inside ====================askMoney=== return===="+status);
  	    return status;
    }

	
    public String askMoneyUpdate(String userId,String trxId,String txnstatus,String ipImei,String msgId,String aggreatorid,String agent){
    	
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"askMoneyUpdate()"+"|"+agent); 
    	//logger.info("start excution ===================== method askMoneyUpdate(userid,trxId,txnstatus,ipImei,msgId)");
    	 String status="1001";
    	 factory=DBUtil.getSessionFactory();
	   	 Session session = factory.openSession();
	  	 Transaction transaction = session.beginTransaction();
	  	TransactionBean transactionBean=new TransactionBean();
		TransactionBean transactionBeanr=new TransactionBean();
		AskMoneyBean askMoneyBean;
		try{
			 walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
		if(txnstatus.equalsIgnoreCase("R")){			
			 askMoneyBean=(AskMoneyBean) session.load(AskMoneyBean.class,trxId);
    		 askMoneyBean.setStatus("9");
    		 
    			if(!(commanUtilDao.checkLoginStatus(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid)==1)){ 
	  		   		GCMPendingMsgBean gCMPendingMsgBean=new GCMPendingMsgBean();
	  		   		gCMPendingMsgBean.setGcmid(msgId);
	  		   		gCMPendingMsgBean.setReceivermobile(getMobileNumber(askMoneyBean.getReqWalletId()));
	  		   	    gCMPendingMsgBean.setMsg(commanUtilDao.getNotificationTemplet("").replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())));
	  		   	    gCMPendingMsgBean.setMsgtype("reply");
	  		    	gCMPendingMsgBean.setSendermobile(getMobileNumber(askMoneyBean.getResMobile()));
	  		       gCMPendingMsgBean.setTxnid(trxId);
	  		     gCMPendingMsgBean.setAggreatorid(aggreatorid);
	  		       session.saveOrUpdate(gCMPendingMsgBean);
 		       
	  		   	}
    			 GCMHistoryMsgBean gCMHistoryMsgBean=(GCMHistoryMsgBean) session.get(GCMHistoryMsgBean.class, msgId);
        		 gCMHistoryMsgBean.setStatus("1");
        		 session.update(gCMHistoryMsgBean);
        		 
        		 
        		 if(walletConfiguration.getSmssend()!=0){
        			 //Dear <<NAME>>, your friend  <<mobile>> has declined your request for money of  Rs. <<AMOUNT>>.
						//String smstemplet = commanUtilDao.getsmsTemplet("askMoneyReject",aggreatorid).replace("<<NAME>>", getName(askMoneyBean.getReqWalletId())).replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount()));
        			 String smstemplet = commanUtilDao.getsmsTemplet("askMoneyReject",aggreatorid).replace("<<NAME>>", getName(askMoneyBean.getReqWalletId())).replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount()));
        			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"walletid"+smstemplet);	
        			// logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", getMobileNumber(askMoneyBean.getReqWalletId()), smstemplet, "SMS",aggreatorid,getWalletIdByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid),getNameByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid),"NOTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}
        		 
    			status="7026";
 		}else{
 			askMoneyBean=(AskMoneyBean) session.load(AskMoneyBean.class,trxId); 
 			if(getWalletBalance(getWalletIdByUserId(askMoneyBean.getResMobile(),aggreatorid))>= askMoneyBean.getReqAmount()){
 				//String velocityCheckSt=velocityCheck(getWalletIdByUserId(askMoneyBean.getResMobile(),askMoneyBean.getAggreatorid()),askMoneyBean.getReqAmount(),3);
 				String velocityCheckSt=velocityCheck(askMoneyBean.getReqWalletId(),askMoneyBean.getReqAmount(),3);
 				if(velocityCheckSt.equals("1000")){
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"askMoneyUpdate()");
 					//logger.info("==========================Start askMoneyUpdate excution==================================");	
 					 String accepttrxId=commanUtilDao.getTrxId("AAMBF",aggreatorid);
 					
 					transactionBean.setTxnid(askMoneyBean.getTrxid());
 					transactionBean.setUserid(userId);
 					transactionBean.setWalletid(getWalletIdByUserId(askMoneyBean.getResMobile(),askMoneyBean.getAggreatorid()));
 	  	  			transactionBean.setTxncode(3);
 	  	  			transactionBean.setTxncredit(0.0);
 	  	  			transactionBean.setTxndebit(askMoneyBean.getReqAmount());
 	  	  			transactionBean.setPayeedtl(getMobileNumber(askMoneyBean.getReqWalletId()));
 	  	  			transactionBean.setAggreatorid(askMoneyBean.getAggreatorid());
 	  	  			transactionBean.setResult("Success");
 	  	  			transactionBean.setResptxnid(accepttrxId);
 	  	  			transactionBean.setWalletuserip(ipImei);
 					transactionBean.setWalletuseragent(agent);
 					
 					transactionBeanr.setTxnid(accepttrxId);
 					transactionBeanr.setUserid(userId);
 				    transactionBeanr.setWalletid(askMoneyBean.getReqWalletId());
 					transactionBeanr.setTxncode(3);
 	  	  		    transactionBeanr.setTxncredit(askMoneyBean.getReqAmount());
 	  	  		    transactionBeanr.setTxndebit(0.0);
 	  	  		    transactionBeanr.setPayeedtl(askMoneyBean.getResMobile());
 	  	  		    transactionBeanr.setResult("Success");
 	  	  		    transactionBeanr.setAggreatorid(askMoneyBean.getAggreatorid());
 	  	  		    transactionBeanr.setResptxnid(askMoneyBean.getTrxid());
 	  	  			transactionBeanr.setWalletuserip(ipImei);
 					transactionBeanr.setWalletuseragent(agent);
 					
 					
 	  	  		if(!(commanUtilDao.checkLoginStatus(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid)==1)){ 
	  		   		GCMPendingMsgBean gCMPendingMsgBean=new GCMPendingMsgBean();
	  		   		gCMPendingMsgBean.setGcmid(msgId);
	  		   		gCMPendingMsgBean.setReceivermobile(getMobileNumber(askMoneyBean.getReqWalletId()));
	  		   	    gCMPendingMsgBean.setMsg(commanUtilDao.getNotificationTemplet("askMoneyAccept").replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())));
	  		   	    gCMPendingMsgBean.setMsgtype("reply");
	  		    	gCMPendingMsgBean.setSendermobile(getMobileNumber(askMoneyBean.getResMobile()));
	  		    	gCMPendingMsgBean.setAggreatorid(askMoneyBean.getAggreatorid());
	  		       gCMPendingMsgBean.setTxnid(trxId);
	  		       session.saveOrUpdate(gCMPendingMsgBean);
 		       
	  		   	}
 	  	  			
 	   	  		 session.merge(transactionBean);
				 session.save(transactionBeanr);
				 askMoneyBean.setStatus("2");
				 status="7027";
				 
				 try{
				 walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
			     walletMastBean=walletUserDao.showUserProfile(userId);
				 
				 
				// String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBean.getReqWalletId())).replace("<<TXNNO>>", askMoneyBean.getTrxid());
			     
			     String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<NAME>>", getNameByUserId(askMoneyBean.getResMobile(), askMoneyBean.getAggreatorid())).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBean.getReqWalletId())).replace("<<TXNNO>>", askMoneyBean.getTrxid()).replace("<<BENE NAME>>",  getName(askMoneyBean.getReqWalletId()));
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"Ask Money  Send SMS"+smstemplet);
			     //logger.info("~~~~~~~~~~~~~~~~~~~~~Ask Money  Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
			     String mailSub= "You have made payment of Rs. "+String.format("%.2f",askMoneyBean.getReqAmount())+" to "+getMobileNumber(askMoneyBean.getReqWalletId())+".";
			     //String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBean.getReqWalletId())).replace("<<TXNNO>>", askMoneyBean.getTrxid()).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
			     String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<NAME>>", getNameByUserId(askMoneyBean.getResMobile(), askMoneyBean.getAggreatorid())).replace("<<AMOUNT>>", String.format("%.2f",askMoneyBean.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBean.getReqWalletId())).replace("<<TXNNO>>", askMoneyBean.getTrxid()).replace("<<BENE NAME>>",  getName(askMoneyBean.getReqWalletId())).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(askMoneyBean.getReqWalletId())));
				    
					
			     
			     
			     
			     
			     
			     
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"Ask Money Send  MAIL" + mailContent);
			     //logger.info("~~~~~~~~~~~~~~~~~~~~~Ask Money Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
			     SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, askMoneyBean.getResMobile(), smstemplet, "Both",aggreatorid,getWalletIdByUserId(askMoneyBean.getResMobile(),askMoneyBean.getAggreatorid()),walletMastBean.getName(),"NOTP");
//				 Thread t = new Thread(smsAndMailUtility);
//				 t.start();
				 
			     ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			     
				 //smstemplet =commanUtilDao.getsmsTemplet("askMoneyAccept",aggreatorid).replace("<<NAME>>", getName(askMoneyBean.getReqWalletId())).replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())).replace("<<TXNNO>>", accepttrxId);
				 smstemplet =commanUtilDao.getsmsTemplet("askMoneyAccept",aggreatorid).replace("<<NAME>>", getName(askMoneyBean.getReqWalletId())).replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount()));
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", trxId,"Ask Money  recived SMS"+smstemplet);
				// logger.info("~~~~~~~~~~~~~~~~~~~~~Ask Money  recived SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
			     mailSub= "Rs. "+String.format("%.2f",askMoneyBean.getReqAmount())+" recived successfully to "+askMoneyBean.getResMobile()+".";
			     mailContent= commanUtilDao.getmailTemplet("askMoneyAccept",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", String.format("%.2f",Double.toString(askMoneyBean.getReqAmount()))).replace("<<MOBILE>>", askMoneyBean.getResMobile()).replace("<<TXNNO>>", accepttrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","askMoneyUpdate()", trxId,"Ask Money Send  MAIL"+ mailContent);
			    // logger.info("~~~~~~~~~~~~~~~~~~~~~Ask Money Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
			      smsAndMailUtility = new SmsAndMailUtility(getEmailByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid), mailContent,mailSub, getMobileNumber(askMoneyBean.getReqWalletId()), smstemplet, "Both",aggreatorid,getWalletIdByUserId(askMoneyBean.getResMobile(),askMoneyBean.getAggreatorid()),getNameByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid),"NOTP");
//				 t = new Thread(smsAndMailUtility);
//				 t.start();
			      
			      ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				 
 				}catch (Exception e) {
 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","askMoneyUpdate()", trxId,"problem in askMoneyUpdate transfer while SMS Mail And GCM"+ e.getMessage()+" "+e);
 					//logger.debug("problem in askMoneyUpdate transfer while SMS Mail And GCM " + e.getMessage(), e);
 				}
				 
 					
				/* if(walletConfiguration.getSmssend()!=0){
						
					 String smstemplet = commanUtilDao.getsmsTemplet("askMoneyAccept").replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount()));
						logger.info("~~~~~~~~~~~~~~~~~~~~~walletid~~~~~~~~~~~~~~~~~~~````" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", getMobileNumber(askMoneyBean.getReqWalletId()), smstemplet, "SMS",aggreatorid,getWalletIdByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid),getNameByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid));
						Thread t = new Thread(smsAndMailUtility);
						t.start();
					
				 
				 
				 }*/	
 					
				 GCMHistoryMsgBean gCMHistoryMsgBean=(GCMHistoryMsgBean) session.get(GCMHistoryMsgBean.class, msgId);
	    		 gCMHistoryMsgBean.setStatus("1");
	    		 session.update(gCMHistoryMsgBean);
 				
 				}else{
 	  	  			status= velocityCheckSt;
 	  	  			} 	
 			}else{
  				status= "7024";
  			}
 		}
		 transaction.commit();
		 
		 
		 if(!status.equalsIgnoreCase("7023") ){
			 transaction = session.beginTransaction();
    		 if(txnstatus.equalsIgnoreCase("R") && commanUtilDao.checkLoginStatus(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid)==1){
    			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "askMoneyUpdate()", trxId,"Send Notification For money Rejected notification");
    			 //logger.info("******************************Send Notification For money Rejected notification***********************");
    			 GCMDeviceBean gCMDeviceBean=(GCMDeviceBean)session.get(GCMDeviceBean.class, getIdByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),askMoneyBean.getAggreatorid()));
    			 GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), commanUtilDao.getNotificationTemplet("").replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())),"reply",askMoneyBean.getResMobile(),trxId,getMobileNumber(askMoneyBean.getReqWalletId()),msgId,askMoneyBean.getAggreatorid());
    		 }else if(txnstatus.equalsIgnoreCase("A") && commanUtilDao.checkLoginStatus(getMobileNumber(askMoneyBean.getReqWalletId()),aggreatorid)==1){
    			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","askMoneyUpdate()", trxId,"Send Notification For money Accepted notification");
    			 //logger.info("******************************Send Notification For money Accepted notification***********************");
    			 GCMDeviceBean gCMDeviceBean=(GCMDeviceBean)session.get(GCMDeviceBean.class,getIdByUserId(getMobileNumber(askMoneyBean.getReqWalletId()),askMoneyBean.getAggreatorid()) );
				 GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), commanUtilDao.getNotificationTemplet("askMoneyAccept").replace("<<mobile>>", askMoneyBean.getResMobile()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())),"reply",askMoneyBean.getResMobile(),trxId,getMobileNumber(askMoneyBean.getReqWalletId()),msgId,askMoneyBean.getAggreatorid());
			 }
    	}
		 
		
	}catch (HibernateException e) {
	         if (transaction!=null)transaction.rollback();
	         e.printStackTrace(); 
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","askMoneyUpdate()", trxId,"problem in askMoneyUpdate through wallet" + e.getMessage()+" "+e);
	         //logger.debug("problem in askMoneyUpdate through wallet=====" + e.getMessage(), e);
	         status= "7000";
	 }catch (Exception ex) {
	    	 if (transaction!=null)transaction.rollback();
		         ex.printStackTrace(); 
		         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "askMoneyUpdate()", trxId,"problem in askMoneyUpdate through wallet" + ex.getMessage()+" "+ex);
		        // logger.debug("problem in askMoneyUpdate through wallet=====" + ex.getMessage(), ex);
		         status= "7000";
	 }finally {
	    	  session.close(); 	      
	  	  }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","askMoneyUpdate()", trxId,"Stop excution askMoneyUpdate with return"+status); 
		//logger.info("stop excution ===================== askMoneyUpdate with return"+status);	 
    	 
    	 
    return status;
    }
	

    
    
    
    public List showPassbook(String walletid,String stDate,String endDate){

    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","" ,"showPassbook()");
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "","method stDate"+stDate);
    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
    	//logger.info("start excution ===================== method stDate"+stDate);
		List passbook=new ArrayList();
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			 StringBuilder serchQuery=new StringBuilder();
			 serchQuery.append(" from PassbookBean where walletid=:walletid");
			 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
				 serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
			 }else{
				//serchQuery.append(" and DATE(txndate)=DATE(NOW())");
				
				//serchQuery.append(" order by txndate DESC");
				//serchQuery.append(" limit 0,100");
				
			 }
			 
			serchQuery.append(" order by rowid DESC");
			 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"","", "showPassbook()","showPassbook"+serchQuery);
			 //logger.info("showPassbook***********************************************~~"+ serchQuery);
			 Query query=session.createQuery(serchQuery.toString());
			 query.setString("walletid", walletid);
			 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
			 }else{
			 query.setFirstResult(0);
			 query.setMaxResults(100);
			 session.evict(passbook);
			 }
			 passbook=query.list();
			 //Collections.reverse(passbook);
		}catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showPassbook()","checking showPassbook error"+ e.getMessage()+" "+e);
			  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showPassbook()","stop excution getTrxdtl return"+passbook.size());
		//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
		return passbook;
    }
    
    
    /////// Developed By Ankit Budhiraja ////////////////
    
    
public List pgTransactionsUpdate(String txnId) {
		
		

		if (txnId.charAt(0) == '"' && txnId.charAt(txnId.length() - 1) == '"') {
			txnId = txnId.substring(1, txnId.length() - 1);
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "", "pgTransactionsUpdate()");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "", "");
		List list = new ArrayList();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			String str = "from PaymentGatewayTxnBean where trxId=:txnId";

			Query query = session.createQuery(str);
			query.setString("txnId", txnId);
			list = query.list();
			System.out.println("SIZE  " + list.size());
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "pgTransactionsUpdate",
					"updating  PG transactions error" + e.getMessage() + " " + e);
			logger.debug("checking PG Transaction*****************error************** " + e.getMessage(), e);
		} finally {

			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "pgTransactionsUpdate()",
				"" + "");

		return list;
	}

	public List<WalletMastBean> pgTxnUpdate(String txnId, String userId) {
		if (txnId.charAt(0) == '"' && txnId.charAt(txnId.length() - 1) == '"') {
			txnId = txnId.substring(1, txnId.length() - 1);
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "", "pgTransactionsUpdate()");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "", "");

		List list2 = new ArrayList();

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			// String str="select w.managerName,w.walletid from WalletMastBean w where w.id
			// in("+ userId + ")";
			String str = "select w.managerName,w.walletid from WalletMastBean w where w.id=:userId";
			Query query = session.createQuery(str);
			query.setString("userId", userId);
			list2 = query.list();

			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "pgTransactionsUpdate",
					"updating  PG transactions error" + e.getMessage() + " " + e);
			logger.debug("checking PG Transaction*****************error************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", txnId, "", "", "pgTransactionsUpdate()",
				"" + "");
		return list2;
	}
	
	
	
public List getPayIdThroughAggID(String aggreatorId) {

List payId=null;

factory = DBUtil.getSessionFactory();
Session session = factory.openSession();
Transaction transaction = session.beginTransaction();
try {
	String str = "Select pgAgId from WalletConfiguration where aggreatorid=:txnId";

	Query query = session.createQuery(str);
	query.setString("txnId", aggreatorId);
 payId=query.list();
	
	transaction.commit();

} catch (Exception e) {
	e.printStackTrace();
	transaction.rollback();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", aggreatorId, "", "", "pgTransactionsUpdate",
			"updating  PG transactions error" + e.getMessage() + " " + e);
	logger.debug("checking PG Transaction*****************error************** " + e.getMessage(), e);
} finally {

	session.close();
}


return payId;
}


public String getStatusChecker(String payId,String txnId) {

String resp=null;
try {
	URL url= new URL("https://merchant.bhartipay.com/crm/services/paymentServices/getStatusAPI?PAY_ID=" + payId + "&ORDER_ID="+txnId);
	String readLine=null;
	HttpURLConnection connection =(HttpURLConnection) url.openConnection(); 
	connection.setRequestMethod("GET");
	int responseCode=connection.getResponseCode();
	
	if(responseCode == HttpURLConnection.HTTP_OK) {
		BufferedReader in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuffer response=new StringBuffer();
		while((readLine = in.readLine()) != null) {
			response.append(readLine);
		} in.close();
		
		
		 resp= response.toString();
	}
	
	
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}


return resp;

}



	@Override
	public String txnUpdate(TxnInputBean txnInputBean) {
		String response;
		/*
		 * TreeMap<String, String> obj = new TreeMap<String, String>();
		 * 
		 * obj.put("userId",txnInputBean.getUserId());
		 * obj.put("walletId",txnInputBean.getWalletId());
		 * obj.put("aggregatorId",txnInputBean.getAggreatorid());
		 * obj.put("CHECKSUMHASH", txnInputBean.getCHECKSUMHASH());
		 * 
		 * int statusCode = WalletSecurityUtility.checkSecurity(obj); if (statusCode !=
		 * 1000) { return response="" + statusCode; }
		 */

		String txnId=txnInputBean.getTxnId();
		
		if (txnId.charAt(0) == '"' && txnId.charAt(txnId.length() - 1) == '"') {
			txnId = txnId.substring(1, txnId.length() - 1);
		}
		String userId = null;
		String aggreatorid = null;
		double txnamount = 0;
		String managerName = null;
		String walletid = null;
		String txnResult=null;
		String mode=null;
		
		String pgDateTime="";
		String pgResCode="";
		String pgMopType="";
		String pgCardMask="";
		String currencyCode="";
		String pgAmount="";
		String pgCustEmail="";
		String pgTxnId="";
		String pgTxnType="";
		String pgAcqId="";
		String pgPayId="";
		String pgOrderid="";

		String pgStatus="";
		String pgOrigintxnId="";
		StringBuffer other=null;
		PgReturnResponse pgRes=null;
		
		factory = DBUtil.getSessionFactory();
		Session session2 = factory.openSession();
		Transaction transaction = session2.beginTransaction();
		try {
		
		List<PaymentGatewayTxnBean> paymentGatewayTxnBeanList = pgTransactionsUpdate(txnId);
		if(paymentGatewayTxnBeanList.size()>0)
		{
			for (PaymentGatewayTxnBean ab : paymentGatewayTxnBeanList) {
				userId = ab.getUserId();
				aggreatorid = ab.getAggreatorid();
				txnamount = ab.getTrxAmount();
				txnResult=ab.getTrxResult();
			}
		
			List payIdList=getPayIdThroughAggID(txnInputBean.getAggreatorid());
			if(payIdList.size()>0) {
			String payId=(String) payIdList.get(0);
			
			String responseOfStatusChecker = getStatusChecker(payId,txnInputBean.getTxnId());
			JSONObject jsonObj = new JSONObject(responseOfStatusChecker.toString());
			Object st=jsonObj.get("status");
			try {
			  if(jsonObj!=null) {
				   
				 if(jsonObj.get("paymentType")!=null )
				    mode=jsonObj.get("paymentType").toString();
				 if(jsonObj.get("dateTime")!=null)
				    pgDateTime=jsonObj.get("dateTime").toString();
				 if(jsonObj.get("responseCode")!=null)
				    pgResCode=jsonObj.get("responseCode").toString();
				 if(jsonObj.get("moptype")!=null)
				    pgMopType=jsonObj.get("moptype").toString();
				 if(jsonObj.get("cardmask")!=null)
				    pgCardMask=jsonObj.get("cardmask").toString();
				 if(jsonObj.get("currency")!=null)
				    currencyCode=jsonObj.get("currency").toString();
				 if(jsonObj.get("status")!=null)
				    pgStatus=jsonObj.get("status").toString();
				 if(jsonObj.get("approvedAmount")!=null)
				    pgAmount=jsonObj.get("approvedAmount").toString();
				 if(jsonObj.get("custEmail")!=null)
				    pgCustEmail=jsonObj.get("custEmail").toString();
				 if(jsonObj.get("transactionId")!=null)
				    pgTxnId=jsonObj.get("transactionId").toString();
				 if(jsonObj.get("txnType")!=null)
				    pgTxnType=jsonObj.get("txnType").toString();
				 if(jsonObj.get("acqId")!=null)
				    pgAcqId=jsonObj.get("acqId").toString();
				 if(jsonObj.get("payId")!=null)
				    pgPayId=jsonObj.get("payId").toString();
				 if(jsonObj.get("orderId")!=null)
				    pgOrderid=jsonObj.get("orderId").toString();
				 if(jsonObj.get("orginTxnId")!=null)
				    pgOrigintxnId=jsonObj.get("orginTxnId").toString(); 
				}
		         
		         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " pgStatus "+pgStatus, " pgAmount "+pgAmount, " pgTxnId "+pgTxnId, " mode "+mode , " pgOrderid "+pgOrderid,
		     			"Insert  PG transactions response into PgReturnResponse pgOrigintxnId "+pgOrigintxnId);
				}catch(Exception e)
				{
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " pgStatus "+pgStatus, " pgAmount "+pgAmount, " pgTxnId "+pgTxnId, " mode "+mode , " pgOrderid "+pgOrderid,
			     			"Problem in to Insert  PG transactions response into PgReturnResponse "+e.getMessage());	
				}
			
			if(!st.equals("Captured")) {
				return response="this txnId is not Captured.";
			  }
			}
			
			if(txnResult.equals("Success")) {
			return response= "Amount for Transaction ID "+txnId+" is already credited.";
			}
		}else
			return response= "Invalid Transaction ID "+txnId;

        List walletMastBeanList = pgTxnUpdate(txnId, userId);

		if(walletMastBeanList.size()>0)
		{
		Object[] array = (Object[]) walletMastBeanList.get(0);
        managerName = array[0].toString();
		walletid = array[1].toString();
		
		String str = "update WalletMastBean w set w.managerName='Ramesh Kharb' where w.id=:userId";
		Query query = session2.createQuery(str);
		query.setString("userId", userId);
		query.executeUpdate();

		transaction.commit();

		//String updatepaymentgatewaytxn = updatepaymentgatewaytxn(txnId);
		pgRes=new PgReturnResponse(pgDateTime,pgResCode,"",pgMopType,pgCardMask,currencyCode,"",pgStatus,pgAmount,"","",pgCustEmail,"",pgTxnId,pgAcqId,pgTxnType,"","",mode,"",pgPayId,pgOrderid,"","");

		String rres=savePgReturnResponse(pgRes);
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " pgStatus "+pgStatus, " pgAmount "+pgAmount, " savePgReturnResponse rres "+rres, " mode "+mode , " pgOrderid "+pgOrderid,"  ");
		//if (updatepaymentgatewaytxn.equals("success")) {
			
			String callCreditwallet = callCreditwallet(aggreatorid, walletid, txnamount, txnId);
			if (callCreditwallet.equals("success")) {
				String updatepaymentgatewaytxn = updatepaymentgatewaytxnMode(txnId,mode,pgCardMask);
				String updateWalletMastAgain = updateWalletMastAgain(managerName, userId);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " pgStatus "+pgStatus, " pgAmount "+pgAmount, " savePgReturnResponse rres "+rres, " mode "+mode , " pgOrderid "+pgOrderid,
		     			"  updatepaymentgatewaytxnMode "+updatepaymentgatewaytxn+"  updateWalletMastAgain "+updateWalletMastAgain);
				
				if (updateWalletMastAgain.equals("success")) {
					response = "txnId : " + txnId + " credited successfully with amount: " + txnamount;
					//transaction.commit();
				} else
					response = updateWalletMastAgain;
			} else
				response = callCreditwallet;
		/*} else
			response = updatepaymentgatewaytxn;
		*/ 
		}else
		 {
			 response = "Something went wrong.Please contact to admin " ;
		 }
		
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			logger.debug("updating walletmast table*****************error************** " + e.getMessage(), e);
			//response = "error";
			//response ="Exception in updating wallet Mast : " + e.toString();
			//return response;
			response ="Something went wrong. Please try later";
			return response;
			
		} finally {
			session2.close();
		}
		return response;

	}
	@Override
	public List getAmountPopup(String txnIdPassed) {
		if (txnIdPassed.charAt(0) == '"' && txnIdPassed.charAt(txnIdPassed.length() - 1) == '"') {
			txnIdPassed = txnIdPassed.substring(1, txnIdPassed.length() - 1);
		}
		
		String userId = null;
		String aggreatorid = null;
		String txnamount = null;
		String managerName = null;
		String walletid = null;
		List<String> list2=new ArrayList<String>();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			
			String str = "select w.trxAmount,w.userId from PaymentGatewayTxnBean w where w.trxId=:txnIdPassed";
			Query query = session.createQuery(str);
			query.setString("txnIdPassed", txnIdPassed);
			
			List paymentGatewayTxnBeanList=query.list();
			Object[] array = (Object[]) paymentGatewayTxnBeanList.get(0);
			System.out.println(array.length);

			/*
			 * for(Object obj:array) { System.out.println(obj); }
			 */
			txnamount = array[0].toString();
			userId = array[1].toString();
			/*
			 * for (String ab : paymentGatewayTxnBeanList) { userId = ab.getUserId();
			 * 
			 * txnamount = ab.getTrxAmount();
			 * 
			 * } list2.add(userId); list2.add(txnamount);
			 */
			
			
			list2.add(userId); list2.add(txnamount);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

			logger.debug("updating paymentgatewaytxn table*****************error************** " + e.getMessage(), e);
			
		} finally {
			session.close();
		}
		return list2;
	}
	public String updatepaymentgatewaytxn(String txnId) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			String str = "UPDATE PaymentGatewayTxnBean u SET u.trxResult='Success' where u.trxId=:txnId";
			Query query = session.createQuery(str);
			query.setString("txnId", txnId);
			query.executeUpdate();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

			logger.debug("updating paymentgatewaytxn table*****************error************** " + e.getMessage(), e);
			return "Exception in updating Txnresult to success" +e.toString();
		} finally {
			session.close();
		}
		return "success";
	}

	// Changes on 24-05-2021 By MANI
	
	public String updatepaymentgatewaytxnMode(String txnId,String mode,String pgCardMask) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		String response="FAIL";
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " txnId "+txnId, "  ", " updatepaymentgatewaytxnMode  ", " mode "+mode , "","  updatepaymentgatewaytxnMode ");
		try {
			 PaymentGatewayTxnBean pgData = (PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
			  if(pgData!=null)
			  {
				  pgData.setPaymentMode(mode);
				  pgData.setCardMask(pgCardMask);
				  pgData.setCreditStatus("CREDIT");
				  pgData.setTrxResult("Success");
				  session.saveOrUpdate(pgData);
				  transaction.commit();
				  response="success";
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), " txnId "+txnId, "  ", " updatepaymentgatewaytxnMode  ", " mode "+mode , "","  response  "+response);
					
			  }
			 
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

			logger.debug("updating Table updatepaymentgatewaytxnMode(String txnId,String mode)  *****************error************** " + e.getMessage(), e);
			
			return "Something went wrong with transaction "+txnId+". Please try later." ;
		} finally {
			session.close();
		}
		return response;
	}
	
	public String callCreditwallet(String aggreatorid, String walletid, double txnamount, String txnId) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			String str = "CALL creditwallet('" + aggreatorid + "','" + walletid + "'," + txnamount + ",'pgTxn','"
					+ txnId + "',92)";
			Query query = session.createSQLQuery(str);
			query.executeUpdate();

		transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

			logger.debug("calling creditwallet procedure*****************error************** " + e.getMessage(), e);
			return "Exception in credit wallet Procedure" + e.toString();
		} finally {
			session.close();
		}
		return "success";
	}

	public String updateWalletMastAgain(String managerName, String userId) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {               // "update WalletMastBean w set w.managerName='Ramesh Kharb' where w.id=:userId"
			String str = "update WalletMastBean w SET w.managerName='" + managerName + "' where w.id=:userId";
			Query query = session.createQuery(str);
			query.setString("userId", userId);
			query.executeUpdate();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		transaction.rollback();

			logger.debug("updating WalletMastAgain *****************error************** " + e.getMessage(), e);
			return "Exception in updating ManagerName to the original Manager Name" + e.toString();
		} finally {
			session.close();
		}
		return "success";
	}

    ////////////// END of Development By Ankit Budhiraja////////////////////
	
	
	
    public List showCashBackPassbook(String walletid,String stDate,String endDate){
    	
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showCashBackPassbook()","showCashBackPassbook()");
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "showCashBackPassbook()","method stDate"+stDate);
    	//logger.info("start excution ===================== method showCashBackPassbook(walletid)"+walletid);
    	//logger.info("start excution ===================== method stDate"+stDate);
		List passbook=new ArrayList();
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			 StringBuilder serchQuery=new StringBuilder();
			 serchQuery.append(" select * from  cashbackpassbook where walletid=:walletid");
			 
			 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
				 serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
				 serchQuery.append(" order by txndate DESC");
			 }else{
				 serchQuery.append("  ORDER BY txndate DESC LIMIT 20");
				
			 }
			
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "","showCashBackPassbook()"+serchQuery);
			 logger.info("showCashBackPassbook***********************************************~~"+ serchQuery);
			 
			 SQLQuery  query=session.createSQLQuery(serchQuery.toString());
			 query.setString("walletid", walletid);
			 query.setResultTransformer(Transformers.aliasToBean(CashBackPassbookBean.class));
			 
			 
			 /*Query query=session.createQuery(serchQuery.toString());
			 query.setString("walletid", walletid);*/
			 passbook=query.list();
			 //Collections.reverse(passbook);
		}catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showCashBackPassbook()","checking showCashBackPassbook error"+ e.getMessage()+" "+e);
			  //logger.debug("checking showCashBackPassbook*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "showCashBackPassbook()","stop excution showCashBackPassbook return"+passbook.size());
		//logger.info("stop excution *********************************showCashBackPassbook******** return**********"+passbook.size());  
		return passbook;
    }
    
    
    
    
     public List showOrder(String walletid,String stDate,String endDate){

    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","" ,"showOrder()");
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "","method stDate"+stDate);
    	// logger.info("start excution ******************************************* method showOrder(walletid)"+walletid);
    	//logger.info("start excution ******************************************* method stDate"+stDate);
		List passbook=new ArrayList();
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try {
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			 StringBuilder serchQuery=new StringBuilder();
			 serchQuery.append(" from PassbookBean where txndebit>0 and  walletid=:walletid");
			 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
				serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
			 }else{
				// serchQuery.append(" LIMIT 200");
			 }
			serchQuery.append(" order by txndate DESC");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showOrder()","serchQuery"+serchQuery);
			//logger.info("serchQuery **************************************************"+ serchQuery);
			Query query=session.createQuery(serchQuery.toString());
			query.setString("walletid", walletid);
			passbook=query.list();
			
		}catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "", "showOrder()","checking showOrder"+e.getMessage()+" "+e);
			  //logger.debug("checking showOrder******************************* " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "","showOrder()","stop excution getTrxdtl return"+passbook.size());
		//logger.info("stop excution ====================getTrxdtl=== return===="+passbook.size());  
		return passbook;
    }
    
 	
/**
 * 	
 * @param walletId
 * @return
 */
	
	
   	public String getName(String walletId){
		String userName=null;
		
  	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getName()");
  	
		//logger.info("getName() method started for retutn name by walletId="	+ walletId );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			Query query = session
					.createQuery("from WalletMastBean where walletid= :walletid and userstatus = :walletStatus");
			query.setString("walletid", walletId);
			query.setString("walletStatus", "A");
			List list = query.list();
			if (list != null && list.size() != 0) {
				userName=((WalletMastBean) list.get(0)).getName();
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","getName()","get name by  walletid fail from TransactionDaoImpl " + e.getMessage()+" "+e);
			//logger.debug("get name by  walletid fail from TransactionDaoImpl " + e.getMessage(), e);
				} finally {
			session.close();
		}
		return userName;
		}
	
	
	/**
	 * for wallet ID by user mobile /email
	 * @return walletId
	 */
	public String getWalletIdByUserId(String mobileEmail,String aggreatorid){
		String walletid="1001";
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getWalletIdByUserId()"+"|"+mobileEmail);
		//logger.info("getWalletIdByMobile() method started for retutn walletid by mobile/email="	+ mobileEmail );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			Query query = session
					.createQuery("select walletid from WalletMastBean where (mobileno= :userid or emailid=:userid or  id=:userid) and userstatus = :walletStatus and aggreatorid=:aggreatorid");
			query.setString("userid", mobileEmail);
			query.setString("walletStatus", "A");
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() == 1) {
				walletid=	(String) list.get(0);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getWalletIdByUserId()","Walletid :"+ walletid);
				//logger.info("==============Wallet Id=============================="+walletid);
			}else {
				throw new Exception("More than one wallet is associated with this id : "+mobileEmail+" aggreatorid : "+aggreatorid);
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getWalletIdByUserId()","checking walletid fail from getWalletIdByUserId"+ e.getMessage()+" "+e);
			//logger.debug("checking walletid fail from getWalletIdByUserId******************************* " + e.getMessage(), e);
		} finally {
			session.close();
		}
		return walletid;
	}
	
	
	
	public String getNameByUserId(String mobileEmail,String aggreatorid){
		String name="1001";
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getNameByUserId()"+"|"+mobileEmail);
		//logger.info("getNameByUserId() method started for retutn name by mobile/email="	+ mobileEmail );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			Query query = session
					.createQuery("select name from WalletMastBean where (mobileno= :userid or emailid=:userid) and userstatus = :walletStatus and aggreatorid=:aggreatorid");
			query.setString("userid", mobileEmail);
			query.setString("walletStatus", "A");
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				name=	(String) list.get(0);
				logger.info("==============name=============================="+name);
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getNameByUserId()","checking walletid fail from getWalletIdByUserId"+e.getMessage()+" "+e);
			logger.debug("checking walletid fail from getWalletIdByUserId******************************* " + e.getMessage(), e);
		} finally {
			session.close();
		}
		return name;
	}
	
	
	
	
	
	/**
	 * for wallet ID by user mobile /email
	 * @return walletId
	 */
	public String getIdByUserId(String mobileEmail,String aggreatorid){
		String walletid="1001";
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getIdByUserId()"+"|"+mobileEmail);
		//logger.info("getIdByUserId() method started for retutn id by mobile/email="	+ mobileEmail );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			//Query query = session.createQuery("select id from WalletMastBean where (mobileno= :userid or emailid=:userid) and userstatus = :walletStatus and aggreatorid=:aggreatorid");
			Query query = session.createQuery("select id from WalletMastBean where (mobileno= :userid or emailid=:userid or id=:userid) and  aggreatorid=:aggreatorid");
			query.setString("userid", mobileEmail);
			//query.setString("walletStatus", "A");
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				walletid=	(String) list.get(0);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getIdByUserId()","Wallet Id"+walletid);
				//logger.info("==============Wallet Id=============================="+walletid);
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getIdByUserId()","checking id fail from getIdByUserId"+e.getMessage()+" "+e);
			//logger.debug("checking id fail from getIdByUserId********************************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
		return walletid;
	}
	
	
	/**
	 * for emailId by user mobile /email
	 * @return walletId
	 */
	public String getEmailByUserId(String mobile,String aggreatorid){
		String emailid="";
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getEmailByUserId()"+"|"+mobile);
		//logger.info("getEmailByUserId() method started for retutn id by mobile="	+ mobile );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			Query query = session
					.createQuery("select emailid from WalletMastBean where mobileno= :userid  and userstatus = :walletStatus and aggreatorid=:aggreatorid");
			query.setString("userid", mobile);
			query.setString("walletStatus", "A");
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				emailid=	(String) list.get(0);
			
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getEmailByUserId()","emailid :"+emailid);
				//logger.info("==============emailid=============================="+emailid);
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","getEmailByUserId()","checking id fail from getEmailByUserId"+e.getMessage()+" "+e);
			//logger.debug("checking id fail from getEmailByUserId********************************** " + e.getMessage(), e);
		} finally {
			session.close();
		}
		return emailid;
	}
	
	
	
	
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	
	public String getMobileNumber(String walletId){
		String mobileNo=null;
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMobileNumber()");
		//logger.info("getMobileNumber() method started for retutn mobile by walletId="	+ walletId );
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try {
			Query query = session
					.createQuery("from WalletMastBean where walletid= :walletid and userstatus = :walletStatus");
			query.setString("walletid", walletId);
			query.setString("walletStatus", "A");
			List list = query.list();
			if (list != null && list.size() != 0) {
				mobileNo=((WalletMastBean) list.get(0)).getMobileno();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","getMobileNumber()","mobile no"+mobileNo);
				//logger.info("==============mobile no=============================="+mobileNo);
			}
			
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","getMobileNumber()","get Mobile by  walletid fail from TransactionDaoImpl"+ e.getMessage()+""+e);
			//logger.debug("get Mobile by  walletid fail from TransactionDaoImpl " + e.getMessage(), e);
				} finally {
			session.close();
		}
		return mobileNo;
		}
	
	
	public String saveDispute(String userId,String walletId,String txnId,String disputeReason){
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,userId, "",txnId ,"saveDispute()");
		//logger.info("start Excute==============================================saveDispute(userId,walletId,txnId,disputeReason) method");
		String status="1001";
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try {
			
			if(validateTrx(txnId,walletId)){
				 String disputeId=commanUtilDao.getTrxId("DISPUTE","OAGG001050");
				 DisputeBean disputeBean =new DisputeBean();
				 disputeBean.setDisputeid(disputeId);
				 disputeBean.setUserid(userId);
				 disputeBean.setWalletid(walletId);
				 disputeBean.setTxnid(txnId);
				 disputeBean.setReason(disputeReason);
				 disputeBean.setStatus("Pending");
				 session.save(disputeBean);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,userId, "",txnId ,"saveDispute()");
				 //logger.info("start Excute==========================================save data dispute mast table");
				 WalletToBankTxnMast walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, txnId);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,userId, "",txnId ,walletToBankTxnMast.getStatus());
				// System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+walletToBankTxnMast.getStatus());
				 walletToBankTxnMast.setStatus("DISPUTE");
				 session.update(walletToBankTxnMast);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,userId,"saveDispute()",txnId ,"update agentMoneyTransferBean  mast table");
				// logger.info("start Excute=====================================================update agentMoneyTransferBean  mast table");
				 transaction.commit(); 
				 status=disputeId;
				 				
			}else{
				status="7030";	
			}
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,userId, "saveDispute()",txnId ,"Problem in save Dispute"+e.getMessage()+" "+e);
			logger.debug("Problem in save Dispute============================================== " + e.getMessage(), e);
			status="7000";
		} finally {
			session.close();
		}
		
		return status;
		
	}
	
	
	 public Boolean validateTrx(String trxid,String walletid){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "",trxid ,"validateTrx()");
		
		 //logger.info("start excution ===================== method validateTrx( trxid, amount)"+trxid+"~~walletid"+walletid);
		 Boolean flag=false;
			factory=DBUtil.getSessionFactory();
	   	 	Session session = factory.openSession();
	  	    Transaction transaction = session.beginTransaction();
	  	    try{
	  	    	Query query=session.createQuery("from WalletToBankTxnMast where wtbtxnid=:trxid and walletid=:walletid and status='Success' ");
	  	    	query.setString("trxid", trxid);
	  	    	query.setString("walletid", walletid);
	  	    	List<WalletToBankTxnMast> list=query.list();
	  	    	 if (list != null && list.size() != 0) {
	  	    		flag=true;
	  	    	 }
	    	  }catch (HibernateException e) {
		         if (transaction!=null)transaction.rollback();
		         e.printStackTrace(); 
		         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"", "validateTrx()",trxid ,"problem in validateTrx"+e.getMessage()+" "+e);
		         //logger.debug("problem in validateTrx=====" + e.getMessage(), e);
		      }finally {
		    	  session.close(); 		      
		  	  }
	  	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletid,"","validateTrx()",trxid ,"Inside validateTrx retur"+flag);
	  	  //logger.info("Inside ====================validateTrx=== return===="+flag);  
			 return flag;
	 }
	
	 
	/**
	 *  
	 */
	 public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean){
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateCashinSurcharge()"+"|"+surchargeBean.getId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"|"+surchargeBean.getAmount());
		 //logger.info("Start excution ===================== method calculateCashinSurcharge(surchargeBean)"+surchargeBean.getId());
			//logger.info("Start excution ===================== method calculateCashinSurcharge(surchargeBean)"+surchargeBean.getAmount());
		 if(surchargeBean.getId()==null ||surchargeBean.getId().isEmpty()){
			 surchargeBean.setStatus("4321"); 
			 return surchargeBean;
		 }
		 
		 if(surchargeBean.getAmount()<=0){
			 surchargeBean.setStatus("4322"); 
			 return surchargeBean;
		 }
		 surchargeBean.setTxnCode(15);
		return  claculateSarcharge(surchargeBean);
		 
	 }
	 
	 
	/**
	 *  
	 */
	 public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean){
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateCashOutSurcharge()"+"|"+surchargeBean.getId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateCashOutSurcharge()"+"|"+surchargeBean.getAmount());
		// logger.info("Start excution ===================== method calculateCashOutSurcharge(surchargeBean)"+surchargeBean.getId());
			//logger.info("Start excution ===================== method calculateCashOutSurcharge(surchargeBean)"+surchargeBean.getAmount());
		 if(surchargeBean.getId()==null ||surchargeBean.getId().isEmpty()){
			 surchargeBean.setStatus("4321"); 
			 return surchargeBean;
		 }
		 
		 if(surchargeBean.getAmount()<=0){
			 surchargeBean.setStatus("4322"); 
			 return surchargeBean;
		 }
		 surchargeBean.setTxnCode(16);
		return  claculateSarcharge(surchargeBean);
		 
	 }
	 
	/**
	 *  
	 */
	public SurchargeBean calculateDMTSurcharge(SurchargeBean surchargeBean) {
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateDMTSurcharge()"+"|"+surchargeBean.getId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"calculateDMTSurcharge()"+"|"+surchargeBean.getAmount());
		//logger.info("Start excution ===================== method calculateDMTSurcharge(surchargeBean)"	+ surchargeBean.getId());
		//logger.info("Start excution ===================== method calculateDMTSurcharge(surchargeBean)"	+ surchargeBean.getAmount());
		if (surchargeBean.getId() == null || surchargeBean.getId().isEmpty()) {
			surchargeBean.setStatus("4321");
			return surchargeBean;
		}

		if (surchargeBean.getAmount() <= 0) {
			surchargeBean.setStatus("4322");
			return surchargeBean;
		}
		surchargeBean.setTxnCode(4);
		return claculateSarcharge(surchargeBean);

	}
	 
	 private SurchargeBean claculateSarcharge(SurchargeBean surchargeBean){
		
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"claculateSarcharge()");
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,methodname+"|"+surchargeBean.getAmount());
		// logger.info("start excution *********************************** method claculateSurcharge( surchargeBean)");
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 double surcharge=0.0;
		 surchargeBean.setStatus("7000");
		 List objectList = null;
			try{
				Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isbank)");

				 query.setParameter("id",surchargeBean.getId());
				 query.setParameter("txnAmount",surchargeBean.getAmount());
				 if(surchargeBean.getTransType() == null)
					 query.setParameter("transType", "");
				 else
					 query.setParameter("transType", surchargeBean.getTransType());
				 query.setParameter("isbank", "0");
				 objectList=query.list();
				 Object[] obj=(Object[])objectList.get(0);
				 surcharge=Double.parseDouble(""+obj[0]);
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","claculateSarcharge()","surcharge :"+surcharge);
				 //logger.info(" excution *********************************** surcharge :"+surcharge);
				 surchargeBean.setSarchargeAmount(surcharge);
				 surchargeBean.setStatus("1000");
				 				
			}catch(Exception e){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","claculateSarcharge()","problem in claculateSurcharge"+e.getMessage()+" "+e);
				//logger.debug("problem in claculateSurcharge*****************************" + e.getMessage(), e);
				 surchargeBean.setStatus("7000");
			e.printStackTrace();
			}finally {
				session.close();
			}
			
		return surchargeBean;
	}
	 
	 
	 
	 
	 
	/**
	 * 	
	 * @param walletId
	 * @param txnAmount
	 * @return
	 */
	
	 private String velocityCheck(String walletId,double txnAmount,int txnCode){
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
			Transaction transaction = session.beginTransaction();
			UserWalletConfigBean userWalletConfigBean;
			String status="7000";
			 List objectList = null;
			 
		     
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"claculateSarcharge()");
			 try{
				SQLQuery query = session.createSQLQuery("call velocityCheck(:walletId,:txnCode,:txnAmount)");
				
				 query.setParameter("walletId",walletId);
				 query.setParameter("txnCode",txnCode);
				 query.setParameter("txnAmount",txnAmount);
				 
				 System.out.println("____walletId_______"+walletId);
				 System.out.println("______txncode_________"+txnCode);
				 System.out.println("______txmAmount_________"+txnAmount);
				
				
				 objectList=query.list();
				 //String[] obj=(String[])objectList.get(0);
				 status=objectList.get(0).toString();
				//status="1000";
				
				/*Query query=session.createQuery("FROM UserWalletConfigBean WHERE  walletid=:userid");
				query.setString("userid", walletId);
				List <UserWalletConfigBean> walletDtlList=query.list();
				if (walletDtlList != null && walletDtlList.size() != 0) {
					userWalletConfigBean = walletDtlList.get(0);
					
					if(userWalletConfigBean.getMinbal() <=(getWalletBalance(walletId)-txnAmount)){
						status= "1000";
					}else{
						return "7048";
					}
					
					if(userWalletConfigBean.getMaxamtpertx()>0){
							if(userWalletConfigBean.getMaxamtpertx()<txnAmount){
								return "7014";	
							}else{
								status= "1000";
							}
						}
						
					
					if(userWalletConfigBean.getMaxamtperday()>0.0){
							if(userWalletConfigBean.getMaxamtperday()<(txnAmount+getmaxAmtPerDay(walletId))){
								return "7017";	
							}else{
								status= "1000";
							}
						}
						
					if(userWalletConfigBean.getMaxamtperweek()>0){
							if(userWalletConfigBean.getMaxamtperweek()<(txnAmount+getMaxAmtPerWeek(walletId))){
								return "7018";	
							}else{
								status= "1000";
							}
							
						}
					
					if(userWalletConfigBean.getMaxamtpermonth()>0){
							if(userWalletConfigBean.getMaxamtpermonth()<(txnAmount+getMaxAmtPerMon(walletId))){
								return "7019";
							}else{
								status= "1000";
							}
							}
					
					if(userWalletConfigBean.getMaxtxperday()>0){
							if(userWalletConfigBean.getMaxtxperday()<(getmaxTxPerDay(walletId)+1)){
								return "7020";	
							}else{
								status= "1000";
							}
							}
					
					if(userWalletConfigBean.getMaxtxperweek()>0){
							if(userWalletConfigBean.getMaxtxperweek()<(getMaxTxPerWeek(walletId)+1)){
								return "7021";	
							}else{
								status= "1000";
							}
							
					}
					if(userWalletConfigBean.getMaxtxpermonth()>0){
							if(userWalletConfigBean.getMaxtxpermonth()<(getMaxTxPerMon(walletId)+1)){
								return "7022";	
							}else{
								status= "1000";
							}
						}
					
					
					if(userWalletConfigBean.getMaxamtperquarter()>0){
						if(userWalletConfigBean.getMaxamtperquarter()<(txnAmount+getMaxAmtPerquarter(walletId))){
							return "7045";
						}else{
							status= "1000";
						}
						}
					
					
					if(userWalletConfigBean.getMaxamtperhalfyearly()>0){
						if(userWalletConfigBean.getMaxamtperhalfyearly()<(txnAmount+getMaxAmtPerhalfyearly(walletId))){
							return "7046";
						}else{
							status= "1000";
						}
						}
					
					
					if(userWalletConfigBean.getMaxamtperyear()>0){
						if(userWalletConfigBean.getMaxamtperyear()<(txnAmount+getMaxAmtPeryearly(walletId))){
							return "7047";
						}else{
							status= "1000";
						}
						}
					*/
					
					
					
					
				
			}catch(Exception e){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","claculateSarcharge()","Problem in velocityCheck"+e.getMessage()+" "+e);
				e.printStackTrace();
				return "7000";
			}
			finally {
				session.close();
			}
			return status;
		}
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	
	private Double getmaxAmtPerDay(String walletId){
	
		Double perDayAmount=0.0;
		factory=DBUtil.getSessionFactory();
	    Session session = factory.openSession();
	     try{
	    	perDayAmount=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate)=DATE(NOW())").setString("walletId",walletId).uniqueResult();
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getmaxAmtPerDay()"+"pperDayAmount trxId"+perDayAmount);
	    	//logger.info(" pperDayAmount===================================================================================== trxId"+perDayAmount);
	     }catch(Exception e){
	   		  e.printStackTrace();
	   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getmaxAmtPerDay()"+"problem in getmaxAmtPerDay"+ e.getMessage()+" "+e);
	   		 // logger.debug("problem in getmaxAmtPerDay========================================" + e.getMessage(), e);	   				
	   	} finally {
	   				session.close();
	   	}
		
	 return perDayAmount;
	}
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
private Double getMaxAmtPerWeek(String walletId){

	Double maxAmtPerWeek=0.0;
		factory=DBUtil.getSessionFactory();
	    Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	maxAmtPerWeek=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -6) AND CURDATE()").setString("walletId",walletId).uniqueResult();
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerWeek()"+"getMaxAmtPerWeek trxId"+maxAmtPerWeek);
	    	//logger.info(" getMaxAmtPerWeek===================================================================================== trxId"+maxAmtPerWeek);
	     }catch(Exception e){
	   		  e.printStackTrace();
	   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerWeek()"+"problem in getMaxAmtPerWeek"+ e.getMessage()+" "+e);
	   		  //logger.debug("problem in getMaxAmtPerWeek========================================" + e.getMessage(), e);	   				
	   	} finally {
	   				session.close();
	   	}
		
	 return maxAmtPerWeek;
	}
	

/**
 * 
 * @param walletId
 * @return
 */

private Double getMaxAmtPerMon(String walletId){

	Double maxAmtPerMon=0.0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
    try{
    	maxAmtPerMon=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -30) AND CURDATE()").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerMon()"+"getMaxAmtPerMon trxId"+ maxAmtPerMon);
    	//logger.info(" getMaxAmtPerMon===================================================================================== trxId"+maxAmtPerMon);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerMon()"+"problem in getMaxAmtPerMon"+ e.getMessage()+" "+e);
   		 // logger.debug("problem in getMaxAmtPerMon========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxAmtPerMon;
}


/**
 * 
 * @param walletId
 * @return
 */

private Double getMaxAmtPerquarter(String walletId){

	Double maxAmtPerquarter=0.0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
    try{
    	maxAmtPerquarter=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -90) AND CURDATE()").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerquarter()"+"getMaxAmtPerquarter trxId"+maxAmtPerquarter);
    	//logger.info(" getMaxAmtPerMon===================================================================================== trxId"+maxAmtPerquarter);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerquarter()"+"problem in getMaxAmtPerquarter"+ e.getMessage()+" "+e);
   		  //logger.debug("problem in getMaxAmtPerMon========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxAmtPerquarter;
}


/**
 * 
 * @param walletId
 * @return
 */

private Double getMaxAmtPerhalfyearly(String walletId){

	Double maxAmtPerhalfyearly=0.0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
    try{
    	maxAmtPerhalfyearly=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -180) AND CURDATE()").setString("walletId",walletId).uniqueResult();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerhalfyearly()"+"getMaxAmtPerhalfyearly  trxId"+ maxAmtPerhalfyearly);
    	//logger.info(" getMaxAmtPerMon===================================================================================== trxId"+maxAmtPerhalfyearly);
     }catch(Exception e){
   		  e.printStackTrace();
     		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPerhalfyearly()"+"problem in getMaxAmtPerhalfyearlyr"+ e.getMessage()+" "+e);
   		  //logger.debug("problem in getMaxAmtPerMon========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxAmtPerhalfyearly;
}



/**
 * 
 * @param walletId
 * @return
 */

private Double getMaxAmtPeryearly(String walletId){

	Double maxAmtPeryearly=0.0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
    try{
    	maxAmtPeryearly=(Double)session.createQuery("SELECT COALESCE(SUM(E.txndebit),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -365) AND CURDATE()").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPeryearly()"+" getMaxAmtPeryearly trxId"+maxAmtPeryearly);
    	//logger.info(" getMaxAmtPerMon===================================================================================== trxId"+maxAmtPeryearly);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxAmtPeryearly()"+"problem in getMaxAmtPeryearly"+ e.getMessage()+" "+e);
   		 // logger.debug("problem in getMaxAmtPerMon========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxAmtPeryearly;
}


/**
 * 
 * @param walletId
 * @return
 */

private int getmaxTxPerDay(String walletId){
	
	int maxTxPerDay=0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
     try{
    	maxTxPerDay=(Integer)session.createQuery("SELECT COALESCE(count(*),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate)=DATE(NOW())").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getmaxTxPerDay()"+" getmaxTxPerDay trxid"+ maxTxPerDay);
    	//logger.info(" getmaxTxPerDay===================================================================================== trxId"+maxTxPerDay);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getmaxTxPerDay()"+"problem in getmaxTxPerDay"+ e.getMessage()+" "+e);
   		  //logger.debug("problem in getmaxTxPerDay========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
 return maxTxPerDay;
}

/**
 * 
 * @param walletId
 * @return
 */
private int getMaxTxPerWeek(String walletId){

	int maxTxPerWeek=0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
     try{
    	maxTxPerWeek=(Integer)session.createQuery("SELECT COALESCE(count(*),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate)  BETWEEN ADDDATE(CURDATE(), -6) AND CURDATE()").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxTxPerWeek()"+"getMaxTxPerWeek trxid"+ maxTxPerWeek);
    	//logger.info(" getMaxAmtPerWeek===================================================================================== trxId"+maxTxPerWeek);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxTxPerWeek()"+"problem in getMaxTxPerWeek"+ e.getMessage()+" "+e);
   		 // logger.debug("problem in getMaxAmtPerWeek========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxTxPerWeek;
}


private int getMaxTxPerMon(String walletId){

	int maxTxPerMon=0;
	factory=DBUtil.getSessionFactory();
    Session session = factory.openSession();
       try{
    	maxTxPerMon=(Integer)session.createQuery("SELECT COALESCE(count(*),0) FROM TransactionBean E WHERE  walletid=:walletId AND result='Success'  AND DATE(txnDate) BETWEEN ADDDATE(CURDATE(), -30) AND CURDATE()").setString("walletId",walletId).uniqueResult();
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxTxPerMon()"+"getMaxTxPerMon trxid"+ maxTxPerMon);
    	//logger.info(" getMaxTxPerMon===================================================================================== trxId"+maxTxPerMon);
     }catch(Exception e){
   		  e.printStackTrace();
   		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getMaxTxPerMon()"+"problem in getMaxTxPerMon"+ e.getMessage()+" "+e);
   		 // logger.debug("problem in getMaxTxPerMon========================================" + e.getMessage(), e);	   				
   	} finally {
   				session.close();
   	}
	
 return maxTxPerMon;
}





/**
 * 
 * @param walletId
 * @param amount
 * @param pgTxnId
 * @param ipImei
 * @return
 */

public String settlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,6);
}

public String settlementByTxnCode(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent,int txnCode){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,txnCode);
}


public String wlRechargeSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,94);
}

public String cashBacksettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return cashBackSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,6);
}


public String dmtSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,10);
}


public String cashDepositSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,13);
}


private String refund(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,14);
}


private String prePaidCardReversalSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,12);
}



public String surchargeRefundSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,20);
}

public String surchargeRefundSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent,double distAmt,String distId,double supDistSurcharge,String supDistId,double agentRefund,double dtdsApply){
	return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,20,distAmt,distId,supDistSurcharge,supDistId,agentRefund,dtdsApply);
}

private String walletSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent,int txnCode){

    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"walletSettlement()"+"|"+agent);
   // logger.info("start excution ===================== method settlement(userId,walletId, amount, settTxnId, ipImei)");
	String status="1001";
	TransactionBean transactionBean=new TransactionBean();
	factory=DBUtil.getSessionFactory();
	 	Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	  try{
			String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"--------inside settlement");
			transactionBean.setTxnid(txnId);
			transactionBean.setUserid(userId);
			transactionBean.setWalletid(walletId);
			transactionBean.setPayeedtl(mobileNo);
			transactionBean.setTxncode(txnCode);
			transactionBean.setTxncredit(amount);
			transactionBean.setTxndebit(0);
			transactionBean.setResult("Success");
			transactionBean.setResptxnid(settTxnId);
			transactionBean.setAggreatorid(aggreatorid);
			transactionBean.setWalletuserip(ipImei);
			
		    // transactionBean.setWalletuseragent(request.getHeader("User-Agent"));
			transactionBean.setWalletuseragent(agent);
			session.save(transactionBean);
			transaction.commit();
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"--------inside settlement done---------walletid"+walletId);
	     	status="1000";
	
	  }catch(Exception e){
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "walletSettlement()",settTxnId ,"problem in Add  monet to wallet by settlement"+e.getMessage()+" "+e);
		  //logger.debug("problem in Add  monet to wallet by settlement" + e.getMessage(), e);
	transaction.rollback();
	return "7000";
	  } finally {
	session.close();
}
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "walletSettlement()",settTxnId ,"Inside addMoney return"+status);
	  //logger.info("Inside ====================addMoney=== return===="+status);
return status;
	
}

public MoneyRequestBean moneyRequest(CashDepositMast requestBean){

    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getUserId(), "","" ,"moneyRequest()");
  
	//logger.info("start excution ===================== method moneyRequest()"+requestBean.getWalletId());
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	MoneyRequestBean bean=new  MoneyRequestBean();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		WalletMastBean walletMastBean=walletUserDao.showUserProfile(requestBean.getUserId());
		WalletMastBean cashWallet=null;
		if(walletMastBean!=null&&walletMastBean.getWhiteLabel()!=null&&walletMastBean.getWhiteLabel().equalsIgnoreCase("1")){
			cashWallet=walletUserDao.showUserProfile(walletMastBean.getCashDepositeWallet());
			if(cashWallet!=null){
				requestBean.setUserId(cashWallet.getId());
				requestBean.setWalletId(cashWallet.getWalletid());
			}
		}
		
		String smsTemplates="<<aggregator>> raise a request of INR <<amount>> via <<bank name>> with UTR no <<utr no>>.";
		
			 smsTemplates=smsTemplates.replace("<<aggregator>>", requestBean.getAggreatorId());
			 smsTemplates=smsTemplates.replace("<<amount>>", ""+requestBean.getAmount());
			 smsTemplates=smsTemplates.replace("<<bank name>>", requestBean.getBankName());
			 smsTemplates=smsTemplates.replace("<<utr no>>", requestBean.getNeftRefNo());
			SmsAndMailUtility smsAndMailUtility;
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getUserId(), "","moneyRequest()","Inside money request alert send on mobile and Email");
			//logger.info("Inside *****************************money request alert send on mobile ******and Email");
			//String mailtemplet = commanUtilDao.getmailTemplet("regOtp",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp);
			
			if(requestBean!=null&&requestBean.getUserId()!=null&&requestBean.getUserId().contains("OAGG00")){
			smsAndMailUtility = new SmsAndMailUtility("vikas.kumar@bhartipay.com",smsTemplates, "DMT Money request", "8059565955",smsTemplates, "BOTH",requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getAggreatorId(),"money request");
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			smsAndMailUtility = new SmsAndMailUtility("akhand.pratap@bhartipay.com",smsTemplates, "DMT Money request", "8004678459",smsTemplates, "BOTH",requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getAggreatorId(),"money request");
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			}
			
			String depositId=commanUtilDao.getTrxId("CASH",requestBean.getAggreatorId());
			requestBean.setDepositId(depositId);
			requestBean.setStatus("Pending");
			requestBean.setApproverStatus("Pending");
			requestBean.setCheckerStatus("Pending");
			
		session.save(requestBean);
		transaction.commit();
		bean.setStatus("1000");
	}catch (Exception e) {
		bean.setStatus("7000");
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),requestBean.getWalletId(),requestBean.getUserId(), "","" ,"problem in moneyRequest"+ e.getMessage()+" "+e);
		//logger.debug("problem in moneyRequest==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return bean;
}

private String walletSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent,int txnCode,double distAmt,String distId,double supDistSurcharge,String supDistId,double agentRefund, double dtdsApply){

    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"walletSettlement()");
  
	//logger.info("start excution ===================== method settlement(userId,walletId, amount, settTxnId, ipImei)");
	String status="1001";
	TransactionBean transactionBean=new TransactionBean();
	factory=DBUtil.getSessionFactory();
	 	Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	  try{
			String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
			transactionBean.setTxnid(txnId);
			transactionBean.setUserid(userId);
			transactionBean.setWalletid(walletId);
			transactionBean.setPayeedtl(mobileNo);
			transactionBean.setTxncode(txnCode);
			transactionBean.setTxncredit(amount);
			transactionBean.setTxndebit(0);
			transactionBean.setResult("Success");
			transactionBean.setResptxnid(settTxnId);
			transactionBean.setAggreatorid(aggreatorid);
			transactionBean.setWalletuserip(ipImei);
			transactionBean.setWalletuseragent(agent);
			session.save(transactionBean);
			
			if(agentRefund>0){
				TransactionBean transactionAgentCredit=new TransactionBean();
				String txnId1=commanUtilDao.getTrxId("AMIW",aggreatorid);
				transactionAgentCredit.setTxnid(txnId1);
				transactionAgentCredit.setUserid(userId);
				transactionAgentCredit.setWalletid(walletId);
				transactionAgentCredit.setPayeedtl(mobileNo);
				transactionAgentCredit.setTxncode(79);
				transactionAgentCredit.setTxncredit(0);
				transactionAgentCredit.setTxndebit(agentRefund);
				transactionAgentCredit.setResult("Success");
				transactionAgentCredit.setResptxnid(settTxnId);
				transactionAgentCredit.setAggreatorid(aggreatorid);
				transactionAgentCredit.setWalletuserip(ipImei);
				transactionAgentCredit.setWalletuseragent(agent);
				session.save(transactionAgentCredit);
				
				if(agentRefund>0 && dtdsApply > 0) {
					double tdsAmount=(agentRefund * dtdsApply)/100;
					if(tdsAmount > 0) {
						TransactionBean transactionAgentCreditTDS=new TransactionBean();
						//String txnId1=commanUtilDao.getTrxId("AMIW",aggreatorid);
						transactionAgentCreditTDS.setTxnid("TC"+txnId1);
						transactionAgentCreditTDS.setUserid(userId);
						transactionAgentCreditTDS.setWalletid(walletId);
						transactionAgentCreditTDS.setPayeedtl(mobileNo);
						transactionAgentCreditTDS.setTxncode(96);
						transactionAgentCreditTDS.setTxncredit(tdsAmount);
						transactionAgentCreditTDS.setTxndebit(0);
						transactionAgentCreditTDS.setResult("Success");
						transactionAgentCreditTDS.setResptxnid(settTxnId);
						transactionAgentCreditTDS.setAggreatorid(aggreatorid);
						transactionAgentCreditTDS.setWalletuserip(ipImei);
						transactionAgentCreditTDS.setWalletuseragent(agent);
						session.save(transactionAgentCreditTDS);
					}
				}
			}
			transaction.commit();
	     	status="1000";

	     	 if(txnCode == 20 )
	         { 
	          paymentFromWalletDist(userId,getWalletIdByUserId(aggreatorid,aggreatorid),56,(((amount-distAmt)-supDistSurcharge)-agentRefund),mobileNo,settTxnId,ipImei,aggreatorid,agent,0);
	          if((((amount-distAmt)-supDistSurcharge)-agentRefund)>0 && dtdsApply>0) {
	        	  double tdsAmount56=((((amount-distAmt)-supDistSurcharge)-agentRefund)*dtdsApply)/100;
	        	  if(tdsAmount56>0) {
	        		  creditMoney("ATC"+txnId, userId, getWalletIdByUserId(aggreatorid, aggreatorid),
	        				  tdsAmount56, mobileNo, settTxnId,
								ipImei, aggreatorid, agent, 97);
	        	  }
	          }
	          if(distAmt!= 0 && (distId != null && !distId.trim().equals("")))
	          {
	           paymentFromWalletDist(userId,getWalletIdByUserId(distId,aggreatorid),55,distAmt,mobileNo,settTxnId,ipImei,aggreatorid,agent,0);
	           if(distAmt > 0 && dtdsApply>0) {
		        	  double tdsAmount55=(distAmt * dtdsApply)/100;
		        	  if(tdsAmount55>0) {
		        		  creditMoney("DTC"+txnId, userId, getWalletIdByUserId(distId, aggreatorid),
		        				  tdsAmount55, mobileNo, settTxnId,	ipImei, aggreatorid, agent, 99);
		        	  }
		          }
	          }
	          if(supDistSurcharge!=0&&(supDistId!=null&&!supDistId.trim().isEmpty()&&!supDistId.equalsIgnoreCase("-1"))){
		      paymentFromWalletDist(userId,getWalletIdByUserId(supDistId,aggreatorid),35,supDistSurcharge,mobileNo,settTxnId,ipImei,aggreatorid,agent,0);
		      if(supDistSurcharge >0 && dtdsApply>0) {
	        	  double tdsAmount35=(supDistSurcharge*dtdsApply)/100;
	        	  if(tdsAmount35>0) {
	        		  creditMoney("STC"+txnId, userId, getWalletIdByUserId(supDistId, aggreatorid),
	        				  tdsAmount35, mobileNo, settTxnId, ipImei, aggreatorid, agent, 101);
	        	  }
	          }
	          }
	         }

	  }catch(Exception e){
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"walletSettlement()"+"|problem in Add  monet to wallet by settlement"+e.getMessage()+" "+e);
		 // logger.debug("problem in Add  monet to wallet by settlement" + e.getMessage(), e);
	transaction.rollback();
	return "7000";
	  } finally {
	session.close();
}
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "walletSettlement()",settTxnId ,"Inside addMoney return"+status);
	 // logger.info("Inside ====================addMoney=== return===="+status);
return status;
	
}



private String cashBackSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent,int txnCode){

    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"cashBackSettlement()"+"|"+agent);
	//logger.info("start excution ===================== method cashBackSettlement(userId,walletId, amount, settTxnId, ipImei)");
	String status="1001";
	CashBackLedgerBean cashBackLedgerBean=new CashBackLedgerBean();
	factory=DBUtil.getSessionFactory();
	 	Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	  try{
			String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
			cashBackLedgerBean.setTxnid(txnId);
			cashBackLedgerBean.setUserid(userId);
			cashBackLedgerBean.setWalletid(walletId);
			cashBackLedgerBean.setPayeedtl(mobileNo);
			cashBackLedgerBean.setTxncode(txnCode);
			cashBackLedgerBean.setTxncredit(amount);
			cashBackLedgerBean.setTxndebit(0);
			cashBackLedgerBean.setResult("Success");
			cashBackLedgerBean.setResptxnid(settTxnId);
			cashBackLedgerBean.setAggreatorid(aggreatorid);
			cashBackLedgerBean.setWalletuserip(ipImei);
			cashBackLedgerBean.setWalletuseragent(agent);
			session.save(cashBackLedgerBean);
			transaction.commit();
	     	status="1000";
	
		  
	  }catch(Exception e){
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId,"cashBackSettlement()",settTxnId ,"problem in cashBackSettlement"+ e.getMessage()+" "+e);
	//logger.debug("problem in cashBackSettlement *************************" + e.getMessage(), e);
	transaction.rollback();
	return "7000";
	  } finally {
	session.close();
}
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId,"cashBackSettlement()",settTxnId ,"Inside cashBackSettlement return"+status);
//logger.info("Inside ****************************cashBackSettlement*******************return******"+status);
return status;
	
}





	public RechargeTxnBean saveInitialRechargeTxn(RechargeTxnBean rechargeTxnBean) {
	
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"saveInitialRechargeTxn()");
		/*logger.info(
				"start excution ==============****************************************************************saveInitialRechargeTxn");*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		RechargeOperatorMasterBean rechargeOperatorMasterBean = new RechargeOperatorMasterBean();
		Transaction transaction = session.beginTransaction();
		rechargeTxnBean.setStatusCode("7000");
		try {
			String rechargeId=commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
			rechargeTxnBean.setRechargeId(rechargeId);
			rechargeTxnBean.setStatus("Cancel");
			session.save(rechargeTxnBean);
			transaction.commit();
			rechargeTxnBean.setStatusCode("1000");
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","saveInitialRechargeTxn()","problem in saveInitialRechargeTxn"+e.getMessage()+" "+e);
			//logger.debug("problem in payForPayment=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return rechargeTxnBean;
	}




	public RechargeTxnBean payForPayment(RechargeTxnBean rechargeTxnBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
				rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "", "payForPayment()");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		RechargeOperatorMasterBean rechargeOperatorMasterBean = new RechargeOperatorMasterBean();
		Transaction transaction = session.beginTransaction();
		RechargeBillRequest recReq=new RechargeBillRequest();
		recReq.setAmount(rechargeTxnBean.getRechargeAmt());
		recReq.setUserId(rechargeTxnBean.getUserId());
		recReq.setMobileNumber(rechargeTxnBean.getRechargeNumber());
		recReq.setOperatorName(rechargeTxnBean.getRechargeOperator());
		
		String status = "1001";
		RechargeDao rcReq=new RechargeDaoImpl();
		if(rcReq.getRechargeAgentRequest(recReq)) {
		try {
			TreeMap<String, String> map = new TreeMap<String, String>();
			map.put("walletId", rechargeTxnBean.getWalletId());
			map.put("userId", rechargeTxnBean.getUserId());
			map.put("rechargeNumber", rechargeTxnBean.getRechargeNumber());
			map.put("rechargeType", rechargeTxnBean.getRechargeType());
			map.put("rechargeOperator", rechargeTxnBean.getRechargeOperator());
			map.put("rechargeCircle",
					rechargeTxnBean.getRechargeCircle() != null ? rechargeTxnBean.getRechargeCircle() : "NA");
			map.put("rechargeAmt", "" + rechargeTxnBean.getRechargeAmt());
			map.put("aggreatorid", rechargeTxnBean.getAggreatorid());

			map.put("CHECKSUMHASH", rechargeTxnBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(map);

			System.out.println("______statusCode________" + statusCode);
			if (statusCode == 1000) {
				try {
					transaction = session.beginTransaction();
					SQLQuery insertQueryt = session.createSQLQuery("insert into trackrequest(walletid) VALUES (?)");
					insertQueryt.setParameter(0, rechargeTxnBean.getWalletId());
					insertQueryt.executeUpdate();
					transaction.commit();
					transaction = session.beginTransaction();
				} catch (Exception e) {

				}
				String rechargeId = commanUtilDao.getTrxId("RECHARGE", rechargeTxnBean.getAggreatorid());

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-dd");
		        String dateString = formatt.format( new Date(System.currentTimeMillis()));
				recReq.setRequestId(rechargeId);
				recReq.setRequestDate(dateString);
				recReq.setCreationDate(timestamp);
				rcReq.saveRechargeAgentRequest(recReq);
				
				rechargeTxnBean.setRechargeId(rechargeId);
				rechargeTxnBean.setStatus("Pending");
				rechargeTxnBean.setUseCashBack("N");
				session.saveOrUpdate(rechargeTxnBean);
				transaction.commit();

				String walletResp = "1001";
				double cashBackWalletBalance = getCashBackWalletBalance(rechargeTxnBean.getWalletId());
				if (cashBackWalletBalance >= rechargeTxnBean.getRechargeAmt()) {
					walletResp = reChargeFromCashBack(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
							rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), rechargeId,
							rechargeTxnBean.getIpIemi(), rechargeTxnBean.getAggreatorid(),
							rechargeTxnBean.getTxnAgent());
					if (walletResp.equals("1000")) {
						transaction = session.beginTransaction();
						rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
						rechargeTxnBean.setUseCashBack("Y");
						session.update(rechargeTxnBean);
						transaction.commit();
					}

				} else {
					WalletMastBean aggWallet = (WalletMastBean) session.get(WalletMastBean.class,
							rechargeTxnBean.getAggreatorid());
					if (aggWallet != null && "1".equalsIgnoreCase(aggWallet.getWhiteLabel())
							&& aggWallet.getCashDepositeWallet() != null) {
						WalletMastBean aggCashWallet = (WalletMastBean) session.get(WalletMastBean.class,
								aggWallet.getCashDepositeWallet());
						String resp = wlWalletDebitOnRecharge(aggCashWallet.getId(), aggCashWallet.getWalletid(),
								rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), rechargeId,
								rechargeTxnBean.getIpIemi(), rechargeTxnBean.getAggreatorid(),
								rechargeTxnBean.getTxnAgent());
						if ("1000".equalsIgnoreCase(resp)) {
							walletResp = reCharge(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
									rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), rechargeId,
									rechargeTxnBean.getIpIemi(), rechargeTxnBean.getAggreatorid(),
									rechargeTxnBean.getTxnAgent());
						}
					} else {
						walletResp = reCharge(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
								rechargeTxnBean.getRechargeAmt(), rechargeTxnBean.getRechargeNumber(), rechargeId,
								rechargeTxnBean.getIpIemi(), rechargeTxnBean.getAggreatorid(),
								rechargeTxnBean.getTxnAgent());
					}
				}

				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
						rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
						"walletResp" + walletResp);

				// logger.info("walletResp ===================== method
				// payForPayment(walletToBankTxnMast)"+walletResp);
				if (walletResp.equals("1000")) {

					String rechargetype = null;
					if (rechargeTxnBean.getRechargeOperator().contains("Special")) {
						rechargetype = "Special";
					} else {
						rechargetype = "Top Up";
					}
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
							rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
							"walletResp rechargeTxnBean.getRechargeOperator()" + rechargeTxnBean.getRechargeOperator());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
							rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
							rechargetype + "walletResp  rechargeTxnBean.getRechargeOperator()"
									+ rechargeTxnBean.getRechargeType());

					List<RechargeOperatorMasterBean> list =null;
					
					Query query = session.createQuery(
							"from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
					query.setString("name", rechargeTxnBean.getRechargeOperator());
					query.setString("servicetype", rechargeTxnBean.getRechargeType());
					query.setString("rechargetype", rechargetype);
				    list = query.list();
				   
				    if(list.size()==0) {
						Query query1 = session.createQuery(
								"from RechargeOperatorMasterBean where operatorCode=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
						query1.setString("name", rechargeTxnBean.getRechargeOperator());
						query1.setString("servicetype", rechargeTxnBean.getRechargeType());
						query1.setString("rechargetype", rechargetype);
					    list = query1.list();
						} 
					 
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),"",rechargeTxnBean.getRechargeOperator(),rechargeTxnBean.getRechargeType(),rechargetype,"   List Size  "+list.size());
							
					if (list == null || list.size() > 0)
                     {
						rechargeOperatorMasterBean = list.get(0);

						if (rechargeOperatorMasterBean.getClient().equals("MyPayStore")) {
						} else if (rechargeOperatorMasterBean.getClient().equals("ezypay")) {
						} else if (rechargeOperatorMasterBean.getClient().equals("PAYWORLD")) {
						} else if (rechargeOperatorMasterBean.getClient().equals("RECHARGEBILL")) {
							
						String rechargeBillResp = new EzyPayIntegration().rechargeBillAPI(
									rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "",
					 				rechargeTxnBean.getRechargeNumber(), rechargeId,
									Double.toString(rechargeTxnBean.getRechargeAmt()), rechargeTxnBean.getStdCode(),
									rechargeTxnBean.getAccountNumber(), rechargeTxnBean.getRechargeCircle());
						
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),
									rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getWalletId(),
									rechargeTxnBean.getUserId(), "", "payForPayment()",
									"walletResp  ezyRes" + rechargeBillResp);

							
							//Gson g = new Gson();  
							//Result result = g.fromJson(rechargeBillResp, Result.class); 
							
							//JSONParser parser = new JSONParser();
							//JSONObject json = (JSONObject) parser.parse(rechargeBillResp);
							
						
  
							//String rechargeStatus = "Pending";
							String rechargeStatus =null;
							String txnId = null;
							String opid = null;
							String orderid = null;
							String amount = null;
							String number = null;
							String apirefid=null;
                            /*
							if (rechargeBillResp == null || rechargeBillResp.isEmpty()
									|| rechargeBillResp.length() == 0) {
								rechargeStatus = "Pending";
							}
							*/
							JSONObject jsonObject = new JSONObject(rechargeBillResp);

							if (jsonObject != null) {
								if (jsonObject.has("status"))
									rechargeStatus = jsonObject.getString("status");
								if (jsonObject.has("reqid"))
									txnId = "" + jsonObject.getString("reqid");
								if (jsonObject.has("reqid"))
									orderid = jsonObject.getString("reqid");
								if (jsonObject.has("amt"))
									amount = jsonObject.getString("amt");
								if (jsonObject.has("mn"))
									number = jsonObject.getString("mn");
								
								if (jsonObject.has("opid"))
									opid = jsonObject.getString("opid");
									
								if (jsonObject.has("apirefid"))
									apirefid = jsonObject.getString("apirefid");	
									
									
							 /*
								if (jsonObject.has("txid"))
									txnId = "" + jsonObject.getInt("txid");
								if (jsonObject.has("opid"))
									opid = jsonObject.getString("opid");
								if (jsonObject.has("orderid"))
									orderid = jsonObject.getString("orderid");
								if (jsonObject.has("amount"))
									amount = jsonObject.getString("amount");
								if (jsonObject.has("number"))
									number = jsonObject.getString("number");
									
									
								if ("SUCCESS".equalsIgnoreCase(rechargeStatus)) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("Success");
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOperatorTxnId(txnId);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							} else /* if ("PENDING".equalsIgnoreCase(rechargeStatus)) *//* {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("Pending");
								rechargeTxnBean.setOperatorTxnId(txnId);
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOther(rechargeStatus);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							}
									
							 */
							 	
							}

							if ("SUCCESS".equalsIgnoreCase(rechargeStatus)) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus(rechargeStatus);
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOperatorTxnId(apirefid);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							} else  if ("PENDING".equalsIgnoreCase(rechargeStatus)) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("PENDING");
								rechargeTxnBean.setOperatorTxnId(apirefid);
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							}else if ("FREQUENT".equalsIgnoreCase(rechargeStatus)) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("PENDING");
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOperatorTxnId(apirefid);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							} else  if ("FAILED".equalsIgnoreCase(rechargeStatus))  {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("PENDING");
								rechargeTxnBean.setOperatorTxnId(apirefid);
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							}else  
                              {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("PENDING");
								rechargeTxnBean.setOperatorTxnId(apirefid);
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOther(rechargeBillResp);
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							}
							
							

							try {
								processRechargeCommission(rechargeId, rechargeTxnBean.getUserId(),
										rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getRechargeOperator(),
										rechargeTxnBean.getRechargeAmt(), "credit");
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						} else if (rechargeOperatorMasterBean.getClient().equals("JOLO")) {
						}

						else if (rechargeOperatorMasterBean.getClient().equals("moneyways")) {
						} else if (rechargeOperatorMasterBean.getClient().equals("billavenue")) {
							Criteria criteria = session.createCriteria(WalletMastBean.class);
							criteria.add(Restrictions.eq("walletid", rechargeTxnBean.getWalletId()));
							List<WalletMastBean> walletList = criteria.list();
							ExtBillPayResponse response = null;
							if (walletList != null && walletList.get(0) != null
									&& walletList.get(0).getAsAgentCode() != null) {
								response = new EzyPayIntegration().rechargeUsingBillAvenue(
										walletList.get(0).getAsAgentCode(), rechargeOperatorMasterBean.getCode(),
										rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "",
										rechargeTxnBean.getRechargeNumber(), rechargeId,
										Double.toString(rechargeTxnBean.getRechargeAmt()), rechargeTxnBean.getStdCode(),
										rechargeTxnBean.getAccountNumber(), rechargeTxnBean.getRechargeCircle(),
										rechargeTxnBean.getIpIemi(), rechargeTxnBean.getUseragent());
							} else {
								response = new ExtBillPayResponse();
								response.setResponseCode("222");
								response.setResponseReason("Agent id is not mapped for agent in walletmast");
								response.setTxnRefId("NA");
							}
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),
									rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getWalletId(),
									rechargeTxnBean.getUserId(), "", "payForPayment()",
									"walletResp billavenue" + response);

							if (null != response && "000".equalsIgnoreCase(response.getResponseCode())) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("Success");
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOperatorTxnId(response.getTxnRefId());
								// rechargeTxnBean.getWa
								rechargeTxnBean.setOther(response.getResponseReason() + "-"
										+ response.getApprovalRefNumber() + "-" + response.getRespBillNumber());
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "1000";
							} else if (null != response && "111".equalsIgnoreCase(response.getResponseCode())) {
								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("Pending");
								rechargeTxnBean.setOperatorTxnId(response.getTxnRefId());
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean.setOther(response.getResponseReason() + "-"
										+ response.getApprovalRefNumber() + "-" + response.getRespBillNumber());
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "100";
							}

							else /* if(null != response && "222".equalsIgnoreCase(response.getResponseCode())) */ {

								WalletMastBean aggWallet = (WalletMastBean) session.get(WalletMastBean.class,
										rechargeTxnBean.getAggreatorid());
								if (aggWallet != null && "1".equalsIgnoreCase(aggWallet.getWhiteLabel())
										&& aggWallet.getCashDepositeWallet() != null) {
									WalletMastBean aggCashWallet = (WalletMastBean) session.get(WalletMastBean.class,
											aggWallet.getCashDepositeWallet());
									String resp = wlRechargeSettlement(rechargeTxnBean.getUserId(),
											aggCashWallet.getWalletid(), rechargeTxnBean.getRechargeAmt(), rechargeId,
											rechargeTxnBean.getIpIemi(), rechargeTxnBean.getRechargeNumber(),
											rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getTxnAgent());
									if ("1000".equalsIgnoreCase(resp)) {
										settlement(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
												rechargeTxnBean.getRechargeAmt(), rechargeId,
												rechargeTxnBean.getIpIemi(), rechargeTxnBean.getRechargeNumber(),
												rechargeTxnBean.getAggreatorid(), rechargeTxnBean.getTxnAgent());
									}
								} else {
									settlement(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
											rechargeTxnBean.getRechargeAmt(), rechargeId, rechargeTxnBean.getIpIemi(),
											rechargeTxnBean.getRechargeNumber(), rechargeTxnBean.getAggreatorid(),
											rechargeTxnBean.getTxnAgent());
								}

								transaction = session.beginTransaction();
								rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
								rechargeTxnBean.setStatus("Failed");
								rechargeTxnBean.setOperatorTxnId(response.getTxnRefId());
								rechargeTxnBean.setWallettxnStatus(walletResp);
								rechargeTxnBean
										.setOther(response.getResponseReason() + "-" + response.getApprovalRefNumber()
												+ "-" + response.getRespBillNumber() + "-" + response.getErrorInfo());
								session.update(rechargeTxnBean);
								transaction.commit();
								status = "7043";
							}
						}

					}

					else {
						logger.info("No Record found for " + rechargeTxnBean.getRechargeOperator() + " "
								+ rechargeTxnBean.getRechargeType() + " " + rechargetype + " having recharge number as "
								+ rechargeTxnBean.getRechargeNumber());
						transaction = session.beginTransaction();
						rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
						if (rechargeTxnBean.getUseCashBack().equalsIgnoreCase("Y")) {
							cashBacksettlement(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
									rechargeTxnBean.getRechargeAmt(), rechargeId, rechargeTxnBean.getIpIemi(),
									rechargeTxnBean.getRechargeNumber(), rechargeTxnBean.getAggreatorid(),
									rechargeTxnBean.getTxnAgent());
						} else {
							/*
							settlement(rechargeTxnBean.getUserId(), rechargeTxnBean.getWalletId(),
									rechargeTxnBean.getRechargeAmt(), rechargeId, rechargeTxnBean.getIpIemi(),
									rechargeTxnBean.getRechargeNumber(), rechargeTxnBean.getAggreatorid(),
									rechargeTxnBean.getTxnAgent());
						    */
						    	Criteria rechCriteria = session.createCriteria(TransactionBean.class);
								rechCriteria.add(Restrictions.eq("resptxnid", rechargeId));
								List<TransactionBean> summarylist = (List<TransactionBean>)rechCriteria.list();
						    	
								
								int txn_5 = 0;
								int txn_6 = 0;
								int txn_93 = 0;
								int txn_94 = 0;
								  	
								TransactionBean txBean = null;
								for (TransactionBean txnBean : summarylist) {
									if (txnBean.getTxncode() == 5) {
										txn_5 = 5;
									}
									if (txnBean.getTxncode() == 6) {
										txn_6 = 6;
									}
									if (txnBean.getTxncode() == 93) {
										txn_93 = 93;
										txBean = txnBean;
									}
									if (txnBean.getTxncode() == 94) {
										txn_94 = 94;
									}
									 
								}
								
								if(txn_5 == 5 && txn_6 == 0 ) {
									settlement(rechargeTxnBean.getUserId(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getRechargeAmt(),rechargeTxnBean.getRechargeId(),rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getUseragent());
									if(txn_93 == 93 && txn_94 == 0) {
									wlRechargeSettlement(txBean.getUserid(), txBean.getWalletid(), txBean.getTxndebit(), txBean.getResptxnid(), rechargeTxnBean.getIpIemi(), rechargeTxnBean.getRechargeNumber(), txBean.getAggreatorid(), rechargeTxnBean.getUseragent());
									}
									 
								}
								
								 
						
						}
					
						Query pendingQuery = session .createQuery("from RechargeTxnBean where rechargeid= :id and rechargeType in ('PREPAID','DTH','LANDLINE','POSTPAID','Electricity','Gas','Insurance','Datacard')");
						pendingQuery.setString("id",rechargeId );
						List<RechargeTxnBean> pendingList = query.list();
						if(pendingList!=null) {
						rechargeTxnBean.setRechargeId(rechargeId);
						rechargeTxnBean.setStatus("FAILED"); 
						session.saveOrUpdate(rechargeTxnBean);
						transaction.commit();
						}
					}
				} else {
					transaction = session.beginTransaction();
					rechargeTxnBean = (RechargeTxnBean) session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("Failed");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					session.update(rechargeTxnBean);
					transaction.commit();
					status = walletResp;
				}
			} else {
				rechargeTxnBean.setStatus("Failed");
				status = "" + statusCode;
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
					rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
					"problem in payForPayment" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
			status = "7000";
		} finally {
			transaction = session.beginTransaction();
			SQLQuery delete = session
					.createSQLQuery("delete from trackrequest where walletid='" + rechargeTxnBean.getWalletId() + "'");
			delete.executeUpdate();
			transaction.commit();
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
				rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
				" excution  status" + status);
		if (status.equalsIgnoreCase("1000")) {
			try {
				walletConfiguration = commanUtilDao.getWalletConfiguration(rechargeTxnBean.getAggreatorid());
				walletMastBean = walletUserDao.showUserProfile(rechargeTxnBean.getUserId());
				String smstemplet = commanUtilDao.getsmsTemplet("recharge", rechargeTxnBean.getAggreatorid())
						.replace("<<NAME>>", walletMastBean.getName())
						.replace("<<OPERATOR>>", rechargeTxnBean.getRechargeOperator())
						.replace("<<TYPE>>", rechargeTxnBean.getRechargeType())
						.replace("<<NUMBER>>", rechargeTxnBean.getRechargeNumber())
						.replace("<<AMOUNT>>", Double.toString(rechargeTxnBean.getRechargeAmt()))
						.replace("<<TXNNO>>", rechargeTxnBean.getRechargeId()).replace("<<WALLETBALANCE>>", Double
								.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
						rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
						"Recharge Send SMS" + smstemplet);
				String mailSub = "Recharge of Rs " + String.format("%.2f", rechargeTxnBean.getRechargeAmt()) + " for "
						+ rechargeTxnBean.getRechargeNumber() + " is successful.";
				String mailContent = commanUtilDao.getmailTemplet("recharge", rechargeTxnBean.getAggreatorid())
						.replace("<<NAME>>", walletMastBean.getName())
						.replace("<<OPERATOR>>", rechargeTxnBean.getRechargeOperator())
						.replace("<<TYPE>>", rechargeTxnBean.getRechargeType())
						.replace("<<NUMBER>>", rechargeTxnBean.getRechargeNumber())
						.replace("<<AMOUNT>>", String.format("%.2f", rechargeTxnBean.getRechargeAmt()))
						.replace("<<TXNNO>>", rechargeTxnBean.getRechargeId())
						.replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>", Double
								.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
				;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
						rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
						"Recharge Send  MAIL" + mailContent);
				/*
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,
						mailSub, walletMastBean.getMobileno(), smstemplet, "Both", rechargeTxnBean.getAggreatorid(),
						rechargeTxnBean.getWalletId(), walletMastBean.getName(), "NOTP");
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				*/
				
			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), rechargeTxnBean.getAggreatorid(),
						rechargeTxnBean.getWalletId(), rechargeTxnBean.getUserId(), "", "payForPayment()",
						"problem in payForPayment transfer while SMS Mail And GCM " + e.getMessage() + " " + e);
			}

		}
		rechargeTxnBean.setStatusCode(status);
	}else {
		rechargeTxnBean.setStatusCode("0");
	}
		
		return rechargeTxnBean;

	}


	
	 public Result getRechargeStatusNew(Result results)
	 {
	  // b5c63372351e4649ab5314751c82dbc7	
	  Result result=new Result();
	  String url="https://www.rechargebill.in/apiservice.asmx/GetRechargeStatus?apiToken=b5c63372351e4649ab5314751c82dbc7&reqid="+results.getReqid();
			
	  logger.info("Start excution **************** reqPara" + url);	
	  DefaultHttpClient httpClient = new DefaultHttpClient();
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"getRecharge Status()"+results.getReqid());
		
	  try
	  {
	     HttpGet getRequest = new HttpGet(url);
	     getRequest.addHeader("accept", "application/xml");
	     HttpResponse response = httpClient.execute(getRequest);
	     int statusCode = response.getStatusLine().getStatusCode();
	     if (statusCode != 200) 
	     {
	         throw new RuntimeException("Failed with HTTP error code : " + statusCode);
	     }
	     HttpEntity httpEntity = response.getEntity();
	     String apiOutput = EntityUtils.toString(httpEntity);
	     
	     System.out.println(apiOutput);
	     org.json.JSONObject jsonObj = XML.toJSONObject(apiOutput); 
	     org.json.JSONObject json = jsonObj.getJSONObject("Result");
	     //System.out.println("MOB "+json.get("mn"));
	    
	     //result.setAmt(json.get("amt").toString());
	     result.setBalance(json.get("balance").toString());
	     //result.setMn(json.get("mn").toString());
	     result.setStatus(json.get("status").toString());
	     //result.setEc(json.get("ec").toString());
	     result.setReqid(json.get("reqid").toString());
	     result.setApirefid(json.get("apirefid").toString());
	     result.setField1(json.get("field1").toString());
	     result.setRemark(json.get("remark").toString());
	     
	     System.out.println(result.toString());
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"getRecharge Status()");
	 	
	     //Gson gson = new Gson();
		 //status=gson.toJson(result);
	     //return result;
		  }catch(Exception e)
		  {
			  e.printStackTrace();
			  return result;
		  }
		  finally
		  {
		     httpClient.getConnectionManager().shutdown();
		 }
  
		 return result;
	 }
	
	

public BillerInfoResponse getBillerInfo(BillInfoRequest billRequst, String ipimei, String agetCode) {
	System.out.println("G E T   B I L L E R   I N F O");
	
	BbpsBillerService bbpsService = new BillerBbpsServiceImpl();
	BillerInfoResponse response = bbpsService.getBillInfo(billRequst);
	
	return response;
}


public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2CMoneyTxnMast) {

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ,"b2CMoneyTxn()");
	//logger.info("start excution ===================== method B2CMoneyTxn(b2CMoneyTxnMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	b2CMoneyTxnMast.setStatusCode("1001");
	b2CMoneyTxnMast.setStatusMsg("FAILED");
	double surChargeAmount=0;
	String walletSurChargeTxnCode="1011";
	try{
		
		TreeMap<String,String> obj=new TreeMap<String,String>();
		obj.put("userId",b2CMoneyTxnMast.getUserId());
        obj.put("walletId",b2CMoneyTxnMast.getWalletId());
        obj.put("amount",""+b2CMoneyTxnMast.getAmount());
        obj.put("ipimei",b2CMoneyTxnMast.getIpimei());
        obj.put("aggreatorId",b2CMoneyTxnMast.getAggreatorId());
        obj.put("accHolderName",b2CMoneyTxnMast.getAccHolderName());
        obj.put("accountNo",b2CMoneyTxnMast.getAccountNo());
        obj.put("ifscCode",b2CMoneyTxnMast.getIfscCode());
        obj.put("remarks",b2CMoneyTxnMast.getRemarks());
        obj.put("transferType",b2CMoneyTxnMast.getTransferType());
        obj.put("CHECKSUMHASH",b2CMoneyTxnMast.getCHECKSUMHASH());
		
		int statusCode= WalletSecurityUtility.checkSecurity(obj);
		 if(statusCode!=1000){
			b2CMoneyTxnMast.setStatusCode(""+statusCode);
		   	b2CMoneyTxnMast.setStatusMsg("ERROR");
			return b2CMoneyTxnMast;
	     }
			
		
		
		transaction = session.beginTransaction();
		SQLQuery insertQueryt = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
		insertQueryt.setParameter(0, b2CMoneyTxnMast.getWalletId());
		insertQueryt.executeUpdate();
		transaction.commit();
		transaction = session.beginTransaction();
		
		if(b2CMoneyTxnMast.getUserId()==null||b2CMoneyTxnMast.getUserId().isEmpty()){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("USER ID INVALID.");
			return b2CMoneyTxnMast;
		}
		
		if(b2CMoneyTxnMast.getAggreatorId()==null||b2CMoneyTxnMast.getAggreatorId().isEmpty()){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("Aggreator ID INVALID.");
			return b2CMoneyTxnMast;
		}
		
		if(b2CMoneyTxnMast.getAccHolderName()==null||b2CMoneyTxnMast.getAccHolderName().isEmpty()){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("Acc Holder Name INVALID.");
			return b2CMoneyTxnMast;
		}
		
		if(b2CMoneyTxnMast.getAccountNo()==null||b2CMoneyTxnMast.getAccountNo().isEmpty()){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("Account Number INVALID.");
			return b2CMoneyTxnMast;
		}
		
		if(b2CMoneyTxnMast.getIfscCode()==null||b2CMoneyTxnMast.getIfscCode().isEmpty()){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("Ifsc Code INVALID.");
			return b2CMoneyTxnMast;
		}
		
		if(b2CMoneyTxnMast.getAmount()<=0){
			b2CMoneyTxnMast.setStatusCode("1001");
			b2CMoneyTxnMast.setStatusMsg("Amount INVALID.");
			return b2CMoneyTxnMast;
		}
		
		
	
	
		
		
		
		String sendtrxId=commanUtilDao.getTrxId("WTAMT",b2CMoneyTxnMast.getAggreatorId());
		b2CMoneyTxnMast.setWtbtxnId(sendtrxId);
		b2CMoneyTxnMast.setStatus("Pending");
		session.save(b2CMoneyTxnMast);
		
		SurchargeBean surchargeBean=new SurchargeBean();
		surchargeBean.setId(b2CMoneyTxnMast.getUserId());
		surchargeBean.setAmount(b2CMoneyTxnMast.getAmount());
		surchargeBean= claculateSarcharge(surchargeBean);
		
		if(surchargeBean.getStatus().equalsIgnoreCase("1000")){
			surChargeAmount=surchargeBean.getSarchargeAmount();
		}else{
			b2CMoneyTxnMast.setStatusCode("1122");
			b2CMoneyTxnMast.setStatusMsg("Surcharge not define.Please contact admin.");
			return b2CMoneyTxnMast;
		}
			
		
		
		if((surChargeAmount+b2CMoneyTxnMast.getAmount())>new TransactionDaoImpl().getWalletBalance(b2CMoneyTxnMast.getWalletId())){
			b2CMoneyTxnMast.setStatusCode("7024");
			b2CMoneyTxnMast.setStatusMsg("Insufficient Agent Wallet Balance.");
			return b2CMoneyTxnMast;
		}
		
		
		String walletResp= walletToAccount(b2CMoneyTxnMast.getUserId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getAmount(),b2CMoneyTxnMast.getAccountNo(),sendtrxId,b2CMoneyTxnMast.getIpimei(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getUseragent(),0);
		
		if(walletResp.equalsIgnoreCase("1000")){
			walletSurChargeTxnCode=new TransactionDaoImpl().walletToAccountSurcharge(b2CMoneyTxnMast.getUserId(),b2CMoneyTxnMast.getWalletId(),surChargeAmount,b2CMoneyTxnMast.getAccountNo(),sendtrxId,b2CMoneyTxnMast.getIpimei(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getUseragent(),0);
		}
			
		if(walletResp.equals("1000") && walletSurChargeTxnCode.equals("1000")){
			
			SQLQuery insertQuery = session.createSQLQuery("" +
			        "insert into daily_neft_icici(mode,txnid,debitacc,creditacc,benname,amount,ifsc,status)VALUES(?,?,?,?,?,?,?,?)");
					insertQuery.setParameter(0, "N");
					insertQuery.setParameter(1, sendtrxId);
					insertQuery.setParameter(2, "003105031195");
					insertQuery.setParameter(3, b2CMoneyTxnMast.getAccountNo());
					insertQuery.setParameter(4, b2CMoneyTxnMast.getAccHolderName());
					insertQuery.setParameter(5, b2CMoneyTxnMast.getAmount());
					insertQuery.setParameter(6, b2CMoneyTxnMast.getIfscCode());
					insertQuery.setParameter(7, 0);
					insertQuery.executeUpdate();
					session.getTransaction().commit();
			
					transaction=session.beginTransaction();
					b2CMoneyTxnMast=(B2CMoneyTxnMast)session.get(B2CMoneyTxnMast.class, sendtrxId);
					b2CMoneyTxnMast.setStatus("NEFT INITIATED");
					b2CMoneyTxnMast.setWallettxnstatus(walletResp);
					b2CMoneyTxnMast.setWallettxnstatus(walletResp);
					session.update(b2CMoneyTxnMast);
					transaction.commit();
					b2CMoneyTxnMast.setStatusCode(walletResp);
					b2CMoneyTxnMast.setStatusMsg("NEFT INITIATED");
					
					
					try{
						 walletConfiguration = commanUtilDao.getWalletConfiguration(b2CMoneyTxnMast.getAggreatorId());
					     walletMastBean=walletUserDao.showUserProfile(b2CMoneyTxnMast.getUserId());
					    
						 
						String mailSub= "Rs. "+String.format("%.2f",b2CMoneyTxnMast.getAmount())+" was successfully sent to Bank.";
					     //String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(askMoneyBean.getReqAmount())).replace("<<MOBILE>>", getMobileNumber(askMoneyBean.getReqWalletId())).replace("<<TXNNO>>", askMoneyBean.getTrxid()).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
					     String mailContent= commanUtilDao.getmailTemplet("sendToAccount",b2CMoneyTxnMast.getAggreatorId()).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", String.format("%.2f",b2CMoneyTxnMast.getAmount())).replace("<<ACCOUNT>>", b2CMoneyTxnMast.getAccountNo()).replace("<<TXNNO>>", sendtrxId).replace("<<DATE>>",getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));;
					     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ,"Ask Money Send  MAIL"+mailContent);
					    // logger.info("~~~~~~~~~~~~~~~~~~~~~Ask Money Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
					     SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub,"", "", "Mail",b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getUserId(),b2CMoneyTxnMast.getAccHolderName(),"NOTP");
//						 Thread t = new Thread(smsAndMailUtility);
//						 t.start();
						 
					     ThreadUtil.getThreadPool().execute(smsAndMailUtility);
						 
		 				}catch (Exception e) {
		 					e.printStackTrace();
		 					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ,"problem in Account transfer mail " + e.getMessage()+" "+e); 
		 					//logger.debug("problem in Account transfer mail " + e.getMessage(), e);
		 				}
					
					
					
		}else{
				if (walletResp.equals("1000")) {
					settlement(b2CMoneyTxnMast.getUserId(), b2CMoneyTxnMast.getWalletId(), b2CMoneyTxnMast.getAmount(),
							sendtrxId, b2CMoneyTxnMast.getIpimei(), b2CMoneyTxnMast.getAccountNo(),
							b2CMoneyTxnMast.getAggreatorId(), b2CMoneyTxnMast.getUseragent());
				}
				if (walletSurChargeTxnCode.equals("1000")) {
					surchargeRefundSettlement(b2CMoneyTxnMast.getUserId(), b2CMoneyTxnMast.getWalletId(),
							surChargeAmount, sendtrxId, b2CMoneyTxnMast.getIpimei(),
							b2CMoneyTxnMast.getAccountNo(), b2CMoneyTxnMast.getAggreatorId(),
							b2CMoneyTxnMast.getUseragent());
				}
						
			transaction=session.beginTransaction();
			b2CMoneyTxnMast=(B2CMoneyTxnMast)session.get(B2CMoneyTxnMast.class, sendtrxId);
			b2CMoneyTxnMast.setStatus("Reject From Wallet");
			b2CMoneyTxnMast.setWallettxnstatus(walletResp);
			session.update(b2CMoneyTxnMast);
			transaction.commit();
			b2CMoneyTxnMast.setStatusCode(walletResp);
			b2CMoneyTxnMast.setStatusMsg("Reject From Wallet");
		}
		
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ,"problem in B2CMoneyTxn" + e.getMessage()+" "+e);
		//logger.debug("problem in B2CMoneyTxn=========================" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		b2CMoneyTxnMast.setStatusCode("1001");
   		b2CMoneyTxnMast.setStatusMsg("ERROR");
   		
   	} finally {
   		transaction=session.beginTransaction();
		SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+ b2CMoneyTxnMast.getWalletId()+"'");
		delete.executeUpdate();
		transaction.commit();
   		session.close();
   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),b2CMoneyTxnMast.getAggreatorId(),b2CMoneyTxnMast.getWalletId(),b2CMoneyTxnMast.getUserId(), "","" ," excution accno status"+b2CMoneyTxnMast.getAccountNo());
	//logger.info(" excution ===============accno====== status"+b2CMoneyTxnMast.getAccountNo());
    return b2CMoneyTxnMast;
	
}






/**
 * 
 * @param walletToBankTxnMast
 * @return
 */

public WalletToBankTxnMast tansferInAccount(WalletToBankTxnMast walletToBankTxnMast) {
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"tansferInAccount()");
	//logger.info("start excution ===================== method tansferInAccount(walletToBankTxnMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	
	walletToBankTxnMast.setStatusCode("1001");
	try{
		
		String sendtrxId=commanUtilDao.getTrxId("WTAMT",walletToBankTxnMast.getBankrefNo());
		walletToBankTxnMast.setWtbtxnid(sendtrxId);
		walletToBankTxnMast.setStatus("Pending");
		session.save(walletToBankTxnMast);
		transaction.commit();
		String walletResp= walletToAccount(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getAmount(),walletToBankTxnMast.getAccno(),sendtrxId,walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getUseragent(),walletToBankTxnMast.getSurCharge());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"walletResp"+walletResp);
		//logger.info("walletResp ===================== method tansferInAccount(walletToBankTxnMast)"+walletResp);
		if(walletResp.equals("1000")){
			if(bankApi().equals("Success")){
				transaction=session.beginTransaction();
				walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
				walletToBankTxnMast.setStatus("Success");
				walletToBankTxnMast.setWallettxnstatus(walletResp);
				walletToBankTxnMast.setBanktxnstatus("Success");
				walletToBankTxnMast.setBankrefNo(walletResp);
				session.update(walletToBankTxnMast);
				transaction.commit();
				walletToBankTxnMast.setStatusCode(walletResp);
				
			}else{
				transaction=session.beginTransaction();
				settlement(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getAmount(),sendtrxId,walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getMobileno(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getUseragent());
				walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
				walletToBankTxnMast.setStatus("Fail");
				walletToBankTxnMast.setWallettxnstatus(walletResp);
				walletToBankTxnMast.setBanktxnstatus("Fail");
				walletToBankTxnMast.setBankrefNo(walletResp);
				session.update(walletToBankTxnMast);
				transaction.commit();
				walletToBankTxnMast.setStatusCode("7031");
			}
		}else{
			transaction=session.beginTransaction();
			walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
			walletToBankTxnMast.setStatus("Reject From Wallet");
			walletToBankTxnMast.setWallettxnstatus(walletResp);
			session.update(walletToBankTxnMast);
			transaction.commit();
			walletToBankTxnMast.setStatusCode(walletResp);
		}
	   }catch(Exception e){
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"problem in tansferInAccount" + e.getMessage()+" "+e);
		  // logger.debug("problem in tansferInAccount=========================" + e.getMessage(), e);
	    	 e.printStackTrace();
	   		transaction.rollback();
	   		walletToBankTxnMast.setStatusCode("7000");
	   		
	   	} finally {
	   				session.close();
	   	}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ," Excution accno status"+walletToBankTxnMast.getAccno());   
	//logger.info(" excution ===============accno====== status"+walletToBankTxnMast.getAccno());
	    return walletToBankTxnMast;
}






public WalletToBankTxnMast dmtTransfer(WalletToBankTxnMast walletToBankTxnMast) {

    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"dmtTransfer()");
    //logger.info("start excution ===================== method tansferInAccount(walletToBankTxnMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	walletToBankTxnMast.setStatusCode("1001");
	try{
		String sendtrxId=commanUtilDao.getTrxId("WTAMT",walletToBankTxnMast.getAggreatorid());
		walletToBankTxnMast.setWtbtxnid(sendtrxId);
		walletToBankTxnMast.setStatus("Pending");
		System.out.println("User Agent :"+walletToBankTxnMast.getUseragent());
		session.save(walletToBankTxnMast);
		transaction.commit();
		String walletResp= walletToAccount(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getAmount(),walletToBankTxnMast.getAccno(),sendtrxId,walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getUseragent(),walletToBankTxnMast.getSurCharge());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"walletResp"+walletResp);
		//logger.info("walletResp ===================== method tansferInAccount(walletToBankTxnMast)"+walletResp);
		if(walletResp.equals("1000")){

			transaction=session.beginTransaction();
			walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
			walletToBankTxnMast.setStatus("Accepted From Wallet");
			walletToBankTxnMast.setWallettxnstatus(walletResp);
			walletToBankTxnMast.setBanktxnstatus("Pending");
			session.update(walletToBankTxnMast);
			transaction.commit();
			walletToBankTxnMast.setStatusCode(walletResp);
		}else{
			transaction=session.beginTransaction();
			walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
			walletToBankTxnMast.setStatus("Reject From Wallet");
			walletToBankTxnMast.setWallettxnstatus(walletResp);
			session.update(walletToBankTxnMast);
			transaction.commit();
			walletToBankTxnMast.setStatusCode(walletResp);
		}
	 }catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"problem in tansferInAccount"+ e.getMessage()+" "+e);	
		 //logger.debug("problem in tansferInAccount=========================" + e.getMessage(), e);
	    	 e.printStackTrace();
	   		transaction.rollback();
	   		walletToBankTxnMast.setStatusCode("7000");
	   		
	   	} finally {
	   				session.close();
	   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ," excution accno status"+walletToBankTxnMast.getAccno());    
	//logger.info(" excution ===============accno====== status"+walletToBankTxnMast.getAccno());
	    return walletToBankTxnMast;
}





public WalletToBankTxnMast dmtReinitiateTransfer(WalletToBankTxnMast walletToBankTxnMast) {
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"dmtReinitiateTransfer()");
	//logger.info("start excution ===================== method dmtReinitiateTransfer(walletToBankTxnMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	walletToBankTxnMast.setStatusCode("1001");
	try{
		String sendtrxId=commanUtilDao.getTrxId("WTAMT",walletToBankTxnMast.getAggreatorid());
		walletToBankTxnMast.setWtbtxnid(sendtrxId);
		walletToBankTxnMast.setStatus("Pending");
		System.out.println("User Agent :"+walletToBankTxnMast.getUseragent());
		session.save(walletToBankTxnMast);
		transaction.commit();
		/*
		
		
		String walletResp= walletToAccount(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getAmount(),walletToBankTxnMast.getAccno(),sendtrxId,walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getUseragent(),walletToBankTxnMast.getSurCharge());
		logger.info("walletResp ===================== method tansferInAccount(walletToBankTxnMast)"+walletResp);
		if(walletResp.equals("1000")){
*/			String walletResp="1000";
			transaction=session.beginTransaction();
			walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
			walletToBankTxnMast.setStatus("Accepted From Wallet-Reinitiate");
			walletToBankTxnMast.setWallettxnstatus(walletResp);
			walletToBankTxnMast.setBanktxnstatus("Pending");
			session.update(walletToBankTxnMast);
			transaction.commit();
			walletToBankTxnMast.setStatusCode(walletResp);
		/*}else{
			transaction=session.beginTransaction();
			walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, sendtrxId);
			walletToBankTxnMast.setStatus("Reject From Wallet");
			walletToBankTxnMast.setWallettxnstatus(walletResp);
			session.update(walletToBankTxnMast);
			transaction.commit();
			walletToBankTxnMast.setStatusCode(walletResp);
		}*/
	 }catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"problem in dmtReinitiateTransfer" + e.getMessage()+" "+e);	
		// logger.debug("problem in dmtReinitiateTransfer=========================" + e.getMessage(), e);
	    	 e.printStackTrace();
	   		transaction.rollback();
	   		walletToBankTxnMast.setStatusCode("7000");
	   		
	   	} finally {
	   				session.close();
	   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ," excution dmtReinitiateTransfer accno status"+walletToBankTxnMast.getAccno());   
	//logger.info(" excution ===========dmtReinitiateTransfer====accno====== status"+walletToBankTxnMast.getAccno());
	    return walletToBankTxnMast;
}



public void dmtTransferUpdate(UserDetails  userDetails,String dmtResponse) {
	
	//logger.info("start excution ===================== method tansferInAccount(walletToBankTxnMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	WalletToBankTxnMast walletToBankTxnMast=new WalletToBankTxnMast();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"dmtTransferUpdate()");
	try{
	
		walletToBankTxnMast=(WalletToBankTxnMast)session.get(WalletToBankTxnMast.class, userDetails.getAgentTransId());
				if(userDetails.getResponse().equalsIgnoreCase("SUCCESS")){
					walletToBankTxnMast.setStatus(userDetails.getMoneyRemittance().getTransferStatus());
					walletToBankTxnMast.setBanktxnstatus(userDetails.getMoneyRemittance().getTransferStatus());
					walletToBankTxnMast.setBankrefNo(userDetails.getRecharge().getPaycTransId());
					walletToBankTxnMast.setPaymentStatus(userDetails.getMoneyRemittance().getPaymentStatus());
					walletToBankTxnMast.setTransferStatus(userDetails.getMoneyRemittance().getTransferStatus());
					walletToBankTxnMast.setFundTransno(userDetails.getMoneyRemittance().getFundTransno());
					walletToBankTxnMast.setPaymentId(userDetails.getMoneyRemittance().getPaymentId());
					if(userDetails.getMoneyRemittance().getTransferStatus().equalsIgnoreCase("FAILED")){
					dmtSettlement(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),(walletToBankTxnMast.getAmount()+walletToBankTxnMast.getSurCharge()),walletToBankTxnMast.getWtbtxnid(),walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getMobileno(),walletToBankTxnMast.getAggreatorid(),"");
					}else{
						try{
						 walletConfiguration = commanUtilDao.getWalletConfiguration(walletToBankTxnMast.getAggreatorid());
					     walletMastBean=walletUserDao.showUserProfile(walletToBankTxnMast.getUserId());
						   
					     
				
					     //String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",walletToBankTxnMast.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(walletToBankTxnMast.getAmount())).replace("<<MOBILE>>", walletToBankTxnMast.getAccno()).replace("<<TXNNO>>", walletToBankTxnMast.getWtbtxnid());
					     String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",walletToBankTxnMast.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(walletToBankTxnMast.getAmount())).replace("<<MOBILE>>", walletToBankTxnMast.getAccno()).replace("<<TXNNO>>", walletToBankTxnMast.getWtbtxnid()).replace("<<BENE NAME>>", getNameByUserId(walletToBankTxnMast.getMobileno(), walletToBankTxnMast.getAggreatorid()));
					     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"Wallet to Wallet Send SMS"+ smstemplet);
					    // logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
					    String mailSub= "You have made payment of Rs. "+String.format("%.2f", Double.toString(walletToBankTxnMast.getAmount()))+" to A/C :"+walletToBankTxnMast.getAccno()+".";
					    //String mailContent= commanUtilDao.getmailTemplet("sendMoney",walletToBankTxnMast.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(walletToBankTxnMast.getAmount())).replace("<<MOBILE>>", walletToBankTxnMast.getAccno()).replace("<<TXNNO>>", walletToBankTxnMast.getWtbtxnid()).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
					    String mailContent= commanUtilDao.getmailTemplet("sendMoney",walletToBankTxnMast.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", String.format("%.2f", walletToBankTxnMast.getAmount())).replace("<<MOBILE>>", walletToBankTxnMast.getAccno()).replace("<<TXNNO>>", walletToBankTxnMast.getWtbtxnid()).replace("<<BENE NAME>>", getNameByUserId(walletToBankTxnMast.getMobileno(), walletToBankTxnMast.getAggreatorid())).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
					    
					    
					    
					    
					    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"Wallet to Wallet Send  MAIL"+ mailContent);
					    //logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
					     	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletMastBean.getName(),"NOTP");
//							Thread t = new Thread(smsAndMailUtility);
//							t.start();	
					     	
					     	ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					     	
					}catch (Exception e) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"problem in dmtTransferUpdate transfer while SMS Mail And GCM " + e.getMessage()+" "+e); 
						//logger.debug("problem in dmtTransferUpdate transfer while SMS Mail And GCM " + e.getMessage(), e);
					}
						
						
					}
				}else{
					walletToBankTxnMast.setStatus("Falied");
					dmtSettlement(walletToBankTxnMast.getUserId(),walletToBankTxnMast.getWalletId(),(walletToBankTxnMast.getAmount()+walletToBankTxnMast.getSurCharge()),walletToBankTxnMast.getWtbtxnid(),walletToBankTxnMast.getIpimei(),walletToBankTxnMast.getMobileno(),walletToBankTxnMast.getAggreatorid(),"");
					
				}
				walletToBankTxnMast.setDmtresponse(dmtResponse);
				session.save(walletToBankTxnMast);
				transaction.commit();
				
	 }catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ,"problem in dmtTransferUpdate" + e.getMessage()+" "+e);	
		// logger.debug("problem in dmtTransferUpdate=========================" + e.getMessage(), e);
	    	 e.printStackTrace();
	   		transaction.rollback();
	   	} finally {
	   				session.close();
	   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletToBankTxnMast.getAggreatorid(),walletToBankTxnMast.getWalletId(),walletToBankTxnMast.getUserId(), "","" ," excution accno status"+walletToBankTxnMast.getAccno());  
	//logger.info(" excution ===============accno====== status"+walletToBankTxnMast.getAccno());
	
}







public  Boolean mailInvoice(String rechargeId){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"mailInvoice()");
	//logger.info("start excution ===================== method mailInvoice(rechargeId)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	boolean flag=false;
	try{
		
		Date dNow = new Date();
        SimpleDateFormat ft=new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
       	RechargeTxnBean rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
		String mailtemplet = commanUtilDao.getmailTemplet("INVOICE",rechargeTxnBean.getAggreatorid());
		
		mailtemplet=mailtemplet.replaceAll("<<TXNID>>", rechargeId);
		mailtemplet=mailtemplet.replaceAll("<<MOBILE>>", rechargeTxnBean.getRechargeNumber());
		mailtemplet=mailtemplet.replaceAll("<<RECHARGETYPE>>", rechargeTxnBean.getRechargeType()+"-"+rechargeTxnBean.getRechargeOperator());
		mailtemplet=mailtemplet.replaceAll("<<DATE>>",  ft.format(dNow));
		mailtemplet=mailtemplet.replaceAll("<<DESC>>", rechargeTxnBean.getRechargeCircle()+"-"+rechargeTxnBean.getRechargeType()+"-"+rechargeTxnBean.getRechargeOperator());
		mailtemplet=mailtemplet.replaceAll("<<AMOUNT>>", ""+String.format("%.2f", rechargeTxnBean.getRechargeAmt()));
		mailtemplet=mailtemplet.replaceAll("<<STATUS>>", ""+rechargeTxnBean.getStatus());
		String subject="Invoice For "+rechargeTxnBean.getRechargeNumber();
		SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(rechargeTxnBean.getUserEmail(), mailtemplet,subject, "", "", "Mail","0","","","NOTP");
//		Thread t = new Thread(smsAndMailUtility);
//		t.start();
		
		ThreadUtil.getThreadPool().execute(smsAndMailUtility);
		flag=true;
	 }catch(Exception e){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in tansferInAccount" + e.getMessage()+" "+e);
	    	//logger.debug("problem in tansferInAccount=========================" + e.getMessage(), e);
	    	 e.printStackTrace();
	   		transaction.rollback();
	   		flag=false;
	   	} finally {
	   				session.close();
	   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ," excution status"+flag);
	    //logger.info(" excution ===================== status"+flag);
	    return flag;
}



public PrePaidCardResponse prePaidCardReversal(PrePaidCardTxnBean prePaidCardTxnBean){

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"prePaidCardReversal()");
	//logger.info("start excution*********************************************************** method prePaidCardReversal(prePaidCardTxnBean)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	PrePaidCardResponse prePaidCardResponse=new PrePaidCardResponse();
	if(prePaidCardTxnBean.getTransaction_ref_no()==null ||prePaidCardTxnBean.getTransaction_ref_no().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"excution THIS TRANSACTION REF NO :"+prePaidCardTxnBean.getTransaction_ref_no()+" IS NOT ALLOWED FORTRANSACTION");
		//logger.info("excution**************************************************THIS TRANSACTION REF NO :"+prePaidCardTxnBean.getTransaction_ref_no()+" IS NOT ALLOWED FORTRANSACTION**********************************");
		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription("Transaction Ref Number is null or emppty");
		prePaidCardResponse.setSettlement_date( getDate());
		return prePaidCardResponse;
	}
	try{
		Criteria cr= session.createCriteria(PrePaidCardTxnBean.class);
		cr.add(Restrictions.eq("transaction_ref_no", prePaidCardTxnBean.getTransaction_ref_no()));
		cr.add(Restrictions.eq("status", "SUCCESS"));
		List<PrePaidCardTxnBean> preTxnList=cr.list();
		if(preTxnList!=null && preTxnList.size()>0){
			prePaidCardTxnBean=preTxnList.get(0);
			cr= session.createCriteria(WalletMastBean.class);
			cr.add(Restrictions.eq("prePaidUserId", prePaidCardTxnBean.getUser_id()));
			cr.add(Restrictions.eq("prePaidCardId", prePaidCardTxnBean.getCard_id()));
			List<WalletMastBean> walletMastList=cr.list();
			if(walletMastList!=null && walletMastList.size()>0){
				WalletMastBean walletMastBean=walletMastList.get(0);
				if(prePaidCardReversalSettlement(walletMastBean.getId(),walletMastBean.getWalletid(),prePaidCardTxnBean.getTransaction_amount(),prePaidCardTxnBean.getTxnReqId(),"",walletMastBean.getMobileno(),walletMastBean.getAggreatorid(),"").equalsIgnoreCase("1000")){
					transaction=session.beginTransaction();
					prePaidCardTxnBean=(PrePaidCardTxnBean)session.get(PrePaidCardTxnBean.class, prePaidCardTxnBean.getTxnReqId());
					prePaidCardTxnBean.setStatus("REFUNDED");
					session.update(prePaidCardTxnBean);
					transaction.commit();
					prePaidCardResponse.setStatus("SUCCESS");
					prePaidCardResponse.setDescription("SUCCESS");
					prePaidCardResponse.setSettlement_date( getDate());
					return prePaidCardResponse;	
				
				}else{
					prePaidCardResponse.setStatus("ERROR");
					prePaidCardResponse.setDescription("Please try after some time.");
					prePaidCardResponse.setSettlement_date( getDate());
					return prePaidCardResponse;	
				}
					
			}else{
				prePaidCardResponse.setStatus("ERROR");
				prePaidCardResponse.setDescription("PaidUserId OR PaidCardId  is not valid.");
				prePaidCardResponse.setSettlement_date( getDate());
				return prePaidCardResponse;	
			}
		}else{
			prePaidCardResponse.setStatus("ERROR");
			prePaidCardResponse.setDescription("Not a valied Transaction Ref Number.");
			prePaidCardResponse.setSettlement_date( getDate());
			return prePaidCardResponse;
			
		}
		
			
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"problem in prePaidCardReversal" + e.getMessage()+" "+e);
		//logger.debug("problem in prePaidCardReversal***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription(e.getMessage());
		prePaidCardResponse.setSettlement_date( getDate());
   	} finally {
   	session.close();
   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"Return Status"+prePaidCardResponse.getStatus());
	//logger.info(" *****************************Return Status******************************"+prePaidCardResponse.getStatus());
	return prePaidCardResponse;	
}


public PrePaidCardResponse prePaidCardPaymentAutorization (PrePaidCardTxnBean prePaidCardTxnBean){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"prePaidCardPaymentAutorization()");
	//logger.info("start excution*********************************************************** method prePaidCardPaymentAutorization(prePaidCardTxnBean)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	PrePaidCardResponse prePaidCardResponse=new PrePaidCardResponse();
	
	if(prePaidCardTxnBean.getCard_id()==null ||prePaidCardTxnBean.getCard_id().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"excution Card id is null or emppty");
		//logger.info("excution**************************************************Card id is null or emppty***********************************");
		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription("Card id is null or emppty");
		prePaidCardResponse.setSettlement_date( getDate());
		return prePaidCardResponse;
	}
	
	if(prePaidCardTxnBean.getUser_id()==null ||prePaidCardTxnBean.getUser_id().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"excution User id is null or emppty");
		//logger.info("excution**************************************************User id is null or emppty***********************************");
		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription("User id is null or emppty");
		prePaidCardResponse.setSettlement_date( getDate());
		return prePaidCardResponse;
	}
	
	if(prePaidCardTxnBean.getTransaction_amount()==0 ||prePaidCardTxnBean.getTransaction_amount()<0){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"Excution THIS TRANSACTION AMOUNT :"+prePaidCardTxnBean.getTransaction_amount()+" IS NOT ALLOWED FORTRANSACTION");
		//logger.info("excution**************************************************THIS TRANSACTION AMOUNT :"+prePaidCardTxnBean.getTransaction_amount()+" IS NOT ALLOWED FORTRANSACTION**********************************");
		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription("THIS TRANSACTION AMOUNT :"+prePaidCardTxnBean.getTransaction_amount()+" IS NOT ALLOWED FORTRANSACTION");
		prePaidCardResponse.setSettlement_date( getDate());
		return prePaidCardResponse;
	}
	if(prePaidCardTxnBean.getTransaction_ref_no()==null ||prePaidCardTxnBean.getTransaction_ref_no().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"Excution THIS TRANSACTION REF NO :"+prePaidCardTxnBean.getTransaction_ref_no()+" IS NOT ALLOWED FORTRANSACTION");
		//logger.info("excution**************************************************THIS TRANSACTION REF NO :"+prePaidCardTxnBean.getTransaction_ref_no()+" IS NOT ALLOWED FORTRANSACTION**********************************");
		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription("Transaction Ref Number is null or emppty");
		prePaidCardResponse.setSettlement_date( getDate());
		return prePaidCardResponse;
	}
	
	try{
		
		String txnReqId=commanUtilDao.getTrxId("SMARTCARDTXN","OAGG001050");
		prePaidCardTxnBean.setTxnReqId(txnReqId);
		prePaidCardTxnBean.setStatus("Pending");
		session.save(prePaidCardTxnBean);
		transaction.commit();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"Data Save With txnReqId"+txnReqId);
		//logger.info("******************************Data Save With txnReqId***************************** "+txnReqId);
		
		Query query=session.createQuery("From WalletMastBean where prePaidUserId=:prePaidUserId and prePaidCardId=:prePaidCardId and prePaidCardStatus=:prePaidCardStatus");
		query.setString("prePaidUserId", prePaidCardTxnBean.getUser_id());
		query.setString("prePaidCardId", prePaidCardTxnBean.getCard_id());
		query.setString("prePaidCardStatus", "Active");
		
		List <WalletMastBean> userList=query.list();
		if(userList!=null && userList.size()>0){
			
			String walletResp=  prePaidCardTxn(userList.get(0).getId(),userList.get(0).getWalletid(),prePaidCardTxnBean.getTransaction_amount(),prePaidCardTxnBean.getMerchant_name(),txnReqId,"",userList.get(0).getAggreatorid(),prePaidCardTxnBean.getChannel());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"walletResp method prePaidCardPaymentAutorization()"+walletResp);
			//logger.info("walletResp ==========*********************=========== method prePaidCardPaymentAutorization()"+walletResp);
			
			if(walletResp.equals("1000")){
				transaction=session.beginTransaction();
				prePaidCardTxnBean=(PrePaidCardTxnBean)session.get(PrePaidCardTxnBean.class, txnReqId);
				prePaidCardTxnBean.setStatus("SUCCESS");
				prePaidCardTxnBean.setStatusdescription("SUCCESS");
				session.update(prePaidCardTxnBean);
				transaction.commit();
				prePaidCardResponse.setStatus("SUCCESS");
				prePaidCardResponse.setDescription("SUCCESS");
				prePaidCardResponse.setSettlement_date(getDate());
				prePaidCardResponse.setBalance(getWalletBalance(userList.get(0).getWalletid()));
				return prePaidCardResponse;
				}else{
					String statusDesc="";
					if(walletResp.equals("7032")){
						statusDesc="Txn amount must be greater than 0.";
					}
					else if(walletResp.equals("7024")){
						statusDesc="Insufficient wallet balance.";
					}
					else if(walletResp.equals("7020")){
						statusDesc="Your per day number of transaction exceed.";
					}
					else if(walletResp.equals("7021")){
						statusDesc="Your per week number of transaction exceed.";
					}
					else if(walletResp.equals("7022")){
						statusDesc="Your per month number of transaction exceed.";
					}
					else if(walletResp.equals("7017")){
						statusDesc="Your per day total transfer amount exceed.";
					}
					else if(walletResp.equals("7018")){
						statusDesc="Your per week total transfer amount exceed.";
					}
					else if(walletResp.equals("7019")){
						statusDesc="Your per month total transfer amount exceed.";
					}
					else if(walletResp.equals("7045")){
						statusDesc="Your maximum transaction per quarter have been exceed.";
					}
					else if(walletResp.equals("7046")){
						statusDesc="Your maximum transaction per half yearly have been exceed.";
					}
					else if(walletResp.equals("7047")){
						statusDesc="Your maximum transaction per year have been exceed.";
					}else{
						statusDesc="Your maximum wallet balance have been exceed.";
					}
					
					
					transaction=session.beginTransaction();
					prePaidCardTxnBean=(PrePaidCardTxnBean)session.get(PrePaidCardTxnBean.class, txnReqId);
					prePaidCardTxnBean.setStatus("ERROR");
					prePaidCardTxnBean.setStatusdescription(statusDesc);
					session.update(prePaidCardTxnBean);
					transaction.commit();
					prePaidCardResponse.setStatus("ERROR");
					prePaidCardResponse.setDescription(statusDesc);
					prePaidCardResponse.setSettlement_date(getDate());
					return prePaidCardResponse;
			}
				
		}else{
			transaction=session.beginTransaction();
			prePaidCardResponse.setStatus("ERROR");
			prePaidCardResponse.setDescription("Card_Id or User_id not registered with Us.");
			prePaidCardResponse.setSettlement_date( getDate());
			prePaidCardTxnBean=(PrePaidCardTxnBean)session.get(PrePaidCardTxnBean.class, txnReqId);
			prePaidCardTxnBean.setStatus("ERROR");
			prePaidCardTxnBean.setStatusdescription("Card_Id or User_id not registered with Us.");
			session.update(prePaidCardTxnBean);
			transaction.commit();
			return prePaidCardResponse;
		}
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"problem in prePaidCardPaymentAutorization" + e.getMessage()+" "+e);
		//logger.debug("problem in prePaidCardPaymentAutorization***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		prePaidCardResponse.setStatus("ERROR");
		prePaidCardResponse.setDescription(e.getMessage());
		prePaidCardResponse.setSettlement_date( getDate());
   	} finally {
   	session.close();
   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",prePaidCardTxnBean.getUser_id(), "","" ,"Return Status"+prePaidCardResponse.getStatus());
	//logger.info(" *****************************Return Status******************************"+prePaidCardResponse.getStatus());
	return prePaidCardResponse;	
}

private String getDate(){
	//TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
	//df.setTimeZone(tz);
	return df.format(new Date());
}

private String getDate(String format){
	//TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat df = new SimpleDateFormat(format); // Quoted "Z" to indicate UTC, no timezone offset
	//df.setTimeZone(tz);
	return df.format(new Date());
}

public P2MTransactionBean P2MTransaction(P2MTransactionBean p2MTransactionBean){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"P2MTransaction");
	//logger.info("start excution*********************************************************** method P2MTransaction(p2MTransactionBean)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	p2MTransactionBean.setStatusCode("1001");
	try{
		String impsId=commanUtilDao.getTrxId("IMPS","OAGG001050");
		p2MTransactionBean.setImpsTxnId(impsId);
		p2MTransactionBean.setStatus("Pending");
		session.save(p2MTransactionBean);
		transaction.commit();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"Data Save With txn Id"+impsId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"Call IMPS Implementation");
		
		//logger.info("******************************Data Save With txn Id***************************** "+impsId);
		//logger.info("******************************Call IMPS Implementation***************************** ");
		HashMap<String, String> impsRes=new IMPSImplementation().callImps(p2MTransactionBean);
		
		if(impsRes.get("ExtUniqueRefId").toString().equals(impsId)){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"IMPS Status "+impsRes.get("Status").toString());
			//logger.info("****************************** IMPS Status***************************** "+impsRes.get("Status").toString());
			if(impsRes.get("Status").toString().equals("Failed")){
				transaction = session.beginTransaction();
				p2MTransactionBean.setStatus(impsRes.get("Status").toString());
				p2MTransactionBean.setImpsRefNo(impsRes.get("ReferenceNumber").toString());
				p2MTransactionBean.setImpsResponseCode(impsRes.get("ErrorCode").toString());
				transaction.commit();
				p2MTransactionBean.setStatusCode("9002");
				
			}else{
				
				transaction = session.beginTransaction();
				p2MTransactionBean.setStatus(impsRes.get("Status").toString());
				p2MTransactionBean.setImpsRefNo(impsRes.get("ReferenceNumber").toString());
				p2MTransactionBean.setImpsResponseCode(impsRes.get("ErrorCode").toString());
				transaction.commit();
				p2MTransactionBean.setStatusCode("1000");
				
			}
		}else{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"TXN Id Not Match");
			//logger.info("****************************** TXN Id Not Match***************************** ");
			transaction = session.beginTransaction();
			p2MTransactionBean.setStatus("Cancel");
			session.save(p2MTransactionBean);
			transaction.commit();
			p2MTransactionBean.setStatusCode("9001");
			
		}
		
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ,"Problem in P2MTransaction"+e.getMessage()+" "+e);
		//logger.debug("problem in P2MTransaction***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		p2MTransactionBean.setStatusCode("7000");
   	} finally {
   	session.close();
   	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",p2MTransactionBean.getWalletId(),p2MTransactionBean.getUserId(), "","" ," Return Status"+p2MTransactionBean.getStatusCode());
	//logger.info(" *****************************Return Status******************************"+p2MTransactionBean.getStatusCode());
	return p2MTransactionBean;
}


public boolean acceptCashDeposit(String depositId,String remarks,String ipIemi,String agent){

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"acceptCashDeposit()"+"|"+agent+"|"+depositId);
	//logger.info("start excution ===================== method acceptCashDeposit(depositId)"+depositId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		CashDepositMast cashDepositMast=(CashDepositMast)session.get(CashDepositMast.class, depositId);
		
			cashDepositMast.setCheckerStatus("Accepted");
			cashDepositMast.setStatus("Accepted");
			//cashDepositMast.setRemark(remarks);
			
			System.out.println("CHECKER "+cashDepositMast.getRemarkChecker());
			
			if(cashDepositMast.getRemarkChecker()==null ||  cashDepositMast.getRemarkChecker()=="") 
				cashDepositMast.setRemarkChecker(remarks);
			else
				cashDepositMast.setRemarkApprover(remarks);	
			 
			session.saveOrUpdate(cashDepositMast);
			transaction.commit();
			flag=true;
		
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in acceptCashDeposit" + e.getMessage()+" "+e);
		//logger.debug("problem in acceptCashDeposit==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return flag;
	
}

public String acceptCashDepositByDist(String depositId,String remarks,String ipIemi,String agent){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"acceptCashDepositByDist()"+"|"+agent+"|"+depositId);
	//logger.info("start excution ===================== method acceptCashDepositByDist(depositId)"+depositId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	Transaction transaction=session.beginTransaction();
	try{
		CashDepositMast cashDepositMast=(CashDepositMast)session.get(CashDepositMast.class, depositId);
		WalletMastBean wmSuperDist=(WalletMastBean)session.get(WalletMastBean.class,cashDepositMast.getAggreatorId());
		WalletMastBean wmDist=(WalletMastBean)session.get(WalletMastBean.class,cashDepositMast.getUserId());
//		String result=superDistToDist(wmSuperDist.getId(), wmSuperDist.getWalletid(),wmDist.getMobileno(),cashDepositMast.getAmount(),ipIemi,wmDist.getAggreatorid(),agent);
//		public String superDistToDist(String userId,       String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent){
		 if(cashDepositMast.getAmount()<=0){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"Txn Amount Not Be Zero"+wmSuperDist.getWalletid());
			// logger.info("==========================Txn Amount Not Be Zero=================================="+wmSuperDist.getWalletid());
			return "7032";
		 }
		  
		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(wmDist.getMobileno(),wmDist.getAggreatorid());
		 if(wmSuperDist.getWalletid().equalsIgnoreCase(recWalletid)){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"You can not transfer money to your own wallet");
			// logger.info("==========================You can not transfer money to your own wallet.==================================");
			return "7036";
		}
		if(recWalletid.equals("1001")){
			//wallet id not found.
			return "7023";
		}else if(getWalletBalance(wmSuperDist.getWalletid())>=cashDepositMast.getAmount()+0){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"Start superDistToDist excution");
			logger.info("==========================Start superDistToDist excution==================================");
				String sendtrxId=commanUtilDao.getTrxId("CMEAPP",cashDepositMast.getAggreatorId());
	  			String recivetrxId=commanUtilDao.getTrxId("CMEAGN",cashDepositMast.getAggreatorId());
	  				  		
	  			TransactionBean transactionBean=new TransactionBean();
	  			TransactionBean transactionBeanr=new TransactionBean();
	  		
	  				
	  			transactionBean.setTxnid(sendtrxId);
	  			transactionBean.setUserid(wmSuperDist.getId());
	  			transactionBean.setWalletid(wmSuperDist.getWalletid());
	  			transactionBean.setAggreatorid(wmDist.getAggreatorid());
	  			transactionBean.setTxncode(23);
	  			transactionBean.setTxncredit(0.0);
	  			transactionBean.setTxndebit(cashDepositMast.getAmount());
	  			transactionBean.setSurCharge(0);
	  			transactionBean.setPayeedtl(new TransactionDaoImpl().getIdByUserId(wmDist.getMobileno(),wmDist.getAggreatorid()));
	  			transactionBean.setResult("Success");
	  			transactionBean.setResptxnid(recivetrxId);
	  			transactionBean.setWalletuserip(ipIemi);
	  			transactionBean.setWalletuseragent(agent);
	  		
	  			
	  			
	  			transactionBeanr.setTxnid(recivetrxId);
	  		    transactionBeanr.setUserid(wmSuperDist.getId());
	  		    transactionBeanr.setWalletid(recWalletid);
	  		    transactionBeanr.setAggreatorid(wmDist.getAggreatorid());
	  		    transactionBeanr.setTxncode(24);
	  		    transactionBeanr.setTxncredit(cashDepositMast.getAmount());
	  		    transactionBeanr.setTxndebit(0.0);
	  		    transactionBeanr.setPayeedtl(wmSuperDist.getId());
	  		    transactionBeanr.setResult("Success");
	  		    transactionBeanr.setResptxnid(sendtrxId);
	  		    transactionBeanr.setWalletuserip(wmSuperDist.getId());
	  		    transactionBeanr.setWalletuseragent(agent);
	  		// transactionBeanr.setWalletuseragent("");
	  		    
	  		   deductMaintanceCharges(wmDist.getId(), wmDist.getAggreatorid(), cashDepositMast.getAmount(), ipIemi, agent);
	  		   session.save(transactionBean);
	  		   session.save(transactionBeanr);
	  		   
	  		   	cashDepositMast.setCheckerStatus("Accepted");
				cashDepositMast.setStatus("Approved");
				cashDepositMast.setApproverStatus("Accepted");
				cashDepositMast.setRemark(remarks);
				session.saveOrUpdate(cashDepositMast);
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"superDistToDist transactin commit sucesfully");
				//logger.info("================superDistToDist transactin commit sucesfully================"); 
				return "1000";
			}else{
				return "7025";
			}

	}catch (Exception e) {
		transaction.rollback();
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in acceptCashDepositByDist" + e.getMessage()+" "+e);
		//logger.debug("problem in acceptCashDepositByDist==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return "7024";
	
}







public boolean rejectCashDeposit(String depositId,String remarks){

	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"rejectCashDeposit()"+"|"+depositId);
	//logger.info("start excution ===================== method rejecttCashDeposit(depositId)"+depositId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		CashDepositMast cashDepositMast=(CashDepositMast)session.get(CashDepositMast.class, depositId);
		cashDepositMast.setStatus("Rejected");
		cashDepositMast.setCheckerStatus("Rejected");
		//cashDepositMast.setRemark(remarks);
		System.out.println("CHECKER "+cashDepositMast.getRemarkChecker());
		
		if(cashDepositMast.getRemarkChecker()==null ||  cashDepositMast.getRemarkChecker()=="") 
			cashDepositMast.setRemarkChecker(remarks);
		else
			cashDepositMast.setRemarkApprover(remarks);	
		
		session.saveOrUpdate(cashDepositMast);
		transaction.commit();
		flag=true;
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in rejecttCashDeposit"+e.getMessage()+" "+e);
		//logger.debug("problem in rejecttCashDeposit==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return flag;
	
}

public List<CashDepositMast> getCashDepositReqByUserId(String userId,String stDate,String endDate){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"getCashDepositReqByUserId()");
	//logger.info("start excution ===================== method getCashDepositReqByUserId(userId)"+userId);
	List<CashDepositMast> cashDepositList=new ArrayList<CashDepositMast>();
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder sqlQuery=new StringBuilder("from CashDepositMast where userId=:userId");
		 if(stDate==null ||stDate.isEmpty() ||endDate==null ||endDate.isEmpty()){
			 sqlQuery.append(" and DATE(requestdate) = STR_TO_DATE(REPLACE('"+formate.format(new Date())+"', '/', '-'),'%d-%m-%Y')");
		 }else{
			 sqlQuery.append(" and DATE(requestdate) BETWEEN STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y') order by DATE_FORMAT(requestdate, '%d/%m/%Y %T') DESC");
		 }
		 Query query=session.createQuery(sqlQuery.toString());
		query.setString("userId", userId);
		cashDepositList=query.list();
		}catch(Exception e){
		e.printStackTrace();
		  transaction.rollback();
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"problem in getCashDepositReqByUserId"+e.getMessage()+" "+e);
		 // logger.debug("problem in getCashDepositReqByUserId***********************************************************" + e.getMessage(), e);
		 }
		 finally{
		  session.close();
	 }
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"getCashDepositReqByUserId return"+cashDepositList.size());
	// logger.info("stop excution *********************************getCashDepositReqByUserId******** return**********"+cashDepositList.size());
	return cashDepositList;
}

public List<CashDepositMast> getCashDepositReqByUserIdPending(String userId,String stDate,String endDate){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"getCashDepositReqByUserIdPending()");
	//logger.info("start excution ===================== method getCashDepositReqByUserId(userId)"+userId);
	List<CashDepositMast> cashDepositList=new ArrayList<CashDepositMast>();
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder sqlQuery=new StringBuilder("from CashDepositMast where userId=:userId and status='Pending' ");
		 if(stDate==null ||stDate.isEmpty() ||endDate==null ||endDate.isEmpty()){
			 sqlQuery.append("  order by DATE_FORMAT(requestdate, '%d/%m/%Y %T') DESC limit 100");
		 }else{
			 sqlQuery.append(" and DATE(requestdate) BETWEEN STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y') order by DATE_FORMAT(requestdate, '%d/%m/%Y %T') DESC");
		 }
		 Query query=session.createQuery(sqlQuery.toString());
		query.setString("userId", userId);
		cashDepositList=query.list();
		}catch(Exception e){
		e.printStackTrace();
		  transaction.rollback();
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"problem in getCashDepositReqByUserIdPending"+e.getMessage()+" "+e);
		 // logger.debug("problem in getCashDepositReqByUserId***********************************************************" + e.getMessage(), e);
		 }
		 finally{
		  session.close();
	 }
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "","" ,"getCashDepositReqByUserIdPending return"+cashDepositList.size());
	// logger.info("stop excution *********************************getCashDepositReqByUserId******** return**********"+cashDepositList.size());
	return cashDepositList;
}

public List<CashDepositMast> getCashDepositReqByAggId(String aggreatorId,String type){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"getCashDepositReqByAggId()");
	//logger.info("start excution ===================== method getCashDepositReqByUserId(userId)"+aggreatorId);
	List<CashDepositMast> cashDepositList=new ArrayList<CashDepositMast>();
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		Query query;
		if(type!=null&&type.equalsIgnoreCase("0")){
			 query=session.createQuery("from CashDepositMast cm where (cm.aggreatorId=:aggreatorId or cm.userId in(select wm.cashDepositeWallet from WalletMastBean wm where whiteLabel=:wl and usertype=:ut)) and status='Pending' and checkerstatus='Pending' order by depositId DESC");
			 query.setInteger("wl",1);
			 query.setInteger("ut",4);
		}else{
			 query=session.createQuery("from CashDepositMast cm where cm.aggreatorId=:aggreatorId  and cm.userId not in(select wm.cashDepositeWallet from WalletMastBean wm where id=:aggreatorId) and status='Pending' and checkerstatus='Pending' order by depositId DESC");
		}
		
		query.setString("aggreatorId", aggreatorId);
		

		cashDepositList=query.list();
		}catch(Exception e){
		e.printStackTrace();
		 // transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"problem in getCashDepositReqByUserId"+e.getMessage()+" "+e);
		 // logger.debug("problem in getCashDepositReqByUserId***********************************************************" + e.getMessage(), e);
		 }
		 finally{
		  session.close();
	 }
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"stop excution getCashDepositReqByUserId return"+cashDepositList.size());
	 logger.info("stop excution *********************************getCashDepositReqByUserId******** return**********"+cashDepositList.size());
	return cashDepositList;
}

public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast){

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"requsetCashDeposit()");
	//logger.info("start excution*********************************************************** method requsetCashDeposit(cashDepositMast)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	cashDepositMast.setStatusCode("1001");
	try{
		if(cashDepositMast.getAmount()<=0){
			cashDepositMast.setStatusCode("5501");
			return cashDepositMast;
			
		}
		if(cashDepositMast.getType()==null ||cashDepositMast.getType().isEmpty()){
			cashDepositMast.setStatusCode("5502");
			return cashDepositMast;
		}
		
		/*if(cashDepositMast.getNeftRefNo()==null ||cashDepositMast.getNeftRefNo().isEmpty() ||cashDepositMast.getReciptPic()==null ||cashDepositMast.getReciptPic().isEmpty()){
			cashDepositMast.setStatusCode("5503");
			return cashDepositMast;
		}
		*/
		String depositId=commanUtilDao.getTrxId("CASH",cashDepositMast.getAggreatorId());
		cashDepositMast.setDepositId(depositId);
		cashDepositMast.setStatus("Pending");
		cashDepositMast.setApproverStatus("Pending");
		cashDepositMast.setCheckerStatus("Pending");
		cashDepositMast.setRequestdate(new Timestamp(new java.util.Date().getTime()));
		
		String cashDep = cashDepositMast.getCashDepositDate();
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		Date dt = df.parse(cashDep);
		DateFormat dff = new SimpleDateFormat("yyyy-MM-dd");
		String cashDepo = dff.format(dt);
		cashDepositMast.setCashDepositDate(cashDepo);
		/*if(!(cashDepositMast.getReciptPic()==null) ||!(cashDepositMast.getReciptPic().isEmpty())){
			cashDepositMast.setReciptPic(cashDepositMast.getReciptPic().getBytes());
		}*/
		//cashDepositMast.setApproveDate(new Timestamp(System.currentTimeMillis()));
		session.save(cashDepositMast);
		transaction.commit();
		cashDepositMast.setStatusCode("1000");
		
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"problem in requsetCashDeposit" + e.getMessage()+" "+e);
		//logger.debug("problem in requsetCashDeposit***********************************************************" + e.getMessage(), e);
   	 e.printStackTrace();
  		transaction.rollback();
  		cashDepositMast.setStatusCode("7000");
  	} finally {
  	session.close();
  	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cashDepositMast.getAggreatorId(),cashDepositMast.getWalletId(),cashDepositMast.getUserId(), "","" ,"Return Status"+cashDepositMast.getStatusCode());
	//logger.info(" *****************************Return Status******************************"+cashDepositMast.getStatusCode());
	return cashDepositMast;
}


public RefundMastBean requestRefund(RefundMastBean refundMastBean){
	
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "",refundMastBean.getTxnid() ,"requestRefund()");
	//logger.info("start excution*********************************************************** method requestRefund(refundMastBean)");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	refundMastBean.setStatusCode("1001");
	try{
		if(refundMastBean.getTxnid()==null ||refundMastBean.getTxnid().isEmpty()){
			refundMastBean.setStatusCode("5504");
			return refundMastBean;
		}
		TransactionBean transactionBean=(TransactionBean)session.get(TransactionBean.class, refundMastBean.getTxnid());
		String refundId=commanUtilDao.getTrxId("REFUND",refundMastBean.getAggreatorId());
		refundMastBean.setRefundId(refundId);
		refundMastBean.setAmount(transactionBean.getTxndebit());
		refundMastBean.setStatus("Pending");
		session.save(refundMastBean);
		transactionBean.setStatus(2);
		session.saveOrUpdate(transactionBean);
		transaction.commit();
		refundMastBean.setStatusCode("1000");
	}catch(Exception e){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "",refundMastBean.getTxnid() ,"requestRefund()"+"|"+"problem in requsetCashDeposit" + e.getMessage()+" "+e);
		//logger.debug("problem in requsetCashDeposit***********************************************************" + e.getMessage(), e);
   	 e.printStackTrace();
  		transaction.rollback();
  		refundMastBean.setStatusCode("7000");
  	} finally {
  	session.close();
  	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),refundMastBean.getAggreatorId(),refundMastBean.getWalletId(),refundMastBean.getUserId(), "",refundMastBean.getTxnid() ,"Return Status"+refundMastBean.getStatusCode());
	//logger.info(" *****************************Return Status******************************"+refundMastBean.getStatusCode());
	return refundMastBean;
}

public List<RefundMastBean> getRefundReqByAggId(String aggreatorId){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"getRefundReqByAggId()");
	//logger.info("start excution ===================== method getRefundReqByAggId(userId)"+aggreatorId);
	List<RefundMastBean> refundList=new ArrayList<RefundMastBean>();
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		Query query=session.createQuery("from RefundMastBean where aggreatorId=:aggreatorId and status='Pending' order by refundId DESC");
		query.setString("aggreatorId", aggreatorId);
		refundList=query.list();
		}catch(Exception e){
		e.printStackTrace();
		  transaction.rollback();
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"problem in getRefundReqByAggId" + e.getMessage()+" "+e);
		 // logger.debug("problem in getRefundReqByAggId***********************************************************" + e.getMessage(), e);
		 }
		 finally{
		  session.close();
	 }
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"stop excution getRefundReqByAggId return"+refundList.size());
	// logger.info("stop excution *********************************getRefundReqByAggId******** return**********"+refundList.size());
	return refundList;
}


public boolean rejectRefundReq(String refundId){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"rejectRefundReq()"+"|"+refundId);
	//logger.info("start excution ===================== method rejectRefundReq(refundId)"+refundId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		RefundMastBean refundMastBean=(RefundMastBean)session.get(RefundMastBean.class, refundId);
		refundMastBean.setStatus("Rejected");
		session.saveOrUpdate(refundMastBean);
		TransactionBean transactionBean=(TransactionBean)session.get(TransactionBean.class, refundMastBean.getTxnid());
		transactionBean.setStatus(3);
		session.saveOrUpdate(transactionBean);
		transaction.commit();
		flag=true;
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in rejecttCashDeposit"+ e.getMessage()+" "+e);
		//logger.debug("problem in rejecttCashDeposit==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return flag;
	
}

public boolean acceptRefundReq(String refundId,String ipIemi,String agent){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"acceptRefundReq()"+"|"+agent+"|"+refundId);
	//logger.info("start excution ===================== method rejectRefundReq(refundId)"+refundId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag=false; 
	try{
		Transaction transaction=session.beginTransaction();
		RefundMastBean refundMastBean=(RefundMastBean)session.get(RefundMastBean.class, refundId);
	String status=refund(refundMastBean.getUserId(),refundMastBean.getWalletId(),refundMastBean.getAmount(),refundId,ipIemi,walletUserDao.showUserProfile(refundMastBean.getUserId()).getMobileno(),refundMastBean.getAggreatorId(),agent);
	if(status.equalsIgnoreCase("1000")){
		
		refundMastBean.setStatus("Accepted");
		session.saveOrUpdate(refundMastBean);
		TransactionBean transactionBean=(TransactionBean)session.get(TransactionBean.class, refundMastBean.getTxnid());
		transactionBean.setStatus(4);
		session.saveOrUpdate(transactionBean);
		transaction.commit();
		flag=true;
	}
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"acceptRefundReq()"+"problem in acceptRefundReq"+ e.getMessage()+" "+e);
		//logger.debug("problem in acceptRefundReq==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return flag;
}
	
public List<RefundTransactionBean> processRefund(String ipIemi,String agent,String txnIds){
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnIds ,"processRefund()"+"|"+agent);
	//logger.info("start excution ===================== method processRefund(txnIds)"+txnIds);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	List<RefundTransactionBean> list=new ArrayList<RefundTransactionBean>();
	try{
		if(txnIds!=null&&txnIds.length()>0){
		String[] txnIdArr=txnIds.split(",");
		
		SQLQuery query;
		for(String singleTxnId:txnIdArr){
			Session session=factory.openSession();
			Transaction transaction=session.beginTransaction();
			try{
			com.bhartipay.transaction.bean.RefundTransactionBean rb=new com.bhartipay.transaction.bean.RefundTransactionBean();
			rb.setId(singleTxnId);
			rb.setStatus("Pending");
		
			session.saveOrUpdate(rb);
			transaction.commit();
			}catch(Exception e){
				session.close();
			}
		}
		
		transaction=session.beginTransaction();
		SQLQuery query1=session.createSQLQuery("call refundtransaction()");
		query1.executeUpdate();
		
		String singleString=StringUtils.join(txnIdArr,"','");
		SQLQuery query2=session.createSQLQuery("select * from refundtransaction where refundid in ('"+singleString+"')");
		query2.setResultTransformer(Transformers.aliasToBean(RefundTransactionBean.class));
		list=query2.list();
		transaction.commit();
		
		}	
	}catch (Exception e) {
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnIds ,"processRefund()"+"problem in acceptRefundReq" + e.getMessage()+" "+e);
		//logger.debug("problem in acceptRefundReq==================" + e.getMessage(), e);
	} finally {
		session.close();
	}
	return list;
}

private String bankApi(){
	
	Random r = new Random();
	int i=r.nextInt((10 - 1) + 1) + 1;
	
if(	i/2 >0){
	return "Success";
}else{
	return "Fail";
}

}




/**
 * for wallet ID by user mobile /email
 * @return walletId
 */
public WalletMastBean getAgnDtlByUserId(String mobileNo,String aggreatorid){
	String walletid="1001";

	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getAgnDtlByUserId()"+"|"+mobileNo);
	//logger.info("getAgnDtlByUserId() method started for retutn id by mobile/email****************="	+ mobileNo );
	//logger.info("getAgnDtlByUserId() method started for retutn id by aggreatorid****************="	+ aggreatorid );
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	     Transaction transaction = session.beginTransaction();
	     WalletMastBean walletMastBean=null;
	try {
		Query query = session.createQuery("from WalletMastBean where mobileno=:userid and  aggreatorid=:aggreatorid and (usertype=2 or usertype=3)");
		query.setString("userid", mobileNo);
		query.setString("aggreatorid", aggreatorid);
		List list = query.list();
		if (list != null && list.size() != 0) {
			walletMastBean=	(WalletMastBean) list.get(0);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"Wallet Id"+walletMastBean.getName());
			//logger.info("==============Wallet Id=============================="+walletMastBean.getName());
		}
	} catch (Exception e) {
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"checking id fail from getAgnDtlByUserId"+e.getMessage()+" "+e);
		logger.debug("checking id fail from getAgnDtlByUserId********************************** " + e.getMessage(), e);
	} finally {
		session.close();
	}
	return walletMastBean;
}



/**
 * for wallet ID by user mobile /email
 * @return walletId
 */
public WalletMastBean getAgnDtlByUserIdCME(String mobileNo,String aggreatorid,String distributorId){
	String walletid="1001";
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getAgnDtlByUserIdCME()"+"|"+mobileNo);
	//logger.info("getAgnDtlByUserIdCME() method started for retutn id by mobile/email****************="	+ mobileNo );
	//logger.info("getAgnDtlByUserIdCME() method started for retutn id by aggreatorid****************="	+ aggreatorid );
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	     Transaction transaction = session.beginTransaction();
	     WalletMastBean walletMastBean=null;
	try {
		Query query;
		if(distributorId==null ||distributorId.isEmpty() ||distributorId.equalsIgnoreCase("-1")||distributorId.equalsIgnoreCase("BPD0001")){
			 query = session.createQuery("from WalletMastBean where  mobileno=:userid and  aggreatorid=:aggreatorid and distributerid=:distributorId and usertype=2 and userstatus in ('A')");
			 query.setString("distributorId","BPD0001");
		}else{
			 query = session.createQuery("from WalletMastBean where mobileno=:userid and  aggreatorid=:aggreatorid and distributerid=:distributorId and usertype=2 and userstatus in ('A') ");
			 query.setString("distributorId", distributorId);
		}
		
		
		
		query.setString("userid", mobileNo);
		query.setString("aggreatorid", aggreatorid);
		List list = query.list();
		if (list != null && list.size() != 0) {
			walletMastBean=	(WalletMastBean) list.get(0);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"Wallet Id getName"+walletMastBean.getName());
			//logger.info("==============Wallet Id========getName======================"+walletMastBean.getName());
		}else {
			if(distributorId==null ||distributorId.isEmpty() ||distributorId.equalsIgnoreCase("-1")||distributorId.equalsIgnoreCase("BPD0001")){
				 query = session.createQuery("from WalletMastBean where  mobileno=:userid and  aggreatorid=:aggreatorid and usertype=3 and userstatus in ('A')");
				// query.setString("distributorId","BPD0001");
				query.setString("userid", mobileNo);
				query.setString("aggreatorid", aggreatorid);
				list = query.list();
				if (list != null && list.size() != 0) {
					walletMastBean=	(WalletMastBean) list.get(0);
				}
			}
		}
	} catch (Exception e) {
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"checking id fail from getAgnDtlByUserIdCME"+e.getMessage()+" "+e);
		//logger.debug("checking id fail from getAgnDtlByUserIdCME********************************** " + e.getMessage(), e);
	} finally {
		session.close();
	}
	return walletMastBean;
}





public WalletMastBean getDistributorDtlByUserId(String mobileNo,String aggreatorid,String distributorId){
	String walletid="1001";
	
   
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getDistributorDtlByUserId()"+"|"+mobileNo+"|distributorId: "+distributorId);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	     Transaction transaction = session.beginTransaction();
	     WalletMastBean walletMastBean=null;
	try {
		Query query;
		
			query = session.createQuery("from WalletMastBean where mobileno=:userid and  aggreatorid=:aggreatorid and (id=:distributorId or distributerid=:distributorId) and usertype=3 and userstatus in ('A') ");
			query.setString("distributorId", distributorId);
			query.setString("userid", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			
			List list = query.list();
			
		if (list != null && list.size() != 0) {
			walletMastBean=	(WalletMastBean) list.get(0);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"+wallet id getName()"+walletMastBean.getName());
			//logger.info("==============Wallet Id========getName======================"+walletMastBean.getName());
		}
	} catch (Exception e) {
		transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"checking id fail from getDistributorDtlByUserId"+e.getMessage()+" "+e);
		//logger.debug("checking id fail from getDistributorDtlByUserId********************************** " + e.getMessage(), e);
	} finally {
		session.close();
	}
	return walletMastBean;
}



/*private String getAggByUserId(String userId){
	logger.info("start excution*********************************************************** method getAggByUserId(userId)"+userId);	
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	String aggId="0";
	try{
		Query query=session.createQuery("from WalletMastBean where id=:id");
		query.setString("id", userId);
		List <WalletMastBean> list=query.list();
		aggId=list.get(0).getAggreatorid();
		if(aggId==null ||aggId.equals("-1"))
				aggId="0";
		
	}catch(Exception e){
    	logger.debug("problem in getAggByUserId***********************************************************" + e.getMessage(), e);
    	 e.printStackTrace();
   		transaction.rollback();
   		aggId="0";
   	} finally {
   	session.close();
   	}
	logger.info(" *****************************Return Status******************************"+aggId);
	return aggId;
	
}*/






/**
 * 
 * @param txnCode
 * @param userId
 * @param sendWalletid
 * @param recMobile
 * @param amount
 * @param ipImei
 * @param aggreatorid
 * @param agent
 * @return
 */
public String cmeToAgent(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent){
	
   
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"cmeToAgent()"+"|"+agent);
		//logger.info("Inside ===================== method cmeToAgent(userId,sendWalletid, recMobile, amount, ipImei)");
	String status="1001";
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction = session.beginTransaction();
	 try{
		  
		  if(amount<=0){
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Txn Amount Not Be Zero"+sendWalletid);
			//logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
    	return "7032";
		  }
		  
		String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(recMobile,aggreatorid);
		 if(sendWalletid.equalsIgnoreCase(recWalletid)){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"You can not transfer money to your own wallet.");
			// logger.info("==========================You can not transfer money to your own wallet.==================================");
     	return "7036";
		  }
	  		
		if(recWalletid.equals("1001")){
			status= "7023";
		  }else{
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Start cmeToAgent excution");
			 // logger.info("==========================Start cmeToAgent excution==================================");
			if(getWalletBalance(sendWalletid)>=amount+0){
				String sendtrxId=commanUtilDao.getTrxId("CMEAPP",aggreatorid);
	  			String recivetrxId=commanUtilDao.getTrxId("CMEAGN",aggreatorid);
	  				  		
	  			TransactionBean transactionBean=new TransactionBean();
	  		
	  			transactionBean.setTxnid(sendtrxId);
	  			transactionBean.setUserid(userId);
	  			transactionBean.setWalletid(sendWalletid);
	  			transactionBean.setAggreatorid(aggreatorid);
	  			transactionBean.setTxncode(23);
	  			transactionBean.setTxncredit(0.0);
	  			transactionBean.setTxndebit(amount);
	  			transactionBean.setSurCharge(0);
	  			transactionBean.setPayeedtl(new TransactionDaoImpl().getIdByUserId(recMobile,aggreatorid));
	  			transactionBean.setResult("Success");
	  			transactionBean.setResptxnid(recivetrxId);
	  			transactionBean.setWalletuserip(ipImei);
	  			transactionBean.setWalletuseragent(agent);
	  			session.save(transactionBean);
	  			transaction.commit();
	  			transaction=session.beginTransaction();
	  			
	  			TransactionBean transactionBeanr=new TransactionBean();
	  			transactionBeanr.setTxnid(recivetrxId);
	  		    transactionBeanr.setUserid(userId);
	  		    transactionBeanr.setWalletid(recWalletid);
	  		    transactionBeanr.setAggreatorid(aggreatorid);
	  		    transactionBeanr.setTxncode(24);
	  		    transactionBeanr.setTxncredit(amount);
	  		    transactionBeanr.setTxndebit(0.0);
	  		   // transactionBeanr.setTxndebit(amount);  //changes should be to create new txnCode
	  		    transactionBeanr.setPayeedtl(userId);
	  		    transactionBeanr.setResult("Success");
	  		    transactionBeanr.setResptxnid(sendtrxId);
	  		    transactionBeanr.setWalletuserip(ipImei);
	  		    transactionBeanr.setWalletuseragent(agent);
	  		 // transactionBeanr.setWalletuseragent("");
	  		    session.save(transactionBeanr);
	  		    transaction.commit();
	  		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"cmeToAgent transactin commit sucesfully");
	  		 // logger.info("================cmeToAgent transactin commit sucesfully================"); 
		     status= "1000";
		     deductMaintanceCharges(recMobile, aggreatorid, amount, ipImei, agent);
		/* try{
		 walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
	     walletMastBean=walletUserDao.showUserProfile(userId);
		    //String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId);
	     String smstemplet = commanUtilDao.getsmsTemplet("sendMoney",aggreatorid).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid));
	     logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
	     String mailSub= "Rs. "+Double.toString(amount)+" Send successfully to "+recMobile+".";
	     //String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
	     String mailContent= commanUtilDao.getmailTemplet("sendMoney",aggreatorid).replace("<<NAME>>", walletMastBean.getName()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", recMobile).replace("<<TXNNO>>", sendtrxId).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
	     	
	     	
	     	logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
	     	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",aggreatorid,sendWalletid,walletMastBean.getName());
			Thread t = new Thread(smsAndMailUtility);
			t.start();
		 
			 //smstemplet = commanUtilDao.getsmsTemplet("reciveMoney",aggreatorid).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<MOBILE>>", getMobileNumber(sendWalletid)).replace("<<TXNNO>>", recivetrxId);
		     smstemplet = commanUtilDao.getsmsTemplet("reciveMoney",aggreatorid).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<NAME>>", walletMastBean.getName()).replace("<<MOBILE>>", getMobileNumber(sendWalletid)).replace("<<TXNNO>>", recivetrxId);
			 logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet recived SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
		     mailSub= "Rs. "+Double.toString(amount)+" received successfully from "+getMobileNumber(sendWalletid)+".";
		     mailContent= commanUtilDao.getmailTemplet("receivedMoney",aggreatorid).replace("<<BENE NAME>>", getNameByUserId(recMobile, aggreatorid)).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<NAME>>", walletMastBean.getName()).replace("<<MOBILE>>", getMobileNumber(sendWalletid)).replace("<<TXNNO>>", recivetrxId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
		    
		     
		     
		     
		     logger.info("~~~~~~~~~~~~~~~~~~~~~Wallet to Wallet recived  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
		     smsAndMailUtility = new SmsAndMailUtility(getEmailByUserId(recMobile,aggreatorid), mailContent,mailSub, recMobile, smstemplet, "Both",aggreatorid,sendWalletid,getNameByUserId(recMobile,aggreatorid));
			 t = new Thread(smsAndMailUtility);
			 t.start();
			 
			
		
		 if(commanUtilDao.checkLoginStatus(recMobile,aggreatorid)==1){  
			  GCMDeviceBean gCMDeviceBean=(GCMDeviceBean)session.get(GCMDeviceBean.class, getIdByUserId(recMobile,aggreatorid));
  			  GCMServeice.pushNotification(gCMDeviceBean.getDeviceId(), commanUtilDao.getNotificationTemplet("walletTowallet").replace("<<mobile>>", getMobileNumber(sendWalletid)).replace("<<AMOUNT>>", Double.toString(amount)),"WTW",getMobileNumber(sendWalletid),recivetrxId,recMobile,msgId,aggreatorid);
			}
		 }catch (Exception e) {
			 logger.debug("problem in wallet to wallet  Money transfer while SMS Mail And GCM " + e.getMessage(), e);
		}*/
		 
		 
	  			  	  			
			}else{
				status= "7024";
			}
		  }
	  }catch(Exception e){
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"problem in cmeToAgent  Money transfer " + e.getMessage()+" "+e);
		 // logger.debug("problem in cmeToAgent  Money transfer " + e.getMessage(), e);
	transaction.rollback();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Inside "+ status+"" + e.getMessage()+" "+e);
	//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
	status="7000";
} finally {
	session.close();
}
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Inside cmeToAgent return"+status);
	 //logger.info("Inside ====================cmeToAgent=== return===="+status);
return status;
}

public String superDistToDist(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent){
	
   
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"superDistToDist()"+"|"+agent);
	//logger.info("Inside ===================== method superDistToDist(userId,sendWalletid, recMobile, amount, ipImei)");
String status="1001";
factory=DBUtil.getSessionFactory();
Session session = factory.openSession();
Transaction transaction = session.beginTransaction();
 try{
	  
	  if(amount<=0){
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Txn Amount Not Be Zero"+sendWalletid);
		//logger.info("==========================Txn Amount Not Be Zero=================================="+sendWalletid);
	return "7032";
	  }
	  
	String recWalletid=new TransactionDaoImpl().getWalletIdByUserId(recMobile,aggreatorid);
	 if(sendWalletid.equalsIgnoreCase(recWalletid)){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"You can not transfer money to your own wallet.");
		//logger.info("==========================You can not transfer money to your own wallet.==================================");
 	return "7036";
	  }
  		
	if(recWalletid.equals("1001")){
		status= "7023";
	  }else{
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"");
		//logger.info("==========================Start superDistToDist excution==================================");
		if(getWalletBalance(sendWalletid)>=amount+0){
			String sendtrxId=commanUtilDao.getTrxId("CMEAPP",aggreatorid);
  			String recivetrxId=commanUtilDao.getTrxId("CMEAGN",aggreatorid);
  				  		
  			TransactionBean transactionBean=new TransactionBean();
  			TransactionBean transactionBeanr=new TransactionBean();
  		
  				
  			transactionBean.setTxnid(sendtrxId);
  			transactionBean.setUserid(userId);
  			transactionBean.setWalletid(sendWalletid);
  			transactionBean.setAggreatorid(aggreatorid);
  			transactionBean.setTxncode(23);
  			transactionBean.setTxncredit(0.0);
  			transactionBean.setTxndebit(amount);
  			transactionBean.setSurCharge(0);
  			transactionBean.setPayeedtl(new TransactionDaoImpl().getIdByUserId(recMobile,aggreatorid));
  			transactionBean.setResult("Success");
  			transactionBean.setResptxnid(recivetrxId);
  			transactionBean.setWalletuserip(ipImei);
  			transactionBean.setWalletuseragent(agent);
  		
  			
  			
  			transactionBeanr.setTxnid(recivetrxId);
  		    transactionBeanr.setUserid(userId);
  		    transactionBeanr.setWalletid(recWalletid);
  		    transactionBeanr.setAggreatorid(aggreatorid);
  		    transactionBeanr.setTxncode(24);
  		    transactionBeanr.setTxncredit(amount);
  		    transactionBeanr.setTxndebit(0.0);
  		    transactionBeanr.setPayeedtl(userId);
  		    transactionBeanr.setResult("Success");
  		    transactionBeanr.setResptxnid(sendtrxId);
  		    transactionBeanr.setWalletuserip(ipImei);
  		    transactionBeanr.setWalletuseragent(agent);
  		// transactionBeanr.setWalletuseragent("");
  		   session.save(transactionBean);
  		   session.save(transactionBeanr);
  		 transaction.commit();
  		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"superDistToDist transactin commit sucesfully");
	 //logger.info("================superDistToDist transactin commit sucesfully================"); 
	 status= "1000";
		}else{
			status= "7024";
		}
	  }
  }catch(Exception e){
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"problem in superDistToDist  Money transfer " + e.getMessage()+" "+e);
//logger.debug("problem in superDistToDist  Money transfer " + e.getMessage(), e);
transaction.rollback();
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Inside "+ status+"" + e.getMessage()+" "+e);
//logger.debug("Inside ======="+ status+"========" + e.getMessage(), e);
status="7000";
} finally {
session.close();
}
 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"",userId, "","" ,"Inside superDistToDist return"+status);
//logger.info("Inside ====================superDistToDist=== return===="+status);
return status;
}









public List<CashDepositMast> getCashDepositReportByAggId(String aggreatorId,String stDate,String endDate,String type){
	
   
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"getCashDepositReportByAggId()");
	//logger.info("start excution ===================== method getCashDepositReportByAggId(userId)"+aggreatorId);
	List<CashDepositMast> cashDepositList=new ArrayList<CashDepositMast>();
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		 
		 StringBuilder rRevenuequery;//=new StringBuilder("from CashDepositMast where aggreatorId=:aggreatorId");
		 if(type!=null&&type.equalsIgnoreCase("0")){
			 rRevenuequery=new StringBuilder("from CashDepositMast cm where (cm.aggreatorId=:aggreatorId or cm.userId in(select wm.id from WalletMastBean wm where whiteLabel=:wl and usertype=:ut))"); 
			 
		 }
		 else{
			 rRevenuequery=new StringBuilder("from CashDepositMast cm where (cm.aggreatorId=:aggreatorId OR cm.userId=:aggreatorId)");
		 }
		 if(stDate==null ||stDate.isEmpty() ||endDate==null ||endDate.isEmpty()){
			 String rRevenuequery1 = rRevenuequery.toString();
			 
			 rRevenuequery1 = rRevenuequery1 +" and  cm.depositId like 'CASH%'  ORDER BY depositId DESC ";
			 
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"start excution 12 query"+rRevenuequery);
			Query query=session.createQuery(rRevenuequery1);
			query.setString("aggreatorId", aggreatorId);
			if(type!=null&&type.equalsIgnoreCase("0")){
			query.setInteger("wl",1);
			query.setInteger("ut",4);
			}
				query.setMaxResults(20);
				cashDepositList=query.list();
				
				String rRevenuequery2 = rRevenuequery.toString();
				 
				 rRevenuequery2 = rRevenuequery2 +" and  cm.depositId like 'CMER%'  ORDER BY depositId DESC ";
				 
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"start excution 12 query"+rRevenuequery);
				Query query1=session.createQuery(rRevenuequery2);
				query1.setString("aggreatorId", aggreatorId);
				if(type!=null&&type.equalsIgnoreCase("0")){
				query1.setInteger("wl",1);
				query1.setInteger("ut",4);
				}
					query1.setMaxResults(20);
					cashDepositList.addAll(query1.list());
		 }else{
			 rRevenuequery.append(" and DATE(requestdate) BETWEEN STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')");
			 
			 rRevenuequery.append("  ORDER BY depositId DESC ");
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"start excution 12 query"+rRevenuequery);
			 //logger.info("start excution ============12========= query***"+rRevenuequery);
			Query query=session.createQuery(rRevenuequery.toString());
			query.setString("aggreatorId", aggreatorId);
			if(type!=null&&type.equalsIgnoreCase("0")){
			query.setInteger("wl",1);
			query.setInteger("ut",4);
			}
				cashDepositList=query.list();
		 }
		
		
		}catch(Exception e){
		e.printStackTrace();
		  transaction.rollback();
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"problem in getCashDepositReportByAggId" + e.getMessage()+" "+e);
		 // logger.debug("problem in getCashDepositReportByAggId***********************************************************" + e.getMessage(), e);
		 }
		 finally{
		  session.close();
	 }
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"stop excution getCashDepositReportByAggId return"+cashDepositList.size());
	//logger.info("stop excution *********************************getCashDepositReportByAggId******** return**********"+cashDepositList.size());
	return cashDepositList;
}







public List showCashBackPassbookById(String id,String stDate,String endDate){
	
   
    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"showCashBackPassbookById()"+"|"+id+"stDate"+stDate);
   // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"stDate"+stDate);
   // logger.info("start excution ===================== method showCashBackPassbookById(walletid)"+id);
    //logger.info("start excution ===================== method stDate"+stDate);
 List passbook=new ArrayList();
 factory=DBUtil.getSessionFactory();
 Session session = factory.openSession();
       Transaction transaction = session.beginTransaction();
 try {
  
	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	 SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
	 StringBuilder serchQuery=new StringBuilder();
	 if(!(id==null ||id.isEmpty()))
	 {
		 if(id.contains("All") )
		 {
			 StringTokenizer str = new StringTokenizer(id, "|");
			 str.nextElement();
			 if(Double.parseDouble(str.nextElement().toString()) == 4)
			 {
				 serchQuery.append(" select * from  cashbackpassbook where walletid in (:walletid)");
				 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
					 serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
					 serchQuery.append(" order by txndate DESC");
				 }else{
					 serchQuery.append("  ORDER BY txndate DESC LIMIT 20");
					
				 }
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"showCashBackPassbook"+serchQuery);
				 //logger.info("showCashBackPassbook***********************************************~~"+ serchQuery);
				 Query qry = session.createSQLQuery("Select walletId from walletmast where aggreatorid='"+str.nextElement()+"';" );
			     List<String> walletId =qry.list();
				 SQLQuery  query=session.createSQLQuery(serchQuery.toString());
				 query.setParameterList("walletid", walletId);
				 query.setResultTransformer(Transformers.aliasToBean(CashBackPassbookBean.class));
				 passbook=query.list();
				 
				 Collections.reverse(passbook);
			 }
		 }
		 else
		 {
			 serchQuery.append(" select * from  cashbackpassbook where walletid=:walletid");
			 if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
				 serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
				 serchQuery.append(" order by txndate DESC");
			 }else{
				 serchQuery.append("  ORDER BY txndate DESC LIMIT 20");
				
			 }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"showCashBackPassbook"+serchQuery);
			 //logger.info("showCashBackPassbook***********************************************~~"+ serchQuery);
			 Query qry = session.createSQLQuery("Select walletId from walletmast where id='"+id+"';" );
		     String walletId =(String)(qry.uniqueResult());
			 SQLQuery  query=session.createSQLQuery(serchQuery.toString());
			 query.setString("walletid", walletId);
			 query.setResultTransformer(Transformers.aliasToBean(CashBackPassbookBean.class));
			 passbook=query.list();
			 
			 Collections.reverse(passbook);
		 }
	 }
 }catch(Exception e){
  e.printStackTrace();
    transaction.rollback();
    System.out.println(e);
   }
   finally{
    session.close();
  }
 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"stop excution showCashBackPassbookById return"+passbook.size());
 // logger.info("stop excution ********************************showCashBackPassbookById******* return**********"+passbook.size());  
 return passbook;
   }










public List showPassbookById(String id,String stDate,String endDate,String aggreatorid){
	
   
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"showPassbookById()"+"|"+id+"stDate"+stDate);
	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"stDate"+stDate);
    //logger.info("start excution ===================== method showPassboo(walletid)"+id);
    //logger.info("start excution ===================== method stDate"+stDate);
 List passbook=new ArrayList();
 factory=DBUtil.getSessionFactory();
 Session session = factory.openSession();
       Transaction transaction = session.beginTransaction();
 try {
   SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
   SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
   StringBuilder serchQuery=new StringBuilder();
   serchQuery.append("from PassbookBean where walletid in (:walletid)");
   if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
    serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
   }else{
    serchQuery.append(" and DATE(txndate)=DATE(now())");
   }
   Query qry=null;
   if(id!=null&&!id.isEmpty()&&!id.equalsIgnoreCase("-1")){
	    qry = session.createQuery("Select walletid from WalletMastBean where id=:id");
	    qry.setString("id",id);
   }else{
	    qry = session.createQuery("Select walletid from WalletMastBean where aggreatorid=:aggreatorid and usertype in (:usertype)" );   
	    qry.setString("aggreatorid", aggreatorid);
	    qry.setParameterList("usertype",new Integer[]{2,3,6});
   }
   
   	List<String> list=qry.list();

   //serchQuery.append(" order by txndate DESC");
   	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"showPassbook"+serchQuery);
   	//logger.info("showPassbook***********************************************~~"+ serchQuery);
   Query query=session.createQuery(serchQuery.toString());
   query.setParameterList("walletid",list);
   passbook=query.list();
 //  Collections.reverse(passbook);
 }catch(Exception e){
  e.printStackTrace();
    transaction.rollback();
    System.out.println(e);
   }
   finally{
    session.close();
  }
 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"stop excution getTrxdtl return"+passbook.size()); 
// logger.info("stop excution ********************************getTrxdtl******* return**********"+passbook.size());  
 return passbook;
   }




public String creditMoney(String txnId,String userId,String walletId,double amount,String payeedtl,String resptxnid, String ipImei,String aggreatorid,String Agent,int txnCode){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,"", "",txnId ,"creditMoney()");
	String status="1001";
	TransactionBean transactionBean=new TransactionBean();
	factory=DBUtil.getSessionFactory();
	 	Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	  try{
		  	transactionBean.setTxnid("D"+txnId);
			transactionBean.setUserid(userId);
			transactionBean.setWalletid(walletId);
			transactionBean.setTxncode(txnCode);
			transactionBean.setTxncredit(amount);
			transactionBean.setTxndebit(0);
			transactionBean.setPayeedtl(payeedtl);
			transactionBean.setResult("Success");
			transactionBean.setResptxnid(resptxnid);
			transactionBean.setAggreatorid(aggreatorid);
			transactionBean.setWalletuserip(ipImei);
			transactionBean.setWalletuseragent(Agent);
			session.save(transactionBean);
	     	transaction.commit();
	     	status="1000";  
	  }catch(Exception e){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,"", "",txnId ,"problem in debitMoney to wallet" + e.getMessage()+" "+e);
	transaction.rollback();
	return "7000";
	  } finally {
	session.close();
}
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,"", "",txnId ,"Inside debitMoney return"+status);
//logger.info("Inside ====================debitMoney=== return===="+status);
return status;
	
}

	
	
	public PassbookBean showTxnByTxnID(String txnId) {
		
	     
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"showTxnByTxnID()");
		//logger.info("Start execution *************method showTxnByTxnID(TxnId)****" + txnId);
		PassbookBean passbook = new PassbookBean();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			Query query = session.createQuery("From PassbookBean where txnid = :txnId").setString("txnId", txnId);
			passbook = (PassbookBean) (query.uniqueResult());
		} catch (Exception ex) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"Problem in method showTxnByTxnID(TxnId)"	+ ex.getMessage()+" "+ex);
			logger.info("Problem in *************method showTxnByTxnID(TxnId)****" + txnId + "*****e.message***"
					+ ex.getMessage());
			ex.printStackTrace();
			return passbook;
		} finally {
			session.close();
		}
		return passbook;
	}
	 
	 
	 
	 public TransactionBean showTxnByRespTxnID(String resptxnId,int txnCode){

			   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",resptxnId ,"showTxnByRespTxnID");
			//logger.info("Start execution *************method showTxnByTxnID(TxnId)****" + txnId);
			   TransactionBean txnBean=null;
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			try {
				Query query = session.createQuery("From TransactionBean where resptxnid = :resptxnid and txncode=:txncode").setString("resptxnid", resptxnId).setInteger("txncode", txnCode);
				txnBean = (TransactionBean) (query.uniqueResult());
			} catch (Exception ex) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",resptxnId ,"Problem in method showTxnByRespTxnID"	+ ex.getMessage()+" "+ex);
				logger.info("Problem in *************method showTxnByRespTxnID***" + resptxnId + "*****e.message***"
						+ ex.getMessage());
				ex.printStackTrace();
				return txnBean;
			} finally {
				session.close();
			}
			return txnBean;
		
	 }
	 
	 
	 
	 
	 
	 
	 public String flightBook(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent){
			return new TransactionDaoImpl().paymentFromWallet(userId,payeeWalletid,22,amount,rechageNumber,rechargeTxnId,ipImei,aggreatorid,agent,0);
	}

	public String payForFlight(FlightTxnBean flightTxnBean){
		
	     
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),flightTxnBean.getAggreatorId(),flightTxnBean.getWalletId(),flightTxnBean.getUserId(), "","" ,"payForFlight()");
		//logger.info("start excution ==============****************************************************************======= method payForPayment(rechargeTxnBean)");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		String walletResp="FAILED";
		Transaction transaction = session.beginTransaction();
		String status="1001";
		try{
			String bookingId=flightTxnBean.getBookingId();
			//flightTxnBean.setBookingId(bookingId);
			//flightTxnBean.setStatus("Cancel");
			//session.save(flightTxnBean);
			transaction.commit();
		    walletResp= flightBook(flightTxnBean.getUserId(),flightTxnBean.getWalletId(),flightTxnBean.getFareAmount(),"",bookingId,flightTxnBean.getIpIemi(),flightTxnBean.getAggreatorId(),"");
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),flightTxnBean.getAggreatorId(),flightTxnBean.getWalletId(),flightTxnBean.getUserId(), "","" ,"walletResp"+walletResp);
		   // logger.info("walletResp ============= method payForPayment(walletToBankTxnMast)"+walletResp);
			
			
			/*	if(walletResp.equals("1000")){
				transaction=session.beginTransaction();
				flightTxnBean=(FlightTxnBean)session.get(FlightTxnBean.class, bookingId);
				flightTxnBean.setStatus("Pending");
				flightTxnBean.setWallettxnStatus(walletResp);
				session.update(flightTxnBean);
				transaction.commit();
				status=walletResp;
				flightTxnBean.setErrorStatus(status);
			
			}else{
				transaction=session.beginTransaction();
				flightTxnBean=(FlightTxnBean)session.get(FlightTxnBean.class, bookingId);
				flightTxnBean.setStatus("Failed");
				flightTxnBean.setWallettxnStatus(walletResp);
				session.update(flightTxnBean);
				transaction.commit();
				status=walletResp;
				flightTxnBean.setErrorStatus(status);
				}
				*/
				}catch(Exception e){
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),flightTxnBean.getAggreatorId(),flightTxnBean.getWalletId(),flightTxnBean.getUserId(), "","" ,"problem in payForFlight" + e.getMessage()+" "+e);
			    	//logger.debug("problem in payForFlight=========================" + e.getMessage(), e);
			    	 e.printStackTrace();
			   		transaction.rollback();
			   		status= "7000";
			   	} finally {
			   				session.close();
			   	}	
			
		//flightTxnBean.setStatusCode(status); 
		return walletResp;
		
	}

	public String payForFlightUpdate(FlightTxnBean flightTxnBean,Response_ bookFlightResp, int successCount ) {
		
	     
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),flightTxnBean.getAggreatorId(),flightTxnBean.getWalletId(),flightTxnBean.getUserId(), "","" ,"payForFlightUpdate()");
		
		//logger.info("start excution ===================== method payForFlightUpdate(flightTxnBean)");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		String succFlag="FAILED";
		//FlightTxnBean flightTxnBeannew=new FlightTxnBean();
		try{
		
			//flightTxnBeannew=(FlightTxnBean)session.get(FlightTxnBean.class, flightTxnBean.getBookingId());
					
			if(bookFlightResp != null && ((bookFlightResp.getStatus() != null && (bookFlightResp.getStatus() == 1 || bookFlightResp.getStatus() == 5)) || (bookFlightResp.getFlightItinerary() !=null && bookFlightResp.getFlightItinerary().getStatus() != null && (bookFlightResp.getFlightItinerary().getStatus() == 1 || bookFlightResp.getFlightItinerary().getStatus() == 5)))){
				//flightTxnBeannew.setStatus("SUCCESS");
				/*if(bookFlightResp.getFlightItinerary() == null || bookFlightResp.getFlightItinerary().getPNR() == null )
				{}
				else
					{flightTxnBeannew.setPnrNumber(bookFlightResp.getFlightItinerary().getPNR());}*/
				
				
				
				//flightTxnBeannew.setBookTransactionId(bookFlightResp.getBookOutput().getFlightTicketDetails().get(0).getTransactionId());
				//flightTxnBeannew.setCustomerContactNumber(bookFlightResp.getBookOutput().getFlightTicketDetails().get(0).getCustomerDetails().getContactNumber());
					
					}else if(successCount == 1){
						//flightTxnBean.setStatus("PARSUCCESS");
						succFlag =flightSettlement(flightTxnBean.getUserId(),flightTxnBean.getWalletId(),flightTxnBean.getFareAmount(),flightTxnBean.getBookingId(),flightTxnBean.getIpIemi(),"",flightTxnBean.getAggreatorId(),"");
						//flightTxnBean.setApiResponse(new Gson().toJson(bookFlightResp));
						//session.save(flightTxnBean);
						//transaction.commit();
					}
					else if(successCount == 0)
					{
						//flightTxnBeannew.setStatus("FAILED");
						succFlag =flightSettlement(flightTxnBean.getUserId(),flightTxnBean.getWalletId(),flightTxnBean.getFareAmount(),flightTxnBean.getBookingId(),flightTxnBean.getIpIemi(),"",flightTxnBean.getAggreatorId(),"");
					}
			if(succFlag.equals("1000"))
			{
				return "SUCCESS";
			}
				return "FAILED";
					//flightTxnBeannew.setApiResponse(new Gson().toJson(bookFlightResp));
					/*session.save(flightTxnBeannew);
					transaction.commit();
					return flightTxnBeannew.getStatus();*/
			
			
		 }catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),flightTxnBean.getAggreatorId(),flightTxnBean.getWalletId(),flightTxnBean.getUserId(), "","" ,"problem in dmtTransferUpdate" + e.getMessage()+" "+e);
		    	//logger.debug("problem in dmtTransferUpdate=========================" + e.getMessage(), e);
		    	 e.printStackTrace();
		   		transaction.rollback();
		   		return "FAILED";
		   	} finally {
		   				session.close();
		   	}
		
	}

		
	private String flightSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent){
		  return walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,40);
		 }

	
	
	public PaymentGatewayTxnBean viewPGTrxFlight(String txnId){
		
	     
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"viewPGTrxFlight()");
		//logger.info("start excution ===================== method viewPGTrxFlight(String txnId)");
		   factory=DBUtil.getSessionFactory();
		   Session session = factory.openSession();
		      PaymentGatewayTxnBean paymentGatewayTrxBean=null;
		      try{
		       paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		       Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,""); 
		       System.out.println("~~~~~~~~~~~"+paymentGatewayTrxBean.getTrxAmount());
		      
		      }catch(Exception e){
		    	  
		    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"problem in updatePGTrx" + e.getMessage()+" "+e);
		    	  //logger.debug("problem in updatePGTrx=======================" + e.getMessage(), e);
		         e.printStackTrace();
		      } finally {
		         session.close();
		      }
		      return paymentGatewayTrxBean;
		      }
	
	public String savePGTrxFlight(String tripId, double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId,String aggreatorid){
		
	     
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ,"savePGTrxFlight()");  
		//logger.info("start excution ===================== method savePGTrx()"+walletId);
		   String status="1001";
		   factory=DBUtil.getSessionFactory();
		   Session session = factory.openSession();
		      Transaction transaction = session.beginTransaction();
		      try{
		       PaymentGatewayTxnBean paymentGatewayTrxBean=new PaymentGatewayTxnBean();
		       String pgtxnid=commanUtilDao.getTrxId("PGTXN",aggreatorid);
		       Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ," excution  pgtxnid"+pgtxnid);
		      // logger.info(" excution ===================== pgtxnid"+pgtxnid);
		       paymentGatewayTrxBean.setTrxId(pgtxnid);
		       paymentGatewayTrxBean.setTrxAmount(trxAmount);
		       paymentGatewayTrxBean.setTrxResult("Cancel");
		       paymentGatewayTrxBean.setWalletId(walletId);
		       paymentGatewayTrxBean.setMobileNo(mobileNo);
		       paymentGatewayTrxBean.setTrxReasion(trxReason);
		       paymentGatewayTrxBean.setEmail(emailId);
		       paymentGatewayTrxBean.setAppName(appName);
		       paymentGatewayTrxBean.setImeiIP(imeiIP);
		       paymentGatewayTrxBean.setUserId(userId);
		       paymentGatewayTrxBean.setPayTxnid(tripId);
		       paymentGatewayTrxBean.setAggreatorid(aggreatorid);
		          session.save(paymentGatewayTrxBean);
		       transaction.commit();
		       status="1000|"+pgtxnid;
		        
		      }catch(Exception e){
		    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ,"problem in Add  PG Txn" + e.getMessage()+" "+e);
		      // logger.debug("problem in Add  PG Txn=========================" + e.getMessage(), e);
		       status="7000";
		         e.printStackTrace();
		         transaction.rollback();
		      } finally {
		         session.close();
		      }
		      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ," excution status"+status);
		     // logger.info(" excution ===================== status"+status);
		      return status;
		  }

		  public String updatePGTrxFlight(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status){
	 
    
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"updatePGTrxFlight()");
	  
	//logger.info("start excution ===================== method updatePGTrx(userId,txnId,pgtxn,trxAmount,status,ipImei)");
		   factory=DBUtil.getSessionFactory();
		   Session session = factory.openSession();
		   String pgTxnId=null;
		      Transaction transaction = session.beginTransaction();
		      String txnStatus="1001|FAIL|"+txnId;
		      PaymentGatewayTxnBean paymentGatewayTrxBean=null;
		      try{
		       paymentGatewayTrxBean=(PaymentGatewayTxnBean)session.get(PaymentGatewayTxnBean.class, txnId);
		       WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration(paymentGatewayTrxBean.getAggreatorid());
		    	responseparam=responseparam+"|"+walletConfiguration.getPgEncKey();
		    	
		    	String newHash=CommanUtil.SHAHashing256(responseparam);
		    	if(newHash.equals(hash)){
		    		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,methodname);
		       System.out.println("~~~~~~~~~~~"+paymentGatewayTrxBean.getTrxAmount());
		        if(trxAmount==paymentGatewayTrxBean.getTrxAmount()){
		        paymentGatewayTrxBean.setPgId(pgtxn);
		        if (status.contains("success") ||status.contains("successfully")){
		      paymentGatewayTrxBean.setTrxResult("Success");
		      List<AirFareBean> airFareList  = new AirTravelDaoImpl().getFareQuoteByTripId( paymentGatewayTrxBean.getPayTxnid(), "");
		      if(airFareList.get(0).getPgTxn() != null && !airFareList.get(0).getPgTxn().trim().equals(""))
		      {
		      	pgTxnId = airFareList.get(0).getPgTxn()+","+txnId;
		      }
		      else
		      {
		    	  pgTxnId = txnId;
		      }
		      int flag1=new AirTravelDaoImpl().updateFareQuote(-1,(airFareList.get(0).getPgAmt()+trxAmount),-1,-1,-1, pgTxnId,"","","","", paymentGatewayTrxBean.getPayTxnid());
		      
		      
		      txnStatus="1000|SUCCESS|"+txnId;
		    
		        }else{
		      paymentGatewayTrxBean.setTrxResult("Fail");
		      txnStatus="7029|FAIL|"+txnId;
		     }
		        }else{
		        	 paymentGatewayTrxBean.setTrxResult("Fail");
				      txnStatus="7029|FAIL|"+txnId;
		        }
		    session.saveOrUpdate(paymentGatewayTrxBean);
		    transaction.commit();
		    }
		      }catch(Exception e){
		    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",txnId ,"problem in updatePGTrx" + e.getMessage()+" "+e);
		       //logger.debug("problem in updatePGTrx=======================" + e.getMessage(), e);
		         e.printStackTrace();
		         transaction.rollback();
		      } finally {
		         session.close();
		      }
		      ////////////////////////remove///////////////////////////////////
		      //txnStatus="1000|SUCCESS|"+txnId;
		      return txnStatus+"|"+paymentGatewayTrxBean.getTrxAmount()+"|"+paymentGatewayTrxBean.getPgId();
		      }
		  
  
		  public List<CashDepositMast> getCDAReqByAggId(String aggreatorId,String type){
			  
		     
			   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"getCDAReqByAggId()");
		
				//logger.info("start excution ===================== method getCDAReqByAggId(userId)"+aggreatorId);
				List<CashDepositMast> cashDepositList=new ArrayList<CashDepositMast>();
				factory=DBUtil.getSessionFactory();
				Session session = factory.openSession();
				try{
					Query query;//=session.createQuery("from CashDepositMast where aggreatorId=:aggreatorId and status='Accepted' and checkerstatus='Accepted' and approverstatus='Pending' order by depositId DESC");
					if(type!=null&&type.equals("0")){
						query=session.createQuery("from CashDepositMast cm where (cm.aggreatorId=:aggreatorId or cm.userId in(select wm.cashDepositeWallet from WalletMastBean wm where whiteLabel=:wl and usertype=:ut)) and status='Accepted' and checkerstatus='Accepted' and approverstatus='Pending' order by depositId DESC");
						query.setInteger("wl",1);
						query.setInteger("ut",4);
					}else{
						query=session.createQuery("from CashDepositMast cm where cm.aggreatorId=:aggreatorId and cm.userId not in(select wm.cashDepositeWallet from WalletMastBean wm where id=:aggreatorId) and status='Accepted' and checkerstatus='Accepted' and approverstatus='Pending' order by depositId DESC");
					}
					query.setString("aggreatorId", aggreatorId);
					
					
					cashDepositList=query.list();
					}catch(Exception e){
					e.printStackTrace();
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"problem in getCDAReqByAggId" + e.getMessage()+" "+e);
					 // logger.debug("problem in getCDAReqByAggId***********************************************************" + e.getMessage(), e);
					 }
					 finally{
					  session.close();
				 }
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "","" ,"stop excution getCDAReqByAggId return"+cashDepositList.size());
				// logger.info("stop excution *********************************getCDAReqByAggId******** return**********"+cashDepositList.size());
				return cashDepositList;
			}		  
		  
		  
		  public boolean approvedCashDeposit(String depositId,String remarkApprover,String ipIemi,String agent){
			  
		     
			   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"approvedCashDeposit()"+"|"+depositId);
		
				//logger.info("start excution ===================== method approvedCashDeposit(depositId)"+depositId);
				factory = DBUtil.getSessionFactory();
				session = factory.openSession();
				boolean flag=false; 
				try{
					Transaction transaction=session.beginTransaction();
					CashDepositMast cashDepositMast=(CashDepositMast)session.get(CashDepositMast.class, depositId);
					
					String status=cashDepositSettlement(cashDepositMast.getUserId(),cashDepositMast.getWalletId(),cashDepositMast.getAmount(),cashDepositMast.getDepositId(),ipIemi,walletUserDao.showUserProfile(cashDepositMast.getUserId()).getMobileno(),cashDepositMast.getAggreatorId(),agent);
					if(status.equals("1000")){
						
						deductMaintanceCharges(cashDepositMast.getUserId(), cashDepositMast.getAggreatorId(), cashDepositMast.getAmount(), ipIemi, agent);
						cashDepositMast.setStatus("Approved");
						cashDepositMast.setApproverStatus("Accepted");
						cashDepositMast.setRemarkApprover(remarkApprover);
						session.saveOrUpdate(cashDepositMast);
						transaction.commit();
						flag=true;
						
						
						// DDear CME, Your limit Replenishment request with <<TXNID>> of Rs.
						// <<AMOUNT>> has been accepted by Bhartipay.
						
						WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class,cashDepositMast.getUserId());
						
						try{ 
						
						String smstemplet = commanUtilDao.getsmsTemplet("CMELIMITREPL",cashDepositMast.getAggreatorId());
						logger.info("~~~~~~~~~acceptCMERequest~~~~~~~~~~~~CMERequest smstemplet~~~~~~~~~~~~~~~~~~~````" + smstemplet); SmsAndMailUtility smsAndMailUtility = new
						SmsAndMailUtility("", "","", walletMastBean.getMobileno(),
						smstemplet.replaceAll("<<AMOUNT>>",
						  cashDepositMast.getAmount().toString()).replaceAll("<<NAME>>",walletMastBean.getName()),
						  "SMS",cashDepositMast.getAggreatorId(),walletMastBean.getWalletid(),
						  walletMastBean.getName(),"NOTP"); 
//						Thread t = new
//						  Thread(smsAndMailUtility); t.start(); 
						  ThreadUtil.getThreadPool().execute(smsAndMailUtility);
						  } 
						catch (Exception e) {
						e.printStackTrace(); 
						logger.debug("problem in acceptCMERequest===message send===============" + e.getMessage(), e);
						  
						  }
						
						                          
						
					}
					
				}catch (Exception e) {
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in approvedCashDeposit" + e.getMessage()+" "+e);
					//logger.debug("problem in approvedCashDeposit==================" + e.getMessage(), e);
				} finally {
					session.close();
				}
				return flag;
				
			}




			public boolean rejectApproverCashDeposit(String depositId,String remarkApprover){
				 
			     
				   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"rejectApproverCashDeposit()"+" "+depositId);
			
				//logger.info("start excution ===================== method rejectApproverCashDeposit(depositId)"+depositId);
				factory = DBUtil.getSessionFactory();
				session = factory.openSession();
				boolean flag=false; 
				try{
					Transaction transaction=session.beginTransaction();
					CashDepositMast cashDepositMast=(CashDepositMast)session.get(CashDepositMast.class, depositId);
					cashDepositMast.setStatus("Rejected");
					cashDepositMast.setApproverStatus("Rejected");
					cashDepositMast.setRemarkApprover(remarkApprover);
					session.saveOrUpdate(cashDepositMast);
					transaction.commit();
					flag=true;
				}catch (Exception e) {
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in rejectApproverCashDeposit" + e.getMessage()+" "+e);
					logger.debug("problem in rejectApproverCashDeposit==================" + e.getMessage(), e);
				} finally {
					session.close();
				}
				return flag;
				
			} 
		  
			 public RemainingLimitBean getRemainingLimit(String walletId){
				 
			     
				   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "","" ,"getRemainingLimit()");
			
			     //logger.info("start excution ===================== method getRemainingLimit(walletId)"+walletId);
			     String userId="";
			     double walletBal=0.0;
			     double addMoney=0.0;
			     double assigneLimit=0.0;
			     double remainingLimit=0.0;
			     factory=DBUtil.getSessionFactory();
			     Session session = factory.openSession();
			     RemainingLimitBean remainingLimitBean=new RemainingLimitBean();
			     DecimalFormat df = new DecimalFormat("####0.00");
			      try{
			       Query query=session.createQuery("from WalletMastBean where walletid= :walletId");
			      query.setString("walletId",walletId);
			     
			      List<WalletMastBean> list=query.list();
			      if(list!=null && list.size()>0){
			    	  userId=list.get(0).getId();
			      }
			      
			      walletBal=(double)session.createSQLQuery("SELECT IFNULL(SUM(finalbalance),0)  FROM walletbalance_audit  WHERE  id= :id AND date(auditdate)=(LAST_DAY(NOW() - INTERVAL 1 MONTH)+ INTERVAL 1 DAY)").setParameter("id", userId).uniqueResult();
			      addMoney=(double)session.createSQLQuery("SELECT IFNULL(SUM(txncredit),0)  FROM wallettransaction  WHERE  walletid= :walletId AND txncode IN (1,3,9) AND DATE(txnDate) BETWEEN (LAST_DAY(NOW() - INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND DATE(NOW())").setParameter("walletId", walletId).uniqueResult();
			     
			      query=session.createQuery("FROM UserWalletConfigBean WHERE  walletid=:walletId");
			     query.setString("walletId", walletId);
			     List <UserWalletConfigBean> walletDtlList=query.list();
			     if(list!=null && list.size()>0){
			      assigneLimit=walletDtlList.get(0).getMinbal();
			     }
			    
			     remainingLimit=Double.valueOf(df.format(assigneLimit-(walletBal+addMoney)));
			     remainingLimitBean.setAssigneLimit(assigneLimit);
			     remainingLimitBean.setRemainingLimit(remainingLimit);
			     
			          
			     }catch (Exception e) {
			           
			            e.printStackTrace();
			            Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"problem in getRemainingLimit" + e.getMessage()+" "+e);
			           // logger.debug("problem in getRemainingLimit=====" + e.getMessage(), e);
			            remainingLimitBean.setAssigneLimit(assigneLimit);
			      remainingLimitBean.setRemainingLimit(remainingLimit);
			         }finally {
			          session.close(); 
			         }
			      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"Inside getRemainingLimit return"+walletBal);
			     //logger.info("Inside ====================getRemainingLimit=== return===="+walletBal);
			    
			   return remainingLimitBean;   
			    }	  
		  
		
			 
			 public List showCashDeposite(String id,String stDate,String endDate){
				 
			     
				   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"showCashDeposite()"+"|"+id+"stDate"+stDate);
				  // Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"stDate"+stDate);
			
				// logger.info("start excution ===================== method showCashDeposite(walletid)"+id);
			     //logger.info("start excution ===================== method stDate"+stDate);
			  List passbook=new ArrayList();
			  factory=DBUtil.getSessionFactory();
			  Session session = factory.openSession();
			        Transaction transaction = session.beginTransaction();
			  try {
			    SQLQuery qry = session.createSQLQuery("Select cashdepositewallet from walletmast where id=(SELECT aggreatorid FROM walletmast WHERE walletid='"+id+"')");
			    String cashDepositeWallet=(String)qry.uniqueResult();
			    if(cashDepositeWallet!=null) {
			   WalletMastBean cashDepBean = (WalletMastBean)session.get(WalletMastBean.class, cashDepositeWallet);
			   SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			    SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			    StringBuilder serchQuery=new StringBuilder();
			    serchQuery.append("  from PassbookBean where walletid = :walletid");
			    if(stDate!=null&&!stDate.isEmpty() &&endDate!=null&&!endDate.isEmpty()){
			     serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(stDate))+"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); 
			    }else{
			     serchQuery.append(" and DATE(txndate)=DATE(NOW())");
			     //serchQuery.append(" LIMIT 200"); 
			    }
			    
			   //serchQuery.append(" order by txndate DESC");
			    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"showCashDeposite"+serchQuery);
			   // logger.info("showCashDeposite***********************************************~~"+ serchQuery);
			    Query query=session.createQuery(serchQuery.toString());
			    query.setParameter("walletid", cashDepBean.getWalletid());
			    passbook=query.list();
			    //Collections.reverse(passbook); 
			  }}catch(Exception e){
			   e.printStackTrace();
			     transaction.rollback();
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"checking showCashDeposite error" + e.getMessage()+" "+e);
			    // logger.debug("checking showCashDeposite*****************error************** " + e.getMessage(), e);
			    }
			    finally{
			     session.close();
			   }
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","" ,"stop excution showCashDeposite return"+passbook.size());
			   //logger.info("stop excution ********************************showCashDeposite******* return**********"+passbook.size());  
			  return passbook;
			    }
		  		 


	public RechargeTxnBean getRechargeStatus(String aggregatorId,String rechargeRespId){
		
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "", "","getRechargeStatus|rechargeRespId:"+rechargeRespId);
		
		// logger.info("start excution ===================== method rechargeReport(userId)"+reportBean.getUserId());
		 factory=DBUtil.getSessionFactory();
		 Session session = factory.openSession();
		 Transaction transaction = session.beginTransaction();
		 RechargeTxnBean result = new RechargeTxnBean();
		 try{
		 
		 StringBuilder serchQuery=new StringBuilder();
		 serchQuery.append(" from RechargeTxnBean where rechargeRespId=:rechargeRespId and aggreatorid=:aggreatorid");
		 serchQuery.append(" order by rechargeId ");
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "", "","serchQuery"+serchQuery 
			);
        //System.out.println("~~~~"+ serchQuery);
       
		 Query query=session.createQuery(serchQuery.toString());
		 query.setString("rechargeRespId",rechargeRespId); 
		 query.setString("aggreatorid",aggregatorId); 
		 
		 
		  List<RechargeTxnBean> list = query.list();
		  if(list.size() > 0)
		  {
			  return list.get(0);
		  }
		  else
		  {
			  result.setStatusCode("1001");
			  result.setStatus("No record found.");
		  }
		 }catch(Exception e){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "", "","problem in rechargeReportAggreator"+e.getMessage()+" "+e 
						);
		   // logger.debug("problem in rechargeReportAggreator*****************************************" + e.getMessage(), e);
		    e.printStackTrace();
		   	transaction.rollback();
		    result.setStatusCode("7000");
			result.setStatus("Something went wrong. Please try after sometime.");
		   	} finally {
		   				session.close();
		   	}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"", "", "","","");
		 //logger.info("start excution ===================== method rechargeReport Result "+results.size());
		 return result;
	}


	
	
	public RechargeTxnBean rechargePayment(RechargeTxnBean rechargeTxnBean) {
		 
	     
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"rechargePayment()");
	
		//logger.info("start excution ==============****************************************************************======= method rechargePayment(rechargeTxnBean)");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		RechargeOperatorMasterBean rechargeOperatorMasterBean=new RechargeOperatorMasterBean();
		Transaction transaction = session.beginTransaction();
		String status="1001";
		rechargeTxnBean.setStatusCode("7000");
		try{
		
			int count = ((BigInteger)session.createSQLQuery("select count(*) from rechargemast where rechargerespid ='"+rechargeTxnBean.getRechargeRespId()+"'").uniqueResult()).intValue(); 
			if(count > 0)
			{
				rechargeTxnBean.setStatus("Duplicate Recharge Response ID.");
				rechargeTxnBean.setStatusCode("7002");
				return rechargeTxnBean;
			}
			transaction = session.beginTransaction();
		String rechargeId=commanUtilDao.getTrxId("RECHARGE",rechargeTxnBean.getAggreatorid());
		
		rechargeTxnBean.setRechargeId(rechargeId);
		rechargeTxnBean.setStatus("Cancel");
		rechargeTxnBean.setUseCashBack("N");
		session.save(rechargeTxnBean);
		transaction.commit();
		
		
		
		WalletMastBean aggWallet = walletUserDao.getUserByMobileNo(rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getAggreatorid(),"");
		//logger.debug("json data------"+new Gson().toJson(aggWallet));
		

		rechargeTxnBean.setUserId(aggWallet.getCashDepositeWallet());
		rechargeTxnBean.setWalletId(getWalletIdByUserId(aggWallet.getCashDepositeWallet(),rechargeTxnBean.getAggreatorid()));
		String walletResp="1000";
		
		if(rechargeTxnBean.getRechargeAmt() < 1)
		{
			rechargeTxnBean.setStatus("Invalid Recharge Amount.");
			rechargeTxnBean.setStatusCode("7032");
			return rechargeTxnBean;
		}
		if(getWalletBalancebyId(aggWallet.getCashDepositeWallet()) < rechargeTxnBean.getRechargeAmt())
		{
			rechargeTxnBean.setStatus("Insufficient Balance.");
			rechargeTxnBean.setStatusCode("7024");
			return rechargeTxnBean;
		}else
		{
			walletResp =  reCharge(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),
					rechargeTxnBean.getRechargeAmt(),rechargeTxnBean.getRechargeNumber(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
		}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"walletResp"+walletResp);
		//logger.info("walletResp ===================== method rechargePayment(walletToBankTxnMast)"+walletResp);
		if(walletResp.equals("1000")){
			
			String rechargetype=null;
			 if(rechargeTxnBean.getRechargeOperator().contains("Special")){
				 rechargetype="Special"; 
			 }else{
				 rechargetype="Top Up"; 
			 }
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,walletResp+"|"+rechargeTxnBean.getRechargeOperator());
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,rechargetype+"walletResp  rechargeTxnBean.getRechargeOperator()"+rechargeTxnBean.getRechargeType());
			// logger.info("walletResp ===================================== rechargeTxnBean.getRechargeOperator()"+rechargeTxnBean.getRechargeOperator());
			 //logger.info(rechargetype+"walletResp ===================================== rechargeTxnBean.getRechargeOperator()"+rechargeTxnBean.getRechargeType());
					
			Query query=session.createQuery( "from RechargeOperatorMasterBean where name=:name and servicetype=:servicetype and rechargetype=:rechargetype and flag=1");
			query.setString("name", rechargeTxnBean.getRechargeOperator());
			query.setString("servicetype", rechargeTxnBean.getRechargeType());
			query.setString("rechargetype", rechargetype);
			List <RechargeOperatorMasterBean> list = query.list();
			if(list==null || list.size()>0){
			rechargeOperatorMasterBean=list.get(0);
			if(rechargeOperatorMasterBean.getClient().equals("ezypay")){
			String ezyRes=new EzyPayIntegration().rechargeUsingEazyPay(rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "", rechargeTxnBean.getRechargeNumber(), rechargeId, Double.toString(rechargeTxnBean.getRechargeAmt()),rechargeTxnBean.getStdCode(),rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getRechargeCircle());
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"walletResp  ezyRes"+ezyRes);
			//logger.info("walletResp ===================== ezyRes****"+ezyRes);
			String rechargeStatus="Failed";
			if(!ezyRes.contains("~")){
				rechargeStatus="Failed";
			}
			String[] ezyResvar=ezyRes.split("~");
			if(ezyResvar[3].equals("1")&&   ezyResvar[4].equals("Transaction Successful")){
				transaction=session.beginTransaction();
				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
				rechargeTxnBean.setStatus("Success");
				rechargeTxnBean.setWallettxnStatus(walletResp);
				//rechargeTxnBean.getWa
				rechargeTxnBean.setOther(ezyRes);
				session.update(rechargeTxnBean);
				transaction.commit();
				status="1000";
				}else if(ezyResvar[3].equals("100")&&   ezyResvar[4].equals("Transaction Pending")){
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("Pending");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					rechargeTxnBean.setOther(ezyRes);
					session.update(rechargeTxnBean);
					transaction.commit();
					status="100";
			}else{
				settlement(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
				transaction=session.beginTransaction();
			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			rechargeTxnBean.setStatus("Failed");
			rechargeTxnBean.setWallettxnStatus(walletResp);
			rechargeTxnBean.setOther(ezyRes);
			session.update(rechargeTxnBean);
			transaction.commit();
			status="7043";
			}
		}else if(rechargeOperatorMasterBean.getClient().equals("PAYWORLD")){
			
			//SANCG1011%$B5614793752414229200%$2016-11-17 15:04:01%$A14.SUCCESS%$Transaction Successful%$161117150401C814F700$$$
			List<String> reslist = new ArrayList<String>();
			PayworldResponseData payworldRes=null;
			String resStatus="1000";
			try{
			payworldRes=new EzyPayIntegration().rechargeUsingPayWorld(rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "", rechargeTxnBean.getRechargeNumber(), rechargeId, Double.toString(rechargeTxnBean.getRechargeAmt()),rechargeTxnBean.getStdCode(),rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getRechargeCircle(),rechargeTxnBean.getAggreatorid());
			logger.info("walletResp ===================== payworldRes****"+payworldRes.getResponseMessage());
				

			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"walletResp  payworldRes****"+payworldRes.getResponseMessage());
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"reslist size"+reslist.size());
			//logger.info("walletResp ===================== payworldRes****"+payworldRes.getResponseMessage());
			//logger.info("********************************************reslist size****************************"+reslist.size());
			
			}catch (Exception e) {
				resStatus="1001";
				e.printStackTrace();
				settlement(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"problem in rechargePayment" + e.getMessage()+" "+e);
				logger.debug("problem in rechargePayment=========================" + e.getMessage(), e);
				//settlement(rechargeTxnBean.getUserId(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
				//transaction=session.beginTransaction();
				//rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			}
		if(payworldRes!=null&&(Double.parseDouble(payworldRes.getResponseCode())==0||(payworldRes.getResponseMessage()!=null&&payworldRes.getResponseMessage().toUpperCase().indexOf("SUCCESS")>-1))){
			if (Double.parseDouble(payworldRes.getResponseCode())==0&&payworldRes.getStatus()!=null&&payworldRes.getStatus().toUpperCase().indexOf("SUCCESS")>-1) {
					transaction=session.beginTransaction();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"---------------inside success");
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("SUCCESS");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					//rechargeTxnBean.getWa
					rechargeTxnBean.setOther(payworldRes.getMessage());
					rechargeTxnBean.setOperatorTxnId(payworldRes.getReferenceNumber()!=null?payworldRes.getReferenceNumber():"");
					session.update(rechargeTxnBean);
					transaction.commit();
					status="1000";
				}
				else if(Double.parseDouble(payworldRes.getResponseCode())==0&&payworldRes.getStatus()!=null&&payworldRes.getStatus().toUpperCase().indexOf("INITIATED")>-1){
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("Pending");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					rechargeTxnBean.setOther(payworldRes.getMessage());
					rechargeTxnBean.setOperatorTxnId(payworldRes.getReferenceNumber()!=null?payworldRes.getReferenceNumber():"");
					session.update(rechargeTxnBean);
					transaction.commit();
					status="100";
				}else{
					
				
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("Failed");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					rechargeTxnBean.setOther(payworldRes.getMessage()!=null?payworldRes.getMessage():payworldRes.getResponseMessage());
					rechargeTxnBean.setOperatorTxnId(payworldRes.getReferenceNumber()!=null?payworldRes.getReferenceNumber():"");
					session.update(rechargeTxnBean);
					transaction.commit();
					settlement(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
					status="7043";
				}
							
			}
			else if(payworldRes!=null&&(Double.parseDouble(payworldRes.getResponseCode())==1001||(payworldRes.getResponseMessage()!=null&&payworldRes.getResponseMessage().toUpperCase().indexOf("FAILED")>-1))){
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"---------------inside fail");
				settlement(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
				
				transaction=session.beginTransaction();
				rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
				rechargeTxnBean.setStatus("Failed");
				rechargeTxnBean.setWallettxnStatus(walletResp);
				rechargeTxnBean.setOther(payworldRes.getMessage()!=null?payworldRes.getMessage():payworldRes.getResponseMessage());
				rechargeTxnBean.setOperatorTxnId(payworldRes.getReferenceNumber()!=null?payworldRes.getReferenceNumber():"");
				session.update(rechargeTxnBean);
				transaction.commit();
				status="7043";
			}
			else{
				status="7043";	
			}
				
			}else if(rechargeOperatorMasterBean.getClient().equals("JOLO")){
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"Indide JOLO"+Double.toString(rechargeTxnBean.getRechargeAmt().intValue()));
				//logger.info("********************************************Indide JOLO****************************"+Double.toString(rechargeTxnBean.getRechargeAmt().intValue()));	
				String joloRes=new EzyPayIntegration().rechargeUsingJOLO(rechargeTxnBean.getRechargeOperator(), rechargeTxnBean.getRechargeType(), "", rechargeTxnBean.getRechargeNumber(), rechargeId, Integer.toString(rechargeTxnBean.getRechargeAmt().intValue()),rechargeTxnBean.getStdCode(),rechargeTxnBean.getAccountNumber(),rechargeTxnBean.getRechargeCircle());
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"walletResp  joloRes"+joloRes);
				//logger.info("walletResp ****************************** joloRes******************************************"+joloRes);
				String rechargeStatus="Failed";
				if(!joloRes.contains(",")){
					rechargeStatus="Failed";
				}
				String respMsg[] = joloRes.split(",");
				if (respMsg.length > 2) {
					
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("SUCCESS");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					rechargeTxnBean.setOther(joloRes);
					rechargeTxnBean.setOperatorTxnId(respMsg[0]);
					rechargeTxnBean.setOperatorName("JOLO");
					session.update(rechargeTxnBean);
					transaction.commit();
					status="1000";
					}else{
						
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					settlement(rechargeTxnBean.getAggreatorid(),getWalletIdByUserId(aggWallet.getCashDepositeWallet(), rechargeTxnBean.getAggreatorid()),rechargeTxnBean.getRechargeAmt(),rechargeId,rechargeTxnBean.getIpIemi(),rechargeTxnBean.getRechargeNumber(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getTxnAgent());
					transaction=session.beginTransaction();
					rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
					rechargeTxnBean.setStatus("Failed");
					rechargeTxnBean.setWallettxnStatus(walletResp);
					rechargeTxnBean.setOther(joloRes);
					rechargeTxnBean.setOperatorName("JOLO");
					session.update(rechargeTxnBean);
					transaction.commit();
					status="7043";
					}
			}
		
			
		}
			}else{
			transaction=session.beginTransaction();
			rechargeTxnBean=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			rechargeTxnBean.setStatus("Failed");
			rechargeTxnBean.setStatusCode(walletResp);
			rechargeTxnBean.setWallettxnStatus(walletResp);
			session.update(rechargeTxnBean);
			transaction.commit();
			status="7000";
			}
		    }catch(Exception e){
		    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(),  "","" ,"problem in rechargePayment" + e.getMessage()+" "+e);
		    	//logger.debug("problem in rechargePayment=========================" + e.getMessage(), e);
		    	 e.printStackTrace();
		   		transaction.rollback();
		   		status= "7000";
		   	} finally {
		   		session.close();
		   	}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ," excution status"+status);
		    logger.info(" excution ===================== status"+status);	
		    
		   if(status.equalsIgnoreCase("1000")){
			   try{
			   walletConfiguration = commanUtilDao.getWalletConfiguration(rechargeTxnBean.getAggreatorid());
			     walletMastBean=walletUserDao.showUserProfile(rechargeTxnBean.getUserId());
			   //Dear <<APPNAME>> User, Recharge/Bill Payment Of <<OPERATOR>> <<TYPE>>  <<Number>> for Rs. <<AMOUNT>>.Transaction id:<<TXNNO>>
			    //String smstemplet = commanUtilDao.getsmsTemplet("recharge",rechargeTxnBean.getAggreatorid()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OPERATOR>>", rechargeTxnBean.getRechargeOperator()).replace("<<TYPE>>", rechargeTxnBean.getRechargeType()).replace("<<NUMBER>>", rechargeTxnBean.getRechargeNumber()).replace("<<AMOUNT>>", Double.toString(rechargeTxnBean.getRechargeAmt())).replace("<<TXNNO>>", rechargeTxnBean.getRechargeId());
			     String smstemplet = commanUtilDao.getsmsTemplet("recharge",rechargeTxnBean.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<OPERATOR>>", rechargeTxnBean.getRechargeOperator()).replace("<<TYPE>>", rechargeTxnBean.getRechargeType()).replace("<<NUMBER>>", rechargeTxnBean.getRechargeNumber()).replace("<<AMOUNT>>", Double.toString(rechargeTxnBean.getRechargeAmt())).replace("<<TXNNO>>", rechargeTxnBean.getRechargeId()).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
		     	
			    //Dear <<NAME>>, Recharge/Bill payment of <<OPERATOR>> <<TYPE>>  <<NUMBER>> for Rs. <<AMOUNT>>.Transaction Id is <<TXNNO>>.
			     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"Recharge Send SMS" + smstemplet);
			    // logger.info("~~~~~~~~~~~~~~~~~~~~~Recharge Send SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
		     	String mailSub= "Recharge of Rs "+String.format("%.2f", rechargeTxnBean.getRechargeAmt())+" for "+rechargeTxnBean.getRechargeNumber() +" is successful.";
		     	String mailContent= commanUtilDao.getmailTemplet("recharge",rechargeTxnBean.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<OPERATOR>>", rechargeTxnBean.getRechargeOperator()).replace("<<TYPE>>", rechargeTxnBean.getRechargeType()).replace("<<NUMBER>>", rechargeTxnBean.getRechargeNumber()).replace("<<AMOUNT>>", String.format("%.2f",rechargeTxnBean.getRechargeAmt())).replace("<<TXNNO>>", rechargeTxnBean.getRechargeId()).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));;
		     	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"Recharge Send  MAIL" + mailContent);
		     	//logger.info("~~~~~~~~~~~~~~~~~~~~~Recharge Send  MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);
		     	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),walletMastBean.getName(),"NOTP");
				Thread t = new Thread(smsAndMailUtility);
				t.start();  
		     	
//		     	ThreadUtil.getThreadPool().execute(smsAndMailUtility);
		   }catch (Exception e) {
			   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),rechargeTxnBean.getAggreatorid(),rechargeTxnBean.getWalletId(),rechargeTxnBean.getUserId(), "","" ,"problem in rechargePayment transfer while SMS Mail And GCM" + e.getMessage()+" "+e);
				 logger.debug("problem in rechargePayment transfer while SMS Mail And GCM " + e.getMessage(), e);
			}
			   
		   }
		    
			    
		rechargeTxnBean.setStatusCode(status); 
		return rechargeTxnBean;
		}
	
	 public double getCashDepositWalletBalance(String aggrigatorId)
	 {
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",aggrigatorId, "", "", "" 
	    + "getCashDepositWalletBalance"
	    );
	  double cashDepositBalance=0.0;
	   factory=DBUtil.getSessionFactory();
	   Session session = factory.openSession();
	   Transaction transaction = session.beginTransaction();
	   try{
	     SQLQuery query=session.createSQLQuery("SELECT finalbalance FROM walletbalance WHERE walletid IN (SELECT walletid FROM walletmast WHERE id IN (SELECT  cashdepositewallet FROM walletmast WHERE id='"+aggrigatorId+"' AND userstatus='A'))");
	    cashDepositBalance=(double) query.uniqueResult();
	    System.out.println("cash depositBalance="+cashDepositBalance);
	   }
	   catch (Exception e) {
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggrigatorId,"",aggrigatorId, "","" ,"problem in getCashDepositWalletBalance " + e.getMessage()+" "+e);
	          e.printStackTrace();
	   }
	   finally {
	        session.close(); 
	       }
	  return cashDepositBalance;
	 }
	 
	  public double getAverageBalance(String aggrigatorId)
	  {
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",aggrigatorId, "", "", "" 
	     + "getAverageBalance"
	     );
	   double avgBalance=0.0;
	   factory=DBUtil.getSessionFactory();
	   Session session = factory.openSession();
	   Transaction transaction = session.beginTransaction();
	   int MILLIS_IN_DAY = 1000*60*60 * 24;
	  
	     Date date = new Date();
	     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	     String currDate = dateFormat.format(date.getTime() - MILLIS_IN_DAY);
	     String prevDate = dateFormat.format(date.getTime() - 6*(MILLIS_IN_DAY));
	     System.out.println("Currnent date: " + currDate);
	     System.out.println("Previous date: " + prevDate);
	   try{
	  
	   SQLQuery query=session.createSQLQuery("SELECT SUM(rechargeamt)*2/6 FROM rechargemast WHERE  DATE(rechargeDate) BETWEEN '"+ prevDate +"' AND '"+currDate +"' AND aggreatorid='"+aggrigatorId+"' AND STATUS='success'");
	   avgBalance=(double) query.uniqueResult();
	    System.out.println("cash depositBalance="+avgBalance);
	   }
	   catch (Exception e) {
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggrigatorId,"",aggrigatorId, "","" ,"problem in getAverageBalance " + e.getMessage()+" "+e);
	          e.printStackTrace();
	   }
	   finally {
	        session.close(); 
	       }
	  return avgBalance;
	   
	  }
	  
	  public String revokeRequest(String userId,String walletId,double amount,String ipImei,String mobileNo,String revokeId,String aggreatorid,String agent){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ,"start execution of revokeRequest");
			factory = DBUtil.getSessionFactory();
			Session session = factory.openSession();
			try
			{
				transaction = session.beginTransaction();
				SQLQuery insertQueryt = session.createSQLQuery( "insert into trackrequest(walletid) VALUES (?)");
				insertQueryt.setParameter(0, walletId);
				insertQueryt.executeUpdate();
				transaction.commit();
				return paymentFromWallet(userId,walletId,50,amount,mobileNo,revokeId,ipImei,aggreatorid,agent,0.0);
				//walletSettlement(userId,walletId,amount,settTxnId,ipImei,mobileNo,aggreatorid,agent,50);
			}
			catch(Exception ex)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "","" ,"problem in Request | e.getMessage "+ex.getMessage());
				ex.printStackTrace();
			}
			finally {
		  		transaction=session.beginTransaction();
				SQLQuery delete = session.createSQLQuery( "delete from trackrequest where walletid='"+walletId+"'");
				delete.executeUpdate();
				transaction.commit();
				session.close();
			}
			return "7000";
		}

	@Override
	public List getRechargePendingStatus(String aggreatorId,Long period) 
	{
		
		

		   Session session = null;
		   Date date = new Date();
		      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   String currDate = dateFormat.format(date.getTime());
		      String prevDate = dateFormat.format(date.getTime() - period);
		      List<RechargeTxnBean> rechargeList = new ArrayList<RechargeTxnBean>();
		   try
		   {
		    factory = DBUtil.getSessionFactory();
		    session = factory.openSession();
		    Criteria criteria = session.createCriteria(RechargeTxnBean.class);
		    criteria.add(Restrictions.like("other", "%|%")).add(Restrictions.eq("aggreatorid", aggreatorId))
		      .add(Restrictions.between("rechargeDate", dateFormat.parse(prevDate), dateFormat.parse(currDate)));
		    rechargeList = criteria.list();
		   }
		   catch(Exception ex)
		   {
		    ex.printStackTrace();
		    logger.error("Exception occure while fetching getRechargePendingStatus for aggregator : "+aggreatorId+" and on Time : "+currDate);
		   }
		   finally
		   {
		    session.close();
		   }
		   return rechargeList;
		  
	}
	
	 

	  
	  public void deductMaintanceCharges(String userId,String aggregatorId,double amount,String ipIemi,String agent)
	  {
		  try
			 {
				WalletMastBean walletAgent =walletUserDao.getUserByMobileNo(userId, aggregatorId, "");
				if(walletAgent != null && walletAgent.getUsertype() == 2)
				{
					Object amt = session.createSQLQuery("SELECT remaining_charge FROM walletmaintenance WHERE agentid='"+walletAgent.getId()+"'").uniqueResult();
					if(amt != null)
					{
						Double deductionAmt = Double.parseDouble(amt.toString());
						Double remainingAmt = 0.0;
						if(amount< deductionAmt)
						{
							remainingAmt = deductionAmt - amount;
							deductionAmt = amount;
						}
						String respTxnId =commanUtilDao.getTrxId("AGNTCHARGES",aggregatorId);
							
						String flagdebit = maintenanceChargeDeduction( respTxnId,walletAgent.getId(),deductionAmt,0.0,ipIemi,aggregatorId,agent);
						if(flagdebit.equals("1000"))
						{
							session.createSQLQuery("UPDATE walletmaintenance SET remaining_charge="+remainingAmt+",lasttxndate=NOW() WHERE agentid='"+walletAgent.getId()+"'").executeUpdate();
							transaction.commit();
							transaction=session.beginTransaction();
						}
					}
				}
		 	}
			catch(Exception e)
			{
				e.printStackTrace(); 
				logger.debug("problem in deductMaintanceCharges while remaining amount debit " + e.getMessage(), e);
			}
	  }
	  
	  public void markFinalStatusMark(String txid,String accountID,String Transtype)
	  {
		  logger.info("start execution of markFinalStatusMark from MyPayStore having requestID as "+accountID);
		  Session session  = null;
		  Transaction transaction = null;
		  try
		  {
			  factory = DBUtil.getSessionFactory();
			  session = factory.openSession();
			  transaction = session.beginTransaction();
			  
			  List<String> status = new ArrayList<>(Arrays.asList("SUBMITTED","Pending"));
			  Criteria criteria = session.createCriteria(RechargeTxnBean.class).add(Restrictions.eq("operatorTxnId", accountID)).add(Restrictions.in("status", status))
					  .add(Restrictions.eq("operatorName", "MyPayStore"));
			  RechargeTxnBean recharge = (RechargeTxnBean)criteria.uniqueResult();
			  if(recharge != null)
				  {
				  if(Transtype.equalsIgnoreCase("F"))
				  {
					  if(recharge.getUseCashBack().equalsIgnoreCase("Y")){
							cashBacksettlement(recharge.getUserId(),recharge.getWalletId(),recharge.getRechargeAmt(),recharge.getRechargeId(),recharge.getIpIemi(),recharge.getRechargeNumber(),recharge.getAggreatorid(),recharge.getTxnAgent());
						}else{
							settlement(recharge.getUserId(),recharge.getWalletId(),recharge.getRechargeAmt(),recharge.getRechargeId(),recharge.getIpIemi(),recharge.getRechargeNumber(),recharge.getAggreatorid(),recharge.getTxnAgent());
						}
					  
					  recharge.setStatus("FAILED");
					  recharge.setRechargeRespId(txid);
					  session.update(recharge);
				  }
				  else if(Transtype.equalsIgnoreCase("S"))
				  {
					  recharge.setStatus("Success");
					  recharge.setRechargeRespId(txid);
					  session.update(recharge);
					  
					  try{
						   walletConfiguration = commanUtilDao.getWalletConfiguration(recharge.getAggreatorid());
						     walletMastBean=walletUserDao.showUserProfile(recharge.getUserId());
						  
						     String smstemplet = commanUtilDao.getsmsTemplet("recharge",recharge.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<OPERATOR>>", recharge.getRechargeOperator()).replace("<<TYPE>>", recharge.getRechargeType()).replace("<<NUMBER>>", recharge.getRechargeNumber()).replace("<<AMOUNT>>", Double.toString(recharge.getRechargeAmt())).replace("<<TXNNO>>", recharge.getRechargeId()).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));
					     	
						    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),recharge.getAggreatorid(),recharge.getWalletId(),recharge.getUserId(), "","markFinalStatusMark()","Recharge Send SMS" + smstemplet);
						    String mailSub= "Recharge of Rs "+String.format("%.2f", recharge.getRechargeAmt())+" for "+recharge.getRechargeNumber() +" is successful.";
					     	String mailContent= commanUtilDao.getmailTemplet("recharge",recharge.getAggreatorid()).replace("<<NAME>>", walletMastBean.getName()).replace("<<OPERATOR>>", recharge.getRechargeOperator()).replace("<<TYPE>>", recharge.getRechargeType()).replace("<<NUMBER>>", recharge.getRechargeNumber()).replace("<<AMOUNT>>", String.format("%.2f",recharge.getRechargeAmt())).replace("<<TXNNO>>", recharge.getRechargeId()).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")).replace("<<WALLETBALANCE>>",Double.toString(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())));;
					     	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),recharge.getAggreatorid(),recharge.getWalletId(),recharge.getUserId(), "","markFinalStatusMark()","Recharge Send  MAIL" + mailContent);
					     	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), mailContent,mailSub, walletMastBean.getMobileno(), smstemplet, "Both",recharge.getAggreatorid(),recharge.getWalletId(),walletMastBean.getName(),"NOTP"); 
					     	
					     	ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					  }
					  catch(Exception ex)
					  {
						  logger.info("problem occured while sending message in markFinalStatusMark having accountId "+accountID+" with reason "+ex.getMessage());
						  ex.printStackTrace();
					  }
				  }
				  
				  transaction.commit();
			 }
			  else
			  {
				  logger.info("No recharge found of "+accountID);
			  }
		  }
		  catch(Exception ex)
		  {
			  logger.info("problem occured while markFinalStatusMark having accountId "+accountID+" with reason "+ex.getMessage());
			  ex.printStackTrace();
			  if(transaction.isActive())
				  transaction.rollback();
		  }
		  finally
		  {
			  if(session.isOpen())
				  session.close();
		  }
			
	  }
	  
	  
	  @Override
		 public RechargeTxnBean  getRechargeTxnBean(String  rechargeId) {
			 
			 
			// TODO Auto-generated method stubsdsd
				
			 RechargeTxnBean bean1=new RechargeTxnBean() ;
				factory=DBUtil.getSessionFactory();
				Session session = factory.openSession();
				Transaction transaction = session.beginTransaction();
			    
			    try{
			    	
			    	bean1=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			    	/* bean1.setId("OAGG001057");
			    	 bean1.setMid("1903260434111000");
			    	 bean1.setPassword("");
			    	 bean1.setTransactionkey("2cafbc664d074721");
			    	*/
			    	 transaction.commit();
			}
			    
			    catch(Exception e) {
			    	
			    }
			    finally {
					session.close();
				}
			    return bean1 ;
			 
		 }
	  
	  
	  @Override
		 public RechargeTxnBean  updateRechargeTxnBean(String  rechargeId,String rechargeStatus) {
			 
		  logger.info("----------------$$$$$$$$$$$$$$$$$$from moneyWaysCallBack##########--------------------");
		  logger.info("----------------$$$$$$$$$$$$$$$$$$   "+rechargeStatus+" ##########--------------------");
		  logger.info("----------------$$$$$$$$$$$$$$$$$$   "+rechargeId+" ##########--------------------");
		  
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",rechargeId, "", "", "" + " $$$$$$$$$$$$$$$$$$from moneyWaysCallBack111##########  "+rechargeStatus);
			 
			// TODO Auto-generated method stubsdsd
				
			 RechargeTxnBean bean1=new RechargeTxnBean() ;
				factory=DBUtil.getSessionFactory();
				Session session = factory.openSession();
				Transaction transaction = session.beginTransaction();
			    
			    try{
			    	
			    	bean1=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			    	
			    	
			    	/* bean1.setId("OAGG001057");
			    	 bean1.setMid("1903260434111000");
			    	 bean1.setPassword("");
			    	 bean1.setTransactionkey("2cafbc664d074721");
			    	*/
			    	bean1.setStatus(rechargeStatus);
			    	
			   session.save(bean1);
			    	
			    	
			    	 transaction.commit();
			    	 
			    	 bean1=(RechargeTxnBean)session.get(RechargeTxnBean.class, rechargeId);
			}
			    
			    catch(Exception e) {
			    	logger.error(e);
			    }
			    finally {
					session.close();
				}
			    return bean1 ;
			 
		 }
	  
	  public List subAggregatorUtility(String aggId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","" ,"subAggregatorUtility()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List subAggregatorList=new ArrayList();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select distinct id,concat(id,'---',name) from walletmast where aggreatorid='"+aggId+"' and usertype in ('2')";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				 subAggregatorList = query.list();
				
				if (subAggregatorList != null && subAggregatorList.size() != 0) {
				//	walletid=	(String) list.get(0);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "","getWalletIdByUserId()","Walletid :"+ aggId);
					//logger.info("==============Wallet Id=============================="+walletid);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return subAggregatorList;
	    }
	  
	  
	  
	  
	  
	    
	  public List superDistributerListChange(String aggId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","" ,"superDistributerListChange()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "", "","method stDate");
			List subAggregatorList=new ArrayList();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				
				String sqlquery="select distinct id,concat(id,'---',name) from walletmast where aggreatorid='"+aggId+"' and usertype in ('7')";
				Query query=session.createSQLQuery(sqlquery);
		
				 subAggregatorList = query.list();
				
				if (subAggregatorList != null && subAggregatorList.size() != 0) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "","superDistributerListChange()","Walletid :"+ aggId);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","superDistributerListChange()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","superDistributerListChange()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			return subAggregatorList;
	    }
	  
	  
	    public List utilProfileChange(String aggId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","" ,"subAggregatorUtility()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List subAggregatorList=new ArrayList();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select distinct id,concat(id,'---',name) from walletmast where aggreatorid='"+aggId+"' and (usertype='3' or usertype='2' or usertype='7' )";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				 subAggregatorList = query.list();
				
				if (subAggregatorList != null && subAggregatorList.size() != 0) {
				//	walletid=	(String) list.get(0);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "","getWalletIdByUserId()","Walletid :"+ aggId);
					//logger.info("==============Wallet Id=============================="+walletid);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return subAggregatorList;
	    }
	    
	    
	    
	    public List distributorList(String aggId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","" ,"subAggregatorUtility()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List distributorList=new ArrayList();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select distinct id,concat(id,'---',name) from walletmast where aggreatorid='"+aggId+"' and usertype in ('3')";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				distributorList = query.list();

				
				if (distributorList != null && distributorList.size() != 0) {
				//	walletid=	(String) list.get(0);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "","getWalletIdByUserId()","Walletid :"+ aggId);
					//logger.info("==============Wallet Id=============================="+walletid);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+distributorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return distributorList;
	    }
	    public String changeDistributor(String id,String distributerId) {
	    	factory=DBUtil.getSessionFactory();
	    	String message="";
			Session session = factory.openSession();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeDistributor start","", "","changeDistributor()","checking changeDistributor ");
			
	  	     Transaction transaction = session.beginTransaction();
	  	     try {
	  	    	WalletMastBean walletToBankTxnMast=(WalletMastBean)session.get(WalletMastBean.class, id);
	  	    	walletToBankTxnMast.setDistributerid(distributerId);
	  	     	session.saveOrUpdate(walletToBankTxnMast);
				transaction.commit();
				message="Successfully Updated.";
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeDistributor","", "","changeDistributor()","checking changeDistributor message "+message);
					
	  	     }
	  	     catch(Exception e) {
	  	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeDistributor","", "","changeDistributor()","checking error changeDistributor message "+e.getMessage()  );
				
	  	    	transaction.rollback();
	  	    	message="Error Occured.";
	  	    	
	  	     }
			 finally{
				  session.close();
			 }
	  	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeDistributor","", "","changeDistributor()","checking  changeDistributor message "+message  );
			
	    	return message;
	    }
	    
	    
	    public String changeSuperDistributor(String id,String distributerId) {
	    	factory=DBUtil.getSessionFactory();
	    	String message="";
			Session session = factory.openSession();
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeSuperDistributor","", "","changeSuperDistributor()","checking changeSuperDistributor ");
			 
	  	     Transaction transaction = session.beginTransaction();
	  	     try {
	  	    	WalletMastBean walletToBankTxnMast=(WalletMastBean)session.get(WalletMastBean.class, id);
	  	    	walletToBankTxnMast.setSuperdistributerid(distributerId);
	  	    	session.saveOrUpdate(walletToBankTxnMast);
	  	   		SQLQuery query = session.createSQLQuery("update walletmast set superdistributerid='"+distributerId+"' where distributerid='"+id+"' and usertype=2");
	  	   		query.executeUpdate();
				transaction.commit();
				message="Successfully Updated.";
				 
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeSuperDistributor","", "","changeSuperDistributor()","success changeSuperDistributor message "+message);
				 
	  	     }
	  	     catch(Exception e) {
	  	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeSuperDistributor error","", "","changeSuperDistributor()","checking changeSuperDistributor error"+ e.getMessage()+" "+e);
				 
	  	    	transaction.rollback();
	  	    	message="Error Occured.";
	  	     }
			 finally{
				  session.close();
			 }
	  	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","changeSuperDistributor","", "","changeSuperDistributor()","success changeSuperDistributor "+message);
			 
	    	return message;
	    }
	    
	    public String utilChangeAgentProfile(String customerId,String agentName,String agentMobile,String agentMail) {
	    	factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
	  	     String message="";
	  	   List recordList=new ArrayList();
	  	     try {
	  	    	
	  	    	
	  	    	String sqlquery="select count(*) Total from walletmast where id !='"+customerId+"' and mobileno='"+agentMobile+"'";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				recordList = query.list();
	  	    	BigInteger bigInteger=(BigInteger)recordList.get(0);
				 

				 
				String totalrecords= String.valueOf(bigInteger);
				
				if(!totalrecords.equals("0")) {
					message="Duplicate Mobile No Found.";
				}
				else {
					WalletMastBean walletToBankTxnMast=(WalletMastBean)session.get(WalletMastBean.class, customerId);
					walletToBankTxnMast.setName(agentName);
					walletToBankTxnMast.setMobileno(agentMobile);
					walletToBankTxnMast.setEmailid(agentMail);
					
					session.saveOrUpdate(walletToBankTxnMast);
					
					
					message="Successfully Updated";
				}
	  	    	
	  	    	
	  	    	
				transaction.commit();
	  	     }
	  	     catch(Exception e) {
	  	    	transaction.rollback();
	  	    	message="Error Occured.";
	  	     }
			 finally{
				  session.close();
			 }
	    	return message;
	    }
	    
	    public Map<String,String> subAggregatorUtilityChange(String customerId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","" ,"subAggregatorUtility()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List subAggregatorList=new ArrayList();
			Map<String,String> detailMap=new HashMap<String ,String>();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select id,name,mobileno,distributerid from walletmast where id='"+customerId+"'";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				 subAggregatorList = query.list();
				 
					
					if (subAggregatorList != null && subAggregatorList.size() != 0) {
					//	walletid=	(String) list.get(0);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
						//logger.info("==============Wallet Id=============================="+walletid);
					}
				 
				 Object[] obj=(Object[])subAggregatorList.get(0);
				 

				 
				 detailMap.put("customerId",String.valueOf(obj[0]) );
				 detailMap.put("customername",String.valueOf(obj[1]));
				 detailMap.put("customermobileNo", String.valueOf(obj[2]));
				 detailMap.put("distributerId",String.valueOf(obj[3]));
				 
				 String distributorId=String.valueOf(obj[3]);
				 
				 sqlquery="select id,name,mobileno from walletmast where id='"+distributorId+"'";
				
				 Query query1=session.createSQLQuery(sqlquery);
				 
				 subAggregatorList = query1.list();

				 
				 
				 Object[] obj1=(Object[])subAggregatorList.get(0);
				 			 
				 
					
					if (subAggregatorList != null && subAggregatorList.size() != 0) {
					//	walletid=	(String) list.get(0);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
						//logger.info("==============Wallet Id=============================="+walletid);
					}
				 detailMap.put("distributerId",String.valueOf(obj1[0]) );
				 detailMap.put("distributername",String.valueOf(obj1[1]));
				 detailMap.put("distributermobileNo", String.valueOf(obj1[2]));
				 
				 
				
				
				if (subAggregatorList != null && subAggregatorList.size() != 0) {
				//	walletid=	(String) list.get(0);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
					//logger.info("==============Wallet Id=============================="+walletid);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return detailMap;
	    }
	    
	    
	    
	    public Map<String,String> superDistributorUtilityChange(String customerId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","" ,"subAggregatorUtility()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List subAggregatorList=new ArrayList();
			Map<String,String> detailMap=new HashMap<String ,String>();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select id,name,mobileno,superdistributerid from walletmast where id='"+customerId+"'";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				 subAggregatorList = query.list();
				 
					
					if (subAggregatorList != null && subAggregatorList.size() != 0) {
					//	walletid=	(String) list.get(0);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
						//logger.info("==============Wallet Id=============================="+walletid);
					}
				 
				 Object[] obj=(Object[])subAggregatorList.get(0);
				 

				 
				 detailMap.put("customerId",String.valueOf(obj[0]) );
				 detailMap.put("customername",String.valueOf(obj[1]));
				 detailMap.put("customermobileNo", String.valueOf(obj[2]));
				 detailMap.put("distributerId",String.valueOf(obj[3]));
				 
				 String distributorId=String.valueOf(obj[3]);
				 
				 sqlquery="select id,name,mobileno from walletmast where id='"+distributorId+"'";
				
				 Query query1=session.createSQLQuery(sqlquery);
				 
				 subAggregatorList = query1.list();

				 
				 
				 Object[] obj1=(Object[])subAggregatorList.get(0);
				 			 
				 
					
					if (subAggregatorList != null && subAggregatorList.size() != 0) {
					//	walletid=	(String) list.get(0);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
						//logger.info("==============Wallet Id=============================="+walletid);
					}
				 detailMap.put("distributerId",String.valueOf(obj1[0]) );
				 detailMap.put("distributername",String.valueOf(obj1[1]));
				 detailMap.put("distributermobileNo", String.valueOf(obj1[2]));
				 
				 
				
				
				if (subAggregatorList != null && subAggregatorList.size() != 0) {
				//	walletid=	(String) list.get(0);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
					//logger.info("==============Wallet Id=============================="+walletid);
				}
				 
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return detailMap;
	    }
	    
	    public Map<String,String> utilProfileAgentDetails(String customerId){

	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","" ,"utilProfileAgentDetails()");
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "", "","method stDate");
	    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
	    	//logger.info("start excution ===================== method stDate"+stDate);
			List subAggregatorList=new ArrayList();
			Map<String,String> detailMap=new HashMap<String ,String>();
			factory=DBUtil.getSessionFactory();
			Session session = factory.openSession();
	  	     Transaction transaction = session.beginTransaction();
			try {
				//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
				
				//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
				
				String sqlquery="select id,name,mobileno,emailid  from walletmast where id='"+customerId+"'";
				Query query=session.createSQLQuery(sqlquery);
				
				//query.setString("aggreatorid", aggId);
			//	query.setInteger("usertype", 2);
				 subAggregatorList = query.list();
				 
					
					if (subAggregatorList != null && subAggregatorList.size() != 0) {
					//	walletid=	(String) list.get(0);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","", "","getWalletIdByUserId()","Walletid :"+ customerId);
						//logger.info("==============Wallet Id=============================="+walletid);
					}
				 
				 Object[] obj=(Object[])subAggregatorList.get(0);
				 

				 
				 detailMap.put("idAgent",String.valueOf(obj[0]) );
				 detailMap.put("nameAgent",String.valueOf(obj[1]));
				 detailMap.put("mobileAgent", String.valueOf(obj[2]));
				 detailMap.put("mailAgent",String.valueOf(obj[3]));
				
				 }
				
			catch(Exception e){
				e.printStackTrace();
				  transaction.rollback();
				  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","checking subAggregatorUtility error"+ e.getMessage()+" "+e);
				  logger.debug("checking showPassbook*****************error************** " + e.getMessage(), e);
				 }
				 finally{
				  session.close();
			 }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","subAggregatorUtility()","stop excution subAggregatorUtility return"+subAggregatorList.size());
			//logger.info("stop excution *********************************getTrxdtl******** return**********"+passbook.size());  
			return detailMap;
	    }
	  
	  public static void main(String[] args) {
			new  TransactionDaoImpl().markFinalStatusMark("","12345","S");
		}

	@Override
	public void callCreditWallet(RechargeTxnBean rechargeTxnBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",rechargeTxnBean.getUserId(),"", "","" ,"callCreditWallet()");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		
		SQLQuery creditWalletQueryWalletId=null;
		SQLQuery creditWalletQuery=null;
		
		
		try {
			
			if(rechargeTxnBean!=null) {
				String queryWalletId = "select walletid from walletmast where id in (select cashdepositewallet  from walletmast where id='"+rechargeTxnBean.getAggreatorid()+"' AND whitelabel=1 );";
				creditWalletQueryWalletId = session.createSQLQuery(queryWalletId);
				
				Object WalletIdObj = creditWalletQueryWalletId.uniqueResult();
				

				String queryStr = "call creditwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";

				if(WalletIdObj != null) {
					
					SQLQuery creditWalletQueryAgent = session.createSQLQuery(queryStr);
					creditWalletQueryAgent.setParameter("aggregatorid", rechargeTxnBean.getAggreatorid());
					creditWalletQueryAgent.setParameter("walletid", WalletIdObj);
					creditWalletQueryAgent.setParameter("amount", rechargeTxnBean.getRechargeAmt());
					creditWalletQueryAgent.setParameter("narration", "rechargerefund");
					creditWalletQueryAgent.setParameter("resptxnid", rechargeTxnBean.getRechargeId());
					creditWalletQueryAgent.setParameter("txncode", 94);
					
					creditWalletQueryAgent.executeUpdate();
					
				}
				
				creditWalletQuery = session.createSQLQuery(queryStr);
				creditWalletQuery.setParameter("aggregatorid", rechargeTxnBean.getAggreatorid());
				creditWalletQuery.setParameter("walletid", rechargeTxnBean.getWalletId());
				creditWalletQuery.setParameter("amount", rechargeTxnBean.getRechargeAmt());
				creditWalletQuery.setParameter("narration", "rechargerefund");
				creditWalletQuery.setParameter("resptxnid", rechargeTxnBean.getRechargeId());
				creditWalletQuery.setParameter("txncode", 6);

				creditWalletQuery.executeUpdate();
				transaction.commit();
			}		
		}
		catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",rechargeTxnBean.getAggreatorid(),"", "","callCreditWallet()","checking callCreditWallet error"+ e.getMessage()+" "+e);
			  logger.debug("checking callCreditWallet *****************error************** " + e.getMessage(), e);
		}
		finally{
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",rechargeTxnBean.getAggreatorid(),"", "","callCreditWallet()","stop excution callCreditWallet return"+rechargeTxnBean.getAggreatorid());
	}

	@Override
	public boolean debitWalletForBBPSTransacitons(String userId, String walletId, String aggreatorId, String dmtTxnID,double dbtAmout,String resptxnId, String userAgent, String ipAddress) {
		// TODO Auto-generated method stub
		boolean statusFlag=false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()|Try to Debit wallet.");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();

		try {
			
			WalletMastBean aggWallet=(WalletMastBean)session.get(WalletMastBean.class, aggreatorId);
			if(aggWallet!=null && "1".equalsIgnoreCase(aggWallet.getWhiteLabel()) && aggWallet.getCashDepositeWallet()!=null) {
				WalletMastBean aggCashWallet=(WalletMastBean)session.get(WalletMastBean.class, aggWallet.getCashDepositeWallet());
				WalletBalanceBean walletDetails=(WalletBalanceBean)session.get(WalletBalanceBean.class, walletId);
				double finalBalAMount=0.0;
				if(walletDetails!=null) {
					finalBalAMount=walletDetails.getFinalBalance()-dbtAmout;
				if(finalBalAMount>=0)
				{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()|Try to debit from whitelabel aggregator wallet.");
				String resp= billPayment(userId,aggCashWallet.getWalletid(),dbtAmout,resptxnId,dmtTxnID,ipAddress,aggreatorId,userAgent,28);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| debit from aggregator wallet "+resp);
				if("1000".equalsIgnoreCase(resp)) {
					String walletResp= reChargeByTxnCode(userId,walletId,dbtAmout,resptxnId,dmtTxnID,ipAddress,aggreatorId,userAgent,134);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| debit from whitelabel agent Try to Debit wallet."+walletResp);
					if("1000".equalsIgnoreCase(walletResp)) {
						statusFlag=true;
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| debit from agent wallet Try to Debit wallet.");
					}
				 }
			    }else
				  statusFlag=false;  
				}else
				 statusFlag=false;
				
			}else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| bhartipay start debit from agent wallet Try to Debit wallet.");
				
				String walletResp= reChargeByTxnCode(userId,walletId,dbtAmout,resptxnId,dmtTxnID,ipAddress,aggreatorId,userAgent,134);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| bhartipay agent wallet debit  else block"+walletResp);
				if("1000".equalsIgnoreCase(walletResp)) {
					statusFlag=true;
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"debitWalletForBBPSTransacitons()| bhartipay agent wallet debited ok  else block"+walletResp);
					
				}
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,walletId, userId, "", "" ,"debitWalletForBBPSTransacitons()| "+e.getMessage());
			e.printStackTrace();
		}finally {
			session.close();
		}
		return statusFlag;
	}
	
	@Override
	public boolean creditWalletForBBPSTransacitons(String userId, String walletId, String aggreatorId, String dmtTxnID,double dbtAmout,String resptxnId, String userAgent, String ipAddress) {
		// TODO Auto-generated method stub
		boolean statusFlag=false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"creditWalletForBBPSTransacitons()|Try to save in wallettransaction");
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		TransactionBean txnBean = new TransactionBean();

		try {
			WalletMastBean aggWallet = (WalletMastBean)session.get(WalletMastBean.class, aggreatorId);
			if(aggWallet!=null && "1".equalsIgnoreCase(aggWallet.getWhiteLabel()) && aggWallet.getCashDepositeWallet()!=null) {
				WalletMastBean aggCashWallet=(WalletMastBean)session.get(WalletMastBean.class, aggWallet.getCashDepositeWallet());
				String resp= settlementByTxnCode(userId,aggCashWallet.getWalletid(),dbtAmout,dmtTxnID,ipAddress,resptxnId,aggreatorId,userAgent,29);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"creditWalletForBBPSTransacitons()|Try to save in wallettransaction resp"+resp);
				if("1000".equalsIgnoreCase(resp)) {
					if("1000".equalsIgnoreCase(settlementByTxnCode(userId,walletId,dbtAmout,dmtTxnID,ipAddress,resptxnId,aggreatorId,userAgent,135))) {
						statusFlag=true;
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"creditWalletForBBPSTransacitons()|Try to save in wallettransaction statusFlag "+statusFlag);
					}
				}
			}else{
				if("1000".equalsIgnoreCase(settlementByTxnCode(userId,walletId,dbtAmout,dmtTxnID,ipAddress,resptxnId,aggreatorId,userAgent,135))) {
					statusFlag=true;
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggreatorId,walletId, userId,"" ,"creditWalletForBBPSTransacitons()|Try to save in wallettransaction statusFlag "+statusFlag);
				}
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,walletId, userId, "", "" ,"creditWalletForBBPSTransacitons()| "+e.getMessage());
			e.printStackTrace();
		}finally {
			session.close();
		}
		return statusFlag;
	}
	
	public void updateRechargeStatus(String reqid, String status, String remark,String balance, String mn, String field1,String ec) {
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reqid,status, mn, "", "" ,"updateRechargeStatus");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reqid,status,remark,mn,ec ,"updateRechargeStatus serviceimpl callback url");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),status,remark, balance,mn,field1,"updateRechargeStatus serviceImpl callback url");
		
				factory=DBUtil.getSessionFactory();
				Session session = factory.openSession();
				Transaction transaction = session.beginTransaction();
				try {
				RechargeTxnBean rechargeBean =(RechargeTxnBean) session.get(RechargeTxnBean.class, reqid);
				if(rechargeBean != null) {
				if("Success".equalsIgnoreCase(status)) {
					rechargeBean.setStatus("Success");
				}else if("Failed".equalsIgnoreCase(status)) {
					rechargeBean.setStatus("Failed");
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reqid,status,remark,mn,ec ,"updateRechargeStatus callback url status "+rechargeBean.getStatus());
						
				if((!"Success".equalsIgnoreCase(rechargeBean.getStatus())) && (!"Failed".equalsIgnoreCase(rechargeBean.getStatus())))
				session.saveOrUpdate(rechargeBean);
				transaction.commit();
				}
				
				} catch (HibernateException e) {
					e.printStackTrace();
				}finally {
					session.close();
				}
				
	}
	

	public void updateRechargeStatusPrev(String txid, String status, String opid) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),txid,status, opid, "", "" ,"updateRechargeStatus");
		
				factory=DBUtil.getSessionFactory();
				Session session = factory.openSession();
				Transaction transaction = session.beginTransaction();
				try {
				RechargeTxnBean rechargeBean =(RechargeTxnBean) session.get(RechargeTxnBean.class, txid);
				if(rechargeBean != null) {
				if("Success".equalsIgnoreCase(status)) {
					rechargeBean.setStatus("Success");
				}else if("Failed".equalsIgnoreCase(status)) {
					rechargeBean.setStatus("Failed");
				}
				if((!"Success".equalsIgnoreCase(rechargeBean.getStatus())) && (!"Failed".equalsIgnoreCase(rechargeBean.getStatus())))
				session.saveOrUpdate(rechargeBean);
				transaction.commit();
				}
				
				} catch (HibernateException e) {
					e.printStackTrace();
				}finally {
					session.close();
				}
				
	}

	
	// change by mani
	
	public Map<String,String> leanAccountDetails(String customerId){

        System.out.println("CustomerID "+customerId);
		List agentList=new ArrayList();
		Map<String,String> detailMap=new HashMap<String ,String>();
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	     Transaction transaction = session.beginTransaction();
		try {
			String sqlquery="select id,name,walletid,aggreatorid,mobileno from walletmast where id='"+customerId.trim()+"'";
			Query query=session.createSQLQuery(sqlquery);
		    agentList = query.list();
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),customerId,"","","","" ,"leanAccountDetails()"+agentList.size());
			Object[] obj=(Object[])agentList.get(0);
			
			 detailMap.put("agentId",String.valueOf(obj[0]) );
			 detailMap.put("userName",String.valueOf(obj[1]));
			 detailMap.put("walletId",String.valueOf(obj[2]));
			 detailMap.put("aggregatorId",String.valueOf(obj[3]));
			 detailMap.put("mobileNo",String.valueOf(obj[4]));
			 }
			
		catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","leanAccountDetails()","checking leanAccountDetails error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","leanAccountDetails()","stop excution leanAccountDetails return"+agentList.size());
		return detailMap;
    }
	
	
	
	public List leanAccountUtility(String aggId){

    	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","" ,"leanAccountUtility()");
    	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "", "","method stDate");
    	//logger.info("start excution ===================== method showPassboo(walletid)"+walletid);
    	//logger.info("start excution ===================== method stDate"+stDate);
		System.out.println("---------------Lean Account");
    	
    	List userList=new ArrayList();
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
		try {
			//Query query = session.createQuery("select id,name from WalletMastBean where aggreatorid=:aggreatorid");
			
			//Query query=session.createSQLQuery("select distinct id,name from walletmast where aggreatorid=:aggreatorid and usertype=:usertype ");
			
			//String sqlquery="select distinct id,concat(id,'---',name) from walletmast where aggreatorid='"+aggId+"' and (usertype='3' or usertype='2' )";
			
			String sqlquery="select id,name from walletmast where aggreatorid= '"+aggId+"'";
			Query query=session.createSQLQuery(sqlquery);
			
			//query.setString("aggreatorid", aggId);
		//	query.setInteger("usertype", 2);
			userList = query.list();
		    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","","","" ,"leanAccountUtility()"+userList.size());
			if (userList != null && userList.size() != 0) {
			//	walletid=	(String) list.get(0);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","", "","leanAccountUtility()","Walletid :"+ aggId);
				//logger.info("==============Wallet Id=============================="+walletid);
			}
			 
			 }
			
		catch(Exception e){
			e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","leanAccountUtility()","checking leanAccountUtility error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",aggId,"", "","leanAccountUtility()","stop excution leanAccountUtility return"+userList.size());
		//logger.info("stop excution ********************************getTrxdtl******* return**********"+passbook.size());  
	
		return userList;
    }

   
    
    public LeanAccount saveLeanAccount(LeanAccount leanAccount)
    {
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    String userId = leanAccount.getUserId().trim();
  	    try
    	{
    		String sqlquery="select * from leanAmount where userid= '"+userId+"'";
			Query query=session.createSQLQuery(sqlquery);
			List list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","saveLeanAccount()","stop excution saveLeanAccount return"+list.size());
			  
		    
			if(!list.isEmpty() && list.size()>0)
			{
				System.out.println("Record already exist");
				
			}else
			{
				leanAccount.setStatus(true);
				session.save(leanAccount);
				transaction.commit();
				
				System.out.println("Record saved");
				
			}
			
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    		transaction.rollback();
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","saveLeanAccount()","stop excution saveLeanAccount return"+e.getMessage()+"   "+e);
    		logger.debug("saving lean amount*****************error************** " + e.getMessage(), e);  
    	}finally
    	{
    		session.close();
    	}
  	  logger.info("stop excution ********************************saveLeanAccount");  
     return leanAccount;
   }
    
    
    
    public LeanAccount updateLeanAmount(LeanAccount leanAccount)
    {
		System.out.println(leanAccount.getAmount()+"    3333  "+leanAccount.getUserId());
    	factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    Boolean status=false;
    	try
    	{
    		String queryStr = "update LeanAccount  set amount='"+leanAccount.getAmount()+"' where userId=:userId";
	        Query query = session.createQuery(queryStr);
	        query.setParameter("userId", leanAccount.getUserId().trim());

	        int count = query.executeUpdate();
	        transaction.commit();
			System.out.println("COUNT  "+count);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",leanAccount.getUserId(),"", "","updateLeanAmount()","excution updateLeanAmount "+count);
    	 
    	}catch(Exception e)
    	{
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",leanAccount.getUserId(),"", "","updateLeanAmount()","stop excution updateLeanAmount return"+e.getMessage()+"   "+e);
    		logger.debug("updateLeanAmount*****************error************** " + e.getMessage(), e);  
    		e.printStackTrace();
    		transaction.rollback();
    	}finally
    	{
    		session.close();
    	}
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",leanAccount.getUserId(),"", "","updateLeanAmount()","stop excution updateLeanAmount");
		logger.debug("updateLeanAmount *****************error************** " );  
    	return leanAccount;
    }
    
    
    public LeanAccount deleteRecord(String userId)
    {
    	
    	factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    LeanAccount lean=new LeanAccount();
  	    try
    	{
  	    	String queryStr = "delete from LeanAccount where userId=:uId";
  	        Query query = session.createQuery(queryStr);
  	        query.setParameter("uId", userId);
  	        int count = query.executeUpdate();
  	        transaction.commit();
  	      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","deleteRecord()","stop excution deleteRecord "+count);
  		
        	System.out.println("COUNT "+count);
    	}catch(Exception e)
    	{
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","deleteRecord()","stop excution deleteRecord return"+e.getMessage()+"   "+e);
    		logger.debug("deleteRecord*****************error************** " + e.getMessage(), e);  
    		e.printStackTrace();
    		transaction.rollback();
    	}finally
    	{
    		session.close();
    	}
  	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","deleteRecord()","stop excution deleteRecord ");
		
    	return lean;
    }
    
    
    
    public LeanAccount findLeanRecord(String userId,String aggId)
    {
    	System.out.println("userID  "+userId);
    	factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
  	    Transaction transaction = session.beginTransaction();
  	    LeanAccount lean= new LeanAccount();
  	    try
    	{
    		String sqlquery="select * from leanAmount where userid= '"+userId+"' and aggregatorId= '"+aggId+"' ";
			Query query=session.createSQLQuery(sqlquery);
			List list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","findLeanRecord()"," excution findLeanRecord "+list.size());
    		
            if(!list.isEmpty() && list.size()>0)
			{
				System.out.println("Record found");
				lean.setStatus(true);
			}else
			{
				lean.setStatus(false);
				System.out.println("Record not found");
				
			}
			
    	}catch(Exception e)
    	{
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","findLeanRecord()","stop excution findLeanRecord return"+e.getMessage()+"   "+e);
    		logger.debug("findLeanRecord*****************error************** " + e.getMessage(), e);  
    		
    		e.printStackTrace();
    		transaction.rollback();
    	}finally
    	{
    		session.close();
    	}
    	
     return lean;
   }
    
//    vrechargeid varchar(20),vgeantid varchar(20),vaggreatorid varchar(20),voperator varchar(200),damount double(12,2),vtype varchar(10))
    public void processRechargeCommission(String rechargeId, String agentId, String aggregatorId, String operator, double amount, String type) {
		
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"processRechargeCommission",rechargeId,agentId,aggregatorId,operator,""+amount);

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction= session.beginTransaction();

		try {
			
			Query query = session.createSQLQuery("call processRechargeCommission(:rechargeId,:agentId,:aggregatorid,:operator,:amount,:type)");
			
			query.setParameter("rechargeId",rechargeId);
			query.setParameter("agentId", agentId);
			query.setParameter("aggregatorid", aggregatorId);
			query.setParameter("operator", operator);
			query.setParameter("amount", amount);
			query.setParameter("type", type);
			
			query.executeUpdate();
			transaction.commit();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"processRechargeCommission",rechargeId,agentId,aggregatorId,operator,""+amount);
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"Problem processRechargeCommission",rechargeId,agentId,aggregatorId,operator,""+amount);
				transaction.rollback();
		} finally {
			session.close();
		}
		
	}
	
	
    

	public List<WalletMastBean> accountDetail(String customerId){

		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<WalletMastBean> walletMast=new ArrayList<WalletMastBean>() ;
  	    Transaction transaction = session.beginTransaction();
		try {
		
			Query query = session.createQuery("from WalletMastBean where id=:agentId");
            query.setString("agentId", customerId);
            walletMast = query.list();
			 }
	    	catch(Exception e){
		      e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",customerId,"", "","leanAccountDetails()","checking leanAccountDetails error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		return walletMast;
    }

	
	
	public WalletMastBean getAgnDtlByUserIdCMENew(String mobileNo,String aggreatorid,String distributorId,String usertype,String userId){

		String walletid="1001";
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"getAgnDtlByUserIdCME()"+"|"+mobileNo);
 		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		     Transaction transaction = session.beginTransaction();
		     WalletMastBean walletMastBean=null;
		try {
			 WalletMastBean mm = (WalletMastBean)session.get(WalletMastBean.class, userId); 
			 if(mm!=null) {
			 Query query = session.createQuery("from WalletMastBean where mobileno=:userid and  aggreatorid=:aggreatorid and id=:uid and usertype=:type and userstatus in ('A') ");
			 query.setString("uid", userId);
			 query.setString("type",""+mm.getUsertype());
			 
			query.setString("userid", mobileNo);
			query.setString("aggreatorid", aggreatorid);
			List list = query.list();
			if (list != null && list.size() != 0) {
				walletMastBean=	(WalletMastBean) list.get(0);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"Wallet Id getName"+walletMastBean.getName());
 			 }
		   }
			
		} catch (Exception e) {
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "","" ,"checking id fail from getAgnDtlByUserIdCME"+e.getMessage()+" "+e);
 		} finally {
			session.close();
		}
		return walletMastBean;
	}

	
	@Override
	public AddBankAccount addAccountSelf(AddBankAccount account) {
		
	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		transaction = session.beginTransaction();
		String userId = account.getUserId();
		if(account!=null) {
		try {
		    String sqlquery="select * from addbankaccount where userId= '"+userId+"'";
			Query query=session.createSQLQuery(sqlquery);
			List list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",userId,"", "","saveAccount()"," return"+list.size());
		   if(list.size()>5)
			{
				System.out.println("You have added maximum account");
				account.setStatus(false);
				account.setAccountNumber("5");
			}else
			{
				 String sqlqueryAc="select * from addbankaccount where userId= '"+userId+"' and accountnumber= '"+account.getAccountNumber()+"' ";
				 Query queryAc=session.createSQLQuery(sqlqueryAc);
				 List listAc = queryAc.list();
				if(listAc.size()>0) 
				{ 
					System.out.println("Account already exist");
					account.setStatus(false);
					account.setAccountNumber("0");
				}else {	
				account.setComment("PENDING");
				account.setStatusCode(0);	
				account.setStatus(false);
				session.save(account);
				transaction.commit();
				account.setAccountNumber("1");
				System.out.println("Record saved");
			    }
		  }
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		}
		return account;
    }		

	
	public List<AddBankAccount> getAllAccount(AddBankAccount bank){

		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AddBankAccount> walletMast=new ArrayList<AddBankAccount>() ;
  	    Transaction transaction = session.beginTransaction();
		try {
		
			Query query = session.createQuery("from AddBankAccount where userId=:uid");
            query.setString("uid", bank.getUserId());
            walletMast = query.list();
			 }
	    	catch(Exception e){
		      e.printStackTrace();
			  transaction.rollback();
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",bank.getUserId(),"", "","AddBankAccount()","checking AddBankAccount error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			 }
			 finally{
			  session.close();
		 }
		return walletMast;
    }

	@Override
	public AddBankAccount updateAccountSelf(AddBankAccount account) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {
			AddBankAccount acc = (AddBankAccount)session.get(AddBankAccount.class, account.getId());
			if(acc!=null)
			{
				acc.setComment(account.getComment());
				
				if("Accept".equalsIgnoreCase(account.getAccountNumber())) 
				{	acc.setStatus(true);
				acc.setStatusCode(1);
				}else if("Reject".equalsIgnoreCase(account.getAccountNumber())) 
				{	acc.setStatus(false);
				acc.setStatusCode(2);
				}
				session.saveOrUpdate(acc);
				transaction.commit();
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
		}finally {
			session.close();
		}
		
		
		return account;
	}

	@Override
	public List<AddBankAccount> getPendingAccount() {
		
		factory=DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AddBankAccount> list=new ArrayList<AddBankAccount>() ;
        try {
         Query query = session.createQuery("FROM AddBankAccount where status=false");
         list=query.list();	
         
        }catch(Exception e)
        {
        e.printStackTrace();	
        }
        
		return list;
	}

	
	
	@Override
	public CmsResponse preCmsDebitNotification(CmsBean cms) {

		 factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		 Transaction trans = session.beginTransaction();
		
		 Gson gson = new Gson();
		 String jsonText=gson.toJson(cms);
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",""," ","" ,"preCmsDebitNotification() "+jsonText);
			
		 CmsResponse cm=new CmsResponse();
			try {
				if(cms!=null)
				{ 
					cms.setTxnDate(new Timestamp(new Date().getTime()));
					session.save(cms);
					trans.commit();
					
				 WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, cms.getAgentId());
					if(mast!=null)
					{
					cms.setAggregatorId(mast.getAggreatorid());
					double amt = Double.parseDouble(cms.getAmount());
					
					transaction=session.beginTransaction();	
					String queryNext = "SELECT nextval(:ad,:aggregatorid)";
					SQLQuery nextQuery = session.createSQLQuery(queryNext);
					nextQuery.setParameter("aggregatorid", mast.getAggreatorid());
					nextQuery.setParameter("ad", "REVOKE");
			        Object nextObj = nextQuery.uniqueResult();
			        String txnId=""+nextObj;	
			        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","txnId "+txnId,""," ","" ,"preCmsDebitNotification() "+jsonText);
					
					String str = "CALL debitwallet('" + mast.getAggreatorid() + "','" + mast.getWalletid() + "'," + amt  + ",'CMS Amount','"+ txnId + "',191)";
			        System.out.println(" str   "+str);
			        Query debitQuery = session.createSQLQuery(str);
			        debitQuery.executeUpdate();
			        transaction.commit();
			        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","Success ","" ,"postCmsDebitNotification() "+cms);
					        
			        cm.setStatus("00");
			        cm.setMessage("Success");
					}else
					{
					cm.setStatus("01");
			        cm.setMessage("Failed");
			        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","Failed ","" ,"postCmsDebitNotification() "+cms);
					
					}
					
				}
				
			}catch(Exception e)
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","Problem  in ","" ,"preCmsDebitNotification() "+e.getMessage());
			    cm.setStatus("02");
		        cm.setMessage("Error");
				e.printStackTrace();
			}finally {
				session.close();
			}
		 return cm;	
	}


	@Override
	public CmsResponse postCmsDebitNotification(CmsBean cms) {

		 Gson gson = new Gson();
		 String jsonText=gson.toJson(cms);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","jsonText  "+jsonText,"","" ,"postCmsDebitNotification() "+cms.getTxnId());
		
		 factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		 transaction=session.beginTransaction();
		 CmsResponse cm=new CmsResponse();
			try {
				if(cms!=null)
				{
				CmsBean cmm = (CmsBean)session.get(CmsBean.class, cms.getTxnId());	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"postCmsDebitNotification() "+cmm);
				
					if(cmm!=null) {
					cmm.setStatus(cms.getStatus());
					cmm.setReferenceId(cms.getReferenceId());
					session.saveOrUpdate(cmm);
					transaction.commit();
			    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","Success ","" ,"postCmsDebitNotification() "+cmm);
						
					 cm.setStatus("00");
				     cm.setMessage("Success");
					}else
					{
						 cm.setStatus("01");
					     cm.setMessage("Failed");	
			   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","Failed ","" ,"postCmsDebitNotification() "+cmm);
							
					}
				}else
				{
					 cm.setStatus("03");
				     cm.setMessage("Failed");
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","Problem in ","","" ,"postCmsDebitNotification() "+e.getStackTrace());
			    cm.setStatus("02");
		        cm.setMessage("Error");
			}finally {
				session.close();
			}
		 return cm;	
	}


	@Override
	public CmsEnquiryResponse searchCmsTxn(CmsEnquiryResponse cms) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"searchCmsTxn()"+cms.getClientTransactionId());
		
		 factory = DBUtil.getSessionFactory();
		 session = factory.openSession();
		// transaction=session.beginTransaction();
		 CmsEnquiryResponse resp =new CmsEnquiryResponse();
		try {
			
			JSONObject jsonText = new JSONObject();
			jsonText.put("clientTransactionId", cms.getClientTransactionId());

			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			client.addFilter(new HTTPBasicAuthFilter("BHARTIPAY SERVICES PRIVATE LIMITED-ALI222714", "vqnz7kgvtb"));
			WebResource webResource = client.resource("https://services.bankit.in:8443/AEPSAPI/customer/searchtxn");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
					jsonText.toString());

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			Gson gson = new Gson();
			String output = response.getEntity(String.class);
			resp = gson.fromJson(output, CmsEnquiryResponse.class);
		
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","" ,"searchCmsTxn()"+resp);
				
		}catch(Exception e)
		{
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",cms.getClientTransactionId(),"", "","searchtxn()","checking CMS error"+ e.getMessage()+" "+e);
			e.printStackTrace();
		}finally {
			session.close();
		}
		
		
		return resp;
	}

	
	
	
	
	
// BBPS settlement changes
	/*
	public String bbpsSettlement(String userId,String walletId,double amount,String settTxnId ,String aggreatorid){
		return bbpsWalletSettlement(userId,walletId,amount,settTxnId,aggreatorid,6);
	}
	
	public String bbpsWlRechargeSettlement(String userId,String walletId,double amount,String settTxnId, String aggreatorid ){
		return bbpsWalletSettlement(userId,walletId,amount,settTxnId, aggreatorid, 94);
	}
	
	private String bbpsWalletSettlement(String userId,String walletId,double amount,String settTxnId,String aggreatorid,int txnCode){

	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"bbpsWalletSettlement()"+"|");
	   // logger.info("start excution ===================== method settlement(userId,walletId, amount, settTxnId, ipImei)");
		String status="1001";
		TransactionBean transactionBean=new TransactionBean();
		factory=DBUtil.getSessionFactory();
		 	Session session = factory.openSession();
		    Transaction transaction = session.beginTransaction();
		  try{
				String txnId=commanUtilDao.getTrxId("AMIW",aggreatorid);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"--------inside bbpsWalletSettlement");
				transactionBean.setTxnid(txnId);
				transactionBean.setUserid(userId);
				transactionBean.setWalletid(walletId); 
				transactionBean.setTxncode(txnCode);
				transactionBean.setTxncredit(amount);
				transactionBean.setTxndebit(0);
				transactionBean.setResult("Success");
				transactionBean.setResptxnid(settTxnId);
				transactionBean.setAggreatorid(aggreatorid);
			  
				session.save(transactionBean);
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "",settTxnId ,"--------inside bbpsWalletSettlement done---------walletid"+walletId);
		     	status="1000";
		
		  }catch(Exception e){
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "bbpsWalletSettlement()",settTxnId ,"problem in Add  monet to wallet by bbpsWalletSettlement"+e.getMessage()+" "+e);
			  //logger.debug("problem in Add  monet to wallet by settlement" + e.getMessage(), e);
		transaction.rollback();
		return "7000";
		  } finally {
		session.close();
	}
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "bbpsWalletSettlement()",settTxnId ,"Inside addMoney return"+status);
		  //logger.info("Inside ====================addMoney=== return===="+status);
	return status;
		
	}
*/
	
/*
    public void processBbpsCommission(String rechargeId, String agentId, String aggregatorId, String operator, double amount, String type) {
		
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"processBbpsCommission",rechargeId,agentId,aggregatorId,operator,""+amount);

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction= session.beginTransaction();

		try {
			
			Query query = session.createSQLQuery("call processRechargeCommission(:rechargeId,:agentId,:aggregatorid,:operator,:amount,:type)");
			
			query.setParameter("rechargeId",rechargeId);
			query.setParameter("agentId", agentId);
			query.setParameter("aggregatorid", aggregatorId);
			query.setParameter("operator", operator);
			query.setParameter("amount", amount);
			query.setParameter("type", type);
			
			query.executeUpdate();
			transaction.commit();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"processBbpsCommission",rechargeId,agentId,aggregatorId,operator,""+amount);
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"Problem processBbpsCommission",rechargeId,agentId,aggregatorId,operator,""+amount);
				transaction.rollback();
		} finally {
			session.close();
		}
		
	}
	
 */	
	
	
}
