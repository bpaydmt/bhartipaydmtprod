package com.bhartipay.transaction.persistence;

import java.util.List;
import java.util.Map;

import com.bhartipay.airTravel.bean.FlightTxnBean;
import com.bhartipay.airTravel.bean.booking.Response_;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.CmsBean;
import com.bhartipay.lean.bean.CmsEnquiryResponse;
import com.bhartipay.lean.bean.CmsResponse;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.lean.bean.PgReturnResponse;
import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.request.BillRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;
import com.bhartipay.recharge.response.Result;
import com.bhartipay.transaction.bean.AppResponse;
import com.bhartipay.transaction.bean.AskMoneyBean;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.bean.MoneyRequestBean;
import com.bhartipay.transaction.bean.P2MTransactionBean;
import com.bhartipay.transaction.bean.PGAggregatorBean;
import com.bhartipay.transaction.bean.PGPayeeBean;
import com.bhartipay.transaction.bean.PassbookBean;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.PrePaidCardResponse;
import com.bhartipay.transaction.bean.PrePaidCardTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.RefundMastBean;
import com.bhartipay.transaction.bean.RefundTransactionBean;
import com.bhartipay.transaction.bean.RemainingLimitBean;
import com.bhartipay.transaction.bean.SMSCallBackBean;
import com.bhartipay.transaction.bean.SurchargeBean;
import com.bhartipay.transaction.bean.TerminalOnBoardingCallBackBean;
import com.bhartipay.transaction.bean.TerminalStatusCallBackBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.bean.TransactionNotificationCallBackBean;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.bean.WalletToBankTxnMast;
import com.bhartipay.user.bean.SmartCardBean;
import com.bhartipay.user.bean.UserProfileBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.ResponseResult;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.matm.MATMLedger;
import com.google.gson.JsonElement;

public interface TransactionDao {
	
	 public double getWalletBalance(String walletId);
	 
	// public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion );
	// public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion ,String payTxnid);
	public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion ,String payTxnid,String aggreatorid,String imei,String agent);
	public String updatePGTrx(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei);
	public String updatePGTrxTest(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei,String mode,String card);

	public String savePgReturnResponse(PgReturnResponse res);
	
	 public UserProfileBean profilebytxnId(String txnId);
	 public String walletToWalletTransfer(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent,String checkSum);
	 public List showPassbook(String walletid,String stDate,String endDate);
	 public String reCharge(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent);
	 public String walletToAccount(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge);
	 public String askMoney(String userid,String reqwalletId,String resmobile,double amount,String ipImei,String aggreatorid,String agent);
	 public String askMoneyUpdate(String userId,String trxId,String txnstatus,String ipImei,String msgId,String aggreatorid,String agent);
	 public WalletToBankTxnMast tansferInAccount(WalletToBankTxnMast walletToBankTxnMast) ;
	 //public String walletToMerchent(String userId,String payeeWalletid,double amount,String mid,String trnsTxnId,String ipImei);
	 public String walletToMerchent(String userId,String payeeWalletid,double amount,String mid,String trnsTxnId,String ipImei,String aggreatorid,String agent);
	 //public String payForPayment(RechargeTxnBean rechargeTxnBean);
	 public List showOrder(String walletid,String stDate,String endDate);
	 public PGPayeeBean profilebyPGtxnId(String txnId);
	 public RechargeTxnBean payForPayment(RechargeTxnBean rechargeTxnBean) ;
	 
	 public Result getRechargeStatusNew(Result result);
	 
	 public BillerInfoResponse getBillerInfo(BillInfoRequest billRequst, String ipimei, String agetCode);
	 public MoneyRequestBean moneyRequest(CashDepositMast requestBean);
	// public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId);
	 public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId,String aggreatorid);
	 public String updatePGTrx(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status);
	 // for android
	 public String updatePGTrx(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status,String mode,String card);
	 
	 public P2MTransactionBean P2MTransaction(P2MTransactionBean p2MTransactionBean);
	 
	 public String getNameByUserId(String mobileEmail,String aggreatorid);
	 public String getWalletIdByUserId(String mobileEmail,String aggreatorid);
	 
	 public WalletToBankTxnMast dmtTransfer(WalletToBankTxnMast walletToBankTxnMast);
	 public void dmtTransferUpdate(UserDetails  userDetails,String dmtResponse) ;
	 
	 public WalletToBankTxnMast dmtReinitiateTransfer(WalletToBankTxnMast walletToBankTxnMast);
	 public PrePaidCardResponse prePaidCardPaymentAutorization (PrePaidCardTxnBean prePaidCardTxnBean);
	 
	 
	 
	 
	 
	 public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast);
	 public List<CashDepositMast> getCashDepositReqByAggId(String aggreatorId,String type);
	 public List<CashDepositMast> getCashDepositReqByUserId(String userId,String stDate,String endDate);
	 public boolean acceptCashDeposit(String depositId,String remarks,String ipIemi,String agent);
	 public String acceptCashDepositByDist(String depositId,String remarks,String ipIemi,String agent);
	 public boolean rejectCashDeposit(String depositId,String remarks);
	 
	 public RefundMastBean requestRefund(RefundMastBean refundMastBean);
	 public List<RefundMastBean> getRefundReqByAggId(String aggreatorId);
	 public boolean rejectRefundReq(String refundId);
	 public boolean acceptRefundReq(String refundId,String ipIemi,String agent);
	 public List<RefundTransactionBean> processRefund(String refundId,String ipIemi,String txnIds);
	 public String cashIn(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent,double surCharge,String checkSum);
	 public AskMoneyBean getCashOut(String userid,String reqwalletId,String resmobile,double amount,String ipImei,String aggreatorid,String agent,double surCharge);
	 public AskMoneyBean cashOutUpdate(AskMoneyBean askMoneyBean) ;
	 public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean);
	 public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean);
	 public SurchargeBean calculateDMTSurcharge(SurchargeBean surchargeBean);
	 public PrePaidCardResponse prePaidCardReversal(PrePaidCardTxnBean prePaidCardTxnBean);
	 
	 
	 public WalletMastBean getAgnDtlByUserId(String mobileNo,String aggreatorid);
	 
	 public String cmeToAgent(String userId,String sendWalletid,String recMobile,double amount,String ipImei,String aggreatorid,String agent);
	 
	 public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2CMoneyTxnMast) ;
	 
	 public List<CashDepositMast> getCashDepositReportByAggId(String aggreatorId,String stDate,String endDate,String type);
	 
	 
	 
	 public RechargeTxnBean saveInitialRechargeTxn(RechargeTxnBean rechargeTxnBean);
	 
	 
	 public String billPayment(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent,int txncode);
	 public String settlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent);
	 public String wlRechargeSettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent);

	 public List showPassbookById(String id,String stDate,String endDate,String aggreatorid);
	 public double getWalletBalancebyId(String agentId);
	 
	 
	
	 
	//  public List showPassbookById(String id,String stDate,String endDate);
	  public PassbookBean showTxnByTxnID(String txnId);
	  
	  public TransactionBean showTxnByRespTxnID(String resptxnId,int txnCode);
	  
	  public double getCashBackWalletBalance(String walletId);
	  public double getCashBackWalletBalancebyId(String agentId);
	  
	  public String cashBacksettlement(String userId,String walletId,double amount,String settTxnId,String ipImei,String mobileNo,String aggreatorid,String agent);
	  
	  public List showCashBackPassbook(String walletid,String stDate,String endDate);
	  
	  public List showCashBackPassbookById(String id,String stDate,String endDate);
	  
	  public PaymentGatewayTxnBean viewPGTrxFlight(String txnId);
	  
	  
	 // public String savePGTrxFlight(double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId,String aggreatorid);
	 // public String updatePGTrxFlight(String txnId,String pgtxn,double trxAmount,String status);
	  
	 
	  
	  public List<CashDepositMast> getCDAReqByAggId(String aggreatorId,String type);
	  public boolean approvedCashDeposit(String depositId,String remarkApprover,String ipIemi,String agent);
	  public boolean rejectApproverCashDeposit(String depositId,String remarkApprover);
	  
	  
	  public String payForFlightUpdate(FlightTxnBean flightTxnBean,Response_ bookFlightResp, int successCount );
	  public String payForFlight(FlightTxnBean flightTxnBean);
	  
	  public String savePGTrxFlight(String tripId,double trxAmount,String walletId, String mobileNo, String trxReason ,String emailId,String appName ,String imeiIP,String userId,String aggreatorid);
	  
	  public String updatePGTrxFlight(String hash,String responseparam,String txnId,String pgtxn,double trxAmount,String status);
	  
	  public RemainingLimitBean getRemainingLimit(String walletId);
	  
	  public String billPaymentFromCashBack(String userId,String payeeWalletid,double amount,String rechageNumber,String rechargeTxnId,String ipImei,String aggreatorid,String agent,int txncode);
	
	  public String walletToAccountSurchargeDist(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distAmt,String distributerId,double supDistSurcharge,String supDistId,double agentRefund,double dtdsApply);
	  
	  public String walletToAggregatorTransfer(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent,double aggReturnVal, double dtdsApply);

	  public String walletToAggregatorTransferExt(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent);

	  public String walletToAccountSurchargeVerifyDist(String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String aggreatorid,String agent,double surCharge,double distAmt,String distributerId,double supDistSurcharge,String supDistId);
	  
	  public String walletToAggregatorTransferVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent);
	  
	  public String walletToAggregatorRefundVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent);
	  
	  public String walletToAggregatorRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent,double aggReturnVal, double dtdsApply);

	  public String walletToAggregatorRefundExt(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String agent);
	 /* public String walletToSuperDistributorTransfer(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent);

	  public String walletToSuperDistributorTransferRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent);

	  public String walletToSuperDistributorTransferVerify(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent);

	  public String walletToSuperDistributorTransferVerifyRefund(String respTxnId,String userId,double amount,double surcharge,String ipImei,String aggreatorid,String superDistributorId,String agent);
*/
	  public List showCashDeposite(String walletid,String stDate,String endDate);

	  public RechargeTxnBean rechargePayment(RechargeTxnBean rechargeTxnBean);
	  
	  public RechargeTxnBean getRechargeStatus(String aggregatorId,String rechargeRespId);
	  
	  public double getCashDepositWalletBalance(String aggrigatorId);
	  
	  public double getAverageBalance(String aggrigatorId);
	  
	  public List getRechargePendingStatus(String aggreatorId,Long period);

	  public AEPSLedger saveAepsRequest(AEPSLedger aepsLedger,String ipimei,String userAgent);
	  public AEPSLedger getAepsLedger(AEPSLedger aepsLedger,String ipimei,String userAgent);
	  
	  public MATMLedger saveMatmRequest(MATMLedger matmLedger,String ipimei,String userAgent);
	  
	  
	  
	  public AEPSLedger updateAepsRequest(AEPSLedger aepsLedger,String ipimei,String userAgent);
	  
	  //public String savePGTrx(double trxAmount,String walletId, String mobileNo, String trxReasion ,String payTxnid,String aggreatorid,String imei,String agent);
		public PGAggregatorBean getPGAGGId(String aggreatorid);
		//public String updatePGTrx(String hash,String responseparam,String userId,String txnId,String pgtxn,double trxAmount,String status,String ipImei);

		public RechargeTxnBean getRechargeTxnBean(String rechargeId);

		public RechargeTxnBean updateRechargeTxnBean(String rechargeId,String rechargeStatus);

		public List<CashDepositMast> getCashDepositReqByUserIdPending(String userId, String stDate, String endDate);
		
		public List subAggregatorUtility(String aggId);
		
		public List superDistributerListChange(String aggId);
		
		public List utilProfileChange(String aggId);
		
		public List distributorList(String aggId);
		
		public String changeDistributor(String id,String aggId);
		
		public String changeSuperDistributor(String id,String aggId);
		
		public Map<String,String>  subAggregatorUtilityChange(String customerId);
		
		
		public Map<String,String>  superDistributorUtilityChange(String customerId);
		
		public Map<String,String> utilProfileAgentDetails(String customerId);
		public String utilChangeAgentProfile(String customerId,String agentName,String agentMobile,String agentMail);

		public void callCreditWallet(RechargeTxnBean rechargeTxnBean);
		
		public void updateRechargeStatus(String reqid, String status, String remark,String balance, String mn, String field1,String ec);
		
		public void updateRechargeStatusPrev(String txid, String status, String opid);
		
		public boolean debitWalletForBBPSTransacitons(String userId, String walletId, String aggreatorId, String dmtTxnID,double dbtAmout,String resptxnId, String userAgent, String ipAddress);

		public boolean creditWalletForBBPSTransacitons(String userId, String walletId, String aggreatorId, String dmtTxnID,double dbtAmout,String resptxnId, String userAgent, String ipAddress);
	  

    // change by mani
		
//		leanAccountUtility
		
		public List leanAccountUtility(String aggId);	
		public Map<String,String> leanAccountDetails(String customerId);
		public LeanAccount saveLeanAccount(LeanAccount lean);
		public LeanAccount updateLeanAmount(LeanAccount lean);
		public LeanAccount deleteRecord(String userId);
		public LeanAccount findLeanRecord(String agentId,String aggId);
		public String txnUpdate(TxnInputBean txnInputBean);

		public List getAmountPopup(String txnIdPassed); 
		public List<WalletMastBean> accountDetail(String customerId);
		
		public AEPSLedger saveAepsRequestApi(AEPSLedger aepsLedger,String ipimei,String userAgent);
		 
	   // public String bbpsSettlement(String userId,String walletId,double amount,String settTxnId ,String aggreatorid);
	   // public String bbpsWlRechargeSettlement(String userId,String walletId,double amount,String settTxnId,String aggreatorid);
	
		public AddBankAccount addAccountSelf(AddBankAccount account); 
		public List<AddBankAccount> getAllAccount(AddBankAccount account);
		public AddBankAccount updateAccountSelf(AddBankAccount account); 
		public List<AddBankAccount> getPendingAccount();
		//================================ CALLBACK API ================================================
		 
 		public ResponseResult saveSMSResponseApi(SMSCallBackBean smsCallBackBean);
 		public ResponseResult saveTransactionNotificationResponseApi(TransactionNotificationCallBackBean transCallBackBean);
 		public ResponseResult saveTerminalonboardingResponseApi(TerminalOnBoardingCallBackBean terminalOnboardingBean,String authorization);
 		public ResponseResult saveTerminalstatusResponseApi(TerminalStatusCallBackBean terminalOnboardingBean,String status_id_check,String   authorization);
 		public ResponseResult saveMerchantOnboardingResponseApi(String jsonData,String authorization);
 		//================================ CALLBACK API ================================================
		public AppResponse getMerchantMid(AppResponse res);
		public CmsResponse preCmsDebitNotification(CmsBean cms); 
 		public CmsResponse postCmsDebitNotification(CmsBean cms);
 		public CmsEnquiryResponse searchCmsTxn(CmsEnquiryResponse cms);
		
}
