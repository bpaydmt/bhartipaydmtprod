package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="modocumentsinformation")
public class MODocumentsInformationBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	

	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "docid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int docid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
				
	@Column(name="name")
	private String name;

	@Column(name="type_id")
	private String type_id;

	
	@ManyToOne(targetEntity=MerchantOnboardingCallBackBean.class)  
	private MerchantOnboardingCallBackBean mobean;

	
	@OneToMany
	private List<MODocumentsInformationURLBean> urlBeanList;


	/*public MODocumentsInformationBean(Date insertedtime, String name, String type_id,
			MerchantOnboardingCallBackBean mobean) {
		super();
		this.insertedtime = insertedtime;
		this.name = name;
		this.type_id = type_id;
		this.mobean = mobean;
	}*/


	
	public int getDocid() {
		return docid;
	}


	public MODocumentsInformationBean(Date insertedtime, String name, String type_id,
			MerchantOnboardingCallBackBean mobean, List<MODocumentsInformationURLBean> urlBeanList) {
		super();
		this.insertedtime = insertedtime;
		this.name = name;
		this.type_id = type_id;
		this.mobean = mobean;
		this.urlBeanList = urlBeanList;
	}


	public List<MODocumentsInformationURLBean> getUrlBeanList() {
		return urlBeanList;
	}


	public void setUrlBeanList(List<MODocumentsInformationURLBean> urlBeanList) {
		this.urlBeanList = urlBeanList;
	}


	public void setDocid(int docid) {
		this.docid = docid;
	}


	public Date getInsertedtime() {
		return insertedtime;
	}


	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}


	public MerchantOnboardingCallBackBean getMobean() {
		return mobean;
	}


	public void setMobean(MerchantOnboardingCallBackBean mobean) {
		this.mobean = mobean;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType_id() {
		return type_id;
	}


	public void setType_id(String type_id) {
		this.type_id = type_id;
	}


	@Override
	public String toString() {
		return "MODocumentsInformationBean [insertedtime=" + insertedtime + ", name=" + name + ", type_id=" + type_id
				+ ", mobean=" + mobean + "]";
	}

	
}