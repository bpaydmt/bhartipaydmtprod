package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="terminalonboardingcallback")
public class TerminalOnBoardingCallBackBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false, name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="userid",length =30)
	private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	

	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------Other------------------
	
	@Column(name="authorization")
	private String authorization;
	
	//----------------------Other------------------

	@Column(name="merchantid")
	private String merchant_id;
	
	@Column(name="location",length =1000)
	private String location;

	
	@Column(name="address",length =1000)
	private String address;
	
	@Column(name="pincode",length =40)
	private String pincode;
	
	
	@Column(name="simnumber",length =25)
	private String sim_number;
	
	@Column(name="terminaltype")
	private String terminal_type;
	
	@Column(name="devicemodel")
	private String device_model;
	
	@Column(name="maxusagedaily")
	private int max_usage_daily;
	
	@Column(name="maxusageweekly")
	private int max_usage_weekly;
	
	@Column(name="maxusagemontly")
	private int max_usage_montly;
	
	@Column(name="velocitycheckminutes")
	private int velocity_check_minutes;
	
	@Column(name="velocitycheckcount")
	private int velocity_check_count;

	public String getAuthorization() {
		return authorization;
	}



	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getAggregatorId() {
		return aggregatorId;
	}



	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}



	public Date getInsertedtime() {
		return insertedtime;
	}



	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}



	public String getMerchant_id() {
		return merchant_id;
	}



	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPincode() {
		return pincode;
	}



	public void setPincode(String pincode) {
		this.pincode = pincode;
	}



	public String getSim_number() {
		return sim_number;
	}



	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}



	public String getTerminal_type() {
		return terminal_type;
	}



	public void setTerminal_type(String terminal_type) {
		this.terminal_type = terminal_type;
	}



	public String getDevice_model() {
		return device_model;
	}



	public void setDevice_model(String device_model) {
		this.device_model = device_model;
	}



	public int getMax_usage_daily() {
		return max_usage_daily;
	}



	public void setMax_usage_daily(int max_usage_daily) {
		this.max_usage_daily = max_usage_daily;
	}



	public int getMax_usage_weekly() {
		return max_usage_weekly;
	}



	public void setMax_usage_weekly(int max_usage_weekly) {
		this.max_usage_weekly = max_usage_weekly;
	}



	public int getMax_usage_montly() {
		return max_usage_montly;
	}



	public void setMax_usage_montly(int max_usage_montly) {
		this.max_usage_montly = max_usage_montly;
	}



	public int getVelocity_check_minutes() {
		return velocity_check_minutes;
	}



	public void setVelocity_check_minutes(int velocity_check_minutes) {
		this.velocity_check_minutes = velocity_check_minutes;
	}



	public int getVelocity_check_count() {
		return velocity_check_count;
	}



	public void setVelocity_check_count(int velocity_check_count) {
		this.velocity_check_count = velocity_check_count;
	}



	@Override
	public String toString() {
		return "TerminalOnBoardingCallBackBean [id=" + id + ", userId=" + userId + ", aggregatorId=" + aggregatorId
				+ ", insertedtime=" + insertedtime + ", merchant_id=" + merchant_id + ", location=" + location
				+ ", address=" + address + ", pincode=" + pincode + ", sim_number=" + sim_number + ", terminal_type="
				+ terminal_type + ", device_model=" + device_model + ", max_usage_daily=" + max_usage_daily
				+ ", max_usage_weekly=" + max_usage_weekly + ", max_usage_montly=" + max_usage_montly
				+ ", velocity_check_minutes=" + velocity_check_minutes + ", velocity_check_count="
				+ velocity_check_count + "]";
	}
}