package com.bhartipay.transaction.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="moneywayscredentials")
public class MoneyWaysCredentials {
	
	
	@Id
	@Column(name = "id", nullable = false)
	private String id;
		
	@Column(name = "token")
	private String token;
	
	@Column(name = "userid")
	private String userid;
	
	
	@Column(name = "password")
	private String password;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
