package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="dmtmoneyrequest")
public class MoneyRequestBean {
	
	@Id
	@Column(updatable = false, name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name = "bank", length = 40, nullable = true)
	private String bank;
	@Column(name = "txnType", length = 20, nullable = true)
	private String txnType;
	@Column(name = "amount")
	private double amount;
	@Column(name = "utrNo", length = 50, nullable = true)
	private String utrNo;
	@Column(name = "aggreatorid", length = 20, nullable = true)
	private String aggId;
	@Column(name = "walletId", length = 30, nullable = true)
	private String walletId;
	@Column(name = "senderName", length = 30, nullable = true)
	private String senderName;
	@Column(name = "ipImei", length = 20, nullable = true)
	private String ipImei;
	@Column(name = "userAgent", length = 200, nullable = true)
	private String userAgent;
	@Column(name = "requestStatus", length = 200, nullable = false)
	private String requestStatus;
	@Column(name="requestDate")
	private String requestDate;
	@Column(name="remark")
	private String remark;
	@Transient
	private String status;
	
	@Transient
	private String aggreatorid;
	
	
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
public String getIpImei() {
	return ipImei;
}
public String getUserAgent() {
	return userAgent;
}
public void setIpImei(String ipImei) {
	this.ipImei = ipImei;
}
public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
}
public String getAggId() {
	return aggId;
}
public String getWalletId() {
	return walletId;
}
public String getSenderName() {
	return senderName;
}
public void setAggId(String aggId) {
	this.aggId = aggId;
}
public void setWalletId(String walletId) {
	this.walletId = walletId;
}
public void setSenderName(String senderName) {
	this.senderName = senderName;
}
public String getBank() {
	return bank;
}
public String getTxnType() {
	return txnType;
}
public double getAmount() {
	return amount;
}
public String getUtrNo() {
	return utrNo;
}
public void setBank(String bank) {
	this.bank = bank;
}
public void setTxnType(String txnType) {
	this.txnType = txnType;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public void setUtrNo(String utrNo) {
	this.utrNo = utrNo;
}



}
