package com.bhartipay.transaction.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="refundtransaction")
public class RefundTransactionBean {
@Id
@Column(name = "id", nullable = false)	
private String	id;
@Column(name = "status")	
private String	status;
@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
private	Date txndate;
@Column(name = "remark")
private String	remark;
@Column(name = "amount")
private	double amount;
@Column(name = "servicecharge")
private	double servicecharge;
@Column(name = "agentorsenderid")
private String	agentorsenderid;
@Column(name = "refundtosenderoragent")
private String	refundtosenderoragent;
	


public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Date getTxndate() {
	return txndate;
}
public void setTxndate(Date txndate) {
	this.txndate = txndate;
}
public String getRemark() {
	return remark;
}
public void setRemark(String remark) {
	this.remark = remark;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public double getServicecharge() {
	return servicecharge;
}
public void setServicecharge(double servicecharge) {
	this.servicecharge = servicecharge;
}
public String getAgentorsenderid() {
	return agentorsenderid;
}
public void setAgentorsenderid(String agentorsenderid) {
	this.agentorsenderid = agentorsenderid;
}
public String getRefundtosenderoragent() {
	return refundtosenderoragent;
}
public void setRefundtosenderoragent(String refundtosenderoragent) {
	this.refundtosenderoragent = refundtosenderoragent;
}

}
