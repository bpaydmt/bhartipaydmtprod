package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="disputemast")
public class DisputeBean {

	@Id
	@Column(name = "disputeid", nullable = false)
	private String disputeid;
	
	@Column(name = "userid", nullable = false)
	private String userid;
	
	@Column(name = "walletid", nullable = false)
	private String walletid;
	
	@Column(name = "txnid", nullable = false)
	private String txnid;
	
	@Column(name = "reason", nullable = false)
	private String reason;
	
	@Column(name = "status", nullable = false)
	private String status;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date disputedate;

	public String getDisputeid() {
		return disputeid;
	}

	public void setDisputeid(String disputeid) {
		this.disputeid = disputeid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDisputedate() {
		return disputedate;
	}

	public void setDisputedate(Date disputedate) {
		this.disputedate = disputedate;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
