package com.bhartipay.transaction.bean;

//import java.sql.Date;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bhartipay.user.bean.WalletOtpBean;


/*@NamedNativeQueries({
	@NamedNativeQuery(
	name = "velocityCheck",
	query = "CALL velocityCheck(:walletId,:txnCode,:txnAmount)",
	resultClass = WalletOtpBean.class
	)
})*/




@Entity
@Table(name="rechargemast")
public class RechargeTxnBean {
	
	@Id
	@Column(updatable = false, name = "rechargeid", nullable = false,  length=12)
	private String rechargeId;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorid;
	
	@Column( name = "userid", length=100, nullable = true)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;

	@Column(updatable = false, name = "rechargenumber", nullable = false, length=25)
	private String rechargeNumber;
	
	@Column(updatable = false, name = "rechargetype", nullable = false, length=50)
	private String rechargeType;
	
	@Column(updatable = false, name = "rechargecircle", nullable = true, length=50)
	private String rechargeCircle;
	
	@Column(updatable = false, name = "rechargeoperator", nullable = false, length=50)
	private String rechargeOperator;
	
	@Column(name = "rechargeamt", nullable = false, length=10)
	private Double rechargeAmt;
	
	@Column( name = "coupanamt", length=10)
	private Double coupanAmt;
	
	@Column(name = "wallettxnstatus", nullable = true, length=15)
	private String wallettxnStatus;
	
	@Column(name = "status", nullable = false, length=15)
	private String status;
	
	
	@Column(name = "other", nullable = true, length=250)
	private String other;
	
	@Column(name = "txnagent", nullable = true, length=300)
	private String txnAgent;
	
	@Column(name = "ipiemi", nullable = true, length=25)
	private String ipIemi;
	
	
	@Column(name = "useragent",  length = 100)
	private String useragent;
	
	@Column(name = "useremail", nullable = true, length=100)
	private String userEmail;
	
	@Column(name = "stdcode", nullable = true, length=5)
	private String stdCode;
	
	@Column(name = "accountnumber", nullable = true, length=20)
	private String accountNumber;
	
	@Column(name = "consumerid",  length=20)
	private String consumerId;
	
	
	@Column(name = "operatortxnid", nullable = true, length=50)
	private String operatorTxnId;
	
	@Column(name = "operatorname", nullable = true, length=50)
	private String operatorName;
	
	private String mobileNumber;
	
	
	
	/*@Temporal(TemporalType.TIMESTAMP)
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txnDate;*/
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	//private Timestamp rechargeDate;
	
	private Date rechargeDate = new java.util.Date();
	
	
	
	
	@Column(name = "usecashback", nullable = true, length=2, columnDefinition = "String default 'N'")
	private String useCashBack;
	
	@Column(name = "processingcycle", length=2)
	private String processingCycle;
	
	@Column(name = "rechargerespid", length=50)
	private String rechargeRespId;
	
	private transient String statusCode;
	@Transient
	private String CHECKSUMHASH;
	

	
	
	



	public String getRechargeRespId() {
		return rechargeRespId;
	}

	public void setRechargeRespId(String rechargeRespId) {
		this.rechargeRespId = rechargeRespId;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getProcessingCycle() {
		return processingCycle;
	}

	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}

	public String getUseCashBack() {
		return useCashBack;
	}

	public void setUseCashBack(String useCashBack) {
		this.useCashBack = useCashBack;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public String getOperatorTxnId() {
		return operatorTxnId;
	}

	public void setOperatorTxnId(String operatorTxnId) {
		this.operatorTxnId = operatorTxnId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getRechargeId() {
		return rechargeId;
	}

	public void setRechargeId(String rechargeId) {
		this.rechargeId = rechargeId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getRechargeNumber() {
		return rechargeNumber;
	}

	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeCircle() {
		return rechargeCircle;
	}

	public void setRechargeCircle(String rechargeCircle) {
		this.rechargeCircle = rechargeCircle;
	}

	public String getRechargeOperator() {
		return rechargeOperator;
	}

	public void setRechargeOperator(String rechargeOperator) {
		this.rechargeOperator = rechargeOperator;
	}

	public Double getRechargeAmt() {
		return rechargeAmt;
	}

	public void setRechargeAmt(Double rechargeAmt) {
		this.rechargeAmt = rechargeAmt;
	}

	public Double getCoupanAmt() {
		return coupanAmt;
	}

	public void setCoupanAmt(Double coupanAmt) {
		this.coupanAmt = coupanAmt;
	}

	public String getWallettxnStatus() {
		return wallettxnStatus;
	}

	public void setWallettxnStatus(String wallettxnStatus) {
		this.wallettxnStatus = wallettxnStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getTxnAgent() {
		return txnAgent;
	}

	public void setTxnAgent(String txnAgent) {
		this.txnAgent = txnAgent;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}



	/*public Timestamp getRechargeDate() {
		return rechargeDate;
	}

	public void setRechargeDate(Timestamp rechargeDate) {
		this.rechargeDate = rechargeDate;
	}*/

	public Date getRechargeDate() {
		return rechargeDate;
	}

	public void setRechargeDate(Date rechargeDate) {
		this.rechargeDate = rechargeDate;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	
	
	
	
	
	

}
