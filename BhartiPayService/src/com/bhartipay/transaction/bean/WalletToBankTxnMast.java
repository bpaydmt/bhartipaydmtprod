package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="wallettobanktxnmast")
public class WalletToBankTxnMast {
	
	@Id
	@Column(name = "wtbtxnid")
	private String wtbtxnid;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorid;
	
	@Column(name = "walletid", nullable = false, length = 30)
	private String walletId;
	
	@Column(name = "userid", nullable = false, length = 100)
	private String userId;
	
	
	@Column(name = "payername", nullable = true, length = 100)
	private String sendarname;
	
	@Column(name = "payermobile", nullable = true, length = 12)
	private String sendarmobile;
	
	@Column(name = "payerEmail", nullable = true, length = 100)
	private String sendaremail;
	
	
	@Column(name = "payeecode", nullable = false, length = 100)
	private String payeecode;
	
	@Column(name = "payeename", nullable = false, length = 100)
	private String payeename;
	
	
	
	
	@Column(name = "accno", nullable = false, length = 20)
	private String accno;
	
	@Column(name = "acctype", nullable = false, length = 100)
	private String acctype;
	
	@Column(name = "ifsccode", nullable = false, length = 20)
	private String ifsccode;
	
	@Column(name = "transfertype", nullable = false, length = 100)
	private String transferType;
	
	@Column(name="amount",nullable = false, length = 6)
	private double amount;
	
	@Column(name="surcharge", columnDefinition="Decimal(10,2) default '00.00'")
	private double surCharge;
	
	@Column(name = "mobileno", nullable = false, length = 20)
	private String mobileno;
	
	@Column(name = "email",  length = 100)
	private String email;
	
	@Column(name = "description",  length = 100)
	private String description;
	
	@Column(name = "type",  length = 100)
	private String type;

	@Column(name = "reinitiate",  length = 100)
	private String reinitiate;
	
	
	@Column(name = "previousTxnId",  length = 100)
	private String previousTxnId;
	
	
	
	
	
	@Column(name = "status", nullable = false, length = 50)
	private String status;
	
	@Column(name = "wallettxnstatus",  length = 50)
	private String wallettxnstatus;
	
	@Column(name = "bankrefNo", length = 20)
	private String bankrefNo;
	
	@Column(name = "banktxnstatus",  length = 20)
	private String banktxnstatus;
	
	@Column(name = "ipimei",  length = 30)
	private String ipimei;
	
	@Column(name = "useragent",  length = 500)
	private String useragent;
	
	
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txndate;

	
	@Column(name = "transferstatus",  length = 100)
	private String transferStatus;
	
	
	@Column(name = "paymentstatus",  length = 100)
	private String paymentStatus;
	
	
	@Column(name = "fundtransno",  length = 100)
	private String fundTransno;
	
	
	@Column(name = "paymentid",  length = 100)
	private String paymentId;
	
	
	
	@Column(name = "dmtresponse",  length = 5000)
	private String dmtresponse;
	
	

	
	private transient String statusCode;
	
	
	

	
	
	
	

	public double getSurCharge() {
		return surCharge;
	}

	public void setSurCharge(double surCharge) {
		this.surCharge = surCharge;
	}

	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getFundTransno() {
		return fundTransno;
	}

	public void setFundTransno(String fundTransno) {
		this.fundTransno = fundTransno;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getDmtresponse() {
		return dmtresponse;
	}

	public void setDmtresponse(String dmtresponse) {
		this.dmtresponse = dmtresponse;
	}

	public String getPreviousTxnId() {
		return previousTxnId;
	}

	public void setPreviousTxnId(String previousTxnId) {
		this.previousTxnId = previousTxnId;
	}

	public String getReinitiate() {
		return reinitiate;
	}

	public void setReinitiate(String reinitiate) {
		this.reinitiate = reinitiate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getAcctype() {
		return acctype;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public String getPayeecode() {
		return payeecode;
	}

	public void setPayeecode(String payeecode) {
		this.payeecode = payeecode;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getSendarname() {
		return sendarname;
	}

	public void setSendarname(String sendarname) {
		this.sendarname = sendarname;
	}

	public String getSendarmobile() {
		return sendarmobile;
	}

	public void setSendarmobile(String sendarmobile) {
		this.sendarmobile = sendarmobile;
	}

	public String getSendaremail() {
		return sendaremail;
	}

	public void setSendaremail(String sendaremail) {
		this.sendaremail = sendaremail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWtbtxnid() {
		return wtbtxnid;
	}

	public void setWtbtxnid(String wtbtxnid) {
		this.wtbtxnid = wtbtxnid;
	}

	public String getPayeename() {
		return payeename;
	}

	public void setPayeename(String payeename) {
		this.payeename = payeename;
	}

	public String getAccno() {
		return accno;
	}

	public void setAccno(String accno) {
		this.accno = accno;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWallettxnstatus() {
		return wallettxnstatus;
	}

	public void setWallettxnstatus(String wallettxnstatus) {
		this.wallettxnstatus = wallettxnstatus;
	}

	public String getBankrefNo() {
		return bankrefNo;
	}

	public void setBankrefNo(String bankrefNo) {
		this.bankrefNo = bankrefNo;
	}

	public String getBanktxnstatus() {
		return banktxnstatus;
	}

	public void setBanktxnstatus(String banktxnstatus) {
		this.banktxnstatus = banktxnstatus;
	}

	public Date getTxndate() {
		return txndate;
	}

	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}

	public String getIpimei() {
		return ipimei;
	}

	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	
	
	
	
	
	

}
