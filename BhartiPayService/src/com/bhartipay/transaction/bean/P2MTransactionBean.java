package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="impstxnmast")

public class P2MTransactionBean {
	
	@Id
	@Column (name="impstxnid",length = 30)
	public String impsTxnId;
	
	@Column(name = "walletid", nullable = false, length = 30)
	private String walletId;
	
	@Column(name = "userid", nullable = false, length = 30)
	private String userId;
	
	@Column(name = "benefmobileno", nullable = false, length = 11)
	private String benefMobileNo;
	
	@Column(name = "benefmmid", nullable = false, length = 11)
	private String benefMMID;
	
	@Column(name = "remittermobileno", nullable = false, length = 11)
	private String remitterMobileNo;
	
	@Column(name = "remittermmid", nullable = false, length = 11)
	private String remitterMMID;
	
	@Column(name = "narration", nullable = true, length = 100)
	private String narration;
	
	@Column(name = "amount", nullable = false, length = 10)
	private Double amount;
	
	@Column(name = "otp", nullable = true, length = 8)
	private String otp;
	
	@Column(name = "status", nullable = false, length = 20)
	private String status;
	
	
	@Column(name = "impsrefno", nullable = true, length = 30)
	private String impsRefNo;
	
	@Column(name = "impsresponsecode", nullable = true, length = 30)
	private String impsResponseCode;
	
	@Column(name = "ipimei",  length = 30)
	private String ipimei;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txndate;

	
	private transient String statusCode;


	public String getImpsTxnId() {
		return impsTxnId;
	}


	public void setImpsTxnId(String impsTxnId) {
		this.impsTxnId = impsTxnId;
	}


	public String getWalletId() {
		return walletId;
	}


	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getBenefMobileNo() {
		return benefMobileNo;
	}


	public void setBenefMobileNo(String benefMobileNo) {
		this.benefMobileNo = benefMobileNo;
	}


	public String getBenefMMID() {
		return benefMMID;
	}


	public void setBenefMMID(String benefMMID) {
		this.benefMMID = benefMMID;
	}


	public String getRemitterMobileNo() {
		return remitterMobileNo;
	}


	public void setRemitterMobileNo(String remitterMobileNo) {
		this.remitterMobileNo = remitterMobileNo;
	}


	public String getRemitterMMID() {
		return remitterMMID;
	}


	public void setRemitterMMID(String remitterMMID) {
		this.remitterMMID = remitterMMID;
	}


	public String getNarration() {
		return narration;
	}


	public void setNarration(String narration) {
		this.narration = narration;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getOtp() {
		return otp;
	}


	public void setOtp(String otp) {
		this.otp = otp;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getIpimei() {
		return ipimei;
	}


	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}


	public Date getTxndate() {
		return txndate;
	}


	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getImpsRefNo() {
		return impsRefNo;
	}


	public void setImpsRefNo(String impsRefNo) {
		this.impsRefNo = impsRefNo;
	}


	public String getImpsResponseCode() {
		return impsResponseCode;
	}


	public void setImpsResponseCode(String impsResponseCode) {
		this.impsResponseCode = impsResponseCode;
	}
	
	
	
	
	
	 
	
	
	
}