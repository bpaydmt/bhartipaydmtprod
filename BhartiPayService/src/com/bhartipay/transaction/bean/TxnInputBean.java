package com.bhartipay.transaction.bean;

public class TxnInputBean {
	
	
	private double trxAmount;
	private double surChargeAmount;
	private  String  walletId;
	private  String  mobileNo;
	private  String  trxReasion;
	private  String  userId;
	private  String  txnId;
	private  String  pgtxn;
	private  String  status;
	private  String  ipImei;
	private  String  stDate;
	private  String  endDate;
	private  String  payeeNumber;
	private  String  payeeTxnId;
	private  String  msgId;
	private String payTxnid;
	private String aggreatorid;
	private String CHECKSUMHASH;
	private String hash;
	private String responseParam;
	private String txnType;
	private String paymentMode;
	private String cardMask;
	
	
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}
	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getResponseParam() {
		return responseParam;
	}
	public void setResponseParam(String responseParam) {
		this.responseParam = responseParam;
	}
	public double getSurChargeAmount() {
		return surChargeAmount;
	}
	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getPayTxnid() {
		return payTxnid;
	}
	public void setPayTxnid(String payTxnid) {
		this.payTxnid = payTxnid;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getPayeeNumber() {
		return payeeNumber;
	}
	public void setPayeeNumber(String payeeNumber) {
		this.payeeNumber = payeeNumber;
	}
	public String getPayeeTxnId() {
		return payeeTxnId;
	}
	public void setPayeeTxnId(String payeeTxnId) {
		this.payeeTxnId = payeeTxnId;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public double getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(double trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getTrxReasion() {
		return trxReasion;
	}
	public void setTrxReasion(String trxReasion) {
		this.trxReasion = trxReasion;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getPgtxn() {
		return pgtxn;
	}
	public void setPgtxn(String pgtxn) {
		this.pgtxn = pgtxn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIpImei() {
		return ipImei;
	}
	public void setIpImei(String ipImei) {
		this.ipImei = ipImei;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getCardMask() {
		return cardMask;
	}
	public void setCardMask(String cardMask) {
		this.cardMask = cardMask;
	}
	
	
	
	

}
