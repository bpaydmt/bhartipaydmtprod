package com.bhartipay.transaction.bean;

import java.util.Date;

//import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="paymentgatewaytxn")
public class PaymentGatewayTxnBean {
	

	@Id
	@Column(name = "txnid", nullable = false)
	private String trxId;
		
	@Column(name = "pgid")
	private String pgId;
		
	@Column(name = "txnamount", nullable = false)
	private double trxAmount;
	
	@Column(name = "txnresult")
	private String trxResult;
	
	@Column(name = "txnreason")
	private String trxReasion;
	
	@Column(name = "txncountry")
	private String trxCountry;
	
	@Column(name = "txncurrency")
	private String trxCurrency;
	
	@Column(name = "walletid")
	private String walletId;
	
	@Column(name = "mobileno")
	private String mobileNo;
	
	@Column(name = "aggreatorid")
	private String aggreatorid;
	
	
	
	@Column(name = "txnotherdetails")
	private String trxOtherDetails;
	
	
	@Column(name = "paytxnid")
	private String payTxnid;
	
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "appname")
	private String appName;
	
	
	@Column(name = "imeiip")
	private String imeiIP;
	
	@Column(name = "userid")
	private String userId;
	
	@Column(name = "useragent")
	private String useragent;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date pgtxnDate = new java.util.Date();
	
	@Column(name = "paymentmode")
	private String paymentMode;
	
	@Column(name = "cardmask")
	private String cardMask;
	
	@Column(name = "creditstatus")
	private String creditStatus;



	public Date getPgtxnDate() {
		return pgtxnDate;
	}

	public void setPgtxnDate(Date pgtxnDate) {
		this.pgtxnDate = pgtxnDate;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getImeiIP() {
		return imeiIP;
	}

	public void setImeiIP(String imeiIP) {
		this.imeiIP = imeiIP;
	}

	public String getPayTxnid() {
		return payTxnid;
	}

	public void setPayTxnid(String payTxnid) {
		this.payTxnid = payTxnid;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public String getPgId() {
		return pgId;
	}

	public void setPgId(String pgId) {
		this.pgId = pgId;
	}

	public double getTrxAmount() {
		return trxAmount;
	}

	public void setTrxAmount(double trxAmount) {
		this.trxAmount = trxAmount;
	}

	public String getTrxResult() {
		return trxResult;
	}

	public void setTrxResult(String trxResult) {
		this.trxResult = trxResult;
	}

	public String getTrxReasion() {
		return trxReasion;
	}

	public void setTrxReasion(String trxReasion) {
		this.trxReasion = trxReasion;
	}

	public String getTrxCountry() {
		return trxCountry;
	}

	public void setTrxCountry(String trxCountry) {
		this.trxCountry = trxCountry;
	}

	public String getTrxCurrency() {
		return trxCurrency;
	}

	public void setTrxCurrency(String trxCurrency) {
		this.trxCurrency = trxCurrency;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getTrxOtherDetails() {
		return trxOtherDetails;
	}

	public void setTrxOtherDetails(String trxOtherDetails) {
		this.trxOtherDetails = trxOtherDetails;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCardMask() {
		return cardMask;
	}

	public void setCardMask(String cardMask) {
		this.cardMask = cardMask;
	}

	public String getCreditStatus() {
		return creditStatus;
	}

	public void setCreditStatus(String creditStatus) {
		this.creditStatus = creditStatus;
	}
	
	
	
	

}
