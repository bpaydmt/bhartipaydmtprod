package com.bhartipay.transaction.bean;

import java.io.Serializable;

public class SurchargeBean implements Serializable{
	
	private String id;
	private  double amount; 
	private  double sarchargeAmount; 
	private  String status;
	private String transType;
	private double aggReturnVal;
	private double dtdsApply;
	
	
	
	public double getAggReturnVal() {
		return aggReturnVal;
	}
	public void setAggReturnVal(double aggReturnVal) {
		this.aggReturnVal = aggReturnVal;
	}
	public double getDtdsApply() {
		return dtdsApply;
	}
	public void setDtdsApply(double dtdsApply) {
		this.dtdsApply = dtdsApply;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	private  int txnCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getSarchargeAmount() {
		return sarchargeAmount;
	}
	public void setSarchargeAmount(double sarchargeAmount) {
		this.sarchargeAmount = sarchargeAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(int txnCode) {
		this.txnCode = txnCode;
	}
	
	
	
	
	
}
