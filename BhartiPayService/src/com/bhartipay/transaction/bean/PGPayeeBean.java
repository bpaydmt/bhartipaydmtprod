package com.bhartipay.transaction.bean;



public class PGPayeeBean {
	
	
	private String txnid;
	private double txnamount;
	private String walletid;
	private String mobileno;
	private String paytxnid;
	private String merchantid;
	private String merchantuser;
	private double amount;
	private String merchanttxnid;
	private String callbackurl;
	private String aggreatorid;
	
	// a.txnid AS txnid,txnamount,walletid,mobileno,paytxnid,merchantid,merchantuser,amount,merchanttxnid,callbackurl 
	
	
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public double getTxnamount() {
		return txnamount;
	}
	public void setTxnamount(double txnamount) {
		this.txnamount = txnamount;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getPaytxnid() {
		return paytxnid;
	}
	public void setPaytxnid(String paytxnid) {
		this.paytxnid = paytxnid;
	}
	public String getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}
	public String getMerchantuser() {
		return merchantuser;
	}
	public void setMerchantuser(String merchantuser) {
		this.merchantuser = merchantuser;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMerchanttxnid() {
		return merchanttxnid;
	}
	public void setMerchanttxnid(String merchanttxnid) {
		this.merchanttxnid = merchanttxnid;
	}
	public String getCallbackurl() {
		return callbackurl;
	}
	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	
	
	
	
	
	
	
	
	
	
	

}
