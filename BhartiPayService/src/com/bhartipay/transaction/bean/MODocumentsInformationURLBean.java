package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="modockinformationurl")
public class MODocumentsInformationURLBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "urlid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int urlid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
		
	@Column(name="path")
	private String path;

	@Column(name="filename")
	private String filename;

	
	@Column(name="mime_type")
	private String mime_type;

	@ManyToOne(targetEntity=MODocumentsInformationBean.class)  
	private MODocumentsInformationBean docinfo;

	
	public MODocumentsInformationURLBean(Date insertedtime, String path, String filename, String mime_type,
			MODocumentsInformationBean docinfo) {
		super();
		this.insertedtime = insertedtime;
		this.path = path;
		this.filename = filename;
		this.mime_type = mime_type;
		this.docinfo = docinfo;
	}

	public int getUrlid() {
		return urlid;
	}

	public void setUrlid(int urlid) {
		this.urlid = urlid;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public MODocumentsInformationBean getDocinfo() {
		return docinfo;
	}

	public void setDocinfo(MODocumentsInformationBean docinfo) {
		this.docinfo = docinfo;
	}

	@Override
	public String toString() {
		return "MODocumentsInformationURLBean [insertedtime=" + insertedtime + ", path=" + path + ", filename="
				+ filename + ", mime_type=" + mime_type + ", docinfo=" + docinfo + "]";
	}
	
	
}