package com.bhartipay.transaction.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="b2cmoneytxnmast")
public class B2CMoneyTxnMast {
	
	@Id
	@Column(name = "wtbtxnid")
	private String wtbtxnId;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorId;
	
	@Column(name = "walletid", nullable = false, length = 30)
	private String walletId;
	
	@Column(name = "userid", nullable = false, length = 100)
	private String userId;
	
	@Column(name = "accholdername", nullable = false, length = 20)
	private String accHolderName;
	
	@Column(name = "accountno", nullable = false, length = 20)
	private String accountNo;
	
	@Column(name = "ifsccode", nullable = false, length = 20)
	private String ifscCode;
	
	@Column(name = "transfertype", nullable = false, length = 100)
	private String transferType;
	
	@Column(name="amount",nullable = false, length = 8)
	private double amount;
	
	@Column(name = "wallettxnstatus",  length = 50)
	private String wallettxnstatus;
	
	@Column(name = "status", nullable = false, length = 50)
	private String status;
	
	@Column(name = "remarks",  length = 200)
	private String remarks;
	
	@Column(name = "ipimei",  length = 30)
	private String ipimei;
	
	@Column(name = "useragent",  length = 500)
	private String useragent;
		
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp txndate;
	
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMsg;
	
	@Transient
	private String CHECKSUMHASH;
	
	
	
	
	
	
	
	

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getWtbtxnId() {
		return wtbtxnId;
	}

	public void setWtbtxnId(String wtbtxnId) {
		this.wtbtxnId = wtbtxnId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getWallettxnstatus() {
		return wallettxnstatus;
	}

	public void setWallettxnstatus(String wallettxnstatus) {
		this.wallettxnstatus = wallettxnstatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpimei() {
		return ipimei;
	}

	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public Timestamp getTxndate() {
		return txndate;
	}

	public void setTxndate(Timestamp txndate) {
		this.txndate = txndate;
	}

	
	
	
	
	
	

}
