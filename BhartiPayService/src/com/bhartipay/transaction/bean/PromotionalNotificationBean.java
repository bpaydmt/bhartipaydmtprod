package com.bhartipay.transaction.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="promotionalnotification")
public class PromotionalNotificationBean 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="msgid")
	private int msgId;
	
	@Column(name="aggreatorid")
	private String aggreatorId;
	
	@Column(name="walletid")
	private String walletId;
	
	@Column(name="promsg")
	private String proMsg;

	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getProMsg() {
		return proMsg;
	}

	public void setProMsg(String proMsg) {
		this.proMsg = proMsg;
	}
	
	
}
