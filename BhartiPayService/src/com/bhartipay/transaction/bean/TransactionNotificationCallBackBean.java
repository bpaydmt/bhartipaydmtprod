package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name="transactionnotificationcallback")
public class TransactionNotificationCallBackBean implements Serializable 
{

	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false, name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="userid",length =30)
	private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	

	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------Other------------------
	
	@Column(name="transactionid")
	private String transaction_id;
	
	
	@Column(name="isvoid")
	private boolean is_void;
	
	@Column(name="isreversed")
	private boolean is_reversed;
	
	@Column(name="isdisputed")
	private boolean is_disputed;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="cardtype")
	private String card_type;
	
	@Column(name="cardpaymenttype")
	private String card_payment_type;
	
	@Column(name="cardusagetype")
	private String card_usage_type;
	
	@Column(name="maskedpan")
	private String masked_pan;
	
	@Column(name="network")
	private String network;
	
	@Column(name="paymentmethod")
	private String payment_method;
	
	@Column(name="tid")
	private String tid;
	
	@Column(name="transactiontype")
	private String transaction_type;
	
	@Column(name="merchantid")
	private String merchant_id;
	
	@Column(name="createdat",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp created_at;
	
	@Column(name="rrn")
	private String rrn;
	
	@Column(name="status")
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}


	public boolean isIs_void() {
		return is_void;
	}

	public void setIs_void(boolean is_void) {
		this.is_void = is_void;
	}

	public boolean isIs_reversed() {
		return is_reversed;
	}

	public void setIs_reversed(boolean is_reversed) {
		this.is_reversed = is_reversed;
	}

	public boolean isIs_disputed() {
		return is_disputed;
	}

	public void setIs_disputed(boolean is_disputed) {
		this.is_disputed = is_disputed;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCard_type() {
		return card_type;
	}

	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}

	public String getCard_payment_type() {
		return card_payment_type;
	}

	public void setCard_payment_type(String card_payment_type) {
		this.card_payment_type = card_payment_type;
	}

	public String getCard_usage_type() {
		return card_usage_type;
	}

	public void setCard_usage_type(String card_usage_type) {
		this.card_usage_type = card_usage_type;
	}

	public String getMasked_pan() {
		return masked_pan;
	}

	public void setMasked_pan(String masked_pan) {
		this.masked_pan = masked_pan;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getTransaction_type() {
		return transaction_type;
	}

	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TransactionNotificationBean [id=" + id + ", userId=" + userId + ", aggregatorId=" + aggregatorId
				+ ", insertedtime=" + insertedtime + ", transaction_id=" + transaction_id + ", is_void=" + is_void
				+ ", is_reversed=" + is_reversed + ", is_disputed=" + is_disputed + ", amount=" + amount
				+ ", card_type=" + card_type + ", card_payment_type=" + card_payment_type + ", card_usage_type="
				+ card_usage_type + ", masked_pan=" + masked_pan + ", network=" + network + ", payment_method="
				+ payment_method + ", tid=" + tid + ", transaction_type=" + transaction_type + ", merchant_id="
				+ merchant_id + ", created_at=" + created_at + ", rrn=" + rrn + ", status=" + status + "]";
	}
	
	
}