package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mosalesinformation")
public class MOSalesInformationBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "saleid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int saleid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------

	@Column(name="aggregatorapplicationnumber",length =60)
	private String aggregatorApplicationNumber;
	
	@Column(name="applicationdate")
	private java.sql.Date  applicationDate;
	
	
	@Column(name="aggreementdate")
	private java.sql.Date  aggreementDate;
	
	@OneToOne(targetEntity=MerchantOnboardingCallBackBean.class)  
	private MerchantOnboardingCallBackBean mobean;  

	

	public MOSalesInformationBean(Date insertedtime, String aggregatorApplicationNumber, java.sql.Date applicationDate,
			java.sql.Date aggreementDate, MerchantOnboardingCallBackBean mobean) {
		super();
		this.insertedtime = insertedtime;
		this.aggregatorApplicationNumber = aggregatorApplicationNumber;
		this.applicationDate = applicationDate;
		this.aggreementDate = aggreementDate;
		this.mobean = mobean;
	}

	public int getSaleid() {
		return saleid;
	}

	public MerchantOnboardingCallBackBean getMobean() {
		return mobean;
	}


	public void setMobean(MerchantOnboardingCallBackBean mobean) {
		this.mobean = mobean;
	}


	public void setSaleid(int saleid) {
		this.saleid = saleid;
	}


	public Date getInsertedtime() {
		return insertedtime;
	}


	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}


	public String getAggregatorApplicationNumber() {
		return aggregatorApplicationNumber;
	}


	public void setAggregatorApplicationNumber(String aggregatorApplicationNumber) {
		this.aggregatorApplicationNumber = aggregatorApplicationNumber;
	}

	public java.sql.Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(java.sql.Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public java.sql.Date getAggreementDate() {
		return aggreementDate;
	}

	public void setAggreementDate(java.sql.Date aggreementDate) {
		this.aggreementDate = aggreementDate;
	}

	@Override
	public String toString() {
		return "MOSalesInformationBean [saleid=" + saleid + ", insertedtime=" + insertedtime
				+ ", aggregatorApplicationNumber=" + aggregatorApplicationNumber + ", applicationDate="
				+ applicationDate + ", aggreementDate=" + aggreementDate + ", mobean=" + mobean + "]";
	}

}