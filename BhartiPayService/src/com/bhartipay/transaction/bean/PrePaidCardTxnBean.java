package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prepaidcardcxnmast")

public class PrePaidCardTxnBean {
	
	
	@Id
	@Column(name = "txnreqid")
	private String txnReqId;
	
	@Column(name = "card_id", length = 50)
	private String card_id;
	
	@Column(name = "user_id", length = 50)
	private String user_id;
	
	@Column(name = "transaction_ref_no", length = 50)
	private String transaction_ref_no;
	
	@Column(name="transaction_amount",nullable = false, length = 6)
	private double transaction_amount;
	
	@Column(name = "transaction_currency_code", length = 50)
	private String transaction_currency_code;
	
	@Column(name = "merchant_category_code", length = 50)
	private String merchant_category_code;
	
	@Column(name = "merchant_id", length = 50)
	private String 	merchant_id;
	
	@Column(name = "merchant_name", length = 100)
	private String 	merchant_name;
	
	@Column(name = "channel", length = 100)
	private String 	channel;
	
	@Column(name = "institution_code", length = 100)
	private String 	institution_code;
	
	@Column(name = "network", length = 100)
	private String 	network;
	
	@Column(name = "terminal_id", length = 100)
	private String 	terminal_id;
	
	@Column(name = "trace_number", length = 100)
	private String 	trace_number;
	
	@Column(name = "transaction_type", length = 100)
	private String 	transaction_type;
	
	@Column(name="card_amount",nullable = false, length = 6)
	private double card_amount;
	
	@Column(name = "card_currency_code", length = 100)
	private String 	card_currency_code;
	
	@Column(name = "post_data", length = 100)
	private String 	pos_data;
	
	@Column(name = "bank_net", length = 100)
	private String 	bank_net;
	
	@Column(name = "status", length = 100)
	private String 	status;
	
	@Column(name = "statusdescription", length = 100)
	private String 	statusdescription;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date settlement_date;
	
	
	@Column(name = "cashbackstatus", columnDefinition="int default 0")
	private int 	cashbackStatus;
	
	
	@Column(name="cashback_amount",nullable = false, columnDefinition="Decimal(10,2) default '00.00'")
	private double cashback_amount;
	
	
	
	
	
	

	public String getPos_data() {
		return pos_data;
	}

	public void setPos_data(String pos_data) {
		this.pos_data = pos_data;
	}

	public String getTxnReqId() {
		return txnReqId;
	}

	public void setTxnReqId(String txnReqId) {
		this.txnReqId = txnReqId;
	}

	public String getCard_id() {
		return card_id;
	}

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTransaction_ref_no() {
		return transaction_ref_no;
	}

	public void setTransaction_ref_no(String transaction_ref_no) {
		this.transaction_ref_no = transaction_ref_no;
	}

	public double getTransaction_amount() {
		return transaction_amount;
	}

	public void setTransaction_amount(double transaction_amount) {
		this.transaction_amount = transaction_amount;
	}

	public String getTransaction_currency_code() {
		return transaction_currency_code;
	}

	public void setTransaction_currency_code(String transaction_currency_code) {
		this.transaction_currency_code = transaction_currency_code;
	}

	public String getMerchant_category_code() {
		return merchant_category_code;
	}

	public void setMerchant_category_code(String merchant_category_code) {
		this.merchant_category_code = merchant_category_code;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getMerchant_name() {
		return merchant_name;
	}

	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getInstitution_code() {
		return institution_code;
	}

	public void setInstitution_code(String institution_code) {
		this.institution_code = institution_code;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getTerminal_id() {
		return terminal_id;
	}

	public void setTerminal_id(String terminal_id) {
		this.terminal_id = terminal_id;
	}

	public String getTrace_number() {
		return trace_number;
	}

	public void setTrace_number(String trace_number) {
		this.trace_number = trace_number;
	}

	public String getTransaction_type() {
		return transaction_type;
	}

	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}

	public double getCard_amount() {
		return card_amount;
	}

	public void setCard_amount(double card_amount) {
		this.card_amount = card_amount;
	}

	public String getCard_currency_code() {
		return card_currency_code;
	}

	public void setCard_currency_code(String card_currency_code) {
		this.card_currency_code = card_currency_code;
	}



	public String getBank_net() {
		return bank_net;
	}

	public void setBank_net(String bank_net) {
		this.bank_net = bank_net;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusdescription() {
		return statusdescription;
	}

	public void setStatusdescription(String statusdescription) {
		this.statusdescription = statusdescription;
	}

	public Date getSettlement_date() {
		return settlement_date;
	}

	public void setSettlement_date(Date settlement_date) {
		this.settlement_date = settlement_date;
	}
	
	
	
	
	

}
