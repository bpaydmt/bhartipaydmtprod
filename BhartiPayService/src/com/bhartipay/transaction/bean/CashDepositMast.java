package com.bhartipay.transaction.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="cashdepositmast")
public class CashDepositMast {
	
	@Id
	@Column (name = "depositid",updatable = false,nullable = false,  length=20)
	private String depositId;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorId;
	
	@Column( name = "userid", length=100, nullable = true)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;
	
	@Column( name = "type", length=25, nullable = true)
	private String type;
	
	@Column(name = "amount", nullable = false, length=10)
	private Double amount;
	
	@Column( name = "neftrefno", length=50)
	private String neftRefNo;
	
	@Column(name = "reciptpic", length = 100000)
    private String reciptPic;
	
	@Column( name = "status", length=50)
	private String status;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date depositDate;
	
//	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//	private Date approveDate;
	
	@Column(name="ipiemi", length=50)
	private String ipIemi;
	
	@Column(name="agent" ,length = 500)
	private String agent;
	
	
	@Column( name = "bankname", length=50)
	private String bankName;
	
	@Column( name = "branchname", length=250)
	private String branchName;
	
	@Column( name = "remark", length=250)
	private String remark;
	
	@Column( name = "remarkapprover", length=250)
	private String remarkApprover;
	
	@Column( name = "remarkchecker", length=250)
	private String remarkChecker;
	
	@Column( name = "checkerstatus", length=50)
	private String checkerStatus;
	
	@Column( name = "approverstatus", length=50)
	private String approverStatus;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp requestdate;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp approveDate;
	
	@Transient
	private String statusCode;
	
	@Transient
	private String stDate;
	
	@Transient
	private String endDate;

	private String cashDepositDate;
	
	@Column( name = "agentname", length=100)
	private String agentname;
	public String getAgentname() {
		return agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}
	
	
	
	


	public Timestamp getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(Timestamp requestdate) {
		this.requestdate = requestdate;
	}

	public String getCheckerStatus() {
		return checkerStatus;
	}

	public void setCheckerStatus(String checkerStatus) {
		this.checkerStatus = checkerStatus;
	}

	public String getApproverStatus() {
		return approverStatus;
	}

	public void setApproverStatus(String approverStatus) {
		this.approverStatus = approverStatus;
	}

	public String getRemarkApprover() {
		return remarkApprover;
	}

	public void setRemarkApprover(String remarkApprover) {
		this.remarkApprover = remarkApprover;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public String getDepositId() {
		return depositId;
	}

	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getNeftRefNo() {
		return neftRefNo;
	}

	public void setNeftRefNo(String neftRefNo) {
		this.neftRefNo = neftRefNo;
	}

	public String getReciptPic() {
		return reciptPic;
	}

	public void setReciptPic(String reciptPic) {
		this.reciptPic = reciptPic;
	}




	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Timestamp getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Timestamp approveDate) {
		this.approveDate = approveDate;
	}

	public String getCashDepositDate() {
		return cashDepositDate;
	}

	public void setCashDepositDate(String cashDepositDate) {
		this.cashDepositDate = cashDepositDate;
	}

	public String getRemarkChecker() {
		return remarkChecker;
	}

	public void setRemarkChecker(String remarkChecker) {
		this.remarkChecker = remarkChecker;
	}

	 
	
	

}
