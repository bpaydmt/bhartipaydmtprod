package com.bhartipay.transaction.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pgaggregatoraccount")
public class PGAggregatorBean {
	
	@Id
	@Column(name = "id", nullable = false)
	private String id;
		
	@Column(name = "mid")
	private String mid;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "transactionkey")
	private String transactionkey;
	
	@Column(name = "domainname")
	private String domainname;
	
	@Column(name = "pgurl")
	private String pgurl;
	
	

	public String getPgurl() {
		return pgurl;
	}

	public void setPgurl(String pgurl) {
		this.pgurl = pgurl;
	}

	public String getDomainname() {
		return domainname;
	}

	public void setDomainname(String domainname) {
		this.domainname = domainname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTransactionkey() {
		return transactionkey;
	}

	public void setTransactionkey(String transactionkey) {
		this.transactionkey = transactionkey;
	}
	
	

}
