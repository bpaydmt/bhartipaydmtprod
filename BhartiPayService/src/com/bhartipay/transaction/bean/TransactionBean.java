package com.bhartipay.transaction.bean;

import java.util.Date;

//import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="wallettransaction")
public class TransactionBean {
	
	@Id
	@Column(name = "txnid")
	private String txnid;
	
	@Column(name="userid")
	private String userid;
	
	
	@Column(name="walletid")
	private String walletid;
	
	@Column(name="aggreatorid")
	private String aggreatorid;
	
	@Column(name="txncode")
	private int txncode;
	
	@Column(name="txncredit", columnDefinition="Decimal(10,2) default '00.00'")
	private double txncredit;
	
	@Column(name="txndebit" , columnDefinition="Decimal(10,2) default '00.00'")
	private double txndebit;
	
	@Column(name="surcharge", columnDefinition="Decimal(10,2) default '00.00'")
	private double surCharge;
	
	
	@Column(name="payeedtl")
	private String payeedtl;
	
	@Column(name="result")
	private String result;
	
	
	@Column(name="resptxnid")
	private String resptxnid;
	
	
	@Column(name="walletuserip")
	private String walletuserip;
	
	@Column(name="walletuseragent")
	private String walletuseragent;
	
	
	@Column(name="closingbal")
	private double closingBal;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txnDate = new java.util.Date();
	
	@Column(name="commflag", nullable = false, length = 2 ,columnDefinition = "int default 0")
	private int commflag;
	 
	
	@Column(name="status",columnDefinition = "int default 1")
	private int status;
	
	
	
	
	
	
	
	
	
	
	public double getSurCharge() {
		return surCharge;
	}

	public void setSurCharge(double surCharge) {
		this.surCharge = surCharge;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public int getCommflag() {
		return commflag;
	}

	public void setCommflag(int commflag) {
		this.commflag = commflag;
	}

	public double getClosingBal() {
		return closingBal;
	}

	public void setClosingBal(double closingBal) {
		this.closingBal = closingBal;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public int getTxncode() {
		return txncode;
	}

	public void setTxncode(int txncode) {
		this.txncode = txncode;
	}

	public double getTxncredit() {
		return txncredit;
	}

	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}

	public double getTxndebit() {
		return txndebit;
	}

	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}

	public String getPayeedtl() {
		return payeedtl;
	}

	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResptxnid() {
		return resptxnid;
	}

	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}

	public String getWalletuserip() {
		return walletuserip;
	}

	public void setWalletuserip(String walletuserip) {
		this.walletuserip = walletuserip;
	}

	public String getWalletuseragent() {
		return walletuseragent;
	}

	public void setWalletuseragent(String walletuseragent) {
		this.walletuseragent = walletuseragent;
	}
}


