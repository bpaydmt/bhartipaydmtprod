package com.bhartipay.transaction.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="refundmast")
public class RefundMastBean {
	
	@Id
	@Column(name = "refundid", nullable = false)
	private String refundId;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorId;
	
	@Column( name = "userid", length=100, nullable = true)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;
	
	@Column(name = "amount", nullable = false, length=10)
	private Double amount;
	
	@Column(name = "txnid", nullable = false)
	private String txnid;
	
	@Column(name = "reason", nullable = false)
	private String reason;
	
	@Column(name = "status", nullable = false)
	private String status;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date disputedate;
	
	@Column(name="ipiemi", length=50)
	private String ipIemi;
	
	@Column(name="agent" ,length = 500)
	private String agent;
	
	@Transient
	private String statusCode;

	
	
	
	
	
	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDisputedate() {
		return disputedate;
	}

	public void setDisputedate(Date disputedate) {
		this.disputedate = disputedate;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	

}
