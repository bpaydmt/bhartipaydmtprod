package com.bhartipay.transaction.bean;

import java.io.Serializable;

public class RemainingLimitBean implements Serializable{
	
	private double assigneLimit;
	private double remainingLimit;
	public double getAssigneLimit() {
		return assigneLimit;
	}
	public void setAssigneLimit(double assigneLimit) {
		this.assigneLimit = assigneLimit;
	}
	public double getRemainingLimit() {
		return remainingLimit;
	}
	public void setRemainingLimit(double remainingLimit) {
		this.remainingLimit = remainingLimit;
	} 
	
	
	

}
