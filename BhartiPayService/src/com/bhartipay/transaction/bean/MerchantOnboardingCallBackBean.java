package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="merchantonboardingcallback")
public class MerchantOnboardingCallBackBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "mid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int mid;
	
	@Column(name="userid",length =30)
	private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	

	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------Other------------------
	
	@Column(name="authorization")
	private String authorization;

	//----------------------Other------------------


	//@OneToOne(targetEntity=MOSalesInformationBean.class,cascade=CascadeType.ALL)  
	//private MOSalesInformationBean salesinfo;  

		
	//@OneToOne(targetEntity=MOSalesInformationBean.class,cascade=CascadeType.ALL)  
	//@JoinColumn(name="saleid")
	//private MOSalesInformationBean salesinfo;
	
	
	//@OneToOne(mappedBy="mosalesinformation",cascade=CascadeType.ALL)
	//private MOSalesInformationBean salesinfo;
	

	
	public int getMid() {
		return mid;
	}

	


	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	/*
	 * public MOSalesInformationBean getSalesinfo() { return salesinfo; }
	 * 
	 * public void setSalesinfo(MOSalesInformationBean salesinfo) { this.salesinfo =
	 * salesinfo; }
	 */
			
	/*@OneToOne(fetch=FetchType.EAGER, targetEntity=MOSalesInformationBean.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
	private Set<CommPercRangeMaster> salesInformation;								//FK- sales_information
	*/

	/*@OneToMany(fetch=FetchType.EAGER, targetEntity=CommPercRangeMaster.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
	private Set<CommPercRangeMaster> companyInformation;							//FK- company_information
*/
	
	
	/*@OneToMany(fetch=FetchType.EAGER, targetEntity=CommPercRangeMaster.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
	private Set<CommPercRangeMaster> personalInformation;							//FK- personal_information

	*/
	
	
	/*@OneToMany(fetch=FetchType.EAGER, targetEntity=CommPercRangeMaster.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
	private Set<CommPercRangeMaster> riskInformation;								//FK- risk_information
*/
	
/*	@OneToMany(fetch=FetchType.EAGER, targetEntity=CommPercRangeMaster.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
	private Set<CommPercRangeMaster> documents;										//documents
*/
}