package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mopersonalinformation")
public class MOPersoanInformationBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "perid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int saleid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
	
	@Column(name="title",length =60)
	private String title;
	
	@Column(name="first_name",length =60)
	private String first_name;
	
	@Column(name="last_name",length =60)
	private String last_name;
	
	@Column(name="address",length =500)
	private String address;
	
	@Column(name="pincode",length =60)
	private String pincode;
	
	@Column(name="mobile",length =60)
	private String mobile;
	
	@Column(name="email",length =60)
	private String email;
	
	@Column(name="nationality",length =60)
	private String nationality;
	
	@Column(name="passport_expiry_date",length =60)			//Date
	private java.sql.Date passport_expiry_date;
	
	@Column(name="is_own_house")	
	private boolean is_own_house;
	
	@Column(name="dob")			//Date
	private java.sql.Date  dob;
	
	@Column(name="pan")			//Date
	private String pan;

	@OneToOne(targetEntity=MerchantOnboardingCallBackBean.class)  
	private MerchantOnboardingCallBackBean mobean;  


	public MOPersoanInformationBean(Date insertedtime, String title, String first_name, String last_name,
			String address, String pincode, String mobile, String email, String nationality,
			java.sql.Date passport_expiry_date, boolean is_own_house, java.sql.Date dob, String pan,
			MerchantOnboardingCallBackBean mobean) {
		super();
		this.insertedtime = insertedtime;
		this.title = title;
		this.first_name = first_name;
		this.last_name = last_name;
		this.address = address;
		this.pincode = pincode;
		this.mobile = mobile;
		this.email = email;
		this.nationality = nationality;
		this.passport_expiry_date = passport_expiry_date;
		this.is_own_house = is_own_house;
		this.dob = dob;
		this.pan = pan;
		this.mobean = mobean;
	}

	public MerchantOnboardingCallBackBean getMobean() {
		return mobean;
	}

	public void setMobean(MerchantOnboardingCallBackBean mobean) {
		this.mobean = mobean;
	}

	public int getSaleid() {
		return saleid;
	}

	public void setSaleid(int saleid) {
		this.saleid = saleid;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public java.sql.Date getPassport_expiry_date() {
		return passport_expiry_date;
	}

	public void setPassport_expiry_date(java.sql.Date passport_expiry_date) {
		this.passport_expiry_date = passport_expiry_date;
	}

	public boolean isIs_own_house() {
		return is_own_house;
	}

	public void setIs_own_house(boolean is_own_house) {
		this.is_own_house = is_own_house;
	}

	public java.sql.Date getDob() {
		return dob;
	}

	public void setDob(java.sql.Date dob) {
		this.dob = dob;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	@Override
	public String toString() {
		return "MOPersoanInformationBean [insertedtime=" + insertedtime + ", title=" + title + ", first_name="
				+ first_name + ", last_name=" + last_name + ", address=" + address + ", pincode=" + pincode
				+ ", mobile=" + mobile + ", email=" + email + ", nationality=" + nationality + ", passport_expiry_date="
				+ passport_expiry_date + ", is_own_house=" + is_own_house + ", dob=" + dob + ", pan=" + pan + "]";
	}
	
}