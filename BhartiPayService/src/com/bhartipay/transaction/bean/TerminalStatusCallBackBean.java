package com.bhartipay.transaction.bean;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="terminalstatuscallback")
public class TerminalStatusCallBackBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false, name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pkid;
	
	@Column(name="userid",length =30)
	private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	

	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------Other------------------
	
	@Column(name="authorization")
	private String authorization;
	
	//----------------------Other------------------
	@Column(name="statusidcheck")
	private String status_id_check;
	
	
	@Column(name="terminalid")
	private String id;
	
	@Column(name="status")
	private String status;
	
	@Column(name="loginid")
	private String login_id;

	
	@Column(name="isactive")
	private boolean is_active;

	@Column(name="tid")
	private String tid;

	public int getPkid() {
		return pkid;
	}

	public String getStatus_id_check() {
		return status_id_check;
	}

	public void setStatus_id_check(String status_id_check) {
		this.status_id_check = status_id_check;
	}

	public void setPkid(int pkid) {
		this.pkid = pkid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public boolean isIs_active() {
		return is_active;
	}

	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}
}