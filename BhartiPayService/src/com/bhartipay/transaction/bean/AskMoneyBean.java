package com.bhartipay.transaction.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="requestmoneymast")

public class AskMoneyBean {
	
	@Id
	@Column(name="requestid", nullable = false)
	private String trxid;
	
	@Column(name="reqwalletid", nullable = false)
	private String reqWalletId;
	
	@Column(name="aggreatorid", nullable = false)
	private String aggreatorid;
	
	
	
	@Column(name="resmobile", nullable = false)
	private String resMobile;
	
	
	@Column(name="reqamount", nullable = false)
	private double reqAmount;
	
	@Column(name="surcharge", columnDefinition="Decimal(10,2) default '00.00'")
	private double surCharge;
	
	@Column(name="status", nullable = false)
	private String status;
	
	@Column(name="userip")
	private String askUserIp;
	
	@Column(name="useragent")
	private String askUserAgent;
	
	
	@Transient
	private String statusCode;
	private String otp;
	private String id;
	
		
	
	

	public double getSurCharge() {
		return surCharge;
	}

	public void setSurCharge(double surCharge) {
		this.surCharge = surCharge;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getTrxid() {
		return trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}

	public String getReqWalletId() {
		return reqWalletId;
	}

	public void setReqWalletId(String reqWalletId) {
		this.reqWalletId = reqWalletId;
	}

	public String getResMobile() {
		return resMobile;
	}

	public void setResMobile(String resMobile) {
		this.resMobile = resMobile;
	}

	public double getReqAmount() {
		return reqAmount;
	}

	public void setReqAmount(double reqAmount) {
		this.reqAmount = reqAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAskUserIp() {
		return askUserIp;
	}

	public void setAskUserIp(String askUserIp) {
		this.askUserIp = askUserIp;
	}

	public String getAskUserAgent() {
		return askUserAgent;
	}

	public void setAskUserAgent(String askUserAgent) {
		this.askUserAgent = askUserAgent;
	}
	
	
	
	
	

}
