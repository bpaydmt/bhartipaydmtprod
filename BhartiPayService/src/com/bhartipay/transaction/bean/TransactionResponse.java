package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.List;

import com.bhartipay.airTravel.bean.PayLoadBean;
import com.bhartipay.report.bean.TxnDetailBean;

public class TransactionResponse implements Serializable {
	
	 private String statusCode;
     private String status;
     private String statusMsg;
     private List<TxnDetailBean> TxnDetlList;
     
     private RemainingLimitBean remainingLimitload;
     
	public List<TxnDetailBean> getTxnDetlList() {
		return TxnDetlList;
	}
	public void setTxnDetlList(List<TxnDetailBean> txnDetlList) {
		TxnDetlList = txnDetlList;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public RemainingLimitBean getRemainingLimitload() {
		return remainingLimitload;
	}
	public void setRemainingLimitload(RemainingLimitBean remainingLimitload) {
		this.remainingLimitload = remainingLimitload;
	}
     
     
     
     
     
     

}
