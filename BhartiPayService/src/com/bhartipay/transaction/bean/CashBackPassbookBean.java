package com.bhartipay.transaction.bean;

import java.util.Date;


public class CashBackPassbookBean {
	
	
	private String walletid;
	private Date txndate;
	private String txnid;
	private String resptxnid;
	private double txncredit;
	private double txndebit;
	
	private String payeedtl;
	private String txndesc;
	private double closingbal;
	
	
	
	
	private int status;
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getResptxnid() {
		return resptxnid;
	}
	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}
	public Date getTxndate() {
		return txndate;
	}
	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public double getTxncredit() {
		return txncredit;
	}
	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}
	public double getTxndebit() {
		return txndebit;
	}
	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}
	public String getPayeedtl() {
		return payeedtl;
	}
	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}
	public String getTxndesc() {
		return txndesc;
	}
	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}

	
	
	

	public double getClosingbal() {
		return closingbal;
	}
	public void setClosingbal(double closingbal) {
		this.closingbal = closingbal;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	

}
