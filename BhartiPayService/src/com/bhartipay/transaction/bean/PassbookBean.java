package com.bhartipay.transaction.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="passbook")
public class PassbookBean {

	
	@Id
	@Column(name = "txnid")
	private String txnid;
	
	@Column(name = "resptxnid")
	private String resptxnid;
	
	@Column(name="txndate")
	private Date txndate;
	
	@Column(name="walletid")
	private String walletid;
	
	@Column(name="txncredit")
	private double txncredit;
	
	@Column(name="txndebit")
	private double txndebit;
	
	@Column(name="payeedtl")
	private String payeedtl;
	
	@Column(name="txndesc")
	private String txndesc;
	
	@Column(name="closingbal")
	private String closingBal;
	
	@Column(name="status")
	private int status;
	
	
	@Column(name="agentname")
	private String agentname;
	
	
	@Column(name="rowid")
	private int rowid;
	
	
	
	
	public int getRowid() {
		return rowid;
	}

	public void setRowid(int rowid) {
		this.rowid = rowid;
	}

	public String getAgentname() {
		return agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public String getResptxnid() {
		return resptxnid;
	}

	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getClosingBal() {
		return closingBal;
	}

	public void setClosingBal(String closingBal) {
		this.closingBal = closingBal;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}



	public Date getTxndate() {
		return txndate;
	}

	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public double getTxncredit() {
		return txncredit;
	}

	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}

	public double getTxndebit() {
		return txndebit;
	}

	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}

	public String getPayeedtl() {
		return payeedtl;
	}

	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}

	public String getTxndesc() {
		return txndesc;
	}

	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}
	
	
	
	
	
}
