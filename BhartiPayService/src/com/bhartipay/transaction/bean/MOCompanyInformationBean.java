package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mocompanyinformation")
public class MOCompanyInformationBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "compid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int saleid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
	
	@Column(name="legal_name",length =60)
	private String legal_name;
	
	@Column(name="brand_name",length =60)
	private String brand_name;
	
	@Column(name="registered_address",length =60)
	private String registered_address;
	
	
	@Column(name="registered_pincode",length =60)
	private String registered_pincode;
	
	@Column(name="pan",length =60)
	private String pan;
	
	
	@Column(name="gstin",length =60)
	private String gstin;
	
	@Column(name="business_type",length =60)
	private String business_type;
	
	
	@Column(name="established_year",length =60)
	private java.sql.Date  established_year;
	
	@Column(name="business_nature",length =60)
	private String  business_nature;
	
	@Column(name="merchant_category_code_id",length =60)
	private String  merchant_category_code_id;

	@Column(name="merchant_type_id",length =60)
	private String  merchant_type_id;

	@Column(name="contact_name",length =60)
	private String  contact_name;

	@Column(name="contact_mobile",length =60)
	private String  contact_mobile;

	@Column(name="contact_alternate_mobile",length =60)
	private String  contact_alternate_mobile;

	@Column(name="contact_email",length =60)
	private String  contact_email;

	
	
	public MOCompanyInformationBean(Date insertedtime, String legal_name, String brand_name, String registered_address,
			String registered_pincode, String pan, String gstin, String business_type, java.sql.Date established_year,
			String business_nature, String merchant_category_code_id, String merchant_type_id, String contact_name,
			String contact_mobile, String contact_alternate_mobile, String contact_email,
			MerchantOnboardingCallBackBean mobean) {
		super();
		this.insertedtime = insertedtime;
		this.legal_name = legal_name;
		this.brand_name = brand_name;
		this.registered_address = registered_address;
		this.registered_pincode = registered_pincode;
		this.pan = pan;
		this.gstin = gstin;
		this.business_type = business_type;
		this.established_year = established_year;
		this.business_nature = business_nature;
		this.merchant_category_code_id = merchant_category_code_id;
		this.merchant_type_id = merchant_type_id;
		this.contact_name = contact_name;
		this.contact_mobile = contact_mobile;
		this.contact_alternate_mobile = contact_alternate_mobile;
		this.contact_email = contact_email;
		this.mobean = mobean;
	}

	@OneToOne(targetEntity=MerchantOnboardingCallBackBean.class)  
	private MerchantOnboardingCallBackBean mobean;  

	public int getSaleid() {
		return saleid;
	}

	public MerchantOnboardingCallBackBean getMobean() {
		return mobean;
	}


	public void setMobean(MerchantOnboardingCallBackBean mobean) {
		this.mobean = mobean;
	}


	public void setSaleid(int saleid) {
		this.saleid = saleid;
	}


	public Date getInsertedtime() {
		return insertedtime;
	}


	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getLegal_name() {
		return legal_name;
	}

	public void setLegal_name(String legal_name) {
		this.legal_name = legal_name;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getRegistered_address() {
		return registered_address;
	}

	public void setRegistered_address(String registered_address) {
		this.registered_address = registered_address;
	}

	public String getRegistered_pincode() {
		return registered_pincode;
	}

	public void setRegistered_pincode(String registered_pincode) {
		this.registered_pincode = registered_pincode;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(String business_type) {
		this.business_type = business_type;
	}

	public java.sql.Date getEstablished_year() {
		return established_year;
	}

	public void setEstablished_year(java.sql.Date established_year) {
		this.established_year = established_year;
	}

	public String getBusiness_nature() {
		return business_nature;
	}

	public void setBusiness_nature(String business_nature) {
		this.business_nature = business_nature;
	}

	

	public String getMerchant_category_code_id() {
		return merchant_category_code_id;
	}

	public void setMerchant_category_code_id(String merchant_category_code_id) {
		this.merchant_category_code_id = merchant_category_code_id;
	}

	public String getMerchant_type_id() {
		return merchant_type_id;
	}

	public void setMerchant_type_id(String merchant_type_id) {
		this.merchant_type_id = merchant_type_id;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getContact_mobile() {
		return contact_mobile;
	}

	public void setContact_mobile(String contact_mobile) {
		this.contact_mobile = contact_mobile;
	}

	public String getContact_alternate_mobile() {
		return contact_alternate_mobile;
	}

	public void setContact_alternate_mobile(String contact_alternate_mobile) {
		this.contact_alternate_mobile = contact_alternate_mobile;
	}

	public String getContact_email() {
		return contact_email;
	}

	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}

	@Override
	public String toString() {
		return "MOCompanyInformationBean [insertedtime=" + insertedtime + ", legal_name=" + legal_name + ", brand_name="
				+ brand_name + ", registered_address=" + registered_address + ", registered_pincode="
				+ registered_pincode + ", pan=" + pan + ", gstin=" + gstin + ", business_type=" + business_type
				+ ", established_year=" + established_year + ", business_nature=" + business_nature
				+ ", merchant_category_code_id=" + merchant_category_code_id + ", merchant_type_id="
				+ merchant_type_id + ", contact_name=" + contact_name + ", contact_mobile=" + contact_mobile
				+ ", contact_alternate_mobile=" + contact_alternate_mobile + ", contact_email=" + contact_email
				+ ", mobean=" + mobean + "]";
	}
	
	
}