package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="moriskinformation")
public class MORiskInformationBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "riskid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int riskid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
	
	@Column(name="expected_card_business")
	private long expected_card_business;

	@Column(name="average_bill_amount")
	private long average_bill_amount;

	
	@Column(name="velocity_check_minutes")
	private long velocity_check_minutes;

	@Column(name="velocity_check_count")
	private long velocity_check_count;

	@Column(name="settlement_days")
	private long settlement_days;

	
	@Column(name="merchant_type_code")
	private String merchant_type_code;
	
	@Column(name="member_since",length =60)
	private java.sql.Date  member_since;
	
	
	@Column(name="international_card_acceptance")	
	private boolean international_card_acceptance;
	
	
	@Column(name="transaction_sets")	
	private String transaction_sets;

	//transaction_limits  -----------> 
	
	@OneToOne(targetEntity=MerchantOnboardingCallBackBean.class)  
	private MerchantOnboardingCallBackBean mobean;

	
	public MORiskInformationBean(Date insertedtime, long expected_card_business, long average_bill_amount,
			long velocity_check_minutes, long velocity_check_count, long settlement_days, String merchant_type_code,
			java.sql.Date member_since, boolean international_card_acceptance, String transaction_sets,
			MerchantOnboardingCallBackBean mobean) {
		super();
		this.insertedtime = insertedtime;
		this.expected_card_business = expected_card_business;
		this.average_bill_amount = average_bill_amount;
		this.velocity_check_minutes = velocity_check_minutes;
		this.velocity_check_count = velocity_check_count;
		this.settlement_days = settlement_days;
		this.merchant_type_code = merchant_type_code;
		this.member_since = member_since;
		this.international_card_acceptance = international_card_acceptance;
		this.transaction_sets = transaction_sets;
		this.mobean = mobean;
	}

	public int getRiskid() {
		return riskid;
	}

	public void setRiskid(int riskid) {
		this.riskid = riskid;
	}

	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public long getExpected_card_business() {
		return expected_card_business;
	}

	public void setExpected_card_business(long expected_card_business) {
		this.expected_card_business = expected_card_business;
	}

	public long getAverage_bill_amount() {
		return average_bill_amount;
	}

	public void setAverage_bill_amount(long average_bill_amount) {
		this.average_bill_amount = average_bill_amount;
	}

	public long getVelocity_check_minutes() {
		return velocity_check_minutes;
	}

	public void setVelocity_check_minutes(long velocity_check_minutes) {
		this.velocity_check_minutes = velocity_check_minutes;
	}

	public long getVelocity_check_count() {
		return velocity_check_count;
	}

	public void setVelocity_check_count(long velocity_check_count) {
		this.velocity_check_count = velocity_check_count;
	}

	public long getSettlement_days() {
		return settlement_days;
	}

	public void setSettlement_days(long settlement_days) {
		this.settlement_days = settlement_days;
	}

	public String getMerchant_type_code() {
		return merchant_type_code;
	}

	public void setMerchant_type_code(String merchant_type_code) {
		this.merchant_type_code = merchant_type_code;
	}

	public java.sql.Date getMember_since() {
		return member_since;
	}

	public void setMember_since(java.sql.Date member_since) {
		this.member_since = member_since;
	}

	public boolean isInternational_card_acceptance() {
		return international_card_acceptance;
	}

	public void setInternational_card_acceptance(boolean international_card_acceptance) {
		this.international_card_acceptance = international_card_acceptance;
	}

	
	
	public String getTransaction_sets() {
		return transaction_sets;
	}

	public void setTransaction_sets(String transaction_sets) {
		this.transaction_sets = transaction_sets;
	}

	public MerchantOnboardingCallBackBean getMobean() {
		return mobean;
	}

	public void setMobean(MerchantOnboardingCallBackBean mobean) {
		this.mobean = mobean;
	}

	@Override
	public String toString() {
		return "MORiskInformationBean [insertedtime=" + insertedtime + ", expected_card_business="
				+ expected_card_business + ", average_bill_amount=" + average_bill_amount + ", velocity_check_minutes="
				+ velocity_check_minutes + ", velocity_check_count=" + velocity_check_count + ", settlement_days="
				+ settlement_days + ", merchant_type_code=" + merchant_type_code + ", member_since=" + member_since
				+ ", international_card_acceptance=" + international_card_acceptance + ", transaction_sets="
				+ transaction_sets + ", mobean=" + mobean + "]";
	}  

	
}