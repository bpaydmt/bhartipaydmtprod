package com.bhartipay.transaction.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="moriskinformationtranslimit")
public class MORiskInformationTransLimitBean implements Serializable 
{
	private static final long serialVersionUID = -8889359917898893779L;	
	
	//----------------------FIX------------------
	@Id
	@Column(updatable = false,nullable = false, name = "transid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int transid;
	
	@Column(name="insertedtime" ,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertedtime = new java.util.Date();
	
	//----------------------FIX------------------
	
	@Column(name="channel")
	private String channel;

	@Column(name="daily_limit")
	private long daily_limit;

	
	@Column(name="weekly_limit")
	private long weekly_limit;

	@Column(name="monthly_limit")
	private long monthly_limit;

	@Column(name="minimum_amount")
	private long minimum_amount;

	@Column(name="maximum_amount")
	private long maximum_amount;

	@ManyToOne(targetEntity=MORiskInformationBean.class)  
	private MORiskInformationBean riskInfo;

	
	
	public MORiskInformationTransLimitBean(Date insertedtime, String channel, long daily_limit, long weekly_limit,
			long monthly_limit, long minimum_amount, long maximum_amount, MORiskInformationBean riskInfo) {
		super();
		this.insertedtime = insertedtime;
		this.channel = channel;
		this.daily_limit = daily_limit;
		this.weekly_limit = weekly_limit;
		this.monthly_limit = monthly_limit;
		this.minimum_amount = minimum_amount;
		this.maximum_amount = maximum_amount;
		this.riskInfo = riskInfo;
	}



	public int getTransid() {
		return transid;
	}



	public void setTransid(int transid) {
		this.transid = transid;
	}



	public Date getInsertedtime() {
		return insertedtime;
	}

	public void setInsertedtime(Date insertedtime) {
		this.insertedtime = insertedtime;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public long getDaily_limit() {
		return daily_limit;
	}

	public void setDaily_limit(long daily_limit) {
		this.daily_limit = daily_limit;
	}

	public long getWeekly_limit() {
		return weekly_limit;
	}

	public void setWeekly_limit(long weekly_limit) {
		this.weekly_limit = weekly_limit;
	}

	public long getMonthly_limit() {
		return monthly_limit;
	}

	public void setMonthly_limit(long monthly_limit) {
		this.monthly_limit = monthly_limit;
	}

	public long getMinimum_amount() {
		return minimum_amount;
	}

	public void setMinimum_amount(long minimum_amount) {
		this.minimum_amount = minimum_amount;
	}

	public long getMaximum_amount() {
		return maximum_amount;
	}

	public void setMaximum_amount(long maximum_amount) {
		this.maximum_amount = maximum_amount;
	}

	public MORiskInformationBean getRiskInfo() {
		return riskInfo;
	}

	public void setRiskInfo(MORiskInformationBean riskInfo) {
		this.riskInfo = riskInfo;
	}  
}