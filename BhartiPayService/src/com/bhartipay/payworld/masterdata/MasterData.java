package com.bhartipay.payworld.masterdata;

import java.util.List;

public class MasterData {

	private List<PayworldMasterData> services;

	public List<PayworldMasterData> getServices() {
		return services;
	}

	public void setServices(List<PayworldMasterData> services) {
		this.services = services;
	}
	
}
