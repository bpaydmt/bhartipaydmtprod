
package com.bhartipay.payworld.masterdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service {

    @SerializedName("serviceCode")
    @Expose
    private Integer serviceCode;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("serviceLogo")
    @Expose
    private String serviceLogo;
    @SerializedName("operators")
    @Expose
    private List<Operator> operators = null;

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceLogo() {
        return serviceLogo;
    }

    public void setServiceLogo(String serviceLogo) {
        this.serviceLogo = serviceLogo;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

}
