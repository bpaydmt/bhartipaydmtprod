
package com.bhartipay.payworld.masterdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Circle {

    @SerializedName("circleCode")
    @Expose
    private String circleCode;
    @SerializedName("circleName")
    @Expose
    private String circleName;

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

}
