
package com.bhartipay.payworld.masterdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("gflx")
    @Expose
    private String gflx;
    @SerializedName("gspl")
    @Expose
    private String gspl;
    @SerializedName("topup")
    @Expose
    private String topup;
    @SerializedName("rtopup")
    @Expose
    private String rtopup;
    @SerializedName("TOPUP")
    @Expose
    private String TOPUP;
    @SerializedName("STV")
    @Expose
    private String STV;    
    
    
    
    public String getTOPUP() {
		return TOPUP;
	}

	public String getSTV() {
		return STV;
	}

	public void setTOPUP(String tOPUP) {
		TOPUP = tOPUP;
	}

	public void setSTV(String sTV) {
		STV = sTV;
	}

	public String getTopup() {
		return topup;
	}

	public String getRtopup() {
		return rtopup;
	}

	public void setTopup(String topup) {
		this.topup = topup;
	}

	public void setRtopup(String rtopup) {
		this.rtopup = rtopup;
	}

	public String getGflx() {
        return gflx;
    }

    public void setGflx(String gflx) {
        this.gflx = gflx;
    }

    public String getGspl() {
        return gspl;
    }

    public void setGspl(String gspl) {
        this.gspl = gspl;
    }

}
