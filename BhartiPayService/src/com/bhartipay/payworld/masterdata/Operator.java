
package com.bhartipay.payworld.masterdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Operator {

    @SerializedName("operatorCode")
    @Expose
    private Integer operatorCode;
    @SerializedName("operatorName")
    @Expose
    private String operatorName;
    @SerializedName("operatorLogo")
    @Expose
    private String operatorLogo;
    @SerializedName("circle")
    @Expose
    private List<Circle> circle = null;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    public Integer getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(Integer operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorLogo() {
        return operatorLogo;
    }

    public void setOperatorLogo(String operatorLogo) {
        this.operatorLogo = operatorLogo;
    }

    public List<Circle> getCircle() {
        return circle;
    }

    public void setCircle(List<Circle> circle) {
        this.circle = circle;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
