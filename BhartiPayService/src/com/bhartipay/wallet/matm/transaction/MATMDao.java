package com.bhartipay.wallet.matm.transaction;

import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.matm.MatmResponseData;

public interface MATMDao {
	
	public String persistTransaction(MatmResponseData matmResponseData);
	
	public String asPersistTransaction(MatmResponseData matmResponseData);
	
	public MATMLedger getMatmLedger(String txnId);
	

}
