package com.bhartipay.wallet.matm.transaction;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.matm.MatmResponseData;

public class MATMDaoImpl implements MATMDao{

	@Override
	public String persistTransaction(MatmResponseData matmResponseData) {
		
		boolean isNotCredit = true;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "excution  persistMatmTransaction");
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session=factory.openSession();
		
		try{
		Transaction transaction=session.beginTransaction();
		
		//aepsResponseData.setTxnDate(new Timestamp(aepsResponseData.getDateTime()));
		if(matmResponseData == null || matmResponseData.getDateTime() == null || "N/A".equalsIgnoreCase(matmResponseData.getDateTime())) {
			matmResponseData.setDateTime("");
		}
		session.saveOrUpdate(matmResponseData);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "MatmResponseData  saved with ref no "+matmResponseData.getRefno());	
		
		
		
		MATMLedger matmLedger=(MATMLedger)session.get(MATMLedger.class, matmResponseData.getRefno());
		matmLedger.setAmount(matmResponseData.getAmount());
		if (!matmLedger.getStatus().equalsIgnoreCase("Success")){
			if (matmResponseData.getStatusCode().equalsIgnoreCase("00")) {
				matmResponseData.setProcessingCode("010000");
				matmResponseData.setRefno("S"+matmResponseData.getRefno());
			}
		}
		SurchargeBean surchargeBean=new SurchargeBean();
		surchargeBean.setAgentId(matmLedger.getAgentId());
		surchargeBean.setAmount(matmResponseData.getAmount());
		//surchargeBean.setTransType("IMPS");
		
		if(matmResponseData.getProcessingCode()!=null&&!matmResponseData.getProcessingCode().isEmpty()&&matmResponseData.getProcessingCode().equalsIgnoreCase("010000")){
			matmLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
			matmResponseData.setStatusCode("00");
			
		}else if(matmResponseData.getProcessingCode()!=null&&!matmResponseData.getProcessingCode().isEmpty()&&matmResponseData.getProcessingCode().equalsIgnoreCase("310000")){
			matmLedger.setTxnType("BALANCE ENQUIRY");
			surchargeBean.setTransType("BEAEPS");
		}else if(matmResponseData.getProcessingCode()!=null&&!matmResponseData.getProcessingCode().isEmpty()&&matmResponseData.getProcessingCode().equalsIgnoreCase("320000")){
			matmLedger.setTxnType("STATUS ENQUIRY");
			surchargeBean.setTransType("CWAEPS");
			TransactionBean txnBean = (TransactionBean) session.get(TransactionBean.class, "D"+matmResponseData.getRefno());
			if(txnBean != null) {
			isNotCredit = false;
			}
		} else{
			matmLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
		}
		
	
		matmLedger.setAmount(matmResponseData.getAmount());
		matmLedger.setStatus(matmResponseData.getPaymentStatus());
		matmLedger.setStatusCode(matmResponseData.getStatusCode());
		matmLedger.setOrderId(matmResponseData.getOrderId());
		matmLedger.setOrderStatus(matmResponseData.getOrderStatus());
		matmLedger.setPaymentStatus(matmResponseData.getPaymentStatus());
		matmLedger.setRequestId(matmResponseData.getRequestId());
		matmLedger.setStan(matmResponseData.getStan());
		matmLedger.setRrn(matmResponseData.getRrn());
		matmLedger.setBankAuth(matmResponseData.getBankAuth());
		matmLedger.setProcessingCode(matmResponseData.getProcessingCode());
		matmLedger.setBankResponseCode(matmResponseData.getBankResponseCode());
		matmLedger.setBankResponseMsg(matmResponseData.getBankResponseMsg());
		//matmLedger.setAadharNumber(matmResponseData.getAadharNumber());
		matmLedger.setCardNumber(matmResponseData.getCardNumber());
		matmLedger.setStatusMessage(matmResponseData.getStatusMessage());
		
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps update aepsledger with ref no "+matmResponseData.getRefno());	

		
		String txnId=matmResponseData.getRefno();
		String agentId=matmLedger.getAgentId();
		double txnAmount=matmResponseData.getAmount();
		String aggregatorId=matmLedger.getAggregator();
		String userAgent=matmLedger.getUserAgnet();
		String userIpimei=matmLedger.getIpimei();
		
		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, agentId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm walletdetails get with id "+agentId);	

		
		SurchargeBean surBean=calculateSurcharge(surchargeBean);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm surcharge calculated with amount "+txnAmount);
		
		session.saveOrUpdate(matmLedger);
		transaction.commit();
		
		double surcharge = surBean.getSurchargeAmount();
		double distsurcharge =surBean.getDistSurCharge();
		double creditSurcharge = surBean.getCreditSurcharge();
		double suDistSurcharge=surBean.getSuDistSurcharge();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm surcharge calculated surcharge "+surcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm surcharge calculated distsurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm surcharge calculated creditSurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm surcharge calculated suDistSurcharge "+suDistSurcharge);
		
		TransactionDaoImpl transactionDao=new TransactionDaoImpl();
		//String mpfwTxnId=new CommanUtilDaoImpl().getTrxId("MPFW");
		if(isNotCredit && matmResponseData.getStatusCode()!=null
				&&(matmResponseData.getStatusCode().equalsIgnoreCase("00") || matmResponseData.getStatusCode().equalsIgnoreCase("0"))
				&& matmResponseData.getOrderStatus()!=null 
				&& (matmResponseData.getOrderStatus().equalsIgnoreCase("CardTransactionSuccessfully") || matmResponseData.getOrderStatus().equalsIgnoreCase("Card Transaction Successfully") 
						|| "Success".equalsIgnoreCase(matmResponseData.getOrderStatus()))){
		
		if(txnAmount>0){
		//for agent actual amount credit.
		transactionDao.creditMoney(txnId, agentId, walletMastBean.getWalletid(), txnAmount, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,88);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm transaction amount credit to agent on id "+agentId);
		}
		if(surcharge>0){
		//for agent commission amount credit.
		transactionDao.creditMoney("A"+txnId, agentId, walletMastBean.getWalletid(), surcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,89);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm transaction commission amount credit to agent on id "+agentId);
		}
		
		//for distributer commission amount credit.
		if(distsurcharge!=0&&walletMastBean.getDistributerid()!=null&&!walletMastBean.getDistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getDistributerid(), aggregatorId), distsurcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,90);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm transaction commission amount credit to distributer on id "+walletMastBean.getDistributerid());

		}
		
		//for superdistributer commission amount credit.
		if(suDistSurcharge!=0&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("SD"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getSuperdistributerid(), aggregatorId), suDistSurcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,91);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "matm transaction commission amount credit to superdistributer on id "+walletMastBean.getSuperdistributerid());

		}
		}
		else {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", matmResponseData.getRefno(), "matm transaction  credit criteria not fulfill, Statuscode "+matmResponseData.getStatusCode() +" -- status message "+matmResponseData.getOrderStatus());

		}
//  	creditMoney("SD"+mpfwTxnId,userId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,36);

		
		
				
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps problem occure while cedit surcharge on transaction ");
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		
		return null;
	}
	
	@Override
	public String asPersistTransaction(MatmResponseData matmResponseData) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "excution  persistAepsTransaction");
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session=factory.openSession();
		
		try{
		Transaction transaction=session.beginTransaction();
		
	    //matmResponseData.setTxnDate(new Timestamp(matmResponseData.getDateTime()));
		session.save(matmResponseData);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aepsResponseData  saved with ref no "+matmResponseData.getRefno());	
		
		
		
		//AEPSLedger aepsLedger=(AEPSLedger)session.get(AEPSLedger.class, matmResponseData.getRefno());
		
		MATMLedger matmLedger=(MATMLedger)session.get(MATMLedger.class, matmResponseData.getRefno());
		
		SurchargeBean surchargeBean=new SurchargeBean();
		surchargeBean.setAgentId(matmLedger.getAgentId());
		surchargeBean.setAmount(matmResponseData.getAmount());
		//surchargeBean.setTransType("IMPS");
		
		if(matmResponseData.getProcessingCode()!=null&&!matmResponseData.getProcessingCode().isEmpty()&&matmResponseData.getProcessingCode().equalsIgnoreCase("010000")){
			matmLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
		}else if(matmResponseData.getProcessingCode()!=null&&!matmResponseData.getProcessingCode().isEmpty()&&matmResponseData.getProcessingCode().equalsIgnoreCase("310000")){
			matmLedger.setTxnType("BALANCE ENQUIRY");
			surchargeBean.setTransType("BEAEPS");
		}else{
			matmLedger.setTxnType("CASH DEPOSIT");
			surchargeBean.setTransType("CDAEPS");
		}
		
	
		matmLedger.setAmount(matmResponseData.getAmount());
		matmLedger.setStatus(matmResponseData.getPaymentStatus());
		matmLedger.setStatusCode(matmResponseData.getStatusCode());
		matmLedger.setOrderId(matmResponseData.getOrderId());
		matmLedger.setOrderStatus(matmResponseData.getOrderStatus());
		matmLedger.setPaymentStatus(matmResponseData.getPaymentStatus());
		matmLedger.setRequestId(matmResponseData.getRequestId());
		//matmLedger.setStan(matmResponseData.getStan());
		matmLedger.setRrn(matmResponseData.getRrn());
		//matmLedger.setBankAuth(matmResponseData.getBankAuth());
		matmLedger.setProcessingCode(matmResponseData.getProcessingCode());
		matmLedger.setBankResponseCode(matmResponseData.getBankResponseCode());
		matmLedger.setBankResponseMsg(matmResponseData.getBankResponseMsg());
		//matmLedger.setAadharNumber(matmResponseData.getAadharNumber());
		matmLedger.setStatusMessage(matmResponseData.getStatusMessage());
		
		session.saveOrUpdate(matmLedger);
		transaction.commit();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps update matmLedger with ref no "+matmResponseData.getRefno());	

		
		String txnId=matmResponseData.getRefno();
		String agentId=matmLedger.getAgentId();
		double txnAmount=matmResponseData.getAmount();
		String aggregatorId=matmLedger.getAggregator();
		String userAgent=matmLedger.getUserAgnet();
		String userIpimei=matmLedger.getIpimei();
		
		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, agentId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps walletdetails get with id "+agentId);	

		
		SurchargeBean surBean=calculateSurcharge(surchargeBean);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated with amount "+txnAmount);	
		
		double surcharge = surBean.getSurchargeAmount();
		double distsurcharge =surBean.getDistSurCharge();
		double creditSurcharge = surBean.getCreditSurcharge();
		double suDistSurcharge=surBean.getSuDistSurcharge();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated surcharge "+surcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated distsurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated creditSurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated suDistSurcharge "+suDistSurcharge);
		
		TransactionDaoImpl transactionDao=new TransactionDaoImpl();
		//String mpfwTxnId=new CommanUtilDaoImpl().getTrxId("MPFW");
		if(matmResponseData.getStatusCode()!=null&&matmResponseData.getStatusCode().equalsIgnoreCase("00")&&matmResponseData.getOrderStatus()!=null&&matmResponseData.getOrderStatus().equalsIgnoreCase("SUCCESS")){
		if(txnAmount>0){
		//for agent actual amount credit.
		transactionDao.creditMoney(txnId, agentId, walletMastBean.getWalletid(), txnAmount, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,80);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction amount credit to agent on id "+agentId);
		}
		if(surcharge>0){
		//for agent commission amount credit.
		transactionDao.creditMoney("A"+txnId, agentId, walletMastBean.getWalletid(), surcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,81);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to agent on id "+agentId);
		}
		
		//for distributer commission amount credit.
		if(distsurcharge!=0&&walletMastBean.getDistributerid()!=null&&!walletMastBean.getDistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getDistributerid(), aggregatorId), distsurcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,82);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to distributer on id "+walletMastBean.getDistributerid());

		}
		
		//for superdistributer commission amount credit.
		if(suDistSurcharge!=0&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("SD"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getSuperdistributerid(), aggregatorId), suDistSurcharge, matmResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,83);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to superdistributer on id "+walletMastBean.getSuperdistributerid());

		}
		}		
//  	creditMoney("SD"+mpfwTxnId,userId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,36);

		
		
				
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps problem occure while cedit surcharge on transaction ");
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		
		return "Success";
	}
	
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" 
				+ "|calculateSurcharge()|"
				+ surchargeBean.getAgentId()+ "|calculateSurcharge()|"
						+ surchargeBean.getAmount());

		List objectList = null;
		SessionFactory factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		double surcharge = 0.0;
		double distsurcharge = 0.0;
		double creditSurcharge = 0.0;
		double suDistSurcharge=0.0;
		double aggReturnVal=0.0;
		double dtdsApply=0.0;
		try {
			if (surchargeBean.getAgentId() == null || surchargeBean.getAgentId().isEmpty()) {
				surchargeBean.setStatus("4321");
				return surchargeBean;
			}

			if (surchargeBean.getAmount() <= 0) {
				surchargeBean.setStatus("4322");
				return surchargeBean;
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******"
					+ "|excution  surcharge :" + surchargeBean.getAgentId()+ "|excution  surcharge :" + surchargeBean.getAmount());
	
			surchargeBean.setTxnCode(15);
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", surchargeBean.getAgentId());
			query.setParameter("txnAmount", surchargeBean.getAmount());
			if (surchargeBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", surchargeBean.getTransType());
			query.setParameter("isBank", "2");
			objectList = query.list();
			Object obj[] = (Object[]) objectList.get(0);
			surcharge = Double.parseDouble("" + obj[0]);
			distsurcharge = Double.parseDouble("" + obj[1]);
			if (obj[2] != null)
				creditSurcharge = Double.parseDouble("" + obj[2]);
			else
				creditSurcharge =0.0;
			if(obj[3]!=null)
				suDistSurcharge=Double.parseDouble(""+obj[3]);
			else
				suDistSurcharge=0.0;
			
			if(obj[5]!=null)
				aggReturnVal=Double.parseDouble(""+obj[5]);
			if(obj[6]!=null)
				dtdsApply=Double.parseDouble(""+obj[6]);
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" 
					+ " excution surcharge :" + surcharge);
		
			surchargeBean.setSurchargeAmount(surcharge);
			surchargeBean.setDistSurCharge(distsurcharge);
			surchargeBean.setCreditSurcharge(creditSurcharge);
			surchargeBean.setSuDistSurcharge(suDistSurcharge);
			surchargeBean.setAggReturnVal(aggReturnVal);
			surchargeBean.setDtdsApply(dtdsApply);
			surchargeBean.setStatus("1000");
		} catch (Exception e) {
			surchargeBean.setStatus("7000");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|Agentid :"+surchargeBean.getAgentId()+ "*******RequestId******" 
				+ "problem in calculateCashinSurcharge" + e.getMessage()+" "+e);
			e.printStackTrace();
			
		} finally {
			session.close();
		}
		return surchargeBean;
	}
	
	public MATMLedger getMatmLedger(String txnId) {

		SessionFactory factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		try {
			
			MATMLedger matmLedger = (MATMLedger) session.get(MATMLedger.class, txnId);
			
			return matmLedger;
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			session.close();
		}
		return null;
	}

}
