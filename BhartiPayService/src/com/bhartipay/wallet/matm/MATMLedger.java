package com.bhartipay.wallet.matm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bhartipay.wallet.aeps.AEPSConfig;
@Entity
@Table(name="matmledger")
public class MATMLedger {

	@Id
	@Column(name = "txnid", nullable = false)
	private String txnId;
	
	@Column(name ="agentcode")
	private String agentCode;
	
	@Column(name = "responseTime")
	private Timestamp responseTime;
	
	@Column(name ="ptytransdt")
	private String ptyTransDt;
			
	@Column(name ="amount")
	private double amount;
	
	@Column(name ="status")
	private String status;
	
	@Column(name ="statuscode")
	private String statusCode;
	
	@Column(name ="txntype")
	private String txnType;
	
	@Column(name ="responsehash")
	private String responseHash;
	
	@Column(name ="aggregator")
	private String  aggregator;
	
	@Column(name ="agentid")
	private String agentId;
	
	@Column(name ="walletid")
	private String walletId;
	
	@Column(name ="useragnet")
	private String userAgnet;
	
	@Column(name ="ipimei")
	private String ipimei;
	
	@Column(name ="orderId")
	private String orderId;
	
	@Column(name ="orderStatus")
	private String orderStatus;
	
	@Column(name ="paymentStatus")
	private String paymentStatus;
	
	@Column(name ="requestId")
	private String requestId;
	
	@Column(name ="stan")
	private String stan;
	
	@Column(name ="rrn")
	private String rrn;
	
	@Column(name ="bankAuth")
	private String bankAuth;
	
	@Column(name ="processingCode")
	private String processingCode;
	
	@Column(name ="bankResponseCode")
	private String bankResponseCode;
	
	@Column(name ="bankResponseMsg")
	private String bankResponseMsg;
	
	@Column(name ="aadharNumber")
	private String aadharNumber;
	
	@Column(name ="statusMessage")
	private String statusMessage;
	
	@Column(name ="aepschannel")
	private String aepsChannel;
	
	@Transient
	private String type;
	
	@Column(name ="accountBalance")
	private String accountBalance;
	
	@Column(name ="terminalId")
	private String terminalId;
	
	@Column(name = "txnDate")
	private Timestamp txnDate;
	
	@Column(name ="cardNumber")
	private String cardNumber;
	
	@Column(name ="commissionAmt")
	private double commissionAmt;
	
	
	@Transient
	AEPSConfig aepsConfig;
	
	
	
	
	
	public double getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public String getRrn() {
		return rrn;
	}
	
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	
	public Timestamp getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public AEPSConfig getAepsConfig() {
		return aepsConfig;
	}
	public void setAepsConfig(AEPSConfig aepsConfig) {
		this.aepsConfig = aepsConfig;
	}
	public String getTxnId() {
		return txnId;
	}
	
	public double getAmount() {
		return amount;
	}
	public String getStatus() {
		return status;
	}
	
	public String getAggregator() {
		return aggregator;
	}
	public String getAgentId() {
		return agentId;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getUserAgnet() {
		return userAgnet;
	}
	public String getIpimei() {
		return ipimei;
	}
	public void setUserAgnet(String userAgnet) {
		this.userAgnet = userAgnet;
	}
	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getPtyTransDt() {
		return ptyTransDt;
	}

	public void setPtyTransDt(String ptyTransDt) {
		this.ptyTransDt = ptyTransDt;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getBankResponseCode() {
		return bankResponseCode;
	}

	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}

	public String getBankResponseMsg() {
		return bankResponseMsg;
	}

	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getResponseHash() {
		return responseHash;
	}

	public void setResponseHash(String responseHash) {
		this.responseHash = responseHash;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getBankAuth() {
		return bankAuth;
	}

	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getAepsChannel() {
		return aepsChannel;
	}

	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}
	
	
	

}
