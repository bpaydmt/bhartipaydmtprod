package com.bhartipay.wallet.aeps;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="aepsconfig")
public class AEPSConfig {
	@Id
	@Column(name = "id", nullable = false)
	private long id;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	
	@Column(name="mid")
	private String mid;
	
	@Column(name="secretkey")
	private String secretKey;
	
	@Column(name="redirectionurl")
	private String redirectionUrl;
	
	@Column(name = "agentauthid")
	private String agentAuthId;
	
	@Column(name = "agentauthpassword")
	private String agentAuthPassword;
	
	@Column(name = "aepscallbackurl")
	private String aepsCallbackURL;
	
	@Column(name = "aepschannel")
	private String aepsChannel;
	
	@Column(name = "type")
	private String type;
	

	
	
	public String getAepsChannel() {
		return aepsChannel;
	}


	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}


	public String getAgentAuthId() {
		return agentAuthId;
	}


	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}


	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}


	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}


	public String getAepsCallbackURL() {
		return aepsCallbackURL;
	}


	public void setAepsCallbackURL(String aepsCallbackURL) {
		this.aepsCallbackURL = aepsCallbackURL;
	}


	
	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	public long getId() {
		return id;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public String getMid() {
		return mid;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	
		
}
