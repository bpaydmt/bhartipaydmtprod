package com.bhartipay.wallet.aeps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusPayload {
	@SerializedName("orderId")
	@Expose
	private Integer orderId;
	@SerializedName("ownerId")
	@Expose
	private Integer ownerId;
	@SerializedName("ownerType")
	@Expose
	private String ownerType;
	@SerializedName("ownerOrderId")
	@Expose
	private String ownerOrderId;
	@SerializedName("consumerNo")
	@Expose
	private String consumerNo;
	@SerializedName("orderAmount")
	@Expose
	private Integer orderAmount;
	@SerializedName("paymentAmount")
	@Expose
	private Integer paymentAmount;
	@SerializedName("orderStatus")
	@Expose
	private String orderStatus;
	@SerializedName("paymentStatus")
	@Expose
	private String paymentStatus;
	@SerializedName("fulfilmentStatus")
	@Expose
	private String fulfilmentStatus;
	@SerializedName("responseCode")
	@Expose
	private String responseCode;
	@SerializedName("responseMessage")
	@Expose
	private String responseMessage;
	@SerializedName("paymentMode")
	@Expose
	private String paymentMode;
	@SerializedName("processingCode")
	@Expose
	private String processingCode;
	@SerializedName("stan")
	@Expose
	private String stan;
	@SerializedName("rrn")
	@Expose
	private String rrn;
	@SerializedName("bankResponseCode")
	@Expose
	private String bankResponseCode;
	@SerializedName("categoryName")
	@Expose
	private String categoryName;
	@SerializedName("subCategoryName")
	@Expose
	private String subCategoryName;
	@SerializedName("updatedDate")
	@Expose
	private Integer updatedDate;
	@SerializedName("createdDate")
	@Expose
	private Integer createdDate;
	@SerializedName("aadharNumber")
	@Expose
	private String aadharNumber;

	public Integer getOrderId() {
	return orderId;
	}

	public void setOrderId(Integer orderId) {
	this.orderId = orderId;
	}

	public Integer getOwnerId() {
	return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
	this.ownerId = ownerId;
	}

	public String getOwnerType() {
	return ownerType;
	}

	public void setOwnerType(String ownerType) {
	this.ownerType = ownerType;
	}

	public String getOwnerOrderId() {
	return ownerOrderId;
	}

	public void setOwnerOrderId(String ownerOrderId) {
	this.ownerOrderId = ownerOrderId;
	}

	public String getConsumerNo() {
	return consumerNo;
	}

	public void setConsumerNo(String consumerNo) {
	this.consumerNo = consumerNo;
	}

	public Integer getOrderAmount() {
	return orderAmount;
	}

	public void setOrderAmount(Integer orderAmount) {
	this.orderAmount = orderAmount;
	}

	public Integer getPaymentAmount() {
	return paymentAmount;
	}

	public void setPaymentAmount(Integer paymentAmount) {
	this.paymentAmount = paymentAmount;
	}

	public String getOrderStatus() {
	return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
	this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
	return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
	this.paymentStatus = paymentStatus;
	}

	public String getFulfilmentStatus() {
	return fulfilmentStatus;
	}

	public void setFulfilmentStatus(String fulfilmentStatus) {
	this.fulfilmentStatus = fulfilmentStatus;
	}

	public String getResponseCode() {
	return responseCode;
	}

	public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
	}

	public String getResponseMessage() {
	return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
	}

	public String getPaymentMode() {
	return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
	this.paymentMode = paymentMode;
	}

	public String getProcessingCode() {
	return processingCode;
	}

	public void setProcessingCode(String processingCode) {
	this.processingCode = processingCode;
	}

	public String getStan() {
	return stan;
	}

	public void setStan(String stan) {
	this.stan = stan;
	}

	public String getRrn() {
	return rrn;
	}

	public void setRrn(String rrn) {
	this.rrn = rrn;
	}

	public String getBankResponseCode() {
	return bankResponseCode;
	}

	public void setBankResponseCode(String bankResponseCode) {
	this.bankResponseCode = bankResponseCode;
	}

	public String getCategoryName() {
	return categoryName;
	}

	public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
	}

	public String getSubCategoryName() {
	return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
	this.subCategoryName = subCategoryName;
	}

	public Integer getUpdatedDate() {
	return updatedDate;
	}

	public void setUpdatedDate(Integer updatedDate) {
	this.updatedDate = updatedDate;
	}

	public Integer getCreatedDate() {
	return createdDate;
	}

	public void setCreatedDate(Integer createdDate) {
	this.createdDate = createdDate;
	}

	public String getAadharNumber() {
	return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
	this.aadharNumber = aadharNumber;
	}

}
