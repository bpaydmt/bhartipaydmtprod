package com.bhartipay.wallet.aeps.transaction;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsResponseData;
import com.bhartipay.wallet.matm.MATMLedger;

public class AEPSDaoImpl implements AEPSDao{
	public static Logger logger=Logger.getLogger(AEPSDaoImpl.class);	

	TransactionDaoImpl transactionDao=new TransactionDaoImpl();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	
	@Override
	public AEPSLedger persistTransaction(AepsResponseData aepsResponseData) {
		
		boolean isNotCredit = true;
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "excution  persistAepsTransaction");
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session=factory.openSession();
		
		try{
		Transaction transaction=session.beginTransaction();
		
		//aepsResponseData.setTxnDate(new Timestamp(aepsResponseData.getDateTime()));
		session.saveOrUpdate(aepsResponseData);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aepsResponseData  saved with ref no "+aepsResponseData.getRefno());	
		logger.debug(aepsResponseData);
		
		
		AEPSLedger aepsLedger=(AEPSLedger)session.get(AEPSLedger.class, aepsResponseData.getRefno());
		//AepsResponseData aepsData = (AepsResponseData) session.get(AepsResponseData.class, aepsResponseData.getRefno());
		//AEPSLedger aepsLedger = (AEPSLedger) session.get(AEPSLedger.class, aepsResponseData.getRefno());
		
		//AEPSLedger aepsLedger = new AEPSLedger();
		aepsLedger.setAmount(aepsResponseData.getAmount());
		if (!aepsLedger.getStatus().equalsIgnoreCase("Success")){
			if (aepsResponseData.getStatusCode().equalsIgnoreCase("00")) {
			  aepsResponseData.setProcessingCode("010000");
			  aepsResponseData.setRefno("S"+aepsResponseData.getRefno());
			}
		}
		
		SurchargeBean surchargeBean=new SurchargeBean();
		surchargeBean.setAgentId(aepsLedger.getAgentId());
		surchargeBean.setAmount(aepsResponseData.getAmount());
		//surchargeBean.setTransType("IMPS");
		
		if((aepsResponseData.getProcessingCode()!=null&&!aepsResponseData.getProcessingCode().isEmpty()&&aepsResponseData.getProcessingCode().equalsIgnoreCase("010000")) || ("CASH WITHDRAWAL".equalsIgnoreCase(aepsLedger.getTxnType()))){
			aepsLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
			aepsResponseData.setStatusCode("00");
			
		}else if(aepsResponseData.getProcessingCode()!=null&&!aepsResponseData.getProcessingCode().isEmpty()&&aepsResponseData.getProcessingCode().equalsIgnoreCase("310000")){
			aepsLedger.setTxnType("BALANCE ENQUIRY");
			surchargeBean.setTransType("BEAEPS");
		}else if((aepsResponseData.getProcessingCode()!=null&&!aepsResponseData.getProcessingCode().isEmpty()&&aepsResponseData.getProcessingCode().equalsIgnoreCase("320000")) || ("STATUS ENQUIRY".equalsIgnoreCase(aepsLedger.getTxnType()))){
			aepsLedger.setTxnType("STATUS ENQUIRY");
			surchargeBean.setTransType("CWAEPS");
			TransactionBean txnBean = (TransactionBean) session.get(TransactionBean.class, "D"+aepsResponseData.getRefno());
			if(txnBean != null) {
			isNotCredit = false;
			}
		} else{
			aepsLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
		}
		
	   
		aepsLedger.setAmount(aepsResponseData.getAmount());
		aepsLedger.setStatus(aepsResponseData.getPaymentStatus());
		aepsLedger.setStatusCode(aepsResponseData.getStatusCode());
		aepsLedger.setOrderId(aepsResponseData.getOrderId());
		aepsLedger.setOrderStatus(aepsResponseData.getOrderStatus());
		aepsLedger.setPaymentStatus(aepsResponseData.getPaymentStatus());
		aepsLedger.setRequestId(aepsResponseData.getRequestId());
		aepsLedger.setStan(aepsResponseData.getStan());
		aepsLedger.setRrn(aepsResponseData.getRrn());
		aepsLedger.setBankAuth(aepsResponseData.getBankAuth());
		aepsLedger.setProcessingCode(aepsResponseData.getProcessingCode());
		aepsLedger.setBankResponseCode(aepsResponseData.getBankResponseCode());
		aepsLedger.setBankResponseMsg(aepsResponseData.getBankResponseMsg());
		aepsLedger.setAadharNumber(aepsResponseData.getAadharNumber());
		aepsLedger.setStatusMessage(aepsResponseData.getStatusMessage());
		aepsLedger.setBcname(aepsResponseData.getBcname());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps update aepsledger with ref no "+aepsResponseData.getRefno());	

		
		String txnId=aepsResponseData.getRefno();
		String agentId=aepsLedger.getAgentId();
		double txnAmount=aepsResponseData.getAmount();
		String aggregatorId=aepsLedger.getAggregator();
		String userAgent=aepsLedger.getUserAgnet();
		String userIpimei=aepsLedger.getIpimei();
		
		WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, agentId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps walletdetails get with id "+agentId);	

		
		SurchargeBean surBean=calculateSurcharge(surchargeBean);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated with amount "+txnAmount);	
		
		if(surBean != null && surBean.getSurchargeAmount() > 0) {
			aepsLedger.setCommissionAmt(surBean.getSurchargeAmount());
		}
		//session.flush();
		//session.update(aepsLedger);
		session.saveOrUpdate(aepsLedger);
		transaction.commit();
		logger.debug(aepsLedger);
		double surcharge = surBean.getSurchargeAmount();
		double distsurcharge =surBean.getDistSurCharge();
		double creditSurcharge = surBean.getCreditSurcharge();
		double suDistSurcharge=surBean.getSuDistSurcharge();
		double dtdsApply=surBean.getDtdsApply();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated surcharge "+surcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated distsurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated creditSurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated suDistSurcharge "+suDistSurcharge);
		
		
		//String mpfwTxnId=new CommanUtilDaoImpl().getTrxId("MPFW");
		if(isNotCredit && aepsResponseData.getStatusCode()!=null&&(aepsResponseData.getStatusCode().equalsIgnoreCase("00")|| aepsResponseData.getStatusCode().equalsIgnoreCase("0")) &&aepsResponseData.getOrderStatus()!=null&&aepsResponseData.getOrderStatus().equalsIgnoreCase("SUCCESS")){
		
			
		if(txnAmount>0){
		//for agent actual amount credit.
		transactionDao.creditMoney(txnId, agentId, walletMastBean.getWalletid(), txnAmount, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,80);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction amount credit to agent on id "+agentId);
		}
		if(surcharge>0){
		//for agent commission amount credit.
		transactionDao.creditMoney("A"+txnId, agentId, walletMastBean.getWalletid(), surcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,81);
		
		if (dtdsApply > 0) {
			double tdsAmount = (surcharge * dtdsApply) / 100;
			if (tdsAmount > 0) {
				transactionDao.paymentFromWalletDist(walletMastBean.getId(),walletMastBean.getWalletid(),95,tdsAmount,walletMastBean.getMobileno(),"A"+txnId,"NA",walletMastBean.getAggreatorid(),"NA",0.0);
			}
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to agent on id "+agentId);
		}
		
		//for distributer commission amount credit.
		if(distsurcharge!=0&&walletMastBean.getDistributerid()!=null&&!walletMastBean.getDistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getDistributerid(), aggregatorId), distsurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,82);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to distributer on id "+walletMastBean.getDistributerid());

		}
		
		//for superdistributer commission amount credit.
		if(suDistSurcharge!=0&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
		transactionDao.creditMoney("SD"+txnId, agentId, transactionDao.getWalletIdByUserId(walletMastBean.getSuperdistributerid(), aggregatorId), suDistSurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,83);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to superdistributer on id "+walletMastBean.getSuperdistributerid());

		}
		}		
//  	creditMoney("SD"+mpfwTxnId,userId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,36);

		
		if(aepsLedger != null)
			aepsLedger.setUserAgnet("");
		return aepsLedger;		
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps problem occure while cedit surcharge on transaction ");
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		
		return null;
	}
	
	@Override
	public AEPSLedger asPersistTransaction(AepsResponseData aepsResponseData) {
		
		boolean isNotCredit = true;
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "excution  persistAepsTransaction");
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session=factory.openSession();
		
		try{
		Transaction transaction=session.beginTransaction();
		
		session.saveOrUpdate(aepsResponseData);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aepsResponseData  saved with ref no "+aepsResponseData.getRefno());	
		logger.debug(aepsResponseData);
		
		WalletMastBean wBean = (WalletMastBean)session.get(WalletMastBean.class, aepsResponseData.getAgentId());
		
		
		CommanUtilDaoImpl comm = new CommanUtilDaoImpl();
		String txnId= comm.getTrxId("ASAEPS", wBean.getAggreatorid());
		
		AEPSLedger aepsLedger= new AEPSLedger();
		aepsLedger.setAgentId(wBean.getId());
		aepsLedger.setTxnId(txnId);
		aepsLedger.setAmount(aepsResponseData.getAmount());
//		if (!aepsLedger.getStatus().equalsIgnoreCase("Success")){
//			if (aepsResponseData.getStatusCode().equalsIgnoreCase("00")) {
//			  aepsResponseData.setProcessingCode("010000");
//			  aepsResponseData.setRefno("S"+aepsResponseData.getRefno());
//			}
//		}
		
		SurchargeBean surchargeBean=new SurchargeBean();
		surchargeBean.setAgentId(aepsLedger.getAgentId());
		surchargeBean.setAmount(aepsResponseData.getAmount());
		
		if("CASH WITHDRAWL".equalsIgnoreCase(aepsResponseData.getTxnType())){
			aepsLedger.setTxnType("CASH WITHDRAWAL");
			surchargeBean.setTransType("CWAEPS");
			aepsResponseData.setStatusCode("00");
			
		}else if("BALANCE ENQUIRY".equalsIgnoreCase(aepsResponseData.getTxnType())){
			aepsLedger.setTxnType("BALANCE ENQUIRY");
			surchargeBean.setTransType("BEAEPS");
		}else if((aepsResponseData.getProcessingCode()!=null&&!aepsResponseData.getProcessingCode().isEmpty()&&aepsResponseData.getProcessingCode().equalsIgnoreCase("320000")) || ("STATUS ENQUIRY".equalsIgnoreCase(aepsLedger.getTxnType()))){
			aepsLedger.setTxnType("STATUS ENQUIRY");
			surchargeBean.setTransType("CWAEPS");
			TransactionBean txnBean = (TransactionBean) session.get(TransactionBean.class, "D"+aepsResponseData.getRefno());
			if(txnBean != null) {
			isNotCredit = false;
			}
		} else{
			aepsLedger.setTxnType("BALANCE ENQUIRY");
			surchargeBean.setTransType("CWAEPS");
		}
		
		aepsLedger.setAgentCode(wBean.getId());
		aepsLedger.setWalletId(wBean.getWalletid());
		aepsLedger.setAggregator(wBean.getAggreatorid());
		aepsLedger.setPtyTransDt(new Date().toString());
		aepsLedger.setAmount(aepsResponseData.getAmount());
		aepsLedger.setStatus(aepsResponseData.getPaymentStatus());
		aepsLedger.setStatusCode(aepsResponseData.getStatusCode());
		aepsLedger.setOrderId(aepsResponseData.getOrderId());
		aepsLedger.setOrderStatus(aepsResponseData.getOrderStatus());
		aepsLedger.setPaymentStatus(aepsResponseData.getPaymentStatus());
		aepsLedger.setRequestId(aepsResponseData.getRequestId());
		aepsLedger.setStan(aepsResponseData.getStan());
		aepsLedger.setRrn(aepsResponseData.getRrn());
		aepsLedger.setBankAuth(aepsResponseData.getBankAuth());
		aepsLedger.setProcessingCode(aepsResponseData.getProcessingCode());
		aepsLedger.setBankResponseCode(aepsResponseData.getBankResponseCode());
		aepsLedger.setBankResponseMsg(aepsResponseData.getBankResponseMsg());
		aepsLedger.setAadharNumber(aepsResponseData.getAadharNumber());
		aepsLedger.setStatusMessage(aepsResponseData.getStatusMessage());
		aepsLedger.setBcname(aepsResponseData.getBcname());
		aepsLedger.setAepsChannel("1");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps update aepsledger with ref no "+aepsResponseData.getRefno());	

		
		String agentId=wBean.getId();
		double txnAmount=aepsResponseData.getAmount();
		String aggregatorId=aepsLedger.getAggregator();
		String userAgent=aepsLedger.getUserAgnet();
		String userIpimei=aepsLedger.getIpimei();
		
		//WalletMastBean walletMastBean=(WalletMastBean)session.get(WalletMastBean.class, agentId);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps walletdetails get with id "+agentId);	

		
		SurchargeBean surBean=calculateSurcharge(surchargeBean);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated with amount "+txnAmount);	
		
		if(surBean != null && surBean.getSurchargeAmount() > 0) {
			aepsLedger.setCommissionAmt(surBean.getSurchargeAmount());
		}
		
		session.saveOrUpdate(aepsLedger);
		transaction.commit();
		logger.debug(aepsLedger);
		double surcharge = surBean.getSurchargeAmount();
		double distsurcharge =surBean.getDistSurCharge();
		double creditSurcharge = surBean.getCreditSurcharge();
		double suDistSurcharge=surBean.getSuDistSurcharge();
		double dtdsApply=surBean.getDtdsApply();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated surcharge "+surcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated distsurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated creditSurcharge "+distsurcharge);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps surcharge calculated suDistSurcharge "+suDistSurcharge);
		
		
		//String mpfwTxnId=new CommanUtilDaoImpl().getTrxId("MPFW");
		if(isNotCredit && aepsResponseData.getStatusCode()!=null&&(aepsResponseData.getStatusCode().equalsIgnoreCase("00")|| aepsResponseData.getStatusCode().equalsIgnoreCase("0")) &&aepsResponseData.getOrderStatus()!=null&&aepsResponseData.getOrderStatus().equalsIgnoreCase("SUCCESS")){
		
			
		if(txnAmount > 0 && !"BALANCE ENQUIRY".equalsIgnoreCase(aepsResponseData.getTxnType())){
		//for agent actual amount credit.
		transactionDao.creditMoney(txnId, agentId, wBean.getWalletid(), txnAmount, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,80);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction amount credit to agent on id "+agentId);
		}
		if(surcharge > 0 && !"BALANCE ENQUIRY".equalsIgnoreCase(aepsResponseData.getTxnType())){
		//for agent commission amount credit.
		transactionDao.creditMoney("A"+txnId, agentId, wBean.getWalletid(), surcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,81);
		
		if (dtdsApply > 0) {
			double tdsAmount = (surcharge * dtdsApply) / 100;
			if (tdsAmount > 0) {
				transactionDao.paymentFromWalletDist(wBean.getId(),wBean.getWalletid(),95,tdsAmount,wBean.getMobileno(),"A"+txnId,"NA",wBean.getAggreatorid(),"NA",0.0);
			}
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to agent on id "+agentId);
		}
		
		//for distributer commission amount credit.
		if(distsurcharge!=0&&wBean.getDistributerid()!=null&&!wBean.getDistributerid().equalsIgnoreCase("-1") && !"BALANCE ENQUIRY".equalsIgnoreCase(aepsResponseData.getTxnType())){
		transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(wBean.getDistributerid(), aggregatorId), distsurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,82);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to distributer on id "+wBean.getDistributerid());

		}
		
		//for superdistributer commission amount credit.
		if(suDistSurcharge!=0&&wBean.getSuperdistributerid()!=null&&!wBean.getSuperdistributerid().equalsIgnoreCase("-1") && !"BALANCE ENQUIRY".equalsIgnoreCase(aepsResponseData.getTxnType())){
		transactionDao.creditMoney("SD"+txnId, agentId, transactionDao.getWalletIdByUserId(wBean.getSuperdistributerid(), aggregatorId), suDistSurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,83);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps transaction commission amount credit to superdistributer on id "+wBean.getSuperdistributerid());

		}
		}		
//  	creditMoney("SD"+mpfwTxnId,userId,getWalletIdByUserId(supDistId,aggreatorid),supDistSurcharge,getMobileNumber(payeeWalletid),reptxnId, ipImei,aggreatorid,agent,36);

		
		if(aepsLedger != null)
			aepsLedger.setUserAgnet("");
		return aepsLedger;		
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "aeps problem occure while cedit surcharge on transaction ");
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		
		return null;
	}
	
	public String sendSMSAlert(TxnInputBean inputBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "sendSMSAlert--"+inputBean.getTxnId());
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session=factory.openSession();
		
		try{
			
		String agentId = inputBean.getUserId();
		double amount = 0;
		String details = "";
		String date = "";
		String bankName  = null;
		String avlBal="N/A";
		
		if(inputBean.getPgtxn()!=null && "AEPS".equalsIgnoreCase(inputBean.getPgtxn())) {
		AEPSLedger aepsLedger = (AEPSLedger) session.get(AEPSLedger.class,inputBean.getTxnId());
		if (aepsLedger != null && aepsLedger.getAgentId() != null) {
			agentId = aepsLedger.getAgentId();
			amount = aepsLedger.getAmount();
			details = aepsLedger.getAadharNumber();
			date = ""+aepsLedger.getTxnDate();
		}
		}else {
			MATMLedger matmLedger = (MATMLedger) session.get(MATMLedger.class,inputBean.getTxnId());
			if (matmLedger != null && matmLedger.getAgentId() != null) {
				agentId = matmLedger.getAgentId();
				amount = matmLedger.getAmount();
				details = matmLedger.getCardNumber();
				date = ""+matmLedger.getTxnDate();
				avlBal = matmLedger.getAccountBalance();
			}
		}
			if (agentId != null) {
				WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class, agentId);
				
				transactionDao.paymentFromWallet(walletMastBean.getId(), walletMastBean.getWalletid(), 122,
						inputBean.getTrxAmount(), walletMastBean.getMobileno(), "SC" + inputBean.getTxnId(), "NA", walletMastBean.getAggreatorid(),
						"NA", 0.0);
				
				WalletMastBean walletMastBeanAgg = (WalletMastBean) session.get(WalletMastBean.class, walletMastBean.getAggreatorid());
				if(walletMastBeanAgg != null && "1".equalsIgnoreCase(walletMastBeanAgg.getWhiteLabel())) {
					WalletMastBean walletMastBeanWl = (WalletMastBean) session.get(WalletMastBean.class, walletMastBeanAgg.getCashDepositeWallet());
					transactionDao.paymentFromWallet(walletMastBeanWl.getId(), walletMastBeanWl.getWalletid(), 122,
							inputBean.getTrxAmount(), walletMastBeanWl.getMobileno(), "SCA" + inputBean.getTxnId(), "NA", walletMastBeanAgg.getId(),
							"NA", 0.0);
				}
//				Rs.<<amount>> was withdrawn from your <<Bank>> linked with <<details>> on <<dated>>. Available Balance : Rs.<<avl bal>>
				String smsMsg="";
				if(inputBean.getTxnType()!=null && ("CASH WITHDRAWAL".equalsIgnoreCase(inputBean.getTxnType()) || "STATUS ENQUIRY".equalsIgnoreCase(inputBean.getTxnType()))) {
 				     smsMsg = commanUtilDao.getsmsTemplet("AEPSCASHALERT", inputBean.getAggreatorid());
 				    smsMsg = smsMsg .replace("<<amount>>", ""+amount)
 							 .replace("<<details>>", details!=null?details: " ")
 							 .replace("<<dated>>", date != null ? date : " " )
 				    		 .replace("<<avl bal>>", avlBal != null ? avlBal: " " );
 							if(bankName !=null && !bankName.isEmpty())
 								smsMsg = smsMsg.replace("<<Bank>>", bankName != null ? bankName: " ");
 							else
 								smsMsg = smsMsg.replace("<<Bank>>", "Bank");
 							
 								
				}else {
//					Current balance for your <<bank>> account seeded with <<details>> is Rs.<<amount>>. Dated <<datewithtime>>
	 				 smsMsg = commanUtilDao.getsmsTemplet("AEPSBALENQUIRY", inputBean.getAggreatorid());
	 				 smsMsg = smsMsg.replace("<<amount>>", avlBal != null ? avlBal : " ")
	 						 .replace("<<details>>", details!=null ? details : " ")
	 						 .replace("<<datewithtime>>", date != null ? date : " ");
	 				if(bankName !=null && !bankName.isEmpty())
							smsMsg = smsMsg.replace("<<bank>>", bankName !=null ? bankName : " ");
					else
							smsMsg = smsMsg.replace("<<bank>>", "bank");
						
				}
				
				
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
						inputBean.getMobileNo(), smsMsg,	"SMS", inputBean.getAggreatorid(), "", "", "OTP");
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				
				return "SMS sent successfully.";

			}
			return "failed";
		}catch (Exception e) {
		   e.printStackTrace();
		   return "failed";
		}finally {
			session.close();
		}
		
		
	}
	
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" 
				+ "|calculateSurcharge()|"
				+ surchargeBean.getAgentId()+ "|calculateSurcharge()|"
						+ surchargeBean.getAmount());

		List objectList = null;
		SessionFactory factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		double surcharge = 0.0;
		double distsurcharge = 0.0;
		double creditSurcharge = 0.0;
		double suDistSurcharge=0.0;
		double aggReturnVal = 0.0;
		double dtdsApply=0.0;
		
		try {
			if (surchargeBean.getAgentId() == null || surchargeBean.getAgentId().isEmpty()) {
				surchargeBean.setStatus("4321");
				return surchargeBean;
			}

			if (surchargeBean.getAmount() <= 0) {
				surchargeBean.setStatus("4322");
				return surchargeBean;
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******"
					+ "|excution  surcharge :" + surchargeBean.getAgentId()+ "|excution  surcharge :" + surchargeBean.getAmount());
	
			surchargeBean.setTxnCode(15);
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", surchargeBean.getAgentId());
			query.setParameter("txnAmount", surchargeBean.getAmount());
			if (surchargeBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", surchargeBean.getTransType());
			query.setParameter("isBank", "2");
			objectList = query.list();
			Object obj[] = (Object[]) objectList.get(0);
			surcharge = Double.parseDouble("" + obj[0]);
			distsurcharge = Double.parseDouble("" + obj[1]);
			if (obj[2] != null)
				creditSurcharge = Double.parseDouble("" + obj[2]);
			else
				creditSurcharge =0.0;
			if(obj[3]!=null)
				suDistSurcharge=Double.parseDouble(""+obj[3]);
			else
				suDistSurcharge=0.0;
			if(obj[5]!=null)
				aggReturnVal=Double.parseDouble(""+obj[5]);
			if(obj[6]!=null)
				dtdsApply=Double.parseDouble(""+obj[6]);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" 
					+ " excution surcharge :" + surcharge);
		
			surchargeBean.setSurchargeAmount(surcharge);
			surchargeBean.setDistSurCharge(distsurcharge);
			surchargeBean.setCreditSurcharge(creditSurcharge);
			surchargeBean.setSuDistSurcharge(suDistSurcharge);
			surchargeBean.setAggReturnVal(aggReturnVal);
			surchargeBean.setDtdsApply(dtdsApply);
			surchargeBean.setStatus("1000");
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "surchargeAmount----"+surchargeBean.getSurchargeAmount());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "setDistSurCharge----"+surchargeBean.getDistSurCharge());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "setCreditSurcharge----"+surchargeBean.getCreditSurcharge());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "setSuDistSurcharge----"+surchargeBean.getSuDistSurcharge());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "txnType----"+surchargeBean.getTransType());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "amount----"+surchargeBean.getAmount());	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "agentId----"+surchargeBean.getAgentId());	

			
			
			
		} catch (Exception e) {
			surchargeBean.setStatus("7000");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","|Agentid :"+surchargeBean.getAgentId()+ "*******RequestId******" 
				+ "problem in calculateCashinSurcharge" + e.getMessage()+" "+e);
			e.printStackTrace();
			
		} finally {
			session.close();
		}
		return surchargeBean;
	}

}
