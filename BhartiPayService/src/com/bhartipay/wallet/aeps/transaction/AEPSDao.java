package com.bhartipay.wallet.aeps.transaction;

import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsResponseData;

public interface AEPSDao {
	
	public AEPSLedger persistTransaction(AepsResponseData aepsResponseData);
	
	public AEPSLedger asPersistTransaction(AepsResponseData aepsResponseData);
	
	public String sendSMSAlert(TxnInputBean inputBean);

}
