package com.bhartipay.wallet.aeps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AepsStatusResponse {


@SerializedName("result")
@Expose
private Result result;

public Result getResult() {
return result;
}

public void setResult(Result result) {
this.result = result;
}
}
