package com.bhartipay.wallet.aeps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload {

@SerializedName("metadata")
@Expose
private String metadata;
@SerializedName("aeps")
@Expose
private Aeps aeps;

public String getMetadata() {
return metadata;
}

public void setMetadata(String metadata) {
this.metadata = metadata;
}

public Aeps getAeps() {
return aeps;
}

public void setAeps(Aeps aeps) {
this.aeps = aeps;
}

}