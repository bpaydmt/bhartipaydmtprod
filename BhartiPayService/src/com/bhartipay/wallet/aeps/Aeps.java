package com.bhartipay.wallet.aeps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Aeps {
private Object payerId;
private Object payertype;
private Object payeeId;
private Object payeetype;
private Object txnType;
private String orderId;
private Integer amount;
private Object txnId;
private Object balance;
private String orderStatus;
private String paymentStatus;
private String requestId;
private String stan;
private String rrn;
private String bankAuth;
private String processingCode;
private String accountBalance;
private Object bankResponseCode;
private String bankResponseMsg;
private String terminalId;
private String agentId;
private String aadharNumber;
private Integer dateTime;
private String statusCode;
private Object statusMessage;
private Double commissionAmt;
private Double gstAmt;
private Double tdsAmt;
private Object walletMessage;
private Boolean isWalletFailed;
private String bcname;
private String bcaddress;
private String refno;
private String name;
private String redirectionUrl;




public String getRefno() {
	return refno;
}

public String getName() {
	return name;
}

public String getRedirectionUrl() {
	return redirectionUrl;
}

public void setRefno(String refno) {
	this.refno = refno;
}

public void setName(String name) {
	this.name = name;
}

public void setRedirectionUrl(String redirectionUrl) {
	this.redirectionUrl = redirectionUrl;
}

public Object getPayerId() {
return payerId;
}

public void setPayerId(Object payerId) {
this.payerId = payerId;
}

public Object getPayertype() {
return payertype;
}

public void setPayertype(Object payertype) {
this.payertype = payertype;
}

public Object getPayeeId() {
return payeeId;
}

public void setPayeeId(Object payeeId) {
this.payeeId = payeeId;
}

public Object getPayeetype() {
return payeetype;
}

public void setPayeetype(Object payeetype) {
this.payeetype = payeetype;
}

public Object getTxnType() {
return txnType;
}

public void setTxnType(Object txnType) {
this.txnType = txnType;
}

public String getOrderId() {
return orderId;
}

public void setOrderId(String orderId) {
this.orderId = orderId;
}

public Integer getAmount() {
return amount;
}

public void setAmount(Integer amount) {
this.amount = amount;
}

public Object getTxnId() {
return txnId;
}

public void setTxnId(Object txnId) {
this.txnId = txnId;
}

public Object getBalance() {
return balance;
}

public void setBalance(Object balance) {
this.balance = balance;
}

public String getOrderStatus() {
return orderStatus;
}

public void setOrderStatus(String orderStatus) {
this.orderStatus = orderStatus;
}

public String getPaymentStatus() {
return paymentStatus;
}

public void setPaymentStatus(String paymentStatus) {
this.paymentStatus = paymentStatus;
}

public String getRequestId() {
return requestId;
}

public void setRequestId(String requestId) {
this.requestId = requestId;
}

public String getStan() {
return stan;
}

public void setStan(String stan) {
this.stan = stan;
}

public String getRrn() {
return rrn;
}

public void setRrn(String rrn) {
this.rrn = rrn;
}

public String getBankAuth() {
return bankAuth;
}

public void setBankAuth(String bankAuth) {
this.bankAuth = bankAuth;
}

public String getProcessingCode() {
return processingCode;
}

public void setProcessingCode(String processingCode) {
this.processingCode = processingCode;
}

public String getAccountBalance() {
return accountBalance;
}

public void setAccountBalance(String accountBalance) {
this.accountBalance = accountBalance;
}

public Object getBankResponseCode() {
return bankResponseCode;
}

public void setBankResponseCode(Object bankResponseCode) {
this.bankResponseCode = bankResponseCode;
}

public String getBankResponseMsg() {
return bankResponseMsg;
}

public void setBankResponseMsg(String bankResponseMsg) {
this.bankResponseMsg = bankResponseMsg;
}

public String getTerminalId() {
return terminalId;
}

public void setTerminalId(String terminalId) {
this.terminalId = terminalId;
}

public String getAgentId() {
return agentId;
}

public void setAgentId(String agentId) {
this.agentId = agentId;
}

public String getAadharNumber() {
return aadharNumber;
}

public void setAadharNumber(String aadharNumber) {
this.aadharNumber = aadharNumber;
}

public Integer getDateTime() {
return dateTime;
}

public void setDateTime(Integer dateTime) {
this.dateTime = dateTime;
}

public String getStatusCode() {
return statusCode;
}

public void setStatusCode(String statusCode) {
this.statusCode = statusCode;
}

public Object getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(Object statusMessage) {
this.statusMessage = statusMessage;
}

public Double getCommissionAmt() {
return commissionAmt;
}

public void setCommissionAmt(Double commissionAmt) {
this.commissionAmt = commissionAmt;
}

public Double getGstAmt() {
return gstAmt;
}

public void setGstAmt(Double gstAmt) {
this.gstAmt = gstAmt;
}

public Double getTdsAmt() {
return tdsAmt;
}

public void setTdsAmt(Double tdsAmt) {
this.tdsAmt = tdsAmt;
}

public Object getWalletMessage() {
return walletMessage;
}

public void setWalletMessage(Object walletMessage) {
this.walletMessage = walletMessage;
}

public Boolean getIsWalletFailed() {
return isWalletFailed;
}

public void setIsWalletFailed(Boolean isWalletFailed) {
this.isWalletFailed = isWalletFailed;
}

public String getBcname() {
return bcname;
}

public void setBcname(String bcname) {
this.bcname = bcname;
}

public String getBcaddress() {
return bcaddress;
}

public void setBcaddress(String bcaddress) {
this.bcaddress = bcaddress;
}

}