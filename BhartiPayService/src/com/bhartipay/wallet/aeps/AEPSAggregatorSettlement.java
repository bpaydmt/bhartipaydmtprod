package com.bhartipay.wallet.aeps;

public class AEPSAggregatorSettlement {
	
	private String aggregatorid = null;
	
	private String totalSettlementAmount = null;
	
	private String aepsSettlementAmount = null;
	
	private String settledAmount = null;
	
	private String availableSettlementAmount = null;

	public String getAggregatorid() {
		return aggregatorid;
	}

	public void setAggregatorid(String aggregatorid) {
		this.aggregatorid = aggregatorid;
	}

	public String getTotalSettlementAmount() {
		return totalSettlementAmount;
	}

	public void setTotalSettlementAmount(String totalSettlementAmount) {
		this.totalSettlementAmount = totalSettlementAmount;
	}

	public String getAepsSettlementAmount() {
		return aepsSettlementAmount;
	}

	public void setAepsSettlementAmount(String aepsSettlementAmount) {
		this.aepsSettlementAmount = aepsSettlementAmount;
	}

	public String getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(String settledAmount) {
		this.settledAmount = settledAmount;
	}

	public String getAvailableSettlementAmount() {
		return availableSettlementAmount;
	}

	public void setAvailableSettlementAmount(String availableSettlementAmount) {
		this.availableSettlementAmount = availableSettlementAmount;
	}
	
}
