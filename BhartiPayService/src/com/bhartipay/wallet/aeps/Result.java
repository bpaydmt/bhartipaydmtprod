package com.bhartipay.wallet.aeps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {


@SerializedName("payload")
@Expose
private StatusPayload payload;
@SerializedName("responseCode")
@Expose
private Integer responseCode;
@SerializedName("status")
@Expose
private String status;

public StatusPayload getPayload() {
return payload;
}

public void setPayload(StatusPayload payload) {
this.payload = payload;
}

public Integer getResponseCode() {
return responseCode;
}

public void setResponseCode(Integer responseCode) {
this.responseCode = responseCode;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}
