package com.bhartipay.wallet.aeps;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="aepsledger")
public class AEPSLedger {

	@Id
	@Column(name = "txnid", nullable = false)
	private String txnId;
	
						@Column(name ="agentcode")
						private String agentCode;
	
	@Column(name ="ptytransdt")
	private String ptyTransDt;
	
	@Column(name ="amount")
	private int amount;
	
	
	@Column(name ="commissionAmt")
	private double commissionAmt;
	
	
	@Column(name ="status")
	private String status;
	
	@Column(name ="statuscode")
	private String statusCode;
	
	@Column(name ="txntype")
	private String txnType;
	
						@Column(name ="responsehash")
						private String responseHash;
	
	@Column(name ="aggregator")
	private String  aggregator;
	
	@Column(name ="agentid")
	private String agentId;
	
	@Column(name ="walletid")
	private String walletId;
	
	@Column(name ="useragnet")
	private String userAgnet;
	
	@Column(name ="ipimei")
	private String ipimei;
	
	@Column(name ="orderId")
	private String orderId;
	
	@Column(name ="orderStatus")
	private String orderStatus;
	
	@Column(name ="paymentStatus")
	private String paymentStatus;
	
	@Column(name ="requestId")
	private String requestId;
	
						@Column(name ="stan")
						private String stan;
	
	@Column(name ="rrn")
	private String rrn;
	
						@Column(name ="bankAuth")
						private String bankAuth;
	
	@Column(name ="processingCode")
	private String processingCode;
	
	@Column(name ="bankResponseCode")
	private String bankResponseCode;
	
	@Column(name ="bankResponseMsg")
	private String bankResponseMsg;
	
						@Column(name ="aadharNumber")
						private String aadharNumber;
	
	@Column(name ="statusMessage")
	private String statusMessage;
	
						@Column(name ="aepschannel")
						private String aepsChannel;
						
						@Column(name ="bcname")
						private String bcname;
	
						
	@Transient
	private String type;
	
	
	
	
	
	
	public String getBcname() {
		return bcname;
	}
	public void setBcname(String bcname) {
		this.bcname = bcname;
	}
	public double getCommissionAmt() {
		return commissionAmt;
	}
	public void setCommissionAmt(double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	public String getAepsChannel() {
		return aepsChannel;
	}
	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}
	public String getOrderId() {
		return orderId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public String getRequestId() {
		return requestId;
	}
	public String getStan() {
		return stan;
	}
	public String getRrn() {
		return rrn;
	}
	public String getBankAuth() {
		return bankAuth;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public String getBankResponseCode() {
		return bankResponseCode;
	}
	public String getBankResponseMsg() {
		return bankResponseMsg;
	}
	public String getAadharNumber() {
		return aadharNumber;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}
	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}
	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Column(name = "txnDate")
	private Timestamp txnDate;
	
	@Transient
	AEPSConfig aepsConfig;
	
	
	
	public Timestamp getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public AEPSConfig getAepsConfig() {
		return aepsConfig;
	}
	public void setAepsConfig(AEPSConfig aepsConfig) {
		this.aepsConfig = aepsConfig;
	}
	public String getTxnId() {
		return txnId;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public String getPtyTransDt() {
		return ptyTransDt;
	}
	public int getAmount() {
		return amount;
	}
	public String getStatus() {
		return status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public String getResponseHash() {
		return responseHash;
	}
	public String getAggregator() {
		return aggregator;
	}
	public String getAgentId() {
		return agentId;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public void setPtyTransDt(String ptyTransDt) {
		this.ptyTransDt = ptyTransDt;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void setResponseHash(String responseHash) {
		this.responseHash = responseHash;
	}
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getUserAgnet() {
		return userAgnet;
	}
	public String getIpimei() {
		return ipimei;
	}
	public void setUserAgnet(String userAgnet) {
		this.userAgnet = userAgnet;
	}
	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "AEPSLedger [txnId=" + txnId + ", agentCode=" + agentCode + ", ptyTransDt=" + ptyTransDt + ", amount="
				+ amount + ", commissionAmt=" + commissionAmt + ", status=" + status + ", txnType=" + txnType
				+ ", aggregator=" + aggregator + ", agentId=" + agentId + ", walletId=" + walletId + ", requestId="
				+ requestId + ", aadharNumber=" + aadharNumber + ", bcname=" + bcname + ", type=" + type + ", txnDate="
				+ txnDate + "]";
	}
	
	

}
