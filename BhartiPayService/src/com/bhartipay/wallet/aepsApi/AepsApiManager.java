package com.bhartipay.wallet.aepsApi;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.bhartipay.wallet.aepsApi.bean.TokenResponse;
import com.bhartipay.wallet.aepsApi.service.AEPSApiDao;
import com.bhartipay.wallet.aepsApi.service.AEPSApiDaoImpl;

@Path("/AepsApi")
public class AepsApiManager {
	
	public static final Logger logger = Logger.getLogger(AepsApiManager.class);
	
	AEPSApiDao aepsApiDao=new AEPSApiDaoImpl();
	
	@POST
	@Path("/generateAsToken")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public TokenResponse generateToken(AepsTokenRequest req) {
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",req.getRetailerId(), "", req.getPipe(), "", "generateToken for Aeps aadharshila ");
	TokenResponse res = aepsApiDao.generateToken(req);
	
	return res;
	}

}
