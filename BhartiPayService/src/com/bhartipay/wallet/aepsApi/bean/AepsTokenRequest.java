package com.bhartipay.wallet.aepsApi.bean;

public class AepsTokenRequest {
	
    public String agentAuthId;
    public String agentAuthPassword;
    public String retailerId;
    public String apiId;
    public String pipe;
    
	public AepsTokenRequest() {
	}

	public String getAgentAuthId() {
		return agentAuthId;
	}

	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}

	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}

	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	public String getPipe() {
		return pipe;
	}

	public void setPipe(String pipe) {
		this.pipe = pipe;
	}
    
    
	
}
