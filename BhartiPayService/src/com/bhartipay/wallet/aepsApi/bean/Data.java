package com.bhartipay.wallet.aepsApi.bean;

public class Data {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
