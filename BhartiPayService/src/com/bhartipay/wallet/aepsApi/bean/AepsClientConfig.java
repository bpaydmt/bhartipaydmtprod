package com.bhartipay.wallet.aepsApi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="aepsclientconfig")
public class AepsClientConfig implements Serializable{
	 
	private static final long serialVersionUID = -8889359917898893779L;

	@Id
	@Column(name="agentauthid")
	private String agentAuthId;
	@Column(name="agentauthpassword")
    private String agentAuthPassword;
	
	public String getAgentAuthId() {
		return agentAuthId;
	}
	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}
	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}
	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}
	
	
	
}
