package com.bhartipay.wallet.aepsApi;

import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.bhartipay.wallet.aepsApi.bean.AEPSAPIUtil;
import com.bhartipay.wallet.aepsApi.bean.TokenResponse; 
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class AadharshilaAeps {
	public static Logger logger=Logger.getLogger(AadharshilaAeps.class);
	
	public TokenResponse generateToken(String agentAuthId, String agentAuthPassword, String agentid, String apiId, String pipe){

	  logger.debug("*************************** calling aadharshila generateToken ***********************************");
	  Gson gson = new Gson();
	  JSONObject jsonText=new JSONObject();
	  jsonText.put("agentAuthId",agentAuthId);
	  jsonText.put("agentAuthPassword",agentAuthPassword);
	  jsonText.put("retailerId",agentid);
	  jsonText.put("apiId",apiId);
	  jsonText.put("pipe",pipe);
	  logger.debug("*************************** calling aadharshila generateToken  request***********************************"+jsonText);

	  WebResource webResource=AadharshilaAeps.getAsAepsTokenUrl();
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  logger.debug("*************************** calling aadharshila generateToken response ***********************************"+output);

	  try{
	  TokenResponse resp = gson.fromJson(output, TokenResponse.class);
	  return resp;
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return null;
	}
	
	
	public static WebResource getAsAepsTokenUrl() {
		logger.debug("***************  calling method getAsAepsTokenUrl for getting service URL  *********** ");
		
		System.out.println("______Inside Service getAsAepsTokenUrl(String URL)__________");
		
		WebResource webResource=null;
		try{
				ClientConfig config = new DefaultClientConfig();
				Client client = Client.create(config);

				Properties prop = new Properties();
				InputStream in = AadharshilaAeps.class.getResourceAsStream("InfrastructureAeps.properties");
				
				try {
					prop.load(in);
					in.close();
				} catch (Exception e) {
					System.out.println(e);
					e.printStackTrace();
				}
				webResource = client.resource(prop.getProperty("aadharshilatokenurl"));
				
				System.out.println("________getAsAepsTokenUrl_________"+webResource);
				

		}catch (Exception e) {
			logger.debug("***************  problem in getAsAepsTokenUrl for getting service URL  *********** "+e);
				e.printStackTrace();
		}
		return webResource;
	}
	
	
	
	
	
}
