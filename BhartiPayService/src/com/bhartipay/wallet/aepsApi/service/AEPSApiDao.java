package com.bhartipay.wallet.aepsApi.service;

import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.bhartipay.wallet.aepsApi.bean.Data;
import com.bhartipay.wallet.aepsApi.bean.TokenResponse;

public interface AEPSApiDao {
	
	public TokenResponse generateToken(AepsTokenRequest req);

}
