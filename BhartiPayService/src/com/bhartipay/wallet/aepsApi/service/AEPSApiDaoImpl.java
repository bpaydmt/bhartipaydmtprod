package com.bhartipay.wallet.aepsApi.service;

import org.apache.log4j.Logger;

import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aepsApi.AadharshilaAeps;
import com.bhartipay.wallet.aepsApi.bean.AEPSAPIUtil;
import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.bhartipay.wallet.aepsApi.bean.Data;
import com.bhartipay.wallet.aepsApi.bean.TokenResponse; 

public class AEPSApiDaoImpl implements AEPSApiDao{
	public static Logger logger=Logger.getLogger(AEPSApiDaoImpl.class);	

	TransactionDaoImpl transactionDao=new TransactionDaoImpl();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	
	
	@Override
	public TokenResponse generateToken(AepsTokenRequest req) {
		 
		String authId = AEPSAPIUtil.getMD5EncryptedValue(req.getAgentAuthId());
		String authPass = AEPSAPIUtil.getMD5EncryptedValue(req.getAgentAuthPassword());
		 
		AadharshilaAeps aeps=new AadharshilaAeps();
		TokenResponse res = aeps.generateToken(authId, authPass, req.getRetailerId(), req.getApiId(), req.getPipe());
		
		
		return res;
	}
	
	 
	
	

}
