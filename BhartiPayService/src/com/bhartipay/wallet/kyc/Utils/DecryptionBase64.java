package com.bhartipay.wallet.kyc.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.xml.ws.Response;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class DecryptionBase64 {

	static byte[] bytes;
	private static final String JCE_PROVIDER = "BC";
	private static final String ASYMMETRIC_ALGO = "RSA/ECB/PKCS1Padding";
	private static final int SYMMETRIC_KEY_SIZE = 256;
	private static final String CERTIFICATE_TYPE = "X.509";

	// private PublicKey privateKey;

	public byte[] decryptUsingPublicKey(byte[] data, PrivateKey privateKey)
			throws IOException, GeneralSecurityException {
		// encrypt the session key with the public key
		// Cipher pkCipher = Cipher.getInstance(ASYMMETRIC_ALGO, JCE_PROVIDER);

		Cipher pkCipher = Cipher.getInstance("RSA");
		// System.out.println("pkVipher"+pkCipher);
		pkCipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] encSessionKey = pkCipher.doFinal(data);
		return encSessionKey;
	}

	public byte[] decryptUsingSessionKey(byte[] skey, byte[] data)
			throws InvalidCipherTextException {
		PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(
				new AESEngine(), new PKCS7Padding());
		cipher.init(false, new KeyParameter(skey));
		int outputSize = cipher.getOutputSize(data.length);
		byte[] tempOP = new byte[outputSize];
		int processLen = cipher.processBytes(data, 0, data.length, tempOP, 0);
		int outputLen = cipher.doFinal(tempOP, processLen);
		byte[] result = new byte[processLen + outputLen];
		System.arraycopy(tempOP, 0, result, 0, result.length);
		return result;
	}

	private static PrivateKey getPrivateKey(File file, String password)
			throws Exception {
		KeyStore ks = KeyStore.getInstance("PKCS12");
		try (FileInputStream fis = new FileInputStream(file)) {
			ks.load(fis, password.toCharArray());
		}

		Enumeration<String> aliases = ks.aliases();
		String alias = aliases.nextElement();
		return (PrivateKey) ks.getKey(alias, password.toCharArray());
	}

}
