package com.bhartipay.wallet.bean;

public class WalletResponseBean 
{
	public String requestId;
	
	public String response;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	
}
