package com.bhartipay.wallet.bean;

public class WalletRequestBean 
{
	private String aggregatorId;
	
	private String request;

	

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

}
