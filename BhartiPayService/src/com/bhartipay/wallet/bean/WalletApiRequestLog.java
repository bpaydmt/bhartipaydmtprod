package com.bhartipay.wallet.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name="walletapirequestlog")
public class WalletApiRequestLog  implements Serializable
{
	@Id
	@Column( name = "requestid" , length = 50)
	private String requestId;
	
	@Column(name = "aggreatorid" , length =50)
	private String aggregatorId;
	
	@Column(name = "requestvalue" , length = 500)
	private String requestValue;
	
	@Column(name = "apiname" , length = 500)
	private String apiName;
	
	@Column(name = "requestdate" , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private String requestDate;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getRequestValue() {
		return requestValue;
	}

	public void setRequestValue(String requestValue) {
		this.requestValue = requestValue;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	
	
}
