package com.bhartipay.wallet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.PassbookBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.EmailValidator;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.wallet.bean.WalletRequestBean;
import com.bhartipay.wallet.bean.WalletResponseBean;
import com.google.gson.Gson;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import io.jsonwebtoken.Claims;

/**
 * 
 * @author shweta.roy
 *
 */

@Path("/WalletApi")
public class WalletAPIManager 
{
	public static final Logger logger = Logger.getLogger(WalletAPIManager.class.getName());
	Gson gson = new Gson();
	HashMap<String , Object> responseMap = new HashMap<String,Object>();
	WalletUserDao walletUserDao = new WalletUserDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
	BPJWTSignUtil oxyJWTSignUtil = new BPJWTSignUtil();

	/**
	 * @param WalletRequestBean
	 * @param HttpRequest
	 * @return
	 */
	@POST
	@Path("/createWalletRequest")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public WalletResponseBean creatWalletRequest(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		
	
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		WalletMastBean walletMastBean= new WalletMastBean();
		String serverName =httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "creatWalletRequest");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", "creatWalletRequest()"+"|"+serverName+"***RequestId***"+requestId+"RequestString "+walletRequestBean.getRequest()
				);
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****creatWalletRequest******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Mobile Number.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(claim.get("emailId") == null || claim.get("emailId").toString().trim().isEmpty() || !(new EmailValidator().validate(claim.get("emailId").toString())))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Email ID.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(claim.get("name") == null || claim.get("name").toString().trim().isEmpty() || !nameValidate(claim.get("name").toString()))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Customer Name.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(claim.get("password") == null || claim.get("password").toString().trim().isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Password.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			walletMastBean.setAggreatorid(walletRequestBean.getAggregatorId());
			walletMastBean.setMobileno(claim.get("mobileNo").toString());
			walletMastBean.setEmailid(claim.get("emailId").toString());
			walletMastBean.setName(claim.get("name").toString());
			walletMastBean.setPassword(claim.get("password").toString());
			String statusCode = walletUserDao.signUpUser( walletMastBean);
			if(statusCode.trim().equals("7040"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Mobile Number.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("7049"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Aggreator ID.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("7041"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Email ID.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("7001"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Email ID already registered with us.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("7002"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Mobile number already registered with us.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("7038"))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Password.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			if(statusCode.trim().equals("1000"))
			{
				WalletMastBean walletuser = walletUserDao.updateUserAsActive(claim.get("mobileNo").toString(),walletRequestBean.getAggregatorId(),serverName);
				if(walletuser == null)
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Message String");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				responseMap.put("walletId", walletuser.getId());
				//responseMap.put("walletId", walletuser.getWalletid());
				responseMap.put("mobileNo", walletuser.getMobileno());			
				responseMap.put("code", "300");
				responseMap.put("response", "SUCCESS");
				responseMap.put("message", "SUCCESS");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", "|"+serverName+"***RequestId***"+requestId+"problem in creatWalletRequest "+ex.getMessage()+" "+ex
					);
			logger.info(serverName+"****RequestId***"+requestId+"*********problem in creatWalletRequest**********e.message****"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
		return walletResponseBean;
	}
	
	
	/**
	 * 
	 * @param walletRequestBean
	 * @param httpServletRequest
	 * @return
	 */
	@POST
	@Path("/checkWalletBalance")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public WalletResponseBean CheckWalletBalance(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		
		
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		//UserProfileBean walletuser =new UserProfileBean();
		WalletMastBean walletMast = new WalletMastBean();
		String mobileNo="";
		String walletId="";
		String serverName =httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "CheckWalletBalance");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|"+"CheckWalletBalance()"+"|"+walletRequestBean.getRequest()
				);
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****CheckWalletBalance******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		Double walletBalance = 0.0;
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
			{
				if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
				{
					if(claim.get("emailId") == null || claim.get("emailId").toString().trim().isEmpty() || new EmailValidator().validate(claim.get("emailId").toString()))
					{
						responseMap.put("code", "1");
						responseMap.put("response", "ERROR");
						responseMap.put("message", "Invalid Mobile Number / Email Id / Wallet ID.");
						walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						return walletResponseBean;
					}
					else
					{
						walletMast = walletUserDao.getUserByMobileNo(claim.get("emailId").toString(),walletRequestBean.getAggregatorId(),serverName);
					}
				}			
				else
				{
					walletMast = walletUserDao.getUserByMobileNo(claim.get("mobileNo").toString(),walletRequestBean.getAggregatorId(),serverName);
				}
				mobileNo=walletMast.getMobileno();
				walletId=walletMast.getWalletid();
			}
			else 
			{
				walletMast = walletUserDao.showUserProfile(claim.get("walletId").toString());
				if(walletMast == null)
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Wallet Id.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				mobileNo=walletMast.getMobileno();
				walletId=walletMast.getWalletid();
			}
			//to check combination of mobile number and aggreator ID
			 int flag = walletUserDao.verifyMobileNoAggId(mobileNo ,walletRequestBean.getAggregatorId(),serverName);
			 if(flag == 0)
			 {
				 responseMap.put("code", "1");
				 responseMap.put("response", "ERROR");
				 responseMap.put("message", "Invalid combination of Aggreator ID and mobile Number.");
			     walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				 return walletResponseBean;
			 }
			
			walletBalance = transactionDao.getWalletBalance(walletId);
			responseMap.put("code", "300");
			responseMap.put("response", "SUCCESS");
			responseMap.put("message", "SUCCESS.");
			responseMap.put("walletId",walletMast.getId());
			responseMap.put("mobileNo", mobileNo);
			responseMap.put("balance", walletBalance);
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
			
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in CheckWalletBalance"+ex.getMessage()+" "+ex
					);
			//logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in CheckWalletBalance************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
	}
	
	/**
	 * 
	 * @param walletRequestBean
	 * @param httpServletRequest
	 * @return
	 */
	@POST
	@Path("/getTransHistory")
	@Produces({MediaType.APPLICATION_XML , MediaType.APPLICATION_JSON})
	public WalletResponseBean getTransHistory(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		 
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		WalletMastBean walletMast = new WalletMastBean();
		List<PassbookBean> passbook = null;
		String startDt , endDt;
		String mobileNo="";
		String walletId="";
		String serverName =httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "getTransHistory");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|"+"getTransHistory()"+"|"+walletRequestBean.getRequest()
				);
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****getTransHistory******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			if(claim.get("startDate") == null || claim.get("startDate").toString().trim().isEmpty() || (!dateValidate(claim.get("startDate").toString(), "dd-MMM-yyyy") && !dateValidate(claim.get("startDate").toString(), "dd/MM/yyyy")))
			{
				startDt = null;
			}
			else
			{
				startDt = claim.get("startDate").toString();
			}
			if(claim.get("endDate") == null || claim.get("endDate").toString().trim().isEmpty() || (!dateValidate(claim.get("endDate").toString(), "dd-MMM-yyyy") && !dateValidate(claim.get("endDate").toString(), "dd/MM/yyyy")))
			{
				endDt =null;
			}
			else
			{
				endDt = claim.get("endDate").toString();
			}
			/*if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
			{*/
				if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
				{
					if(claim.get("emailId") == null || claim.get("emailId").toString().trim().isEmpty() || new EmailValidator().validate(claim.get("emailId").toString()))
					{
						if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
						{
							responseMap.put("code", "1");
							responseMap.put("response", "ERROR");
							responseMap.put("message", "Invalid Mobile Number / Email Id / Wallet ID.");
							walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
							return walletResponseBean;
						}
						else
						{
							walletMast = walletUserDao.getUserByMobileNo(claim.get("walletId").toString(),walletRequestBean.getAggregatorId(),serverName);
						}
					}
					else
					{
						walletMast = walletUserDao.getUserByMobileNo(claim.get("emailId").toString(),walletRequestBean.getAggregatorId(),serverName);
					}
				}			
				else
				{
					walletMast = walletUserDao.getUserByMobileNo(claim.get("mobileNo").toString(),walletRequestBean.getAggregatorId(),serverName);
				}
				mobileNo=walletMast.getMobileno();
				walletId=walletMast.getWalletid();
			/*}
			else 
			{
				walletMast = walletUserDao.getUserByWalletId(claim.get("walletId").toString(),serverName);
				if(walletMast == null)
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Wallet Id.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				mobileNo=walletMast.getMobileno();
				walletId=walletMast.getWalletid();
			}*/
			//to check combination of mobile number and aggreator ID
			 int flag = walletUserDao.verifyMobileNoAggId(mobileNo ,walletRequestBean.getAggregatorId(),serverName);
			 if(flag == 0)
			 {
				 responseMap.put("code", "1");
				 responseMap.put("response", "ERROR");
				 responseMap.put("message", "Invalid combination of Aggreator ID and mobile Number.");
			     walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				 return walletResponseBean;
			 }
			passbook = transactionDao.showPassbook(walletId,startDt,endDt);	
			List<HashMap> passbookResp = new ArrayList<HashMap>();
			
						
			Iterator<PassbookBean> itr = passbook.iterator();
			while(itr.hasNext())
			{
				PassbookBean passbk = itr.next();
				HashMap<String,Object> passbookMap = new HashMap<String,Object>();
				passbookMap.put("txnId",passbk.getTxnid());
				passbookMap.put("respTxnId", passbk.getResptxnid());
				passbookMap.put("txnDate", passbk.getTxndate());
				passbookMap.put("txnCredit",passbk.getTxncredit());
				passbookMap.put("txnDebit",passbk.getTxndebit());
				passbookMap.put("txnDesc",passbk.getTxndesc());
				passbookMap.put("closingBal", passbk.getClosingBal());
				passbookResp.add(passbookMap);
			}
			
			responseMap.put("code", "300");
			responseMap.put("response", "SUCCESS");
			responseMap.put("message", "SUCCESS.");
			responseMap.put("count", passbook.size());
			responseMap.put("passbook", passbookResp);
			responseMap.put("walletId",walletMast.getId());
			responseMap.put("mobileNo", mobileNo);
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
			
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in getTransHistory"+ex.getMessage()+" "+ex
					);
			//logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in getTransHistory************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
	}
	
	/**
	 * 
	 * @param walletRequestBean
	 * @param httpServletRequest
	 * @return
	 */
	@POST
	@Path("/getTransStatus")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public WalletResponseBean getTransStatus(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		WalletMastBean walletMast = new WalletMastBean();
		PassbookBean passbook = new PassbookBean();
		String serverName =httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "getTransHistory");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|"+"getTransStatus"+"|"+walletRequestBean.getRequest()
				);
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****getTransHistory******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			
			if(claim.get("txnId") == null || claim.get("txnId").toString().trim().isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Txn ID.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
					
			passbook = transactionDao.showTxnByTxnID(claim.get("txnId").toString());
			if(passbook == null)
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Transaction Id.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			walletMast =walletUserDao.getUserByWalletId(passbook.getWalletid() ,serverName);
			if(walletMast == null)
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "No Data Found For Given Wallet ID.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			responseMap.put("code", "300");
			responseMap.put("response", "SUCCESS");
			responseMap.put("message", "SUCCESS.");
			responseMap.put("status", passbook.getTxndesc());
			responseMap.put("creditAmount", passbook.getTxncredit());
			responseMap.put("debitAmount", passbook.getTxndebit());
			responseMap.put("txnDate", passbook.getTxndate());
			responseMap.put("walletId",walletMast.getId());
			responseMap.put("mobileNo", walletMast.getMobileno());
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
			
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in getTransHistory"+ex.getMessage()+" "+ex
					);
			logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in getTransHistory************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
	
	}
	
	/**
	 * 
	 * @param walletRequestBean
	 * @param httpServletRequest
	 * @return
	 */
	@POST
	@Path("/otpOperations")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public WalletResponseBean otpOperations(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		int otpOperation;
		String serverName =httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "otpOperations");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|"+"otpOperations"+"|"+walletRequestBean.getRequest()
				);
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****otpOperations******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			if(claim.get("otpOperation") == null || claim.get("otpOperation").toString().trim().isEmpty() || !(((Pattern.compile("^[123]{1}$")).matcher(claim.get("otpOperation").toString())).matches()))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid OTP Operation.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			otpOperation = Integer.parseInt(claim.get("otpOperation").toString());
			
			/*if(otpOperation == 1)
			{
				if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Mobile Number.");
				 	walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				String sendOtpstatus = walletUserDao.mobileValidation(claim.get("mobileNo").toString());
				if(sendOtpstatus.trim().equals("7002"))
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Mobile Number Already Registered With Us.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				if(sendOtpstatus.trim().equals("1001"))
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Mobile Number.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				if(sendOtpstatus.trim().equals("101"))
				{
					responseMap.put("code", "300");
					responseMap.put("response", "SUCCESS");
					responseMap.put("message", "OTP has been sent to sender mobile number.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
			}*/
			if(otpOperation == 1)
			{
				String userId = "";
				if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
				{
					if(claim.get("emailId") == null || claim.get("emailId").toString().trim().isEmpty() || !(new EmailValidator().validate(claim.get("emailId").toString())))
					{
						if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
						{
							responseMap.put("code", "1");
							responseMap.put("response", "ERROR");
							responseMap.put("message", "Invalid Mobile Number / Email ID / Wallet ID.");
							walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
							return walletResponseBean;
						}
						else
						{
							userId=claim.get("walletId").toString();
						}
					}
					else
					{
						userId=claim.get("emailId").toString();
					}
				}
				else
				{
					userId=claim.get("mobileNo").toString();
				}
				Boolean otpResendStatus = walletUserDao.otpResend(userId, walletRequestBean.getAggregatorId() );
				 if(otpResendStatus == true)
				 {
					 responseMap.put("code", "300");
					 responseMap.put("response", "SUCCESS");
					 responseMap.put("message", "OTP has been sent to sender mobile number.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
				 else
				 {
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid OTP.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
			}
			else if(otpOperation == 2 || otpOperation == 3)
			{
				String userId = "";
				if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("mobileNo").toString()))
				{
					if(claim.get("emailId") == null || claim.get("emailId").toString().trim().isEmpty() || !(new EmailValidator().validate(claim.get("emailId").toString())))
					{
						if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
						{
							responseMap.put("code", "1");
							responseMap.put("response", "ERROR");
							responseMap.put("message", "Invalid Mobile Number / Email ID / User ID.");
							walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
							return walletResponseBean;
						}
						else
						{
							userId=claim.get("walletId").toString();
						}
					}
					else
					{
						userId=claim.get("emailId").toString();
					}
				}
				else
				{
					userId=claim.get("mobileNo").toString();
				}
				if(otpOperation == 2)
				{
					if(claim.get("otp") == null || claim.get("otp").toString().trim().isEmpty())
					{
						responseMap.put("code", "1");
						responseMap.put("response", "ERROR");
						responseMap.put("message", "Invalid OTP.");
						walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						return walletResponseBean;
					}
					Boolean validateOtpStatus= walletUserDao.validateOTP(userId,walletRequestBean.getAggregatorId(),claim.get("otp").toString());
					if(validateOtpStatus == true)
					{
						responseMap.put("code", "300");
						 responseMap.put("response", "SUCCESS");
						 responseMap.put("message", "OTP Validated Successfully.");
						 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						 return walletResponseBean;
					}
					else
					{
						responseMap.put("code", "1");
						responseMap.put("response", "ERROR");
						responseMap.put("message", "Invalid OTP.");
						walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						return walletResponseBean;
					}
				}
				else if(otpOperation == 3)
				{
					 Boolean otpResendStatus = walletUserDao.otpResend(userId, walletRequestBean.getAggregatorId() );
					 if(otpResendStatus == true)
					 {
						 responseMap.put("code", "300");
						 responseMap.put("response", "SUCCESS");
						 responseMap.put("message", "OTP has been sent to sender mobile number.");
						 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						 return walletResponseBean;
					 }
					 else
					 {
						 responseMap.put("code", "1");
						 responseMap.put("response", "ERROR");
						 responseMap.put("message", "Invalid OTP.");
						 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						 return walletResponseBean;
					 }
				 }
				else
				{
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid OTP Operation.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				}
			}
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid OTP Operation.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in otpOperations"+ex.getMessage()+" "+ex
					);
			//logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in otpOperations************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
	}
	
	
	/**
	 * 
	 * @param walletRequestBean
	 * @param httpServletRequest
	 * @return
	 */
	@POST
	@Path("/walletTransactions")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public WalletResponseBean walletTransactions(WalletRequestBean walletRequestBean , @Context HttpServletRequest httpServletRequest)
	{
		
		WalletResponseBean walletResponseBean = new WalletResponseBean();
		int transactionOperation;
		WalletMastBean walletMast = new WalletMastBean();
		PassbookBean passbook = new PassbookBean();
		double amount =0.0;
		String walletId = null;
		String serverName = httpServletRequest.getServletContext().getInitParameter("serverName");
		String requestId = walletUserDao.saveWalletApiRequest(walletRequestBean.getAggregatorId(),walletRequestBean.getRequest(), serverName, "getTransHistory");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|"+"walletTransactions()"+"|"+walletRequestBean.getRequest()
				);
	
		//logger.info(serverName+"***RequestId***"+requestId+"****Start Execution****walletTransactions******************RequestString***"+walletRequestBean.getRequest());
		walletResponseBean.setRequestId(requestId);
		Double walletBalance = 0.0;
		if(walletRequestBean.getAggregatorId() == null || walletRequestBean.getAggregatorId().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Wallet Aggreator ID.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		if(walletRequestBean.getRequest() == null || walletRequestBean.getRequest().isEmpty())
		{
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Request.");
			walletResponseBean.setResponse(gson.toJson(responseMap));
			return walletResponseBean;
		}
		String mKey = "";
		try
		{
			mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(walletRequestBean.getAggregatorId());
			if(mKey == null || mKey.isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Aggreator ID.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			Claims claim =oxyJWTSignUtil.parseToken(walletRequestBean.getRequest(), mKey);
			if(claim == null )
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "JWT signature does not match.");
				walletResponseBean.setResponse(gson.toJson(responseMap));
				return walletResponseBean;
			}
			if(claim.get("transactionOperation") == null || claim.get("transactionOperation").toString().trim().isEmpty() || !(Pattern.compile("^[1234]{1}$").matcher(claim.get("transactionOperation").toString()).matches()))
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Transaction Operation.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			else
			{
				transactionOperation = Integer.parseInt(claim.get("transactionOperation").toString());
			}
			if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid User Id.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			/*if(claim.get("walletId") == null || claim.get("walletId").toString().trim().isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Wallet Id.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}*/
			walletMast = walletUserDao.showUserProfile(claim.get("walletId").toString());
			walletId = walletMast.getWalletid();
			
			if(claim.get("amount") == null || claim.get("amount").toString().trim().isEmpty())
			{
				responseMap.put("code", "1");
				responseMap.put("response", "ERROR");
				responseMap.put("message", "Invalid Amount.");
				walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
				return walletResponseBean;
			}
			amount = Double.parseDouble(claim.get("amount").toString());
			if(transactionOperation == 1)
			{
				if(claim.get("receiverMobileNo") == null || claim.get("receiverMobileNo").toString().trim().isEmpty() || !mobileValidate(claim.get("receiverMobileNo").toString()))
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Receiver Mobile Number.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				//String userId,String sendWalletid,String recMobile,double amount,String ipImei,String AggregatorId,String agent
				 String w2wStatus= transactionDao.walletToWalletTransfer(claim.get("walletId").toString(),walletId,claim.get("receiverMobileNo").toString(),amount,"",walletRequestBean.getAggregatorId(),"","");
				 switch(w2wStatus)
				 {
				 case "7000" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Message String.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7032" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Amount.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7036" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Money Cannot Be Transfered to same Wallet.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7023" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "No wallet Exist For Given Receiver Mobile Number.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7024" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Insufficient Balance.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "1000" :
					 responseMap.put("amount", amount);
					 responseMap.put("code", "300");
					 responseMap.put("response", "SUCCESS");
					 responseMap.put("message", "Money Trasferred.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 default :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Velocity check error.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
			}
			else if(transactionOperation == 2)
			{
				B2CMoneyTxnMast b2CMoneyTxnMast = new B2CMoneyTxnMast();
				if(claim.get("accountNo") == null || claim.get("accountNo").toString().trim().isEmpty() || !accountValidation(claim.get("accountNo").toString()))
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Account Number.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				if(claim.get("trnsTxnId") == null || claim.get("trnsTxnId").toString().trim().isEmpty())
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Transfer Transaction ID.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
			
				b2CMoneyTxnMast.setUserId(walletMast.getId());
				b2CMoneyTxnMast.setAggreatorId(walletRequestBean.getAggregatorId());
				/*b2CMoneyTxnMast.setAccHolderName(accHolderName);
				b2CMoneyTxnMast.setAccountNo(claim.get("accountNo").toString());
				b2CMoneyTxnMast.setIfscCode(ifscCode);*/
				b2CMoneyTxnMast.setAmount(amount);
				//String userId,String payeeWalletid,double amount,String accNumber,String trnsTxnId,String ipImei,String AggregatorId,String agent,double surCharge
				//String w2aStatus = transactionDao.walletToAccount(claim.get("walletId").toString(),walletId,amount,claim.get("accountNo").toString(),claim.get("trnsTxnId").toString(),"",walletRequestBean.getAggregatorId(),"",10.0);
				//b2CMoneyTxn(B2CMoneyTxnMast b2CMoneyTxnMast) 
				B2CMoneyTxnMast b2ctxn = transactionDao.b2CMoneyTxn(b2CMoneyTxnMast);
				String w2aStatus = b2ctxn.getStatusCode();
				switch(w2aStatus)
				 {
				 case "7000" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Message String.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7032" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Amount.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7024" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Insufficient Balance.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "1000" :
					 responseMap.put("amount", amount);
					 responseMap.put("code", "300");
					 responseMap.put("response", "SUCCESS");
					 responseMap.put("message", "Money Trasferred.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 default :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", b2ctxn.getStatusMsg());
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
			}
			else if(transactionOperation == 3)
			{

				if(claim.get("mid") == null || claim.get("mid").toString().trim().isEmpty())
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid MID.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				if(claim.get("trnsTxnId") == null || claim.get("trnsTxnId").toString().trim().isEmpty())
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Transfer Transaction ID.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				// (String userId,String payeeWalletid,double amount,String mid,String trnsTxnId,String ipImei,String AggregatorId,String agent);
				String w2mStatus = transactionDao.walletToMerchent(claim.get("walletId").toString(),walletId,amount,claim.get("mid").toString(),claim.get("trnsTxnId").toString(),"",walletRequestBean.getAggregatorId(),"");
				switch(w2mStatus)
				 {

				 case "7000" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Message String.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7032" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Amount.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7024" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Insufficient Balance.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "1000" :
					
				 default :
					 if(w2mStatus.contains("Success"))
					 {
						 responseMap.put("amount", amount);
						 responseMap.put("code", "300");
						 responseMap.put("response", "SUCCESS");
						 responseMap.put("message", "Money Trasferred.");
						 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
						 return walletResponseBean;
					 }
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Velocity check error.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
			}
			else if(transactionOperation == 4)
			{
				if(claim.get("mid") == null || claim.get("mid").toString().trim().isEmpty())
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid MID.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				if(claim.get("trnsTxnId") == null || claim.get("trnsTxnId").toString().trim().isEmpty())
				{
					responseMap.put("code", "1");
					responseMap.put("response", "ERROR");
					responseMap.put("message", "Invalid Transfer Transaction ID.");
					walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					return walletResponseBean;
				}
				// (String userId,String payeeWalletid,double amount,String mid,String trnsTxnId,String ipImei,String AggregatorId,String agent);
				String w2mStatus = transactionDao.walletToMerchent(claim.get("walletId").toString(),walletId,amount,claim.get("mid").toString(),claim.get("trnsTxnId").toString(),"",walletRequestBean.getAggregatorId(),"");
				switch(w2mStatus)
				 {

				 case "7000" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Message String.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7032" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Invalid Amount.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "7024" :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Insufficient Balance.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 case "1000" :
					 responseMap.put("amount", amount);
					 responseMap.put("code", "300");
					 responseMap.put("response", "SUCCESS");
					 responseMap.put("message", "Money Trasferred.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 default :
					 responseMap.put("code", "1");
					 responseMap.put("response", "ERROR");
					 responseMap.put("message", "Velocity check error.");
					 walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
					 return walletResponseBean;
				 }
			}
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Transaction Operation.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
		catch(NumberFormatException ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in walletTransactions"+ex.getMessage()+" "+ex
					);
					
			//logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in walletTransactions************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Amount.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
		catch(Exception ex)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletRequestBean.getAggregatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId +"|Problem in walletTransactions"+ex.getMessage()+" "+ex
					);
					
			//logger.info(serverName+"*****RequestId***"+requestId+"*********Problem in walletTransactions************e.message***"+ex.getMessage());
			ex.printStackTrace();
			responseMap.put("code", "1");
			responseMap.put("response", "ERROR");
			responseMap.put("message", "Invalid Message String.");
			walletResponseBean.setResponse(oxyJWTSignUtil.generateToken(responseMap, mKey));
			return walletResponseBean;
		}
	
	
	}
	
	public  boolean accountValidation(String account) {

	    Pattern pattern = Pattern.compile("^[0-9]{9,26}$");
	    Matcher matcher = pattern.matcher(account);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	public boolean mobileValidate(String mobileNo)
	{
		Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		Matcher matcher = pattern.matcher(mobileNo);
		if(matcher.matches())
			return true;
		else
			return false;
	}
	
	public boolean nameValidate(String name)
	{
		Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
		Matcher matcher = pattern.matcher(name);
		if(matcher.matches())
			return true;
		else
			return false;
	}
	
	public boolean dateValidate(String dateString , String dateFormat)
	{
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		try
		{
			Date date = df.parse(dateString);
		}
		catch(ParseException e)
		{
			e.printStackTrace();
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void main(String arg[])
	{
		WalletResponseBean resp = new WalletResponseBean();
		HttpServletRequest httpServletRequest = null;
		String key="e391ab57590132714ad32da9acf3013eb88c";
		WalletRequestBean walletRequestBean = new WalletRequestBean();
		HashMap<String, Object> test = new HashMap<String,Object>();
		walletRequestBean.setAggregatorId("AGGR001035");
		
		test.put("emailId", "shweta.mriu@gmail.com");
		test.put("name", "Shweta");
		test.put("password", "Shweta@27");
		test.put("txnId", "AAMF001003");
		//test.put("walletId", "993504128730082016122217");
		test.put("otpOperation", "2");
		test.put("otp", "962427");
		//test.put("mobileNo", "7011605528");//for new user
		test.put("mobileNo", "9810471689");
		test.put("transactionOperation", "1");
		test.put("walletId","CUST001153");
		//test.put("walletId", "981047168911082017114755");
		test.put("amount", "120");
		test.put("receiverMobileNo", "8505899301");
		walletRequestBean.setRequest(new BPJWTSignUtil().generateToken(test, key));
		walletRequestBean.setRequest("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3YWxsZXRJZCI6IkNVU1QwMDEyMDEifQ.3B0LBX1AiQHFeso1_haOQ_VW0gq4kgJDHRdkhyVsRZk");
		//resp = new WalletAPIManager().creatWalletRequest( walletRequestBean, httpServletRequest );
		//resp = new WalletAPIManager().CheckWalletBalance( walletRequestBean, httpServletRequest );
		//resp = new WalletAPIManager().getTransStatus( walletRequestBean, httpServletRequest );
		//resp = new WalletAPIManager().getTransHistory( walletRequestBean, httpServletRequest );
		//resp = new WalletAPIManager().otpOperations( walletRequestBean, httpServletRequest );
		resp = new WalletAPIManager().getTransHistory( walletRequestBean, httpServletRequest );
		//System.out.println("-------------------------Response-----------"+new OxyJWTSignUtil().parseToken(resp.getResponse(), key));
		/*try
		{
		java.util.Date sqlDate = new java.util.Date(System.currentTimeMillis()); 
		SimpleDateFormat format = new SimpleDateFormat("ddmmyyyyHHmm");
		Date parsed = format.parse(sqlDate.toString());
		java.sql.Date sql = new java.sql.Date(parsed.getTime());
		System.out.println(sqlDate);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}*/
		
	}
}
