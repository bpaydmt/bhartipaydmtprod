package com.bhartipay.lean.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="idcreationcharge")
public class IdCreationCharge {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="userid")
    private String userId;
    
	@Column(name="walletid")
	private String walletId;
	
    @Column(name="amount")
    private double amount;

    @Column(name="aggregatorid")
    private String aggregatorId;
    
    @Column(name="txncode")
    private int txnCode;
     
    @Column(name="status")
    private String status; 
    
    @Column(name="charges")
    private String charges; 
    
    public IdCreationCharge() {
   	 
	}
    
	 
	
	public IdCreationCharge(String userId, String walletId, double amount, String aggregatorId, int txnCode, String status,String charges) {
		
		this.userId = userId;
		this.walletId = walletId;
		this.amount = amount;
		this.aggregatorId = aggregatorId;
		this.txnCode = txnCode;
		this.status = status;
		this.charges = charges;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}



	public int getTxnCode() {
		return txnCode;
	}



	public void setTxnCode(int txnCode) {
		this.txnCode = txnCode;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getCharges() {
		return charges;
	}



	public void setCharges(String charges) {
		this.charges = charges;
	}

        
}
