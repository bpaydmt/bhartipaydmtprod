package com.bhartipay.lean.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="distributoridallot")
public class DistributorIdAllot {

	private static final long serialVersionUID = 1L;

	
	
	@Id
	@Column(name="userid")
    private String userId;
	@Column(name="alltoken")
	private int allToken;
	@Column(name="consumetoken")
	private int consumeToken;
	@Column(name="remainingtoken")
	private int remainingToken;
	@Column(name="aggregatorid")
	private String aggregatorId;
	
	public DistributorIdAllot() {
	}

	public DistributorIdAllot(String userId, int allToken, int consumeToken, int remainingToken,String aggregatorId) {
		super();
		this.userId = userId;
		this.allToken = allToken;
		this.consumeToken = consumeToken;
		this.remainingToken = remainingToken;
		this.aggregatorId=aggregatorId;
	}
 

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getAllToken() {
		return allToken;
	}

	public void setAllToken(int allToken) {
		this.allToken = allToken;
	}

	public int getConsumeToken() {
		return consumeToken;
	}

	public void setConsumeToken(int consumeToken) {
		this.consumeToken = consumeToken;
	}

	public int getRemainingToken() {
		return remainingToken;
	}

	public void setRemainingToken(int remainingToken) {
		this.remainingToken = remainingToken;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	
	
	
}
