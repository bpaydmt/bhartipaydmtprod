package com.bhartipay.lean.bean;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="cmsledger")
public class CmsBean {

	@Id
	@Column(name="txnid")
	private String txnId;
	@Column(name="agentid")
	private String agentId;
	@Column(name="amount")
	private String amount;
	@Column(name="status")
	private String status;
	@Column(name="apiid")
	private String apiId;
	@Column(name="operator")
	private String operator;
	@Column(name="action")
	private String action;
	@Column(name="mobileno")
	private String mobileNo;
	@Column(name="bankname")
	private String bankName;
	@Column(name="accountno")
	private String accountNo;
	@Column(name="referenceid")
	private String referenceId;
	@Column(name="aggregatorid")
	private String aggregatorId;
	@Column(name="resptxnid")
	private String respTxnid;
	@Column(name = "txnDate")
	private Timestamp txnDate;
	
	@Transient
	private String agentAuthId;
	@Transient
	private String agentAuthPassword;
	
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApiId() {
		return apiId;
	}
	public void setApiId(String apiId) {
		this.apiId = apiId;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	public String getRespTxnid() {
		return respTxnid;
	}
	public void setRespTxnid(String respTxnid) {
		this.respTxnid = respTxnid;
	}
	 
	public String getAgentAuthId() {
		return agentAuthId;
	}
	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}
	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}
	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}
	public Timestamp getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}

	
	
	
	
	
}
