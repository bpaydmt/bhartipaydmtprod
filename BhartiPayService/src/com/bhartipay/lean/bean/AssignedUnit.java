package com.bhartipay.lean.bean;

public class AssignedUnit {

    private String userId; 
	private int allToken; 
	private int consumeToken; 
	private int remainingToken; 
	private String aggregatorId;
	private String salesId;
	private String managerId;
	private int usertype;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getAllToken() {
		return allToken;
	}
	public void setAllToken(int allToken) {
		this.allToken = allToken;
	}
	public int getConsumeToken() {
		return consumeToken;
	}
	public void setConsumeToken(int consumeToken) {
		this.consumeToken = consumeToken;
	}
	public int getRemainingToken() {
		return remainingToken;
	}
	public void setRemainingToken(int remainingToken) {
		this.remainingToken = remainingToken;
	}
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	public String getSalesId() {
		return salesId;
	}
	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	
	
	
	
}
