package com.bhartipay.lean.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dmtpricing")
public class DmtPricing
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="userid")
    private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	
	//@Column(name="distributorid")
	//private String distributorId;
	
	//@Column(name="name")
	//private String name;
	
	//@Column(name="walletid")
	//private String walletId;
	
	@Column(name="rangefrom")
	private Integer rangeFrom;
	
	@Column(name="rangeto")
	private Integer rangeTo;
	
	@Column(name="distributorpercentage")
	private double distributorPercentage;
	
	@Column(name="superdistributorpercentage")
	private double superDistributorPercentage;
	
	//@Column(name="agentpercentage")
	//private double agentPercentage;
	
	private boolean status;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

		public Integer getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(Integer rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	public Integer getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(Integer rangeTo) {
		this.rangeTo = rangeTo;
	}

	public double getDistributorPercentage() {
		return distributorPercentage;
	}

	public void setDistributorPercentage(double distributorPercentage) {
		this.distributorPercentage = distributorPercentage;
	}

	public double getSuperDistributorPercentage() {
		return superDistributorPercentage;
	}

	public void setSuperDistributorPercentage(double superDistributorPercentage) {
		this.superDistributorPercentage = superDistributorPercentage;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	
	
}
