package com.bhartipay.lean.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="leanAmount")
public class LeanAccount implements Serializable 
{

	private static final long serialVersionUID = -8889359917898893779L;	
	
	@Id
	@Column(name="userid",length =30)
	private String userId;
	
	@Column(name="aggregatorid")
	private String aggregatorId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="walletid",length =50,nullable = false,updatable=false)
	private String walletId;
	
	@Column(name = "amount",  columnDefinition="Decimal(10,2)" )
	private double amount;
	
	@Column(name="status")
	private boolean status;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
		
	
}
