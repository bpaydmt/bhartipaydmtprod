package com.bhartipay.lean.bean;

import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.user.bean.WalletBalanceBean;
import com.bhartipay.util.DBUtil;

public class ActivationCharge  extends  TimerTask{

public static final Logger logger=Logger.getLogger(ActivationCharge.class);
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	@Override
	public synchronized void run() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular started");

		debitActivationCharge();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","schedular finished his task");
	}
	

	public void debitActivationCharge()
	{
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		
		try {
			Query query = session .createQuery("from IdCreationCharge where status='1' and aggregatorid='OAGG001050' ");
			List<IdCreationCharge> list = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  debitActivationCharge  "+list.size());
			logger.info(" debitActivationCharge  ***************************************:"+list.size());
			Iterator<IdCreationCharge> ite = list.iterator();
			while (ite.hasNext()) {
				IdCreationCharge mast=ite.next(); 
				String userId = mast.getUserId();
				
				
				WalletBalanceBean agentWalletMast = (WalletBalanceBean) session.get(WalletBalanceBean.class, mast.getWalletId()); 
				System.out.println(userId+"     "+agentWalletMast);
				
				if(agentWalletMast.getFinalBalance()>=250.0)
				{
					String queryNext = "SELECT nextval(:ad,:aggregatorid)";
					SQLQuery nextQuery = session.createSQLQuery(queryNext);
					   nextQuery.setParameter("aggregatorid", mast.getAggregatorId());
					   nextQuery.setParameter("ad", "REVOKE");
			        Object nextObj = nextQuery.uniqueResult();
			        String txnId=""+nextObj;
					
			        transaction=session.beginTransaction();
			        String str = "CALL debitwallet('" + mast.getAggregatorId() + "','" + mast.getWalletId() + "'," + mast.getAmount() + ",'Allot ID Charge','"+ txnId + "',113)";
			        System.out.println(" str   "+str);
			        Query debitQuery = session.createSQLQuery(str);
			        debitQuery.executeUpdate();
			     
			    	mast.setStatus("0");
			    	session.saveOrUpdate(mast);
			    	
			    	transaction.commit();
			     }
				
			 }
		 }catch(Exception e)
		 {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in debitActivationCharge"+e.getMessage()+" "+e);
				e.printStackTrace();
				
		 } finally {
				session.close();
		 }		
	}
	
}
