package com.bhartipay.lean.bean;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="agentbillpayrequest")
public class AgentBillPayRequest 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	private String userId; 
	private String requestDate;
    private double txnAmount;
    private String requestId;
	private String billerId;
	private String caNumber;
	private Timestamp creationDate; 
	
	public AgentBillPayRequest() {
	
	}
	public AgentBillPayRequest(String userId, Timestamp creationDate,String requestDate, double txnAmount, String requestId, String billerId,String caNumber) {
		this.userId = userId;
		this.creationDate = creationDate;
		this.requestDate = requestDate;
		this.txnAmount = txnAmount;
		this.requestId = requestId;
		this.billerId = billerId;
		this.caNumber = caNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public double getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(double txnAmount) {
		this.txnAmount = txnAmount;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getBillerId() {
		return billerId;
	}
	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}
	 
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public String getCaNumber() {
		return caNumber;
	}
	public void setCaNumber(String caNumber) {
		this.caNumber = caNumber;
	}
	 
	
	
	
}
