package com.bhartipay.lean.bean;

public class OnboardStatusResponse 
{

	private String icicionboardstatus;
	
	private String finoonboardstatus;
	
	private String yesonboardstatus;
	
	private String status;
	
	private String message;
	private String localizedMessage;

	public String getIcicionboardstatus() {
		return icicionboardstatus;
	}

	public void setIcicionboardstatus(String icicionboardstatus) {
		this.icicionboardstatus = icicionboardstatus;
	}

	public String getFinoonboardstatus() {
		return finoonboardstatus;
	}

	public void setFinoonboardstatus(String finoonboardstatus) {
		this.finoonboardstatus = finoonboardstatus;
	}

	public String getYesonboardstatus() {
		return yesonboardstatus;
	}

	public void setYesonboardstatus(String yesonboardstatus) {
		this.yesonboardstatus = yesonboardstatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLocalizedMessage() {
		return localizedMessage;
	}

	public void setLocalizedMessage(String localizedMessage) {
		this.localizedMessage = localizedMessage;
	}
	

	
	
}
