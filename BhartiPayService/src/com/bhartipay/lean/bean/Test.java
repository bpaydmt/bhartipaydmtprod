package com.bhartipay.lean.bean;

import java.io.File;

import javax.mail.Multipart;

public class Test {

	private String userId;
	private String pan;
	private Multipart file;
	private File file1;
	private String file1FileName;
	private String file1ContentType;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public File getFile1() {
		return file1;
	}
	public void setFile1(File file1) {
		this.file1 = file1;
	}
	public String getFile1FileName() {
		return file1FileName;
	}
	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}
	public String getFile1ContentType() {
		return file1ContentType;
	}
	public void setFile1ContentType(String file1ContentType) {
		this.file1ContentType = file1ContentType;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public Multipart getFile() {
		return file;
	}
	public void setFile(Multipart file) {
		this.file = file;
	}
	
	
	
}
