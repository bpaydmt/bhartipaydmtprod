package com.bhartipay.lean.bean;

import java.sql.Date;

public class MappingReport {

	private String id;
	private String distributerid;
	private String superdistributerid;
	private String salesId;
	private String managerId;
	private String aggreatorid;
	private String userstatus;
	private int usertype;
	private Date approveDate;
	private String subAggregatorId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDistributerid() {
		return distributerid;
	}
	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}
	public String getSuperdistributerid() {
		return superdistributerid;
	}
	public void setSuperdistributerid(String superdistributerid) {
		this.superdistributerid = superdistributerid;
	}
	public String getSalesId() {
		return salesId;
	}
	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getUserstatus() {
		return userstatus;
	}
	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public String getSubAggregatorId() {
		return subAggregatorId;
	}
	public void setSubAggregatorId(String subAggregatorId) {
		this.subAggregatorId = subAggregatorId;
	}
	
	
	
}
