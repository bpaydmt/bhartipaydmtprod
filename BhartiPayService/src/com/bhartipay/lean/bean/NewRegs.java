package com.bhartipay.lean.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="newregs")
public class NewRegs {
	

	@Id
	public String mobile;
	public String name;
	public String email;
	public String website;
	public String description;
	
	public NewRegs() {
	}
	
	public NewRegs(String mobile, String name, String email, String website, String description) {
		this.mobile = mobile;
		this.name = name;
		this.email = email;
		this.website = website;
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
    
	
	
}
