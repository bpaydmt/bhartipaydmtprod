package com.bhartipay.lean.bean;

public class BbpsStatusUpdateResponse 
{

	private String status;
	private String txnId;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	
	
	
}
