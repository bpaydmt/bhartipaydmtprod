package com.bhartipay.lean.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="addbankaccount")
public class AddBankAccount implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="userid")
    private String userId;
    
	@Column(name="walletid")
	private String walletId;
	
    @Column(name="aggregatorid")
    private String aggregatorId;
    
    @Column(name="name")
    private String name;
    
    @Column(name="bankname")
    private String bankName;
    
    @Column(name="accountnumber")
    private String accountNumber;
	
    @Column(name="ifsccode")
    private String ifscCode;
	
    @Column(name="mobileno")
    private String mobileNo;
    
    @Column(name="status")
    private boolean status;  

    @Column(name="statuscode")
    private int statusCode; 
    
    @Column(name="comment") 
    private String comment;
    
	public AddBankAccount() {
		
	}

	
	
	public AddBankAccount(String userId, String walletId, String aggregatorId, String name, String bankName,
			String accountNumber, String ifscCode, String mobileNo, boolean status) {
		this.userId = userId;
		this.walletId = walletId;
		this.aggregatorId = aggregatorId;
		this.name = name;
		this.bankName = bankName;
		this.accountNumber = accountNumber;
		this.ifscCode = ifscCode;
		this.mobileNo = mobileNo;
		this.status = status;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}



	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}



	public int getStatusCode() {
		return statusCode;
	}



	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	
	
	
	
	
	
}
