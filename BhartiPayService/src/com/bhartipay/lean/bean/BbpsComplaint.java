package com.bhartipay.lean.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bbpscomplaint")
public class BbpsComplaint 
{

	@Id
	@Column(name="complaintid")
	private String complaintId;
	
	@Column(name="userid")
	private String userId;
	
	@Column(name="txnrefid")
	private String txnRefId;
	
	@Column(name="requestid")
	private String requestId;
	
	@Column(name="complaintdisposition")
	private String complaintDisposition;
	
	@Column(name="complaintdesc")
	private String complaintDesc;
	
	@Column(name="complaintassigned")
	private String complaintAssigned;
	
	@Column(name="responsecode")
	private String responseCode;
	
	@Column(name="responsereason")
	private String responseReason;

	
	
	public BbpsComplaint() { 
	}

	public BbpsComplaint(String userId, String txnRefId, String requestId, String complaintDisposition,
			String complaintDesc, String complaintId, String complaintAssigned, String responseCode,
			String responseReason) {
	
		this.userId = userId;
		this.txnRefId = txnRefId;
		this.requestId = requestId;
		this.complaintDisposition = complaintDisposition;
		this.complaintDesc = complaintDesc;
		this.complaintId = complaintId;
		this.complaintAssigned = complaintAssigned;
		this.responseCode = responseCode;
		this.responseReason = responseReason;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTxnRefId() {
		return txnRefId;
	}

	public void setTxnRefId(String txnRefId) {
		this.txnRefId = txnRefId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getComplaintDisposition() {
		return complaintDisposition;
	}

	public void setComplaintDisposition(String complaintDisposition) {
		this.complaintDisposition = complaintDisposition;
	}

	public String getComplaintDesc() {
		return complaintDesc;
	}

	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}

	public String getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(String complaintId) {
		this.complaintId = complaintId;
	}

	public String getComplaintAssigned() {
		return complaintAssigned;
	}

	public void setComplaintAssigned(String complaintAssigned) {
		this.complaintAssigned = complaintAssigned;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseReason() {
		return responseReason;
	}

	public void setResponseReason(String responseReason) {
		this.responseReason = responseReason;
	}
	

	
	
}
