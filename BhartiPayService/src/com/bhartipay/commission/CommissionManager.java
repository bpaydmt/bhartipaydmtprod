package com.bhartipay.commission;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.commission.bean.CommInputBean;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.commission.bean.CommPlanMaster;
import com.bhartipay.commission.bean.CommRequest;
import com.bhartipay.commission.bean.CommissionBean;
import com.bhartipay.commission.persistence.CommissionDao;
import com.bhartipay.commission.persistence.CommissionDaoImpl;
import com.bhartipay.report.bean.ReconciliationReport;
import com.bhartipay.report.bean.ReportBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Path("/CommissionManager")
public class CommissionManager {
	private static final Logger logger = Logger.getLogger(CommissionManager.class.getName());
	
	CommissionDao commissionDao=new CommissionDaoImpl();
	
	
	@POST
	@Path("/createPlan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CommPlanMaster createPlan(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|createPlan()"
				);
		//logger.info("Start excution ******************************************************* method createPlan(commPlanMaster)");
		commPlanMaster.setIpiemi(imei);
		commPlanMaster.setAgent(agent);
		return commissionDao.createPlan(commPlanMaster);
		
	}
	
	@POST
	@Path("/getPlanDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	public String getPlanDtl(CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|getPlanDtl()");
		//logger.info("Start excution ******************************************************* method getPlanDtl()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commissionDao.getPlanDtl(commPlanMaster.getAggreatorid()));
	}
	
	@POST
	@Path("/getTxnTypeDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getTxnTypeDtl(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|getTxnTypeDtl()"
				);
		//logger.info("Start excution ******************************************************* method getTxnTypeDtl()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commissionDao.getTxnTypeDtl());
	}
	
	
	
	
	
	
	@POST
	@Path("/getAllPlans")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getAllPlans(CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|getAllPlans()"
				);
		//logger.info("Start excution ******************************************************* method getPlanDtl()"+commPlanMaster.getAggreatorid());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commissionDao.getAllPlans(commPlanMaster.getAggreatorid()));
	}
	
	
	
	@POST
	@Path("/getCommPercDtl")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public  CommPercMaster getCommPerc(CommInputBean commInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|getCommPerc()"
				);
		//logger.info("Start excution ******************************************************* method getCommPerc(commInputBean)");
		return commissionDao.getCommPerc(commInputBean);
		
	}
	
	@POST
	@Path("/updatePlan")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CommPlanMaster updatePlan(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|updatePlan()"
				);
		//logger.info("Start excution ******************************************************* method updatePlan(commPlanMaster)");
		commPlanMaster.setIpiemi(imei);
		commPlanMaster.setAgent(agent);
		return commissionDao.updatePlan(commPlanMaster);
		
	}
	
	
	
	
	
	@POST
	@Path("/saveUpdateCommPerc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CommPercMaster saveUpdateCommPerc(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,CommInputBean commInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|saveUpdateCommPerc()"
				);
		//logger.info("Start excution ******************************************************* method saveUpdateCommPerc(commPercMaster)");
		return commissionDao.saveUpdateCommPerc(commInputBean,imei,agent);
		
	}
	
	
/*	@POST
	@Path("/assignCommission")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public ReportBean assignCommission(ReportBean reportBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",   "|assignCommission()"
				);
		//logger.info("Start excution ******************************************************* method saveUpdateCommPerc(commPercMaster)");
		return commissionDao.assignCommission(reportBean);
		
	}*/
	
	@POST
	@Path("/getCommission")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCommission(ReportBean reportBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",   "|getCommission()"
				);
		//logger.info("Start excution ******************************************************* method getCommission(reportBean)");	
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
		return  gson.toJson(commissionDao.getCommission(reportBean.getUserId()));
	}
	

	@POST
	@Path("/ShowCommByPlanId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String ShowCommByPlanId(ReportBean reportBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",   "|ShowCommByPlanId()"+reportBean.getPlanId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",   "|ShowCommByPlanId()"+reportBean.getTxnType());		
		//logger.info("Start excution ******************************************************* method ShowCommByPlanId(reportBean)"+reportBean.getPlanId());
		//logger.info("Start excution ***********************************getTxnType******************** method ShowCommByPlanId(reportBean)"+reportBean.getTxnType());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return  gson.toJson(commissionDao.ShowCommByPlanId(reportBean.getPlanId(),Integer.parseInt(reportBean.getTxnType())));
	    
	    
	   // List <CommPercMaster> list= commissionDao.ShowCommByPlanId(reportBean.getPlanId(),Integer.parseInt(reportBean.getTxnType()));
	   // ReconciliationReport reconciliationReport=new ReconciliationReport();
	   // reconciliationReport.setPercList(list);
		//return  gson.toJson(list);
	   // return reconciliationReport;
	}
	@POST
	@Path("/AssignCommission")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String assignCommission(CommRequest assignComm){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",assignComm.getAgentid(), "", "",   "|assignCommissioin(assignComm)"+assignComm.getAgentid());		
	
	//	logger.info("Start excution ******************************************************* method assignCommissioin(assignComm)"+assignComm.getAgent());
	  return commissionDao.assignCommission(assignComm);
	}
	
	
	@POST
	@Path("/GetCommissionByAgentId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public List<CommissionBean> getCommissionByAgentId(CommissionBean commBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|assignCommissioin(assignComm)"+commBean.getAgent());		
		
	//	logger.info("Start excution ******************************************************* method assignCommissioin(assignComm)"+commBean.getAgent());
		return commissionDao.getCommission(commBean);
	}
	@POST
	@Path("/ShowPlan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CommPlanMaster ShowPlan(ReportBean reportBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",   "|ShowPlan()"+reportBean.getPlanId());		
		
		//logger.info("Start excution ******************************************************* method ShowPlan(planId)"+reportBean.getPlanId());
		return commissionDao.ShowPlan(reportBean.getPlanId());
	}
	
	@POST
	@Path("/ShowCommByCommId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public CommInputBean ShowCommByCommId(CommInputBean commInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|ShowCommByCommId()"+commInputBean.getCommid()
				);
		//logger.info("Start excution ******************************************************* method ShowCommByCommId(planId)"+commInputBean.getCommid());
		return commissionDao.ShowCommByCommId(commInputBean);
	}
	
	
	
}
