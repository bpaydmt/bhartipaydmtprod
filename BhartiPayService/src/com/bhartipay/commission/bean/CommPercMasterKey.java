package com.bhartipay.commission.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CommPercMasterKey implements Serializable{
	
	@Column(name="planid", nullable = false,length=20)
	private String planid ;
	
	
	@Column(name="txnid", nullable = false,length=4)
	private int txnid ;


	public String getPlanid() {
		return planid;
	}


	public void setPlanid(String planid) {
		this.planid = planid;
	}


	public int getTxnid() {
		return txnid;
	}


	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}




	
	
	
	
	
	
	

}
