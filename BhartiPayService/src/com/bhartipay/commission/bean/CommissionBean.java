package com.bhartipay.commission.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table (name="commpercmaster")
public class CommissionBean {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="commid")	
	private int commid;
	
	@Column(name="planid")
	private String planId;
	
	@Transient
	private String distributorId;
	
	@Transient
	private String agentId;
	
	
	@Column(name="agentperc")
	private double agentPer;
	
	@Column(name="maxfixedcharge")
	private double maxCharges;	
	
	@Column(name="ismaxfixedapply" , columnDefinition = "int DEFAULT 0")
	private int maxChargesCheck;	
	
	@Column(name="distributorperc")
	private double disPer;
	
	@Column(name="superdistperc")
	private double superDisPer;
	
	@Column(name="servicetax")
	private double serviceCharges=18;
	
	@Column(name="rangeto")
	private double rangeTo;
	
	@Column(name="rangefrom")
	private double rangeFrom;
	
	@Column(name="fixedcharge")
	private double fixedCharges;
	
	@Column(name="txntype")
	private String txntype;
	
	@Transient
	private int fixedChargesCheck;
	
	@Column(name="ipiemi")
	private String ipiemi;
	
	@Column(name="agent")
	private String agent;

	@Column(name="txnid")
	private int txnid=0;
	
	@Column(name="fixedorperc")
	private String fixedorperc;
	
	@Column(name="description")       
	private String description;
	
	@Column(name="aggregatorperc")
	private double aggregatorperc; 
	
	@Column(name="commperc")
	private double commperc;  
	
	@Column(name="wlaggreatorchg")           
	private double wlaggreatorchg;  
	
	@Column(name="wlaggreatorservicechg")
	private double wlaggreatorservicechg; 

	@Column(name="bankrefid")
	private int bankrefid;
	
	@Column(name="subagentperc")
	private double subagentperc;
	
	
	
	public double getSubagentperc() {
		return subagentperc;
	}

	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}

	public int getBankrefid() {
		return bankrefid;
	}

	public void setBankrefid(int bankrefid) {
		this.bankrefid = bankrefid;
	}

	
	
	public String getDescription() {
		return description;
	}

	public double getAggregatorperc() {
		return aggregatorperc;
	}

	public double getCommperc() {
		return commperc;
	}

	public double getWlaggreatorchg() {
		return wlaggreatorchg;
	}

	public double getWlaggreatorservicechg() {
		return wlaggreatorservicechg;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}

	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}

	public void setWlaggreatorchg(double wlaggreatorchg) {
		this.wlaggreatorchg = wlaggreatorchg;
	}

	public void setWlaggreatorservicechg(double wlaggreatorservicechg) {
		this.wlaggreatorservicechg = wlaggreatorservicechg;
	}

	public String getFixedorperc() {
		return fixedorperc;
	}

	public void setFixedorperc(String fixedorperc) {
		this.fixedorperc = fixedorperc;
	}

	public int getTxnid() {
		return txnid;
	}

	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}

	public String getTxntype() {
		return txntype;
	}

	public void setTxntype(String txntype) {
		this.txntype = txntype;
	}

	public int getCommid() {
		return commid;
	}

	public void setCommid(int commid) {
		this.commid = commid;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public double getAgentPer() {
		return agentPer;
	}

	public void setAgentPer(double agentPer) {
		this.agentPer = agentPer;
	}

	public double getMaxCharges() {
		return maxCharges;
	}

	public void setMaxCharges(double maxCharges) {
		this.maxCharges = maxCharges;
	}

	public int getMaxChargesCheck() {
		return maxChargesCheck;
	}

	public void setMaxChargesCheck(int maxChargesCheck) {
		this.maxChargesCheck = maxChargesCheck;
	}

	public double getDisPer() {
		return disPer;
	}

	public void setDisPer(double disPer) {
		this.disPer = disPer;
	}

	public double getSuperDisPer() {
		return superDisPer;
	}

	public void setSuperDisPer(double superDisPer) {
		this.superDisPer = superDisPer;
	}

	public double getServiceCharges() {
		return serviceCharges;
	}

	public void setServiceCharges(double serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public double getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(double rangeTo) {
		this.rangeTo = rangeTo;
	}

	public double getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(double rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	public double getFixedCharges() {
		return fixedCharges;
	}

	public void setFixedCharges(double fixedCharges) {
		this.fixedCharges = fixedCharges;
	}

	public int getFixedChargesCheck() {
		return fixedChargesCheck;
	}

	public void setFixedChargesCheck(int fixedChargesCheck) {
		this.fixedChargesCheck = fixedChargesCheck;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	
	
}
