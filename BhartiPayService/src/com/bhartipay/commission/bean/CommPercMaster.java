package com.bhartipay.commission.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.IndexColumn;

//@Entity(name = "ForeignKeyAssoEntity")
@Entity
@Table(name="commpercmaster")

public class CommPercMaster implements Serializable{
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="commid", nullable = false,length=5)
	private int commid ;
	
	
	@Column(name="planid", nullable = false,length=20)
	private String planid ;
	
	@Column(name="txnid", nullable = false,length=4)
	private int txnid ;

	@Column(name="rangefrom")
	private double rangeFrom ;
	
	@Column(name="rangeto" )
	private double rangeTo ;
	
	
	@Column(name="servicetax", nullable = true, length=10)
	private double servicetax;
	
	@Column(name="bankrefid", nullable = true, length=4)
	private int bankrefid;
	
	@Column(name="description", nullable = true, length=200)
	private String description;


	@Column(name="sbcess", nullable = true, length=10)
	private double sbcess;
	
	@Column(name="kycess", nullable = true, length=10)
	private double kycess;
	
	@Column(name="aggregatorperc", nullable = true, length=10)
	private double aggregatorperc;
	
	@Column(name="distributorperc", nullable = true, length=10)
	private double distributorperc;

	@Column(name="superdistperc", nullable = true, length=10)
	private double superdistperc;
	
	@Column(name="agentperc", nullable = true, length=10)
	private double agentperc;
	
	@Column(name="subagentperc", nullable = true, length=10)
	private double subagentperc;
	
	@Column(name="commperc", nullable = true, length=10)
	private double commperc;
	
	//private Set<CommPercRangeMaster> commPercRangeMaster;
	
	@OneToMany(fetch=FetchType.EAGER, targetEntity=CommPercRangeMaster.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "comm_id", referencedColumnName="commid")
 
	//private Set children;
	
	private Set<CommPercRangeMaster> commPercRangeMaster;

	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	
	
	@Transient
	private String statuscode ;
	
	//@Transient
	//private Set<CommPercRangeMaster> commPercRangeMaster ;


	
	
	
	

	public Set<CommPercRangeMaster> getCommPercRangeMaster() {
		return commPercRangeMaster;
	}



	public void setCommPercRangeMaster(Set<CommPercRangeMaster> commPercRangeMaster) {
		this.commPercRangeMaster = commPercRangeMaster;
	}



	/*public Set getChildren() {
		return children;
	}



	public void setChildren(Set children) {
		this.children = children;
	}*/



	public int getCommid() {
		return commid;
	}



	public void setCommid(int commid) {
		this.commid = commid;
	}



	public String getPlanid() {
		return planid;
	}



	public void setPlanid(String planid) {
		this.planid = planid;
	}



	public int getTxnid() {
		return txnid;
	}



	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}



	public double getServicetax() {
		return servicetax;
	}



	public void setServicetax(double servicetax) {
		this.servicetax = servicetax;
	}



	public int getBankrefid() {
		return bankrefid;
	}



	public void setBankrefid(int bankrefid) {
		this.bankrefid = bankrefid;
	}










	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public double getSbcess() {
		return sbcess;
	}



	public void setSbcess(double sbcess) {
		this.sbcess = sbcess;
	}



	public double getKycess() {
		return kycess;
	}



	public void setKycess(double kycess) {
		this.kycess = kycess;
	}



	public double getAggregatorperc() {
		return aggregatorperc;
	}



	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}



	public double getDistributorperc() {
		return distributorperc;
	}



	public void setDistributorperc(double distributorperc) {
		this.distributorperc = distributorperc;
	}



	public double getAgentperc() {
		return agentperc;
	}



	public void setAgentperc(double agentperc) {
		this.agentperc = agentperc;
	}



	public double getSubagentperc() {
		return subagentperc;
	}



	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}



	public double getCommperc() {
		return commperc;
	}



	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}



	public String getIpiemi() {
		return ipiemi;
	}



	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}



	public String getAgent() {
		return agent;
	}



	public void setAgent(String agent) {
		this.agent = agent;
	}



	public String getStatuscode() {
		return statuscode;
	}



	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

 
	public double getSuperdistperc() {
		return superdistperc;
	}



	public void setSuperdistperc(double superdistperc) {
		this.superdistperc = superdistperc;
	}



	public double getRangeFrom() {
		return rangeFrom;
	}



	public void setRangeFrom(double rangeFrom) {
		this.rangeFrom = rangeFrom;
	}



	public double getRangeTo() {
		return rangeTo;
	}



	public void setRangeTo(double rangeTo) {
		this.rangeTo = rangeTo;
	}



	

	
	

}
