package com.bhartipay.commission.bean;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//@Entity(name = "ForeignKeyAssoAccountEntity")
@Entity
@Table(name="commpercrangemaster")

public class CommPercRangeMaster implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="commrangeid", nullable = false,length=5)
	private int commrangeid ;
	
	@Column(name="fixedcharge", nullable = true, length=10)
	private double fixedcharge;
	
	@Column(name="rangefrom", nullable = true, length=8)
	private int rangefrom;
	
	@Column(name="rangeto", nullable = true, length=8)
	private int rangeto;
	
	


	public int getCommrangeid() {
		return commrangeid;
	}


	public void setCommrangeid(int commrangeid) {
		this.commrangeid = commrangeid;
	}


	public double getFixedcharge() {
		return fixedcharge;
	}


	public void setFixedcharge(double fixedcharge) {
		this.fixedcharge = fixedcharge;
	}


	public int getRangefrom() {
		return rangefrom;
	}


	public void setRangefrom(int rangefrom) {
		this.rangefrom = rangefrom;
	}


	public int getRangeto() {
		return rangeto;
	}


	public void setRangeto(int rangeto) {
		this.rangeto = rangeto;
	}

	



	


}
