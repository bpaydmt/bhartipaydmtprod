package com.bhartipay.commission.bean;

import java.util.List;
import java.util.Set;

public class CommInputBean {
	
	private int commid;
	private String planid;
	private int txnid ;
	//private double fixedcharge;
	private double servicetax;
	private int bankrefid;
	//private int rangefrom;
	//private int rangeto;
	private String description;
	private double sbcess;
	private double kycess;
	private double aggregatorperc;
	private double distributorperc;
	private double agentperc;
	private double subagentperc;
	private double commperc;
	private Set<CommPercRangeMaster> commPercRangeMaster;
	
	 /*private List<Double> fixedcharge;
	 private List<Integer> rangefrom;
	 private List<Integer> rangeto;*/
	
	
	
	
	

	

	
	public int getCommid() {
		return commid;
	}
	public Set<CommPercRangeMaster> getCommPercRangeMaster() {
		return commPercRangeMaster;
	}
	public void setCommPercRangeMaster(Set<CommPercRangeMaster> commPercRangeMaster) {
		this.commPercRangeMaster = commPercRangeMaster;
	}
	public void setCommid(int commid) {
		this.commid = commid;
	}
	public String getPlanid() {
		return planid;
	}
	public void setPlanid(String planid) {
		this.planid = planid;
	}
	public int getTxnid() {
		return txnid;
	}
	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}
	
	public double getServicetax() {
		return servicetax;
	}
	public void setServicetax(double servicetax) {
		this.servicetax = servicetax;
	}

	
	
	
	
	
	public int getBankrefid() {
		return bankrefid;
	}
	public void setBankrefid(int bankrefid) {
		this.bankrefid = bankrefid;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getSbcess() {
		return sbcess;
	}
	public void setSbcess(double sbcess) {
		this.sbcess = sbcess;
	}
	public double getKycess() {
		return kycess;
	}
	public void setKycess(double kycess) {
		this.kycess = kycess;
	}
	public double getAggregatorperc() {
		return aggregatorperc;
	}
	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}
	public double getDistributorperc() {
		return distributorperc;
	}
	public void setDistributorperc(double distributorperc) {
		this.distributorperc = distributorperc;
	}
	public double getAgentperc() {
		return agentperc;
	}
	public void setAgentperc(double agentperc) {
		this.agentperc = agentperc;
	}
	public double getSubagentperc() {
		return subagentperc;
	}
	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}
	public double getCommperc() {
		return commperc;
	}
	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}
	
	
	
	
	

}
