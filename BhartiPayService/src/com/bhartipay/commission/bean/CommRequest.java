package com.bhartipay.commission.bean;

import java.util.List;

public class CommRequest {

	private int userType;
	private String agentid;
	private List<CommissionBean> list;
	
	
	
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public String getAgentid() {
		return agentid;
	}
	public List<CommissionBean> getList() {
		return list;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public void setList(List<CommissionBean> list) {
		this.list = list;
	}	
}
