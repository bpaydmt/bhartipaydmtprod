package com.bhartipay.commission.persistence;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cme.persistence.CMEDaoImpl;
import com.bhartipay.commission.bean.CommInputBean;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.commission.bean.CommPercMasterKey;
import com.bhartipay.commission.bean.CommPercViewMast;
import com.bhartipay.commission.bean.CommPlanMaster;
import com.bhartipay.commission.bean.CommRequest;
import com.bhartipay.commission.bean.CommissionBean;
import com.bhartipay.report.bean.ReportBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;


public class CommissionDaoImpl implements CommissionDao{
	
	
/*	private static CommissionDaoImpl commissionDaoImpl=null;
	private CommissionDaoImpl(){
		
	}
	
	public static CommissionDaoImpl getCommissionDaoImpl(){
		if(commissionDaoImpl==null){
			commissionDaoImpl=new CommissionDaoImpl();
		}
		return commissionDaoImpl;
	}*/
	
	private static final Logger logger = Logger.getLogger(CommissionDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
	
	public CommPlanMaster createPlan( CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|createPlan()"+commPlanMaster.getPlanType()
				);
		//logger.info("start excution ***************************************************** method createPlan()"+commPlanMaster.getPlanType());
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	    commPlanMaster.setStatusCode("1001");
	    try{
	    	if(commPlanMaster.getPlanType()==null ||commPlanMaster.getPlanType().isEmpty()){
	    		 commPlanMaster.setStatusCode("1001");
	    		 return commPlanMaster;
	    	}
	    	
	    	if(commPlanMaster.getAggreatorid()==null ||commPlanMaster.getAggreatorid().isEmpty()){
	    		 commPlanMaster.setStatusCode("1001");
	    		 return commPlanMaster;
	    	}
	    	if(commPlanMaster.getPlanId()==null ||commPlanMaster.getPlanId().isEmpty()){
	    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|new Plan"
	    				);
	    		//logger.info("new Plan ***************************************************** ");
	    	String planId=commanUtilDao.getTrxId("COMMPLAN",commPlanMaster.getAggreatorid());
	    	commPlanMaster.setPlanId(planId);
	    	}else{
	    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|update Plan"
	    				);
	    		//logger.info("update Plan ***************************************************** ");
	    	}
	    	session.saveOrUpdate(commPlanMaster);	 
	    	transaction.commit();
	    	commPlanMaster.setStatusCode("1000");
		}catch (HibernateException he) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|problem in createPlan HibernateException"+he.getMessage()+" "+he
    				);
    		//logger.debug("problem in createPlan===========HibernateException==============" + he.getMessage(), he);
        	transaction.rollback();
        	commPlanMaster.setStatusCode("9004");
           	he.printStackTrace();
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|problem in createPlan"+e.getMessage()+" "+e
    				);
	    	//logger.debug("problem in createPlan=========================" + e.getMessage(), e);
	    	commPlanMaster.setStatusCode("7000");
	   		  e.printStackTrace();
	   			 transaction.rollback();
		}finally {
			session.close();
		}
		return commPlanMaster;
	}

	
	
	public CommPlanMaster updatePlan(CommPlanMaster commPlanMaster){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|updatePlan()"+commPlanMaster.getPlanType()
				);
		//logger.info("start excution ***************************************************** method createPlan()"+commPlanMaster.getPlanType());
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
	    Transaction transaction = session.beginTransaction();
	    commPlanMaster.setStatusCode("1001");
	    try{
	    	if(commPlanMaster.getPlanId()==null ||commPlanMaster.getPlanId().isEmpty()){
	    		 commPlanMaster.setStatusCode("9003");
	    		 return commPlanMaster;
	    	}
	    	CommPlanMaster oldCommPlanMaster=(CommPlanMaster)session.load(CommPlanMaster.class, commPlanMaster.getPlanId());
	    	oldCommPlanMaster.setVersion(commPlanMaster.getVersion());
	    	transaction.commit();
	    	commPlanMaster.setStatusCode("1000");
	    	
	    	
	    }catch (Exception e) {
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),commPlanMaster.getAggreatorid(),"","", "", "",   "|problem in createPlan"+e.getMessage()+" "+e
					);
	    	//logger.debug("problem in createPlan=========================" + e.getMessage(), e);
	    	commPlanMaster.setStatusCode("7000");
	   		  e.printStackTrace();
	   			 transaction.rollback();
		}finally {
			session.close();
		}
		return commPlanMaster;
	}
	
	
	
	public CommPlanMaster ShowPlan(String planId){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|ShowPlan()"+planId
				);
		//logger.info("start excution ***************************************************** method ShowPlan()"+planId);
		
		factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		CommPlanMaster commPlanMaster=new CommPlanMaster();
		try{
			Criteria commCriteria= session.createCriteria(CommPlanMaster.class);
			  commCriteria.add(Restrictions.eq("planId", planId));
			  List <CommPlanMaster> list=commCriteria.list();
			  if(list!=null && list.size()>0){
				  commPlanMaster=list.get(0);
			  }
			  
			}catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in createPlan"+e.getMessage()+" "+e
						);
	    	//logger.debug("problem in createPlan=========================" + e.getMessage(), e);
	    	commPlanMaster.setStatusCode("7000");
	   		  e.printStackTrace();
	   			 transaction.rollback();
		}finally {
			session.close();
		}
		commPlanMaster.setStatusCode("1000");
		return commPlanMaster;
	}
	
	
	
	
public HashMap<String, HashMap> getPlanDtl(String aggreatorid){
	
	System.out.println("_________aggreatorid__________"+aggreatorid);
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "",   "|getPlanDtl()"
			);
	//logger.info("start excution ***************************************************** method getPlanDtl()"+aggreatorid);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
    HashMap <String,String> planDetails=new HashMap<String,String>();
    HashMap <String,String> txnType=new HashMap<String,String>();
    HashMap <String,HashMap> planTxnDtls=new HashMap<String,HashMap>();
    try{
    	SQLQuery query;
    	query=session.createSQLQuery("SELECT planid,plantype FROM commplanmaster WHERE VERSION=1 and aggreatorid=:aggreatorid");
    	query.setString("aggreatorid", aggreatorid);
    	List<Object[]> rows = query.list();
    	
    	
		for (Object[] row : rows) {
			System.out.println("row_____"+row[0].toString());
			planDetails.put(row[0].toString(), row[1].toString());
		}
 		query=session.createSQLQuery("SELECT txnid,txndesc FROM transactionmastercode WHERE txnid IN(SELECT txncode  FROM walletaggreatortxnmast WHERE aggreatorid=:aggreatorid)");
 		query.setString("aggreatorid", aggreatorid);
 		List<Object[]> txnTypeRows = query.list();
 		
 		
		for (Object[] row : txnTypeRows) {
			System.out.println(row[0].toString());
			txnType.put(row[0].toString(), row[1].toString());
		}
		planTxnDtls.put("PLAN", planDetails);
		planTxnDtls.put("TXNTYPE", txnType);

    }catch (Exception e) {
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "",   "|problem in getPlanDtl"+e.getMessage()+" "+e
    			);
    	//logger.debug("problem in getPlanDtl=========================" + e.getMessage(), e);
    	  e.printStackTrace();
	}finally {
		session.close();
	}
    
	return planTxnDtls;
}


public HashMap<String, String> getTxnTypeDtl(){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|getTxnTypeDtl()"
			);
	//logger.info("start excution ***************************************************** method getTxnTypeDtl()");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
     HashMap <String,String> txnType=new HashMap<String,String>();
      try{
    	SQLQuery query;
    	
 		query=session.createSQLQuery("SELECT txnid,txndesc FROM transactionmastercode");
    	List<Object[]> txnTypeRows = query.list();
		for (Object[] row : txnTypeRows) {
			System.out.println(row[0].toString());
			txnType.put(row[0].toString(), row[1].toString());
		}
		
    }catch (Exception e) {
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in getTxnTypeDtl"+e.getMessage()+" "+e
    			);
    	//logger.debug("problem in getPlanDtl=========================" + e.getMessage(), e);
    	  e.printStackTrace();
	}finally {
		session.close();
	}
    
	return txnType;
}






public HashMap<String, HashMap> getAllPlans(String aggreatorid){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "",   "|getAllPlans()"
			);
	//logger.info("start excution ***************************************************** method getAllPlans()"+aggreatorid);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
    HashMap <String,String> planDetails=new HashMap<String,String>();
    HashMap <String,String> txnType=new HashMap<String,String>();
    HashMap <String,HashMap> planTxnDtls=new HashMap<String,HashMap>();
    try{
    	SQLQuery query;
    	query=session.createSQLQuery("SELECT planid,plantype FROM commplanmaster where aggreatorid=:aggreatorid");
    	query.setString("aggreatorid", aggreatorid);
    	List<Object[]> rows = query.list();
		for (Object[] row : rows) {
			System.out.println(row[0].toString());
			planDetails.put(row[0].toString(), row[1].toString());
		}
 		query=session.createSQLQuery("SELECT txnid,txndesc FROM transactionmastercode");
    	List<Object[]> txnTypeRows = query.list();
		for (Object[] row : txnTypeRows) {
			System.out.println(row[0].toString());
			txnType.put(row[0].toString(), row[1].toString());
		}
		planTxnDtls.put("PLAN", planDetails);
		planTxnDtls.put("TXNTYPE", txnType);

    }catch (Exception e) {
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "",   "|problem in getAllPlans"+e.getMessage()+" "+e
    			);
    	//logger.debug("problem in getAllPlans=========================" + e.getMessage(), e);
    	  e.printStackTrace();
	}finally {
		session.close();
	}
    
	return planTxnDtls;
}



public CommPercMaster saveUpdateCommPerc(CommInputBean commInputBean,String ipiemi,String agent){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "|saveUpdateCommPerc()"
			);
	//logger.info("start excution ***************************************************** method saveUpdateCommPerc()");
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	CommPercMasterKey commPercMasterKey=new CommPercMasterKey();
	CommPercMaster commPercMaster=new CommPercMaster();
	commPercMaster.setStatuscode("1001");
    Transaction transaction = session.beginTransaction();
    try{
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getPlanid"+commInputBean.getPlanid() 
    			);
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getTxnid"+commInputBean.getTxnid() 
    			);
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getCommid"+commInputBean.getCommid() 
    			);
    	/*System.out.println("====================================================="+(commInputBean.getPlanid() ));
    	System.out.println("====================================================="+(commInputBean.getTxnid()));
    	System.out.println("====================================================="+(commInputBean.getCommid()));*/

    	if(commInputBean.getCommid()!=0)
    	commPercMaster.setCommid(commInputBean.getCommid());
    	
    	
    	
    	
    	
    	commPercMaster.setPlanid(commInputBean.getPlanid());
    	commPercMaster.setTxnid(commInputBean.getTxnid());
    	commPercMaster.setBankrefid(commInputBean.getBankrefid());
    	commPercMaster.setDescription(commInputBean.getDescription());
    	commPercMaster.setSbcess(commInputBean.getSbcess());
    	commPercMaster.setKycess(commInputBean.getKycess());
    	commPercMaster.setAggregatorperc(commInputBean.getAggregatorperc());
    	commPercMaster.setDistributorperc(commInputBean.getDistributorperc());
    	commPercMaster.setAgentperc(commInputBean.getAgentperc());
    	commPercMaster.setSubagentperc(commInputBean.getSubagentperc());
    	commPercMaster.setCommperc(commInputBean.getCommperc());
    	commPercMaster.setIpiemi(ipiemi);
    	commPercMaster.setAgent(agent);
    	//commPercMaster.setChildren(commInputBean.getCommPercRangeMaster());
    	commPercMaster.setCommPercRangeMaster(commInputBean.getCommPercRangeMaster());
    	
    	
    	
    	
    	
    	
    	//session.save(commPercMaster);
    	
     	session.saveOrUpdate(commPercMaster);
    	transaction.commit();
    	commPercMaster.setStatuscode("1000");
    	
    	}catch (HibernateException he) {
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in saveUpdateCommPerc HibernateException"+ he.getMessage()+" "+he 
        			);
    		//logger.debug("problem in saveUpdateCommPerc===========HibernateException==============" + he.getMessage(), he);
        	transaction.rollback();
        	commPercMaster.setStatuscode("9005");
           	he.printStackTrace();
		} 	catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in saveUpdateCommPerc"+e.getMessage()+" "+e 
	    			);
			//logger.debug("problem in saveUpdateCommPerc=========================" + e.getMessage(), e);
			transaction.rollback();
			commPercMaster.setStatuscode("7000");
			e.printStackTrace();
	}finally {
		session.close();
	}
	return commPercMaster;
}

public List <CommPercViewMast> ShowCommByPlanId(String palnId,int txnid){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "ShowCommByPlanId()|palnId "+palnId 
			);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "txnid()"+txnid 
			);
	//logger.info("start excution ***************************************************** method ShowCommByPlanId()"+palnId);
	//logger.info("start excution ***************************************************** method ShowCommByPlanId()"+txnid);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List <CommPercViewMast> percList=new ArrayList <CommPercViewMast>();
	  try{
		 
		  
		  
		  SQLQuery  query=session.createSQLQuery("SELECT planid,txnid,fixedcharge,rangefrom,rangeto ,description,servicetax ,sbcess,kycess,aggregatorperc,distributorperc,agentperc,subagentperc,commperc FROM  commpercview WHERE planid=:planid AND txnid=:txnid");
			query.setString("planid", palnId);
			query.setInteger("txnid", txnid);
			query.setResultTransformer(Transformers.aliasToBean(CommPercViewMast.class)); 
			percList=query.list();
			
		/*	
			
		  
		  
		  
		  
		  
		  
		  Criteria commCriteria= session.createCriteria(CommPercMaster.class);
		  commCriteria.add(Restrictions.eq("planid", palnId));
		  
		  if(txnid !=-1)
		  commCriteria.add(Restrictions.eq("txnid", txnid));
		  
		 percList=commCriteria.list();
		 */
		 
		 
		 
		 
	  }catch (HibernateException he) {
		  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in ShowCommByPlanId =HibernateException"+he.getMessage()+" "+he
					);
  		//logger.debug("problem in ShowCommByPlanId===========HibernateException==============" + he.getMessage(), he);
      	transaction.rollback();
      	he.printStackTrace();
	} 	catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in ShowCommByPlanId"+e.getMessage()+" "+e 
				);
		//logger.debug("problem in ShowCommByPlanId=========================" + e.getMessage(), e);
		transaction.rollback();
		e.printStackTrace();
	}finally {
		session.close();
	}
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "percList.size"+percList.size() 
				);
	 // logger.info("start excution ****************************percList.size************************* percList.size:"+percList.size()); 
	return percList;
	
}


/**
 * 
 * @param commId
 * @return
 */
/*public CommPercMaster ShowCommByCommId(String commId){
	logger.info("start excution ***************************************************** method ShowCommByCommId()"+commId);
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	CommPercMaster percList=new CommPercMaster();
	  try{
		  percList=(CommPercMaster)session.get(CommPercMaster.class, commId);
	
	  }catch (HibernateException he) {
  		logger.debug("problem in ShowCommByCommId===========HibernateException==============" + he.getMessage(), he);
      	transaction.rollback();
      	he.printStackTrace();
	} 	catch (Exception e) {
		logger.debug("problem in ShowCommByCommId=========================" + e.getMessage(), e);
		transaction.rollback();
		e.printStackTrace();
	}finally {
		session.close();
	}
	return percList;
	
}*/


public CommInputBean ShowCommByCommId(CommInputBean commInputBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "ShowCommByCommId()" +commInputBean.getCommid()
			);
	/*logger.info("start excution ***************************************************** method saveUpdateCommPerc()");
	System.out.println("========================22============================="+commInputBean.getCommid());*/
		
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{

		SQLQuery  query=session.createSQLQuery("SELECT commid,planid,txnid,fixedcharge,servicetax,bankrefid,rangefrom,rangeto,description,sbcess,kycess,distributorperc,aggregatorperc,agentperc,subagentperc,commperc FROM commpercmaster WHERE commid=:commid");
		query.setInteger("commid", commInputBean.getCommid());
		query.setResultTransformer(Transformers.aliasToBean(CommInputBean.class)); 
		if(query.list().size()>0 )	
			commInputBean=(CommInputBean)query.list().get(0);
    	  
      }catch (Exception e) {
    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in getCommPerc" +e.getMessage()+" "+e
    				);
    	//logger.debug("problem in getCommPerc=========================" + e.getMessage(), e);
    	transaction.rollback();
    	
  	  e.printStackTrace();
	}finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "return" +commInputBean.getCommid()
			);
	//System.out.println("============================return========================="+commInputBean.getCommid());
	return commInputBean;
}





public CommPercMaster getCommPerc(CommInputBean commInputBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getCommPerc()" +commInputBean.getPlanid()
			);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getCommPerc()" +commInputBean.getTxnid()
			);
	/*logger.info("start excution ***************************************************** method getCommPerc()");
	
	System.out.println("========================22========getCommPerc====================="+commInputBean.getPlanid());
	System.out.println("============================33=========getCommPerc================"+commInputBean.getTxnid());*/
	
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	CommPercMaster commPercMaster=new CommPercMaster();
	try{

		Query query=session.createQuery("from CommPercMaster where planid=:planid and txnid=:txnid");
		query.setString("planid", commInputBean.getPlanid());
		query.setInteger("txnid", commInputBean.getTxnid());
		
		List<CommPercMaster> list=query.list();
		if(!(list==null) &&list.size()>0){
			 commPercMaster=(CommPercMaster)list.get(0);
			
		}else{
			commPercMaster.setPlanid(commInputBean.getPlanid());
			commPercMaster.setTxnid(commInputBean.getTxnid());
		}
		
		
		
		
		
		
/*		
		SQLQuery  query=session.createSQLQuery("SELECT commid,planid,txnid,fixedcharge,servicetax,bankrefid,rangefrom,rangeto,description,sbcess,kycess,distributorperc,aggregatorperc,agentperc,subagentperc,commperc FROM commpercmaster WHERE planid=:planid  AND txnid=:txnid");
		query.setString("planid", commInputBean.getPlanid());
		query.setInteger("txnid", commInputBean.getTxnid());
		query.setResultTransformer(Transformers.aliasToBean(CommInputBean.class)); 
		if(query.list().size()>0 )	
			commInputBean=(CommInputBean)query.list().get(0);*/
    	  
      }catch (Exception e) {
    		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in getCommPerc)" +e.getMessage()+" "+e
    				);
    	//logger.debug("problem in getCommPerc=========================" + e.getMessage(), e);
    	transaction.rollback();
    	
  	  e.printStackTrace();
	}finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "return" +commPercMaster.getCommid()
			);
	//System.out.println("============================return========================="+commPercMaster.getCommid());
	return commPercMaster;
}

/*public ReportBean assignCommission(ReportBean reportBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",  "assignCommission()|2 distId"+reportBean.getDistId()
			);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",  "assignCommission()|3 agentId"+reportBean.getAgentId()
			);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",  "assignCommission()|4 subagentId"+reportBean.getSubagentId()
			);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",  "assignCommission()|4 commId"+reportBean.getPlanId()
			);
	logger.info("Start excution =============2======== method assignCommission(dist,agent,subagentId)--distId"+reportBean.getDistId());
	logger.info("Start excution ==============3======= method assignCommission(dist,agent,subagentId)--agentId"+reportBean.getAgentId());
	logger.info("Start excution ===============4====== method assignCommission(dist,agent,subagentId)--subagentId"+reportBean.getSubagentId());
	logger.info("Start excution ===============4====== method assignCommission(dist,agent,subagentId)--commId"+reportBean.getPlanId());
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	SQLQuery	 query=null;
	Transaction transaction=session.beginTransaction();
	try{
		if(reportBean.getDistId().equals("-1") && reportBean.getAgentId().equals("-1") && reportBean.getSubagentId().equals("-1")){
			reportBean.setStatusCode("8001");
		}
		
		if(reportBean.getPlanId().equals("-1")){
			reportBean.setStatusCode("8002");
		}
		
		if(!reportBean.getDistId().equals("-1") && reportBean.getAgentId().equals("-1") && reportBean.getSubagentId().equals("-1")){
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE id=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getDistId());
			query.executeUpdate();
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE distributerid=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getDistId());
			query.executeUpdate();
			transaction.commit();
			reportBean.setStatusCode("1000");
		}
		
		if(!reportBean.getDistId().equals("-1") && !reportBean.getAgentId().equals("-1") && reportBean.getSubagentId().equals("-1")){
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE id=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getAgentId());
			query.executeUpdate();
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE agentid=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getAgentId());
			query.executeUpdate();
			transaction.commit();
			reportBean.setStatusCode("1000");
		}
		
		if(!reportBean.getDistId().equals("-1") && !reportBean.getAgentId().equals("-1") && !reportBean.getSubagentId().equals("-1")){
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE id=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getSubagentId());
			query.executeUpdate();
			query=session.createSQLQuery("UPDATE walletmast SET planid=:planid  WHERE subagentid=:distId");
			query.setString("planid", reportBean.getPlanId());
			query.setString("distId", reportBean.getSubagentId());
			query.executeUpdate();
			transaction.commit();
			reportBean.setStatusCode("1000");
		}
		
		
    }catch (Exception e) {
    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",  "problem in assignCommission()"+e.getMessage()+" "+e
    			);
    	//logger.debug("problem in assignCommission=========================" + e.getMessage(), e);
    	reportBean.setStatusCode("7000");
    	transaction.rollback();
    	e.printStackTrace();
	}finally {
		session.close();
	}
return reportBean;	
}*/


public HashMap getCommission(String userId){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",  "getCommission()");
	//logger.info("Start excution ===============4====== method getCommission(id)--commId"+userId);
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
    List objectList = null; 
    HashMap CommissionDtl=new HashMap();
	try{
		Query query = session.createSQLQuery("call MisReportConsole(:userId)");
        query.setParameter("userId",userId);
        objectList=query.list(); 
        Object[] obj=(Object[])objectList.get(0);
        CommissionDtl.put("Principal", obj[0]);
        CommissionDtl.put("Payout", obj[1]);
        CommissionDtl.put("Commission", obj[2]);
	 }catch (Exception e) {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "",  "problem in getCommission"+e.getMessage()+" "+e);
	    	//logger.debug("problem in getCommission=========================" + e.getMessage(), e);
	    	transaction.rollback();
	    	e.printStackTrace();
		}finally {
			session.close();
		}
	return CommissionDtl;
}

public String assignCommission(CommRequest commRequest){
	String resp="fail";
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "assignCommission()"+commRequest.getAgentid());
	    
	//logger.info("Start excution =========3============================= methopd assignCommission" + assignCommissionObj.getAgent());
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		
		
			if(commRequest.getAgentid()!=null){
				String agent=commRequest.getAgentid().split("-")[0];
				commRequest.setAgentid(agent);
			}
			Query query1=session.createQuery("from WalletMastBean where id=:agentId");
			query1.setParameter("agentId", commRequest.getAgentid());
			List<WalletMastBean> wMastList=query1.list();
			List<String> listPlan=null;
			if(wMastList != null && wMastList.size() != 0 && wMastList.get(0) != null)
			{
				WalletMastBean user=wMastList.get(0);
				
				if(user.getUsertype()==2){
				if(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0")){
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getDistributerid());
					listPlan=query2.list();
				}else{
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getId());
					listPlan=query2.list();
				}
				}else if(user.getUsertype()==3){
				if(user.getUsertype()==3&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))&&user.getSuperdistributerid()!=null&&!user.getSuperdistributerid().equalsIgnoreCase("-1")){
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getSuperdistributerid());
					listPlan=query2.list();
				}else if(user.getUsertype()==3&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))){
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getAggreatorid());
					listPlan=query2.list();
				}else{
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getId());
					listPlan=query2.list();
				}
				}else if(user.getUsertype()==7){
				if(user.getUsertype()==7&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))){
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getAggreatorid());
					listPlan=query2.list();
				}else{
					Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
					query2.setParameter("agent", user.getId());
					listPlan=query2.list();
				}
				}
			}
			
			List<CommissionBean> respList=null;
			if(listPlan != null && listPlan.size() != 0 && listPlan.get(0) != null && !listPlan.get(0).trim().equals("")){
			 Query  query=session.createQuery("from CommissionBean where planId=:planId");
			 query.setParameter("planId",listPlan.get(0));
			 respList=query.list();
			 
			}
		
		
		String planId=commanUtilDao.getTrxId("COMMPLAN","OAGG001050");
		CommissionBean cBean=new CommissionBean();
		if(respList!=null&&respList.size()>0)
			cBean=respList.get(0);
		
		for(CommissionBean commBean:commRequest.getList()){
			Transaction t=session.beginTransaction();
			
				WalletMastBean user=wMastList.get(0);
				/*if(user.getUsertype()==2){				
					commBean.setDisPer(cBean.getDisPer()!=0?cBean.getDisPer():0);
					commBean.setSuperDisPer(cBean.getSuperDisPer()!=0?cBean.getSuperDisPer():0);
					
				}else if(user.getUsertype()==3){
					commBean.setSuperDisPer(cBean.getSuperDisPer()!=0?cBean.getSuperDisPer():0);
				}*/
	
			commBean.setPlanId(planId);
			commBean.setDescription("Commission plan  "+user.getAggreatorid());
			commBean.setServiceCharges(0);
			commBean.setSubagentperc(0);
			
			//commBean.setCommperc(cBean.getCommperc()!=0?cBean.getCommperc():0.0);
			
			if(commBean.getTxntype()!=null&&!commBean.getTxntype().equalsIgnoreCase("VERIFY")){
			commBean.setWlaggreatorservicechg(cBean.getWlaggreatorservicechg()!=0?cBean.getWlaggreatorservicechg():18.0);
			commBean.setWlaggreatorchg(cBean.getWlaggreatorchg()!=0?cBean.getWlaggreatorchg():3.5);
			commBean.setAggregatorperc(cBean.getAggregatorperc()!=0?cBean.getAggregatorperc():100.0);
			}else{
			commBean.setWlaggreatorservicechg(cBean.getWlaggreatorservicechg()!=0?cBean.getWlaggreatorservicechg():0.0);
			commBean.setWlaggreatorchg(cBean.getWlaggreatorchg()!=0?cBean.getWlaggreatorchg():3);
			commBean.setAggregatorperc(cBean.getAggregatorperc()!=0?cBean.getAggregatorperc():0.0);
			}
			session.save(commBean);
			t.commit();
			
			
		}

		Transaction tx=session.beginTransaction();
		SQLQuery query=session.createSQLQuery("update walletmast set planid=:planid where id=:id");
		query.setParameter("planid",planId);
		query.setParameter("id",commRequest.getAgentid());
		query.executeUpdate();
		tx.commit();
		resp="success";
		
	}
	catch(Exception e){
		e.printStackTrace();
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in excution in assignCommission"+e.getMessage()+" "+e);
		 
		//logger.info("problem in excution =========3============================= methopd assignCommission"+e);

	}finally {
		session.close();
	}
	
	return resp;
}

public List<CommissionBean> getCommission(CommissionBean commBean){

	List<CommissionBean> resp=null;
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getCommission()"+commBean.getAgent());
	  
	//logger.info("Start excution =========3============================= methopd getCommission" + commBean.getAgent());
	factory=DBUtil.getSessionFactory();
	Session session = factory.openSession();
	try{
		  
		Query query1=session.createQuery("from WalletMastBean where id=:agentId");
		query1.setParameter("agentId", commBean.getAgentId());
		List<WalletMastBean> wMastList=query1.list();
		List<String> listPlan=null;
		if(wMastList != null && wMastList.size() != 0 && wMastList.get(0) != null)
		{
			WalletMastBean user=wMastList.get(0);
			
			if(user.getUsertype()==2){
			if(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0")){
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getDistributerid());
				listPlan=query2.list();
			}else{
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getId());
				listPlan=query2.list();
			}
			}else if(user.getUsertype()==3){
			if(user.getUsertype()==3&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))&&user.getSuperdistributerid()!=null&&!user.getSuperdistributerid().equalsIgnoreCase("-1")){
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getSuperdistributerid());
				listPlan=query2.list();
			}else if(user.getUsertype()==3&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))){
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getAggreatorid());
				listPlan=query2.list();
			}else{
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getId());
				listPlan=query2.list();
			}
			}else if(user.getUsertype()==7){
			if(user.getUsertype()==7&&(user.getPlanId()==null||user.getPlanId().trim().isEmpty()||user.getPlanId().equalsIgnoreCase("0"))){
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getAggreatorid());
				listPlan=query2.list();
			}else{
				Query query2=session.createQuery("select planId from WalletMastBean where id=:agent");
				query2.setParameter("agent", user.getId());
				listPlan=query2.list();
			}
			}
			
			
			
			
		}
		
		if(listPlan != null && listPlan.size() != 0 && listPlan.get(0) != null && !listPlan.get(0).trim().equals("")){
		 Query  query=session.createQuery("from CommissionBean where planId=:planId");
		 query.setParameter("planId",listPlan.get(0));
		 resp=query.list();
		 
		}
		  
		
	}
	catch(Exception e){
		e.printStackTrace();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "problem in excution getCommission"+e.getMessage()+" "+e);
	}
	finally{
		session.close();
	}
	return resp;
}

public static void main(String[] args) {
	//new CommissionDaoImpl().ShowCommByPlanId("COMM001017",1);
	CommInputBean commInputBean=new CommInputBean();
	//commInputBean.set
	
}
}
