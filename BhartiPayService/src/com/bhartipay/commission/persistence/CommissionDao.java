package com.bhartipay.commission.persistence;

import java.util.HashMap;
import java.util.List;

import com.bhartipay.commission.bean.CommInputBean;
import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.commission.bean.CommPercViewMast;
import com.bhartipay.commission.bean.CommPlanMaster;
import com.bhartipay.commission.bean.CommRequest;
import com.bhartipay.commission.bean.CommissionBean;
import com.bhartipay.report.bean.ReportBean;

public interface CommissionDao {
	public CommPlanMaster createPlan( CommPlanMaster commPlanMaster);
	public HashMap<String, HashMap> getPlanDtl(String aggreatorid);
	public CommPercMaster getCommPerc(CommInputBean commInputBean);
	public CommPercMaster saveUpdateCommPerc(CommInputBean commInputBean,String ipiemi,String agent);
	//public ReportBean assignCommission(ReportBean reportBean);
	public HashMap getCommission(String userId);
	public List <CommPercViewMast> ShowCommByPlanId(String palnId,int txnid);
	//public CommPlanMaster ShowPlan(String planId);
	public CommPlanMaster ShowPlan(String planId);
	public CommPlanMaster updatePlan(CommPlanMaster commPlanMaster);
	public HashMap<String, HashMap> getAllPlans(String aggreatorid);
	public CommInputBean ShowCommByCommId(CommInputBean commInputBean);
	public HashMap<String, String> getTxnTypeDtl();
	public List<CommissionBean> getCommission(CommissionBean commBean);
	public String assignCommission(CommRequest assignCommissionObj);
	
}
