package com.bhartipay.mudra;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.ResponseBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.OTPGeneration;

@Path("/mudraCust")
public class MudraCustManager 
{
	Logger logger = Logger.getLogger(MudraCustManager.class.getName());
	WalletUserDao userDao = new WalletUserDaoImpl();
	OTPGeneration oTPGeneration = new OTPGeneration();
	MudraDao mudraDao = new MudraDaoImpl();
	
	@POST
	@Path("/addBeneficiaryRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public ResponseBean addBeneRequest(String mobileNo,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request)
	{
		ResponseBean responseBean = new ResponseBean();
		try
		{
			String serverName=request.getServletContext().getInitParameter("serverName");
			WalletMastBean walletMast = userDao.getUserByMobileNoWithoutAgg(mobileNo, serverName);
			if(walletMast == null)
			{
				responseBean.setCode("2000");
				responseBean.setMessage("Invalid Mobile Number");
				responseBean.setResponse("FAILED");
				return responseBean;
			}
			logger.info("Inside *********account wallet====send otp for validate mobile on" + mobileNo);

			boolean flag = mudraDao.otpSenderResend(mobileNo, "", serverName, "");
			if(flag)
			{ 
				responseBean.setCode("300");
				responseBean.setMessage("OTP has been send on "+mobileNo+" .");
				responseBean.setResponse("FAILED");
				return responseBean;			
			}
			else
			{
				responseBean.setCode("2000");
				responseBean.setMessage("Failed to send OTP");
				responseBean.setResponse("FAILED");
				return responseBean;
			}
		}
		catch(Exception ex)
		{
			logger.info("******problem in addBeneRequest********e.message**"+ex.getMessage() );
			ex.printStackTrace();
		}
		return responseBean;
	}
	
	@POST
	@Path("/addBeneficiaryConfirm")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryWallet addBeneficiaryConfirm(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request)
	{
		logger.info("***********start execution of addBeneficiaryConfirm**********");
		MudraBeneficiaryWallet beneResp  = null;
		try
		{
			String serverName=request.getServletContext().getInitParameter("serverName");
			beneResp= mudraDao.registerBeneficiaryCust(mudraBeneficiaryMastBean,serverName,"") ;	
		}
		catch(Exception e)
		{
			logger.info("***********problem in addBeneficiaryConfirm*********e.getMessage"+e.getMessage());
			e.printStackTrace();
		}
		return beneResp;
	}
	
	@POST
	@Path("/deactivateBeneficiary")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet deactivateBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,@Context HttpServletRequest request)
	{
		logger.info("*************start execution of********deactivateBeneficiary******");
		MudraSenderWallet mudraSender = null;
		try
		{
			String serverName=request.getServletContext().getInitParameter("serverName");
			mudraSender = mudraDao.deActiveBeneficiary(mudraBeneficiaryMastBean, serverName, "");
		}
		catch(Exception e)
		{
			logger.info("*************problem in*****deactivateBeneficiary******e.message**"+e.getMessage());
			e.printStackTrace();
		}
		return mudraSender;
	}
}

