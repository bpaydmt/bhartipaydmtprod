package com.bhartipay.mudra.persistence;

import java.util.HashMap;
import java.util.List;

import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.BankMastBean;
import com.bhartipay.mudra.bean.BankNameListBean;
import com.bhartipay.mudra.bean.DMTApiRequest;
import com.bhartipay.mudra.bean.DMTReportInOut;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.KycConfigBean;
import com.bhartipay.mudra.bean.MerchantDmtTransBean;
import com.bhartipay.mudra.bean.MiniKycResponse;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderMastBean;
import com.bhartipay.mudra.bean.MudraSenderPanDetails;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.ResponseBean;
import com.bhartipay.mudra.bean.SenderFavouriteBean;
import com.bhartipay.mudra.bean.SenderFavouriteViewBean;
import com.bhartipay.mudra.bean.SenderKYCBean;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.bean.TransacationLedgerBean;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;

public interface MudraDao {
	/**
	 * 
	 * @param mudraSenderMastBean
	 * @param serverName
	 * @return
	 */
	public MudraSenderWallet validateSender(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @param serverName
	 * @param requestId
	 * @return
	 */
	public MudraSenderWallet deletedBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraSenderMastBean
	 * @param serverName
	 * @return
	 */
	public MudraSenderWallet registerSender(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param bankDetailsBean
	 * @param serverName
	 * @return
	 */
	public BankDetailsBean getBankDetails(BankDetailsBean bankDetailsBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param bankDetailsBean
	 * @param serverName
	 * @return
	 */
	public List getBankDetailsByIFSC(BankDetailsBean bankDetailsBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @param serverName
	 * @return
	 */
	public MudraBeneficiaryWallet registerBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraSenderMastBean
	 * @param serverName
	 * @return
	 */
	public MudraSenderWallet forgotMPIN(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraSenderMastBean
	 * @param serverName
	 * @return
	 */
	public MudraSenderWallet updateMPIN(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mobileno
	 * @param aggreatorid
	 * @param serverName
	 * @return
	 */
	public Boolean otpSenderResend(String mobileno,String aggreatorid,String serverName,String requestId);
	
	
	/**
	 * 
	 * @param mobileno
	 * @param aggreatorid
	 * @param serverName
	 * @return
	 */
	public Boolean refundOtp(String id,String mobileno,String aggreatorid,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @param serverName
	 * @return
	 */
	public MudraBeneficiaryWallet deleteBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param senderFavouriteBean
	 * @param serverName
	 * @return
	 */
	public SenderFavouriteBean setFavourite(SenderFavouriteBean senderFavouriteBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param senderFavouriteBean
	 * @param serverName
	 * @return
	 */
	public SenderFavouriteBean deleteFavourite(SenderFavouriteBean senderFavouriteBean,String serverName, String requestId);
	
	/**
	 * 
	 * @param agentId
	 * @param serverName
	 * @return
	 */
	public List<SenderFavouriteViewBean> getFavouriteList(String agentId,String serverName,String requestId);

	/**
	 * 
	 * @param surchargeBean
	 * @param serverName
	 * @return
	 */
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraSenderMastBean
	 * @return
	 */
	public MudraSenderWallet getBeneficiaryListForImport(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @return
	 */
	
	public MudraBeneficiaryWallet copyBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @return
	 */
	
	public MudraSenderWallet activeBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraBeneficiaryMastBean
	 * @return
	 */
	public MudraSenderWallet deActiveBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName,String requestId);
	
	/**
	 * 
	 * @return
	 */
	public List<BankMastBean> getBankDtls(String serverName);
	
	
	/**
	 * 
	 * @param serverName
	 * @return
	 */
	
	public List<BankNameListBean> getBankDtl(String serverName,String requestId);
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param mudraMoneyTransactionBean
	 * @param serverName
	 * @return
	 */
	public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName,String requestId);
	
	
	
	/**
	 * 
	 * @param mudraMoneyTransactionBean
	 * @param serverName
	 * @return
	 */
	public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param mudraMoneyTransactionBean
	 * @param serverName
	 * @return
	 */
	public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName,String requestId);
	
	
	/**
	 * 
	 * @param mudraMoneyTransactionBean
	 * @param serverName
	 * @return
	 */
	public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName,String requestId);
	
	/**
	 * 
	 * @param beneficiaryId
	 * @param serverName
	 * @return
	 */
	public MudraBeneficiaryWallet getBeneficiary(String beneficiaryId,String serverName) ;
	
	/**
	 * 
	 * @param merchantId
	 * @param mobileNo
	 * @param serverName
	 * @return
	 */
	public List<MerchantDmtTransBean> gettransDtl(String merchantId,String mobileNo, String serverName) ;
	
	
	/**
	 * 
	 * @param agentId
	 * @param stDate
	 * @param edDate
	 * @param serverName
	 * @return
	 */
	public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut dMTReportInOut,String serverName,String requestId);
	
	/**
	 * 
	 * @param dMTReportInOut
	 * @param serverName
	 * @return
	 */
	public DMTReportInOut getAgentLedgerDtl(DMTReportInOut dMTReportInOut,String serverName,String requestId);
	
	/**
	 * 
	 * @param dMTReportInOut
	 * @param serverName
	 * @return
	 */
	public DMTReportInOut getSenderLedgerDtl(DMTReportInOut dMTReportInOut,String serverName,String requestId);
	
	/**
	 * 
	 * @param dMTApiRequest
	 * @param serverName
	 * @return
	 */
	public String saveDMTApiRequest(String agentId,String reqVal,String serverName,String apiName);
	
	/**
	 * 
	 * @param merchantId
	 * @param agentTransId
	 * @param serverName
	 * @return
	 */
	public List<MerchantDmtTransBean> getTransStatus(String merchantId,String agentTransId, String serverName) ;
	
	/**
	 * 
	 * @param merchantId
	 * @param senderId
	 * @param serverName
	 * @return
	 */
	public double getUserBalance(String merchantId,String senderId, String serverName);
	
	
	/**
	  * 
	  * @param senderId
	  * @return
	  */
	 //added for defect id 6.1
	 public String getSenderMobileNo(String senderId);
	 
	 
	//changed on 27th july
	 /**
	  * 
	  * @param bankName
	  * @param ifsc
	  * @param branchName
	  * @param serverName
	  * @return
	  */
	 public List<BankDetailsBean> getBankDetails(String bankName, String ifsc, String branchName , String serverName);
	 
	 
	 /**
	  * 
	  * @param mudraSenderPanDetails
	  * @param serverName
	  * @return
	  */
	 public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,String serverName);
	 
	 /**
	  * 
	  * @param mudraSenderPanDetails
	  * @param serverName
	  * @return
	  */
	 public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails,String serverName);
	 
	 
	 public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails,String serverName);
	 
	 
	 /**
	  * 
	  * @param mudraSenderPanDetails
	  * @param serverName
	  * @return
	  */
	 public List<UploadSenderKyc> getPendingKycList(UploadSenderKyc senderKyc,String serverName);
	 
	 
	 public UploadSenderKyc rejectAcceptedSenderKyc(UploadSenderKyc senderKyc,String serverName);
	 
	 /**
	  * 
	  * @param KycConfigBean
	  * @param MudraSenderWallet
	  * @param servicetype
	  * @return boolean
	  */
	 
	 public void saveSenderKycResponse(EkycResponseBean ekycResponseBean);
	 
	 /**
	  * 
	  * @param KycConfigBean
	  * @param MudraSenderWallet
	  * @param servicetype
	  * @return boolean
	  */
	 
	 public EkycResponseBean getEKycSenderData(EkycResponseBean ekycResponseBean);
	 
	 /**
	  * 
	  * @param KycConfigBean
	  * @param MudraSenderWallet
	  * @param servicetype
	  * @return boolean
	  */
	 
	 public boolean setSenderKycMast(KycConfigBean kycconfigbean,SenderKYCBean senderKycBean,String servicetype,String timestamp,String kyctxnid);
	
	 /**
	  * 
	  * @param MudraSenderWallet
	  * @param servicetype
	  * @return KycConfigBean
	  */
	 
	 public KycConfigBean getkycConfig(SenderKYCBean senderKYCBean,String servicetype);
	 
	 public MiniKycResponse updateMiniKyc(String userId,String documentType,String documentId);
	 
	 public IMPSIciciResponseBean fundTransferICICIBankIMPS(String beneAccNo,String beneIfsc,double amount,String refNo,String remName,String remMobileNo);

	 public MudraBeneficiaryWallet registerBeneficiaryCust(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName, String requestId);
		
		public HashMap getAgentSenderBalance(MudraSenderWallet mudra);
		
		public MudraSenderWallet getBeneficiaryList(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId);
		
		public BankDetailsBean graminBankIFSC(String bank);
		
		public MudraSenderWallet getSender(String id, String serverName);
}
