package com.bhartipay.mudra.persistence;

import java.io.InputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cache.CacheFactory;
import com.bhartipay.cache.CacheModel;
import com.bhartipay.cache.TableCache;
import com.bhartipay.cache.TableCacheImpl;
import com.bhartipay.mudra.bean.AgentLedgerBean;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.BankMastBean;
import com.bhartipay.mudra.bean.BankNameListBean;
import com.bhartipay.mudra.bean.DMTApiRequest;
import com.bhartipay.mudra.bean.DMTReportInOut;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.KycConfigBean;
import com.bhartipay.mudra.bean.MerchantDmtTransBean;
import com.bhartipay.mudra.bean.MiniKycResponse;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderPanDetails;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.SenderFavouriteBean;
import com.bhartipay.mudra.bean.SenderFavouriteViewBean;
import com.bhartipay.mudra.bean.SenderKYCBean;
import com.bhartipay.mudra.bean.SenderLedgerBean;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.bean.TransacationLedgerBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.MailSchedular;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.XmlParser;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.ICICIBankIMPS;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;
import com.bhartipay.util.thirdParty.bean.IMPSParameterBean;
/**
 * @author APPNIT
 *
 */
public class MudraDaoImpl implements MudraDao {

	
/*	private static MudraDaoImpl mudraDaoImpl=null;
	private MudraDaoImpl(){
		
	}
	
	public static MudraDaoImpl getMudraDaoImpl(){
		if(mudraDaoImpl==null){
			mudraDaoImpl=new MudraDaoImpl();
		}
		return mudraDaoImpl;
	}
	*/
	
	private static final Logger logger = Logger.getLogger(MudraDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	OTPGeneration oTPGeneration = new OTPGeneration();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
	//private static final String PKEY = "21d805f7-a299-4eac-804c-1f9c5649990c";
	// 21d805f7-a299-4eac-804c-1f9c5649990c
	MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
	//MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
    int level=0;
	@SuppressWarnings("unchecked")
	@Override
	public MudraSenderWallet validateSender(MudraSenderWallet mudraSenderWallet, String serverName, String requestId) {
		MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "*******RequestId******" + requestId+ "|validateSender()|"
				
				+ mudraSenderWallet.getMobileNo());
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Start excution ********************************* validateSender method**:"
				+ mudraSenderWallet.getMobileNo());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		mudraSenderWallet.setStatusCode("1001");
		mudraSenderWallet.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderWallet.getMobileNo() == null || mudraSenderWallet.getMobileNo().isEmpty()
					|| mudraSenderWallet.getMobileNo().length() != 10) {
				mudraSenderWallet.setStatusCode("1104");
				mudraSenderWallet.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderWallet;
			}

			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderWallet.getMobileNo())).list();
			if (senderList.size() == 0) {

				WalletConfiguration walletConfiguration = commanUtilDao
						.getWalletConfiguration(mudraSenderWallet.getAggreatorId());
				transaction = session.beginTransaction();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "*******RequestId******" + requestId
						+ "Sender Not Register With Us Send OTP for Register");
				/*logger.info("*******RequestId******" + requestId
						+ "*******Sender Not Register With Us *********************************************Send OTP for Register");*/
				newOtp = oTPGeneration.generateOTP();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId******" + requestId
						+ "Inside" + newOtp);
				
				/*logger.info(serverName + "******RequestId******" + requestId
						+ "*******Inside *********************************************" + newOtp);*/
				if (!(newOtp == null)) {
					Query insertOtp = session.getNamedQuery("callOTPMANAGE")
							.setParameter("mobile", mudraSenderWallet.getMobileNo())
							.setParameter("aggreatorid", mudraSenderWallet.getAggreatorId())
							.setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					insertOtp.executeUpdate();
				}
				transaction.commit();
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId******" + requestId
							+ "Inside otp send on mobile"
							+ mudraSenderWallet.getMobileNo() + "===otp===" + newOtp);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId******" + requestId
							+ "Inside otp1 send on mobile"
							+ mudraSenderWallet.getMobileNo() + "===otp===" + mudraSenderWallet.getMobileNo());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId******" + requestId
							+ "Inside otp2 send on mobile"
							+ mudraSenderWallet.getMobileNo() + "===otp===" + mudraSenderWallet.getAggreatorId());
					/*logger.info(serverName + "******RequestId******" + requestId
							+ "*******Inside ====================otp send on mobile=========="
							+ mudraSenderWallet.getMobileNo() + "===otp===" + newOtp);
					logger.info(serverName + "******RequestId******" + requestId
							+ "*******Inside ====================otp1 send on mobile=========="
							+ mudraSenderWallet.getMobileNo() + "===otp===" + mudraSenderWallet.getMobileNo());
					logger.info(serverName + "******RequestId******" + requestId
							+ "*******Inside ====================otp2 send on mobile=========="
							+ mudraSenderWallet.getMobileNo() + "===otp===" + mudraSenderWallet.getAggreatorId());
*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderWallet.getMobileNo(),
							commanUtilDao.getsmsTemplet("dmtRegOtp", mudraSenderWallet.getAggreatorId())
									 .replace("<<APPNAME>>",
									  walletConfiguration.getAppname())
									 .replace("<<OTP>>", newOtp),
							"SMS", mudraSenderWallet.getAggreatorId(), "", "", "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				mudraSenderWallet.setStatusCode("2000");
				mudraSenderWallet.setStatusDesc("OTP has been sent on Mobile Number.");
			} else {

				mudraSenderWallet = senderList.get(0);

				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraSenderWallet.getId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status"))
						.list();

				mudraSenderWallet.setBeneficiaryList(benfList);
				List<SenderFavouriteViewBean> senderFavouriteList = getFavouriteList(mudraSenderWallet.getAgentId(),
						serverName, requestId);
				mudraSenderWallet.setSenderFavouriteList(senderFavouriteList);
				mudraSenderWallet.setStatusCode("1000");
				mudraSenderWallet.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId******" + requestId
					+ "|validateSender()|"
					+ "problem in validateSender"+e.getMessage());
			/*logger.debug(serverName + "******RequestId******" + requestId
					+ "*******problem in validateSender=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;
	}

	@Override
	public MudraSenderWallet registerSender(MudraSenderWallet mudraSenderWallet, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId+ "|registerSender()|"
				+ "**Mobile:"
				+ mudraSenderWallet.getMobileNo());
		/*logger.info(serverName + "*****RequestId*******" + requestId
				+ "*******Start excution ************************************* registerSender method**Mobile:"
				+ mudraSenderWallet.getMobileNo());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderWallet.setStatusCode("1001");
		mudraSenderWallet.setStatusDesc("Error");
		Transaction transaction = null;
		String nmpin = "";
		try {
			if (mudraSenderWallet.getMobileNo() == null || mudraSenderWallet.getMobileNo().isEmpty()
					|| mudraSenderWallet.getMobileNo().length() != 10) {
				mudraSenderWallet.setStatusCode("1104");
				mudraSenderWallet.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderWallet;
			}
			if (mudraSenderWallet.getFirstName() == null || mudraSenderWallet.getFirstName().isEmpty()) {
				mudraSenderWallet.setStatusCode("1101");
				mudraSenderWallet.setStatusDesc("First Name can't be empty.");
				return mudraSenderWallet;
			}

			if (mudraSenderWallet.getMpin() == null || mudraSenderWallet.getMpin().isEmpty()
					|| (mudraSenderWallet.getMpin().length() < 4 || mudraSenderWallet.getMpin().length() > 6)) {
				mudraSenderWallet.setStatusCode("1102");
				mudraSenderWallet.setStatusDesc("Invalid MPIN.");
				return mudraSenderWallet;
			}

			if (mudraSenderWallet.getOtp() == null || mudraSenderWallet.getOtp().isEmpty()) {
				mudraSenderWallet.setStatusCode("1103");
				mudraSenderWallet.setStatusDesc("OTP can't be empty.");
				return mudraSenderWallet;
			}

			if (mudraSenderWallet.getAgentId() == null || mudraSenderWallet.getAgentId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1117");
				mudraSenderWallet.setStatusDesc("Agent Id can't be empty.");
				return mudraSenderWallet;
			}

			// validate OTP
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId
					
					+ " mobile number"
					+ mudraSenderWallet.getMobileNo());
			
					
			/*logger.info(serverName + "*****RequestId*******" + requestId
					+ "*******Start excution ********************************************* mobile number"
					+ mudraSenderWallet.getMobileNo());
			logger.info(serverName + "*****RequestId*******" + requestId
					+ "*******Start excution ********************************************* mobile number"
					+ mudraSenderWallet.getMobileNo());*/

			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderWallet.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderWallet.getAggreatorId())).list();
			if (senderList.size() == 0) {

				Query queryOTP = session.createQuery(
						" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
				queryOTP.setString("mobile", mudraSenderWallet.getMobileNo());
				queryOTP.setString("otp", CommanUtil.SHAHashing256(mudraSenderWallet.getOtp()));
				queryOTP.setString("aggreatorid", mudraSenderWallet.getAggreatorId());
				List list = queryOTP.list();
				if (list != null && list.size() != 0) {
					if (new OTPGeneration().validateOTP(mudraSenderWallet.getOtp())) {
					
							nmpin = mudraSenderWallet.getMpin();
							transaction = session.beginTransaction();
							mudraSenderWallet.setId(commanUtilDao.getTrxId("MSENDER",mudraSenderWallet.getAggreatorId()));
							mudraSenderWallet.setMpin(CommanUtil.SHAHashing256(mudraSenderWallet.getMpin()));
							mudraSenderWallet.setStatus("ACTIVE");
							// mudraSenderWallet.setBank(1);
							// mudraSenderWallet.setWallet(1);
							mudraSenderWallet.setKycStatus("MIN KYC");
							mudraSenderWallet.setTransferLimit(0.00);
							mudraSenderWallet.setFavourite("");
							session.save(mudraSenderWallet);
							transaction.commit();
						
						List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
								.add(Restrictions.eq("senderId", mudraSenderWallet.getId()))
								.add(Restrictions.ne("status", "Deleted"))
								.addOrder(Order.asc("status")).list();
						mudraSenderWallet.setBeneficiaryList(benfList);
						mudraSenderWallet.setStatusCode("1000");
						mudraSenderWallet.setStatusDesc("Sender has been registered successfully.");

						/*
						String smstemplet = commanUtilDao.getsmsTemplet("senderReg",
								mudraSenderWallet.getAggreatorId());
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "","", "", serverName +"*****RequestId*******" + requestId
								+ "mudraSenderWallet~````"+ smstemplet);
						*/		
						/*logger.info("*****RequestId*******" + requestId
								+ "*******~~~~~~~~~~~~~~~~~~~~~mudraSenderWallet.getMobileNo()~~~~~~~~~~~~~~~~~~~````"
								+ smstemplet);*/
					/*
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
								mudraSenderWallet.getMobileNo(), smstemplet.replaceAll("<<MPIN>>", nmpin), "SMS",
								mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "OTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					*/

					} else {
						mudraSenderWallet.setStatusCode("1105");
						mudraSenderWallet.setStatusDesc("OTP has expired.");
						return mudraSenderWallet;
					}

				} else {
					mudraSenderWallet.setStatusCode("1106");
					mudraSenderWallet.setStatusDesc("Invalid OTP.");
					return mudraSenderWallet;
				}
			} else {
				mudraSenderWallet.setStatusCode("11206");
				mudraSenderWallet.setStatusDesc("Mobile Number Register With Us.");
				return mudraSenderWallet;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "*****RequestId*******" + requestId
					+ "|registerSender()|"+ "problem in registerSender" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*****RequestId*******" + requestId
					+ "*******problem in registerSender=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;

	}

	@Override
	public BankDetailsBean getBankDetails(BankDetailsBean bankDetailsBean, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId*******" + requestId+"BankID :"+bankDetailsBean.getBankId()
				+ "|getBankDetails()|"
				+ bankDetailsBean.getBankId());
		/*logger.info(serverName + "*****RequestId*******" + requestId
				+ "***Start excution **************11********************** getBankDetails method**:"
				+ bankDetailsBean.getBankId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			if (bankDetailsBean.getBankName() == null || bankDetailsBean.getBankName().isEmpty()) {
				bankDetailsBean.setStatusCode("1107");
				bankDetailsBean.setStatusDesc("Bank Name can't be empty.");
				return bankDetailsBean;
			}

			if (bankDetailsBean.getBeneficiaryAccountNo() == null
					|| bankDetailsBean.getBeneficiaryAccountNo().isEmpty()) {
				bankDetailsBean.setStatusCode("1108");
				bankDetailsBean.setStatusDesc("Beneficiary Account Number can't be empty.");
				return bankDetailsBean;
			}

			bankDetailsBean.setIfscCode("ICIC0003476");
			bankDetailsBean.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			bankDetailsBean.setCity("DELHI");
			bankDetailsBean.setState("DELHI");
			bankDetailsBean.setAddress(" K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");

			bankDetailsBean.setStatusCode("1000");
			bankDetailsBean.setStatusDesc("Bank details populate successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId*******" + requestId
					+ "|getBankDetails()|"
					+ "problem in getBankDetails" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*****RequestId*******" + requestId
					+ "***problem in getBankDetails=========================" + e.getMessage(), e);*/
			bankDetailsBean.setStatusCode("7000");
			bankDetailsBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return bankDetailsBean;
	}

	@Override
	public List getBankDetailsByIFSC(BankDetailsBean bankDetailsBean, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId***" + requestId
				+ "|getBankDetailsByIFSC()|"
				+ " getBankDetailsByIFSC "
				+ bankDetailsBean.getIfscCode()+ "*1122* getBankDetails :"
						+ bankDetailsBean.getBankId()+ "*1122****getBankName "
								+ bankDetailsBean.getState());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId***" + requestId
				+ "*1122* getBankDetails :"
				+ bankDetailsBean.getBankId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId***" + requestId
				+ "**1122****getBankName "
				+ bankDetailsBean.getBranchName());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*****RequestId***" + requestId
				+ "*1122****getBankName "
				+ bankDetailsBean.getState());*/
		
		/*logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************getIfscCode()*********************** getBankDetailsByIFSC method**:"
				+ bankDetailsBean.getIfscCode());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122********************** getBankDetails method**:"
				+ bankDetailsBean.getBankId());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122****getBankName****************** getBankDetails method**:"
				+ bankDetailsBean.getBranchName());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122****getBankName****************** getBankDetails method**:"
				+ bankDetailsBean.getState());*/

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List bankList = new ArrayList();
		try {

			if (bankDetailsBean.getIfscCode() != null && bankDetailsBean.getIfscCode()
					.length() > 0 /*
									 * !(bankDetailsBean.getIfscCode().isEmpty())
									 */) {

				SQLQuery query = session.createSQLQuery(
						"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE ifsccode like :bankId");
				query.setParameter("bankId", "%" + bankDetailsBean.getIfscCode() + "%");
				// query.setString("bankId", bankDetailsBean.getBankId());
				query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
				bankList = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName")
						.addScalar("address").addScalar("city").addScalar("state").list();
				return bankList;

			} else if (!(bankDetailsBean.getBankId().equalsIgnoreCase("-1"))
					|| !(bankDetailsBean.getBankId().isEmpty())) {
				SQLQuery query = null;
				if (bankDetailsBean.getBranchName() == null || bankDetailsBean.getBranchName().isEmpty()) {
					query = session.createSQLQuery(
							"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId and state=:state");
					query.setString("bankId", bankDetailsBean.getBankId());
					query.setString("state", bankDetailsBean.getState());

				} else {
					query = session.createSQLQuery(
							"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId and state=:state and branchname like :BranchName");
					query.setString("bankId", bankDetailsBean.getBankId());
					query.setString("state", bankDetailsBean.getState());
					query.setParameter("BranchName", "%" + bankDetailsBean.getBranchName() + "%");
				}

				/*
				 * SQLQuery query=session.createSQLQuery(
				 * "SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId"
				 * ); query.setString("bankId", bankDetailsBean.getBankId());
				 */
				query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
				bankList = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName")
						.addScalar("address").addScalar("city").addScalar("state").list();

			}

			/*
			 * if (bankDetailsBean.getIfscCode() == null ||
			 * bankDetailsBean.getIfscCode().isEmpty()) {
			 * bankDetailsBean.setStatusCode("1109");
			 * bankDetailsBean.setStatusDesc("IFSC Code can't be empty.");
			 * return bankList; }
			 */

			/*
			 * bankDetailsBean.setIfscCode("ICIC0003476");
			 * bankDetailsBean.setBankName("ICICI BANK LTD");
			 * bankDetailsBean.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			 * bankDetailsBean.setCity("DELHI");
			 * bankDetailsBean.setState("DELHI"); bankDetailsBean.setAddress(
			 * " K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");
			 * bankList.add(bankDetailsBean); BankDetailsBean
			 * bankDetailsBean1=new BankDetailsBean();
			 * bankDetailsBean1.setIfscCode("HDFC0003176");
			 * bankDetailsBean1.setBankName("HDFC BANK ");
			 * bankDetailsBean1.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			 * bankDetailsBean1.setCity("DELHI");
			 * bankDetailsBean1.setState("DELHI"); bankDetailsBean1.setAddress(
			 * " K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");
			 * bankList.add(bankDetailsBean1);
			 */

			bankDetailsBean.setStatusCode("1000");
			bankDetailsBean.setStatusDesc("Bank details populate successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName + "*****RequestId***" + requestId
					+ "problem in getBankDetailsByIFSC" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*****RequestId***" + requestId
					+ "******problem in getBankDetailsByIFSC=========================" + e.getMessage(), e);*/
			bankDetailsBean.setStatusCode("7000");
			bankDetailsBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return bankList;
	}

	@Override
	public MudraBeneficiaryWallet deleteBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean, String serverName,
			String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "*******RequestId****" + requestId
				+ "|deleteBeneficiary()|"
				+ mudraBeneficiaryMastBean.getId());
		/*logger.info(serverName + "*******RequestId****" + requestId
				+ "******Start excution ************************************* deleteBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			if (mudraBeneficiaryMastBean.getId() == null || mudraBeneficiaryMastBean.getId().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1116");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryMastBean;
			}

			Object mudraBeneficiaryMastBeanDel = session.load(MudraBeneficiaryWallet.class,
					mudraBeneficiaryMastBean.getId());
			if (mudraBeneficiaryMastBeanDel != null) {
				transaction = session.beginTransaction();
				session.delete(mudraBeneficiaryMastBeanDel);
				transaction.commit();
				mudraBeneficiaryMastBean.setStatusCode("1000");
				mudraBeneficiaryMastBean.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "*******RequestId****" + requestId
					+ "|deleteBeneficiary()|"
					+ "problem in deleteBeneficiary" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId****" + requestId
					+ "******problem in deleteBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryMastBean.setStatusCode("7000");
			mudraBeneficiaryMastBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryMastBean;
	}

	@Override
	public MudraBeneficiaryWallet registerBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,
			String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "*********RequestId*******" + requestId
				+ "|registerBeneficiary()|"
				+ mudraBeneficiaryMastBean.getAccountNo());
		/*logger.info(serverName + "*********RequestId*******" + requestId
				+ "****Start excution ************************************* registerBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getAccountNo());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1110");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getName() == null || mudraBeneficiaryMastBean.getName().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1101");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Name.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getBankName() == null || mudraBeneficiaryMastBean.getBankName().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1107");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Bank Name.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getAccountNo() == null || mudraBeneficiaryMastBean.getAccountNo().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1108");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Account Number.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getIfscCode() == null || mudraBeneficiaryMastBean.getIfscCode().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1109");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid IFSC Code.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getTransferType() == null
					|| mudraBeneficiaryMastBean.getTransferType().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1111");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Transfer Type.");
				return mudraBeneficiaryMastBean;
			}
			
			/*if(mudraBeneficiaryMastBean.getIsCust() != 1)
			{
			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("id", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mudraBeneficiaryMastBean.getMpin()))).list();
			if (senderList.size() <= 0) {
				mudraBeneficiaryMastBean.setStatusCode("1102");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid MPIN.");
				return mudraBeneficiaryMastBean;
			}
			}*/
			
			
			if(mudraBeneficiaryMastBean.getIsCust() != 1)
			{	
				List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
						.add(Restrictions.eq("id", mudraBeneficiaryMastBean.getSenderId())).list();
				
				if (senderList.size() <= 0) {
					mudraBeneficiaryMastBean.setStatusCode("1102");
					mudraBeneficiaryMastBean.setStatusDesc("Invalid Sender.");
					return mudraBeneficiaryMastBean;
				}
				WalletMastBean agentUser = (WalletMastBean)session.get(WalletMastBean.class, mudraBeneficiaryMastBean.getAgentId());
				if(agentUser.getOtpVerificationRequired()!= null  && agentUser.getOtpVerificationRequired().trim().equals("1"))
				{

					if (mudraBeneficiaryMastBean.getMpin() == null || mudraBeneficiaryMastBean.getMpin().isEmpty()) {
						mudraBeneficiaryMastBean.setStatusCode("1102");
						mudraBeneficiaryMastBean.setStatusDesc("Empty OTP.");
						return mudraBeneficiaryMastBean;
					}
					
					Query queryOTP = session.createQuery(" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
					queryOTP.setString("mobile", senderList.get(0).getMobileNo());
					queryOTP.setString("otp", CommanUtil.SHAHashing256(mudraBeneficiaryMastBean.getMpin()));
					queryOTP.setString("aggreatorid", senderList.get(0).getAggreatorId());
					
					List list = queryOTP.list();
					if (list != null && list.size() != 0) {
							 if(!(new OTPGeneration().validateOTP(mudraBeneficiaryMastBean.getMpin())))
							 {
								 mudraBeneficiaryMastBean.setStatusCode("1102");
									mudraBeneficiaryMastBean.setStatusDesc("Invalid OTP.");
									return mudraBeneficiaryMastBean;
							 }
					}
					else
					{
						mudraBeneficiaryMastBean.setStatusCode("1102");
						mudraBeneficiaryMastBean.setStatusDesc("Invalid OTP.");
						return mudraBeneficiaryMastBean;
					}
				}
				else
				{

					if (mudraBeneficiaryMastBean.getMpin() == null || mudraBeneficiaryMastBean.getMpin().isEmpty()) {
						mudraBeneficiaryMastBean.setStatusCode("1102");
						mudraBeneficiaryMastBean.setStatusDesc("Empty MPIN.");
						return mudraBeneficiaryMastBean;
					}
					if(!senderList.get(0).getMpin().equals(CommanUtil.SHAHashing256(mudraBeneficiaryMastBean.getMpin())))
					{
						mudraBeneficiaryMastBean.setStatusCode("1102");
						mudraBeneficiaryMastBean.setStatusDesc("Invalid MPIN.");
						return mudraBeneficiaryMastBean;
					}
				}
			}

			/*List<MudraBeneficiaryWallet> benList = session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("accountNo", mudraBeneficiaryMastBean.getAccountNo())).list();*/
			/*add by naveen*/
			List<MudraBeneficiaryWallet> benList = session.createCriteria(MudraBeneficiaryWallet.class)
				     .add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
				     .add(Restrictions.eq("accountNo", mudraBeneficiaryMastBean.getAccountNo()))
				     .add(Restrictions.ne("status","Deleted")).list();
			
			
			if (benList.size() > 0) {
				mudraBeneficiaryMastBean.setStatusCode("1124");
				mudraBeneficiaryMastBean.setStatusDesc("Your provided account number is already added with sender.");
				return mudraBeneficiaryMastBean;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			if (activeCount >= 15) {
				mudraBeneficiaryMastBean.setStatusCode("1123");
				mudraBeneficiaryMastBean.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
				return mudraBeneficiaryMastBean;
			}

			transaction = session.beginTransaction();
			mudraBeneficiaryMastBean.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryMastBean.getAggreatorId()));
			// mudraBeneficiaryMastBean.setWallet(1);
			mudraBeneficiaryMastBean.setStatus("Active");
			mudraBeneficiaryMastBean.setVerified("NV");
			session.save(mudraBeneficiaryMastBean);
			transaction.commit();
			mudraBeneficiaryMastBean.setStatusCode("1000");
			mudraBeneficiaryMastBean.setStatusDesc("Beneficiary has been registered successfully.");

			if(mudraBeneficiaryMastBean.getIsCust() != 1){
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					mudraBeneficiaryMastBean.getSenderId());

			String smstemplet = commanUtilDao.getsmsTemplet("addBene", mudraSenderWallet.getAggreatorId());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "~mudraSenderWallet.getMobileNo :````" + smstemplet);
			/*logger.info("*********RequestId*******" + requestId
					+ "****~~~~~~~~~~~~~~~~~~~~~mudraSenderWallet.getMobileNo()~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderWallet.getMobileNo(),
					smstemplet.replaceAll("<<BENE NAME>>", mudraBeneficiaryMastBean.getName())
							.replace("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
							.replace("<<IFSC>>", mudraBeneficiaryMastBean.getIfscCode()),
					"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "OTP");
//			Thread t = new Thread(smsAndMailUtility);
//			t.start();
			
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
		}
		else
		{
			WalletMastBean walletMast = new WalletUserDaoImpl().getUserByMobileNo(mudraBeneficiaryMastBean.getSenderId(), mudraBeneficiaryMastBean.getAggreatorId(), "");
			String smstemplet = commanUtilDao.getsmsTemplet("addBene",walletMast.getAggreatorid());
			logger.info("*********RequestId*******"+requestId+"****~~~~~~~~~~~~~~~~~~~~~mudraSenderMastBean.getMobileNo()~~~~~~~~~~~~~~~~~~~````" + smstemplet);
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "","", walletMast.getMobileno(), smstemplet.replaceAll("<<BENE NAME>>", mudraBeneficiaryMastBean.getName()).replace("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo()).replace("<<IFSC>>", mudraBeneficiaryMastBean.getIfscCode()), "SMS",walletMast.getAggreatorid(),"",walletMast.getName(),"OTP");
			Thread t = new Thread(smsAndMailUtility);
			t.start();
		}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "problem in registerBeneficiary" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*********RequestId*******" + requestId
					+ "****problem in registerBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryMastBean.setStatusCode("7000");
			mudraBeneficiaryMastBean.setStatusDesc("Error");

			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryMastBean;
	}

	@Override
	public MudraSenderWallet forgotMPIN(MudraSenderWallet mudraSenderWallet, String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
				+ "|forgotMPIN()|"
				+ mudraSenderWallet.getId()+"|MobileNo "+ mudraSenderWallet.getMobileNo());
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
				
				+ mudraSenderWallet.getMobileNo());*/
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "******RequestId*******" + requestId
				+ "******Start excution ************************************* forgotMPIN method**:"
				+ mudraSenderWallet.getAggreatorId());*/
		
		/*logger.info(serverName + "******RequestId*******" + requestId
				+ "******Start excution ************************************* forgotMPIN method**:"
				+ mudraSenderWallet.getId());
		logger.info(serverName + "******RequestId*******" + requestId
				+ "******Start excution ************************************* forgotMPIN method**:"
				+ mudraSenderWallet.getMobileNo());
		logger.info(serverName + "******RequestId*******" + requestId
				+ "******Start excution ************************************* forgotMPIN method**:"
				+ mudraSenderWallet.getAggreatorId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderWallet.setStatusCode("1001");
		mudraSenderWallet.setStatusDesc("Error");
		String newOtp = null;
		try {
			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("id", mudraSenderWallet.getId()))
					.add(Restrictions.eq("mobileNo", mudraSenderWallet.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderWallet.getAggreatorId())).list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
					+ "Inside senderList" + senderList.size());
			/*logger.info(serverName + "******RequestId*******" + requestId
					+ "******Inside ====================senderList==========" + senderList.size());*/
			if (senderList.size() > 0) {
				transaction = session.beginTransaction();
				newOtp = oTPGeneration.generateOTP();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
						+ "Inside otp" + newOtp);
				/*logger.info(serverName + "******RequestId*******" + requestId
						+ "******Inside ***********************otp**********************" + newOtp);*/
				if (!(newOtp == null)) {
					Query insertOtp = session.getNamedQuery("callOTPMANAGE")
							.setParameter("mobile", mudraSenderWallet.getMobileNo())
							.setParameter("aggreatorid", mudraSenderWallet.getAggreatorId())
							.setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					insertOtp.executeUpdate();
				}
				transaction.commit();
				WalletConfiguration walletConfiguration = commanUtilDao
						.getWalletConfiguration(mudraSenderWallet.getAggreatorId());
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
							+ "Inside otp send on mobile"
							+ mudraSenderWallet.getMobileNo() + "===otp===" + newOtp);
					/*logger.info(serverName + "******RequestId*******" + requestId
							+ "******Inside ====================otp send on mobile=========="
							+ mudraSenderWallet.getMobileNo() + "===otp===" + newOtp);*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderWallet.getMobileNo(),
							commanUtilDao.getsmsTemplet("forgotMpin", mudraSenderWallet.getAggreatorId())
									/*
									 * .replace("<<APPNAME>>",
									 * walletConfiguration.getAppname())
									 */ .replace("<<OTP>>", newOtp),
							"SMS", mudraSenderWallet.getAggreatorId(), "", "", "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				mudraSenderWallet.setStatusCode("1000");
				mudraSenderWallet.setStatusDesc("OTP has been sent on registered Mobile Number.");
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId*******" + requestId
					+ "|forgotMPIN()|"
					+ "problem in forgotMPIN" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "******RequestId*******" + requestId
					+ "******problem in forgotMPIN=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;
	}

	@Override
	public MudraSenderWallet updateMPIN(MudraSenderWallet mudraSenderWallet, String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
				+ "|updateMPIN()|"
				+ mudraSenderWallet.getId()+"|MobileNo "+ mudraSenderWallet.getMobileNo()+ "|Mpin"+mudraSenderWallet.getMpin()+"|otp "+ mudraSenderWallet.getOtp());
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
				
				+ mudraSenderWallet.getMobileNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getAggreatorId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
				
				+ mudraSenderWallet.getMpin());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "","", "", serverName + "******RequestId***" + requestId
				
				+ mudraSenderWallet.getOtp());
		*/
		/*logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getId());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getMobileNo());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getAggreatorId());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getMpin());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderWallet.getOtp());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderWallet.setStatusCode("1001");
		mudraSenderWallet.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderWallet.getMpin() == null || mudraSenderWallet.getMpin().isEmpty()
					|| (mudraSenderWallet.getMpin().length() < 4 || mudraSenderWallet.getMpin().length() > 6)) {
				mudraSenderWallet.setStatusCode("1102");
				mudraSenderWallet.setStatusDesc("Invalid MPIN.");
				return mudraSenderWallet;
			}
			if (mudraSenderWallet.getOtp() == null || mudraSenderWallet.getOtp().isEmpty()) {
				mudraSenderWallet.setStatusCode("1103");
				mudraSenderWallet.setStatusDesc("OTP can't be empty.");
				return mudraSenderWallet;
			}

			Query queryOTP = session.createQuery(
					" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
			queryOTP.setString("mobile", mudraSenderWallet.getMobileNo());
			queryOTP.setString("otp", CommanUtil.SHAHashing256(mudraSenderWallet.getOtp()));
			queryOTP.setString("aggreatorid", mudraSenderWallet.getAggreatorId());
			List list = queryOTP.list();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
					+ "updateMPIN ~otp :" + mudraSenderWallet.getOtp()
					+ "----" + CommanUtil.SHAHashing256(mudraSenderWallet.getOtp())+"|MobileNo "+ mudraSenderWallet.getMobileNo());
		/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
					
					+ mudraSenderWallet.getMobileNo());*/
			/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~aggreatorid :"
					+ mudraSenderWallet.getAggreatorId());*/
			
			/*logger.info("******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~otp :" + mudraSenderWallet.getOtp()
					+ "----" + CommanUtil.SHAHashing256(mudraSenderWallet.getOtp()));
			logger.info("******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~Mobile Number :"
					+ mudraSenderWallet.getMobileNo());
			logger.info("******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~aggreatorid :"
					+ mudraSenderWallet.getAggreatorId());*/
			if (list != null && list.size() != 0) {
				if (new OTPGeneration().validateOTP(mudraSenderWallet.getOtp())) {

					transaction = session.beginTransaction();
					MudraSenderWallet mudraSenderWalletUpdate = (MudraSenderWallet) session.get(MudraSenderWallet.class,
							mudraSenderWallet.getId());
					mudraSenderWalletUpdate.setMpin(CommanUtil.SHAHashing256(mudraSenderWallet.getMpin()));
					session.update(mudraSenderWalletUpdate);
					transaction.commit();
					mudraSenderWallet.setStatusCode("1000");
					mudraSenderWallet.setStatusDesc("MPIN has been changed successfully.");

					String smstemplet = commanUtilDao.getsmsTemplet("senderMpinChange",
							mudraSenderWallet.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
							+ "senderMpinChange````" + smstemplet);
					/*logger.info("******RequestId***" + requestId
							+ "******~~~~~~~~~~~~~~~~~~~~~senderMpinChange~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderWallet.getMobileNo(),
							smstemplet.replaceAll("<<MPIN>>", mudraSenderWallet.getMpin()), "SMS",
							mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);

				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
							+ "updateMPIN~~~1105");
					/*logger.info("******RequestId***" + requestId
							+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~1105");*/
					mudraSenderWallet.setStatusCode("1105");
					mudraSenderWallet.setStatusDesc("OTP has expired.");
					return mudraSenderWallet;
				}

			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", serverName + "******RequestId***" + requestId
						+ "~updateMPIN ~1106~No data found in walletotpmast");
				/*logger.info("******RequestId***" + requestId
						+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~1106~~~~~~~~~~~No data found in walletotpmast");*/
				mudraSenderWallet.setStatusCode("1106");
				mudraSenderWallet.setStatusDesc("Invalid OTP.");
				return mudraSenderWallet;
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "","", "", serverName + "******RequestId***" + requestId
					+ "|updateMPIN()|"
					+ "problem in updateMPIN" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "******RequestId***" + requestId
					+ "******problem in updateMPIN=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;
	}

	/*
	 * @Override public MudraSenderWallet updateMPIN(MudraSenderWallet
	 * mudraSenderWallet,String serverName) { logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderWallet.getId()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderWallet.getMobileNo()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderWallet.getAggreatorId()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderWallet.getMpin()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderWallet.getOtp()); factory = DBUtil.getSessionFactory();
	 * session = factory.openSession(); mudraSenderWallet.setStatusCode("1001");
	 * mudraSenderWallet.setStatusDesc("Error"); String newOtp = null; try {
	 * 
	 * if (mudraSenderWallet.getMpin() == null ||
	 * mudraSenderWallet.getMpin().isEmpty() ||
	 * (mudraSenderWallet.getMpin().length() < 4 ||
	 * mudraSenderWallet.getMpin().length() > 6)) {
	 * mudraSenderWallet.setStatusCode("1102"); mudraSenderWallet.setStatusDesc(
	 * "Invalid MPIN."); return mudraSenderWallet; } if
	 * (mudraSenderWallet.getOtp() == null ||
	 * mudraSenderWallet.getOtp().isEmpty()) {
	 * mudraSenderWallet.setStatusCode("1103"); mudraSenderWallet.setStatusDesc(
	 * "OTP can't be empty."); return mudraSenderWallet; }
	 * 
	 * Query queryOTP = session.createQuery(
	 * " FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid"
	 * ); queryOTP.setString("mobile", mudraSenderWallet.getMobileNo());
	 * queryOTP.setString("otp",
	 * CommanUtil.SHAHashing256(mudraSenderWallet.getOtp()));
	 * queryOTP.setString("aggreatorid", mudraSenderWallet.getAggreatorId());
	 * List list = queryOTP.list(); if (list != null && list.size() != 0) { if
	 * (new OTPGeneration().validateOTP(mudraSenderWallet.getOtp())) {
	 * 
	 * transaction = session.beginTransaction(); MudraSenderWallet
	 * mudraSenderWalletUpdate = (MudraSenderWallet) session
	 * .get(MudraSenderWallet.class, mudraSenderWallet.getId());
	 * mudraSenderWalletUpdate.setMpin(CommanUtil.SHAHashing256(
	 * mudraSenderWallet.getMpin())); session.update(mudraSenderWalletUpdate);
	 * transaction.commit(); mudraSenderWallet.setStatusCode("1000");
	 * mudraSenderWallet.setStatusDesc("MPIN has been changed successfully.");
	 * 
	 * String smstemplet =
	 * commanUtilDao.getsmsTemplet("senderMpinChange",mudraSenderWallet.
	 * getAggreatorId()); logger.info(
	 * "~~~~~~~~~~~~~~~~~~~~~senderMpinChange~~~~~~~~~~~~~~~~~~~````" +
	 * smstemplet); SmsAndMailUtility smsAndMailUtility = new
	 * SmsAndMailUtility("", "","", mudraSenderWallet.getMobileNo(),
	 * smstemplet.replaceAll("<<MPIN>>", mudraSenderWallet.getMpin()),
	 * "SMS",mudraSenderWallet.getAggreatorId(),"",mudraSenderWallet.
	 * getFirstName(),"OTP"); Thread t = new Thread(smsAndMailUtility);
	 * t.start();
	 * 
	 * 
	 * 
	 * 
	 * } else { mudraSenderWallet.setStatusCode("1105");
	 * mudraSenderWallet.setStatusDesc("OTP has expired."); return
	 * mudraSenderWallet; }
	 * 
	 * } else { mudraSenderWallet.setStatusCode("1106");
	 * mudraSenderWallet.setStatusDesc("Invalid OTP."); return
	 * mudraSenderWallet; }
	 * 
	 * } catch (Exception e) { logger.debug(serverName+
	 * "problem in updateMPIN=========================" + e.getMessage(), e);
	 * mudraSenderWallet.setStatusCode("7000");
	 * mudraSenderWallet.setStatusDesc("Error"); e.printStackTrace();
	 * transaction.rollback(); } finally { session.close(); }
	 * 
	 * return mudraSenderWallet; }
	 */
	@Override
	public Boolean otpSenderResend(String mobileno, String aggreatorid, String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
				+ "|otpSenderResend()|"
				+ " otpSenderResend(mobileno)");
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "********Start excution ********************************************* method otpSenderResend(mobileno)");*/
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query1 = null;
		try {
			Query query = session
					.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileno);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			logger.info(serverName + "*******RequestId******" + requestId
					+ "Inside Delete old otp from otpmast");
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "","", "", serverName + "*******RequestId******" + requestId + "*******~~~~~~~`"
						+ CommanUtil.SHAHashing256(newOtp));
				/*logger.info(serverName + "*******RequestId******" + requestId + "*******~~~~~~~`"
						+ CommanUtil.SHAHashing256(newOtp));*/
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno)
						.setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
							+ "Inside otp send on mobile No :" + mobileno + "===otp :"
							+ newOtp);
					/*logger.info(serverName + "*******RequestId******" + requestId
							+ "*******Inside ====================otp send on mobile==========" + mobileno + "===otp==="
							+ newOtp);*/
					smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,
							commanUtilDao.getsmsTemplet("regOtp", aggreatorid)
									.replace("<<APPNAME>>", walletConfiguration.getAppname())
									.replace("<<OTP>>", newOtp),
							"SMS", aggreatorid, "", "", "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					flag = true;
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
						+ "Inside otp send on mobile No:" + mobileno + "===otp===" + newOtp);
				/*logger.info(serverName + "*******RequestId******" + requestId
						+ "*******Inside ==============otp send on mobile========" + mobileno + "===otp===" + newOtp);*/
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
					+ "|otpSenderResend()|"
					+ "problem in otpSenderResend otp" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "*******problem in otpSenderResend otp==============" + e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
				+ "Inside return flag and end " + flag);
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Inside ====================return flag=== and end=======" + flag);*/
		return flag;

	}
	
	
	
	@Override
	public Boolean refundOtp(String id,String mobileno, String aggreatorid, String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
				+ "|refundOtp()|"
				+ " refundOtp(mobileno)");
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "********Start excution ********************************************* method otpSenderResend(mobileno)");*/
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query1 = null;
		try {
			Query query = session
					.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileno);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			logger.info(serverName + "*******RequestId******" + requestId
					+ "Inside Delete old otp from otpmast");
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "","", "", serverName + "*******RequestId******" + requestId + "*******~~~~~~~`"
						+ CommanUtil.SHAHashing256(newOtp));
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno)
						.setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
							+ "Inside otp send on mobile No :" + mobileno + "===otp :"
							+ newOtp);
					//OTP for Transaction refund of Transaction ID <<id>>  is <<otp>> valid for <<time>> mins. Pls do not share it with anyone <<other>>.
					String smsTemp=commanUtilDao.getsmsTemplet("refundOTP", aggreatorid).replaceAll("<<APPNAME>>",walletConfiguration.getAppname()).replaceAll("<<OTP>>",newOtp);
					
					smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,smsTemp,
							"SMS", aggreatorid, "", "", "OTP");
					
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					flag = true;
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
						+ "Inside otp send on mobile No:" + mobileno + "===otp===" + newOtp);
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
					+ "|otpSenderResend()|"
					+ "problem in otpSenderResend otp" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "*******problem in otpSenderResend otp==============" + e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"", "", "", "", serverName + "*******RequestId******" + requestId
				+ "Inside return flag and end " + flag);
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Inside ====================return flag=== and end=======" + flag);*/
		return flag;

	}

	@Override
	public SenderFavouriteBean setFavourite(SenderFavouriteBean senderFavouriteBean, String serverName,
			String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "****RequestId******" + requestId
				+ "|setFavourite()|"
				+ senderFavouriteBean.getAgentId()+"|MobileNo " +senderFavouriteBean.getMobileNo());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", senderFavouriteBean.getSenderId(), "", serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getSenderId());*/
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "****RequestId******" + requestId
				
				+ senderFavouriteBean.getMobileNo());*/
		
		/*logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getAgentId());
		logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getSenderId());
		logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getMobileNo());*/

		if (senderFavouriteBean.getAgentId() == null || senderFavouriteBean.getAgentId().isEmpty()) {
			senderFavouriteBean.setStatusCode("1117");
			senderFavouriteBean.setStatusDesc("Invalid Agent Id.");
			return senderFavouriteBean;
		}

		if (senderFavouriteBean.getSenderId() == null || senderFavouriteBean.getSenderId().isEmpty()) {
			senderFavouriteBean.setStatusCode("1110");
			senderFavouriteBean.setStatusDesc("Invalid Sender Id.");
			return senderFavouriteBean;
		}

		if (senderFavouriteBean.getMobileNo() == null || senderFavouriteBean.getMobileNo().isEmpty()) {
			senderFavouriteBean.setStatusCode("1104");
			senderFavouriteBean.setStatusDesc("Invalid Mobile Number.");
			return senderFavouriteBean;
		}

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			transaction = session.beginTransaction();
			session.save(senderFavouriteBean);
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					senderFavouriteBean.getSenderId());

			if (mudraSenderWallet.getFavourite() == null || mudraSenderWallet.getFavourite().length() > 0) {
				mudraSenderWallet
						.setFavourite(mudraSenderWallet.getFavourite() + "," + senderFavouriteBean.getAgentId());
			} else {
				mudraSenderWallet.setFavourite(senderFavouriteBean.getAgentId());
			}

			session.update(mudraSenderWallet);
			transaction.commit();

			senderFavouriteBean
					.setSenderFavouriteList(getFavouriteList(senderFavouriteBean.getAgentId(), serverName, requestId));

			senderFavouriteBean.setStatusCode("1000");
			senderFavouriteBean.setStatusDesc("Sender has been marked as Favourite.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "****RequestId******" + requestId
					+ "|setFavourite()|"
					+ "problem in setFavourite" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "****RequestId******" + requestId
					+ "******problem in setFavourite=========================" + e.getMessage(), e);*/
			senderFavouriteBean.setStatusCode("7000");
			senderFavouriteBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return senderFavouriteBean;
	}

	@Override
	public SenderFavouriteBean deleteFavourite(SenderFavouriteBean senderFavouriteBean, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "***RequestId*****" + requestId
			 +"deleteFavourite()"
				+ senderFavouriteBean.getId());
		/*logger.info(serverName + "***RequestId*****" + requestId
				+ "******Start excution ************************************* deleteFavourite method**:"
				+ senderFavouriteBean.getId());*/

		if (senderFavouriteBean.getId() <= 0) {
			senderFavouriteBean.setStatusCode("1118");
			senderFavouriteBean.setStatusDesc("Invalid Favourite Id.");
			return senderFavouriteBean;
		}

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			SenderFavouriteBean senderFavouriteBeanDel = (SenderFavouriteBean) session.get(SenderFavouriteBean.class,
					new Integer(senderFavouriteBean.getId()));

			if (senderFavouriteBeanDel != null) {
				transaction = session.beginTransaction();
				String agnId = senderFavouriteBeanDel.getAgentId();
				session.delete(senderFavouriteBeanDel);
				MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
						senderFavouriteBean.getSenderId());

				String agId = mudraSenderWallet.getFavourite().concat(",");
				agId = agId.replace(agnId + ",", "");
				if (agId.length() > 0) {
					agId = agId.substring(0, agId.length() - 1);
				}
				mudraSenderWallet.setFavourite(agId);
				session.update(mudraSenderWallet);
				transaction.commit();
				senderFavouriteBean.setSenderFavouriteList(
						getFavouriteList(senderFavouriteBean.getAgentId(), serverName, requestId));
				senderFavouriteBean.setStatusCode("1000");
				senderFavouriteBean.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "***RequestId*****" + requestId
					+ "problem in deleteFavourite" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "***RequestId*****" + requestId
					+ "******problem in deleteFavourite=========================" + e.getMessage(), e);*/
			senderFavouriteBean.setStatusCode("7000");
			senderFavouriteBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return senderFavouriteBean;
	}

	// changes has been done for only wallet agent
	@Override
	public List<SenderFavouriteViewBean> getFavouriteList(String agentId, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId
				+ "|getFavouriteList()| Agent id:" + agentId);
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "********Start excution ************************************* getFavouriteList method**:" + agentId);*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<SenderFavouriteViewBean> senderFavouriteList = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT senderid,favouritid,NAME,mobileno,agentid,walletbalance,transferlimit,kycstatus FROM walletsenderfavouriteview WHERE agentid=:agentId");
			query.setParameter("agentId", agentId);
			query.setResultTransformer(Transformers.aliasToBean(SenderFavouriteViewBean.class));
			senderFavouriteList = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId
					+ "|getFavouriteList()|"
					+ "problem in getFavouriteList"+agentId+"????????????????" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "********problem in getFavouriteList*****************************************" + e.getMessage(),
					e);*/

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId
			
				+ "return --"
				+ senderFavouriteList.size());
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "******** **********************return **********************************************************"
				+ senderFavouriteList.size());*/

		return senderFavouriteList;

	}

	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId******" + requestId
				+ "|calculateSurcharge()"
				+ surchargeBean.getAgentId()+ "|calculateSurcharge()"
						+ surchargeBean.getAmount());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId******" + requestId
				+ "|calculateSurcharge()"
				+ surchargeBean.getAmount());*/
		
		/*logger.info(serverName + "****RequestId******" + requestId
				+ "********Start excution =======****************======== method calculateCashinSurcharge(surchargeBean)"
				+ surchargeBean.getAgentId());
		logger.info(serverName + "****RequestId******" + requestId
				+ "********Start excution ========***************======== method calculateCashinSurcharge(surchargeBean)"
				+ surchargeBean.getAmount());*/
		List objectList = null;
		Session session = factory.openSession();
		double surcharge = 0.0;
		double distsurcharge = 0.0;
		double creditSurcharge=0.0;
		double supDistSurcharge=0.0;
		double agentRefund=0.0;
		double aggReturnVal = 0.0;
		double dtdsApply = 0.0;
		try {
			if (surchargeBean.getAgentId() == null || surchargeBean.getAgentId().isEmpty()) {
				surchargeBean.setStatus("4321");
				return surchargeBean;
			}

			if (surchargeBean.getAmount() <= 0) {
				surchargeBean.setStatus("4322");
				return surchargeBean;
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId******" + requestId
					+ "Agnet Id:" + surchargeBean.getAgentId()+ "Amount :" + surchargeBean.getAmount());

			surchargeBean.setTxnCode(15);
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", surchargeBean.getAgentId());
			query.setParameter("txnAmount", surchargeBean.getAmount());
			if (surchargeBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", surchargeBean.getTransType());
			query.setParameter("isBank", "0");
			objectList = query.list();
			 Object[] obj=(Object[])objectList.get(0);
			 surcharge = Double.parseDouble("" + obj[0]);
			distsurcharge = Double.parseDouble("" + obj[1]);
			
			 if(obj[2] != null)
				 creditSurcharge =Double.parseDouble(""+obj[2]);
			 if(obj[3] != null)
				 supDistSurcharge =Double.parseDouble(""+obj[3]);
			 if(obj[4] != null)
				 agentRefund =Double.parseDouble(""+obj[4]);
			 if(obj[5]!=null)
					aggReturnVal = surcharge + Double.parseDouble("" + obj[5]);
			 if(obj[6]!=null)
					dtdsApply = surcharge + Double.parseDouble("" + obj[6]);
			 
				//else
				//	supDistSurcharge=0.0;
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", "****RequestId******" + requestId
						+ " surcharge :" + surcharge); 
			/*logger.info("****RequestId******" + requestId
					+ "******** excution *********************************** surcharge :" + surcharge);*/
			surchargeBean.setSurchargeAmount(surcharge);
			surchargeBean.setDistSurCharge(distsurcharge);
			surchargeBean.setCreditSurcharge(creditSurcharge);
			surchargeBean.setSuDistSurcharge(supDistSurcharge);
			surchargeBean.setAgentRefund(agentRefund);
			surchargeBean.setAggReturnVal(aggReturnVal);
			surchargeBean.setDtdsApply(dtdsApply);
			surchargeBean.setStatus("1000");
		} catch (Exception e) {
			surchargeBean.setStatus("7000");
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "","", "", serverName + "*******RequestId******" + requestId
					+ "problem in calculate Cash in Surcharge" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "********problem in calculateCashinSurcharge**********************************" + e.getMessage(),
					e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return surchargeBean;

	}

	/*
	 * public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean
	 * mudraMoneyTransactionBean,String serverName,String requestId) {
	 * logger.info(serverName +"*******RequestId****"+requestId+
	 * "*****Start excution ******************************************* verifyAccount method******:"
	 * ); factory = DBUtil.getSessionFactory(); session = factory.openSession();
	 * mudraMoneyTransactionBean.setStatusCode("1001");
	 * mudraMoneyTransactionBean.setStatusDesc("Error");
	 * 
	 * try { logger.info(serverName +"*******RequestId****"+requestId+
	 * "*************************mudraMoneyTransactionBean.getSenderId()************* verifyAccount method****:"
	 * +mudraMoneyTransactionBean.getSenderId()); logger.info(serverName
	 * +"*******RequestId****"+requestId+
	 * "*************************mudraMoneyTransactionBean.getBeneficiaryId()************* verifyAccount method****:"
	 * +mudraMoneyTransactionBean.getBeneficiaryId()); logger.info(serverName
	 * +"*******RequestId****"+requestId+
	 * "*************************mudraMoneyTransactionBean.getNarrartion()************* verifyAccount method****:"
	 * +mudraMoneyTransactionBean.getNarrartion()); logger.info(serverName
	 * +"*******RequestId****"+requestId+
	 * "*************************mudraMoneyTransactionBean.getVerificationAmount()************* verifyAccount method****:"
	 * +mudraMoneyTransactionBean.getVerificationAmount());
	 * logger.info(serverName +"*******RequestId****"+requestId+
	 * "*************************mudraMoneyTransactionBean.getWalletId()************* verifyAccount method****:"
	 * +mudraMoneyTransactionBean.getWalletId());
	 * 
	 * 
	 * if (mudraMoneyTransactionBean.getSenderId() == null ||
	 * mudraMoneyTransactionBean.getSenderId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getBeneficiaryId() == null||
	 * mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getNarrartion() == null ||
	 * mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1111");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Transfer Type."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Amount can't be zero."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getWalletId() == null ||
	 * mudraMoneyTransactionBean.getWalletId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Wallet Id can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * 
	 * transaction=session.beginTransaction(); WalletMastBean
	 * walletMastBean=(WalletMastBean)session.get(WalletMastBean.class,
	 * mudraMoneyTransactionBean.getUserId()); MudraSenderWallet
	 * mudraSenderWallet=(MudraSenderWallet)session.get(MudraSenderWallet.class,
	 * mudraMoneyTransactionBean.getSenderId()); MudraBeneficiaryWallet
	 * mudraBeneficiaryMastBean=(MudraBeneficiaryWallet)session.get(
	 * MudraBeneficiaryWallet.class,
	 * mudraMoneyTransactionBean.getBeneficiaryId());
	 * 
	 * //added for d if(!mudraMoneyTransactionBean.getSenderId().trim().equals(
	 * mudraBeneficiaryMastBean.getSenderId().trim())) {
	 * mudraMoneyTransactionBean.setStatusCode("1130");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Beneficiary does not belong to mentioned Sender ID"); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * //added for defect id 13.3 if(mudraBeneficiaryMastBean != null ) {
	 * if(!mudraMoneyTransactionBean.getSenderId().trim().equals(
	 * mudraBeneficiaryMastBean.getSenderId().trim())) {
	 * mudraMoneyTransactionBean.setStatusCode("1130");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Beneficiary does not belong to mentioned Sender ID"); return
	 * mudraMoneyTransactionBean; } } else {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * MudraMoneyTransactionBean
	 * mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean )
	 * mudraMoneyTransactionBean.clone();
	 * 
	 * if (mudraSenderWallet.getTransferLimit()<mudraMoneyTransactionBean.
	 * getTxnAmount()) { mudraMoneyTransactionBean.setStatusCode("1122");
	 * mudraMoneyTransactionBean.setStatusDesc("Transfer Limit exceed."); return
	 * mudraMoneyTransactionBean; } String
	 * ppiTxnIdVerification=commanUtilDao.getTrxId("MMTXN");
	 * logger.info(serverName +"*******RequestId****"+requestId+
	 * "*************************ppiTxnIdVerification************* verifyAccount method****:"
	 * +ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxn.setId("VT"+ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
	 * 
	 * 
	 * mudraMoneyTransactionBeanSaveTxn.setCrAmount(mudraMoneyTransactionBean.
	 * getVerificationAmount());
	 * mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
	 * mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
	 * mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS(Verification)");
	 * session.save(mudraMoneyTransactionBeanSaveTxn); transaction.commit();
	 * 
	 * String walletTxnCode=new
	 * TransactionDaoImpl().walletToAccount(walletMastBean.getId(),
	 * walletMastBean.getWalletid(),mudraMoneyTransactionBean.
	 * getVerificationAmount(),mudraBeneficiaryMastBean.getAccountNo(),
	 * ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),walletMastBean
	 * .getAggreatorid(),mudraMoneyTransactionBean.getAgent(),0.0); String
	 * walletSurChargeTxnCode=new
	 * TransactionDaoImpl().walletToAccountSurcharge(walletMastBean.getId(),
	 * walletMastBean.getWalletid(),mudraMoneyTransactionBean.getSurChargeAmount
	 * (),mudraBeneficiaryMastBean.getAccountNo(),ppiTxnIdVerification,
	 * mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(),
	 * mudraMoneyTransactionBean.getAgent(),0.0);
	 * 
	 * 
	 * if(walletTxnCode.equals("1000") &&
	 * walletSurChargeTxnCode.equalsIgnoreCase("1000")){
	 * transaction=session.beginTransaction();
	 * mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean)session.get(
	 * MudraMoneyTransactionBean.class, "VT"+ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
	 * session.update(mudraMoneyTransactionBeanSaveTxn);
	 * mudraMoneyTransactionBean.setId("VTB"+ppiTxnIdVerification);
	 * mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
	 * mudraMoneyTransactionBean.setDrAmount(mudraMoneyTransactionBean.
	 * getVerificationAmount()); mudraMoneyTransactionBean.setStatus("PENDING");
	 * session.save(mudraMoneyTransactionBean);
	 * 
	 * String xmlInput = "<impsTransactionRequest>" +
	 * "<remAccountNo>143200100201152</remAccountNo>" +
	 * "<custNo>5863713</custNo>" +
	 * "<accountNo>"+mudraBeneficiaryMastBean.getAccountNo()+"</accountNo>" +
	 * "<ifscCode>"+mudraBeneficiaryMastBean.getIfscCode()+"</ifscCode>" +
	 * "<transAmt>"+mudraMoneyTransactionBean.getVerificationAmount()+
	 * "</transAmt>" + "<source>B</source>" +
	 * "<benefName>"+mudraBeneficiaryMastBean.getName()+"</benefName>" +
	 * "<benefMobileNo>"+mudraSenderWallet.getMobileNo()+"</benefMobileNo>" +
	 * "<narration>DMT test</narration>" +
	 * "<remitterUniqueNo>"+mudraMoneyTransactionBean.getId()+
	 * "</remitterUniqueNo>" + "<remitterAdd>Noida</remitterAdd>" +
	 * "<channel>DMT</channel>" + "</impsTransactionRequest>"; String
	 * bankRespXml=new SaraswatBankIMPS().callIMPSService(xmlInput);
	 * logger.info(serverName +"*******RequestId****"+requestId+
	 * "*************************bankRespXml**for money transfer while acc verivication **:"
	 * +bankRespXml); HashMap<String, String> bankResp=new
	 * XmlParser().xmlParser(bankRespXml);
	 * 
	 * if(bankResp.get("response").equalsIgnoreCase("ERROR")){
	 * 
	 * mudraMoneyTransactionBean.setStatus("FAILED");
	 * mudraMoneyTransactionBean.setRemark("Transaction Failed(Verification)");
	 * mudraMoneyTransactionBean.setBankRrn(bankResp.get("rrnNo"));
	 * mudraMoneyTransactionBean.setBankResp(bankRespXml);
	 * session.update(mudraMoneyTransactionBean); transaction.commit();
	 * mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
	 * mudraMoneyTransactionBean.setStatusCode("9000");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Transaction Failed(Verification)");
	 * 
	 * MudraMoneyTransactionBean wallRefundTxnRef=new
	 * MudraMoneyTransactionBean();
	 * 
	 * wallRefundTxnRef.setId("VTR"+ppiTxnIdVerification);
	 * wallRefundTxnRef.setTxnId(ppiTxnIdVerification);
	 * wallRefundTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
	 * wallRefundTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
	 * wallRefundTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.
	 * getBeneficiaryId());
	 * wallRefundTxnRef.setCrAmount(mudraMoneyTransactionBean.
	 * getVerificationAmount()); wallRefundTxnRef.setDrAmount(0.0);
	 * wallRefundTxnRef.setNarrartion("WALLET LOADING(Verification-REFUND)");
	 * wallRefundTxnRef.setStatus("SUCCESS"); wallRefundTxnRef.setRemark(
	 * "Transaction SUCCESSFUL(Verification-REFUND)");
	 * session.save(wallRefundTxnRef);
	 * 
	 * MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef=new
	 * MudraMoneyTransactionBean(); //(MudraMoneyTransactionBean )
	 * mudraMoneyTransactionBean.clone();
	 * 
	 * mudraMoneyTransactionBeanSaveTxnRef.setId("VTRA"+ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean
	 * .getWalletId());
	 * mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean
	 * .getSenderId()); mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(
	 * mudraMoneyTransactionBean.getBeneficiaryId());
	 * mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
	 * mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(mudraMoneyTransactionBean
	 * .getVerificationAmount());
	 * mudraMoneyTransactionBeanSaveTxnRef.setNarrartion(
	 * "REFUND To AGENT(Verification)");
	 * mudraMoneyTransactionBeanSaveTxnRef.setStatus("SUCCESS");
	 * mudraMoneyTransactionBeanSaveTxnRef.setRemark(
	 * "Transaction SUCCESSFUL(Verification-REFUND To AGENT)");
	 * session.save(mudraMoneyTransactionBeanSaveTxnRef);
	 * 
	 * String dmtSettlementCode=new
	 * TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),walletMastBean.
	 * getWalletid(),mudraMoneyTransactionBean.getVerificationAmount(),
	 * ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",
	 * walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent());
	 * String surchargeRefundSettlementCode=new
	 * TransactionDaoImpl().surchargeRefundSettlement(walletMastBean.getId(),
	 * walletMastBean.getWalletid(),mudraMoneyTransactionBean.getSurChargeAmount
	 * (),ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",
	 * walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent());
	 * 
	 * 
	 * 
	 * }else{
	 * 
	 * mudraMoneyTransactionBean.setStatus("SUCCESS");
	 * mudraMoneyTransactionBean.setRemark(
	 * "Transaction SUCCESSFUL(Verification)");
	 * mudraMoneyTransactionBean.setBankRrn(bankResp.get("rrnNo"));
	 * mudraMoneyTransactionBean.setBankResp(bankRespXml);
	 * session.update(mudraMoneyTransactionBean);
	 * 
	 * 
	 * String txnStatus=new
	 * SaraswatBankIMPS().getTransactionStatus(bankResp.get("rrnNo"));
	 * logger.info(serverName +"*******RequestId****"+requestId+
	 * "*************************txnStatus**for  while acc verivication **:"
	 * +txnStatus); HashMap<String, String> txnStatusResp=new
	 * XmlParser().xmlParser(txnStatus);
	 * 
	 * 
	 * if(txnStatusResp.get("benefName").equalsIgnoreCase(
	 * mudraBeneficiaryMastBean.getName())){
	 * mudraBeneficiaryMastBean.setVerified("V");
	 * session.update(mudraBeneficiaryMastBean);
	 * mudraMoneyTransactionBean.setVerified("V");
	 * mudraMoneyTransactionBean.setAccHolderName(txnStatusResp.get("benefName")
	 * +"(V)"); mudraMoneyTransactionBean.setVerificationDesc("Match"); }else{
	 * mudraBeneficiaryMastBean.setVerified("NV");
	 * session.update(mudraBeneficiaryMastBean);
	 * mudraMoneyTransactionBean.setVerified("NV");
	 * mudraMoneyTransactionBean.setAccHolderName(mudraBeneficiaryMastBean.
	 * getName()+"(NV)"); mudraMoneyTransactionBean.setVerificationDesc(
	 * "Not Matched"); }
	 * 
	 * transaction.commit();
	 * 
	 * if(txnStatusResp.get("response").equalsIgnoreCase("SUCCESS")){
	 * mudraMoneyTransactionBean.setAccHolderName(txnStatusResp.get("benefName")
	 * +"(V)"); }else
	 * mudraMoneyTransactionBean.setAccHolderName(mudraBeneficiaryMastBean.
	 * getName()+"(NV)"); } //added on 4nd Aug java.sql.Date sqlDate = new
	 * java.sql.Date(System.currentTimeMillis());
	 * mudraMoneyTransactionBean.setPtytransdt(sqlDate);
	 * mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
	 * mudraMoneyTransactionBean.setStatusCode("1000");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Transaction Successful(Verification)");
	 * 
	 * String smstemplet =
	 * commanUtilDao.getsmsTemplet("b2bMoneyTransfer",mudraSenderWallet.
	 * getAggreatorId()); logger.info("*******RequestId****"+requestId+
	 * "*****~~~~~~~~~~~~~~~~~~~~~verifyAccount.getMobileNo()~~~~~~~~~~~~~~~~~~~````"
	 * + smstemplet.replaceAll("<<MOBILENO>>",
	 * mudraSenderWallet.getMobileNo()).replaceAll("<<AMOUNT>>",
	 * ""+(int)mudraMoneyTransactionBean.getVerificationAmount()).replaceAll(
	 * "<<TXNID>>", ppiTxnIdVerification).replaceAll("<<ACCNO>>",
	 * mudraBeneficiaryMastBean.getAccountNo()).replaceAll("<<IFSCCODE>>",
	 * mudraBeneficiaryMastBean.getIfscCode())); //Your wallet <<MOBILENO>> has
	 * been debited with Rs. <<AMOUNT>>.TID <<TXNID>>.Fund transfer to account
	 * <<ACCNO>>,IFSC <<IFSCCODE>> SmsAndMailUtility smsAndMailUtility = new
	 * SmsAndMailUtility("", "","", mudraSenderWallet.getMobileNo(),
	 * smstemplet.replaceAll("<<MOBILENO>>",
	 * mudraSenderWallet.getMobileNo()).replaceAll("<<AMOUNT>>",
	 * ""+(int)mudraMoneyTransactionBean.getVerificationAmount()).replaceAll(
	 * "<<TXNID>>", ppiTxnIdVerification).replaceAll("<<ACCNO>>",
	 * mudraBeneficiaryMastBean.getAccountNo()).replaceAll("<<IFSCCODE>>",
	 * mudraBeneficiaryMastBean.getIfscCode()),
	 * "SMS",mudraSenderWallet.getAggreatorId(),"",mudraSenderWallet.
	 * getFirstName(),"NOTP"); Thread t = new Thread(smsAndMailUtility);
	 * t.start();
	 * 
	 * 
	 * }
	 * 
	 * }else{ transaction=session.beginTransaction();
	 * mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean)session.get(
	 * MudraMoneyTransactionBean.class, "VT"+ppiTxnIdVerification);
	 * mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
	 * mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
	 * session.update(mudraMoneyTransactionBeanSaveTxn); transaction.commit();
	 * mudraMoneyTransactionBean.setStatusCode(walletTxnCode);
	 * mudraMoneyTransactionBean.setStatusDesc("Velocity check error"); }
	 * 
	 * 
	 * } catch (Exception e) { logger.debug(serverName
	 * +"*******RequestId****"+requestId+
	 * "*****problem in verifyAccount*****************************************"
	 * + e.getMessage(), e); mudraMoneyTransactionBean.setStatusCode("7000");
	 * mudraMoneyTransactionBean.setStatusDesc("Error"); e.printStackTrace();
	 * transaction.rollback(); } finally { session.close(); }
	 * 
	 * return mudraMoneyTransactionBean;
	 * 
	 * }
	 * 
	 */

	/*public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
			
			}*/

	public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
				+ "|verifyAccount()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraMoneyTransactionBean.setStatusCode("1001");
		mudraMoneyTransactionBean.setStatusDesc("Error");

		try {
			Transaction transaction = null;
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
					+ "|verifyAccount()"
					+ mudraMoneyTransactionBean.getBeneficiaryId()+ "|mudraMoneyTransactionBean.getNarrartion"
							+ mudraMoneyTransactionBean.getNarrartion()+ "|mudraMoneyTransactionBean.getVerificationAmount"
									+ mudraMoneyTransactionBean.getVerificationAmount());
		
			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1116");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id.");
				return mudraMoneyTransactionBean;
			}
			
			//to make agents configuable
			Properties prop = new Properties();
				try
				{
			    	 String filename = "Balance.properties";
			    	 InputStream input1 = MailSchedular.class.getResourceAsStream("Balance.properties");
					if (input1 == null) 
					{
						System.out.println("Sorry, unable to find " + filename);		
					}
					prop.load(input1);
					input1.close();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				StringTokenizer split = new StringTokenizer(prop.getProperty("allowedAgent"), "|");
				int isAgentValid=0;
				while(split.hasMoreTokens())
				{
					String token = split.nextToken();
					if(token.equals("ALL"))
					{
						isAgentValid++;
						break;
					}
					else if(mudraMoneyTransactionBean.getUserId().equalsIgnoreCase(token))
					{
						isAgentValid++;
						break;
					}
					else
					{
						continue;
					}
				}
				if(isAgentValid < 1)
				{
					mudraMoneyTransactionBean.setStatusCode("2000");
			 		mudraMoneyTransactionBean.setStatusDesc("Coming soon...");
					return mudraMoneyTransactionBean;
				}
			if (mudraMoneyTransactionBean.getBeneficiaryId() == null
					|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1116");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null
					|| mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1111");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Transfer Type.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() < 0) {
				mudraMoneyTransactionBean.setStatusCode("1120");
				mudraMoneyTransactionBean.setStatusDesc("Amount can't be zero.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1120");
				mudraMoneyTransactionBean.setStatusDesc("Wallet Id can't be empty.");
				return mudraMoneyTransactionBean;
			}

			transaction = session.beginTransaction();
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					mudraMoneyTransactionBean.getUserId());
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					mudraMoneyTransactionBean.getSenderId());
			MudraBeneficiaryWallet mudraBeneficiaryMastBean = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraMoneyTransactionBean.getBeneficiaryId());
			WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
			 superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class,	walletMastBean.getSuperdistributerid());
			}
			
			double senderBalInit , senderUpdatedBal = 0;
			senderBalInit = mudraSenderWallet.getWalletBalance();
			senderUpdatedBal=mudraSenderWallet.getWalletBalance();
			if(walletMastBean.getUsertype() != 2)
			{
				mudraMoneyTransactionBean.setStatusCode("7000");
				mudraMoneyTransactionBean.setStatusDesc("Invalid User.");
				return mudraMoneyTransactionBean;				
			}
				WalletMastBean walletmastAgg = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getAggreatorid());
				List<String> benList=null;
				if(mudraBeneficiaryMastBean!=null){
				SQLQuery benQuery=session.createSQLQuery("select name from mudrabeneficiarymast where "
						+ "accountno='"+mudraBeneficiaryMastBean.getAccountNo()+"' and ifsccode='"+mudraBeneficiaryMastBean.getIfscCode()+"' and verified='V'");
				benList=benQuery.list();
				}
				
				WalletMastBean walletMastBeanAgg=null;
				if(walletmastAgg!=null&&walletmastAgg.getCashDepositeWallet()!=null){
					walletMastBeanAgg=(WalletMastBean)session.get(WalletMastBean.class, walletmastAgg.getCashDepositeWallet());
				}
				
				if (mudraBeneficiaryMastBean != null) {
					if (!mudraMoneyTransactionBean.getSenderId().trim().equals(mudraBeneficiaryMastBean.getSenderId().trim())) {
						mudraMoneyTransactionBean.setStatusCode("1130");
						mudraMoneyTransactionBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
						return mudraMoneyTransactionBean;
					}
				} else {
					mudraMoneyTransactionBean.setStatusCode("1116");
					mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
					return mudraMoneyTransactionBean;
				}

				MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
						.clone();
				mudraMoneyTransactionBeanSaveTxn.setIsBank(0);

				if (mudraSenderWallet.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
					mudraMoneyTransactionBean.setStatusCode("1122");
					mudraMoneyTransactionBean.setStatusDesc("Transfer Limit exceed.");
					return mudraMoneyTransactionBean;
				}
				mudraMoneyTransactionBean.setBankName(mudraBeneficiaryMastBean.getBankName());
				mudraMoneyTransactionBean.setAgentName(walletMastBean.getName());
				mudraMoneyTransactionBean.setAgentMobileNo(walletMastBean.getMobileno());
				double verifyAmt=mudraMoneyTransactionBean.getVerificationAmount();
				double surcharge=0.0;
				double distSurcharge=0.0;
				double supDistributorSurcharge=0.0;
				double agentRefund=0.0;
				String walletTxnCodeAgg ="1000";
				String superDistributorTxnCode="1000";
				String responseCode=null;
				String actCode = null;
				String rrn = null;
				String beneName=null;
				SurchargeBean surchargeBean = new SurchargeBean();
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(verifyAmt);
				surchargeBean.setTransType("VERIFY");
				
				surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);
				if(surchargeBean.getStatus().equals("1000")){
					surcharge=surchargeBean.getSurchargeAmount();
					distSurcharge=surchargeBean.getDistSurCharge();
					supDistributorSurcharge=surchargeBean.getSuDistSurcharge();
					agentRefund=surchargeBean.getAgentRefund();
				}
				else{
					mudraMoneyTransactionBean.setStatusCode("1122");
					mudraMoneyTransactionBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return mudraMoneyTransactionBean;			
				}
				
				if(senderBalInit < verifyAmt &&((new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()))<
						(surcharge+distSurcharge+supDistributorSurcharge+agentRefund) && (new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()))<
						verifyAmt))
				{
					mudraMoneyTransactionBean.setStatusCode("7024");
					mudraMoneyTransactionBean.setStatusDesc("Insufficient Agent Wallet Balance.");
					return mudraMoneyTransactionBean;
				}
				
				/*if((verifyAmt+surcharge+distSurcharge+supDistributorSurcharge)>new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())){
					mudraMoneyTransactionBean.setStatusCode("7024");
					mudraMoneyTransactionBean.setStatusDesc("Insufficient Agent Wallet Balance.");
					return mudraMoneyTransactionBean;
				}*/
				
				
				
				String ppiTxnIdVerification = commanUtilDao.getTrxId("MMTXN",mudraBeneficiaryMastBean.getAggreatorId());//bankTxnIdVerificationppiTxnIdfundTransfer = commanUtilDao.getTrxId("MMTXN");
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
						+ "|verifyAccount()"
						+ ppiTxnIdVerification+"Agentid: "+mudraMoneyTransactionBean.getAgentid());
				/*logger.info(serverName + "*******RequestId****" + requestId
						+ "*************************ppiTxnIdVerification************* verifyAccount method****:"
						+ ppiTxnIdVerification);*/
				String walletTxnCode ="1001";
				String walletSurChargeTxnCode = "1001";
				if(senderBalInit < verifyAmt)
				{
				mudraMoneyTransactionBeanSaveTxn.setId("VT" + ppiTxnIdVerification);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdVerification);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");

				mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(mudraMoneyTransactionBean.getVerificationAmount());
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS(Verification)");
				session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();
				transaction = session.beginTransaction();

				if(senderBalInit < verifyAmt &&walletmastAgg.getWhiteLabel().equals("1")&&transactionDao.getWalletBalance(walletMastBeanAgg.getWalletid())<(verifyAmt+surchargeBean.getCreditSurcharge()))
				{
					mudraMoneyTransactionBean.setStatusCode("7024");
					mudraMoneyTransactionBean.setStatusDesc("Something went wrong please try again later.");
					return mudraMoneyTransactionBean;	
				} 
				
				
					if(senderBalInit < verifyAmt && walletmastAgg.getWhiteLabel().equals("1")){
						walletTxnCodeAgg = transactionDao.walletToAggregatorTransferVerify(ppiTxnIdVerification,walletmastAgg.getCashDepositeWallet(),(verifyAmt),surchargeBean.getCreditSurcharge(), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
					}
					walletTxnCode = new TransactionDaoImpl().walletToAccountVerify(walletMastBean.getId(),
							walletMastBean.getWalletid(), /*mudraMoneyTransactionBean.getVerificationAmount()*/verifyAmt,
							mudraBeneficiaryMastBean.getAccountNo(), ppiTxnIdVerification, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0);
					
					walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurchargeVerifyDist(
							walletMastBean.getId(), walletMastBean.getWalletid(),
							/*mudraMoneyTransactionBean.getSurChargeAmount()*/surcharge+distSurcharge+supDistributorSurcharge, mudraBeneficiaryMastBean.getAccountNo(),
							ppiTxnIdVerification, mudraMoneyTransactionBean.getIpiemi(), walletMastBean.getAggreatorid(),
							mudraMoneyTransactionBean.getAgent(), 0.0,surchargeBean.getDistSurCharge(),walletMastBean.getDistributerid(),supDistributorSurcharge,
							((superDistributorMast.getCashDepositeWallet() == null || superDistributorMast.getCashDepositeWallet().trim().equals(""))
									?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()));
					if (walletTxnCode.equals("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")&&walletTxnCodeAgg.equals("1000")) {
						transaction = session.beginTransaction();
						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session
								.get(MudraMoneyTransactionBean.class, "VT" + ppiTxnIdVerification);
						mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
						session.update(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();
					}
				}
				else
				{
					transaction = session.beginTransaction();
					senderUpdatedBal =senderUpdatedBal - verifyAmt;
					
					/*session.evict(mudraSenderWallet);
					mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,mudraMoneyTransactionBean.getSenderId());
					mudraSenderWallet.setWalletBalance(senderUpdatedBal);
					session.merge(mudraSenderWallet);
					transaction.commit();*/
					updateSenderbalance(senderUpdatedBal, mudraMoneyTransactionBean.getSenderId());
					mudraMoneyTransactionBeanSaveTxn.setId("VTS" + ppiTxnIdVerification);
					mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdVerification);
					mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING(SENDER)");

					mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
					mudraMoneyTransactionBeanSaveTxn.setCrAmount(mudraMoneyTransactionBean.getVerificationAmount());
					mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
					mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
					mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS(Verification)");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
					transaction = session.beginTransaction();
					walletTxnCode ="1000";
					 walletSurChargeTxnCode = "1000";
				}
				
				mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
				mudraMoneyTransactionBean.setAgentid(walletMastBean.getId());
				if (walletTxnCode.equals("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")&&walletTxnCodeAgg.equals("1000")) {
					
					transaction = session.beginTransaction();
					
					mudraMoneyTransactionBean.setId("VTB" + ppiTxnIdVerification);
					mudraMoneyTransactionBean.setIsBank(0);
					mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
					mudraMoneyTransactionBean.setDrAmount(mudraMoneyTransactionBean.getVerificationAmount());
					mudraMoneyTransactionBean.setStatus("PENDING");
					session.saveOrUpdate(mudraMoneyTransactionBean);
					transaction.commit();
					transaction = session.beginTransaction();
					IMPSIciciResponseBean respBean=null;
					if (benList==null||benList.size()==0) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId +
								"calling iciciImps for verify account "+"Agentid: "+mudraMoneyTransactionBean.getAgentid());
						//System.out.println("++++++++++++++++++++++++++++++++calling finoimps for verify account++++++++++++++++++++++");

						//List<String> respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),mudraMoneyTransactionBean.getVerificationAmount(),mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraBeneficiaryMastBean.getName(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getMobileNo(),mudraSenderWallet.getFirstName());
						respBean =fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraMoneyTransactionBean.getVerificationAmount(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId +
								"getting result from ICICI imps after verifying account"+"Agentid: "+mudraMoneyTransactionBean.getAgentid()+"response : "+respBean);
						//System.out.println("++++++++++++++++++++++++++++++++getting result from fino imps after verifying account ++++++++++++++++++++++");
						//System.out.println("++++++++++++++++++++++++++++++++response is +++++++++++++++++++++"+respBean);
						/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId +
								"response is "+respBean +"|Agentid: "+mudraMoneyTransactionBean.getAgentid());*/
						actCode=Integer.toString(respBean.getActCode());
						responseCode=Integer.toString(respBean.getResponseCode());
						beneName = respBean.getBeneName();
						rrn=respBean.getBankRRN();
						transaction = session.beginTransaction();
						} 
					else {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(),
								mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), " already verified beneficiary for wallet");
						String finotrxId = commanUtilDao.getTrxId("ICICI",mudraBeneficiaryMastBean.getAggreatorId());
						respBean = new IMPSIciciResponseBean();
						actCode="0";
						responseCode="0";
						respBean.setActCode(0);
						respBean.setResponseCode(0);
						if(benList!=null&&benList.size()>0&&benList.get(0)!=null&&!benList.get(0).isEmpty())
							beneName =benList.get(0);
						else
							beneName = mudraBeneficiaryMastBean.getName();
						rrn=finotrxId.substring(4);
						
					}
					
					if(respBean.getActCode() != 0) {
						
						if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
							try{	
							TableCache tableCache=CacheFactory.getCache();
							CacheModel model=tableCache.getCache(mudraBeneficiaryMastBean.getBankName());
							if(model==null){
								model=new CacheModel();
								model.setLastAccess(System.currentTimeMillis());
								model.setCount(1);
							}
							else
							{
							 int count=model.getCount();
							 if(count>=TableCacheImpl.TXN_FAILED_COUNT){
								Session sessionForCache=factory.openSession(); 
								Transaction transactionForCache=sessionForCache.beginTransaction();
								Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,2)");
								query.setParameter("bankName",mudraBeneficiaryMastBean.getBankName());
								query.executeUpdate();
								transactionForCache.commit();
							 }
							 count++;
							 model.setLastAccess(System.currentTimeMillis());
							 model.setCount(count);
							}
							tableCache.putCache(mudraBeneficiaryMastBean.getBankName(), model);
							}catch(Exception exception){
								exception.printStackTrace();
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryMastBean.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
										+ mudraMoneyTransactionBean.getAgentid() 
										);
								
							}
							}
						if(respBean.getResponseCode() == 1)
						{
							mudraMoneyTransactionBean.setStatus("FAILED");
							mudraMoneyTransactionBean.setRemark("Transaction Failed(Verification)");
							mudraMoneyTransactionBean.setBankRrn(rrn);
							mudraMoneyTransactionBean.setBankResp("FAILED");
							session.update(mudraMoneyTransactionBean);
							transaction.commit();
							transaction = session.beginTransaction();
							
						}
						else
						{
							mudraMoneyTransactionBean.setStatus("Confirmation Awaited");
							mudraMoneyTransactionBean.setRemark("Transaction Pending");
							mudraMoneyTransactionBean.setBankRrn(rrn);
							mudraMoneyTransactionBean.setBankResp("Confirmation Awaited");
							session.update(mudraMoneyTransactionBean);
							transaction.commit();
							transaction = session.beginTransaction();							
						}

						mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
						mudraMoneyTransactionBean.setStatusCode("9000");
						mudraMoneyTransactionBean.setStatusDesc("Transaction Failed(Verification)");
						if(senderBalInit < verifyAmt)
						{
							

							/*MudraMoneyTransactionBean wallRefundTxnRef = new MudraMoneyTransactionBean();

							wallRefundTxnRef.setId("VTR" + ppiTxnIdVerification);
							wallRefundTxnRef.setIsBank(0);
							wallRefundTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
							wallRefundTxnRef.setTxnId(ppiTxnIdVerification);
							wallRefundTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							wallRefundTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							wallRefundTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							wallRefundTxnRef.setCrAmount(mudraMoneyTransactionBean.getVerificationAmount());
							wallRefundTxnRef.setDrAmount(0.0);
							wallRefundTxnRef.setNarrartion("WALLET LOADING(Verification-REFUND)");
							wallRefundTxnRef.setStatus("SUCCESS");
							wallRefundTxnRef.setRemark("Transaction SUCCESSFUL(Verification-REFUND)");
							transaction=session.beginTransaction();
							session.save(wallRefundTxnRef);*/

							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
							// (MudraMoneyTransactionBean )
							// mudraMoneyTransactionBean.clone();

							mudraMoneyTransactionBeanSaveTxnRef.setId("VTRA" + ppiTxnIdVerification);
							mudraMoneyTransactionBeanSaveTxnRef.setIsBank(0);
							mudraMoneyTransactionBeanSaveTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdVerification);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(mudraMoneyTransactionBean.getVerificationAmount());
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND To AGENT(Verification)");
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(Verification-REFUND To AGENT)");
							session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxnRef);
							transaction.commit();
							transaction = session.beginTransaction();
							 String dmtSettlementCode=new TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),walletMastBean.getWalletid(),
									 mudraMoneyTransactionBean.getVerificationAmount(),ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",
									 walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent());
							 walletSurChargeTxnCode = new TransactionDaoImpl().surchargeRefundSettlement(walletMastBean.getId(),walletMastBean.getWalletid(),(surcharge+distSurcharge+supDistributorSurcharge)
									 ,ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent(),
									 distSurcharge,walletMastBean.getDistributerid(),supDistributorSurcharge,(superDistributorMast.getCashDepositeWallet() == null ?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),0.0,0.0);
							 if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
							 {
								 String AggRefundCode=transactionDao.walletToAggregatorRefund(ppiTxnIdVerification,walletmastAgg.getCashDepositeWallet(),mudraMoneyTransactionBean.getVerificationAmount(),surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),0.0,0.0);
							 }

						}
						else
						{
							senderUpdatedBal = senderUpdatedBal + verifyAmt;
							/*session.evict(mudraSenderWallet);
							mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,mudraMoneyTransactionBean.getSenderId());
							mudraSenderWallet.setWalletBalance(senderUpdatedBal);
							session.merge(mudraSenderWallet);
							transaction.commit();*/
							updateSenderbalance(senderUpdatedBal, mudraMoneyTransactionBean.getSenderId());
							transaction = session.beginTransaction();
							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
							// (MudraMoneyTransactionBean )
							// mudraMoneyTransactionBean.clone();
							// (MudraMoneyTransactionBean )
							// mudraMoneyTransactionBean.clone();

							transaction = session.beginTransaction();
							mudraMoneyTransactionBeanSaveTxnRef.setId("VTRAS" + ppiTxnIdVerification);
							mudraMoneyTransactionBeanSaveTxnRef.setIsBank(0);
							mudraMoneyTransactionBeanSaveTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdVerification);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(verifyAmt);
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND To SENDER(Verification)");
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(Verification-REFUND To SENDER)");
							session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxnRef);
							transaction.commit();
							transaction = session.beginTransaction();
						}
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId +
								mudraMoneyTransactionBean.getTxnId()+"inside if block"+respBean.getTranRefNo()+"|Agentid: "+mudraMoneyTransactionBean.getAgentid());
						//System.out.println(mudraMoneyTransactionBean.getTxnId()+"+++++++++++++++++++ inside if block ++++++++++++++++++++++"+respBean.getTranRefNo());
						
						}
						
						/* if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()){
//								(respTxnId, userId, amount, supDistributorSurcharge, ipImei, aggreatorid, superDistributorId, agent)
								superDistributorTxnCode = transactionDao.walletToSuperDistributorTransferRefund(ppiTxnIdVerification,walletMastBean.getId(),(verifyAmt),surchargeBean.getSuDistSurcharge(), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(),walletMastBean.getSuperdistributerid(), mudraMoneyTransactionBean.getAgent());
							}*/
					  else if(respBean.getActCode() == 0 && respBean.getResponseCode() == 0){
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + mudraMoneyTransactionBean.getTxnId()+"inside else block "+actCode+"|Agentid: "+mudraMoneyTransactionBean.getAgentid());
						//System.out.println(mudraMoneyTransactionBean.getTxnId()+"+++++++++++++++++++ inside else block ++++++++++++++++++++++"+actCode);


						mudraMoneyTransactionBean.setStatus("SUCCESS");
						mudraMoneyTransactionBean.setRemark("Transaction SUCCESSFUL(Verification)");
						mudraMoneyTransactionBean.setBankRrn(rrn);
						mudraMoneyTransactionBean.setBankResp("SUCCESS");
						session.update(mudraMoneyTransactionBean);
						transaction.commit();
						transaction = session.beginTransaction();
						

						if (/* txnStatusResp.get("benefName") */beneName.trim()
								.equalsIgnoreCase(mudraBeneficiaryMastBean.getName().trim())) {
							mudraBeneficiaryMastBean.setVerified("V");
							session.update(mudraBeneficiaryMastBean);
							mudraMoneyTransactionBean.setVerified("V");
							mudraMoneyTransactionBean.setAccHolderName(
									/* txnStatusResp.get("benefName") */beneName + "(V)");
							mudraMoneyTransactionBean.setVerificationDesc("Match");
							// mudraMoneyTransactionBean.set

						} else {
							mudraBeneficiaryMastBean.setVerified("NV");
							session.update(mudraBeneficiaryMastBean);
							mudraMoneyTransactionBean.setVerified("NV");
							mudraMoneyTransactionBean.setAccHolderName(beneName + "(NV)");
							mudraMoneyTransactionBean.setVerificationDesc("Not Matched");
						}

						transaction.commit();

						java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
						mudraMoneyTransactionBean.setPtytransdt(sqlDate);
						mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
						mudraMoneyTransactionBean.setStatusCode("1000");
						mudraMoneyTransactionBean.setStatusDesc("Transaction Successful(Verification)");

						String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer",
								mudraSenderWallet.getAggreatorId());
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
								+ "verifyAccount.getMobileNo"
								+ smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
										.replaceAll("<<AMOUNT>>",
												"" + (int) mudraMoneyTransactionBean.getVerificationAmount())
										.replaceAll("<<TXNID>>", ppiTxnIdVerification)
										.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
										.replaceAll("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")));
						
						/*logger.info("*******RequestId****" + requestId
								+ "*****~~~~~~~~~~~~~~~~~~~~~verifyAccount.getMobileNo()~~~~~~~~~~~~~~~~~~~````"
								+ smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
										.replaceAll("<<AMOUNT>>",
												"" + (int) mudraMoneyTransactionBean.getVerificationAmount())
										.replaceAll("<<TXNID>>", ppiTxnIdVerification)
										.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
										.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()));*/
						// Your wallet <<MOBILENO>> has been debited with Rs.
						// <<AMOUNT>>.TID <<TXNID>>.Fund transfer to account
						// <<ACCNO>>,IFSC <<IFSCCODE>>
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
								mudraSenderWallet.getMobileNo(),
								smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
										.replaceAll("<<AMOUNT>>",
												"" + (int) mudraMoneyTransactionBean.getVerificationAmount())
										.replaceAll("<<TXNID>>", ppiTxnIdVerification)
										.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
										.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()),
								"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "NOTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
						
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}

				} else {
					transaction = session.beginTransaction();
					mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session
							.get(MudraMoneyTransactionBean.class, "VT" + ppiTxnIdVerification);
					mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
					mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
					session.update(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
					if(walletTxnCode.equals("1000")){
						if( walletSurChargeTxnCode.equalsIgnoreCase("1000")){
							if(!walletTxnCodeAgg.equals("1000")){
								mudraMoneyTransactionBean.setStatusCode(walletTxnCodeAgg);
							}else{
								mudraMoneyTransactionBean.setStatusCode("1001");
							}
						}else{
							mudraMoneyTransactionBean.setStatusCode(walletSurChargeTxnCode);
						}
					}else{
						mudraMoneyTransactionBean.setStatusCode(walletTxnCode);
					}
					/*mudraMoneyTransactionBean.setStatusCode(walletTxnCode);*/
					mudraMoneyTransactionBean.setStatusDesc("Velocity check error");
				}

				mudraMoneyTransactionBean
						.setAgentWalletAmount(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()));
				MudraSenderWallet mudraSenderNew = (MudraSenderWallet) session.get(MudraSenderWallet.class,
						mudraSenderWallet.getId());
				mudraMoneyTransactionBean.setSenderLimit(mudraSenderNew.getTransferLimit());

			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + serverName + "*******RequestId****" + requestId
						+ "problem in verifyAccount"+"|Agentid: "+mudraMoneyTransactionBean.getAgentid() + e.getMessage()+" "+e);
				/*logger.debug(
						serverName + "*******RequestId****" + requestId
								+ "*****problem in verifyAccount*****************************************" + e.getMessage(),
						e);*/
				mudraMoneyTransactionBean.setStatusCode("7000");
				mudraMoneyTransactionBean.setStatusDesc("Error");
				e.printStackTrace();
				if(transaction.isActive())
					transaction.rollback();
			} finally {
				if(transaction.isActive())
					transaction.commit();
				session.close();
			}

			return mudraMoneyTransactionBean;

		
	}


	public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + serverName + serverName + "****RequestId****" + requestId
				+  "|fundTransfer()");
		/*logger.info(serverName + "****RequestId****" + requestId
				+ "******Start excution ******************************************* fundTransfer method******:");*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		 Properties prop = new Properties();
		FundTransactionSummaryBean fundTransactionSummaryBean = new FundTransactionSummaryBean();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");
		double refundAgt = 0.0;
		double refundSender=0.0;
		try {
			transaction = session.beginTransaction();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					
					);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() + "****" 
					+ "|getBeneficiaryId() fundTransfer method****:"
					+ mudraMoneyTransactionBean.getBeneficiaryId()+"|getNarrartion() fundTransfer method:"
					+ mudraMoneyTransactionBean.getNarrartion()+ "|getTxnAmount() fundTransfer method:"
							+ mudraMoneyTransactionBean.getTxnAmount());
			TreeMap<String, String> treeMap = new TreeMap<String, String>();
			treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(treeMap);
			if (statusCode != 1000) {
				fundTransactionSummaryBean.setStatusCode("" + statusCode);
				fundTransactionSummaryBean.setStatusDesc("Security error.");
				return fundTransactionSummaryBean;
			}
			

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}
			
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					mudraMoneyTransactionBean.getUserId());
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					mudraMoneyTransactionBean.getSenderId());

			double senderBalInit , senderUpdatedBal = 0;
			senderBalInit = mudraSenderWallet.getWalletBalance();
			senderUpdatedBal=mudraSenderWallet.getWalletBalance();
			//refundSender = senderUpdatedBal;
			if(walletMastBean != null)
			{
				if(walletMastBean.getOtpVerificationRequired() != null && (walletMastBean.getOtpVerificationRequired().equals("1") || walletMastBean.getOtpVerificationRequired().equals("3")))
				{
					if( mudraMoneyTransactionBean.getOtp() == null ||  mudraMoneyTransactionBean.getOtp().isEmpty())
					{
						fundTransactionSummaryBean.setStatusCode("1110");
						fundTransactionSummaryBean.setStatusDesc("OTP cannot be empty.");
						return fundTransactionSummaryBean;
					}
					if(!new WalletUserDaoImpl().validateOTP(mudraSenderWallet.getMobileNo(), walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getOtp()))
					{
						fundTransactionSummaryBean.setStatusCode("1110");
						fundTransactionSummaryBean.setStatusDesc("Invalid OTP.");
						return fundTransactionSummaryBean;
					}
				}else
				{
					if( mudraMoneyTransactionBean.getOtp() == null ||  mudraMoneyTransactionBean.getOtp().isEmpty())
					{
						fundTransactionSummaryBean.setStatusCode("1110");
						fundTransactionSummaryBean.setStatusDesc("MPIN cannot be empty.");
						return fundTransactionSummaryBean;
					}
					if(!mudraSenderWallet.getMpin().equals(CommanUtil.SHAHashing256( mudraMoneyTransactionBean.getOtp())))
					{
						fundTransactionSummaryBean.setStatusCode("1110");
						fundTransactionSummaryBean.setStatusDesc("Invalid MPIN.");
						return fundTransactionSummaryBean;
					}
				}
			}
			
			if (mudraMoneyTransactionBean.getTxnAmount() > 49999.00) {  
				fundTransactionSummaryBean.setStatusCode("7024");
		     fundTransactionSummaryBean.setStatusDesc("Transaction greater than 49,999 INR is not allowed.");
		     return fundTransactionSummaryBean;}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null
					|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null
					|| mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}

			//to make agents configuable
			if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) 
			{
				try
				{
			    	 String filename = "Balance.properties";
			    	 InputStream input1 = MailSchedular.class.getResourceAsStream("Balance.properties");
					if (input1 == null) 
					{
						System.out.println("Sorry, unable to find " + filename);		
					}
					prop.load(input1);
					input1.close();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				StringTokenizer split = new StringTokenizer(prop.getProperty("allowedAgent"), "|");
				int isAgentValid=0;
				while(split.hasMoreTokens())
				{
					String token = split.nextToken();
					if(token.equals("ALL"))
					{
						isAgentValid++;
						break;
					}
					else if(mudraMoneyTransactionBean.getUserId().equalsIgnoreCase(token))
					{
						isAgentValid++;
						break;
					}
					else
					{
						continue;
					}
				}
				if(isAgentValid < 1)
				{
					fundTransactionSummaryBean.setStatusCode("2000");
					fundTransactionSummaryBean.setStatusDesc("Coming soon...");
					return fundTransactionSummaryBean;
				}
			}
			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Wallet Id can't be empty.");
				return fundTransactionSummaryBean;
			}
			

			/**  this is use for checking double transaction.  **/
			CommanUtilDaoImpl commanUtilDaoImpl=new CommanUtilDaoImpl();
			String insertFlag=commanUtilDaoImpl.insertTransDtl(mudraMoneyTransactionBean.getUserId()+"|"+mudraMoneyTransactionBean.getSenderId()+"|"+mudraMoneyTransactionBean.getBeneficiaryId()+"|"+mudraMoneyTransactionBean.getTxnAmount(),mudraMoneyTransactionBean.getNarrartion());
					if(!insertFlag.equalsIgnoreCase("SUCCESS"))
					{
						try{
							fundTransactionSummaryBean.setStatusCode("7111");
							if(!insertFlag.equalsIgnoreCase("FAIL"))
							fundTransactionSummaryBean.setStatusDesc(insertFlag);
							else
							fundTransactionSummaryBean.setStatusDesc("You have already processed transaction with same details please wait for 10 minutes.");
							}catch(Exception e){
								e.printStackTrace();
							}
						
						return fundTransactionSummaryBean;
			}
			

			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double agnWalletTxnAmount = 0;
			double surChargeAmount = 0;
			double distSurcharge=0;
			double supDistSurcharge=0.0;
			double agentRefund=0.0;
			double aggReturnVal=0.0;
			double dtdsApply=0.0;
			String walletAggTxnCode="1000";
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn=null;
			
			WalletMastBean walletmastAgg = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getAggreatorid());
			
			MudraBeneficiaryWallet mudraBeneficiaryMastBean = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraMoneyTransactionBean.getBeneficiaryId());
			WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
			 superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class,	walletMastBean.getSuperdistributerid());
			}
			// added for defect id 14.7
			/*
			 * if(!mudraMoneyTransactionBean.getSenderId().trim().equals(
			 * mudraBeneficiaryMastBean.getSenderId().trim())) {
			 * mudraMoneyTransactionBean.setStatusCode("1130");
			 * mudraMoneyTransactionBean.setStatusDesc(
			 * "Beneficiary does not belong to mentioned Sender ID"); return
			 * fundTransactionSummaryBean; }
			 */

			
			if(walletMastBean.getUsertype() != 2)
			{
				fundTransactionSummaryBean.setStatusCode("7000");
				fundTransactionSummaryBean.setStatusDesc("Invalid User.");
				return fundTransactionSummaryBean;	
			}
			if (mudraBeneficiaryMastBean != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim()
						.equals(mudraBeneficiaryMastBean.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			

			// monthly Transfer Limit validation
			if (mudraSenderWallet.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
				return fundTransactionSummaryBean;
			}

			// Yearly Transfer Limit validation
			/*if (mudraSenderWallet.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderWallet.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}*/

			fundTransactionSummaryBean.setBankName(mudraBeneficiaryMastBean.getBankName());
			fundTransactionSummaryBean.setAgentName(walletMastBean.getName());
			fundTransactionSummaryBean.setAgentMobileNo(walletMastBean.getMobileno());
			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {
				ppiTxnIdfundTransfer = commanUtilDao.getTrxId("MMTXN",mudraBeneficiaryMastBean.getAggreatorId());
			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();
				
				if (mudraMoneyTransactionBean != null && mudraMoneyTransactionBean.getAccHolderName() != null
						&& !mudraMoneyTransactionBean.getAccHolderName().isEmpty()) {
					try {
						System.out.println(mudraMoneyTransactionBean.getAccHolderName());
						mudraBeneficiaryMastBean.setVerified("V");
						mudraBeneficiaryMastBean.setName(mudraMoneyTransactionBean.getAccHolderName());
						session.update(mudraBeneficiaryMastBean);
					} catch (Exception e) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), "************************************* problem in update bene details "
								+ mudraBeneficiaryMastBean.getName());
						
					}
				}
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "|fundTransfer()" + ppiTxnIdfundTransfer);
			/*logger.info(serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
					+ "********************fundTransfer start with fundtxnid*************:" + ppiTxnIdfundTransfer);
*/			String walletTxnCode = "1000";
			String walletSurChargeTxnCode = "1000";
			if ((senderBalInit - mudraMoneyTransactionBean.getTxnAmount()) < 0) {
				agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount() - senderBalInit;
				double amtTxn=mudraMoneyTransactionBean.getTxnAmount();
				refundAgt = agnWalletTxnAmount;
				refundSender = senderBalInit;
				surChargeAmount = 0;
				SurchargeBean surchargeBean = new SurchargeBean();
				// surchargeBean.setAgentId(mudraSenderWallet.getAgentId());
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(agnWalletTxnAmount);
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					surchargeBean.setTransType("NEFT");
				} else {
					surchargeBean.setTransType("IMPS");
				}
				surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

				if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
					surChargeAmount = surchargeBean.getSurchargeAmount();
					distSurcharge = surchargeBean.getDistSurCharge();
					supDistSurcharge=surchargeBean.getSuDistSurcharge();
					agentRefund=surchargeBean.getAgentRefund();
					aggReturnVal=surchargeBean.getAggReturnVal();
					dtdsApply=surchargeBean.getDtdsApply();
					
				} else {
					fundTransactionSummaryBean.setStatusCode("1122");
					fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return fundTransactionSummaryBean;
				}

				if ((surChargeAmount + agnWalletTxnAmount+supDistSurcharge) > new TransactionDaoImpl()
						.getWalletBalance(walletMastBean.getWalletid())) {
					fundTransactionSummaryBean.setStatusCode("7024");
					fundTransactionSummaryBean.setStatusDesc("Insufficient Agent Wallet Balance.");
					return fundTransactionSummaryBean;
				}
				
			
				if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1")&&(surchargeBean.getCreditSurcharge()+agnWalletTxnAmount)>new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())){
				     fundTransactionSummaryBean.setStatusCode("7024");
				     fundTransactionSummaryBean.setStatusDesc("Something went wrong please try again later.");
				     return fundTransactionSummaryBean;
				}
				else if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
				{
					/*DecimalFormat df = new DecimalFormat("###");
					System.out.println(df.format(20005.08/5000));
					if(agnWalletTxnAmount > 5000.00)
					{
						if((agnWalletTxnAmount%5000) == 0)
							countSur = Integer.parseInt(df.format((agnWalletTxnAmount/5000)));
						else
							countSur = Integer.parseInt(df.format((agnWalletTxnAmount/5000)))+1;
					}*/

					/*if (amtTxn > 25000.00) {  
						fundTransactionSummaryBean.setStatusCode("7024");
				     fundTransactionSummaryBean.setStatusDesc("Transaction greater than 25,000 INR is not allowed.");
				     return fundTransactionSummaryBean;}*/
					
					
					walletAggTxnCode = transactionDao.walletToAggregatorTransfer(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),(agnWalletTxnAmount),surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent(),  aggReturnVal, dtdsApply );
			
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() 
						+  "|fundTransfer()" + ppiTxnIdfundTransfer);
				/*logger.info(serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
						+ "********************fundTransfer************* method****:" + ppiTxnIdfundTransfer);*/
				/*mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				session.save(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();*/
				
				if(walletAggTxnCode.equalsIgnoreCase("1000"))
				{
					senderUpdatedBal = 0;
					//remove sender from session
					/*session.evict(mudraSenderWallet);
					mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
							mudraMoneyTransactionBean.getSenderId());
					mudraSenderWallet.setWalletBalance(0);
					session.saveOrUpdate(mudraSenderWallet);
					//session.update(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();*/
					updateSenderbalance(senderUpdatedBal, mudraMoneyTransactionBean.getSenderId());
					transaction = session.beginTransaction();
					
					if(senderBalInit > 0)
					{
						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
								.clone();
						mudraMoneyTransactionBeanSaveTxn.setId("TS" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING(SENDER)");
						mudraMoneyTransactionBeanSaveTxn.setCrAmount(senderBalInit);
						mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
						mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();
						transaction = session.beginTransaction();
					}

					walletTxnCode = new TransactionDaoImpl().walletToAccount(walletMastBean.getId(),
							walletMastBean.getWalletid(), agnWalletTxnAmount, mudraBeneficiaryMastBean.getAccountNo(),
							ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(), walletMastBean.getAggreatorid(),
							mudraMoneyTransactionBean.getAgent(), 0.0);
				}
				
				/*mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				session.save(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();*/
				
				if (walletTxnCode.equalsIgnoreCase("1000") && walletAggTxnCode.equalsIgnoreCase("1000")) {
					/*walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurcharge(walletMastBean.getId(),
							walletMastBean.getWalletid(), surChargeAmount, mudraBeneficiaryMastBean.getAccountNo(),
							ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0);*/ 
							walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurchargeDist(walletMastBean.getId(),
									walletMastBean.getWalletid(),(surChargeAmount +distSurcharge+supDistSurcharge), mudraBeneficiaryMastBean.getAccountNo(),
									ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(),
									walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0,distSurcharge,walletMastBean.getDistributerid(),supDistSurcharge,
									((superDistributorMast.getCashDepositeWallet() == null || superDistributorMast.getCashDepositeWallet().trim().equals(""))? walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund, dtdsApply);
				}

				if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000") && walletAggTxnCode.equalsIgnoreCase("1000")) {
					transaction.commit();
					transaction = session.beginTransaction();
					//mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class, "T" + ppiTxnIdfundTransfer);
					
					mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
							.clone();
					mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
					mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
					mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
					mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
					mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
					mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
					mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
					mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					
				/*	mudraSenderWallet.setWalletBalance(agnWalletTxnAmount+senderUpdatedBal);
					session.saveOrUpdate(mudraSenderWallet);*/
					//session.update(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
					transaction = session.beginTransaction();
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid()
							+ "velocity error verifyAccount method:"
							+ walletTxnCode);
					/*logger.info(serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
							+ "********************velocity error************* verifyAccount method****:"
							+ walletTxnCode);*/
					transaction = session.beginTransaction();
//					mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session
//							.get(MudraMoneyTransactionBean.class, "T" + ppiTxnIdfundTransfer);
					mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
							.clone();
					mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
					mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
					mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
					mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
					mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
					mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
					mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
					mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
					transaction = session.beginTransaction();
				}

			}
			else
			{
				refundSender= mudraMoneyTransactionBean.getTxnAmount() ;
				senderUpdatedBal =senderUpdatedBal - mudraMoneyTransactionBean.getTxnAmount();
				//remove sender from session
				/*session.evict(mudraSenderWallet);
				mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
						mudraMoneyTransactionBean.getSenderId());
				mudraSenderWallet.setWalletBalance(senderUpdatedBal);
				session.saveOrUpdate(mudraSenderWallet);
				transaction.commit();*/
				updateSenderbalance(senderUpdatedBal, mudraMoneyTransactionBean.getSenderId());
				transaction = session.beginTransaction();
				
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
						.clone();
				mudraMoneyTransactionBeanSaveTxn.setId("TS" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING(SENDER)");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(mudraMoneyTransactionBean.getTxnAmount());
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setAgentid(walletMastBean.getId());
				mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
				mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
				session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
				
			/*	mudraSenderWallet.setWalletBalance(agnWalletTxnAmount+senderUpdatedBal);
				session.saveOrUpdate(mudraSenderWallet);*/
				//session.update(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();
				transaction = session.beginTransaction();
			}
			if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000") && walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				int i = 1;
				do {
					MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
							.clone();
					transaction = session.beginTransaction();
					fundSaveTxn.setId("TB" + i + ppiTxnIdfundTransfer);
					fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
					fundSaveTxn.setAgentid(walletMastBean.getId());
					if (txnAmount > 25000) {
						fundSaveTxn.setDrAmount(25000);
						txnAmount = txnAmount - 25000;

					} else {
						fundSaveTxn.setDrAmount(txnAmount);
						txnAmount = 0;
					}
					if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
						fundSaveTxn.setStatus("NEFT INITIATED");
					} else {
						fundSaveTxn.setStatus("PENDING");
					}
					session.saveOrUpdate(fundSaveTxn);
					transaction.commit();
					transaction = session.beginTransaction();
					if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() + "****"
								+ mudraMoneyTransactionBean.getSenderId()
								+ "start insert into daily_neft_icici with txnid"
								+ fundSaveTxn.getId());
						/*logger.info(serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() + "****"
								+ mudraMoneyTransactionBean.getSenderId()
								+ "*************start insert into daily_neft_icici with txnid*************"
								+ fundSaveTxn.getId());*/
						SQLQuery insertQuery = session.createSQLQuery(""
								+ "insert into daily_neft_icici(mode,txnid,debitacc,creditacc,benname,amount,ifsc,status)VALUES(?,?,?,?,?,?,?,?)");
						// insertQuery.setParameter(0,
						// mudraMoneyTransactionBean.getNarrartion());
						insertQuery.setParameter(0, "N");
						insertQuery.setParameter(1, fundSaveTxn.getId());
						insertQuery.setParameter(2, "003105031195");
						insertQuery.setParameter(3, mudraBeneficiaryMastBean.getAccountNo());
						insertQuery.setParameter(4, mudraBeneficiaryMastBean.getName());
						insertQuery.setParameter(5, fundSaveTxn.getDrAmount());
						insertQuery.setParameter(6, mudraBeneficiaryMastBean.getIfscCode());
						insertQuery.setParameter(7, 0);
						insertQuery.executeUpdate();
						session.getTransaction().commit();
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() 
								
								+ "completed insert into daily_neft_icici with txnid"
								+ fundSaveTxn.getId());
						/*logger.info(serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() + "****"
								+ mudraMoneyTransactionBean.getSenderId()
								+ "*************completed insert into daily_neft_icici with txnid*************"
								+ fundSaveTxn.getId());*/
						i++;
					} else {


						IMPSIciciResponseBean respBean =fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),fundSaveTxn.getDrAmount(),fundSaveTxn.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());

							
						transaction = session.beginTransaction();
						
						if(respBean.getActCode() != 0) {
							
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								try{	
								TableCache tableCache=CacheFactory.getCache();
								CacheModel model=tableCache.getCache(mudraBeneficiaryMastBean.getBankName());
								if(model==null){
									model=new CacheModel();
									model.setLastAccess(System.currentTimeMillis());
									model.setCount(1);
								}
								else
								{
								 int count=model.getCount();
								 if(count>=TableCacheImpl.TXN_FAILED_COUNT){
									Session sessionForCache=factory.openSession(); 
									Transaction transactionForCache=sessionForCache.beginTransaction();
									Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,2)");
									query.setParameter("bankName",mudraBeneficiaryMastBean.getBankName());
									query.executeUpdate();
									transactionForCache.commit();
								 }
								 count++;
								 model.setLastAccess(System.currentTimeMillis());
								 model.setCount(count);
								}
								tableCache.putCache(mudraBeneficiaryMastBean.getBankName(), model);
								}catch(Exception exception){
									exception.printStackTrace();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryMastBean.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
											+ mudraMoneyTransactionBean.getAgentid() 
											);
									
								}
								}
							if(respBean.getResponseCode() == 1)
							{
								if(respBean.getBankRRN() != null)
									fundSaveTxn.setBankRrn(respBean.getBankRRN());
								fundSaveTxn.setBankResp("FAILED");
								fundSaveTxn.setStatus("FAILED");
								fundSaveTxn.setRemark("Transaction FAILED");
							}
							else
							{
								if(refundSender > fundSaveTxn.getDrAmount())
								{
									refundSender = refundSender -fundSaveTxn.getDrAmount();
								}
								else
								{
									refundAgt = (refundAgt+refundSender) - fundSaveTxn.getDrAmount();
									refundSender = 0;
								}
								fundSaveTxn.setStatus("Confirmation Awaited");
								fundSaveTxn.setRemark("Transaction Pending");
								if(respBean.getBankRRN() != null)
									fundSaveTxn.setBankRrn(respBean.getBankRRN());
								fundSaveTxn.setBankResp("Confirmation Awaited");
								
							}
							fundSaveTxn.setBankRrn(respBean.getBankRRN());
							fundSaveTxn.setBankResp(respBean.getBankRRN());
							session.update(fundSaveTxn);
							
							transaction.commit();
							transaction = session.beginTransaction();
							if (/*i == 1*/ refundAgt >= agnWalletTxnAmount && refundAgt > 0 ) {
								/*MudraMoneyTransactionBean wallRefundTxnRef = new MudraMoneyTransactionBean();

								wallRefundTxnRef.setId("TR" + ppiTxnIdfundTransfer);
								wallRefundTxnRef.setTxnId(ppiTxnIdfundTransfer);
								wallRefundTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
								wallRefundTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
								wallRefundTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
								wallRefundTxnRef.setCrAmount(agnWalletTxnAmount);
								wallRefundTxnRef.setDrAmount(0.0);
								wallRefundTxnRef.setNarrartion("WALLET LOADING(REFUND)");
								wallRefundTxnRef.setStatus("SUCCESS");
								wallRefundTxnRef.setRemark("Transaction SUCCESSFUL(REFUND)");
								session.save(wallRefundTxnRef);*/


									MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
									// (MudraMoneyTransactionBean )
									// mudraMoneyTransactionBean.clone();

									mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
									mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
									mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
									mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
									mudraMoneyTransactionBeanSaveTxnRef.setAgentid(walletMastBean.getId());
									mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
									mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
									mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(agnWalletTxnAmount);
									mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND To AGENT");
									mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
									mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To AGENT)");
									session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxnRef);
									transaction.commit();
									transaction = session.beginTransaction();
									
									SurchargeBean surchargeBean = new SurchargeBean();
									// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
									surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
									surchargeBean.setAmount(agnWalletTxnAmount);
									if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
										surchargeBean.setTransType("NEFT");
									} else {
										surchargeBean.setTransType("IMPS");
									}
									surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

									if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
										surChargeAmount = surchargeBean.getSurchargeAmount();
										distSurcharge = surchargeBean.getDistSurCharge();
										supDistSurcharge=surchargeBean.getSuDistSurcharge();
										agentRefund=surchargeBean.getAgentRefund();
									} else {
										fundTransactionSummaryBean.setStatusCode("1122");
										fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
										return fundTransactionSummaryBean;
									}
									
									String dmtSettlementCode = new TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),
											walletMastBean.getWalletid(), /* agnWalletTxnAmount */refundAgt,
											ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(), "",
											walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
									String surchargeRefundSettlementCode = new TransactionDaoImpl().surchargeRefundSettlement(
											walletMastBean.getId(), walletMastBean.getWalletid(),
											(surChargeAmount + distSurcharge+supDistSurcharge), ppiTxnIdfundTransfer,
											mudraMoneyTransactionBean.getIpiemi(), "", walletMastBean.getAggreatorid(),
											mudraMoneyTransactionBean.getAgent(), distSurcharge,
											walletMastBean.getDistributerid(),supDistSurcharge,
											((superDistributorMast.getCashDepositeWallet() == null || superDistributorMast.getCashDepositeWallet().equals(""))?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund,dtdsApply);
									
									if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
									{
										//ppiTxnIdfundTransfer,walletMastBean.getId(),( amtTxn ),(countSur * surchargeBean.getCreditSurcharge()), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent()
										String AggRefundCode =  transactionDao.walletToAggregatorRefund(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),refundAgt,surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),aggReturnVal,dtdsApply);
									}
									
									//remove sender from session
									/*session.evict(mudraSenderWallet);
									mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
											mudraMoneyTransactionBean.getSenderId());
									mudraSenderWallet.setWalletBalance(mudraSenderWallet.getWalletBalance()+refundSender);
									session.update(mudraSenderWallet);
									transaction.commit();
									transaction = session.beginTransaction();*/
									Double balance = getSenderbalance( mudraMoneyTransactionBean.getSenderId());
									double upBal = balance+refundSender;
									updateSenderbalance(upBal, mudraMoneyTransactionBean.getSenderId());
									if(upBal > 0)
									{
										MudraMoneyTransactionBean mudraMoneyTransactionBeanSenderRef = new MudraMoneyTransactionBean();
										// (MudraMoneyTransactionBean )
										// mudraMoneyTransactionBean.clone();

										mudraMoneyTransactionBeanSenderRef.setId("TRS" + ppiTxnIdfundTransfer);
										mudraMoneyTransactionBeanSenderRef.setTxnId(ppiTxnIdfundTransfer);
										mudraMoneyTransactionBeanSenderRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
										mudraMoneyTransactionBeanSenderRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
										mudraMoneyTransactionBeanSenderRef.setAgentid(walletMastBean.getId());
										mudraMoneyTransactionBeanSenderRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
										mudraMoneyTransactionBeanSenderRef.setCrAmount(0.0);
										mudraMoneyTransactionBeanSenderRef.setDrAmount(upBal);
										mudraMoneyTransactionBeanSenderRef.setNarrartion("REFUND To SENDER");
										mudraMoneyTransactionBeanSenderRef.setStatus("REFUNDED");
										mudraMoneyTransactionBeanSenderRef.setRemark("Transaction SUCCESSFUL(REFUND To SENDER)");
										session.saveOrUpdate(mudraMoneyTransactionBeanSenderRef);
										transaction.commit();
										transaction = session.beginTransaction();
									}
									
								}
								/*
								 * fundSaveTxn.setBankResp(bankRespXml);
								 * session.update(fundSaveTxn);
								 * transaction.commit();
								 */
								else {	
									//remove sender from session
									/*session.evict(mudraSenderWallet);
									mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
											mudraMoneyTransactionBean.getSenderId());
								mudraSenderWallet.setWalletBalance(mudraSenderWallet.getWalletBalance()+refundSender+refundAgt);
								session.update(mudraSenderWallet);
								transaction.commit();*/
								Double balance = getSenderbalance( mudraMoneyTransactionBean.getSenderId());
								double upBal = balance+refundSender+refundAgt;
								updateSenderbalance(upBal, mudraMoneyTransactionBean.getSenderId());
								if(upBal > 0)
								{
									MudraMoneyTransactionBean mudraMoneyTransactionBeanSenderRef = new MudraMoneyTransactionBean();
									// (MudraMoneyTransactionBean )
									// mudraMoneyTransactionBean.clone();

									mudraMoneyTransactionBeanSenderRef.setId("TRS" + ppiTxnIdfundTransfer);
									mudraMoneyTransactionBeanSenderRef.setTxnId(ppiTxnIdfundTransfer);
									mudraMoneyTransactionBeanSenderRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
									mudraMoneyTransactionBeanSenderRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
									mudraMoneyTransactionBeanSenderRef.setAgentid(walletMastBean.getId());
									mudraMoneyTransactionBeanSenderRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
									mudraMoneyTransactionBeanSenderRef.setCrAmount(0.0);
									mudraMoneyTransactionBeanSenderRef.setDrAmount(upBal);
									mudraMoneyTransactionBeanSenderRef.setNarrartion("REFUND To SENDER");
									mudraMoneyTransactionBeanSenderRef.setStatus("REFUNDED");
									mudraMoneyTransactionBeanSenderRef.setRemark("Transaction SUCCESSFUL(REFUND To SENDER)");
									session.saveOrUpdate(mudraMoneyTransactionBeanSenderRef);
									transaction.commit();
									transaction = session.beginTransaction();
								}
								transaction = session.beginTransaction();
							
							}

							txnAmount = 0;

						} else if(respBean.getActCode() == 0 && respBean.getResponseCode() == 0){
							i++;
							
							if(refundSender > fundSaveTxn.getDrAmount())
							{
								refundSender = refundSender -fundSaveTxn.getDrAmount();
							}
							else
							{
								refundAgt = (refundAgt+refundSender) - fundSaveTxn.getDrAmount();
								refundSender = 0;
							}

							fundSaveTxn.setStatus("SUCCESS");
							fundSaveTxn.setRemark("Transaction SUCCESSFUL");
							fundSaveTxn.setBankRrn(respBean.getBankRRN());
							fundSaveTxn.setBankResp(respBean.getBankRRN());
							session.update(fundSaveTxn);
							
							transaction.commit();
							transaction = session.beginTransaction();
						}
						else
						{
							i++;
							if(refundSender > fundSaveTxn.getDrAmount())
							{
								refundSender = refundSender -fundSaveTxn.getDrAmount();
							}
							else
							{
								refundAgt = (refundAgt+refundSender) - fundSaveTxn.getDrAmount();
								refundSender = 0;
							}
							fundSaveTxn.setStatus("Confirmation Awaited");
							fundSaveTxn.setRemark("Transaction Pending");
							if(respBean.getBankRRN() != null)
								fundSaveTxn.setBankRrn(respBean.getBankRRN());
							fundSaveTxn.setBankResp("Confirmation Awaited");
							
							fundSaveTxn.setBankResp(respBean.getBankRRN());
							session.update(fundSaveTxn);
							
							transaction.commit();
							transaction = session.beginTransaction();
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								try{	
								TableCache tableCache=CacheFactory.getCache();
								CacheModel model=tableCache.getCache(mudraBeneficiaryMastBean.getBankName());
								if(model==null){
									model=new CacheModel();
									model.setLastAccess(System.currentTimeMillis());
									model.setCount(1);
								}
								else
								{
								 int count=model.getCount();
								 if(count>=TableCacheImpl.TXN_FAILED_COUNT){
									Session sessionForCache=factory.openSession(); 
									Transaction transactionForCache=sessionForCache.beginTransaction();
									Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,2)");
									query.setParameter("bankName",mudraBeneficiaryMastBean.getBankName());
									query.executeUpdate();
									transactionForCache.commit();
								 }
								 count++;
								 model.setLastAccess(System.currentTimeMillis());
								 model.setCount(count);
								}
								tableCache.putCache(mudraBeneficiaryMastBean.getBankName(), model);
								}catch(Exception exception){
									exception.printStackTrace();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryMastBean.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
											+ mudraMoneyTransactionBean.getAgentid() 
											);
									
								}
								}
							
						}
						/*mudraSenderWallet.setWalletBalance(refundSender);
						session.merge(mudraSenderWallet);
						transaction.commit();
						transaction = session.beginTransaction();*/
						
					}
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid() 
							+ "start insert into dmtdetailsmast");
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid() 
							+ "completed insert into dmtdetailsmast");
					
				} while (txnAmount != 0);

				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderWallet.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryMastBean.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryMastBean.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryMastBean.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("As applicable.");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());

				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));

				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				fundTransactionSummaryBean
						.setAgentWalletAmount(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()));
				MudraSenderWallet mudraSenderNew=(MudraSenderWallet)session.get(MudraSenderWallet.class, mudraSenderWallet.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());
				String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer", mudraSenderWallet.getAggreatorId());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() 
						+ "getMobileNo()~````"
						+ smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int)mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")));
			
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderWallet.getMobileNo(),
						smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss")),
						"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "NOTP");
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);

			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid()
						+ "velocity error  method****:" + walletTxnCode);
				if(walletTxnCode.equalsIgnoreCase("1000") )
				{
					if(walletSurChargeTxnCode.equalsIgnoreCase("1000") )
					{
						if(!walletAggTxnCode.equalsIgnoreCase("1000"))
						{
							fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
						}
						else
						{
							fundTransactionSummaryBean.setStatusCode("1001");
						}
					}
					else
					{
						/*String dmtSettlementCode = new TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),
								walletMastBean.getWalletid(),  agnWalletTxnAmount refundAgt,
								ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(), "",
								walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
								
								*
								senderUpdatedBal = senderUpdatedBal +refundAgt;
								
								mudraSenderWallet.setWalletBalance(senderUpdatedBal);
								session.merge(mudraSenderWallet);*/
						fundTransactionSummaryBean.setStatusCode(walletSurChargeTxnCode);
					}
				}
				else
				{
					fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				}
				//fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "problem in fundTransfer" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
					+ "problem in fundTransfer=========================" + e.getMessage(), e);*/
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return fundTransactionSummaryBean;

	}
	
	
	
	public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName, String requestId){
		FundTransactionSummaryBean fundTransactionSummaryBean=new FundTransactionSummaryBean();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", mudraMoneyTransactionBean.getTxnId(), serverName +"mudraMoneyTransactionBean()");	
		
		Session session = factory.openSession();
		try{
		
			MudraMoneyTransactionBean mBean=new MudraMoneyTransactionBean();
			TransactionBean transactionBean=new TransactionBean();
			
			Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
			cr.add(Restrictions.eq("txnId", mudraMoneyTransactionBean.getTxnId()));
			
			List<MudraMoneyTransactionBean> results = cr.list();
			if(results!=null&&results.size()>0){
			mBean=results.get(0);
			}
			
			
			String mode="";
			if(results!=null){
				for (MudraMoneyTransactionBean mudraMoneyTransactionBean2 : results) {
					if(mudraMoneyTransactionBean2!=null&&(mudraMoneyTransactionBean2.getNarrartion().equalsIgnoreCase("NEFT")||mudraMoneyTransactionBean2.getNarrartion().equalsIgnoreCase("IMPS"))){
						mode=mudraMoneyTransactionBean2.getNarrartion();
						break;
					}
				}
			}
			
			MudraBeneficiaryWallet mudraBeneficiaryWallet=(MudraBeneficiaryWallet)session.get(MudraBeneficiaryWallet.class, mBean.getBeneficiaryId());
			MudraSenderWallet mudraSenderWallet=(MudraSenderWallet)session.get(MudraSenderWallet.class, mBean.getSenderId());
			
			Criteria criteria= session.createCriteria(TransactionBean.class);
			criteria.add(Restrictions.eq("resptxnid", mudraMoneyTransactionBean.getTxnId()));
			criteria.add(Restrictions.eq("txncode",4));
			List<TransactionBean> tList=criteria.list();
			if(tList!=null&&tList.size()>0){
				transactionBean=tList.get(0);
			}
			
			
			
		fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
		fundTransactionSummaryBean.setStatusCode("1000");
		fundTransactionSummaryBean.setStatusDesc("SUCCESS");
		fundTransactionSummaryBean.setSenderMobile(mudraSenderWallet.getMobileNo());
		fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryWallet.getName());
		fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryWallet.getIfscCode());
		fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryWallet.getAccountNo());
		fundTransactionSummaryBean.setAmount(transactionBean.getTxndebit());
		fundTransactionSummaryBean.setCharges("As applicable.");
		fundTransactionSummaryBean.setMode(mode);


		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		return fundTransactionSummaryBean;
	}



	public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId
				+  "|calculateSurCharge()|" +mudraMoneyTransactionBean.getAgentid());
		/*logger.info(serverName + "****RequestId****" + requestId
				+ "*****Start excution ******************************************* verifyAccount method******:");*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraMoneyTransactionBean.setStatusCode("1001");
		mudraMoneyTransactionBean.setStatusDesc("Error");
		List objectList = null;
		double surcharge = 0.0;
		double agentRefund = 0.0;
		double aggReturnVal = 0.0;
		double dtdsApply = 0.0;
		try {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + " txnAmount :"
					+ mudraMoneyTransactionBean.getTxnAmount()+"|transType"+ mudraMoneyTransactionBean.getTransType());

			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", mudraMoneyTransactionBean.getUserId());
			query.setParameter("txnAmount", mudraMoneyTransactionBean.getTxnAmount());
			if (mudraMoneyTransactionBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", mudraMoneyTransactionBean.getTransType());
			query.setParameter("isBank", "0");
			// query.setBigDecimal("txnAmount",new
			// BigDecimal(mudraMoneyTransactionBean.getTxnAmount()));
			objectList = query.list();
			 Object[] obj=(Object[])objectList.get(0);
			 if(obj.length>1){
				 surcharge=(Double.parseDouble(""+obj[0])+Double.parseDouble(""+obj[1]));	
			if(obj[3]!=null)
						surcharge = surcharge + Double.parseDouble("" + obj[3]); 	
			 }else{
				 surcharge=Double.parseDouble(""+obj[0]);
			 }
			 
			 if(obj[4]!=null)
					agentRefund = surcharge + Double.parseDouble("" + obj[4]);
			 if(obj[5]!=null)
					aggReturnVal = surcharge + Double.parseDouble("" + obj[5]);
			 if(obj[6]!=null)
					dtdsApply = surcharge + Double.parseDouble("" + obj[6]);
			 
		 else{
			 agentRefund=0.0;
		 }
			 
			 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId
						+"|Agentid:" +mudraMoneyTransactionBean.getAgentid()+ " surcharge :" + surcharge);
			/*logger.info("****RequestId****" + requestId
					+ "***** excution *********************************** surcharge :" + surcharge);*/

			mudraMoneyTransactionBean.setSurChargeAmount(surcharge);
			mudraMoneyTransactionBean.setAgentRefund(agentRefund);
			mudraMoneyTransactionBean.setStatusCode("1000");
			mudraMoneyTransactionBean.setStatusDesc("SurCharge Calculated");
			mudraMoneyTransactionBean.setAggReturnVal(aggReturnVal);
			mudraMoneyTransactionBean.setDtdsApply(dtdsApply);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId
					+"|Agentid:" +mudraMoneyTransactionBean.getAgentid()+ "problem in MudraMoneyTransfer" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "****RequestId****" + requestId
					+ "*****problem in MudraMoneyTransfer=========================" + e.getMessage(), e);*/
			mudraMoneyTransactionBean.setStatusCode("7000");
			mudraMoneyTransactionBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraMoneyTransactionBean;

	}

	/*
	 * public MudraMoneyTransactionBean
	 * mudraMoneyTransfer(MudraMoneyTransactionBean
	 * mudraMoneyTransactionBean,String serverName) { logger.info(serverName+
	 * "Start excution ******************************************* MudraMoneyTransfer method******:"
	 * ); factory = DBUtil.getSessionFactory(); session = factory.openSession();
	 * try {
	 * 
	 * if (mudraMoneyTransactionBean.getUserId() == null ||
	 * mudraMoneyTransactionBean.getUserId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1117");
	 * mudraMoneyTransactionBean.setStatusDesc("UserId/AgentId can't be empty."
	 * ); return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getSenderId() == null ||
	 * mudraMoneyTransactionBean.getSenderId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getBeneficiaryId() == null ||
	 * mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
	 * return mudraMoneyTransactionBean; } if
	 * (mudraMoneyTransactionBean.getAggreatorId() == null ||
	 * mudraMoneyTransactionBean.getAggreatorId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1119");
	 * mudraMoneyTransactionBean.setStatusDesc("Aggregator Id can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getWalletId() == null ||
	 * mudraMoneyTransactionBean.getWalletId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Wallet Id can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getSenderId() == null ||
	 * mudraMoneyTransactionBean.getSenderId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1110");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getAmount() <= 0) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Amount can't be zero."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getTransferType() == null ||
	 * mudraMoneyTransactionBean.getTransferType().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1111");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Transfer Type."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * MudraBeneficiaryWallet mudraBeneficiaryMastBean =
	 * (MudraBeneficiaryWallet) session .get(MudraBeneficiaryWallet.class,
	 * mudraMoneyTransactionBean.getBeneficiaryId());
	 * 
	 * mudraMoneyTransactionBean.setBankName(mudraBeneficiaryMastBean.
	 * getBankName());
	 * mudraMoneyTransactionBean.setAccountNo(mudraBeneficiaryMastBean.
	 * getAccountNo());
	 * mudraMoneyTransactionBean.setIfsccode(mudraBeneficiaryMastBean.
	 * getIfscCode());
	 * 
	 * if (mudraMoneyTransactionBean.getBankName() == null ||
	 * mudraMoneyTransactionBean.getBankName().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1107");
	 * mudraMoneyTransactionBean.setStatusDesc("Bank Name can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getAccountNo() == null ||
	 * mudraMoneyTransactionBean.getAccountNo().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1108");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Beneficiary Account Number can't be empty."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getIfsccode() == null ||
	 * mudraMoneyTransactionBean.getIfsccode().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1109");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid IFSC Code."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * transaction = session.beginTransaction(); String sendtrxId =
	 * commanUtilDao.getTrxId("MMTXN");
	 * mudraMoneyTransactionBean.setTxnId(sendtrxId);
	 * mudraMoneyTransactionBean.setStatus("Initiate");
	 * session.save(mudraMoneyTransactionBean); transaction.commit();
	 * 
	 * String walletResp =
	 * transactionDao.walletToAccount(mudraMoneyTransactionBean.getUserId(),
	 * mudraMoneyTransactionBean.getWalletId(),
	 * mudraMoneyTransactionBean.getAmount(),
	 * mudraMoneyTransactionBean.getAccountNo(), sendtrxId,
	 * mudraMoneyTransactionBean.getIpiemi(),
	 * mudraMoneyTransactionBean.getAggreatorId(),
	 * mudraMoneyTransactionBean.getUseragent(),
	 * mudraMoneyTransactionBean.getSurCharge()); logger.info(serverName+
	 * "walletResp ===================== method tansferInAccount(walletToBankTxnMast)"
	 * + walletResp);
	 * 
	 * if (walletResp.equals("1000")) {
	 * 
	 * transaction = session.beginTransaction(); mudraMoneyTransactionBean =
	 * (MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class,
	 * sendtrxId); mudraMoneyTransactionBean = (MudraMoneyTransactionBean)
	 * session.get(MudraMoneyTransactionBean.class, sendtrxId);
	 * mudraMoneyTransactionBean.setStatus("Pending");
	 * mudraMoneyTransactionBean.setWalletTxnStatus("Accepted");
	 * mudraMoneyTransactionBean.setBanktxnstatus("Initiate");
	 * session.update(mudraMoneyTransactionBean); transaction.commit(); // call
	 * Bank API
	 * 
	 * mudraMoneyTransactionBean.setStatusCode("1000");
	 * mudraMoneyTransactionBean.setStatusDesc("Accepted"); } else {
	 * 
	 * transaction = session.beginTransaction(); mudraMoneyTransactionBean =
	 * (MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class,
	 * sendtrxId); mudraMoneyTransactionBean.setStatus("Rejected");
	 * mudraMoneyTransactionBean.setWalletTxnStatus("Rejected");
	 * session.update(mudraMoneyTransactionBean); transaction.commit();
	 * mudraMoneyTransactionBean.setStatusCode(walletResp);
	 * mudraMoneyTransactionBean.setStatusDesc("Rejected from Wallet"); }
	 * 
	 * } catch (Exception e) { logger.debug(serverName+
	 * "problem in MudraMoneyTransfer=========================" +
	 * e.getMessage(), e); mudraMoneyTransactionBean.setStatusCode("7000");
	 * mudraMoneyTransactionBean.setStatusDesc("Error"); e.printStackTrace();
	 * transaction.rollback(); } finally { session.close(); }
	 * 
	 * return mudraMoneyTransactionBean; }
	 */

	public MudraSenderWallet getBeneficiaryListForImport(MudraSenderWallet mudraSenderWallet, String serverName,
			String requestId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "","", "", serverName + "*****RequestId***" + requestId
				+ "|getBeneficiaryListForImport()|");
		/*logger.info(serverName + "*****RequestId***" + requestId
				+ "*****Start excution ************************************* getBeneficiaryListForImport method**:");*/
		if (mudraSenderWallet.getMobileNo() == null || mudraSenderWallet.getMobileNo().isEmpty()) {
			mudraSenderWallet.setStatusCode("1104");
			mudraSenderWallet.setStatusDesc("Invalid Mobile Number.");
			return mudraSenderWallet;
		}
		if (mudraSenderWallet.getMpin() == null || mudraSenderWallet.getMpin().isEmpty()
				|| (mudraSenderWallet.getMpin().length() < 4 || mudraSenderWallet.getMpin().length() > 6)) {
			mudraSenderWallet.setStatusCode("1102");
			mudraSenderWallet.setStatusDesc("Invalid MPIN.");
			return mudraSenderWallet;
		}

		if (mudraSenderWallet.getExportMobile() == null || mudraSenderWallet.getExportMobile().isEmpty()) {
			mudraSenderWallet.setStatusCode("1104");
			mudraSenderWallet.setStatusDesc("Invalid Mobile Number.");
			return mudraSenderWallet;
		}
		try {

			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderWallet.getExportMobile()))
					.add(Restrictions.eq("aggreatorId", mudraSenderWallet.getAggreatorId()))
					.add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mudraSenderWallet.getMpin()))).list();

			if (senderList.size() > 0) {
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", senderList.get(0).getId()))
						.add(Restrictions.ne("status", "Deleted"))
						.list();
				if (benfList.size() > 0) {
					mudraSenderWallet.setBeneficiaryList(benfList);
					mudraSenderWallet.setStatusCode("1000");
					mudraSenderWallet.setStatusDesc("Success");
				} else {
					mudraSenderWallet.setStatusCode("1122");
					mudraSenderWallet.setStatusDesc("Sender does not have any beneficiary.");
				}
			} else {
				mudraSenderWallet.setStatusCode("1121");
				mudraSenderWallet
						.setStatusDesc("This Mobile Number is not registered with us as Sender or Invalid MPIN.");
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "","", "", serverName + "*****RequestId***" + requestId
					+ "problem in getBeneficiaryListForImport" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*****RequestId***" + requestId
					+ "*****problem in getBeneficiaryListForImport********************************" + e.getMessage(),
					e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderWallet;
	}

	public MudraBeneficiaryWallet getBeneficiary(String beneficiaryId, String serverName) {
		MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "","", "", serverName
				+ "|getBeneficiary()|"
				+ beneficiaryId);
		/*logger.info(serverName
				+ "Start excution **********************beneficiaryId*************** getBeneficiary method**:"
				+ beneficiaryId);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraBeneficiaryWallet bResult = new MudraBeneficiaryWallet();
		try {
			// bResult=
			// (MudraBeneficiaryWallet)session.get(MudraBeneficiaryWallet.class,
			// beneficiaryId);
			bResult = (MudraBeneficiaryWallet) session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("id", beneficiaryId)).uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "","", "", serverName + "problem in getBeneficiary" + e.getMessage()+" "+e);
			//logger.debug(serverName + "problem in getBeneficiary=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return bResult;
	}
	
	
	public MudraSenderWallet getSender(String id, String serverName) {
		MudraSenderWallet mudraSenderWallet=new MudraSenderWallet();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "", serverName
				+ "|getSender()|"
				+ id);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraBeneficiaryWallet bResult = new MudraBeneficiaryWallet();
		try {
			// bResult=
			// (MudraBeneficiaryWallet)session.get(MudraBeneficiaryWallet.class,
			// beneficiaryId);
			mudraSenderWallet = (MudraSenderWallet) session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("id", id)).uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "", serverName + "problem in getBeneficiary" + e.getMessage()+" "+e);
			//logger.debug(serverName + "problem in getBeneficiary=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderWallet;
	}

	public MudraBeneficiaryWallet copyBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "",mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****ReqestId****" + requestId
				+ "|copyBeneficiary()|"
				);
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "",mudraBeneficiaryMastBean.getSenderId(), "",serverName + "****ReqestId****" + requestId
				+ "******Start excution ************************getSenderId************* copyBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getSenderId());*/
		/*logger.info(serverName + "****ReqestId****" + requestId
				+ "******Start excution **********************getSenderId*************** copyBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getSenderId());
		logger.info(serverName + "****ReqestId****" + requestId
				+ "******Start excution ************************getSenderId************* copyBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getSenderId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraBeneficiaryMastBean.setStatusCode("1001");
		try {

			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1116");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getId() == null || mudraBeneficiaryMastBean.getId().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1116");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryMastBean;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "",mudraBeneficiaryMastBean.getSenderId(), "",serverName + "****ReqestId****" + requestId
					+ "Number of active Beneficiary :" + activeCount);
			/*logger.info(serverName + "****ReqestId****" + requestId
					+ "******Number ************************of************* active Beneficiary  **:" + activeCount);*/
			if (activeCount >= 15) {
				mudraBeneficiaryMastBean.setStatusCode("1123");
				mudraBeneficiaryMastBean.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before importing a new beneficiary.");
				return mudraBeneficiaryMastBean;
			}

			MudraBeneficiaryWallet mudraBeneficiaryMastCopy = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraBeneficiaryMastBean.getId());

			if (mudraBeneficiaryMastCopy == null) {
				mudraBeneficiaryMastBean.setStatusCode("1116");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryMastBean;
			} else {

				List<MudraBeneficiaryWallet> benList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
						.add(Restrictions.eq("accountNo", mudraBeneficiaryMastCopy.getAccountNo()))
						.add(Restrictions.ne("status", "Deleted")).list();
				if (benList.size() > 0) {
					mudraBeneficiaryMastBean.setStatusCode("1124");
					mudraBeneficiaryMastBean.setStatusDesc("Your provided beneficiary is already added with sender.");
					return mudraBeneficiaryMastBean;
				}

				transaction = session.beginTransaction();
				MudraBeneficiaryWallet mudraBeneficiaryMastpast = (MudraBeneficiaryWallet) mudraBeneficiaryMastCopy
						.cloneMe();

				mudraBeneficiaryMastpast.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryMastpast.getAggreatorId()));
				mudraBeneficiaryMastpast.setStatus("Active");
				mudraBeneficiaryMastpast.setSenderId(mudraBeneficiaryMastBean.getSenderId());

				session.save(mudraBeneficiaryMastpast);
				transaction.commit();
				mudraBeneficiaryMastBean.setName(mudraBeneficiaryMastpast.getName());
				mudraBeneficiaryMastBean.setAccountNo(mudraBeneficiaryMastpast.getAccountNo());
				mudraBeneficiaryMastBean.setTransferType(mudraBeneficiaryMastpast.getTransferType());
				mudraBeneficiaryMastBean.setIfscCode(mudraBeneficiaryMastpast.getIfscCode());
				mudraBeneficiaryMastBean.setStatus(mudraBeneficiaryMastpast.getStatus());
				mudraBeneficiaryMastBean.setVerified(mudraBeneficiaryMastpast.getVerified());
				mudraBeneficiaryMastBean.setStatusCode("1000");
				mudraBeneficiaryMastBean.setStatusDesc("Beneficiary Imported.");
				mudraBeneficiaryMastBean.setId(mudraBeneficiaryMastpast.getId());
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "",mudraBeneficiaryMastBean.getSenderId(), "",serverName + "****ReqestId****" + requestId
					+ "problem in activeBeneficiary" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "****ReqestId****" + requestId
					+ "******problem in activeBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryMastBean.setStatusCode("7000");
			mudraBeneficiaryMastBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryMastBean;
	}

	public MudraSenderWallet activeBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean, String serverName,
			String requestId) {																																																		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId****" + requestId
				+ "|activeBeneficiary()|"
				+ mudraBeneficiaryMastBean.getId());
		//commonlogger.log(INFO,this.getClass().getName(),aggregatorid,walletid,userid,senderid,txnid,"")
		/*logger.info(serverName + "****RequestId****" + requestId
				+ "*******Start excution **********************getId*************** activeBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getId());*/
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId****" + requestId
				+ "*******Start excution ************************getSenderId************* activeBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getSenderId());*/

		/*logger.info(serverName + "****RequestId****" + requestId
				+ "*******Start excution ************************getSenderId************* activeBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getSenderId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderWallet mudraSenderWallet = new MudraSenderWallet();
		mudraSenderWallet.setStatusCode("1001");
		try {

			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1110");
				mudraSenderWallet.setStatusDesc("Invalid Sender Id.");
				return mudraSenderWallet;
			}

			if (mudraBeneficiaryMastBean.getId() == null || mudraBeneficiaryMastBean.getId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId****" + requestId
					+ "Number of active Beneficiary  **:" + activeCount);
			/*logger.info(serverName + "****RequestId****" + requestId
					+ "*******Number ************************of************* active Beneficiary  **:" + activeCount);*/
			if (activeCount >= 15) {
				mudraSenderWallet.setStatusCode("1123");
				mudraSenderWallet.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before activating a new beneficiary.");
				return mudraSenderWallet;
			}

			MudraBeneficiaryWallet mudraBeneficiaryMastUpdate = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraBeneficiaryMastBean.getId());

			if (mudraBeneficiaryMastUpdate == null) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			} else {

				// added for defect id 8.2
				if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryMastBean.getSenderId())) {
					mudraSenderWallet.setStatusCode("1130");
					mudraSenderWallet.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraSenderWallet;
				}

				transaction = session.beginTransaction();
				mudraBeneficiaryMastUpdate.setStatus("Active");
				session.update(mudraBeneficiaryMastUpdate);
				transaction.commit();
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				mudraSenderWallet.setBeneficiaryList(benfList);

				mudraSenderWallet.setStatusCode("1000");
				mudraSenderWallet.setStatusDesc("Beneficiary Activated.");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId****" + requestId
					+ "problem in activeBeneficiary" + e.getMessage());
			/*logger.debug(serverName + "****RequestId****" + requestId
					+ "*******problem in activeBeneficiary=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderWallet;
	}

	public MudraSenderWallet deActiveBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean, String serverName,
			String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId******" + requestId
				+ "|deActiveBeneficiary()|" 
				+ mudraBeneficiaryMastBean.getId());
		/*logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* deActiveBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderWallet mudraSenderWallet = new MudraSenderWallet();
		mudraSenderWallet.setStatusCode("1001");
		try {

			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1110");
				mudraSenderWallet.setStatusDesc("Invalid Sender Id.");
				return mudraSenderWallet;
			}
			if (mudraBeneficiaryMastBean.getId() == null || mudraBeneficiaryMastBean.getId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			}

			MudraBeneficiaryWallet mudraBeneficiaryMastUpdate = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraBeneficiaryMastBean.getId());

			if (mudraBeneficiaryMastUpdate == null) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			} else {
				// added for defect id 7.1
				if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryMastBean.getSenderId())) {
					mudraSenderWallet.setStatusCode("1130");
					mudraSenderWallet.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraSenderWallet;
				}

				transaction = session.beginTransaction();
				mudraBeneficiaryMastUpdate.setStatus("Deactive");
				session.update(mudraBeneficiaryMastUpdate);
				transaction.commit();
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status"))
						.list();
				mudraSenderWallet.setBeneficiaryList(benfList);
				mudraSenderWallet.setStatusCode("1000");
				mudraSenderWallet.setStatusDesc("Beneficiary Deactivated.");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId******" + requestId
					+ "problem in deActiveBeneficiary" + e.getMessage());
			/*logger.debug(serverName + "****RequestId******" + requestId
					+ "******problem in deActiveBeneficiary=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;
	}
//for deleted beneficiary wallet start
	public MudraSenderWallet deletedBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean, String serverName,
			String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId******" + requestId
				+ "|deletedBeneficiary()|"
				+ mudraBeneficiaryMastBean.getId());
		/*logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* deletedBeneficiary method**:"
				+ mudraBeneficiaryMastBean.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderWallet mudraSenderWallet = new MudraSenderWallet();
		mudraSenderWallet.setStatusCode("1001");
		try {

			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1110");
				mudraSenderWallet.setStatusDesc("Invalid Sender Id.");
				return mudraSenderWallet;
			}
			if (mudraBeneficiaryMastBean.getId() == null || mudraBeneficiaryMastBean.getId().isEmpty()) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			}

			MudraBeneficiaryWallet mudraBeneficiaryMastUpdate = (MudraBeneficiaryWallet) session
					.get(MudraBeneficiaryWallet.class, mudraBeneficiaryMastBean.getId());

			if (mudraBeneficiaryMastUpdate == null) {
				mudraSenderWallet.setStatusCode("1116");
				mudraSenderWallet.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderWallet;
			} else {
				// added for defect id 7.1
				if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryMastBean.getSenderId())) {
					mudraSenderWallet.setStatusCode("1130");
					mudraSenderWallet.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraSenderWallet;
				}
				///---change
				
				//String delcheck = "SELECT count(1) FROM remittanceledger WHERE beneficiaryid=:beneficiaryid";
				//Query query = session.createSQLQuery(delcheck);				
				//query.setParameter("beneficiaryid" , mudraBeneficiaryMastBean.getId());
		
		
				List<String> delcheck = session.createCriteria(MudraMoneyTransactionBean.class)
					      .add(Restrictions.eq("beneficiaryId", mudraBeneficiaryMastBean.getId()))					      
					      .add(Restrictions.in("narrartion",new String[] {"IMPS","NEFT"}))
					      .add(Restrictions.in("status",new String[]{"Deleted"}))
					      .list();
										
				if (delcheck.size() == 0) {
												
				transaction = session.beginTransaction();
				mudraBeneficiaryMastUpdate.setStatus("Deleted");
				session.save(mudraBeneficiaryMastUpdate);
				transaction.commit();
				//query4
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId())).add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				mudraSenderWallet.setBeneficiaryList(benfList);
				mudraSenderWallet.setStatusCode("1000");
				mudraSenderWallet.setStatusDesc("Beneficiary Deleted.");
			}
				else
				{
					mudraSenderWallet.setStatusCode("1001");
					mudraSenderWallet.setStatusDesc("Sorry !! This is a transacting beneficiary so cannot be deleted.");
				}
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "****RequestId******" + requestId
					+ "|deletedBeneficiary()|"
					+ "problem in deletedBeneficiary" + e.getMessage());
			/*logger.debug(serverName + "****RequestId******" + requestId
					+ "******problem in deletedBeneficiary=========================" + e.getMessage(), e);*/
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderWallet;
	}
	public List<MerchantDmtTransBean> gettransDtl(String merchantId, String mobileNo, String serverName) {
		  
		  
		      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "|gettransDtl()|"+ "Start excution  gettransDtl method**:" + mobileNo);
		/*logger.info(
				serverName + "Start excution ************************************* gettransDtl method**:" + mobileNo);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<MerchantDmtTransBean> transList = new ArrayList<MerchantDmtTransBean>();
		try {
			// SQLQuery query = session.createSQLQuery("SELECT
			// merchantid,senderid,sendermobileno,beneficiaryname,bankname,accountno,transfertype,amount,transid,DATE_FORMAT(transdate,
			// '%d/%m/%Y') AS transdate,STATUS, merchanttransid FROM
			// merchant_dmt_trans WHERE merchantid=:merchantid AND
			// sendermobileno=:sendermobileno");
			SQLQuery query = session.createSQLQuery(
					"SELECT transid AS agentTransId,merchanttransid AS mrTransId,status ,amount,DATE_FORMAT(transdate, '%d/%m/%Y %T') AS transDateTime,accountno as benefAccNo,transfertype as remark,sendermobileno AS mobileNo FROM merchant_dmt_trans WHERE merchantid=:merchantid AND sendermobileno=:sendermobileno");
			// SELECT transid AS agentTransId,merchanttransid AS
			// mrTransId,status ,amount,transdate as transDateTime,accountno as
			// benefAccNo,transfertype as remark FROM merchant_dmt_trans WHERE
			// merchantid='AGGR001035' AND sendermobileno='9935041287'
			query.setString("merchantid", merchantId);
			query.setString("sendermobileno", mobileNo);
			query.setResultTransformer(Transformers.aliasToBean(MerchantDmtTransBean.class));
			query.addScalar("agentTransId").addScalar("mrTransId").addScalar("status").addScalar("amount")
					.addScalar("transDateTime").addScalar("benefAccNo").addScalar("remark").addScalar("mobileNo");
			transList = query.list();

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName + "|gettransDtl()|"+"problem in gettransDtl" + e.getMessage());
			//logger.debug(serverName + "problem in gettransDtl" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return transList;
	}

	public List<MerchantDmtTransBean> getTransStatus(String merchantId, String agentTransId, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName 
				+"|getTransStatus()|"
				+ agentTransId);
		/*logger.info(serverName + "Start excution ************************************* getagentTranssDtl method**:"
				+ agentTransId);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<MerchantDmtTransBean> transList = new ArrayList<MerchantDmtTransBean>();
		try {
			// SQLQuery query = session.createSQLQuery("SELECT
			// merchantid,senderid,sendermobileno,beneficiaryname,bankname,accountno,transfertype,amount,transid,DATE_FORMAT(transdate,
			// '%d/%m/%Y') AS transdate,STATUS, merchanttransid FROM
			// merchant_dmt_trans WHERE merchantid=:merchantid AND
			// sendermobileno=:sendermobileno");
			SQLQuery query = session.createSQLQuery(
					"SELECT transid AS agentTransId,merchanttransid AS mrTransId,status ,amount,DATE_FORMAT(transdate, '%d/%m/%Y %T') AS transDateTime,accountno as benefAccNo,transfertype as remark,sendermobileno AS mobileNo FROM merchant_dmt_trans WHERE merchantid=:merchantid AND merchanttransid=:merchanttransid");
			// SELECT transid AS agentTransId,merchanttransid AS
			// mrTransId,status ,amount,transdate as transDateTime,accountno as
			// benefAccNo,transfertype as remark FROM merchant_dmt_trans WHERE
			// merchantid='AGGR001035' AND sendermobileno='9935041287'
			query.setString("merchantid", merchantId);
			query.setString("merchanttransid", agentTransId);
			query.setResultTransformer(Transformers.aliasToBean(MerchantDmtTransBean.class));
			query.addScalar("agentTransId");
			query.addScalar("agentTransId").addScalar("mrTransId").addScalar("status").addScalar("amount")
					.addScalar("transDateTime").addScalar("benefAccNo").addScalar("remark").addScalar("mobileNo");
			transList = query.list();

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|getTransStatus()|"+ "problem in getagentTranssDtl" + e.getMessage());
			/*logger.debug(serverName + "problem in getagentTranssDtl=========================" + e.getMessage(), e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return transList;
	}

	public double getUserBalance(String merchantId, String senderId, String serverName) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "", serverName + "|getUserBalance()|"
				+ senderId);
		/*logger.info(serverName + "Start excution ************************************* getUserBalance method**:"
				+ senderId);*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Double userBalance = 0.00;
		try {
			// added on 2nd Aug
			Object balance = session
					.createSQLQuery("SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  id=:senderId")
					.setString("senderId", senderId).uniqueResult();
			if (balance == null) {
				userBalance = 0.00;
			} else {
				userBalance = ((BigDecimal) balance).doubleValue();
				if (userBalance == null) {
					userBalance = 0.00;
				}
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|getUserBalance()|"+ "problem in getUserBalance" + e.getMessage());
			/*logger.debug(serverName + "problem in getUserBalance=========================" + e.getMessage(), e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return userBalance;
	}

	/*
	 * public double getUserBalance(String merchantId,String senderId, String
	 * serverName) { logger.info(serverName+
	 * "Start excution ************************************* getUserBalance method**:"
	 * + senderId); factory = DBUtil.getSessionFactory(); session =
	 * factory.openSession(); double userBalance=0.00; try{ // userBalance =
	 * (Double)session.createSQLQuery(
	 * "SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  aggreatorid=:merchantId AND id=:senderId"
	 * ).setString("merchantId", merchantId).setString("senderId",
	 * senderId).uniqueResult(); userBalance =
	 * ((BigDecimal)session.createSQLQuery(
	 * "SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  aggreatorid=:merchantId AND id=:senderId"
	 * ).setString("merchantId", merchantId).setString("senderId",
	 * senderId).uniqueResult()).doubleValue(); } catch (Exception e) {
	 * logger.debug(serverName+
	 * "problem in getagentTranssDtl=========================" + e.getMessage(),
	 * e); e.printStackTrace(); transaction.rollback(); } finally {
	 * session.close(); } return userBalance; }
	 */

	/*
	 * public Double getmaxAmtPerDay(String merchantId,String senderId, String
	 * serverName){
	 * 
	 * Double perDayAmount=0.0; factory = DBUtil.getSessionFactory(); session =
	 * factory.openSession(); double userBalance=0.00; try{
	 * perDayAmount=(Double)session.createQuery(
	 * "SELECT COALESCE(SUM(E.trxCredit),0) FROM TransactionBean E WHERE  WALLET_ID=:walletId AND TRX_RESULT='Success'  AND DATE(WALLET_TXN_DATE)=DATE(NOW())"
	 * ).setString("walletId",walletId).uniqueResult(); logger.info(
	 * " pperDayAmount===================================================================================== trxId"
	 * +perDayAmount); }catch(Exception e){ e.printStackTrace(); logger.debug(
	 * "problem in getmaxAmtPerDay========================================" +
	 * e.getMessage(), e); } finally { session.close(); }
	 * 
	 * return perDayAmount; }
	 */

	// SELECT walletbalance FROM mudrasendermast WHERE aggreatorid='AGGR001035'
	// AND id='MSEN001011'

	public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId****" + requestId
				+"|getTransacationLedgerDtl()|"
				+ dMTReportInOut.getAgentId()+ "| getTransacationLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ "| getTransacationLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "", serverName + "****RequestId****" + requestId
				+ " getTransacationLedgerDtl stDAte**:"
				+ dMTReportInOut.getStDate());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId****" + requestId
				+ " getTransacationLedgerDtl edDate**:"
				+ dMTReportInOut.getEdDate());*/
		
		/*logger.info(serverName + "****RequestId****" + requestId
				+ "**********Start excution ************************************* getTransacationLedgerDtl method**:"
				+ dMTReportInOut.getAgentId());
		logger.info(serverName + "****RequestId****" + requestId
				+ "**********Start excution ************************************* getTransacationLedgerDtl stDAte**:"
				+ dMTReportInOut.getStDate());
		logger.info(serverName + "****RequestId****" + requestId
				+ "**********Start excution ************************************* getTransacationLedgerDtl edDate**:"
				+ dMTReportInOut.getEdDate());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status FROM wallettransactionledger WHERE agentid=:agentid and isbank=:isbank ");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			serchQuery.append(" ORDER BY ptytransdt");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|Agentid:"+ dMTReportInOut.getAgentId()+ "****RequestId****" + requestId
					+ "Query:" + serchQuery.toString());
			/*logger.info(serverName + "****RequestId****" + requestId
					+ "**********Query*****************************************:" + serchQuery.toString());*/
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			query.setInteger("isbank",0);
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|Agentid"+ dMTReportInOut.getAgentId()+ "****RequestId****" + requestId
					+ "totla Count:" + transList.size());
			/*logger.info(serverName + "****RequestId****" + requestId
					+ "**********************totla Count*****************************:" + transList.size());*/
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName +"|Agentid"+ dMTReportInOut.getAgentId()+ "****RequestId****" + requestId
					+ "problem in getTransacationLedgerDtl" + e.getMessage());
			/*logger.debug(
					serverName + "****RequestId****" + requestId
							+ "**********problem in getTransacationLedgerDtl=========================" + e.getMessage(),
					e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}

	public DMTReportInOut getAgentLedgerDtl(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId****" + requestId
				+"|getAgentLedgerDtl()|"
				+ " getAgentLedgerDtl :"
				+ dMTReportInOut.getAgentId()+ " |getAgentLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ "| getAgentLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId****" + requestId
				+ " getAgentLedgerDtl stDAte**:"
				+ dMTReportInOut.getStDate());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId****" + requestId
				+ " getAgentLedgerDtl edDate**:"
				+ dMTReportInOut.getEdDate());*/
		
		/*logger.info(serverName + "****RequestId****" + requestId
				+ "******Start excution ************************************* getAgentLedgerDtl method**:"
				+ dMTReportInOut.getAgentId());
		logger.info(serverName + "****RequestId****" + requestId
				+ "******Start excution ************************************* getAgentLedgerDtl stDAte**:"
				+ dMTReportInOut.getStDate());
		logger.info(serverName + "****RequestId****" + requestId
				+ "******Start excution ************************************* getAgentLedgerDtl edDate**:"
				+ dMTReportInOut.getEdDate());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentLedgerBean> agentLedgerList = new ArrayList<AgentLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(txnDate, '%d/%m/%Y %T') AS txnDate,userid,txntype,resptxnid,mobileno,accountno,NAME,ifsccode,dramount,txncredit,closingbal FROM agentledger WHERE userid=:agentid ");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(txnDate) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			serchQuery.append(" ORDER BY txnDate");
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|Agentid:"+dMTReportInOut.getAgentId()+ "****RequestId****" + requestId
					+ "******Query*****************************************:" + serchQuery.toString());
			/*logger.info(serverName + "****RequestId****" + requestId
					+ "******Query*****************************************:" + serchQuery.toString());*/
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			query.setResultTransformer(Transformers.aliasToBean(AgentLedgerBean.class));
			agentLedgerList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +"|Agentid:"+dMTReportInOut.getAgentId()+ "****RequestId****" + requestId
					+ "totla Count:" + agentLedgerList.size());
			/*logger.info(serverName + "****RequestId****" + requestId
					+ "******************totla Count*****************************:" + agentLedgerList.size());*/
			dMTReportInOut.setAgentLedger(agentLedgerList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName  +"|Agentid:"+dMTReportInOut.getAgentId()+"****RequestId****" + requestId
					+"|getAgentLedgerDtl()|"+ "problem in getAgentLedgerDtl" + e.getMessage());
			/*logger.debug(serverName + "****RequestId****" + requestId
					+ "******problem in getAgentLedgerDtl=========================" + e.getMessage(), e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}

	 public DMTReportInOut getSenderLedgerDtl(DMTReportInOut dMTReportInOut,String serverName,String requestId) { 
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName+"***RequestId***"+requestId+ "|getSenderLedgerDtl()|" + dMTReportInOut.getAgentId()+ "|MobileNo " + dMTReportInOut.getMobileNo());
		 //Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName+"***RequestId***"+requestId+ "|getSenderLedgerDtl()|" + dMTReportInOut.getMobileNo());
		 
		 /*logger.info(serverName+"***RequestId***"+requestId+"****Start excution ************************************* getsenderLedgerDtl method**:" + dMTReportInOut.getAgentId());
		  logger.info(serverName+"***RequestId***"+requestId+"****Start excution ************************************* getsenderLedgerDtl mobileNo**:" + dMTReportInOut.getMobileNo());*/
		  
		  factory = DBUtil.getSessionFactory();
		  session = factory.openSession();
		  List <SenderLedgerBean> senderLedgerList=new ArrayList<SenderLedgerBean>();
		  try{
		  
		  
		   if(dMTReportInOut.getMobileNo()!=null&&!dMTReportInOut.getMobileNo().isEmpty()) {
			   StringBuilder serchQuery=new StringBuilder();
		   serchQuery.append("SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt,a.senderid AS senderid,a.id AS id,narrartion,c.mobileno AS mobileno,b.accountno AS accountno,b.ifsccode AS ifsccode,cramount,dramount,IFNULL(bankrrn,'') AS bankrrn,remark,a.isbank as isbank FROM remittanceledger a,mudrabeneficiarymast b,mudrasendermast c  WHERE a.senderid =(SELECT id FROM mudrasendermast WHERE mobileno=:mobileno and type='Wallet')  AND a.senderid=c.id AND a.beneficiaryid=b.id ");
		      serchQuery.append(" ORDER BY ptytransdt");
		      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName+"***RequestId***"+requestId+"Query:" + serchQuery.toString());
		     /* logger.info(serverName+"***RequestId***"+requestId+"****Query*****************************************:" + serchQuery.toString());*/
		   SQLQuery  query=session.createSQLQuery(serchQuery.toString());
		   query.setString("mobileno", dMTReportInOut.getMobileNo());
		   query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
		   senderLedgerList=query.list();
		   }
		   
		   else if(dMTReportInOut.getAccountno()!=null) {
			   StringBuilder serchQuery=new StringBuilder();
			    serchQuery.append(
			      "SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt, a.senderid AS senderid,a.id AS id,a.narrartion AS narrartion,c.mobileno AS mobileno, b.accountno AS accountno,b.ifsccode AS ifsccode,a.cramount AS cramount,a.dramount AS dramount,IFNULL(bankrrn,'') AS bankrrn,a.bankrrn AS bankrrn,a.remark AS remark,a.isbank as isbank FROM remittanceledger a, mudrabeneficiarymast b,mudrasendermast c where b.accountno=:accountno and a.agentid=:agentid and b.type='Wallet' AND a.senderid=c.id AND a.beneficiaryid=b.id");
			    serchQuery.append(" ORDER BY ptytransdt");
			    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "***RequestId***" + requestId
					      + "Query:" + serchQuery.toString());
			    /*logger.info(serverName + "***RequestId***" + requestId
			      + "****Query*****************************************:" + serchQuery.toString());*/
			    SQLQuery query = session.createSQLQuery(serchQuery.toString());
			    query.setString("agentid", dMTReportInOut.getAgentId());
			    query.setString("accountno", dMTReportInOut.getAccountno());
			    query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
			    senderLedgerList = query.list();
			    //System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ "+dMTReportInOut.getAccountno());
			}
		   
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName+"***RequestId***"+requestId+"totla Count :" + senderLedgerList.size());
		   /*logger.info(serverName+"***RequestId***"+requestId+"****************totla Count*****************************:" + senderLedgerList.size());*/
		   dMTReportInOut.setSenderLedgerList(senderLedgerList);
		   
		  } catch (Exception e) {
			  Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName+"|Agent:"+dMTReportInOut.getAgentId() +"***RequestId***"+requestId+"|getsenderLedgerDtl()|"+"problem in getsenderLedgerDtl" + e.getMessage());
		 /* logger.debug(serverName+"***RequestId***"+requestId+"****problem in getsenderLedgerDtl=========================" + e.getMessage(), e);*/
		  e.printStackTrace();
		  transaction.rollback();
		  } finally {
		  session.close();
		  }
		  return dMTReportInOut;
		 }
		

	public List<BankMastBean> getBankDtls(String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "", serverName +""+"|getBankDtls()|");
		/*logger.info(serverName + "Start excution ************************************* getBankDtl method**:");*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<BankMastBean> bankDtl = session.createCriteria(BankMastBean.class).list();
		session.close();
		return bankDtl;
	}

	public List<BankNameListBean> getBankDtl(String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId**" + requestId
				+ "|getBankDtl()|");
		/*logger.info(serverName + "****RequestId**" + requestId
				+ "********Start excution ************************************* getBankDtl method**:");*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<BankNameListBean> bankDtl = new ArrayList<BankNameListBean>();
		// bankDtl.add(0, new BankNameListBean("-1","Select Bank"));
		SQLQuery query = session
				.createSQLQuery("SELECT a.bankname AS id, a.bankname AS bankName FROM ifscmaster a GROUP BY bankname");
		query.setResultTransformer(Transformers.aliasToBean(BankNameListBean.class));
		bankDtl = query.addScalar("id").addScalar("bankName").list();
		session.close();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "****RequestId**" + requestId
				+ "totla Count:" + bankDtl.size());
		/*logger.info(serverName + "****RequestId**" + requestId
				+ "*************totla Count*****************************:" + bankDtl.size());*/
		return bankDtl;
	}

	public String saveDMTApiRequest(String agentId, String reqVal, String serverName, String apiName) {
		//navin
		MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
		MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "|saveDMTApiRequest|"+  "reqVal :"
				+ reqVal
				+ agentId);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "|"+ "reqVal :"
				+ reqVal);*/
		/*logger.info(serverName + "Start excution ************************************* saveDMTApiRequest agentId**:"
				+ agentId);
		logger.info(serverName + "Start excution ************************************* saveDMTApiRequest reqVal**:"
				+ reqVal);
*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String requestId = "";
		try {
			transaction = session.beginTransaction();
			DMTApiRequest dMTApiRequest = new DMTApiRequest();

			int leftLimit = 65; // letter 'a'
		    int rightLimit = 90; // letter 'z'
		    int targetStringLength = 10;
		    Random random = new Random();
		    StringBuilder buffer = new StringBuilder(targetStringLength);
		    for (int i = 0; i < targetStringLength; i++) {
		        int randomLimitedInt = leftLimit + (int) 
		          (random.nextFloat() * (rightLimit - leftLimit + 1));
		        buffer.append((char) randomLimitedInt);
		    }
		    String generatedString = buffer.toString();
			dMTApiRequest.setRequestId(Long.toString(generateLongId()) + generatedString);
			dMTApiRequest.setAgentId(agentId);
			dMTApiRequest.setRequestValue(reqVal);
			dMTApiRequest.setApiName(apiName);
			session.save(dMTApiRequest);
			transaction.commit();
			requestId = dMTApiRequest.getRequestId();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName +"|saveDMTApiRequest()|"
					+ requestId);
			/*logger.info(
					serverName + "Start excution ************************************* saveDMTApiRequest requestId**:"
							+ requestId);*/
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", serverName + "problem in getsenderLedgerDtl" + e.getMessage()+" "+e);
			logger.debug(serverName + "problem in getsenderLedgerDtl" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return requestId;
	}

	public String bankApi() {
		Random r = new Random();
		int i = r.nextInt((10 - 1) + 1) + 1;

		if (i / 2 > 0) {
			return Integer.toString(i);
		} else {
			return "Fail";
		}
	}

	private static final long twepoch = 1288834974657L;
	private static final long sequenceBits = 17;
	private static final long sequenceMax = 65536;
	private static volatile long lastTimestamp = -1L;
	private static volatile long sequence = 0L;

	private static synchronized Long generateLongId() {
		long timestamp = System.currentTimeMillis();
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) % sequenceMax;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimestamp);
			}
		} else {
			sequence = 0;
		}
		lastTimestamp = timestamp;
		Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
		return id;
	}

	private static long tilNextMillis(long lastTimestamp) {
		long timestamp = System.currentTimeMillis();
		while (timestamp <= lastTimestamp) {
			timestamp = System.currentTimeMillis();
		}
		return timestamp;
	}

	// added for defect id 6.1
	public String getSenderMobileNo(String senderId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderId, "", ""+"|getSenderMobileNo()" );
		/*logger.info("Start excution ************************************* getSenderMobileNo method**:" + senderId);*/

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String mobileNo = new String();
		try {
			mobileNo = (String) session.createSQLQuery("SELECT mobileno from mudrasendermast  where id = :senderId")
					.setString("senderId", senderId).uniqueResult();

			return mobileNo;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", senderId, "",  "|getSenderMobileNo()|"+"problem in getsenderLedgerDtl" + e.getMessage());
			/*logger.debug("problem in getsenderLedgerDtl=========================" + e.getMessage(), e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mobileNo;
	}

	// changed on 27th july
	/**
	 * 
	 * @param bankName
	 * @param ifsc
	 * @param branchName
	 * @param serverName
	 * @return
	 */
	public List<BankDetailsBean> getBankDetails(String bankName, String ifsc, String branchName, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName
				+ "|getBankDetails()|"+ "param--- bankName :"
				+ bankName + "--IFSC--" + ifsc + "---branchName---" + branchName);
		/*logger.info(serverName
				+ "Start excution ************************************* getBankDetails method**: param--- bankName :"
				+ bankName + "--IFSC--" + ifsc + "---branchName---" + branchName);
*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<BankDetailsBean> bankDtl = new ArrayList<BankDetailsBean>();
		try {
			// bankDtl.add(0, new BankNameListBean("-1","Select Bank"));
			String querystmt = "SELECT a.bankname AS bankName, a.ifsccode AS ifscCode , a.branchname as branchName ,a.address as address , a.city as city , a.state as state FROM ifscmaster a where ";

			if (!(ifsc == null || ifsc.trim().equals(""))) {
				querystmt = querystmt + " a.ifsccode = '" + ifsc + "'";
			} else if (!(bankName == null || bankName.trim().equals(""))
					&& (branchName == null || branchName.trim().equals(""))) {
				querystmt = querystmt + " a.bankname = '" + bankName + "'";
			} else if ((bankName == null || bankName.trim().equals(""))
					&& !(branchName == null || branchName.trim().equals(""))) {
				querystmt = querystmt + " a.branchname = '" + branchName + "'";
			} else {
				querystmt = querystmt + " a.bankname = '" + bankName + "'";
				querystmt = querystmt + " and a.branchname = '" + branchName + "'";
			}

			querystmt = querystmt + "  order by bankname ";
			SQLQuery query = session.createSQLQuery(querystmt);
			query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
			bankDtl = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName").addScalar("address")
					.addScalar("city").addScalar("state").list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "totla Count:" + bankDtl.size());
			/*logger.info(serverName + "************totla Count*****************************:" + bankDtl.size());*/
			return bankDtl;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "|getBankDetails()|"+ "exception in getBankDetails" + e.getMessage());
			/*logger.info(serverName + "-------------exception in getBankDetails------" + e.getMessage());*/
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bankDtl;
	}

	public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "|uploadSenderPanF60()|");
		/*logger.info(serverName + "************start excution*********uploadSenderPanF60***********senderid*********"
				+ mudraSenderPanDetails.getSenderId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			transaction = session.beginTransaction();
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					mudraSenderPanDetails.getSenderId());

			mudraSenderPanDetails.setSenderName(mudraSenderWallet.getFirstName());
			mudraSenderPanDetails.setStatus("Pending");
			session.saveOrUpdate(mudraSenderPanDetails);

			transaction.commit();
			mudraSenderPanDetails.setStatusCode("1000");
			mudraSenderPanDetails.setStatusMessage("Pan /Form 60 uploaded successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "|uploadSenderPanF60()|"+ "exception in uploadSenderPanF60" + e.getMessage());
			/*logger.info(serverName + "-------------exception in uploadSenderPanF60------" + e.getMessage());*/
			e.printStackTrace();
		} finally {
			session.close();
		}

		return mudraSenderPanDetails;
	}

	public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails,
			String serverName) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "|getPendingPanReq|"
				);
		/*logger.info(
				serverName + "***start excution ===================== method getPendingPanReq(mudraSenderPanDetails)"
						+ mudraSenderPanDetails.getAggreatorId());*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<MudraSenderPanDetails> results = new ArrayList<MudraSenderPanDetails>();
		try {
			if (!(mudraSenderPanDetails.getAggreatorId() == null || mudraSenderPanDetails.getAggreatorId().isEmpty()
					|| mudraSenderPanDetails.getAggreatorId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				StringBuilder serchQuery = new StringBuilder();

//				serchQuery.append(" from MudraSenderPanDetails where AggreatorId=:AggreatorId and status='Pending'");
				serchQuery.append(" select mpan.id as id,mpan.aggreatorId as aggreatorId,mpan.senderId as senderId,mpan.senderName as senderName,mpan.proofType as proofType,mpan.panNumber as panNumber,mpan.proofTypePic as proofTypePic,mpan.status as status,mpan.loaddate as loaddate,mpan.comment as comment,mpan.approveDate as approveDate,msen.mobileNo as mobileNo from MudraSenderPanDetails mpan,MudraSenderWallet msen where mpan.senderId=msen.id and mpan.aggreatorId=:AggreatorId ");

				if (mudraSenderPanDetails.getStDate() != null && !mudraSenderPanDetails.getStDate().isEmpty()
						&& mudraSenderPanDetails.getEndDate() != null
						&& !mudraSenderPanDetails.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(loaddate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(mudraSenderPanDetails.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(mudraSenderPanDetails.getEndDate()))
							+ "', '/', '-'),'%d-%m-%Y')");
				} else {
					serchQuery.append(" and DATE(loaddate)=DATE(NOW())");
				}
				serchQuery.append(" order by loaddate DESC");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "getPendingPanReq query" + serchQuery);
				/*logger.info(
						serverName + "****getPendingPanReq*************************query*************" + serchQuery);*/

				Query query = session.createQuery(serchQuery.toString());
				query.setString("AggreatorId", mudraSenderPanDetails.getAggreatorId());
				query.setResultTransformer(Transformers.aliasToBean(MudraSenderPanDetails.class));
				results = query.list();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "|"+ "problem in getPendingPanReq" + e.getMessage());
			/*logger.debug(serverName + "*****problem in getPendingPanReq==================" + e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + " method getPendingPanReq Result "
				+ results.size());
		/*logger.info(serverName + "****start excution ===================== method getPendingPanReq Result "
				+ results.size());*/
		return results;

	}

	public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails,
			String serverName) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag = false;
		
		 
	      
		try {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName +"|rejectAcceptedSenderPan()|" );
			
			Transaction transaction = session.beginTransaction();
			MudraSenderPanDetails mudraSenderPanDetailsD = (MudraSenderPanDetails) session
					.get(MudraSenderPanDetails.class, mudraSenderPanDetails.getId());

			mudraSenderPanDetailsD.setStatus(mudraSenderPanDetails.getStatus());
			mudraSenderPanDetailsD.setComment(mudraSenderPanDetails.getComment());
			session.saveOrUpdate(mudraSenderPanDetailsD);
			if (mudraSenderPanDetails.getStatus().equalsIgnoreCase("ACCEPT")) {
				MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
						mudraSenderPanDetailsD.getSenderId());
				mudraSenderWallet.setFirstName(mudraSenderPanDetails.getSenderName());
				mudraSenderWallet.setIsPanValidate(1);
				session.saveOrUpdate(mudraSenderWallet);
			}
			transaction.commit();
			mudraSenderPanDetails.setStatusCode("1000");
			mudraSenderPanDetails.setStatusMessage("Sender Pan/Form 60 " + mudraSenderPanDetails.getStatus() + ".");

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			mudraSenderPanDetails.setStatusCode("7000");
			mudraSenderPanDetails.setStatusMessage(e.getMessage());
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName + "|rejectAcceptedSenderPan|"+ "problem in rejecttSmartCard" + e.getMessage());
			/*logger.debug(
					serverName + "**********************problem in rejecttSmartCard==================" + e.getMessage(),
					e);*/
		} finally {
			session.close();
		}
		return mudraSenderPanDetails;
	}

	
	 
public List<UploadSenderKyc> getPendingKycList(UploadSenderKyc senderKyc,
		String serverName) {
	  

	factory = DBUtil.getSessionFactory();
	Session session = factory.openSession();
	List<UploadSenderKyc> results = new ArrayList<UploadSenderKyc>();
	try {
		if (!(senderKyc.getAggreagatorId() == null || senderKyc.getAggreagatorId().isEmpty()|| senderKyc.getAggreagatorId() == "") || 
				!(senderKyc.getAgentId() == null || senderKyc.getAgentId().isEmpty()|| senderKyc.getAgentId() == "")) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			Criteria criteria=session.createCriteria(UploadSenderKyc.class);
			Criteria criteria1=session.createCriteria(UploadSenderKyc.class);
			
			if (senderKyc.getStDate() != null && !senderKyc.getStDate().isEmpty()
					&& senderKyc.getEndDate() != null
					&& !senderKyc.getEndDate().isEmpty()) {
				/*serchQuery.append(" and DATE(loaddate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(mudraSenderPanDetails.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(mudraSenderPanDetails.getEndDate()))
						+ "', '/', '-'),'%d-%m-%Y')");*/
				Date endDate = new Date(formatter.parse(senderKyc.getEndDate()).getTime() + 86400000);
				/*Date endDate =DateUtil.parseDate(formate.format(formatter.parse(senderKyc.getEndDate())));*/
				criteria.add(Restrictions.between("requestDate",formatter.parse(senderKyc.getStDate()),endDate));
				criteria1.add(Restrictions.between("requestDate",formatter.parse(senderKyc.getStDate()),endDate));
				
			}
			else
			{
				criteria.add(Restrictions.ge("requestDate",formate.parse(formate.format(new Date()))));
				criteria1.add(Restrictions.ge("requestDate",formate.parse(formate.format(new Date()))));
			}
			if(senderKyc.getAggreagatorId() != null && !senderKyc.getAggreagatorId().isEmpty())
			{
				criteria.add( Restrictions.eq("aggreagatorId",senderKyc.getAggreagatorId()));
				criteria1.add( Restrictions.eq("aggreagatorId",senderKyc.getAggreagatorId()));
				criteria.add( Restrictions.eq("finalStatus","pending"));
				criteria1.add( Restrictions.ne("finalStatus","pending"));
				criteria.addOrder(Order.desc("requestDate"));
				criteria1.addOrder(Order.desc("requestDate"));
				
				List<UploadSenderKyc> crit = criteria.list();
				List<UploadSenderKyc> crit1 = criteria1.list();
				if(crit != null && crit.size() > 0)
					results.addAll(crit);
				if(crit1 != null && crit1.size() > 0)
					results.addAll(crit1);
			}
			else if(senderKyc.getAgentId() != null && !senderKyc.getAgentId().isEmpty())
			{
				criteria.add( Restrictions.eq("agentId",senderKyc.getAgentId()));
				criteria.addOrder(Order.desc("requestDate"));
				results = criteria.list();
			}
			
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		session.close();
	}
	return results;

}

public UploadSenderKyc rejectAcceptedSenderKyc(UploadSenderKyc senderKyc,String serverName) {
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	boolean flag = false;
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "1. start rejectAcceptedSenderKyc" + senderKyc.getKycId());
 
      
	try {
		Transaction transaction = session.beginTransaction();
		UploadSenderKyc uploadSenderKyc = (UploadSenderKyc) session.get(UploadSenderKyc.class, senderKyc.getKycId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "2. getting kycdetails from DB " + uploadSenderKyc);
		uploadSenderKyc.setFinalStatus(senderKyc.getFinalStatus());
		uploadSenderKyc.setCheckarStatus(senderKyc.getFinalStatus());
		uploadSenderKyc.setRemark(senderKyc.getRemark());;
		
		uploadSenderKyc.setApproveDate(new Date());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "3. final status " + senderKyc.getFinalStatus());

		if (senderKyc.getFinalStatus().equalsIgnoreCase("ACCEPT")) {
			
			uploadSenderKyc.setSenderAddress(senderKyc.getSenderAddress());
			uploadSenderKyc.setSenderName(senderKyc.getSenderName());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "4. final status ACCEPT with name and address" + senderKyc.getSenderName()+"---"+senderKyc.getSenderAddress());

			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					senderKyc.getSenderId());
			if(senderKyc.getSenderName()!=null&&!senderKyc.getSenderName().isEmpty()&&senderKyc.getSenderName().length()>0)
			mudraSenderWallet.setFirstName(senderKyc.getSenderName());
			mudraSenderWallet.setKycStatus("FULL KYC");
			session.saveOrUpdate(mudraSenderWallet);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "5. sender kyc is updated to FULL KYC with sender name -" + senderKyc.getSenderName());

		}
		session.saveOrUpdate(uploadSenderKyc);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "6. final status ACCEPT with name and address" + senderKyc.getSenderName()+"---"+senderKyc.getSenderAddress());

		transaction.commit();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "7. changes commit with name and address -" + senderKyc.getSenderName()+"---"+senderKyc.getSenderAddress());

		senderKyc.setStatusCode("1000");
		senderKyc.setStatusMessage("Sender KYC status  " + senderKyc.getFinalStatus() + "ED.");

		flag = true;
	} catch (Exception e) {
		e.printStackTrace();
		senderKyc.setStatusCode("7000");
		senderKyc.setStatusMessage(e.getMessage());
		Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "problem in rejectAcceptedSenderKyc" + e.getMessage());
		/*logger.debug(
				serverName + "**********************problem in rejecttSmartCard==================" + e.getMessage(),
				e);*/
	} finally {
		session.close();
	}
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKyc.getAggreagatorId(),"", "", senderKyc.getSenderId(), "", serverName + "|rejectAcceptedSenderKyc|"+ "8. final return with status code and status message-" + senderKyc.getStatusCode()+"---"+senderKyc.getStatusMessage());

	return senderKyc;
}
	 
	 
	 
	 
	 
	 
	 
	public void saveSenderKycResponse(EkycResponseBean ekycResponseBean) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|saveSenderKycResponse()|"
				+ ekycResponseBean.getKyctxnid());
		/*logger.info("******************Start excution ********************************* saveSenderKycResponse method**:"
				+ ekycResponseBean.getKyctxnid());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		try {

			session.saveOrUpdate(ekycResponseBean);
			transaction.commit();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"problem in saveSenderKycResponse" + e.getMessage()+" "+e);				
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public EkycResponseBean getEKycSenderData(EkycResponseBean ekycResponseBean) {
		EkycResponseBean bean = null;
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|getEKycSenderData()|"
				+ ekycResponseBean.getKyctxnid());
		/*logger.info("******************Start excution ********************************* getEKycSenderData method**:"
				+ ekycResponseBean.getKyctxnid());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {

			bean = (EkycResponseBean) session.get(EkycResponseBean.class, ekycResponseBean.getKyctxnid());

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "problem in rejecttSmartCard" + e.getMessage());
			// transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bean;
	}

	
	
	
	public boolean setSenderKycMast(KycConfigBean kycconfigbean, SenderKYCBean senderKYCBean, String servicetype,
			String timestamp, String kyctxnid) {
		EkycResponseBean senderkycmast = new EkycResponseBean();
		Transaction transaction = null;
		
	      
	      Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "","", "", servicetype + "*******RequestId******" + senderKYCBean.getAgent()
			+  "|setSenderKycMast()|"
			+ senderKYCBean.getMobileno());
	      session=factory.openSession();
		try {
			senderkycmast.setAgentid(senderKYCBean.getAgentid());

			senderkycmast.setAggreator(senderKYCBean.getAggreatorid());
			senderkycmast.setDeviceid(kycconfigbean.getDeviceid());
			senderkycmast.setSenderid(senderKYCBean.getMobileno());

			senderkycmast.setServicetype(servicetype);
			senderkycmast.setStatus("Pending");
			senderkycmast.setTime(timestamp);
			senderkycmast.setOthers("");
			senderkycmast.setKyctxnid(kyctxnid);

			transaction = session.beginTransaction();
			session.save(senderkycmast);
			transaction.commit();
			return true;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "","", "",  "|setSenderKycMast()|"+"problem in save KYC request" + e.getMessage());
			/*logger.debug("*******problem in save KYC request=========================" + e.getMessage(), e);*/
			senderKYCBean.setStatusCode("7000");
			senderKYCBean.setStatusDesc("Error");

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return false;
	}

	public KycConfigBean getkycConfig(SenderKYCBean senderKYCBean, String servicetype) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", servicetype + "*******RequestId******" + senderKYCBean.getAgent()
		+  "|getkycConfig()|"
		+ senderKYCBean.getMobileno());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"|senderKYCBean.getMobileno"+senderKYCBean.getMobileno()+"|servicetype"+servicetype+"|senderKYCBean.getRequestFrom"+senderKYCBean.getRequestFrom());

	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"open session factory");
		/*logger.debug("mno-"+senderKYCBean.getMobileno()+"*******open session factory=========================");*/
		KycConfigBean kycconfigbean = new KycConfigBean();
		try{
		if (senderKYCBean.getMobileno() == null || senderKYCBean.getMobileno().isEmpty()) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"Sender id is missing");
			/*logger.debug("mno-"+senderKYCBean.getMobileno()+"*******Sender id is missing=========================");*/
			kycconfigbean.setStatusCode("1999");
			kycconfigbean.setStatusDesc("Sender id is missing");
			return kycconfigbean;
		}
		@SuppressWarnings("unchecked")
		/*List<KycConfigBean> kycconfigbeanlist = session.createCriteria(KycConfigBean.class)
				.add(Restrictions.eq("servicetype", servicetype))
				.add(Restrictions.eq("aggreator", senderKYCBean.getAggreatorid()))
				.add(Restrictions.eq("requestFrom",senderKYCBean.getRequestFrom())).list();*/
		
		Query query=session.createQuery("from KycConfigBean where servicetype=:st and aggreator=:ag and requestFrom=:rf");
		query.setString("st",servicetype);
		query.setString("ag",senderKYCBean.getAggreatorid());
		query.setString("rf",senderKYCBean.getRequestFrom());
		List<KycConfigBean> kycconfigbeanlist=query.list();

		if (kycconfigbeanlist.size() == 0) {
			kycconfigbean.setStatusCode("1999");
			kycconfigbean.setStatusDesc("Sender id is missing");
			return kycconfigbean;
		} else {
			kycconfigbean = kycconfigbeanlist.get(0);
			kycconfigbean.setStatusCode("1000");
			kycconfigbean.setStatusDesc("Success");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"returning kyc config data"+kycconfigbean);
			/*logger.debug("mno-"+senderKYCBean.getMobileno()+"*******returning kyc config data========================="+kycconfigbean!=null?kycconfigbean.getAggreator():kycconfigbean);*/
			return kycconfigbean;
		}
		}catch(Exception e){
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"exception occure"+e.getMessage());
			logger.debug("mno-"+senderKYCBean.getMobileno()+"exception occure"+e.getMessage());
		}
		finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+senderKYCBean.getMobileno()+"returning in last"+kycconfigbean);
		logger.debug("mno-"+senderKYCBean.getMobileno()+" returning in last"+kycconfigbean!=null?kycconfigbean.getAggreator():kycconfigbean);
		return kycconfigbean;
	}

	public IMPSIciciResponseBean fundTransferICICIBankIMPS(String beneAccNo,String beneIfsc,double amount,String refNo,String remName,String remMobileNo)
	{
		
	    Session session = null;
	    Transaction transaction = null;
		IMPSParameterBean l_impsParameter= new IMPSParameterBean();
		IMPSIciciResponseBean l_impsResponse = new IMPSIciciResponseBean();
		l_impsResponse.setActCode(300);
		l_impsResponse.setResponse("Error");
		l_impsResponse.setResponseString("Transaction Failed");
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Long impsId = null;
		try
		{
			l_impsParameter.setBeneAccNo(beneAccNo);
			l_impsParameter.setBeneIFSC(beneIfsc);
			l_impsParameter.setAmount(amount);
			l_impsParameter.setTranRefNo(refNo);
			l_impsParameter.setRemName(remName);
			l_impsParameter.setRemMobile(remMobileNo);
			l_impsParameter.setPaymentRef("FTTransferP2A");
			l_impsParameter.setRemMMID("8839123");
			l_impsParameter.setPassCode("ba861896473b4176a8d7acde392c293a");
			l_impsParameter.setDeliveryChannel("Internet");
			
			try
			{
				factory = DBUtil.getSessionFactory();
				session = factory.openSession();
				transaction = session.beginTransaction();
				impsId =(Long) session.save(l_impsParameter);
				transaction.commit();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				logger.info("Exception occured while saving ICICI Request in database "+e.getMessage());
			}
		
			HashMap<String,String> request= l_impsParameter.getRequestForFundTransfer(l_impsParameter);
			
			if(request.get("RespCode").trim().equals("103"))
			{
				l_impsResponse.setActCode(300);
				l_impsResponse.setResponse("Format Error");
				l_impsResponse.setResponseString("Insufficient input - "+request.get("RespString"));
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", "|fundTransferICICIBankIMPS()|"+ "bankRespXml for money transfer : IMPS ICICI **: "+"Insufficient input - "+request.get("RespString"));
				/*logger.info("********************bankRespXml**for money transfer : IMPS ICICI **: "+"Insufficient input - "+request.get("RespString"));*/
				return l_impsResponse;
			}
			else if(request.get("RespCode").trim().equals("100"))
			{
				Date tomcatDate = new Date();
				String RequestImps=request.get("RespString");
				String bankRespXml=new ICICIBankIMPS().callIMPSServicePOST(RequestImps);
				
				if(!bankRespXml.equalsIgnoreCase("Fail"))
				{
					l_impsResponse.setBankRespXml(bankRespXml);
					
					
					saveIciciData(bankRespXml, (impsId == null ? null:impsId));
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "|fundTransferICICIBankIMPS() |"+"bankRespXml**for money transfer : IMPS ICICI **:"+bankRespXml);
					logger.info("********************bankRespXml**for money transfer : IMPS ICICI **:"+bankRespXml);
					HashMap<String, String> bankResp=new XmlParser().xmlParser(bankRespXml);
					
					
					HashMap<String,String> errorResp = new HashMap<String,String>();
					
					if(bankResp.get("ActCode").trim().equals("0"))
					{
						errorResp=null;
						l_impsResponse.setActCode(0);
						l_impsResponse.setResponseCode(0);
						l_impsResponse.setResponse("Success");
						l_impsResponse.setResponseString("Transaction Successful");	
						l_impsResponse.setBeneName(bankResp.get("BeneName"));
						l_impsResponse.setTranRefNo(bankResp.get("TranRefNo"));
						l_impsResponse.setBankRRN(bankResp.get("BankRRN"));
					}
					else
					{
						errorResp=l_impsResponse.returnResposeToUser(bankResp);
						l_impsResponse.setResponseCode(Integer.parseInt(errorResp.get("ResponseCode").trim()));
						l_impsResponse.setActCode(Integer.parseInt(errorResp.get("ActCode").trim()));
						l_impsResponse.setResponse(errorResp.get("Response"));
						l_impsResponse.setResponseString(errorResp.get("ResponseString"));
					}
					HashMap<String,String> errorRespVM =null;
					int count=0;
					
					while(((errorResp != null && (errorResp.get("ActCode").trim().equals("30") || errorResp.get("ActCode").trim().equals("11"))) || (errorRespVM != null &&(errorRespVM.get("ActCode").trim().equals("30") || errorRespVM.get("ActCode").trim().equals("11")  || errorRespVM.get("ActCode").trim().equals("91")))) && count <3)
					{
						Date date = new Date();				
						String vmRequest= RequestImps+"&"+"Timestamp="+dateFormat.format(date);
						errorResp = null;
						errorRespVM=null;
						count ++;
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "|fundTransferICICIBankIMPS()|"+"fundTransferICICIBankIMPS Count of VM"+count);
						String bankRespVM = new ICICIBankIMPS().callIMPSVMServicePOST(vmRequest);
						
						HashMap<String, String> RespVM=new XmlParser().xmlParser(bankRespVM);
						
						if(RespVM.get("ActCode").trim().equals("0"))
						{
							l_impsResponse.setActCode(0);
							l_impsResponse.setResponseCode(0);
							l_impsResponse.setResponse("Success");
							l_impsResponse.setResponseString("Transaction Successful");	
							l_impsResponse.setBeneName(RespVM.get("BeneName"));
							l_impsResponse.setTranRefNo(RespVM.get("TranRefNo"));
							l_impsResponse.setBankRRN(RespVM.get("BankRRN"));
						}
						else
						{
							errorRespVM=l_impsResponse.returnResposeToUser(RespVM);
							l_impsResponse.setResponseCode(Integer.parseInt(errorRespVM.get("ResponseCode").trim()));
							l_impsResponse.setActCode(Integer.parseInt(errorRespVM.get("ActCode").trim()));
							l_impsResponse.setResponse(errorRespVM.get("Response"));
							l_impsResponse.setResponseString(errorRespVM.get("ResponseString"));
						}
					}
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",  "|"+"bankRespXml**for money transfer : IMPS ICICI :"+l_impsResponse.getActCode()+"\n"+l_impsResponse.getResponse()+"\n"+l_impsResponse.getResponseString());
					
					return l_impsResponse;					
				}
				else
				{
					l_impsResponse.setActCode(1);
					l_impsResponse.setResponse("Transaction is pending");
					l_impsResponse.setResponseCode(0);
					l_impsResponse.setResponseString("Transaction is pending");
					return l_impsResponse;
				}
				
			}
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "", "|"+ "Problem in fund transfer : IMPS ICICI **:"+e.getMessage());
			/*logger.info("**********************Problem in fund transfer : IMPS ICICI **:"+e.getMessage());*/
			l_impsResponse.setActCode(1);
			l_impsResponse.setResponse("Transaction is pending");
			l_impsResponse.setResponseCode(0);
			l_impsResponse.setResponseString("");
			e.printStackTrace();
		}
		finally {
			if(session!=null)
				session.close();
		}
		return l_impsResponse;
		
	}


	public MiniKycResponse updateMiniKyc(String userId,String documentType,String documentId)
	{
		
	      
		MiniKycResponse resp = new MiniKycResponse();
		WalletMastBean walletMast = new WalletMastBean();
		resp.setMiniKycStatus(0);
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction  = session.beginTransaction();
			walletMast = (WalletMastBean)session.get(WalletMastBean.class, userId);
			walletMast.setDocumentId(documentId);
			walletMast.setDocumentType(documentType);
			if (documentId == null || documentId.trim().isEmpty() || documentType == null || documentType.trim().isEmpty()) 
			{
				walletMast.setMiniKycStatus(0);
			}
			else
			{
				walletMast.setMiniKycStatus(1);
			}
			session.update(walletMast);
			transaction.commit();
			resp.setStatusCode("1000");
			resp.setMiniKycStatus(walletMast.getMiniKycStatus());
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","", "", "", "",  "|updateMiniKyc()|"+"problem in updateMiniKyc"+e.getMessage());
			/*logger.info("*********problem in updateMiniKyc***e.message***"+e.getMessage() );*/
			e.printStackTrace();
			transaction.rollback();
			resp.setStatusCode("7000");
		}
		finally
		{
			session.close();
		}
		return resp;
	}



	@Override
	public MudraBeneficiaryWallet registerBeneficiaryCust(MudraBeneficiaryWallet mudraBeneficiaryMastBean,String serverName, String requestId) {

		Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", "", "",  serverName + "****RequestId*******" + requestId+ " registerBeneficiaryCust()"+ mudraBeneficiaryMastBean.getAccountNo());
		
		//logger.info(serverName + "****RequestId*******" + requestId+ "****Start excution ********* registerBeneficiaryCust method**:"+ mudraBeneficiaryMastBean.getAccountNo());
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			
			if (mudraBeneficiaryMastBean.getSenderId() == null || mudraBeneficiaryMastBean.getSenderId().isEmpty() || !mobileValidation(mudraBeneficiaryMastBean.getSenderId())) {
				mudraBeneficiaryMastBean.setStatusCode("1110");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Mobile Number.");
				return mudraBeneficiaryMastBean;
			}
			
			if (mudraBeneficiaryMastBean.getMpin() == null || mudraBeneficiaryMastBean.getMpin().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1102");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid OTP.");
				return mudraBeneficiaryMastBean;
			}
			
			if (mudraBeneficiaryMastBean.getName() == null || mudraBeneficiaryMastBean.getName().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1101");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Name.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getBankName() == null || mudraBeneficiaryMastBean.getBankName().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1107");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Bank Name.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getAccountNo() == null || mudraBeneficiaryMastBean.getAccountNo().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1108");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Account Number.");
				return mudraBeneficiaryMastBean;
			}

			if (mudraBeneficiaryMastBean.getIfscCode() == null || mudraBeneficiaryMastBean.getIfscCode().isEmpty()) {
				mudraBeneficiaryMastBean.setStatusCode("1109");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid IFSC Code.");
				return mudraBeneficiaryMastBean;
			}

			

			WalletMastBean walletSender = new WalletUserDaoImpl().getUserByMobileNo(mudraBeneficiaryMastBean.getSenderId(), mudraBeneficiaryMastBean.getAggreatorId(), serverName);
			if (walletSender == null) {
				mudraBeneficiaryMastBean.setStatusCode("1102");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid Mobile Number.");
				return mudraBeneficiaryMastBean;
			}
			boolean flag = new WalletUserDaoImpl().validateOTP(mudraBeneficiaryMastBean.getSenderId(), mudraBeneficiaryMastBean.getAggreatorId(), mudraBeneficiaryMastBean.getMpin());
			if(flag){
			List<MudraBeneficiaryWallet> benList = session.createCriteria(MudraBeneficiaryWallet.class)
				     .add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
				     .add(Restrictions.eq("accountNo", mudraBeneficiaryMastBean.getAccountNo()))
				     .add(Restrictions.ne("status","Deleted")).list();
			
			
			if (benList.size() > 0) {
				mudraBeneficiaryMastBean.setStatusCode("1124");
				mudraBeneficiaryMastBean.setStatusDesc("Your provided account number is already added with given Mobile number.");
				return mudraBeneficiaryMastBean;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryWallet.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryMastBean.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			if (activeCount >= 15) {
				mudraBeneficiaryMastBean.setStatusCode("1123");
				mudraBeneficiaryMastBean.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
				return mudraBeneficiaryMastBean;
			}

			transaction = session.beginTransaction();
			mudraBeneficiaryMastBean.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryMastBean.getAggreatorId()));
			// mudraBeneficiaryMastBean.setWallet(1);
			mudraBeneficiaryMastBean.setStatus("Active");
			mudraBeneficiaryMastBean.setVerified("NV");
			session.save(mudraBeneficiaryMastBean);
			transaction.commit();
			mudraBeneficiaryMastBean.setStatusCode("1000");
			mudraBeneficiaryMastBean.setStatusDesc("Beneficiary has been registered successfully.");

			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,
					mudraBeneficiaryMastBean.getSenderId());

			String smstemplet = commanUtilDao.getsmsTemplet("addBene", mudraSenderWallet.getAggreatorId());
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", "", "",  serverName + "****RequestId*******" + requestId+"|+ smstemplet" + smstemplet);
			
			/*logger.info("*********RequestId*******" + requestId
					+ "****~~~~~~~~~~~~~~~~~~~~~mudraSenderWallet.getMobileNo()~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderWallet.getMobileNo(),
					smstemplet.replaceAll("<<BENE NAME>>", mudraBeneficiaryMastBean.getName())
							.replace("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
							.replace("<<IFSC>>", mudraBeneficiaryMastBean.getIfscCode()),
					"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "OTP");
//			Thread t = new Thread(smsAndMailUtility);
//			t.start();
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			}
			else{

				mudraBeneficiaryMastBean.setStatusCode("1102");
				mudraBeneficiaryMastBean.setStatusDesc("Invalid OTP.");
				return mudraBeneficiaryMastBean;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", "", "",  serverName + "****RequestId*******" + requestId+"|+ problem in registerBeneficiary " + e.getMessage()+" "+e);
			
			/*logger.debug(serverName + "*********RequestId*******" + requestId
					+ "****problem in registerBeneficiary=========================" + e.getMessage(), e);*/

			mudraBeneficiaryMastBean.setStatusCode("7000");
			mudraBeneficiaryMastBean.setStatusDesc("Error");

			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryMastBean;
	}
	
	public  boolean mobileValidation(String mobile) {
		//added on 2nd Aug
		    Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		    Matcher matcher = pattern.matcher(mobile);

		    if (matcher.matches()) {
		  	  return true;
		    }
		    else
		    {
		    	return false;
		    }
		}


	public MudraSenderWallet getBeneficiaryList(MudraSenderWallet mudraSenderMastBean,String serverName,String requestId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		logger.info(serverName+"*****RequestId***"+requestId+"*****Start excution ************************************* getBeneficiaryListForImport method**:");
		if (mudraSenderMastBean.getId() == null || mudraSenderMastBean.getId().isEmpty()) {
			mudraSenderMastBean.setStatusCode("1104");
			mudraSenderMastBean.setStatusDesc("Invalid User Id.");
			return mudraSenderMastBean;
		}
		try {
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class).add(Restrictions.eq("senderId",mudraSenderMastBean.getId())).list();
				if (benfList.size() > 0) {
					mudraSenderMastBean.setBeneficiaryList(benfList);
					mudraSenderMastBean.setStatusCode("1000");
					mudraSenderMastBean.setStatusDesc("Success");
				} else {
					mudraSenderMastBean.setStatusCode("1122");
					mudraSenderMastBean.setStatusDesc("Customer does not have any beneficiary.");
				}
		} catch (Exception e) {
			logger.debug(serverName+"*****RequestId***"+requestId+"*****problem in getBeneficiaryListForImport********************************" + e.getMessage(), e);
			mudraSenderMastBean.setStatusCode("7000");
			mudraSenderMastBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderMastBean;
	}
	

	
	
	public HashMap getAgentSenderBalance(MudraSenderWallet mudra)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudra.getAggreatorId(),mudra.getAgentId(),mudra.getMobileNo(), "","",  "|Method Name :getAgentSenderBalance");
		HashMap<String,String> resp = new HashMap<String,String>();
		factory = DBUtil.getSessionFactory();
		try
		{
			session =factory.openSession();
			//SQLQuery query = session.createSQLQuery("SELECT walletbalance ,transferlimit FROM mudrasendermast WHERE id =:id AND aggreatorid=:aggreatorid");
			Criteria cr = session.createCriteria(MudraSenderWallet.class)
				    .setProjection(Projections.projectionList()
				      .add(Projections.property("walletBalance"),  "walletBalance")
				      .add(Projections.property("transferLimit"),  "transferLimit"))
				    .add(Restrictions.eq("id", mudra.getId())).add(Restrictions.eq("aggreatorId", mudra.getAggreatorId()))
				    .setResultTransformer(Transformers.aliasToBean(MudraSenderWallet.class));

			/*	  List<User> list = cr.list();
			query.setString("id", mudra.getId());
			query.setString("aggreatorid", mudra.getAggreatorId());
			query.setResultTransformer(Transformers.aliasToBean(MudraSenderWallet.class));*/
			MudraSenderWallet senderMast = (MudraSenderWallet)cr.uniqueResult();
			resp.put("walletBalance",Double.toString(senderMast.getWalletBalance()));
			resp.put("SenderTransferLimit",Double.toString(senderMast.getTransferLimit()));
			resp.put("AgentBalance", Double.toString(new TransactionDaoImpl().getWalletBalancebyId(mudra.getAgentId())));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudra.getAggreatorId(),mudra.getAgentId(),mudra.getMobileNo(), "","",  "|Method Name :getAgentSenderBalance|sender:walletbal:translimit:agentbal["+mudra.getId()
			+":"+senderMast.getWalletBalance()+":"+senderMast.getTransferLimit()+":"+resp.get("AgentBalance")+"]");
			return resp;
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getAgentSenderBalance | problem found" + e.getMessage()+" "+e);
			e.printStackTrace();
			if(transaction!=null)
			transaction.rollback();
		}
		finally
		{
			session.close();
		}
		return resp;
	}
	
	
	public BankDetailsBean graminBankIFSC(String bank)
	{
		 BankDetailsBean bankDetailsBean= new BankDetailsBean();
		 bankDetailsBean.setStatusCode("1001");
		 bankDetailsBean.setStatusDesc("No record found.");
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", "*****RequestId***|graminBankIFSC:" + bank);

				factory = DBUtil.getSessionFactory();
				session = factory.openSession();
				List<BankDetailsBean> bankList = new ArrayList();
				try {

					if (bank != null) {

						SQLQuery query = session.createSQLQuery(
								"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname like :bankname");
						query.setParameter("bankname", "%" + bank + "%");
						query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
						bankList = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName")
								.addScalar("address").addScalar("city").addScalar("state").list();

					}
					bankDetailsBean.setBankDetailsList(bankList);
					bankDetailsBean.setStatusCode("1000");
					bankDetailsBean.setStatusDesc("Bank details populate successfully.");

				} catch (Exception e) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", "*problem in graminBankIFSC" + e.getMessage()+" "+e);
					bankDetailsBean.setStatusCode("7000");
					bankDetailsBean.setStatusDesc("Error");
					e.printStackTrace();
					transaction.rollback();
				} finally {
					session.close();
				}

				return bankDetailsBean;
			 
		 }
		 
		 
	private void saveIciciData(String xmlResponse,Long id)
	{
		Session session = null;
		Transaction transaction = null;
		IMPSIciciResponseBean saveResponse = new IMPSIciciResponseBean();
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			
			
			JAXBContext jaxbContext = JAXBContext.newInstance(IMPSIciciResponseBean.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			StringReader reader = new StringReader(xmlResponse);
			saveResponse = (IMPSIciciResponseBean) jaxbUnmarshaller.unmarshal(reader);
			saveResponse.setId(id);
			session.saveOrUpdate(saveResponse);
			transaction.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.info("Exception occured while saving ICICI Request in database "+e.getMessage());
		}
	}
	
	
	private void updateSenderbalance(double balance,String senderId) throws Exception
	{
		Session session = null;
		Transaction transaction = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();

		SQLQuery query = session.createSQLQuery("update mudrasendermast set walletBalance=:balance where id = :senderId");
		query.setDouble("balance", balance);
		query.setString("senderId", senderId);

		query.executeUpdate();
		transaction.commit();
	
	}
	
	
	private Double getSenderbalance(String senderId) throws Exception
	{
		Session session = null;
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		SQLQuery query = session.createSQLQuery("select walletBalance from mudrasendermast where id = :senderId");
		query.setString("senderId", senderId);
		
		BigDecimal balDec = (BigDecimal)query.uniqueResult();

		Double balance = (balDec.setScale(2, RoundingMode.DOWN)).doubleValue();
	
		return balance;
	}
	
	
	
public static void main(String[] args) {
	/*new MudraDaoImpl().fundTransferICICIBankIMPS("1119876543217", "ICIC00HSBLW", 10, "1234", "Shweta", "9860001121");*/
	
	SessionFactory factory = DBUtil.getSessionFactory();
	Session session = factory.openSession();
	Transaction trans = session.beginTransaction();;
	
	trans.commit();
	trans = session.beginTransaction();
	
	
}
private String getDate(String string) {
	
	//TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat df = new SimpleDateFormat(string); // Quoted "Z" to indicate UTC, no timezone offset
	//df.setTimeZone(tz);
	return df.format(new Date());
}

}