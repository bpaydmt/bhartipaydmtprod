package com.bhartipay.mudra;



import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.DMTReportInOut;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.KycConfigBean;
import com.bhartipay.mudra.bean.MiniKycResponse;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderPanDetails;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.ResponseBean;
import com.bhartipay.mudra.bean.SenderFavouriteBean;
import com.bhartipay.mudra.bean.SenderKYCBean;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.user.bean.UploadSenderKyc;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.InfrastructureProperties;
import com.bhartipay.util.KYCUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.kyc.Utils.Constants;
import com.bhartipay.wallet.kyc.Utils.DecryptionBase64;
import com.bhartipay.wallet.kyc.Utils.ResponseXml;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.sun.xml.internal.ws.resources.SenderMessages;

import org.apache.log4j.PropertyConfigurator;
@Path("/MudraManager")
public class MudraManager {
	private static final Logger logger = Logger.getLogger(MudraManager.class.getName());
	MudraDao mudraDao=new MudraDaoImpl();
	CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	public static String ekycuid = "";	
	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
	//DMTInputBean DMTB=new DMTInputBean();
	//CommInputBean CIB=new CommInputBean();
	//MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
	@POST
	@Path("/validateSender")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet validateSender(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest(mudraSenderMastBean.getAgent(),imei,serverName,"validateSender");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"","" , mudraBeneficiaryMastBean.getSenderId(),"", serverName + "|RequestId"+requestId+"|validateSender()|"+"|Agentid"+mudraSenderMastBean.getAgentId());
		/*logger.info("RequestId****"+requestId+"********Start excution ******************************************************* method validateSender(mudraSenderMastBean)");*/
		mudraSenderMastBean.setIpiemi(imei);
		mudraSenderMastBean.setAgent(agent);
		return mudraDao.validateSender(mudraSenderMastBean,serverName,requestId);
		
	}
	
	
	@POST
	@Path("/registerSender")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet registerSender(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest(mudraSenderMastBean.getAgentId(),imei,serverName,"registerSender");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|registerSender()|"+"|Agentid:"+mudraSenderMastBean.getAgentId());
		/*logger.info("RequestId****"+requestId+"*****Start excution ******************************************************* method registerSender(mudraSenderMastBean)");*/
		mudraSenderMastBean.setIpiemi(imei);
		mudraSenderMastBean.setAgent(agent);
		return mudraDao.registerSender(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public BankDetailsBean getBankDetails(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,BankDetailsBean bankDetailsBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("BenAccNo-"+bankDetailsBean.getBeneficiaryAccountNo(),imei,serverName,"getBankDetails");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|getBankDetails()|"+"Bankid:"+bankDetailsBean.getBankId());
		/*logger.info("RequestId*********"+requestId+"******Start excution ******************************************************* method getBankDetails(bankDetailsBean)");*/
		return mudraDao.getBankDetails(bankDetailsBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDetailsByIFSC")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getBankDetailsByIFSC(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,BankDetailsBean bankDetailsBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String ifscCode="";
		if(bankDetailsBean.getIfscCode() != null)
		{
			ifscCode=bankDetailsBean.getIfscCode();
		}
		String requestId=mudraDao.saveDMTApiRequest("IFSC-"+ifscCode,imei,serverName,"getBankDetailsByIFSC");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|getBankDetailsByIFSC()|"+"Bankid:"+bankDetailsBean.getBankId());
		/*logger.info("*****RequestId******"+requestId+"*Start excution ******************************************************* method getBankDetailsByIFSC(bankDetailsBean)");*/
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getBankDetailsByIFSC(bankDetailsBean,serverName,requestId));
		//return mudraDao.getBankDetailsByIFSC(bankDetailsBean,request.getServletContext().getInitParameter("serverName"));
	}
	

	@POST
	@Path("/registerBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryWallet registerBeneficiary(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),imei,serverName,"registerBeneficiary");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|registerBeneficiary()");
		/*logger.info("*****RequestId******"+requestId+"*Start excution ******************************************************* method registerBeneficiary(mudraBeneficiaryMastBean)");*/
		mudraBeneficiaryMastBean.setIpiemi(imei);
		mudraBeneficiaryMastBean.setAgent(agent);
		return mudraDao.registerBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/forgotMPIN")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet forgotMPIN(MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraSenderMastBean.getId(),"",serverName,"forgotMPIN");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|forgotMPIN()|"+"|Agnetid:"+mudraSenderMastBean.getAgentId());
		/*logger.info("*****RequestId******"+requestId+"****Start excution ******************************************************* method forgotMPIN(mudraSenderMastBean)");*/
		return mudraDao.forgotMPIN(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/updateMPIN")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet updateMPIN(MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Aggreatorid-"+mudraSenderMastBean.getAggreatorId(),"",serverName,"updateMPIN");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"","", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|updateMPIN()|"+"|Agentid"+mudraSenderMastBean.getAgentId());
		/*logger.info("*****RequestId******"+requestId+"****Start excution ******************************************************* method updateMPIN(mudraSenderMastBean)");*/
		return mudraDao.updateMPIN(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/otpResend")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String otpResend(WalletBean walletBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Aggreatorid-"+walletBean.getAggreatorid(),"",serverName,"otpResend");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|otpResend()");
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), mudraBeneficiaryMastBean.getSenderId(),"", serverName + "|RequestId"+requestId);
		/*logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getUserId());
		logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getAggreatorid());*/
		return ""+mudraDao.otpSenderResend(walletBean.getUserId(),walletBean.getAggreatorid(),serverName,requestId);
	}
	
	
	@POST
	@Path("/refundOtp")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String refundOtp(WalletBean walletBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Aggreatorid-"+walletBean.getAggreatorid(),"",serverName,"otpResend");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|refundOtp()");
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), mudraBeneficiaryMastBean.getSenderId(),"", serverName + "|RequestId"+requestId);
		/*logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getUserId());
		logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getAggreatorid());*/
		return ""+mudraDao.refundOtp(walletBean.getTxnId(),walletBean.getUserId(),walletBean.getAggreatorid(),serverName,requestId);
	}
	
	@POST
	@Path("/deleteBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryWallet deleteBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deleteBeneficiary");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|deleteBeneficiary()");
		/*logger.info("*****RequestId******"+requestId+"****Start excution ******************************************** method deleteBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());*/
		return mudraDao.deleteBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/setFavourite")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SenderFavouriteBean setFavourite(SenderFavouriteBean senderFavouriteBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+senderFavouriteBean.getSenderId(),"",serverName,"setFavourite");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|setFavourite()|"+"|Agentid:"+senderFavouriteBean.getAgentId());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", senderFavouriteBean.getSenderId(), "", serverName + "RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getSenderId());*/
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|"+"|Agentid:"+senderFavouriteBean.getAgentId());
		
		/*logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getAgentId());
		logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getSenderId());
		logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getMobileNo());*/
		return mudraDao.setFavourite(senderFavouriteBean,serverName,requestId);
	}
	
	@POST
	@Path("/deleteFavourite")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SenderFavouriteBean deleteFavourite(SenderFavouriteBean senderFavouriteBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+senderFavouriteBean.getSenderId(),"",serverName,"deleteFavourite");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(),"", serverName + "RequestId******"+requestId+"|deleteFavourite()|"+"|Agentid"+senderFavouriteBean.getAgentId()+ "id:"+senderFavouriteBean.getId());
		//logger.info("RequestId******"+requestId+"*******Start excution ************************************* deleteFavourite method**:"+senderFavouriteBean.getId());
		return mudraDao.deleteFavourite(senderFavouriteBean,serverName,requestId);
	}
	
	@POST
	@Path("/getFavouriteList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getFavouriteList(MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraSenderMastBean.getAgentId(),"",serverName,"getFavouriteList");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"","", mudraBeneficiaryMastBean.getSenderId(),"", serverName + "RequestId******"+requestId+"|getFavouriteList()|"+"|Agentid:"+mudraSenderMastBean.getAgentId());
		/*logger.info("RequestId******"+requestId+"*******Start excution ===================== method getFavouriteList()" +mudraSenderMastBean.getAgentId());*/
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getFavouriteList(mudraSenderMastBean.getAgentId(),serverName,requestId));
		
	}
	
	@POST
	@Path("/calculateSurcharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+surchargeBean.getAgentId(),"",serverName,"calculateSurcharge");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId******"+requestId+"|calculateSurcharge()|"+"|Agnetid "+surchargeBean.getAgentId());
		/*logger.info("RequestId******"+requestId+"*******Start excution ********************************************* method calculateSurcharge()" );*/
		return mudraDao.calculateSurcharge(surchargeBean,serverName,requestId);
	}
	
	
	/*@POST
	@Path("/mudraMoneyTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean mudraMoneyTransfer(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		logger.info("Start excution ********************************************* method mudraMoneyTransfer()" );
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setUseragent(agent);
		return mudraDao.mudraMoneyTransfer(mudraMoneyTransactionBean,request.getServletContext().getInitParameter("serverName"));
	}*/
	
	
	@POST
	@Path("/verifyAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean verifyAccount(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"verifyAccount");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "RequestId****"+requestId+"|verifyAccount()|"+"|Agentid:"+mudraMoneyTransactionBean.getAgentid() );
	/*	logger.info("RequestId****"+requestId+"*****Start excution ********************************************* method verifyAccount()" );*/
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.verifyAccount(mudraMoneyTransactionBean,serverName,requestId);
	}
	
	
	@POST
	@Path("/fundTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public FundTransactionSummaryBean fundTransfer(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"fundTransfer");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "RequestId***"+requestId+"|fundTransfer()|"+"|Agentid"+mudraMoneyTransactionBean.getAgentid() );
		/*logger.info("RequestId***"+requestId+"******Start excution ********************************************* method fundTransfer()" );*/
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.fundTransfer(mudraMoneyTransactionBean,serverName,requestId);
	}

	@POST
	@Path("/getTransactionDetailsByTxnId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public FundTransactionSummaryBean getTransactionDetailsByTxnId(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"fundTransfer");
		
	      
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(),mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"RequestId***"+requestId+"|fundTransfer()|"+mudraMoneyTransactionBean.getAgentid());
		
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.getTransactionDetailsByTxnId(mudraMoneyTransactionBean,serverName,requestId);
	}
	
	@POST
	@Path("/calculateSurCharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Amount-"+mudraMoneyTransactionBean.getTxnAmount(),"",serverName,"calculateSurCharge");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "RequestId*****"+requestId+"|calculateSurCharge()|"+"|Agentid:"+mudraMoneyTransactionBean.getAgentid());
		//logger.info("RequestId*****"+requestId+"******Start excution ******************************220*************** method calculateSurCharge()" );
		return mudraDao.calculateSurCharge(mudraMoneyTransactionBean,serverName,requestId);

	}
	
	
	
	@POST
	@Path("/getBeneficiaryListForImport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet getBeneficiaryListForImport(MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AggreatorId-"+mudraSenderMastBean.getAggreatorId(),"",serverName,"getBeneficiaryListForImport");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId*****"+requestId+"|getBeneficiaryListForImport()|"+"|Agentid:"+mudraSenderMastBean.getAgentId());
		/*logger.info("RequestId*****"+requestId+"*******Start excution ********************************************* method getBeneficiaryListForImport()" );*/
		return mudraDao.getBeneficiaryListForImport(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/copyBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryWallet copyBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"copyBeneficiary");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId******"+requestId+"|copyBeneficiary()|"+"|AccountNO"+mudraBeneficiaryMastBean.getAccountNo());
		//logger.info("RequestId******"+requestId+"*********Start excution ********************************************* method copyBeneficiary()" );
		return mudraDao.copyBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	
	
	@POST
	@Path("/activeBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet activeBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"activeBeneficiary");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId****"+requestId+"|activeBeneficiary()|"+"|AccountNo "+mudraBeneficiaryMastBean.getAccountNo());
		/*logger.info("RequestId****"+requestId+"*******Start excution ********************************************* method activeBeneficiary()" );*/
		return mudraDao.activeBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/deletedBeneficiary")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public MudraSenderWallet deletedBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
	  //added for logger on 3rd Aug
	  String serverName=request.getServletContext().getInitParameter("serverName");
	  String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deletedBeneficiary");
	  
      
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId****"+requestId+"|deletedBeneficiary()|"+"|AccountNO"+mudraBeneficiaryMastBean.getAccountNo());
	  /*logger.info("RequestId****"+requestId+"******Start excution ********************************************* method deletedBeneficiary()" );*/
	  return mudraDao.deletedBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	 }
	
	@POST
	@Path("/deActiveBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet deActiveBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deActiveBeneficiary");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "RequestId****"+requestId+"|deActiveBeneficiary()|"+"|AccountNO"+mudraBeneficiaryMastBean.getAccountNo());
		/*logger.info("RequestId****"+requestId+"******Start excution ********************************************* method deActiveBeneficiary()" );*/
		return mudraDao.deActiveBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getBankDtl(@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Bank-","",serverName,"getBankDtl");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "","", serverName + "RequestId*******"+requestId+"|getBankDtl()");
		/*logger.info("RequestId*******"+requestId+"*****Start excution ********************************************* method getBankDtl()" );*/
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getBankDtl(serverName,requestId));
	}
	
	
	@POST
	@Path("/getTransacationLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("TxnLedger-","",serverName,"getTransacationLedgerDtl");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "RequestId*****"+requestId+"|getTransacationLedgerDtl()|"+"|Agentid:"+dMTReportInOut.getAgentId());
		/*logger.info("RequestId*****"+requestId+"******Start excution ********************************************* method getTransacationLedgerDtl()" );*/
		return mudraDao.getTransacationLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	
	@POST
	@Path("/getAgentLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getAgentLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentLedger-","",serverName,"getAgentLedgerDtl");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "RequestId***"+requestId+"|getAgentLedgerDtl()|"+"|Agentid"+dMTReportInOut.getAgentId());
		logger.info("RequestId***"+requestId+"*******Start excution ********************************************* method getAgentLedgerDtl()" );
		return mudraDao.getAgentLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	
	@POST
	@Path("/getSenderLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getSenderLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentLedger-","",serverName,"getSenderLedgerDtl");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "RequestId*****"+requestId+"|getSenderLedgerDtl()|"+"|Agentid"+dMTReportInOut.getAgentId());
		/*logger.info("RequestId*****"+requestId+"******Start excution ********************************************* method getSenderLedgerDtl()" );*/
		return mudraDao.getSenderLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	
	
	
	@POST
	@Path("/getBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryWallet getBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(),"", serverName +"|getBeneficiary()");
		/*logger.info("serverName*****"+serverName+"******Start excution ********************************************* method getBeneficiary()" );*/
		return mudraDao.getBeneficiary(mudraBeneficiaryMastBean.getId(),serverName);
	}
	
	

	@POST
	@Path("/getSender")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderWallet getSender(MudraSenderWallet mudraSenderWallet,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", mudraSenderWallet.getId(),"", serverName +"|getSender()");
		/*logger.info("serverName*****"+serverName+"******Start excution ********************************************* method getBeneficiary()" );*/
		return mudraDao.getSender(mudraSenderWallet.getId(),serverName);
	}
	
	
	
	@POST
	@Path("/uploadSenderPanF60")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String userAgent,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		mudraSenderPanDetails.setIpiemi(ipiemi);
		mudraSenderPanDetails.setUserAgent(userAgent);
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName +"|uploadSenderPanF60()|"+"ID: "+mudraSenderPanDetails.getId());
		//logger.info("serverName*****"+serverName+"******Start excution ********************************************* method uploadSenderPanF60()" );
		return mudraDao.uploadSenderPanF60(mudraSenderPanDetails,serverName);
	}
	
	
	@POST
	@Path("/uploadSenderPanF60Android")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderPanDetails uploadSenderPanF60Android(MudraSenderPanDetails mudraSenderPanDetails,@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String userAgent,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		mudraSenderPanDetails.setIpiemi(ipiemi);
		mudraSenderPanDetails.setUserAgent(userAgent);
		
		try{

//		        String base64String = mudraSenderPanDetails.getProofTypePic();
//		        String[] strings = base64String.split(",");
//		        String extension="jpg";
//		        byte[] data;
//		        if(strings.length>=2){
//		        switch (strings[0]) {//check image's extension
//		            case "data:image/jpeg;base64":
//		                extension = "jpeg";
//		                break;
//		            case "data:image/png;base64":
//		                extension = "png";
//		                break;
//		            default://should write cases for more images types
//		                extension = "jpg";
//		                break;
//		        }
//		        //convert base64 string to binary data
//		        data = DatatypeConverter.parseBase64Binary(strings[1]);
//		        }else{
//		        data = DatatypeConverter.parseBase64Binary(strings[0]);	
//		        }
		        

				String filePath=InfrastructureProperties.getInstance().getProperty("senderkyc");
				 System.out.println("Image Location:" + filePath);
				 filePath=filePath.concat("/SENDERPAN");
				 File dir=new File(filePath,mudraSenderPanDetails.getSenderId());
				 boolean mkdir=dir.mkdir();
			     filePath=filePath.concat("/"+mudraSenderPanDetails.getSenderId());
			     
			     	byte[] imageDataBytes = Base64.decodeBase64(mudraSenderPanDetails.getProofTypePic().getBytes());
					InputStream in = new ByteArrayInputStream(imageDataBytes);
					BufferedImage bImageFromConvert = ImageIO.read(in);
					ImageIO.write(bImageFromConvert, "png", new File(filePath+"/"+mudraSenderPanDetails.getSenderId()+"-proof.png"));
					mudraSenderPanDetails.setProofTypePic("./SENDERPAN/"+mudraSenderPanDetails.getSenderId()+"/"+mudraSenderPanDetails.getSenderId()+"-proof.png");
				   // senderKyc.setAddressDesc("");
				   			
			     
		        
		        
		        
		        
		        
		        
//		        String filePath = request.getServletContext().getRealPath("/").concat("SENDERPAN"); 
//		 		 java.io.File dir=new java.io.File(filePath,mudraSenderPanDetails.getSenderId());
//		 	     boolean mkdir=dir.mkdir();
//		 	     filePath=filePath.concat("/"+mudraSenderPanDetails.getSenderId()+"/"+mudraSenderPanDetails.getSenderId()+"."+extension);
//		        
//		        java.io.File file = new java.io.File(filePath);
//		        
//		        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
//		            outputStream.write(data);
//		            mudraSenderPanDetails.setProofTypePic("http://b2b.bhartipay.com/BhartiPayService/SENDERPAN/"+mudraSenderPanDetails.getSenderId()+"/"+mudraSenderPanDetails.getSenderId()+"."+extension);
//		        } catch (IOException e) {
//		            e.printStackTrace();
//		        }
		        
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName +"|uploadSenderPanF60()|"+"ID: "+mudraSenderPanDetails.getId());
		//logger.info("serverName*****"+serverName+"******Start excution ********************************************* method uploadSenderPanF60()" );
		
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "", serverName +"|problem in uploadSenderPanF60()|"+"ID: "+mudraSenderPanDetails.getId());
				
		}
		return mudraDao.uploadSenderPanF60(mudraSenderPanDetails,serverName);
	}
	
	
	@POST
	@Path("/getPendingPanReq")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(),"", serverName +"|getPendingPanReq()|"+ "ID: "+mudraSenderPanDetails.getId());
		/*logger.info("serverName*****"+serverName+"******Start excution ********************************************* method getPendingPanReq()" );*/
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(mudraDao.getPendingPanReq(mudraSenderPanDetails,serverName));
		
		
	}
	
	
	@POST
	@Path("/rejectAcceptedSenderPan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails,@Context HttpServletRequest request){
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "", serverName+"|rejectAcceptedSenderPan()" );
		/* logger.info("serverName*****"+serverName+"******Start excution ********************************************* method rejectAcceptedSenderPan()" );*/
		 return mudraDao.rejectAcceptedSenderPan(mudraSenderPanDetails,serverName);
	 }
	
	
	@POST
	@Path("/getPendingKycList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getPendingKycList(UploadSenderKyc senderKyc,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(mudraDao.getPendingKycList(senderKyc,serverName));
		
		
	}
	
	
	@POST
	@Path("/rejectAcceptedSenderKyc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public UploadSenderKyc rejectAcceptedSenderKyc(UploadSenderKyc senderKyc,@Context HttpServletRequest request){
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 return mudraDao.rejectAcceptedSenderKyc(senderKyc,serverName);
	 }
	
	
	public String getLogic(String xmlstring,String slk, String deviceId, String stack, String rrn, String backupurl, String ServiceType,String UniversalUniqueIdentifier ) 
	 {    
	  String encodedFinalString = "";   
	  byte[] finaldata = null;   
	  StringBuffer xmlStr = null;    
	  try {    
	   xmlStr = new StringBuffer();     
	   xmlStr.append("<"+xmlstring+">").append("<SLK>").append(slk).append("</SLK>").append("<SERVICETYPE>").append(ServiceType).append("</SERVICETYPE>").append("<RRN>").append(rrn).append("</RRN>").append("<DEVICEID>").append(deviceId).append("</DEVICEID>").append("<STACK>").append(stack).append("</STACK>"). append("<UUID>").append(UniversalUniqueIdentifier).append("</UUID>").append("<REDIRECTURL>").append(backupurl).append("</REDIRECTURL>").append("<RFKEY>").append("").append("</RFKEY>").append("</"+xmlstring+">");     
	   System.out.println("xmlStr"+xmlStr);
	   finaldata = xmlStr.toString().getBytes();
	   System.out.println( finaldata);   
	   } catch (Exception ext) {    
	    ext.printStackTrace();   
	  }   
	  
	  encodedFinalString = new String(org.apache.commons.codec.binary.Base64.encodeBase64(finaldata));   
	  
	  return encodedFinalString;  
	}
	 
	
	public String getLogicNew(String key,String initVector,String transType,String transId, String aAdhaarNo, String deviceId, String failedUrl, String successUrl,String customData1,String customData2,String customData3,String customData4,String customData5,String customData6,String customData7,String customData8,String customData9,String customData10) 
	 {    
	  String encodedFinalString = "";   
	  String  value = null;    
	  try {    
		  value=transType+"|"+transId+"|"+aAdhaarNo+"|"+deviceId+"|"+failedUrl+"|"+successUrl+"|"+customData1+"|"+customData2+"|"+customData3+"|"+customData4+"|"+customData5+"|"+customData6+"|"+customData7+"|"+customData8+"|"+customData9+"|"+customData10;
	   } catch (Exception ext) {    
	    ext.printStackTrace();   
	  }   
	  
	  KYCUtil kycUtil=new KYCUtil();
	  encodedFinalString = kycUtil.encrypt(key, initVector, value);
	  
	  return encodedFinalString;  
	}
	 
	 @POST
	 @Path("/eKYCResponse")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public EkycResponseBean eKYCResponse(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,EkycResponseBean ekycResponseBean,@Context HttpServletRequest request){
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|eKYCResponse()|"+"Create kyc log");
		 //logger.info("Create kyc log");
	     mudraDao.saveSenderKycResponse(ekycResponseBean);
	     
	     return ekycResponseBean;
	   
	 }
	 
	 @POST
	 @Path("/sendereKYC")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public KycConfigBean sendereKYC(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,SenderKYCBean SenderKYCBean,@Context HttpServletRequest request){
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"","", "", "",""+"|sendereKYC()");
		 /*logger.info("sender KYC configuration call method getkycConfig");*/
	   
	   
	   TreeMap<String,String> obj=new TreeMap<String,String>();
	    obj.put("userId", SenderKYCBean.getAgentid());
	    obj.put("agentid", SenderKYCBean.getAgentid());
	       obj.put("aggreatorid",SenderKYCBean.getAggreatorid());
	       obj.put("servicetype",SenderKYCBean.getServicetype());
	       obj.put("mobileno",SenderKYCBean.getMobileno());
	       obj.put("CHECKSUMHASH",SenderKYCBean.getCHECKSUMHASH());
	       
	       Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"treemap which for checksecurity "+obj);
	    /*logger.debug("mno-"+SenderKYCBean.getMobileno()+"treemap which for checksecurity********************"+obj);*/       
	     int statusCode= WalletSecurityUtility.checkSecurity(obj);
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"after checking security status code is "+statusCode);
	    /* logger.debug("mno-"+SenderKYCBean.getMobileno()+"after checking security status code is********************"+statusCode);*/ 
	     if(statusCode!=1000){
	    	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration !1000");
	    	/* logger.info("mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration----------------------> !1000");*/
	      KycConfigBean kyBean=new KycConfigBean();
	      kyBean.setStatusCode(""+statusCode);
	      kyBean.setStatusDesc("Security error.");
	      return kyBean;
	     }
	     
	     
	 /*	 WalletUserDao userDao=new WalletUserDaoImpl();
	     if(SenderKYCBean.getRequestFor()==null||!SenderKYCBean.getRequestFor().equalsIgnoreCase("sender")){
	     String kycStatus=userDao.checkUserKycStatus(SenderKYCBean.getMobileno());
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+ "after kyc check bean");
	     logger.info("mno-"+SenderKYCBean.getMobileno()+ "after kyc check bean");
	     if(kycStatus!=null&&kycStatus.equalsIgnoreCase("active")){
	    	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration already kyc done"); 
	    	 logger.info("mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration---------------------->already kyc done");
	      KycConfigBean kyBean=new KycConfigBean();
	      kyBean.setStatusCode("300");
	      kyBean.setStatusDesc("Already kyc done.");
	     return kyBean;
	  }
	 }*/
	     
	     
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration before calling kycconfig");
	     /*logger.info("mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration---------------------->before calling kycconfig");*/
	   KycConfigBean kycconfigbean=mudraDao.getkycConfig(SenderKYCBean,SenderKYCBean.getServicetype());
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration after calling kycconfig"+kycconfigbean);
	   //logger.info("mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration---------------------->after calling kycconfig"+kycconfigbean!=null?kycconfigbean.getRedirecturl():kycconfigbean);
	   
	   if(kycconfigbean.getStatusCode().equals("1000")){
	   if (agent.toLowerCase().indexOf("andriod")>0)
	    kycconfigbean.setStack("02");
	   else 
	    kycconfigbean.setStack("01");
	  
	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");  
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"Create kyc timestamp value yyyyMMddhhmmssSSS"+dateFormat);
	        String time = dateFormat.format(new Date());   // params from encodedFinalString  
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "","", "", "mno-"+SenderKYCBean.getMobileno()+"generate unique reference number");
	        
	     String kyctrxId=commanUtilDao.getTrxId("KYCT",SenderKYCBean.getAggreatorid());
	     kycconfigbean.setTxnId(kyctrxId);
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+" unique reference number for kyc transaction :"+kyctrxId);
	     String callBackUrl="";
	     if(SenderKYCBean.getRequestFrom()!=null&&SenderKYCBean.getRequestFrom().equalsIgnoreCase("APP")){
	    	 callBackUrl=kycconfigbean.getCallbackurl()+"/kycresponseAndriod";
	     }else{
	    	 callBackUrl=kycconfigbean.getCallbackurl()+"/kycresponse";
	     }
	     
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "","mno-"+SenderKYCBean.getMobileno()+"sender KYC setAuthxml before calling setAuthxml");
	    // kycconfigbean.setAuthxml(getLogic(kycconfigbean.getXmlstring(), kycconfigbean.getSlk(),kycconfigbean.getDeviceid(),kycconfigbean.getStack(),kyctrxId+"-"+time,callBackUrl,kycconfigbean.getServicevalue(),""));
	     String initVector= KYCUtil.generateRandomIV(16);
	     kycconfigbean.setIv(initVector);
	     kycconfigbean.setAuthxml(getLogicNew(kycconfigbean.getSlk(), initVector, "E", kyctrxId, "",SenderKYCBean.getDeviceId(), callBackUrl, callBackUrl, "", "", "", "", "", "", "", "", "", ""));
	     
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"save partial information to DB ref:"+kyctrxId);
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC setAuthxml before calling setAuthxml"+kycconfigbean.getAuthxml()); 
	     Boolean isvalid=mudraDao.setSenderKycMast(kycconfigbean, SenderKYCBean, kycconfigbean.getServicetype(),time,kyctrxId);
	     
	      }else{
	    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "mno-"+SenderKYCBean.getMobileno()+"sender KYC configuration call method getkycConfig error code"+kycconfigbean.getStatusCode());
	   }
 	   return kycconfigbean;
	   
	  
	 }
	    @POST
		@Path("/getResponseXml")
		@Produces({ MediaType.APPLICATION_JSON })
		public EkycResponseBean getResponseXml(@Context HttpServletRequest request,@Context HttpServletResponse response,String ekycBean){
	    	
	    	/*logger.info("Start excution ************************************ method getResponseXml()");*/
	    	
	    	//System.out.println("got responseXML");
			String userAgent=request.getHeader("User-Agent");
			com.bhartipay.mudra.bean.EkycResponseBean ekycResponseBean = new com.bhartipay.mudra.bean.EkycResponseBean();
			
			EkycResponseBean ekyc=new Gson().fromJson(ekycBean,EkycResponseBean.class);
		      
			
			
			try {
		
				MudraDao mudradao=new MudraDaoImpl();


								ekycResponseBean=mudradao.getEKycSenderData(ekyc);
								
								ekycResponseBean.setUID(ekyc.getUuid());
								ekycResponseBean.setServicetype(ekyc.getServicetype());
								ekycResponseBean.setPht(ekyc.getPht());
								ekycResponseBean.setRCode(ekyc.getRCode());
								
								ekycResponseBean.setRtxn(ekyc.getRtxn());
								ekycResponseBean.setRts(ekyc.getRts());
								ekycResponseBean.setUuid(ekyc.getUuid());
								
								ekycResponseBean.setPoidob(ekyc.getPoidob());
								ekycResponseBean.setPoigender(ekyc.getPoigender());
								ekycResponseBean.setPoiname(ekyc.getPoiname());
								ekycResponseBean.setPoaco(ekyc.getPoaco());
								ekycResponseBean.setPoadist(ekyc.getPoadist());
								ekycResponseBean.setPoalm(ekyc.getPoalm());
								ekycResponseBean.setPoaloc(ekyc.getPoaloc());
								ekycResponseBean.setPoapc(ekyc.getPoapc());
								ekycResponseBean.setPoapo(ekyc.getPoapo());
								ekycResponseBean.setPoastate(ekyc.getPoastate());
								ekycResponseBean.setPoavtc(ekyc.getPoavtc());
								if(ekyc.getStatus()!=null&&ekyc.getStatus().equalsIgnoreCase("Y"))
								ekycResponseBean.setStatus("1");
								else
								ekycResponseBean.setStatus(ekyc.getStatus());
								mudradao.saveSenderKycResponse(ekycResponseBean);
								ekycResponseBean.setStatus("1000");
								return ekycResponseBean;
								// System.out.println("KYCResponse ::" result.);
						} catch (Exception e) {
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "******problem in get response xml==" + e.getMessage());
							
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			
			return ekycResponseBean;
		 }
	
	 
	 @POST
	 @Path("/getEKycSenderData")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public EkycResponseBean getEKycSenderData(EkycResponseBean ekycResponseBean){
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|getEKycSenderData()|"+"Create kyc log");
		 /*logger.info("Create kyc log");*/
	     ekycResponseBean=mudraDao.getEKycSenderData(ekycResponseBean);
	     return ekycResponseBean;
	   
	 }
	 
	 @POST
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Path("/updateMiniKyc")
	 public MiniKycResponse updateMiniKyc(String miniKycDtls)
	 {
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "","|updateMiniKyc()");
	 	/*logger.info("***********start execution updateMiniKyc**********");*/
	 	MiniKycResponse response = new MiniKycResponse();
	 	try
	 	{
	 		JSONObject json = new JSONObject(miniKycDtls);
	 		response= mudraDao.updateMiniKyc(json.getString("userId"), json.getString("documentType"), json.getString("documentId"));
	 	}
	 	catch(Exception e)
	 	{
	 		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", "problem in updateMiniKyc"+e.getMessage());
	 		/*logger.info("*******problem in updateMiniKyc**********e.messgage**"+e.getMessage());*/
	 		e.printStackTrace();
	 		response.setStatusCode("7000");
	 		response.setMiniKycStatus(0);
	 	}
	 	return response;
	 }
	 
	 
	 @POST
	 @Path("/getAgentSenderBalance")
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Consumes({MediaType.APPLICATION_JSON})
	 public String getAgentSenderBalance(MudraSenderWallet mudraSenderMastBean)
	 {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "","", "","|Method Name :getAgentSenderBalance");
		 HashMap resp=mudraDao.getAgentSenderBalance(mudraSenderMastBean);
		 String respJson = new Gson().toJson(resp);
	     return respJson;
	 }

	 @POST
	 @Path("/graminBankIFSC")
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Consumes({MediaType.APPLICATION_JSON})
	 public List<BankDetailsBean> graminBankIFSC(String bank)
	 {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :graminBankIFSC");
		 BankDetailsBean resp=mudraDao.graminBankIFSC(bank);
	     return resp.getBankDetailsList();
	 }
	 
	 public static void main(String[] s)
		{
			MudraSenderWallet mudra = new MudraSenderWallet();
			mudra.setAgentId("OXMA1000");
			mudra.setId("MSEN001026");
			mudra.setAggreatorId("OAGG001050");
			new MudraManager().getAgentSenderBalance(mudra);
		}
	 
	 public String ekycBackUp(HttpServletRequest request){

	    	
	    	/*logger.info("Start excution ************************************ method getResponseXml()");*/
	    	
	    	//System.out.println("got responseXML");
			String userAgent=request.getHeader("User-Agent");
			com.bhartipay.mudra.bean.EkycResponseBean ekycResponseBean = new com.bhartipay.mudra.bean.EkycResponseBean();
			
		//	EkycResponseBean ekyc=new Gson().fromJson(ekycBean,EkycResponseBean.class);
		      
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), ""+"|getResponseXml");
	    	
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|getResponseXml|"+"got responseXML");
			
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "**************getResponseXML****************************");
			//System.out.println("getResponseXML");
			String resp = null;
			try {
				resp = convertStreamToString(request.getInputStream());
			} catch (IOException e1) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "problem in responseXM " + e1.getMessage()+" "+e1);
				
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "getResponseXML "+resp);
			
			//System.out.println("getResponseXML"+resp);
			JAXBContext jaxbContext;
			/*
			 * Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			 * KYCRES1 que= (KYCRES1) jaxbUnmarshaller.;
			 */
			StringReader reader = new StringReader(resp);
			Unmarshaller unmarshaller;
			ResponseXml result = null;
			try {
				jaxbContext = JAXBContext.newInstance(ResponseXml.class);
				unmarshaller = jaxbContext.createUnmarshaller();
				result = (ResponseXml) unmarshaller.unmarshal(reader);

				String UUID = result.getUUID();
				
				/*logger.debug("UUID************************************"+UUID);*/
		
				String servicetype = result.getSERVICETYPE();
				/*logger.debug("servicetype************************************"+servicetype);*/
				
				String errCode = result.getERRCODE();
				/*logger.debug("errCode************************************"+errCode);*/
				
				String dkey = result.getDKEY();
				/*logger.debug("dkey************************************"+dkey);*/
				
				String rfkey = result.getRFKEY();
				/*logger.debug("rfkey************************************"+rfkey);*/
				
				String status = result.getSTATUS();
				/*logger.debug("status************************************"+status);*/
				
				String rrn = result.getRRN();
				/*logger.debug("rrn************************************"+rrn);*/
				
				String errMsg = result.getERRMSG();
				/*logger.debug("errMsg************************************"+errMsg);*/
				
				String responseData = result.getRESPONSE();
				/*logger.debug("responseData************************************"+responseData);*/
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "|"+" UUID** "+UUID +" servicetype** "+servicetype +"errCode** "+errCode +" dkey** "+dkey +" rfkey** "+rfkey +" status** "+ status +"rrn** "+rrn +" errMsg** "+errMsg+" responseData** "+responseData);
				
				MudraDao mudradao=new MudraDaoImpl();

				if (status.equalsIgnoreCase("1")) {
					if (servicetype.equalsIgnoreCase("04") || servicetype.equalsIgnoreCase("05")
							|| servicetype.equalsIgnoreCase("06")) {

						DecryptionBase64 decrpt = new DecryptionBase64();
						FileInputStream fileInputStream = null;
						String p12Password = Constants.certificatePassword;
						try {
							Security.addProvider(new BouncyCastleProvider());

							FileInputStream file = new FileInputStream(Constants.certificatefilepath);

							KeyStore keystore = KeyStore.getInstance("PKCS12", "SunJSSE");
							keystore.load(file, p12Password.toCharArray());
							String alias = keystore.aliases().nextElement();
							PrivateKey privateKey = (PrivateKey) keystore.getKey(alias, p12Password.toCharArray());

							byte[] originalSessionKey = decrpt.decryptUsingPublicKey(  org.apache.commons.codec.binary.Base64.decodeBase64(result.getDKEY().getBytes()), privateKey);

							byte[] orgData = decrpt.decryptUsingSessionKey(originalSessionKey,org.apache.commons.codec.binary.Base64.decodeBase64(result.getRESPONSE().getBytes()));

							String kycResponseXml = new String(orgData);

							try {
								DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
								DocumentBuilder builder = factory.newDocumentBuilder();
								InputSource is = new InputSource(new StringReader(kycResponseXml));

								Document doc = builder.parse(is);

								String Rcode = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes()
										.getNamedItem("code") != null) {
									Rcode = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("code")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "Rcode**"+Rcode);
								/*logger.debug("Rcode************************************"+Rcode);*/

								String Rtxn = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes()
										.getNamedItem("txn") != null) {
									Rtxn = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("txn")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "Rtxn**"+Rtxn);
								/*logger.debug("Rtxn************************************"+Rtxn);*/

								
								String Rts = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts") != null) {
									Rts = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "Rts**"+Rts);
								/*logger.debug("Rts************************************"+Rts);*/
								
								String Uuid = "";
								if (doc.getElementsByTagName("UidData").item(0).getAttributes()
										.getNamedItem("uuid") != null) {
									Uuid = doc.getElementsByTagName("UidData").item(0).getAttributes().getNamedItem("uuid")
											.getTextContent();
									ekycuid = Uuid;
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "Uuid**"+Uuid);
								/*logger.debug("Uuid************************************"+Uuid);*/
								
								String dob = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob") != null) {
									dob = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "dob**"+dob);
								/*logger.debug("dob************************************"+dob);*/
								
								String gender = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes()
										.getNamedItem("gender") != null) {
									gender = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("gender")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "gender**"+gender);
								/*logger.debug("gender************************************"+gender);*/

								String name = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name") != null) {
									name = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "name**"+name);
								/*logger.debug("name************************************"+name);*/

								String co = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co") != null) {
									co = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "co**"+co);
								/*logger.debug("co************************************"+co);*/

								String country = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes()
										.getNamedItem("country") != null) {
									country = doc.getElementsByTagName("Poa").item(0).getAttributes()
											.getNamedItem("country").getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "dob**"+dob);
								/*logger.debug("dob************************************"+dob);*/

								String dist = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist") != null) {
									dist = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "dist**"+dist);
								/*logger.debug("dist************************************"+dist);*/

								String lm = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm") != null) {
									lm = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "lm**"+lm);
								/*logger.debug("lm************************************"+lm);*/

								String loc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc") != null) {
									loc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "loc**"+loc);
								/*logger.debug("loc************************************"+loc);*/

								String pc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc") != null) {
									pc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "pc**"+pc);
								/*logger.debug(" pc************************************"+ pc);*/

								String po = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po") != null) {
									po = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po")
											.getTextContent();
								}
								
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "po**"+po);
								/*logger.debug(" po************************************"+ po);*/

								String state = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state") != null) {
									state = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "state**"+state);
								/*logger.debug("state************************************"+state);*/

								String vtc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc") != null) {
									vtc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "vtc**"+vtc);
								/*logger.debug("vtc************************************"+vtc);*/

								String pht = "";
								if (doc.getElementsByTagName("Pht") != null) {
									pht = doc.getElementsByTagName("Pht").item(0).getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "pht**"+pht);
								/*logger.debug("pht************************************"+pht);*/

								
								
								ekycResponseBean.setKyctxnid(rrn.substring(0,rrn.indexOf("-")));
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", "", ekycResponseBean.getSenderid(), "", " RRN ="+ekycResponseBean.getKyctxnid());
								//System.out.println(" RRN ="+ekycResponseBean.getKyctxnid());
								
								

								ekycResponseBean=mudradao.getEKycSenderData(ekycResponseBean);
								
								ekycResponseBean.setUID(UUID);
								ekycResponseBean.setServicetype(servicetype);
								ekycResponseBean.setPht(pht);
								ekycResponseBean.setRCode(Rcode);
								
								ekycResponseBean.setRtxn(Rtxn);
								ekycResponseBean.setRts(Rts);
								ekycResponseBean.setUuid(Uuid);
								
								ekycResponseBean.setPoidob(dob);
								ekycResponseBean.setPoigender(gender);
								ekycResponseBean.setPoiname(name);
								ekycResponseBean.setPoaco(co);
								ekycResponseBean.setPoadist(dist);
								ekycResponseBean.setPoalm(lm);
								ekycResponseBean.setPoaloc(loc);
								ekycResponseBean.setPoapc(pc);
								ekycResponseBean.setPoapo(po);
								ekycResponseBean.setPoastate(state);
								ekycResponseBean.setPoavtc(vtc);
								ekycResponseBean.setStatus(status);

							} catch (ParserConfigurationException | SAXException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}

						} catch (Exception e) {
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "******problem in get response xml==" + e.getMessage());
							
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (servicetype.equalsIgnoreCase("01") || servicetype.equalsIgnoreCase("02")
							|| servicetype.equalsIgnoreCase("03")) {

						
						byte[] authresponse = org.apache.commons.codec.binary.Base64.decodeBase64(result.getRESPONSE().getBytes());

						// String authresponseXml = authresponse.toString();
						String authresponseXml = new String(authresponse);

						// DocumentBuilderFactory factory =
						// DocumentBuilderFactory.newInstance();
						try {
							/*
							 * DocumentBuilder builder =
							 * factory.newDocumentBuilder(); Document doc =
							 * builder.parse("");
							 */

							InputStream is = new ByteArrayInputStream(authresponseXml.getBytes());

							DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
							domFactory.setIgnoringComments(true);
							DocumentBuilder builder = domFactory.newDocumentBuilder();
							Document doc = builder.parse(is);
							String Rcode = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("code")
									.getTextContent();
							String Rtxn = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("txn")
									.getTextContent();
							String Rts = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("ts")
									.getTextContent();

							
						} catch (ParserConfigurationException | SAXException | IOException e) {
							
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} else {
					ekycuid = "";
				}
				
				mudradao.saveSenderKycResponse(ekycResponseBean);
				// System.out.println("KYCResponse ::" result.);

			} catch (JAXBException e) {
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "***problem==" + e.getMessage()+" "+e);
				 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "200:ok";
		 
	 }
}
