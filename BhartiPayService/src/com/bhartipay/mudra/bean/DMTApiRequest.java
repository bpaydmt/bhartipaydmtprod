package com.bhartipay.mudra.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dmtapirequestlog")
public class DMTApiRequest implements Serializable{
	

	@Id
	@Column(name="requestid",length =50)
	private String requestId;
	
	@Column(name="agentid",length =50)
	private String agentId;
	
	@Column(name="requestvalue",length =500)
	private String requestValue;
	
	@Column(name="apiname",length =500)
	private String apiName;
	
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date requestdate;
	
	
	
	
	
	

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getRequestValue() {
		return requestValue;
	}

	public void setRequestValue(String requestValue) {
		this.requestValue = requestValue;
	}

	public Date getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(Date requestdate) {
		this.requestdate = requestdate;
	}
	
	
	
	
	
	

}
