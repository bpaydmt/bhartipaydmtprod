package com.bhartipay.mudra.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity(name="blockbank")
public class BlockedBank {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="bankname")
     private String bankName;
	@Column(name="datetime")
     private Date DateTime;
	@Column(name="isbank",length=1)
    private int isBank;
     
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public Date getDateTime() {
		return DateTime;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public void setDateTime(Date dateTime) {
		DateTime = dateTime;
	}
	public int getIsBank() {
		return isBank;
	}
	public void setIsBank(int isBank) {
		this.isBank = isBank;
	}
     
     

}
