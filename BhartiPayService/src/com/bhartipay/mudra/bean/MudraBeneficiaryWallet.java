package com.bhartipay.mudra.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
//@Table(name = "walletbeneficiarymast")
@Table(name = "mudrabeneficiarymast")
public class MudraBeneficiaryWallet implements Serializable,Cloneable{

	@Id
	@Column(name="id",length =30)
	private String id;
	
	@Column(name = "aggreatorid", nullable = false, length = 10)
	private String aggreatorId;
	
	@Column(name = "senderid", nullable = false, length = 10)
	private String senderId;
	
	@Column(name = "name", nullable = false, length = 50)
	private String name;
		
	@Column(name = "bankname", nullable = false, length = 50)
	private String bankName;
	
	@Column(name = "accountno", nullable = false, length = 27)
	private String accountNo;
	
	@Column(name = "ifsccode", nullable = false, length = 20)
	private String ifscCode;
	
	@Column(name = "branchname", nullable = true, length = 50)
	private String branchName;
	
	@Column(name = "transfertype", nullable = true, length = 10)
	private String transferType;
	

	
	@Column(name = "city", length = 50)
	private String city;
	
	@Column(name = "state", length = 50)
	private String state ;
	
	@Column(name = "address", length = 100)
	private String address  ;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 500 )
	private String agent;
	
	@Column(name = "status", nullable = false, length = 20)
	private String status;
		
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp regdate;
	
	
	@Column(name = "verified", nullable = false, length = 2)
	private String verified;
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusDesc;
	
	@Transient
	private String mpin;
	
	@Transient
	 private int isCust;
	
	@Transient
	 private String agentId;
	 
	 
	 public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public int getIsCust() {
	  return isCust;
	 }

	 public void setIsCust(int isCust) {
	  this.isCust = isCust;
	 }
		
	





	public String getMpin() {
		return mpin;
	}





	public void setMpin(String mpin) {
		this.mpin = mpin;
	}





	public Object cloneMe()throws CloneNotSupportedException{  
		return super.clone();  
		}  
	
	
	
	

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getRegdate() {
		return regdate;
	}

	public void setRegdate(Timestamp regdate) {
		this.regdate = regdate;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	
	
	
	
}
