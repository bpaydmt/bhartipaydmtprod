package com.bhartipay.mudra.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bendataV")
public class VerifiedBeneficiaryData implements Serializable,Cloneable{

	
	@Column(name="id",length =30)
	private String id;

	@Id
	@Column(name = "accountno", nullable = false, length = 27)
	private String accountNo;
	
	@Column(name = "ifsccode", nullable = false, length = 20)
	private String ifscCode;
	
	@Column(name = "benename", nullable = false, length = 100)
	private String beneName;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@Column(name = "bankname", length = 100)
	private String bankName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	
	
}
