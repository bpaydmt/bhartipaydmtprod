package com.bhartipay.mudra.bean;

public class MiniKycResponse 
{
	private int miniKycStatus;
	
	private String statusCode;

	public int getMiniKycStatus() {
		return miniKycStatus;
	}

	public void setMiniKycStatus(int miniKycStatus) {
		this.miniKycStatus = miniKycStatus;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	
}

