package com.bhartipay.mudra.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "kycconfig")
public class KycConfigBean {
	
	@Id
	@Column(name = "aggreator", nullable = false, length = 10)
	private String aggreator;
	
	@Column(name = "slk", nullable = false, length = 10)
	private String slk;
	@Column(name = "servicetype", nullable = false, length = 10)
	private String servicetype;
	@Column(name = "deviceid", nullable = false, length = 10)
	private String deviceid;
	@Column(name = "stack", nullable = false, length = 10)
	private String stack;	
	@Column(name = "redirecturl", nullable = false, length = 10)
	private String redirecturl;

	@Column(name = "callbackurl", nullable = false, length = 10)
	private String callbackurl;

	@Column(name = "servicevalue", nullable = false, length = 10)
	private String servicevalue;
	
	@Column(name = "xmlstring", nullable = false, length = 10)
	private String xmlstring;
	
	
	@Column(name = "requestfrom", nullable = false, length = 10)
	private String requestFrom;
	
	@Transient
	public String authxml;
	
	@Transient
	public String statusCode;
	
	@Transient
	public String statusDesc;
	
	@Transient
	private String iv;
	
	@Transient
	private String txnId;
	
	
	
	    
		public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

		public String getIv() {
			return iv;
		}

		public void setIv(String iv) {
			this.iv = iv;
		}
	
	
	
	
	public String getRequestFrom() {
		return requestFrom;
	}

	public void setRequestFrom(String requestFrom) {
		this.requestFrom = requestFrom;
	}

	public String getAggreator() {
		return aggreator;
	}
	
	public void setAggreator(String aggreator) {
		this.aggreator = aggreator;
	}
	
	
	public String getSlk() {
		return slk;
	}
	
	public void setSlk(String slk) {
		this.slk = slk;
	}
	
	public String getServicetype() {
		return servicetype;
	}
	
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	
	public String getDeviceid() {
		return deviceid;
	}
	
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	
	public String getStack() {
		return stack;
	}
	
	public void setStack(String stack) {
		this.stack = stack;
	}
	
	public String getRedirecturl() {
		return redirecturl;
	}
	
	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}

	public String getAuthxml() {
		return authxml;
	}

	public void setAuthxml(String authxml) {
		this.authxml = authxml;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getCallbackurl() {
		return callbackurl;
	}

	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}

	public String getServicevalue() {
		return servicevalue;
	}

	public void setServicevalue(String servicevalue) {
		this.servicevalue = servicevalue;
	}

	public String getXmlstring() {
		return xmlstring;
	}

	public void setXmlstring(String xmlstring) {
		this.xmlstring = xmlstring;
	}

		

}
