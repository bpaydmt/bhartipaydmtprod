package com.bhartipay.mudra.bean;

import java.io.Serializable;

public class SurchargeBean implements Serializable{
	
	private String agentId;
	private  double amount; 
	private  double surchargeAmount; 
	private double distSurCharge;
	private  String status;
	private  int txnCode;
	private String transType;
	private double creditSurcharge;
	private double suDistSurcharge;
	private double agentRefund;
	private double aggReturnVal;
	private double dtdsApply;
	
	

	
	
	public double getAggReturnVal() {
		return aggReturnVal;
	}
	public void setAggReturnVal(double aggReturnVal) {
		this.aggReturnVal = aggReturnVal;
	}
	public double getDtdsApply() {
		return dtdsApply;
	}
	public void setDtdsApply(double dtdsApply) {
		this.dtdsApply = dtdsApply;
	}
	public double getAgentRefund() {
		return agentRefund;
	}
	public void setAgentRefund(double agentRefund) {
		this.agentRefund = agentRefund;
	}
	public double getSuDistSurcharge() {
		return suDistSurcharge;
	}
	public void setSuDistSurcharge(double suDistSurcharge) {
		this.suDistSurcharge = suDistSurcharge;
	}
	public double getCreditSurcharge() {
		return creditSurcharge;
	}
	public void setCreditSurcharge(double creditSurcharge) {
		this.creditSurcharge = creditSurcharge;
	}
	public double getDistSurCharge() {
		return distSurCharge;
	}
	public void setDistSurCharge(double distSurCharge) {
		this.distSurCharge = distSurCharge;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	public double getSurchargeAmount() {
		return surchargeAmount;
	}
	public void setSurchargeAmount(double surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(int txnCode) {
		this.txnCode = txnCode;
	}
	

}
