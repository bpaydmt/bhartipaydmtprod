package com.bhartipay.mudra.bean;

import java.math.BigInteger;
import java.sql.Date;


public class FundTransferBean {

	private String id;

	private String txnId;

	private String walletId;

	private Date ptytransdt;

	private String merchantTransId;
	
	private int isBank;

	private double surChargeAmount;

	private double amount;

	private String userId;

	private String statusCode;

	private String statusDesc;

	private String senderFName;
	
	private String senderLName;
	
	private String accHolderName;

	private String transType;
	
	private String aggregatorId;
	
	private String senderMobileNo;
	
	private String beneName;
	
	private String bankName;
	
	private String accountNo;
	
	private String ifscCode;
	
	private String branchName;
	
	private String transferType;
	
	
	
	
	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getTxnId() {
		return txnId;
	}


	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}


	public String getWalletId() {
		return walletId;
	}


	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}


	public Date getPtytransdt() {
		return ptytransdt;
	}


	public void setPtytransdt(Date ptytransdt) {
		this.ptytransdt = ptytransdt;
	}


	public String getMerchantTransId() {
		return merchantTransId;
	}


	public void setMerchantTransId(String merchantTransId) {
		this.merchantTransId = merchantTransId;
	}


	public int getIsBank() {
		return isBank;
	}


	public void setIsBank(int isBank) {
		this.isBank = isBank;
	}


	public double getSurChargeAmount() {
		return surChargeAmount;
	}


	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getStatusDesc() {
		return statusDesc;
	}


	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}


	public String getSenderFName() {
		return senderFName;
	}


	public void setSenderFName(String senderFName) {
		this.senderFName = senderFName;
	}


	public String getSenderLName() {
		return senderLName;
	}


	public void setSenderLName(String senderLName) {
		this.senderLName = senderLName;
	}


	public String getAccHolderName() {
		return accHolderName;
	}


	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}


	public String getTransType() {
		return transType;
	}


	public void setTransType(String transType) {
		this.transType = transType;
	}


	public String getAggregatorId() {
		return aggregatorId;
	}


	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}


	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
