package com.bhartipay.mudra.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "senderfavouritemast")
public class SenderFavouriteBean {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column( name ="id", length=5, nullable = false)
	private int id;	
	
	@Column( name = "agentid", length=30, nullable = false)
	private String agentId;
	
	@Column( name = "senderid", length=30, nullable = false)
	private String senderId;
	
	
	@Column(name = "mobileno",  nullable = false, length = 10)
	private String mobileNo;
	
	
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusDesc;
	
	@Transient
	List<SenderFavouriteViewBean> senderFavouriteList;
	
	
	
	
	
	
	
	

	public List<SenderFavouriteViewBean> getSenderFavouriteList() {
		return senderFavouriteList;
	}

	public void setSenderFavouriteList(List<SenderFavouriteViewBean> senderFavouriteList) {
		this.senderFavouriteList = senderFavouriteList;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	
	
	
	
	
}
