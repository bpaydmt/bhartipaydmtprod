package com.bhartipay.mudra.bank3.payoutapi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import com.paytm.pg.merchant.PaytmChecksum;

public class PayoutMasterDMT {
	
	/* for UAT */
	
//	public static final String MERCHANT_ID  = "TheCos79234936457541";
//	public static final String MERCHANT_KEY = "9&0I52BZpOv@6pTC";
//	public static final String SUB_WALLET_GUID = "ddecbc33-d6ec-11ea-b443-fa163e429e83";
//	
//	public static final String BANK_TRANSFER_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
//	public static final String WALLET_TRANSFER_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/{solution}";
//	public static final String DISBURSE_STATUS_QUERY_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/query";
//	public static final String ADD_FUND_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/account/credit";
//	public static final String CLAIM_BACK_FUND_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/account/debit";
//	public static final String FUNDING_DISBURSAL_ACCOUNTLIST_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/account/debit";
//	public static final String BANK_ACOUNT_VALIDATION_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/beneficiary/validate";
//	public static final String ACCOUNT_PASSBOOK_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v1/account/statement";
//	public static final String ORDER_LIST_API_URL = "https://staging-dashboard.paytm.com/bpay/api/v2/report";

	/* for Production */
	
//	public static final String MERCHANT_ID = "TheCos13225251263900";
//	public static final String MERCHANT_KEY = "U4WFesuJIBleYX09";
//	public static final String SUB_WALLET_GUID = "2ea6ee3b-456c-4ff0-9b4d-402a4e2044f1";
	
	
	
	public static final String MERCHANT_ID = "Bharti44507436910086";
	public static final String MERCHANT_KEY = "33L_CazCAdGH_w6!";
	public static final String SUB_WALLET_GUID = "1fcf388f-bfd7-426f-b7c3-f836fa404e89";
	
	public static final String ORDER_LIST_API_URL = "https://dashboard.paytm.com/bpay/api/v2/report";
	public static final String ACCOUNT_PASSBOOK_API_URL = "https://dashboard.paytm.com/bpay/api/v1/account/statement";
	public static final String BANK_ACOUNT_VALIDATION_API_URL = "https://dashboard.paytm.com/bpay/api/v1/beneficiary/validate";
	public static final String FUNDING_DISBURSAL_ACCOUNTLIST_API_URL = "https://dashboard.paytm.com/bpay/api/v1/account/debit";
	public static final String CLAIM_BACK_FUND_API_URL = "https://dashboard.paytm.com/bpay/api/v1/account/debit";
	public static final String ADD_FUND_API_URL = "https://dashboard.paytm.com/bpay/api/v1/account/credit";
	public static final String DISBURSE_STATUS_QUERY_URL = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/query";
	public static final String WALLET_TRANSFER_URL = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/{solution}";
	public static final String BANK_TRANSFER_URL = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
	
	
	

	public String bankTransfer(String subwalletGuid, String orderId, String beneficiaryAccount, String beneficiaryIFSC, String amount, String purpose, String date, String transferMode) throws Exception {
		JSONObject paytmParams = new JSONObject();
		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);// "28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("orderId", orderId);//"ORDERID_98765");
		paytmParams.put("beneficiaryAccount", beneficiaryAccount);//"918008484891");
		paytmParams.put("beneficiaryIFSC", beneficiaryIFSC );//"PYTM0123456");
		paytmParams.put("amount", amount);//"1.00");
		paytmParams.put("purpose", purpose);//"SALARY_DISBURSEMENT");
		paytmParams.put("date", date);//"2020-06-01");
		paytmParams.put("transferMode", transferMode);
		String post_data = paytmParams.toString();

		String checksum = PaytmChecksum.generateSignature(post_data, MERCHANT_KEY);

		String x_mid = MERCHANT_ID;
		String x_checksum = checksum;

		
		URL url = new URL(BANK_TRANSFER_URL);


		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", x_mid);
			connection.setRequestProperty("x-checksum", x_checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String walletTransferApi(String subwalletGuid, String orderId, String beneficiaryPhoneNo, String amount) throws Exception {

		JSONObject paytmParams = new JSONObject();
		paytmParams.put("subwalletGuid",  SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("orderId", orderId);//"ORDERID_98765");
		paytmParams.put("beneficiaryPhoneNo", beneficiaryPhoneNo);//"55555666667");
		paytmParams.put("amount", amount);//"1.00");

		String post_data = paytmParams.toString();

		String checksum = PaytmChecksum.generateSignature(post_data, MERCHANT_KEY);

		String x_mid = MERCHANT_ID;
		String x_checksum = checksum;

		/*
		 * Solutions offered are: food, gift, gratification, loyalty, allowance,
		 * communication
		 */

		URL url = new URL(WALLET_TRANSFER_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", x_mid);
			connection.setRequestProperty("x-checksum", x_checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;

	}

	public String disburseStatusQueryAPI(String orderId) throws Exception {

		JSONObject paytmParams = new JSONObject();
		paytmParams.put("orderId", orderId);//"ORDERID_98765");

		String post_data = paytmParams.toString();

		String checksum = PaytmChecksum.generateSignature(post_data, MERCHANT_KEY);

		String x_mid = MERCHANT_ID;
		String x_checksum = checksum;

		URL url = new URL(DISBURSE_STATUS_QUERY_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", x_mid);
			connection.setRequestProperty("x-checksum", x_checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String addFundAPI(String subwalletGuid, String amount) throws Exception {

		JSONObject paytmParams = new JSONObject();

		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("amount", amount);//"1.00");

		
		String checksum = PaytmChecksum.generateSignature(paytmParams.toString(), MERCHANT_KEY);

		String post_data = paytmParams.toString();


		URL url = new URL(ADD_FUND_API_URL);


		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", MERCHANT_ID);
			connection.setRequestProperty("x-checksum", checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String claimBackFundAPI(String subwalletGuid, String amount) throws Exception {

		JSONObject paytmParams = new JSONObject();

		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("amount", amount);//"1.00");

		String checksum = PaytmChecksum.generateSignature(paytmParams.toString(), MERCHANT_KEY);

		String post_data = paytmParams.toString();

		URL url = new URL(CLAIM_BACK_FUND_API_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", MERCHANT_ID);
			connection.setRequestProperty("x-checksum", checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String fundingDisbursalAccountListAPI(String subwalletGuid, String amount) throws Exception {

		JSONObject paytmParams = new JSONObject();

		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("amount", amount);//"1.00");

		String checksum = PaytmChecksum.generateSignature(paytmParams.toString(), MERCHANT_KEY);

		String post_data = paytmParams.toString();

		URL url = new URL(FUNDING_DISBURSAL_ACCOUNTLIST_API_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", MERCHANT_ID);
			connection.setRequestProperty("x-checksum", checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String bankAcountValidationAPI(String orderId, String subwalletGuid, String beneficiaryAccount, String beneficiaryIFSC) throws Exception {

		JSONObject paytmParams = new JSONObject();
		paytmParams.put("orderId", orderId);//"ORDERID_98765");
		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("beneficiaryAccount", beneficiaryAccount);//"918008484891");
		paytmParams.put("beneficiaryIFSC", beneficiaryIFSC);//"PYTM0123456");

		String checksum = PaytmChecksum.generateSignature(paytmParams.toString(), MERCHANT_KEY);

		String post_data = paytmParams.toString();

		URL url = new URL(BANK_ACOUNT_VALIDATION_API_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", MERCHANT_ID);
			connection.setRequestProperty("x-checksum", checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String accountPassbookAPI(String subwalletGuid, String fromDate, String toDate) throws Exception {

		JSONObject paytmParams = new JSONObject();
		paytmParams.put("subwalletGuid", subwalletGuid );//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("fromDate", fromDate);//"2020-01-20");
		paytmParams.put("toDate", toDate);//"2020-02-20");

		String post_data = paytmParams.toString();

		String checksum = PaytmChecksum.generateSignature(post_data, MERCHANT_KEY);

		String x_mid = MERCHANT_ID;
		String x_checksum = checksum;

		URL url = new URL(ACCOUNT_PASSBOOK_API_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", x_mid);
			connection.setRequestProperty("x-checksum", x_checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public String orderListAPI(String subwalletGuid, String fromDate, String toDate) throws Exception {

		JSONObject paytmParams = new JSONObject();
		paytmParams.put("subwalletGuid", SUB_WALLET_GUID);//"28054249-XXXX-XXXX-af8f-fa163e429e83");
		paytmParams.put("fromDate", fromDate);//"2020-01-20");
		paytmParams.put("toDate", toDate);//"2020-02-20");

		String post_data = paytmParams.toString();

		String checksum = PaytmChecksum.generateSignature(post_data, MERCHANT_KEY);

		String x_mid = MERCHANT_ID;
		String x_checksum = checksum;

		URL url = new URL(ORDER_LIST_API_URL);

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("x-mid", x_mid);
			connection.setRequestProperty("x-checksum", x_checksum);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(post_data);
			requestWriter.close();
			String responseData = "";
			InputStream is = connection.getInputStream();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response: " + responseData);
			}
			responseReader.close();
			System.out.println(responseData);
			return responseData;
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) {
		PayoutMasterDMT pay = new PayoutMasterDMT();
		//PayoutMaster pay = new PayoutMaster();
		try {
//		String response1 = pay.bankTransfer("", "TestOrder5", "30000100006987", "BARB0GNOIDA","1.00", "OTHERS", "2020-7-15","IMPS");
//System.out.println(response1);
		//		String response1 = pay.orderListAPI("", "2020-07-15", "2020-07-15");
		
		//String response = pay.disburseStatusQueryAPI("AS1BPI0215155");

		String response = pay.disburseStatusQueryAPI("TestOrder5");
		System.out.println("Response "+response);
		JSONObject jsonObject = new JSONObject(response);
		String status =  jsonObject.getString("status");
		String statusCode = jsonObject.getString("statusCode");
		String statusMessage = jsonObject.getString("statusMessage");
		JSONObject jsonOb = jsonObject.getJSONObject("result");
		String paytmOrderId = jsonOb.getString("paytmOrderId");
		String rrn = jsonOb.getString("rrn");
		
		System.out.println(status);
		System.out.println(statusCode);
		System.out.println(statusMessage);
		System.out.println(jsonOb);
		System.out.println(paytmOrderId);
		System.out.println(rrn);
		
//		{"status":"SUCCESS","statusCode":"DE_001","statusMessage":"Successful disbursal to Bank Account is done",
//		"result":{"mid":"Bharti44507436910086","orderId":"AS1BPI0215155","paytmOrderId":"202007201237564114633733","amount":"1.00","commissionAmount":"3.00","tax":"0.54","rrn":"020212909933","beneficiaryName":null,"isCachedData":null,"cachedTime":null}}
		//String response1 = pay.orderListAPI("", "2020-07-15", "2020-07-15");
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
}

