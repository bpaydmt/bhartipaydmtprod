package com.bhartipay.mudra.bank2;



import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bank.bean.ResponseBeanBank;
import com.bhartipay.mudra.bank2.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank2.persistence.MudraDaoBankImpl;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.DMTReportInOut;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.KycConfigBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderPanDetails;
import com.bhartipay.mudra.bean.SenderFavouriteBean;
import com.bhartipay.mudra.bean.SenderKYCBean;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.transaction.bean.RefundTransactionBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.kyc.Utils.Constants;
import com.bhartipay.wallet.kyc.Utils.DecryptionBase64;
import com.bhartipay.wallet.kyc.Utils.ResponseXml;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/MudraManagerBK2")
public class MudraManagerBank {
	public static final String SERVICE_FROM="AADHARSHILA";
	private static final Logger logger = Logger.getLogger(MudraManagerBank.class.getName());
	MudraDaoBank mudraDao=new MudraDaoBankImpl();
	CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	public static String ekycuid = "";	
	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	//MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
	MudraBeneficiaryBank mudraBeneficiaryMastBean=new MudraBeneficiaryBank();
	@POST
	@Path("/validateSender")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank validateSender(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest(mudraSenderMastBean.getAgent(),imei,serverName,"validateSender");
		 
		setLogRequestId(requestId, "validateSender",SERVICE_FROM);  
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName + "RequestId****"+requestId+ "|validateSender()");
		
		mudraSenderMastBean.setIpiemi(imei);
		mudraSenderMastBean.setAgent(agent);
		return mudraDao.validateSender(mudraSenderMastBean,serverName,requestId);
		
	}
	
	
	@POST
	@Path("/registerSender")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank registerSender(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest(mudraSenderMastBean.getAgentId(),imei,serverName,"registerSender");
		
		setLogRequestId(requestId, "registerSender",SERVICE_FROM);   
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName + "RequestId****"+requestId+ "registerSender()");
		
		//logger.info("RequestId****"+requestId+"*****Start excution ******************************************************* method registerSender(mudraSenderMastBean)");
		mudraSenderMastBean.setIpiemi(imei);
		mudraSenderMastBean.setAgent(agent);
		return mudraDao.registerSender(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public BankDetailsBean getBankDetails(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,BankDetailsBean bankDetailsBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("BenAccNo-"+bankDetailsBean.getBeneficiaryAccountNo(),imei,serverName,"getBankDetails");
		
		setLogRequestId(requestId, "getBankDetails",SERVICE_FROM);   
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName + "RequestId*********"+requestId+"getBankDetails()"
				  );
		
		//logger.info("RequestId*********"+requestId+"******Start excution ******************************************************* method getBankDetails(bankDetailsBean)");
		return mudraDao.getBankDetails(bankDetailsBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDetailsByIFSC")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getBankDetailsByIFSC(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,BankDetailsBean bankDetailsBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String ifscCode="";
		if(bankDetailsBean.getIfscCode() != null)
		{
			ifscCode=bankDetailsBean.getIfscCode();
		}
		String requestId=mudraDao.saveDMTApiRequest("IFSC-"+ifscCode,imei,serverName,"getBankDetailsByIFSC");
		setLogRequestId(requestId, "getBankDetailsByIFSC",SERVICE_FROM);  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName + "*****RequestId******"+requestId+ "|getBankDetailsByIFSC()");
		
		//logger.info("*****RequestId******"+requestId+"*Start excution ******************************************************* method getBankDetailsByIFSC(bankDetailsBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getBankDetailsByIFSC(bankDetailsBean,serverName,requestId));
		//return mudraDao.getBankDetailsByIFSC(bankDetailsBean,request.getServletContext().getInitParameter("serverName"));
	}
	

	@POST
	@Path("/registerBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank registerBeneficiary(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),imei,serverName,"registerBeneficiary");
		setLogRequestId(requestId, "registerBeneficiary",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName + "*****RequestId******"+requestId+"|registerBeneficiary()");
		
		//logger.info("*****RequestId******"+requestId+"*Start excution ******************************************************* method registerBeneficiary(mudraBeneficiaryMastBean)");
		mudraBeneficiaryMastBean.setIpiemi(imei);
		mudraBeneficiaryMastBean.setAgent(agent);
		return mudraDao.registerBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	
	@POST
	@Path("/initiateDMTRefundByAgent")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut initiateDMTRefundByAgent(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,DMTReportInOut dmtReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+dmtReportInOut.getSenderid(),imei,serverName,"initiateDMTRefundByAgent");
		setLogRequestId(requestId, "initiateDMTRefundByAgent",SERVICE_FROM); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtReportInOut.getSenderid(),dmtReportInOut.getTxnid(), "", dmtReportInOut.getSenderid(), "",serverName + "*****RequestId******"+requestId+"|initiateDMTRefundByAgent()");
		return mudraDao.initiateDMTRefundByAgent(dmtReportInOut,serverName,requestId);
	
	}
	
	
	@POST
	@Path("/forgotMPIN")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank forgotMPIN(MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraSenderMastBean.getId(),"",serverName,"forgotMPIN");
		
		setLogRequestId(requestId, "forgotMPIN",SERVICE_FROM);   
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName +"*****RequestId******"+requestId+"|forgotMPIN()");
		
		//logger.info("*****RequestId******"+requestId+"****Start excution ******************************************************* method forgotMPIN(mudraSenderMastBean)");
		return mudraDao.forgotMPIN(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/updateMPIN")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank updateMPIN(MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Aggreatorid-"+mudraSenderMastBean.getAggreatorId(),"",serverName,"updateMPIN");
		setLogRequestId(requestId, "updateMPIN",SERVICE_FROM);   
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName +"*****RequestId******"+requestId+"| updateMPIN(mudraSenderMastBean)");
		
		//logger.info("*****RequestId******"+requestId+"****Start excution ******************************************************* method updateMPIN(mudraSenderMastBean)");
		return mudraDao.updateMPIN(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/otpResend")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String otpResend(WalletBean walletBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Aggreatorid-"+walletBean.getAggreatorid(),"",serverName,"otpResend");
		
		setLogRequestId(requestId, "otpResend",SERVICE_FROM);      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), "", "",serverName +"*****RequestId******"+requestId+"|otpResend()");
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(), walletBean.getUserId(), mudraBeneficiaryMastBean.getSenderId(), "",serverName +"*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getAggreatorid());
		
		//logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getUserId());
		//logger.info("*****RequestId******"+requestId+"*Start excution ===================== method otpResend(walletBean)"+walletBean.getAggreatorid());
		return ""+mudraDao.otpSenderResend(walletBean.getUserId(),walletBean.getAggreatorid(),serverName,requestId);
	}
	
	
	@POST
	@Path("/deleteBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank deleteBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deleteBeneficiary");
		setLogRequestId(requestId, "deleteBeneficiary",SERVICE_FROM);   
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"*****RequestId******"+requestId+"|deleteBeneficiary()");
		
		//logger.info("*****RequestId******"+requestId+"****Start excution ******************************************** method deleteBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());
		return mudraDao.deleteBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	
	@POST
	@Path("/acceptBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank acceptBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"acceptBeneficiary");
		setLogRequestId(requestId, "acceptBeneficiary",SERVICE_FROM);    
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +" method acceptBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());
		
		//logger.info("*********Start excution ******************************************** method acceptBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());
		return mudraDao.acceptBeneficiary(mudraBeneficiaryMastBean,serverName,"");
	}
	
	@POST
	@Path("/rejectBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank rejectBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"rejectBeneficiary");
		setLogRequestId(requestId, "rejectBeneficiary",SERVICE_FROM); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"|rejectBeneficiary()|"+mudraBeneficiaryMastBean.getId());
		
		//logger.info("*****RequestId**********Start excution ******************************************** method rejectBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());
		return mudraDao.rejectBeneficiary(mudraBeneficiaryMastBean,serverName,"");
	}
	
	@POST
	@Path("/setFavourite")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SenderFavouriteBean setFavourite(SenderFavouriteBean senderFavouriteBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+senderFavouriteBean.getSenderId(),"",serverName,"setFavourite");
		setLogRequestId(requestId, "setFavourite",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "",serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+"RequestId******"+requestId+"|setFavourite()|"+senderFavouriteBean.getAgentId()+"|"+senderFavouriteBean.getMobileNo());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", senderFavouriteBean.getSenderId(), "",serverName +"RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getSenderId());
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "",serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+"RequestId******"+requestId+"|Method Name :"+methodname+"|"+senderFavouriteBean.getMobileNo());
		
		//logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getAgentId());
		//logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getSenderId());
		//logger.info("RequestId******"+requestId+"*******Start excution ************************************* setFavourite method**:"+senderFavouriteBean.getMobileNo());
		return mudraDao.setFavourite(senderFavouriteBean,serverName,requestId);
	}
	
	@POST
	@Path("/deleteFavourite")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SenderFavouriteBean deleteFavourite(SenderFavouriteBean senderFavouriteBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+senderFavouriteBean.getSenderId(),"",serverName,"deleteFavourite");
		setLogRequestId(requestId, "deleteFavourite",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", senderFavouriteBean.getSenderId(), "",serverName +"RequestId******"+requestId+ "|deleteFavourite()|"+senderFavouriteBean.getId());
		
		//logger.info("RequestId******"+requestId+"*******Start excution ************************************* deleteFavourite method**:"+senderFavouriteBean.getId());
		return mudraDao.deleteFavourite(senderFavouriteBean,serverName,requestId);
	}
	
	@POST
	@Path("/getFavouriteList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getFavouriteList(MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraSenderMastBean.getAgentId(),"",serverName,"getFavouriteList");
		setLogRequestId(requestId, "getFavouriteList",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName +"RequestId******"+requestId+"|getFavouriteList()|" +mudraSenderMastBean.getAgentId());
		
		//logger.info("RequestId******"+requestId+"*******Start excution ===================== method getFavouriteList()" +mudraSenderMastBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getFavouriteList(mudraSenderMastBean.getAgentId(),serverName,requestId));
		
	}
	
	@POST
	@Path("/calculateSurcharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+surchargeBean.getAgentId(),"",serverName,"calculateSurcharge");
		setLogRequestId(requestId, "calculateSurcharge",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"RequestId******"+requestId+"|calculateSurcharge()|"+surchargeBean.getAgentId() );
		
		//logger.info("RequestId******"+requestId+"*******Start excution ********************************************* method calculateSurcharge()" );
		return mudraDao.calculateSurcharge(surchargeBean,serverName,requestId);
	}
	
	
	/*@POST
	@Path("/mudraMoneyTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean mudraMoneyTransfer(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		logger.info("Start excution ********************************************* method mudraMoneyTransfer()" );
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setUseragent(agent);
		return mudraDao.mudraMoneyTransfer(mudraMoneyTransactionBean,request.getServletContext().getInitParameter("serverName"));
	}*/
	
	
	@POST
	@Path("/verifyAccount")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean verifyAccount(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"verifyAccount");
		setLogRequestId(requestId, "verifyAccount",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"RequestId****"+requestId+"|verifyAccount() "+"|Agentid:"+mudraMoneyTransactionBean.getAgentid() );
	
		//logger.info("RequestId****"+requestId+"*****Start excution ********************************************* method verifyAccount()" );
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.verifyAccount(mudraMoneyTransactionBean,serverName,requestId);
	}
	
	
	@POST
	@Path("/fundTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public FundTransactionSummaryBean fundTransfer(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"fundTransfer");
		setLogRequestId(requestId, "fundTransfer",SERVICE_FROM); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(),mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"RequestId***"+requestId+"|fundTransfer()|"+mudraMoneyTransactionBean.getAgentid());
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.fundTransfer(mudraMoneyTransactionBean,serverName,requestId);
	}
		
	
	
	@POST
	@Path("/getTransactionDetailsByTxnId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public FundTransactionSummaryBean getTransactionDetailsByTxnId(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentId-"+mudraMoneyTransactionBean.getAgentid(),imei,serverName,"fundTransfer");
		setLogRequestId(requestId, "getTransactionDetailsByTxnId",SERVICE_FROM); 
	      
		//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(),mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"RequestId***"+requestId+"|fundTransfer()|"+mudraMoneyTransactionBean.getAgentid());
		
		mudraMoneyTransactionBean.setIpiemi(imei);
		mudraMoneyTransactionBean.setAgent(agent);
		return mudraDao.getTransactionDetailsByTxnId(mudraMoneyTransactionBean,serverName,requestId);
	}
	
	@POST
	@Path("/calculateSurCharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean mudraMoneyTransactionBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Amount-"+mudraMoneyTransactionBean.getTxnAmount(),"",serverName,"calculateSurCharge");
		setLogRequestId(requestId, "calculateSurCharge",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"|"+requestId+"");
		
		//logger.info("RequestId*****"+requestId+"******Start excution ******************************220*************** method calculateSurCharge()" );
		return mudraDao.calculateSurCharge(mudraMoneyTransactionBean,serverName,requestId);

	}
	
	@POST
	@Path("/getBeneficiaryListForImport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank getBeneficiaryListForImport(MudraSenderBank mudraSenderMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AggreatorId-"+mudraSenderMastBean.getAggreatorId(),"",serverName,"getBeneficiaryListForImport");
		setLogRequestId(requestId, "getBeneficiaryListForImport",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "", "", "",serverName +"RequestId*****"+requestId+"|getBeneficiaryListForImport()" );
		
		//logger.info("RequestId*****"+requestId+"*******Start excution ********************************************* method getBeneficiaryListForImport()" );
		return mudraDao.getBeneficiaryListForImport(mudraSenderMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/copyBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank copyBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"copyBeneficiary");
		setLogRequestId(requestId, "copyBeneficiary",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"RequestId******"+requestId+"|copyBeneficiary()");
		
		//logger.info("RequestId******"+requestId+"*********Start excution ********************************************* method copyBeneficiary()" );
		return mudraDao.copyBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/activeBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank activeBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"activeBeneficiary");
		setLogRequestId(requestId, "activeBeneficiary",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"RequestId****"+requestId+"|activeBeneficiary()");
		
		//logger.info("RequestId****"+requestId+"*******Start excution ********************************************* method activeBeneficiary()" );
		return mudraDao.activeBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/deletedBeneficiary")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public MudraSenderBank deletedBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
	  
	  String serverName=request.getServletContext().getInitParameter("serverName");
	  String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deletedBeneficiary");
	  setLogRequestId(requestId, "deletedBeneficiary",SERVICE_FROM); 
      
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"RequestId****"+requestId+"|deletedBeneficiary()");
	  
	 // logger.info("RequestId****"+requestId+"******Start excution ********************************************* method deletedBeneficiary()" );
	  return mudraDao.deletedBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	 }
	
	@POST
	@Path("/deActiveBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderBank deActiveBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deActiveBeneficiary");
		 setLogRequestId(requestId, "deActiveBeneficiary",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"RequestId****"+requestId+"|deActiveBeneficiary()");
		
		//logger.info("RequestId****"+requestId+"******Start excution ********************************************* method deActiveBeneficiary()" );
		return mudraDao.deActiveBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	}
	
	@POST
	@Path("/getBankDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getBankDtl(@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("Bank-","",serverName,"getBankDtl");
		setLogRequestId(requestId, "getBankDtl",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName +"RequestId*******"+requestId+"|getBankDtl()" );
		
		//logger.info("RequestId*******"+requestId+"*****Start excution ********************************************* method getBankDtl()" );
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(mudraDao.getBankDtl(serverName,requestId));
	}
	
	
	@POST
	@Path("/getTransacationLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("TxnLedger-","",serverName,"getTransacationLedgerDtl");
		setLogRequestId(requestId, "getTransacationLedgerDtl",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName +"RequestId*****"+requestId+"|getTransacationLedgerDtl()|"+dMTReportInOut.getAgentId() );
		
		//logger.info("RequestId*****"+requestId+"******Start excution ********************************************* method getTransacationLedgerDtl()" );
		return mudraDao.getTransacationLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	
	@POST
	@Path("/getAgentLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getAgentLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentLedger-","",serverName,"getAgentLedgerDtl");
		setLogRequestId(requestId, "getAgentLedgerDtl",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName +"RequestId***"+requestId+"|getAgentLedgerDtl() |"+dMTReportInOut.getAgentId() );
		
		//logger.info("RequestId***"+requestId+"*******Start excution ********************************************* method getAgentLedgerDtl()" );
		return mudraDao.getAgentLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	
	@POST
	@Path("/getSenderLedgerDtl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public DMTReportInOut getSenderLedgerDtl(DMTReportInOut dMTReportInOut,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("AgentLedger-","",serverName,"getSenderLedgerDtl");
		setLogRequestId(requestId, "getSenderLedgerDtl",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "",serverName +"RequestId*****"+requestId+"|getSenderLedgerDtl()|"+dMTReportInOut.getAgentId() );
		
		//logger.info("RequestId*****"+requestId+"******Start excution ********************************************* method getSenderLedgerDtl()" );
		return mudraDao.getSenderLedgerDtl(dMTReportInOut,serverName,requestId);
	}
	
	@POST
	@Path("/getBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraBeneficiaryBank getBeneficiary(MudraBeneficiaryBank mudraBeneficiaryMastBean,@Context HttpServletRequest request){
		//added for logger on 3rd Aug
		String serverName=request.getServletContext().getInitParameter("serverName");
		String requestId=mudraDao.saveDMTApiRequest("bene id-"+mudraBeneficiaryMastBean.getId(),"",serverName,"getBeneficiary");
		setLogRequestId(requestId, "getBeneficiary",SERVICE_FROM); 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "",serverName +"serverName*****"+serverName+"|getBeneficiary()" );
		
		//logger.info("serverName*****"+serverName+"******Start excution ********************************************* method getBeneficiary()" );
		return mudraDao.getBeneficiary(mudraBeneficiaryMastBean.getId(),serverName);
	}
	
	@POST
	@Path("/uploadSenderPanF60")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String userAgent,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		mudraSenderPanDetails.setIpiemi(ipiemi);
		mudraSenderPanDetails.setUserAgent(userAgent);
		String requestId=mudraDao.saveDMTApiRequest("agentid id-"+mudraSenderPanDetails.getId(),"",serverName,"uploadSenderPanF60");
		setLogRequestId(requestId, "uploadSenderPanF60",SERVICE_FROM);
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", "", "",serverName +"serverName*****"+serverName+"|uploadSenderPanF60()");
		//logger.info("serverName*****"+serverName+"******Start excution ********************************************* method uploadSenderPanF60()" );
		return mudraDao.uploadSenderPanF60(mudraSenderPanDetails,serverName);
	}
	
	@POST
	@Path("/getPendingPanReq")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		
		String requestId=mudraDao.saveDMTApiRequest("agg id-"+mudraSenderPanDetails.getId(),"",serverName,"getPendingPanReq");
		setLogRequestId(requestId, "getPendingPanReq",SERVICE_FROM);  
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", mudraSenderPanDetails.getSenderId(), "",serverName +"serverName*****"+serverName+"|getPendingPanReq()");
		
		//logger.info("serverName*****"+serverName+"******Start excution ********************************************* method getPendingPanReq()" );
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(mudraDao.getPendingPanReq(mudraSenderPanDetails,serverName));
		
		
	}
	
	
	@POST
	@Path("/rejectAcceptedSenderPan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	 public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails,@Context HttpServletRequest request){
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"", "", "", "",serverName +"serverName*****"+serverName+"|rejectAcceptedSenderPan()");
			
		// logger.info("serverName*****"+serverName+"******Start excution ********************************************* method rejectAcceptedSenderPan()" );
		 return mudraDao.rejectAcceptedSenderPan(mudraSenderPanDetails,serverName);
	 }
	
	public String getLogic(String xmlstring,String slk, String deviceId, String stack, String rrn, String backupurl, String ServiceType,String UniversalUniqueIdentifier ) 
	 {    
	  String encodedFinalString = "";   
	  byte[] finaldata = null;   
	  StringBuffer xmlStr = null;    
	  try {    
	   xmlStr = new StringBuffer();     
	   xmlStr.append("<"+xmlstring+">").append("<SLK>").append(slk).append("</SLK>").append("<SERVICETYPE>").append(ServiceType).append("</SERVICETYPE>").append("<RRN>").append(rrn).append("</RRN>").append("<DEVICEID>").append(deviceId).append("</DEVICEID>").append("<STACK>").append(stack).append("</STACK>"). append("<UUID>").append(UniversalUniqueIdentifier).append("</UUID>").append("<REDIRECTURL>").append(backupurl).append("</REDIRECTURL>").append("<RFKEY>").append("").append("</RFKEY>").append("</"+xmlstring+">");     
	   System.out.println("xmlStr"+xmlStr);
	   finaldata = xmlStr.toString().getBytes();
	   System.out.println( finaldata);   
	   } catch (Exception ext) {    
	    ext.printStackTrace();   
	    }   
	  
	  encodedFinalString = new String(org.apache.commons.codec.binary.Base64.encodeBase64(finaldata));   
	  
	  return encodedFinalString;  
	    }
	 
	 
	 @POST
	 @Path("/eKYCResponse")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public EkycResponseBean eKYCResponse(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,EkycResponseBean ekycResponseBean,@Context HttpServletRequest request){
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"Create kyc log");
			
		// logger.info("Create kyc log");
	     mudraDao.saveSenderKycResponse(ekycResponseBean);
	     return ekycResponseBean;
	   
	 }
	 
	 @POST
	 @Path("/sendereKYC")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public KycConfigBean sendereKYC(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,SenderKYCBean SenderKYCBean,@Context HttpServletRequest request){
		 
	      
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "", "|sendereKYC()|"+"sender KYC configuration call ");
			
	   //logger.info("sender KYC configuration call method getkycConfig");
	   
	   
	   TreeMap<String,String> obj=new TreeMap<String,String>();
	    obj.put("userId", SenderKYCBean.getAgentid());
	    obj.put("agentid", SenderKYCBean.getAgentid());
	       obj.put("aggreatorid",SenderKYCBean.getAggreatorid());
	       obj.put("servicetype",SenderKYCBean.getServicetype());
	       obj.put("mobileno",SenderKYCBean.getMobileno());
	       obj.put("CHECKSUMHASH",SenderKYCBean.getCHECKSUMHASH());
	       
	     int statusCode= WalletSecurityUtility.checkSecurity(obj);
	     if(statusCode!=1000){
	      KycConfigBean kyBean=new KycConfigBean();
	      kyBean.setStatusCode(""+statusCode);
	      kyBean.setStatusDesc("Security error.");
	      return kyBean;
	     }
	     
	     WalletUserDao userDao=new WalletUserDaoImpl();
	     String kycStatus=userDao.checkUserKycStatus(SenderKYCBean.getAgentid());
	     if(kycStatus!=null&&kycStatus.equalsIgnoreCase("active")){
	      KycConfigBean kyBean=new KycConfigBean();
	      kyBean.setStatusCode("300");
	      kyBean.setStatusDesc("Already kyc done.");
	     return kyBean;
	  }
	   
	   
	   KycConfigBean kycconfigbean=mudraDao.getkycConfig(SenderKYCBean,SenderKYCBean.getServicetype());
	      
	   if(kycconfigbean.getStatusCode().equals("1000")){
	   if (agent.toLowerCase().indexOf("andriod")>0)
	    kycconfigbean.setStack("02");
	   else 
	    kycconfigbean.setStack("01");
	  
	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");  
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),kycconfigbean.getAggreator(),"", "", "", "","Create kyc timestamp value yyyyMMddhhmmssSSS"+dateFormat);
			
	       // logger.info("Create kyc timestamp value yyyyMMddhhmmssSSS"+dateFormat);
	        
	        String time = dateFormat.format(new Date());   // params from encodedFinalString  
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),kycconfigbean.getAggreator(),"", "", "", "","generate unique reference number");
			
	       // logger.info("generate unique reference number");
	        
	     String kyctrxId=commanUtilDao.getTrxId("KYCT",kycconfigbean.getAggreator());
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),kycconfigbean.getAggreator(),"", "", "", ""," unique reference number for kyc transaction :"+kyctrxId);
			
	    // logger.info(" unique reference number for kyc transaction :"+kyctrxId);
	     String callBackUrl="";
	     if(SenderKYCBean.getRequestFrom()!=null&&SenderKYCBean.getRequestFrom().equalsIgnoreCase("APP")){
	    	 callBackUrl=kycconfigbean.getCallbackurl()+"/kycresponseAndriod";
	     }else{
	    	 callBackUrl=kycconfigbean.getCallbackurl()+"/kycresponse";
	     }
	     kycconfigbean.setAuthxml(getLogic(kycconfigbean.getXmlstring(), kycconfigbean.getSlk(),kycconfigbean.getDeviceid(),kycconfigbean.getStack(),kyctrxId+"-"+time,callBackUrl,kycconfigbean.getServicevalue(),""));
	     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),kycconfigbean.getAggreator(),"", "", "", "","save partial information to DB ref:"+kyctrxId);
			
	     //logger.info("save partial information to DB ref:"+kyctrxId);
	          
	     Boolean isvalid=mudraDao.setSenderKycMast(kycconfigbean, SenderKYCBean, kycconfigbean.getServicetype(),time,kyctrxId);
	     
	      }else{
	    	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),SenderKYCBean.getAggreatorid(),"", "", "", "","sender KYC configuration call method getkycConfig error code"+kycconfigbean.getStatusCode() );
	  		
	    	 // logger.info("sender KYC configuration call method getkycConfig error code"+kycconfigbean.getStatusCode() );
	   }
	   return kycconfigbean;
	   
	  
	 }
	    @POST
		@Path("/getResponseXml")
		@Produces({ MediaType.APPLICATION_JSON })
		@Consumes(MediaType.APPLICATION_XML)
		public String getResponseXml(@Context HttpServletRequest request,@Context HttpServletResponse response){
	    	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", "", "","Start excution ************************************ method getResponseXml()");
	  		
	    	//logger.info("Start excution ************************************ method getResponseXml()");
	    	//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", "", "","got responseXML");
	  		
	    	//System.out.println("got responseXML");
			String userAgent=request.getHeader("User-Agent");
			com.bhartipay.mudra.bean.EkycResponseBean ekycResponseBean = new com.bhartipay.mudra.bean.EkycResponseBean();
			
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
		      
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"getResponseXML");
	  		
			//System.out.println("getResponseXML");
			String resp = null;
			try {
				resp = convertStreamToString(request.getInputStream());
			} catch (IOException e1) {
				
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"getResponseXML"+resp);
	  		
			//System.out.println("getResponseXML"+resp);
			JAXBContext jaxbContext;
			/*
			 * Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			 * KYCRES1 que= (KYCRES1) jaxbUnmarshaller.;
			 */
			StringReader reader = new StringReader(resp);
			Unmarshaller unmarshaller;
			ResponseXml result = null;
			try {
				jaxbContext = JAXBContext.newInstance(ResponseXml.class);
				unmarshaller = jaxbContext.createUnmarshaller();
				result = (ResponseXml) unmarshaller.unmarshal(reader);

				String UUID = result.getUUID();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"UUID**"+UUID);
		  		
				//logger.debug("UUID************************************"+UUID);
		
				String servicetype = result.getSERVICETYPE();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"servicetype** "+servicetype);
		  		
				//logger.debug("servicetype************************************"+servicetype);
				
				String errCode = result.getERRCODE();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(),ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"errCode**"+errCode);
		  		
				//logger.debug("errCode************************************"+errCode);
				
				String dkey = result.getDKEY();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(),ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"dkey**"+dkey);
		  		
				//logger.debug("dkey************************************"+dkey);
				
				String rfkey = result.getRFKEY();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(),ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"rfkey**"+rfkey);
		  		
				//logger.debug("rfkey************************************"+rfkey);
				
				String status = result.getSTATUS();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"status**"+status);
		  		
				//logger.debug("status************************************"+status);
				
				String rrn = result.getRRN();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"rrn**"+rrn);
		  		
				//logger.debug("rrn************************************"+rrn);
				
				String errMsg = result.getERRMSG();
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"errMsg**"+errMsg);
		  		
				//logger.debug("errMsg************************************"+errMsg);
				
				String responseData = result.getRESPONSE();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"|UUID**"+UUID+"|servicetype** "+servicetype+"|errCode**"+errCode+"|dkey**"+dkey+"|rfkey**"+rfkey+"|status**"+status+"|rrn**"+rrn+"|errMsg**"+errMsg+"|responseData**"+responseData);
		  		
				//logger.debug("responseData************************************"+responseData);
				MudraDao mudradao=new MudraDaoImpl();

				if (status.equalsIgnoreCase("1")) {
					if (servicetype.equalsIgnoreCase("04") || servicetype.equalsIgnoreCase("05")
							|| servicetype.equalsIgnoreCase("06")) {

						DecryptionBase64 decrpt = new DecryptionBase64();
						FileInputStream fileInputStream = null;
						String p12Password = Constants.certificatePassword;
						try {
							Security.addProvider(new BouncyCastleProvider());

							FileInputStream file = new FileInputStream(Constants.certificatefilepath);

							KeyStore keystore = KeyStore.getInstance("PKCS12", "SunJSSE");
							keystore.load(file, p12Password.toCharArray());
							String alias = keystore.aliases().nextElement();
							PrivateKey privateKey = (PrivateKey) keystore.getKey(alias, p12Password.toCharArray());

							byte[] originalSessionKey = decrpt.decryptUsingPublicKey(  org.apache.commons.codec.binary.Base64.decodeBase64(result.getDKEY().getBytes()), privateKey);

							byte[] orgData = decrpt.decryptUsingSessionKey(originalSessionKey,org.apache.commons.codec.binary.Base64.decodeBase64(result.getRESPONSE().getBytes()));

							String kycResponseXml = new String(orgData);

							try {
								DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
								DocumentBuilder builder = factory.newDocumentBuilder();
								InputSource is = new InputSource(new StringReader(kycResponseXml));

								Document doc = builder.parse(is);

								String Rcode = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes()
										.getNamedItem("code") != null) {
									Rcode = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("code")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(),ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"Rcode**"+Rcode);
						  		
								//logger.debug("Rcode************************************"+Rcode);

								String Rtxn = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes()
										.getNamedItem("txn") != null) {
									Rtxn = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("txn")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"Rtxn**"+Rtxn);
						  		
								//logger.debug("Rtxn************************************"+Rtxn);

								
								String Rts = "";
								if (doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts") != null) {
									Rts = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"Rts**"+Rts);
						  		
								//logger.debug("Rts************************************"+Rts);
								
								String Uuid = "";
								if (doc.getElementsByTagName("UidData").item(0).getAttributes()
										.getNamedItem("uuid") != null) {
									Uuid = doc.getElementsByTagName("UidData").item(0).getAttributes().getNamedItem("uuid")
											.getTextContent();
									ekycuid = Uuid;
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"Uuid**"+Uuid);
						  		
								//logger.debug("Uuid************************************"+Uuid);
								
								String dob = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob") != null) {
									dob = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"dob**"+dob);
						  		
								//logger.debug("dob************************************"+dob);
								
								String gender = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes()
										.getNamedItem("gender") != null) {
									gender = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("gender")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"gender**"+gender);
						  		
								//logger.debug("gender************************************"+gender);

								String name = "";
								if (doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name") != null) {
									name = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"name**"+name);
						  		
								//logger.debug("name************************************"+name);

								String co = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co") != null) {
									co = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"co**"+co);
						  		
								//logger.debug("co************************************"+co);

								String country = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes()
										.getNamedItem("country") != null) {
									country = doc.getElementsByTagName("Poa").item(0).getAttributes()
											.getNamedItem("country").getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"dob**"+dob);
						  		
								//logger.debug("dob************************************"+dob);

								String dist = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist") != null) {
									dist = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"dist**"+dist);
						  		
								//logger.debug("dist************************************"+dist);

								String lm = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm") != null) {
									lm = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"lm**"+lm);
						  		
								//logger.debug("lm************************************"+lm);

								String loc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc") != null) {
									loc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"loc**"+loc);
						  		
								//logger.debug("loc************************************"+loc);

								String pc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc") != null) {
									pc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID()," pc**"+ pc);
						  		
								//logger.debug(" pc************************************"+ pc);

								String po = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po") != null) {
									po = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID()," po**"+ po);
						  		
								//logger.debug(" po************************************"+ po);

								String state = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state") != null) {
									state = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"state**"+state);
						  		
								
						  		
								//logger.debug("state************************************"+state);

								String vtc = "";
								if (doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc") != null) {
									vtc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc")
											.getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"vtc**"+vtc);
						  		
								//logger.debug("vtc************************************"+vtc);

								String pht = "";
								if (doc.getElementsByTagName("Pht") != null) {
									pht = doc.getElementsByTagName("Pht").item(0).getTextContent();
								}
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"pht**"+pht);
						  		
								//logger.debug("pht************************************"+pht);

								
								
								ekycResponseBean.setKyctxnid(rrn.substring(0,rrn.indexOf("-")));
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID()," RRN ="+ekycResponseBean.getKyctxnid());
						  		
								//System.out.println(" RRN ="+ekycResponseBean.getKyctxnid());
								
								

								ekycResponseBean=mudradao.getEKycSenderData(ekycResponseBean);
								
								ekycResponseBean.setUID(UUID);
								ekycResponseBean.setServicetype(servicetype);
								ekycResponseBean.setPht(pht);
								ekycResponseBean.setRCode(Rcode);
								
								ekycResponseBean.setRtxn(Rtxn);
								ekycResponseBean.setRts(Rts);
								ekycResponseBean.setUuid(Uuid);
								
								ekycResponseBean.setPoidob(dob);
								ekycResponseBean.setPoigender(gender);
								ekycResponseBean.setPoiname(name);
								ekycResponseBean.setPoaco(co);
								ekycResponseBean.setPoadist(dist);
								ekycResponseBean.setPoalm(lm);
								ekycResponseBean.setPoaloc(loc);
								ekycResponseBean.setPoapc(pc);
								ekycResponseBean.setPoapo(po);
								ekycResponseBean.setPoastate(state);
								ekycResponseBean.setPoavtc(vtc);
								ekycResponseBean.setStatus(status);

							} catch (ParserConfigurationException | SAXException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (servicetype.equalsIgnoreCase("01") || servicetype.equalsIgnoreCase("02")
							|| servicetype.equalsIgnoreCase("03")) {

						
						byte[] authresponse = org.apache.commons.codec.binary.Base64.decodeBase64(result.getRESPONSE().getBytes());

						// String authresponseXml = authresponse.toString();
						String authresponseXml = new String(authresponse);

						// DocumentBuilderFactory factory =
						// DocumentBuilderFactory.newInstance();
						try {
							/*
							 * DocumentBuilder builder =
							 * factory.newDocumentBuilder(); Document doc =
							 * builder.parse("");
							 */

							InputStream is = new ByteArrayInputStream(authresponseXml.getBytes());

							DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
							domFactory.setIgnoringComments(true);
							DocumentBuilder builder = domFactory.newDocumentBuilder();
							Document doc = builder.parse(is);
							String Rcode = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("code")
									.getTextContent();
							String Rtxn = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("txn")
									.getTextContent();
							String Rts = doc.getElementsByTagName("AuthRes").item(0).getAttributes().getNamedItem("ts")
									.getTextContent();

							
						} catch (ParserConfigurationException | SAXException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} else {
					ekycuid = "";
				}
				
				mudradao.saveSenderKycResponse(ekycResponseBean);
				// System.out.println("KYCResponse ::" result.);

			} catch (JAXBException e) {
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "problem" + e.getMessage()+" "+e);
				 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "200:ok";
		 }
	
	 
	 @POST
	 @Path("/getEKycSenderData")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public EkycResponseBean getEKycSenderData(EkycResponseBean ekycResponseBean){
			
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+ekycResponseBean.getAgentid(),"","","getEKycSenderData");
		 setLogRequestId(requestId, "getEKycSenderData",SERVICE_FROM);  
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"|getEKycSenderData()|"+ "Create kyc log");
		// logger.info("Create kyc log");
	     ekycResponseBean=mudraDao.getEKycSenderData(ekycResponseBean);
	     return ekycResponseBean;
	   
	 }
	 
	 
	 @POST
	 @Path("/initiateDMTRefund")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public List<RefundTransactionBean> initiateDMTRefund(DmtDetailsMastBean dmtMast){
		 
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+dmtMast.getAgentid(),"","","initiateDMTRefund");
		 setLogRequestId(requestId, "initiateDMTRefund",SERVICE_FROM);  
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"", "", dmtMast.getSenderid(), dmtMast.getTxnid(),"|initiateDMTRefund()|"+ "initiateDMTRefund log");
			
		 //logger.info( "initiateDMTRefund log");
	    List<RefundTransactionBean> mudra =mudraDao.initiateDMTRefund(dmtMast);
	     return mudra;
	   
	 }
	 
	 @POST
	 @Path("/markFailedDmtTxn")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String markFailedDmtTxn(DmtDetailsMastBean dmtMast){
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+dmtMast.getAgentid(),"","","markFailedDmtTxn");
		 setLogRequestId(requestId, "markFailedDmtTxn",SERVICE_FROM);  
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"", "", dmtMast.getSenderid(), dmtMast.getTxnid(),"|markFailedDmtTxn()|"+ "initiateDMTRefund log");
			
		 //logger.info( "initiateDMTRefund log");
	    String status =mudraDao.markFailedDmtTxn(dmtMast);
	    return status;
	   
	 }
	 @POST
	 @Path("/checkDMTStatus")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public DmtDetailsMastBean checkDMTStatus(DmtDetailsMastBean dmtMast){
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+dmtMast.getAgentid(),"","","checkDMTStatus");
		 setLogRequestId(requestId, "checkDMTStatus",SERVICE_FROM);  
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"", "", dmtMast.getSenderid(), dmtMast.getTxnid(),"|checkDMTStatus()|"+ "checkDMTStatus log");
			
		// logger.info("checkDMTStatus log");
	     dmtMast=mudraDao.checkDMTStatus(dmtMast);
	     return dmtMast;
	   
	 }
	 

	  @POST
	  @Path("/refundAgent")
	  @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	  @Consumes({MediaType.APPLICATION_JSON})
	  public String refundAgent(String senderDetail)
	  {
	   String respJson = null;
	   try
	   {
	    JSONObject json = new JSONObject(senderDetail);
	    String requestId=mudraDao.saveDMTApiRequest("mobile no-"+json.get("senderMobile"),"","","refundAgent");
	    setLogRequestId(requestId, "refundAgent",SERVICE_FROM);  
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :getAgentSenderBalance|Request:"+senderDetail);
	    ResponseBeanBank resp=mudraDao.refundAgent(json.get("userId").toString(),json.get("senderMobile").toString(),json.get("txnId").toString());
	    respJson = new Gson().toJson(resp);
	   }
	   catch(Exception ex)
	   {
	    logger.info("*******problem in refundAgent***e.message**"+ex.getMessage());
	    ex.printStackTrace();
	   }
	      return respJson;
	  }
	 
	 
	 @POST
	  @Path("/refundAgentRequest")
	  @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	  @Consumes({MediaType.APPLICATION_JSON})
	  public String refundAgentRequest(String senderDetail)
	  {
	   String respJson = null;
	   try
	   {
	    JSONObject json = new JSONObject(senderDetail);
	    String requestId=mudraDao.saveDMTApiRequest("mobile no-"+json.get("senderMobile"),"","","refundAgentRequest");
	    setLogRequestId(requestId, "refundAgentRequest",SERVICE_FROM);  
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "","", "","|Method Name :refundAgentRequest|Request:"+senderDetail);
	    ResponseBeanBank resp=mudraDao.refundAgentRequest(json.get("userId").toString(),json.get("senderMobile").toString(),json.get("mpin").toString(),json.get("txnId").toString());
	    respJson = new Gson().toJson(resp);
	   }
	   catch(Exception ex)
	   {
	    logger.info("*******problem in refundAgentRequest***e.message**"+ex.getMessage());
	    ex.printStackTrace();
	   }
	      return respJson;
	  }
	 
	 @POST
	 @Path("/getAgentSenderBalance")
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Consumes({MediaType.APPLICATION_JSON})
	 public String getAgentSenderBalance(MudraSenderBank mudraSenderMastBean)
	 {
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+mudraSenderMastBean.getAgentId(),"","","getAgentSenderBalance");
		 setLogRequestId(requestId, "getAgentSenderBalance",SERVICE_FROM);  
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderMastBean.getAggreatorId(),"", "","", "","|Method Name :getAgentSenderBalance");
		 HashMap resp=mudraDao.getAgentSenderBalance(mudraSenderMastBean);
		 String respJson = new Gson().toJson(resp);
	     return respJson;
	 }
	 
	 public void setLogRequestId(String requestId, String serviceName, String from) {
//		 {requestId} %X{serviceName} %X{from} 
		 MDC.put("requestId","-"+requestId);
		 MDC.put("serviceName","-"+serviceName);
		 MDC.put("from", "-"+from);
	 }

	 
	@POST
	@Path("/checkLimit")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public String checkLimit(WalletMastBean wallet){
		
		   
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", wallet.getMobileno() , wallet.getId(),"" +"RequestId******checkLimit()|");
		
		return mudraDao.checkLimit(wallet.getMobileno(),wallet.getId());
	}
		
		
	 
	 
	 public static void main(String[] args) {
		 MudraDaoBank mudraDao=new MudraDaoBankImpl();
		 String requestId=mudraDao.saveDMTApiRequest("agent id-"+null,"","","getAgentSenderBalance");
		 
		 
		System.out.println(requestId);
	}
}
