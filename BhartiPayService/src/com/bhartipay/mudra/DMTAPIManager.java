package com.bhartipay.mudra;

import java.util.HashMap;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.merchant.persistence.MerchantDaoImpl;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.MerchantDmtTransBean;
import com.bhartipay.mudra.bean.MerchantRequestBean;
import com.bhartipay.mudra.bean.MerchantResponseBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryMastBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.ResponseBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.BPJWTSignUtil;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 
 * @author santosh.kumar
 *
 */

@Path("/DistributorBhartipayApi")
public class DMTAPIManager {
	private static final Logger logger = Logger.getLogger(DMTAPIManager.class.getName());
	Gson gson = new Gson();
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	MudraDao mudraDao=new MudraDaoImpl();
	HashMap<String,Object> responsemap= new HashMap<String,Object>();

	
/**
 * 	
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/checkWalletExistRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean checkWalletExistRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	
	String serverName=request.getServletContext().getInitParameter("serverName");
	MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"checkWalletExistRequest");
	 
     
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|checkWalletExistRequest()|"
			
			+ merchantRequestBean.getAgentId()+"|After Saving request");
	//logger.info(serverName +"*****RequestId******"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	//logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|"+methodname+"|After Saving request"
			
			//);
	//logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
	
	merchantResponseBean.setRequestId(requestId);
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		//merchantResponseBean.setRequestId(requestId);
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		//merchantResponseBean.setRequestId(requestId);
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|mKey "
			
			+ mKey);
	// logger.info(serverName +"*****RequestId******"+requestId+"**************************************mKey*****"+mKey);
	
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Merchant. ");
		//merchantResponseBean.setRequestId(requestId);
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	 try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	 String mobileNo=claim.get("mobileNo").toString();
	 
	 if(!mobileValidation(mobileNo)){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "INVALID MOBILE NUMBER.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean;
	 }
	
	 mudraSenderMastBean.setMobileNo(mobileNo);
	 
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 mudraSenderMastBean=mudraDao.validateSender(mudraSenderMastBean,serverName,requestId);
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|StatusCode "
				
			+ mudraSenderMastBean.getStatusCode());
	 //logger.info(serverName+"*****RequestId******"+requestId+"************************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1104")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Mobile Number.");
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("2000")){
			responsemap.put("code", "300");
			responsemap.put("response", "SUCCESS");
			responsemap.put("mobileNo", mudraSenderMastBean.getMobileNo());
			responsemap.put("cardExists", "N");
			responsemap.put("message", "OTP has been sent on Mobile Number.");
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "SENDER EXISTS");
			responsemap.put("cardExists", "Y");
			responsemap.put("response", "SUCCESS.");
			//responsemap.put("senderId", mudraSenderMastBean.getId());
			responsemap.put("mobileNo", mudraSenderMastBean.getMobileNo());
			
			/*HashMap<String,Object> cardDetails= new HashMap<String,Object>();
			cardDetails.put("mobileNo", mudraSenderMastBean.getMobileNo());
			cardDetails.put("balance", mudraSenderMastBean.getWalletBalance());
			cardDetails.put("remitLimitAvailable", mudraSenderMastBean.getTransferLimit());
			responsemap.put("cardDetails", cardDetails);
			responsemap.put("beneficiary", mudraSenderMastBean.getBeneficiaryList());*/
			//merchantResponseBean.setRequestId("");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	
	 
	 
	}catch (Exception e) {
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|Problem in checkWalletExistRequest "
				
			+ e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
	 
	 
	return merchantResponseBean;
}

/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/createWalletRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean createWalletRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"createWalletRequest");
	 
     
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|"
			
			+ merchantRequestBean.getAgentId());

	//logger.info(serverName +"****RequestId*****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	//logger.info(serverName +"*****RequestId******"+requestId+"**************************************Request*****"+merchantRequestBean.getRequest());
	
	//logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|mKey"
			
			+ mKey);
	// logger.info(serverName +"*****RequestId******"+requestId+"**************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
						merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 //mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 
	 
	 String mobileNo=claim.get("mobileNo").toString();
	 if(!mobileValidation(mobileNo)){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "INVALID MOBILE NUMBER.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean;
	 }
	 
	 if(!nameValidation(claim.get("name").toString())){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Only alphabets are allowed in name.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean; 
	 }
	 
	 if(!mpinValidation(claim.get("mpin").toString())){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean; 
	 }
	 
	 
	 mudraSenderMastBean.setMobileNo(mobileNo);
	 mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 mudraSenderMastBean.setAgentId(merchantRequestBean.getAgentId());
	 mudraSenderMastBean.setFirstName(claim.get("name").toString());
	 mudraSenderMastBean.setMpin(claim.get("mpin").toString());
	 mudraSenderMastBean.setOtp(claim.get("otp").toString());
	 mudraSenderMastBean=mudraDao.registerSender(mudraSenderMastBean,serverName,requestId);
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|StatusCode"
				
			+ mudraSenderMastBean.getStatusCode());
	// logger.info(serverName +"*****RequestId******"+requestId+"**********************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1104")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Mobile Number.");
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1101")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Name can't be empty.");
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1102")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid MPIN or MPIN empty.");
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1103")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1105")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1106")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid OTP.");
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("11206")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Remitter (Sender) Mobile Number already registered with us.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 
	 
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "Sender has been registered successfully.");
			responsemap.put("cardExists", "Y");
			responsemap.put("response", "SUCCESS.");
			responsemap.put("senderId", mudraSenderMastBean.getId());
			responsemap.put("mobileNo", mudraSenderMastBean.getMobileNo());
			
		/*	
			HashMap<String,Object> cardDetails= new HashMap<String,Object>();
			cardDetails.put("mobileno", mudraSenderMastBean.getMobileNo());
			cardDetails.put("balance", mudraSenderMastBean.getWalletBalance());
			cardDetails.put("remitLimitAvailable", mudraSenderMastBean.getTransferLimit());
			responsemap.put("cardDetails", cardDetails);
			responsemap.put("beneficiary", mudraSenderMastBean.getBeneficiaryList());*/
			
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }else{
		 responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING.");
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean; 
	 }
	
}catch (Exception e) {
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|Problem"
			
			+ e.getMessage()+" "+e);
	logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "INVALID MESSAGE STRING");
	//merchantResponseBean.setRequestId("");
	merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	return merchantResponseBean;
}
	 
	
}



/**
* 	
* @param merchantRequestBean
* @param request
* @return
*/
@POST
@Path("/getUserDetails")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean getUserDetails(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
String serverName=request.getServletContext().getInitParameter("serverName");
MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();

String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getUserDetails");


Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|getUserDetails()|"
		
		+ merchantRequestBean.getAgentId());
/*logger.info(serverName +"*******RequestId*********"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
merchantResponseBean.setRequestId(requestId);

if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "Invalid wallet Agent ID.");
	merchantResponseBean.setResponse(gson.toJson(responsemap));
	return merchantResponseBean;
}
if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "Invalid Request.");
	merchantResponseBean.setResponse(gson.toJson(responsemap));
	return merchantResponseBean;		
}
String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|mKey :"+mKey);
//logger.info(serverName+"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
if(mKey==null ||mKey.isEmpty()){
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "Invalid Merchant. ");
	merchantResponseBean.setResponse(gson.toJson(responsemap));
	return merchantResponseBean;
}
try{
 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
 if(claim==null){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "JWT signature does not match. ");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
 
 String mobileNo=claim.get("mobileNo").toString();
 if(!mobileValidation(mobileNo)){
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "INVALID MOBILE NUMBER.");
	 oxyJWTSignUtil.generateToken(responsemap, mKey);
	 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
	 return merchantResponseBean;
 }
 mudraSenderMastBean.setMobileNo(mobileNo);
 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
 mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
// mudraSenderMastBean.setAggreatorId(merchantRequestBean.getAgentId());
 mudraSenderMastBean=mudraDao.validateSender(mudraSenderMastBean,serverName,requestId);
 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|StatusCode :"+mudraSenderMastBean.getStatusCode());

 //logger.info(serverName +"*****RequestId******"+requestId+"*************************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "Invalid wallet Agent ID.");
	oxyJWTSignUtil.generateToken(responsemap, mKey);
	
	merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
	return merchantResponseBean;
 }
 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1104")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Sender Mobile Number.");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
 }
 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("2000")){
		responsemap.put("code", "300");
		responsemap.put("response", "SUCCESS");
		responsemap.put("mobileNo", mudraSenderMastBean.getMobileNo());
		responsemap.put("cardExists", "N");
		responsemap.put("message", "OTP has been sent on Mobile Number.");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
}
 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
		responsemap.put("code", "300");
		responsemap.put("message", "SUCCESS");
		responsemap.put("cardExists", "Y");
		responsemap.put("response", "SUCCESS.");
		responsemap.put("senderId", mudraSenderMastBean.getId());
		HashMap<String,Object> cardDetails= new HashMap<String,Object>();
		cardDetails.put("mobileNo", mudraSenderMastBean.getMobileNo());
		cardDetails.put("balance", mudraSenderMastBean.getWalletBalance());
		cardDetails.put("remitLimitAvailable", mudraSenderMastBean.getTransferLimit());
		responsemap.put("cardDetails", cardDetails);
		responsemap.put("beneficiary", mudraSenderMastBean.getBeneficiaryList());
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
 }
}catch (Exception e) {
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|Problem "+e.getMessage()+" "+e);

	//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "INVALID MESSAGE STRING");
	//merchantResponseBean.setRequestId("");
	merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	return merchantResponseBean;
}
return merchantResponseBean;
}





/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/resendOtp")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean resendOtp(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"resendOtp");
	
	
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|resendOtp()|"
			
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"******RequestId******"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"**************************************Request*****"+merchantRequestBean.getRequest());
	
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|mKey :"+mKey);
	 //logger.info(serverName+"*****RequestId******"+requestId+"**********************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 String mobileNo=claim.get("mobileNo").toString();
	 if(!mobileValidation(mobileNo)){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "INVALID MOBILE NUMBER.");
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean;
	 }
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 //mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());

	
	 Boolean flag=mudraDao.otpSenderResend(mobileNo,walletMastBean.getAggreatorid(),serverName,requestId);
	 
	if(flag){
		responsemap.put("code", "300");
		//changed on 02 Aug
		responsemap.put("response", "SUCCESS");
		responsemap.put("message", "OTP has been sent to sender mobile number.");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));;
		return merchantResponseBean;
	}else{
		responsemap.put("code", "1");
		//changed on 02 Aug
		responsemap.put("response", "ERROR");
		responsemap.put("message", "FAILED.");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
}catch (Exception e) {
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|Problem :"+e.getMessage()+" "+e);

	//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "INVALID MESSAGE STRING");
	//merchantResponseBean.setRequestId("");
	merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	return merchantResponseBean;
}
	
}


/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/addBeneficiaryRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean addBeneficiaryRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	 MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"addBeneficiaryRequest");
	 
     
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|addBeneficiaryRequest()|"
			
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"****RequestId***"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);
*/	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|mKey"+mKey);
	 //logger.info(serverName +"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	 
	 //added for defect id 6.1
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 
	 	if (claim.get("name").toString() == null || claim.get("name").toString().isEmpty() || !nameValidation(claim.get("name").toString())) {
	 		 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid Name.");
			 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			 return merchantResponseBean;
		   }

		   if (claim.get("bankName").toString() == null || claim.get("bankName").toString().isEmpty() || !nameValidation(claim.get("bankName").toString())) {
			   responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid Bank Name.");
				 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				 return merchantResponseBean;
		   }
		   if (claim.get("accountNo").toString() == null ||claim.get("accountNo").toString().isEmpty() || !accountValidation(claim.get("accountNo").toString())) {
			   responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid Account Number.");
				 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				 return merchantResponseBean;
			}

			if (claim.get("ifscCode").toString() == null || claim.get("ifscCode").toString().isEmpty() || !iFSCValidation(claim.get("ifscCode").toString())) {
				responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid IFSC Code.");
				 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				 return merchantResponseBean;
			}

			if (claim.get("transferType").toString() == null || claim.get("transferType").toString().isEmpty() || !(claim.get("transferType").toString().trim().equalsIgnoreCase("IMPS") || claim.get("transferType").toString().trim().equalsIgnoreCase("NEFT"))) {
				responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid Transfer Type.");
				 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
				 return merchantResponseBean;
			}
			
	 
	 
	 
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 //mudraBeneficiaryMastBean.setAggreatorId(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setSenderId(claim.get("senderId").toString());
	 mudraBeneficiaryMastBean.setName(claim.get("name").toString());
	 mudraBeneficiaryMastBean.setBankName(claim.get("bankName").toString());
	 mudraBeneficiaryMastBean.setAccountNo(claim.get("accountNo").toString());
	 mudraBeneficiaryMastBean.setIfscCode(claim.get("ifscCode").toString());
	 mudraBeneficiaryMastBean.setTransferType(claim.get("transferType").toString());
	 mudraBeneficiaryMastBean.setMpin(claim.get("mpin").toString());
	 
	 mudraBeneficiaryMastBean=mudraDao.registerBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	 
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|StatusCode"+mudraBeneficiaryMastBean.getStatusCode());

	 
	 //logger.info(serverName+"*****RequestId******"+requestId+"********************************StatusCode*****"+mudraBeneficiaryMastBean.getStatusCode());
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1110")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 //added for defect id 6.4
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1101") ){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary name.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	//added for defect id 6.4
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1107") ){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid bank name.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1108")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid account number.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1109")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid IFSC code.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1111")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transfer Type.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1102")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1124")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Provided account number is already added with sender.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1123")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1000")){
		 responsemap.put("code", "300");
		 //added on 2nd Aug
			responsemap.put("response", "SUCCESS");
			responsemap.put("message", "Beneficiary has been registered successfully.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			//merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
	 }
}catch (Exception e) {
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName + "*******RequestId******" + requestId+ "|Problem in addBeneficiaryRequest :"+e.getMessage()+" "+e);

	//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
	responsemap.put("code", "1");
	responsemap.put("response", "ERROR");
	responsemap.put("message", "INVALID MESSAGE STRING");
	System.out.println(e.getMessage());
	e.printStackTrace();
	//merchantResponseBean.setRequestId("");
	merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	return merchantResponseBean;
}
	 return merchantResponseBean; 
	 
}

/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/deactiveBeneficiaryRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean deactiveBeneficiaryRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	 MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
	 MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"deactiveBeneficiaryRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|deactiveBeneficiaryRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"***RequestId***"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName+"*****RequestId******"+requestId+"********************************Request*****"+merchantRequestBean.getRequest());
	
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	 //logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	// added for defect id 7.1
	 
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	// mudraBeneficiaryMastBean.setAggreatorId(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setSenderId(claim.get("senderId").toString());
	 mudraBeneficiaryMastBean.setId(claim.get("beneficiaryId").toString());
	 mudraSenderMastBean=mudraDao.deActiveBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	 
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+mudraSenderMastBean.getStatusCode());
	 //logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 //added for defect id 7.1
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1130")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 //changed on 21 july ,2017
	 /*if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Provided account number is already added with sender.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }*/
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1110")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "Beneficiary Deactivated SUCCESSFULLY.");
			//added for defect id 7.6
			//responsemap.put("cardExists", "Y");
			responsemap.put("response", "SUCCESS.");
			responsemap.put("beneficiary", mudraSenderMastBean.getBeneficiaryList());
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	}catch (Exception e) {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
					+"|Problem in deactiveBeneficiaryRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
	 return merchantResponseBean; 
	 
	 
}

/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/activeBeneficiaryRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean activeBeneficiaryRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	 MudraBeneficiaryWallet mudraBeneficiaryMastBean=new MudraBeneficiaryWallet();
	 MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"activeBeneficiaryRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|activeBeneficiaryRequest()|"
			+ merchantRequestBean.getAgentId());
	
	/*logger.info(serverName +"********RequestId****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	
	//logger.info(serverName +"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 //added for defect id 8.1
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 //mudraBeneficiaryMastBean.setAggreatorId(merchantRequestBean.getAgentId());
	 mudraBeneficiaryMastBean.setSenderId(claim.get("senderId").toString());
	 mudraBeneficiaryMastBean.setId(claim.get("beneficiaryId").toString());
	 mudraSenderMastBean=mudraDao.activeBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
	 
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+mudraSenderMastBean.getStatusCode());
	 //logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 //added for defect id 8.2
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1130")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 //added on 21 july 
	/* if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Provided account number is already added with sender.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }*/
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1110")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1123")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "Beneficiary activated SUCCESSFULLY.");
			//added for defect id 8.6
			//responsemap.put("cardExists", "Y");
			responsemap.put("response", "SUCCESS.");
			responsemap.put("beneficiary", mudraSenderMastBean.getBeneficiaryList());
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in activeBeneficiaryRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
	 return merchantResponseBean; 
	 
	 
}

/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/forgotMPINRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean forgotMPINRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	 MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"forgotMPINRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|forgotMPINRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"****RequestId*******"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName+"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey); 
	//logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	//added for defect id 9.2
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 //changed on 2nd Aug
	 //mudraSenderMastBean.setAggreatorId(merchantRequestBean.getAgentId());
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 mudraSenderMastBean.setId(claim.get("senderId").toString());
	 mudraSenderMastBean.setMobileNo(claim.get("mobileNo").toString());
	 mudraSenderMastBean=mudraDao.forgotMPIN(mudraSenderMastBean,serverName,requestId);
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+mudraSenderMastBean.getStatusCode());
	
	// logger.info(serverName +"*****RequestId******"+requestId+"***********************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }

	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "OTP has been sent on registered Mobile Number.");
			responsemap.put("response", "SUCCESS.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in forgotMPINRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	} 
	 return merchantResponseBean;

}

/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/updateMPINRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean updateMPINRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	 MudraSenderWallet mudraSenderMastBean=new MudraSenderWallet();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"updateMPINRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|updateMPINRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"********RequestID****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	// logger.info(serverName+"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	//added for defect id 10.1
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 //added on 21 July
	 if(claim.get("mpin").toString() == null || claim.get("mpin").toString().isEmpty() || !mpinValidation(claim.get("mpin").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 //changed on 2nd Aug
	 //mudraSenderMastBean.setAggreatorId(merchantRequestBean.getAgentId());
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 
	 mudraSenderMastBean.setId(claim.get("senderId").toString());
	 mudraSenderMastBean.setMobileNo(claim.get("mobileNo").toString());
	 mudraSenderMastBean.setMpin(claim.get("mpin").toString());
	 mudraSenderMastBean.setOtp(claim.get("otp").toString());
	 mudraSenderMastBean=mudraDao.updateMPIN(mudraSenderMastBean,serverName,requestId);
	 
	
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+mudraSenderMastBean.getStatusCode());
	// logger.info(serverName+"*****RequestId******"+requestId+"*************************************StatusCode*****"+mudraSenderMastBean.getStatusCode());
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1103")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1105")||mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1106")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid OTP.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1102")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid mpin.mpin must have 4 numerical value.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }

	 if(mudraSenderMastBean.getStatusCode().equalsIgnoreCase("1000")){
			responsemap.put("code", "300");
			responsemap.put("message", "MPIN has been changed successfully.");
			responsemap.put("response", "SUCCESS.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in updateMPINRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
	 return merchantResponseBean;
}





/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/mrTransferRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean mrTransferRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"mrTransferRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mrTransferRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"****RequestId****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	// logger.info(serverName +"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 //added for defect id 14.4
	 if(claim.get("beneficiaryName").toString().trim().isEmpty() || !nameValidation(claim.get("beneficiaryName").toString()))
	 {
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Beneficiary Name is not Valid.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 } 
	 if(claim.get("bankName").toString().trim().isEmpty() || !nameValidation(claim.get("bankName").toString()))
	 {
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Bank Name is not Valid.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 } 

		if(claim.get("accountNo").toString().trim().equalsIgnoreCase("") || !accountValidation(claim.get("accountNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Account Number");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		
		if(claim.get("ifscCode").toString().trim().equalsIgnoreCase("") || !iFSCValidation(claim.get("ifscCode").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid IFSC Code");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		 
	 if(Double.parseDouble(claim.get("amount").toString())>5000){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Your max transaction limit is 5000.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 
	 WalletMastBean walletMast=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 	
	 mudraMoneyTransactionBean.setUserId(merchantRequestBean.getAgentId());
	 mudraMoneyTransactionBean.setSenderId(claim.get("senderId").toString());
	 mudraMoneyTransactionBean.setBeneficiaryId(claim.get("beneficiaryId").toString());
	 
	//IMPS BLOCKED
	  
	  if(claim.get("transferType").toString().equalsIgnoreCase("IMPS")){
	   responsemap.put("code", "1");
	   responsemap.put("response", "ERROR");
	   responsemap.put("message", "IMPS transfer channel not open. ");
	   oxyJWTSignUtil.generateToken(responsemap, mKey);
	   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
	   return merchantResponseBean;  
	  }
	 //END
	 
	 if(claim.get("transferType").toString().equalsIgnoreCase("NEFT")||claim.get("transferType").toString().equalsIgnoreCase("IMPS")){
	  mudraMoneyTransactionBean.setNarrartion(claim.get("transferType").toString());
	 }else{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid transfer Type :"+claim.get("transferType").toString());
		 oxyJWTSignUtil.generateToken(responsemap, mKey);
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		 return merchantResponseBean; 
		 
	 }
	 //added for defect id 6.1
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 mudraMoneyTransactionBean.setTxnAmount(Double.parseDouble(claim.get("amount").toString()));
	 mudraMoneyTransactionBean.setSurChargeAmount(mudraDao.calculateSurCharge(mudraMoneyTransactionBean, serverName,requestId).getSurChargeAmount());
	 mudraMoneyTransactionBean.setWalletId(walletMast.getWalletid());
	 mudraMoneyTransactionBean.setMerchantTransId(claim.get("merchantTransId").toString());
	 mudraMoneyTransactionBean.setAgentid(merchantRequestBean.getAgentId());
	 
	 FundTransactionSummaryBean fundTransactionSummaryBean=mudraDao.fundTransfer(mudraMoneyTransactionBean, serverName,requestId);
	 
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+fundTransactionSummaryBean.getStatusCode());
	 logger.info(serverName +"*****RequestId******"+requestId+"**********************************StatusCode*****"+fundTransactionSummaryBean.getStatusCode());
	 
	//added for defect id 14.7
		
		if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1130")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1001")||fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1110")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1111")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transfer Type.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1120")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Amount can't be zero.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1122")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Transfer Limit exceed.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1000")){
	
		  				
			MudraMoneyTransactionBean mResult=new MudraMoneyTransactionBean();
			
			List <MudraMoneyTransactionBean> results=fundTransactionSummaryBean.getMudraMoneyTransactionBean();
			Iterator<MudraMoneyTransactionBean> iterator = results.iterator();
			
			while (iterator.hasNext()) {
				mResult=iterator.next();
				if(mResult.getCrAmount()==0){
					break;
				}
				}
			HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
			moneyRemittance.put("amount", fundTransactionSummaryBean.getAmount());
			moneyRemittance.put("charges", mudraMoneyTransactionBean.getSurChargeAmount());
			moneyRemittance.put("totalAmount", fundTransactionSummaryBean.getAmount()+mudraMoneyTransactionBean.getSurChargeAmount());
			moneyRemittance.put("transferType", mResult.getNarrartion());
			moneyRemittance.put("transId", mResult.getId());
			moneyRemittance.put("paymentId", mResult.getTxnId());
			moneyRemittance.put("bankTransId", mResult.getBankRrn());
			moneyRemittance.put("transferStatus", mResult.getStatus());
			moneyRemittance.put("transDate", mResult.getPtytransdt());
			responsemap.put("moneyRemittance", moneyRemittance);
			HashMap<String,Object> beneficiary= new HashMap<String,Object>();
			MudraBeneficiaryWallet bResult=mudraDao.getBeneficiary(mResult.getBeneficiaryId(), serverName);
			beneficiary.put("beneficiaryId", bResult.getId());
			beneficiary.put("beneficiaryName", bResult.getName());
			beneficiary.put("accountNo", bResult.getAccountNo());
			//changed on 28th july
			beneficiary.put("ifscCode", bResult.getIfscCode() );
			responsemap.put("beneficiary", beneficiary);
			HashMap<String,Object> bankDetail= new HashMap<String,Object>();
			bankDetail.put("bankName", bResult.getBankName());
			bankDetail.put("branchName", bResult.getBranchName());
			bankDetail.put("address", bResult.getAddress());
			bankDetail.put("city", bResult.getCity());
			responsemap.put("bankDetail", bankDetail);
			
			responsemap.put("code", "300");
			responsemap.put("response", "SUCCESS");
			responsemap.put("message", "SUCCESS.");
			responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
			responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
			//added for defect id  14.5
			//responsemap.put("mobileNo", mudraMoneyTransactionBean.getSenderId());
			responsemap.put("mobileNo", mobileNo);
			
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 
	 }else{
		 responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Velocity check error.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	}//added on 3rd Aug
	 catch (NumberFormatException e) 
	 {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
					+"|Problem in mrTransferRequest "+e.getMessage()+" "+e);
	 // logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
	  e.printStackTrace();
	  responsemap.put("code", "1");
	  responsemap.put("response", "ERROR");
	  responsemap.put("message", "Invalid amount");
	  merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	  return merchantResponseBean;
	 }catch (Exception e) {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
					+"|Problem in mrTransferRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	} 
}





/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/accountVerificationRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean accountVerificationRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
//public MerchantResponseBean accountVerificationRequest(MerchantRequestBean merchantRequestBean){
	MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
	String serverName=request.getServletContext().getInitParameter("serverName");
	//String serverName="bcd";
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"accountVerificationRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|accountVerificationRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"******RequestId***"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"**************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
			
	 //logger.info(serverName+"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}
	 
	 
	
	 
	//IMPS BLOCKED
	  
	  if(claim.get("transferType").toString().equalsIgnoreCase("IMPS")){
	   responsemap.put("code", "1");
	   responsemap.put("response", "ERROR");
	   responsemap.put("message", "IMPS transfer channel not open. ");
	   oxyJWTSignUtil.generateToken(responsemap, mKey);
	   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
	   return merchantResponseBean;  
	  }
	 //END
	  
	  //added for defect id 13.3
		
		if(claim.get("beneficiaryName").toString().trim().equalsIgnoreCase("") || !nameValidation(claim.get("beneficiaryName").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary Name");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		
		if(claim.get("bankName").toString().trim().equalsIgnoreCase("") || !nameValidation(claim.get("bankName").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Bank Name");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	   
		if(claim.get("accountNo").toString().trim().equalsIgnoreCase("") || !accountValidation(claim.get("accountNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Account Number");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
		
		if(claim.get("ifscCode").toString().trim().equalsIgnoreCase("") || !iFSCValidation(claim.get("ifscCode").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid IFSC Code");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
	 
	 
/*	 if(Double.parseDouble(claim.get("amount").toString())>5000){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Your max transaction limit is 5000.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }*/
	 //added for defect id 13.2
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
	 String mobileNo = "";
	 
	 if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 } 
	 else
	 {
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	 }
	 if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	 {
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	 WalletMastBean walletMast=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 	
	 mudraMoneyTransactionBean.setUserId(merchantRequestBean.getAgentId());
	 mudraMoneyTransactionBean.setSenderId(claim.get("senderId").toString());
	 mudraMoneyTransactionBean.setBeneficiaryId(claim.get("beneficiaryId").toString());
	 mudraMoneyTransactionBean.setNarrartion("IMPS");
	 mudraMoneyTransactionBean.setTxnAmount(1);
	 mudraMoneyTransactionBean.setVerificationAmount(1);
	 
	 mudraMoneyTransactionBean.setSurChargeAmount(mudraDao.calculateSurCharge(mudraMoneyTransactionBean, serverName,requestId).getSurChargeAmount());
	 mudraMoneyTransactionBean.setWalletId(walletMast.getWalletid());
	 mudraMoneyTransactionBean.setMerchantTransId(claim.get("merchantTransId").toString());
	
	 
	 MudraMoneyTransactionBean mudraMoneyTransactionBeanres=mudraDao.verifyAccount(mudraMoneyTransactionBean, serverName,requestId);
	 
	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|StatusCode"+mudraMoneyTransactionBeanres.getStatusCode());
				
	 //logger.info(serverName +"*****RequestId******"+requestId+"************************************StatusCode*****"+mudraMoneyTransactionBeanres.getStatusCode());
	 
	 //added for defect id 13.3
	
	if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1130")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Beneficiary does not belong to mentioned Sender ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1001")||mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("7000")){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		oxyJWTSignUtil.generateToken(responsemap, mKey);
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
		return merchantResponseBean;
	 }
	 
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1110")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Sender Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1116")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Beneficiary Id.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1111")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Transfer Type.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1120")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Amount can't be zero.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1122")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Transfer Limit exceed.");
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 }
	 
	 if(mudraMoneyTransactionBeanres.getStatusCode().equalsIgnoreCase("1000")){
	

			HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
			moneyRemittance.put("amount", mudraMoneyTransactionBeanres.getTxnAmount());
			moneyRemittance.put("transId", mudraMoneyTransactionBeanres.getTxnId());
			moneyRemittance.put("paymentId", mudraMoneyTransactionBeanres.getTxnId());
			moneyRemittance.put("bankTransId", mudraMoneyTransactionBeanres.getBankRrn());
			moneyRemittance.put("transferStatus", mudraMoneyTransactionBeanres.getStatusDesc());
			moneyRemittance.put("transDate", mudraMoneyTransactionBeanres.getPtytransdt());
		
			responsemap.put("moneyRemittance", moneyRemittance);
			HashMap<String,Object> verification= new HashMap<String,Object>();
			verification.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			verification.put("verified", mudraMoneyTransactionBeanres.getVerified());
			verification.put("accHolderName", mudraMoneyTransactionBeanres.getAccHolderName());
			verification.put("verificationDesc", mudraMoneyTransactionBeanres.getVerificationDesc());
			responsemap.put("verification", verification);
			responsemap.put("code", "300");
			responsemap.put("response", "SUCCESS");
			responsemap.put("message", "SUCCESS.");
			//changed on 28th july
			responsemap.put("mobileNo", claim.get("mobileNo").toString());
			responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
			responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
	 
	 }else{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Velocity check error.");
		 responsemap.put("mobileNo",claim.get("mobileNo").toString());
		 responsemap.put("senderId", mudraMoneyTransactionBean.getSenderId());
		 responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in accountVerificationRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	} 
}


/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/getTransHistoryRequest")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean getTransHistoryRequest(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getTransHistoryRequest");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|getTransHistoryRequest()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"******RequestId*****"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"*************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	 //logger.info(serverName+"*****RequestId******"+requestId+"*************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			//merchantResponseBean.setResponse(gson.toJson(responsemap));
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
		}	
	//added on 2nd Aug 
		 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Mobile Number.");
				oxyJWTSignUtil.generateToken(responsemap, mKey);
				merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
				return merchantResponseBean;
			 }
	String mobileNo = "";

	if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid senderId");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	} 
	else
	{
		 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
	}
	if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
	{
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
		 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	}
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	// mudraSenderMastBean.setAggreatorId(walletMastBean.getAggreatorid());
	 
	 	List<MerchantDmtTransBean> gettransDtl=mudraDao.gettransDtl(walletMastBean.getAggreatorid(), claim.get("mobileNo").toString(), serverName);
	 	responsemap.put("count", gettransDtl.size());
	 	responsemap.put("mobileNo", claim.get("mobileNo").toString());
	 	responsemap.put("transDetails", gettransDtl);
	 	responsemap.put("response", "SUCCESS");
		responsemap.put("message", "SUCCESS");
		responsemap.put("code", "300");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in accountVerificationRequest "+e.getMessage()+" "+e);
		//logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
}







/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */
@POST
@Path("/getTransStatus")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean getTransStatus(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	String serverName=request.getServletContext().getInitParameter("serverName");
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getTransStatus");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|getTransStatus()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName +"********RequestId***"+requestId+"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName +"*****RequestId******"+requestId+"**************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	// logger.info(serverName+"*****RequestId******"+requestId+"************************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
			return merchantResponseBean;
		}	
	 WalletMastBean walletMastBean=new WalletUserDaoImpl().showUserProfile(merchantRequestBean.getAgentId());
	 List<MerchantDmtTransBean> gettransDtl=mudraDao.getTransStatus(walletMastBean.getAggreatorid(), claim.get("agentTransId").toString(), serverName);
	 //if(gettransDtl==null && gettransDtl.size()<=0){
	 if(gettransDtl.size()<=0){
		 responsemap.put("code", "1");
		 responsemap.put("response", "ERROR");
		 responsemap.put("message", "Invalid Agent Transaction Id. ");
		 responsemap.put("agentTransId", claim.get("agentTransId") );
		// merchantResponseBean.setResponse(gson.toJson(responsemap));
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean;
	 }else{
		 responsemap.put("code", "300");
		 responsemap.put("response", "SUCCESS");
		 responsemap.put("message", "SUCCESS. ");
		 responsemap.put("agentTransId", claim.get("agentTransId") );
		 responsemap.put("status", gettransDtl.get(0).getStatus() );
		 responsemap.put("mobileNo", gettransDtl.get(0).getMobileNo() );
		 responsemap.put("amount", gettransDtl.get(0).getAmount() );
		 responsemap.put("transDateTime", gettransDtl.get(0).getTransDateTime() );
		// merchantResponseBean.setResponse(gson.toJson(responsemap));
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		 return merchantResponseBean; 
	 }
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in getTransStatus "+e.getMessage()+" "+e);
	//	logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
}


/**
 * 
 * @param merchantRequestBean
 * @param request
 * @return
 */

@POST
@Path("/getUserBalance")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public MerchantResponseBean getUserBalance(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getUserBalance");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|getUserBalance()|"
			+ merchantRequestBean.getAgentId());
	/*logger.info(serverName+"***RequestId*****"+requestId +"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
	logger.info(serverName+"*****RequestId******"+requestId+"************************************Request*****"+merchantRequestBean.getRequest());
	logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
	MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
	merchantResponseBean.setRequestId(requestId);
	
	
	
	if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid Request.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;		
	}
	String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
	// logger.info(serverName +"*****RequestId******"+requestId+"***********************************mKey*****"+mKey);
	if(mKey==null ||mKey.isEmpty()){
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "Invalid wallet Agent ID.");
		merchantResponseBean.setResponse(gson.toJson(responsemap));
		return merchantResponseBean;
	}
	try{
	 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
	 if(claim==null){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "JWT signature does not match. ");
			merchantResponseBean.setResponse(gson.toJson(responsemap));
			return merchantResponseBean;
		}	
	 
//added on 2nd Aug 
	 if(claim.get("mobileNo") == null || claim.get("mobileNo").toString().trim().equalsIgnoreCase("") || !mobileValidation(claim.get("mobileNo").toString())){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Mobile Number.");
			oxyJWTSignUtil.generateToken(responsemap, mKey);
			merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey)); 
			return merchantResponseBean;
		 }
String mobileNo = "";

if((mudraDao.getSenderMobileNo(claim.get("senderId").toString()) == null) || mudraDao.getSenderMobileNo(claim.get("senderId").toString()).equals(""))
{
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "Invalid senderId");
	 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	 return merchantResponseBean;
} 
else
{
	 mobileNo = mudraDao.getSenderMobileNo(claim.get("senderId").toString()).trim();
}
if(!(claim.get("mobileNo").toString().trim().equals(mobileNo)))
{
	 responsemap.put("code", "1");
	 responsemap.put("response", "ERROR");
	 responsemap.put("message", "Invalid combination of senderId and mobile Number.");
	 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
	 return merchantResponseBean;
}
	 	double userBalance=mudraDao.getUserBalance(merchantRequestBean.getAgentId(), claim.get("senderId").toString(), serverName);
	 	
	 	responsemap.put("code", "300");
		responsemap.put("response", "SUCCESS");
		responsemap.put("message", "SUCCESS.");
		responsemap.put("mobileNo", claim.get("mobileNo").toString());
		responsemap.put("senderId", claim.get("senderId").toString());
		responsemap.put("balance", userBalance);
		//merchantResponseBean.setResponse(gson.toJson(responsemap));
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in getUserBalance "+e.getMessage()+" "+e);
		logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		responsemap.put("code", "1");
		responsemap.put("response", "ERROR");
		responsemap.put("message", "INVALID MESSAGE STRING");
		//merchantResponseBean.setRequestId("");
		merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
		return merchantResponseBean;
	}
}

public  boolean mobileValidation(String mobile) {
//added on 2nd Aug
    Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
    Matcher matcher = pattern.matcher(mobile);

    if (matcher.matches()) {
  	  return true;
    }
    else
    {
    	return false;
    }
}

public  boolean accountValidation(String account) {

    Pattern pattern = Pattern.compile("^[0-9]{9,26}$");
    Matcher matcher = pattern.matcher(account);

    if (matcher.matches()) {
  	  return true;
    }
    else
    {
    	return false;
    }
}

public  boolean iFSCValidation(String ifsc) {

    Pattern pattern = Pattern.compile("^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$");
    Matcher matcher = pattern.matcher(ifsc);

    if (matcher.matches()) {
  	  return true;
    }
    else
    {
    	return false;
    }
}

public  boolean nameValidation(String mobile) {

    Pattern pattern = Pattern.compile("^[a-zA-Z ]+$");
    Matcher matcher = pattern.matcher(mobile);

    if (matcher.matches()) {
  	  return true;
    }
    else
    {
    	return false;
    }
}

public  boolean mpinValidation(String mobile) {

    Pattern pattern = Pattern.compile("^[0-9]{4}+$");
    Matcher matcher = pattern.matcher(mobile);

    if (matcher.matches()) {
  	  return true;
    }
    else
    {
    	return false;
    }
}



//changed on 27th july
/**
* 
* @param merchantRequestBean
* @param request
* @return
*/
@POST
@Path("/getBankDetails")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML }) 
public MerchantResponseBean getBankDetails(MerchantRequestBean merchantRequestBean,@Context HttpServletRequest request){
 String serverName=request.getServletContext().getInitParameter("serverName");
 
 String requestId=mudraDao.saveDMTApiRequest(merchantRequestBean.getAgentId(),merchantRequestBean.getRequest(),serverName,"getBankDetails");
	
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|getBankDetails()|"
			+ merchantRequestBean.getAgentId());
 /*logger.info(serverName+"***RequestId*****"+requestId +"**************************************Merchantid*****"+merchantRequestBean.getAgentId());
 logger.info(serverName +"***RequestId*****"+requestId +"**************************************Request*****"+merchantRequestBean.getRequest());
 logger.info(serverName +"***********************After Saving request ***************requestId*****"+requestId);*/
 MerchantResponseBean merchantResponseBean=new MerchantResponseBean();
 merchantResponseBean.setRequestId(requestId);
 
 
 
 if(merchantRequestBean.getAgentId()==null ||merchantRequestBean.getAgentId().isEmpty()){
 responsemap.put("code", "1");
 responsemap.put("response", "ERROR");
 responsemap.put("message", "Invalid wallet Agent ID.");
 merchantResponseBean.setResponse(gson.toJson(responsemap));
 return merchantResponseBean;
 }
 if(merchantRequestBean.getRequest()==null ||merchantRequestBean.getRequest().isEmpty()){
 responsemap.put("code", "1");
 responsemap.put("response", "ERROR");
 responsemap.put("message", "Invalid Request.");
 merchantResponseBean.setResponse(gson.toJson(responsemap));
 return merchantResponseBean;  
 }
 String mKey=new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
			+"|mKey"+mKey);
 //logger.info(serverName +"***RequestId*****"+requestId +"***************************************mKey*****"+mKey);
 if(mKey==null ||mKey.isEmpty()){
 responsemap.put("code", "1");
 responsemap.put("response", "ERROR");
 responsemap.put("message", "Invalid wallet Agent ID.");
 merchantResponseBean.setResponse(gson.toJson(responsemap));
 return merchantResponseBean;
 }
 try{
 Claims claim=oxyJWTSignUtil.parseToken(merchantRequestBean.getRequest(),mKey);
 if(claim==null){
  responsemap.put("code", "1");
  responsemap.put("response", "ERROR");
  responsemap.put("message", "JWT signature does not match. ");
  merchantResponseBean.setResponse(gson.toJson(responsemap));
  return merchantResponseBean;
 } 
 int errorCount = 0;
 String bankName , branchName , ifscCode;
 if(claim.get("bankName") == null || claim.get("bankName").toString().isEmpty())
 {
   errorCount++;
   bankName="";
 }
 else
 {
  if( !nameValidation(claim.get("bankName").toString()))
  {
   responsemap.put("code", "1");
   responsemap.put("response", "Error");
   responsemap.put("message", "Invalid Bank Name");
   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
   return merchantResponseBean;
  }
  else
  {
   bankName=claim.get("bankName").toString();
  }
 }
 if(claim.get("branchName") == null || claim.get("branchName").toString().isEmpty() )
 {
  errorCount++;
  branchName="";
 }
 else
 {
  if( !nameValidation(claim.get("branchName").toString()))
  {
   responsemap.put("code", "1");
   responsemap.put("response", "Error");
   responsemap.put("message", "Invalid Branch Name");
   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
   return merchantResponseBean;
  }
  else
  {
   branchName=claim.get("branchName").toString();
  }
 }
 if(claim.get("ifscCode") == null || claim.get("ifscCode").toString().isEmpty())
 {
  errorCount++;
  ifscCode="";
 }
 else
 {
  if(!iFSCValidation(claim.get("ifscCode").toString()))
  {
   responsemap.put("code", "1");
   responsemap.put("response", "Error");
   responsemap.put("message", "Invalid IFSC");
   //merchantResponseBean.setResponse(gson.toJson(responsemap));
   merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
   return merchantResponseBean;
  }
  else
  {
   ifscCode=claim.get("ifscCode").toString();
  }
 }
 if(errorCount == 3)
 {
  responsemap.put("code", "1");
  responsemap.put("response", "Error");
  responsemap.put("message", "Atleast one request Parameter should come Bank Name/ Branch Name / IFSC");
 // merchantResponseBean.setResponse(gson.toJson(responsemap));
  merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
  return merchantResponseBean;
 }
  BankDetailsBean bankDetails= new BankDetailsBean();
  List<BankDetailsBean> bankSDetailsList=mudraDao.getBankDetails(bankName,ifscCode,branchName, serverName);
  bankDetails.setBankDetailsList(bankSDetailsList);
  responsemap.put("code", "300");
 responsemap.put("response", "SUCCESS");
 responsemap.put("message", "SUCCESS.");
 responsemap.put("bankBranchDetails ", bankDetails.getBankDetailsList());
 responsemap.put("count", bankSDetailsList.size());
 //merchantResponseBean.setResponse(gson.toJson(responsemap));
 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
 return merchantResponseBean;
 }catch (Exception e) {
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId****" + requestId
				+"|Problem in getBankDetails "+e.getMessage()+" "+e);
  //logger.info(serverName +"***RequestId*****"+requestId +"********************Exception*******************e.getMessage()*****"+e.getMessage());
 responsemap.put("code", "1");
 responsemap.put("response", "ERROR");
 responsemap.put("message", "INVALID MESSAGE STRING");
 //merchantResponseBean.setRequestId("");
 merchantResponseBean.setResponse(oxyJWTSignUtil.generateToken(responsemap, mKey));
 return merchantResponseBean;
 }
}

@POST
@Path("/addBeneficiaryB2C")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
public ResponseBean addBeneficiaryB2C(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
	 //MudraBeneficiaryMastBean mudraBeneficiaryMastBean=new MudraBeneficiaryMastBean();
	String serverName=request.getServletContext().getInitParameter("serverName");
	
	ResponseBean merchantResponseBean=new ResponseBean();

	try{
	
		if(mudraBeneficiaryMastBean.getAggreatorId()==null ||mudraBeneficiaryMastBean.getAggreatorId().isEmpty()){
			 merchantResponseBean.setCode("1");
			 merchantResponseBean.setResponse("ERROR");
			 merchantResponseBean.setMessage( "Invalid wallet Aggregator ID.");
			return merchantResponseBean;
		}		 
		 
		WalletMastBean wallet = new WalletMastBean();
		wallet = new WalletUserDaoImpl().getUserByMobileNo(mudraBeneficiaryMastBean.getSenderId(), mudraBeneficiaryMastBean.getAggreatorId(), serverName);
		if(wallet == null ){
			 merchantResponseBean.setCode("1");
			 merchantResponseBean.setResponse("ERROR");
			 merchantResponseBean.setMessage( "Invalid Customer Id.");
			return merchantResponseBean;
			 }
		 String mobileNo = "";
		  
		 	if (mudraBeneficiaryMastBean.getName() == null || mudraBeneficiaryMastBean.getName().isEmpty() || !nameValidation(mudraBeneficiaryMastBean.getName())) {
		 		 merchantResponseBean.setCode("1");
				 merchantResponseBean.setResponse("ERROR");
				 merchantResponseBean.setMessage( "Invalid Name.");
				return merchantResponseBean;
			   }

			   if (mudraBeneficiaryMastBean.getBankName() == null || mudraBeneficiaryMastBean.getBankName().isEmpty() || !nameValidation(mudraBeneficiaryMastBean.getBankName())) {
				   merchantResponseBean.setCode("1");
					 merchantResponseBean.setResponse("ERROR");
					 merchantResponseBean.setMessage( "Invalid Bank Name.");
					return merchantResponseBean;
			   }
			   if (mudraBeneficiaryMastBean.getAccountNo() == null ||mudraBeneficiaryMastBean.getAccountNo().isEmpty() || !accountValidation(mudraBeneficiaryMastBean.getAccountNo())) {
				   merchantResponseBean.setCode("1");
					 merchantResponseBean.setResponse("ERROR");
					 merchantResponseBean.setMessage( "Invalid Account Number.");
					return merchantResponseBean;
				}

				if (mudraBeneficiaryMastBean.getIfscCode() == null || mudraBeneficiaryMastBean.getIfscCode() .isEmpty() || !iFSCValidation(mudraBeneficiaryMastBean.getIfscCode() )) {
					 merchantResponseBean.setCode("1");
					 merchantResponseBean.setResponse("ERROR");
					 merchantResponseBean.setMessage( "Invalid IFSC Code.");
					return merchantResponseBean;
				}

				if (mudraBeneficiaryMastBean.getTransferType() == null || mudraBeneficiaryMastBean.getTransferType().isEmpty() || !(mudraBeneficiaryMastBean.getTransferType().trim().equalsIgnoreCase("IMPS") || mudraBeneficiaryMastBean.getTransferType().trim().equalsIgnoreCase("NEFT"))) {
					 merchantResponseBean.setCode("1");
					 merchantResponseBean.setResponse("ERROR");
					 merchantResponseBean.setMessage( "Invalid Transfer Type.");
					return merchantResponseBean;
				}
				
				if (mudraBeneficiaryMastBean.getMpin() == null || mudraBeneficiaryMastBean.getMpin().isEmpty()) {
					 merchantResponseBean.setCode("1");
					 merchantResponseBean.setResponse("ERROR");
					 merchantResponseBean.setMessage( "Invalid OTP.");
					return merchantResponseBean;
				}
		 

		boolean flag = new WalletUserDaoImpl().validateOTP(mudraBeneficiaryMastBean.getSenderId(), mudraBeneficiaryMastBean.getAggreatorId(),mudraBeneficiaryMastBean.getMpin());
		if(flag){	
		mudraBeneficiaryMastBean=mudraDao.registerBeneficiary(mudraBeneficiaryMastBean,serverName,"");
	 
	
	 
	 logger.info(serverName+"**********************StatusCode*****"+mudraBeneficiaryMastBean.getStatusCode());
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1001")||mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("7000")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid Message String.");
		return merchantResponseBean;
	 }
	
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1110")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");

		 merchantResponseBean.setMessage( "Invalid Customer Id.");
		return merchantResponseBean;
	 }
	 //added for defect id 6.4
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1101") ){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid Beneficiary name.");
		return merchantResponseBean;	
	 }
	//added for defect id 6.4
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1107") ){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid bank name.");
		return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1108")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid account number.");
		return merchantResponseBean;
	}
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1109")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid IFSC code.");
		return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1111")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid Transfer Type.");
		return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1102")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Invalid mpin.mpin must have 4 numerical value.");
		return merchantResponseBean;
	}
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1124")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "Provided account number is already added with customer.");

		return merchantResponseBean;
	}
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1123")){
		 merchantResponseBean.setCode("1");
		 merchantResponseBean.setResponse("ERROR");
		 merchantResponseBean.setMessage( "You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
		return merchantResponseBean;
	 }
	 
	 if(mudraBeneficiaryMastBean.getStatusCode().equalsIgnoreCase("1000")){
		 merchantResponseBean.setCode("300");
		 merchantResponseBean.setResponse("SUCCESS");
		 merchantResponseBean.setMessage( "Beneficiary has been registered successfully.");
		return merchantResponseBean;
	 }
		}
		else
		{
			 merchantResponseBean.setCode("1");
			 merchantResponseBean.setResponse("ERROR");
			 merchantResponseBean.setMessage( "Invalid OTP.");
			return merchantResponseBean;
		}
}catch (Exception e) {
	logger.info(serverName+"*****************************e.getMessage()*****"+e.getMessage());
	merchantResponseBean.setCode("1");
	merchantResponseBean.setResponse("ERROR");
	merchantResponseBean.setMessage( "INVALID MESSAGE STRING");
	
	System.out.println(e.getMessage());
	e.printStackTrace();
}
  return merchantResponseBean;
	 
}

@POST
@Path("/getBeneficiaryList")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public MudraSenderWallet getBeneficiaryList(MudraSenderWallet mudraSenderMastBean,@Context HttpServletRequest request){
	//added for logger on 3rd Aug
	String serverName=request.getServletContext().getInitParameter("serverName");
	String requestId=mudraDao.saveDMTApiRequest("AggreatorId-"+mudraSenderMastBean.getAggreatorId(),"",serverName,"getBeneficiaryListForImport");
	logger.info("RequestId*****"+requestId+"*******Start excution ********************************************* method getBeneficiaryListForImport()" );
	return mudraDao.getBeneficiaryList(mudraSenderMastBean,serverName,requestId);
}

@POST
@Path("/deleteBeneficiary")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(MediaType.APPLICATION_JSON)
public MudraBeneficiaryWallet deleteBeneficiary(MudraBeneficiaryWallet mudraBeneficiaryMastBean,@Context HttpServletRequest request){
	//added for logger on 3rd Aug
	String serverName=request.getServletContext().getInitParameter("serverName");
	String requestId=mudraDao.saveDMTApiRequest("SenderId-"+mudraBeneficiaryMastBean.getSenderId(),"",serverName,"deleteBeneficiary");
	
      
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryMastBean.getAggreatorId(),"", "", mudraBeneficiaryMastBean.getSenderId(), "", serverName + "|RequestId"+requestId+"|deleteBeneficiary()");
	/*logger.info("*****RequestId******"+requestId+"****Start excution ******************************************** method deleteBeneficiary(mudraBeneficiaryMastBean)"+mudraBeneficiaryMastBean.getId());*/
	return mudraDao.deleteBeneficiary(mudraBeneficiaryMastBean,serverName,requestId);
}


	public static void main(String[] args) {
		HashMap<String,Object> test= new HashMap<String,Object>();
		String mid="MERC001003";
		String key="e391ab57590132714ad32da9acf3013eb88c";
		//test.put("mobileNo", "9935041287");//----already created user
		test.put("mobileNo", "9876544500");//---for new created issues
		
		test.put("senderId", "MSEN001019");
		//test.put("senderId", "MSEN009");
		//test.put("beneficiaryId", "MBEN001018");
		//test.put("beneficiaryId", "Mdsf01018");//valid
		test.put("beneficiaryId", "MBEN001004");
		//test.put("beneficiaryId", "MBE004");
		test.put("beneficiaryName", "Shweta");
		test.put("accountNo", "3504276546687");
		test.put("amount", "10");
		//test.put("ifscCode", "ICIC0003476");
		test.put("Ifsc", "ICIC0003476");
		test.put("bankName", "ICICI");
		test.put("transferType", "NEFT");
		test.put("beneficiaryMobile", "9935041287");
    	test.put("merchantTransId", "9935041287a");
    	test.put("agentTransId", "abcd");
//		
//		
		test.put("name", "santosh");
 		test.put("mpin", "1113");
		test.put("otp", "167383");
		//new OxyJWTSignUtil().generateToken(test, key);
		MerchantRequestBean merchantRequest=new MerchantRequestBean ();
		merchantRequest.setAgentId(mid);
		merchantRequest.setRequest(new BPJWTSignUtil().generateToken(test, key));
		HttpServletRequest httpRequest = null;
		//MerchantResponseBean obj =new DMTAPIManager().createWalletRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().getUserDetails(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().resendOtp(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().addBeneficiaryRequest(merchantRequest,httpRequest); 
		//MerchantResponseBean obj =new DMTAPIManager().deactiveBeneficiaryRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().activeBeneficiaryRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().forgotMPINRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().updateMPINRequest(merchantRequest,httpRequest);
		MerchantResponseBean obj =new DMTAPIManager().getUserBalance(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().accountVerificationRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().mrTransferRequest(merchantRequest,httpRequest);
		//MerchantResponseBean obj =new DMTAPIManager().getTransStatus(merchantRequest,httpRequest);
		System.out.println(obj.getRequestId() + "     " + obj.getResponse());
		
		
		
		//System.out.println(new DMTAPIManager().mobileValidation("99341287"));
		
		
		MudraDao mudraDao = new MudraDaoImpl();
		String mobileNo = mudraDao.getSenderMobileNo("MSEN001019").trim();
		 if(!("9935041287".trim().equals(mobileNo)))
		 {
			System.out.println(true);		 }
		 else
		 {
			 System.out.println(false);
		 }
	}

}
