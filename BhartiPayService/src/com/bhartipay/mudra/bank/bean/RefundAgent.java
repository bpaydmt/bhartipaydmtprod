package com.bhartipay.mudra.bank.bean;

public class RefundAgent {
	private String id;
	
	private String status;
	
	private String dramount;
	
	private String narrartion;
	
	private String mobileno;
	
	private String otp;

	
	
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMobileno() {
		return mobileno;
	}

	public String getDramount() {
		return dramount;
	}

	public void setDramount(String dramount) {
		this.dramount = dramount;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getNarrartion() {
		return narrartion;
	}

	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}
}
