package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class AgentLedgerBeanBank implements Serializable {
	
	
	private String 	txnDate;
	private String 	userid;
	private String 	txntype;
	private String 	resptxnid;
	private String 	mobileno;
	private String 	accountno;
	private String 	name;
	private String 	ifsccode;
	private BigDecimal 	dramount;
	private double 	txncredit;
	private double 	closingbal;
	
	
	
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getTxntype() {
		return txntype;
	}
	public void setTxntype(String txntype) {
		this.txntype = txntype;
	}
	public String getResptxnid() {
		return resptxnid;
	}
	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public BigDecimal getDramount() {
		return dramount;
	}
	public void setDramount(BigDecimal dramount) {
		this.dramount = dramount;
	}
	public double getTxncredit() {
		return txncredit;
	}
	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}
	public double getClosingbal() {
		return closingbal;
	}
	public void setClosingbal(double closingbal) {
		this.closingbal = closingbal;
	}
	

}
