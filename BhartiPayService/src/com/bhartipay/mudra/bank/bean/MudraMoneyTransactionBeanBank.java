package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "remittanceledger")
public class MudraMoneyTransactionBeanBank implements Cloneable, Serializable {

	@Id
	@Column(name = "id", length = 30)
	private String id;

	@Column(name = "txnid", length = 30)
	private String txnId;

	@Column(name = "walletid", nullable = false, length = 30)
	private String walletId;

	@Column(name = "senderid", nullable = false, length = 30)
	private String senderId;

	@Column(name = "beneficiaryid", nullable = false, length = 30)
	private String beneficiaryId;

	@Column(name = "dramount", nullable = false, columnDefinition = "Decimal(10,2) default '00.00'")
	private double drAmount;

	@Column(name = "cramount", nullable = false, columnDefinition = "Decimal(10,2) default '00.00'")
	private double crAmount;

	@Column(name = "narrartion", nullable = false, length = 50)
	private String narrartion;

	@Column(name = "status", nullable = false, length = 200)
	private String status;

	@Column(name = "bankrrn", length = 200)
	private String bankRrn;

	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date ptytransdt;

	@Column(name = "remark", length = 50)
	private String remark;

	@Column(name = "bankresp", length = 5000)
	private String bankResp;

	@Column(name = "merchanttransid", length = 50)
	private String merchantTransId;

	@Column(name = "agentid", nullable = true, length = 30)
	private String agentid;

	@Column(name = "usernarration", length = 300)
	private String userNarration;

	@Transient
	private double surChargeAmount;

	@Transient
	private double txnAmount;

	@Transient
	private String userId;

	@Transient
	private String statusCode;

	@Transient
	private String statusDesc;

	@Transient
	private String accHolderName;

	@Transient
	private String ipiemi;

	@Transient
	private String agent;

	@Transient
	private double verificationAmount;

	@Transient
	private String verificationDesc;

	@Transient
	private String verified;
	@Transient
	private String CHECKSUMHASH;
	
	
	

	

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getUserNarration() {
		return userNarration;
	}

	public void setUserNarration(String userNarration) {
		this.userNarration = userNarration;
	}

	public String getVerified() {
		return verified;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getVerificationDesc() {
		return verificationDesc;
	}

	public void setVerificationDesc(String verificationDesc) {
		this.verificationDesc = verificationDesc;
	}

	public String getMerchantTransId() {
		return merchantTransId;
	}

	public void setMerchantTransId(String merchantTransId) {
		this.merchantTransId = merchantTransId;
	}

	public String getBankResp() {
		return bankResp;
	}

	public void setBankResp(String bankResp) {
		this.bankResp = bankResp;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getVerificationAmount() {
		return verificationAmount;
	}

	public void setVerificationAmount(double verificationAmount) {
		this.verificationAmount = verificationAmount;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public double getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(double txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBankRrn() {
		return bankRrn;
	}

	public void setBankRrn(String bankRrn) {
		this.bankRrn = bankRrn;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public double getDrAmount() {
		return drAmount;
	}

	public void setDrAmount(double drAmount) {
		this.drAmount = drAmount;
	}

	public double getCrAmount() {
		return crAmount;
	}

	public void setCrAmount(double crAmount) {
		this.crAmount = crAmount;
	}

	public String getNarrartion() {
		return narrartion;
	}

	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getPtytransdt() {
		return ptytransdt;
	}

	public void setPtytransdt(Date ptytransdt) {
		this.ptytransdt = ptytransdt;
	}

	public double getSurChargeAmount() {
		return surChargeAmount;
	}

	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
