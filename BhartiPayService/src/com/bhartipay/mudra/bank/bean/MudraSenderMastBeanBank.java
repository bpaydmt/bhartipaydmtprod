package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mudrasendermast")
//@Table(name = "banksendermast")

public class MudraSenderMastBeanBank implements Serializable{

	
	private static final long serialVersionUID = 2222256954892985829L;

	@Id
	@Column(name="id",length =30)
	private String id;
	
	@Column(name = "aggreatorid", nullable = false, length = 10)
	private String aggreatorId;
	
	@Column(name = "agentid", nullable = false, length = 10)
	private String agentId;
	
	@Column(name = "mobileno",  nullable = false, length = 10)
	private String mobileNo;
	
	@Column(name = "emailid", length = 100)
	private String emailId;
	
	@Column(name = "firstname", nullable = false, length = 100)
	private String firstName;
	
	@Column(name = "lastname", length = 100)
	private String lastName;
	
	@Column(name = "mpin", nullable = false, length = 100)
	private String mpin;
	
	@Column(name = "status", nullable = false, length = 20)
	private String status;
	
	@Column(name = "kycstatus", nullable = false, length = 20)
	private String kycStatus;
	
	@Column(name="walletbalance",nullable = false, columnDefinition="Decimal(10,2) default '00.00'")
	private double walletBalance;
	
	@Column(name="transferlimit",nullable = false, columnDefinition="Decimal(10,2) default '20000.00'")
	private double transferLimit;
	
	@Column(name="impsBCLimit",nullable = false, columnDefinition="Decimal(10,2) default '25000.00'")
	private double impsBCLimit;
	
	@Column(name="yearlytransferlimit",nullable = false, columnDefinition="Decimal(10,2) default '50000.00'")
	private double yearlyTransferLimit;
	
	@Column(name = "ispanvalidate", nullable = false, columnDefinition = "int default 0")
	private int isPanValidate;
	
		
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date creationdate;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 500 )
	private String agent;
	
	
	@Column(name = "favourite",  length = 200, nullable = false, columnDefinition=" default ''")
	private String favourite;
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusDesc;
	
	@Transient
	private String otp;
	
	@Transient
	private List <MudraBeneficiaryMastBeanBank> beneficiaryList;
	
	@Transient
	List<SenderFavouriteViewBeanBank> senderFavouriteList;
	
	
	@Transient
	private String exportMobile;
	
	@Transient
	private String CHECKSUMHASH;
	
	
	
	
	
	
	
	
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public double getYearlyTransferLimit() {
		return yearlyTransferLimit;
	}

	public void setYearlyTransferLimit(double yearlyTransferLimit) {
		this.yearlyTransferLimit = yearlyTransferLimit;
	}

	public int getIsPanValidate() {
		return isPanValidate;
	}

	public void setIsPanValidate(int isPanValidate) {
		this.isPanValidate = isPanValidate;
	}

	public String getFavourite() {
		return favourite;
	}

	public void setFavourite(String favourite) {
		this.favourite = favourite;
	}

	public String getExportMobile() {
		return exportMobile;
	}

	public void setExportMobile(String exportMobile) {
		this.exportMobile = exportMobile;
	}

	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public double getTransferLimit() {
		return transferLimit;
	}

	public void setTransferLimit(double transferLimit) {
		this.transferLimit = transferLimit;
	}

	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public List<MudraBeneficiaryMastBeanBank> getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List<MudraBeneficiaryMastBeanBank> beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public List<SenderFavouriteViewBeanBank> getSenderFavouriteList() {
		return senderFavouriteList;
	}

	public void setSenderFavouriteList(List<SenderFavouriteViewBeanBank> senderFavouriteList) {
		this.senderFavouriteList = senderFavouriteList;
	}
	
	

	

	
	
}
