package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MerchantDmtTransBeanBank implements Serializable{
	
	
/*	private String merchantid;
	private String senderid;
	private String sendermobileno;
	private String beneficiaryname;
	private String bankname;
	private String accountno;
	private String transfertype;
	private BigDecimal amount;
	private String transid;
	private String transdate;
	private String status;
	private String merchant;*/
	
	//SELECT transid AS agentTransId,merchanttransid AS mrTransId,status ,amount,transdate as transDateTime,accountno as benefAccNo,transfertype as remark FROM merchant_dmt_trans WHERE merchantid='AGGR001035' AND sendermobileno='9935041287'
	
	
	private String agentTransId;
	private String mrTransId;
	private String status;
	private BigDecimal amount;
	private String transDateTime;
	private String benefAccNo;
	private String remark;
	private String mobileNo;
	
	
	
	
	
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getAgentTransId() {
		return agentTransId;
	}
	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}
	public String getMrTransId() {
		return mrTransId;
	}
	public void setMrTransId(String mrTransId) {
		this.mrTransId = mrTransId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getTransDateTime() {
		return transDateTime;
	}
	public void setTransDateTime(String transDateTime) {
		this.transDateTime = transDateTime;
	}
	public String getBenefAccNo() {
		return benefAccNo;
	}
	public void setBenefAccNo(String benefAccNo) {
		this.benefAccNo = benefAccNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
