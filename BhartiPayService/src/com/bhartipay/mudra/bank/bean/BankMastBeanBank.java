package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bankmast")
public class BankMastBeanBank implements Serializable{
	
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column( name ="id", length=5, nullable = false)
	private int id;	
	
	@Column( name = "bankname", length=200, nullable = false)
	private String bankName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	
	
	
	
	

}
