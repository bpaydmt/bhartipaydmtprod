package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.util.List;


public class BankDetailsBeanBank implements Serializable{
	
	private String bankId;
	
	
	
	private String bankName;
	private String beneficiaryAccountNo;
	private String ifscCode;
	private String branchName;
	private String city;
	private String state ;
	private String address  ;
	
	private String statusCode;
	private String statusDesc;
	
	
	List<BankDetailsBeanBank> bankDetailsList;
	
	
		
	public List<BankDetailsBeanBank> getBankDetailsList() {
		return bankDetailsList;
	}
	public void setBankDetailsList(List<BankDetailsBeanBank> bankDetailsList) {
		this.bankDetailsList = bankDetailsList;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}
	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	
	
	
	
	
	
	
	
	

}
