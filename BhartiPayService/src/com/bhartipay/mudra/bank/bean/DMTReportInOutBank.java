package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.util.List;

public class DMTReportInOutBank implements Serializable {
	
	private String agentId;
	private String stDate; 
	private String edDate;
	private String mobileNo;
	private List <TransacationLedgerBeanBank> transList;
	private List <AgentLedgerBeanBank> agentLedger;
	private List <SenderLedgerBeanBank> senderLedgerList;
	
	
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEdDate() {
		return edDate;
	}
	public void setEdDate(String edDate) {
		this.edDate = edDate;
	}
	
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public List<TransacationLedgerBeanBank> getTransList() {
		return transList;
	}
	public void setTransList(List<TransacationLedgerBeanBank> transList) {
		this.transList = transList;
	}
	public List<AgentLedgerBeanBank> getAgentLedger() {
		return agentLedger;
	}
	public void setAgentLedger(List<AgentLedgerBeanBank> agentLedger) {
		this.agentLedger = agentLedger;
	}
	public List<SenderLedgerBeanBank> getSenderLedgerList() {
		return senderLedgerList;
	}
	public void setSenderLedgerList(List<SenderLedgerBeanBank> senderLedgerList) {
		this.senderLedgerList = senderLedgerList;
	}
	
	
	
	
	
	
	
	

}
