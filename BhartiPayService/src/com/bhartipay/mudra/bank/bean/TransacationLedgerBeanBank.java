package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransacationLedgerBeanBank implements Serializable{
	
private String 	agentid;	
private String 	senderid;
private String ptytransdt;
private String txnid;
private BigDecimal dramount;
private String narrartion;
private String mobileno;
private String firstname;
private String lastname;
private String accountno;
private String name;
private String ifsccode;
private String bankname;
private String bankrrn;
private String status;



public String getAgentid() {
	return agentid;
}
public void setAgentid(String agentid) {
	this.agentid = agentid;
}
public String getSenderid() {
	return senderid;
}
public void setSenderid(String senderid) {
	this.senderid = senderid;
}
public String getPtytransdt() {
	return ptytransdt;
}
public void setPtytransdt(String ptytransdt) {
	this.ptytransdt = ptytransdt;
}
public String getTxnid() {
	return txnid;
}
public void setTxnid(String txnid) {
	this.txnid = txnid;
}
public BigDecimal getDramount() {
	return dramount;
}
public void setDramount(BigDecimal dramount) {
	this.dramount = dramount;
}
public String getNarrartion() {
	return narrartion;
}
public void setNarrartion(String narrartion) {
	this.narrartion = narrartion;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getAccountno() {
	return accountno;
}
public void setAccountno(String accountno) {
	this.accountno = accountno;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getIfsccode() {
	return ifsccode;
}
public void setIfsccode(String ifsccode) {
	this.ifsccode = ifsccode;
}
public String getBankname() {
	return bankname;
}
public void setBankname(String bankname) {
	this.bankname = bankname;
}
public String getBankrrn() {
	return bankrrn;
}
public void setBankrrn(String bankrrn) {
	this.bankrrn = bankrrn;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}








}
