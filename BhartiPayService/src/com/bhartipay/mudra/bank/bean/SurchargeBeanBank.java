package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;

public class SurchargeBeanBank implements Serializable{
	
	private String agentId;
	private  double amount; 
	private  double surchargeAmount; 
	private  String status;
	private  int txnCode;
	
	

	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	public double getSurchargeAmount() {
		return surchargeAmount;
	}
	public void setSurchargeAmount(double surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(int txnCode) {
		this.txnCode = txnCode;
	}
	

}
