package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "senderkycmast")
public class EkycResponseBeanBank implements Serializable{
	
	@Id
	@Column(name = "kyctxnid", nullable = false, length = 10)
	private String kyctxnid;
	
	@Column(name = "senderid", nullable = false, length = 10)
	private String senderid;
	
	@Column(name = "status",  length = 10)
	private String status; 
	
	@Column(name = "agentid",nullable = false, length = 10)
	private String agentid; 
	
	@Column(name = "others",  length = 10)
	private String others; 
	
	@Column(name = "aggreator", length = 10)
	private String aggreator; 
	
	@Column(name = "deviceid", length = 10)
	private String deviceid; 
	
	@Column(name = "time", length = 10)
	private String time; 
	
	@Column(name = "servicetype",  length = 10)
	private String servicetype; 
	
	@Column(name = "UID",  length = 10)
	private String UID ;
	
	@Column(name = "Poiname",  length = 10)
	private String Poiname; 
	
	@Column(name = "Poidob",  length = 10)
	private String Poidob;
	
	@Column(name = "Poigender",  length = 10)
	private String Poigender; 
	
	@Column(name = "Poiphone",  length = 10)
	private String Poiphone;
	
	@Column(name = "Poiemail",  length = 10)
	private String Poiemail; 
	
	@Column(name = "Poaco",  length = 10)
	private String Poaco; 
	
	@Column(name = "Poadist",  length = 10)
	private String Poadist; 
	
	@Column(name = "Poahouse",  length = 10)
	private String Poahouse; 
	
	@Column(name = "Poaloc",  length = 10)
	private String Poaloc;
	
	@Column(name = "Poapc",  length = 10)
	private String Poapc; 
	
	@Column(name = "Poastate",  length = 10)
	private String Poastate; 

	@Column(name = "Poavtc",  length = 10)
	private String Poavtc; 
	
	@Column(name = "PoavtcCode",  length = 10)
	private String PoavtcCode; 

	@Column(name = "Poastreet",  length = 10)
	private String Poastreet;
	
	@Column(name = "Poalm",  length = 10)
	private String Poalm;
	
	@Column(name = "Poasubdist",  length = 10)
	private String Poasubdist; 
	
	@Column(name = "Poapo",  length = 10)
	private String Poapo; 
	
	@Column(name = "Pht",  length = 10)
	private String Pht;
	
	@Column(name = "CreatedDate",  length = 10)
	private String CreatedDate;
	
	@Column(name = "UserID",  length = 10)
	private String UserID;
	
	@Column(name = "TxnID",  length = 10)
	private String TxnID;
	
	@Column(name = "RCode",  length = 10)
	private String RCode; 
	
	@Column(name = "RTimeStamp",  length = 10)
	private String RTimeStamp;
	
	
	@Transient
	private String Rtxn;
	
	@Transient
	private String Rts;
	
	@Transient
	private String Uuid;
	
	public String getKyctxnid() {
		return kyctxnid;
	}
	public void setKyctxnid(String kyctxnid) {
		this.kyctxnid = kyctxnid;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public String getAggreator() {
		return aggreator;
	}
	public void setAggreator(String aggreator) {
		this.aggreator = aggreator;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getServicetype() {
		return servicetype;
	}
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getPoiname() {
		return Poiname;
	}
	public void setPoiname(String poiname) {
		Poiname = poiname;
	}
	public String getPoidob() {
		return Poidob;
	}
	public void setPoidob(String poidob) {
		Poidob = poidob;
	}
	public String getPoigender() {
		return Poigender;
	}
	public void setPoigender(String poigender) {
		Poigender = poigender;
	}
	public String getPoiphone() {
		return Poiphone;
	}
	public void setPoiphone(String poiphone) {
		Poiphone = poiphone;
	}
	public String getPoiemail() {
		return Poiemail;
	}
	public void setPoiemail(String poiemail) {
		Poiemail = poiemail;
	}
	public String getPoaco() {
		return Poaco;
	}
	public void setPoaco(String poaco) {
		Poaco = poaco;
	}
	public String getPoadist() {
		return Poadist;
	}
	public void setPoadist(String poadist) {
		Poadist = poadist;
	}
	public String getPoahouse() {
		return Poahouse;
	}
	public void setPoahouse(String poahouse) {
		Poahouse = poahouse;
	}
	public String getPoaloc() {
		return Poaloc;
	}
	public void setPoaloc(String poaloc) {
		Poaloc = poaloc;
	}
	public String getPoapc() {
		return Poapc;
	}
	public void setPoapc(String poapc) {
		Poapc = poapc;
	}
	public String getPoastate() {
		return Poastate;
	}
	public void setPoastate(String poastate) {
		Poastate = poastate;
	}
	public String getPoavtc() {
		return Poavtc;
	}
	public void setPoavtc(String poavtc) {
		Poavtc = poavtc;
	}
	public String getPoavtcCode() {
		return PoavtcCode;
	}
	public void setPoavtcCode(String poavtcCode) {
		PoavtcCode = poavtcCode;
	}
	public String getPoastreet() {
		return Poastreet;
	}
	public void setPoastreet(String poastreet) {
		Poastreet = poastreet;
	}
	public String getPoalm() {
		return Poalm;
	}
	public void setPoalm(String poalm) {
		Poalm = poalm;
	}
	public String getPoasubdist() {
		return Poasubdist;
	}
	public void setPoasubdist(String poasubdist) {
		Poasubdist = poasubdist;
	}
	public String getPoapo() {
		return Poapo;
	}
	public void setPoapo(String poapo) {
		Poapo = poapo;
	}
	public String getPht() {
		return Pht;
	}
	public void setPht(String pht) {
		Pht = pht;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getTxnID() {
		return TxnID;
	}
	public void setTxnID(String txnID) {
		TxnID = txnID;
	}
	public String getRCode() {
		return RCode;
	}
	public void setRCode(String rCode) {
		RCode = rCode;
	}
	public String getRTimeStamp() {
		return RTimeStamp;
	}
	public void setRTimeStamp(String rTimeStamp) {
		RTimeStamp = rTimeStamp;
	}
	public String getRtxn() {
		return Rtxn;
	}
	public void setRtxn(String rtxn) {
		Rtxn = rtxn;
	}
	public String getRts() {
		return Rts;
	}
	public void setRts(String rts) {
		Rts = rts;
	}
	public String getUuid() {
		return Uuid;
	}
	public void setUuid(String uuid) {
		Uuid = uuid;
	}
	

}
