package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class FundTransactionSummaryBeanBank implements Serializable{
	
	private String senderMobile;
	private String beneficiaryName;
	private String beneficiaryIFSC;
	private String beneficiaryAccNo;
	private double amount;
	private String charges;
	private String mode;
	private List<MudraMoneyTransactionBeanBank> mudraMoneyTransactionBean=new ArrayList<MudraMoneyTransactionBeanBank>();
	private String statusCode;
	private String statusDesc;
	private double agentWalletAmount;
	
	
	public double getAgentWalletAmount() {
		return agentWalletAmount;
	}
	public void setAgentWalletAmount(double agentWalletAmount) {
		this.agentWalletAmount = agentWalletAmount;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryIFSC() {
		return beneficiaryIFSC;
	}
	public void setBeneficiaryIFSC(String beneficiaryIFSC) {
		this.beneficiaryIFSC = beneficiaryIFSC;
	}
	public String getBeneficiaryAccNo() {
		return beneficiaryAccNo;
	}
	public void setBeneficiaryAccNo(String beneficiaryAccNo) {
		this.beneficiaryAccNo = beneficiaryAccNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public List<MudraMoneyTransactionBeanBank> getMudraMoneyTransactionBean() {
		return mudraMoneyTransactionBean;
	}
	public void setMudraMoneyTransactionBean(List<MudraMoneyTransactionBeanBank> mudraMoneyTransactionBean) {
		this.mudraMoneyTransactionBean = mudraMoneyTransactionBean;
	}
	
	
	
	
	
	
	
	
	
	

}
