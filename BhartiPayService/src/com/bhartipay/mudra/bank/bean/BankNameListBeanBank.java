package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;

public class BankNameListBeanBank implements Serializable{
	
	
	private String id;	
	private String bankName;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	

}
