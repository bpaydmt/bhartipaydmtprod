package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mudrasenderpanmast")

public class MudraSenderPanDetailsBank implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",length=12)
	private int id;
	
	
	@Column(name = "aggreatorid", nullable = false, length = 20)
	private String aggreatorId;
	
	@Column(name = "senderid", nullable = false, length = 20)
	private String senderId;
	
	
	@Column(name = "sendername", nullable = false, length = 100)
	private String senderName;
	
	@Column(name = "prooftype ", nullable = false, length = 10)
	private String proofType;
	
	
	@Column(name = "pannumber ",  length = 10)
	private String panNumber;
	
	
	@Column(name = "prooftypepic", unique = false, nullable = false, length = 1000)
    private String proofTypePic;
	
	
	@Column(name="status",length =10,nullable = false)
	private String status;
	
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date loaddate;


	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "useragent",  length = 300 )
	private String userAgent;
	
	
	@Column(name = "comment",  length = 300 )
	private String comment;
	
	
	@Column(name ="approvedate", columnDefinition="TIMESTAMP")
	private Date approveDate;
	
	
	
	@Transient
	private String statusCode;
	
	@Transient
	private String statusMessage;
	
	
	
	
	
	@Transient
	private String stDate;
	
	@Transient
	private String endDate;
	
	
	
	
	
	
	
	
	


	public String getSenderName() {
		return senderName;
	}


	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}


	public String getStDate() {
		return stDate;
	}


	public void setStDate(String stDate) {
		this.stDate = stDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getAggreatorId() {
		return aggreatorId;
	}


	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getSenderId() {
		return senderId;
	}


	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}


	public String getProofType() {
		return proofType;
	}


	public void setProofType(String proofType) {
		this.proofType = proofType;
	}


	public String getPanNumber() {
		return panNumber;
	}


	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}




	public String getProofTypePic() {
		return proofTypePic;
	}


	public void setProofTypePic(String proofTypePic) {
		this.proofTypePic = proofTypePic;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getLoaddate() {
		return loaddate;
	}


	public void setLoaddate(Date loaddate) {
		this.loaddate = loaddate;
	}


	public String getIpiemi() {
		return ipiemi;
	}


	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}


	public String getUserAgent() {
		return userAgent;
	}


	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public Date getApproveDate() {
		return approveDate;
	}


	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getStatusMessage() {
		return statusMessage;
	}


	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	
	
	
	
	
	
	
	

}
