package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SenderLedgerBeanBank implements Serializable{
	
	private String ptytransdt;
	private String senderid;
	private String id;
	/*private String txnid;*/
	private String narrartion;
	private String mobileno;
	private String accountno;
	private String ifsccode;
	private BigDecimal cramount;
	private BigDecimal dramount;
	private String bankrrn;
	private String remark;
	
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPtytransdt() {
		return ptytransdt;
	}
	public void setPtytransdt(String ptytransdt) {
		this.ptytransdt = ptytransdt;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	/*public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}*/
	public String getNarrartion() {
		return narrartion;
	}
	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	
	
	
	
	public BigDecimal getCramount() {
		return cramount;
	}
	public void setCramount(BigDecimal cramount) {
		this.cramount = cramount;
	}
	public BigDecimal getDramount() {
		return dramount;
	}
	public void setDramount(BigDecimal dramount) {
		this.dramount = dramount;
	}
	public String getBankrrn() {
		return bankrrn;
	}
	public void setBankrrn(String bankrrn) {
		this.bankrrn = bankrrn;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
	

}
