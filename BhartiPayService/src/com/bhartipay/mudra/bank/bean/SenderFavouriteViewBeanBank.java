package com.bhartipay.mudra.bank.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SenderFavouriteViewBeanBank implements Serializable{
	
	
	private String senderid;           
	private int favouritid ;           
	private String name   ;           
	private String mobileno ;           
	private String agentid  ;
	private BigDecimal walletbalance ;           
	private BigDecimal transferlimit  ;
	private String  kycstatus;
	
	
	
	
	
	
	public String getKycstatus() {
		return kycstatus;
	}
	public void setKycstatus(String kycstatus) {
		this.kycstatus = kycstatus;
	}
	public BigDecimal getWalletbalance() {
		return walletbalance;
	}
	public void setWalletbalance(BigDecimal walletbalance) {
		this.walletbalance = walletbalance;
	}
	public BigDecimal getTransferlimit() {
		return transferlimit;
	}
	public void setTransferlimit(BigDecimal transferlimit) {
		this.transferlimit = transferlimit;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public int getFavouritid() {
		return favouritid;
	}
	public void setFavouritid(int favouritid) {
		this.favouritid = favouritid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}  
	
	
	
	
	
	

	

}
