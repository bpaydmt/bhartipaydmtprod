package com.bhartipay.mudra.bank.bean;

public class MerchantResponseBeanBank {
	private String requestId;
	private String response;
	
	
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	
	
	
	

}
