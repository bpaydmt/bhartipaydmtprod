package com.bhartipay.mudra.bank.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cache.CacheFactory;
import com.bhartipay.cache.CacheModel;
import com.bhartipay.cache.TableCache;
import com.bhartipay.cache.TableCacheImpl;
import com.bhartipay.mudra.bank.bean.RefundAgent;
import com.bhartipay.mudra.bank.bean.ResponseBeanBank;
import com.bhartipay.mudra.bank3.payoutapi.PayoutMaster;
import com.bhartipay.mudra.bank3.payoutapi.PayoutMasterDMT;
import com.bhartipay.mudra.bean.AgentLedgerBean;
import com.bhartipay.mudra.bean.BankDetailsBean;
import com.bhartipay.mudra.bean.BankMastBean;
import com.bhartipay.mudra.bean.BankNameListBean;
import com.bhartipay.mudra.bean.DMTApiRequest;
import com.bhartipay.mudra.bean.DMTReportInOut;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.KycConfigBean;
import com.bhartipay.mudra.bean.MerchantDmtTransBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderPanDetails;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.SenderFavouriteBean;
import com.bhartipay.mudra.bean.SenderFavouriteViewBean;
import com.bhartipay.mudra.bean.SenderKYCBean;
import com.bhartipay.mudra.bean.SenderLedgerBean;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.bean.TransacationLedgerBean;
import com.bhartipay.mudra.bean.VerifiedBeneficiaryData;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.transaction.bean.RefundTransactionBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.EncryptionByEnc256;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.AadharShilaDMT;
import com.bhartipay.util.thirdParty.AadharShilaDMTResponse;
import com.bhartipay.util.thirdParty.FINOIMPSImplementation;
import com.bhartipay.util.thirdParty.bean.FINOCredentials;
import com.bhartipay.util.thirdParty.bean.FINORequestBean;
import com.bhartipay.util.thirdParty.bean.FINORespData;
import com.bhartipay.util.thirdParty.bean.FINOResponseBean;
import com.google.gson.Gson;

/**
 * @author Bhartipay
 *
 */
public class MudraDaoBankImpl implements MudraDaoBank {
	
	private static final Logger logger = Logger.getLogger(MudraDaoBankImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	OTPGeneration oTPGeneration = new OTPGeneration();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
//    private static final String PKEY="d1f01fcd-9b79-40ff-b93b-2c10b5ef856d";//for production.
//   private static String PKEY = "21d805f7-a299-4eac-804c-1f9c5649990c";// for test server.

   //MudraMoneyTransactionBean MMTB=new MudraMoneyTransactionBean();
   MudraBeneficiaryBank mudraBeneficiaryBank=new MudraBeneficiaryBank();
	/*List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
			.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
			.add(Restrictions.ne("status", "Deleted"))
			.addOrder(Order.asc("status")).list();

	//mudraBeneficiaryBank.setList(benfList);
*/	
   	@SuppressWarnings("unchecked")
	@Override
	public MudraSenderBank validateSender(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		
	     
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*******RequestId******" + requestId
				+ "|validateSender()|"
				+ mudraSenderBank.getMobileNo());
	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = null;
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()
					|| mudraSenderBank.getMobileNo().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo()))
					/*.add(Restrictions.eq("aggreatorId", mudraSenderBank.getAggreatorId()))*/.list();
			WalletMastBean walletMast=(WalletMastBean)session.get(WalletMastBean.class,mudraSenderBank.getAgentId());
			AadharShilaDMT aadharShilaDMT=new AadharShilaDMT();
			AadharShilaDMTResponse dmtResponse=null;
			if (senderList.size() == 0) {
				WalletConfiguration walletConfiguration = commanUtilDao
						.getWalletConfiguration(mudraSenderBank.getAggreatorId());
				transaction = session.beginTransaction();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*******RequestId******" + requestId
						+ "Sender Not Register With Us Send OTP for Register");
				newOtp = oTPGeneration.generateOTP();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId******" + requestId
						+ "Inside" + newOtp);
				if (newOtp != null) {
					Query insertOtp = session.getNamedQuery("callOTPMANAGE")
							.setParameter("mobile", mudraSenderBank.getMobileNo())
							.setParameter("aggreatorid", mudraSenderBank.getAggreatorId())
							.setParameter("otp", CommanUtil.SHAHashing256(newOtp));
							insertOtp.executeUpdate();
				}
				transaction.commit();
//				if (!(newOtp == null)) {
//					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId******" + requestId
//							+ "Inside otp send on mobile"
//							+ mudraSenderBank.getMobileNo() + "===otp===" + newOtp);
//					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId******" + requestId
//							+ "Inside otp1 send on mobile"
//							+ mudraSenderBank.getMobileNo() + "===otp===" + mudraSenderBank.getMobileNo());
//
////					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
////							mudraSenderBank.getMobileNo(),
////							commanUtilDao.getsmsTemplet("dmtRegOtp", mudraSenderBank.getAggreatorId())
////									/*
////									 * .replace("<<APPNAME>>",
////									 * walletConfiguration.getAppname())
////									 */.replace("<<OTP>>", newOtp),
////							"SMS", mudraSenderBank.getAggreatorId(), "", "", "OTP");
////					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
//					
//					dmtResponse=aadharShilaDMT.senderRegistrationOTP(mudraSenderBank.getMobileNo(),walletMast.getId());
//				}
//				if(dmtResponse!=null&&"SUCCESS".equalsIgnoreCase(dmtResponse.getErrorMsg())&& "00".equalsIgnoreCase(dmtResponse.getErrorCode())) {
//					mudraSenderBank.setStatusCode("2000");
//					mudraSenderBank.setStatusDesc("OTP has been sent on Mobile Number.");
//				} else {
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderBank.getMobileNo(),
							commanUtilDao.getsmsTemplet("dmtRegOtp", mudraSenderBank.getAggreatorId())
									 .replace("<<APPNAME>>", walletConfiguration.getAppname())
									 .replace("<<OTP>>", newOtp),
							"SMS", mudraSenderBank.getAggreatorId(), "", "", "OTP");
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					mudraSenderBank.setStatusCode("2000");
					mudraSenderBank.setStatusDesc("OTP has been sent on Mobile Number.");
//				}
			} else {
				
				mudraSenderBank = senderList.get(0);
				
//				dmtResponse=aadharShilaDMT.fetchSender(mudraSenderBank.getMobileNo(),walletMast.getId());
//				if(dmtResponse!=null&&"SUCCESS".equalsIgnoreCase(dmtResponse.getErrorMsg())&& "00".equalsIgnoreCase(dmtResponse.getErrorCode())) {
//				mudraSenderBank.setYearlyTransferLimit(dmtResponse.getData().getWalletbal());
//				}
				
				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();

				mudraSenderBank.setBeneficiaryList(benfList);
				List<SenderFavouriteViewBean> senderFavouriteList = getFavouriteList(mudraSenderBank.getAgentId(),
						serverName, requestId);
				mudraSenderBank.setSenderFavouriteList(senderFavouriteList);
				mudraSenderBank.setStatusCode("1000");
				mudraSenderBank.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId******" + requestId
					+ "problem in validateSender" + e.getMessage()+ " "+e);
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;	
	
}

	// changes done
	@Override
	public MudraSenderBank registerSender(MudraSenderBank mudraSenderBank, String serverName, String requestId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "", "",
				serverName + "*****RequestId*******" + requestId + "|registerSender()|"
						+ mudraSenderBank.getMobileNo());

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		Transaction transaction = null;
		String nmpin = "";
		try {
			if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()
					|| mudraSenderBank.getMobileNo().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}
			if (mudraSenderBank.getFirstName() == null || mudraSenderBank.getFirstName().isEmpty()) {
				mudraSenderBank.setStatusCode("1101");
				mudraSenderBank.setStatusDesc("First Name can't be empty.");
				return mudraSenderBank;
			}

			if (mudraSenderBank.getMpin() == null || mudraSenderBank.getMpin().isEmpty()
					|| (mudraSenderBank.getMpin().length() < 4 || mudraSenderBank.getMpin().length() > 6)) {
				mudraSenderBank.setStatusCode("1102");
				mudraSenderBank.setStatusDesc("Invalid MPIN.");
				return mudraSenderBank;
			}

			if (mudraSenderBank.getOtp() == null || mudraSenderBank.getOtp().isEmpty()) {
				mudraSenderBank.setStatusCode("1103");
				mudraSenderBank.setStatusDesc("OTP can't be empty.");
				return mudraSenderBank;
			}

			if (mudraSenderBank.getAgentId() == null || mudraSenderBank.getAgentId().isEmpty()) {
				mudraSenderBank.setStatusCode("1117");
				mudraSenderBank.setStatusDesc("Agent Id can't be empty.");
				return mudraSenderBank;
			}

			// validate OTP
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "",
					"", serverName + "*****RequestId*******" + requestId + "|registerSender()|"
							+ mudraSenderBank.getMobileNo());

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo()))
					.list();
			if (senderList.size() == 0) {

				Query queryOTP = session.createQuery(
						" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
				queryOTP.setString("mobile", mudraSenderBank.getMobileNo());
				queryOTP.setString("otp", CommanUtil.SHAHashing256(mudraSenderBank.getOtp()));
				queryOTP.setString("aggreatorid", mudraSenderBank.getAggreatorId());
				List list = queryOTP.list();

				WalletMastBean walletMast = (WalletMastBean) session.get(WalletMastBean.class,
						mudraSenderBank.getAgentId());
				
				AadharShilaDMT aadharShilaDMT = new AadharShilaDMT();
				AadharShilaDMTResponse response = null;
				if(list!=null && list.size()>0 && new OTPGeneration().validateOTP(mudraSenderBank.getOtp())) {
				response = aadharShilaDMT.senderRegistration(mudraSenderBank,
						walletMast.getId(),walletMast.getDob(),walletMast.getAddress1());
				} else {
					mudraSenderBank.setStatusCode("1105");
					mudraSenderBank.setStatusDesc("OTP has expired.");
					return mudraSenderBank;
				}

				if (response != null && (("SUCCESS".equalsIgnoreCase(response.getErrorMsg())
						&& "00".equalsIgnoreCase(response.getErrorCode()))
						|| ("Customer Id is already present.".equalsIgnoreCase(response.getErrorMsg())
								&& "V0003".equalsIgnoreCase(response.getErrorCode())))) {
							
					nmpin = mudraSenderBank.getMpin();
					transaction = session.beginTransaction();
					mudraSenderBank.setId(commanUtilDao.getTrxId("MSENDER",mudraSenderBank.getAggreatorId()));
					mudraSenderBank.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.getMpin()));
					mudraSenderBank.setStatus("ACTIVE");
					mudraSenderBank.setKycStatus("MIN KYC");
					mudraSenderBank.setTransferLimit(25000.00);
					mudraSenderBank.setAsRegistration("Y");
					mudraSenderBank.setFavourite("");   
					// mudraSenderBank.setBank(1);//added to make it active
					session.save(mudraSenderBank);
					transaction.commit();
					List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
							.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
							.add(Restrictions.ne("status", "Deleted")).addOrder(Order.asc("status")).list();
					mudraSenderBank.setBeneficiaryList(benfList);
					mudraSenderBank.setStatusCode("1000");
					mudraSenderBank.setStatusDesc("Sender has been registered successfully.");

					/*
					String smstemplet = commanUtilDao.getsmsTemplet("senderReg", mudraSenderBank.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "",
							"", "", "", serverName + "*****RequestId*******" + requestId
									+ "|mudraSenderBank.getMobileNo()" + smstemplet);
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderBank.getMobileNo(), smstemplet.replaceAll("<<MPIN>>", nmpin), "SMS",  
							mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "OTP");
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				   */

				} else {

					if (new OTPGeneration().validateOTP(mudraSenderBank.getOtp())) {
						nmpin = mudraSenderBank.getMpin();
						transaction = session.beginTransaction();
						mudraSenderBank.setId(commanUtilDao.getTrxId("MSENDER",mudraSenderBank.getAggreatorId()));
						mudraSenderBank.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.getMpin()));
						mudraSenderBank.setStatus("ACTIVE");
						mudraSenderBank.setKycStatus("MIN KYC");
						mudraSenderBank.setTransferLimit(25000.00);
						mudraSenderBank.setAsRegistration("N");
						mudraSenderBank.setFavourite("");
						// mudraSenderBank.setBank(1);//added to make it active
						session.save(mudraSenderBank);
						transaction.commit();
						List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
								.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
								.add(Restrictions.ne("status", "Deleted")).addOrder(Order.asc("status")).list();
						mudraSenderBank.setBeneficiaryList(benfList);
						mudraSenderBank.setStatusCode("1000");
						mudraSenderBank.setStatusDesc("Sender has been registered successfully.");
 
				   /*		 
						String smstemplet = commanUtilDao.getsmsTemplet("senderReg", mudraSenderBank.getAggreatorId());
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(),
								"", "", "", "", serverName + "*****RequestId*******" + requestId
										+ "|mudraSenderBank.getMobileNo()" + smstemplet);
						SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
								mudraSenderBank.getMobileNo(), smstemplet.replaceAll("<<MPIN>>", nmpin), "SMS",
								mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "OTP");
						ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			         */
						
					} else {
						mudraSenderBank.setStatusCode("1105");
						mudraSenderBank.setStatusDesc("OTP has expired.");
						return mudraSenderBank;
					}
				}

			} else {
				mudraSenderBank.setStatusCode("11206");
				mudraSenderBank.setStatusDesc("Mobile Number Register With Us.");
				return mudraSenderBank;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "",
					"", serverName + "*****RequestId*******" + requestId + "problem in registerSender" + e.getMessage()
							+ " " + e);
			/*
			 * logger.debug(serverName + "*****RequestId*******" + requestId +
			 * "*******problem in registerSender=========================" + e.getMessage(),
			 * e);
			 */
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;

	}
	
	@Override
	public MudraSenderWallet validateSenderMobile(MudraSenderWallet mudraSenderWallet) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "", "+serverName+"+ mudraSenderWallet.getMobileNo());
	    factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderWallet.setStatusCode("1001");
		mudraSenderWallet.setStatusDesc("Error");
		Transaction transaction = session.beginTransaction();
		try {
		   List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderWallet.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderWallet.getAggreatorId())).list();
		   
		/*	Query query = session.createSQLQuery("select * from mudrasendermast where mobileno='"+mudraSenderWallet.getMobileNo()+"' and aggreatorid='"+mudraSenderWallet.getAggreatorId()+"'");
			List senderList = query.list();
			*/
			if (senderList.size() == 0) {
			
				mudraSenderWallet.setStatusCode("1000");
			 return mudraSenderWallet;	
		   } else {
			mudraSenderWallet.setStatusCode("11206");
			mudraSenderWallet.setStatusDesc("Mobile Number Register With Us.");
			return mudraSenderWallet;
           }

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderWallet.getAggreatorId(),"", "", "", "","problem in registerSenderMobile" + e.getMessage()+" "+e);
		
			mudraSenderWallet.setStatusCode("7000");
			mudraSenderWallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
	   return mudraSenderWallet;
	 }

	


	@Override
	public BankDetailsBean getBankDetails(BankDetailsBean bankDetailsBean, String serverName, String requestId) {
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId*******" + requestId
				+ "getBankDetails()|"
				+ bankDetailsBean.getBankId());
		/*logger.info(serverName + "*****RequestId*******" + requestId
				+ "***Start excution **************11********************** getBankDetails method**:"
				+ bankDetailsBean.getBankId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			if (bankDetailsBean.getBankName() == null || bankDetailsBean.getBankName().isEmpty()) {
				bankDetailsBean.setStatusCode("1107");
				bankDetailsBean.setStatusDesc("Bank Name can't be empty.");
				return bankDetailsBean;
			}

			if (bankDetailsBean.getBeneficiaryAccountNo() == null
					|| bankDetailsBean.getBeneficiaryAccountNo().isEmpty()) {
				bankDetailsBean.setStatusCode("1108");
				bankDetailsBean.setStatusDesc("Beneficiary Account Number can't be empty.");
				return bankDetailsBean;
			}

			bankDetailsBean.setIfscCode("ICIC0003476");
			bankDetailsBean.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			bankDetailsBean.setCity("DELHI");
			bankDetailsBean.setState("DELHI");
			bankDetailsBean.setAddress(" K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");

			bankDetailsBean.setStatusCode("1000");
			bankDetailsBean.setStatusDesc("Bank details populate successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId*******" + requestId
					+ "problem in getBankDetails" + e.getMessage()+ " "+e);
			/*logger.debug(serverName + "*****RequestId*******" + requestId
					+ "***problem in getBankDetails=========================" + e.getMessage(), e);*/
			bankDetailsBean.setStatusCode("7000");
			bankDetailsBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return bankDetailsBean;
	}

	@Override
	public List getBankDetailsByIFSC(BankDetailsBean bankDetailsBean, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId***" + requestId
				+ "|getBankDetailsByIFSC()|"
				+ "getIfscCode() getBankDetailsByIFSC method**:"
				+ bankDetailsBean.getIfscCode()+ "|**1122*** getBankDetails method**:"
						+ bankDetailsBean.getBankId()+ "|**1122**getBankName getBankDetails method**:"
								+ bankDetailsBean.getBranchName() +"|**1122****getBankName getBankDetails method**:"
								+ bankDetailsBean.getState());
		
/*		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId***" + requestId
				+ "**1122*** getBankDetails method**:"
				+ bankDetailsBean.getBankId());
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId***" + requestId
				+ "**1122**getBankName getBankDetails method**:"
				+ bankDetailsBean.getBranchName());
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*****RequestId***" + requestId
				+ "**1122****getBankName getBankDetails method**:
				+ bankDetailsBean.getState());"*/
		/*logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************getIfscCode()*********************** getBankDetailsByIFSC method**:"
				+ bankDetailsBean.getIfscCode());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122********************** getBankDetails method**:"
				+ bankDetailsBean.getBankId());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122****getBankName****************** getBankDetails method**:"
				+ bankDetailsBean.getBranchName());

		logger.info(serverName + "*****RequestId***" + requestId
				+ "******Start excution **************1122****getBankName****************** getBankDetails method**:"
				+ bankDetailsBean.getState());*/

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List bankList = new ArrayList();
		try {

			if (bankDetailsBean.getIfscCode() != null && bankDetailsBean.getIfscCode()
					.length() > 0 /*
									 * !(bankDetailsBean.getIfscCode().isEmpty())
									 */) {

				SQLQuery query = session.createSQLQuery(
						"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE ifsccode like :bankId");
				query.setParameter("bankId", "%" + bankDetailsBean.getIfscCode() + "%");
				// query.setString("bankId", bankDetailsBean.getBankId());
				query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
				bankList = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName")
						.addScalar("address").addScalar("city").addScalar("state").list();
				return bankList;

			} else if (!(bankDetailsBean.getBankId().equalsIgnoreCase("-1"))
					|| !(bankDetailsBean.getBankId().isEmpty())) {
				SQLQuery query = null;
				if (bankDetailsBean.getBranchName() == null || bankDetailsBean.getBranchName().isEmpty()) {
					query = session.createSQLQuery(
							"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId and state=:state");
					query.setString("bankId", bankDetailsBean.getBankId());
					query.setString("state", bankDetailsBean.getState());

				} else {
					query = session.createSQLQuery(
							"SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId and state=:state and branchname like :BranchName");
					query.setString("bankId", bankDetailsBean.getBankId());
					query.setString("state", bankDetailsBean.getState());
					query.setParameter("BranchName", "%" + bankDetailsBean.getBranchName() + "%");
				}

				/*
				 * SQLQuery query=session.createSQLQuery(
				 * "SELECT bankname AS bankName,ifsccode AS ifscCode, branchname AS branchName,address,city,state FROM ifscmaster WHERE bankname =:bankId"
				 * ); query.setString("bankId", bankDetailsBean.getBankId());
				 */
				query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
				bankList = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName")
						.addScalar("address").addScalar("city").addScalar("state").list();

			}

			/*
			 * if (bankDetailsBean.getIfscCode() == null ||
			 * bankDetailsBean.getIfscCode().isEmpty()) {
			 * bankDetailsBean.setStatusCode("1109");
			 * bankDetailsBean.setStatusDesc("IFSC Code can't be empty.");
			 * return bankList; }
			 */

			/*
			 * bankDetailsBean.setIfscCode("ICIC0003476");
			 * bankDetailsBean.setBankName("ICICI BANK LTD");
			 * bankDetailsBean.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			 * bankDetailsBean.setCity("DELHI");
			 * bankDetailsBean.setState("DELHI"); bankDetailsBean.setAddress(
			 * " K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");
			 * bankList.add(bankDetailsBean); BankDetailsBean
			 * bankDetailsBean1=new BankDetailsBean();
			 * bankDetailsBean1.setIfscCode("HDFC0003176");
			 * bankDetailsBean1.setBankName("HDFC BANK ");
			 * bankDetailsBean1.setBranchName("CENTRAL MARKET LAJPAT NAGAR");
			 * bankDetailsBean1.setCity("DELHI");
			 * bankDetailsBean1.setState("DELHI"); bankDetailsBean1.setAddress(
			 * " K-91,CENTRAL MARKET,LAJPAT NAGAR II,NEW DELHI 110024");
			 * bankList.add(bankDetailsBean1);
			 */

			bankDetailsBean.setStatusCode("1000");
			bankDetailsBean.setStatusDesc("Bank details populate successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "|getBankDetailsByIFSC()|"+ "problem in getBankDetailsByIFSC" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*****RequestId***" + requestId
					+ "******problem in getBankDetailsByIFSC=========================" + e.getMessage(), e);*/
			bankDetailsBean.setStatusCode("7000");
			bankDetailsBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return bankList;
	}

	@Override
	public MudraBeneficiaryBank deleteBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
				+  "|deleteBeneficiary()|"
				+ mudraBeneficiaryBank.getId());
		/*logger.info(serverName + "*******RequestId****" + requestId
				+ "******Start excution ************************************* deleteBeneficiary method**:"
				+ mudraBeneficiaryBank.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryBank;
			}

			Object mudraBeneficiaryBankDel = session.load(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());
			if (mudraBeneficiaryBankDel != null) {
				transaction = session.beginTransaction();
				session.delete(mudraBeneficiaryBankDel);
				transaction.commit();
				mudraBeneficiaryBank.setStatusCode("1000");
				mudraBeneficiaryBank.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
					+ "problem in deleteBeneficiary" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId****" + requestId
					+ "******problem in deleteBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}

	@Override
	public MudraBeneficiaryBank acceptBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		MudraSenderBank mudraSenderBank=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
				+"|acceptBeneficiary()|"
				+ mudraBeneficiaryBank.getId());
				
		/*logger.info(serverName + "*******RequestId****" + requestId
				+ "******Start excution ************************************* acceptBeneficiary method**:"
				+ mudraBeneficiaryBank.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryBank;
			}

			MudraBeneficiaryBank mudraBeneficiaryBankDel = (MudraBeneficiaryBank) session
					.load(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());
			if (mudraBeneficiaryBankDel != null) {
				mudraBeneficiaryBankDel.setName(mudraBeneficiaryBank.getName());
				mudraBeneficiaryBankDel.setVerified("V");
				mudraBeneficiaryBankDel.setVerifiedStatus("V");
				mudraBeneficiaryBankDel.setVerifiedon(new java.sql.Date(System.currentTimeMillis()));
				transaction = session.beginTransaction();
				session.saveOrUpdate(mudraBeneficiaryBankDel);
				transaction.commit();
				mudraBeneficiaryBank.setStatusCode("1000");
				mudraBeneficiaryBank.setStatusDesc("Success");

				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();

				mudraBeneficiaryBank.setList(benfList);
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
					+ "problem in acceptBeneficiary" + e.getMessage()+" "+e);
					
			/*logger.debug(serverName + "*******RequestId****" + requestId
					+ "******problem in acceptBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}

	@Override
	public MudraBeneficiaryBank rejectBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
				+ "|rejectBeneficiary()|"
				+ mudraBeneficiaryBank.getId());
				
		/*logger.info(serverName + "*******RequestId****" + requestId
				+ "******Start excution ************************************* rejectBeneficiary method**:"
				+ mudraBeneficiaryBank.getId());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryBank;
			}

			Object mudraBeneficiaryBankDel = session.load(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());

			if (mudraBeneficiaryBankDel != null) {
				transaction = session.beginTransaction();
				session.delete(mudraBeneficiaryBankDel);
				transaction.commit();
				mudraBeneficiaryBank.setStatusCode("1000");
				mudraBeneficiaryBank.setStatusDesc("Success");
				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();

				mudraBeneficiaryBank.setList(benfList);
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*******RequestId****" + requestId
					+ "problem in rejectBeneficiary" + e.getMessage()+" "+e);
					
			/*logger.debug(serverName + "*******RequestId****" + requestId
					+ "******problem in rejectBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}

	// changes has been added
	@Override
	public MudraBeneficiaryBank registerBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
				+ "|rejectBeneficiary()|"
				+ mudraBeneficiaryBank.getAccountNo());
				
		/*logger.info(serverName + "*********RequestId*******" + requestId
				+ "****Start excution ************************************* registerBeneficiary method**:"
				+ mudraBeneficiaryBank.getAccountNo());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1110");
				mudraBeneficiaryBank.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getName() == null || mudraBeneficiaryBank.getName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1101");
				mudraBeneficiaryBank.setStatusDesc("Invalid Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getBankName() == null || mudraBeneficiaryBank.getBankName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1107");
				mudraBeneficiaryBank.setStatusDesc("Invalid Bank Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getAccountNo() == null || mudraBeneficiaryBank.getAccountNo().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1108");
				mudraBeneficiaryBank.setStatusDesc("Invalid Account Number.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getIfscCode() == null || mudraBeneficiaryBank.getIfscCode().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1109");
				mudraBeneficiaryBank.setStatusDesc("Invalid IFSC Code.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getTransferType() == null || mudraBeneficiaryBank.getTransferType().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1111");
				mudraBeneficiaryBank.setStatusDesc("Invalid Transfer Type.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getMpin() == null || mudraBeneficiaryBank.getMpin().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1102");
				mudraBeneficiaryBank.setStatusDesc("Invalid/Empty MPIN.");
				return mudraBeneficiaryBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("id", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mudraBeneficiaryBank.getMpin()))).list();
			if (senderList.size() <= 0) {
				mudraBeneficiaryBank.setStatusCode("1102");
				mudraBeneficiaryBank.setStatusDesc("Invalid MPIN.");
				return mudraBeneficiaryBank;
			}

			/*List<MudraBeneficiaryBank> benList = session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("accountNo", mudraBeneficiaryBank.getAccountNo())).list();*/
			/*add by naveen*/
			List<MudraBeneficiaryBank> benList = session.createCriteria(MudraBeneficiaryBank.class)
				     .add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
				     .add(Restrictions.eq("accountNo", mudraBeneficiaryBank.getAccountNo()))
				     .add(Restrictions.ne("status","Deleted")).list();
			
			if (benList.size() > 0) {
				mudraBeneficiaryBank.setStatusCode("1124");
				mudraBeneficiaryBank.setStatusDesc("Your provided account number is already added with sender.");
				return mudraBeneficiaryBank;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			if (activeCount >= 15) {
				mudraBeneficiaryBank.setStatusCode("1123");
				mudraBeneficiaryBank.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
				return mudraBeneficiaryBank;
			}
			
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraBeneficiaryBank.getSenderId());
			String bankCode=getBankCodeFromIfscMaster(mudraBeneficiaryBank.getIfscCode());
			if(bankCode!=null&&bankCode.length()>0) {
				mudraBeneficiaryBank.setBankCode(bankCode);
			}
//			AadharShilaDMT aadharShilaDMT=new AadharShilaDMT();
//			AadharShilaDMTResponse dmtResponse=aadharShilaDMT.registerBeneficiary(mudraBeneficiaryBank,mudraSenderBank.getMobileNo(),mudraSenderBank.getAgentId());
//			if (dmtResponse != null && "SUCCESS".equalsIgnoreCase(dmtResponse.getErrorMsg())
//					&& "00".equalsIgnoreCase(dmtResponse.getErrorCode())) {
//				mudraBeneficiaryBank.setRecipientId(dmtResponse.getData().getRecipientId());
//			}else {
				mudraBeneficiaryBank.setRecipientId("NA");
//			}
			transaction = session.beginTransaction();
			mudraBeneficiaryBank.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryBank.getAggreatorId()));
			mudraBeneficiaryBank.setStatus("Active");
			mudraBeneficiaryBank.setVerified("NV");
			mudraBeneficiaryBank.setVerifiedStatus("V");
			// mudraBeneficiaryBank.setBank(1);//changes added for bank enabled
			// transactions
			session.save(mudraBeneficiaryBank);
			transaction.commit();
			mudraBeneficiaryBank.setStatusCode("1000");
			mudraBeneficiaryBank.setStatusDesc("Beneficiary has been registered successfully.");

			

			String smstemplet = commanUtilDao.getsmsTemplet("addBene", mudraSenderBank.getAggreatorId());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "mudraSenderBank.getMobileNo()" + smstemplet);
					
			/*logger.info("*********RequestId*******" + requestId
					+ "****~~~~~~~~~~~~~~~~~~~~~mudraSenderBank.getMobileNo()~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
			if(smstemplet!=null) {
			SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderBank.getMobileNo(),
					smstemplet.replaceAll("<<BENE NAME>>", mudraBeneficiaryBank.getName())
							.replace("<<ACCNO>>", mudraBeneficiaryBank.getAccountNo())
							.replace("<<IFSC>>", mudraBeneficiaryBank.getIfscCode()),
					"SMS", mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "OTP");
//			Thread t = new Thread(smsAndMailUtility);
//			t.start();
			ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			}


		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "problem in registerBeneficiary" + e.getMessage()+" "+e);
					
			/*logger.debug(serverName + "*********RequestId*******" + requestId
					+ "****problem in registerBeneficiary=========================" + e.getMessage(), e);*/
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");

			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}
	
	public String getTxnStatusFino(String txnid,String txnType)
	{
		String resp="SUCCESS";
		try
		{
			FINOCredentials.setFinoCredentials();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId", FINOCredentials.FINO_CLIENT_ID);
			jsonObject.put("AuthKey" , FINOCredentials.FINO_AUTH_KEY);
			 String str= new FINOIMPSImplementation().callGetStatusServicePOST(txnid,jsonObject.toString());
			  
			 JSONObject jsonObject1=new JSONObject(str);
	         String ResponseCode=jsonObject1.get("ResponseCode").toString(); 
	         String responseData=jsonObject1.get("ResponseData").toString(); 
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "responsecode status check for refund"
						+ ResponseCode);
	          String decData;
				if (responseData.contains("\\")) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "fino status check for refund"
							+ responseData.replaceAll("\\\\", ""));
					
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY,
							responseData.replaceAll("\\\\", ""));
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "fino status check for refund"
							+ responseData);
					
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY, responseData);
					
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "decData fino status check for refund"
						+ decData);
	          
	         JSONObject jsonObject2=new JSONObject(decData);
	         String actcode=jsonObject2.getString("ActCode"); 
	         Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "actcode fino status check for refund"
						+ actcode);
	         if(txnType!=null&&txnType.equalsIgnoreCase("NEFT")){
	        	 
	        	 
	        	 if(ResponseCode.equals("0") && (actcode.equals("0") || actcode.equals("26") || actcode.equals("11"))){
	                  System.out.println("Transaction id mark as success :"+txnid);
	         	}
	         	else if((ResponseCode.equals("0") && (actcode.equals("1") ||actcode.equals("20") ||actcode.equals("9") ||actcode.equals("7") ||actcode.equals("2")
	         			||actcode.equals("5001"))) || 
	         			(ResponseCode.equals("1") && (actcode.equals("1012") ||actcode.equals("1009") ||actcode.equals("200")  ||actcode.equals("504") ||actcode.equals("503")
	         					||actcode.equals("9999") ||actcode.equals("1012") ||actcode.equals("1011") ||actcode.equals("1009") ||actcode.equals("999") || 
	         					actcode.equals("1004") || actcode.equals("1002")|| actcode.equals("1010")|| actcode.equals("401")|| actcode.equals("5001"))))
	         	{}
	         	else if(ResponseCode.equals("0") && (actcode.equals("100") ||actcode.equals("23") ||actcode.equals("21") ||actcode.equals("12") ))
	         	{
	         		resp="FAILED";
	                System.out.println("Transaction id mark as FAILED :"+txnid);
	         	}
	        	 
	        	 
	         }else if(txnType!=null&&(txnType.equalsIgnoreCase("IMPS")||txnType.equalsIgnoreCase("Verify"))){
	        	 if(ResponseCode.equals("0") && (actcode.equals("0") || actcode.equalsIgnoreCase("S")))
	        	  {
	                  System.out.println("Transaction id mark as success :"+txnid); 
	        	  }
	        	  else if((ResponseCode.equals("0") && (actcode.equals("1") ||actcode.equalsIgnoreCase("P") ||actcode.equals("5001"))) || 
	        			  (ResponseCode.equals("1") && (actcode.equals("1012") ||actcode.equals("1009") ||actcode.equals("200") ||actcode.equals("504") ||actcode.equals("503")
	        					  ||actcode.equals("9999") ||actcode.equals("1012") || actcode.equals("1011") ||actcode.equals("1009") ||actcode.equals("999") ||
	        					  actcode.equals("1004") ||actcode.equals("1002") ||actcode.equals("1010") || actcode.equals("401") || actcode.equals("5001") )))
	        	  {}
	        	  else if(ResponseCode.equals("0") && (actcode.equals("100") || actcode.equalsIgnoreCase("R")))
	        	  {
	        		  resp="FAILED";
	                  System.out.println("Transaction id mark as FAILED :"+txnid);
	        	  }
	         }
	         
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			
		}
		
		return resp;
	}
	
	
	public String getAadharShilaStatus(String txnId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "Status query for aadharshila txn id is : "+ txnId);
		String status = "SUCCESS";
		
		try {
			AadharShilaDMT aadharShilaDMT = new AadharShilaDMT();
			AadharShilaDMTResponse responseDmt = aadharShilaDMT.statusQuery(txnId);
			if (responseDmt != null && "00".equalsIgnoreCase(responseDmt.getErrorCode())
					&& "SUCCESS".equalsIgnoreCase(responseDmt.getErrorMsg())) {
				status = "SUCCESS";
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "getting SUCCESS status by status query for aadharshila is : "+ status);

			} else if (responseDmt != null && "02".equalsIgnoreCase(responseDmt.getErrorCode())
					&& "Failure".equalsIgnoreCase(responseDmt.getErrorMsg())) {
				status = "FAILED";
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "getting FAILURE status by status query for aadharshila is : "+ status);

			} else {
				status = "PENDING";
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "getting "+responseDmt!=null ?responseDmt.getErrorMsg() : "PENDING" +"status by status query for aadharshila is : "+ status);
			}
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "problem with status query for aadharshila is : "+ e.getMessage());
			e.printStackTrace();
		}
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "getting status by status query for aadharshila is : "+ status);
		return status;
	}
	
	
	
	@Override
	public MudraSenderBank forgotMPIN(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
				+ "|forgotMPIN()|"
				+ mudraSenderBank.getId()+ "|MobileNo " +mudraSenderBank.getMobileNo());
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
				
				+ mudraSenderBank.getMobileNo());*/

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		String newOtp = null;
		try {
			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("id", mudraSenderBank.getId()))
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderBank.getAggreatorId())).list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
					+ "Inside senderList" + senderList.size());
					
			/*logger.info(serverName + "******RequestId*******" + requestId
					+ "******Inside ====================senderList==========" + senderList.size());*/
			if (senderList.size() > 0) {
				transaction = session.beginTransaction();
				newOtp = oTPGeneration.generateOTP();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
						+ "Inside otp" + newOtp);
						
				/*logger.info(serverName + "******RequestId*******" + requestId
						+ "******Inside ***********************otp**********************" + newOtp);*/
				if (!(newOtp == null)) {
					Query insertOtp = session.getNamedQuery("callOTPMANAGE")
							.setParameter("mobile", mudraSenderBank.getMobileNo())
							.setParameter("aggreatorid", mudraSenderBank.getAggreatorId())
							.setParameter("otp", CommanUtil.SHAHashing256(newOtp));
					insertOtp.executeUpdate();
				}
				transaction.commit();
				WalletConfiguration walletConfiguration = commanUtilDao
						.getWalletConfiguration(mudraSenderBank.getAggreatorId());
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
							+ "Inside otp send on mobil"
							+ mudraSenderBank.getMobileNo() + "===otp===" + newOtp);
							
					/*logger.info(serverName + "******RequestId*******" + requestId
							+ "******Inside ====================otp send on mobile=========="
							+ mudraSenderBank.getMobileNo() + "===otp===" + newOtp);*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderBank.getMobileNo(),
							commanUtilDao.getsmsTemplet("forgotMpin", mudraSenderBank.getAggreatorId())
									/*
									 * .replace("<<APPNAME>>",
									 * walletConfiguration.getAppname())
									 */ .replace("<<OTP>>", newOtp),
							"SMS", mudraSenderBank.getAggreatorId(), "", "", "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				}
				mudraSenderBank.setStatusCode("1000");
				mudraSenderBank.setStatusDesc("OTP has been sent on registered Mobile Number.");
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId*******" + requestId
					+ "problem in forgotMPIN" + e.getMessage()+" "+e);
					
			/*logger.debug(serverName + "******RequestId*******" + requestId
					+ "******problem in forgotMPIN=========================" + e.getMessage(), e);*/
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;
	}

	@Override
	public MudraSenderBank updateMPIN(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
				+ "|updateMPIN()|"
				+ mudraSenderBank.getId()+ "|MobileNo"+mudraSenderBank.getMobileNo()+ "|MPIN :"
						+ mudraSenderBank.getMpin()+ "|OTP :"
								+ mudraSenderBank.getOtp());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","","", "", serverName + "******RequestId***" + requestId
				
				+ mudraSenderBank.getMobileNo());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getAggreatorId());*/
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
				+ "MPIN :"
				+ mudraSenderBank.getMpin());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
				+ "OTP :"
				+ mudraSenderBank.getOtp());*/
				
		/*logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getId());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getMobileNo());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getAggreatorId());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getMpin());
		logger.info(serverName + "******RequestId***" + requestId
				+ "******Start excution ************************************* updateMPIN method**:"
				+ mudraSenderBank.getOtp());*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderBank.getMpin() == null || mudraSenderBank.getMpin().isEmpty()
					|| (mudraSenderBank.getMpin().length() < 4 || mudraSenderBank.getMpin().length() > 6)) {
				mudraSenderBank.setStatusCode("1102");
				mudraSenderBank.setStatusDesc("Invalid MPIN.");
				return mudraSenderBank;
			}
			if (mudraSenderBank.getOtp() == null || mudraSenderBank.getOtp().isEmpty()) {
				mudraSenderBank.setStatusCode("1103");
				mudraSenderBank.setStatusDesc("OTP can't be empty.");
				return mudraSenderBank;
			}

			Query queryOTP = session.createQuery(
					" FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid");
			queryOTP.setString("mobile", mudraSenderBank.getMobileNo());
			queryOTP.setString("otp", CommanUtil.SHAHashing256(mudraSenderBank.getOtp()));
			queryOTP.setString("aggreatorid", mudraSenderBank.getAggreatorId());
			List list = queryOTP.list();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId + "******updateMPIN()*********otp :"
					+ mudraSenderBank.getOtp() + "----" + CommanUtil.SHAHashing256(mudraSenderBank.getOtp())+ "|updateMPIN() Mobile Number :"
							+ mudraSenderBank.getMobileNo());
			/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
					+ "updateMPIN() Mobile Number :"
					+ mudraSenderBank.getMobileNo());*/
		/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~aggreatorid :"
					+ mudraSenderBank.getAggreatorId());*/
					
			/*logger.info(
					"******RequestId***" + requestId + "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~otp :"
							+ mudraSenderBank.getOtp() + "----" + CommanUtil.SHAHashing256(mudraSenderBank.getOtp()));
			logger.info("******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~Mobile Number :"
					+ mudraSenderBank.getMobileNo());
			logger.info("******RequestId***" + requestId
					+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~aggreatorid :"
					+ mudraSenderBank.getAggreatorId());*/
			if (list != null && list.size() != 0) {
				if (new OTPGeneration().validateOTP(mudraSenderBank.getOtp())) {

					transaction = session.beginTransaction();
					MudraSenderBank mudraSenderBankUpdate = (MudraSenderBank) session.get(MudraSenderBank.class,
							mudraSenderBank.getId());
					mudraSenderBankUpdate.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.getMpin()));
					session.update(mudraSenderBankUpdate);
					transaction.commit();
					mudraSenderBank.setStatusCode("1000");
					mudraSenderBank.setStatusDesc("MPIN has been changed successfully.");

					String smstemplet = commanUtilDao.getsmsTemplet("senderMpinChange",
							mudraSenderBank.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
							+ "senderMpinChange " + smstemplet);
							
					/*logger.info("******RequestId***" + requestId
							+ "******~~~~~~~~~~~~~~~~~~~~~senderMpinChange~~~~~~~~~~~~~~~~~~~````" + smstemplet);*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderBank.getMobileNo(), smstemplet.replaceAll("<<MPIN>>", mudraSenderBank.getMpin()),
							"SMS", mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);

				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
							+ "updateMPIN()~~1105");
							
					/*logger.info("******RequestId***" + requestId
							+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~~~~~~~~~~~~1105");*/
					mudraSenderBank.setStatusCode("1105");
					mudraSenderBank.setStatusDesc("OTP has expired.");
					return mudraSenderBank;
				}

			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
						+ "~updateMPIN()~~~1106~~No data found in walletotpmast");
						
				/*logger.info("******RequestId***" + requestId
						+ "******~~~~~~~~~~~~~~~~~~~~~updateMPIN()~~~~~~~~1106~~~~~~~~~~~No data found in walletotpmast");*/
				mudraSenderBank.setStatusCode("1106");
				mudraSenderBank.setStatusDesc("Invalid OTP.");
				return mudraSenderBank;
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "******RequestId***" + requestId
					+ "|updateMPIN()|"+ "problem in updateMPIN" + e.getMessage()+" "+e);
					
			/*logger.debug(serverName + "******RequestId***" + requestId
					+ "******problem in updateMPIN=========================" + e.getMessage(), e);*/
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;
	}

	/*
	 * @Override public MudraSenderBank updateMPIN(MudraSenderBank
	 * mudraSenderBank,String serverName) { logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderBank.getId()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderBank.getMobileNo()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderBank.getAggreatorId()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderBank.getMpin()); logger.info(serverName+
	 * "Start excution ************************************* updateMPIN method**:"
	 * + mudraSenderBank.getOtp()); factory = DBUtil.getSessionFactory();
	 * session = factory.openSession(); mudraSenderBank.setStatusCode("1001");
	 * mudraSenderBank.setStatusDesc("Error"); String newOtp = null; try {
	 * 
	 * if (mudraSenderBank.getMpin() == null ||
	 * mudraSenderBank.getMpin().isEmpty() ||
	 * (mudraSenderBank.getMpin().length() < 4 ||
	 * mudraSenderBank.getMpin().length() > 6)) {
	 * mudraSenderBank.setStatusCode("1102"); mudraSenderBank.setStatusDesc(
	 * "Invalid MPIN."); return mudraSenderBank; } if (mudraSenderBank.getOtp()
	 * == null || mudraSenderBank.getOtp().isEmpty()) {
	 * mudraSenderBank.setStatusCode("1103"); mudraSenderBank.setStatusDesc(
	 * "OTP can't be empty."); return mudraSenderBank; }
	 * 
	 * Query queryOTP = session.createQuery(
	 * " FROM WalletOtpBean WHERE mobileno=:mobile and otp=:otp and aggreatorid=:aggreatorid"
	 * ); queryOTP.setString("mobile", mudraSenderBank.getMobileNo());
	 * queryOTP.setString("otp",
	 * CommanUtil.SHAHashing256(mudraSenderBank.getOtp()));
	 * queryOTP.setString("aggreatorid", mudraSenderBank.getAggreatorId()); List
	 * list = queryOTP.list(); if (list != null && list.size() != 0) { if (new
	 * OTPGeneration().validateOTP(mudraSenderBank.getOtp())) {
	 * 
	 * transaction = session.beginTransaction(); MudraSenderBank
	 * mudraSenderBankUpdate = (MudraSenderBank) session
	 * .get(MudraSenderBank.class, mudraSenderBank.getId());
	 * mudraSenderBankUpdate.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.
	 * getMpin())); session.update(mudraSenderBankUpdate); transaction.commit();
	 * mudraSenderBank.setStatusCode("1000"); mudraSenderBank.setStatusDesc(
	 * "MPIN has been changed successfully.");
	 * 
	 * String smstemplet =
	 * commanUtilDao.getsmsTemplet("senderMpinChange",mudraSenderBank.
	 * getAggreatorId()); logger.info(
	 * "~~~~~~~~~~~~~~~~~~~~~senderMpinChange~~~~~~~~~~~~~~~~~~~````" +
	 * smstemplet); SmsAndMailUtility smsAndMailUtility = new
	 * SmsAndMailUtility("", "","", mudraSenderBank.getMobileNo(),
	 * smstemplet.replaceAll("<<MPIN>>", mudraSenderBank.getMpin()),
	 * "SMS",mudraSenderBank.getAggreatorId(),"",mudraSenderBank.getFirstName(),
	 * "OTP"); Thread t = new Thread(smsAndMailUtility); t.start();
	 * 
	 * 
	 * 
	 * 
	 * } else { mudraSenderBank.setStatusCode("1105");
	 * mudraSenderBank.setStatusDesc("OTP has expired."); return
	 * mudraSenderBank; }
	 * 
	 * } else { mudraSenderBank.setStatusCode("1106");
	 * mudraSenderBank.setStatusDesc("Invalid OTP."); return mudraSenderBank; }
	 * 
	 * } catch (Exception e) { logger.debug(serverName+
	 * "problem in updateMPIN=========================" + e.getMessage(), e);
	 * mudraSenderBank.setStatusCode("7000");
	 * mudraSenderBank.setStatusDesc("Error"); e.printStackTrace();
	 * transaction.rollback(); } finally { session.close(); }
	 * 
	 * return mudraSenderBank; }
	 */
	@Override
	public Boolean otpSenderResend(String mobileno, String aggreatorid, String serverName, String requestId) {
		MudraSenderBank mudraSenderBank=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId
				+ "|otpSenderResend()");
				
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "********Start excution ********************************************* method otpSenderResend(mobileno)");*/
		Boolean flag = false;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query1 = null;
		try {
			Query query = session
					.createQuery(" delete FROM WalletOtpBean WHERE mobileno=:mobile and aggreatorid=:aggreatorid ");
			query.setString("mobile", mobileno);
			query.setString("aggreatorid", aggreatorid);
			query.executeUpdate();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId
					+ "Inside Delete old otp from otpmast==");
			/*logger.info(serverName + "*******RequestId******" + requestId
					+ "*******Inside ==============Delete old otp from otpmast==");*/
			new OTPGeneration();
			String newOtp = new OTPGeneration().generateOTP();
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId + "*******~~~~~~~`"
						+ CommanUtil.SHAHashing256(newOtp));
				/*logger.info(serverName + "*******RequestId******" + requestId + "*******~~~~~~~`"
						+ CommanUtil.SHAHashing256(newOtp));*/
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", mobileno)
						.setParameter("aggreatorid", aggreatorid).setParameter("otp", CommanUtil.SHAHashing256(newOtp));
				insertOtp.executeUpdate();
				SmsAndMailUtility smsAndMailUtility;
				transaction.commit();
				WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorid);
				if (!(newOtp == null)) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId
							+ "Inside otp send on mobile" + mobileno + "===otp==="
							+ newOtp);
					/*logger.info(serverName + "*******RequestId******" + requestId
							+ "*******Inside ====================otp send on mobile==========" + mobileno + "===otp==="
							+ newOtp);*/
					smsAndMailUtility = new SmsAndMailUtility("", "", "", mobileno,
							commanUtilDao.getsmsTemplet("regOtp", aggreatorid)
									.replace("<<APPNAME>>", walletConfiguration.getAppname())
									.replace("<<OTP>>", newOtp),
							"SMS", aggreatorid, "", "", "OTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					flag = true;
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId
						+ "Inside otp send on mobile" + mobileno + "===otp===" + newOtp);
				/*logger.info(serverName + "*******RequestId******" + requestId
						+ "*******Inside ==============otp send on mobile========" + mobileno + "===otp===" + newOtp);*/
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),aggreatorid,"","", "", "", serverName + "*******RequestId******" + requestId
					+ "problem in otpSenderResend otp" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "*******problem in otpSenderResend otp==============" + e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,"","","", "", serverName + "*******RequestId******" + requestId
				+ "Inside return flag=== and end=======" + flag);
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Inside ====================return flag=== and end=======" + flag);*/
		return flag;

	}

	@Override
	public SenderFavouriteBean setFavourite(SenderFavouriteBean senderFavouriteBean, String serverName,
			String requestId) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "****RequestId******" + requestId
				+ "|setFavourite()|"
				+ senderFavouriteBean.getAgentId()+"|MobileNo " +senderFavouriteBean.getMobileNo());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBanktest.getAggreatorId(),"","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getSenderId());*/
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "****RequestId******" + requestId
				
				+ senderFavouriteBean.getMobileNo());*/
		
		/*logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getAgentId());
		logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getSenderId());
		logger.info(serverName + "****RequestId******" + requestId
				+ "******Start excution ************************************* setFavourite method**:"
				+ senderFavouriteBean.getMobileNo());*/

		if (senderFavouriteBean.getAgentId() == null || senderFavouriteBean.getAgentId().isEmpty()) {
			senderFavouriteBean.setStatusCode("1117");
			senderFavouriteBean.setStatusDesc("Invalid Agent Id.");
			return senderFavouriteBean;
		}

		if (senderFavouriteBean.getSenderId() == null || senderFavouriteBean.getSenderId().isEmpty()) {
			senderFavouriteBean.setStatusCode("1110");
			senderFavouriteBean.setStatusDesc("Invalid Sender Id.");
			return senderFavouriteBean;
		}

		if (senderFavouriteBean.getMobileNo() == null || senderFavouriteBean.getMobileNo().isEmpty()) {
			senderFavouriteBean.setStatusCode("1104");
			senderFavouriteBean.setStatusDesc("Invalid Mobile Number.");
			return senderFavouriteBean;
		}

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			transaction = session.beginTransaction();
			session.save(senderFavouriteBean);
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					senderFavouriteBean.getSenderId());

			if (mudraSenderBank.getFavourite() == null || mudraSenderBank.getFavourite().length() > 0) {
				mudraSenderBank.setFavourite(mudraSenderBank.getFavourite() + "," + senderFavouriteBean.getAgentId());
			} else {
				mudraSenderBank.setFavourite(senderFavouriteBean.getAgentId());
			}

			session.update(mudraSenderBank);
			transaction.commit();

			senderFavouriteBean
					.setSenderFavouriteList(getFavouriteList(senderFavouriteBean.getAgentId(), serverName, requestId));

			senderFavouriteBean.setStatusCode("1000");
			senderFavouriteBean.setStatusDesc("Sender has been marked as Favourite.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "****RequestId******" + requestId
					+ "problem in setFavourite" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "****RequestId******" + requestId
					+ "******problem in setFavourite=========================" + e.getMessage(), e);*/
			senderFavouriteBean.setStatusCode("7000");
			senderFavouriteBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return senderFavouriteBean;
	}

	@Override
	public SenderFavouriteBean deleteFavourite(SenderFavouriteBean senderFavouriteBean, String serverName,
			String requestId) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "***RequestId*****" + requestId
				+ "|deleteFavourite()|"
				+ senderFavouriteBean.getId());
		/*logger.info(serverName + "***RequestId*****" + requestId
				+ "******Start excution ************************************* deleteFavourite method**:"
				+ senderFavouriteBean.getId());*/

		if (senderFavouriteBean.getId() <= 0) {
			senderFavouriteBean.setStatusCode("1118");
			senderFavouriteBean.setStatusDesc("Invalid Favourite Id.");
			return senderFavouriteBean;
		}

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {

			SenderFavouriteBean senderFavouriteBeanDel = (SenderFavouriteBean) session.get(SenderFavouriteBean.class,
					new Integer(senderFavouriteBean.getId()));

			if (senderFavouriteBeanDel != null) {
				transaction = session.beginTransaction();
				String agnId = senderFavouriteBeanDel.getAgentId();
				session.delete(senderFavouriteBeanDel);
				MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
						senderFavouriteBean.getSenderId());

				String agId = mudraSenderBank.getFavourite().concat(",");
				agId = agId.replace(agnId + ",", "");
				if (agId.length() > 0) {
					agId = agId.substring(0, agId.length() - 1);
				}
				mudraSenderBank.setFavourite(agId);
				session.update(mudraSenderBank);
				transaction.commit();
				senderFavouriteBean.setSenderFavouriteList(
						getFavouriteList(senderFavouriteBean.getAgentId(), serverName, requestId));
				senderFavouriteBean.setStatusCode("1000");
				senderFavouriteBean.setStatusDesc("Success");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderFavouriteBean.getSenderId(), "", serverName +"|Agentid: "+senderFavouriteBean.getAgentId()+ "***RequestId*****" + requestId
					+ "problem in deleteFavourite" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "***RequestId*****" + requestId
					+ "******problem in deleteFavourite=========================" + e.getMessage(), e);*/
			senderFavouriteBean.setStatusCode("7000");
			senderFavouriteBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return senderFavouriteBean;
	}

	@Override
	public List<SenderFavouriteViewBean> getFavouriteList(String agentId, String serverName, String requestId) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "*******RequestId******" + requestId
				+ "|getFavouriteList()|" + agentId);
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "********Start excution ************************************* getFavouriteList method**:" + agentId);*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<SenderFavouriteViewBean> senderFavouriteList = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT senderid,favouritid,NAME,mobileno,agentid,walletbalance,transferlimit,kycstatus FROM banksenderfavouriteview WHERE agentid=:agentId ");
			query.setParameter("agentId", agentId);
			query.setResultTransformer(Transformers.aliasToBean(SenderFavouriteViewBean.class));
			senderFavouriteList = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "", serverName +"|Agentid: "+agentId +"*******RequestId******" + requestId
					+ "|getFavouriteList()|"
					+ "problem in getFavouriteList" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "********problem in getFavouriteList*****************************************" + e.getMessage(),
					e);*/

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName+"|Agentid: "+agentId + "*******RequestId******" + requestId
				+ "return "
				+ senderFavouriteList.size());
		/*logger.info(serverName + "*******RequestId******" + requestId
				+ "******** **********************return **********************************************************"
				+ senderFavouriteList.size());*/

		return senderFavouriteList;

	}

	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean, String serverName, String requestId) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "****RequestId******" + requestId
				+ "|calculateSurcharge()|"
				+ surchargeBean.getAgentId()+ "|calculateSurcharge()|"
						+ surchargeBean.getAmount());

		List objectList = null;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		double surcharge = 0.0;
		double distsurcharge = 0.0;
		double creditSurcharge = 0.0;
		double suDistSurcharge=0.0;
		double agentRefund=0.0;
		double aggReturnVal=0.0;
		double dtdsApply=0.0;
		try {
			if (surchargeBean.getAgentId() == null || surchargeBean.getAgentId().isEmpty()) {
				surchargeBean.setStatus("4321");
				return surchargeBean;
			}

			if (surchargeBean.getAmount() <= 0) {
				surchargeBean.setStatus("4322");
				return surchargeBean;
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" + requestId
					+ "|excution  surcharge :" + surchargeBean.getAgentId()+ "|excution  surcharge :" + surchargeBean.getAmount());
		
			surchargeBean.setTxnCode(15);
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", surchargeBean.getAgentId());
			query.setParameter("txnAmount", surchargeBean.getAmount());
			if (surchargeBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", surchargeBean.getTransType());
			query.setParameter("isBank", "1");
			objectList = query.list();
			Object obj[] = (Object[]) objectList.get(0);
			surcharge = Double.parseDouble("" + obj[0]);
			distsurcharge = Double.parseDouble("" + obj[1]);
			if (obj[2] != null)
				creditSurcharge = Double.parseDouble("" + obj[2]);
			else
				creditSurcharge =0.0;
			if(obj[3]!=null)
				suDistSurcharge=Double.parseDouble(""+obj[3]);
			else
				suDistSurcharge=0.0;
			if(obj[4]!=null)
				agentRefund=Double.parseDouble(""+obj[4]);
			else
				agentRefund=0.0;
			if(obj[5]!=null)
				aggReturnVal=Double.parseDouble(""+obj[5]);
			if(obj[6]!=null)
				dtdsApply=Double.parseDouble(""+obj[6]);
			
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" + requestId
					+ " excution surcharge :" + surcharge);
			
			/*logger.info("****RequestId******" + requestId
					+ "******** excution *********************************** surcharge :" + surcharge);*/
			surchargeBean.setSurchargeAmount(surcharge);
			surchargeBean.setDistSurCharge(distsurcharge);
			surchargeBean.setCreditSurcharge(creditSurcharge);
			surchargeBean.setSuDistSurcharge(suDistSurcharge);
			surchargeBean.setAgentRefund(agentRefund);
			surchargeBean.setAggReturnVal(aggReturnVal);
			surchargeBean.setDtdsApply(dtdsApply);
			surchargeBean.setStatus("1000");
		} catch (Exception e) {
			surchargeBean.setStatus("7000");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +"|Agentid :"+surchargeBean.getAgentId()+ "*******RequestId******" + requestId
				+ "problem in calculateCashinSurcharge" + e.getMessage()+" "+e);

			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "********problem in calculateCashinSurcharge**********************************" + e.getMessage(),
					e);*/
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return surchargeBean;
	}
public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
	String verificationStatus="V";
    
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
			+ "|verifyAccount()");
	/*logger.info(serverName + "*******RequestId****" + requestId
				+ "*****Start excution ******************************************* verifyAccount method******:");*/
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		mudraMoneyTransactionBean.setStatusCode("1001");
		mudraMoneyTransactionBean.setStatusDesc("Error");

		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
					+ "|verifyAccount()|"
					+ mudraMoneyTransactionBean.getAgentid()+  "|verifyAccount()|"
							+ mudraMoneyTransactionBean.getBeneficiaryId()+  "|verifyAccount()|"
							+ mudraMoneyTransactionBean.getVerificationAmount());

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1116");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null
					|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1116");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null
					|| mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1111");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Transfer Type.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() < 0) {
				mudraMoneyTransactionBean.setStatusCode("1120");
				mudraMoneyTransactionBean.setStatusDesc("Amount can't be zero.");
				return mudraMoneyTransactionBean;
			}

			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				mudraMoneyTransactionBean.setStatusCode("1120");
				mudraMoneyTransactionBean.setStatusDesc("Wallet Id can't be empty.");
				return mudraMoneyTransactionBean;
			}
			
			transaction = session.beginTransaction();
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					mudraMoneyTransactionBean.getUserId());
			WalletMastBean walletmastAgg = (WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getAggreatorid());
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraMoneyTransactionBean.getSenderId());
			MudraBeneficiaryBank mudraBeneficiaryBank = (MudraBeneficiaryBank) session.get(MudraBeneficiaryBank.class,
					mudraMoneyTransactionBean.getBeneficiaryId());
			WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
			 superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class,	walletMastBean.getSuperdistributerid());
			}
			
			if(walletMastBean.getUsertype() != 2)
			{
				mudraMoneyTransactionBean.setStatusCode("7000");
				mudraMoneyTransactionBean.setStatusDesc("Invalid User.");
				return mudraMoneyTransactionBean;				
			}
			List<MudraBeneficiaryBank> benList=null;
			List<VerifiedBeneficiaryData> benDataV = null;
			if(mudraBeneficiaryBank!=null){
			Query benQuery=session.createQuery("from MudraBeneficiaryBank where accountNo=:accountNo and ifscCode=:ifscCode and verified=:verified");
			benQuery.setParameter("accountNo",mudraBeneficiaryBank.getAccountNo());
			benQuery.setParameter("ifscCode",mudraBeneficiaryBank.getIfscCode());
			benQuery.setParameter("verified","V");
			benList=benQuery.list();
			}
			
			if(benList == null || benList.size() == 0) {
				Query benQuery=session.createQuery("from VerifiedBeneficiaryData where accountNo=:accountNo and bankName=:bankName");
				benQuery.setParameter("accountNo", mudraBeneficiaryBank.getAccountNo());
				benQuery.setParameter("bankName", mudraBeneficiaryBank.getBankName().trim().toUpperCase());
				benDataV = benQuery.list();
				
			}
			
			
			
			WalletMastBean walletMastBeanAgg=null;
			if(walletmastAgg!=null&&walletmastAgg.getCashDepositeWallet()!=null){
				walletMastBeanAgg=(WalletMastBean)session.get(WalletMastBean.class, walletmastAgg.getCashDepositeWallet());
			}
			/*WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()){
				superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class, walletMastBean.getSuperdistributerid());
			}*/
			
			/*
			 * //added for d
			 * if(!mudraMoneyTransactionBean.getSenderId().trim().equals(
			 * mudraBeneficiaryBank.getSenderId().trim())) {
			 * mudraMoneyTransactionBean.setStatusCode("1130");
			 * mudraMoneyTransactionBean.setStatusDesc(
			 * "Beneficiary does not belong to mentioned Sender ID"); return
			 * mudraMoneyTransactionBean; }
			 */

			// added for defect id 13.3
			if (mudraBeneficiaryBank != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId().trim())) {
					mudraMoneyTransactionBean.setStatusCode("1130");
					mudraMoneyTransactionBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraMoneyTransactionBean;  
				}
			} else {
				mudraMoneyTransactionBean.setStatusCode("1116");
				mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
				return mudraMoneyTransactionBean;
			}

			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
					.clone();
			mudraMoneyTransactionBeanSaveTxn.setIsBank(1);

			if (mudraSenderBank.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				mudraMoneyTransactionBean.setStatusCode("1122");
				mudraMoneyTransactionBean.setStatusDesc("Transfer Limit exceed.");
				return mudraMoneyTransactionBean;
			}
			
			mudraMoneyTransactionBean.setBankName(mudraBeneficiaryBank.getBankName());
			mudraMoneyTransactionBean.setAgentName(walletMastBean.getName());
			mudraMoneyTransactionBean.setAgentMobileNo(walletMastBean.getMobileno());
			double verifyAmt=mudraMoneyTransactionBean.getVerificationAmount();
			double surcharge=0.0;
			double distSurcharge=0.0;
			double supDistributorSurcharge=0.0;
			String walletTxnCodeAgg ="1000";
			String superDistributorTxnCode="1000";
			String responseCode=null;
			String actCode = null;
			String rrn = null;
			String beneName=null;
			SurchargeBean surchargeBean = new SurchargeBean();
			surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
			surchargeBean.setAmount(verifyAmt);
			surchargeBean.setTransType("VERIFY");
			
			surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);
			if(surchargeBean.getStatus().equals("1000"))
			{
				surcharge=surchargeBean.getSurchargeAmount();
				distSurcharge=surchargeBean.getDistSurCharge();
				supDistributorSurcharge=surchargeBean.getSuDistSurcharge();
			}
			else
			{
				mudraMoneyTransactionBean.setStatusCode("1122");
				mudraMoneyTransactionBean.setStatusDesc("Surcharge not define.Please contact admin.");
				return mudraMoneyTransactionBean;			
			}
			
			if((verifyAmt+surcharge+distSurcharge+supDistributorSurcharge)>new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())){
				mudraMoneyTransactionBean.setStatusCode("7024");
				mudraMoneyTransactionBean.setStatusDesc("Insufficient Agent Wallet Balance.");
				return mudraMoneyTransactionBean;
			}
			
			
			
			String ppiTxnIdVerification = commanUtilDao.getTrxId("FBTX",walletMastBean.getAggreatorid());//bankTxnIdVerification
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "*******RequestId****" + requestId
					+"|Agentid: "+mudraMoneyTransactionBean.getAgentid()
					+ "ppiTxnIdVerification verifyAccount method****:"
					+ ppiTxnIdVerification);
			
			mudraMoneyTransactionBeanSaveTxn.setId("VT" + ppiTxnIdVerification);
			mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdVerification);
			mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");

			mudraMoneyTransactionBeanSaveTxn.setCrAmount(mudraMoneyTransactionBean.getVerificationAmount());
			mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
			mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
			mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS(Verification)");
			session.save(mudraMoneyTransactionBeanSaveTxn);
			transaction.commit();
			transaction = session.beginTransaction();

			if(walletmastAgg.getWhiteLabel().equals("1")&&transactionDao.getWalletBalance(walletMastBeanAgg.getWalletid())<(verifyAmt+surchargeBean.getCreditSurcharge()))
			{
				mudraMoneyTransactionBean.setStatusCode("7024");
				mudraMoneyTransactionBean.setStatusDesc("Something went wrong please try again later.");
				return mudraMoneyTransactionBean;	
			}
			else if(walletmastAgg.getWhiteLabel().equals("1"))
			{
				walletTxnCodeAgg = transactionDao.walletToAggregatorTransferVerify(ppiTxnIdVerification,walletmastAgg.getCashDepositeWallet(),(verifyAmt),surchargeBean.getCreditSurcharge(), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
			}
			/*if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()){
//				(respTxnId, userId, amount, supDistributorSurcharge, ipImei, aggreatorid, superDistributorId, agent)
				superDistributorTxnCode = transactionDao.walletToSuperDistributorTransfer(ppiTxnIdVerification,walletMastBean.getId(),(verifyAmt),surchargeBean.getSuDistSurcharge(), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(),walletMastBean.getSuperdistributerid(), mudraMoneyTransactionBean.getAgent());
			}*/
			
			
			String walletTxnCode = new TransactionDaoImpl().walletToAccountVerify(walletMastBean.getId(),
					walletMastBean.getWalletid(), /*mudraMoneyTransactionBean.getVerificationAmount()*/verifyAmt,
					mudraBeneficiaryBank.getAccountNo(), ppiTxnIdVerification, mudraMoneyTransactionBean.getIpiemi(),
					walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0);
			String walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurchargeVerifyDist(
					walletMastBean.getId(), walletMastBean.getWalletid(),
					/*mudraMoneyTransactionBean.getSurChargeAmount()*/surcharge+distSurcharge+supDistributorSurcharge, mudraBeneficiaryBank.getAccountNo(),
					ppiTxnIdVerification, mudraMoneyTransactionBean.getIpiemi(), walletMastBean.getAggreatorid(),
					mudraMoneyTransactionBean.getAgent(), 0.0,surchargeBean.getDistSurCharge(),walletMastBean.getDistributerid(),supDistributorSurcharge,(superDistributorMast.getCashDepositeWallet() == null ?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()));

			if (walletTxnCode.equals("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")&&walletTxnCodeAgg.equals("1000")&&superDistributorTxnCode.equalsIgnoreCase("1000")) {
				transaction = session.beginTransaction();
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session
						.get(MudraMoneyTransactionBean.class, "VT" + ppiTxnIdVerification);
				mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
				session.update(mudraMoneyTransactionBeanSaveTxn);
				mudraMoneyTransactionBean.setId("VFB" + ppiTxnIdVerification);
				mudraMoneyTransactionBean.setIsBank(1);
				mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
				mudraMoneyTransactionBean.setDrAmount(mudraMoneyTransactionBean.getVerificationAmount());
				mudraMoneyTransactionBean.setStatus("PENDING");
				session.save(mudraMoneyTransactionBean);
				transaction.commit();
				transaction = session.beginTransaction();

				if ((benList == null || benList.size() == 0) && (benDataV == null || benDataV.size() == 0)){
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "calling finoimps for verify account");

					List<String> respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),mudraMoneyTransactionBean.getVerificationAmount(),mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getName(),mudraMoneyTransactionBean.getId(),mudraSenderBank.getMobileNo(),mudraSenderBank.getFirstName());

					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "getting result from fino imps after verifying account "+"|Agentid: "+mudraMoneyTransactionBean.getAgentid()+ "response :  "+respList);

					responseCode = respList.get(0);
					actCode=respList.get(1);
					if(!respList.get(2).equals("null"))
							rrn=respList.get(2);
					if(!respList.get(3).equals("null"))
						beneName=respList.get(3);
					else
					{	
						beneName = "null";
					}
					transaction = session.beginTransaction();
	
					} 
				else {
					String finotrxId = commanUtilDao.getTrxId("FINO",mudraBeneficiaryBank.getAggreatorId());
					/*res.setActCode("0");
					finores.setResponseCode("0");
					res.setBeneName(mudraBeneficiaryBank.getName());
					res.setTxnID(Double.parseDouble(finotrxId.substring(4)));*/
					actCode="0";
					responseCode="0";
					verificationStatus="NV";
					
					
					if(benDataV != null && benDataV.size() > 0) {
						if(benDataV.get(0).getName()!=null) {
							beneName = benDataV.get(0).getName();
						}
					} else if (benList != null && benList.size() > 0) {
						if (benList.get(0).getName() != null && !benList.get(0).getName().isEmpty()) {
							beneName = benList.get(0).getName();
						} 
					} else {
						beneName = mudraBeneficiaryBank.getName();
					}
					
					rrn=finotrxId.substring(4);
					
				}
				mudraMoneyTransactionBean.setVerifyStatus(verificationStatus);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid: "+mudraMoneyTransactionBean.getAgentid()+ "response code"+responseCode+ "act code"+actCode);
				//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid: "+mudraMoneyTransactionBean.getAgentid()+ "act code"+responseCode);
				
				
				if (!responseCode.equals("0")|| !(actCode.equals("0") ||actCode.equalsIgnoreCase("S")|| actCode.equals("26") || actCode.equals("11"))) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),  serverName +"|Agentid: "+mudraMoneyTransactionBean.getAgentid()+" inside if block "+actCode);
				
				if(responseCode.equals("1")){
					mudraMoneyTransactionBean.setStatus("FAILED");
					mudraMoneyTransactionBean.setRemark("Transaction Failed(Verification)");
					mudraMoneyTransactionBean.setBankRrn(rrn);
					mudraMoneyTransactionBean.setBankResp("FAILED");
					session.update(mudraMoneyTransactionBean);
					transaction.commit();
				}
				 else{
					 mudraMoneyTransactionBean.setStatus("Confirmation Awaited");
						mudraMoneyTransactionBean.setRemark("Transaction Pending");
						mudraMoneyTransactionBean.setBankRrn(rrn);
						mudraMoneyTransactionBean.setBankResp("Confirmation Awaited");
						session.update(mudraMoneyTransactionBean);
						transaction.commit();
					} 
					
					mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
					mudraMoneyTransactionBean.setStatusCode("9000");
					mudraMoneyTransactionBean.setStatusDesc("Transaction Failed(Verification)");

					MudraMoneyTransactionBean wallRefundTxnRef = new MudraMoneyTransactionBean();

					wallRefundTxnRef.setId("VTR" + ppiTxnIdVerification);
					wallRefundTxnRef.setIsBank(1);
					wallRefundTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
					wallRefundTxnRef.setTxnId(ppiTxnIdVerification);
					wallRefundTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
					wallRefundTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
					wallRefundTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
					wallRefundTxnRef.setCrAmount(mudraMoneyTransactionBean.getVerificationAmount());
					wallRefundTxnRef.setDrAmount(0.0);
					wallRefundTxnRef.setNarrartion("WALLET LOADING(Verification-REFUND)");
					wallRefundTxnRef.setStatus("SUCCESS");
					wallRefundTxnRef.setRemark("Transaction SUCCESSFUL(Verification-REFUND)");
					transaction=session.beginTransaction();
					session.save(wallRefundTxnRef);
					transaction.commit();
					transaction = session.beginTransaction();
					
					MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
					// (MudraMoneyTransactionBean )
					// mudraMoneyTransactionBean.clone();

					mudraMoneyTransactionBeanSaveTxnRef.setId("VTRA" + ppiTxnIdVerification);
					mudraMoneyTransactionBeanSaveTxnRef.setIsBank(1);
					mudraMoneyTransactionBeanSaveTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
					mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdVerification);
					mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
					mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
					mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
					mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
					mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(mudraMoneyTransactionBean.getVerificationAmount());
					mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND To AGENT(Verification)");
					mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
					mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(Verification-REFUND To AGENT)");
					session.save(mudraMoneyTransactionBeanSaveTxnRef);
					transaction.commit();
					transaction = session.beginTransaction();
					 String dmtSettlementCode=new
					 TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),walletMastBean.getWalletid(),mudraMoneyTransactionBean.getVerificationAmount(),ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent());
					 String surchargeRefundSettlementCode=new
					 TransactionDaoImpl().surchargeRefundSettlement(walletMastBean.getId(),walletMastBean.getWalletid(),/*mudraMoneyTransactionBean.getSurChargeAmount()*/surcharge+distSurcharge+supDistributorSurcharge,ppiTxnIdVerification,mudraMoneyTransactionBean.getIpiemi(),"",walletMastBean.getAggreatorid(),mudraMoneyTransactionBean.getAgent(),distSurcharge,walletMastBean.getDistributerid(),supDistributorSurcharge,(superDistributorMast.getCashDepositeWallet() == null ?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),0.0,0.0);
					 if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
					 {
						 String AggRefundCode=transactionDao.walletToAggregatorRefund(ppiTxnIdVerification,walletmastAgg.getCashDepositeWallet(),mudraMoneyTransactionBean.getVerificationAmount(),surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),0,0);
					 }

					/* if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()){
//							(respTxnId, userId, amount, supDistributorSurcharge, ipImei, aggreatorid, superDistributorId, agent)
							superDistributorTxnCode = transactionDao.walletToSuperDistributorTransferRefund(ppiTxnIdVerification,walletMastBean.getId(),(verifyAmt),surchargeBean.getSuDistSurcharge(), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(),walletMastBean.getSuperdistributerid(), mudraMoneyTransactionBean.getAgent());
						}*/
					 transaction.commit();
				} else {
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid: "+mudraMoneyTransactionBean.getAgentid()+" inside else block "+actCode);
					
					mudraMoneyTransactionBean.setStatus("SUCCESS");
					mudraMoneyTransactionBean.setRemark("Transaction SUCCESSFUL(Verification)");
					mudraMoneyTransactionBean.setBankRrn(rrn);
					mudraMoneyTransactionBean.setBankResp("SUCCESS");
					session.update(mudraMoneyTransactionBean);

					/*
					 * String txnStatus=new
					 * SaraswatBankIMPS().getTransactionStatus(bankResp.get(
					 * "rrnNo")); logger.info(serverName
					 * +"*******RequestId****"+requestId+
					 * "*************************txnStatus**for  while acc verivication **:"
					 * +txnStatus); HashMap<String, String> txnStatusResp=new
					 * XmlParser().xmlParser(txnStatus);
					 */

					if (/* txnStatusResp.get("benefName") */beneName.trim()
							.equalsIgnoreCase(mudraBeneficiaryBank.getName().trim())) {
						mudraBeneficiaryBank.setVerified("V");
						mudraBeneficiaryBank.setVerifiedStatus("V");
						mudraBeneficiaryBank.setVerifiedon(new java.sql.Date(System.currentTimeMillis()));
						session.update(mudraBeneficiaryBank);
						mudraMoneyTransactionBean.setVerified("V");
						mudraMoneyTransactionBean.setAccHolderName(
								/* txnStatusResp.get("benefName") */beneName + "(V)");
						mudraMoneyTransactionBean.setVerificationDesc("Match");
						// mudraMoneyTransactionBean.set

					} else {
						mudraBeneficiaryBank.setVerified("NV");
						mudraBeneficiaryBank.setVerifiedStatus("V");
						mudraBeneficiaryBank.setVerifiedon(new java.sql.Date(System.currentTimeMillis()));
						session.update(mudraBeneficiaryBank);
						mudraMoneyTransactionBean.setVerified("NV");
						mudraMoneyTransactionBean.setAccHolderName(beneName + "(NV)");
						mudraMoneyTransactionBean.setVerificationDesc("Not Matched");
					}

					transaction.commit();

					/*
					 * if(txnStatusResp.get("response").equalsIgnoreCase(
					 * "SUCCESS")){
					 * mudraMoneyTransactionBean.setAccHolderName(txnStatusResp.
					 * get("benefName")+"(V)"); }else
					 * mudraMoneyTransactionBean.setAccHolderName(
					 * mudraBeneficiaryBank.getName()+"(NV)"); }
					 */
					// added on 4nd Aug
					java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
					mudraMoneyTransactionBean.setPtytransdt(sqlDate);
					mudraMoneyTransactionBean.setTxnId(ppiTxnIdVerification);
					mudraMoneyTransactionBean.setStatusCode("1000");
					mudraMoneyTransactionBean.setStatusDesc("Transaction Successful(Verification)");

					String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer",
							mudraSenderBank.getAggreatorId());
					if(smstemplet!=null) {
					
					smstemplet = smstemplet.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getVerificationAmount())
							.replaceAll("<<ACCNO>>", mudraBeneficiaryBank.getAccountNo())
							.replaceAll("<<IFSCCODE>>", " "+getDate("yyyy/MM/dd HH:mm:ss"));
						
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid:"+mudraMoneyTransactionBean.getAgentid()+"*******RequestId****" + requestId
							+ "verifyAccount.getMobileNo()``"+smstemplet
							 );
					
					//Rs.<<AMOUNT>> successfully transferred to a/c no.<<ACCNO>> on<<IFSCCODE>>.Powered by Bhartipay.
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							mudraSenderBank.getMobileNo(),
							smstemplet,
							"SMS", mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "NOTP");

					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					}
				}

			} else {
				transaction = session.beginTransaction();
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) session
						.get(MudraMoneyTransactionBean.class, "VT" + ppiTxnIdVerification);
				mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
				mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
				session.update(mudraMoneyTransactionBeanSaveTxn);
				transaction.commit();
				if(walletTxnCode.equals("1000"))
				{
					if( walletSurChargeTxnCode.equalsIgnoreCase("1000"))
					{
						if(!walletTxnCodeAgg.equals("1000"))
						{
							mudraMoneyTransactionBean.setStatusCode(walletTxnCodeAgg);
						}
						else
						{
							mudraMoneyTransactionBean.setStatusCode("1001");
						}
					}
					else
					{
						mudraMoneyTransactionBean.setStatusCode(walletSurChargeTxnCode);
					}
				}
				else
				{
					mudraMoneyTransactionBean.setStatusCode(walletTxnCode);
				}
				/*mudraMoneyTransactionBean.setStatusCode(walletTxnCode);*/
				mudraMoneyTransactionBean.setStatusDesc("Velocity check error");
			}

			mudraMoneyTransactionBean
					.setAgentWalletAmount(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()));
			session.evict(mudraSenderBank);
			MudraSenderBank mudraSenderNew = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraSenderBank.getId());
			mudraMoneyTransactionBean.setSenderLimit(mudraSenderNew.getTransferLimit());

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid:"+ mudraMoneyTransactionBean.getAgentid()+"*******RequestId****" + requestId
					+ "problem in verifyAccount" + e.getMessage()+" "+e);
			
			mudraMoneyTransactionBean.setStatusCode("7000");
			mudraMoneyTransactionBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(transaction.isActive())
				transaction.commit();
			session.close();
		}

		return mudraMoneyTransactionBean;

	}


		private String getDate(String string) {
			
				//TimeZone tz = TimeZone.getTimeZone("IST");
				DateFormat df = new SimpleDateFormat(string); // Quoted "Z" to indicate UTC, no timezone offset
				//df.setTimeZone(tz);
				return df.format(new Date());
			}

		public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName, String requestId){
			FundTransactionSummaryBean fundTransactionSummaryBean=new FundTransactionSummaryBean();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", mudraMoneyTransactionBean.getTxnId(), serverName +"mudraMoneyTransactionBean()");	
			String txId="";
			Session session = factory.openSession();
			try{
			
				MudraMoneyTransactionBean mBean=new MudraMoneyTransactionBean();
				TransactionBean transactionBean=new TransactionBean();
				
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", mudraMoneyTransactionBean.getTxnId()));
				
				List<MudraMoneyTransactionBean> results = cr.list();
				if(results!=null&&results.size()>0){
				mBean=results.get(0);
				txId=mBean.getId();
				
				WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, mBean.getAgentid());
				if(mast!=null) {
				mBean.setAgentName(mast.getName());
				mBean.setAgentMobileNo(mast.getMobileno());
				 }
				}
				String mode="";
				if(results!=null){
					for (MudraMoneyTransactionBean mudraMoneyTransactionBean2 : results) {
						if(mudraMoneyTransactionBean2!=null&&(mudraMoneyTransactionBean2.getNarrartion().equalsIgnoreCase("NEFT")||mudraMoneyTransactionBean2.getNarrartion().equalsIgnoreCase("IMPS"))){
							mode=mudraMoneyTransactionBean2.getNarrartion();
							break;
						}
					}
				}
				
				MudraBeneficiaryBank mudraBeneficiaryBank=(MudraBeneficiaryBank)session.get(MudraBeneficiaryBank.class, mBean.getBeneficiaryId());
				MudraSenderBank mudraSenderBank=(MudraSenderBank)session.get(MudraSenderBank.class, mBean.getSenderId());
				
				Criteria criteria= session.createCriteria(TransactionBean.class);
				criteria.add(Restrictions.eq("resptxnid", mudraMoneyTransactionBean.getTxnId()));
				criteria.add(Restrictions.eq("txncode",4));
				List<TransactionBean> tList=criteria.list();
				if(tList!=null&&tList.size()>0){
					transactionBean=tList.get(0);
				}
				
				List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
				StringBuilder serchQuery = new StringBuilder();
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE id=:agentid");
				SQLQuery query = session.createSQLQuery(serchQuery.toString());
				query.setString("agentid", txId); 
				query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
				transList = query.list();
				
				TransacationLedgerBean transacationLedgerBean=new TransacationLedgerBean();
				if(transList.size()>0)
				{
					transacationLedgerBean = transList.get(0);
				}	
				
			fundTransactionSummaryBean.setPtDate(transacationLedgerBean.getPtytransdt());	
			fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
			fundTransactionSummaryBean.setStatusCode("1000");
			fundTransactionSummaryBean.setStatusDesc("SUCCESS");
			fundTransactionSummaryBean.setSenderMobile(mudraSenderBank.getMobileNo());
			fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryBank.getName());
			fundTransactionSummaryBean.setBankName(mudraBeneficiaryBank.getBankName());
			fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryBank.getIfscCode());
			fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryBank.getAccountNo());
			fundTransactionSummaryBean.setAgentName(mBean.getAgentName());
			fundTransactionSummaryBean.setShopName(mBean.getAgentName());
			fundTransactionSummaryBean.setAgentMobileNo(mBean.getAgentMobileNo());
			
			fundTransactionSummaryBean.setAmount(transactionBean.getTxndebit());
			fundTransactionSummaryBean.setCharges("As applicable.");
			fundTransactionSummaryBean.setMode(mode);

	
			}catch(Exception e){
				e.printStackTrace();
			}
			finally {
				session.close();
			}
			return fundTransactionSummaryBean;
		}


	public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName +"|Agentid :"+mudraBeneficiaryBank.getAggreatorId()+"****RequestId****" + requestId
				+ "fundTransfer()");
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		FundTransactionSummaryBean fundTransactionSummaryBean = new FundTransactionSummaryBean();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");
		double refundAgt = 0.0;
		boolean smsFlag = false;
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "|getBeneficiaryId:"
					+ mudraMoneyTransactionBean.getBeneficiaryId()
					+ "|getNarrartion:"
					+ mudraMoneyTransactionBean.getNarrartion()
					+ "getTxnAmount :"
					+ mudraMoneyTransactionBean.getTxnAmount());
			

			TreeMap<String, String> treeMap = new TreeMap<String, String>();
			treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(treeMap);
			if (statusCode != 1000) {
				fundTransactionSummaryBean.setStatusCode("" + statusCode);
				fundTransactionSummaryBean.setStatusDesc("Security error.");
				return fundTransactionSummaryBean;
			}

			
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					mudraMoneyTransactionBean.getUserId());
			

			WalletConfiguration walletConfig  = (WalletConfiguration)session.get(WalletConfiguration.class, walletMastBean.getAggreatorid());
			
			if("IMPS".equalsIgnoreCase(mudraMoneyTransactionBean.getTransType()) && "0".equalsIgnoreCase(walletConfig.getfImps())) {
				
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Bank2: IMPS services is down.");
				return fundTransactionSummaryBean;
			}
			
			if("NEFT".equalsIgnoreCase(mudraMoneyTransactionBean.getTransType()) && "0".equalsIgnoreCase(walletConfig.getfNeft())) {
				
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Bank2: NEFT Services is down.");
				return fundTransactionSummaryBean;
				
			}
			
			if("0".equalsIgnoreCase(walletMastBean.getBank2())) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Bank2: Please ask your distributer to enable services.");
				return fundTransactionSummaryBean;
			}
			
			
			
			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null
					|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null
					|| mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Wallet Id can't be empty.");
				return fundTransactionSummaryBean;
			}

			
			/**  this is use for checking double transaction.  **/
			CommanUtilDaoImpl commanUtilDaoImpl=new CommanUtilDaoImpl();
			String insertFlag=commanUtilDaoImpl.insertTransDtl(mudraMoneyTransactionBean.getUserId()+"|"+mudraMoneyTransactionBean.getSenderId()+"|"+mudraMoneyTransactionBean.getBeneficiaryId()+"|"+mudraMoneyTransactionBean.getTxnAmount(),mudraMoneyTransactionBean.getNarrartion());
					if(!insertFlag.equalsIgnoreCase("SUCCESS"))
					{
						try{
							fundTransactionSummaryBean.setStatusCode("7111");
							if(!insertFlag.equalsIgnoreCase("FAIL"))
							fundTransactionSummaryBean.setStatusDesc(insertFlag);
							else
							fundTransactionSummaryBean.setStatusDesc("You have already processed transaction with same details please wait for 10 minutes.");
							}catch(Exception e){
								e.printStackTrace();
							}
						
						return fundTransactionSummaryBean;
					}
			
					//String deleteBene = new com.appnit.bhartipay.wallet.user.service.impl.UserServiceImpl().deleteTransDtl(mudraMoney.getUserId()+"|"+mudraMoney.getSenderId()+"|"+mudraMoney.getBeneficiaryId()+"|"+mudraMoney.getTxnAmount());

			
			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double amtTxn=mudraMoneyTransactionBean.getTxnAmount();
			double agnWalletTxnAmount = 0;
			double surChargeAmount = 0;
			double wlSurcharge = 0;//while label Aggregator Surcharge
			double surChargeDist = 0;
			double supDistSurcharge=0;//super distributor surcharge
			double agentRefund=0;
			double aggReturnVal=0.0;
			double dtdsApply=0.0;
			String planId="N/A";
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn = null;
			
			WalletMastBean walletmastAgg = (WalletMastBean) session.get(WalletMastBean.class,
					walletMastBean.getAggreatorid());
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraMoneyTransactionBean.getSenderId());
			MudraBeneficiaryBank mudraBeneficiaryBank = (MudraBeneficiaryBank) session.get(MudraBeneficiaryBank.class,
					mudraMoneyTransactionBean.getBeneficiaryId());
			WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
			 superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class,	walletMastBean.getSuperdistributerid());
			}
			 
			// logic needs to be changed for verifing beneficiary and setting V in below try and catch inside if
			if(walletMastBean.getUsertype() != 2)
			{
				fundTransactionSummaryBean.setStatusCode("7000");
				fundTransactionSummaryBean.setStatusDesc("Invalid User.");
				return fundTransactionSummaryBean;	
			}
			
			if (mudraBeneficiaryBank != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			// monthly Transfer Limit validation
			if (mudraSenderBank.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
				return fundTransactionSummaryBean;
			}

			fundTransactionSummaryBean.setBankName(mudraBeneficiaryBank.getBankName());
			fundTransactionSummaryBean.setAgentName(walletMastBean.getName());
			fundTransactionSummaryBean.setShopName(walletMastBean.getShopName());
			fundTransactionSummaryBean.setAgentMobileNo(walletMastBean.getMobileno());
			/*// Yearly Transfer Limit validation
			if (mudraSenderBank.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderBank.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}*/

			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {
				if (mudraMoneyTransactionBean.getTransType().equalsIgnoreCase("IMPS"))
				   ppiTxnIdfundTransfer = commanUtilDao.getTrxId("FBTX",walletMastBean.getAggreatorid());
				else 
				  ppiTxnIdfundTransfer = commanUtilDao.getTrxId("FBTXNEFT",walletMastBean.getAggreatorid());
			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();

				
				if (mudraMoneyTransactionBean != null && mudraMoneyTransactionBean.getAccHolderName() != null
						&& !mudraMoneyTransactionBean.getAccHolderName().isEmpty()) {
					try {
						System.out.println(mudraMoneyTransactionBean.getAccHolderName());
						mudraBeneficiaryBank.setVerified("V");
						mudraBeneficiaryBank.setVerifiedStatus("V");
						mudraBeneficiaryBank.setVerifiedon(new java.sql.Date(System.currentTimeMillis()));
						mudraBeneficiaryBank.setName(mudraMoneyTransactionBean.getAccHolderName());
						session.update(mudraBeneficiaryBank);
					} catch (Exception e) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), "************************************* problem in update bene details "
								+ mudraBeneficiaryBank.getName());
						
					}
				}
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "fundTransfer start with fundtxnid:" + ppiTxnIdfundTransfer);
			
			String walletTxnCode = "1000";
			String walletSurChargeTxnCode = "1000";
			String walletAggTxnCode = "1000";
			String walletSuperDistributorTxnCode="1000";
			
			if (mudraSenderBank.getWalletBalance() - mudraMoneyTransactionBean.getTxnAmount() < 0) {
				agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount() - mudraSenderBank.getWalletBalance();
				refundAgt = agnWalletTxnAmount;
				surChargeAmount = 0;
				SurchargeBean surchargeBean = new SurchargeBean();
				// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(agnWalletTxnAmount);
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					surchargeBean.setTransType("NEFT");
				} else {
					surchargeBean.setTransType("IMPS");
				}
				surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

				if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
					surChargeAmount = surchargeBean.getSurchargeAmount();
					surChargeDist = surchargeBean.getDistSurCharge();
					wlSurcharge=surchargeBean.getCreditSurcharge();
					supDistSurcharge=surchargeBean.getSuDistSurcharge();
					agentRefund=surchargeBean.getAgentRefund();
					aggReturnVal = surchargeBean.getAggReturnVal();
					dtdsApply = surchargeBean.getDtdsApply();

				} else {
					fundTransactionSummaryBean.setStatusCode("1122");
					fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return fundTransactionSummaryBean;
				}

				if ((surChargeAmount + agnWalletTxnAmount + surChargeDist+supDistSurcharge) > new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())) {
					fundTransactionSummaryBean.setStatusCode("7024");
					fundTransactionSummaryBean.setStatusDesc("Insufficient Agent Wallet Balance.");
					return fundTransactionSummaryBean;
				}

				if (walletmastAgg.getWhiteLabel().equalsIgnoreCase("1") &&(amtTxn +wlSurcharge) > new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())) {
					fundTransactionSummaryBean.setStatusCode("7024");
					fundTransactionSummaryBean.setStatusDesc("Something went wrong please try again later.");
					return fundTransactionSummaryBean;
				} else if (walletmastAgg.getWhiteLabel().equalsIgnoreCase("1")) {
					double countSur = 1;
					DecimalFormat df = new DecimalFormat("###");
					double modCharge= amtTxn % 5000;
					double lastCreditSur = 0;
					if (amtTxn > 5000.00) {
						if (modCharge == 0)
						{
							countSur = Integer.parseInt(df.format((amtTxn / 5000)));
						}
						else
						{
							SurchargeBean surchargeBeanlast = new SurchargeBean();
							// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
							surchargeBeanlast.setAgentId(mudraMoneyTransactionBean.getUserId());
							surchargeBeanlast.setAmount(modCharge);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								surchargeBeanlast.setTransType("NEFT");
							} else {
								surchargeBeanlast.setTransType("IMPS");
							}
							surchargeBeanlast = calculateSurcharge(surchargeBeanlast, serverName, requestId);

							if (surchargeBeanlast.getStatus().equalsIgnoreCase("1000")) {
								lastCreditSur=surchargeBeanlast.getCreditSurcharge();
							} else {
								fundTransactionSummaryBean.setStatusCode("1122");
								fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
								return fundTransactionSummaryBean;
							}
							countSur = Integer.parseInt(df.format(((amtTxn-modCharge) / 5000)));
							
						}
					}
					
					
					
					
					walletAggTxnCode = transactionDao.walletToAggregatorTransfer(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),
							( amtTxn ),
							wlSurcharge, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),aggReturnVal,dtdsApply);


				}
				
	/*			if(superDistributorMast!=null&&superDistributorMast.getId()!=null&&!superDistributorMast.getId().isEmpty()){
					double countSur = 1;
					DecimalFormat df = new DecimalFormat("###");
					double modCharge= amtTxn % 5000;
					double lastCreditSur = 0;
					if (amtTxn > 5000.00) {
						if (modCharge == 0)
						{
							countSur = Integer.parseInt(df.format((amtTxn / 5000)));
						}
						else
						{
							SurchargeBean surchargeBeanlast = new SurchargeBean();
							// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
							surchargeBeanlast.setAgentId(mudraMoneyTransactionBean.getUserId());
							surchargeBeanlast.setAmount(modCharge);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								surchargeBeanlast.setTransType("NEFT");
							} else {
								surchargeBeanlast.setTransType("IMPS");
							}
							surchargeBeanlast = calculateSurcharge(surchargeBeanlast, serverName, requestId);

							if (surchargeBeanlast.getStatus().equalsIgnoreCase("1000")) {
								lastCreditSur=surchargeBeanlast.getCreditSurcharge();
							} else {
								fundTransactionSummaryBean.setStatusCode("1122");
								fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
								return fundTransactionSummaryBean;
							}
							countSur = Integer.parseInt(df.format(((amtTxn-modCharge) / 5000)));
							
						}
					}
					
					
					
//					(respTxnId, userId, amount, surcharge, ipImei, aggreatorid, superDistributorId, agent)
					walletAggTxnCode = transactionDao.walletToSuperDistributorTransfer(ppiTxnIdfundTransfer,walletMastBean.getId(),
							( amtTxn ),
							((countSur * wlSurcharge)+lastCreditSur), mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(),superDistributorMast.getId(), mudraMoneyTransactionBean.getAgent());
				}*/
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" 
						+ "fundTransfer :" + ppiTxnIdfundTransfer);

				

				// mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				/*
				 * session.save(mudraMoneyTransactionBeanSaveTxn);
				 * transaction.commit();
				 */
				planId=getPlanIdByAgentId(walletMastBean.getId());
				
				if (walletAggTxnCode.equalsIgnoreCase("1000")) {
					walletTxnCode = new TransactionDaoImpl().walletToAccount(walletMastBean.getId(),
							walletMastBean.getWalletid(), agnWalletTxnAmount, mudraBeneficiaryBank.getAccountNo(),
							ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0);
				}
				if (walletTxnCode.equalsIgnoreCase("1000") && walletAggTxnCode.equalsIgnoreCase("1000")) {
					walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurchargeDist(
							walletMastBean.getId(), walletMastBean.getWalletid(), (surChargeAmount + surChargeDist+supDistSurcharge),
							mudraBeneficiaryBank.getAccountNo(), ppiTxnIdfundTransfer,
							mudraMoneyTransactionBean.getIpiemi(), walletMastBean.getAggreatorid(),
							mudraMoneyTransactionBean.getAgent(), 0.0, surChargeDist,
							walletMastBean.getDistributerid(),supDistSurcharge,(superDistributorMast.getCashDepositeWallet() == null ? walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund,dtdsApply);
				}
				
				
					if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")
							&& walletAggTxnCode.equalsIgnoreCase("1000")) {
						
						/*transaction.commit();
						transaction = session.beginTransaction();*/
						// mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean)session.get(MudraMoneyTransactionBean.class,
						// "T"+ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
								.clone();

//						mudraMoneyTransactionBeanSaveTxn.setPtytransdt(new java.sql.Date(new Date().getTime()));
						mudraMoneyTransactionBeanSaveTxn.setIsBank(1);
						mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
						mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
						mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setPlanId(planId);
						// session.update(mudraMoneyTransactionBeanSaveTxn);
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();
					
					
					
					
					} else {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() + "****"
								
								+ "velocity error :"
								+ walletTxnCode);

						
						transaction = session.beginTransaction();
						// mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean)session.get(MudraMoneyTransactionBean.class,
						// "T"+ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
								.clone();

//						mudraMoneyTransactionBeanSaveTxn.setPtytransdt(new java.sql.Date(new Date().getTime()));
						mudraMoneyTransactionBeanSaveTxn.setIsBank(1);
						mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
						mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
						mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
						mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
						mudraMoneyTransactionBeanSaveTxn.setPlanId(planId);
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();

					}

			}

			if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")
					&& walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				int i = 1;
				do {
					String responseCode = null;
					String actCode=null;
					MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
							.clone();
//					fundSaveTxn.setPtytransdt(new java.sql.Date(new Date().getTime()));
					fundSaveTxn.setIsBank(1);
					fundSaveTxn.setPlanId(planId);
					transaction = session.beginTransaction();
					fundSaveTxn.setId("FB" + i + ppiTxnIdfundTransfer);
					fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
					if (txnAmount > 5000) {
						fundSaveTxn.setDrAmount(5000);
						txnAmount = txnAmount - 5000;

					} else {
						fundSaveTxn.setDrAmount(txnAmount);
						txnAmount = 0;
					}
					if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
						fundSaveTxn.setStatus("NEFT INITIATED");
					} else {
						fundSaveTxn.setStatus("PENDING");
						// fundSaveTxn.setStatus("IMPS INITIATED");
					}
					session.save(fundSaveTxn);
					transaction.commit();
					
					transaction = session.beginTransaction();
					List<String> respList=new ArrayList<>();
					if(!(walletMastBean.getNEFTBatch()==1 && mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT"))){
						respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),fundSaveTxn.getDrAmount(),mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getName(),fundSaveTxn.getId(),mudraSenderBank.getMobileNo(),mudraSenderBank.getFirstName());
					}
					else{
						respList.add("0");
						respList.add("S");
						respList.add("null");
					}
					
					
					
					responseCode = respList.get(0);
					actCode = respList.get(1);
						transaction = session.beginTransaction();
					
					 fundSaveTxn.setUserNarration("from service");
					
					if (!responseCode.equals("0")|| !(actCode.equals("0") ||actCode.equalsIgnoreCase("S")|| actCode.equals("26") || actCode.equals("11"))) {
						
						
					/*******************start Dynamic bank blocking **************************/	
					if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					try{	
					TableCache tableCache=CacheFactory.getCache();
					CacheModel model=tableCache.getCache(mudraBeneficiaryBank.getBankName());
					if(model==null){
						model=new CacheModel();
						model.setLastAccess(System.currentTimeMillis());
						model.setCount(1);
					}
					else
					{
					 int count=model.getCount();
					 if(count>=TableCacheImpl.TXN_FAILED_COUNT){
						Session sessionForCache=factory.openSession(); 
						Transaction transactionForCache=sessionForCache.beginTransaction();
						Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,2)");
						query.setParameter("bankName",mudraBeneficiaryBank.getBankName());
						query.executeUpdate();
						transactionForCache.commit();
					 }
					 count++;
					 model.setLastAccess(System.currentTimeMillis());
					 model.setCount(count);
					}
					tableCache.putCache(mudraBeneficiaryBank.getBankName(), model);
					}catch(Exception exception){
						exception.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
								+ mudraMoneyTransactionBean.getAgentid() 
								);
						
					}
					}
					/*******************end Dynamic bank blocking **************************/	
						
					if(responseCode.equals("1")){
							fundSaveTxn.setStatus("FAILED");
							fundSaveTxn.setRemark("Transaction FAILED");
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("FAILED");
						}
						 else{
								refundAgt = refundAgt - fundSaveTxn.getDrAmount();
								fundSaveTxn.setStatus("Confirmation Awaited");
								fundSaveTxn.setRemark("Transaction Pending");
								if(!respList.get(2).equals("null"))
									fundSaveTxn.setBankRrn(respList.get(2));
								fundSaveTxn.setBankResp("Confirmation Awaited");
							} 
						// if(i==1){
						/*
						 * MudraMoneyTransactionBean wallRefundTxnRef=new
						 * MudraMoneyTransactionBean();
						 * 
						 * wallRefundTxnRef.setId("TR"+ppiTxnIdfundTransfer);
						 * wallRefundTxnRef.setIsBank(1);
						 * wallRefundTxnRef.setTxnId(ppiTxnIdfundTransfer);
						 * wallRefundTxnRef.setWalletId(
						 * mudraMoneyTransactionBean.getWalletId());
						 * wallRefundTxnRef.setSenderId(
						 * mudraMoneyTransactionBean.getSenderId());
						 * wallRefundTxnRef.setBeneficiaryId(
						 * mudraMoneyTransactionBean.getBeneficiaryId());
						 * wallRefundTxnRef.setCrAmount(fundSaveTxn.getDrAmount(
						 * )); wallRefundTxnRef.setDrAmount(0.0);
						 * wallRefundTxnRef.setNarrartion(
						 * "WALLET LOADING(REFUND)");
						 * wallRefundTxnRef.setStatus("SUCCESS");
						 * wallRefundTxnRef.setRemark(
						 * "Transaction SUCCESSFUL(REFUND)");
						 * session.save(wallRefundTxnRef);
						 */

						if (/* agnWalletTxnAmount */refundAgt > 0) {

							smsFlag = true;
							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
							// (MudraMoneyTransactionBean )
							// mudraMoneyTransactionBean.clone();

							mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setIsBank(1);
							mudraMoneyTransactionBeanSaveTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef
									.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(/* agnWalletTxnAmount */refundAgt);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("NEFT");
							} else {
								mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("IMPS");
							}
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
							mudraMoneyTransactionBeanSaveTxnRef.setPlanId(planId);
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To AGENT)");
//							mudraMoneyTransactionBeanSaveTxnRef.setPtytransdt(new java.sql.Date(new Date().getTime()));
							session.save(mudraMoneyTransactionBeanSaveTxnRef);

							SurchargeBean surchargeBean = new SurchargeBean();
							// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
							surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
							surchargeBean.setAmount(refundAgt);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								surchargeBean.setTransType("NEFT");
							} else {
								surchargeBean.setTransType("IMPS");
							}
							surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

							if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
								surChargeAmount = surchargeBean.getSurchargeAmount();
								surChargeDist = surchargeBean.getDistSurCharge();
								supDistSurcharge=surchargeBean.getSuDistSurcharge();
								agentRefund=surchargeBean.getAgentRefund();
								aggReturnVal=surchargeBean.getAggReturnVal();
								dtdsApply=surchargeBean.getDtdsApply();
								wlSurcharge=surchargeBean.getCreditSurcharge();
								
							} else {
								fundTransactionSummaryBean.setStatusCode("1122");
								fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
								return fundTransactionSummaryBean;
							}

							String dmtSettlementCode = new TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),
									walletMastBean.getWalletid(), /* agnWalletTxnAmount */refundAgt,
									ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(), "",
									walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
							String surchargeRefundSettlementCode = new TransactionDaoImpl().surchargeRefundSettlement(
									walletMastBean.getId(), walletMastBean.getWalletid(),
									(surChargeAmount + surChargeDist+supDistSurcharge), ppiTxnIdfundTransfer,
									mudraMoneyTransactionBean.getIpiemi(), "", walletMastBean.getAggreatorid(),
									mudraMoneyTransactionBean.getAgent(), surChargeDist,
									walletMastBean.getDistributerid(),supDistSurcharge,(superDistributorMast.getCashDepositeWallet() == null ?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund,dtdsApply);
							if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
							{
							
								double countSur = 1;
								DecimalFormat df = new DecimalFormat("###");
								
								
								double modCharge= refundAgt % 5000;
								double lastCreditSur = 0;
								double lastAggReturnVal = 0;
								if (refundAgt > 5000.00) {
									if (modCharge == 0)
									{
										countSur = Integer.parseInt(df.format((refundAgt / 5000)));
									}
									else
									{
										SurchargeBean surchargeBeanlast = new SurchargeBean();
										// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
										surchargeBeanlast.setAgentId(mudraMoneyTransactionBean.getUserId());
										surchargeBeanlast.setAmount(modCharge);
										if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
											surchargeBeanlast.setTransType("NEFT");
										} else {
											surchargeBeanlast.setTransType("IMPS");
										}
										surchargeBeanlast = calculateSurcharge(surchargeBeanlast, serverName, requestId);

										if (surchargeBeanlast.getStatus().equalsIgnoreCase("1000")) {
											lastCreditSur=surchargeBeanlast.getCreditSurcharge();
										} else {
											fundTransactionSummaryBean.setStatusCode("1122");
											fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
											return fundTransactionSummaryBean;
										}
										countSur = Integer.parseInt(df.format(((refundAgt-modCharge) / 5000)));
										
									}
								}
								
								//ppiTxnIdfundTransfer,walletMastBean.getId(),( amtTxn ),(countSur * surchargeBean.getCreditSurcharge()), mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent()
								String AggRefundCode =  transactionDao.walletToAggregatorRefund(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),refundAgt,wlSurcharge,mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),aggReturnVal,dtdsApply);

							}
							
						}
						

						txnAmount = 0;

					} else {
//dynamic routing.
						i++;
						refundAgt = refundAgt - fundSaveTxn.getDrAmount();
						
						if (actCode.equals("0") || actCode.equalsIgnoreCase("S")
								|| actCode.equals("26") || actCode.equals("11")) {
							if(mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")){
								fundSaveTxn.setStatus("NEFT INITIATED");
								fundSaveTxn.setRemark("NEFT INITIATED");
							}else{
								fundSaveTxn.setStatus("SUCCESS");
								fundSaveTxn.setRemark("Transaction SUCCESSFUL");
							}
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("SUCCESS");
							
							/*******************start Dynamic bank blocking **************************/	
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
							try{	
							TableCache tableCache=CacheFactory.getCache();
							tableCache.removeCache(mudraBeneficiaryBank.getBankName());
							
							}catch(Exception exception){
								exception.printStackTrace();
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
										+ mudraMoneyTransactionBean.getAgentid() 
										);
								
							}
							}
							/*******************end Dynamic bank blocking **************************/	
							
							
						} else {
							smsFlag = true;
							fundSaveTxn.setStatus("Confirmation Awaited");
							fundSaveTxn.setRemark("Transaction Pending");
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("Confirmation Awaited");
							
							/*******************start Dynamic bank blocking **************************/	
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) 
							{
								try{	
								TableCache tableCache=CacheFactory.getCache();
								CacheModel model=tableCache.getCache(mudraBeneficiaryBank.getBankName());
								if(model==null)
								{
									model=new CacheModel();
									model.setLastAccess(System.currentTimeMillis());
									model.setCount(1);
								}
								else
								{
								 int count=model.getCount();
								 if(count>=TableCacheImpl.TXN_FAILED_COUNT)
								 {
									Session sessionForCache=factory.openSession(); 
									Transaction transactionForCache=sessionForCache.beginTransaction();
									Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,2)");
									query.setParameter("bankName",mudraBeneficiaryBank.getBankName());
									query.executeUpdate();
									transactionForCache.commit();
								 }
								 count++;
								 model.setLastAccess(System.currentTimeMillis());
								 model.setCount(count);
								}
								tableCache.putCache(mudraBeneficiaryBank.getBankName(), model);
								}catch(Exception exception){
									exception.printStackTrace();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
											+ mudraMoneyTransactionBean.getAgentid() 
											);
									
								}
							}
							/*******************end Dynamic bank blocking **************************/	
						}
					}
					// fundSaveTxn.setBankResp(bankRespXml);
//					fundSaveTxn.setPtytransdt(new java.sql.Date(new Date().getTime()));
					session.update(fundSaveTxn);
					transaction.commit();
					// }
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "|" + requestId + "|"
							+ mudraMoneyTransactionBean.getAgentid() 
							);
					
					/*
					 * transaction=session.beginTransaction(); SQLQuery
					 * insertQuery = session.createSQLQuery("" +
					 * "insert into dmtdetailsmast(senderid,agentid,txnid,sendermobileno,sendername,beneaccountno,beneifsccode,txnamount,remittancetype,remittancemode,remittancestatus,kyctype,beneid,distributerid,aggreatorid)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
					 * ); insertQuery.setParameter(0, mudraSenderBank.getId());
					 * insertQuery.setParameter(1,
					 * mudraMoneyTransactionBean.getAgentid());
					 * insertQuery.setParameter(2, fundSaveTxn.getId());
					 * insertQuery.setParameter(3,mudraSenderBank.getMobileNo())
					 * ; insertQuery.setParameter(4,
					 * mudraSenderBank.getFirstName());
					 * insertQuery.setParameter(5,
					 * mudraBeneficiaryBank.getAccountNo());
					 * insertQuery.setParameter(6,
					 * mudraBeneficiaryBank.getIfscCode());
					 * insertQuery.setParameter(7, fundSaveTxn.getDrAmount());
					 * insertQuery.setParameter(8, "M");
					 * insertQuery.setParameter(9,
					 * mudraMoneyTransactionBean.getNarrartion());
					 * insertQuery.setParameter(10, fundSaveTxn.getStatus());
					 * insertQuery.setParameter(11,
					 * mudraSenderBank.getKycStatus());
					 * insertQuery.setParameter(12,
					 * mudraBeneficiaryBank.getId());
					 * insertQuery.setParameter(13,
					 * walletMastBean.getDistributerid());
					 * insertQuery.setParameter(14,
					 * walletMastBean.getAggreatorid());
					 * 
					 * insertQuery.executeUpdate();
					 * session.getTransaction().commit();
					 */
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
							+ "completed insert into dmtdetailsmast");
					

				} while (txnAmount != 0);

				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderBank.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryBank.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryBank.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryBank.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("As applicable");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());
				if ("NV".equalsIgnoreCase(mudraBeneficiaryBank.getVerifiedStatus())) {
					try {
						Transaction tx = session.beginTransaction();
						mudraBeneficiaryBank.setVerifiedStatus("V");
						session.saveOrUpdate(mudraBeneficiaryBank);
						tx.commit();
					} catch (Exception e) {

					}
				}
				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));
//				if("NV".equalsIgnoreCase(mudraMoneyTransactionBean.getVerifyStatus())) {
//					cr.add(Restrictions.ne("remark", "Transaction SUCCESSFUL(Verification)"));
//				}

				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				fundTransactionSummaryBean
						.setAgentWalletAmount(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()));
				MudraSenderBank mudraSenderNew = (MudraSenderBank) session.get(MudraSenderBank.class,
						mudraSenderBank.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());

				String smsTemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer", walletMastBean.getAggreatorid());
				if(smsTemplet!=null && ! smsFlag) {
					
//				Rs.<<AMOUNT>> successfully transferred to a/c no.<<ACCNO>> on<<IFSCCODE>>.Powered by Bhartipay. 
					
				smsTemplet =	smsTemplet.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
						.replaceAll("<<ACCNO>>", mudraBeneficiaryBank.getAccountNo())
						.replaceAll("<<IFSCCODE>>", " "+getDate("yyyy/MM/dd HH:mm:ss"));
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
						+ "getMobileNo()"+smsTemplet);
				
	
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderBank.getMobileNo(),
						smsTemplet,
						"SMS", mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "NOTP");
				
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				//Dear <<NAME>> you have made payment of Rs.<<AMOUNT>> to <<BENE NAME>> on mobile no. <<MOBILE>>. Transaction ID is <<TXNNO>>.
				String sendMoneySmsTemplet = commanUtilDao.getsmsTemplet("sendMoney", walletMastBean.getAggreatorid());
				sendMoneySmsTemplet = sendMoneySmsTemplet.replaceAll("<<NAME>>", walletMastBean.getId())
						.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
						.replaceAll("<<BENE NAME>>", mudraBeneficiaryBank.getName())
						.replaceAll("<<MOBILE>>", mudraSenderBank.getMobileNo())
						.replaceAll("<<TXNNO>>", ppiTxnIdfundTransfer);
				
				/*
				SmsAndMailUtility smsOnSendMoney = new SmsAndMailUtility("", "", "",walletMastBean.getMobileno(),
						sendMoneySmsTemplet,
						"SMS", mudraSenderBank.getAggreatorId(), "", walletMastBean.getName(), "NOTP");
				ThreadUtil.getThreadPool().execute(smsOnSendMoney);
				*/
				
				}

			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
						+ "velocity error :" + walletTxnCode);
			
				
				/*
				 * transaction=session.beginTransaction();
				 * mudraMoneyTransactionBeanSaveTxn=(MudraMoneyTransactionBean)
				 * session.get(MudraMoneyTransactionBean.class,
				 * "T"+ppiTxnIdfundTransfer);
				 * mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
				 * mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
				 * session.update(mudraMoneyTransactionBeanSaveTxn);
				 * transaction.commit();
				 */
				if(walletTxnCode.equalsIgnoreCase("1000"))
				{
					if(walletSurChargeTxnCode.equalsIgnoreCase("1000"))
					{
						if(!walletAggTxnCode.equalsIgnoreCase("1000"))
						{
							fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
						}
						else
						{
							fundTransactionSummaryBean.setStatusCode("1001");
						}
					}
					else
					{
						fundTransactionSummaryBean.setStatusCode(walletSurChargeTxnCode);
					}
						
				}
				else
				{
					fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				}
				//fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
					+ "problem in fundTransfer" + e.getMessage()+" "+e);
			
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(transaction.isActive())
				transaction.commit();
			session.close();
		}

		return fundTransactionSummaryBean;

	}

	public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean mudraMoneyTransactionBean,
			String serverName, String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "|Agentid:" +mudraMoneyTransactionBean.getAgentid()+ requestId
				);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraMoneyTransactionBean.setStatusCode("1001");
		mudraMoneyTransactionBean.setStatusDesc("Error");
		List objectList = null;
		double surcharge = 0.0;
		double agentCashBack=0.0;
		double aggReturnVal=0.0;
		double dtdsApply=0.0;
		
		try {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "|Agentid:" + requestId + mudraMoneyTransactionBean.getAgentid()+" txnAmount :"
					+ mudraMoneyTransactionBean.getTxnAmount()+"|TransType "+ mudraMoneyTransactionBean.getTransType());
			/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "|Agentid:" + requestId + mudraMoneyTransactionBean.getAgentid()+" TransType :"
					+ mudraMoneyTransactionBean.getTransType());*/

			
			// SQLQuery query = session.createSQLQuery("call
			// surchargemodel(:id,:txnAmount)");
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", mudraMoneyTransactionBean.getUserId());
			query.setParameter("txnAmount", mudraMoneyTransactionBean.getTxnAmount());
			if (mudraMoneyTransactionBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", mudraMoneyTransactionBean.getTransType());
			query.setParameter("isBank", "1");
			// query.setBigDecimal("txnAmount",new
			// BigDecimal(mudraMoneyTransactionBean.getTxnAmount()));
			objectList = query.list();

			Object[] obj = (Object[]) objectList.get(0);
			if (obj.length > 1) {
				surcharge = (Double.parseDouble("" + obj[0]) + Double.parseDouble("" + obj[1]));
				if(obj[3]!=null)
					surcharge = surcharge + Double.parseDouble("" + obj[3]); 
				if(obj[4]!=null) {
					agentCashBack=Double.parseDouble(""+obj[4]);
				}
				if(obj[5]!=null) {
					aggReturnVal=Double.parseDouble(""+obj[5]);
				}
				if(obj[6]!=null) {
					dtdsApply=Double.parseDouble(""+obj[6]);
				}
			} else {
				surcharge = Double.parseDouble("" + obj[0]);
			}
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"|Agentid:" + requestId + mudraMoneyTransactionBean.getAgentid()+"surcharge :" + surcharge);
			

			mudraMoneyTransactionBean.setSurChargeAmount(surcharge);
			mudraMoneyTransactionBean.setAgentRefund(agentCashBack);
			mudraMoneyTransactionBean.setAggReturnVal(aggReturnVal);
			mudraMoneyTransactionBean.setDtdsApply(dtdsApply);
			mudraMoneyTransactionBean.setStatusCode("1000");
			mudraMoneyTransactionBean.setStatusDesc("SurCharge Calculated");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName +"|Agentid:" + requestId + mudraMoneyTransactionBean.getAgentid()+"problem in MudraMoneyTransfer" + e.getMessage()+" "+e);
			mudraMoneyTransactionBean.setStatusCode("7000");
			mudraMoneyTransactionBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraMoneyTransactionBean;

	}

	/*
	 * public MudraMoneyTransactionBean
	 * mudraMoneyTransfer(MudraMoneyTransactionBean
	 * mudraMoneyTransactionBean,String serverName) { logger.info(serverName+
	 * "Start excution ******************************************* MudraMoneyTransfer method******:"
	 * ); factory = DBUtil.getSessionFactory(); session = factory.openSession();
	 * try {
	 * 
	 * if (mudraMoneyTransactionBean.getUserId() == null ||
	 * mudraMoneyTransactionBean.getUserId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1117");
	 * mudraMoneyTransactionBean.setStatusDesc("UserId/AgentId can't be empty."
	 * ); return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getSenderId() == null ||
	 * mudraMoneyTransactionBean.getSenderId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getBeneficiaryId() == null ||
	 * mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1116");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Beneficiary Id.");
	 * return mudraMoneyTransactionBean; } if
	 * (mudraMoneyTransactionBean.getAggreatorId() == null ||
	 * mudraMoneyTransactionBean.getAggreatorId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1119");
	 * mudraMoneyTransactionBean.setStatusDesc("Aggregator Id can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getWalletId() == null ||
	 * mudraMoneyTransactionBean.getWalletId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Wallet Id can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getSenderId() == null ||
	 * mudraMoneyTransactionBean.getSenderId().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1110");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Sender Id."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getAmount() <= 0) {
	 * mudraMoneyTransactionBean.setStatusCode("1120");
	 * mudraMoneyTransactionBean.setStatusDesc("Amount can't be zero."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getTransferType() == null ||
	 * mudraMoneyTransactionBean.getTransferType().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1111");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid Transfer Type."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * MudraBeneficiaryBank mudraBeneficiaryBank = (MudraBeneficiaryBank)
	 * session .get(MudraBeneficiaryBank.class,
	 * mudraMoneyTransactionBean.getBeneficiaryId());
	 * 
	 * mudraMoneyTransactionBean.setBankName(mudraBeneficiaryBank.getBankName())
	 * ;
	 * mudraMoneyTransactionBean.setAccountNo(mudraBeneficiaryBank.getAccountNo(
	 * ));
	 * mudraMoneyTransactionBean.setIfsccode(mudraBeneficiaryBank.getIfscCode())
	 * ;
	 * 
	 * if (mudraMoneyTransactionBean.getBankName() == null ||
	 * mudraMoneyTransactionBean.getBankName().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1107");
	 * mudraMoneyTransactionBean.setStatusDesc("Bank Name can't be empty.");
	 * return mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getAccountNo() == null ||
	 * mudraMoneyTransactionBean.getAccountNo().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1108");
	 * mudraMoneyTransactionBean.setStatusDesc(
	 * "Beneficiary Account Number can't be empty."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * if (mudraMoneyTransactionBean.getIfsccode() == null ||
	 * mudraMoneyTransactionBean.getIfsccode().isEmpty()) {
	 * mudraMoneyTransactionBean.setStatusCode("1109");
	 * mudraMoneyTransactionBean.setStatusDesc("Invalid IFSC Code."); return
	 * mudraMoneyTransactionBean; }
	 * 
	 * transaction = session.beginTransaction(); String sendtrxId =
	 * commanUtilDao.getTrxId("FBTX");
	 * mudraMoneyTransactionBean.setTxnId(sendtrxId);
	 * mudraMoneyTransactionBean.setStatus("Initiate");
	 * session.save(mudraMoneyTransactionBean); transaction.commit();
	 * 
	 * String walletResp =
	 * transactionDao.walletToAccount(mudraMoneyTransactionBean.getUserId(),
	 * mudraMoneyTransactionBean.getWalletId(),
	 * mudraMoneyTransactionBean.getAmount(),
	 * mudraMoneyTransactionBean.getAccountNo(), sendtrxId,
	 * mudraMoneyTransactionBean.getIpiemi(),
	 * mudraMoneyTransactionBean.getAggreatorId(),
	 * mudraMoneyTransactionBean.getUseragent(),
	 * mudraMoneyTransactionBean.getSurCharge()); logger.info(serverName+
	 * "walletResp ===================== method tansferInAccount(walletToBankTxnMast)"
	 * + walletResp);
	 * 
	 * if (walletResp.equals("1000")) {
	 * 
	 * transaction = session.beginTransaction(); mudraMoneyTransactionBean =
	 * (MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class,
	 * sendtrxId); mudraMoneyTransactionBean = (MudraMoneyTransactionBean)
	 * session.get(MudraMoneyTransactionBean.class, sendtrxId);
	 * mudraMoneyTransactionBean.setStatus("Pending");
	 * mudraMoneyTransactionBean.setWalletTxnStatus("Accepted");
	 * mudraMoneyTransactionBean.setBanktxnstatus("Initiate");
	 * session.update(mudraMoneyTransactionBean); transaction.commit(); // call
	 * Bank API
	 * 
	 * mudraMoneyTransactionBean.setStatusCode("1000");
	 * mudraMoneyTransactionBean.setStatusDesc("Accepted"); } else {
	 * 
	 * transaction = session.beginTransaction(); mudraMoneyTransactionBean =
	 * (MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class,
	 * sendtrxId); mudraMoneyTransactionBean.setStatus("Rejected");
	 * mudraMoneyTransactionBean.setWalletTxnStatus("Rejected");
	 * session.update(mudraMoneyTransactionBean); transaction.commit();
	 * mudraMoneyTransactionBean.setStatusCode(walletResp);
	 * mudraMoneyTransactionBean.setStatusDesc("Rejected from Wallet"); }
	 * 
	 * } catch (Exception e) { logger.debug(serverName+
	 * "problem in MudraMoneyTransfer=========================" +
	 * e.getMessage(), e); mudraMoneyTransactionBean.setStatusCode("7000");
	 * mudraMoneyTransactionBean.setStatusDesc("Error"); e.printStackTrace();
	 * transaction.rollback(); } finally { session.close(); }
	 * 
	 * return mudraMoneyTransactionBean; }
	 */

	public MudraSenderBank getBeneficiaryListForImport(MudraSenderBank mudraSenderBank, String serverName,
			String requestId) {
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "",serverName + "*****RequestId***" + requestId
				+"getBeneficiaryListForImport");
		
		if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()) {
			mudraSenderBank.setStatusCode("1104");
			mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
			return mudraSenderBank;
		}
		if (mudraSenderBank.getMpin() == null || mudraSenderBank.getMpin().isEmpty()
				|| (mudraSenderBank.getMpin().length() < 4 || mudraSenderBank.getMpin().length() > 6)) {
			mudraSenderBank.setStatusCode("1102");
			mudraSenderBank.setStatusDesc("Invalid MPIN.");
			return mudraSenderBank;
		}

		if (mudraSenderBank.getExportMobile() == null || mudraSenderBank.getExportMobile().isEmpty()) {
			mudraSenderBank.setStatusCode("1104");
			mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
			return mudraSenderBank;
		}
		try {

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getExportMobile()))
					.add(Restrictions.eq("aggreatorId", mudraSenderBank.getAggreatorId()))
					.add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mudraSenderBank.getMpin()))).list();

			if (senderList.size() > 0) {
				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", senderList.get(0).getId())).list();
				if (benfList.size() > 0) {
					mudraSenderBank.setBeneficiaryList(benfList);
					mudraSenderBank.setStatusCode("1000");
					mudraSenderBank.setStatusDesc("Success");
				} else {
					mudraSenderBank.setStatusCode("1122");
					mudraSenderBank.setStatusDesc("Sender does not have any beneficiary.");
				}
			} else {
				mudraSenderBank.setStatusCode("1121");
				mudraSenderBank
						.setStatusDesc("This Mobile Number is not registered with us as Sender or Invalid MPIN.");
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "",serverName + "*****RequestId***" + requestId
					+ "problem in getBeneficiaryListForImport" + e.getMessage()+" "+e);
			
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderBank;
	}

	public MudraBeneficiaryBank getBeneficiary(String beneficiaryId, String serverName) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBanktest.getAggreatorId(),"","", "", "",serverName
				+ "|getBeneficiary()|"
				+ beneficiaryId);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraBeneficiaryBank bResult = new MudraBeneficiaryBank();
		try {
			// bResult=
			// (MudraBeneficiaryBank)session.get(MudraBeneficiaryBank.class,
			// beneficiaryId);
			bResult = (MudraBeneficiaryBank) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("id", beneficiaryId)).uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBanktest.getAggreatorId(),"","", "", "",serverName + "problem in getBeneficiary" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return bResult;
	}

	public MudraBeneficiaryBank copyBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****ReqestId****" + requestId
				+  "|copyBeneficiary()|"
				+ mudraBeneficiaryBank.getSenderId());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		mudraBeneficiaryBank.setStatusCode("1001");
		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryBank;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****ReqestId****" + requestId
					+ "Number of active Beneficiary  **:" + activeCount);
			
			if (activeCount >= 15) {
				mudraBeneficiaryBank.setStatusCode("1123");
				mudraBeneficiaryBank.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before importing a new beneficiary.");
				return mudraBeneficiaryBank;
			}

			MudraBeneficiaryBank mudraBeneficiaryMastCopy = (MudraBeneficiaryBank) session
					.get(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());

			if (mudraBeneficiaryMastCopy == null) {
				mudraBeneficiaryBank.setStatusCode("1116");
				mudraBeneficiaryBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraBeneficiaryBank;
			} else {

				List<MudraBeneficiaryBank> benList = session.createCriteria(MudraBeneficiaryBank.class)
					     .add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					     .add(Restrictions.eq("accountNo", mudraBeneficiaryBank.getAccountNo()))
					     .add(Restrictions.ne("status","Deleted")).list();
				
				
				if (benList.size() > 0) {
					mudraBeneficiaryBank.setStatusCode("1124");
					mudraBeneficiaryBank.setStatusDesc("Your provided beneficiary is already added with sender.");
					return mudraBeneficiaryBank;
				}

				transaction = session.beginTransaction();
				MudraBeneficiaryBank mudraBeneficiaryMastpast = (MudraBeneficiaryBank) mudraBeneficiaryMastCopy
						.cloneMe();

				mudraBeneficiaryMastpast.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryBank.getAggreatorId()));
				mudraBeneficiaryMastpast.setStatus("Active");
				mudraBeneficiaryMastpast.setSenderId(mudraBeneficiaryBank.getSenderId());

				session.save(mudraBeneficiaryMastpast);
				transaction.commit();
				mudraBeneficiaryBank.setName(mudraBeneficiaryMastpast.getName());
				mudraBeneficiaryBank.setAccountNo(mudraBeneficiaryMastpast.getAccountNo());
				mudraBeneficiaryBank.setTransferType(mudraBeneficiaryMastpast.getTransferType());
				mudraBeneficiaryBank.setIfscCode(mudraBeneficiaryMastpast.getIfscCode());
				mudraBeneficiaryBank.setStatus(mudraBeneficiaryMastpast.getStatus());
				mudraBeneficiaryBank.setVerified(mudraBeneficiaryMastpast.getVerified());
				mudraBeneficiaryBank.setStatusCode("1000");
				mudraBeneficiaryBank.setStatusDesc("Beneficiary Imported.");
				mudraBeneficiaryBank.setId(mudraBeneficiaryMastpast.getId());
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****ReqestId****" + requestId
					+ "problem in activeBeneficiary" + e.getMessage()+" "+e);
			
			
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}

	public MudraSenderBank activeBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
				+  "|activeBeneficiary()|"
				+ mudraBeneficiaryBank.getId());
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
				+ "*******Start excution ************************getSenderId************* activeBeneficiary method**:"
				);*/
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderBank mudraSenderBank = new MudraSenderBank();
		mudraSenderBank.setStatusCode("1001");
		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraSenderBank.setStatusCode("1110");
				mudraSenderBank.setStatusDesc("Invalid Sender Id.");
				return mudraSenderBank;
			}

			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraSenderBank.setStatusCode("1116");
				mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderBank;
			}

			int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
					+ "Number ofactive Beneficiary :" + activeCount);
			
			if (activeCount >= 15) {
				mudraSenderBank.setStatusCode("1123");
				mudraSenderBank.setStatusDesc(
						"You have already 15 active beneficiaries. Please deactivate anyone before activating a new beneficiary.");
				return mudraSenderBank;
			}

			MudraBeneficiaryBank mudraBeneficiaryMastUpdate = (MudraBeneficiaryBank) session
					.get(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());

			if (mudraBeneficiaryMastUpdate == null) {
				mudraSenderBank.setStatusCode("1116");
				mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderBank;
			} else {

				// added for defect id 8.2
				if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId())) {
					mudraSenderBank.setStatusCode("1130");
					mudraSenderBank.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraSenderBank;
				}

				transaction = session.beginTransaction();
				mudraBeneficiaryMastUpdate.setStatus("Active");
				session.update(mudraBeneficiaryMastUpdate);
				transaction.commit();
				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				mudraSenderBank.setBeneficiaryList(benfList);

				mudraSenderBank.setStatusCode("1000");
				mudraSenderBank.setStatusDesc("Beneficiary Activated.");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
					+ "problem in activeBeneficiary" + e.getMessage()+" "+e);
			
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraSenderBank;
	}

	public MudraSenderBank deActiveBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			String requestId) {
		
	      
		Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId******" + requestId
				+ "|deActiveBeneficiary()|"
				+ mudraBeneficiaryBank.getId());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		MudraSenderBank mudraSenderBank = new MudraSenderBank();
		mudraSenderBank.setStatusCode("1001");
		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraSenderBank.setStatusCode("1110");
				mudraSenderBank.setStatusDesc("Invalid Sender Id.");
				return mudraSenderBank;
			}
			if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
				mudraSenderBank.setStatusCode("1116");
				mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderBank;
			}

			MudraBeneficiaryBank mudraBeneficiaryMastUpdate = (MudraBeneficiaryBank) session
					.get(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());

			if (mudraBeneficiaryMastUpdate == null) {
				mudraSenderBank.setStatusCode("1116");
				mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
				return mudraSenderBank;
			} else {
				// added for defect id 7.1
				if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId())) {
					mudraSenderBank.setStatusCode("1130");
					mudraSenderBank.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return mudraSenderBank;
				}

				transaction = session.beginTransaction();
				if(mudraBeneficiaryMastUpdate.getVerifiedStatus()== null) {
					mudraBeneficiaryMastUpdate.setVerifiedStatus("NV");
				}
				mudraBeneficiaryMastUpdate.setStatus("Deactive");
				session.update(mudraBeneficiaryMastUpdate);
				transaction.commit();
				List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				mudraSenderBank.setBeneficiaryList(benfList);
				mudraSenderBank.setStatusCode("1000");
				mudraSenderBank.setStatusDesc("Beneficiary Deactivated.");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.ERROR, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId******" + requestId
					+ "problem in deActiveBeneficiary" + e.getMessage()+" "+e);
			
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;
	}

	public List<MerchantDmtTransBean> gettransDtl(String merchantId, String mobileNo, String serverName) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName +  "|gettransDtl()|"+ mobileNo);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<MerchantDmtTransBean> transList = new ArrayList<MerchantDmtTransBean>();
		try {
			// SQLQuery query = session.createSQLQuery("SELECT
			// merchantid,senderid,sendermobileno,beneficiaryname,bankname,accountno,transfertype,amount,transid,DATE_FORMAT(transdate,
			// '%d/%m/%Y') AS transdate,STATUS, merchanttransid FROM
			// merchant_dmt_trans WHERE merchantid=:merchantid AND
			// sendermobileno=:sendermobileno");
			SQLQuery query = session.createSQLQuery(
					"SELECT transid AS agentTransId,merchanttransid AS mrTransId,status ,amount,DATE_FORMAT(transdate, '%d/%m/%Y %T') AS transDateTime,accountno as benefAccNo,transfertype as remark,sendermobileno AS mobileNo FROM merchant_dmt_trans WHERE merchantid=:merchantid AND sendermobileno=:sendermobileno");
			// SELECT transid AS agentTransId,merchanttransid AS
			// mrTransId,status ,amount,transdate as transDateTime,accountno as
			// benefAccNo,transfertype as remark FROM merchant_dmt_trans WHERE
			// merchantid='AGGR001035' AND sendermobileno='9935041287'
			query.setString("merchantid", merchantId);
			query.setString("sendermobileno", mobileNo);
			query.setResultTransformer(Transformers.aliasToBean(MerchantDmtTransBean.class));
			query.addScalar("agentTransId").addScalar("mrTransId").addScalar("status").addScalar("amount")
					.addScalar("transDateTime").addScalar("benefAccNo").addScalar("remark").addScalar("mobileNo");
			transList = query.list();

		} catch (Exception e) {
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "|gettransDtl()|"+ "problem in gettransDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return transList;
	}

	public List<MerchantDmtTransBean> getTransStatus(String merchantId, String agentTransId, String serverName) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName +  "|getTransStatus()|"
				+ agentTransId);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<MerchantDmtTransBean> transList = new ArrayList<MerchantDmtTransBean>();
		try {
			// SQLQuery query = session.createSQLQuery("SELECT
			// merchantid,senderid,sendermobileno,beneficiaryname,bankname,accountno,transfertype,amount,transid,DATE_FORMAT(transdate,
			// '%d/%m/%Y') AS transdate,STATUS, merchanttransid FROM
			// merchant_dmt_trans WHERE merchantid=:merchantid AND
			// sendermobileno=:sendermobileno");
			SQLQuery query = session.createSQLQuery(
					"SELECT transid AS agentTransId,merchanttransid AS mrTransId,status ,amount,DATE_FORMAT(transdate, '%d/%m/%Y %T') AS transDateTime,accountno as benefAccNo,transfertype as remark,sendermobileno AS mobileNo FROM merchant_dmt_trans WHERE merchantid=:merchantid AND merchanttransid=:merchanttransid");
			// SELECT transid AS agentTransId,merchanttransid AS
			// mrTransId,status ,amount,transdate as transDateTime,accountno as
			// benefAccNo,transfertype as remark FROM merchant_dmt_trans WHERE
			// merchantid='AGGR001035' AND sendermobileno='9935041287'
			query.setString("merchantid", merchantId);
			query.setString("merchanttransid", agentTransId);
			query.setResultTransformer(Transformers.aliasToBean(MerchantDmtTransBean.class));
			query.addScalar("agentTransId");
			query.addScalar("agentTransId").addScalar("mrTransId").addScalar("status").addScalar("amount")
					.addScalar("transDateTime").addScalar("benefAccNo").addScalar("remark").addScalar("mobileNo");
			transList = query.list();

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "problem in getagentTranssDtl" + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return transList;
	}

	public double getUserBalance(String merchantId, String senderId, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "",serverName + "|getUserBalance()|"
				+ senderId);
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Double userBalance = 0.00;
		try {
			// added on 2nd Aug
			Object balance = session
					.createSQLQuery("SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  id=:senderId")
					.setString("senderId", senderId).uniqueResult();
			if (balance == null) {
				userBalance = 0.00;
			} else {
				userBalance = ((BigDecimal) balance).doubleValue();
				if (userBalance == null) {
					userBalance = 0.00;
				}
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "",serverName +  "|"+"problem in getUserBalance" + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return userBalance;
	}

	/*
	 * public double getUserBalance(String merchantId,String senderId, String
	 * serverName) { logger.info(serverName+
	 * "Start excution ************************************* getUserBalance method**:"
	 * + senderId); factory = DBUtil.getSessionFactory(); session =
	 * factory.openSession(); double userBalance=0.00; try{ // userBalance =
	 * (Double)session.createSQLQuery(
	 * "SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  aggreatorid=:merchantId AND id=:senderId"
	 * ).setString("merchantId", merchantId).setString("senderId",
	 * senderId).uniqueResult(); userBalance =
	 * ((BigDecimal)session.createSQLQuery(
	 * "SELECT IFNULL(walletbalance,0) FROM mudrasendermast WHERE  aggreatorid=:merchantId AND id=:senderId"
	 * ).setString("merchantId", merchantId).setString("senderId",
	 * senderId).uniqueResult()).doubleValue(); } catch (Exception e) {
	 * logger.debug(serverName+
	 * "problem in getagentTranssDtl=========================" + e.getMessage(),
	 * e); e.printStackTrace(); transaction.rollback(); } finally {
	 * session.close(); } return userBalance; }
	 */

	/*
	 * public Double getmaxAmtPerDay(String merchantId,String senderId, String
	 * serverName){
	 * 
	 * Double perDayAmount=0.0; factory = DBUtil.getSessionFactory(); session =
	 * factory.openSession(); double userBalance=0.00; try{
	 * perDayAmount=(Double)session.createQuery(
	 * "SELECT COALESCE(SUM(E.trxCredit),0) FROM TransactionBean E WHERE  WALLET_ID=:walletId AND TRX_RESULT='Success'  AND DATE(WALLET_TXN_DATE)=DATE(NOW())"
	 * ).setString("walletId",walletId).uniqueResult(); logger.info(
	 * " pperDayAmount===================================================================================== trxId"
	 * +perDayAmount); }catch(Exception e){ e.printStackTrace(); logger.debug(
	 * "problem in getmaxAmtPerDay========================================" +
	 * e.getMessage(), e); } finally { session.close(); }
	 * 
	 * return perDayAmount; }
	 */

	// SELECT walletbalance FROM mudrasendermast WHERE aggreatorid='AGGR001035'
	// AND id='MSEN001011'

	public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ "|getTransacationLedgerDtl()|"
				+ dMTReportInOut.getAgentId()+ " |getTransacationLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ " |getTransacationLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if (dMTReportInOut.getUsertype()==2.0) {
			serchQuery.append(
					"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid=:agentid  and isbank in (1,2,3,4)");
				
			}else
				serchQuery.append(
						"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where distributerid=:agentid)  and isbank in (1,2,3,4)");
			
			if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
				serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
			}
			
			serchQuery.append(" AND narrartion not like 'WALLET LOADING%'");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			else {
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) = STR_TO_DATE(REPLACE('"
						+ formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			//query.setString("isbank","1");
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "totla Count :" + transList.size());
			
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "problem in getTransacationLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}
	
	

	public DMTReportInOut getTransacationLedgerDtlBk(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ "|getTransacationLedgerDtl()|"
				+ dMTReportInOut.getAgentId()+ " |getTransacationLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ " |getTransacationLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if (dMTReportInOut.getUsertype()==2.0) {
			serchQuery.append(
					"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM payoutledger WHERE agentid=:agentid  and isbank in (1,2,3)");
				
			}else
				serchQuery.append(
						"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM payoutledger WHERE agentid in (select id from walletmast where distributerid=:agentid)  and isbank in (1,2,3)");
			
			if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
				serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
			}
			
			serchQuery.append(" AND narrartion not like 'WALLET LOADING%'");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			else {
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) = STR_TO_DATE(REPLACE('"
						+ formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			//query.setString("isbank","1");
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "totla Count :" + transList.size());
			
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "problem in getTransacationLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}
	
	
	
	public DMTReportInOut getTransacationLedgerDtlForAgg(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ "|getTransacationLedgerDtlForAgg()|"
				+ dMTReportInOut.getAgentId()+ " |getTransacationLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ " |getTransacationLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if (dMTReportInOut.getUsertype()==2.0) {
			serchQuery.append(
					"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank FROM transactionledger WHERE agentid=:agentid  and isbank in (1,2,3)");
			}else if(dMTReportInOut.getUsertype()!=2.0 && (dMTReportInOut.getUsertype()==4.0 || dMTReportInOut.getUsertype()==6.0) && "OAGG001050".equalsIgnoreCase(dMTReportInOut.getAggregatorId())) {
				serchQuery.append(
						"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where id like 'MERCSPAY%')  and isbank in (3)");
						if(dMTReportInOut.getAgentId()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getAgentId())) {
							serchQuery.append(" and agentid='"+dMTReportInOut.getAgentId()+"'");
						}
						if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
							serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
						}
			}else {
				serchQuery.append(
						"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where id like 'MERCSPAY%' and aggreatorid='"+dMTReportInOut.getAggregatorId()+"')  and isbank in (3)");
			
				if(dMTReportInOut.getAgentId()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getAgentId())) {
					serchQuery.append(" and agentid='"+dMTReportInOut.getAgentId()+"'");
				}
				if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
					serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
				}
			}
				serchQuery.append(" AND narrartion not like 'WALLET LOADING%'");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			else {
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) = STR_TO_DATE(REPLACE('"
						+ formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			//query.setString("agentid", dMTReportInOut.getAgentId());
			//query.setString("isbank","1");
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "totla Count :" + transList.size());
			
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "problem in getTransacationLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}
	
	
	
	public DMTReportInOut getTransacationLedgerPayoutForAgg(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ "|getTransacationLedgerPayoutForAgg()|"
				+ dMTReportInOut.getAgentId()+ " |getTransacationLedgerPayoutForAgg stDAte**:"
						+ dMTReportInOut.getStDate()+ " |getTransacationLedgerPayoutForAgg edDate**:"
								+ dMTReportInOut.getEdDate());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if (dMTReportInOut.getUsertype()==2.0) {
			serchQuery.append(
					"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank FROM payoutledger WHERE agentid=:agentid  and isbank in (1,2,3)");
			}else if(dMTReportInOut.getUsertype()!=2.0 && (dMTReportInOut.getUsertype()==4.0 || dMTReportInOut.getUsertype()==6.0) && "OAGG001050".equalsIgnoreCase(dMTReportInOut.getAggregatorId())) {
				serchQuery.append(
						"SELECT acq,id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid,charges FROM payoutledger WHERE agentid in (select id from walletmast where id like 'MERCSPAY%')  and isbank in (3)");
						if(dMTReportInOut.getAgentId()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getAgentId())) {
							serchQuery.append(" and agentid='"+dMTReportInOut.getAgentId()+"'");
						}
						if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
							serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
						}
			}else {
				serchQuery.append(
						"SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid,charges FROM payoutledger WHERE agentid in (select id from walletmast where id like 'MERCSPAY%' and aggreatorid='"+dMTReportInOut.getAggregatorId()+"')  and isbank in (3)");
			
				if(dMTReportInOut.getAgentId()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getAgentId())) {
					serchQuery.append(" and agentid='"+dMTReportInOut.getAgentId()+"'");
				}
				if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
					serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
				}
			}
				serchQuery.append(" AND narrartion not like 'WALLET LOADING%'");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			else {
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) = STR_TO_DATE(REPLACE('"
						+ formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			//query.setString("agentid", dMTReportInOut.getAgentId());
			//query.setString("isbank","1");
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "totla Count :" + transList.size());
			
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "problem in getTransacationLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}
	
	
	public DMTReportInOut getAgentLedgerDtl(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "",serverName + "****RequestId****" + requestId
				+ "|getAgentLedgerDtl()|"
				+ dMTReportInOut.getAgentId()+ " getAgentLedgerDtl stDAte**:"
						+ dMTReportInOut.getStDate()+ " getAgentLedgerDtl edDate**:"
								+ dMTReportInOut.getEdDate());
		
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ " getAgentLedgerDtl stDAte**:"
				+ dMTReportInOut.getStDate());
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
				+ " getAgentLedgerDtl edDate**:"
				+ dMTReportInOut.getEdDate());*/
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<AgentLedgerBean> agentLedgerList = new ArrayList<AgentLedgerBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(txnDate, '%d/%m/%Y %T') AS txnDate,userid,txntype,resptxnid,mobileno,accountno,NAME,ifsccode,dramount,txncredit,closingbal FROM agentledger WHERE userid=:agentid ");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(txnDate) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(txnDate, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "Query :" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			query.setResultTransformer(Transformers.aliasToBean(AgentLedgerBean.class));
			agentLedgerList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "totla Count:" + agentLedgerList.size());
			
			dMTReportInOut.setAgentLedger(agentLedgerList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestId
					+ "problem in getAgentLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}

	public DMTReportInOut getSenderLedgerDtl(DMTReportInOut dMTReportInOut, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
				+ "|getSenderLedgerDtl()|"
				+ dMTReportInOut.getAgentId()+ "|getSenderLedgerDtl()|"
						+ dMTReportInOut.getMobileNo());
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "",serverName + "***RequestId***" + requestId
				+ "|getSenderLedgerDtl()|"
				+ dMTReportInOut.getMobileNo());*/
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<SenderLedgerBean> senderLedgerList = new ArrayList<SenderLedgerBean>();
		try {

			
			 if(dMTReportInOut.getMobileNo()!=null&& !dMTReportInOut.getMobileNo().isEmpty() && StringUtils.isNumeric(dMTReportInOut.getMobileNo()) && dMTReportInOut.getMobileNo().length()==10) {
				 StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt,a.senderid AS senderid,a.id AS id,narrartion,c.mobileno AS mobileno,b.accountno AS accountno,b.ifsccode AS ifsccode,cramount,dramount,IFNULL(bankrrn,'') AS bankrrn,remark,a.isbank as isbank,a.status as status FROM remittanceledger a,mudrabeneficiarymast b,mudrasendermast c  WHERE a.senderid =(SELECT id FROM mudrasendermast WHERE mobileno=:mobileno)  AND a.senderid=c.id AND a.beneficiaryid=b.id ");
			serchQuery.append(" and narrartion not like 'WALLET LOADIN%'");
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
					+ "Query:" + serchQuery.toString());
		
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			//query.setString("agentid", dMTReportInOut.getAgentId());
			query.setString("mobileno", dMTReportInOut.getMobileNo());
			query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
			senderLedgerList = query.list();
			 }
			
			 else if(dMTReportInOut.getAccountno()!=null && !dMTReportInOut.getAccountno().isEmpty()) {
				 StringBuilder serchQuery = new StringBuilder();
				    serchQuery.append(
				      "SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt, a.senderid AS senderid,a.id AS id,a.narrartion AS narrartion,c.mobileno AS mobileno, b.accountno AS accountno,b.ifsccode AS ifsccode,a.cramount AS cramount,a.dramount AS dramount,IFNULL(bankrrn,'') AS bankrrn,a.bankrrn AS bankrrn,a.remark AS remark,a.isbank as isbank,a.status as status FROM remittanceledger a, mudrabeneficiarymast b,mudrasendermast c where b.accountno=:accountno AND a.senderid=c.id AND a.beneficiaryid=b.id");
				    serchQuery.append(" and narrartion not like 'WALLET LOADIN%'");
				    serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
				    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						      + "Query :" + serchQuery.toString());
				   
				    SQLQuery query = session.createSQLQuery(serchQuery.toString());
				   // query.setString("agentid", dMTReportInOut.getAgentId());
				    query.setString("accountno", dMTReportInOut.getAccountno());
				    query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
				    senderLedgerList = query.list();
			}
			 else if(dMTReportInOut.getMobileNo()!=null&& !dMTReportInOut.getMobileNo().isEmpty() && !StringUtils.isNumeric(dMTReportInOut.getMobileNo())) {
				 StringBuilder serchQuery = new StringBuilder();
				    serchQuery.append(
				      "SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt, a.senderid AS senderid,a.id AS id,a.narrartion AS narrartion,c.mobileno AS mobileno, b.accountno AS accountno,b.ifsccode AS ifsccode,a.cramount AS cramount,a.dramount AS dramount,IFNULL(bankrrn,'') AS bankrrn,a.bankrrn AS bankrrn,a.remark AS remark,a.isbank as isbank,a.status as status FROM remittanceledger a, mudrabeneficiarymast b,mudrasendermast c where a.txnid=:txnid AND a.senderid=c.id AND a.beneficiaryid=b.id");
				    serchQuery.append(" and narrartion not like 'WALLET LOADIN%'");
				    serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
				    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						      + "Query :" + serchQuery.toString());
				   
				    SQLQuery query = session.createSQLQuery(serchQuery.toString());
				   // query.setString("agentid", dMTReportInOut.getAgentId());
				    query.setString("txnid", dMTReportInOut.getMobileNo());
				    query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
				    senderLedgerList = query.list();
			}
			 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						+ "totla Count:" + senderLedgerList.size());
		
			dMTReportInOut.setSenderLedgerList(senderLedgerList);

		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						+ "problem in getsenderLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}

	
	public SenderLedgerBean gettxndetails(String txnId,String aggreatorid, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
				+ "|getSenderLedgerDtl()|"
				+ txnId+ "|getSenderLedgerDtl()|"
						+ aggreatorid);
	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		SenderLedgerBean senderLedgerList = new SenderLedgerBean();
		try {

				 StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS ptytransdt,a.senderid AS senderid,a.id AS id,narrartion,cramount,dramount,IFNULL(bankrrn,'') AS bankrrn,remark,a.isbank AS isbank,a.status AS STATUS FROM remittanceledger a WHERE a.merchanttransid=:txnid AND a.isbank in (3) AND a.agentid=:aggreatorid  AND a.narrartion in ('IMPS','NEFT')");
		
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			//query.setString("agentid", dMTReportInOut.getAgentId());
			query.setString("txnid", txnId);
			query.setString("aggreatorid", aggreatorid);
			query.setResultTransformer(Transformers.aliasToBean(SenderLedgerBean.class));
			senderLedgerList = (SenderLedgerBean)query.uniqueResult();
	
			 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId);
		


		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						+ "problem in getsenderLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			//transaction.rollback();
		} finally {
			session.close();
		}
		return senderLedgerList;
	}


	public int gettxndetailsCount(String txnId,String aggreatorid, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
				+ "|getSenderLedgerDtl()|"
				+ txnId+ "|getSenderLedgerDtl()|"
						+ aggreatorid);
	
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		int count =0;
		try {

				 StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT count(*) FROM remittanceledger a WHERE a.merchanttransid=:txnid AND a.isbank in (1,2,3) AND a.agentid=:aggreatorid");
		
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			//query.setString("agentid", dMTReportInOut.getAgentId());
			query.setString("txnid", txnId);
			query.setString("aggreatorid", aggreatorid);
			 count = ((BigInteger)query.uniqueResult()).intValue();
	
			 
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId);
		


		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "***RequestId***" + requestId
						+ "problem in gettxndetailsCount" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return count;
	}

	public List<BankMastBean> getBankDtls(String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "|getBankDtls()");
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			List<BankMastBean> bankDtl = session.createCriteria(BankMastBean.class).list();
			return bankDtl;
		} finally {
			session.close();
		}
	}

	public List<BankNameListBean> getBankDtl(String serverName, String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId**" + requestId
				+ "getBankDtl");
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<BankNameListBean> bankDtl = new ArrayList<BankNameListBean>();
		// bankDtl.add(0, new BankNameListBean("-1","Select Bank"));
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT a.bankname AS id, a.bankname AS bankName FROM ifscmaster a GROUP BY bankname");
			query.setResultTransformer(Transformers.aliasToBean(BankNameListBean.class));
			bankDtl = query.addScalar("id").addScalar("bankName").list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId**" + requestId
					+ "totla Count :" + bankDtl.size());
			
			return bankDtl;
		} finally {
			session.close();
		}
	}

	public String saveDMTApiRequest(String agentId, String reqVal, String serverName, String apiName) {
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName +  "|saveDMTApiRequest()|"
				+ agentId+"|+ reqVal"+ reqVal);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","", "",serverName +  "|saveDMTApiRequest()|"
				+ reqVal);*/
		

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String requestId = "";
		try {
			transaction = session.beginTransaction();
			DMTApiRequest dMTApiRequest = new DMTApiRequest();
			
			int leftLimit = 65; // letter 'a'
		    int rightLimit = 90; // letter 'z'
		    int targetStringLength = 10;
		    Random random = new Random();
		    StringBuilder buffer = new StringBuilder(targetStringLength);
		    for (int i = 0; i < targetStringLength; i++) {
		        int randomLimitedInt = leftLimit + (int) 
		          (random.nextFloat() * (rightLimit - leftLimit + 1));
		        buffer.append((char) randomLimitedInt);
		    }
		    String generatedString = buffer.toString();
			dMTApiRequest.setRequestId(Long.toString(generateLongId()) + generatedString);
			dMTApiRequest.setAgentId(agentId);
			dMTApiRequest.setRequestValue(reqVal);
			dMTApiRequest.setApiName(apiName);
			dMTApiRequest.setRequestdate(new java.sql.Date(new java.util.Date().getTime()));
			session.save(dMTApiRequest);
			transaction.commit();
			requestId = dMTApiRequest.getRequestId();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName +  "|saveDMTApiRequest()|"
					+ requestId);
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName +"|saveDMTApiRequest()|"+ "problem in getsenderLedgerDtl" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return requestId;
	}

	public String bankApi() {
		Random r = new Random();
		int i = r.nextInt((10 - 1) + 1) + 1;

		if (i / 2 > 0) {
			return Integer.toString(i);
		} else {
			return "Fail";
		}
	}

	private static final long twepoch = 1288834974657L;
	private static final long sequenceBits = 17;
	private static final long sequenceMax = 65536;
	private static volatile long lastTimestamp = -1L;
	private static volatile long sequence = 0L;

	private static synchronized Long generateLongId() {
		long timestamp = System.currentTimeMillis();
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) % sequenceMax;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimestamp);
			}
		} else {
			sequence = 0;
		}
		lastTimestamp = timestamp;
		Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
		return id;
	}

	private static long tilNextMillis(long lastTimestamp) {
		long timestamp = System.currentTimeMillis();
		while (timestamp <= lastTimestamp) {
			timestamp = System.currentTimeMillis();
		}
		return timestamp;
	}

	// added for defect id 6.1
	public String getSenderMobileNo(String senderId) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderId, "", "|getSenderMobileNo()");
		
		

		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String mobileNo = new String();
		try {
			mobileNo = (String) session.createSQLQuery("SELECT mobileno from mudrasendermast  where id = :senderId")
					.setString("senderId", senderId).uniqueResult();

			return mobileNo;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", senderId, "", "|"+"problem in getsenderLedgerDtl" + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return mobileNo;
	}

	// changed on 27th july
	/**
	 * 
	 * @param bankName
	 * @param ifsc
	 * @param branchName
	 * @param serverName
	 * @return
	 */
	public List<BankDetailsBean> getBankDetails(String bankName, String ifsc, String branchName, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName
				+ "|getBankDetails()|param--- bankName :"
				+ bankName + "--IFSC--" + ifsc + "---branchName---" + branchName);
		
		
	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<BankDetailsBean> bankDtl = new ArrayList<BankDetailsBean>();
		try {
			// bankDtl.add(0, new BankNameListBean("-1","Select Bank"));
			String querystmt = "SELECT a.bankname AS bankName, a.ifsccode AS ifscCode , a.branchname as branchName ,a.address as address , a.city as city , a.state as state FROM ifscmaster a where ";

			if (!(ifsc == null || ifsc.trim().equals(""))) {
				querystmt = querystmt + " a.ifsccode = '" + ifsc + "'";
			} else if (!(bankName == null || bankName.trim().equals(""))
					&& (branchName == null || branchName.trim().equals(""))) {
				querystmt = querystmt + " a.bankname = '" + bankName + "'";
			} else if ((bankName == null || bankName.trim().equals(""))
					&& !(branchName == null || branchName.trim().equals(""))) {
				querystmt = querystmt + " a.branchname = '" + branchName + "'";
			} else {
				querystmt = querystmt + " a.bankname = '" + bankName + "'";
				querystmt = querystmt + " and a.branchname = '" + branchName + "'";
			}

			querystmt = querystmt + "  order by bankname ";
			SQLQuery query = session.createSQLQuery(querystmt);
			query.setResultTransformer(Transformers.aliasToBean(BankDetailsBean.class));
			bankDtl = query.addScalar("bankName").addScalar("ifscCode").addScalar("branchName").addScalar("address")
					.addScalar("city").addScalar("state").list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "totla Count:" + bankDtl.size());
			
			return bankDtl;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "exception in getBankDetails-" + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bankDtl;
	}

	public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails, String serverName) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "", serverName +"|uploadSenderPanF60()|"
				+ mudraSenderPanDetails.getSenderId());
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		try {
			transaction = session.beginTransaction();
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraSenderPanDetails.getSenderId());

			mudraSenderPanDetails.setSenderName(mudraSenderBank.getFirstName());
			mudraSenderPanDetails.setStatus("Pending");
			session.saveOrUpdate(mudraSenderPanDetails);

			transaction.commit();
			mudraSenderPanDetails.setStatusCode("1000");
			mudraSenderPanDetails.setStatusMessage("Pan /Form 60 uploaded successfully.");

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "", serverName + "exception in uploadSenderPanF60" + e.getMessage()+" "+e);
			
			e.printStackTrace();
		} finally {
			session.close();
		}

		return mudraSenderPanDetails;
	}

	public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails,
			String serverName) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + "|getPendingPanReq()|"
				+ mudraSenderPanDetails.getAggreatorId());
		
		
		
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<MudraSenderPanDetails> results = new ArrayList<MudraSenderPanDetails>();
		try {
			if (!(mudraSenderPanDetails.getAggreatorId() == null || mudraSenderPanDetails.getAggreatorId().isEmpty()
					|| mudraSenderPanDetails.getAggreatorId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				StringBuilder serchQuery = new StringBuilder();

				serchQuery.append(" from MudraSenderPanDetails where AggreatorId=:AggreatorId and status='Pending'");

				if (mudraSenderPanDetails.getStDate() != null && !mudraSenderPanDetails.getStDate().isEmpty()
						&& mudraSenderPanDetails.getEndDate() != null
						&& !mudraSenderPanDetails.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(loaddate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(mudraSenderPanDetails.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(mudraSenderPanDetails.getEndDate()))
							+ "', '/', '-'),'%d-%m-%Y')");
				} else {
					serchQuery.append(" and DATE(loaddate)=DATE(NOW())");
				}
				serchQuery.append(" order by loaddate DESC");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + "getPendingPanReq query" + serchQuery);
				

				Query query = session.createQuery(serchQuery.toString());
				query.setString("AggreatorId", mudraSenderPanDetails.getAggreatorId());
				results = query.list();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + "problem in getPendingPanReq" + e.getMessage()+" "+e);
			
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + " getPendingPanReq Result "
				+ results.size());
		
		
		return results;

	}

	public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails,
			String serverName) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + "|rejectAcceptedSenderPan()" );
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag = false;
		try {
			
			Transaction transaction = session.beginTransaction();
			MudraSenderPanDetails mudraSenderPanDetailsD = (MudraSenderPanDetails) session
					.get(MudraSenderPanDetails.class, mudraSenderPanDetails.getId());

			mudraSenderPanDetailsD.setStatus(mudraSenderPanDetails.getStatus());
			mudraSenderPanDetailsD.setComment(mudraSenderPanDetails.getComment());
			session.saveOrUpdate(mudraSenderPanDetailsD);
			if (mudraSenderPanDetails.getStatus().equalsIgnoreCase("ACCEPT")) {
				MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
						mudraSenderPanDetailsD.getSenderId());
				mudraSenderBank.setFirstName(mudraSenderPanDetails.getSenderName());
				mudraSenderBank.setIsPanValidate(1);
				session.saveOrUpdate(mudraSenderBank);
			}
			transaction.commit();
			mudraSenderPanDetails.setStatusCode("1000");
			mudraSenderPanDetails.setStatusMessage("Sender Pan/Form 60 " + mudraSenderPanDetails.getStatus() + ".");

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			mudraSenderPanDetails.setStatusCode("7000");
			mudraSenderPanDetails.setStatusMessage(e.getMessage());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderPanDetails.getAggreatorId(),"","", mudraSenderPanDetails.getSenderId(), "",serverName + "problem in rejecttSmartCard" + e.getMessage()+" "+e);
			
		} finally {
			session.close();
		}
		return mudraSenderPanDetails;
	}

	public void saveSenderKycResponse(EkycResponseBean ekycResponseBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"|saveSenderKycResponse()|"
				+ ekycResponseBean.getKyctxnid());
		
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction = session.beginTransaction();
		//String threadName = event.getThreadName();
		
		try {

			session.saveOrUpdate(ekycResponseBean);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public EkycResponseBean getEKycSenderData(EkycResponseBean ekycResponseBean) {
		EkycResponseBean bean = null;
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(),"|getEKycSenderData()|"
				+ ekycResponseBean.getKyctxnid());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {

			bean = (EkycResponseBean) session.get(EkycResponseBean.class, ekycResponseBean.getKyctxnid());

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),ekycResponseBean.getAggreator(),"", ekycResponseBean.getUserID(), ekycResponseBean.getSenderid(), ekycResponseBean.getTxnID(), "problem in getEKycSenderData" + e.getMessage()+" "+e);
			// transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bean;
	}

	public boolean setSenderKycMast(KycConfigBean kycconfigbean, SenderKYCBean senderKYCBean, String servicetype,
		String timestamp, String kyctxnid) {
		EkycResponseBean senderkycmast = new EkycResponseBean();
		Transaction transaction = null;
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"","", "", "",  "|setSenderKycMast()");
		session=factory.openSession();
		try {
			senderkycmast.setAgentid(senderKYCBean.getAgentid());

			senderkycmast.setAggreator(senderKYCBean.getAggreatorid());
			senderkycmast.setDeviceid(kycconfigbean.getDeviceid());
			senderkycmast.setSenderid(senderKYCBean.getMobileno());

			senderkycmast.setServicetype(servicetype);
			senderkycmast.setStatus("Pending");
			senderkycmast.setTime(timestamp);
			senderkycmast.setOthers("");
			senderkycmast.setKyctxnid(kyctxnid);

			transaction = session.beginTransaction();
			session.save(senderkycmast);
			transaction.commit();
			return true;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"","", "", "", "|setSenderKycMast()|"+"problem in save KYC request" + e.getMessage()+" "+e);
			//logger.debug("problem in save KYC request" + e.getMessage(), e);
			senderKYCBean.setStatusCode("7000");
			senderKYCBean.setStatusDesc("Error");

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return false;
	}

	public KycConfigBean getkycConfig(SenderKYCBean senderKYCBean, String servicetype) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"","", "", "", servicetype + "*******RequestId******" + senderKYCBean.getAgent()
		+ "|getkycConfig()|"
		+ senderKYCBean.getMobileno());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"","", "", "", "open session factory");
		
		KycConfigBean kycconfigbean = new KycConfigBean();
		if (senderKYCBean.getMobileno() == null || senderKYCBean.getMobileno().isEmpty()) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),senderKYCBean.getAggreatorid(),"","", "", "", servicetype + "Sender id is missing");
			
			kycconfigbean.setStatusCode("1999");
			kycconfigbean.setStatusDesc("Sender id is missing");
			return kycconfigbean;
		}
		List<KycConfigBean> kycconfigbeanlist=null;
		try{
		
		 kycconfigbeanlist = session.createCriteria(KycConfigBean.class)
				.add(Restrictions.eq("servicetype", servicetype))
				.add(Restrictions.eq("aggreator", senderKYCBean.getAggreatorid())).list();

		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		if (kycconfigbeanlist==null||kycconfigbeanlist.size() == 0) {

			kycconfigbean.setStatusCode("1999");
			kycconfigbean.setStatusDesc("Sender id is missing");
			return kycconfigbean;
		} else {
			kycconfigbean = kycconfigbeanlist.get(0);
			kycconfigbean.setStatusCode("1000");
			kycconfigbean.setStatusDesc("Success");
			return kycconfigbean;
		}
		

		
	}

	@Override
	public DMTReportInOut initiateDMTRefundByAgent(DMTReportInOut reportInOut,String serverName,String requestId){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","",reportInOut.getTxnid(),reportInOut.getSenderid(), serverName + "******RequestId*******" + requestId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=session.beginTransaction();
		DMTReportInOut dmtReportInOut=new DMTReportInOut();
		dmtReportInOut.setStatuscode("1001");
		dmtReportInOut.setStatusmsg("Something went wrong please try again later.");
		try{
		List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
				.add(Restrictions.eq("id", reportInOut.getSenderid()))
				.add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(reportInOut.getMpin()))).list();
		
		 MudraMoneyTransactionBean mtBean=(MudraMoneyTransactionBean)session.get(MudraMoneyTransactionBean.class,reportInOut.getTxnid());
		 String status = null;
		 logger.info("Logged in user is "+reportInOut.getAgentId()+" taking refund of transaction id "+reportInOut.getTxnid());
		if(mtBean.getNarration().equalsIgnoreCase("manually")  || reportInOut.getAgentId().trim().equalsIgnoreCase("ADMA001043"))
		{
			status = "FAILED";
		}
		else
		{
			status = getTxnStatusFino(reportInOut.getTxnid(),mtBean.getNarrartion());
		}
		 //String status="FAILED";//getTxnStatusFino(reportInOut.getTxnid(),mtBean.getNarrartion());
		 
		 if(senderList!=null&&senderList.size()>0&&status!=null&&status.equalsIgnoreCase("FAILED")){
			SQLQuery query=session.createSQLQuery("call refundtransaction(:txnid,:agentid,:senderid)");
			query.setParameter("txnid", reportInOut.getTxnid());
			query.setParameter("agentid", reportInOut.getAgentId());
			query.setParameter("senderid", reportInOut.getSenderid());
			query.executeUpdate();
			transaction.commit();
			dmtReportInOut=getSenderLedgerDtl(reportInOut, serverName, requestId);
			
			dmtReportInOut.setStatuscode("1000");
			dmtReportInOut.setStatusmsg("Transaction refunded successfuly.");
			
		}else{
		dmtReportInOut.setStatuscode("1002");
		dmtReportInOut.setStatusmsg("Invalid sender MPIN.");
		}
		
		}catch(Exception e){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","",reportInOut.getTxnid(),reportInOut.getSenderid(), serverName + "******RequestId*******" + requestId+"|"+e);
			transaction.rollback();
			dmtReportInOut.setStatuscode("7000");
			dmtReportInOut.setStatusmsg("Something went wrong please try again later.");
		}finally {
			session.close();
		}
		return dmtReportInOut;
	}


	public List<RefundTransactionBean> initiateDMTRefund(DmtDetailsMastBean dmtMast) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(), "|initiateDMTRefund()|"
				+ dmtMast.getTxnid());
		
		
				
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		
		List<RefundTransactionBean> list = new ArrayList<RefundTransactionBean>();
		try {
			
			MudraMoneyTransactionBean mTxnBean=(MudraMoneyTransactionBean)session.get(MudraMoneyTransactionBean.class,dmtMast.getTxnid());
			if(mTxnBean != null )
			{
				String status = null;
				logger.info("Logged in user is "+mTxnBean.getAgentid()+" taking refund of transaction id "+dmtMast.getTxnid());

				if(mTxnBean.getNarration().equalsIgnoreCase("manually") || mTxnBean.getAgentid().trim().equalsIgnoreCase("ADMA001043"))
				{
					status = "FAILED";
				}
				else
				{
					status = getTxnStatusFino(dmtMast.getTxnid(),mTxnBean.getNarrartion());
				}
				//String status="FAILED";//getTxnStatusFino(dmtMast.getTxnid(),mtBean.getNarrartion());
				
				if(mTxnBean!=null&&status!=null&&status.equalsIgnoreCase("FAILED")){
				Transaction t = session.beginTransaction();
				Query query = session.createSQLQuery("call refundtransaction(:txnid,:agentid,:senderid)");
				query.setParameter("agentid",mTxnBean.getAgentid());
				query.setParameter("senderid",mTxnBean.getSenderId());
				query.setParameter("txnid", dmtMast.getTxnid());
				query.executeUpdate();
				t.commit();
				}
	
				if (dmtMast != null && dmtMast.getTxnid().split(",").length == 1) {
					RefundTransactionBean mudra = (RefundTransactionBean) session.get(RefundTransactionBean.class,
							dmtMast.getTxnid());
					if (mudra != null)
						list.add(mudra);
				} else if (dmtMast != null && dmtMast.getTxnid().split(",").length > 1) {
					String stArr[] = dmtMast.getTxnid().split(",");
					for (String st : stArr) {
						RefundTransactionBean mudra = (RefundTransactionBean) session.get(RefundTransactionBean.class, st);
						if (mudra != null)
							list.add(mudra);
					}
				}
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(),"problem in initiateDMTRefund" + e.getMessage()+" "+e);
			
			// transaction.rollback();
			e.printStackTrace();
			RefundTransactionBean rb = new RefundTransactionBean();
			rb.setStatus("Ambiguous transaction status.");
			list.add(rb);

		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(),"finish excution with list size :"
				+ list.size());
		
		return list;
	}
	
	

	public String markFailedDmtTxn(DmtDetailsMastBean dmtMast) {
		  
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(), "|markFailedDmtTxn()|"
				+ dmtMast.getTxnid());
		
		
		String status="FAILED";	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction t = session.beginTransaction();
		try {

//			Query query = session.createSQLQuery("call refundwallettransaction(:txnid)");
			Query query = session.createSQLQuery("update remittanceledger set status='FAILED',remark='Transaction Failed',bankresp='FAILED' where id=:txnid and isbank in (1,2) and status!='SUCCESS';");
			query.setParameter("txnid", dmtMast.getTxnid());
			int i=query.executeUpdate();
			t.commit();
			
			if(i>0){
				status="SUCCESS";
			}
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(),"problem in initiateDMTRefund" + e.getMessage()+" "+e);
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return status;
	}
	
	
	
	public DmtDetailsMastBean checkDMTStatus(DmtDetailsMastBean dmtMast) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(),"|checkDMTStatus()|"
				+ dmtMast.getTxnid());
		
		
		String actcode = "";
		String responseCode = "";
		String status = "Not found";
		try {
			FINOCredentials.setFinoCredentials();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId",FINOCredentials.FINO_CLIENT_ID);
			jsonObject.put("AuthKey", FINOCredentials.FINO_AUTH_KEY);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(), dmtMast.getTxnid(),"jsonObject== " + jsonObject);
			
			JSONObject jsonObject1 = null;
			JSONObject jsonObject2 = null;

			FINOIMPSImplementation fno = new FINOIMPSImplementation();
			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "calling statusServicePost with txn id == " + dmtMast.getTxnid());
			
			String str = fno.callGetStatusServicePOST(dmtMast.getTxnid(), jsonObject.toString());
			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "getting response from statusServicePost str == " + str);
			
			jsonObject1 = new JSONObject(str);
			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "jsonObject1== " + jsonObject1);
			
			
			responseCode = jsonObject1.get("ResponseCode").toString();
			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "responseCode== " + responseCode);
			
			
			str = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY, parseJson(str));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(),"|calling statusServicePost with txn id == " + dmtMast.getTxnid()+"|getting response from statusServicePost str == " + str+"|jsonObject1== " + jsonObject1+"|responseCode== " + responseCode+ "|after decrypt str is == " + str);
			
			
			jsonObject2 = new JSONObject(str);
			actcode = jsonObject2.getString("ActCode");

			if (responseCode.equals("0")
					&& (actcode.equals("0") || actcode.equalsIgnoreCase("S") || actcode.equals("11") || actcode.equals("26"))) {
				status = "SUCCESS";
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "status is success== " + status);
				
				
			} else if ((responseCode.equals("0") || responseCode.equals("1")) && (!actcode.equals("P")
					&& !actcode.equals("NO") && !actcode.equals("Z4") && !actcode.equals("-1") && !actcode.equals("1")
					&& !actcode.equals("2") && !actcode.equals("7") && !actcode.equals("9") && !actcode.equals("8")
					&& !actcode.equals("M0") && !actcode.equals("91") && !actcode.equals("99") && !actcode.equals("998")
					&& !actcode.equals("999") && !actcode.equals("5001") && !actcode.equals("504")
					&& !actcode.equals("401") && !actcode.equals("500") && !actcode.equals("51")
					&& !actcode.equals("54") && !actcode.equals("61") && !actcode.equals("65")
					&& !actcode.equals("700292") && !actcode.equals("57") && !actcode.equals("56")
					&& !actcode.equals("62") && !actcode.equals("96") && !actcode.equals("97")
					&& !actcode.equals("20"))) {
				if (responseCode.equals("0") && (!actcode.equals("0") && !actcode.equalsIgnoreCase("S") && !actcode.equals("11")
						&& !actcode.equals("26"))) {
					status = "FAILURE";
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "status is failure== " + status);
					
					
				} else {
					status = "PENDING";
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "status is pending== " + status);
					
				}
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "problem in checkDMTStatus" + e.getMessage()+" "+e);
			
			logger.debug("exception == " + e);
			e.printStackTrace();

		}

		dmtMast.setBankresponse(status);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dmtMast.getAggreatorid(),"","", dmtMast.getSenderid(),dmtMast.getTxnid(), "finish excution with return:"
				+ dmtMast.getBankresponse());
		
		
		return dmtMast;

	}

	
	
	public static String parseJson(String req) {
		String response = "";
		try {
			JSONObject jsonObject = new JSONObject(req);
			response = (String) jsonObject.getString("ResponseData");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
/*	public List<String> finoImplementation(String narration,double txnAmount,String beneAccountNo,String beneIfsc,String beneName,String clientId,String debtorMobileNo,String debtorName)
	{
		
	      
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation");
		
		
		List<String> response = new ArrayList<String>();
		String responseCode=null;
		String actcode=null;
		FINOResponseBean finores = new FINOResponseBean();
		FINORespData res = new FINORespData();
		FINORequestBean finoReq = new FINORequestBean();
		String bankRRN = null;
		String BankBeneName = null;
		SessionFactory factory = null;
		Session session=null;
		Transaction transaction =null;
		try
		{	
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			String finotrxId = commanUtilDao.getTrxId("FINO");
			finoReq.setReqId(finotrxId);
			finoReq.setAmount(txnAmount);
			finoReq.setBeneAccountNo(beneAccountNo);
			finoReq.setBeneIFSCCode(beneIfsc);
			finoReq.setBeneName(beneName);
			finoReq.setClientUniqueID(clientId);
			finoReq.setCustomerMobileNo(debtorMobileNo);
			finoReq.setCustomerName(debtorName);
			finoReq.setrFU1("");
			finoReq.setrFU2("");
			finoReq.setrFU3("");
			session.save(finoReq);
			transaction.commit();
			transaction = session.beginTransaction();
			String parm = new Gson().toJson(finoReq);
			System.out.println(parm);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId", "79");
			jsonObject.put("AuthKey", "6e5fef67-3dae-4925-8271-cb0ca4a3fa11");
			String bankRespXml = "Fail";
			if (narration.equalsIgnoreCase("NEFT")) {
				bankRespXml = new FINOIMPSImplementation().callNEFTServicePOST(parm, jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation neft ");
				
				
			} else {
				bankRespXml = new FINOIMPSImplementation().callIMPSServicePOST(parm, jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation IMPS");
				
			

			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "bankrespxml after calling imps service"+bankRespXml);
			
			

			String bankRes = new String();
			if (bankRespXml.equalsIgnoreCase("Fail")) {
				bankRes = new FINOIMPSImplementation().callGetStatusServicePOST(finoReq.getClientUniqueID(),
						jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling get status for fail imps result");
				
				
			}
			if (bankRes.equalsIgnoreCase("Fail")) {
				finores.setResponseCode("0");
				res.setActCode("7000");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", " bank response is fail"+bankRes);
				
				  

			} else {
				
				if (bankRespXml.equalsIgnoreCase("Fail")) {
					finores = new Gson().fromJson(bankRes, FINOResponseBean.class);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "convertiong fail response to finoimps bean");
					
					

				} else {
					finores = new Gson().fromJson(bankRespXml, FINOResponseBean.class);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "convertiong success response to finoimps bean");
					
					

				}
				finores.setRespId(finotrxId);
				session.saveOrUpdate(finores);
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "convertiong success response to finoimps bean");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "committed finoimps bean db ");
				
				//System.out.println("++++++++++++++++++++++++++++++++committed finoimps bean db ++++++++++++++++++++++");

				
				String decData;
				if (finores.getResponseData().contains("\\")) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "if after call fino finalString "
							+ finores.getResponseData().replaceAll("\\\\", ""));
					
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY,
							finores.getResponseData().replaceAll("\\\\", ""));
				} else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "else after call fino finalString "
							+ finores.getResponseData());
					
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY, finores.getResponseData());
				}
				
				res = new Gson().fromJson(decData, FINORespData.class);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "decrypted response "+decData);
				
				

//				transaction = session.beginTransaction();
//				res.setRespId(finotrxId);
//				bankRRN= Double.toString(res.getTxnID());
				JSONObject json = new JSONObject(decData);			    
			    transaction = session.beginTransaction();
			    res.setRespId(finotrxId);
			    //bankRRN= Double.toString(res.getTxnID());
			    bankRRN=json.get("TxnID").toString();
				

				BankBeneName =res.getBeneName();
				session.saveOrUpdate(res);
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "commited finoimps bean 2 ");
				
				

				
			}
			
		}
		catch(Exception e)
		{
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|finoImplementation()|"+"problem found" + e.getMessage()+" "+e);
			
			

			e.printStackTrace();
			finores.setResponseCode("0");
			
			res.setActCode("7000");
			if(transaction.isActive())
				transaction.rollback();
		}
		try
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "inside try block after finoimps commited ");
			
			

			responseCode = finores.getResponseCode();
			actcode =res.getActCode();
			response.add(responseCode);
			response.add(actcode);
			if(bankRRN == null)
				response.add("null");
			else
				response.add(bankRRN);
			if(BankBeneName == null)
				response.add("null");
			else
				response.add(BankBeneName);
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","|finoImplementation()|"+ "excepiton occur while adding response value in list " + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			response.add("0");
			response.add("7000");
		}
		finally
		{
			session.close();
		}
		return response;
	}
*/
//	respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),fundSaveTxn.getDrAmount(),mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getName(),fundSaveTxn.getId(),mudraSenderBank.getMobileNo(),mudraSenderBank.getFirstName());

	public List<String> finoImplementation(String narration,double txnAmount,String beneAccountNo,String beneIfsc,String beneName,String clientId,String debtorMobileNo,String debtorName)
	{      
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation : "+clientId);		
		List<String> response = new ArrayList<String>();
		String responseCode=null;
		String actcode=null;
		FINOResponseBean finores = new FINOResponseBean();
		FINORespData res = new FINORespData();
		FINORequestBean finoReq = new FINORequestBean();
		String bankRRN = null;
		String BankBeneName = null;
		SessionFactory factory = null;
		Session session=null;
		Transaction transaction =null;
		try
		{	FINOCredentials.setFinoCredentials();
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			String finotrxId = commanUtilDao.getTrxId("FINO","OAGG001050");
			finoReq.setReqId(finotrxId);
			finoReq.setAmount(txnAmount);
			finoReq.setBeneAccountNo(beneAccountNo);
			finoReq.setBeneIFSCCode(beneIfsc);
			finoReq.setBeneName(beneName);
			finoReq.setClientUniqueID(clientId);
			finoReq.setCustomerMobileNo(debtorMobileNo);
			finoReq.setCustomerName(debtorName);
			finoReq.setrFU1("");
			finoReq.setrFU2("");
			finoReq.setrFU3("");
			session.save(finoReq);
			transaction.commit();
			transaction = session.beginTransaction();
			String parm = new Gson().toJson(finoReq);
			System.out.println(parm);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId", FINOCredentials.FINO_CLIENT_ID);
			jsonObject.put("AuthKey", FINOCredentials.FINO_AUTH_KEY);
			String bankRespXml = "Fail";
			if (narration.equalsIgnoreCase("NEFT")) {
				bankRespXml = new FINOIMPSImplementation().callNEFTServicePOST(parm, jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation neft ");
			} else if (narration.equalsIgnoreCase("IMPS")) {
				bankRespXml = new FINOIMPSImplementation().callIMPSServicePOST(parm, jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation IMPS");

			}
			else if (narration.equalsIgnoreCase("VERIFY")) {
				bankRespXml = new FINOIMPSImplementation().callIMPSServicePOST(parm, jsonObject.toString());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling finoimplementation VERIFY");

			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "bankrespxml after calling imps service"+bankRespXml);
			
			
			if (bankRespXml.equalsIgnoreCase("Fail")) {
				finores.setResponseCode("0");
				res.setActCode("7000");
			} else {
				try {
					finores = new Gson().fromJson(bankRespXml, FINOResponseBean.class);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","convertiong success response to finoimps bean");
				}catch (Exception e) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "problem in convertiong success response to finoimps bean");
					e.printStackTrace();
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "converted success response to finoimps bean "+finores);
				if (finores != null) {
					if (finores.getRespId() != null)
						finores.setRespId(finores.getRespId());
					else
						finores.setRespId(finotrxId);
				} else {
					finores = new FINOResponseBean();
					finores.setRespId(finotrxId);
				}
				session.saveOrUpdate(finores);
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "committed finoimps bean db "+finores);
				
				String decData = "";
				try {
				
				if (finores.getResponseData()!=null && finores.getResponseData().contains("\\")) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "if after call fino finalString "
							+ finores.getResponseData().replaceAll("\\\\", ""));
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY,
							finores.getResponseData().replaceAll("\\\\", ""));
				} else if(finores.getResponseData() != null){
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "else after call fino finalString "
							+ finores.getResponseData());
					
					decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY, finores.getResponseData());
				}
				System.out.println("sandeep"+decData);
					if (decData != null && decData.length() > 0) {
						res = new Gson().fromJson(decData, FINORespData.class);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
								"decrypted response " + decData);
					} else {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
								"problem in decrypting encrypted response");
					}
				}catch (Exception e) {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "problem in parsing decrypted response "+res);
				}

				if (res != null) {
					transaction = session.beginTransaction();
					res.setRespId(finotrxId);
					bankRRN = res.getTxnID();

					BankBeneName = res.getBeneName();
					session.saveOrUpdate(res);
					transaction.commit();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "commited finoimps bean 2 ");
				}else {
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "Response is null or empty after parsing decrypted response.");
				}
				
				if (narration.equalsIgnoreCase("NEFT")) {
					if(finores.getResponseCode().equals("0") && res.getActCode().equals("0"))
					{
						finores.setResponseCode("0");
						res.setActCode("0");
					}
					else if(finores.getResponseCode().equals("0") && (res.getActCode().equals("401") || res.getActCode().equals("500") ||res.getActCode().equals("998") ||
							res.getActCode().equals("5001") ||res.getActCode().equals("504") ||res.getActCode().equals("-1") ))
					{
						finores.setResponseCode("0");
						res.setActCode("7000");
					}
					else if(finores.getResponseCode().equals("1") && (res.getActCode().equals("1") ||res.getActCode().equals("100030") ||res.getActCode().equals("100050")
							||res.getActCode().equals("2388") ||res.getActCode().equals("3247") ||res.getActCode().equals("3599") ||res.getActCode().equals("700420") ||
							res.getActCode().equals("700511") ||res.getActCode().equals("7774") ||res.getActCode().equals("9999") ||res.getActCode().equals("503") ||
							res.getActCode().equals("-5") ||res.getActCode().equals("1008") ||res.getActCode().equals("1009") ||res.getActCode().equals("1012") ||
							res.getActCode().equals("100000") ||res.getActCode().equals("1003") ||res.getActCode().equals("1002") ||res.getActCode().equals("1004") ||
							res.getActCode().equals("1006") ||res.getActCode().equals("1007") ||res.getActCode().equals("1010") ||res.getActCode().equals("1011")  ))
					{
						finores.setResponseCode("1");
						res.setActCode("7000");
					}
					else
					{
						finores.setResponseCode("0");
						res.setActCode("7000");
					}
				} else if(narration.equalsIgnoreCase("IMPS")||narration.equalsIgnoreCase("VERIFY")){
					System.out.println("sandeep :re"+finores.getResponseCode()+"prajapati"+res.getActCode());
					if(finores.getResponseCode().equals("0") && res.getActCode().equals("0"))
					{
						finores.setResponseCode("0");
						res.setActCode("0");
					}
					else if(finores.getResponseCode().equals("0") && (res.getActCode().equals("08") || res.getActCode().equals("401") || res.getActCode().equals("500") ||
							res.getActCode().equals("51") || res.getActCode().equals("54") || res.getActCode().equals("61") || res.getActCode().equals("65") ||
							res.getActCode().equals("700292") || res.getActCode().equals("91") || res.getActCode().equals("99") || res.getActCode().equals("998") || 
							res.getActCode().equals("999") || res.getActCode().equalsIgnoreCase("M0") || res.getActCode().equals("5001") ||res.getActCode().equals("504") ||
							res.getActCode().equals("-1") || res.getActCode().equals("57") || res.getActCode().equals("56") || res.getActCode().equals("62") ||
							res.getActCode().equals("96") ||res.getActCode().equalsIgnoreCase("Z4") || res.getActCode().equals("97") || 
							res.getActCode().equalsIgnoreCase("NO") || res.getActCode().equals("39") || res.getActCode().equals("997") ||
							res.getActCode().equals("42") || res.getActCode().equals("86") || res.getActCode().equalsIgnoreCase("MA") ))
					{
						finores.setResponseCode("0");
						res.setActCode("7000");
					}
					else if(finores.getResponseCode().equals("1") && (res.getActCode().equals("05") || res.getActCode().equals("1") || res.getActCode().equals("100030") ||
							res.getActCode().equals("12") || res.getActCode().equals("13") || res.getActCode().equals("14") || res.getActCode().equals("1515") ||
							res.getActCode().equals("20") || res.getActCode().equals("22") || res.getActCode().equals("2388") || res.getActCode().equals("30") ||
							res.getActCode().equals("3098") || res.getActCode().equals("3247") || res.getActCode().equals("52") || res.getActCode().equals("62") || 
							res.getActCode().equals("64") || res.getActCode().equals("700322") || res.getActCode().equals("700420") || res.getActCode().equals("700511") ||
							res.getActCode().equals("92") || res.getActCode().equals("94") || res.getActCode().equals("96") || res.getActCode().equalsIgnoreCase("M1") ||
							res.getActCode().equalsIgnoreCase("M2") || res.getActCode().equalsIgnoreCase("M3") || res.getActCode().equalsIgnoreCase("M4") || 
							res.getActCode().equalsIgnoreCase("M5") || res.getActCode().equalsIgnoreCase("MP") || res.getActCode().equals("9999")|| 
							res.getActCode().equals("503")|| res.getActCode().equals("-5")|| res.getActCode().equals("1003")|| res.getActCode().equals("1002")||
							res.getActCode().equals("1004")|| res.getActCode().equals("1006")|| res.getActCode().equals("1007")|| res.getActCode().equals("1010") || 
							res.getActCode().equals("1011") || res.getActCode().equals("1008") || res.getActCode().equals("1009") || res.getActCode().equals("1012") || 
							res.getActCode().equals("1013")))
					{
						finores.setResponseCode("1");
						res.setActCode("7000");
					}
				}
				
			}
			
		}
		catch(Exception e)
		{
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|finoImplementation()|"+"problem found" + e.getMessage()+" "+e);
			
			

			e.printStackTrace();
			finores.setResponseCode("0");
			
			res.setActCode("7000");
			if(transaction.isActive())
				transaction.rollback();
		}
		try
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "inside try block after finoimps commited ");
			
			

			responseCode = finores.getResponseCode();
			actcode =res.getActCode();
			response.add(responseCode);
			response.add(actcode);
			if(bankRRN == null)
				response.add("null");
			else
				response.add(bankRRN);
			if(BankBeneName == null)
				response.add("null");
			else
				response.add(BankBeneName);
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","|finoImplementation()|"+ "excepiton occur while adding response value in list " + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			response.add("0");
			response.add("7000");
		}
		finally
		{
			session.close();
		}
		return response;
	}

	
	
	public MudraSenderBank deletedBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,
			   String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(),"",serverName + "****RequestId******" + requestId
			    +"| deletedBeneficiary()|"
			    + mudraBeneficiaryBank.getId());	 
	
			  factory = DBUtil.getSessionFactory();
			  session = factory.openSession();
			  transaction = session.beginTransaction();
			  MudraSenderBank mudraSenderBank = new MudraSenderBank();
			  mudraSenderBank.setStatusCode("1001");
			  try {

			   if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
			    mudraSenderBank.setStatusCode("1110");
			    mudraSenderBank.setStatusDesc("Invalid Sender Id.");
			    return mudraSenderBank;
			   }
			   if (mudraBeneficiaryBank.getId() == null || mudraBeneficiaryBank.getId().isEmpty()) {
			    mudraSenderBank.setStatusCode("1116");
			    mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
			    return mudraSenderBank;
			   }

			   MudraBeneficiaryBank mudraBeneficiaryMastUpdate = (MudraBeneficiaryBank) session
			     .get(MudraBeneficiaryBank.class, mudraBeneficiaryBank.getId());

			   if (mudraBeneficiaryMastUpdate == null) {
			    mudraSenderBank.setStatusCode("1116");
			    mudraSenderBank.setStatusDesc("Invalid Beneficiary Id.");
			    return mudraSenderBank;
			   } else {
			    // added for defect id 7.1
			    if (!mudraBeneficiaryMastUpdate.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId())) {
			     mudraSenderBank.setStatusCode("1130");
			     mudraSenderBank.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
			     return mudraSenderBank;
			    }
			 
			    List<String> delcheck = null;
			   
			     delcheck=session.createCriteria(MudraMoneyTransactionBean.class)
			      .add(Restrictions.eq("beneficiaryId", mudraBeneficiaryBank.getId()))
			      .add(Restrictions.in("narrartion",new String[] {"IMPS","NEFT"}))
			      .add(Restrictions.in("status",new String[]{"deleted"}))
			      .list();
			          
			    if (delcheck!=null&&delcheck.size() == 0) {
			   
			    mudraBeneficiaryMastUpdate.setStatus("Deleted");
			    session.update(mudraBeneficiaryMastUpdate);
			    transaction.commit();
			    List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
			      .add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
			      .add(Restrictions.ne("status", "Deleted"))
			      .addOrder(Order.asc("status")).list();
			    mudraSenderBank.setBeneficiaryList(benfList);
			    mudraSenderBank.setStatusCode("1000");
			    mudraSenderBank.setStatusDesc("Beneficiary Deleted.");
			   }
			    else
			    {
			     mudraSenderBank.setStatusCode("1001");
			     mudraSenderBank.setStatusDesc("Sorry !! This is a transacting beneficiary so cannot be deleted.");
			    }

			  } 
			   }catch (Exception e) {
				   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","",mudraBeneficiaryBank.getSenderId(),"",serverName + serverName + "****RequestId******" + requestId
						     + "problem in deletedBeneficiary" + e.getMessage()+" "+e);
				  
			   mudraSenderBank.setStatusCode("7000");
			   mudraSenderBank.setStatusDesc("Error");
			   e.printStackTrace();
			   transaction.rollback();
			  } finally {
			   session.close();
			  }

			  return mudraSenderBank;
			 }
	
	
/*	public ResponseBeanBank refundAgent(String userId,String senderMobile,String mpin,String isBank)
	 {
	  ResponseBeanBank resp = new ResponseBeanBank();
	  resp.setCode("1001");
	  resp.setMessage("Please try after sometime.");
	  try
	  {
	   factory = DBUtil.getSessionFactory();
	   session=factory.openSession();
	   List<RefundAgent> refundList = new ArrayList<RefundAgent>();
		   MudraSenderBank sender = (MudraSenderBank)session.createCriteria(MudraSenderBank.class).add(Restrictions.eq("mobileNo", senderMobile))
		     .add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mpin))).uniqueResult();
		   if(sender == null)
		   {
		    resp.setCode("2000");
		    resp.setMessage("Invalid Sender details.");
		    return resp;
		   }
		   else
		   {
		   
		    SQLQuery query = session.createSQLQuery("select CASE WHEN DATE_ADD(DATE(TXNDATE), INTERVAL +10 DAY)<DATE(now()) THEN 1 ELSE 0 END as otp,a.id ,txndebit txnamount,dramount ,a.status ,narrartion ,mobileno from "
		      + "remittanceledger a inner join mudrasendermast b inner join wallettransaction c on a.txnid=c.resptxnid and a.senderid=b.id  where c.txncode=4 and a.txnid in "
		      + "(select txnid from remittanceledger where (narrartion='WALLET LOADING' or remark like '%AGENT%' or status='SUCCESS' or status='REFUND')  group by txnid having "
		      + "sum(cramount)-sum(dramount)>0) and (a.status='FAILED' or a.status like 'Confirmation%') and dramount>1 and isbank in (1,0)  and date(ptytransdt)>=DATE_ADD(now(),INTERVAL -90 DAY)"
		      + " and senderid='"+sender.getId()+"' and a.walletid in (select walletid from walletmast where id='"+userId+"')   order by ptytransdt""select a.id as id,a.status as status,dramount,narrartion ,mobileno  from remittanceledger a inner "
		      + "join mudrasendermast b on a.senderid=b.id  where txnid in (select txnid from remittanceledger where (narrartion='WALLET LOADING' or remark like '%AGENT%' or "
		      + "status='SUCCESS' or status='REFUND')  group by txnid having sum(cramount)-sum(dramount)>0) and (a.status='FAILED' or "
		      + "a.status like 'Confirmation%') and dramount>1 and isbank=1  and date(ptytransdt)>=DATE_ADD(now(),INTERVAL -3 DAY) and senderid='"+senderId+"' "
		        + "and walletid=(select walletid from walletmast where id='"+userId+"') order by ptytransdt ");
		     
		    query.addScalar("otp",Hibernate.STRING).addScalar("id").addScalar("txnamount",Hibernate.STRING).addScalar("status").addScalar("dramount",Hibernate.STRING).addScalar("narrartion").addScalar("mobileno");
		    query.setResultTransformer(Transformers.aliasToBean(RefundAgent.class));
		    refundList=query.list();
		   }
	    if( refundList == null || refundList.size() < 1)
	    {
	     resp.setCode("2001");
	     resp.setMessage("No record found for refund.");
	     return resp;
	    }
	    else
	    {
	     resp.setCode("1000");
	     resp.setMessage("Successful");
	     resp.setRefundList(refundList);
	   }
	  }
	  catch(Exception e)
	  {
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :refundAgent|problem found" + e.getMessage()+" "+e);
	   e.printStackTrace();
	  }
	  finally
	  {
	   session.close();
	  }
	  return resp;
	 }*/
	
	
	public ResponseBeanBank refundAgent(String userId,String senderMobile,String txnId)
	{
	  ResponseBeanBank resp = new ResponseBeanBank();
	  resp.setCode("1001");
	  resp.setMessage("Please try after sometime.");
	  try
	  {
	   factory = DBUtil.getSessionFactory();
	   session=factory.openSession();
	   Boolean proceed = false;
	   List<MudraMoneyTransactionBean> mtBean = session.createCriteria(MudraMoneyTransactionBean.class).add(Restrictions.eq("txnId", txnId)).list();
	   List<RefundAgent> refundList = new ArrayList<RefundAgent>();
  	   //get count in refund list
	   if(mtBean != null && mtBean.size() > 0)
	   {
		   int val=0;
		   for(MudraMoneyTransactionBean trans:mtBean)
		   {
			   if(trans.getNarrartion().trim().equalsIgnoreCase("IMPS") || trans.getNarrartion().trim().equalsIgnoreCase("NEFT"))
			   {
				   val++;
			   }
		   }
		   int count = ((BigInteger)session.createSQLQuery("select count(1) from refundtransaction where id like '%"+txnId+"'").uniqueResult()).intValue();
		   if(count < val)
		   {
			   if(mtBean.get(0).getIsBank() == 0)
			   {
				   MudraSenderWallet sender = (MudraSenderWallet)session.createCriteria(MudraSenderWallet.class).add(Restrictions.eq("mobileNo", senderMobile)).uniqueResult();
				   if(sender.getMobileNo().equals(senderMobile))
				   {
					   proceed = true;
					   SQLQuery query = session.createSQLQuery("select CASE WHEN DATE_ADD(DATE(TXNDATE), INTERVAL +10 DAY)<DATE(now()) THEN 1 ELSE 0 END as otp,a.id ,"
					   		+ "ptytransdt,txndebit txnamount,dramount ,a.status ,narrartion ,mobileno from remittanceledger a inner join mudrasendermast b inner join wallettransaction c on a.txnid=c.resptxnid a.txnid IN ('"+txnId+"') and a.senderid=b.id where c.txncode=4 AND senderid='"+sender.getId()+"' and a.txnid in (select txnid from remittanceledger where (narrartion='WALLET LOADING' or remark like '%AGENT%' or status='SUCCESS' or status='REFUND') group by txnid having sum(cramount)-sum(dramount)>0) and (a.status='FAILED' or a.status like 'Confirmation%') and dramount>1 and isbank in (1,2) and date(ptytransdt)>=DATE_ADD(now(),INTERVAL -90 DAY)  and a.walletid in (select walletid from walletmast WHERE id='"+userId+"') order by ptytransdt;");
					
					   query.addScalar("otp",Hibernate.STRING).addScalar("id").addScalar("status").addScalar("dramount",Hibernate.STRING).addScalar("narrartion").addScalar("mobileno");
					    query.setResultTransformer(Transformers.aliasToBean(RefundAgent.class));
					    refundList=query.list();
				   }
				   else
				   {
					   resp.setCode("2000");
					   resp.setMessage("This transaction does not belong to "+senderMobile+" sender");
					   return resp;
				   }
			   }
			   else if(mtBean.get(0).getIsBank() == 1 || mtBean.get(0).getIsBank() == 2 || mtBean.get(0).getIsBank() == 4)
			   {
				   MudraSenderBank sender = (MudraSenderBank)session.createCriteria(MudraSenderBank.class).add(Restrictions.eq("mobileNo", senderMobile)).uniqueResult();
				   if(sender.getMobileNo().equals(senderMobile))
				   {
					   proceed = true;
					    SQLQuery query = session.createSQLQuery("select CASE WHEN DATE_ADD(DATE(TXNDATE), INTERVAL  +10 DAY)<DATE(now()) THEN 1 ELSE 0 END as otp,a.id ,ptytransdt,txndebit txnamount,dramount ,a.status ,narrartion ,mobileno from remittanceledger a inner join mudrasendermast b inner join wallettransaction c on a.txnid=c.resptxnid and a.senderid=b.id where c.txncode=4 and a.txnid in (select txnid from remittanceledger where ((narrartion='WALLET LOADING' or remark like '%AGENT%' or status='SUCCESS' or status='REFUND') and txnid IN ('"+txnId+"')) group by txnid having sum(cramount)-sum(dramount)>0) and (a.status='FAILED' or a.status like 'Confirmation%') and dramount>1 and isbank in (1,2,4) and senderid='"+sender.getId()+"' and date(ptytransdt)>=DATE_ADD(now(),INTERVAL -90 DAY)  and a.walletid in (select walletid from walletmast where id='"+userId+"') order by ptytransdt");
					     
					    query.addScalar("otp",Hibernate.STRING).addScalar("id").addScalar("status").addScalar("dramount",Hibernate.STRING).addScalar("narrartion").addScalar("mobileno");
					    query.setResultTransformer(Transformers.aliasToBean(RefundAgent.class));
					    refundList=query.list();
				   }
				   else
				   {
					   resp.setCode("2000");
					   resp.setMessage("This transaction does not belong to "+senderMobile+" sender");
					   return resp;
				   }
			   }
			   if(proceed)
			   {
				    if( refundList == null || refundList.size() < 1)
				    {
				     resp.setCode("2001");
				     resp.setMessage("No record found for refund.");
				     return resp;
				    }
				    else
				    {
				     resp.setCode("1000");
				     resp.setMessage("Successful");
				     resp.setRefundList(refundList);
				   }
			   }
		   }
	   }
	   else
	   {
		   resp.setCode("2001");
	       resp.setMessage("No record found for refund.");
	       return resp;
	   }
	  }
	  catch(Exception e)
	  {
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :refundAgent|problem found" + e.getMessage()+" "+e);
	   e.printStackTrace();
	  }
	  finally
	  {
	   session.close();
	  }
	  return resp;
	 }
	
	public ResponseBeanBank refundAgentRequest(String userId,String senderMobile,String mpin,String txnId)
	 {
		
	  ResponseBeanBank resp = new ResponseBeanBank();
	  resp.setCode("1001");
	  resp.setMessage("Please try after sometime.");
	  Transaction t = null;
	  String sender = null;
	  try
	  {
	   factory = DBUtil.getSessionFactory();
	   session=factory.openSession();
	   t = session.beginTransaction();
	   MudraMoneyTransactionBean mtBean =(MudraMoneyTransactionBean) session.get(MudraMoneyTransactionBean.class,txnId);
	   if(mtBean != null)
	   {
		  // WalletMastBean agent = (WalletMastBean)session.get(WalletMastBean.class, mtBean.getAgentid());
		   /*if(agent != null)
		   {*/
			   Long transactionDate = mtBean.getPtytransdt().getTime();
			   Long todayDate =new Date().getTime();
			   MudraSenderBank senderBank  = null;
			   MudraSenderWallet senderWallet  = null;
			   
			   long days = TimeUnit.DAYS.convert((todayDate - transactionDate), TimeUnit.MILLISECONDS);
			   if(mtBean.getIsBank() == 1 || mtBean.getIsBank() == 2|| mtBean.getIsBank() == 4)
			   {
				   if(days >= 10 && mpin.equals("-1"))
				   {
					   senderBank = (MudraSenderBank)session.createCriteria(MudraSenderBank.class).add(Restrictions.eq("mobileNo", senderMobile)).uniqueResult();
				   }
				   else
				   {
					   senderBank = (MudraSenderBank)session.createCriteria(MudraSenderBank.class).add(Restrictions.eq("mobileNo", senderMobile)).
							   add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mpin))).uniqueResult();
				   }
				   if(senderBank != null)
					   sender = senderBank.getId();
			   }
			   else if(mtBean.getIsBank() == 0)//check date 10 days then otp
			   {
				   if(days >= 10 && mpin.equals("-1"))
				   {
					   senderWallet = (MudraSenderWallet)session.createCriteria(MudraSenderWallet.class).add(Restrictions.eq("mobileNo", senderMobile)).uniqueResult();
				   }
				   else
				   {
					   senderWallet = (MudraSenderWallet)session.createCriteria(MudraSenderWallet.class).add(Restrictions.eq("mobileNo", senderMobile)).
							   add(Restrictions.eq("mpin", CommanUtil.SHAHashing256(mpin))).uniqueResult();
				   }
				   if(senderWallet != null)
					   sender = senderWallet.getId();
			   }
		   if(sender == null)
		   {
		    resp.setCode("2000");
		    resp.setMessage("Invalid Sender details.");
		    return resp;
		   }
		   else
				{
						String txnStatus = null;
						logger.info("Logged in user is "+userId+" taking refund of transaction id "+txnId);
						if((mtBean.getNarration() != null && mtBean.getNarration().trim().equalsIgnoreCase("manually")) || userId.trim().equalsIgnoreCase("ADMA001043")){
							txnStatus = "FAILED";
						}
						else if(mtBean.getIsBank() == 1){
							txnStatus = getTxnStatusFino(txnId,mtBean.getNarrartion());
							
						}else if(mtBean.getIsBank() == 2) {
							txnStatus = getAadharShilaStatus(txnId);
						}
						else if(mtBean.getIsBank() == 4) {
							PayoutMasterDMT pay = new PayoutMasterDMT();
						    String bankRes = pay.disburseStatusQueryAPI(txnId);
						    JSONObject jsonObject = new JSONObject(bankRes);
							txnStatus = jsonObject.getString("status");
						}
						else if(mtBean.getIsBank() == 0){
							txnStatus = "FAILED";
						}
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :refundAgentRequest|txn status :- " + txnStatus);
		
						if (txnStatus != null && (txnStatus.equals("FAILED")|| txnStatus.equals("FAILURE"))) {
							Query queryRefund = session.createSQLQuery(
									"call refundtransaction(:txnId,:agentId,:senderId)");
							queryRefund.setParameter("txnId", txnId);
							queryRefund.setParameter("agentId", userId);
							queryRefund.setParameter("senderId", sender);
							queryRefund.executeUpdate();
							t.commit();
						} else {
							resp.setCode("2000");
							resp.setCode("Problem in refund process. ");
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :refundAgentRequest|fino status :- " + txnStatus);
		
							return resp;
						}
						SQLQuery queryConfirm = session.createSQLQuery(
								"select remark from refundtransaction  where id='"
										+ txnId + "'");
		
						String status = (String) queryConfirm.uniqueResult();
						resp.setMessage(status);
					}
		   }
	  //}
	  }
	  catch(Exception e)
	  {
	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :refundAgentRequest|problem found" + e.getMessage()+" "+e);
	   e.printStackTrace();
	   t.rollback();
	   resp.setCode("Problem in refund process.");
	  }
	  finally
	  {
	   resp.setCode("3000");
	   session.close();
	  }
	  return resp;
	  }

	public HashMap getAgentSenderBalance(MudraSenderBank mudra)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudra.getAggreatorId(),mudra.getAgentId(),mudra.getMobileNo(), "","",  "|Method Name :getAgentSenderBalance");
		HashMap<String,String> resp = new HashMap<String,String>();
		factory = DBUtil.getSessionFactory();
		try
		{
			session =factory.openSession();
			//SQLQuery query = session.createSQLQuery("SELECT walletbalance ,transferlimit FROM mudrasendermast WHERE id =:id AND aggreatorid=:aggreatorid");
			Criteria cr = session.createCriteria(MudraSenderBank.class)
				    .setProjection(Projections.projectionList()
				      .add(Projections.property("walletBalance"),  "walletBalance")
				      .add(Projections.property("transferLimit"),  "transferLimit"))
				    .add(Restrictions.eq("id", mudra.getId()))
				    .setResultTransformer(Transformers.aliasToBean(MudraSenderBank.class));

			
			MudraSenderBank senderMast = (MudraSenderBank)cr.uniqueResult();
			
			resp.put("walletBalance",Double.toString(senderMast.getWalletBalance()));
			
			resp.put("SenderTransferLimit",Double.toString(senderMast.getTransferLimit()));
			resp.put("AgentBalance", Double.toString(new TransactionDaoImpl().getWalletBalancebyId(mudra.getAgentId())));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudra.getAggreatorId(),mudra.getAgentId(),mudra.getMobileNo(), "","",  "|Method Name :getAgentSenderBalance|sender:walletbal:translimit:agentbal["+mudra.getId()
			+":"+senderMast.getWalletBalance()+":"+senderMast.getTransferLimit()+":"+resp.get("AgentBalance")+"]");
			return resp;
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getAgentSenderBalance | problem found" + e.getMessage()+" "+e);
			e.printStackTrace();
			transaction.rollback();
		}
		finally
		{
			session.close();
		}
		return resp;
	}
	
	
	public String getBankCodeFromIfscMaster(String IFSCCode)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getBankCodeFromIfscMaster IFSCCode :-"+IFSCCode);

		factory = DBUtil.getSessionFactory();
		Session session=null;
		try
		{
			session =factory.openSession();
			SQLQuery query = session.createSQLQuery("SELECT bankcode FROM ifscmaster WHERE ifsccode =:ifsccode");
			query.setParameter("ifsccode", IFSCCode );
			List list=query.list();
			if(list!=null&& list.size()>0) {
			return (String)list.get(0);
			}
			return "";
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getBankCodeFromIfscMaster | problem found" + e.getMessage()+" "+e);
		}
		finally
		{
			if(session!=null)
			session.close();
		}
		return "";
	}
	

	
	public static void main(String[] s)
	{
		try{
		//int count = new MudraDaoBankImpl().gettxndetailsCount("75325629","OAGG001062", "","");
		File file = new File("C:/Users/shweta.roy/Desktop/data.txt");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String st = null;
			String	bankRespXml = "";
		while((st = reader.readLine()) != null)
		{
			bankRespXml = bankRespXml+st;
		}
		
		FINOResponseBean finores = new FINOResponseBean();
		finores = new Gson().fromJson(bankRespXml, FINOResponseBean.class);
		
		String decData;
		if (finores.getResponseData().contains("\\")) {
		
			decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY,finores.getResponseData().replaceAll("\\\\", ""));
		} else {
			
			decData = EncryptionByEnc256.decryptOpenSSL(FINOCredentials.FINO_BODY_ENCRYPTION_KEY, finores.getResponseData());
		}
		System.out.println("decrpt ------"+decData);
		}
		catch(Exception e)
		{
			
		}
		finally{}
	}
	
	public String getPlanIdByAgentId(String agentId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getPlanIdByAgentId IFSCCode :-"+agentId);

		factory = DBUtil.getSessionFactory();
		Session session=null;
		try
		{
			session =factory.openSession();
			WalletMastBean walletMast=(WalletMastBean)session.get(WalletMastBean.class, agentId);
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			walletMast=(WalletMastBean)session.get(WalletMastBean.class, walletMast.getDistributerid());
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			walletMast=(WalletMastBean)session.get(WalletMastBean.class, walletMast.getAggreatorid());
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			
			
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getPlanIdByAgentId | problem found" + e.getMessage()+" "+e);
			
		}
		finally
		{
			if(session!=null)
			session.close();
		}
		return "N/A";
	}
	
	
	
	
	
	
	public String getWalletBalance(String aggreatorid) {
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","" + "******" + "" + "" + ""+ "" + aggreatorid);
	
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String wallet=null;
		try {
            StringBuilder serchQuery = new StringBuilder();
            serchQuery.append("select finalBalance from walletbalance where walletid=(select walletid from walletmast where id=:agentId); ");
            SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentId", aggreatorid);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","" + "******" + "" + "Query:" + serchQuery.toString());
			List list = query.list(); 
			if(list.size()>0 && !list.isEmpty())
			{
			 wallet = ""+list.get(0);	
			System.out.println("Object  "+wallet);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","" + "******" + ""+list.size());
			}
	      } catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","" + "******" + "" + "problem in getWalletBalanceDtl" + e.getMessage()+" "+e);
			 e.printStackTrace();
			//transaction.rollback();
		} finally {
			session.close();
		}
		return wallet;
	}

	
	public DMTReportInOut getDmtReportBySuper(DMTReportInOut dMTReportInOut, String serverName, String requestIds) {
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestIds
				+ "|getDmtReportBySuper()|" + dMTReportInOut.getAgentId()+ " |getDmtReportBySuper stDAte**:"
						+ dMTReportInOut.getStDate()+ " |getDmtReportBySuper edDate**:" + dMTReportInOut.getEdDate());
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<TransacationLedgerBean> transList = new ArrayList<TransacationLedgerBean>();
		try {
            StringBuilder serchQuery = new StringBuilder();
			if (dMTReportInOut.getUsertype()==2.0) 
			    serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid=:agentid  and isbank in (1,2,3,4)");
			else if (dMTReportInOut.getUsertype()==3.0) 
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where distributerid=:agentid)  and isbank in (1,2,3,4)");
			else if (dMTReportInOut.getUsertype()==7.0) 
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where superdistributerid=:agentid)  and isbank in (1,2,3,4)");
			else if (dMTReportInOut.getUsertype()==8.0) 
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where salesId=:agentid)  and isbank in (1,2,3,4)");
			else if (dMTReportInOut.getUsertype()==9.0) 
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where managerId=:agentid)  and isbank in (1,2,3,4)");
            /*  else
				serchQuery.append("SELECT id,agentid, senderid, DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') AS  ptytransdt,txnid,dramount,narrartion,mobileno,firstname,lastname,accountno,NAME,ifsccode,bankname,bankrrn,status,isbank,merchanttransid FROM transactionledger WHERE agentid in (select id from walletmast where distributerid=:agentid)  and isbank in (1,2,3,4)");
			*/
			if(dMTReportInOut.getStatuscode()!= null && !"-1".equalsIgnoreCase(dMTReportInOut.getStatuscode())) {
				serchQuery.append(" and status='"+dMTReportInOut.getStatuscode()+"'");
			}
			
			serchQuery.append(" AND narrartion not like 'WALLET LOADING%'");
			if (!(dMTReportInOut.getStDate() == null || dMTReportInOut.getStDate().isEmpty())
					&& !(dMTReportInOut.getEdDate() == null || dMTReportInOut.getEdDate().isEmpty())) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(dMTReportInOut.getEdDate())) + "', '/', '-'),'%d-%m-%Y') ");
			}
			else {
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				serchQuery.append(" AND DATE(ptytransdt) = STR_TO_DATE(REPLACE('"
						+ formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
			}
			serchQuery.append(" ORDER BY DATE_FORMAT(ptytransdt, '%d/%m/%Y %T') desc");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestIds
					+ "Query:" + serchQuery.toString());
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("agentid", dMTReportInOut.getAgentId());
			//query.setString("isbank","1");
			query.setResultTransformer(Transformers.aliasToBean(TransacationLedgerBean.class));
			transList = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","getDmtReportBySuper", "", "",serverName + "****RequestId****" + requestIds
					+ "totla Count :" + transList.size());
			
			dMTReportInOut.setTransList(transList);

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "****RequestId****" + requestIds
					+ "problem in getDmtReportBySuper" + e.getMessage()+" "+e);
			
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return dMTReportInOut;
	}
	
	
	
	
	
	
}
