package com.bhartipay.cache;

public class CacheFactory {
	
	public static TableCacheImpl globalCache=null;
	
	public static TableCache getCache() {
		synchronized (CacheFactory.class) {
			if(globalCache==null) {
				synchronized (CacheFactory.class) {
				globalCache=new TableCacheImpl();
				}
			}
			return globalCache;
		}
	}
	
	
}
