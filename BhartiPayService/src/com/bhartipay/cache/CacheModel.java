package com.bhartipay.cache;


public class CacheModel {

	private long lastAccess;
	private Integer count;
	
	
	public long getLastAccess() {
		return lastAccess;
	}
	public Integer getCount() {
		return count;
	}
	public void setLastAccess(long lastAccess) {
		this.lastAccess = lastAccess;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
}
