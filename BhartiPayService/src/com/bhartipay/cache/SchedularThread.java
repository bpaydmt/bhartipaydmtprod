package com.bhartipay.cache;

import java.util.TimerTask;

/*
 * schedularThread is for removing object in cache which is expired.
 */
public class SchedularThread extends TimerTask{

	@Override
	public void run() {
		System.out.println("*******************schedular start for cleaning expired object in cache ********************");
		TableCache cacheImpl=CacheFactory.getCache();
		cacheImpl.cleanUpCache();
		System.out.println("*******************schedular finish task for cleaning expired object in cache ********************");
		
	}

}
