package com.bhartipay.cache;


public interface TableCache {
	
	public CacheModel putCache(String bank,CacheModel model)throws Exception;
	
	public void removeCache(String bank)throws Exception;

	public CacheModel getCache(String bank)throws Exception;
	
	public void clearCache();
	
	public int size();
	
	public void cleanUpCache();
	
}
