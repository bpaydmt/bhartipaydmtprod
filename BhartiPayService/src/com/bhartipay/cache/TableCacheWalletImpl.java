package com.bhartipay.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.util.DBUtil;

/**
 * 
 * @author ambuj.singh
 *
 */
public class TableCacheWalletImpl implements TableCache {
	
	
	private ReentrantReadWriteLock readwriteLock = null;
	private Lock readLock = null;
	private Lock writeLock = null;
	private final Map<String, CacheModel> globalCache;
	private static final int TIME_TO_LIVE=30;//in minute
	public static final int TXN_FAILED_COUNT=10;
	
	public TableCacheWalletImpl() {
		readwriteLock = new ReentrantReadWriteLock();
		readLock = readwriteLock.readLock();
		writeLock = readwriteLock.writeLock();
		globalCache = new HashMap<>();
	    SchedularThread schedularThread=new SchedularThread();
		Timer timer=new Timer(true);
		timer.scheduleAtFixedRate(schedularThread,0,30000);
		
	}

	
	/**
	 * it is use for adding object in cache
	 */
	public CacheModel putCache(String bank,CacheModel model)throws Exception{
	
		try {
			writeLock.lock();
			CacheModel cacheModel=new CacheModel();
		
			if(globalCache.containsKey(bank)) {
				cacheModel=globalCache.get(bank);				
			}
			
			cacheModel.setLastAccess(System.currentTimeMillis());
			cacheModel.setCount(model.getCount());
			globalCache.put(bank, cacheModel);
			return cacheModel;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			writeLock.unlock();
		}
		return null;
	}
	
	/**
	 * it is use for getting object from cache on the basis of table name and id.
	 */
	@Override
	public CacheModel getCache(String bank)throws Exception{
		
		try {
		readLock.lock();
		if(!globalCache.containsKey(bank)) {
			return null;
		}else {
			CacheModel cacheModel=globalCache.get(bank);
			cacheModel.setLastAccess(System.currentTimeMillis());
			return cacheModel;
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		finally {
			readLock.unlock();
		}
		return null;
	}
	
	
	/**
	 * it is use for remove object from cache on the basis of table name.
	 */
	@Override
	public void removeCache(String bank)throws Exception{
		
		try {
		writeLock.lock();
		globalCache.remove(bank);
		}
		catch (Exception e) {
			System.out.println("**************** problem occur while deleting object in cache ***********************");
		}finally {
			writeLock.unlock();
		}
	}
	
	
	/**
	 * it is use for remove all object from cache.	 
	 * */
	@Override
	public void clearCache(){
		writeLock.lock();
		try {
			if(globalCache!=null) {
				globalCache.clear();
			}
			
		}finally {
			writeLock.unlock();
		}
	}

	/**
	 * it is use for getting the size of object stored in cache.
	 */
	@Override
	public int size() {
		return globalCache.size();
	}
	
	
	/**
	 * it is use for cleanup expired object from cache manually and auto by daemon thread.
	 */
	public void cleanUpCache() {
		try {
			Long now=System.currentTimeMillis();
			ArrayList<String> removeSet=new ArrayList<String>();
			readLock.lock();
			Set<Entry<String, CacheModel>> keySet=globalCache.entrySet();
			Iterator<Entry<String,CacheModel>> ite=keySet.iterator();
			while(ite.hasNext()) {
				Entry<String,CacheModel> entry=ite.next();
				if(now>(entry.getValue().getLastAccess()+(TIME_TO_LIVE*1000*60))&&entry.getValue().getCount()>=TXN_FAILED_COUNT) {
				    removeSet.add(entry.getKey());
				}
			}
			
			for (String keyToRemove : removeSet) {
				System.out.println("*************** removed object from cache ********************"+keyToRemove);
				globalCache.remove(keyToRemove);
				DBUtil dbUtil=new DBUtil();
				SessionFactory factory=dbUtil.getSessionFactory();
				Session session=factory.openSession();
				Transaction transaction=session.beginTransaction();
				try{
				Query query=session.createSQLQuery("delete from blockbank where bankname=:bankName");
				query.setParameter("bankName",keyToRemove);
				query.executeUpdate();
				transaction.commit();
				}catch(Exception e){
					if(transaction!=null)
						transaction.rollback();
				}finally{
					session.close();
				}
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		finally {
			readLock.unlock();
		}
		
		
	}
		
}
