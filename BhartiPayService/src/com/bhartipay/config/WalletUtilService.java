package com.bhartipay.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.report.persistence.ReportDao;
import com.bhartipay.report.persistence.ReportDaoImpl;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.bean.CouponsBean;
import com.bhartipay.util.bean.InstallationDetails;
import com.bhartipay.util.bean.MailConfigMast;
import com.bhartipay.util.bean.MerchantOffersMast;
import com.bhartipay.util.bean.PartnerPrefundMastBean;
import com.bhartipay.util.bean.PaymentGatewayURL;
import com.bhartipay.util.bean.SMSConfigMast;
import com.bhartipay.util.bean.UserMenuMapping;
import com.bhartipay.util.bean.UserRoleMapping;
import com.bhartipay.util.bean.VersionControl;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.recharge.plan.bean.TariffPlans;
import com.bhartipay.util.thirdParty.CouponsImplementation;
import com.bhartipay.util.thirdParty.KikbakImplementation;
import com.bhartipay.util.thirdParty.bean.KikbagBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/WalletUtil")
public class WalletUtilService {
	private static final Logger logger = Logger.getLogger(WalletUtilService.class.getName());
	CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	ReportDao reportDao=new ReportDaoImpl();
	
	/**
	 * getCountry method return the list of Active wallet group country
	 * @return
	 */
	
	@POST
	@Path("/getCountry")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCountry(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getCountry()" );
		
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getCountry());
		
	}
	
	
	
	
	/**
	 * getCountry method return the list of Active wallet group country
	 * @return
	 */
	
	@POST
	@Path("/getCountryCode")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCountryCode(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getCountryCode()" );
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getCountryCode());
		
	}
	
	
	@POST
	@Path("/getcurrencyByCountryId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getcurrencyByCountryId(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getcurrencyByCountryId()"+walletBean.getCountryId() );
		
		// logger.info("Start excution ===================== method getcurrencyByCountryId(countryId)"+walletBean.getCountryId()); 
		 return commanUtilDao.getcurrencyByCountryId(walletBean.getCountryId());
	 }
		
	@POST
	@Path("/getcurrencyByCountryCode")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getcurrencyByCountryCode(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getcurrencyByCountryCode()"+walletBean.getCountryId() );
		
		// logger.info("Start excution ===================== method getcurrencyByCountryId(countryId)"+walletBean.getCountryCode()); 
		 return commanUtilDao.getcurrencyByCountryCode(walletBean.getCountryCode());
	 }
	
	
	@POST
	@Path("/getRechargeOperator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public String getRechargeOperator(){
		
		System.out.println("____________________Inside WalletutilService.java_______________method getRechargeOperator()________________");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRechargeOperator()" );
		
		//logger.info("Start excution ===================== method getRechargeOperator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getRechargeOperator());
		
	}
	
	
	
	@POST
	@Path("/getBillerDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public String getBillerDetails(RechargeTxnBean rechargeTxnBean){
		
		System.out.println("____________________Inside WalletutilService.java_______________method getRechargeOperator()________________");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRechargeOperator()" );
		
		//logger.info("Start excution ===================== method getRechargeOperator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return commanUtilDao.getBillerDetails(rechargeTxnBean);
		
	}
	
	@POST
	@Path("/getPostPaidBillerDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public String getPostPaidBillerDetails(RechargeTxnBean rechargeTxnBean){
		
		System.out.println("____________________Inside WalletutilService.java_______________method getRechargeOperator()________________");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRechargeOperator()" );
		
		//logger.info("Start excution ===================== method getRechargeOperator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return commanUtilDao.getPostPaidBillerDetails(rechargeTxnBean);
		
	}
	
	
	@POST
	@Path("/getInsuranceBillerDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public String getInsuranceBillerDetails(RechargeTxnBean rechargeTxnBean){
		
		System.out.println("____________________Inside WalletutilService.java_______________method getRechargeOperator()________________");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRechargeOperator()" );
		
		//logger.info("Start excution ===================== method getRechargeOperator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return commanUtilDao.getInsuranceBillerDetails(rechargeTxnBean);
		
	}

	@POST
	@Path("/getGasBillerDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public String getGasBillerDetails(RechargeTxnBean rechargeTxnBean){
		
		System.out.println("____________________Inside WalletutilService.java_______________method getRechargeOperator()________________");
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRechargeOperator()" );
		
		//logger.info("Start excution ===================== method getRechargeOperator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return commanUtilDao.getGasBillerDetails(rechargeTxnBean);
		
	}

	
	/*
	@POST
	@Path("/getWalletConfig")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletConfiguration getWalletConfig(WalletBean walletBean){
		logger.info("Start excution ===================== method getWalletConfig(walletBean)");
	    return commanUtilDao.getWalletConfiguration(walletBean.getCountryId());
		
	}
	*/
	
/*
	@POST
	@Path("/getWalletConfig")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletConfiguration getWalletConfig(){
		logger.info("Start excution ===================== method getWalletConfig(walletBean)");
	    return commanUtilDao.getWalletConfiguration("0");
	}*/
	
	 
	 @POST
	 @Path("/getSMSConfiguration")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public SMSConfigMast getSMSConfiguration(WalletBean walletBean){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getSMSConfiguration()" );
			
			//logger.info("Start excution ===================== method getSMSConfiguration(walletBean)");
		    return commanUtilDao.getSMSConfiguration(walletBean.getAggreatorid());
	 }
	 
	
	 @POST
	 @Path("/saveSMSConfiguration")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public SMSConfigMast saveSMSConfiguration(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,SMSConfigMast smsConfigMast){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),smsConfigMast.getAggreatorid(),"","", "", "","saveSMSConfiguration()" );
			
		// logger.info("Start excution ===================== method saveSMSConfiguration(smsConfigMast)");
		smsConfigMast.setIpiemi(ipiemi);
		smsConfigMast.setAgent(agent);
	    return commanUtilDao.saveSMSConfiguration(smsConfigMast);
	 }
	 
	 
	 @POST
	 @Path("/getMailConfiguration")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public MailConfigMast getMailConfiguration(WalletBean walletBean){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getMailConfiguration()" );
			
			//logger.info("Start excution ===================== method getMailConfiguration(walletBean)");
		    return commanUtilDao.getMailConfiguration(walletBean.getAggreatorid());
	 }
		
	 
	 @POST
	 @Path("/saveMailConfiguration")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public MailConfigMast saveMailConfiguration(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MailConfigMast mailConfigMast){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mailConfigMast.getAggreatorid(),"","", "", "","saveMailConfiguration()" );
			
		// logger.info("Start excution ===================== method saveMailConfiguration(mailConfigMast)");
		mailConfigMast.setIpiemi(ipiemi);
		mailConfigMast.setAgent(agent);
	    return commanUtilDao.saveMailConfiguration(mailConfigMast);
	 }
		
		
	@POST
	@Path("/getWalletConfig")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public WalletConfiguration getWalletConfig(WalletBean walletBean){
		
		System.out.println("------Inside WalletUtilService.java---------- getWalletConfig(WalletBean walletBean------");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getWalletConfig()" );
		
		System.out.println("-----walletBean.getAggreatorid()"+walletBean.getAggreatorid());
		System.out.println("-----walletBean.getWalletId()"+walletBean.getWalletid());
		System.out.println("-----walletBean.getuserId()"+walletBean.getUserId());

		WalletConfiguration walletConfiguration= commanUtilDao.getWalletConfiguration(walletBean.getAggreatorid());
		
		//logger.info("Start excution ===================== method getWalletConfig(walletBean)");
		
		walletConfiguration.setAgent("");
		//walletConfiguration.setDomainName("");
		walletConfiguration.setGcmAppKey("");
		walletConfiguration.setIpiemi("");
		walletConfiguration.setPgAgId("");
		walletConfiguration.setPgCallBackUrl("");
		walletConfiguration.setPgEncKey("");
		walletConfiguration.setPgMid("");
		walletConfiguration.setPgUrl("");
		
		System.out.println("Agent____________"+walletConfiguration.getAgent());
		System.out.println("GcmAppKey____________"+walletConfiguration.getGcmAppKey());
		System.out.println("Ipiemi____________"+walletConfiguration.getIpiemi());
		System.out.println("pgAgId____________"+walletConfiguration.getPgAgId());
		System.out.println("pgcallbackUrl____________"+walletConfiguration.getPgCallBackUrl());
		System.out.println("pgEncKey____________"+walletConfiguration.getPgEncKey());
		System.out.println("pgMid____________"+walletConfiguration.getPgMid());
		System.out.println("pgUrl____________"+walletConfiguration.getPgUrl());



	    return walletConfiguration;  
	}
	
	
	
	
	@POST
	@Path("/getAepsUpStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	
	public WalletConfiguration getAepsUpStatus(WalletBean walletBean){
		
		System.out.println("------Inside WalletUtilService.java---------- getAepsUpStatus(WalletBean walletBean------");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getWalletConfig()" );
		System.out.println("-----walletBean.getAggreatorid()"+walletBean.getAggreatorid());
		WalletConfiguration walletConfiguration= commanUtilDao.getAepsUpStatus(walletBean.getAggreatorid());
		
		WalletConfiguration json = new WalletConfiguration();
			json.setFinoAeps(walletConfiguration.getFinoAeps()!=null ?  walletConfiguration.getFinoAeps() :"1");
			json.setYesAeps(walletConfiguration.getYesAeps()!=null ?  walletConfiguration.getYesAeps() :"1");
			json.setIciciAeps(walletConfiguration.getIciciAeps()!=null ?  walletConfiguration.getIciciAeps():"1");
			
			return json;
	}
	
	
	@POST
	@Path("/getUserDetailsById")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletMastBean getUserDetailsById(WalletMastBean walletConfig){
		
		return commanUtilDao.getUserDetailsById(walletConfig);
	}
	
	@POST
	@Path("/getWalletConfigPG")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletConfiguration getWalletConfigPG(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getWalletConfigPG()" );
		
		//logger.info("Start excution ===================== method getWalletConfig(walletBean)");
	    return commanUtilDao.getWalletConfiguration(walletBean.getAggreatorid());
	}
	
	
	
	@POST
	@Path("/getAggIdByDomain")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAggIdByDomain(WalletBean walletBean){
		
		System.out.println("-----Inside WalletUtilService.java ------- method ----getAggIdByDomain(WalletBean walletBean)---");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getAggIdByDomain()" );
		
		//logger.info("Start excution ===================== method getAggIdByDomain(walletBean)");
	    return commanUtilDao.getAggIdByDomain(walletBean.getDomainName());
	}
	
	
	@POST
	@Path("/saveWalletConfiguration")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletConfiguration saveWalletConfiguration(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,WalletConfiguration walletConfiguration){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletConfiguration.getAggreatorid(),"","", "", "","getWalletConfigPG()" );
		
		//logger.info("Start excution ===================== method saveWalletConfiguration(walletConfiguration)");
		walletConfiguration.setIpiemi(ipiemi);
		walletConfiguration.setAgent(agent);
	    return commanUtilDao.saveWalletConfiguration(walletConfiguration);
	}
	
	
	@POST
	@Path("/getKyc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getKyc(){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getKyc()" );
			
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getKyc());
		
	}
	

	
		
		@POST
		@Path("/getKycType")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public String getKycType(){
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getKyc()" );
				
			//logger.info("Start excution ===================== method getCountry()");
			GsonBuilder builder = new GsonBuilder();
		    Gson gson = builder.create();
		    return gson.toJson(commanUtilDao.getKycType());
			
		}	
		
	@POST
	@Path("/getIdProofKyc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getIdProofKyc(){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getIdProofKyc()" );
			
		 //logger.info("Start excution ===================== method getIdProofKyc()");
			GsonBuilder builder = new GsonBuilder();
		    Gson gson = builder.create();
		    return gson.toJson(commanUtilDao.getIdProofKyc()); 
	 }
	
	
	
	
	@POST
	@Path("/getVersionControl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public VersionControl getVersionControl(VersionControl versionControl ){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getVersionControl()" );
			
		//logger.info("Start excution ===================== method getVersionControl(versionControl)");
		 return commanUtilDao.getVersionControl(versionControl.getAggreatorId(),versionControl.getVersionId());
	 }
	
	@POST
	@Path("/getMenu")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getMenu(){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getMenu()" );
			
		//logger.info("Start excution ==========*********************=========== method getMenu()"); 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getMenu());
	 }
	
	
	@POST
	@Path("/getSubAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getSubAggreator(WalletBean walletBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getSubAggreator()" );
		
		// logger.info("Start excution ===================== method getSubAggreator()"+walletBean.getAggreatorid()); 
			GsonBuilder builder = new GsonBuilder();
		    Gson gson = builder.create();
		    return gson.toJson(commanUtilDao.getSubAggreator(walletBean.getAggreatorid())); 
	 }
	
	@POST
	@Path("/saveUserMenuMapping")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserMenuMapping saveUserMenuMapping(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,UserMenuMapping userMenuMapping){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userMenuMapping.getUserId(), "", "","saveUserMenuMapping()" );
		
		//logger.info("Start excution ===================== method getSubAggreator()"+userMenuMapping.getUserId()); 
		 userMenuMapping.setIpiemi(ipiemi);
		 userMenuMapping.setAgent(agent);
		 return commanUtilDao.saveUserMenuMapping(userMenuMapping);
	}
	
	@POST
	@Path("/getUserMenuMapping")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserMenuMapping getUserMenuMapping(UserMenuMapping userMenuMapping){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUserMenuMapping()" +userMenuMapping.getUserId());
		 //logger.info("Start excution ===================== method getSubAggreator()"+userMenuMapping.getUserId()); 
	   return commanUtilDao.getUserMenuMapping(userMenuMapping.getUserId());
		}
	
	@POST
	@Path("/getCouponsCategory")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCouponsCategory() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getCouponsCategory()" );
		// logger.info("Start excution ===================== method getCouponsCategory()"); 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(new CouponsImplementation().getCouponsCategory());
		
	}
	
	@POST
	@Path("/getCouponsByCategory")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCouponsByCategory(CouponsBean couponsBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",couponsBean.getUserId(), "", "","getCouponsByCategory()"+couponsBean.getCategory() );
		// logger.info("Start excution ===================== method getCouponsCategory()"+couponsBean.getCategory()); 
		 GsonBuilder builder = new GsonBuilder();
		 Gson gson = builder.create();
		 return gson.toJson(new CouponsImplementation().getCouponsByCategory(couponsBean.getCategory()));
	}
	
	
	@POST
	@Path("/mailCoupan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String mailCoupan(CouponsBean couponsBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",couponsBean.getUserId(), "", "","mailCoupan()" );
		// logger.info("Start excution ===================== method mailCoupan()"+couponsBean.getIds()); 
		 return new CouponsImplementation().mailCoupan(couponsBean.getUserId(),couponsBean.getIds(),couponsBean.getRechargeMobile(),couponsBean.getRechargeAmount());
	}
	
		
	@POST
	@Path("/getCoupons")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCoupons(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getCoupons()" );
		// logger.info("Start excution ===================== method getCoupons()");
		 return new CouponsImplementation().getCoupons();
	}
	
	
	@POST
	@Path("/getCityCategory")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCityCategory(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getCityCategory()" );
		// logger.info("Start excution ===================== method getCityCategory()");
		 return new KikbakImplementation().getCityCategory();
	}
	
	
	@POST
	@Path("/getOfflineStores")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getOfflineStores(KikbagBean kikbagBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getOfflineStores()"+kikbagBean.getCat_name()+"getCity "+kikbagBean.getCity() );
		// logger.info("Start excution ==========Cat_name=========== method getOfflineStores()"+kikbagBean.getCat_name());
		// logger.info("Start excution ==========getCity=========== method getOfflineStores()"+kikbagBean.getCity());
		 return new KikbakImplementation().getOfflineStores(kikbagBean);
	}
	
	
	@POST
	@Path("/getretailAddress")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getretailAddress(KikbagBean kikbagBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getretailAddress()"+kikbagBean.getRetail_id() );
		// logger.info("Start excution ==========Retail_id=========== method getOfflineStores()"+kikbagBean.getRetail_id());
		 return new KikbakImplementation().getretailAddress(kikbagBean);
	}
	
	@POST
	@Path("/uploadMerchantOffer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String uploadMerchantOffer(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","uploadMerchantOffer()" );
		
		//logger.info("Start excution ******************************************** method uploadMerchantOffer()"+walletBean.getOfferList());
		 Gson gson=new Gson();
		List<MerchantOffersMast> offList= gson.fromJson(walletBean.getOfferList(), OfferListImpl.class);
		 return commanUtilDao.uploadMerchantOffer(walletBean.getAggreatorid(),offList);	
	}
	
	@POST
	@Path("/getMerchantOffer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getMerchantOffer(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getMerchantOffer()" );
		
		//logger.info("Start excution ===================== method getCouponsCategory()"+walletBean.getAggreatorid()); 
		 GsonBuilder builder = new GsonBuilder();
		 Gson gson = builder.create();
		 return gson.toJson(commanUtilDao.getMerchantOffer(walletBean.getAggreatorid()));
	}
	
	
	
	@POST
	@Path("/mailCoupanios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String mailCoupanios(CouponsBean couponsBean){
		JSONObject json=new JSONObject();
		try {
		json.put("status", "1001");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",couponsBean.getUserId(), "", "","mailCoupanios()" +couponsBean.getIds());
		
		// logger.info("Start excution ==============mailCoupanios======= method mailCoupanios()"+couponsBean.getIds());
		 json.put("status", new CouponsImplementation().mailCoupan(couponsBean.getUserId(),couponsBean.getIds(),couponsBean.getRechargeMobile(),couponsBean.getRechargeAmount()));
		} catch (Exception e) {
		e.printStackTrace();
		}
		return json.toString();
		
	}

	
	
	@POST
	@Path("/getRole")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getRole(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getRole()" );
		
		//logger.info("Start excution ==========*********************=========== method getRole()"); 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getRole());
	 }
	
	
	@POST
	@Path("/getOperatorCircle")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getOperatorCircle(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getOperatorCircle()" +walletBean.getNumberSeries());
		
		// logger.info("Start excution ===================== method getOperatorCircle(NumberSeries)"+walletBean.getNumberSeries()); 
		 return commanUtilDao.getOperatorCircle(walletBean.getNumberSeries());
	 }
	
	
	
	@POST
	@Path("/getOperatorCircleios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getOperatorCircleios(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getOperatorCircleios()"+walletBean.getNumberSeries() );
		
		// logger.info("Start excution ===================== method getOperatorCircle(NumberSeries)"+walletBean.getNumberSeries()); 
		 
		 JSONObject json=new JSONObject();
			try {
			json.put("OperatorCircle",commanUtilDao.getOperatorCircle(walletBean.getNumberSeries()));
			} catch (Exception e) {
			e.printStackTrace();
			}
			return json.toString(); 

	 }
	
	
	@POST
	@Path("/getPlans")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public TariffPlans getPlans(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getPlans()"+walletBean.getOperatorName() +walletBean.getCircleName());
		
		// logger.info("Start excution ===================== method getPlans(walletBean)"+walletBean.getOperatorName()); 
		// logger.info("Start excution ===================== method getPlans(walletBean)"+walletBean.getCircleName());
		 return commanUtilDao.getPlans(walletBean.getOperatorName(), walletBean.getCircleName());
		 
	 }
	
	
	
	
	
	@POST
	@Path("/saveUserRoleMapping")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserRoleMapping saveUserRoleMapping(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,UserRoleMapping userRoleMapping){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","saveUserRoleMapping()" +userRoleMapping.getUserId());

		//logger.info("Start excution ===================== method getSubAggreator()"+userRoleMapping.getUserId()); 
		 userRoleMapping.setIpiemi(ipiemi);
		 userRoleMapping.setAgent(agent);
		 return commanUtilDao.saveUserRoleMapping(userRoleMapping);
	}
	
	
	
	
	@POST
	@Path("/getUserRoleMapping")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserRoleMapping getUserRoleMapping(UserRoleMapping userRoleMapping){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUserRoleMapping()" +userRoleMapping.getUserId());

		//logger.info("Start excution ===================== method getSubAggreator()"+userRoleMapping.getUserId()); 
	   return commanUtilDao.getUserRoleMapping(userRoleMapping.getUserId());
		}
	
	
	
	@POST
	@Path("/validatePan")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String validatePan(WalletBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","validatePan()"+walletBean.getPan() );
 
		//logger.info("Start excution ===================== method validatePan(walletBean)"+walletBean.getPan()); 
		 return ""+commanUtilDao.validatePan(walletBean.getPan(),walletBean.getAggreatorid(),walletBean.getUserType());
	 }
	
	

@POST
@Path("/getAgentDtlsByDistId")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String getAgentDtlsByDistId(WalletBean walletBean,@Context HttpServletRequest request){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getAgentDtlsByDistId()" );
	
	//logger.info("Start excution ===================== method getAgentDtlsByDistId()");
	String serverName=request.getServletContext().getInitParameter("serverName");
	GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    return gson.toJson(commanUtilDao.getAgentDtlsByDistId(walletBean.getAggreatorid(),walletBean.getDistributerId(),serverName));
	
}

@POST
@Path("/getAgentDtlsBySupDistId")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public String getAgentDtlsBySupDistId(WalletBean walletBean,@Context HttpServletRequest request){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getAgentDtlsBySupDistId()" );
	
	//logger.info("Start excution ===================== method getAgentDtlsByDistId()");
	String serverName=request.getServletContext().getInitParameter("serverName");
	GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    return gson.toJson(commanUtilDao.getAgentDtlsBySupDistId(walletBean.getAggreatorid(),walletBean.getSuperdistributorid(),walletBean.getDistributerId(),serverName));
	
}



	@POST
	@Path("/getPartnerList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getPartnerList(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getPartnerList()" );

		//logger.info("Start excution ==========*********************=========== method getPartnerList()"); 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getPartnerList());
	 }
	
	
	@POST
	@Path("/getTxnSourceList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getTxnSourceList(){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getTxnSourceList()" );

		//logger.info("Start excution ==========*********************=========== method getPartnerList()"); 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getTxnSourceList());
	 }

 

	@POST
	@Path("/savePartnerPrefund")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public PartnerPrefundMastBean savePartnerPrefund(PartnerPrefundMastBean partnerPrefundMastBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","savePartnerPrefund()"+partnerPrefundMastBean.getAmount() );

		// logger.info("Start excution ===================== method getSubAggreator()"+partnerPrefundMastBean.getAmount()); 
	   return commanUtilDao.savePartnerPrefund(partnerPrefundMastBean);
		}
	

	@POST
	@Path("/InstallationDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public InstallationDetails saveInstallationDetails(InstallationDetails installationDetails){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","saveInstallationDetails()" +installationDetails.getImei());

		//logger.info("Start excution ===================== method saveInstallationDetails()"+installationDetails.getImei()); 
		 return commanUtilDao.saveInstallationDetails(installationDetails);
	 }
	
	
	@POST
	@Path("/savePaymentGatewayURL")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public PaymentGatewayURL savePaymentGatewayURL(PaymentGatewayURL paymentGatewayURL){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","savePaymentGatewayURL()" +paymentGatewayURL.getTxnId());

		//logger.info("Start excution ===================== method savePaymentGatewayURL()"+paymentGatewayURL.getTxnId()); 
		 return commanUtilDao.savePaymentGatewayURL(paymentGatewayURL);
	}
	
	 @POST
	 @Path("/getPublicIP")
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 public String getPublicIP()
	 {
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getPublicIP()" );

		// logger.info("Start excution ===================== method sgetPublicIP()"); 
	  return commanUtilDao.getPublicIP();
			  
	 }
	 
	 
	@POST
	@Path("/validateEmail")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String validateEmail(String email){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","validatePan()"+email );
 
		 return ""+commanUtilDao.validateEmail(email);
	 }
	 
	@POST
	@Path("/validateMobile")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String validateMobile(String mobile){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","validatePan()"+mobile );
 
		 return ""+commanUtilDao.validateMobile(mobile);
	 }
	
	
	@POST
	@Path("/validateAadhaar")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String validateAadhaar(WalletMastBean walletBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","validatePan()"  );
 
		 return ""+commanUtilDao.validateAadhaar(walletBean.getAdhar(),walletBean.getAggreatorid(),walletBean.getUsertype());
	 }
	
	 
	 
//	 @POST
//	 @Consumes(MediaType.APPLICATION_JSON)
//	 @Produces(MediaType.APPLICATION_JSON)
//	 @Path("/changeServiceStatus")
//	 public String changeServiceStatus(String statusRequest){
//		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","changeServiceStatus()" );
//		int ct = 0;
//		try {
//			Gson g = new Gson();
//			ServiceMasterRequestBean serviceMasterBean = g.fromJson(statusRequest, ServiceMasterRequestBean.class);
//			
//			if(serviceMasterBean.getAggreatorid().equalsIgnoreCase("ALL")) {
//				Map<String, String> aggrgatorMap = reportDao.getSettledAggreator();
//				for (Map.Entry<String, String> entry : aggrgatorMap.entrySet()) {
//					serviceMasterBean.setAggreatorid(entry.getValue());
//					ct += totalUpdatedServiceMasterStatus(serviceMasterBean);
//				}
//			}else {
//				ct = totalUpdatedServiceMasterStatus(serviceMasterBean);
//			}
//		}catch (Exception e) {
//			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","changeServiceStatus()| Exception "+e.getMessage() );
//		}
//		return ""+ct+" Aggregator Service has been changed.";
//	 }
	 
//	 private int totalUpdatedServiceMasterStatus(ServiceMasterRequestBean serviceMasterBean) {
//		 
//			String service[] = serviceMasterBean.getServiceType().split("-");
//			if(service.length==2) {
//					serviceMasterBean.setServicesname(service[0]);
//					serviceMasterBean.setType(service[1]);
//			}else {
//				serviceMasterBean.setServicesname(serviceMasterBean.getServiceType());
//				serviceMasterBean.setType("FULL");
//			}
//			return commanUtilDao.changeServiceStatus(serviceMasterBean);
//	 }
	 
	 
//	 @POST
//	 @Consumes(MediaType.APPLICATION_JSON)
//	 @Produces(MediaType.APPLICATION_JSON)
//	 @Path("/getServiceStatus")
//	 public String getServiceStatus(String serviceRequest){
//		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()" );
//		
//		String strResponse = null;
//		
//		try {
//			Gson g = new Gson();
//			ServiceMasterRequestBean serviceMasterBean = g.fromJson(serviceRequest, ServiceMasterRequestBean.class);
//			
//			serviceMasterBean.setStatus(null);
//			String service[] = serviceMasterBean.getServiceType().split("-");
//			if(service.length==2) {
//					serviceMasterBean.setServicesname(service[0]);
//					serviceMasterBean.setType(service[1]);
//			}else {
//				serviceMasterBean.setServicesname(serviceMasterBean.getServiceType());
//				serviceMasterBean.setType("FULL");
//			}
//			
//			ServiceMasterRequestBean newServiceBean = new ServiceMasterRequestBean();
//			newServiceBean = commanUtilDao.getServiceStatusByAggId(serviceMasterBean);
//			
//			Gson gson = new Gson();
//			strResponse = gson.toJson(newServiceBean);
//			
//		}catch (Exception e) {
//			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getServiceStatus()| Exception "+e.getMessage() );
//		}
//		return strResponse;
//	 }


	@POST
	@Path("/markPending")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<RechargeTxnBean> markPending(String  str){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","markPending()"  );
 
		 return commanUtilDao.markPending(str);
	 }
	

	@POST
	@Path("/markDmtPending")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<MudraMoneyTransactionBean> markDmtPending(String  str){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","markPending()"  );
 
		 return commanUtilDao.markDmtPending(str);
	 }
	
	
	@POST
	@Path("/getAgentDtlsBySo")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentDtlsBySo(WalletBean walletBean,@Context HttpServletRequest request){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getAgentDtlsBySupDistId()" );
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getAgentDtlsBySo(walletBean.getAggreatorid(),walletBean.getUserId(),walletBean.getDistributerId(),serverName));
		
	}
	
	@POST
	@Path("/getAgentDtlsBySoList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentDtlsBySoList(WalletBean walletBean,@Context HttpServletRequest request){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),walletBean.getUserId(), "", "","getAgentDtlsBySupDistId()" );
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getAgentDtlsBySoList(walletBean.getAggreatorid(),walletBean.getUserId(),walletBean.getDistributerId(),serverName,walletBean.getSuperdistributorid()));
		
	}
	
	
	@POST
	@Path("/enablePg")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<WalletMastBean> enablePg(RechargeTxnBean str){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","markPending()"  );
 
		 return commanUtilDao.enablePg(str);
	 }
	
}




class OfferListImpl extends ArrayList<MerchantOffersMast>{
	
}
