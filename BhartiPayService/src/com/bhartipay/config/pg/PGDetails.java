package com.bhartipay.config.pg;

public class PGDetails {

	
	String custName = "PowerPay ";
	String custAddress = "Bombay";
	String custCity = "Bombay";
	String custState = "Uttar Pradesh";
	String custPinCode = "201301";
	String custCountry = "IN";
	String custPhoneNo1 = "01206900771";
	String custPhoneNo2 = "01246900772";
	String custPhoneNo3 = "01246900771";
	String custMobileNo = "9335235731";
	String custEmailId = "abc@PowerPay.com";
	String otherNotes = "PowerPay";
	
	
	/**
	 * getBillingDtls is use fro getting the billing details for 
	 * hitting the PG api
	 * @return return billing details as string
	 */
		public String getBillingDtls() {

			String billingDtls = custName + "|" + custAddress + "|" + custCity
					+ "|" + custState + "|" + custPinCode + "|" + custCountry + "|"
					+ custPhoneNo1 + "|" + custPhoneNo2 + "|" + custPhoneNo3 + "|"
					+ custMobileNo + "|" + custEmailId + "|" + otherNotes;

			return billingDtls;
		}
	
	
		
		String deliveryName = "PowerPay";
		String deliveryAddress = "Bombay";
		String deliveryCity = "Bombay";
		String deliveryState = "Uttar Pradesh";
		String deliveryPinCode = "201301";
		String deliveryCountry = "IN";
		String deliveryPhNo1 = "01246330881";
		String deliveryPhNo2 = "01246900881";
		String deliveryPhNo3 = "01246900882";
		String deliveryMobileNo = "9335235731";
	/**
	 * getShippingDtls is use for getting the shipping details for hitting the
	 * PG api
	 * @return return shipping details as string
	 */
		public String getShippingDtls() {
			String shippingDtls = deliveryName + "|" + deliveryAddress + "|"
					+ deliveryCity + "|" + deliveryState + "|" + deliveryPinCode
					+ "|" + deliveryCountry + "|" + deliveryPhNo1 + "|"
					+ deliveryPhNo2 + "|" + deliveryPhNo3 + "|" + deliveryMobileNo;
			return shippingDtls;
		}
	
	
	
	
}
