package com.bhartipay.merchant.persistence;

import java.util.List;

import com.bhartipay.merchant.bean.MerchantMastBean;
import com.bhartipay.merchant.bean.MerchantTxnMast;

public interface MerchantDao {
	public MerchantMastBean saveMerchant(MerchantMastBean merchantMastBean);
	public MerchantTxnMast validateSaveTxn(MerchantTxnMast merchantTxnMast);
	public MerchantTxnMast validateSaveTxnReq(MerchantTxnMast merchantTxnMast);
	public MerchantTxnMast merchentPaymetConfirm(String checksum,String otp,String userId,String walletId,double amount,String mid,String txnId,String ipImei,String aggreatorid,String agent);
	public String merchentPaymet(String otp,String userId,String walletId,double amount,String mid,String txnId,String ipImei,String aggreatorid,String agent);
	public List<MerchantMastBean>  getMerchantList(MerchantMastBean merchantMastBean);
	public MerchantMastBean getMerchantKey(MerchantMastBean merchantMastBean);

}
