package com.bhartipay.merchant.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.merchant.bean.MerchantMastBean;
import com.bhartipay.merchant.bean.MerchantTxnMast;
import com.bhartipay.mudra.bean.MudraBeneficiaryMastBean;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.SmsTemplates;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class MerchantDaoImpl implements MerchantDao{
	
	private static final Logger logger = Logger.getLogger(MerchantDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	TransactionDao transactionDao=new TransactionDaoImpl();
	
	
/**
 * saveMerchant is used for create the new Merchant
 */
	
public MerchantMastBean saveMerchant(MerchantMastBean merchantMastBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|saveMerchant()"+merchantMastBean.getMerchantType()
			);
	//logger.info("Start excution ********************************************************* method saveMerchant(merchantMastBean)"+merchantMastBean.getMerchantType());
	merchantMastBean.setStatusCode("1001");
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	try {
		transaction =session.beginTransaction();
	
	if(merchantMastBean.getMerchantName()==null || merchantMastBean.getMerchantName().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantName cant be empt"+merchantMastBean.getMerchantName()
				);
		//logger.info("***********************************getMerchantName cant be empty********************* "+merchantMastBean.getMerchantName());
		merchantMastBean.setStatusCode("8501");
		return merchantMastBean;
		
	}
	
	if(merchantMastBean.getAggreatorid()==null || merchantMastBean.getAggreatorid().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getAggreatorid cant be empty"
				);
		//logger.info("***********************************getAggreatorid cant be empty********************* "+merchantMastBean.getAggreatorid());
		merchantMastBean.setStatusCode("8502");
		return merchantMastBean;
		
	}
		
	if(merchantMastBean.getMerchantMobile()==null || merchantMastBean.getMerchantMobile().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantMobile cant be empty"+merchantMastBean.getMerchantMobile()
				);
		//logger.info("***********************************getMerchantMobile cant be empty********************* "+merchantMastBean.getMerchantMobile());
		merchantMastBean.setStatusCode("8503");
		return merchantMastBean;
	}
	
	if(merchantMastBean.getMerchantEmail()==null || merchantMastBean.getMerchantEmail().isEmpty()){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantemail cant be empty"+merchantMastBean.getMerchantEmail()
				);
		//logger.info("***********************************getMerchantemail cant be empty********************* "+merchantMastBean.getMerchantEmail());
		merchantMastBean.setStatusCode("8504");
		return merchantMastBean;
	}
	
	Criteria merchantCriteria= session.createCriteria(MerchantMastBean.class);
	merchantCriteria.add(Restrictions.eq("merchantUser", merchantMastBean.getMerchantUser()));
	List <MerchantMastBean>merchantList=merchantCriteria.list();
	if(merchantList.size()>0){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|Duplicate userid"+merchantList.get(0).getMerchantUser()
				);
		//logger.info("***********************************Duplicate userid********************* "+merchantList.get(0).getMerchantUser());
		merchantMastBean.setStatusCode("8505");
		return merchantMastBean;
	}	
	
	
	String	merchantId = commanUtilDao.getTrxId("MERCHANT",merchantMastBean.getAggreatorid());
	if(merchantMastBean.getMerchantType().equalsIgnoreCase("DMT")){
		WalletMastBean walletMastBean=new WalletMastBean();
		walletMastBean.setId(merchantId);
		walletMastBean.setAggreatorid(merchantMastBean.getAggreatorid());
		walletMastBean.setDistributerid("-1");
		walletMastBean.setAgentid("-1");
		walletMastBean.setSubAgentId("-1");
		walletMastBean.setCreatedby(merchantMastBean.getCreateBy());
		walletMastBean.setMobileno(merchantMastBean.getMerchantMobile());
		walletMastBean.setEmailid(merchantMastBean.getMerchantEmail());
		walletMastBean.setName(merchantMastBean.getMerchantName());
		walletMastBean.setPassword(merchantMastBean.getMerchantPassword());
		walletMastBean.setUsertype(2);
		walletMastBean.setAgent(merchantMastBean.getAgent());
		walletMastBean.setIpiemi(merchantMastBean.getIpiemi());
		String resCode=new WalletUserDaoImpl().signUpUserDMT( walletMastBean);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|resCode"+resCode
				);
		//logger.info("***********************************resCode********************* "+resCode);
		if(!resCode.equalsIgnoreCase("1000")){
			merchantMastBean.setStatusCode(resCode);
			return merchantMastBean;
		}
	
	}
	
	String  key=EncryptionDecryption.getKey();	
	merchantMastBean.setMerchantID(merchantId);
	merchantMastBean.setEncryptionKey(key);
	merchantMastBean.setMerchantPassword(CommanUtil.SHAHashing256(merchantMastBean.getMerchantPassword()));
	merchantMastBean.setValidateBy("NOTP");
	
	
	session.save(merchantMastBean);
	transaction.commit();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|merchant Save with Id"+merchantId
			);
	//logger.info("***********************************merchant Save with Id********************* "+merchantId);
	
	merchantMastBean.setStatusCode("1000");
	
	
	} catch (Exception e) {
		merchantMastBean.setStatusCode("7000");
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|problem in saveMerchant"+e.getMessage()+" "+e
				);
		//logger.debug("problem in saveMerchant******************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|statusCode"+merchantMastBean.getStatusCode()
			);
	//logger.info("Return ***********************************statusCode********************* "+merchantMastBean.getStatusCode());
	return merchantMastBean;
	
}
	

public List<MerchantMastBean>  getMerchantList(MerchantMastBean merchantMastBean){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantList()"
			);
	
	//logger.info("Start excution ********************************************************* method getMerchantList(merchantMastBean)"+merchantMastBean.getAggreatorid());
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	List <MerchantMastBean> merchantList= new ArrayList<MerchantMastBean>();
	try {
		
		Criteria merchantCriteria= session.createCriteria(MerchantMastBean.class);
		merchantCriteria.add(Restrictions.eq("aggreatorid", merchantMastBean.getAggreatorid()));
		merchantCriteria.addOrder(Order.desc("merchantID"));
		merchantList=merchantCriteria.list();
	} catch (Exception e) {
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|problem in getMerchantList"+e.getMessage()+" "+e
				);
		//logger.debug("problem in getMerchantList******************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|Size "+merchantList.size()
			);
	//logger.info("merchantList********************************************************* Size :"+merchantList.size());
	return merchantList;
	
}




/**
 * 	
 */
public MerchantTxnMast validateSaveTxn(MerchantTxnMast merchantTxnMast){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|validateSaveTxn()"
			);
	
	merchantTxnMast.setStatusCode("1001");
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	Transaction transaction=null;
	MerchantMastBean merchantMast = new MerchantMastBean();
	try {
		transaction =session.beginTransaction();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|MID "+merchantTxnMast.getMid()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|TXN ID "+merchantTxnMast.getMerchantTxnId()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Amount "+merchantTxnMast.getTxnAmount()
				);
		
		if(merchantTxnMast.getMid()!=null && !(merchantTxnMast.getMid().isEmpty())){
			merchantMast.setMerchantID(merchantTxnMast.getMid());
			merchantMast.setAggreatorid(merchantTxnMast.getAggreatorId());
		MerchantMastBean merchantMastBean=getMerchantKey(merchantMast);//(MerchantMastBean)session.get(MerchantMastBean.class, merchantTxnMast.getMid());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|return merchantMastBean "+merchantMastBean
				);
		
		if(merchantMastBean==null){
			merchantTxnMast.setStatusCode("7033");
			merchantTxnMast.setStatusDesc("Invalied MID");
			return merchantTxnMast;
		}
		if(merchantMastBean.getMerchantID()==null){
			merchantTxnMast.setStatusCode("7033");
			merchantTxnMast.setStatusDesc("Invalied MID");
			return merchantTxnMast;
		}
	
		String mUserName=EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantUserName(),merchantMastBean.getEncryptionKey());
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|MobileNo "+merchantTxnMast.getWalletMobileNo()
				);
		//logger.info("**********************************************************"+merchantTxnMast.getWalletMobileNo());
		String walletUserMobile=EncryptionDecryption.getdecrypted(merchantTxnMast.getWalletMobileNo(),merchantMastBean.getEncryptionKey());
		String walletAggreatorId=merchantTxnMast.getAggreatorId();
		String aggreatorId=merchantTxnMast.getAggreatorId();
		WalletMastBean walletmast = new WalletUserDaoImpl().getUserByMobileNo(walletUserMobile,walletAggreatorId,"");
		WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorId);
		String mTxnId=EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantTxnId(),merchantMastBean.getEncryptionKey());
		String callBackURL=EncryptionDecryption.getdecrypted(merchantTxnMast.getCallBackURL(),merchantMastBean.getEncryptionKey());
		String other=EncryptionDecryption.getdecrypted(merchantTxnMast.getOthers(),merchantMastBean.getEncryptionKey());
		double amount=Double.parseDouble(EncryptionDecryption.getdecrypted(merchantTxnMast.getTxnAmount(),merchantMastBean.getEncryptionKey()));
		
		
		merchantTxnMast.setAggreatorId(aggreatorId);
		merchantTxnMast.setMerchantUserName(mUserName);
		merchantTxnMast.setAmount(amount);
		merchantTxnMast.setTxnStatus("Pending");
		merchantTxnMast.setCallBackURL(callBackURL);
		merchantTxnMast.setMerchantTxnId(mTxnId);
		merchantTxnMast.setOthers(other);
		merchantTxnMast.setWalletMobileNo(walletUserMobile);
		
		if(!new WalletUserDaoImpl().checkMobileNo(walletUserMobile,walletAggreatorId)){
			if(new WalletUserDaoImpl().checkActiveUser(walletUserMobile, walletAggreatorId))
			{
				merchantTxnMast.setStatusCode("1010");
				merchantTxnMast.setStatusDesc("Deactivated User.");
				return merchantTxnMast;
			}
			merchantTxnMast.setStatusCode("7011");
			merchantTxnMast.setStatusDesc("Mobile Number Not Register With Us");
			return merchantTxnMast;
			
		}
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   merchantMastBean.getMerchantUser()+"mUserName 11"+mUserName
				);
		//logger.info(merchantMastBean.getMerchantUser()+"**************mUserName***********11******************************="+mUserName);
		if(!merchantMastBean.getMerchantUser().equals(mUserName)){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Invalid mUserName 11"+mUserName
					);
			//logger.info("************==Invalid mUserName*********==11******************************="+mUserName);
			merchantTxnMast.setStatusCode("7034");
			merchantTxnMast.setStatusDesc("Invalied Merchent User Id");
			return merchantTxnMast;
		}
		
		if (merchantMastBean.getMerchantStatus()==null||!merchantMastBean.getMerchantStatus().equalsIgnoreCase("A")) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Invalid Mid 7034 "+merchantTxnMast.getMid()
					);
			//logger.info("***************Invalid Mid************7034*******************************"+merchantTxnMast.getMid());
			merchantTxnMast.setStatusCode("7034");
			merchantTxnMast.setStatusDesc("Invalied MID");
			return merchantTxnMast;
		}
		
		/*String aggreatorId=merchantTxnMast.getAggreatorId();
		WalletMastBean walletmast = new WalletUserDaoImpl().getUserByMobileNo(walletUserMobile,walletAggreatorId,"");
		WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorId);
		String mTxnId=EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantTxnId(),merchantMastBean.getEncryptionKey());
		String callBackURL=EncryptionDecryption.getdecrypted(merchantTxnMast.getCallBackURL(),merchantMastBean.getEncryptionKey());
		String other=EncryptionDecryption.getdecrypted(merchantTxnMast.getOthers(),merchantMastBean.getEncryptionKey());
		double amount=Double.parseDouble(EncryptionDecryption.getdecrypted(merchantTxnMast.getTxnAmount(),merchantMastBean.getEncryptionKey()));*/
		if(new TransactionDaoImpl().getWalletBalance(walletmast.getWalletid()) < amount){
			merchantTxnMast.setStatusCode("17024");
			merchantTxnMast.setStatusDesc("Insufficient Wallet Balance.");
			return merchantTxnMast;
			
		}
		merchantTxnMast.setMerchantName(merchantMastBean.getMerchantName());
		String	txnid = commanUtilDao.getTrxId("MERCHTXN",merchantMastBean.getAggreatorid());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| MERCHTXN "+txnid
				);
		//logger.info("**************MERCHTXN******************************************"+txnid);
		merchantTxnMast.setTxnId(txnid);
		session.save(merchantTxnMast);
		transaction.commit();
		merchantTxnMast.setStatusCode("1000");	
		merchantTxnMast.setStatusDesc(" Partial Txn Save.");
		if(merchantMastBean.getValidateBy().equals("OTP")){
			transaction=session.beginTransaction();
			String newOtp = null;
			OTPGeneration oTPGeneration = new OTPGeneration();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Inside account wallet send otp for validate mobile on " + walletUserMobile
					);
			//logger.info("Inside *********************account wallet****send otp for validate mobile on" + walletUserMobile);
			newOtp = oTPGeneration.generateOTP();
			if (!(newOtp == null)) {
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", walletUserMobile)
						.setParameter("aggreatorid", merchantTxnMast.getAggreatorId())
						.setParameter("otp",  CommanUtil.SHAHashing256(newOtp)/*newOtp*/);
				insertOtp.executeUpdate();
			}
			transaction.commit();
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Inside otp send on mobile" + walletUserMobile + "otp"+ newOtp
						);
				//logger.info("Inside ********************otp send on mobile**********" + walletUserMobile + "***otp***"+ newOtp);
				
				//One time password for your <<APPNAME>> wallet is <<OTP>>. For security reasons we advise you not to disclose your 
				 String smsMsg = commanUtilDao.getsmsTemplet("regOtp","").replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp);
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", walletUserMobile,smsMsg, "SMS","0",transactionDao.getWalletIdByUserId(walletUserMobile,merchantTxnMast.getAggreatorId()),transactionDao.getNameByUserId(walletUserMobile,merchantTxnMast.getAggreatorId()),"OTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			}
		}
		
		merchantTxnMast.setValidateBy(merchantMastBean.getValidateBy());
		
		return merchantTxnMast;
		}else{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Invalid Mid "
					);
			//logger.info("**************Invalid Mid*****************************************");
			merchantTxnMast.setStatusCode("1001");
			merchantTxnMast.setStatusDesc("MID is Null");
			return merchantTxnMast;
		}
		
		} catch (Exception e) {
		merchantTxnMast.setStatusCode("7000");
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|problem in saveMerchant "+e.getMessage()+" "+e
				);
		//logger.debug("problem in saveMerchant******************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	
	return merchantTxnMast;
	
	
}

public MerchantTxnMast validateSaveTxnReq(MerchantTxnMast merchantTxnMast){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| validateSaveTxnReq() "
			);

	//logger.info("Start excution ********************************************** method validateSaveTxnReq(merchantTxnMast)");
	merchantTxnMast.setStatusCode("1001");
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	MerchantTxnMast merchantTxnMastResp = new MerchantTxnMast();
	Transaction transaction=null;
	TreeMap<String,String> obj=new TreeMap<String,String>();
	obj.put("userId",merchantTxnMast.getUserId());
    obj.put("walletmobileno",merchantTxnMast.getWalletMobileNo());
    obj.put("txnAmount",""+Double.parseDouble(merchantTxnMast.getTxnAmount()));
    obj.put("ipiemi", merchantTxnMast.getIpiemi());
    obj.put("aggreatorId",merchantTxnMast.getAggreatorId());
    obj.put("mid",merchantTxnMast.getMid());
    obj.put("merchanttxnid",merchantTxnMast.getMerchantTxnId());
    obj.put("CHECKSUMHASH",merchantTxnMast.getCHECKSUMHASH());
    
  int statusCode= WalletSecurityUtility.checkSecurity(obj);
  if(statusCode!=1000){
	  merchantTxnMastResp.setStatusCode(""+statusCode);
	  merchantTxnMastResp.setStatusDesc("Failed");
		return merchantTxnMastResp;
  }
	MerchantMastBean merchantMast = new MerchantMastBean();
	try {
		transaction =session.beginTransaction();
				
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| MID"+merchantTxnMast.getMid()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| TXN ID"+merchantTxnMast.getMerchantTxnId()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| Amount"+merchantTxnMast.getTxnAmount()
				);
		/*logger.info("*********************MID*************************"+merchantTxnMast.getMid());
		logger.info("*********************TXN ID*************************"+merchantTxnMast.getMerchantTxnId());
		logger.info("***********************Amount***********************"+merchantTxnMast.getTxnAmount());
		logger.info("***********************Amount***********************"+merchantTxnMast.getAggreatorId());*/
		
		if(merchantTxnMast.getMid()!=null && !(merchantTxnMast.getMid().isEmpty())){
			merchantMast.setMerchantID(merchantTxnMast.getMid());
			merchantMast.setAggreatorid(merchantTxnMast.getAggreatorId());
		MerchantMastBean merchantMastBean=getMerchantKey(merchantMast);//(MerchantMastBean)session.get(MerchantMastBean.class, merchantTxnMast.getMid());
		merchantTxnMastResp.setValidateBy(merchantMastBean.getValidateBy());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| return merchantMastBean "+merchantMastBean
				);
		//logger.info("***********************return merchantMastBean***********************"+merchantMastBean);
		if(merchantMastBean==null){
			merchantTxnMastResp.setStatusCode("7033");
			merchantTxnMastResp.setStatusDesc("Invalied MID");
			return merchantTxnMastResp;
		}
		if(merchantMastBean.getMerchantID()==null){
			merchantTxnMastResp.setStatusCode("7033");
			merchantTxnMastResp.setStatusDesc("Invalied MID");
			return merchantTxnMastResp;
		}
	
		String mUserName=merchantTxnMast.getMerchantUserName();//EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantUserName(),merchantMastBean.getEncryptionKey());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| MobileNO "+merchantTxnMast.getWalletMobileNo()
				);
		//logger.info("**********************************************************"+merchantTxnMast.getWalletMobileNo());
		String walletUserMobile=merchantTxnMast.getWalletMobileNo();//EncryptionDecryption.getdecrypted(merchantTxnMast.getWalletMobileNo(),merchantMastBean.getEncryptionKey());
		String walletAggreatorId=merchantTxnMast.getAggreatorId();
		String aggreatorId=merchantTxnMast.getAggreatorId();
		WalletMastBean walletmast = new WalletUserDaoImpl().getUserByMobileNo(walletUserMobile,walletAggreatorId,"");
		WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorId);
		String mTxnId=merchantTxnMast.getMerchantTxnId();//EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantTxnId(),merchantMastBean.getEncryptionKey());
		////String callBackURL=merchantTxnMast.getCallBackURL();//EncryptionDecryption.getdecrypted(merchantTxnMast.getCallBackURL(),merchantMastBean.getEncryptionKey());
		String other="";
		if(merchantTxnMast.getOthers() != null && !merchantTxnMast.getOthers().trim().equals(""))
			other=merchantTxnMast.getOthers();//EncryptionDecryption.getdecrypted(merchantTxnMast.getOthers(),merchantMastBean.getEncryptionKey());
		double amount=Double.parseDouble(merchantTxnMast.getTxnAmount());//Double.parseDouble(EncryptionDecryption.getdecrypted(merchantTxnMast.getTxnAmount(),merchantMastBean.getEncryptionKey()));
		
		
		merchantTxnMast.setAggreatorId(aggreatorId);
		merchantTxnMast.setMerchantUserName(mUserName);
		merchantTxnMast.setAmount(amount);
		merchantTxnMast.setTxnStatus("Pending");
		merchantTxnMast.setCallBackURL("NA");
		merchantTxnMast.setMerchantTxnId(mTxnId);
		merchantTxnMast.setOthers(other);
		merchantTxnMast.setWalletMobileNo(walletUserMobile);
		
		if(!new WalletUserDaoImpl().checkMobileNo(walletUserMobile,walletAggreatorId)){
			if(new WalletUserDaoImpl().checkActiveUser(walletUserMobile, walletAggreatorId))
			{
				merchantTxnMastResp.setStatusCode("1010");
				merchantTxnMastResp.setStatusDesc("Deactivated User.");
				return merchantTxnMastResp;
			}
			merchantTxnMastResp.setStatusCode("7011");
			merchantTxnMastResp.setStatusDesc("Mobile Number Not Register With Us");
			return merchantTxnMastResp;
			
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Invalid mUserName 11 "+mUserName
				);
		//logger.info(merchantMastBean.getMerchantUser()+"**************mUserName***********11******************************="+mUserName);
		if(!merchantMastBean.getMerchantUser().equals(mUserName)){
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Invalid mUserName 11 "+mUserName
					);
			//logger.info("************==Invalid mUserName*********==11******************************="+mUserName);
			merchantTxnMastResp.setStatusCode("7034");
			merchantTxnMastResp.setStatusDesc("Invalid Merchent User Id");
			return merchantTxnMastResp;
		}
		
		if (merchantMastBean.getMerchantStatus()==null||!merchantMastBean.getMerchantStatus().equalsIgnoreCase("A")) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| Invalid Mid 7034 "+merchantTxnMast.getMid()
					);
			//logger.info("***************Invalid Mid************7034*******************************"+merchantTxnMast.getMid());
			merchantTxnMastResp.setStatusCode("7034");
			merchantTxnMastResp.setStatusDesc("Invalid MID");
			return merchantTxnMastResp;
		}
		
		/*String aggreatorId=merchantTxnMast.getAggreatorId();
		WalletMastBean walletmast = new WalletUserDaoImpl().getUserByMobileNo(walletUserMobile,walletAggreatorId,"");
		WalletConfiguration walletConfiguration = commanUtilDao.getWalletConfiguration(aggreatorId);
		String mTxnId=EncryptionDecryption.getdecrypted(merchantTxnMast.getMerchantTxnId(),merchantMastBean.getEncryptionKey());
		String callBackURL=EncryptionDecryption.getdecrypted(merchantTxnMast.getCallBackURL(),merchantMastBean.getEncryptionKey());
		String other=EncryptionDecryption.getdecrypted(merchantTxnMast.getOthers(),merchantMastBean.getEncryptionKey());
		double amount=Double.parseDouble(EncryptionDecryption.getdecrypted(merchantTxnMast.getTxnAmount(),merchantMastBean.getEncryptionKey()));*/
		if(new TransactionDaoImpl().getWalletBalance(walletmast.getWalletid()) < amount){
			merchantTxnMastResp.setStatusCode("17024");
			merchantTxnMastResp.setStatusDesc("Insufficient Wallet Balance.");
			return merchantTxnMastResp;
			
		}
		merchantTxnMast.setMerchantName(merchantMastBean.getMerchantName());
		String	txnid = commanUtilDao.getTrxId("MERCHTXN",merchantMastBean.getAggreatorid());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| MERCHTXN "+txnid
				);
		
		merchantTxnMast.setTxnId(txnid);
		merchantTxnMastResp.setTxnId(txnid);
		session.save(merchantTxnMast);
		transaction.commit();
		
		if(merchantMastBean.getValidateBy().equals("OTP")){
			transaction=session.beginTransaction();
			String newOtp = null;
			OTPGeneration oTPGeneration = new OTPGeneration();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|Inside account wallet send otp for validate mobile on" + walletUserMobile
					);
			//logger.info("Inside *********************account wallet****send otp for validate mobile on" + walletUserMobile);
			newOtp = oTPGeneration.generateOTP();
			if (!(newOtp == null)) {
				Query insertOtp = session.getNamedQuery("callOTPMANAGE").setParameter("mobile", walletUserMobile)
						.setParameter("aggreatorid", merchantTxnMast.getAggreatorId())
						.setParameter("otp",  CommanUtil.SHAHashing256(newOtp)/*newOtp*/);
				insertOtp.executeUpdate();
			}
			transaction.commit();
			if (!(newOtp == null)) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| Inside otp send on mobile" + walletUserMobile + "otp"+ newOtp
						);
				//logger.info("Inside ********************otp send on mobile**********" + walletUserMobile + "***otp***"+ newOtp);
				
				//One time password for your <<APPNAME>> wallet is <<OTP>>. For security reasons we advise you not to disclose your 
				 String smsMsg = commanUtilDao.getsmsTemplet("regOtp","").replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<OTP>>", newOtp);
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", walletUserMobile,smsMsg, "SMS","0",transactionDao.getWalletIdByUserId(walletUserMobile,merchantTxnMast.getAggreatorId()),transactionDao.getNameByUserId(walletUserMobile,merchantTxnMast.getAggreatorId()),"OTP");
//				Thread t = new Thread(smsAndMailUtility);
//				t.start();
				ThreadUtil.getThreadPool().execute(smsAndMailUtility);
			}
			merchantTxnMastResp.setStatusCode("1000");	
			merchantTxnMastResp.setStatusDesc(" Partial Txn Save.");
		}
		else
		{

			String walletStatus=transactionDao.walletToMerchent(merchantTxnMast.getUserId(), walletmast.getWalletid(), amount, merchantTxnMast.getMid(), txnid,  merchantTxnMast.getIpiemi(),merchantTxnMast.getAggreatorId(),merchantTxnMast.getAgent());
			StringTokenizer st=new StringTokenizer(walletStatus, "|");
			transaction=session.beginTransaction();
			//merchantTxnMastResp.setValidateBy(merchantMastBean.getValidateBy());
			if(st.hasMoreTokens()){
			if(st.nextToken().equals("Success")){
				merchantTxnMast.setTxnStatus("Success");
				merchantTxnMast.setWalletTxnId(st.nextToken());
				session.save(merchantTxnMast);
				transaction.commit();
				merchantTxnMastResp.setStatusCode("1000");
				merchantTxnMastResp.setStatusDesc("Success");
				return merchantTxnMastResp;
			}else{
				merchantTxnMast.setTxnStatus("Fail");
				session.save(merchantTxnMast);
				transaction.commit();
				merchantTxnMastResp.setStatusCode(st.nextToken());
				merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
			}
			}
			
		}
		//merchantTxnMastResp.setValidateBy(merchantMastBean.getValidateBy());
		
		return merchantTxnMastResp;
		}else{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| Invalid Mid "
					);
			
			merchantTxnMastResp.setStatusCode("1001");
			merchantTxnMastResp.setStatusDesc("MID is Null");
			return merchantTxnMastResp;
		}
		
		} catch (Exception e) {
		merchantTxnMastResp.setStatusCode("7000");
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "| problem in saveMerchant "+e.getMessage()+" "+e
				);
		
	} finally {
		session.close();
	}
	
	return merchantTxnMastResp;
	
	
}

/**
 * 
 */
public String merchentPaymet(String otp,String userId,String walletId,double amount,String mid,String txnId,String ipImei,String aggreatorid,String agent){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| merchentPaymet() "
			);
	
	//logger.info("Start excution ********************* method merchentPaymet(userId,walletId,amount,mid,txnId,ipImei)");
	String status="1001";
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	Transaction transaction=null;
	try {
		transaction =session.beginTransaction();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| amount"+amount
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| txnId"+txnId
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| mid"+mid
				);
		
		MerchantTxnMast merchantTxnMast=(MerchantTxnMast)session.get(MerchantTxnMast.class, txnId);
		if(!merchantTxnMast.getMid().equals(mid)){
			return "7034";
		}
		
		if(merchantTxnMast.getAmount()!= amount){
			return "7035";
		}
		
		
		if(!merchantTxnMast.getTxnStatus().equals("Pending")){
			return "7036";
		}
		if (new WalletUserDaoImpl().validateOTP(userId,aggreatorid,otp)) {
		String walletStatus=transactionDao.walletToMerchent(userId, walletId, amount, mid, txnId, ipImei,aggreatorid,agent);
		StringTokenizer st=new StringTokenizer(walletStatus, "|");
		
		if(st.hasMoreTokens()){
		if(st.nextToken().equals("Success")){
			merchantTxnMast.setTxnStatus("Success");
			merchantTxnMast.setWalletTxnId(st.nextToken());
			session.save(merchantTxnMast);
			transaction.commit();
			return "1000";
		}else{
			merchantTxnMast.setTxnStatus("Fail");
			session.save(merchantTxnMast);
			transaction.commit();
			return st.nextToken();
		}
		}
		}
		else
		{
			merchantTxnMast.setTxnStatus("Fail");
			session.save(merchantTxnMast);
			transaction.commit();
			return "Fail";
		}
		
	} catch (Exception e) {
		status="7000";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| problem in merchentPaymet "+e.getMessage()+" "+e
				);
		//logger.debug("problem in merchentPaymet******************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	
	return status;
}

public MerchantTxnMast merchentPaymetConfirm(String checksum,String otp,String userId,String walletId,double amount,String mid,String txnId,String ipImei,String aggreatorid,String agent){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| merchentPaymetConfirm()"
			);
	//logger.info("Start excution ********************* method merchentPaymet(userId,walletId,amount,mid,txnId,ipImei)");
	String status="1001";
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	Transaction transaction=null;
	MerchantTxnMast merchantTxnMastResp = new MerchantTxnMast();
	merchantTxnMastResp.setStatusCode("1001");
	merchantTxnMastResp.setStatusDesc("Failed");
	try {
		TreeMap<String,String> obj = new TreeMap<String,String>();
		obj.put("userId", userId);
		obj.put("walletId", walletId);
		obj.put("mid", mid);
		obj.put("ipImei", ipImei);
		obj.put("txnId", txnId);
		obj.put("aggreatorid", aggreatorid);
		obj.put("otp", otp);
		obj.put("CHECKSUMHASH", checksum);
		obj.put("amount", Double.toString(amount));
		
		int statusCode= WalletSecurityUtility.checkSecurity(obj);
		  if(statusCode!=1000){
			  merchantTxnMastResp.setStatusCode(""+statusCode);
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
		  }
		  
		transaction =session.beginTransaction();

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| amount"+amount
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "|txnId"+txnId
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| mid"+mid
				);
		
		MerchantTxnMast merchantTxnMast=(MerchantTxnMast)session.get(MerchantTxnMast.class, txnId);
		if(!merchantTxnMast.getMid().equals(mid)){
			  merchantTxnMastResp.setStatusCode("7034");
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
		}
		
		if(merchantTxnMast.getAmount()!= amount){
			 merchantTxnMastResp.setStatusCode("7035");
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
			
		}
		
		
		if(!merchantTxnMast.getTxnStatus().equals("Pending")){
			 merchantTxnMastResp.setStatusCode("7036");
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
		}
		if (new WalletUserDaoImpl().validateOTP(userId,aggreatorid,otp)) {
		String walletStatus=transactionDao.walletToMerchent(userId, walletId, amount, mid, txnId, ipImei,aggreatorid,agent);
		StringTokenizer st=new StringTokenizer(walletStatus, "|");
		
		if(st.hasMoreTokens()){
		if(st.nextToken().equals("Success")){
			merchantTxnMast.setTxnStatus("Success");
			merchantTxnMast.setWalletTxnId(st.nextToken());
			session.save(merchantTxnMast);
			transaction.commit();
			 merchantTxnMastResp.setStatusCode("1000");
			  merchantTxnMastResp.setStatusDesc("Success");
				return merchantTxnMastResp;
		}else{
			merchantTxnMast.setTxnStatus("Fail");
			session.save(merchantTxnMast);
			transaction.commit();
			 merchantTxnMastResp.setStatusCode(""+st.nextToken());
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
		}
		}
		}
		else
		{
			merchantTxnMast.setTxnStatus("Fail");
			session.save(merchantTxnMast);
			transaction.commit();
			 merchantTxnMastResp.setStatusCode("1001");
			  merchantTxnMastResp.setStatusDesc("Failed");
				return merchantTxnMastResp;
		}
		
	} catch (Exception e) {
		status="7000";
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,walletId,userId, "", "",   "| problem in merchentPaymet"+e.getMessage()+" "+e
				);
		//logger.debug("problem in merchentPaymet******************" + e.getMessage(), e);
	} finally {
		session.close();
	}
	
	return merchantTxnMastResp;
}



public String getKeyByMerchantid(String merchantid){
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "| getKeyByMerchantid()"
			);
	//logger.info("Start excution ********************************************************* method getKeyByMerchantid");
	factory = DBUtil.getSessionFactory();
	session = factory.openSession();
	String mKey=null;
	try {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "| MID"+merchantid
				);
		
		MerchantMastBean merchantMastBean=(MerchantMastBean)session.get(MerchantMastBean.class, merchantid);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "| return merchantMastBean"+merchantMastBean
				);
	
		if(!(merchantMastBean==null)){
			mKey=merchantMastBean.getEncryptionKey();
			}
		
		
	} catch (Exception e) {
		e.printStackTrace();
		if (transaction != null)
			transaction.rollback();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "| problem in getKeyByMerchantid"+e.getMessage()+" "+e
				);
		
	} finally {
		session.close();
	}
	return mKey;
}

public MerchantMastBean getMerchantKey(MerchantMastBean merchantMastBean)
{
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantKey()"
			);
	//logger.info("******start execution ****getMerchantKey(MerchantMastBean)");
	MerchantMastBean merchantResp = new MerchantMastBean();
	try
	{
		factory=DBUtil.getSessionFactory();
		session=factory.openSession();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|MID "+merchantMastBean.getMerchantID()
				);
			
			List<MerchantMastBean> merchantMastBeanList=session.createCriteria(MerchantMastBean.class)
					.add(Restrictions.eq("merchantID", merchantMastBean.getMerchantID())).add(Restrictions.eq("aggreatorid", merchantMastBean.getAggreatorid())).list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|return merchantMastBean"+merchantMastBean
					);
			
			if(!(merchantMastBeanList==null)){
				merchantResp=merchantMastBeanList.get(0);
				}
	}
	catch(Exception e)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|problem in getMerchantKey(MerchantMastBean)"+e.getMessage()+" "+e
				);
		
		e.printStackTrace();
	}
	finally
	{
		//session.close();
	}
	return merchantResp;
}

public static void main(String[] args) {
	/*MerchantMastBean merchantMastBean=new MerchantMastBean();
	merchantMastBean.setAggreatorid("AGGR001035");
	new MerchantDaoImpl().getMerchantList(merchantMastBean);*/
	try{
	String mid="MERC001009";
	String txnId=EncryptionDecryption.getEncrypted("Txn123","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"Txn123";
	String txnAmount=EncryptionDecryption.getEncrypted("10","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"10";
	String merchantUserName=EncryptionDecryption.getEncrypted("santosh3","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"Abcd";
	String callBackURL=EncryptionDecryption.getEncrypted("http://localhost:8080","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"123";
	String others=EncryptionDecryption.getEncrypted("others","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"others";
	String billingDtls=EncryptionDecryption.getEncrypted("billingDtls","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"billingDtls";
	String shippingDtls=EncryptionDecryption.getEncrypted("shippingDtls","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"shippingDtls";
	String walletMobileNo=EncryptionDecryption.getEncrypted("7011605528","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"8505830303";
	
	
	MerchantTxnMast merchantTxnMast = new MerchantTxnMast();
	merchantTxnMast.setMid(mid);
	merchantTxnMast.setMerchantTxnId(txnId);
	merchantTxnMast.setTxnAmount(txnAmount);
	merchantTxnMast.setAggreatorId("AGGR001035");
	merchantTxnMast.setMerchantUserName(merchantUserName);
	merchantTxnMast.setCallBackURL(callBackURL);
	merchantTxnMast.setOthers(others);
	merchantTxnMast.setWalletMobileNo(walletMobileNo);
	new MerchantDaoImpl().validateSaveTxn(merchantTxnMast);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}

}
