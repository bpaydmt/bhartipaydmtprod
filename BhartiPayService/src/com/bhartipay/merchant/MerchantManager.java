package com.bhartipay.merchant;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.config.WalletUtilService;
import com.bhartipay.merchant.bean.MerchantMastBean;
import com.bhartipay.merchant.bean.MerchantTxnMast;
import com.bhartipay.merchant.bean.MerchentBean;
import com.bhartipay.merchant.persistence.MerchantDao;
import com.bhartipay.merchant.persistence.MerchantDaoImpl;

@Path("/MerchantManager")
public class MerchantManager {
	private static final Logger logger = Logger.getLogger(MerchantManager.class.getName());
	MerchantDao merchantDao=new MerchantDaoImpl();
	
	
	
	@POST
	@Path("/saveMerchant")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MerchantMastBean saveMerchant(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MerchantMastBean merchantMastBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|saveMerchant()"
				);
		//logger.info("Start excution ******************************************************* method saveMerchant(merchantMastBean)");
		merchantMastBean.setIpiemi(ipiemi);
		merchantMastBean.setAgent(agent);
		return merchantDao.saveMerchant(merchantMastBean);
		
	}
	
	
	@POST
	@Path("/validateSaveTxn")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MerchantTxnMast validateSaveTxn(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MerchantTxnMast merchantTxnMast){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "", "",   "|validateSaveTxn()"
				);
		//logger.info("Start excution ******************************************************* method validateSaveTxn(merchantTxnMast)");
		merchantTxnMast.setIpiemi(ipiemi);
		merchantTxnMast.setAgent(agent);
		return merchantDao.validateSaveTxn(merchantTxnMast);
	}
	
	@POST
	@Path("/validateSaveTxnReq")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public MerchantTxnMast validateSaveTxnReq(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MerchantTxnMast merchantTxnMast){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantTxnMast.getAggreatorId(),"",merchantTxnMast.getUserId(), "",merchantTxnMast.getTxnId(),   "|validateSaveTxnReq()"
				);
		//logger.info("Start excution ******************************************************* method validateSaveTxn(merchantTxnMast)");
		merchantTxnMast.setIpiemi(ipiemi);
		merchantTxnMast.setAgent(agent);
		return merchantDao.validateSaveTxnReq(merchantTxnMast);
	}
	
	@POST
	@Path("/merchentPaymetConfirm")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	public MerchantTxnMast merchentPaymetConfirm(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MerchentBean merchentBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchentBean.getAggreatorid(),"","", "", merchentBean.getTxnId(),   "|merchentPaymetConfirm()"
				);
		//logger.info("Start excution ******************************************************* method merchentPaymet(merchentBean)");
		return merchantDao.merchentPaymetConfirm(merchentBean.getCHECKSUMHASH(),merchentBean.getOtp(),merchentBean.getUserId(),merchentBean.getWalletId(),merchentBean.getAmount(),merchentBean.getMid(),merchentBean.getTxnId(),ipiemi,merchentBean.getAggreatorid(),agent);
	}
	
	@POST
	@Path("/merchentPaymet")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	
	public String merchentPaymet(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,MerchentBean merchentBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchentBean.getAggreatorid(),"","", "", merchentBean.getTxnId(),   "|merchentPaymet()"
				);
		//logger.info("Start excution ******************************************************* method merchentPaymet(merchentBean)");
		return merchantDao.merchentPaymet(merchentBean.getOtp(),merchentBean.getUserId(),merchentBean.getWalletId(),merchentBean.getAmount(),merchentBean.getMid(),merchentBean.getTxnId(),ipiemi,merchentBean.getAggreatorid(),agent);
	}
	
	@POST
	@Path("/getMerchantList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_JSON)
	public List<MerchantMastBean>  getMerchantList(MerchantMastBean merchantMastBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),merchantMastBean.getAggreatorid(),"","", "", "",   "|getMerchantList()"
				);
		//logger.info("Start excution ******************************************************* method getMerchantList(merchantMastBean)"+merchantMastBean.getAggreatorid());
		return merchantDao.getMerchantList(merchantMastBean);
	}
	
	
	

}
