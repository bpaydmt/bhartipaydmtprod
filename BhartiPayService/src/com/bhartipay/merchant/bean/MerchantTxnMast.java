package com.bhartipay.merchant.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="merchanttxnmast")
public class MerchantTxnMast {
	
	
	@Id
	@Column(name="txnid")
	private String txnId;
	
	@Column(name="merchantid")
	private String mid;
	
	@Column(name = "aggreatorid", nullable = false)
	private String aggreatorId;
	
	@Column(name="merchantuser")
	private String merchantUserName;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="txnstatus")
	private String txnStatus;
	
	@Column(name="merchanttxnid")
	private String merchantTxnId;
	
	@Column(name="wallettxnid")
	private String walletTxnId;
	
	@Column(name="others")
	private String others;
	
	@Column(name="callbackurl")
	private String callBackURL;
	
	@Column(name="useragent")
	private String userAgent;
	
	@Column(name="reqip")
	private String reqIp;
	
	@Column(name="commflag")
	private int commFlag;
	
	@Column(name="walletmobileno")
	private String walletMobileNo;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txnDate;
	
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	@Transient
	private String txnAmount;
	private transient String statusCode;
	
	@Transient
	private  String statusDesc;
	
	@Transient
	private  String merchantName;
	
	@Transient
	private  String validateBy;
	
	
	@Transient
	private  String userId;
	
	@Transient
	private  String CHECKSUMHASH;
	

	
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getWalletMobileNo() {
		return walletMobileNo;
	}

	public void setWalletMobileNo(String walletMobileNo) {
		this.walletMobileNo = walletMobileNo;
	}


	public String getValidateBy() {
		return validateBy;
	}

	public void setValidateBy(String validateBy) {
		this.validateBy = validateBy;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}



	

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	public String getWalletTxnId() {
		return walletTxnId;
	}

	public void setWalletTxnId(String walletTxnId) {
		this.walletTxnId = walletTxnId;
	}



	public String getMerchantUserName() {
		return merchantUserName;
	}

	public void setMerchantUserName(String merchantUserName) {
		this.merchantUserName = merchantUserName;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCallBackURL() {
		return callBackURL;
	}

	public void setCallBackURL(String callBackURL) {
		this.callBackURL = callBackURL;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getReqIp() {
		return reqIp;
	}

	public void setReqIp(String reqIp) {
		this.reqIp = reqIp;
	}

	public int getCommFlag() {
		return commFlag;
	}

	public void setCommFlag(int commFlag) {
		this.commFlag = commFlag;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}
	
	
	
	
	

}
