package com.bhartipay.merchant.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="merchantmast")
public class MerchantMastBean {
	
	
	
	@Id
	@Column(name="merchantid", nullable = false)
	private String merchantID;
	
	@Column(name="aggreatorid", nullable = false)
	private String aggreatorid;
	
	
	@Column(name="merchantuser" ,unique=true, nullable = false)
	private String merchantUser;
	
	@Column(name="merchantname")
	private String merchantName;
	
	@Column(name="merchantemail")
	private String merchantEmail;
	
	@Column(name="merchantmobile")
	private String merchantMobile;
	
	@Column(name="merchantsite")
	private String merchantSite;
	
	@Column(name="merchantstatus")
	private String merchantStatus;
	
	@Column(name="merchantpassword")
	private String merchantPassword;
	
	@Column(name="callbackurl")
	private String callBackUrl;
	
	@Column(name="encryptionkey")
	private String encryptionKey;
	
	@Column(name="validateby")
	private String validateBy;
	
	@Column(name = "ipiemi",  length = 20 )
	private String ipiemi;
	
	@Column(name = "agent",  length = 300 )
	private String agent;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createDate;
	
	
	@Column(name = "merchanttype",  length = 50 )
	private String merchantType;
	
	@Column(name = "createby",  length = 50 )
	 private String createBy;
	
	
	@Transient
	private String  statusCode;
	
	
	
	
	
	
	
	
	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getValidateBy() {
		return validateBy;
	}

	public void setValidateBy(String validateBy) {
		this.validateBy = validateBy;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getMerchantUser() {
		return merchantUser;
	}

	public void setMerchantUser(String merchantUser) {
		this.merchantUser = merchantUser;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantEmail() {
		return merchantEmail;
	}

	public void setMerchantEmail(String merchantEmail) {
		this.merchantEmail = merchantEmail;
	}

	public String getMerchantMobile() {
		return merchantMobile;
	}

	public void setMerchantMobile(String merchantMobile) {
		this.merchantMobile = merchantMobile;
	}

	public String getMerchantSite() {
		return merchantSite;
	}

	public void setMerchantSite(String merchantSite) {
		this.merchantSite = merchantSite;
	}

	public String getMerchantStatus() {
		return merchantStatus;
	}

	public void setMerchantStatus(String merchantStatus) {
		this.merchantStatus = merchantStatus;
	}

	public String getMerchantPassword() {
		return merchantPassword;
	}

	public void setMerchantPassword(String merchantPassword) {
		this.merchantPassword = merchantPassword;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	
	
		

}
