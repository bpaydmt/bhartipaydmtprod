package com.bhartipay.airTravel.persistence;

import java.util.List;

import com.bhartipay.airTravel.bean.AirFareBean;
import com.bhartipay.airTravel.bean.AirportDetails;
import com.bhartipay.airTravel.bean.AuthenticationRequestBean;
import com.bhartipay.airTravel.bean.CancelledDetails;
import com.bhartipay.airTravel.bean.ConvenienceFeeBean;
import com.bhartipay.airTravel.bean.PassangerDetails;
import com.bhartipay.airTravel.bean.TicketDetails;
import com.bhartipay.airTravel.bean.TicketSegment;
import com.bhartipay.airTravel.bean.VersionStatusBean;

public interface AirTravelDao 
{
	public List<AirportDetails> getAirportList();
	
	public List<AirportDetails> getAirportPopularList();
	
	public AuthenticationRequestBean getAuthDetails(String apiName);
	
	public AuthenticationRequestBean updateAuthUser(AuthenticationRequestBean reqbean);
	
	public String saveFareQuote(AirFareBean airFareBean);
	
	public AirFareBean getFareQuote(String airFareId);
	
	public String saveTravelApiRequest(String agreatorid,String reqVal,String serverName,String apiName);
	
	public List<AirFareBean> getFareQuoteByTripId(String tripId , String status);
	
	public int updateFareQuote(double totalAmt ,double pgAmt , double walletAmt ,  double pgAmtRefund , double walletAmtRefund , String pgTxn ,String mobileno , String walletId,String eventStatus,String wallettxn, String tripId);
	
	public List<ConvenienceFeeBean> getConvenienceFeeStruct(int status,String process);
	
	public int updateTotalRefundFareQuote(double totalRefund , String airFareId);
	
	public String saveTicketDetails(TicketDetails ticketDetails );
	
	public long saveTicketSegments(TicketSegment ticketSegment);
	
	public long savePassengerDetails(PassangerDetails passangerDetails);
	
	public List<TicketDetails> getTicketDetails(String tripId);
	
	public List<TicketSegment> getTicketSegment(String tripId);
	
	public List<TicketSegment> getTicketSegmentByAirFareId(String airFareId);
	
	public List<PassangerDetails> getPassangerDetails(String tripId);
	
	public List<PassangerDetails> getPassangerDetailsByAirFareId(String airFareId);
	
	public AirFareBean getFareQuoteByPNR(String TripId , String pnr);
	
	public String saveCancelDetails(CancelledDetails cancelDetails);
	
	public List<CancelledDetails> getCancelDetails(String airFareId,List<Integer> ticketid);
	
	public PassangerDetails getPassangerDetailsByTicketId(String ticketId,String tripId,String pnr);
	
	public List<PassangerDetails> getPassangerDetailsByPNR(String tripId,String pnr);
	
	public CancelledDetails getCancelDetails(String airFareId,String ticketid);
}
