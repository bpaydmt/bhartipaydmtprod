package com.bhartipay.airTravel.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;

import com.bhartipay.airTravel.bean.AirFareBean;
import com.bhartipay.airTravel.bean.AirportDetails;
import com.bhartipay.airTravel.bean.AuthenticationRequestBean;
import com.bhartipay.airTravel.bean.CancelledDetails;
import com.bhartipay.airTravel.bean.ConvenienceFeeBean;
import com.bhartipay.airTravel.bean.PassangerDetails;
import com.bhartipay.airTravel.bean.TicketDetails;
import com.bhartipay.airTravel.bean.TicketSegment;
import com.bhartipay.airTravel.bean.TravelApiRequestId;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;

public class AirTravelDaoImpl implements AirTravelDao
{
	
	
	public static final Logger logger = Logger.getLogger(AirTravelDaoImpl.class.getName());
	SessionFactory factory = null;
	Session session = null;
	Transaction transaction = null;
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
	private static final long twepoch = 1288834974657L;
	 private static final long sequenceBits = 17;
	 private static final long sequenceMax = 65536;
	 private static volatile long lastTimestamp = -1L;
	 private static volatile long sequence = 0L;
	
	
	private static synchronized Long generateLongId() {
       long timestamp = System.currentTimeMillis();
       if (lastTimestamp == timestamp) {
           sequence = (sequence + 1) % sequenceMax;
           if (sequence == 0) {
               timestamp = tilNextMillis(lastTimestamp);
           }
       } else {
           sequence = 0;
       }
       lastTimestamp = timestamp;
       Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
       return id;
   }

   private static long tilNextMillis(long lastTimestamp) {
       long timestamp = System.currentTimeMillis();
       while (timestamp <= lastTimestamp) {
           timestamp = System.currentTimeMillis();
       }
       return timestamp;
   }
	
   public String saveTravelApiRequest(String agreatorid,String reqVal,String serverName,String apiName){
		
		logger.info(serverName+"Start excution ************************* saveTravelApiRequest aggreatorId**:"	+ agreatorid);
		logger.info(serverName+"Start excution ************************* saveTravelApiRequest reqVal**:"	+ reqVal);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String requestId="";
		try{
			transaction=session.beginTransaction();
			TravelApiRequestId apiLog=new TravelApiRequestId();
			
			
			apiLog.setRequestId(Long.toString(generateLongId())+commanUtilDao.getTrxId("TravelLog",agreatorid));
			apiLog.setAggreatorId(agreatorid);
			apiLog.setRequestValue(reqVal);
			apiLog.setApiName(apiName);
			session.save(apiLog);
			transaction.commit();
			requestId=apiLog.getRequestId();
			logger.info(serverName+"Start excution ************************************* saveTravelApiRequest requestId**:"	+ requestId);
		} catch (Exception e) {
			logger.debug(serverName+"problem in saveTravelApiRequest=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
			} finally {
			session.close();
			}
		return requestId;
	}
   
   
	/**
	 * 
	 */
	public List<AirportDetails> getAirportList()
	{
		logger.info("*****start execution **************getAirportList");
		List<AirportDetails> airportList = null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			Query query = session.createQuery("FROM AirportDetails where serviceProvider=:serviceProvider and countryCode='IN'");
			query.setString("serviceProvider", "TBU");
			airportList = query.list();
		}
		catch(Exception ex)
		{
			logger.info("*****problem in **************getAirportList");
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return airportList;
	}
	
	public List<AirportDetails> getAirportPopularList()
	{
		logger.info("*****start execution **************getAirportPopularList");
		List<AirportDetails> airportList = null;
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			Query  query = session.createQuery(" FROM AirportDetails where popularity='1' and serviceProvider=:serviceProvider and countryCode='IN'");
			query.setString("serviceProvider", "TBU");
			airportList = query.list();
		}
		catch(Exception ex)
		{
			logger.info("*****Problem in **************getAirportPopularList");
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return airportList;
	}
	
	public AuthenticationRequestBean getAuthDetails(String apiName)
	{
		logger.info("*****start execution **************getAuthDetails");
		AuthenticationRequestBean requestbean = new AuthenticationRequestBean();
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			Query query = session.createQuery("FROM AuthenticationRequestBean where apiName = :apiName").setString("apiName", apiName);
			requestbean =(AuthenticationRequestBean)(query.uniqueResult());
		}
		catch(Exception e)
		{
			logger.info("*****problem in execution **************getAuthDetails");
			e.printStackTrace();
		}
		finally {
			session.close();
			}
		return requestbean;
	}
	public AuthenticationRequestBean updateAuthUser(AuthenticationRequestBean reqbean)
	{
		logger.info("*****start execution **************updateAuthUser");
		AuthenticationRequestBean req = new AuthenticationRequestBean();
		try
		{
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			req = (AuthenticationRequestBean)(session.get(AuthenticationRequestBean.class, reqbean.getClientId()));
			req.setTokenId(reqbean.getTokenId());
			session.save(req);
			transaction.commit();
		}
		catch(Exception e)
		{
			logger.info("*****start execution **************updateAuthUser");
			e.printStackTrace();	
		}
		finally {
			session.close();
			}
		return req;
	}

	public String saveFareQuote(AirFareBean airFareBean)
	{
		logger.info("*******************Start Execution********saveFareQuote***");
		String status="1001";
		try
		{
			factory=DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction=session.beginTransaction();
			if(airFareBean.getAirFareId() == null || airFareBean.getAirFareId().trim().equals(""))
			{
				String travelId=commanUtilDao.getTrxId("TravelFare",airFareBean.getAggreatorId());
				airFareBean.setAirFareId(travelId);
			}			
			session.saveOrUpdate(airFareBean);
			transaction.commit();
			return airFareBean.getAirFareId();
		}
		catch(Exception ex)
		{
			logger.info("*******************problem in Execution********saveFareQuote*******e.message"+ex.getMessage());
			ex.printStackTrace();
			return "700";
		}
		finally {
			session.close();
			}
	}
	
	public AirFareBean getFareQuote(String airFareId)
	{
		logger.info("*****************Start Execution***************getFareQuote******");
		AirFareBean airFareBean = new AirFareBean();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			Query query = session.createQuery("From AirFareBean where airFareId = :id").setString("id", airFareId);
			airFareBean = (AirFareBean)query.uniqueResult();
		}
		catch(Exception ex)
		{
			logger.info("************problem in getFareQuote**************e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return airFareBean;
	}
	
	public AirFareBean getFareQuoteByPNR(String TripId , String pnr)
	{
		logger.info("*****************Start Execution***************getFareQuoteByPNR******");
		AirFareBean airFareBean = new AirFareBean();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			Query query = session.createQuery("From AirFareBean where tripId = :id and pnrNo = :pnrNo").setString("id", TripId).setString("pnrNo", pnr);
			airFareBean = (AirFareBean)query.uniqueResult();
		}
		catch(Exception ex)
		{
			logger.info("************problem in getFareQuoteByPNR**************e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return airFareBean;
	}
	
	public int updateFareQuote(double totalAmt ,double pgAmt , double walletAmt ,  double pgAmtRefund , double walletAmtRefund , String pgTxn ,String mobileno , String walletId,String eventStatus,String wallettxn, String tripId)
	{
		logger.info("*****************Start Execution***************getFareQuote******");
		int flag =0;
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction = session.beginTransaction();
			String qry="update airfare set ";
			if(totalAmt != -1)
			{
				qry=qry+ " totalamt="+totalAmt+" ,";
			}
			if(pgAmt != -1)
			{
				qry=qry	+ " pgamt="+pgAmt+" ,";
			}
			if(walletAmt != -1)
			{
				qry=qry	+ " walletamt="+walletAmt+" ,";
			}
			if(pgAmtRefund != -1)
			{
				qry=qry	+ " pgamtrefund="+pgAmtRefund+" ,";
			}
			if(walletAmtRefund != -1)
			{
				qry=qry	+ " walletamtrefund="+walletAmtRefund+" ,";
			}
			if(pgTxn != null && !pgTxn.trim().equals("") )
			{
				qry = qry	+ " pgtxn='"+pgTxn+"' ,";
			}
			if(mobileno != null && !mobileno.trim().equals("") )
			{
				qry = qry	+ " mobileno='"+mobileno+"' ,";
			}
			if(walletId != null && !walletId.trim().equals("") )
			{
				qry = qry	+ " walletid='"+walletId+"' ,";
			}
			if(eventStatus != null && !eventStatus.trim().equals("") )
			{
				qry = qry	+ " eventstatus='"+eventStatus+"'";
			}
			if(wallettxn != null && !wallettxn.trim().equals("") )
			{
				qry = qry	+ " wallettxn='"+wallettxn+"'";
			}
			if(qry.charAt(qry.length()-1) == ',')
			{
				qry=qry.substring(0,qry.length()-2);
			}
				qry=qry			+ " where tripid='"+tripId+"'";
			Query query = session.createSQLQuery(qry);
			flag = query.executeUpdate();
			transaction.commit();
			
		}
		catch(Exception ex)
		{
			logger.info("************problem in getFareQuote**************e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
			
			}
		return flag;
	}

	public int updateTotalRefundFareQuote(double totalRefund , String airFareId)
	{
	logger.info("*****************Start Execution***************getFareQuote******");
	int flag =0;
	try
	{
		factory = DBUtil.getSessionFactory();
		session=factory.openSession();
		transaction = session.beginTransaction();
		String qry="update airfare set  totalrefundamt="+totalRefund+" where airfareid='"+airFareId+"'";
		Query query = session.createSQLQuery(qry);
		flag = query.executeUpdate();
		transaction.commit();
		
	}
	catch(Exception ex)
	{
		logger.info("************problem in getFareQuote**************e.message*****"+ex.getMessage());
		ex.printStackTrace();
	}
	finally {
		session.close();
		}
	return flag;
}
	
	public List<AirFareBean> getFareQuoteByTripId(String tripId , String status)
	{
		logger.info("*****************Start Execution***************getFareQuote******");
		List<AirFareBean> airFareBean = new ArrayList<AirFareBean>();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			/*String qry ="Select * From airfare where tripid = '"+ tripId+"'";
			if(status != null && !status.trim().equals(""))
			{
				qry= qry + "status = '"+status+"'";
			}
			
			Query query = session.createSQLQuery(qry);
			query.setResultTransformer(Transformers.aliasToBean(AirFareBean.class));
			airFareBean = query.list();*/
			//session.createCriteria(AirFareBean.class).add(Restrictions.eq("tripId", tripId));
			if(status != null && !status.trim().equals(""))
			{
				airFareBean = session.createCriteria(AirFareBean.class).add(Restrictions.eq("tripId", tripId)).add(Restrictions.eq("status", status)).addOrder(Order.asc("airFareId")).list();
			}
			else
			{
				airFareBean = session.createCriteria(AirFareBean.class).add(Restrictions.eq("tripId", tripId)).addOrder(Order.asc("airFareId")).list();
			}
		}
		catch(Exception ex)
		{
			logger.info("************problem in getFareQuote**************e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return airFareBean;
	}

	public List<ConvenienceFeeBean> getConvenienceFeeStruct(int status,String process)
	{
		logger.info("*****************Start Execution***************getFareQuote******");
		List<ConvenienceFeeBean> confee = new ArrayList<ConvenienceFeeBean>();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			confee = session.createCriteria(ConvenienceFeeBean.class).add(Restrictions.eq("status", status)).add(Restrictions.eq("process", process)).addOrder(Order.asc("way")).list();
		}
		catch(Exception ex)
		{
			logger.info("************problem in getFareQuote**************e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		finally {
			session.close();
			}
		return confee;
	}
	
	
	
	
	public String saveTicketDetails(TicketDetails ticketDetails )
	{
		logger.info("*******************Start Execution********saveTicketDetails***");
		String status="1001";
		try
		{
			factory=DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction=session.beginTransaction();		
			session.saveOrUpdate(ticketDetails);
			transaction.commit();
			return ticketDetails.getAirFareId();
		}
		catch(Exception ex)
		{
			logger.info("*******************problem in Execution********saveTicketDetails*******e.message"+ex.getMessage());
			ex.printStackTrace();
			return "700";
		}
		finally {
			session.close();
			}
	}
 
	public long saveTicketSegments(TicketSegment ticketSegment)
	{
		logger.info("*******************Start Execution********saveTicketSegments***");
		long status=1001;
		try
		{
			factory=DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction=session.beginTransaction();		
			session.saveOrUpdate(ticketSegment);
			transaction.commit();
			return ticketSegment.getSegmentId();
		}
		catch(Exception ex)
		{
			logger.info("*******************problem in Execution********saveTicketSegments*******e.message"+ex.getMessage());
			ex.printStackTrace();
			return 700;
		}
		finally {
			session.close();
			}
	}
	
	public long savePassengerDetails(PassangerDetails passangerDetails)
	{
		logger.info("*******************Start Execution********savePassengerDetails***");
		long status=1001;
		try
		{
			factory=DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction=session.beginTransaction();		
			session.saveOrUpdate(passangerDetails);
			transaction.commit();
			return passangerDetails.getPassangerId();
		}
		catch(Exception ex)
		{
			logger.info("*******************problem in Execution********savePassengerDetails*******e.message"+ex.getMessage());
			ex.printStackTrace();
			return 700;
		}
		finally {
			session.close();
			}
	}
	 
public List<TicketDetails> getTicketDetails(String tripId)
{
	logger.info("*************start execution*************getTicketDetails");
	List<TicketDetails> ticketDetailList = new ArrayList<TicketDetails>();
	try
	{

		factory = DBUtil.getSessionFactory();
		session=factory.openSession();
		ticketDetailList = session.createCriteria(TicketDetails.class).add(Restrictions.eq("tripId", tripId)).addOrder(Order.asc("airFareId")).list();
	}
	catch(Exception e)
	{
		logger.info("****************problem in*************getTicketDetails****e.message"+e.getMessage());
		e.printStackTrace();
	}
	finally
	{
		session.close();
	}
	return ticketDetailList;
}
	
	public List<TicketSegment> getTicketSegment(String tripId)
	{

		logger.info("*************start execution*************getTicketSegment");
		List<TicketSegment> ticketSegmentList = new ArrayList<TicketSegment>();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			ticketSegmentList = session.createCriteria(TicketSegment.class).add(Restrictions.eq("tripId", tripId)).addOrder(Order.asc("airFareId")).addOrder(Order.asc("segmentId")).list();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getTicketSegment****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return ticketSegmentList;

	}
	
	public List<TicketSegment> getTicketSegmentByAirFareId(String airFareId)
	{


		logger.info("*************start execution*************getTicketSegment");
		List<TicketSegment> ticketSegmentList = new ArrayList<TicketSegment>();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			ticketSegmentList = session.createCriteria(TicketSegment.class).add(Restrictions.eq("airFareId", airFareId)).addOrder(Order.asc("segmentId")).list();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getTicketSegment****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return ticketSegmentList;

	
	}
	
	public List<PassangerDetails> getPassangerDetails(String tripId)
	{
		logger.info("*************start execution*************getPassangerDetails");
		List<PassangerDetails> passangerDetailsList = new ArrayList<PassangerDetails>();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			passangerDetailsList = session.createCriteria(PassangerDetails.class).add(Restrictions.eq("tripId", tripId)).addOrder(Order.asc("airFareId")).addOrder(Order.asc("passangerId")).list();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getPassangerDetails****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return passangerDetailsList;

	
	}
	public List<PassangerDetails> getPassangerDetailsByPNR(String tripId,String pnr)
	{
		logger.info("*************start execution*************getPassangerDetailsByPNR");
		List<PassangerDetails> passangerDetails = new ArrayList<PassangerDetails>();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			if(pnr.trim().equals(""))
			{
				passangerDetails =session.createCriteria(PassangerDetails.class).add(Restrictions.eq("tripId", tripId)).list();
			}
			else
			{
				passangerDetails =session.createCriteria(PassangerDetails.class).add(Restrictions.eq("tripId", tripId)).add(Restrictions.eq("pnr", pnr)).list();
			}
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getPassangerDetailsByPNR****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return passangerDetails;

	
	}
	public PassangerDetails getPassangerDetailsByTicketId(String ticketId,String tripId,String pnr)
	{
		logger.info("*************start execution*************getPassangerDetailsByTicketId");
		PassangerDetails passangerDetails = new PassangerDetails();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			if(pnr.trim().equals(""))
			{
				passangerDetails =(PassangerDetails) session.createCriteria(PassangerDetails.class).add(Restrictions.eq("ticketId", ticketId)).add(Restrictions.eq("tripId", tripId)).uniqueResult();
			}
			else
			{
				passangerDetails =(PassangerDetails) session.createCriteria(PassangerDetails.class).add(Restrictions.eq("ticketId", ticketId)).add(Restrictions.eq("tripId", tripId)).add(Restrictions.eq("pnr", pnr)).uniqueResult();
			}
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getPassangerDetailsByTicketId****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return passangerDetails;

	
	}
	public List<PassangerDetails> getPassangerDetailsByAirFareId(String airFareId)
	{
		logger.info("*************start execution*************getPassangerDetailsByAirFareId");
		List<PassangerDetails> passangerDetailsList = new ArrayList<PassangerDetails>();
		try
		{

			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			passangerDetailsList = session.createCriteria(PassangerDetails.class).add(Restrictions.eq("airFareId", airFareId)).addOrder(Order.asc("passangerId")).list();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getPassangerDetailsByAirFareId****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return passangerDetailsList;

	
	}
	
	
	public String saveCancelDetails(CancelledDetails cancelDetails)
	{
		logger.info("*******************Start Execution********saveCancelDetails***");
		String status="1001";
		try
		{
			factory=DBUtil.getSessionFactory();
			session=factory.openSession();
			transaction=session.beginTransaction();
			if(cancelDetails.getCancelId() == null || cancelDetails.getCancelId().trim().equals(""))
			{
				String cancelId=commanUtilDao.getTrxId("CnlTkt","OAGG001050");
				cancelDetails.setCancelId(cancelId);
			}			
			session.saveOrUpdate(cancelDetails);
			transaction.commit();
			return cancelDetails.getCancelId();
		}
		catch(Exception ex)
		{
			logger.info("*******************problem in Execution********saveCancelDetails*******e.message"+ex.getMessage());
			ex.printStackTrace();
			return "700";
		}
		finally {
			session.close();
			}
	}
	
	
	public List<CancelledDetails> getCancelDetails(String airFareId,List<Integer> ticketid)
	{

		logger.info("*************start execution*************getCancelDetails");
		List<CancelledDetails> cancelledDetailList = new ArrayList<CancelledDetails>();
		List<String> ticketList = new ArrayList<String>();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			Iterator<Integer> itr = ticketid.iterator();
			while(itr.hasNext())
			{
				ticketList.add(Integer.toString(itr.next()));
			}
			cancelledDetailList = session.createCriteria(CancelledDetails.class).add(Restrictions.eq("airFareId", airFareId)).add(Restrictions.in("ticketId", ticketList)).list();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getCancelDetails****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return cancelledDetailList;

	
	
	}
	
	public CancelledDetails getCancelDetails(String airFareId,String ticketid)
	{

		logger.info("*************start execution*************getCancelDetails");
		CancelledDetails cancelledDetail = null;
		List<String> ticketList = new ArrayList<String>();
		try
		{
			factory = DBUtil.getSessionFactory();
			session=factory.openSession();
			
			cancelledDetail = (CancelledDetails)session.createCriteria(CancelledDetails.class).add(Restrictions.eq("airFareId", airFareId)).add(Restrictions.eq("ticketId", ticketid)).uniqueResult();
		}
		catch(Exception e)
		{
			logger.info("****************problem in*************getCancelDetails****e.message"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return cancelledDetail;

	
	
	}
public static void main(String s[])
{

	/*AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
	long date = System.currentTimeMillis();
	List<AirportDetails> list = new AirTravelDaoImpl().getAirportList();
	System.out.println("*******"+date+"**********Time: "+System.currentTimeMillis()+"**************"+list.size());
	date = System.currentTimeMillis();
	 list = new AirTravelDaoImpl().getAirportList();
	System.out.println("*******"+date+"**********Time: "+System.currentTimeMillis()+"**************"+list.size());
	date = System.currentTimeMillis();
	 list = new AirTravelDaoImpl().getAirportList();
	System.out.println("*******"+date+"**********Time: "+System.currentTimeMillis()+"**************"+list.size());*/
	

	//new AirTravelDaoImpl().updateFareQuote(12.0, 12.0, 13.0,-1,-1, "ASDSD", "TPID001102","","","","");
	
}
}
