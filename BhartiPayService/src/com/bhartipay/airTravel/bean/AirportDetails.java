package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="airportdetails")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class AirportDetails 
{
	@Id
	@Column(name = "airportname" , length = 50)
	private String airportName;
	
	@Column(name="airportcode" , length = 10)
	private String airportCode ;
	
	@Column(name="cityname" , length=50)
	private String cityName ;
	
	@Column(name="citycode" , length=10)
	private String cityCode ;
	
	@Column(name="countryname" , length=50)
	private String countryName ;
	
	@Column(name="countrycode" , length=10)
	private String countryCode;
	
	@Column(name="nationality" , length=50)
	private String nationality;
	
	@Column(name="currency" , length=5)
	private String currency ;
	
	@Column(name="popularity" , length=3)
	private String popularity;
	
	
	@Column(name="serviceprovider" , length=3)
	private String serviceProvider;
	
	
	
	
	
	
	
	

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}
	
	
}
