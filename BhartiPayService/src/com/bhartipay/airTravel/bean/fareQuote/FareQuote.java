
package com.bhartipay.airTravel.bean.fareQuote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareQuote {

    @SerializedName("Response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
