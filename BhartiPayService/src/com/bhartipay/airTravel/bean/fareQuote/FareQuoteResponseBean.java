package com.bhartipay.airTravel.bean.fareQuote;

import com.bhartipay.airTravel.bean.PayLoadBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FareQuoteResponseBean 
{
	 @SerializedName("statusCode")
	    @Expose
	 private String statusCode;
	
	 
	 @SerializedName("status")
	    @Expose
     private String status;
	 
	 @SerializedName("travelId")
	    @Expose
  private String travelId;
	 
	@SerializedName("statusMsg")
	    @Expose
private String statusMsg;
	 
	 @SerializedName("fareQuote")
	    @Expose
 private  FareQuote fareQuote;
 
 
	 public String getTravelId() {
			return travelId;
		}
		public void setTravelId(String travelId) {
			this.travelId = travelId;
		}

	public FareQuote getFareQuote() {
		return fareQuote;
	}
	public void setFareQuote(FareQuote fareQuote) {
		this.fareQuote = fareQuote;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

 
 
 
}
