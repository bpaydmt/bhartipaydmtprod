
package com.bhartipay.airTravel.bean.fareQuote;

import java.util.List;

import com.bhartipay.airTravel.bean.booking.TicketCRInfo;
import com.bhartipay.airTravel.bean.flightSearch.Error;
import com.bhartipay.airTravel.bean.flightSearch.Result;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("Error")
    @Expose
    private Error error;
    @SerializedName("IsPriceChanged")
    @Expose
    private Boolean isPriceChanged;
    @SerializedName("ChangeRequestId")
    @Expose
    private String changeRequestId;
    @SerializedName("RefundedAmount")
    @Expose
    private double RefundedAmount;
    @SerializedName("CancellationCharge")
    @Expose
    private double cancellationCharge;
    @SerializedName("ServiceTaxOnRAF")
    @Expose
    private double serviceTaxOnRAF;
    @SerializedName("SwachhBharatCess")
    @Expose
    private double swachhBharatCess;
    @SerializedName("KrishiKalyanCess")
    @Expose
    private double krishiKalyanCess;
    @SerializedName("CreditNoteNo")
    @Expose
    private String creditNoteNo;
    @SerializedName("CreditNoteCreatedOn")
    @Expose
    private String creditNoteCreatedOn;
    @SerializedName("ChangeRequestStatus")
    @Expose
    private int ChangeRequestStatus;
    @SerializedName("Results")
    @Expose
    private Result result;
    @SerializedName("ResultList")
    @Expose
    private List<Result> resultList;
    
    @SerializedName("B2B2BStatus")
    @Expose
    private Boolean b2B2BStatus;
    @SerializedName("TicketCRInfo")
    @Expose
    private List<TicketCRInfo> ticketCRInfo = null;
    @SerializedName("ResponseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("TraceId")
    @Expose
    private String traceId;

   
    
    
    public double getRefundedAmount() {
		return RefundedAmount;
	}

	public void setRefundedAmount(double refundedAmount) {
		RefundedAmount = refundedAmount;
	}

	public double getSwachhBharatCess() {
		return swachhBharatCess;
	}

	public void setSwachhBharatCess(double swachhBharatCess) {
		this.swachhBharatCess = swachhBharatCess;
	}

	public double getKrishiKalyanCess() {
		return krishiKalyanCess;
	}

	public void setKrishiKalyanCess(double krishiKalyanCess) {
		this.krishiKalyanCess = krishiKalyanCess;
	}

	public String getCreditNoteNo() {
		return creditNoteNo;
	}

	public void setCreditNoteNo(String creditNoteNo) {
		this.creditNoteNo = creditNoteNo;
	}

	public String getCreditNoteCreatedOn() {
		return creditNoteCreatedOn;
	}

	public void setCreditNoteCreatedOn(String creditNoteCreatedOn) {
		this.creditNoteCreatedOn = creditNoteCreatedOn;
	}

	public String getChangeRequestId() {
		return changeRequestId;
	}

	public void setChangeRequestId(String changeRequestId) {
		this.changeRequestId = changeRequestId;
	}

	public double getCancellationCharge() {
		return cancellationCharge;
	}

	public void setCancellationCharge(double cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	public double getServiceTaxOnRAF() {
		return serviceTaxOnRAF;
	}

	public void setServiceTaxOnRAF(double serviceTaxOnRAF) {
		this.serviceTaxOnRAF = serviceTaxOnRAF;
	}

	public int getChangeRequestStatus() {
		return ChangeRequestStatus;
	}

	public void setChangeRequestStatus(int changeRequestStatus) {
		ChangeRequestStatus = changeRequestStatus;
	}

	public Integer getResponseStatus() {
    return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
    this.responseStatus = responseStatus;
    }

    public String getTraceId() {
    return traceId;
    }

    public void setTraceId(String traceId) {
    this.traceId = traceId;
    }
    

	public Boolean getB2B2BStatus() {
		return b2B2BStatus;
	}

	public void setB2B2BStatus(Boolean b2b2bStatus) {
		b2B2BStatus = b2b2bStatus;
	}

	public List<TicketCRInfo> getTicketCRInfo() {
		return ticketCRInfo;
	}

	public void setTicketCRInfo(List<TicketCRInfo> ticketCRInfo) {
		this.ticketCRInfo = ticketCRInfo;
	}

	public List<Result> getResultList() {
		return resultList;
	}

	public void setResultList(List<Result> resultList) {
		this.resultList = resultList;
	}

	public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Boolean getIsPriceChanged() {
        return isPriceChanged;
    }

    public void setIsPriceChanged(Boolean isPriceChanged) {
        this.isPriceChanged = isPriceChanged;
    }




    public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}


}
