package com.bhartipay.airTravel.bean;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="passangerdetails")
public class PassangerDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="passangerid")
	private long passangerId;
	
	@Column(name="airfareid",length=20)
	private String airFareId;
	
	@Column(name="tripid",length=20)
	private String tripId;
	
	@Column(name="tripindicator", length=1)
	private int tripIndicator;
	
	@Column(name="passangername", length=50)
	private String passangername;
	
	@Column(name="airlinecode",length=5)
	private String airlineCode;
	
	@Column(name="status", length=20)
	private String status;
	
	@Column(name="lifeinsurance", length=20)
	private String lifeInsurance;
	
	@Column(name="checkinbag", length=20)
	private String checkinBag;
	
	@Column(name="checkincabin", length=20)
	private String checkinCabin;
	
	@Column(name="mealtype", length=20)
	private String mealType;

	@Column(name="sourcedestinationcode" , length=20)
	private String sourceDestinationcode;
	
	@Column(name="ticketnumber" , length=20)
	private String ticketNumber;
	
	@Column(name="ticketid" , length=20)
	private String ticketId;
	
	@Column(name="pnr" , length=20)
	private String pnr;

	@Column(name="ispnrcancelled",length=10)
	private String isPnrCancelled;
	
	@Column(name="paxtype",length=2)
	private int paxType;
	
	@Column(name="publishedfare" , columnDefinition="Decimal(10,2) default '00.00'")
	private double publishedFare;
	
	@Transient
	private  double cancellationCharge;

	@Transient
	private  double cancellationFees;
	
	@Transient
	private  double refundedAmount;
	
	@Transient
	private  String cancelledMode;
	
	
	
	public String getCancelledMode() {
		return cancelledMode;
	}

	public void setCancelledMode(String cancelledMode) {
		this.cancelledMode = cancelledMode;
	}

	public double getCancellationCharge() {
		return cancellationCharge;
	}

	public void setCancellationCharge(double cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	public double getCancellationFees() {
		return cancellationFees;
	}

	public void setCancellationFees(double cancellationFees) {
		this.cancellationFees = cancellationFees;
	}

	public double getRefundedAmount() {
		return refundedAmount;
	}

	public void setRefundedAmount(double refundedAmount) {
		this.refundedAmount = refundedAmount;
	}

	public double getPublishedFare() {
		return publishedFare;
	}

	public void setPublishedFare(double publishedFare) {
		this.publishedFare = publishedFare;
	}

	public int getPaxType() {
		return paxType;
	}

	public void setPaxType(int paxType) {
		this.paxType = paxType;
	}

	public String getIsPnrCancelled() {
		return isPnrCancelled;
	}

	public void setIsPnrCancelled(String isPnrCancelled) {
		this.isPnrCancelled = isPnrCancelled;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getLifeInsurance() {
		return lifeInsurance;
	}

	public void setLifeInsurance(String lifeInsurance) {
		this.lifeInsurance = lifeInsurance;
	}

	public String getCheckinBag() {
		return checkinBag;
	}

	public void setCheckinBag(String checkinBag) {
		this.checkinBag = checkinBag;
	}

	public String getCheckinCabin() {
		return checkinCabin;
	}

	public void setCheckinCabin(String checkinCabin) {
		this.checkinCabin = checkinCabin;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public long getPassangerId() {
		return passangerId;
	}

	public void setPassangerId(long passangerId) {
		this.passangerId = passangerId;
	}

	public int getTripIndicator() {
		return tripIndicator;
	}

	public void setTripIndicator(int tripIndicator) {
		this.tripIndicator = tripIndicator;
	}

	public String getPassangername() {
		return passangername;
	}

	public void setPassangername(String passangername) {
		this.passangername = passangername;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSourceDestinationcode() {
		return sourceDestinationcode;
	}

	public void setSourceDestinationcode(String sourceDestinationcode) {
		this.sourceDestinationcode = sourceDestinationcode;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	
}
