package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Entity
@Table(name="versionstatus")
public class VersionStatusBean 
{
	@Id
	@Column(name="version_number")
	@SerializedName("version_number")
	@Expose
	private int version_number;
	
	@Column(name="update_type")
	@SerializedName("update_type")
	@Expose
	private String update_type;

	public int getVersion_number() {
		return version_number;
	}

	public void setVersion_number(int version_number) {
		this.version_number = version_number;
	}

	public String getUpdate_type() {
		return update_type;
	}

	public void setUpdate_type(String update_type) {
		this.update_type = update_type;
	}
	
	
}
