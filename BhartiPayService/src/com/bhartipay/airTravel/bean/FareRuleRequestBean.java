package com.bhartipay.airTravel.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareRuleRequestBean 
{
	@SerializedName("TokenId")
	@Expose
	private String tokenId;
	
	@SerializedName("TraceId")
	@Expose
	private String traceId;
	
	@SerializedName("EndUserIp")
	@Expose
	private String endUserIp;
	
	@SerializedName("ResultIndex")
	@Expose
	private String resultIndex;

	
	/*private int adultCount;
	
	private int childCount;
	
	private int infantCount;
	

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}*/

	public String getResultIndex() {
		return resultIndex;
	}

	public void setResultIndex(String resultIndex) {
		this.resultIndex = resultIndex;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getEndUserIp() {
		return endUserIp;
	}

	public void setEndUserIp(String endUserIp) {
		this.endUserIp = endUserIp;
	}
	
}
