package com.bhartipay.airTravel.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="flightmastTBU")
public class FlightTxnBean {
	
	@Id
	@Column(updatable = false, name = "bookingid", nullable = false,  length=50)
	private String bookingId;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorId;
	
	@Column( name = "userid", length=100, nullable = true)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;
	
	@Column(name = "fareamount",  length=10)
	private Double fareAmount;
	
	@Column(name = "wallettxnstatus",  length=15)
	private String wallettxnStatus;
	
	@Column(name = "status",  length=15)
	private String status;
	
	@Column(name="txndate" , columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date txnDate;
	
	@Column(name = "pnrnumber",  length=15)
	private String pnrNumber;
	
	@Column(name="ipiemi")
	private String ipIemi;
	
	@Column(name="useragent")
	private String userAgent;
	
	
	@Column(name="apiresponse",  length=4000)
	private String apiResponse;

	@Transient
	private String ErrorStatus;
	
	
	public String getUserAgent() {
		return userAgent;
	}


	public Date getTxnDate() {
		return txnDate;
	}


	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}


	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}





	public String getErrorStatus() {
		return ErrorStatus;
	}


	
	
	
	public String getApiResponse() {
		return apiResponse;
	}





	public void setApiResponse(String apiResponse) {
		this.apiResponse = apiResponse;
	}





	public void setErrorStatus(String errorStatus) {
		ErrorStatus = errorStatus;
	}


	public String getIpIemi() {
		return ipIemi;
	}


	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}


	public String getBookingId() {
		return bookingId;
	}


	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}


	


	public String getAggreatorId() {
		return aggreatorId;
	}


	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getWalletId() {
		return walletId;
	}


	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public Double getFareAmount() {
		return fareAmount;
	}


	public void setFareAmount(Double fareAmount) {
		this.fareAmount = fareAmount;
	}


	public String getWallettxnStatus() {
		return wallettxnStatus;
	}


	public void setWallettxnStatus(String wallettxnStatus) {
		this.wallettxnStatus = wallettxnStatus;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getPnrNumber() {
		return pnrNumber;
	}


	public void setPnrNumber(String pnrNumber) {
		this.pnrNumber = pnrNumber;
	}

}
