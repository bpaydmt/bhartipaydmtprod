package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="convenienceconfig")
public class ConvenienceFeeBean 
{
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="way")
	private int way;
	
	@Column(name="type")
	private String type;
	
	@Column(name="adult")
	private double adult;
	
	@Column (name="infant")
	private double infant;

	@Column(name="appliedon")
	private String appliedOn;
	
	@Column(name="status")
	private int status;
	
	@Column(name="process")
	private String process;
	


	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAppliedOn() {
		return appliedOn;
	}

	public void setAppliedOn(String appliedOn) {
		this.appliedOn = appliedOn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWay() {
		return way;
	}

	public void setWay(int way) {
		this.way = way;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getAdult() {
		return adult;
	}

	public void setAdult(double adult) {
		this.adult = adult;
	}

	public double getInfant() {
		return infant;
	}

	public void setInfant(double infant) {
		this.infant = infant;
	}
	
	
}
