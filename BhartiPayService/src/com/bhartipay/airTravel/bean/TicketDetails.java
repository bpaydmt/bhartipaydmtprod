package com.bhartipay.airTravel.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ticketdetails")
public class TicketDetails 
{
	@Id	
	@Column(name="airfareid" , length=20)
	private String airFareId;
	
	@Column(name="tripid",length=20)
	private String tripId;
	
	@Column(name="ticketResponse" , length = 9000)
	private String ticketResponse;

	@Column(name="bookingdate")
	private String bookingDate;
	
	@Column(name="way",length=20)
	private String way;
	
	@Column(name="passangerid",length=20)
	private long passangerId;
	
	@Column(name="segmentid",length=20)
	private long SegmentId;
	
	@Column(name="basefare")
	private double baseFare;
	
	@Column(name="taxes")
	private double taxes;
	
	@Column(name="passengercount" , length=50)
	private String passengerCount;
	
	@Column(name="conveniencefee")
	private double convenienceFee;
	
	@Column(name="source",length=50)
	private String source;
	
	@Column(name="destination" , length=50)
	private String destination;
	
/*	@Column(name="regulation",length=6000)
	private String regulation;*/
		
	
	public String getAirFareId() {
		return airFareId;
	}


	public String getPassengerCount() {
		return passengerCount;
	}


	public void setPassengerCount(String passengerCount) {
		this.passengerCount = passengerCount;
	}


	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getTicketResponse() {
		return ticketResponse;
	}

	public void setTicketResponse(String ticketResponse) {
		this.ticketResponse = ticketResponse;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	public long getSegmentId() {
		return SegmentId;
	}

	public void setSegmentId(long segmentId) {
		SegmentId = segmentId;
	}

	public double getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}

	public double getTaxes() {
		return taxes;
	}

	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

	public double getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(double convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

/*	public String getRegulation() {
		return regulation;
	}

	public void setRegulation(String regulation) {
		this.regulation = regulation;
	}*/

	public long getPassangerId() {
		return passangerId;
	}

	public void setPassangerId(long passangerId) {
		this.passangerId = passangerId;
	}
	
	
}
