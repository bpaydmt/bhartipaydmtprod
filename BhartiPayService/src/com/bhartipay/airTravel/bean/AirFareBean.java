package com.bhartipay.airTravel.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="airfare")
public class AirFareBean implements Serializable, Cloneable
{
	@Id
	@Column(name="airfareid", nullable=false ,length = 5)
	private String airFareId;
	
	@Column(name="tripid")
	private String tripId;	
	
	@Column(name="aggreatorid")
	private String aggreatorId;
	
	@Column(name="userid")
	private String userId;
	
	@Column(name="pgtxn")
	private String pgTxn;
	
	@Column(name="wallettxn")
	private String walletTxn;
	
	@Column(name="responsestring" , length=5000)
	private String responseString;
	
	@Column(name="totalamt"  /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double totalAmt;
	
	@Column(name="cancelconvfee"  , columnDefinition="Decimal(10,2) default '00.00'")
	private double cancelconvfee;
	
	@Column(name="publishedfare" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double publishedFare;
	
	@Column(name="offeredfare" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double offeredFare;
	
	@Column(name="conveniencefee" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double convenienceFee;
	
	@Column(name="walletamt" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double walletAmt;
	
	@Column(name="pgamt" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double pgAmt;
	
	@Column(name="totalrefundamt" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double totalRefundAmt;
	
	@Column(name="walletamtrefund" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double walletAmtRefund;
	
	@Column(name="pgamtrefund" /*, columnDefinition="Decimal(10,2) default '00.00'"*/)
	private double pgAmtRefund;
	
	@Column(name="traceid")
	private String traceId;
	
	@Column(name="appliedOn")
	private String appliedOn;
	
	@Column(name="totalfare" , nullable = false)
	private double totalFare;
	
	@Column(name="othercharges")
	private double otherCharges;
	
	@Column(name="totalbasefare")
	private double totalBaseFare;
	
	@Column(name="totaltax")
	private double totalTax;
	
	@Column(name="adultcount")
	private int adultCount;
	
	@Column(name="adultbasefare")
	private double adultBaseFare;
	
	@Column(name="adulttax")
	private double adultTax;
	
	@Column(name="childcount")
	private int childCount;
	 
	@Column(name="childbasefare")
	private double childBaseFare;
	
	@Column(name="childtax")
	private double childTax;
	
	@Column(name="infantcount")
	private int infantCount;
	
	@Column(name="source")
	private int source;
	
	@Column(name="infantbasefare")
	private double infantBaseFare;
	
	@Column(name="infanttax")
	private double infantTax;
	
	@Column(name="commissionearned")
	private double commissionEarned;
	
	@Column(name="servicetaxonraf")
	private double serviceTaxOnRAF;

	@Column(name="flight")
	private String flight;
	
	@Column(name="flightsource")
	private String flightSource;
	
	@Column(name="flightdestination")
	private String flightDestination;
	
	@Column(name="ipimei",length=30)
	private String ipimei;
	
	@Column(name="agent",length=30)
	private String agent;
	
	@Column(name="pnrno")
	private String pnrNo;
	
	@Column(name="bookingid")
	private String bookingId;
	
	@Column(name="invoiceno")
	private String invoiceNo;
	
	@Column(name="status")
	private String status;
	
	@Column(name="cancellationstatus")
	private String cancellationStatus;
	
	@Column(name="eventstatus")
	private String eventStatus;
	
	@Column(name="fareqdate" , columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date fareQdate;
	
	
	@Column(name="arrdate")
	private Date arrDate;
	
	@Column(name="tripindicator")
	private int tripIndicator;
	
	@Column(name="deptdate")
	private Date deptDate;
		
	@Column(name="resultindex")
	private String resultIndex;
	
	@Column(name="journeytype")
	private int journeyType;
	
	@Column(name="isdomestic")
	private int isDomestic;
	
	@Column(name="islcc")
	private int isLCC;
	
	@Column(name="leadpassengername" ,  length=50)
	private String passName;
	
	@Column(name="apistatus")
	private String apiStatus;
	
	@Column(name="mobileno")
	private String mobileNo ;
	
	@Column(name="walletid")
	private String walletId;
	
	@Column(name="isrefundable" , length=4)
	private int isRefundable;
	
	@Column(name="cancellationcharge" )
	private double cancellationCharge;
	
	@Column(name="cancellationrefundamount" )
	private double cancellationRefundAmount;
	
	@Column(name="cancelledpasscount" )
	private int cancelledPassCount;
	
	
	 
	public double getCancelconvfee() {
		return cancelconvfee;
	}

	public void setCancelconvfee(double cancelconvfee) {
		this.cancelconvfee = cancelconvfee;
	}

	public int getCancelledPassCount() {
		return cancelledPassCount;
	}

	public void setCancelledPassCount(int cancelledPassCount) {
		this.cancelledPassCount = cancelledPassCount;
	}

	public String getCancellationStatus() {
		return cancellationStatus;
	}

	public void setCancellationStatus(String cancellationStatus) {
		this.cancellationStatus = cancellationStatus;
	}

	public double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public String getIpimei() {
		return ipimei;
	}

	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public double getServiceTaxOnRAF() {
		return serviceTaxOnRAF;
	}

	public void setServiceTaxOnRAF(double serviceTaxOnRAF) {
		this.serviceTaxOnRAF = serviceTaxOnRAF;
	}

	public double getCancellationRefundAmount() {
		return cancellationRefundAmount;
	}

	public void setCancellationRefundAmount(double cancellationRefundAmount) {
		this.cancellationRefundAmount = cancellationRefundAmount;
	}

	public double getCancellationCharge() {
		return cancellationCharge;
	}

	public void setCancellationCharge(double cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	public int getIsRefundable() {
		return isRefundable;
	}

	public void setIsRefundable(int isRefundable) {
		this.isRefundable = isRefundable;
	}

	public Date getArrDate() {
		return arrDate;
	}

	public void setArrDate(Date arrDate) {
		this.arrDate = arrDate;
	}

	public int getTripIndicator() {
		return tripIndicator;
	}

	public void setTripIndicator(int tripIndicator) {
		this.tripIndicator = tripIndicator;
	}

	public Date getDeptDate() {
		return deptDate;
	}

	public void setDeptDate(Date deptDate) {
		this.deptDate = deptDate;
	}

	public String getWalletTxn() {
		return walletTxn;
	}

	public void setWalletTxn(String walletTxn) {
		this.walletTxn = walletTxn;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public String getAppliedOn() {
		return appliedOn;
	}

	public void setAppliedOn(String appliedOn) {
		this.appliedOn = appliedOn;
	}

	public double getTotalRefundAmt() {
		return totalRefundAmt;
	}

	public void setTotalRefundAmt(double totalRefundAmt) {
		this.totalRefundAmt = totalRefundAmt;
	}

	public double getPublishedFare() {
		return publishedFare;
	}

	public void setPublishedFare(double publishedFare) {
		this.publishedFare = publishedFare;
	}

	public double getOfferedFare() {
		return offeredFare;
	}

	public void setOfferedFare(double offeredFare) {
		this.offeredFare = offeredFare;
	}

	public double getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(double convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public double getWalletAmtRefund() {
		return walletAmtRefund;
	}

	public void setWalletAmtRefund(double walletAmtRefund) {
		this.walletAmtRefund = walletAmtRefund;
	}

	public double getPgAmtRefund() {
		return pgAmtRefund;
	}

	public void setPgAmtRefund(double pgAmtRefund) {
		this.pgAmtRefund = pgAmtRefund;
	}

	public String getPgTxn() {
		return pgTxn;
	}

	public void setPgTxn(String pgTxn) {
		this.pgTxn = pgTxn;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public double getWalletAmt() {
		return walletAmt;
	}

	public void setWalletAmt(double walletAmt) {
		this.walletAmt = walletAmt;
	}

	public double getPgAmt() {
		return pgAmt;
	}

	public void setPgAmt(double pgAmt) {
		this.pgAmt = pgAmt;
	}

	public String getPassName() {
		return passName;
	}

	public void setPassName(String passName) {
		this.passName = passName;
	}

	public String getApiStatus() {
		return apiStatus;
	}

	public void setApiStatus(String apiStatus) {
		this.apiStatus = apiStatus;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public Date getFareQdate() {
		return fareQdate;
	}

	public void setFareQdate(Date fareQdate) {
		this.fareQdate = fareQdate;
	}


	public String getResultIndex() {
		return resultIndex;
	}

	public void setResultIndex(String resultIndex) {
		this.resultIndex = resultIndex;
	}

	public int getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(int journeyType) {
		this.journeyType = journeyType;
	}

	public int getIsDomestic() {
		return isDomestic;
	}

	public void setIsDomestic(int isDomestic) {
		this.isDomestic = isDomestic;
	}

	public int getIsLCC() {
		return isLCC;
	}

	public void setIsLCC(int isLCC) {
		this.isLCC = isLCC;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPnrNo() {
		return pnrNo;
	}

	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}
	

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getFlightSource() {
		return flightSource;
	}

	public void setFlightSource(String flightSource) {
		this.flightSource = flightSource;
	}

	public String getFlightDestination() {
		return flightDestination;
	}

	public void setFlightDestination(String flightDestination) {
		this.flightDestination = flightDestination;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public double getTotalBaseFare() {
		return totalBaseFare;
	}

	public void setTotalBaseFare(double totalBaseFare) {
		this.totalBaseFare = totalBaseFare;
	}

	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public double getAdultBaseFare() {
		return adultBaseFare;
	}

	public void setAdultBaseFare(double adultBaseFare) {
		this.adultBaseFare = adultBaseFare;
	}

	public double getAdultTax() {
		return adultTax;
	}

	public void setAdultTax(double adultTax) {
		this.adultTax = adultTax;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public double getChildBaseFare() {
		return childBaseFare;
	}

	public void setChildBaseFare(double childBaseFare) {
		this.childBaseFare = childBaseFare;
	}

	public double getChildTax() {
		return childTax;
	}

	public void setChildTax(double childTax) {
		this.childTax = childTax;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public double getInfantBaseFare() {
		return infantBaseFare;
	}

	public void setInfantBaseFare(double infantBaseFare) {
		this.infantBaseFare = infantBaseFare;
	}

	public double getInfantTax() {
		return infantTax;
	}

	public void setInfantTax(double infantTax) {
		this.infantTax = infantTax;
	}

	public double getCommissionEarned() {
		return commissionEarned;
	}

	public void setCommissionEarned(double commissionEarned) {
		this.commissionEarned = commissionEarned;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}
	public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }	
	
	
	public static void main(String a[])
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
		 DateFormat f2 = new SimpleDateFormat("E, dd MMM yy HH:mm");
		 Date date = new Date();
		 try
		 {
		 System.out.println(format.format(date));
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
	}
}
