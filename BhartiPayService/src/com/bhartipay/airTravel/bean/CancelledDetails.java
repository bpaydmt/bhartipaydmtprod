package com.bhartipay.airTravel.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cancelleddetails")
public class CancelledDetails 
{
	@Id
	@Column(name="cancelid",length=20)
	private String cancelId;
	
	@Column(name="changerequestid" , length=50)
	private String changeRequestId;
	
	@Column(name="airfareid",length=20)
	private String airFareId;
	
	@Column(name="cancellationdate", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date cancellationDate;
	
	@Column(name="passengername",length=50)
	private String passengerName;
	
	@Column(name="ticketid",length=30)
	private String ticketId;

	@Column(name="refundedamount",length=30)
	private double RefundedAmount;
	
	@Column(name="cancellationcharge",length=30)
	private double CancellationCharge;
	
	@Column(name="servicetaxonraf",length=30)
	private double ServiceTaxOnRAF;
	
	@Column(name="cancelledstatus",length=2)
	private int cancelledStatus;
	
	@Column(name="cancellationtype",length=2)
	private int cancellationType;
	
	@Column(name="creditnoteno",length=30)
	private String creditNoteNo;
	
	@Column(name="creditcreatedon")
	private Date creditCreatedOn;
	
	@Column(name="responsestring",length=1000)
	private String responseString;
	
	@Column(name="conveniencefee",columnDefinition=" Decimal(10,2) default '00.00'")
	private double conveniencefee;
	
	
	
	public double getConveniencefee() {
		return conveniencefee;
	}

	public void setConveniencefee(double conveniencefee) {
		this.conveniencefee = conveniencefee;
	}

	public int getCancellationType() {
		return cancellationType;
	}

	public void setCancellationType(int cancellationType) {
		this.cancellationType = cancellationType;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public double getRefundedAmount() {
		return RefundedAmount;
	}

	public void setRefundedAmount(double refundedAmount) {
		RefundedAmount = refundedAmount;
	}

	public double getCancellationCharge() {
		return CancellationCharge;
	}

	public void setCancellationCharge(double cancellationCharge) {
		CancellationCharge = cancellationCharge;
	}

	public double getServiceTaxOnRAF() {
		return ServiceTaxOnRAF;
	}

	public void setServiceTaxOnRAF(double serviceTaxOnRAF) {
		ServiceTaxOnRAF = serviceTaxOnRAF;
	}

	public int getCancelledStatus() {
		return cancelledStatus;
	}

	public void setCancelledStatus(int cancelledStatus) {
		this.cancelledStatus = cancelledStatus;
	}

	public String getCreditNoteNo() {
		return creditNoteNo;
	}

	public void setCreditNoteNo(String creditNoteNo) {
		this.creditNoteNo = creditNoteNo;
	}

	public Date getCreditCreatedOn() {
		return creditCreatedOn;
	}

	public void setCreditCreatedOn(Date creditCreatedOn) {
		this.creditCreatedOn = creditCreatedOn;
	}

	public String getCancelId() {
		return cancelId;
	}

	public void setCancelId(String cancelId) {
		this.cancelId = cancelId;
	}

	public String getChangeRequestId() {
		return changeRequestId;
	}

	public void setChangeRequestId(String changeRequestId) {
		this.changeRequestId = changeRequestId;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	
	
}
