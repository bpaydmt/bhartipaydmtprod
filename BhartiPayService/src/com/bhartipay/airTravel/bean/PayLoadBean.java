package com.bhartipay.airTravel.bean;

import java.io.Serializable;
import java.util.List;

public class PayLoadBean implements Serializable{

	
	private List<AirportDetails> airportList;
	private List<AirportDetails> popularAirportList;
	public List<AirportDetails> getAirportList() {
		return airportList;
	}
	public void setAirportList(List<AirportDetails> airportList) {
		this.airportList = airportList;
	}
	public List<AirportDetails> getPopularAirportList() {
		return popularAirportList;
	}
	public void setPopularAirportList(List<AirportDetails> popularAirportList) {
		this.popularAirportList = popularAirportList;
	}
	
	
	
	
}
