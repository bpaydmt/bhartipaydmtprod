package com.bhartipay.airTravel.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bhartipay.airTravel.bean.flightSearch.Destination;
import com.bhartipay.airTravel.bean.flightSearch.Origin;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="ticketsegment")
public class TicketSegment implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="segmentid")
	private long segmentId;
	
	@Column(name="airfareid", length=20)
	private String airFareId;
	
	@Column(name="tripid", length=20)
	private String tripId;
	
	@Column(name="source",length=50)
	private String source;
	
	@Column(name="destination",length=50)
	private String destination;
	
	@Column(name="airlinename" ,length=50)
	private String airlineName;
	
	@Column(name="airlinecode" ,length=20)
	private String airlineCode;
	
	@Column(name="deptdate",length=30)
	private String deptdate;
	
	@Column(name="arrdate",length=30)
	private String arrdate;
	
	@Column(name="sourceairportname",length=50)
	private String sourceAirportName;
	
	@Column(name="destairportname",length=50)
	private String destAirportName;

	@Column(name="regulations",length=6000)
	private String regulations;
	
	@Column(name="airlineNumber",length=10)
	private String airlineNumber;
	
	@Column(name="sourceairportcode",length=10)
	private String sourceAirportCode;
	
	@Column(name="sourcecitycode",length=10)
	private String sourceCityCode;
	
	@Column(name="destairportcode",length=10)
	private String destAirportCode;
	
	@Column(name="destcitycode",length=10)
	private String destCityCode;
	
	
	
	
	@SerializedName("Destination")
	@Expose
	@Transient
	private  Destination dest;
	
	@Transient
	private  Origin origin;
	
	public String getSourceAirportCode() {
		return sourceAirportCode;
	}

	public void setSourceAirportCode(String sourceAirportCode) {
		this.sourceAirportCode = sourceAirportCode;
	}

	public String getSourceCityCode() {
		return sourceCityCode;
	}

	public void setSourceCityCode(String sourceCityCode) {
		this.sourceCityCode = sourceCityCode;
	}

	public String getDestAirportCode() {
		return destAirportCode;
	}

	public void setDestAirportCode(String destAirportCode) {
		this.destAirportCode = destAirportCode;
	}

	public String getDestCityCode() {
		return destCityCode;
	}

	public void setDestCityCode(String destCityCode) {
		this.destCityCode = destCityCode;
	}

	public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin;
	}

	public Destination getDest() {
		return dest;
	}

	public void setDest(Destination dest) {
		this.dest = dest;
	}

	public String getAirlineNumber() {
		return airlineNumber;
	}

	public void setAirlineNumber(String airlineNumber) {
		this.airlineNumber = airlineNumber;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public String getRegulations() {
		return regulations;
	}

	public void setRegulations(String regulations) {
		this.regulations = regulations;
	}

	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getDeptdate() {
		return deptdate;
	}

	public void setDeptdate(String deptdate) {
		this.deptdate = deptdate;
	}

	public String getArrdate() {
		return arrdate;
	}

	public void setArrdate(String arrdate) {
		this.arrdate = arrdate;
	}

	public String getSourceAirportName() {
		return sourceAirportName;
	}

	public void setSourceAirportName(String sourceAirportName) {
		this.sourceAirportName = sourceAirportName;
	}

	public String getDestAirportName() {
		return destAirportName;
	}

	public void setDestAirportName(String destAirportName) {
		this.destAirportName = destAirportName;
	}
	
}
