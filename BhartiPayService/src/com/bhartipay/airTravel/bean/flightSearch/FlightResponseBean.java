package com.bhartipay.airTravel.bean.flightSearch;

import com.bhartipay.airTravel.bean.PayLoadBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FlightResponseBean 
{
	 @SerializedName("statusCode")
	    @Expose
	 private String statusCode;
	
	 
	 @SerializedName("status")
	    @Expose
     private String status;
	 
	 @SerializedName("statusMsg")
	    @Expose
private String statusMsg;
	 
	 @SerializedName("flightSearch")
	    @Expose
 private  Example flightSearch;
 
 
 
	public Example getFlightSearch() {
	return flightSearch;
}
public void setFlightSearch(Example flightSearch) {
	this.flightSearch = flightSearch;
}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

 
 
 
}
