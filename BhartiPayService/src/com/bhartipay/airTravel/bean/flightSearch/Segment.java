
package com.bhartipay.airTravel.bean.flightSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Segment {

    @SerializedName("Baggage")
    @Expose
    private String baggage;
    @SerializedName("CabinBaggage")
    @Expose
    private Object cabinBaggage;
    @SerializedName("TripIndicator")
    @Expose
    private String tripIndicator;
    @SerializedName("SegmentIndicator")
    @Expose
    private String segmentIndicator;
    @SerializedName("Airline")
    @Expose
    private Airline airline;
    @SerializedName("NoOfSeatAvailable")
    @Expose
    private String noOfSeatAvailable;
    @SerializedName("Origin")
    @Expose
    private Origin origin;
    @SerializedName("Destination")
    @Expose
    private Destination destination;
    @SerializedName("Duration")
    @Expose
    private String duration;
    @SerializedName("GroundTime")
    @Expose
    private String groundTime;
    @SerializedName("Mile")
    @Expose
    private String mile;
    @SerializedName("StopOver")
    @Expose
    private Boolean stopOver;
    @SerializedName("StopPoint")
    @Expose
    private String stopPoint;
    @SerializedName("StopPointArrivalTime")
    @Expose
    private String stopPointArrivalTime;
    @SerializedName("StopPointDepartureTime")
    @Expose
    private String stopPointDepartureTime;
    @SerializedName("Craft")
    @Expose
    private String craft;
    @SerializedName("Remark")
    @Expose
    private Object remark;
    @SerializedName("IsETicketEligible")
    @Expose
    private Boolean isETicketEligible;
    @SerializedName("FlightStatus")
    @Expose
    private String flightStatus;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("AccumulatedDuration")
    @Expose
    private String accumulatedDuration;
    @SerializedName("AirlinePNR")
    @Expose
    private String airlinePNR;
    
    
    public String getAirlinePNR() {
		return airlinePNR;
	}

	public void setAirlinePNR(String airlinePNR) {
		this.airlinePNR = airlinePNR;
	}

	public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public Object getCabinBaggage() {
        return cabinBaggage;
    }

    public void setCabinBaggage(Object cabinBaggage) {
        this.cabinBaggage = cabinBaggage;
    }

    public String getTripIndicator() {
        return tripIndicator;
    }

    public void setTripIndicator(String tripIndicator) {
        this.tripIndicator = tripIndicator;
    }

    public String getSegmentIndicator() {
        return segmentIndicator;
    }

    public void setSegmentIndicator(String segmentIndicator) {
        this.segmentIndicator = segmentIndicator;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public String getNoOfSeatAvailable() {
        return noOfSeatAvailable;
    }

    public void setNoOfSeatAvailable(String noOfSeatAvailable) {
        this.noOfSeatAvailable = noOfSeatAvailable;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getGroundTime() {
        return groundTime;
    }

    public void setGroundTime(String groundTime) {
        this.groundTime = groundTime;
    }

    public String getMile() {
        return mile;
    }

    public void setMile(String mile) {
        this.mile = mile;
    }

    public Boolean getStopOver() {
        return stopOver;
    }

    public void setStopOver(Boolean stopOver) {
        this.stopOver = stopOver;
    }

    public String getStopPoint() {
        return stopPoint;
    }

    public void setStopPoint(String stopPoint) {
        this.stopPoint = stopPoint;
    }

    public String getStopPointArrivalTime() {
        return stopPointArrivalTime;
    }

    public void setStopPointArrivalTime(String stopPointArrivalTime) {
        this.stopPointArrivalTime = stopPointArrivalTime;
    }

    public String getStopPointDepartureTime() {
        return stopPointDepartureTime;
    }

    public void setStopPointDepartureTime(String stopPointDepartureTime) {
        this.stopPointDepartureTime = stopPointDepartureTime;
    }

    public String getCraft() {
        return craft;
    }

    public void setCraft(String craft) {
        this.craft = craft;
    }

    public Object getRemark() {
        return remark;
    }

    public void setRemark(Object remark) {
        this.remark = remark;
    }

    public Boolean getIsETicketEligible() {
        return isETicketEligible;
    }

    public void setIsETicketEligible(Boolean isETicketEligible) {
        this.isETicketEligible = isETicketEligible;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccumulatedDuration() {
        return accumulatedDuration;
    }

    public void setAccumulatedDuration(String accumulatedDuration) {
        this.accumulatedDuration = accumulatedDuration;
    }

}
