package com.bhartipay.airTravel.bean;

public class AuthenticationResponseBean 
{
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private int memberId;
	
	private int AgencyId;
	
	private String loginName;
	
	private String loginDetails;
	
	private String tokenId;
	
	private int status;
	
	private ResponseBean error;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public int getAgencyId() {
		return AgencyId;
	}

	public void setAgencyId(int agencyId) {
		AgencyId = agencyId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginDetails() {
		return loginDetails;
	}

	public void setLoginDetails(String loginDetails) {
		this.loginDetails = loginDetails;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ResponseBean getError() {
		return error;
	}

	public void setError(ResponseBean error) {
		this.error = error;
	}
}
