package com.bhartipay.airTravel.bean;

public class Segment   
{
	private String Origin;
	
	private String Destination;
	
	private String FlightCabinClass;
	
	private String PreferredDepartureTime;
	
	private String PreferredArrivalTime;

	public String getOrigin() {
		return Origin;
	}

	public void setOrigin(String origin) {
		Origin = origin;
	}

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public String getFlightCabinClass() {
		return FlightCabinClass;
	}

	public void setFlightCabinClass(String flightCabinClass) {
		FlightCabinClass = flightCabinClass;
	}

	public String getPreferredDepartureTime() {
		return PreferredDepartureTime;
	}

	public void setPreferredDepartureTime(String preferredDepartureTime) {
		PreferredDepartureTime = preferredDepartureTime;
	}

	public String getPreferredArrivalTime() {
		return PreferredArrivalTime;
	}

	public void setPreferredArrivalTime(String preferredArrivalTime) {
		PreferredArrivalTime = preferredArrivalTime;
	}
	
	
}
