package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "airuserauth")
public class AuthenticationRequestBean 
{
	@Id
	@SerializedName("ClientId")
	@Expose
	@Column(name = "clientid" )
	private String clientId;
	
	@SerializedName("UserName")
	@Expose
	@Column(name = "username" )
	private String userName;
	
	@SerializedName("Password")
	@Expose
	@Column(name = "password" )
	private String password;
	
	@SerializedName("EndUserIp")
	@Expose
	@Column(name = "enduserip" )
	private String endUserIp;

	@SerializedName("ApiName")
	@Expose
	@Column(name = "api")
	private String apiName;
	
	@SerializedName("TokenId")
	@Expose
	@Column(name="tokenid")
	private String tokenId;
	
	
	
	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEndUserIp() {
		return endUserIp;
	}

	public void setEndUserIp(String endUserIp) {
		this.endUserIp = endUserIp;
	}
	
}
