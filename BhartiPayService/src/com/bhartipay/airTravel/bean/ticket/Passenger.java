
package com.bhartipay.airTravel.bean.ticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("PaxType")
    @Expose
    private Integer paxType;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("PassportNo")
    @Expose
    private String passportNo;
    @SerializedName("PassportExpiry")
    @Expose
    private String passportExpiry;
    @SerializedName("AddressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("AddressLine2")
    @Expose
    private String addressLine2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("CountryName")
    @Expose
    private String countryName;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("IsLeadPax")
    @Expose
    private Boolean isLeadPax;
    @SerializedName("FFAirline")
    @Expose
    private String fFAirline;
    @SerializedName("FFNumber")
    @Expose
    private String fFNumber;
    @SerializedName("Fare")
    @Expose
    private Fare fare;
    @SerializedName("MealDynamic")
    @Expose
    private List<MealDynamic> mealDynamic = null;
    @SerializedName("Baggage")
    @Expose
    private List<Baggage> baggage = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPaxType() {
        return paxType;
    }

    public void setPaxType(Integer paxType) {
        this.paxType = paxType;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportExpiry() {
        return passportExpiry;
    }

    public void setPassportExpiry(String passportExpiry) {
        this.passportExpiry = passportExpiry;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsLeadPax() {
        return isLeadPax;
    }

    public void setIsLeadPax(Boolean isLeadPax) {
        this.isLeadPax = isLeadPax;
    }

    public String getFFAirline() {
        return fFAirline;
    }

    public void setFFAirline(String fFAirline) {
        this.fFAirline = fFAirline;
    }

    public String getFFNumber() {
        return fFNumber;
    }

    public void setFFNumber(String fFNumber) {
        this.fFNumber = fFNumber;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public List<MealDynamic> getMealDynamic() {
        return mealDynamic;
    }

    public void setMealDynamic(List<MealDynamic> mealDynamic) {
        this.mealDynamic = mealDynamic;
    }

    public List<Baggage> getBaggage() {
        return baggage;
    }

    public void setBaggage(List<Baggage> baggage) {
        this.baggage = baggage;
    }

}
