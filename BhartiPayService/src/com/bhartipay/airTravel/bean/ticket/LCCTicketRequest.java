
package com.bhartipay.airTravel.bean.ticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LCCTicketRequest {

    @SerializedName("PreferredCurrency")
    @Expose
    private String preferredCurrency;
    @SerializedName("IsBaseCurrencyRequired")
    @Expose
    private String isBaseCurrencyRequired;
    @SerializedName("EndUserIp")
    @Expose
    private String endUserIp;
    @SerializedName("TokenId")
    @Expose
    private String tokenId;
    @SerializedName("TraceId")
    @Expose
    private String traceId;
    @SerializedName("ResultIndex")
    @Expose
    private String resultIndex;
    @SerializedName("Passengers")
    @Expose
    private List<Passenger> passengers = null;

    public String getPreferredCurrency() {
        return preferredCurrency;
    }

    public void setPreferredCurrency(String preferredCurrency) {
        this.preferredCurrency = preferredCurrency;
    }

    public String getIsBaseCurrencyRequired() {
        return isBaseCurrencyRequired;
    }

    public void setIsBaseCurrencyRequired(String isBaseCurrencyRequired) {
        this.isBaseCurrencyRequired = isBaseCurrencyRequired;
    }

    public String getEndUserIp() {
        return endUserIp;
    }

    public void setEndUserIp(String endUserIp) {
        this.endUserIp = endUserIp;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getResultIndex() {
        return resultIndex;
    }

    public void setResultIndex(String resultIndex) {
        this.resultIndex = resultIndex;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

}
