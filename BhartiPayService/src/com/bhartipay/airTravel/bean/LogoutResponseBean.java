package com.bhartipay.airTravel.bean;

public class LogoutResponseBean 
{
	private int status ;
	
	private ResponseBean error;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ResponseBean getError() {
		return error;
	}

	public void setError(ResponseBean error) {
		this.error = error;
	}
}
