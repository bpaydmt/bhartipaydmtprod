
package com.bhartipay.airTravel.bean.travelRule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareRule {

    @SerializedName("Airline")
    @Expose
    private String airline;
    @SerializedName("Destination")
    @Expose
    private String destination;
    @SerializedName("FareBasisCode")
    @Expose
    private String fareBasisCode;
    @SerializedName("FareRestriction")
    @Expose
    private Object fareRestriction;
    @SerializedName("FareRuleDetail")
    @Expose
    private String fareRuleDetail;
    @SerializedName("Origin")
    @Expose
    private String origin;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFareBasisCode() {
        return fareBasisCode;
    }

    public void setFareBasisCode(String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }

    public Object getFareRestriction() {
        return fareRestriction;
    }

    public void setFareRestriction(Object fareRestriction) {
        this.fareRestriction = fareRestriction;
    }

    public String getFareRuleDetail() {
        return fareRuleDetail;
    }

    public void setFareRuleDetail(String fareRuleDetail) {
        this.fareRuleDetail = fareRuleDetail;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

}
