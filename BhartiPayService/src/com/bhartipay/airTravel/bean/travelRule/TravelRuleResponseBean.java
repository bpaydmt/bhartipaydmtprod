package com.bhartipay.airTravel.bean.travelRule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TravelRuleResponseBean 
{

	@SerializedName("status")
	@Expose
	private String status;
	
	@SerializedName("statusCode")
	@Expose
	private String statusCode;
	
	@SerializedName("statusMsg")
	@Expose
	private String statusMsg;
    
	@SerializedName("fareRule")
    @Expose
    private TravelRuleResponse fareRule;

    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public TravelRuleResponse getFareRule() {
		return fareRule;
	}

	public void setFareRule(TravelRuleResponse fareRule) {
		this.fareRule = fareRule;
	}

	

}
