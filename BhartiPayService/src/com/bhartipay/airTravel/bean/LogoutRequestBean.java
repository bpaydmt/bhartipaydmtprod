package com.bhartipay.airTravel.bean;

public class LogoutRequestBean 
{
	private String clientId;
	
	private String tokenAgencyId;
	
	private String tokenMemberId;
	
	private String endUserIp ;
	
	private String tokenId ;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTokenAgencyId() {
		return tokenAgencyId;
	}

	public void setTokenAgencyId(String tokenAgencyId) {
		this.tokenAgencyId = tokenAgencyId;
	}

	public String getTokenMemberId() {
		return tokenMemberId;
	}

	public void setTokenMemberId(String tokenMemberId) {
		this.tokenMemberId = tokenMemberId;
	}

	public String getEndUserIp() {
		return endUserIp;
	}

	public void setEndUserIp(String endUserIp) {
		this.endUserIp = endUserIp;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
}
