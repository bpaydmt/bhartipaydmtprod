
package com.bhartipay.airTravel.bean.booking;

import com.bhartipay.airTravel.bean.flightSearch.Error;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("Error")
    @Expose
    private Error error;
    @SerializedName("Response")
    @Expose
    private Response_ response;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Response_ getResponse() {
        return response;
    }

    public void setResponse(Response_ response) {
        this.response = response;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    @SerializedName("TicketCRInfo")
    @Expose
    private List<TicketCRInfo> ticketCRInfo = null;
    @SerializedName("ResponseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("TraceId")
    @Expose
    private String traceId;

    public List<TicketCRInfo> getTicketCRInfo() {
        return ticketCRInfo;
    }

    public void setTicketCRInfo(List<TicketCRInfo> ticketCRInfo) {
        this.ticketCRInfo = ticketCRInfo;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }


}
