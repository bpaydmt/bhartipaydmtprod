package com.bhartipay.airTravel.bean.booking;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingResponseBean 
{
 @SerializedName("statusCode")
    @Expose
    private String statusCode;

 @SerializedName("Response")
 @Expose
 private com.bhartipay.airTravel.bean.Response response;
 
 @SerializedName("status")
    @Expose
    private String status;
 
 @SerializedName("amount")
    @Expose
    private double amount;
 
 @SerializedName("travelId")
    @Expose
    private String travelId;
 
 @SerializedName("statusMsg")
    @Expose
    private String statusMsg;
 
 @SerializedName("flightBooking")
    @Expose
    private FlightBookingResp flightBooking;
 
 @SerializedName("bookingDetails")
    @Expose
    private FlightBookingResp bookingDetails;

 @SerializedName("getBookingDetails")
 @Expose
 private FlightBookingResp getBookingDetails;
 
 @SerializedName("flightBookingList")
    @Expose
    private List<FlightBookingResp> flightBookingList;
 
 @SerializedName("cancellationResp")
 @Expose
 private FlightBookingResp cancellationResp;
 
 @SerializedName("version")
    @Expose
    private String version;

	
	private double flight1TotalFare;
	 
	 private double flight1ConvienceFee;
	 
	 private double flight2TotalFare;
	 
	 private double flight2ConvienceFee;
	 
	 private double cancelRefundAmount;
	

	public double getCancelRefundAmount() {
		return cancelRefundAmount;
	}

	public void setCancelRefundAmount(double cancelRefundAmount) {
		this.cancelRefundAmount = cancelRefundAmount;
	}

	public double getFlight1TotalFare() {
		return flight1TotalFare;
	}

	public void setFlight1TotalFare(double flight1TotalFare) {
		this.flight1TotalFare = flight1TotalFare;
	}

	public double getFlight1ConvienceFee() {
		return flight1ConvienceFee;
	}

	public void setFlight1ConvienceFee(double flight1ConvienceFee) {
		this.flight1ConvienceFee = flight1ConvienceFee;
	}

	public double getFlight2TotalFare() {
		return flight2TotalFare;
	}

	public void setFlight2TotalFare(double flight2TotalFare) {
		this.flight2TotalFare = flight2TotalFare;
	}

	public double getFlight2ConvienceFee() {
		return flight2ConvienceFee;
	}

	public void setFlight2ConvienceFee(double flight2ConvienceFee) {
		this.flight2ConvienceFee = flight2ConvienceFee;
	}

	public com.bhartipay.airTravel.bean.Response getResponse() {
		return response;
	}

	public void setResponse(com.bhartipay.airTravel.bean.Response response) {
		this.response = response;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public FlightBookingResp getCancellationResp() {
		return cancellationResp;
	}

	public void setCancellationResp(FlightBookingResp cancellationResp) {
		this.cancellationResp = cancellationResp;
	}

	public List<FlightBookingResp> getFlightBookingList() {
		return flightBookingList;
	}

	public void setFlightBookingList(List<FlightBookingResp> flightBookingList) {
		this.flightBookingList = flightBookingList;
	}

	public FlightBookingResp getGetBookingDetails() {
		return getBookingDetails;
	}

	public void setGetBookingDetails(FlightBookingResp getBookingDetails) {
		this.getBookingDetails = getBookingDetails;
	}

	public FlightBookingResp getBookingDetails() {
		return bookingDetails;
	}

	public void setBookingDetails(FlightBookingResp bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

	public FlightBookingResp getFlightBooking() {
		return flightBooking;
	}

	public void setFlightBooking(FlightBookingResp flightBooking) {
		this.flightBooking = flightBooking;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

 
}