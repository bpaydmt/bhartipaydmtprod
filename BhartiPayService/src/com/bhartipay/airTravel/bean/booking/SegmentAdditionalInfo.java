package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SegmentAdditionalInfo {

@SerializedName("FareBasis")
@Expose
private String fareBasis;
@SerializedName("NVA")
@Expose
private Object nVA;
@SerializedName("NVB")
@Expose
private Object nVB;
@SerializedName("Baggage")
@Expose
private String baggage;
@SerializedName("Meal")
@Expose
private String meal;

public String getFareBasis() {
return fareBasis;
}

public void setFareBasis(String fareBasis) {
this.fareBasis = fareBasis;
}

public Object getNVA() {
return nVA;
}

public void setNVA(Object nVA) {
this.nVA = nVA;
}

public Object getNVB() {
return nVB;
}

public void setNVB(Object nVB) {
this.nVB = nVB;
}

public String getBaggage() {
return baggage;
}

public void setBaggage(String baggage) {
this.baggage = baggage;
}

public String getMeal() {
return meal;
}

public void setMeal(String meal) {
this.meal = meal;
}

}
