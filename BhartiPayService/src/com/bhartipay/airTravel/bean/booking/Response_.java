
package com.bhartipay.airTravel.bean.booking;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response_ {

    @SerializedName("PNR")
    @Expose
    private String pNR;
    @SerializedName("BookingId")
    @Expose
    private Long bookingId;
    @SerializedName("SSRDenied")
    @Expose
    private Boolean sSRDenied;
    @SerializedName("SSRMessage")
    @Expose
    private Object sSRMessage;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("IsPriceChanged")
    @Expose
    private Boolean isPriceChanged;
    @SerializedName("IsTimeChanged")
    @Expose
    private Boolean isTimeChanged;
    @SerializedName("FlightItinerary")
    @Expose
    private FlightItinerary flightItinerary;
    
    @SerializedName("TicketCRInfo")
    @Expose
    private List<TicketCRInfo> ticketCRInfo = null;
    @SerializedName("ResponseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("TraceId")
    @Expose
    private String traceId;
    @SerializedName("TicketStatus")
    @Expose
    private int ticketStatus;
    
    public int getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public List<TicketCRInfo> getTicketCRInfo() {
        return ticketCRInfo;
    }

    public void setTicketCRInfo(List<TicketCRInfo> ticketCRInfo) {
        this.ticketCRInfo = ticketCRInfo;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    
    
    
    
    
    
    
    
    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPNR() {
        return pNR;
    }

    public void setPNR(String pNR) {
        this.pNR = pNR;
    }

 

    public String getpNR() {
		return pNR;
	}

	public void setpNR(String pNR) {
		this.pNR = pNR;
	}

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public Boolean getsSRDenied() {
		return sSRDenied;
	}

	public void setsSRDenied(Boolean sSRDenied) {
		this.sSRDenied = sSRDenied;
	}

	public Object getsSRMessage() {
		return sSRMessage;
	}

	public void setsSRMessage(Object sSRMessage) {
		this.sSRMessage = sSRMessage;
	}

	public Boolean getSSRDenied() {
        return sSRDenied;
    }

    public void setSSRDenied(Boolean sSRDenied) {
        this.sSRDenied = sSRDenied;
    }

    public Object getSSRMessage() {
        return sSRMessage;
    }

    public void setSSRMessage(Object sSRMessage) {
        this.sSRMessage = sSRMessage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getIsPriceChanged() {
        return isPriceChanged;
    }

    public void setIsPriceChanged(Boolean isPriceChanged) {
        this.isPriceChanged = isPriceChanged;
    }

    public Boolean getIsTimeChanged() {
        return isTimeChanged;
    }

    public void setIsTimeChanged(Boolean isTimeChanged) {
        this.isTimeChanged = isTimeChanged;
    }

    public FlightItinerary getFlightItinerary() {
        return flightItinerary;
    }

    public void setFlightItinerary(FlightItinerary flightItinerary) {
        this.flightItinerary = flightItinerary;
    }

}
