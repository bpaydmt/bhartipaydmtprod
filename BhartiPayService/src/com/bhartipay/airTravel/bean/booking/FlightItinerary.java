
package com.bhartipay.airTravel.bean.booking;

import java.util.List;

import com.bhartipay.airTravel.bean.flightSearch.Fare;
import com.bhartipay.airTravel.bean.flightSearch.FareRule;
import com.bhartipay.airTravel.bean.flightSearch.Segment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightItinerary {

    @SerializedName("BookingId")
    @Expose
    private String bookingId;
    @SerializedName("PNR")
    @Expose
    private String pNR;
    @SerializedName("IsManual")
    @Expose
    private Boolean isManual;
    @SerializedName("IsDomestic")
    @Expose
    private Boolean isDomestic;
    @SerializedName("Source")
    @Expose
    private Integer source;
    @SerializedName("Origin")
    @Expose
    private String origin;
    @SerializedName("Destination")
    @Expose
    private String destination;
    @SerializedName("AirlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("ValidatingAirlineCode")
    @Expose
    private String validatingAirlineCode;
    @SerializedName("AirlineRemark")
    @Expose
    private String airlineRemark;
    @SerializedName("IsLCC")
    @Expose
    private Boolean isLCC;
    @SerializedName("NonRefundable")
    @Expose
    private Boolean nonRefundable;
    @SerializedName("FareType")
    @Expose
    private String fareType;
    @SerializedName("LastTicketDate")
    @Expose
    private String lastTicketDate;
    @SerializedName("Fare")
    @Expose
    private Fare fare;
    @SerializedName("Passenger")
    @Expose
    private List<Passenger> passenger = null;
    @SerializedName("Segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("FareRules")
    @Expose
    private List<FareRule> fareRules = null;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("ResponseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("TraceId")
    @Expose
    private String traceId;
    @SerializedName("InvoiceNo")
    @Expose
    private String invoiceNo;
    @SerializedName("CreditNoteCreatedOn")
    @Expose
    private Object creditNoteCreatedOn;
   
    @SerializedName("CancellationCharges")
    @Expose
    private Object cancellationCharges;
  
    @SerializedName("CreditNoteNo")
    @Expose
    private Object creditNoteNo;
    
    
    public Boolean getIsManual() {
		return isManual;
	}

	public void setIsManual(Boolean isManual) {
		this.isManual = isManual;
	}

	public String getLastTicketDate() {
		return lastTicketDate;
	}

	public void setLastTicketDate(String lastTicketDate) {
		this.lastTicketDate = lastTicketDate;
	}

	public Object getCreditNoteCreatedOn() {
		return creditNoteCreatedOn;
	}

	public void setCreditNoteCreatedOn(Object creditNoteCreatedOn) {
		this.creditNoteCreatedOn = creditNoteCreatedOn;
	}

	public Object getCancellationCharges() {
		return cancellationCharges;
	}

	public void setCancellationCharges(Object cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}

	public Object getCreditNoteNo() {
		return creditNoteNo;
	}

	public void setCreditNoteNo(Object creditNoteNo) {
		this.creditNoteNo = creditNoteNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceCreatedOn() {
		return invoiceCreatedOn;
	}

	public void setInvoiceCreatedOn(String invoiceCreatedOn) {
		this.invoiceCreatedOn = invoiceCreatedOn;
	}

	@SerializedName("InvoiceCreatedOn")
    @Expose
    private String invoiceCreatedOn;
    
    
   

    public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getpNR() {
		return pNR;
	}

	public void setpNR(String pNR) {
		this.pNR = pNR;
	}

	public String getPNR() {
        return pNR;
    }

    public void setPNR(String pNR) {
        this.pNR = pNR;
    }

    public Boolean getIsDomestic() {
        return isDomestic;
    }

    public void setIsDomestic(Boolean isDomestic) {
        this.isDomestic = isDomestic;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getValidatingAirlineCode() {
        return validatingAirlineCode;
    }

    public void setValidatingAirlineCode(String validatingAirlineCode) {
        this.validatingAirlineCode = validatingAirlineCode;
    }

    public String getAirlineRemark() {
        return airlineRemark;
    }

    public void setAirlineRemark(String airlineRemark) {
        this.airlineRemark = airlineRemark;
    }

    public Boolean getIsLCC() {
        return isLCC;
    }

    public void setIsLCC(Boolean isLCC) {
        this.isLCC = isLCC;
    }

    public Boolean getNonRefundable() {
        return nonRefundable;
    }

    public void setNonRefundable(Boolean nonRefundable) {
        this.nonRefundable = nonRefundable;
    }

    public String getFareType() {
        return fareType;
    }

    public void setFareType(String fareType) {
        this.fareType = fareType;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public List<Passenger> getPassenger() {
        return passenger;
    }

    public void setPassenger(List<Passenger> passenger) {
        this.passenger = passenger;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public List<FareRule> getFareRules() {
        return fareRules;
    }

    public void setFareRules(List<FareRule> fareRules) {
        this.fareRules = fareRules;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

}
