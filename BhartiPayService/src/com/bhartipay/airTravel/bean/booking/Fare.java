
package com.bhartipay.airTravel.bean.booking;

import java.util.List;

import com.bhartipay.airTravel.bean.flightSearch.ChargeBU;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fare {

    @SerializedName("BaseFare")
    @Expose
    private double baseFare;
    @SerializedName("Tax")
    @Expose
    private double tax;
    @SerializedName("TransactionFee")
    @Expose
    private double transactionFee;
    @SerializedName("YQTax")
    @Expose
    private double yQTax;
    @SerializedName("AdditionalTxnFeeOfrd")
    @Expose
    private double additionalTxnFeeOfrd;
    @SerializedName("AdditionalTxnFeePub")
    @Expose
    private double additionalTxnFeePub;
    @SerializedName("AirTransFee")
    @Expose
    private double airTransFee;
    @SerializedName("AdditionalTxnFee")
    @Expose
    private double additionalTxnFee;
    
    
    @SerializedName("Currency")
    @Expose
    private String currency;

    @SerializedName("TaxBreakup")
    @Expose
    private Object taxBreakup;
   
    @SerializedName("PGCharge")
    @Expose
    private Integer pGCharge;
    @SerializedName("OtherCharges")
    @Expose
    private Integer otherCharges;
    @SerializedName("ChargeBU")
    @Expose
    private List<ChargeBU> chargeBU = null;
    @SerializedName("Discount")
    @Expose
    private Integer discount;
    @SerializedName("PublishedFare")
    @Expose
    private Integer publishedFare;
    @SerializedName("CommissionEarned")
    @Expose
    private Double commissionEarned;
    @SerializedName("PLBEarned")
    @Expose
    private Integer pLBEarned;
    @SerializedName("IncentiveEarned")
    @Expose
    private Integer incentiveEarned;
    @SerializedName("OfferedFare")
    @Expose
    private Double offeredFare;
    @SerializedName("TdsOnCommission")
    @Expose
    private Double tdsOnCommission;
    @SerializedName("TdsOnPLB")
    @Expose
    private Integer tdsOnPLB;
    @SerializedName("TdsOnIncentive")
    @Expose
    private Integer tdsOnIncentive;
    @SerializedName("ServiceFee")
    @Expose
    private Integer serviceFee;
    @SerializedName("TotalBaggageCharges")
    @Expose
    private Integer totalBaggageCharges;
    @SerializedName("TotalMealCharges")
    @Expose
    private Integer totalMealCharges;
    @SerializedName("TotalSeatCharges")
    @Expose
    private Integer totalSeatCharges;
    @SerializedName("TotalSpecialServiceCharges")
    @Expose
    private Integer totalSpecialServiceCharges;

    
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Object getTaxBreakup() {
		return taxBreakup;
	}
	public void setTaxBreakup(Object taxBreakup) {
		this.taxBreakup = taxBreakup;
	}
	public Integer getpGCharge() {
		return pGCharge;
	}
	public void setpGCharge(Integer pGCharge) {
		this.pGCharge = pGCharge;
	}
	public Integer getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(Integer otherCharges) {
		this.otherCharges = otherCharges;
	}
	public List<ChargeBU> getChargeBU() {
		return chargeBU;
	}
	public void setChargeBU(List<ChargeBU> chargeBU) {
		this.chargeBU = chargeBU;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	public Integer getPublishedFare() {
		return publishedFare;
	}
	public void setPublishedFare(Integer publishedFare) {
		this.publishedFare = publishedFare;
	}
	public Double getCommissionEarned() {
		return commissionEarned;
	}
	public void setCommissionEarned(Double commissionEarned) {
		this.commissionEarned = commissionEarned;
	}
	public Integer getpLBEarned() {
		return pLBEarned;
	}
	public void setpLBEarned(Integer pLBEarned) {
		this.pLBEarned = pLBEarned;
	}
	public Integer getIncentiveEarned() {
		return incentiveEarned;
	}
	public void setIncentiveEarned(Integer incentiveEarned) {
		this.incentiveEarned = incentiveEarned;
	}
	public Double getOfferedFare() {
		return offeredFare;
	}
	public void setOfferedFare(Double offeredFare) {
		this.offeredFare = offeredFare;
	}
	public Double getTdsOnCommission() {
		return tdsOnCommission;
	}
	public void setTdsOnCommission(Double tdsOnCommission) {
		this.tdsOnCommission = tdsOnCommission;
	}
	public Integer getTdsOnPLB() {
		return tdsOnPLB;
	}
	public void setTdsOnPLB(Integer tdsOnPLB) {
		this.tdsOnPLB = tdsOnPLB;
	}
	public Integer getTdsOnIncentive() {
		return tdsOnIncentive;
	}
	public void setTdsOnIncentive(Integer tdsOnIncentive) {
		this.tdsOnIncentive = tdsOnIncentive;
	}
	public Integer getServiceFee() {
		return serviceFee;
	}
	public void setServiceFee(Integer serviceFee) {
		this.serviceFee = serviceFee;
	}
	public Integer getTotalBaggageCharges() {
		return totalBaggageCharges;
	}
	public void setTotalBaggageCharges(Integer totalBaggageCharges) {
		this.totalBaggageCharges = totalBaggageCharges;
	}
	public Integer getTotalMealCharges() {
		return totalMealCharges;
	}
	public void setTotalMealCharges(Integer totalMealCharges) {
		this.totalMealCharges = totalMealCharges;
	}
	public Integer getTotalSeatCharges() {
		return totalSeatCharges;
	}
	public void setTotalSeatCharges(Integer totalSeatCharges) {
		this.totalSeatCharges = totalSeatCharges;
	}
	public Integer getTotalSpecialServiceCharges() {
		return totalSpecialServiceCharges;
	}
	public void setTotalSpecialServiceCharges(Integer totalSpecialServiceCharges) {
		this.totalSpecialServiceCharges = totalSpecialServiceCharges;
	}
	public double getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getTransactionFee() {
		return transactionFee;
	}
	public void setTransactionFee(double transactionFee) {
		this.transactionFee = transactionFee;
	}
	public double getyQTax() {
		return yQTax;
	}
	public void setyQTax(double yQTax) {
		this.yQTax = yQTax;
	}
	public double getAdditionalTxnFeeOfrd() {
		return additionalTxnFeeOfrd;
	}
	public void setAdditionalTxnFeeOfrd(double additionalTxnFeeOfrd) {
		this.additionalTxnFeeOfrd = additionalTxnFeeOfrd;
	}
	public double getAdditionalTxnFeePub() {
		return additionalTxnFeePub;
	}
	public void setAdditionalTxnFeePub(double additionalTxnFeePub) {
		this.additionalTxnFeePub = additionalTxnFeePub;
	}
	public double getAirTransFee() {
		return airTransFee;
	}
	public void setAirTransFee(double airTransFee) {
		this.airTransFee = airTransFee;
	}
	public double getAdditionalTxnFee() {
		return additionalTxnFee;
	}
	public void setAdditionalTxnFee(double additionalTxnFee) {
		this.additionalTxnFee = additionalTxnFee;
	}


}
