
package com.bhartipay.airTravel.bean.booking;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingRequest implements Cloneable {

	@SerializedName("TravelId")
    @Expose
    private String travelId;
    @SerializedName("EndUserIp")
    @Expose
    private String endUserIp;
    @SerializedName("TokenId")
    @Expose
    private String tokenId;
    @SerializedName("TraceId")
    @Expose
    private String traceId;
    @SerializedName("ResultIndex")
    @Expose
    private String resultIndex;
    @SerializedName("Passengers")
    @Expose
    private List<Passenger> passengers = null;

    
    public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public String getEndUserIp() {
        return endUserIp;
    }

    public void setEndUserIp(String endUserIp) {
        this.endUserIp = endUserIp;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getResultIndex() {
        return resultIndex;
    }

    public void setResultIndex(String resultIndex) {
        this.resultIndex = resultIndex;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }
    
	public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }	
	

}
