package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBookingDetailResponse 
{
	@SerializedName("Error")
	@Expose
	private Error error;
	@SerializedName("ResponseStatus")
	@Expose
	private Integer responseStatus;
	@SerializedName("TraceId")
	@Expose
	private String traceId;
	@SerializedName("FlightItinerary")
	@Expose
	private FlightItinerary flightItinerary;

	public Error getError() {
	return error;
	}

	public void setError(Error error) {
	this.error = error;
	}

	public Integer getResponseStatus() {
	return responseStatus;
	}

	public void setResponseStatus(Integer responseStatus) {
	this.responseStatus = responseStatus;
	}

	public String getTraceId() {
	return traceId;
	}

	public void setTraceId(String traceId) {
	this.traceId = traceId;
	}

	public FlightItinerary getFlightItinerary() {
	return flightItinerary;
	}

	public void setFlightItinerary(FlightItinerary flightItinerary) {
	this.flightItinerary = flightItinerary;
	}
}
