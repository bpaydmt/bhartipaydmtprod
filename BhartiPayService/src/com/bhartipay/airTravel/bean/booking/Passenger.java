
package com.bhartipay.airTravel.bean.booking;

import java.util.List;

import com.bhartipay.airTravel.bean.flightSearch.Fare;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger implements Cloneable {

	@SerializedName("PaxId")
	@Expose
	private Integer paxId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("PaxType")
    @Expose
    private Integer paxType;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("PassportNo")
    @Expose
    private String passportNo;
    @SerializedName("PassportExpiry")
    @Expose
    private String passportExpiry;
    @SerializedName("AddressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("AddressLine2")
    @Expose
    private String addressLine2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("CountryName")
    @Expose
    private String countryName;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("IsLeadPax")
    @Expose
    private Boolean isLeadPax;
    @SerializedName("FFAirline")
    @Expose
    private String fFAirline;
    @SerializedName("FFNumber")
    @Expose
    private String fFNumber;
    @SerializedName("Fare")
    @Expose
    private Fare fare;
    @SerializedName("FareList")
    @Expose
    private List<Fare> fareList;
    @SerializedName("Meal")
    @Expose
    private Meal meal;
    @SerializedName("Seat")
    @Expose
    private Seat seat;
    @SerializedName("MealDynamic")
    @Expose
    private List<MealDynamic> mealDynamic = null;
    @SerializedName("Baggage")
    @Expose
    private List<Baggage> baggage = null;
    @SerializedName("Ticket")
    @Expose
    private Ticket ticket;
    @SerializedName("SegmentAdditionalInfo")
    @Expose
    private List<SegmentAdditionalInfo> segmentAdditionalInfo = null;
    @SerializedName("Nationality")
    @Expose
    private String nationality;
    @SerializedName("FFAirlineCode")
    @Expose
    private Object fFAirlineCode;
    
    
    
   

	public List<Fare> getFareList() {
		return fareList;
	}

	public void setFareList(List<Fare> fareList) {
		this.fareList = fareList;
	}

	public Integer getPaxId() {
		return paxId;
	}

	public void setPaxId(Integer paxId) {
		this.paxId = paxId;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public List<SegmentAdditionalInfo> getSegmentAdditionalInfo() {
		return segmentAdditionalInfo;
	}

	public void setSegmentAdditionalInfo(List<SegmentAdditionalInfo> segmentAdditionalInfo) {
		this.segmentAdditionalInfo = segmentAdditionalInfo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Object getfFAirlineCode() {
		return fFAirlineCode;
	}

	public void setfFAirlineCode(Object fFAirlineCode) {
		this.fFAirlineCode = fFAirlineCode;
	}

	public String getfFAirline() {
		return fFAirline;
	}

	public void setfFAirline(String fFAirline) {
		this.fFAirline = fFAirline;
	}

	public String getfFNumber() {
		return fFNumber;
	}

	public void setfFNumber(String fFNumber) {
		this.fFNumber = fFNumber;
	}

	public List<MealDynamic> getMealDynamic() {
		return mealDynamic;
	}

	public void setMealDynamic(List<MealDynamic> mealDynamic) {
		this.mealDynamic = mealDynamic;
	}

	public List<Baggage> getBaggage() {
		return baggage;
	}

	public void setBaggage(List<Baggage> baggage) {
		this.baggage = baggage;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPaxType() {
        return paxType;
    }

    public void setPaxType(Integer paxType) {
        this.paxType = paxType;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportExpiry() {
        return passportExpiry;
    }

    public void setPassportExpiry(String passportExpiry) {
        this.passportExpiry = passportExpiry;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsLeadPax() {
        return isLeadPax;
    }

    public void setIsLeadPax(Boolean isLeadPax) {
        this.isLeadPax = isLeadPax;
    }

    public String getFFAirline() {
        return fFAirline;
    }

    public void setFFAirline(String fFAirline) {
        this.fFAirline = fFAirline;
    }

    public String getFFNumber() {
        return fFNumber;
    }

    public void setFFNumber(String fFNumber) {
        this.fFNumber = fFNumber;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }	
}
