package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBookingDetailResp 
{
	@SerializedName("Response")
	@Expose
	private GetBookingDetailResponse response;

	public GetBookingDetailResponse getResponse() {
	return response;
	}

	public void setResponse(GetBookingDetailResponse response) {
	this.response = response;
	}
}
