
package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketCRInfo {

@SerializedName("ChangeRequestId")
@Expose
private Integer changeRequestId;
@SerializedName("TicketId")
@Expose
private Integer ticketId;
@SerializedName("Status")
@Expose
private Integer status;
@SerializedName("Remarks")
@Expose
private String remarks;
@SerializedName("ChangeRequestStatus")
@Expose
private Integer changeRequestStatus;

public Integer getChangeRequestId() {
return changeRequestId;
}

public void setChangeRequestId(Integer changeRequestId) {
this.changeRequestId = changeRequestId;
}

public Integer getTicketId() {
return ticketId;
}

public void setTicketId(Integer ticketId) {
this.ticketId = ticketId;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getRemarks() {
return remarks;
}

public void setRemarks(String remarks) {
this.remarks = remarks;
}

public Integer getChangeRequestStatus() {
return changeRequestStatus;
}

public void setChangeRequestStatus(Integer changeRequestStatus) {
this.changeRequestStatus = changeRequestStatus;
}

}

