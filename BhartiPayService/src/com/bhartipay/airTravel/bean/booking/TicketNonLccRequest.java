package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketNonLccRequest 
{
	@SerializedName("EndUserIp")
	@Expose
	private String endUserIp;
	@SerializedName("TokenId")
	@Expose
	private String tokenId;
	@SerializedName("TraceId")
	@Expose
	private String traceId;
	@SerializedName("PNR")
	@Expose
	private String pNR;
	@SerializedName("BookingId")
	@Expose
	private String bookingId;
	@SerializedName("FirstName")
	@Expose
	private String firstName;
	@SerializedName("LastName")
	@Expose
	private String lastName;
	@SerializedName("travelId")
	@Expose
	private String travelId;
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getpNR() {
		return pNR;
	}

	public void setpNR(String pNR) {
		this.pNR = pNR;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public String getEndUserIp() {
	return endUserIp;
	}

	public void setEndUserIp(String endUserIp) {
	this.endUserIp = endUserIp;
	}

	public String getTokenId() {
	return tokenId;
	}

	public void setTokenId(String tokenId) {
	this.tokenId = tokenId;
	}

	public String getTraceId() {
	return traceId;
	}

	public void setTraceId(String traceId) {
	this.traceId = traceId;
	}

	public String getPNR() {
	return pNR;
	}

	public void setPNR(String pNR) {
	this.pNR = pNR;
	}

	public String getBookingId() {
	return bookingId;
	}

	public void setBookingId(String bookingId) {
	this.bookingId = bookingId;
	}
}
