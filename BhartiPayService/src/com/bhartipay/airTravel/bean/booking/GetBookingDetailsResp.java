
package com.bhartipay.airTravel.bean.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBookingDetailsResp {

    @SerializedName("Error")
    @Expose
    private Error error;
    @SerializedName("FlightItinerary")
    @Expose
    private FlightItinerary flightItinerary;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public FlightItinerary getFlightItinerary() {
        return flightItinerary;
    }

    public void setFlightItinerary(FlightItinerary flightItinerary) {
        this.flightItinerary = flightItinerary;
    }

}
