package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="airusrtrace")
public class UserTrace 
{
	@Id
	@Column(name="userid" , nullable=false)
	private String userId;
	
	@Column(name="aggreatorid" , nullable=false)
	private String aggreatorId;
	
	@Column(name="traceId")
	private String traceId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}


	

}
