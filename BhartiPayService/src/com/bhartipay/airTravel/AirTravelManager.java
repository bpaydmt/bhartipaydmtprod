package com.bhartipay.airTravel;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.airTravel.bean.AirFareBean;
import com.bhartipay.airTravel.bean.AirportDetails;
import com.bhartipay.airTravel.bean.AuthenticationRequestBean;
import com.bhartipay.airTravel.bean.CancellationCharges;
import com.bhartipay.airTravel.bean.CancelledDetails;
import com.bhartipay.airTravel.bean.ConvenienceFeeBean;
import com.bhartipay.airTravel.bean.FareRuleRequestBean;
import com.bhartipay.airTravel.bean.FlightSearchRequestBean;
import com.bhartipay.airTravel.bean.FlightTxnBean;
import com.bhartipay.airTravel.bean.PassangerDetails;
import com.bhartipay.airTravel.bean.PayLoadBean;
import com.bhartipay.airTravel.bean.RequestBean;
import com.bhartipay.airTravel.bean.Response;
import com.bhartipay.airTravel.bean.ResponseBean;
import com.bhartipay.airTravel.bean.TicketDetails;
import com.bhartipay.airTravel.bean.TicketSegment;
import com.bhartipay.airTravel.bean.booking.BookingRequest;
import com.bhartipay.airTravel.bean.booking.BookingResponseBean;
import com.bhartipay.airTravel.bean.booking.CancelRequest;
import com.bhartipay.airTravel.bean.booking.FlightBookingResp;
import com.bhartipay.airTravel.bean.booking.FlightItinerary;
import com.bhartipay.airTravel.bean.booking.GetBookingDetailResp;
import com.bhartipay.airTravel.bean.booking.GetBookingDetailsResp;
import com.bhartipay.airTravel.bean.booking.Passenger;
import com.bhartipay.airTravel.bean.booking.Response_;
import com.bhartipay.airTravel.bean.booking.Sector;
import com.bhartipay.airTravel.bean.booking.TicketCRInfo;
import com.bhartipay.airTravel.bean.booking.TicketNonLccRequest;
import com.bhartipay.airTravel.bean.fareQuote.FareQuote;
import com.bhartipay.airTravel.bean.fareQuote.FareQuoteResponseBean;
import com.bhartipay.airTravel.bean.flightSearch.Airport;
import com.bhartipay.airTravel.bean.flightSearch.Destination;
import com.bhartipay.airTravel.bean.flightSearch.Example;
import com.bhartipay.airTravel.bean.flightSearch.Fare;
import com.bhartipay.airTravel.bean.flightSearch.FareBreakdown;
import com.bhartipay.airTravel.bean.flightSearch.FareRule;
import com.bhartipay.airTravel.bean.flightSearch.FlightResponseBean;
import com.bhartipay.airTravel.bean.flightSearch.Origin;
import com.bhartipay.airTravel.bean.flightSearch.Result;
import com.bhartipay.airTravel.bean.flightSearch.Segment;
import com.bhartipay.airTravel.bean.travelRule.TravelRuleResponse;
import com.bhartipay.airTravel.bean.travelRule.TravelRuleResponseBean;
import com.bhartipay.airTravel.persistence.AirTravelDao;
import com.bhartipay.airTravel.persistence.AirTravelDaoImpl;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommUtility;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.jsonwebtoken.Claims;
import net.pg.appnit.utility.AES128Bit;


@Path("/AirTravel")
public class AirTravelManager 
{

	@Context ServletContext servletContext;
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	private static final Logger logger =  Logger.getLogger(AirTravelManager.class.getName());
	AirTravelDao airTravelDao = new AirTravelDaoImpl();
	AirTravelAPIManager airApiManager = new AirTravelAPIManager();
	Gson gson = new Gson();
	WalletUserDao walletUserDao = new WalletUserDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
	CommanUtil commanUtil = new CommanUtil();
	CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
	
	ObjectMapper oMapper = new ObjectMapper();
	
	//String ticketPath="E:/home/flightticket/";//local

	/*//String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "ROOT");//for production and test
	String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "BhartiPay");//for local
*/	//String ticketPath="E:/EclipseWS/.metadata/.plugins/org.eclipse.wst.server.core/tmp1/wtpwebapps/BhartiPay/flightPDF/";
	//String ticketPath="/home/flightticket/";  //for test
	//String ticketPath="/root/flightticket/";  //for production

	String filePath = "https://b2b.bhartipay.com/BhartiPayService/image/flights";//for production
	//String filePath="http://test.bhartipay.com:9080/BhartiPayService/image/flights"; //for test
	
	public void compressFile(String sourcePath , String destinationPath , String fileName)
	{
		byte[] buffer = new byte[1024];
		try
		{
			FileOutputStream fout = new FileOutputStream(destinationPath);
			ZipOutputStream zout = new ZipOutputStream(fout);
			ZipEntry ze = new ZipEntry(fileName);
			zout.putNextEntry(ze);
			FileInputStream fin = new FileInputStream(sourcePath);
			int len;
			while((len=fin.read(buffer)) > 0)
				
			{
				zout.write(buffer, 0, len);
			}
			fin.close();
			zout.closeEntry();
			zout.close();
			System.out.println("File Compressed. Please Check");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public  boolean isCompressed( byte[] compressed) {
		return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
	  }
	
	public  byte[] compress(String str) throws IOException 
	{
	    if ((str == null) || (str.length() == 0)) {
	      return null;
	    }
	    ByteArrayOutputStream obj = new ByteArrayOutputStream();
	    GZIPOutputStream gzip = new GZIPOutputStream(obj);
	    gzip.write(str.getBytes("UTF-8"));
	    gzip.flush();
	    gzip.close();
	    return obj.toByteArray();
	  }
	
	 public String decompress(final byte[] compressed) throws IOException {
		    final StringBuilder outStr = new StringBuilder();
		    if ((compressed == null) || (compressed.length == 0)) {
		      return "";
		    }
		    if (isCompressed(compressed)) {
		      final GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(compressed));
		      final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gis, "UTF-8"));

		      String line;
		      while ((line = bufferedReader.readLine()) != null) {
		        outStr.append(line);
		      }
		    } else {
		      outStr.append(compressed);
		    }
		    return outStr.toString();
		  }
	 
	 @POST
	 @Path("/getInitialSearch")
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Consumes(MediaType.APPLICATION_JSON)
	 public ResponseBean getInitialSearch(RequestBean requestBean ,@Context HttpServletRequest request)
	 {
		 logger.info("***********Start Execution *******getInitialSearch*******");
		 ResponseBean responseBean = new ResponseBean();
		 Response response=new Response();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 
		 if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 response.setStatus("FAILED");
			 response.setStatusCode("1");
			 response.setStatusMsg("Invalid Aggreator ID.");	
			 responseBean.setResponse(gson.toJson(response));
			return responseBean;
		}
		 try
		 {
		 String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"getInitialSearch");
			
		logger.info(serverName +"*****RequestId******"+requestId+"************getInitialSearch***************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"**********getInitialSearch*************After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		 logger.info(serverName+"*******getInitialSearch*******mkey**********"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 response.setStatus("FAILED");
			 response.setStatusCode("1");
			 response.setStatusMsg("FAILED");
			 responseBean.setResponse(gson.toJson(response));
			return responseBean;
		}
		 
		 try
		 {
			 List<AirportDetails> airportList = airTravelDao.getAirportList();
			 List<AirportDetails> popularAirportList = airTravelDao.getAirportPopularList();
			 logger.info(serverName+"*******getInitialSearch*******airportList.size()**********"+airportList.size());
			 
			 response.setStatus("SUCCESS");
			 response.setStatusCode("300");
			 response.setStatusMsg("SUCCESS");
			 PayLoadBean payLoadBean=new PayLoadBean();
			 payLoadBean.setAirportList(airportList);
			 payLoadBean.setPopularAirportList(popularAirportList);
			 response.setResPayload(payLoadBean);
			 
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(response, HashMap.class), mKey));
			 
			return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********getInitialSearch*********");
			 ex.printStackTrace();
			 response.setStatus("FAILED");
			 response.setStatusCode("1");
			 response.setStatusMsg("FAILED");
			 //responseBean.setResponse(gson.toJson(response));
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(response, HashMap.class), mKey));
			return responseBean;
		 }
		}
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********getInitialSearch*********");
			 ex.printStackTrace();
			 response.setStatus("FAILED");
			 response.setStatusCode("1");
			 response.setStatusMsg("FAILED");
			 responseBean.setResponse(gson.toJson(response));
			return responseBean;
		 }
	 }
	
	 @POST
	 @Path("/flightSearch")
	 @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	 @Consumes(MediaType.APPLICATION_JSON)
	 public ResponseBean flightSearch( RequestBean requestBean ,@Context HttpServletRequest request)
	 {
		 logger.info("***********Start Execution *******flightSearch*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 String apiResp="";
		 FlightResponseBean respBean = new FlightResponseBean();
		 ResponseBean responseBean = new ResponseBean();
		 Response response=new Response();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"flightSearch");
		
		logger.info(serverName +"*****RequestId******"+requestId+"*********flightSearch******************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"*********flightSearch*********************Request*****"+requestBean.getRequest());
		
		logger.info(serverName +"**********flightSearch*************After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			respBean.setStatusMsg("Invalid Aggreator ID.");
			responseBean.setResponse(gson.toJson(respBean));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			respBean.setStatusMsg("Invalid Request.");
			responseBean.setResponse(gson.toJson(respBean));
			return responseBean;	
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"***********flightSearch************mKey*****"+mKey);
		
		 logger.info(serverName+"*******flightSearch*******mkey**********"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
			 
			 JSONObject claimJson = new JSONObject(claim); 
			// JsonElement je = new JsonParser().parse(claim.toString());
			 FlightSearchRequestBean flightreqbean = new FlightSearchRequestBean();
			 flightreqbean = gson.fromJson(claimJson.toString() ,FlightSearchRequestBean.class);
			 reqbean = airTravelDao.getAuthDetails("Air");
			 
			 flightreqbean.setEndUserIp(reqbean.getEndUserIp());
			 flightreqbean.setTokenId(reqbean.getTokenId());
			 apiResp =airApiManager.flightSearchApi(flightreqbean);
			/* responseMap.put("status", "SUCCESS");
			 responseMap.put("statusCode", "300");
			 responseMap.put("statusMsg", "SUCCESS.");
			 responseMap.put("flightSearch", new  Gson().fromJson(apiResp,Example.class));
			 */
			 respBean.setFlightSearch(gson.fromJson(apiResp,Example.class));
			 
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********flightSearch********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 if(respBean.getFlightSearch().getResponse().getError().getErrorMessage()!= null)
			 {
				 respBean.setStatusMsg(respBean.getFlightSearch().getResponse().getError().getErrorMessage());
			 }
			 else
				 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 //responseBean.setResponse(gson.toJson(responseMap));
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(respBean, HashMap.class), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			logger.info(serverName+"************problem in **********flightSearch********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	 }
	 
	 /**
	  * 
	  * @param requestBean
	  * @param httpServletRequest
	  * @return
	  */
	@POST
	@Path("/fareRule")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean fareRule(RequestBean requestBean , @Context HttpServletRequest request)
	{
		 logger.info("***********Start Execution *******fareRuleApi*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 Response response=new Response();
		 String apiResp ="";
		 TravelRuleResponseBean travelRespBean = new TravelRuleResponseBean();
		 TravelRuleResponse travelResp = new TravelRuleResponse();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"fareRule");
		
		logger.info(serverName +"*****RequestId******"+requestId+"*********fareRuleApi**********Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"*********fareRuleApi**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****fareRuleApi********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			travelRespBean.setStatus("FAILED");
			travelRespBean.setStatusCode("1");
			travelRespBean.setStatusMsg("Invalid Aggreator Id.");
			responseBean.setResponse(gson.toJson(travelRespBean));
			return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			travelRespBean.setStatus("FAILED");
			travelRespBean.setStatusCode("1");
			travelRespBean.setStatusMsg("Invalid Request.");
			responseBean.setResponse(gson.toJson(travelRespBean));
			return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		 logger.info(serverName +"*****RequestId******"+requestId+"*****fareRuleApi********mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 travelRespBean.setStatus("FAILED");
			 travelRespBean.setStatusCode("1");
			 travelRespBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(travelRespBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
			 /*if(claim.get("TraceId") == null || claim.get("TraceId").toString().trim().isEmpty())
			 {
				 travelRespBean.setStatus("FAILED");
				 travelRespBean.setStatusCode("1");
				 travelRespBean.setStatusMsg("Invalid TraceId.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(travelRespBean, HashMap.class), mKey));
				 return responseBean;
			 }*/
			 JSONObject claimJson = new JSONObject(claim); 
			 FareRuleRequestBean fareBean = new FareRuleRequestBean();
			 fareBean = gson.fromJson(claimJson.toString() ,FareRuleRequestBean.class);
			 reqbean = airTravelDao.getAuthDetails("Air");
			 fareBean.setEndUserIp(reqbean.getEndUserIp());
			 fareBean.setTokenId(reqbean.getTokenId());
			 /*fareBean.setTraceId(claim.get("TraceId").toString());
			 fareBean.setResultIndex(claim.get("ResultIndex").toString());*/
			 //fareBean.setTraceId(fareBean.get);
			 //fareBean.setResultIndex("OB9");
			 apiResp=airApiManager.fareRuleApi(fareBean);
			 travelRespBean.setStatus("SUCCESS");
			 travelRespBean.setStatusCode("300");
			 travelRespBean.setStatusMsg("SUCCESS.");
			 travelRespBean.setFareRule(gson.fromJson(apiResp,TravelRuleResponse.class));
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(travelRespBean), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********fareRuleApi********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 travelRespBean.setStatus("FAILED");
			 travelRespBean.setStatusCode("1");
			 if(travelRespBean.getFareRule().getResponse().getError().getErrorMessage() != null)
				 travelRespBean.setStatusMsg(travelRespBean.getFareRule().getResponse().getError().getErrorMessage());
			 else
				 travelRespBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(travelRespBean), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			logger.info(serverName+"************problem in **********fareRuleApi********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 travelRespBean.setStatus("FAILED");
			 travelRespBean.setStatusCode("1");
			 travelRespBean.setStatusMsg("INVALID MESSAGE STRING.");	
			 responseBean.setResponse(gson.toJson(travelRespBean));
			return responseBean;
		}
	 
	}
	
	@POST
	@Path("/fareQuote")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean fareQuote(RequestBean requestBean , @Context HttpServletRequest request,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent)
	{
		 logger.info("***********Start Execution *******fareQuoteApi*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 FareQuoteResponseBean respBean = new FareQuoteResponseBean();
		 AirFareBean airFareBean = new AirFareBean();
		 String apiResp ="";
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"fareQuote");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******fareQuoteApi************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******fareQuoteApi**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****fareQuoteApi***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****fareQuoteApi******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 JSONObject claimJson = new JSONObject(claim); 
			 List<Result> resultList = new ArrayList<Result>();
			 FareQuote fareQuote = new FareQuote();
			 FareQuote fareQuoteFnl = new FareQuote();
			 double convenienceFee=0.0;
			 List<ConvenienceFeeBean> convFeeList = new ArrayList<ConvenienceFeeBean>();
			 
			 airFareBean.setTripId(commanUtilDao.getTrxId("TripId","OAGG001050"));
			 airFareBean.setAggreatorId(requestBean.getAggreatorId());
			 airFareBean.setUserId(claimJson.getString("userId"));
			 airFareBean.setAdultCount(claimJson.getInt("AdultCount"));
			 airFareBean.setChildCount(claimJson.getInt("ChildCount"));
			 airFareBean.setInfantCount(claimJson.getInt("InfantCount"));
			 airFareBean.setIpimei(imei);
			 airFareBean.setAgent(agent);
			 airFareBean.setTraceId(claimJson.getString("TraceId"));
			/* if(claimJson.getBoolean("IsDomestic"))
				 airFareBean.setIsDomestic(1);
			 else
				 airFareBean.setIsDomestic(0);*/
			 airFareBean.setIsDomestic(1);
			 airFareBean.setJourneyType(claimJson.getInt("JourneyType"));
			 FareRuleRequestBean fareBean = new FareRuleRequestBean();
			 fareBean = gson.fromJson(claimJson.toString() ,FareRuleRequestBean.class);
			 reqbean = airTravelDao.getAuthDetails("Air");
			 convFeeList = airTravelDao.getConvenienceFeeStruct(1,"Booking");
			 fareBean.setEndUserIp(reqbean.getEndUserIp());
			 fareBean.setTokenId(reqbean.getTokenId());
			 String[] fareRequest = fareBean.getResultIndex().split(",");
			 String[] isLcc=claimJson.getString("IsLCC").split(",");
			 String[] airlineCode = claimJson.getString("AirlineCode").split(",");
			 if((isLcc.length == 2 && airlineCode.length == 2) && (isLcc[0].equalsIgnoreCase("false") && isLcc[1].equalsIgnoreCase("false")) && (airlineCode[0].equals(airlineCode[1])))
			 {
				 AirFareBean airFareBeansave = new AirFareBean();
				 airFareBeansave = (AirFareBean)airFareBean.clone();
				/* if(airlineCode[0] != null)
					 {airFareBeansave.setFlight(airlineCode[0]);}*/
				 airFareBeansave.setIsLCC(0);
				 airFareBeansave.setTripIndicator(6);
				 convenienceFee=((convFeeList.get(0).getAdult()+convFeeList.get(1).getAdult())*(claimJson.getInt("AdultCount")+claimJson.getInt("ChildCount")))+((convFeeList.get(0).getInfant()+convFeeList.get(1).getInfant())*claimJson.getInt("InfantCount"));
				 airFareBeansave.setConvenienceFee(convenienceFee);
				 airFareBeansave.setAppliedOn(convFeeList.get(0).getAppliedOn());
				 fareQuote = fareQuoteImpl(airFareBeansave , fareBean , convFeeList.get(0).getAppliedOn(),convenienceFee);
				 
				 if(fareQuote.getResponse().getError().getErrorCode() != 0)
				 {
					 respBean.setStatus("FAILED");
					 if(fareQuote.getResponse().getError().getErrorCode() == 6)
						 respBean.setStatusCode("9");
					 else
						 respBean.setStatusCode("1");
					 respBean.setFareQuote(fareQuote);
					 if(respBean.getFareQuote().getResponse().getError().getErrorMessage() != null)
					 {
						 respBean.setStatusMsg(respBean.getFareQuote().getResponse().getError().getErrorMessage());
					 }
					 else
					 {
						 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
					 }
					 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,FareQuoteResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
					 return responseBean;
				 }
				 fareQuote.getResponse().getResult().getFare().setConvenienceFee(convenienceFee);
				 fareQuote.getResponse().getResult().getFare().setChargedFare(airFareBeansave.getTotalFare());
				 resultList.add(fareQuote.getResponse().getResult());	 
			 }
			 else
			 { 
				 for(int i =0 ; i<fareRequest.length ; i++)
				 {
					 AirFareBean airFareBeansave = new AirFareBean();
					 airFareBeansave = (AirFareBean)airFareBean.clone();
					 airFareBeansave.setTripIndicator(i+1);
					/* if(airlineCode[i] != null)
					 {airFareBeansave.setFlight(airlineCode[i]);}*/
					 if(isLcc[i].equalsIgnoreCase("true"))
						 airFareBeansave.setIsLCC(1);
					 else
						 airFareBeansave.setIsLCC(0);
					 //FareQuote farequoteRes = new FareQuote();
					 fareBean.setResultIndex(fareRequest[i]);
					 convenienceFee=(convFeeList.get(i).getAdult()*(claimJson.getInt("AdultCount")+claimJson.getInt("ChildCount")))+(convFeeList.get(i).getInfant()*claimJson.getInt("InfantCount"));
					 airFareBeansave.setConvenienceFee(convenienceFee);
					 airFareBeansave.setAppliedOn(convFeeList.get(i).getAppliedOn());
					 fareQuote = fareQuoteImpl( airFareBeansave, fareBean ,convFeeList.get(i).getAppliedOn(),convenienceFee);
					
					 if(fareQuote.getResponse().getError().getErrorCode() != 0)
					 {
						 respBean.setStatus("FAILED");
						 if(fareQuote.getResponse().getError().getErrorCode() == 6)
							 respBean.setStatusCode("9");
						 else
							 respBean.setStatusCode("1");
						 respBean.setFareQuote(fareQuote);
						 if(respBean.getFareQuote().getResponse().getError().getErrorMessage() != null)
						 {
							 respBean.setStatusMsg(respBean.getFareQuote().getResponse().getError().getErrorMessage());
						 }
						 else
						 {
							 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
						 }
						 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,FareQuoteResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
						 return responseBean;
					 }
					 fareQuote.getResponse().getResult().getFare().setConvenienceFee(convenienceFee);
					 fareQuote.getResponse().getResult().getFare().setChargedFare(airFareBeansave.getTotalFare());
					 resultList.add(fareQuote.getResponse().getResult());
					 
				 }
			 }
			 
			 fareQuote.getResponse().setResultList(resultList);
			 if(resultList.size() == 2)
			 {
				 fareQuote.getResponse().setResult(addResult(resultList.get(0),resultList.get(1)));
			 }
			 respBean.setFareQuote(fareQuote);
			 
			 //airFareBean.setFlightSource(respBean.getFareQuote().getResponse().getResult().getSegments());;
			 respBean.setTravelId(airFareBean.getTripId());
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,FareQuoteResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********fareQuoteApi********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 if(respBean.getFareQuote().getResponse().getError().getErrorMessage() != null)
			 {
				 respBean.setStatusMsg(respBean.getFareQuote().getResponse().getError().getErrorMessage());
			 }
			 else
			 {
				 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
			 }
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,FareQuoteResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			 logger.info(serverName+"************problem in **********fareQuoteApi********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	}
	
	/**
	 * 
	 * @param airFareBean
	 * @param fareBean
	 * @param mKey
	 * @return
	 */
	public FareQuote fareQuoteImpl(AirFareBean airFareBean , FareRuleRequestBean fareBean ,String appliedOn ,double convenienceFee)
	{
		logger.info("***************Start Execution *************fareQuoteImpl*********");
		FareQuoteResponseBean respBean = new FareQuoteResponseBean();
		List<Result> resultList = new ArrayList<Result>();
		ResponseBean responseBean = new ResponseBean();
		String apiResp ="";
		FareQuote fareQuote = new FareQuote();
		try
		{
		 apiResp=airApiManager.fareQuoteApi(fareBean);
		 System.out.println("\n\n\n----------------lenth---------"+apiResp.length());
		 fareQuote = gson.fromJson(apiResp,FareQuote.class);
		 if(fareQuote.getResponse().getError().getErrorCode() != 0)
		 {
			 return fareQuote;
		 }
		 if(appliedOn.trim().equalsIgnoreCase("offeredFare"))
		 {
			airFareBean.setTotalFare(fareQuote.getResponse().getResult().getFare().getOfferedFare()+convenienceFee);
		 }
		 else
		 {
			 airFareBean.setTotalFare(fareQuote.getResponse().getResult().getFare().getPublishedFare()+convenienceFee);
		 }
		 if(fareQuote.getResponse().getResult().getIsRefundable())
			 airFareBean.setIsRefundable(1);
		 else
			 airFareBean.setIsRefundable(2);
		 //airFareBean.setTotalFare(fareQuote.getResponse().getResult().getFare().getPublishedFare());
		 airFareBean.setSource(fareQuote.getResponse().getResult().getSource());
		 airFareBean.setPublishedFare(fareQuote.getResponse().getResult().getFare().getPublishedFare());
		 airFareBean.setOfferedFare(fareQuote.getResponse().getResult().getFare().getOfferedFare());
		 airFareBean.setTotalBaseFare(fareQuote.getResponse().getResult().getFare().getBaseFare());
		 airFareBean.setTotalTax(fareQuote.getResponse().getResult().getFare().getPublishedFare() - fareQuote.getResponse().getResult().getFare().getBaseFare());
		 airFareBean.setCommissionEarned(fareQuote.getResponse().getResult().getFare().getCommissionEarned());
		 airFareBean.setResultIndex(fareQuote.getResponse().getResult().getResultIndex());
		 airFareBean.setStatus("FareQuoteDone");
		 airFareBean.setEventStatus("AmountUnPaid");
		 int seqmentsize = fareQuote.getResponse().getResult().getSegments().get(0).size();
		 Date deptDate = new Date();
		 Date arrDate = new Date();
		 DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
         DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 
		 if(fareQuote.getResponse() != null && fareQuote.getResponse().getResult() != null && fareQuote.getResponse().getResult().getSegments() != null && fareQuote.getResponse().getResult().getSegments().get(0) != null && (fareQuote.getResponse().getResult().getSegments().get(0).get(0) != null && fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1) != null ))		 
		 {
			 if(fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline().getAirlineName() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline().getAirlineCode() != null)
			 {
				 airFareBean.setFlight(fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline().getAirlineCode()+","+fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline().getAirlineName()+","+fareQuote.getResponse().getResult().getSegments().get(0).get(0).getAirline().getFlightNumber());
			 }
			 if(fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin().getAirport() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin().getAirport().getAirportCode() != null)
			 {
				 airFareBean.setFlightSource(fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin().getAirport().getAirportCode()+","+fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin().getAirport().getCityName());
				 deptDate = f.parse(fareQuote.getResponse().getResult().getSegments().get(0).get(0).getOrigin().getDepTime());
				 airFareBean.setDeptDate(f2.parse(f2.format(deptDate)));
			 }
			 if(fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination().getAirport() != null && fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination().getAirport().getAirportCode() != null)
			 {
				 airFareBean.setFlightDestination(fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination().getAirport().getAirportCode()+","+fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination().getAirport().getCityName());
				 arrDate = f.parse(fareQuote.getResponse().getResult().getSegments().get(0).get(seqmentsize-1).getDestination().getArrTime());
				 airFareBean.setArrDate(f2.parse(f2.format(arrDate)));
			 }
			 airFareBean.setOtherCharges(fareQuote.getResponse().getResult().getFare().getOtherCharges());
			 
		 }
		 List<FareBreakdown> fareBreakDown = fareQuote.getResponse().getResult().getFareBreakdown();
		 Iterator<FareBreakdown> itr = fareBreakDown.iterator();
		 while(itr.hasNext())
		 {
			 FareBreakdown farebreak = itr.next();
			 if(Integer.parseInt(farebreak.getPassengerType().trim()) == 1)
			 {
				 airFareBean.setAdultBaseFare(Double.parseDouble(farebreak.getBaseFare())/farebreak.getPassengerCount());
				 airFareBean.setAdultTax((Double.parseDouble(farebreak.getTax()) + Double.parseDouble(farebreak.getPGCharge()) + Double.parseDouble(farebreak.getAdditionalTxnFeePub()))/farebreak.getPassengerCount());
			 }
			 if(Integer.parseInt(farebreak.getPassengerType().trim()) == 2)
			 {
				 airFareBean.setChildBaseFare(Double.parseDouble(farebreak.getBaseFare())/farebreak.getPassengerCount());
				 airFareBean.setChildTax((Double.parseDouble(farebreak.getTax()) + Double.parseDouble(farebreak.getPGCharge()) + Double.parseDouble(farebreak.getAdditionalTxnFeePub()))/farebreak.getPassengerCount());
			 }
			 if(Integer.parseInt(farebreak.getPassengerType().trim()) == 3)
			 {
				 airFareBean.setInfantBaseFare(Double.parseDouble(farebreak.getBaseFare())/farebreak.getPassengerCount());
				 airFareBean.setInfantTax((Double.parseDouble(farebreak.getTax()) + Double.parseDouble(farebreak.getPGCharge()) + Double.parseDouble(farebreak.getAdditionalTxnFeePub()))/farebreak.getPassengerCount());
			 }
		 }
		 
		 
		 String airfareid = airTravelDao.saveFareQuote(airFareBean);
		 if(airfareid.equals("1001") || airfareid.equals("700"))
		 {
			 fareQuote.getResponse().getError().setErrorCode(700);
			 return fareQuote;
		 }
		}
		catch(Exception ex)
		{
			logger.info("**************problem in fareQuoteImpl************e.message"+ex.getMessage());
			ex.printStackTrace();
		}
		 return fareQuote;
	}
	
	
	public Result addResult(Result result1,Result result2){
        try {
            if (result2 != null && result2.getSegments().get(0).size() == 0) {
                return  result1;
            }
            Result result = new Result();
            result = (Result)result1.clone();
            result.setFare(getReTotalFare(result1.getFare(),result2.getFare()));
            result.setFareBreakdown(getReFareBreakdown(result1.getFareBreakdown(),result2.getFareBreakdown()));
            result.setSegments(getReSegment(result1.getSegments(),result2.getSegments()));
            result.setFareRules(getReFareRules(result1.getFareRules(),result2.getFareRules()));


            return result;
        }catch (Exception e){
            e.printStackTrace();
            return result1;
        }

    }

	private List<FareRule> getReFareRules(List<FareRule> fareRules, List<FareRule> fareRules1) {
        if(fareRules!=null && fareRules1 !=null) {
            List<FareRule> newList = new ArrayList<FareRule>(fareRules);
            newList.addAll(fareRules1);
            return newList;
        }
        return fareRules==null?fareRules1:fareRules;
    }

    private Fare getReTotalFare(Fare fare1, Fare fare2) {
        Fare fare = new Fare();
        fare.setTransactionFee(fare1.getTransactionFee()+fare2.getTransactionFee());
        fare.setAirTransFee(fare1.getAirTransFee()+fare2.getAirTransFee());
        fare.setAdditionalTxnFee(fare1.getAdditionalTxnFee()+fare2.getAdditionalTxnFee());
        fare.setCurrency(fare1.getCurrency());
        fare.setBaseFare(fare1.getBaseFare()+fare2.getBaseFare());
        fare.setTax(fare1.getTax()+fare2.getTax());
        fare.setConvenienceFee(fare1.getConvenienceFee()+fare2.getConvenienceFee());
        fare.setChargedFare(fare1.getChargedFare()+fare2.getChargedFare());
        /*************************TaxBreakup not adding****************************************/
        fare.setTaxBreakup(fare1.getTaxBreakup());
        /*************************************************************************************/

        fare.setYQTax(fare1.getYQTax()+fare2.getYQTax());
        fare.setAdditionalTxnFeeOfrd(fare1.getAdditionalTxnFeeOfrd()+fare2.getAdditionalTxnFeeOfrd());
        fare.setAdditionalTxnFeePub(fare1.getAdditionalTxnFeePub()+fare2.getAdditionalTxnFeePub());
        fare.setPGCharge(fare1.getPGCharge()+fare2.getPGCharge());
        fare.setOtherCharges(fare1.getOtherCharges()+fare2.getOtherCharges());
        /*************************ChargeBU not adding****************************************/
        fare.setChargeBU(fare1.getChargeBU());
        /*************************************************************************************/
        fare.setDiscount(fare1.getDiscount()+fare2.getDiscount());
        fare.setPublishedFare(fare1.getPublishedFare()+fare2.getPublishedFare());
        fare.setCommissionEarned(fare1.getCommissionEarned()+fare2.getCommissionEarned());
        fare.setpLBEarned(fare1.getpLBEarned()+fare2.getpLBEarned());
        fare.setIncentiveEarned(fare1.getIncentiveEarned()+fare2.getIncentiveEarned());
        fare.setOfferedFare(fare1.getOfferedFare()+fare2.getOfferedFare());
        fare.setTdsOnCommission(fare1.getTdsOnCommission()+fare2.getTdsOnCommission());
        fare.setTdsOnPLB(fare1.getTdsOnPLB()+fare2.getTdsOnPLB());
        fare.setTdsOnIncentive(fare1.getTdsOnIncentive()+fare2.getTdsOnIncentive());
        fare.setServiceFee(fare1.getServiceFee()+fare2.getServiceFee());
        fare.setTotalBaggageCharges(fare1.getTotalBaggageCharges()+fare2.getTotalBaggageCharges());
        fare.setTotalMealCharges(fare1.getTotalMealCharges()+fare2.getTotalMealCharges());
        fare.setTotalSeatCharges(fare1.getTotalSeatCharges()+fare2.getTotalSeatCharges());
        fare.setTotalSpecialServiceCharges(fare1.getTotalSpecialServiceCharges()+fare2.getTotalSpecialServiceCharges());

        return  fare;
    }
    private String addNonStringValue(String v,String v2){
        try {
            if (v != null && v2 != null) {
                return String.valueOf(Double.valueOf(v) + Double.valueOf(v2));
            } else if (v == null && v2 != null) {
                return v2;
            } else if (v != null && v2 == null) {
                return v;
            } else if (v == null && v2 == null) {
                return "0";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
        return "0";
    }
    private List<FareBreakdown> getReFareBreakdown(List<FareBreakdown> d,List<FareBreakdown> d2){
        try {
            List<FareBreakdown> breakdowns = new ArrayList<>();
            
            for (int i = 0; i < d.size(); i++) {
            	FareBreakdown breakdown = new FareBreakdown();
                breakdown.setAdditionalTxnFeeOfrd(addNonStringValue(d.get(i).getAdditionalTxnFeeOfrd(), d2.get(i).getAdditionalTxnFeeOfrd()));
                breakdown.setAdditionalTxnFeePub(addNonStringValue(d.get(i).getAdditionalTxnFeePub(), d2.get(i).getAdditionalTxnFeePub()));
                breakdown.setPGCharge(addNonStringValue(d.get(i).getPGCharge(), d2.get(i).getPGCharge()));
                breakdown.setCurrency(d.get(i).getCurrency());
                breakdown.setPassengerType(d.get(i).getPassengerType());
                breakdown.setPassengerCount(d.get(i).getPassengerCount());
                breakdown.setBaseFare(addNonStringValue(d.get(i).getBaseFare(), d2.get(i).getBaseFare()));
                breakdown.setTax(addNonStringValue(d.get(i).getTax(), d2.get(i).getTax()));
                breakdown.setYQTax(addNonStringValue(d.get(i).getYQTax(), d2.get(i).getYQTax()));
                breakdowns.add(breakdown);
            }

            return breakdowns;
        }catch (Exception e){
            e.printStackTrace();
            return d;
        }
    }
    private List<List<Segment>> getReSegment(List<List<Segment>> segments, List<List<Segment>> result2Segments){
        try {
            List<List<Segment>> lists = new ArrayList<>();
            lists.add(segments.get(0));
            lists.add(result2Segments.get(0));
            return lists;
        }catch (Exception e){
            e.printStackTrace();
            return segments;
        }

    }

	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/flightBooking")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean flightBooking(RequestBean requestBean , @Context HttpServletRequest request,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent )
	{
		 logger.info("***********Start Execution *******flightBooking*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 TicketNonLccRequest ticketNonLccRequest = new TicketNonLccRequest();
		 BookingResponseBean respBean = new BookingResponseBean();
		 BookingRequest bookingRequest = new BookingRequest();
		 BookingRequest bookingRequestSend = new BookingRequest();
		 FlightBookingResp flightBookingresp = new FlightBookingResp();
		 String apiResp ="";
		 String upStatus="";
		 double deductionAmount = 0.0;
		 /*double firstTicketAmt = 0.0;
		 double secondTicketAmt=0.0;*/
		 Response_ txnFailure = new Response_();
		 FlightTxnBean flightTxnBean = new FlightTxnBean();
		 String txnStatus="1001";
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"flightBooking");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******flightBooking************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******flightBooking**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****flightBooking***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****flightBooking******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 JSONObject claimJson = new JSONObject(claim); 
			 String s = claim.toString();
			/* TreeMap<String,String> treeMap=new TreeMap<String,String>();
			 	treeMap.put("userId",claimJson.getString("userId"));
			 	treeMap.put("TraceId",claimJson.getString("TraceId"));
			 	treeMap.put("TravelId",claimJson.getString("TravelId"));
			 	treeMap.put("CHECKSUMHASH",claimJson.getString("CHECKSUMHASH"));
				
				int statusCode= WalletSecurityUtility.checkSecurity(treeMap);
				 if(statusCode!=1000){
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode(""+statusCode);
					 respBean.setStatusMsg("Security Error.");
					 responseBean.setResponse(gson.toJson(respBean));
					 return responseBean;
			     }
			 */
			 bookingRequest = gson.fromJson(claimJson.toString() ,BookingRequest.class);
			 respBean.setTravelId(bookingRequest.getTravelId());
			 reqbean = airTravelDao.getAuthDetails("Air");
			 bookingRequest.setEndUserIp(reqbean.getEndUserIp());
			 bookingRequest.setTokenId(reqbean.getTokenId());
			 List<FlightBookingResp> flightBookList = new ArrayList<FlightBookingResp>();
			// double walletAmt=0.0;
			 String tripId=bookingRequest.getTravelId();
			 double tableWalletAmt=0.0;
			 double tablePGAmt=0.0;
			 double totalAmt=0.0;
			 double availWalletAmt=0.0;	
			 double refundAmt=0.0;
			 String walletTxnFlag=null;
			 String walletId="";
			 String customername="";
			 String sendername="";
			 String emailId="";
			// aggId, walletid, sendername
			 String aggreatorid=requestBean.getAggreatorId();
			 List<AirFareBean> airfarelist = new ArrayList<AirFareBean>();
			 
			 airfarelist = airTravelDao.getFareQuoteByTripId(bookingRequest.getTravelId(),"FareQuoteDone");
			 List<AirFareBean> airfarelistBT= airTravelDao.getFareQuoteByTripId(bookingRequest.getTravelId(),"BookingDone");
			 Iterator<AirFareBean> itrFQ = airfarelistBT.iterator();
			 while(itrFQ.hasNext())
			 {
				 airfarelist.add(itrFQ.next());
			 }
			 String userId = airfarelist.get(0).getUserId();
			 String AggreatorId = airfarelist.get(0).getAggreatorId();
			 flightTxnBean.setAggreatorId(AggreatorId);
			 flightTxnBean.setUserId(userId);
			 flightTxnBean.setBookingId(/*bookingRequest.getTravelId()+commanUtilDao.getTrxId("TvlTxn")*/bookingRequest.getTravelId());
			 flightTxnBean.setIpIemi(imei);
			 flightTxnBean.setUserAgent(agent);
			 WalletMastBean walletmast = walletUserDao.showUserProfile(userId);
			 flightTxnBean.setWalletId(walletmast.getWalletid());
			 walletId = walletmast.getWalletid();
			 customername=walletmast.getName();
			 sendername = walletmast.getName();
			 emailId=walletmast.getEmailid();
			 Iterator<AirFareBean> itr1 =airfarelist.iterator();
			 while(itr1.hasNext())
			 {
				 totalAmt +=itr1.next().getTotalFare();
			 }
			 availWalletAmt =  transactionDao.getWalletBalance(walletmast.getWalletid());
	
			 deductionAmount = totalAmt - (airfarelist.get(0).getWalletAmt()+airfarelist.get(0).getPgAmt())+(airfarelist.get(0).getWalletAmtRefund()+airfarelist.get(0).getPgAmtRefund());
			if(availWalletAmt < deductionAmount )
			{
				 respBean.setTravelId(bookingRequest.getTravelId());
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("9");
				 respBean.setStatusMsg("Insufficient Amount in wallet ");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			}
			 
			 
			/* if(airfarelist.size() == 2)
			 {
				 firstTicketAmt = airfarelist.get(0).getTotalFare();
				 secondTicketAmt=airfarelist.get(1).getTotalFare();
				 deductionAmount =firstTicketAmt +secondTicketAmt;
			 }
			 else if(airfarelist.size() == 1)
			 {
				 deductionAmount=airfarelist.get(0).getTotalFare();
			 }*/
			 
			 if(deductionAmount != 0.0)
			 {
			 tableWalletAmt=airfarelist.get(0).getWalletAmt()+deductionAmount;
			 tablePGAmt = airfarelist.get(0).getPgAmt();

			 int flag1=airTravelDao.updateFareQuote(totalAmt,  -1,-1,-1,-1, "",walletmast.getMobileno() ,walletmast.getWalletid(),"","",bookingRequest.getTravelId());
			 flightTxnBean.setFareAmount(deductionAmount);
			 walletTxnFlag = transactionDao.payForFlight(flightTxnBean);
			 txnStatus=walletTxnFlag;
			 if(walletTxnFlag == null || !walletTxnFlag.equalsIgnoreCase("1000"))
			 { 
				 respBean.setTravelId(bookingRequest.getTravelId());
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("9");
				 respBean.setStatusMsg(commanUtil.getErrorMsg(walletTxnFlag));
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }

			 }
			 List<AirFareBean> airfarelist_ = airTravelDao.getFareQuoteByTripId(bookingRequest.getTravelId(),"FareQuoteDone");
			 Iterator<AirFareBean> itrFQ1 = airfarelistBT.iterator();
			 while(itrFQ1.hasNext())
			 {
				 airfarelist_.add(itrFQ1.next());
			 }
             //For sending sms to customer for transaction
				try
				 {
					WalletMastBean walletMastBean=walletUserDao.showUserProfile(userId);
					 String smstemplet = commanUtilDao.getsmsTemplet("flightPayment",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<AMOUNT>>",Double.toString(deductionAmount)).replace("<<TXNNO>>", airfarelist_.get(0).getWalletTxn());
				     	
				       	//Dear <<NAME>>, you have added Rs. <<AMOUNT>> to your <<APPNAME>> wallet. Transaction ID is <<TXNNO>>.
				       	logger.info("~~~~~~~~~~~~~~~~~~~~~Add Money SMS~~~~~~~~~~~~~~~~~~~````" + smstemplet);
				     	/*String mailSub= "Rs. "+Double.toString(deductionAmount)+" deducted successfully.";
				     	String mailContent= commanUtilDao.getmailTemplet("addMoney",aggreatorid).replace("<<NAME>>",walletMastBean.getName()).replace("<<APPNAME>>", walletConfiguration.getAppname()).replace("<<AMOUNT>>", Double.toString(amount)).replace("<<TXNNO>>", txnId).replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"));
				     	logger.info("~~~~~~~~~~~~~~~~~~~~~Add Money MAIL~~~~~~~~~~~~~~~~~~~````" + mailContent);*/
				       	SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(walletMastBean.getEmailid(), /*mailContent,mailSub*/"","", walletMastBean.getMobileno(), smstemplet, "SMS",aggreatorid, airfarelist_.get(0).getWalletId(),walletMastBean.getName(),"NOTP");
//						Thread t = new Thread(smsAndMailUtility);
//						t.start();
				       	
				       	ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				 }
				 catch(Exception e)
				 {
					 logger.info("***************problem in sms/email sending***********flightBooking ******");
				 }
			 
			 Iterator<AirFareBean> itr = airfarelist_.iterator();
			 int j=0;
			 while(itr.hasNext())
			 {
				AirFareBean airFareBean = itr.next();
				refundAmt = airFareBean.getTotalRefundAmt();
				if((airFareBean.getStatus().equalsIgnoreCase("FareQuoteDone") || airFareBean.getStatus().equalsIgnoreCase("BookingDone")) && !(airFareBean.getStatus().equalsIgnoreCase("TicketBooked")))
				{	
					bookingRequest.setResultIndex(airFareBean.getResultIndex());
					List<Passenger> passengerList = new ArrayList<Passenger>();
					for (Passenger item : bookingRequest.getPassengers()) 
						{passengerList.add((Passenger)item.clone());}
					/*passengerList = bookingRequest.getPassengers();*/
					for(int i=0 ;i<passengerList.size();i++)
					{
						passengerList.get(i).setFare(passengerList.get(i).getFareList().get(j));
					}
					j++;
					bookingRequestSend = (BookingRequest)bookingRequest.clone();
					/*for(int i=0 ;i<passengerList.size();i++)
					{
						passengerList.get(i).setFareList(null);
					}*/
					bookingRequestSend.setPassengers(passengerList);
					for(int i=0 ;i<passengerList.size();i++)
					{
						 bookingRequestSend.getPassengers().get(i).setFareList(null);
					}
									
					 airFareBean.setEventStatus("AmountPaid");
					 airFareBean.setTraceId(bookingRequest.getTraceId());
					
					 /*double deductionAmount = airFareBean.getTotalFare();
					 String userId = airFareBean.getUserId();
					 String AggreatorId = airFareBean.getAggreatorId();
					 flightTxnBean.setAggreatorId(AggreatorId);
					 flightTxnBean.setFareAmount(deductionAmount);
					 flightTxnBean.setUserId(userId);
					 flightTxnBean.setBookingId(bookingRequest.getTravelId()+commanUtilDao.getTrxId("TvlTxn"));
					 flightTxnBean.setIpIemi(imei);
					 flightTxnBean.setUserAgent(agent);
					 WalletMastBean walletmast = walletUserDao.showUserProfile(userId);
					 flightTxnBean.setWalletId(walletmast.getWalletid());*/
					
					 respBean = bookFlight(bookingRequestSend ,airFareBean ,/*flightTxnBean ,*/ airFareBean.getIsLCC() ,mKey);
					 airFareBean.setApiStatus(respBean.getStatusCode());
					 airTravelDao.saveFareQuote(airFareBean);
					 if(!respBean.getStatusCode().equals("300"))
					 {
						 break;
					 }
				}
			 }
			 
			 List<AirFareBean> airfarelistFnl = airTravelDao.getFareQuoteByTripId(bookingRequest.getTravelId(),"");
			 List<FlightBookingResp> flightList = new ArrayList<FlightBookingResp>();
			 Iterator<AirFareBean> itrFnl = airfarelist_.iterator();
			 int flightNo = 1;
			 while(itrFnl.hasNext())
			 {
				 AirFareBean airfare= itrFnl.next();
				 if(airfare.getResponseString() == null || (!airfare.getApiStatus().equals("300") && !airfare.getApiStatus().equals("302") && !airfare.getApiStatus().equals("303")))
				 {
					// flightList.add(null);
				 }
				 else
				 {
					 String apiRespFnl =airfare.getResponseString();
					 FlightBookingResp flig = gson.fromJson(apiRespFnl,FlightBookingResp.class);
					 if(flightNo == 1)
					 {
						 respBean.setFlight1TotalFare(airfare.getTotalFare());
						 respBean.setFlight1ConvienceFee(airfare.getConvenienceFee());
						
					 }
					 if(flightNo == 2)
					 {
						 respBean.setFlight2TotalFare(airfare.getTotalFare());
						 respBean.setFlight2ConvienceFee(airfare.getConvenienceFee());
					 }
					/* if(flig.getResponse() == null || flig.getResponse().getResponse() == null ||flig.getResponse().getResponse().getFlightItinerary() == null)
					 {
						 GetBookingDetailResp getBookingDetails =gson.fromJson(apiRespFnl,GetBookingDetailResp.class);
						 Response_ resp = new Response_();
						 resp.setFlightItinerary(getBookingDetails);
						 flig.getResponse().ge;
					 }*/
					 flightNo++;
					 flightList.add(flig);	 
				 }		
			 }
			 if(airfarelistFnl.size() == 2)
			 {
				 
				 AirFareBean obj1 =airfarelistFnl.get(0);
				 AirFareBean obj2 = airfarelistFnl.get(1);
				 if((obj1.getApiStatus() == null && obj2.getApiStatus() == null) || (obj1.getApiStatus() != null && obj2.getApiStatus() == null && obj1.getApiStatus().equals("1")))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
					 }*/
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Ticket Booking unsuccessful. With Reason - "+respBean.getStatusMsg());
					 refundAmountImp(bookingRequest.getTravelId(),imei,0);
				 }
				 else if (obj1.getApiStatus() != null && obj2.getApiStatus() == null && obj1.getApiStatus().equals("9"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
					 }*/
					 respBean.setStatusCode("9");
					 respBean.setStatusMsg("Velocity Failed / Trace ID Expired.  With Reason - "+respBean.getStatusMsg());
					 releaseFlightPNR(bookingRequest.getTravelId(), imei,mKey);
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() == null && obj1.getApiStatus().equals("302"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
					 }*/
					 respBean.setStatusCode("302");
					 String Msg="";
					 if(respBean.getAmount() != 0)
					 {
						 Msg=Msg+ "Price has been increased by Amount:-"+respBean.getAmount();
					 }
					 else
					 {
						 Msg = Msg+" Time has changed for selected flight.";
					 }
					 respBean.setStatusMsg( Msg+"  With Reason - "+respBean.getStatusMsg());
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() == null && obj1.getApiStatus().equals("303"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
					 }*/
					 respBean.setStatusCode("303");
					 String Msg="";
					 if(respBean.getAmount() != 0)
					 {
						 Msg=Msg+ "Price has been decreased by Amount:-"+respBean.getAmount();
					 }
					 else
					 {
						 Msg = Msg+" Time has changed for selected flight.";
					 }
					 respBean.setStatusMsg(Msg+" With Reason - "+respBean.getStatusMsg());
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300") && obj2.getApiStatus().equals("9"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 flightTxnBean.setBookingId(obj2.getAirFareId()+commanUtilDao.getTrxId("TvlTxn"));
					 }*/
					 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,1);
					 respBean.setStatusCode("8");
					 respBean.setStatusMsg("First Flight has been booked with PNR -"+obj1.getPnrNo()+" and Booking ID-"+obj1.getBookingId()+". For Second Ticket Velocity Check/TraceID expired. With Reason - "+respBean.getStatusMsg());
					 releaseFlightPNR(bookingRequest.getTravelId(), imei,mKey);
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300") && obj2.getApiStatus().equals("302"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 flightTxnBean.setBookingId(obj2.getAirFareId()+commanUtilDao.getTrxId("TvlTxn"));
					 }*/
					 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,1);
					 respBean.setStatusCode("301");
					 String Msg="";
					 if(respBean.getAmount() != 0)
					 {
						 Msg=Msg+ "For Second Ticket, Price has been increaded by Amount:-"+respBean.getAmount()+".";
					 }
					 else
					 {
						 Msg = Msg+"For Second Ticket, Time has changed for selected flight.";
					 }
					 respBean.setStatusMsg("First Flight has been booked with PNR -"+obj1.getPnrNo()+" and Booking ID-"+obj1.getBookingId()+". "+Msg+". With Reason - "+respBean.getStatusMsg());
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300") && obj2.getApiStatus().equals("303"))
				 {
					 /*if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 flightTxnBean.setBookingId(obj2.getAirFareId()+commanUtilDao.getTrxId("TvlTxn"));
					 }
					 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,1);
					*/ respBean.setStatusCode("304");
					 String Msg="";
					 if(respBean.getAmount() != 0)
					 {
						 Msg=Msg+ "For Second Ticket, Price has been decreased by Amount:-"+respBean.getAmount()+".";
					 }
					 else
					 {
						 Msg = Msg+"For Second Ticket, Time has changed for selected flight.";
					 }
					 respBean.setStatusMsg("First Flight has been booked with PNR -"+obj1.getPnrNo()+" and Booking ID-"+obj1.getBookingId()+". "+Msg+" With Reason - "+respBean.getStatusMsg());
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300") && obj2.getApiStatus().equals("300"))
				 {
					 FlightItinerary flifhtIt = new FlightItinerary();
					/* if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(1);
						 flifhtIt.setPNR(obj1.getPnrNo());
						 txnFailure.setFlightItinerary(flifhtIt);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,2);
					 }*/
					 respBean.setStatusCode("300");
					 respBean.setStatusMsg("Both Tickets have booked with respective PNR and Booking Id. PNR - "+obj1.getPnrNo()+" Booking ID - "+obj1.getBookingId()+". PNR - "+obj2.getPnrNo()+" Booking ID - "+obj2.getBookingId()+".");
				 }
				 else if((obj1.getApiStatus() != null && obj2.getApiStatus() == null && obj1.getApiStatus().equals("300")) || (obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300")) || (obj1.getApiStatus() != null && obj2.getApiStatus() != null && obj1.getApiStatus().equals("300") && obj2.getApiStatus().equals("1")))
				 {
					/* if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 flightTxnBean.setBookingId(obj2.getAirFareId()+commanUtilDao.getTrxId("TvlTxn"));
					 }
					 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,1);
					*/ respBean.setStatusCode("8");
					 respBean.setStatusMsg("First Flight has been booked with PNR -"+obj1.getPnrNo()+" and Booking ID-"+obj1.getBookingId()+". Returning Ticket was unsuccessful. With Reason - "+respBean.getStatusMsg());
					 releaseFlightPNR(bookingRequest.getTravelId(), imei,mKey);
				 }
				 else if(obj1.getApiStatus() != null && obj2.getApiStatus() != null && !obj1.getApiStatus().equals("300") && !obj2.getApiStatus().equals("300"))
				 {
					/* if(txnStatus.equalsIgnoreCase("1000"))
					 {
						 txnFailure.setStatus(700);
						 flightTxnBean.setFareAmount(deductionAmount0.0);
						 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
					 }*/
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Ticket booking unsuccessful. With Reason - "+respBean.getStatusMsg());
					 releaseFlightPNR(bookingRequest.getTravelId(), imei,mKey);
				 }
				 else
				 {
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Internal Problem - Please Contact Admin for futher enquiry");
				 }
			 }
			 else if(airfarelistFnl.size() == 1)
			 {
				 AirFareBean obj1 =airfarelistFnl.get(0);
				 if(obj1.getApiStatus() != null && !obj1.getApiStatus().equals("300") &&  !obj1.getApiStatus().equals("302") && !obj1.getApiStatus().equals("303"))
				 {
					releaseFlightPNR(bookingRequest.getTravelId(), imei,mKey);		 
				 }
				 else
				 {}
				 if(obj1.getApiStatus() != null && obj1.getApiStatus().equals("300"))
				 {
					 respBean.setStatusMsg("Ticket has been booked with PNR and Booking Id. PNR - "+obj1.getPnrNo()+" Booking ID - "+obj1.getBookingId());
				 }
				 else
				 {
				}
				 
			 }
			 respBean.setTravelId(bookingRequest.getTravelId());
			 respBean.setFlightBookingList(flightList);
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			
			 try
			 {
				if(respBean.getStatusCode().equalsIgnoreCase("300"))
				{
					//ticketPath = servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "BhartiPay");
					String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "ROOT");//for production and test
					//String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "BhartiPay");//for local
					String pdfPath=ticketPath+"Bhartipay_Ticket_"+tripId+".pdf";
					Iterator<AirFareBean> itrterms = airfarelistFnl.iterator();
					String termsCondition="";
					String sourceAirportCode ="";
					String destAirportCode="";
					while(itrterms.hasNext())
					 {
						 AirFareBean airfare = itrterms.next();
						 int i=0;
						 if(airfare.getResponseString() != null)
						 {
							 String apiRespFnl =airfare.getResponseString();
							 FlightBookingResp flig = gson.fromJson(apiRespFnl,FlightBookingResp.class);
							 if(i==0)
							 {
								 int size=flig.getResponse().getResponse().getFlightItinerary().getSegments().size();
								 sourceAirportCode = flig.getResponse().getResponse().getFlightItinerary().getSegments().get(0).getOrigin().getAirport().getAirportCode();
								 destAirportCode = flig.getResponse().getResponse().getFlightItinerary().getSegments().get(size-1).getDestination().getAirport().getAirportCode();
								 i++;
							 }
							 List<FareRule> farerulelist =flig.getResponse().getResponse().getFlightItinerary().getFareRules();
							 Iterator<FareRule> itr2 = farerulelist.iterator();
							 while(itr2.hasNext())
							 {
								 FareRule farerule = itr2.next();
								 termsCondition = termsCondition.concat("<br><br>").concat("AirlineName  :-" + farerule.getAirline()).concat("<br>").concat(farerule.getFareRuleDetail());
							 }
						 
						 }
					 }
					String mailTemplate=sendSuccessMailPdf( customername,airfarelistFnl.get(0).getAirFareId() , airfarelistFnl.get(0).getTripId(),request,termsCondition,requestBean.getAggreatorId());
					new CommUtility().sendMail(emailId,"Booking Confirmation | "+ sourceAirportCode+"-"+destAirportCode+" | "+airfarelistFnl.get(0).getArrDate() , mailTemplate, pdfPath, aggreatorid, walletId, sendername);
				}
			 }
			 catch(Exception ex)
			 {
				 ex.printStackTrace();
			 }
			 return responseBean;
			 
		 }
		 catch(NumberFormatException ex)
		{
			logger.info("****************problem in execution*******flightBooking********e.message*****"+ex.getMessage());
			/*if(txnStatus.equalsIgnoreCase("1000"))
			 {
				txnFailure.setStatus(700);
				 flightTxnBean.setFareAmount(deductionAmount0.0);
				 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
			 }*/
			ex.printStackTrace();
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			respBean.setStatusMsg("Invalid Travel Id.");
			responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			return responseBean;
		}
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********flightBooking********e.message*******"+ex.getMessage());
			/* if(txnStatus.equalsIgnoreCase("1000"))
			 {
				 txnFailure.setStatus(700);
				 flightTxnBean.setFareAmount(deductionAmount0.0);
				 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,txnFailure,0);
			 }*/
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 if(flightBookingresp.getResponse().getError().getErrorMessage() != null)
			 {
				 respBean.setStatusMsg(flightBookingresp.getResponse().getError().getErrorMessage());
			 }
			 else
			 {
				 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
			 }
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
	}
	
	public BookingResponseBean bookFlight(BookingRequest bookingRequest , AirFareBean airFareBean /*, FlightTxnBean flightTxnBean*/ , int isLCC , String mKey)
	{
		 logger.info("***********Start Execution *******bookFlight*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 TicketNonLccRequest ticketNonLccRequest = new TicketNonLccRequest();
		 BookingResponseBean respBean = new BookingResponseBean();
		 FlightBookingResp flightBookingresp = new FlightBookingResp();
		 FlightBookingResp flightBookingrespTicket = new FlightBookingResp();
		 GetBookingDetailResp getBookingDetails = new GetBookingDetailResp();
		 String apiResp ="";
		 String apiRespTick="";
		 //String upStatus="";
		 //Response_ failureResp = new Response_();
		 String bookticketInput=null;
		 try
			 {
			 reqbean = airTravelDao.getAuthDetails("Air");
			 List<Passenger> passList = bookingRequest.getPassengers();
			 Iterator<Passenger> itr = passList.iterator();
			 while(itr.hasNext())
			 {
				 Passenger pass = itr.next();
				 if(pass.getIsLeadPax())
				 {
					 airFareBean.setPassName(pass.getFirstName()+" "+pass.getLastName());
				 }
			 }
			 if(isLCC != 1)
			 {
				/* flightTxnBean = transactionDao.payForFlight(flightTxnBean);
				 if(!flightTxnBean.getErrorStatus().equalsIgnoreCase("1000"))
				 { 
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode("9");
					 respBean.setStatusMsg(commanUtil.getErrorMsg(flightTxnBean.getErrorStatus()));
					 return respBean;
				 }*/
				 
				 apiResp=airApiManager.flightBooking(bookingRequest);
				 if(apiResp.equals("400"))
				 {
					 airFareBean.setStatus("TicketTimeOut");
					 respBean.setStatusCode("1");
					 respBean.setStatus("FAILED");
					 respBean.setStatusMsg("Connection Time-Out From Server."); 
					 return respBean;
				 }
				 airFareBean.setResponseString(apiResp);
				 flightBookingresp =gson.fromJson(apiResp,FlightBookingResp.class);;
				 respBean.setFlightBooking(flightBookingresp);
				 
				 if(flightBookingresp.getResponse().getError().getErrorCode() != 0 )
				 {
					// failureResp.setStatus(700);
					// upStatus=transactionDao.payForFlightUpdate(flightTxnBean,failureResp);
					 respBean.setStatus("FAILED");
					 if(flightBookingresp.getResponse().getError().getErrorCode() == 6)
						 respBean.setStatusCode("9");
					 else
						 respBean.setStatusCode("1");
					 if(flightBookingresp.getResponse().getError().getErrorMessage() != null)
					 {
						 respBean.setStatusMsg("Something went wrong. Please try after sometime.");
					 }
					 else
					 {
						 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
					 }
					 return respBean;
				 }
				 if(flightBookingresp.getResponse().getResponse().getPNR() != null)
					 airFareBean.setPnrNo(flightBookingresp.getResponse().getResponse().getpNR());
					if(flightBookingresp.getResponse().getResponse().getBookingId() != null)
					 airFareBean.setBookingId(Long.toString(flightBookingresp.getResponse().getResponse().getBookingId()));
				 airFareBean.setStatus("BookingDone");
				 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,flightBookingresp.getResponse().getResponse());
				 if(flightBookingresp.getResponse().getResponse().getIsPriceChanged() == true )
				 {
					 //airFareBean.setStatus("FareQuoteDone");
					 double PreviousAmt =  airFareBean.getTotalAmt();
						
					 assignAirFare(flightBookingresp,airFareBean);
					 flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().setChargedFare(airFareBean.getTotalAmt());
					 double amtChanged=airFareBean.getTotalFare() - PreviousAmt;
					 if(amtChanged < 0)
					 {
						 respBean.setAmount(-amtChanged);
						 airFareBean.setTotalRefundAmt(-(amtChanged)+airFareBean.getTotalRefundAmt());
						 respBean.setStatus("SUCCESS");
						 respBean.setStatusCode("303");
						 respBean.setStatusMsg("Price has been decreased by Amount:-"+respBean.getAmount()+".");
						 return respBean;
					 }
					 else
					 {
						 respBean.setAmount(amtChanged);
						 respBean.setStatus("SUCCESS");
						 respBean.setStatusCode("302");
						 respBean.setStatusMsg("Price has been increased by Amount:-"+respBean.getAmount()+".");
						 return respBean;
					 }
					 
				 }
				 else if(flightBookingresp.getResponse().getResponse().getIsTimeChanged() == true)
				 {
					 respBean.setAmount(0.0);
					 respBean.setStatus("SUCCESS");
					 respBean.setStatusCode("302");
					 respBean.setStatusMsg("Time has changed for selected flight.");
					 return respBean;					 
				 }
				 else
				 {
					 ticketNonLccRequest.setBookingId(flightBookingresp.getResponse().getResponse().getFlightItinerary().getBookingId());
					 ticketNonLccRequest.setEndUserIp(reqbean.getEndUserIp());
					 ticketNonLccRequest.setPNR(flightBookingresp.getResponse().getResponse().getFlightItinerary().getPNR());
					 ticketNonLccRequest.setTokenId(reqbean.getTokenId());
					 ticketNonLccRequest.setTraceId(bookingRequest.getTraceId());
					  bookticketInput = gson.toJson(ticketNonLccRequest);
				 }
			 }
			 else
			 {
				 
				 //flightTxnBean = transactionDao.payForFlight(flightTxnBean);
				 /*if(!flightTxnBean.getErrorStatus().equalsIgnoreCase("1000"))
				 { 
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode("9");
					 System.out.println("payForFlight**********************************************************"+flightTxnBean.getStatus());
					 
					 respBean.setStatusMsg(commanUtil.getErrorMsg(flightTxnBean.getErrorStatus()));
					 return respBean;
				 }*/
				  bookticketInput = gson.toJson(bookingRequest);
			 }
			 /*double deductionAmount = airFareBean.getTotalFare();
			 String userId = airFareBean.getUserId();
			 String AggreatorId = airFareBean.getAggreatorId();	
			 FlightTxnBean flightTxnBean = new FlightTxnBean();
			 flightTxnBean.setAggreatorId(AggreatorId);
			 flightTxnBean.setBookingId(airFareBean.getAirFareId());
			 flightTxnBean.setFareAmount(deductionAmount);
			 if(flightBookingresp.getResponse().getResponse().getFlightItinerary().getPNR() != null)
			 {
				 flightTxnBean.setPnrNumber(flightBookingresp.getResponse().getResponse().getFlightItinerary().getPNR());
			 }
			 flightTxnBean.setUserId(userId);
			// flightTxnBean.setIpIemi(reqbean.getEndUserIp());
			 WalletMastBean walletmast = walletUserDao.showUserProfile(userId);
			 flightTxnBean.setWalletId(walletmast.getWalletid());
			 flightTxnBean = transactionDao.payForFlight(flightTxnBean);
			 if(flightTxnBean.getStatus() != "1000")
			 { 
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg(commanUtil.getErrorMsg(flightTxnBean.getStatus()));
				 responseBean.setResponse(oxyJWTSignUtil.generateToken(gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }*/
			 
			 
			 apiRespTick=airApiManager.bookTicket(bookticketInput);
			 
			 if(apiRespTick.equals("400"))
			 {
				 airFareBean.setStatus("TicketTimeOut");
				 respBean.setStatusCode("1");
				 respBean.setStatus("FAILED");
				 respBean.setStatusMsg("Connection Time-Out From Server."); 
				 return respBean;
			 }
			
			 airFareBean.setResponseString(apiRespTick);
			 System.out.println("\n\n\n\n\n\n********************length***********"+apiRespTick.length());
			 flightBookingrespTicket=gson.fromJson(apiRespTick,FlightBookingResp.class);
			 //adding ticket details
			 ticketDetails(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary(), airFareBean,passList);
			 if(flightBookingrespTicket.getResponse().getError().getErrorCode() != 0 )
			 {
				 //failureResp.setStatus(700);
				 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,failureResp);
				 respBean.setStatus("FAILED");
				 if(flightBookingrespTicket.getResponse().getError().getErrorCode() == 6)
					 respBean.setStatusCode("9");
				 else
					 respBean.setStatusCode("1");
				 if(flightBookingrespTicket.getResponse().getError().getErrorMessage() != null)
				 {
					 respBean.setStatusMsg(flightBookingrespTicket.getResponse().getError().getErrorMessage());
				 }
				 else if(flightBookingrespTicket.getResponse().getResponse().getMessage() != null)
				 {
					 respBean.setStatusMsg(flightBookingrespTicket.getResponse().getResponse().getMessage());
				 }
				 else
				 {
					 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
				 }
				 return respBean;
			 }
			 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,flightBookingrespTicket.getResponse().getResponse());
			 /*if(upStatus.equalsIgnoreCase("FAILED"))
			 {
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 if(flightBookingresp.getResponse().getError().getErrorMessage() != null)
				 {
					 respBean.setStatusMsg(flightBookingresp.getResponse().getError().getErrorMessage());
				 }
				 else
				 {
					 respBean.setStatusMsg("Transaction Failed."); 
				 }
				 return respBean;
			 }*/
			 respBean.setFlightBooking(flightBookingrespTicket);
			 
			 try
			 {
				 if(flightBookingrespTicket.getResponse().getResponse().getIsPriceChanged() == true)
				 {
					 //airFareBean.setStatus("FareQuoteDone");
					 double PreviousAmt =  airFareBean.getTotalAmt();
					
					 assignAirFare(flightBookingrespTicket,airFareBean);
					 flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().getFare().setChargedFare(airFareBean.getTotalFare());
					 double amtChanged=airFareBean.getTotalFare() - PreviousAmt;
					 if(amtChanged < 0)
					 {
						 respBean.setAmount(-amtChanged);
						 airFareBean.setTotalRefundAmt(-(amtChanged)+airFareBean.getTotalRefundAmt());
						 respBean.setStatus("SUCCESS");
						 respBean.setStatusCode("303");
						 respBean.setStatusMsg("Price has been decreased by Amount:-"+respBean.getAmount()+".");
						 return respBean;
					 }
					 else
					 {
						 respBean.setAmount(amtChanged);
						 respBean.setStatus("SUCCESS");
						 respBean.setStatusCode("302");
						 respBean.setStatusMsg("Price has been decreased by Amount:-"+respBean.getAmount()+".");
						 return respBean;
					 }
					 //wallet transaction reversal
					
				 }			 
				 else if( flightBookingrespTicket.getResponse().getResponse().getIsTimeChanged() == true)
				 {
					 respBean.setAmount(0.0);
					 respBean.setStatus("SUCCESS");
					 respBean.setStatusCode("302");
					 respBean.setStatusMsg("Time has changed for selected flight.");
					 return respBean;
				 }
				 if(flightBookingrespTicket.getResponse().getResponse().getTicketStatus() != 1 && flightBookingrespTicket.getResponse().getResponse().getTicketStatus() != 5 && flightBookingrespTicket.getResponse().getResponse().getTicketStatus() != 6  )
				 {
					 //failureResp.setStatus(700);
					 //upStatus=transactionDao.payForFlightUpdate(flightTxnBean,failureResp);
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Ticketing Status is Unsuccessful."); 
					 return respBean;
				 }
				// throw new NullPointerException();
			 
			 airFareBean.setApiStatus(respBean.getStatusCode());
			/* if(!flightBookingValidation(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary(),airFareBean))
			 {
				 upStatus=transactionDao.payForFlightUpdate(flightTxnBean,flightBookingrespTicket.getResponse().getResponse());
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg("Amount Mismatch.");
				 return respBean;
			 }*/
			
			 
			if(flightBookingrespTicket.getResponse().getResponse().getpNR() != null)
			 airFareBean.setPnrNo(flightBookingrespTicket.getResponse().getResponse().getpNR());
			if(flightBookingrespTicket.getResponse().getResponse().getBookingId() != null)
			 airFareBean.setBookingId(Long.toString(flightBookingrespTicket.getResponse().getResponse().getBookingId()));
			if(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().getInvoiceNo() != null)
			 airFareBean.setInvoiceNo(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().getInvoiceNo());
			if(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().getStatus() != null)
			 airFareBean.setStatus(Integer.toString(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().getStatus()));
			//if(flightBookingrespTicket.getResponse().getResponse().getFlightItinerary().get != null)
			
			 respBean.setFlightBooking(flightBookingrespTicket);
			 airFareBean.setApiStatus(respBean.getStatusCode());
			 airFareBean.setStatus("TicketBooked");
			 airTravelDao.saveFareQuote(airFareBean);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 return respBean;
			 }
			 catch(NullPointerException ex)
			 {
				 logger.info("*****************************Problem in bookFlight********e.message"+ex.getMessage());
				 ex.printStackTrace();
				 if(flightBookingrespTicket.getResponse().getResponse().getPNR() != null)
				 {
					 TicketNonLccRequest bookingdetailrequest = new TicketNonLccRequest();
					 bookingdetailrequest.setEndUserIp(reqbean.getEndUserIp());
					 bookingdetailrequest.setTokenId(reqbean.getTokenId());
					 if(airFareBean.getPassName() != null)
					 {
						 String[] name = airFareBean.getPassName().split(" ");
						 if(name.length==2)
						 {
							 bookingdetailrequest.setFirstName(name[0]);
							 bookingdetailrequest.setLastName(name[1]);
						 }
						 else
						 {
							 throw new Exception();
						 }
					 }
					 else
					 {
						 throw new Exception();
					 }
					 if(flightBookingrespTicket.getResponse().getResponse() != null && flightBookingrespTicket.getResponse().getResponse().getPNR() != null) 
					 {
						 bookingdetailrequest.setPNR(flightBookingrespTicket.getResponse().getResponse().getPNR());
					 }
					 else
					 {
						 bookingdetailrequest.setTraceId(bookingRequest.getTraceId());
					 }
					 apiResp = airApiManager.getBookingDetails(bookingdetailrequest);
					 if(apiResp.equals("400"))
					 {
						 airFareBean.setStatus("TicketTimeOut");
						 respBean.setStatusCode("1");
						 respBean.setStatus("FAILED");
						 respBean.setStatusMsg("Connection Time-Out From Server."); 
						 return respBean;
					 }
					
					 
					 getBookingDetails = gson.fromJson(apiResp, GetBookingDetailResp.class);
					 
					 ticketDetails(getBookingDetails.getResponse().getFlightItinerary(), airFareBean,passList);
					 flightBookingrespTicket.getResponse().getResponse().setFlightItinerary(getBookingDetails.getResponse().getFlightItinerary());
					 
					 apiResp = gson.toJson(flightBookingrespTicket);
					 airFareBean.setResponseString(apiResp);
					 respBean.setFlightBooking(flightBookingrespTicket);
					 airFareBean.setApiStatus(respBean.getStatusCode());
					 airFareBean.setStatus("TicketBooked");
					 airTravelDao.saveFareQuote(airFareBean);
					 respBean.setStatus("SUCCESS");
					 respBean.setStatusCode("300");
					 respBean.setStatusMsg("SUCCESS");
					 return respBean;
				 }
			 }
			airFareBean.setStatus("TicketBooked");
			 airTravelDao.saveFareQuote(airFareBean);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 return respBean;
		 }

		catch(Exception ex)
		 {
			logger.info("****************problem in bookFlight*****************e.message"+ex.getMessage());
			ex.printStackTrace();
			if(flightBookingrespTicket != null && flightBookingrespTicket.getResponse() != null && flightBookingrespTicket.getResponse().getResponse() != null && flightBookingrespTicket.getResponse().getResponse().getPNR() != null)
			 {
				if(flightBookingrespTicket.getResponse().getResponse().getpNR() != null)
					 airFareBean.setPnrNo(flightBookingrespTicket.getResponse().getResponse().getpNR());
				airFareBean.setStatus("TicketBooked");
				airFareBean.setApiStatus("300");
				 airTravelDao.saveFareQuote(airFareBean);
				 respBean.setStatus("SUCCESS");
				 respBean.setStatusCode("300");
				 respBean.setStatusMsg("Please Check Ticket Details using getBookingDetails");
				 return respBean;
			 }
			/*if(flightTxnBean.getErrorStatus().equalsIgnoreCase("1000") && !upStatus.equalsIgnoreCase("FAILED"))
			 { b
				failureResp.setStatus(700);
				upStatus=transactionDao.payForFlightUpdate(flightTxnBean,failureResp);
			 }*/
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			if(flightBookingrespTicket != null && flightBookingrespTicket.getResponse()!= null && flightBookingrespTicket.getResponse().getError()!= null && flightBookingrespTicket.getResponse().getError().getErrorMessage() != null)
			 {
				 respBean.setStatusMsg(flightBookingrespTicket.getResponse().getError().getErrorMessage());
			 }
			else if(flightBookingresp != null && flightBookingresp.getResponse()!= null && flightBookingresp.getResponse().getError() != null && flightBookingresp.getResponse().getError().getErrorMessage() != null)
			{
				respBean.setStatusMsg(flightBookingresp.getResponse().getError().getErrorMessage());
			}
			 else
			 {
				 respBean.setStatusMsg("FAILED"); 
			 }
			return respBean;
		 }
	}
	
	public BookingResponseBean refundAmountImp(String travelId , String imei,int conv)
	{
		 logger.info("***********Start Execution *******refundAmountImp*******");
		 String mid=null;
		 String pgRefId=null;
		 String appName=null;
		 double amt=0.0;
		 String orderNo=null;
		 BookingResponseBean respBean = new BookingResponseBean();
	
		 try
		 {
			 double totalAmt=0.0;
			 double TotlrefundAmt=0.0;
			 double refundAmt=0.0;
			 double walletAmtRefund=0.0;
			 double walletAmt=0.0;
			 double pgRefund=0.0;
			 double pgRefundItr=0.0;
			 double pendingRefund=0.0;
			 String walletId=null;
			 String mobile=null;
			 String userId=null;
			 String pgTxnId=null;
			 String shippingDtls=null;
			 String billingDtls=null;
			 String aggreatorId=null;
			 String bookingId=null;
			 FlightTxnBean flightTxnBean = new FlightTxnBean();
			 List<AirFareBean> airfarelist = airTravelDao.getFareQuoteByTripId(travelId,"");
			 userId=airfarelist.get(0).getUserId();
			 mobile=airfarelist.get(0).getMobileNo();
			 walletId=airfarelist.get(0).getWalletId();
			 aggreatorId=airfarelist.get(0).getAggreatorId();
			 Iterator<AirFareBean> itr1 =airfarelist.iterator();
			 while(itr1.hasNext())
			 {
				 AirFareBean airFare = new AirFareBean();
				 airFare=itr1.next();
				 totalAmt +=(airFare.getPgAmt()+airFare.getWalletAmt());
				 if((airFare.getStatus().trim().equalsIgnoreCase("FareQuoteDone") ||  airFare.getStatus().trim().equalsIgnoreCase("PnrReleased")) /*&& !airFare.getEventStatus().trim().equalsIgnoreCase("AmountRefunded")*/)
				 {
					 if(conv == 1)
						 {TotlrefundAmt+= airFare.getTotalFare()-airFare.getConvenienceFee();}
					 else
					 	{TotlrefundAmt+= airFare.getTotalFare();}
					 pendingRefund = airFare.getTotalRefundAmt();
				 }	
				 else if(airFare.getCancellationStatus() != null && (airFare.getCancellationStatus().trim().equalsIgnoreCase("Cancelled") || airFare.getCancellationStatus().trim().equalsIgnoreCase("PartiallyCancelled")) /* && !airFare.getEventStatus().trim().equalsIgnoreCase("AmountRefunded")*/)
				 {
					 TotlrefundAmt += airFare.getCancellationRefundAmount();
					 pendingRefund = airFare.getTotalRefundAmt();
				 }
				 else if(airFare.getTotalRefundAmt() != 0)
				 {
					 pendingRefund = airFare.getTotalRefundAmt();
				 }
				 pgTxnId=airFare.getPgTxn();
			 }
			 refundAmt = TotlrefundAmt + pendingRefund - (airfarelist.get(0).getWalletAmtRefund()+airfarelist.get(0).getPgAmtRefund());
			 /*if(refundAmount != refundAmt)
			 {
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg("Refund Amount Validation Failed.");
				 return respBean;
			 }*/
			 TransactionBean txnBean = transactionDao.showTxnByRespTxnID(airfarelist.get(0).getTripId(),40);
			 if(txnBean != null && txnBean.getResptxnid() != null)
			 {
				 if(txnBean.getResptxnid().contains("-"))
				 {
					 StringTokenizer str = new StringTokenizer(txnBean.getResptxnid(), "-");
					 str.nextToken();
					 int val = Integer.parseInt(str.nextToken());
					 val++;
					 bookingId =airfarelist.get(0).getTripId()+"-"+val;
				 }
				 else
					 {
					 bookingId =airfarelist.get(0).getTripId()+"-1";
					 }
					 
			 }
			 else
			 {
				 bookingId =airfarelist.get(0).getTripId();
			 }
			 walletAmt = airfarelist.get(0).getWalletAmt()-airfarelist.get(0).getWalletAmtRefund();
			 flightTxnBean.setUserId(userId);
			 flightTxnBean.setWalletId(walletId);
			 flightTxnBean.setBookingId(/*airfarelist.get(0).getTripId()*/bookingId);
			 flightTxnBean.setIpIemi(imei);
			 flightTxnBean.setAggreatorId(aggreatorId);
			 if(walletAmt != 0 )
			 {
				 if(walletAmt > refundAmt)
				 {
					 pgRefund=0.0;
					 walletAmtRefund = refundAmt;
				 }
				 else
				 {
					 walletAmtRefund = walletAmt;
					 pgRefund = refundAmt - walletAmtRefund;
				 }
				 flightTxnBean.setFareAmount(walletAmtRefund);
				 String flag = transactionDao.payForFlightUpdate(flightTxnBean,null,0);
				 if(flag.equals("SUCCESS"))
				 {
					 int flag1=airTravelDao.updateFareQuote(totalAmt, -1, -1, -1 ,walletAmtRefund, "","","","AmountRefunded","",travelId);
				 }
				 else
				 {
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Refund Process UnSuccessful.");
					 return respBean;
				 }
			 }
			 else 
			 {
				 pgRefund = refundAmt - walletAmtRefund;
				 
				 
			 }
			if(pgRefund > 0)
			{
				String requestParam=null;
				pgRefundItr=pgRefund;
				String[] pgTxnArr = pgTxnId.split(",");
				WalletConfiguration walletConfiguration=commanUtilDao.getWalletConfiguration(aggreatorId);
				 mid=walletConfiguration.getPgMid();
				 String key =walletConfiguration.getPgEncKey();
				 appName =walletConfiguration.getPgAgId();
				 
				 String respPgRefund=null;
				 for(int i=(pgTxnArr.length-1) ;i >= 0 ;i--)
				 {
					  PaymentGatewayTxnBean paymentGatewayTrxBean=transactionDao.viewPGTrxFlight(pgTxnArr[i]);
					  pgRefId=paymentGatewayTrxBean.getPgId();
					  orderNo=paymentGatewayTrxBean.getTrxId();					  
					  if(paymentGatewayTrxBean.getTrxAmount() < pgRefundItr)
					  {
						  amt=paymentGatewayTrxBean.getTrxAmount();
						  pgRefundItr=pgRefundItr-amt;
						  requestParam="requestparameter=MID="+mid+"|"+"PGTxnId="+pgRefId+"|"+"Amount="+amt+"|"+"OrderId="+orderNo;
						  logger.info("############################################################################## requestparams de ##"+requestParam);
						  requestParam = AES128Bit.encrypt(requestParam, key);
						  respPgRefund = airApiManager.pgTxnApi(requestParam,mid);
						  if(!respPgRefund.equals("01"))
						  {
							  break;
						  }
					  }
					  else
					  {
						  amt=pgRefundItr;
						  requestParam="requestparameter=MID="+mid+"|"+"PGTxnId="+pgRefId+"|"+"Amount="+amt+"|"+"OrderId="+orderNo;
						  logger.info("############################################################################## requestparams de ##"+requestParam);
						  requestParam = AES128Bit.encrypt(requestParam, key);
						  respPgRefund = airApiManager.pgTxnApi(requestParam,mid);
						  break;
					  }
				 }
				 if(!respPgRefund.equals("01"))
				 {
					 respBean.setStatus("FAILED");
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Refund Process UnSuccessful.");
					 return respBean;
				 }
			}
		
			 int flag1=airTravelDao.updateFareQuote(totalAmt, -1, -1, pgRefund ,-1, "","","","AmountRefunded","",travelId);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 return respBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info("************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 return respBean;
		 }
	
	}
	
	
	public AirFareBean assignAirFare(FlightBookingResp flightBookingresp , AirFareBean airFareBean)
	{
		logger.info("**************start execution ******assignAirFare****");
		try
		{
			if(airFareBean.getAppliedOn().trim().equalsIgnoreCase("offeredFare"))
			{
				airFareBean.setTotalFare(flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getOfferedFare()+airFareBean.getConvenienceFee());
			}
			else
			{
				airFareBean.setTotalFare(flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getPublishedFare()+airFareBean.getConvenienceFee());
			}
			airFareBean.setTotalBaseFare(flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getBaseFare());
			airFareBean.setTotalTax(flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getPublishedFare() - flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getBaseFare());
			airFareBean.setCommissionEarned(flightBookingresp.getResponse().getResponse().getFlightItinerary().getFare().getCommissionEarned());
			List<Passenger> passengerlist = flightBookingresp.getResponse().getResponse().getFlightItinerary().getPassenger();
			Iterator<Passenger> itr = passengerlist.iterator();
			int adultCount = 0;
			int childCount=0;
			int infantCount=0;
			
			
			while(itr.hasNext())
			{
				Passenger passenger = itr.next();
				if(passenger.getPaxType() == 1)
				{
					adultCount++;
					airFareBean.setAdultBaseFare(passenger.getFare().getBaseFare());
					airFareBean.setAdultTax(passenger.getFare().getPublishedFare() - passenger.getFare().getBaseFare());
				}
				if(passenger.getPaxType() == 2)
				{
					childCount++;
					airFareBean.setChildBaseFare(passenger.getFare().getBaseFare());
					airFareBean.setChildTax(passenger.getFare().getPublishedFare() - passenger.getFare().getBaseFare());
				}
				if(passenger.getPaxType() == 3)
				{
					infantCount++;
					airFareBean.setInfantBaseFare(passenger.getFare().getBaseFare());
					airFareBean.setInfantTax(passenger.getFare().getPublishedFare() - passenger.getFare().getBaseFare());
				}		
			}
			airFareBean.setAdultCount(adultCount);
			airFareBean.setChildCount(childCount);
			airFareBean.setInfantCount(infantCount);
			airTravelDao.saveFareQuote(airFareBean);
		}
		catch(Exception ex)
		{
			logger.info("******problem in ************assignAirFare************e.message****"+ex.getMessage());
			ex.printStackTrace();
		}
		return  airFareBean;
	}
	
	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("getBookingDetails")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean getBookingDetails(RequestBean requestBean , @Context HttpServletRequest request)
	{
		 logger.info("***********Start Execution *******getBookingDetails*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 FlightBookingResp getBookingDetailsRespBean = new FlightBookingResp();
		 GetBookingDetailsResp getBookingDetailsResp = new GetBookingDetailsResp();
		 BookingResponseBean respBean = new BookingResponseBean();
		 TicketNonLccRequest bookingdetailrequest = new TicketNonLccRequest();
		 String apiResp ="";
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"getBookingDetails");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******getBookingDetails************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******getBookingDetails**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****getBookingDetails***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****getBookingDetails******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 FlightBookingResp flightBookingresp = new FlightBookingResp();
			 JSONObject claimJson = new JSONObject(claim); 
			 bookingdetailrequest = gson.fromJson(claimJson.toString() ,TicketNonLccRequest.class);
			 respBean.setTravelId(bookingdetailrequest.getTravelId());
			 reqbean = airTravelDao.getAuthDetails("Air");
			 List<AirFareBean> airFareBeanList = airTravelDao.getFareQuoteByTripId(bookingdetailrequest.getTravelId(),"");
			 bookingdetailrequest.setEndUserIp(reqbean.getEndUserIp());
			 bookingdetailrequest.setTokenId(reqbean.getTokenId());
			 
			 apiResp = airApiManager.getBookingDetails(bookingdetailrequest);
			 getBookingDetailsRespBean = gson.fromJson(apiResp, FlightBookingResp.class);
			 
			 respBean.setGetBookingDetails(getBookingDetailsRespBean);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(NumberFormatException ex)
		{
			logger.info("****************problem in execution********getBookingDetails**********e.message*****"+ex.getMessage());
			ex.printStackTrace();
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			respBean.setStatusMsg("Invalid Travel Id.");
			responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			return responseBean;
		}
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********getBookingDetails********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			 logger.info(serverName+"************problem in **********getBookingDetails********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	}
	
	@POST
	@Path("/releasePNR")
	@Produces({MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean releasePNR(RequestBean requestBean , @Context HttpServletRequest request ,@HeaderParam("IPIMEI") String imei)
	{
		 logger.info("***********Start Execution *******getBookingDetails*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 FareQuote releasePNRResp = new FareQuote();
		 BookingResponseBean respBean = new BookingResponseBean();
		 CancelRequest cancelRequest = new CancelRequest();
		 String apiResp ="";
		String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"releasePNR");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******releasePNR************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******releasePNR**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****releasePNR***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****releasePNR******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 FlightBookingResp flightBookingresp = new FlightBookingResp();
			 JSONObject claimJson = new JSONObject(claim); 
			 respBean.setTravelId(claimJson.getString("TravelId"));
			 responseBean = releaseFlightPNR(claimJson.getString("TravelId"), imei,mKey);
			 return responseBean;
			
		 }
		 catch(NumberFormatException ex)
		{
			logger.info("****************problem in execution********releasePNR**********e.message*****"+ex.getMessage());
			ex.printStackTrace();
			respBean.setStatus("FAILED");
			respBean.setStatusCode("1");
			respBean.setStatusMsg("Invalid Booking Id.");
			responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			return responseBean;
		}
		 catch(Exception ex)
		 {
			// logger.info(serverName+"************problem in **********releasePNR********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			 logger.info(serverName+"************problem in **********releasePNR********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	}
	
	public ResponseBean releaseFlightPNR(String travelId,String imei,String mKey)
	{
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 FareQuote releasePNRResp = new FareQuote();
		 BookingResponseBean respBean = new BookingResponseBean();
		 CancelRequest cancelRequest = new CancelRequest();
		 String apiResp ="";
		try
		{
		 reqbean = airTravelDao.getAuthDetails("Air");
		 List<AirFareBean> airFareBeanList = airTravelDao.getFareQuoteByTripId(travelId,"BookingDone");
		 cancelRequest.setEndUserIp(reqbean.getEndUserIp());
		 cancelRequest.setTokenId(reqbean.getTokenId());
		 Iterator<AirFareBean> itr = airFareBeanList.iterator();
		 while(itr.hasNext())
		 {
			 AirFareBean airFare = new AirFareBean();
			 airFare = itr.next();
			 cancelRequest.setBookingId(airFare.getBookingId());
			 cancelRequest.setSource(airFare.getSource());
			 apiResp = airApiManager.releasePNR(cancelRequest);
			 releasePNRResp = gson.fromJson(apiResp, FareQuote.class);
			 if(releasePNRResp.getResponse().getResponseStatus() == 1)
			 {
				 airFare.setStatus("PnrReleased");
				 String airfareid = airTravelDao.saveFareQuote(airFare);
			 }
		 }
		 if(airFareBeanList.size() <1)
		 {
			 refundAmountImp(travelId,imei,0);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("No PNR for releasing.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 if(releasePNRResp.getResponse().getResponseStatus() == 1)
		 {
			 refundAmountImp(travelId,imei,0);
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("PNR released successfully.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 else
		 {
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Ticket . Please contact system Admin.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			logger.info("*****problem in releaseFlightPNR*******e.message**"+ex.getMessage());
			ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	}
	public boolean flightBookingValidation(FlightItinerary flightitinerary , AirFareBean airFareBean)
	{
		logger.info("********************Start Execution************flightBookingValidation***");
		try
		{
			if(flightitinerary.getFare().getPublishedFare() != airFareBean.getTotalFare())
			{
				return false;
			}
		}
		catch(NumberFormatException ex)
		{
			logger.info("****************problem in execution******e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			logger.info("****************problem in execution******e.message*****"+ex.getMessage());
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/resendEmail")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean resendEmail(RequestBean requestBean , @Context HttpServletRequest request )
	{
		 logger.info("***********Start Execution *******resendEmail*******");
		 ResponseBean responseBean = new ResponseBean();
		 TicketNonLccRequest ticketNonLccRequest = new TicketNonLccRequest();
		 BookingResponseBean respBean = new BookingResponseBean();
		 FlightBookingResp flightBookingresp = new FlightBookingResp();
		 List<AirFareBean> airfarelist = new ArrayList<AirFareBean>();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"flightBooking");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******resendEmail************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******resendEmail**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****resendEmail***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****resendEmail******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 JSONObject claimJson = new JSONObject(claim); 
			 

			 
			 String walletId="";
			 String sendername="";
			 String emailId="";
			 String termsCondition="";
			 String tripId=claimJson.getString("travelId");
			/* if(claimJson.getString("emailId") == null || )
			 {
				 
			 }*/
			// aggId, walletid, sendername
			 String aggreatorid=requestBean.getAggreatorId();
			 
			 airfarelist = airTravelDao.getFareQuoteByTripId(claimJson.getString("travelId"),"TicketBooked");
			
			 WalletMastBean walletmast = walletUserDao.showUserProfile(airfarelist.get(0).getUserId());
			 walletId = walletmast.getWalletid();
			 sendername = walletmast.getName();
			 emailId=claimJson.getString("emailId");
			 Iterator<AirFareBean> itr = airfarelist.iterator();
			 while(itr.hasNext())
			 {
				 AirFareBean airfare = itr.next();
				 String apiRespFnl =airfare.getResponseString();
				 FlightBookingResp flig = gson.fromJson(apiRespFnl,FlightBookingResp.class);
				 List<FareRule> farerulelist =flig.getResponse().getResponse().getFlightItinerary().getFareRules();
				 Iterator<FareRule> itr2 = farerulelist.iterator();
				 while(itr2.hasNext())
				 {
					 FareRule farerule = itr2.next();
					 termsCondition = termsCondition.concat("\\n\\n\\n").concat("AirlineName  :-" + farerule.getAirline()).concat("\\n").concat(farerule.getFareRuleDetail());
				 }
				 
				 
			 }
			//String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "ROOT");//for production and test
				String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "BhartiPay");//for local
					String pdfPath=ticketPath+"Bhartipay_Ticket_"+tripId+".pdf";
					String mailTemplate=sendSuccessMailPdf(sendername, airfarelist.get(0).getAirFareId() , tripId,request,termsCondition,requestBean.getAggreatorId());
					new CommUtility().sendMail(emailId, "BhartiPay:- Flight Ticket", mailTemplate, pdfPath, aggreatorid, walletId, sendername);

				respBean.setStatus("SUCCESSFUL");
				 respBean.setStatusCode("300");
				 respBean.setStatusMsg("Email has been sent successfully on "+emailId+" emailId.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(respBean, HashMap.class), mKey));
			 return responseBean;
			 
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********flightBooking********e.message*******"+ex.getMessage());
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING."); 
			 responseBean.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(respBean, HashMap.class), mKey));
			return responseBean;
		 }
	}
	
	
	
	public String ticketDetails( FlightItinerary flightBookingresp, AirFareBean airFare,List<Passenger> passList)
	{
		logger.info("******************Start Execution*********ticketDetails");
		TicketDetails ticketDetails = new TicketDetails();
		
	
		List<Segment> segmentList = new ArrayList<Segment>();
		List<Passenger> passangerList = new ArrayList<Passenger>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
		 DateFormat f2 = new SimpleDateFormat("E, dd MMM yy HH:mm");
		 String passengerCount="";
		try
		{
			ticketDetails.setAirFareId(airFare.getAirFareId());
			ticketDetails.setTripId(airFare.getTripId());
			ticketDetails.setBaseFare(flightBookingresp.getFare().getBaseFare());
			ticketDetails.setConvenienceFee(airFare.getConvenienceFee());
			ticketDetails.setBookingDate(f2.format(airFare.getFareQdate()));
			ticketDetails.setTaxes(flightBookingresp.getFare().getPublishedFare() - flightBookingresp.getFare().getBaseFare());
			segmentList= flightBookingresp.getSegments();
			ticketDetails.setSource(segmentList.get(0).getOrigin().getAirport().getCityName());
			int j=0;
			if(airFare.getAdultCount() > 0)
			{
				passengerCount = passengerCount+airFare.getAdultCount()+" Adult/s ";
				j++;
			}
			if(airFare.getChildCount() > 0)
			{
				if(j > 0)
				{
					passengerCount = passengerCount+"and ";
				}
				passengerCount = passengerCount+airFare.getChildCount()+" Child/ren ";
				j++;
			}
			if(airFare.getInfantCount() > 0)
			{
				if(j > 0)
				{
					passengerCount = passengerCount+"and ";
				}
				passengerCount = passengerCount+airFare.getInfantCount()+" Infant/s";
			}
			ticketDetails.setPassengerCount(passengerCount);
			ticketDetails.setDestination(segmentList.get(segmentList.size() - 1).getDestination().getAirport().getCityName());
			if(airFare.getTripIndicator() == 2)
			{
				ticketDetails.setWay("Round-Trip");
			}
			else 
			{
				ticketDetails.setWay("One-Way");
			}
			airTravelDao.saveTicketDetails(ticketDetails);
			
			Iterator<Segment> itr = segmentList.iterator();
			int i=0;
			while(itr.hasNext())
			{
				Segment segment=itr.next();             
				TicketSegment ticketSegment = new TicketSegment();
				ticketSegment.setAirFareId(airFare.getAirFareId());
				ticketSegment.setTripId(airFare.getTripId());
				ticketSegment.setAirlineName(segment.getAirline().getAirlineName());
				ticketSegment.setAirlineCode(segment.getAirline().getAirlineCode());
				ticketSegment.setArrdate(f2.format(format.parse(segment.getDestination().getArrTime())));
				ticketSegment.setDeptdate(f2.format(format.parse(segment.getOrigin().getDepTime())));
				ticketSegment.setDestAirportName(segment.getDestination().getAirport().getAirportName());
				ticketSegment.setDestination(segment.getDestination().getAirport().getCityName());
				ticketSegment.setDestAirportCode(segment.getDestination().getAirport().getAirportCode());
				ticketSegment.setDestCityCode(segment.getDestination().getAirport().getCityCode());
				ticketSegment.setRegulations(flightBookingresp.getFareRules().get(i).getFareRuleDetail());
				ticketSegment.setSource(segment.getOrigin().getAirport().getCityName());
				ticketSegment.setSourceAirportName(segment.getOrigin().getAirport().getAirportName());
				ticketSegment.setSourceAirportCode(segment.getOrigin().getAirport().getAirportCode());
				ticketSegment.setSourceCityCode(segment.getOrigin().getAirport().getCityCode());
				ticketSegment.setAirlineNumber(segment.getAirline().getFlightNumber());
				airTravelDao.saveTicketSegments(ticketSegment);
			}
			passangerList =flightBookingresp.getPassenger();
			Iterator<Passenger> itr2 = passangerList.iterator();
			while(itr2.hasNext())
			{
				PassangerDetails passangerDetails = new PassangerDetails();
				Passenger passenger = itr2.next();
				passangerDetails.setAirFareId(airFare.getAirFareId());
				passangerDetails.setTripId(airFare.getTripId());
				passangerDetails.setMealType("Not Confirmed");
				passangerDetails.setLifeInsurance("Not Confirmed");
				passangerDetails.setCheckinBag("15 KG");
				passangerDetails.setCheckinCabin("7 KG");
				passangerDetails.setAirlineCode(flightBookingresp.getSegments().get(0).getAirline().getAirlineName()+"-"+flightBookingresp.getSegments().get(0).getAirline().getFlightNumber());
				passangerDetails.setPublishedFare(passenger.getFare().getPublishedFare());
				passangerDetails.setPassangername(passenger.getTitle()+" "+passenger.getFirstName()+" "+passenger.getLastName());
				passangerDetails.setPnr(flightBookingresp.getPNR());
				passangerDetails.setSourceDestinationcode(flightBookingresp.getOrigin()+"-"+flightBookingresp.getDestination());
				passangerDetails.setIsPnrCancelled("false");
				passangerDetails.setStatus("Confirmed");

				passangerDetails.setPaxType(passenger.getPaxType());
				if(passenger.getTicket() != null && passenger.getTicket().getTicketId() != null)
					passangerDetails.setTicketId(Integer.toString(passenger.getTicket().getTicketId()));
				else
					passangerDetails.setTicketId("null");
				if(passenger.getTicket() != null && passenger.getTicket().getTicketNumber() != null)
					passangerDetails.setTicketNumber(passenger.getTicket().getTicketNumber());
				else
					passangerDetails.setTicketNumber("null");
				
				passangerDetails.setTripIndicator(airFare.getTripIndicator());
				airTravelDao.savePassengerDetails(passangerDetails);
			}
			return "300";
		}
		catch(Exception e)
		{
			logger.info("************problem in *************ticketDetails**********e.message****"+e.getMessage());
			e.printStackTrace();
			return "7000";
		}
	}
	
	public String sendSuccessMailPdf(String customername ,String airFareId ,String tripId,HttpServletRequest request,String termsAndCondition,String AggreatorId)
	{
		logger.info("************************Start execution ************sendMailPdf********");
		String status="1001";
		List<PassangerDetails> passangerList = new ArrayList<PassangerDetails>();
		List<TicketSegment> ticketSegmentList = new ArrayList<TicketSegment>();
		List<TicketDetails> ticketDetailsList = new ArrayList<TicketDetails>();
		double baseFare =0.0;
		double taxes=0.0;
		double convenienceFee=0.0;
		double total=0.0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat f2 = new SimpleDateFormat("E, dd MMM yy HH:mm");
		CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();
		String pdfTemplate="";
		try
		{
			passangerList = airTravelDao.getPassangerDetails(tripId);
			ticketSegmentList = airTravelDao.getTicketSegment(tripId);
			ticketDetailsList = airTravelDao.getTicketDetails(tripId);
			
			
			
			
			pdfTemplate=commanUtilDao.getmailTemplet("flightMailPDFTempHead", AggreatorId);
			pdfTemplate = pdfTemplate.replaceAll("<<CustomerName>>", customername).replaceAll("<<Source>>", ticketDetailsList.get(0).getSource()).replaceAll("<<Destination>>", ticketDetailsList.get(0).getDestination()).replaceAll("<<way>>", ticketDetailsList.get(ticketDetailsList.size() - 1 ).getWay()).replaceAll("<<tripId>>", ticketDetailsList.get(0).getTripId()).replaceAll("<<BookingDate>>", ticketDetailsList.get(0).getBookingDate()).replaceAll("<<filepath>>", filePath);
	    	    
			Iterator<TicketDetails> itr = ticketDetailsList.iterator();
			while(itr.hasNext())
			{
				TicketDetails tickDet = itr.next();
				baseFare = baseFare + tickDet.getBaseFare();
				taxes = taxes+tickDet.getTaxes();
				convenienceFee = convenienceFee+tickDet.getConvenienceFee();
			}
			total = baseFare+taxes+convenienceFee;
	    	Iterator<TicketSegment> itr1 =   ticketSegmentList.iterator(); 
	    	while(itr1.hasNext())
	    	{
	    		TicketSegment ticketSeg = itr1.next();
	    		Date date1 = f2.parse(ticketSeg.getDeptdate());
	    		Date date2= f2.parse(ticketSeg.getArrdate());
	    		long time = (date2.getTime() - date1.getTime())/(1000*60);
	    		String timeS="";
	    		if(time >= 60)
	    		{
	    			timeS=time / 60 +"Hr "+time % 60 +"Min";
	    		}
	    		else
	    		{
	    			timeS = time +"Min";
	    		}
	    		
	    		
	    		
	    		
	    		//String seg=" <tr> <td class=\"content\" > <table > <tr> <td style=\"width:30px; \"> <img src=\""+ filePath+"/<<airlineCode>>.png\" alt=\"Air Asia\"> </td> <td> <table> <tr> <td style=\"font-size:11px;\"> <<Source>> ---- <<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"> <<Airlinename>> </td> </tr> </table> </td> <td> <table> <tr> <td style=\"font-size:11px;\"><<Source>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Deptdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Sourceairportname>></td> </tr> </table> </td> <td style=\"width:20;text-align:center\"> <table> <tr> <td style=\"text-align:center\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"images/clock.png\" style=\"float:right\"></td> </tr> <tr> <td style=\"font-size:9px;color:#757575;text-align:center\"><<TimeDiff>></td> </tr> </table> </td> <td> <table style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray\"> <tr> <td style=\"font-size:11px;\"><<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Arrdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Destairportname>></td> </tr> </table> </td> </tr> </table> </td> </tr>";
	    		String seg=commanUtilDao.getmailTemplet("flightMailPDFTempSeg", AggreatorId);
	    		seg = seg.replaceAll("<<Source>>", ticketSeg.getSource()).replaceAll("<<Destination>>", ticketSeg.getDestination()).replaceAll("<<Airlinename>>", ticketSeg.getAirlineName()+"-"+ticketSeg.getAirlineNumber()).replaceAll("<<Deptdate>>", ticketSeg.getDeptdate()).replaceAll("<<Sourceairportname>>", ticketSeg.getSourceAirportName()).replaceAll("<<Arrdate>>", ticketSeg.getArrdate()).replaceAll("<<Destairportname>>", ticketSeg.getDestAirportName()).replaceAll("<<TimeDiff>>", timeS).replaceAll("<<airlineCode>>", ticketSeg.getAirlineCode()).replaceAll("<<filepath>>", filePath);	    		
	    		pdfTemplate = pdfTemplate + seg;
	    	}
	    	pdfTemplate = pdfTemplate +commanUtilDao.getmailTemplet("flightMailPDFTempSegNext", AggreatorId);
	    	pdfTemplate = pdfTemplate.replaceAll("<<Count>>", ticketDetailsList.get(0).getPassengerCount());
	    	Iterator<PassangerDetails> itr2 =   passangerList.iterator(); 
	    	while(itr2.hasNext())
	    	{
	    		PassangerDetails passDetails = itr2.next();
	    		String passenger=commanUtilDao.getmailTemplet("flightMailPDFTempPassDtl1", AggreatorId);
	    		passenger = passenger.replaceAll("<<Passangername>>", passDetails.getPassangername()).replaceAll("<<Airlinecode>>",passDetails.getAirlineCode()).replaceAll("<<Status>>",passDetails.getStatus()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<Ticketid>>",passDetails.getTicketNumber()).replaceAll("<<Pnr>>", passDetails.getPnr());
	    		pdfTemplate = pdfTemplate +passenger;
	    	}
	    	pdfTemplate = pdfTemplate+commanUtilDao.getmailTemplet("flightMailPDFTempPassDtl1Next", AggreatorId);
	    	Iterator<PassangerDetails> itr3 =   passangerList.iterator(); 
	    	while(itr3.hasNext())
	    	{
	    		PassangerDetails passDetails = itr3.next();
	    		String passenger=commanUtilDao.getmailTemplet("flightMailPDFTempPassDtl2", AggreatorId);
	    		passenger = passenger.replaceAll("<<Passangername>>", passDetails.getPassangername()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<Airlinecode>>",passDetails.getAirlineCode()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<lifeInsurance>>",passDetails.getLifeInsurance()).replaceAll("<<Checkinbag>>", passDetails.getCheckinBag()).replaceAll("<<Checkincabin>>", passDetails.getCheckinCabin()).replaceAll("<<Mealtype>>", passDetails.getMealType());
	    		pdfTemplate = pdfTemplate +passenger;
	    	}
	    	pdfTemplate = pdfTemplate+commanUtilDao.getmailTemplet("flightMailPDFTempPassDtl2Next", AggreatorId);
	    	
	    	{
	    		String seg=commanUtilDao.getmailTemplet("flightMailPDFTempFee", AggreatorId);
	    		seg= seg.replaceAll("<<Basefare>>", Double.toString(baseFare)).replaceAll("<<taxes>>", Double.toString(taxes)).replaceAll("<<convenienceFee>>", Double.toString(convenienceFee)).replaceAll("<<totalfare>>", Double.toString(total));
	    		pdfTemplate = pdfTemplate +seg;
	    	}
	    	
	    	termsAndCondition = "<ul><li>" + termsAndCondition ;
	    	termsAndCondition = termsAndCondition.replaceAll("[0-9]{1,3}[.]{1}[ ]{1}", "</li><li>");//.replaceAll("[\\]{1}[u]{1}[0]{3}[a-zA-z]{1}","").;
	    	termsAndCondition = termsAndCondition + "</li></ul>";
	    	
	    	pdfTemplate = pdfTemplate + commanUtilDao.getmailTemplet("flightMailPDFTempT&C", AggreatorId);
	    	List<ConvenienceFeeBean> convFeeList = new ArrayList<ConvenienceFeeBean>();
			 convFeeList = airTravelDao.getConvenienceFeeStruct(1,"Cancellation");
	    	
	    	pdfTemplate = pdfTemplate.replaceAll("<<Terms and Conditions>>", termsAndCondition).replaceAll("<<cancellationfee>>",Double.toString(convFeeList.get(0).getAdult()));
	    	String pdfName="Bhartipay_Ticket_"+tripId;
	    	
	    	String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "ROOT");//for production and test
	    	//String ticketPath= servletContext.getRealPath("/flightPDF").replaceAll("BhartiPayService", "BhartiPay");//for local
	    	new CommanUtil().htmlToPdfGeneration(ticketPath+ pdfName+".pdf", pdfTemplate);
		}
		catch(Exception e)
		{
			logger.info("***************problem in sendMailPdf**********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return pdfTemplate;
	}
	
	
	public String sendCancelMail(String customername ,String airFareId ,String tripId, List<Integer> ticketId, AirFareBean airfare,String AggreatorId)
	{
		logger.info("************************Start execution ************sendCancelMail********");
		String status="1001";
		List<TicketSegment> ticketSegmentList = new ArrayList<TicketSegment>();
		List<CancelledDetails> canceldetailList = new ArrayList<CancelledDetails>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat f2 = new SimpleDateFormat("E, dd MMM yy HH:mm");
		String pdfTemplate="";
		try
		{
			ticketSegmentList = airTravelDao.getTicketSegmentByAirFareId(airFareId);
			canceldetailList = airTravelDao.getCancelDetails(airFareId, ticketId);
			
			pdfTemplate=commanUtilDao.getmailTemplet("flightCancelMailTempHead", AggreatorId);
			pdfTemplate = pdfTemplate.replaceAll("<<CustomerName>>", customername).replaceAll("<<BookingId>>", tripId).replaceAll("<<filePath>>", filePath);
	    	    
	    	Iterator<TicketSegment> itr1 =   ticketSegmentList.iterator(); 
	    	while(itr1.hasNext())
	    	{
	    		TicketSegment ticketSeg = itr1.next();
	    		Date date1 = f2.parse(ticketSeg.getDeptdate());
	    		Date date2= f2.parse(ticketSeg.getArrdate());
	    		long time = (date2.getTime() - date1.getTime())/(1000*60);
	    		String timeS="";
	    		if(time >= 60)
	    		{
	    			timeS=time / 60 +"Hr "+time % 60 +"Min";
	    		}
	    		else
	    		{
	    			timeS = time +"Min";
	    		}
	    		
	    		//String seg="<tr> <td class='content'> <table> <tbody><tr> <td> <img src='images/airlinelogo.png' width='33px' alt='Air Asia'> </td> <td> <table> <tbody><tr> <td style='font-size:13px;'> New Delhi ---- Banglore </td> </tr> <tr> <td style='font-size:11px;color:#757575'> AirAsia India 15/214 </td> </tr> </tbody></table> </td> <td> <table> <tbody><tr> <td style='font-size:13px;'>DEL 20:40</td> </tr> <tr> <td style='font-size:11px;color:#757575'>Wed 05 Jul</td> </tr> <tr> <td style='font-size:11px;color:#757575'>Indira Gandhi Int Airport</td> </tr> </tbody></table> </td> <td style='width:70px;text-align:center'> <table> <tbody><tr> <td><img src='images/clock.png' alt='bhartipay'></td> </tr> <tr> <td style='font-size:11px;color:#757575'>2hr 5m</td> </tr> </tbody></table> </td> <td> <table> <tbody><tr> <td style='font-size:13px;'>DEL 20:40</td> </tr> <tr> <td style='font-size:11px;color:#757575'>Wed 05 Jul</td> </tr> <tr> <td style='font-size:11px;color:#757575'>Indira Gandhi Int Airport</td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr>'";
	    		String seg=commanUtilDao.getmailTemplet("flightCancelMailTempSeg", AggreatorId);
	    		seg = seg.replaceAll("<<Source>>", ticketSeg.getSource()).replaceAll("<<Destination>>", ticketSeg.getDestination()).replaceAll("<<Airlinename>>", ticketSeg.getAirlineName()).replaceAll("<<Deptdate>>", ticketSeg.getDeptdate()).replaceAll("<<Sourceairportname>>", ticketSeg.getSourceAirportName()).replaceAll("<<Arrdate>>", ticketSeg.getArrdate()).replaceAll("<<Destairportname>>", ticketSeg.getDestAirportName()).replaceAll("<<TravelDuration>>", timeS).replaceAll("<<AirlineCode>>", ticketSeg.getAirlineCode()).replaceAll("<<filePath>>", filePath);	    		
	    		pdfTemplate = pdfTemplate + seg;
	    	}
	    	
	    	pdfTemplate = pdfTemplate + commanUtilDao.getmailTemplet("flightCancelMailTempSegNext", AggreatorId);
	    	
	    	
	    	Iterator<CancelledDetails> itr2 = canceldetailList.iterator();
	    	double refundAmt=0.0;
	    	while(itr2.hasNext())
	    	{
	    		double airlineCancelCharge=0.0;
	    		double cancellationTax=0.0;
	    		CancelledDetails cancelDetails = itr2.next();
	    		PassangerDetails passDetails = new PassangerDetails();
	    		passDetails = airTravelDao.getPassangerDetailsByTicketId(cancelDetails.getTicketId(),tripId,"");
	    		airlineCancelCharge = cancelDetails.getCancellationCharge();
	    		cancellationTax = passDetails.getPublishedFare() - cancelDetails.getCancellationCharge();
	    		refundAmt += cancelDetails.getRefundedAmount();
	    		String cancel=commanUtilDao.getmailTemplet("flightCancelMailTempPass", AggreatorId);
	    		cancel = cancel.replaceAll("<<PassengerName>>", cancelDetails.getPassengerName()).replaceAll("<<CancellationId>>", cancelDetails.getCancelId()).replaceAll("<<Pnr>>", passDetails.getPnr()).replaceAll("<<TicketNumber>>", passDetails.getTicketNumber()).replaceAll("<<CancellationMode>>", airfare.getCancellationStatus()).replaceAll("<<AirlineCancellationFee>>", Double.toString(airlineCancelCharge)).replaceAll("<<CancellationTax>>",Double.toString(cancellationTax));
	    		pdfTemplate = pdfTemplate + cancel;
	    	}
	    	
	    	
	    	pdfTemplate = pdfTemplate+commanUtilDao.getmailTemplet("flightCancelMailTempPassNext", AggreatorId);
	    	pdfTemplate = pdfTemplate.replaceAll("<<TotalRefund>>", Double.toString(refundAmt));
	    		}
		catch(Exception e)
		{
			logger.info("***************problem in sendCancelMail**********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return pdfTemplate;
	}
	
	
	/*public String sendMailPdf(String airFareId ,String tripId,HttpServletRequest request)
	{
		logger.info("************************Start execution ************sendMailPdf********");
		String status="1001";
		List<PassangerDetails> passangerList = new ArrayList<PassangerDetails>();
		List<TicketSegment> ticketSegmentList = new ArrayList<TicketSegment>();
		List<TicketDetails> ticketDetailsList = new ArrayList<TicketDetails>();
		double baseFare =0.0;
		double taxes=0.0;
		double convenienceFee=0.0;
		double total=0.0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat f2 = new SimpleDateFormat("E, dd MMM yy HH:mm");
		String pdfTemplate="";
		try
		{
			passangerList = airTravelDao.getPassangerDetails(tripId);
			ticketSegmentList = airTravelDao.getTicketSegment(tripId);
			ticketDetailsList = airTravelDao.getTicketDetails(tripId);
			
			String filePath = "https://b2b.bhartipay.com/BhartiPayService/image/flights";//"E:\\EclipseWS\\BhartiPayService\\WebContent\\image\\flights";//
			
			
			pdfTemplate="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /> <meta name=\"viewport\" content=\"width=device-width\"/> <!-- For development, pass document through inliner --> <link rel=\"stylesheet\" href=\"css/simple.css\"></head><body><table class=\"body-wrap\" > <tr> <td class=\"container\"> <!-- Message start --> <table> <tr> <td align=\"left\" class=\"content\"> <table> <tr> <td style=\" width: 156px;\"> <img src=\""+filePath+"/logo116x50.png\" style=\" border: none; outline: none; width: 112px;\" alt=\"bhartipay\"> </td> <td style=\"width:189px;text-align:right\"> <table> <tr> <td style=\"text-align:right\"> <table> <tr>  <td colspan=\"2\" style=\"width: 201px;\"> <div style=\"color: #ff6e26;text-align:right; font-size: 17px;margin-bottom:0px\"> Booking Confirmation</div></td> </tr> </table> </td> </tr> <tr> <td > <div style=\"color:#a9a8a8;font-size:8px\">Booking Date: <<BookingDate>></div> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td class=\"content\" style=\"font-size:12px;line-height:18px\"> <div> Hi, </div> <div> Thank you for booking with bhartipay </div> <div> Your <<Source>> - <<Destination>> <<way>> flight is confirmed. we will email your ticket on registered email ID. </div> <div> your booking ID is <span style=\"color:#ff6e26\"><<tripId>>.</span> please use this boooking ID for any communication with us. </div> </td> </tr>";
			pdfTemplate = pdfTemplate.replaceAll("<<Source>>", ticketDetailsList.get(0).getSource()).replaceAll("<<Destination>>", ticketDetailsList.get(0).getDestination()).replaceAll("<<way>>", ticketDetailsList.get(ticketDetailsList.size() - 1 ).getWay()).replaceAll("<<tripId>>", ticketDetailsList.get(0).getTripId()).replaceAll("<<BookingDate>>", ticketDetailsList.get(0).getBookingDate());
	    	    
			Iterator<TicketDetails> itr = ticketDetailsList.iterator();
			while(itr.hasNext())
			{
				TicketDetails tickDet = itr.next();
				baseFare = baseFare + tickDet.getBaseFare();
				taxes = taxes+tickDet.getTaxes();
				convenienceFee = convenienceFee+tickDet.getConvenienceFee();
			}
			total = baseFare+taxes+convenienceFee;
	    	Iterator<TicketSegment> itr1 =   ticketSegmentList.iterator(); 
	    	while(itr1.hasNext())
	    	{
	    		TicketSegment ticketSeg = itr1.next();
	    		Date date1 = f2.parse(ticketSeg.getDeptdate());
	    		Date date2= f2.parse(ticketSeg.getArrdate());
	    		long time = (date2.getTime() - date1.getTime())/(1000*60);
	    		String timeS="";
	    		if(time >= 60)
	    		{
	    			timeS=time / 60 +"Hr "+time % 60 +"Min";
	    		}
	    		else
	    		{
	    			timeS = time +"Min";
	    		}
	    		
	    		
	    		
	    		
	    		//String seg=" <tr> <td class=\"content\" > <table > <tr> <td style=\"width:30px; \"> <img src=\""+ filePath+"/<<airlineCode>>.png\" alt=\"Air Asia\"> </td> <td> <table> <tr> <td style=\"font-size:11px;\"> <<Source>> ---- <<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"> <<Airlinename>> </td> </tr> </table> </td> <td> <table> <tr> <td style=\"font-size:11px;\"><<Source>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Deptdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Sourceairportname>></td> </tr> </table> </td> <td style=\"width:20;text-align:center\"> <table> <tr> <td style=\"text-align:center\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"images/clock.png\" style=\"float:right\"></td> </tr> <tr> <td style=\"font-size:9px;color:#757575;text-align:center\"><<TimeDiff>></td> </tr> </table> </td> <td> <table style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray\"> <tr> <td style=\"font-size:11px;\"><<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Arrdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Destairportname>></td> </tr> </table> </td> </tr> </table> </td> </tr>";
	    		String seg=" <tr> <td class=\"content\" > <table > <tr> <td style=\"width:30px; \"> <img src=\""+ filePath+"/<<airlineCode>>.png\" alt=\"Air Asia\"> </td> <td> <table> <tr> <td style=\"font-size:11px;\"> <<Source>> ---- <<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"> <<Airlinename>> </td> </tr> </table> </td> <td> <table> <tr> <td style=\"font-size:11px;\"><<Source>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Deptdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Sourceairportname>></td> </tr> </table> </td> <td style=\"width:20;text-align:center\"> <table> <tr> <td style=\"text-align:center\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\""+ filePath+"/clock.png\" style=\"float:right\"></td> </tr> <tr> <td style=\"font-size:9px;color:#757575;text-align:center\"><<TimeDiff>></td> </tr> </table> </td> <td> <table style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray\"> <tr> <td style=\"font-size:11px;\"><<Destination>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Arrdate>></td> </tr> <tr> <td style=\"font-size:9px;color:#757575\"><<Destairportname>></td> </tr> </table> </td> </tr> </table> </td> </tr>";
	    		seg = seg.replaceAll("<<Source>>", ticketSeg.getSource()).replaceAll("<<Destination>>", ticketSeg.getDestination()).replaceAll("<<Airlinename>>", ticketSeg.getAirlineName()).replaceAll("<<Deptdate>>", ticketSeg.getDeptdate()).replaceAll("<<Sourceairportname>>", ticketSeg.getSourceAirportName()).replaceAll("<<Arrdate>>", ticketSeg.getArrdate()).replaceAll("<<Destairportname>>", ticketSeg.getDestAirportName()).replaceAll("<<TimeDiff>>", timeS).replaceAll("<<airlineCode>>", ticketSeg.getAirlineCode());	    		
	    		pdfTemplate = pdfTemplate + seg;
	    	}
	    	pdfTemplate = pdfTemplate +"<tr> <td class=\"content\" style=\"font-size: 13px; padding: 0px 20px;\"><strong style=\"color:#ff6e26\">Passangers</strong> - <<Count>></td> </tr> <tr> <td class=\"content\" style=\"font-size:9px; padding: 5px 20px;\"> <table style=\"font-size:14px;border-color:#ccc\" cellpadding=\"5\" border=\"1\" valign=\"middle\"> <tr> <td   bg=\"#f3f3f3\" style=\"vertical-align: middle; background-color: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; borde: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Passanger Name</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Airlines</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Status</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Sector</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Ticket Number</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Airline PNR</td> </tr>";
	    	pdfTemplate = pdfTemplate.replaceAll("<<Count>>", ticketDetailsList.get(0).getPassengerCount());
	    	Iterator<PassangerDetails> itr2 =   passangerList.iterator(); 
	    	while(itr2.hasNext())
	    	{
	    		PassangerDetails passDetails = itr2.next();
	    		String passenger="<tr> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Passangername>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Airlinecode>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Status>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<SourceDest>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Ticketid>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"> <<Pnr>></td> </tr>";
	    		passenger = passenger.replaceAll("<<Passangername>>", passDetails.getPassangername()).replaceAll("<<Airlinecode>>", passDetails.getAirlineCode()).replaceAll("<<Status>>",passDetails.getStatus()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<Ticketid>>",passDetails.getTicketNumber()).replaceAll("<<Pnr>>", passDetails.getPnr());
	    		pdfTemplate = pdfTemplate +passenger;
	    	}
	    	pdfTemplate = pdfTemplate+" </table></td> </tr> <tr> <td class=\"content\" style=\"font-size: 13px; padding: 0px 20px;\"><strong style=\"color:#ff6e26\">Flight Inclusion</strong></td> </tr> <tr> <td class=\"content\" style=\"font-size: 9px; padding: 5px 20px; \"> <table cellpadding=\"5\" border=\"1\" style=\"font-size:9px\"> <tr> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Passanger Name</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Sector</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Airline</td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Flight Insurance Status</td> <td style=\" width: 105px; background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\"> <table border=\"0\" cellspacing=\"0\"> <tr> <td colspan=\"2\">Baggage</td> </tr> <tr> <td style=\"text-align: left;font-size:7px\">Check in</td> <td style=\"text-align: right;font-size:7px\">Cabin</td> </tr> </table> </td> <td style=\" background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;\">Meal Type</td> </tr>";
	    	Iterator<PassangerDetails> itr3 =   passangerList.iterator(); 
	    	while(itr3.hasNext())
	    	{
	    		PassangerDetails passDetails = itr3.next();
	    		String passenger=" <tr> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Passangername>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<SourceDest>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Airlinecode>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<lifeInsurance>></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"> <table border=\"0\" cellspacing=\"0\"> <tr> <td style=\"text-align:left;font-size:7px\"><<Checkinbag>></td> <td style=\"text-align:right;font-size:7px\"><<Checkincabin>></td> </tr> </table></td> <td style=\" font-size: 9px; text-align: center; padding: 5px;\"><<Mealtype>></td> </tr>";
	    		passenger = passenger.replaceAll("<<Passangername>>", passDetails.getPassangername()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<Airlinecode>>", passDetails.getAirlineCode()).replaceAll("<<SourceDest>>", passDetails.getSourceDestinationcode()).replaceAll("<<lifeInsurance>>",passDetails.getLifeInsurance()).replaceAll("<<Checkinbag>>", passDetails.getCheckinBag()).replaceAll("<<Checkincabin>>", passDetails.getCheckinCabin()).replaceAll("<<Mealtype>>", passDetails.getMealType());
	    		pdfTemplate = pdfTemplate +passenger;
	    	}
	    	pdfTemplate = pdfTemplate+ "</table></td> </tr> <tr> <td class=\"content\" > <table  style=\"border:1px solid #999\"> <tr> <td style=\"font-size: 13px;font-weight:bold; padding: 0px 20px;border:1px solid #999;color:#ff6e26\">Fare Details</td> </tr> </table> </td></tr>";
	    	
	    	{
	    		String seg="<tr> <td class=\"content\" style=\"padding: 0px 20px;\"> <table style=\" border: 1px solid #cacaca;\"> <tr> <td style=\"font-size:12px; padding: 5px 10px;text-align:left\">Base Fare:</td><td style=\"text-align:right;font-size:12px; padding:5px 10px;\">INR <<Basefare>></td> </tr> <tr> <td style=\"font-size:12px; padding: 5px 10px;text-align:left\">Taxes & Fee:</td><td style=\"text-align:right;font-size:12px; padding:5px 10px;\">INR <<taxes>></td> </tr> <tr> <td style=\"font-size:12px; padding: 5px 10px;text-align:left\">Convenience Fee:</td> <td style=\"text-align:right;font-size:12px; padding:5px 10px;\">INR <<convenienceFee>></td> </tr> <tr> <td style=\"font-size:12px; padding: 5px 10px;text-align:left;border-top: 1px solid #cacaca;\"><strong>Total:</strong></td> <td style=\"font-size:12px; padding: 5px 10px;text-align:right;border-top: 1px solid #cacaca;\"><strong>INR <<totalfare>></strong></td> </tr> </table></td> </tr>";
	    		seg= seg.replaceAll("<<Basefare>>", Double.toString(baseFare)).replaceAll("<<taxes>>", Double.toString(taxes)).replaceAll("<<convenienceFee>>", Double.toString(convenienceFee)).replaceAll("<<totalfare>>", Double.toString(total));
	    		pdfTemplate = pdfTemplate +seg;
	    	}
	    	pdfTemplate = pdfTemplate + "<tr> <td class=\"content\"> <table> <tr> <td style=\"font-size:12px;\"> <strong>Cancellation Charges </strong> </td> </tr> <tr> <td style=\"font-size:12px;\"> <strong>Bhartipay Fee:</strong> <span style=\"color:#888\"></span> 250 per pax per sector </td> </tr> </table> </td> </tr> <tr> <td class=\"content\" style=\"font-size:14px;padding:5px 20px;\"> <strong>Terms & Conditions</strong> </td> </tr> <tr> <td class=\"content\" style=\"font-size:8px;color:#888;padding:5px 50px;\"> <ul> <li>All passengers, including children and infants, have to present valid ID proof at check-in.</li> <li>Please contact airlines for Terminal Queries</li> <li>Due to the security reasons and Government regulations, passengers flying on destination like Jammu and Srinagar are not allowed to carry any Hand Baggage.</li> <li>If the basic fare is lesser than cancellation charges then only statutory taxes would be refunded.</li> <li>Kindly contact the airline at least 24 hrs before to reconfirm your flight details giving reference of Airline PNR Number.</li> <li>Bhartipay is a travel agent and all reservations done through our website are as per the terms and conditions of the concerned airlines. Any modification, cancellation and refund of the airline tickets shall be strictly as per the policy of the concerned airlines and we deny all liability in connection thereof.</li> </ul> </td> </tr> <tr> <td class=\"content\" style=\"font-size:12px;padding:5px 20px 50px\"> <div>Contact Information:</div> <div>Airline Contact Information : Air Asia - 1800222111,9223222111</div> <div>Bhartipay Contact Information : 011-43131313, 011-43030303 </div> <div> Please reference the Airline PNR Number when communicating with the airline regarding this booking. </div> </td> </tr> </table> </td> </tr> <tr> </table></body></html>";
	    	
	    	String pdfName="Bhartipay_Ticket_"+tripId;
	    	
	    	new CommanUtil().htmlToPdfGeneration("/home/flightticket/"+ pdfName+".pdf", pdfTemplate);
		}
		catch(Exception e)
		{
			logger.info("***************problem in sendMailPdf**********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return pdfTemplate;
	}
	
	*/
	//getTicketDetails
	@Path("/getTicketDetails")
	@POST
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_JSON)
	public  ResponseBean getTicketDetails(RequestBean requestBean , @Context HttpServletRequest request)
	{
		 logger.info("***********Start Execution *******getTicketDetails*******");
		 ResponseBean responseBean = new ResponseBean();
		 Response respBean = new Response();
		 List<PassangerDetails> passengerList = new ArrayList<PassangerDetails>();
		 AirFareBean airfare = new AirFareBean();
		 CancellationCharges cancel = new CancellationCharges();
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"cancelTicket");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******getTicketDetails************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******getTicketDetails**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****getTicketDetails***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****getTicketDetails******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 JSONObject claimJson = new JSONObject(claim); 
			 
			 airfare = airTravelDao.getFareQuoteByPNR(claimJson.getString("travelId"), claimJson.getString("pnr"));
			 
			 passengerList = airTravelDao.getPassangerDetailsByAirFareId(airfare.getAirFareId());
			 
			 cancel.setConvenienceFee(airfare.getConvenienceFee());
			 if(airfare.getIsRefundable() == 1)
			 {
				 cancel.setIsRefundable("true");
				 cancel.setAirlinePenalty(airfare.getCancellationCharge());
			 }
			 else
			 {
				 cancel.setIsRefundable("false");
				 cancel.setAirlinePenalty(airfare.getTotalFare()-airfare.getConvenienceFee()-airfare.getCommissionEarned());
			 }
			 cancel.setTotalFare(airfare.getTotalFare());
			 cancel.setTbuServiceFee(100);
			 cancel.setRefundableAmount(airfare.getTotalFare() - (airfare.getConvenienceFee() + cancel.getTbuServiceFee() + airfare.getCancellationCharge()));
			 respBean.setCancel(cancel);
			 respBean.setPassengerList(passengerList);
			 if(passengerList.size() < 1)
			 {
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg("No Data Found.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,Response.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean; 
			 }
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 respBean.setStatusMsg("SUCCESS");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,Response.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********getTicketDetails********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,Response.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			 logger.info(serverName+"************problem in **********getTicketDetails********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}		
	}
	
	
	@Path("/CancelTicket")
	@POST
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseBean cancelTicket(RequestBean requestBean , @Context HttpServletRequest request,@HeaderParam("IPIMEI") String imei)
	{
		 logger.info("***********Start Execution *******cancelTicket*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 ResponseBean responseBean = new ResponseBean();
		 BookingResponseBean respBean = new BookingResponseBean();
		 Response resp = new Response();
		 FlightBookingResp cancelResp = new FlightBookingResp();
		 FareQuote cancelTicketResp = new FareQuote();
		 List<FareQuote> getChangeReqList = new ArrayList<FareQuote>();
		
		 PassangerDetails passengerdetails = new PassangerDetails();
		 String apiResp ="";
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 AirFareBean airfare  = new AirFareBean();
		 int status=0;
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"cancelTicket");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******cancelTicket************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******cancelTicket**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****cancelTicket***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****cancelTicket******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		} 
		 try
		 {
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 String strMsg=null;
			 CancelRequest cancelRequest = new CancelRequest();
			 BookingResponseBean refundResp = new BookingResponseBean();
			 JSONObject claimJson = new JSONObject(claim); 
			 cancelRequest = gson.fromJson(claimJson.toString() ,CancelRequest.class);
			 respBean.setTravelId(cancelRequest.getTravelId());
			 reqbean = airTravelDao.getAuthDetails("Air");
			 cancelRequest.setEndUserIp(reqbean.getEndUserIp());
			 cancelRequest.setTokenId(reqbean.getTokenId());
			 /*List<ConvenienceFeeBean> convFeeList = new ArrayList<ConvenienceFeeBean>();
			 convFeeList = airTravelDao.getConvenienceFeeStruct(1,"Cancellation");*/
			 
			 airfare = airTravelDao.getFareQuoteByPNR(claimJson.getString("travelId"),claimJson.getString("pnr"));
			 List<Sector> sectorList = new ArrayList<Sector>();
			 Sector sector = new Sector();
			 StringTokenizer strOrigin = new StringTokenizer(airfare.getFlightSource(), ",");
			 sector.setOrigin(strOrigin.nextToken());
			 StringTokenizer strDest = new StringTokenizer(airfare.getFlightDestination(), ",");
			 sector.setDestination(strDest.nextToken());
			 sectorList.add(sector);
			 cancelRequest.setSectors(sectorList);
			 cancelRequest.setBookingId(airfare.getBookingId());
			 Iterator<Integer> itr = cancelRequest.getTicketId().iterator();
			 List<PassangerDetails> passList = new ArrayList<PassangerDetails>();
			 FlightBookingResp flightBookingrespTicket = new FlightBookingResp();
			 resp.setPnr(airfare.getPnrNo());
			 resp.setBookingId(airfare.getBookingId());
			 int passangerCount=0;
			 double totalservicetxn=0.0;
			 double totalcommision = 0.0;
			 double totalrefundAmount=0.0;
			 double cancellationCharge=0.0;
			 double conveniencefee=0.0;
			 double swach =0.0;
			 int airFarePassCount=airfare.getAdultCount()+airfare.getChildCount()+airfare.getInfantCount();
			 while(itr.hasNext())
			 {
				 PassangerDetails pass = new PassangerDetails();
				 pass =airTravelDao.getPassangerDetailsByTicketId(itr.next().toString(),claimJson.getString("travelId"),claimJson.getString("pnr"));
				 if(pass == null)
				 {
					 respBean.setStatus("Failed");
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Any of the ticket is already cancelled.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
					 return responseBean;
				 }
				 else
				 {
					 passangerCount++;
				 }
				 if( pass.getIsPnrCancelled() == null  || !pass.getIsPnrCancelled().equalsIgnoreCase("true"))
				 {
					// airfare = airTravelDao.getFareQuote(pass.getAirFareId());
				 }
				 else
				 {
					 respBean.setStatus("Failed");
					 respBean.setStatusCode("1");
					 respBean.setStatusMsg("Any of the ticket is already cancelled.");
					 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
					 return responseBean;
				 
				 }
				
				
			 }
			 
			 
			 
			 List<Integer> ticketId = new ArrayList<Integer>();
			 Iterator<Integer> it =cancelRequest.getTicketId().iterator();
			 while(it.hasNext())
			 {
				 ticketId.add(it.next());
			 }
			 if(passangerCount == airFarePassCount)
			 {
				 cancelRequest.setTicketId(null);
				 cancelRequest.setRequestType(1);
				 cancelRequest.setSectors(null);
			 }
			 else
			 {
				 cancelRequest.setRequestType(2);
			 }
			/* if(airfare.getCancelledPassCount() == airFarePassCount)
			 {
				 airfare.setCancellationStatus("Cancelled");
			 }
			 else
			 {
				 airfare.setCancellationStatus("PartiallyCancelled");
			 }*/
			 //List<AirFareBean> airFareBeanList = airTravelDao.getFareQuoteByTripId(cancelRequest.getTravelId(),"");
			 apiResp=airApiManager.cancelTicket(cancelRequest);
			 cancelTicketResp=gson.fromJson(apiResp, FareQuote.class);
			//{"Response":{"B2B2BStatus":false,"TicketCRInfo":[],"ResponseStatus":1,"TraceId":"706d8ba3-7fd4-4e06-9ab6-ad6d764fdc85"}}
			 if(cancelTicketResp.getResponse().getResponseStatus() != 1 && (cancelTicketResp.getResponse().getError() != null && cancelTicketResp.getResponse().getError().getErrorCode() != 0))
			 {
				 respBean.setStatus("Failed");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg(cancelTicketResp.getResponse().getError().getErrorMessage());
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }
			 respBean.setCancelRefundAmount(cancelTicketResp.getResponse().getRefundedAmount());
			 Iterator<Integer> itr2 = ticketId.iterator();
			 Iterator<TicketCRInfo> itr3 = cancelTicketResp.getResponse().getTicketCRInfo().iterator();
			 int noOfCancelledTickets=0;
			 StringBuffer cancelledticketId = new StringBuffer("Ticket Id :- ");
			 int canPassCnt=0;
			 while(itr2.hasNext() && itr3.hasNext())
			 {
				 String getChangeReqResp="";
				 FareQuote getChangeReqRespObj = new FareQuote();
				 Date createdDate = new Date();
				 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				 SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 PassangerDetails pass = new PassangerDetails();
				 TicketCRInfo ticketcr = new TicketCRInfo();
				 ticketcr = itr3.next();
				 CancelledDetails  cancelDetails = null; 
				 CancelRequest cancelRequestReq = new CancelRequest();
				 
				 cancelRequestReq.setEndUserIp(reqbean.getEndUserIp());
				 cancelRequestReq.setTokenId(reqbean.getTokenId());		
				 cancelRequestReq.setChangeRequestId(ticketcr.getChangeRequestId());
				
				 
				 pass =airTravelDao.getPassangerDetailsByTicketId(itr2.next().toString(),claimJson.getString("travelId"),claimJson.getString("pnr"));
				 getChangeReqResp = airApiManager.getCancelTicket(cancelRequestReq);
				 											
				 getChangeReqRespObj = gson.fromJson(getChangeReqResp, FareQuote.class);
				 status=getChangeReqRespObj.getResponse().getChangeRequestStatus();
				 totalrefundAmount += getChangeReqRespObj.getResponse().getRefundedAmount();
				 cancelDetails = airTravelDao.getCancelDetails(pass.getAirFareId(), pass.getTicketId());
				 if(cancelDetails == null)
				 {
					 cancelDetails = new CancelledDetails();
					 canPassCnt++;
				 }
				 cancelDetails.setCancellationType(cancelRequest.getCancellationType());
				 getChangeReqList.add(getChangeReqRespObj);
				 cancelDetails.setResponseString(getChangeReqResp);
				/* if(pass.getPaxType() == 1 || pass.getPaxType() == 2)
				 {
					 cancelDetails.setConveniencefee(convFeeList.get(0).getAdult());
				 }
				 else
				 {
					 cancelDetails.setConveniencefee(convFeeList.get(0).getInfant());
				 }*/
				 cancelDetails.setCancellationCharge(getChangeReqRespObj.getResponse().getCancellationCharge());
				 cancellationCharge += getChangeReqRespObj.getResponse().getCancellationCharge();
				 cancelDetails.setCancelledStatus(getChangeReqRespObj.getResponse().getChangeRequestStatus());
				 if(getChangeReqRespObj.getResponse().getCreditNoteCreatedOn() != null)
					 cancelDetails.setCreditCreatedOn(format2.parse(format2.format(format.parse(getChangeReqRespObj.getResponse().getCreditNoteCreatedOn()))));
				 if(getChangeReqRespObj.getResponse().getCreditNoteNo() != null)
					 cancelDetails.setCreditNoteNo(getChangeReqRespObj.getResponse().getCreditNoteNo());
				 cancelDetails.setRefundedAmount(getChangeReqRespObj.getResponse().getRefundedAmount());
				 cancelDetails.setServiceTaxOnRAF(getChangeReqRespObj.getResponse().getServiceTaxOnRAF());
				 totalservicetxn += getChangeReqRespObj.getResponse().getServiceTaxOnRAF();
				// conveniencefee = conveniencefee+cancelDetails.getConveniencefee();
				 cancelDetails.setAirFareId(pass.getAirFareId());
				 cancelDetails.setChangeRequestId(Integer.toString(ticketcr.getChangeRequestId()));
				 int crStatus=getChangeReqRespObj.getResponse().getChangeRequestStatus();
				 if(ticketcr.getStatus() == 1 && crStatus == 4)
				 {
					 noOfCancelledTickets++;
					 cancelledticketId.append(pass.getTicketId() + " ");
					 pass.setIsPnrCancelled("true");

					 if((airfare.getCancelledPassCount()+canPassCnt) == airFarePassCount)
					 {
						 airfare.setCancellationStatus("Cancelled");
					 }
					 else
					 {
						 airfare.setCancellationStatus("PartiallyCancelled");
					 }
				 }
				 else if(crStatus == 0 ||  crStatus == 1 ||crStatus == 2 ||crStatus == 3 || crStatus == 6 || crStatus == 7 || crStatus == 8)
				 {
					 switch(crStatus)
					 {
					 case 0:
						 pass.setIsPnrCancelled("Cancellation is in process");
						 break;
					 case 1:
						 pass.setIsPnrCancelled("Cancellation is in process");
						 break;
					 case 2:
						 pass.setIsPnrCancelled("Cancellation Assigned");
						 break;
					 case 3:
						 pass.setIsPnrCancelled("Cancellation Acknowledged");
						 break;
					 case 6:
						 pass.setIsPnrCancelled("Cancellation Closed");
						 break;
					 case 7:
						 pass.setIsPnrCancelled("Cancellation is in process");
						 break;
					 case 8:
						 pass.setIsPnrCancelled("Cancellation is in process");
						 break;
					 }
					// pass.setIsPnrCancelled("pending");
					 strMsg="Ticket Cancellation is in process. Please contact Customer Care.";
				 }
				 else
				 {
					 pass.setIsPnrCancelled("false");
					 strMsg="Ticket Cancellation is in process. Please contact Customer Care.";
				 }
				 airTravelDao.savePassengerDetails(pass);
				 
				 cancelDetails.setPassengerName(pass.getPassangername());
				 cancelDetails.setTicketId(pass.getTicketId());
				 airTravelDao.saveCancelDetails(cancelDetails);
				 pass.setCancellationCharge(cancelDetails.getCancellationCharge());
				 pass.setCancellationFees(pass.getPublishedFare() - cancelDetails.getCancellationCharge());
				 pass.setRefundedAmount(cancelDetails.getRefundedAmount()-cancelDetails.getConveniencefee());
				 pass.setCancelledMode(airfare.getCancellationStatus());
				 passList.add(pass);
				 
		 }

			 
			 airfare.setCancelledPassCount(airfare.getCancelledPassCount()+canPassCnt);
			 if(totalrefundAmount != 0)
				 respBean.setCancelRefundAmount(totalrefundAmount);	 
			 
			 List<PassangerDetails> passListSend = airTravelDao.getPassangerDetailsByPNR(claimJson.getString("travelId"),claimJson.getString("pnr"));
			 List<PassangerDetails> passListResp = cancelPassResp(passListSend);
			 resp.setPassengerList(passListResp);
			 airfare.setCancelconvfee(airfare.getCancelconvfee()+conveniencefee);
			 airfare.setCancellationCharge(airfare.getCancellationCharge()+cancellationCharge+conveniencefee);
			 airfare.setServiceTaxOnRAF(airfare.getServiceTaxOnRAF()+totalservicetxn);
			 airfare.setCancellationRefundAmount(airfare.getCancellationRefundAmount() + totalrefundAmount-conveniencefee);

			 List<TicketSegment> ticketSegmentList = airTravelDao.getTicketSegmentByAirFareId(airfare.getAirFareId());
			 List<TicketSegment> segResponse = cancelSegmentResp(ticketSegmentList);
			 
			 
			 
			 resp.setSegmentList(segResponse);
			 
			 respBean.setResponse(resp);
			 if(passangerCount == noOfCancelledTickets)
			 {
				 strMsg="Ticket Cancelled Successfully.";
				 
			 }
			 else if(noOfCancelledTickets == 0)
			 {
				 strMsg="Ticket Cancellation is under process. Please contact customer Care.";
				 airfare.setCancellationStatus("Cancellation pending");
			 }
			 else
			 {
				 strMsg=cancelledticketId + "has/have been cancelled. Rest are under process. Please contact customer care for their cancellation status.";
				 airfare.setCancellationStatus("Cancellation pending");
			 }
			 airTravelDao.saveFareQuote(airfare);
			 //String status = airTravelDao.saveCancelDetails(cancelDetails);
			 try
			 {
				 WalletMastBean walletmast = walletUserDao.showUserProfile(airfare.getUserId());
				 String mailTemplate=sendCancelMail(walletmast.getName() ,airfare.getAirFareId() ,airfare.getTripId(), ticketId, airfare,requestBean.getAggreatorId());
				 new CommUtility().sendMail(walletmast.getEmailid(), "Booking Cancellation | "+ticketSegmentList.get(0).getSourceAirportCode()+"-"+ticketSegmentList.get(ticketSegmentList.size()-1).getDestAirportCode() +" | "+ticketSegmentList.get(0).getArrdate(), mailTemplate, "", requestBean.getAggreatorId(), walletmast.getWalletid(), walletmast.getName());
			 }
			 catch(Exception ex)
			 {
				 logger.info(serverName+"************problem in ****Mailing******cancelTicket********e.message*******"+ex.getMessage());
				 ex.printStackTrace();
			 }
			 refundResp = refundAmountImp(airfare.getTripId() , imei,1);
			
			 if(refundResp.getStatusCode().equals("1"))
			 {
				 respBean.setStatus("SUCCESS");
				 respBean.setStatusCode("300");
				 if(passangerCount == noOfCancelledTickets)
					 respBean.setStatusMsg(strMsg+".Please contact customer care for Refund.");
				 else
					 respBean.setStatusMsg(strMsg);
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }
			 respBean.setStatus("SUCCESS");
			 respBean.setStatusCode("300");
			 if(strMsg == null)

				 respBean.setStatusMsg("Ticket Cancelled Successfully. Total Refunded Amount is Rs."+( totalrefundAmount-conveniencefee));
			 else
				 respBean.setStatusMsg(strMsg+" Total Refunded Amount is Rs."+( totalrefundAmount-conveniencefee));
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(NullPointerException ex)
		 {
			 logger.info(serverName+"************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 if(status == 1)
			 {
				 respBean.setStatus("Failed");
				 respBean.setStatusCode("301");
				 respBean.setStatusMsg("Ticket Cancellation is in pending status.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }
			 else
			 {
				 respBean.setStatus("FAILED");
				 respBean.setStatusCode("1");
				 respBean.setStatusMsg("INVALID MESSAGE STRING.");
				 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
				 return responseBean;
			 }
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
		     logger.info(serverName+"************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	
	}

	public List<TicketSegment> cancelSegmentResp(List<TicketSegment> ticketSeg)
	{
		logger.info("*********************Start Execution***************cancelSegmentResp");
		List<TicketSegment> tickSegReturn = new ArrayList<TicketSegment>();
		try
		{
			Iterator<TicketSegment> itr = ticketSeg.iterator();
			while(itr.hasNext())
			{
				TicketSegment ticSeg = itr.next();
				Origin org = new Origin();
				Destination dest = new Destination();
				Airport airS = new Airport();
				Airport airD = new Airport();
				TicketSegment tickSegRes = new TicketSegment();
				tickSegRes.setAirlineCode(ticSeg.getAirlineCode());
				tickSegRes.setAirlineName(ticSeg.getAirlineName());
				tickSegRes.setAirlineNumber(ticSeg.getAirlineNumber());
				airS.setAirportCode(ticSeg.getSourceAirportCode());
				airS.setAirportName(ticSeg.getSourceAirportName());
				airS.setCityCode(ticSeg.getSourceCityCode());
				airS.setCityName(ticSeg.getSource());
				org.setAirport(airS);
				org.setDepTime(ticSeg.getDeptdate());
				airD.setAirportCode(ticSeg.getDestAirportCode());
				airD.setAirportName(ticSeg.getDestAirportName());
				airD.setCityCode(ticSeg.getDestCityCode());
				airD.setCityName(ticSeg.getDestination());
				dest.setAirport(airD);
				dest.setArrTime(ticSeg.getArrdate());
				tickSegRes.setOrigin(org);
				tickSegRes.setDest(dest);
				tickSegReturn.add(tickSegRes);
			}
		}
		catch(Exception ex)
		{
			logger.info("***************Problem in ************cancelSegmentResp*******e.message"+ex.getMessage());
			ex.printStackTrace();
		}
		return tickSegReturn;
	}
	
	
	public List<PassangerDetails> cancelPassResp(List<PassangerDetails> ticketPass)
	{
		logger.info("*********************Start Execution***************cancelPassResp");
		List<PassangerDetails> tickPassReturn = new ArrayList<PassangerDetails>();
		try
		{
			Iterator<PassangerDetails> itr = ticketPass.iterator();
			while(itr.hasNext())
			{
				PassangerDetails ticPass = itr.next();
				PassangerDetails tickPassRes = new PassangerDetails();
				tickPassRes.setPassangername(ticPass.getPassangername());
				tickPassRes.setTicketNumber(ticPass.getTicketNumber());
				tickPassRes.setTicketId(ticPass.getTicketId());
				tickPassRes.setTripId(ticPass.getTripId());
				tickPassRes.setCancelledMode(ticPass.getCancelledMode());
				tickPassRes.setSourceDestinationcode(ticPass.getSourceDestinationcode());
				tickPassRes.setCancellationCharge(ticPass.getCancellationCharge());
				tickPassRes.setCancellationFees(ticPass.getCancellationFees());
				tickPassRes.setRefundedAmount(ticPass.getRefundedAmount());
				tickPassRes.setIsPnrCancelled(ticPass.getIsPnrCancelled());

				tickPassRes.setPnr(ticPass.getPnr());

				tickPassReturn.add(tickPassRes);
			}
		}
		catch(Exception ex)
		{
			logger.info("***************Problem in ************cancelPassResp*******e.message"+ex.getMessage());
			ex.printStackTrace();
		}
		return tickPassReturn;
	}
	
	
	/**
	 * 
	 * @param requestBean
	 * @param request
	 * @param imei
	 * @param agent
	 * @return
	 */
	public ResponseBean refundAmount(RequestBean requestBean , @Context HttpServletRequest request ,@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent )
	{
		 logger.info("***********Start Execution *******cancelTicket*******");
		 ResponseBean responseBean = new ResponseBean();
		 BookingResponseBean respBean = new BookingResponseBean();
		 String mid=null;
		 String pgRefId=null;
		 String appName=null;
		 double amt=0.0;
		 String orderNo=null;
		 String serverName=request.getServletContext().getInitParameter("serverName");
		 
		 String requestId=airTravelDao.saveTravelApiRequest(requestBean.getAggreatorId(),requestBean.getRequest(),serverName,"refundAmount");
		
		logger.info(serverName +"*****RequestId******"+requestId+"******cancelTicket************Aggreator id*****"+requestBean.getAggreatorId());
		logger.info(serverName +"*****RequestId******"+requestId+"******cancelTicket**********Request*****"+requestBean.getRequest());
		logger.info(serverName +"****cancelTicket***********After Saving request ***************requestId*****"+requestId);
		
		responseBean.setRequestId(requestId);
		
		if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Aggreator Id.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		if(requestBean.getRequest()==null ||requestBean.getRequest().isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("Invalid Request.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
		try
		{
		String mKey="e391ab57590132714ad32da9acf3013eb88c";//new MerchantDaoImpl().getKeyByMerchantid(merchantRequestBean.getAgentId());
		
		 logger.info(serverName +"*****RequestId******"+requestId+"****cancelTicket******mKey*****"+mKey);
		 if(mKey==null ||mKey.isEmpty()){
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("FAILED.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}	 
		 try
		 {
			 
			 Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey); 
			 JSONObject jsonclaim = new JSONObject(claim);
			 
			 respBean =  refundAmountImp(jsonclaim.getString("travelId") ,  imei,1);
			 
			
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		 catch(Exception ex)
		 {
			 logger.info(serverName+"************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(oxyJWTSignUtil.generateToken((HashMap)gson.fromJson(gson.toJson(respBean,BookingResponseBean.class), new TypeToken<Map<String, Object>>(){}.getType()), mKey));
			 return responseBean;
		 }
		}
		catch(Exception ex)
		{
			 logger.info(serverName+"************problem in **********cancelTicket********e.message*******"+ex.getMessage());
			 ex.printStackTrace();
			 respBean.setStatus("FAILED");
			 respBean.setStatusCode("1");
			 respBean.setStatusMsg("INVALID MESSAGE STRING.");
			 responseBean.setResponse(gson.toJson(respBean));
			 return responseBean;
		}
	
	}
	

	@POST
	@Path("/test")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public String  hello()
	{
		String ticketPath = servletContext.getRealPath("/flightPDF");
		String pdfPath=ticketPath+"Bhartipay_Ticket_"+"abc"+".pdf";
		return pdfPath;
		
	}
	
	
	public static void main(String s[])
	{
		String data="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> <meta name='viewport' content='width=device-width'/> <!-- For development, pass document through inliner --> <link rel='stylesheet' href='css/simple.css'></head><body><table class='body-wrap' > <tr> <td class='container' style='width:600px'> <!-- Message start --> <table> <tr> <td align='left' class='content'> <table> <tr> <td style=' width: 156px;'> <img src='https://b2b.bhartipay.com/BhartiPayService/image/flights/logo116x50.png' style=' border: none; outline: none; width: 112px;' alt='bhartipay'> </td> <td style='width:189px;text-align:right'> <table> <tr> <td style='text-align:right'> <table> <tr>  <td colspan='2' style='width: 201px;'> <div style='color: #ff6e26;text-align:right; font-size: 15px;margin-bottom:0px'> Booking Confirmation</div></td> </tr> </table> </td> </tr> <tr> <td > <div style='color:#a9a8a8;font-size:8px'>Booking Date: Thu, 10 Jan 19 19:48</div> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td class='content' style='font-size:11px;line-height:18px'> <div> Dear Mr sahajul islam , </div> <div> Thank you for booking with bhartipay </div> <div> Your Mumbai -Kolkata One-way flight is confirmed. we will email your ticket on registered email ID. </div> <div> your booking ID is <span style='color:#ff6e26'>OXTL001206.</span> Please use this boooking ID for any communication with us. </div> </td> </tr> <tr> <td class='content' > <table > <tr> <td style='width:30px; '> <img src='https://b2b.bhartipay.com/BhartiPayService/image/flights/9W.png' alt='Air Asia'> </td> <td> <table> <tr> <td style='font-size:11px;'> Mumbai ---- Kolkata</td> </tr> <tr> <td style='font-size:9px;color:#757575'>Jet Airways-619 </td> </tr> </table> </td> <td> <table> <tr> <td style='font-size:11px;'>Mumbai</td> </tr> <tr> <td style='font-size:9px;color:#757575'>Fri, 11 Jan 19 05:05</td> </tr> <tr> <td style='font-size:9px;color:#757575'>Mumbai</td> </tr> </table> </td> <td style='width:20;text-align:center'> <table> <tr> <td style='text-align:center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='https://b2b.bhartipay.com/BhartiPayService/image/flights/clock.png' style='float:right'></td> </tr> <tr> <td style='font-size:9px;color:#757575;text-align:center'>2 hr 40 min</td> </tr> </table> </td> <td> <table style='border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray'> <tr> <td style='font-size:11px;'>Kolkata</td> </tr> <tr> <td style='font-size:9px;color:#757575'>Fri, 11 Jan 19 07:45</td> </tr> <tr> <td style='font-size:9px;color:#757575'>Kolkata</td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td class='content' style='font-size: 12px; padding: 0px 20px;'><strong style='color:#ff6e26'>Passengers</strong> - 1</td> </tr> <tr> <td class='content' style='font-size:9px; padding: 5px 20px;'> <table style='font-size:14px;border-color:#ccc' cellpadding='5' border='1' valign='middle'> <tr> <td   bg='#f3f3f3' style='vertical-align: middle; background-color: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; borde: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Passenger Name</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Airlines</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Status</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Sector</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Ticket Number</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Airline PNR</td> </tr> <tr> <td style=' font-size: 9px; text-align: center; padding: 5px;'>Mr sahajul islam </td> <td style=' font-size: 9px; text-align: center; padding: 5px;'>Jet Airways-619</td> <td style=' font-size: 9px; text-align: center; padding: 5px;'>Confirmed</td> <td style=' font-size: 9px; text-align: center; padding: 5px;'>BOM-CCU</td> <td style=' font-size: 9px; text-align: center; padding: 5px;'>2988972988</td> <td style=' font-size: 9px; text-align: center; padding: 5px;'> MQCDCU</td> </tr>  </table></td> </tr> <tr> <td class='content' style='font-size: 12px; padding: 0px 20px;'><strong style='color:#ff6e26'>Flight Inclusion</strong></td> </tr> <tr> <td class='content' style='font-size: 9px; padding: 5px 20px; '> <table cellpadding='3' border='1' style='font-size:9px'> <tr> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Passenger Name</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Sector</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Airline</td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Flight Insurance Status</td> <td style=' width: 105px; background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'> <table border='0' cellspacing='0'> <tr> <td colspan='2'>Baggage</td> </tr> <tr> <td style='text-align: left;font-size:7px'>Check in</td> <td style='text-align: right;font-size:7px'>Cabin</td> </tr> </table> </td> <td style=' background: #f3f3f3; text-align: center; font-weight: bold; font-size: 9px; border-top: 1px solid #cacaca; padding: 5px; border-left: 1px solid #cacaca; border-right: 1px solid #cacaca;'>Meal Type</td> </tr> <tr> <td style=' font-size: 9px; text-align: center; padding: 3px;'>Mr sahajul islam </td> <td style=' font-size: 9px; text-align: center; padding: 3px;'>BOM-CCU</td> <td style=' font-size: 9px; text-align: center; padding: 3px;'>Jet Airways-619</td> <td style=' font-size: 9px; text-align: center; padding: 3px;'>Not Confirmed</td> <td style=' font-size: 9px; text-align: center; padding: 3px;'> <table border='0' cellspacing='0'> <tr> <td style='text-align:left;font-size:7px'>15 kg</td> <td style='text-align:right;font-size:7px'>7 kg</td> </tr> </table></td> <td style=' font-size: 9px; text-align: center; padding: 3px;'>Not Confirmed</td> </tr> </table></td> </tr> <tr> <td class='content' > <table  style='border:1px solid #999'> <tr> <td style='font-size: 12px;font-weight:bold; padding: 0px 20px;border:1px solid #999;color:#ff6e26'>Fare Details</td> </tr> </table> </td></tr>   <tr> <td class='content' style='padding: 0px 20px;'> <table style=' border: 1px solid #cacaca;'> <tr> <td style='font-size:11px; padding: 5px 10px;text-align:left'>Base Fare:</td><td style='text-align:right;font-size:11px; padding:5px 10px;'>INR 7463.00</td> </tr> <tr> <td style='font-size:11px; padding: 5px 10px;text-align:left'>Taxes & Fee:</td><td style='text-align:right;font-size:11px; padding:5px 10px;'>INR 2061.00</td> </tr> <tr> <td style='font-size:11px; padding: 5px 10px;text-align:left'>Convenience Fee:</td> <td style='text-align:right;font-size:11px; padding:5px 10px;'>INR 100</td> </tr> <tr> <td style='font-size:11px; padding: 5px 10px;text-align:left;border-top: 1px solid #cacaca;'><strong>Total:</strong></td> <td style='font-size:11px; padding: 5px 10px;text-align:right;border-top: 1px solid #cacaca;'><strong>INR 9624.00</strong></td> </tr> </table></td> </tr>   <tr> <td class='content'> <table> <tr> <td style='font-size:12px;'> <strong>Cancellation Charges </strong> </td> </tr> <tr> <td style='font-size:12px;'> <strong>Bhartipay Fee:</strong> <span style='color:#888'></span> 100 INR per pax per sector </td> </tr> </table> </td> </tr> <tr> <td class='content' style='font-size:12px;padding:5px 20px;'> <strong>Terms & Conditions</strong> </td> </tr> <tr> <td class='content' style='font-size:8px;color:#888;padding:5px 50px;'><ul><li><b>There is no Minimum Stay requirement</b><br><li><b>There is no Maximum Stay requirement</b><br><fieldset><legend><b><b>Adult</b><br></b></legend><ul><li><b>Revalidation/Reissue</b><br>Fare Component <b>N8CORU</b>&emsp; Sector <b>BOM-CCU</b> &nbsp;<ul><li>Revalidation before/after departure, including failure to show at first/subsequent flight(s) : <b>Not Allowed</b></li><li>Reissue, including failure to show at first flight : <b>Allowed with Restrictions</b></li><li><b>Prior to Departure of journey</b><ul><u>Reissue</u><li>Reissue : <b>Allowed, Check with Agent for further details</b></li><li>Maximum reissue penalty fee for entire ticket (per reissue) : <b>No charge</b></li><li>Revalidation/Reissue request must be made prior to : <b>No restriction</b></li></ul></li><li><b>Failure to show at first flight</b><ul><u>Reissue</u><li>Reissue : <b>Allowed, Check with Agent for further details</b></li><li>Maximum reissue penalty fee for entire ticket (per reissue) : <b>No charge</b></li><li>Revalidation/Reissue request must be made prior to : <b>No restriction</b></li></ul></li><li><b>After departure of journey</b><ul><li>Revalidation/Reissue request must be made prior to : <b>No restriction</b></li></ul></li><li><b>Failure to show at subsequent flight(s)</b><ul><li>Revalidation/Reissue request must be made prior to : <b>No restriction</b></li></ul></li></ul></li><li><b>Refund</b><br>Fare Component <b>N8CORU</b>&emsp; Sector <b>BOM-CCU</b> &nbsp;<ul><li>Refund, including failure to show at subsequent flight(s) : <b>Not Allowed</b></li><li><b>Prior to Departure of journey</b><ul><li>Refund : <b>Allowed with Restrictions</b></li><li>Refund : <b>Allowed, Check with Agent for further details</b></li><li>Maximum Refund penalty fee for entire ticket : <b>No charge</b></li><li>Refund request must be made prior to : <b>No restriction</b></li></ul></li><li><b>Failure to show at first flight</b><ul><li>Refund : <b>Not Allowed</b></li><li>Refund request must be made prior to : <b>No restriction</b></li></ul></li><li><b>After departure of journey</b><ul><li>Refund request must be made prior to : <b>No restriction</b></li></ul></li><li><b>Failure to show at subsequent flight(s)</b><ul><li>Refund request must be made prior to : <b>No restriction</b></li></ul></li></ul></li><br><b>* Revalidation is a modification of the original ticket without any reissue of a new ticket.</b><br><b>* For Reissue, Penalty fees are in addition to any difference in fare.</b><br><b>* For Refund, certain Taxes are non-refundable.</b></ul></fieldset></ul><br /><br/> <br/> </td> </tr> <tr> <td class='content' style='font-size:12px;padding:5px 20px 50px'> <div>Contact Information:</div> <div>Bhartipay Contact Information : 0120-3759599</div> <div> Please reference the Airline PNR Number when communicating with the airline regarding this booking. </div> </td> </tr> </table> </td> </tr> <tr> </table></body></html>";
		new CommanUtil().htmlToPdfGeneration("E:/EclipseWS/BhartiPayService/WebContent/ticket.pdf", data);
	}
}


