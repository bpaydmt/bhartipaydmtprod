package com.bhartipay.airTravel.util;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;




public class TBOContextListner implements ServletContextListener{
	
public static final Logger logger = Logger.getLogger(TBOContextListner.class.getName());
	
	Timer t=new Timer();

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		t.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("****************************TBOContextListner context Listener Start*********************** ");
		logger.info("****************************TBOContextListner context Listener Start*********************** ");
		TBOAuthorization tBOAuthorization=new TBOAuthorization();
		int delay=1000*60;
		int interval=1000*60*60*23;
		t.scheduleAtFixedRate(tBOAuthorization, delay, interval);
		logger.info("****************************TBOContextListner context Listener Stop*********************** ");
		
	}

}
