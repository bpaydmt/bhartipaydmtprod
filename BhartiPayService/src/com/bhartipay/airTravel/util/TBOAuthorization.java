package com.bhartipay.airTravel.util;

import java.net.URL;
import java.util.StringTokenizer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.bhartipay.airTravel.AirTravelAPIManager;
import com.bhartipay.airTravel.bean.AuthenticationRequestBean;
import com.bhartipay.airTravel.persistence.AirTravelDao;
import com.bhartipay.airTravel.persistence.AirTravelDaoImpl;
import com.bhartipay.biller.util.UpdateBillStatus;
import com.google.gson.Gson;

public class TBOAuthorization extends TimerTask{
	public static final Logger logger=Logger.getLogger(UpdateBillStatus.class);
	
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	Gson gson = new Gson();
	
	
	@Override
	public synchronized void run() {
		
		authentication();
		
	}
	
	 public void authentication()
	 {
		 logger.info("***********Start Execution *******TBOAuthorization******authentication*");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 AirTravelDao airTravelDao = new AirTravelDaoImpl();
		 String response = null;
		 try
		 {
			 reqbean = airTravelDao.getAuthDetails("Air");
			 //String requestJson = gson.toJson(authenticationRequestBean);
			 String requestJson=gson.toJson(reqbean).toString();//"{\"ClientId\":\""+reqbean.getClientId()+"\",\"UserName\":\""+reqbean.getUserName()+"\",\"Password\":\""+reqbean.getPassword()+"\",\"EndUserIp\":\""+reqbean.getEndUserIp()+"\"}";

			//URL url = new URL("http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate");
			 URL url = new URL("");

			 response = new AirTravelAPIManager().airApi(requestJson, url,"authentication");
			 JSONObject json = new JSONObject(response);//(JSONObject) JSONSerializer.toJSON(responseJson);  
			 reqbean.setTokenId(json.getString("TokenId"));
			 airTravelDao.updateAuthUser(reqbean);
		 }
			 catch(Exception e)
		 {
			 logger.info("***********Problem in******authentication****authentication***e.message"+e.getMessage());
			 e.printStackTrace();
		 }
	
	 }
	 
	 public static void main(String[] s)
	 {
		 String str ="OXMA001002";
		 StringTokenizer token = new StringTokenizer(str, "|");
	 }
	
}
