package com.bhartipay.airTravel;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.xml.ws.WebServiceException;

import org.apache.commons.httpclient.ConnectTimeoutException;
//import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.airTravel.bean.AuthenticationRequestBean;
import com.bhartipay.airTravel.bean.AuthenticationResponseBean;
import com.bhartipay.airTravel.bean.FareRuleRequestBean;
import com.bhartipay.airTravel.bean.FlightSearchRequestBean;
import com.bhartipay.airTravel.bean.LogoutResponseBean;
import com.bhartipay.airTravel.bean.booking.BookingRequest;
import com.bhartipay.airTravel.bean.booking.CancelRequest;
import com.bhartipay.airTravel.bean.booking.TicketNonLccRequest;
import com.bhartipay.airTravel.bean.fareQuote.FareQuote;
import com.bhartipay.airTravel.persistence.AirTravelDao;
import com.bhartipay.airTravel.persistence.AirTravelDaoImpl;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.google.gson.Gson;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import net.pg.appnit.utility.AES128Bit;


public class AirTravelAPIManager 
{
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	private static final Logger logger =  Logger.getLogger(AirTravelAPIManager.class.getName());
	AirTravelDao airTravelDao = new AirTravelDaoImpl();
	Gson gson = new Gson();
		
	public String airApi(String requestJson , URL url , String apiName)
	{
		//logger.info("******Start Execution*********"+apiName+"*******************");
		Commonlogger.log(Commonlogger.INFO,this.getClass().getName(), "","","","","", apiName+"|"+"Request-String:-|"+requestJson);
		String status="1001";
		 try
		 {
			 Commonlogger.log(Commonlogger.INFO,this.getClass().getName(), "","","","","", apiName+"|"+"Request-String:-|"+requestJson);
			 Commonlogger.log(Commonlogger.INFO,this.getClass().getName(), "","","","","", apiName+"|"+"URL-:-|"+url);
			 HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
			 httpconn.setRequestMethod("POST");
			 httpconn.setRequestProperty("Accept-Encoding", "gzip");
			 httpconn.setDoOutput(true);
			 httpconn.setRequestProperty("Content-Type", "application/json");
			 DataOutputStream wr = new DataOutputStream(httpconn.getOutputStream());
			 wr.writeBytes(requestJson);
			 wr.flush();
			 wr.close();

			 int responseCode = httpconn.getResponseCode();
			 Commonlogger.log(Commonlogger.INFO,this.getClass().getName(), "","","","","", apiName+"|"+"responseCode-:-|"+responseCode);
			 BufferedReader in;
			 if(responseCode==200 && httpconn.getContentEncoding().equalsIgnoreCase("gzip")){
			in = new BufferedReader(new InputStreamReader(new GZIPInputStream( httpconn.getInputStream())));
			
		    }else {
			 in = new BufferedReader(new InputStreamReader( httpconn.getInputStream()));
		     }
			 String inputLine;
			 StringBuffer responseJson = new StringBuffer();

			 while ((inputLine = in.readLine()) != null) {
				 responseJson.append(inputLine);
			}
			 Commonlogger.log(Commonlogger.INFO,this.getClass().getName(), "","","","","", apiName+"|"+"Response-:-|"+responseJson.toString());
			 in.close();
			 return responseJson.toString();
		 }
		 catch(ConnectTimeoutException ex)
		 {
			 logger.info("***********problem in ********"+apiName+"****ConnectTimeoutException*****e.message"+ex.getMessage());
			 ex.printStackTrace();
			 return "400";
		 }
		 catch(WebServiceException wex)
		 {
			 logger.info("***********problem in  ********"+apiName+"****WebServiceException*****e.message"+wex.getMessage());
			 wex.printStackTrace();
			 return "7000";
		 }
		 catch(Exception e)
		 {
			 logger.info("*******problem in ******"+apiName+"***********e.message********"+e.getMessage());
			 e.printStackTrace();
			 return "7000";
		 }
	}
	
	public String pgTxnApi(String request , String mid)
	{
		 logger.info("***********Start Execution *******pgTxnApi*******");
		 String status="1001";
		 String responseString="";
			
			try{
				 URL url = new URL("http://testpg.payplutus.in:9080/AppnitPG/service/txn/refund?MID="+mid+"&requestparameter="+request);
			 logger.info("*************************Request String******"+request);
			 logger.info("***********************URL*************"+url);
			 HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
			 httpconn.setRequestMethod("POST");
			 httpconn.setDoOutput(true);
			 httpconn.setRequestProperty("Content-Type", "application/json");
			 DataOutputStream wr = new DataOutputStream(httpconn.getOutputStream());
			 wr.writeBytes(request);
			 wr.flush();
			 wr.close();

			 int responseCode = httpconn.getResponseCode();
			 logger.info("************************responseCode* ***"+responseCode);
			 //long time=Calendar.getInstance().getTimeInMillis();
			 BufferedReader in;
			 
			 in = new BufferedReader(new InputStreamReader( httpconn.getInputStream()));
			 System.out.println(in);
			 String inputLine;
			 StringBuffer responseJson = new StringBuffer();

			 while ((inputLine = in.readLine()) != null) {
				 responseJson.append(inputLine);
			}
			//  String responseStr = decompress(responseJson.toString().getBytes());
			 logger.info("**********************response****"+responseJson.toString());
			 System.out.println(responseJson.toString());
			in.close();
			 return responseJson.toString();
		 
		
			/* MultivaluedMapImpl formData = new MultivaluedMapImpl();


			 formData.add("MID","202");
			 formData.add("requestparameter",request);
		 
			   HttpClient client = HttpClientBuilder.create().build();
		        HttpPost post = new HttpPost("http://testpg.payplutus.in:9080/AppnitPG/service/txn/refund?MID="+mid+"&requestparameter="+request);


		            HttpResponse response = client.execute(post);

		            System.out.println(EntityUtils.toString(response.getEntity()));
		 */
		 
		 
		 
		 
		 
		 }
		 catch(Exception wex)
		 {
			 logger.info("***********Problem in*******pgTxnApi*******e.message"+wex.getMessage());
			 wex.printStackTrace();
		 }
//		 catch(Exception e)
//		 {
//			 logger.info("*******problem in *****pgTxnApi*********e.message********"+e.getMessage());
//			 e.printStackTrace();
//			 status="7000";
//			 return status;
//		 }
		 return responseString;
	 
	}
	
	
	 /**
	  * 
	  * @param authenticationRequestBean
	  * @return
	  */
	 public AuthenticationResponseBean authentication()
	 {
		 logger.info("***********Start Execution *******authentication*******");
		 AuthenticationRequestBean reqbean = new AuthenticationRequestBean();
		 AuthenticationResponseBean authenticationResponseBean = new AuthenticationResponseBean();
		 String response = null;
		 try
		 {
			 reqbean = airTravelDao.getAuthDetails("Air");
			 //String requestJson = gson.toJson(authenticationRequestBean);
			 String requestJson=gson.toJson(reqbean).toString();//"{\"ClientId\":\""+reqbean.getClientId()+"\",\"UserName\":\""+reqbean.getUserName()+"\",\"Password\":\""+reqbean.getPassword()+"\",\"EndUserIp\":\""+reqbean.getEndUserIp()+"\"}";

			// URL url = new URL("http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate");
			 URL url = new URL("");

			 response = airApi(requestJson, url,"authentication");
			 JSONObject json = new JSONObject(response);//(JSONObject) JSONSerializer.toJSON(responseJson);  
			 reqbean.setTokenId(json.getString("TokenId"));
			 airTravelDao.updateAuthUser(reqbean);
		 }
		 
		 catch(Exception e)
		 {
			 logger.info("***********Problem in******authentication*******e.message"+e.getMessage());
			 e.printStackTrace();
		 }
		 return authenticationResponseBean;
	 }

	 /**
	  * 
	  * @param flightreqbean
	  * @return
	  */
	 public String flightSearchApi(FlightSearchRequestBean flightreqbean)
	 {
		 logger.info("***********Start Execution *******flightSearchApi*******");
		 LogoutResponseBean logoutResponseBean = new LogoutResponseBean();
		 String status="1001";
		 try
		 {
			 //JSONObject json = new JSONObject(flightreqbean);
			 String requestJson=new Gson().toJson(flightreqbean);//"{\"EndUserIp\":\"203.122.47.170\",\"TokenId\":\"7201456c-7b4e-476f-8b0b-215baf640d7d\",\"AdultCount\":\"1\",\"ChildCount\":\"0\",\"DirectFlight\":\"false\",\"InfantCount\":\"0\",\"JourneyType\":\"1\",\"OneStopFlight\":\"false\",\"PreferredAirlines\":null,\"Segments\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"FlightCabinClass\":\"1\",\"PreferredDepartureTime\":\"2017-09-25T00:00:00\",\"PreferredArrivalTime\":\"2017-09-25T00:00:00\"}],\"Sources\":null}";//

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search/"/*"https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Search/"*/);
			URL url = new URL("https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Search/");

			 status = airApi(requestJson, url,"flightSearchApi");
			 return status;
		 }
		 catch(WebServiceException wex)
		 {
			 logger.info("***********Problem in*******flightSearchApi*******e.message"+wex.getMessage());
			 wex.printStackTrace();
		 }
		 catch(Exception e)
		 {
			 logger.info("*******problem in *****flightSearchApi*********e.message********"+e.getMessage());
			 e.printStackTrace();
			 status="7000";
			 return status;
		 }
		 return status;
	 }
	 
	 /**
	  * 
	  * @param fareBean
	  * @return
	  */
	 public String fareRuleApi(FareRuleRequestBean fareBean)
	 {
		 logger.info("***********Start Execution *******fareRuleApi*******");
		 String status="1001";
		 try
		 {		

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareRule"/*"https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareRule/"*/);
			URL url = new URL("https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareRule");



			// JSONObject json = new JSONObject(fareBean);
			 String request =new Gson().toJson(fareBean);//  "{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"TraceId\": \"a1dddf74-ed12-432c-bfd8-9d5d1c288601\",\"ResultIndex\": \"OB8\""; // 
			 status = airApi(request, url,"fareRuleApi");
			 return status;
		 }
		 catch(WebServiceException wex)
		 {
			 logger.info("***********Problem in*******fareRuleApi*******e.message"+wex.getMessage());
			 wex.printStackTrace();
		 }
		 catch(Exception e)
		 {
			 logger.info("*******problem in *****fareRuleApi*********e.message********"+e.getMessage());
			 e.printStackTrace();
			 status="7000";
			 return status;
		 }
		 return status;
	 }
	 
	 /**
	  * 
	  * @param fareBean
	  * @return
	  */
	 public String fareQuoteApi(FareRuleRequestBean fareBean)
	 {
		 logger.info("***********Start Execution *******fareQuoteApi*******");
		 String status="1001";
		 try
		 {			 


			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareQuote");

			URL url = new URL("https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareQuote/");

			 String request = new Gson().toJson(fareBean);//"{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"TraceId\": \"b69088ec-63e7-488b-bfe4-cb4c7a355ec7\",\"ResultIndex\": \"OB42\""; //
			 status = airApi(request, url,"fareQuoteApi");
		 }
		 catch(WebServiceException wex)
		 {
			 logger.info("***********Problem in*******fareQuoteApi*******e.message"+wex.getMessage());
			 wex.printStackTrace(); 
			 return "7000";
		 }
		 catch(Exception e)
		 {
			 logger.info("*******problem in *****fareQuoteApi*********e.message********"+e.getMessage());
			 e.printStackTrace();
			 status="7000";
			 return status;
		 }
		 return status;
	 }
	 
	 public String flightBooking(BookingRequest bookingRequest)
	 {
		 logger.info("********************Start Execution*********flightBooking");
		 try
		 {
			 String req=gson.toJson(bookingRequest).toString();//"{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"TraceId\": \"2217124c-199f-4909-b2e9-f44e28b66065\",\"ResultIndex\": \"OB57\",\"Passengers\":[{\"Title\":\"Mr\",\"FirstName\":\"BICVHYCLG\",\"LastName\":\"gupta\",\"PaxType\":2,\"DateOfBirth\":\"2000-12-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"TransactionFee\":1,\"YQTax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"AirTransFee\":1},\"Meal\":{\"Code\":\"AVML\",\"Description\":\"Asian - Vegetarian\"},\"Seat\":{\"Code\":\"A\",\"Description\":\"Aisle\"}},{\"Title\":\"Mr\",\"FirstName\":\"BICVHYCLG\",\"LastName\":\"Singh\",\"PaxType\":1,\"DateOfBirth\":\"2014-12-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"TransactionFee\":1,\"YQTax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"AirTransFee\":1},\"Meal\":{\"Code\":\"AVML\",\"Description\":\"Asian - Vegetarian\"},\"Seat\":{\"Code\":\"A\",\"Description\":\"Aisle\"}},{\"Title\":\"Mr\",\"FirstName\":\"fgddf\",\"LastName\":\"Singh\",\"PaxType\":3,\"DateOfBirth\":\"2017-05-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"YQTax\":1,\"AdditionalTxnFee\":1,\"AirTransFee\":1}}]}";//

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Book");

			 URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book/");

			 String resp=airApi(req, url,"flightBooking");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("*******************Problem in *********flightBooking**********e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 
	 public String bookTicket(String req)
	 {
		 //String req=gson.toJson(bookingRequest);//"{\"EndUserIp\":\"192.168.11.58\",\"TokenId\":\"5d667e62-1c57-4a17-b427-82fbf64dbf3f\",\"TraceId\":\"24f13874-21c5-45d4-974b-92dccbc40fbf\",\"ResultIndex\":\"OB2\",\"Passengers\":[{\"Title\":\"Mr\",\"FirstName\":\"BICVHYCLG\",\"LastName\":\"gupta\",\"PaxType\":2,\"DateOfBirth\":\"2012-12-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"TransactionFee\":1,\"YQTax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"AirTransFee\":1},\"Meal\":{\"Code\":\"AVML\",\"Description\":\"Asian - Vegetarian\"},\"Seat\":{\"Code\":\"A\",\"Description\":\"Aisle\"}},{\"Title\":\"Mr\",\"FirstName\":\"BICVHYCLG\",\"LastName\":\"Singh\",\"PaxType\":1,\"DateOfBirth\":\"2002-12-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"TransactionFee\":1,\"YQTax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"AirTransFee\":1},\"Meal\":{\"Code\":\"AVML\",\"Description\":\"Asian - Vegetarian\"},\"Seat\":{\"Code\":\"A\",\"Description\":\"Aisle\"}},{\"Title\":\"Mr\",\"FirstName\":\"fgddf\",\"LastName\":\"Singh\",\"PaxType\":3,\"DateOfBirth\":\"2013-12-06T00:00:00\",\"Gender\":1,\"PassportNo\":\"KJHHJKHKJH\",\"PassportExpiry\":\"2020-12-06T00:00:00\",\"AddressLine1\":\"123, Test\",\"AddressLine2\":\"\",\"City\":\"Gurgaon\",\"CountryCode\":\"IN\",\"CountryName\":\"India\",\"ContactNo\":\"9879879877\",\"Email\":\"harsh@tbtq.in\",\"IsLeadPax\":true,\"FFAirline\":\"\",\"FFNumber\":\"\",\"Fare\":{\"BaseFare\":1,\"Tax\":1,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":1,\"YQTax\":1,\"AdditionalTxnFee\":1,\"AirTransFee\":1}}]}";
		 try
		 {

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket");

			 URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/");

			 String resp=airApi(req, url,"bookTicket");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 
	 public String getBookingDetails(TicketNonLccRequest bookingdetailReq)
	 {
		 logger.info("**********************Start Execution***********************getBookingDetails");
		 try
		 {
			 String req=gson.toJson(bookingdetailReq).toString();//"{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"PNR\": \"E94ZPG\", \"FirstName\" :  \"AJ\",\"LastName\" : \"MA\" } ";//

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetBookingDetails"/*"https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetBookingDetails/"*/);
			URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetBookingDetails/");

			 String resp=airApi(req, url,"getBookingDetails");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("*********************Problem in************getBookingDetails********e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 
	 public String sSR(FareRuleRequestBean fareBean)
	 {
		 logger.info("*********************Start Execution**************sSR*****");
		 try
		 {
			 String req=gson.toJson(fareBean);//"{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"TraceId\": \"2217124c-199f-4909-b2e9-f44e28b66065\",\"ResultIndex\": \"OB57\"";//
			 URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SSR/");
			 String resp=airApi(req, url,"sSR");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("********************Problem in**************sSR*****e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 
	 
	 public String releasePNR(CancelRequest cancelRequest)
	 {
		 logger.info("*********************Start Execution**************releasePNR*****");
		 try
		 {
			 String req=gson.toJson(cancelRequest);//"{\"BookingId\": 24771,\"RequestType\": 2,\"CancellationType\": 3,\"Sectors\": [{\"Origin\": \"BOM\",\"Destination\": \"GOI\"}],\"TicketId\" : [20749],\"Remarks\": \"Test remarks\",\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\"}";//

			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/ReleasePNRRequest/");
			 URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/ReleasePNRRequest/");
			 String resp=airApi(req, url,"releasePNR");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("********************Problem in**************releasePNR*****e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 public String cancelTicket(CancelRequest cancelRequest)
	 {
		 logger.info("*********************Start Execution**************cancelTicket*****");
		 try
		 {
			 String req=gson.toJson(cancelRequest);//"{\"BookingId\": 34624,\"RequestType\": 2,\"CancellationType\": 3,\"Sectors\": [{\"Origin\": \"BOM\",\"Destination\": \"DEL\"}],\"TicketId\" : [28603,28604], \"Remarks\": \"Test remarks\", \"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\"} ";//


			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SendChangeRequest/"/*"https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SendChangeRequest/"*/);
			 URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SendChangeRequest/");

			 String resp=airApi(req, url,"cancelTicket");
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("********************Problem in**************cancelTicket*****e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 
	 public String getCancelTicket(CancelRequest cancelRequest)
	 {
		 logger.info("*********************Start Execution**************getCancelTicket*****");
		 try
		 {
			 String req=gson.toJson(cancelRequest);//"{\"BookingId\": 34624,\"RequestType\": 2,\"CancellationType\": 3,\"Sectors\": [{\"Origin\": \"BOM\",\"Destination\": \"DEL\"}],\"TicketId\" : [28603,28604], \"Remarks\": \"Test remarks\", \"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\"} ";//


			//URL url = new URL("http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetChangeRequestStatus");

			URL url = new URL("https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetChangeRequestStatus/");

			 String resp=airApi(req, url,"getCancelTicket");//"{\"Response\":{\"ResponseStatus\":1,\"TraceId\":\"a51bb9f8-5168-408c-986f-1e90afdeafcd\",\"ChangeRequestId\":207033,\"TicketId\":1618808,\"RefundedAmount\":528.0000,\"CancellationCharge\":2416.0000,\"ServiceTaxOnRAF\":0.00,\"ChangeRequestStatus\":4,\"SwachhBharatCess\":0.00,\"CreditNoteNo\":\"DZ\\/1819\\/339\",\"KrishiKalyanCess\":0.00,\"CreditNoteCreatedOn\":\"2018-04-27T11:13:55\"}}";//
			 return resp;
		 }
		 catch(Exception ex)
		 {
			 logger.info("********************Problem in**************getCancelTicket*****e.message"+ex.getMessage());
			 ex.printStackTrace();
		 }
		 return null;
	 }
	 public static void main(String s[]) throws Exception
	 {
		/* FlightSearchRequestBean flightreqbean = new FlightSearchRequestBean();
		 FareRuleRequestBean fareBean = new FareRuleRequestBean();
		 BookingRequest bookingRequest = new BookingRequest();
		 CancelRequest cancelRequest = new CancelRequest();
		 cancelRequest.setTokenId("6cc281ad-07fc-4b89-a2e7-2461ada2160d");
		 cancelRequest.setEndUserIp("203.122.47.170");
		 cancelRequest.setSource(4);
		 cancelRequest.setBookingId(28647);
		// fareBean.setTraceId(traceId);
		 //fareBean.setResultIndex(resultIndex);
		// new AirTravelAPIManager().authentication();
		new AirTravelAPIManager().cancelTicket(cancelRequest);
		// WalletConfiguration walletConfiguration=new CommanUtilDaoImpl().getWalletConfiguration("AGGR001035");
		 String key ="ltKDLtXh+WmslIbQ+aL6z4xmWjkU9rkhu3BoWubSjg4=";
		 String requestParam="MID="+"202"+"|"+"PGTxnId="+"13640"+"|"+"Amount="+"12"+"|"+"OrderId="+"PGTX005327";
		 String mid="202";
		 requestParam = AES128Bit.encrypt(requestParam, key);
		 new AirTravelAPIManager().pgTxnApi(requestParam,mid);*/
		//JSONObject claimJson = new JSONObject(new AirTravelAPIManager().flightSearchApi(flightreqbean)); 
		// JsonElement je = new JsonParser().parse(claim.toString());
		 //FlightSearchRequestBean flightreqbean1 = new FlightSearchRequestBean();
		 //flightreqbean = new Gson().fromJson(claimJson.toString() ,FlightSearchRequestBean.class);
	  // new AirTravelAPIManager().flightBooking();
		 FareQuote resp = new FareQuote();
		 CancelRequest req = new CancelRequest();
		 req.setChangeRequestId(206208);
		 req.setEndUserIp("203.122.47.170");
		 req.setTokenId("80d1c86a-f309-44d1-9531-72fa366d2571");
		 
		 
		 String ss =new AirTravelAPIManager().getCancelTicket(req);
		 resp = new Gson().fromJson(ss, FareQuote.class);
		 System.out.println(resp);
		 /*
		 FareRuleRequestBean fareBean = new FareRuleRequestBean();
		new AirTravelAPIManager().fareRuleApi(fareBean);*/
		//fareBean.setTraceId(traceId);
		 //fareBean.setResultIndex(resultIndex);
		 
		/* CancelRequest cancelRequest = new CancelRequest();
		 new AirTravelAPIManager().cancelTicket(cancelRequest);*/
		/*TicketNonLccRequest bookingdetailReq = new TicketNonLccRequest();
		new AirTravelAPIManager().getBookingDetails(bookingdetailReq);*/
		//new AirTravelAPIManager().flightBooking(bookingRequest);
		//String req="{\"EndUserIp\": \"203.122.47.170\",\"TokenId\": \"80d1c86a-f309-44d1-9531-72fa366d2571\",\"TraceId\": \"4df0bf5a-3203-442f-8f1d-58ef3090b054\",\"PNR\": \"5UM3TP\",\"BookingId\": \"1301907\"}";//airApi(req, url);
		//new AirTravelAPIManager().bookTicket(req);
		//new AirTravelAPIManager().sSR(fareBean);
	 }
	
}
