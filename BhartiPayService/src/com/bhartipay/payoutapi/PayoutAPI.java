
	package com.bhartipay.payoutapi;
	

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.mudra.bean.FundTransferBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.SenderLedgerBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.payoutapi.bean.ExtRemittanceResponseBean;
import com.bhartipay.payoutapi.bean.FundTransferResponse;
import com.bhartipay.payoutapi.cashfree.persistnace.CashFreeExtremmitapiDaoImpl;
import com.bhartipay.payoutapi.cashfree.persistnace.PayoutClientConfig;
import com.bhartipay.payoutapi.payu.persistnace.PayuExtremmitapiDaoImpl;
import com.bhartipay.payoutapi.persistnace.ExtremmitapiDao;
import com.bhartipay.payoutapi.persistnace.ExtremmitapiDaoImpl;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.PayoutEncryption;
import com.bhartipay.util.PayoutEncryptionFactory;
import com.google.gson.Gson;

@Path("/payout")
public class PayoutAPI {
		

		private static final Logger logger = Logger.getLogger(PayoutAPI.class.getName());
		Gson gson = new Gson();
		BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
		MudraDao mudraDaoWallet=new MudraDaoImpl();
		MudraDaoBank mudraDao=new MudraDaoBankImpl();
		ExtremmitapiDao remmiDao = null;
		TransactionDao transactionDao = new TransactionDaoImpl();
		WalletUserDao walletUserDao = new WalletUserDaoImpl();
		HashMap<String,Object> responsemap= new HashMap<String,Object>();
		
	@POST
	@Path("/fundTransferRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean mrTransferRequest(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,String encryptedRequest,@Context HttpServletRequest request)
	{
		MudraMoneyTransactionBean mudraMoneyTransactionBean=new MudraMoneyTransactionBean();
		MudraSenderBank mudraSender = new MudraSenderBank();
		MudraBeneficiaryBank mudraBeneficiary = new MudraBeneficiaryBank();
		FundTransferBean fundTransferBean = new FundTransferBean();
		ExtRemittanceResponseBean mrTransferResponse=new ExtRemittanceResponseBean();
		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		String serverName="";//request.getServletContext().getInitParameter("serverName");
		String aggregatorId=null;
		String privateKey = null;
		InputStream in =null;
		String operationType="";
		String requestId = null;
		JSONObject requestJson =null;
		PayoutEncryption payoutEncryption =  null;
		String transferVia =  null;
		String encType = null;
		try{

			
			requestJson = new JSONObject(encryptedRequest);
			aggregatorId = requestJson.getString("aggregatorId");
			
			Properties prop = new Properties();
			in = this.getClass().getResourceAsStream("remittanceKey.properties");
			prop.load(in);
			in.close();
			
			String transferApp=prop.getProperty("transferApp");
			PayoutClientConfig config = walletUserDao.getPayoutClientDetails(aggregatorId, transferApp);
			
			if(config == null) {
				return null;
			}else {
				privateKey = config.getPrivateKey();
				transferVia = config.getTransferVia();
				encType = config.getEncType();
			}
			String ips=prop.getProperty("whiteListIP");
//			privateKey = prop.getProperty(aggregatorId+"PrivateKey");
//			transferVia = prop.getProperty(aggregatorId+"TransferVia");
//			encType = prop.getProperty(aggregatorId+"EncyType");
			
			payoutEncryption = PayoutEncryptionFactory.getEncryptionObject(encType);
			List<String> ipList = new ArrayList<String>(Arrays.asList(ips.split(","))); 
			if(ipiemi == null)
			{
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Authentication Problem.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
			}

			if("CASHFREE".equalsIgnoreCase(transferVia)) {
				remmiDao = new CashFreeExtremmitapiDaoImpl();
			}else if("PAYTM".equalsIgnoreCase(transferVia)){
				remmiDao = new ExtremmitapiDaoImpl();
			}else if("PAYU".equalsIgnoreCase(transferVia)){
				remmiDao = new PayuExtremmitapiDaoImpl();
			}
			requestId=remmiDao.saveRemitApiRequest(aggregatorId,encryptedRequest,serverName,"mrTransferRequest");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "Inside mrTransferRequest|RequestId:"+requestId);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", requestId ,"", "", "Inside mrTransferRequest|Request : "+encryptedRequest);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "Inside mrTransferRequest|Request:"+requestJson.getString("request"));
			mrTransferResponse.setRequestId(requestId);
			String decryptJson = payoutEncryption.decryptOpenSSL(privateKey,requestJson.getString("request"));
			fundTransferBean = gson.fromJson(decryptJson, FundTransferBean.class);
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggregatorId, "", "", "", "", "problem while getting property file|e.getMessage"+e.getMessage());
			e.printStackTrace();

			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Authentication Problem.");
			
			try {
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			} catch (IOException | GeneralSecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			return mrTransferResponse;
		
		}

		if(encryptedRequest == null || encryptedRequest.isEmpty())
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Request.");
			try {
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			} catch (IOException | GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mrTransferResponse;
		}
		try{
		
		fundTransferBean.setAggregatorId(aggregatorId);
		if(fundTransferBean.getAggregatorId()==null ||fundTransferBean.getAggregatorId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		if(fundTransferBean.getMerchantTransId()==null ||fundTransferBean.getMerchantTransId().isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Merchant Transaction ID.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}			
		int count = mudraDao.gettxndetailsCount(fundTransferBean.getMerchantTransId(),aggregatorId, serverName,requestId);
		if(count > 0)
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Duplicate Merchant Transaction ID.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}

		if(fundTransferBean.getBankName() == null || fundTransferBean.getBankName().isEmpty() || !nameValidation(fundTransferBean.getBankName()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid bank name.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		if(fundTransferBean.getAccountNo() == null || fundTransferBean.getAccountNo().isEmpty() || !accountValidation(fundTransferBean.getAccountNo()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid beneficiary account number.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		if(fundTransferBean.getIfscCode() == null || fundTransferBean.getIfscCode().isEmpty() || !iFSCValidation(fundTransferBean.getIfscCode()))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid beneficiary IFSC.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		if(fundTransferBean.getTransferType() == null || (!fundTransferBean.getTransferType().equals("NEFT") && !fundTransferBean.getTransferType().equals("IMPS")))
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid transfer type.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		if(fundTransferBean.getAmount() < 1 )
		{
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Transaction Amount cannot be less than Re 1.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return mrTransferResponse;
		}
		 if(fundTransferBean.getAmount() > 200000)
		 {
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Transaction Amount cannot be greater than Rs 200000.");
			 mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			 return mrTransferResponse;
		 }
		 mudraSender.setAgentId(aggregatorId);
		 mudraSender = remmiDao.createSenderBankIfNotExt(mudraSender,serverName,requestId);
		 
		 mudraBeneficiary.setAccountNo(fundTransferBean.getAccountNo());
		 mudraBeneficiary.setSenderId(mudraSender.getId());
		 mudraBeneficiary.setName(fundTransferBean.getBeneName());
		 mudraBeneficiary.setBankName(fundTransferBean.getBankName());
		 mudraBeneficiary.setIfscCode(fundTransferBean.getIfscCode());
		 mudraBeneficiary.setTransferType(fundTransferBean.getTransferType());
		 mudraBeneficiary.setAgent(agent);
		 mudraBeneficiary.setIpiemi(ipiemi);
		 mudraBeneficiary.setAggreatorId(fundTransferBean.getAggregatorId());
		 mudraBeneficiary.setVerifiedStatus("NV");
		 mudraBeneficiary.setVerified("NV");
		 mudraBeneficiary = remmiDao.createBeneBankIfNotExt(mudraBeneficiary,serverName,requestId);
		
		 
		 mudraMoneyTransactionBean.setUserId(aggregatorId);
		 mudraMoneyTransactionBean.setNarrartion(fundTransferBean.getTransferType());
		 mudraMoneyTransactionBean.setSenderId(mudraSender.getId());
		 mudraMoneyTransactionBean.setBeneficiaryId(mudraBeneficiary.getId());
		 mudraMoneyTransactionBean.setNarrartion(fundTransferBean.getTransferType());
		 mudraMoneyTransactionBean.setTxnAmount(fundTransferBean.getAmount());
		 mudraMoneyTransactionBean.setTransType(fundTransferBean.getTransferType());
		 mudraMoneyTransactionBean.setAccHolderName(mudraBeneficiary.getName());
		 mudraMoneyTransactionBean.setMerchantTransId(fundTransferBean.getMerchantTransId());
		 mudraMoneyTransactionBean.setIpiemi(ipiemi);
		 mudraMoneyTransactionBean.setAgent(agent);
		 mudraMoneyTransactionBean.setAgentid(aggregatorId);
		 mudraMoneyTransactionBean.setWalletId(transferVia);
		 
		
		 
		fundTransactionSummaryBean=remmiDao.fundTransferBank(mudraMoneyTransactionBean,serverName,requestId);

		 logger.info(serverName +"*****RequestId******"+requestId+"**********StatusCode*****"+fundTransactionSummaryBean.getStatusCode());
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7024")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Insufficient Transfer Limit.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1333")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Insufficient Balance.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
			if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1130")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Beneficiary does not belong.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey)); 
				return mrTransferResponse;
			 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1001")||fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("7000")){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey)); 
			return mrTransferResponse;
		 }

		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1116")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Beneficiary Id.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1111")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Transfer Type.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1170"))
		 {
			   responsemap.put("code", "1");
			   responsemap.put("response", "ERROR");
			   responsemap.put("message", "IMPS transfer channel not open. ");
			   mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey)); 
			   return mrTransferResponse;  
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1120")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Amount can't be zero.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1122")){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transfer Limit exceed.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		 
		 if(fundTransactionSummaryBean.getStatusCode().equalsIgnoreCase("1000")){
		
			  				
				MudraMoneyTransactionBean mResult=new MudraMoneyTransactionBean();
				
				List <MudraMoneyTransactionBean> results=fundTransactionSummaryBean.getMudraMoneyTransactionBean();
				Iterator<MudraMoneyTransactionBean> iterator = results.iterator();
				
				while (iterator.hasNext()) {
					mResult=iterator.next();
					if(mResult.getCrAmount()==0){
						break;
					}
					}
				HashMap<String,Object> moneyRemittance= new HashMap<String,Object>();
				moneyRemittance.put("amount", fundTransactionSummaryBean.getAmount());
				moneyRemittance.put("paymentId", mResult.getTxnId());
				moneyRemittance.put("bankTransId", mResult.getBankRrn());
				moneyRemittance.put("transferStatus", mResult.getStatus());
				moneyRemittance.put("transDate", mResult.getPtytransdt());
				HashMap<String,Object> beneficiary= new HashMap<String,Object>();
				MudraBeneficiaryBank bResult=mudraDao.getBeneficiary(mResult.getBeneficiaryId(), serverName);
				if(!operationType.equalsIgnoreCase("verify"))
				{
					moneyRemittance.put("charges", mudraMoneyTransactionBean.getSurChargeAmount());
					moneyRemittance.put("transferType", mResult.getNarrartion());
					moneyRemittance.put("transId", mResult.getId());
					responsemap.put("moneyRemittance", moneyRemittance);					
					beneficiary.put("beneficiaryName", bResult.getName());
					beneficiary.put("accountNo", bResult.getAccountNo());
					beneficiary.put("ifscCode", bResult.getIfscCode() );
					responsemap.put("beneficiary", beneficiary);					
					responsemap.put("merchantTransId", mudraMoneyTransactionBean.getMerchantTransId());
					
				}
				else{
					responsemap.put("mt_dtl", moneyRemittance);
					beneficiary.put("verified", fundTransactionSummaryBean.getVerified());
					beneficiary.put("accholdername", fundTransactionSummaryBean.getAccHolderName());
					beneficiary.put("verificationdesc", fundTransactionSummaryBean.getVerificationDesc());
					responsemap.put("verification",beneficiary);
					responsemap.put("transid",mResult.getMerchantTransId());
				}

				responsemap.put("code", "300");
				responsemap.put("response", "SUCCESS");
				responsemap.put("message", "SUCCESS.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 
		 }else{
			 	responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Velocity check error.");
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return mrTransferResponse;
		 }
		}//added on 3rd Aug
		 catch (NumberFormatException e) 
		 {
		  logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
		  e.printStackTrace();
		  responsemap.put("code", "1");
		  responsemap.put("response", "ERROR");
		  responsemap.put("message", "Invalid amount");
		  try {
		  mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
		  }catch (Exception ex) {
			// TODO: handle exception
		}
		  return mrTransferResponse;
		 }
		catch (Exception e) {
			logger.info(serverName+"*****RequestId******"+requestId+"************************************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "INVALID MESSAGE STRING");
			try {
				mrTransferResponse.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (GeneralSecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return mrTransferResponse;
		} 
	}



	/**
	 * 
	 * @param merchantRequestBean
	 * @param request
	 * @return
	 */
	@POST
	@Path("/getTransStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
	public ExtRemittanceResponseBean getTransStatus(String requestBean,@Context HttpServletRequest request){
		
		
		String serverName=request.getServletContext().getInitParameter("serverName");
		//String serverName="PAYOUT";
		String aggregatorId=null;
		String privateKey = null;
		InputStream in =null;
		String requestId = null;
		JSONObject requestJson =null;
		PayoutEncryption payoutEncryption = null;
		String transferVia = null;
		String encType = null;
		ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
		try{

			requestJson = new JSONObject(requestBean);
			aggregatorId = requestJson.getString("aggregatorId");
			Properties prop = new Properties();
			in = this.getClass().getResourceAsStream("remittanceKey.properties");
			prop.load(in);
			in.close();
			
			String transferApp=prop.getProperty("transferApp");
			PayoutClientConfig config = walletUserDao.getPayoutClientDetails(aggregatorId, transferApp);
			
			if(config == null) {
				return null;
			}else {
				privateKey = config.getPrivateKey();
				transferVia = config.getTransferVia();
				encType = config.getEncType();
			}
			
//			privateKey = prop.getProperty(aggregatorId+"PrivateKey");
			remmiDao = new ExtremmitapiDaoImpl();
			String ips=prop.getProperty("whiteListIP");
			payoutEncryption = PayoutEncryptionFactory.getEncryptionObject(encType);
			List<String> ipList = new ArrayList<String>(Arrays.asList(ips.split(","))); 
			requestId=remmiDao.saveRemitApiRequest(aggregatorId,requestBean,serverName,"mrTransferRequest");

		 JSONObject claim = new JSONObject(requestBean);
			
		if(claim.getString("aggregatorId") ==null ||claim.getString("aggregatorId").isEmpty()){
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Invalid Aggregator ID.");
			responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			return responseBean;
		}
		String aggreatorid=claim.getString("aggregatorId");
		responseBean.setRequestId(requestId);
		logger.info(serverName +"********RequestId***"+requestId+"********"+requestBean);
		
		String decryptJson = payoutEncryption.decryptOpenSSL(privateKey,requestJson.getString("request"));
		JSONObject jsonObject = new JSONObject(decryptJson);
		String txnId = jsonObject.getString("transid");
		
		if(txnId==null || txnId.isEmpty()){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Transaction ID missing. ");
				responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return responseBean;
		}
		 SenderLedgerBean gettransDtl=mudraDao.gettxndetails(txnId,aggreatorid,  serverName, requestId);
		 //if(gettransDtl==null && gettransDtl.size()<=0){
		 if(gettransDtl == null || gettransDtl.getStatus() == null || gettransDtl.getStatus().isEmpty()){
			 responsemap.put("code", "1");
			 responsemap.put("response", "ERROR");
			 responsemap.put("message", "Invalid Transaction Id. ");
			 responsemap.put("agentTransId", claim.get("agentTransId"));
			 responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			 return responseBean;
		 }else{
			 responsemap.put("code", "300");
			 responsemap.put("response", "SUCCESS");
			 responsemap.put("message", "SUCCESS. ");
			 responsemap.put("transId", txnId);
			 responsemap.put("status", gettransDtl.getStatus() );
			 responsemap.put("BankRRN", gettransDtl.getBankrrn());
			 responsemap.put("amount", gettransDtl.getDramount() );
			 responsemap.put("transDateTime", gettransDtl.getPtytransdt() );
			 responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			 	 
			return responseBean; 
		 }
		}catch (Exception e) {
			logger.info(serverName+"*****RequestId*****************e.getMessage()*****"+e.getMessage());
			responsemap.put("code", "1");
			responsemap.put("response", "ERROR");
			responsemap.put("message", "Transaction not found in our records.");
			//responseBean.setRequestId("");
			try {
				responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
			} catch (IOException | GeneralSecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return responseBean;
		}
	}
	
	public  boolean iFSCValidation(String ifsc) {

	    Pattern pattern = Pattern.compile("^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$");
	    Matcher matcher = pattern.matcher(ifsc);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	public  boolean nameValidation(String mobile) {

	    Pattern pattern = Pattern.compile("^[a-zA-Z ]+$");
	    Matcher matcher = pattern.matcher(mobile);

	    if (matcher.matches()) {
	  	  return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	public  boolean mobileValidation(String mobile) {
		//added on 2nd Aug
		    Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		    Matcher matcher = pattern.matcher(mobile);

		    if (matcher.matches()) {
		  	  return true;
		    }
		    else
		    {
		    	return false;
		    }
		}

		public  boolean accountValidation(String account) {

		    Pattern pattern = Pattern.compile("^[0-9]{9,26}$");
		    Matcher matcher = pattern.matcher(account);

		    if (matcher.matches()) {
		  	  return true;
		    }
		    else
		    {
		    	return false;
		    }
		}
	
		
		@POST
		@Path("/getWalletBalanceDtl")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	
		public ExtRemittanceResponseBean getWalletBalanceDtl(String requestBean ,@Context HttpServletRequest request){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","getWalletBalanceDtl","  getWalletBalanceDtl request "+requestBean);
			
			String serverName=request.getServletContext().getInitParameter("serverName");
			//String serverName="PAYOUT";
			String aggregatorId=null;
			String privateKey = null;
			InputStream in =null;
			String requestId = null;
		    JSONObject requestJson =null;
			PayoutEncryption payoutEncryption = null;
			String encType = null;
			ExtRemittanceResponseBean responseBean=new ExtRemittanceResponseBean();
			try{

				requestJson = new JSONObject(requestBean);
				aggregatorId = requestJson.getString("aggregatorId");
				Properties prop = new Properties();
				in = this.getClass().getResourceAsStream("remittanceKey.properties");
				prop.load(in);
				in.close();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","aggregatorId",aggregatorId, "","getWalletBalanceDtl","  getWalletBalanceDtl request "+requestBean);
				
				String transferApp=prop.getProperty("transferApp");
				PayoutClientConfig config = walletUserDao.getPayoutClientDetails(aggregatorId, transferApp);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","aggregatorId", aggregatorId, transferApp ,"  getWalletBalanceDtl  "+config);
				
				if(config == null) {
					return null;
				}else {
					privateKey = config.getPrivateKey();
					encType = config.getEncType();
				}
		     remmiDao = new ExtremmitapiDaoImpl();
             payoutEncryption = PayoutEncryptionFactory.getEncryptionObject(encType);
			 requestId=remmiDao.saveRemitApiRequest(aggregatorId,requestBean,serverName,"mrTransferRequest");
			 responseBean.setRequestId(requestId);
			 JSONObject claim = new JSONObject(requestBean);
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",requestId,"aggregatorId", aggregatorId, transferApp ,"  getWalletBalanceDtl payout enc "+payoutEncryption);
					
			if(claim.getString("aggregatorId") ==null ||claim.getString("aggregatorId").isEmpty()){
				responsemap.put("code", "1");
				responsemap.put("response", "ERROR");
				responsemap.put("message", "Invalid Aggregator ID.");
				responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				return responseBean;
			}
			
			String decryptJson = payoutEncryption.decryptOpenSSL(privateKey,requestJson.getString("agentId"));
			
			String getWalletDtl = mudraDao.getWalletBalance(decryptJson);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName()," Wallet Balance",getWalletDtl, requestId, aggregatorId, transferApp ,"  getWalletBalanceDtl decrypted json "+decryptJson);
			 
			 if(getWalletDtl == null ){
				 responsemap.put("code", "1");
				 responsemap.put("response", "ERROR");
				 responsemap.put("message", "Invalid aggreatorid Id. ");
				 responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				 return responseBean;
			 }else{
				 responsemap.put("code", "300");
				 responsemap.put("response", "SUCCESS");
				 responsemap.put("message", "SUCCESS. ");
				 responsemap.put("amount", getWalletDtl );
				 responseBean.setResponse(payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey));
				 //responseBean.setResponse(payoutEncryption.decryptOpenSSL(privateKey, payoutEncryption.encryptOpenSSL(gson.toJson(responsemap),privateKey)));
				 //responseBean.setRequestId(payoutEncryption.decryptOpenSSL(privateKey,"ra2zY97OcFwUGu9Hx4Rpxzj2wsbKP4FTrrDlea7HdqxES9pK1hJGvwT3LaelFIBT3Yseqzm/PwViOHGK1CLPkImzvWoPTz8dtmvdKtzlnQ4="));
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName()," Wallet Balance",getWalletDtl, requestId, aggregatorId, responseBean.getRequestId() ,"  getWalletBalanceDtl response  "+responseBean.getResponse());
							
				 return responseBean; 
			 }
			}catch (Exception e) {
				logger.info("*****RequestId*****************e.getMessage()*****"+e.getMessage());
				 //Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", requestId, aggregatorId, responseBean.getRequestId() ,"  getWalletBalanceDtl problem in  "+e.getMessage() );
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName()," Wallet Balance","", requestId, aggregatorId, responseBean.getRequestId() ,"Problem in  getWalletBalanceDtl   "+e.getMessage());
						
				e.printStackTrace();
			}
			return responseBean; 
		}


	public static void main(String[] args) {
		for(int i=0;i<1;i++) {
		FundTransferBean fundTransferBean = new FundTransferBean();
		fundTransferBean.setBeneName("Ambujs Singh");
		fundTransferBean.setAccountNo("50100102602137");
		fundTransferBean.setIfscCode("HDFC0001898");
		fundTransferBean.setBankName("Ambuj");
		fundTransferBean.setAmount(1);
		fundTransferBean.setTransferType("IMPS");
		fundTransferBean.setMerchantTransId("TEST1813T2004"+i);

			String jsonObj = new Gson().toJson(fundTransferBean);
//			String jsonObj = "{\"transid\":\"TEST1813T180\"}";
			PayoutEncryption payoutEncryption =null;
			try{
				
				payoutEncryption = PayoutEncryptionFactory.getEncryptionObject("AES128NEW");

				String encRequest = payoutEncryption.encryptOpenSSL(jsonObj, "G4hq1i17OyFjWUeCY8jzyw==");
				payoutEncryption.decryptOpenSSL("G4hq1i17OyFjWUeCY8jzyw==",encRequest);
				
				
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("aggregatorId", "BPA001000");
				jsonObject.put("request", encRequest);
				
				
//				String jsonRequest ="{\"aggregatorId\":\"BPA001000\",\"request\":\""+encRequest+"\"}";
//				
//				
//				String jsonRequest ="{\"aggregatorId\":\"BPA001000\",\"request\":\"fjKi6+3oZcwhZeTzozwVKdVucNR1uJ6wERGTyr1k07l7uA0hXNl22W+H+VgZqDlDk7UekptJ\\/cbwjQFAd36v8x2x055303SiW5ae1YpsJ0WeowFOy9WzxBuA3lI4NRPMKO+vaAJZoPVtRvEFzYADDZljhnkNCapeiPapmLcnlgDLj+UFyw+p4xqdaGjLSNfJqkvSMwnk1iTxJ9kownseDi\\/zIn6BOOZ42IEkzw+mFW8=\"}";
				
				PayoutAPI payoutaApi = new PayoutAPI();
				ExtRemittanceResponseBean rBean = payoutaApi.mrTransferRequest("10101001", "abcd", jsonObject.toString(), null);
				//ExtRemittanceResponseBean rBean = payoutaApi.getTransStatus(jsonRequest, null);
				System.out.println(rBean);
				System.out.println(rBean.getRequestId());
//				System.out.println(payoutEncryption.decryptOpenSSL("db868108-5b65-4477-ab79-618d724eb3b8",rBean.getResponse()));
				
				System.out.println(payoutEncryption.decryptOpenSSL("G4hq1i17OyFjWUeCY8jzyw==",rBean.getResponse()));

				
//				JSONObject json = new JSONObject();
//				json.put("transid", "TEST1");
//				
//				
//				String encRequest = payoutEncryption.encryptOpenSSL(json.toString(), "db868108-5b65-4477-ab79-618d724eb3b8");
//				payoutEncryption.decryptOpenSSL("db868108-5b65-4477-ab79-618d724eb3b8",encRequest);
//				
//				JSONObject jsonObject = new JSONObject();
//				jsonObject.put("aggregatorId", "BPA001000");
//				jsonObject.put("request", encRequest);
//				
//				
//				PayoutAPI payoutaApi = new PayoutAPI();
//				ExtRemittanceResponseBean rBean = payoutaApi.getTransStatus(jsonObject.toString(), null);
				System.out.println(rBean);
				System.out.println(rBean.getRequestId());
//				System.out.println(payoutEncryption.decryptOpenSSL("db868108-5b65-4477-ab79-618d724eb3b8",rBean.getResponse()));
				System.out.println(payoutEncryption.decryptOpenSSL("G4hq1i17OyFjWUeCY8jzyw==",rBean.getResponse()));
				

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		}
 
	}

	

