package com.bhartipay.payoutapi.cashfree.persistnace;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="payoutclientconfig")
public class PayoutClientConfig {
	
	@Id
	@Column(name="agentid")
	private String agentId;
	
	@Column(name="transfervia")
	private String transferVia;
	
	@Column(name="privatekey")
	private String privateKey;
	
	@Column(name="enctype")
	private String encType;
	
	@Column(name="status")
	private String status;
	
	@Column(name="transferapp")
	private String transferApp;
	
	@Column(name="clienttech")
	private String clientTech;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getTransferVia() {
		return transferVia;
	}

	public void setTransferVia(String transferVia) {
		this.transferVia = transferVia;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getEncType() {
		return encType;
	}

	public void setEncType(String encType) {
		this.encType = encType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferApp() {
		return transferApp;
	}

	public void setTransferApp(String transferApp) {
		this.transferApp = transferApp;
	}

	public String getClientTech() {
		return clientTech;
	}

	public void setClientTech(String clientTech) {
		this.clientTech = clientTech;
	}
	
}
