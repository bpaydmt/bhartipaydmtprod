package com.bhartipay.payoutapi.cashfree.persistnace;

import java.math.BigDecimal;

import com.cashfree.lib.constants.Constants.Environment;
import com.cashfree.lib.payout.clients.Beneficiary;
import com.cashfree.lib.payout.clients.Payouts;
import com.cashfree.lib.payout.clients.Transfers;
import com.cashfree.lib.payout.domains.BeneficiaryDetails;
import com.cashfree.lib.payout.domains.request.RequestTransferRequest;
import com.cashfree.lib.payout.domains.response.GetTransferResponse;
import com.cashfree.lib.payout.domains.response.RequestTransferResponse;

public class CashFreePayoutIntegration {
	
	// For uat
//	public static final String CLIENT_ID =  "CF31320D6PQ6NMZ7VA6IEQ";
//	public static final String CLIENT_SECRET = "828e5f81f7a53e7eb8f6e2acd5ebaee4d4697e14";
	
	// For prod
	public static final String CLIENT_ID =  "CF71536BLCR9RNLA91EAUY";
	public static final String CLIENT_SECRET = "80fd50352f0af22d198c14a8fe6eac82b2d43c91";
	
	Payouts payouts = null;
	public CashFreePayoutIntegration(){
		try {
		payouts = Payouts.getInstance(
			    Environment.PRODUCTION, CLIENT_ID, CLIENT_SECRET);
		boolean status = payouts.init();
		System.out.println("Authorisation status "+status);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public BeneficiaryDetails getBeneficiaryDetails(String beneficiaryId) {
		BeneficiaryDetails beneficiaryDetails = null;
		try {
		Beneficiary beneficiary = new Beneficiary(payouts);
		beneficiaryDetails = beneficiary.getBeneficiaryDetails(beneficiaryId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return beneficiaryDetails;
	}
	
	
	public String getBeneficiaryId(String accountNo, String ifscCode) {
		String beneficiaryId = null;
		try {
		Beneficiary beneficiary = new Beneficiary(payouts);
		beneficiaryId = beneficiary.getBeneficiaryId(accountNo, ifscCode);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return beneficiaryId;
	}
	
	
	
	public boolean addBeneficiary(String beneId, String name, String email, String mobile, String accountNo,
			String ifscCode, String address1, String city, String state, String pinCode) {
		
		boolean status = false;
		try {
		Beneficiary beneficiary = new Beneficiary(payouts);
		BeneficiaryDetails beneficiaryDetails = new BeneficiaryDetails()
		        .setBeneId(beneId)
		        .setName(name)
		        .setEmail(email)
		        .setPhone(mobile)
		        .setBankAccount(accountNo)
		        .setIfsc(ifscCode)
		        .setAddress1(address1)
		        .setCity(city)
		        .setState(state)
		        .setPincode(pinCode);

		status = beneficiary.addBeneficiary(beneficiaryDetails);
		System.out.println(status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public RequestTransferResponse fundTransfer(String beneficiaryId, String amount, String transactionId) {
		RequestTransferResponse response = null;
		try {
		Transfers transfer = new Transfers(payouts);

		RequestTransferRequest request = new RequestTransferRequest()
		        .setBeneId(beneficiaryId)
		        .setAmount(new BigDecimal(amount))
		        .setTransferId(transactionId);
		response = transfer.requestTransfer(request);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	public GetTransferResponse getTransferStatus(String referanceId, String transactionId) {
		GetTransferResponse response = null;
		try {
		Transfers transfer = new Transfers(payouts);
		response = transfer.getTransferStatus(referanceId, transactionId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	public static void main(String[] args) {
		CashFreePayoutIntegration cashFree = new CashFreePayoutIntegration();
		
		
//		boolean benStatus = cashFree.addBeneficiary("DEMO1236", "ambuj singh", "ambujsingh44@gmail.com", "9999215888", "026291800001191", "YESB0000262", "NOIDA", "GB NAGAR", "UP", "201305");
//		System.out.println("adding beneficiary status "+benStatus);
		
//		String id = cashFree.getBeneficiaryId("30000100006987", "BARB0GNOIDA");
//		System.out.println("Beneficiary ID "+id);
		
//		BeneficiaryDetails details = cashFree.getBeneficiaryDetails("DEMO123633");
//		System.out.println("Beneficiary ID "+details);
		
		RequestTransferResponse detail = cashFree.fundTransfer("DEMO1236","10", "Test128");
		System.out.println("transafer ID "+detail);
		

	}
	
}
