package com.bhartipay.payoutapi.persistnace;

import java.util.List;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.payoutapi.bean.FundTransferResponse;

public interface ExtremmitapiDao {

	public MudraSenderBank getSenderDtlbank(MudraSenderBank mudraSenderBank, String serverName, String requestId);
	public MudraSenderWallet getSenderDtlWallet(MudraSenderWallet mudraSenderwallet, String serverName, String requestId);
	public FundTransferResponse fundTransferBank(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName, String requestId);
	public FundTransferResponse fundTransferWallet(MudraMoneyTransactionBean mudraMoneyTransactionBean,String operationType,String serverName, String requestId) ;
	public List getBeneficiaryListWallet(List<MudraBeneficiaryWallet> beneList);
	public List getBeneficiaryListBank(List<MudraBeneficiaryBank> beneList);
	public String saveRemitApiRequest(String aggregatorId,String reqVal,String serverName,String apiName);
	public MudraSenderBank createSenderBankIfNotExt(MudraSenderBank mudraSenderBank, String serverName, String requestId);
	public MudraBeneficiaryBank createBeneBankIfNotExt(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId);
}
