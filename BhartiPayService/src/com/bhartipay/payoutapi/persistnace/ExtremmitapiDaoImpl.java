package com.bhartipay.payoutapi.persistnace;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cache.CacheFactory;
import com.bhartipay.cache.CacheModel;
import com.bhartipay.cache.TableCache;
import com.bhartipay.cache.TableCacheImpl;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.mudra.bank3.payoutapi.PayoutMaster;
import com.bhartipay.mudra.bean.FundTransactionSummaryBean;
import com.bhartipay.mudra.bean.MudraBeneficiaryBank;
import com.bhartipay.mudra.bean.MudraBeneficiaryWallet;
import com.bhartipay.mudra.bean.MudraMoneyTransactionBean;
import com.bhartipay.mudra.bean.MudraSenderBank;
import com.bhartipay.mudra.bean.MudraSenderWallet;
import com.bhartipay.mudra.bean.SurchargeBean;
import com.bhartipay.mudra.persistence.MudraDao;
import com.bhartipay.mudra.persistence.MudraDaoImpl;
import com.bhartipay.payoutapi.bean.PayoutApiLog;
import com.bhartipay.payoutapi.bean.FundTransferResponse;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.OTPGeneration;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.bean.AadharShilaRequestResponse;
import com.bhartipay.util.thirdParty.bean.IMPSIciciResponseBean;


public class ExtremmitapiDaoImpl implements ExtremmitapiDao {
	
	private static final Logger logger = Logger.getLogger(ExtremmitapiDaoImpl.class.getName());
	SessionFactory factory;

	//Transaction transaction = null;

	OTPGeneration oTPGeneration = new OTPGeneration();
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	MudraDaoBank mudraDao = new MudraDaoBankImpl();
	MudraDao mudraDaoWallet = new MudraDaoImpl();
	TransactionDao transactionDao = new TransactionDaoImpl();
	
	public MudraSenderBank getSenderDtlbank(MudraSenderBank mudraSenderBank, String serverName, String requestId) {

		logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Start excution ********************************* validateSender method**:"
				+ mudraSenderBank.getMobileNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		try {
			WalletMastBean walletMast = (WalletMastBean)session.get(WalletMastBean.class, mudraSenderBank.getAgentId());
			if (walletMast.getMobileno() == null || walletMast.getMobileno().isEmpty()
					|| walletMast.getMobileno().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", walletMast.getMobileno())).list();
			
			if (senderList.size() > 0) {
				mudraSenderBank = senderList.get(0);
				mudraSenderBank.setStatusCode("1000");
				mudraSenderBank.setStatusDesc("Y");
			} else{ 
				mudraSenderBank.setStatusCode("1000");
			mudraSenderBank.setStatusDesc("N");
			}

		} catch (Exception e) {
			logger.debug(serverName + "******RequestId******" + requestId
					+ "*******problem in validateSender=========================" + e.getMessage(), e);
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;
	}

	public MudraSenderWallet getSenderDtlWallet(MudraSenderWallet mudraSenderwallet, String serverName, String requestId) {

		logger.info(serverName + "*******RequestId******" + requestId
				+ "*******Start excution ********************************* validateSender method**:"
				+ mudraSenderwallet.getMobileNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		mudraSenderwallet.setStatusCode("1001");
		mudraSenderwallet.setStatusDesc("Error");
		String newOtp = null;
		try {

			if (mudraSenderwallet.getMobileNo() == null || mudraSenderwallet.getMobileNo().isEmpty()
					|| mudraSenderwallet.getMobileNo().length() != 10) {
				mudraSenderwallet.setStatusCode("1104");
				mudraSenderwallet.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderwallet;
			}

			List<MudraSenderWallet> senderList = session.createCriteria(MudraSenderWallet.class)
					.add(Restrictions.eq("mobileNo", mudraSenderwallet.getMobileNo()))
					.add(Restrictions.eq("aggreatorId", mudraSenderwallet.getAggreatorId())).list();
			if (senderList.size() > 0) {
				mudraSenderwallet = (MudraSenderWallet)senderList.get(0);
				mudraSenderwallet.setStatusCode("1000");
				mudraSenderwallet.setStatusDesc("Y");
				
				
				List<MudraBeneficiaryWallet> benfList = session.createCriteria(MudraBeneficiaryWallet.class)
						.add(Restrictions.eq("senderId", mudraSenderwallet.getId()))
						.add(Restrictions.ne("status", "Deleted"))
						.addOrder(Order.asc("status")).list();
				
				mudraSenderwallet.setBeneficiaryList(benfList);
				
				
			} else{ 
				mudraSenderwallet.setStatusCode("1000");
				mudraSenderwallet.setStatusDesc("N");
			}

		} catch (Exception e) {
			logger.debug(serverName + "******RequestId******" + requestId
					+ "*******problem in validateSender=========================" + e.getMessage(), e);
			mudraSenderwallet.setStatusCode("7000");
			mudraSenderwallet.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderwallet;
	}
	
	
	public FundTransferResponse fundTransferBank(MudraMoneyTransactionBean mudraMoneyTransactionBean,String serverName, String requestId) 
	{
		
		Session session=null;
		 
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName);
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");
		double refundAgt = 0.0;
		boolean smsFlag = false;

		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "|getBeneficiaryId:"
					+ mudraMoneyTransactionBean.getBeneficiaryId()
					+ "|getNarrartion:"
					+ mudraMoneyTransactionBean.getNarrartion()
					+ "getTxnAmount :"
					+ mudraMoneyTransactionBean.getTxnAmount());
			

			/*
			 * TreeMap<String, String> treeMap = new TreeMap<String, String>();
			 * treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			 * treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			 * treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			 * treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());
			 * 
			 * int statusCode = WalletSecurityUtility.checkSecurity(treeMap); if (statusCode
			 * != 1000) { fundTransactionSummaryBean.setStatusCode("" + statusCode);
			 * fundTransactionSummaryBean.setStatusDesc("Security error."); return
			 * fundTransactionSummaryBean; }
			 */

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null
					|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null
					|| mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Wallet Id can't be empty.");
				return fundTransactionSummaryBean;
			}

			
			/**  this is use for checking double transaction.  **/
//			CommanUtilDaoImpl commanUtilDaoImpl=new CommanUtilDaoImpl();
//			String insertFlag=commanUtilDaoImpl.insertTransDtl(mudraMoneyTransactionBean.getUserId()+"|"+mudraMoneyTransactionBean.getSenderId()+"|"+mudraMoneyTransactionBean.getBeneficiaryId()+"|"+mudraMoneyTransactionBean.getTxnAmount(),mudraMoneyTransactionBean.getNarrartion());
//					if(!insertFlag.equalsIgnoreCase("SUCCESS"))
//					{
//						try{
//							fundTransactionSummaryBean.setStatusCode("7111");
//							if(!insertFlag.equalsIgnoreCase("FAIL"))
//							fundTransactionSummaryBean.setStatusDesc(insertFlag);
//							else
//							fundTransactionSummaryBean.setStatusDesc("You have already processed transaction with same details please wait for 10 minutes.");
//							}catch(Exception e){
//								e.printStackTrace();
//							}
//						
//						return fundTransactionSummaryBean;
//					}
			
					//String deleteBene = new com.appnit.bhartipay.wallet.user.service.impl.UserServiceImpl().deleteTransDtl(mudraMoney.getUserId()+"|"+mudraMoney.getSenderId()+"|"+mudraMoney.getBeneficiaryId()+"|"+mudraMoney.getTxnAmount());

			
			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double amtTxn=mudraMoneyTransactionBean.getTxnAmount();
			double agnWalletTxnAmount = 0;
			double surChargeAmount = 0;
			double wlSurcharge = 0;//while label Aggregator Surcharge
			double surChargeDist = 0;
			double supDistSurcharge=0;//super distributor surcharge
			double agentRefund=0;
			double aggReturnVal=0.0;
			double dtdsApply=0.0;
			String planId="N/A";
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn = null;
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					mudraMoneyTransactionBean.getUserId());
			WalletMastBean walletmastAgg = (WalletMastBean) session.get(WalletMastBean.class,
					walletMastBean.getAggreatorid());
			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraMoneyTransactionBean.getSenderId());
			MudraBeneficiaryBank mudraBeneficiaryBank = (MudraBeneficiaryBank) session.get(MudraBeneficiaryBank.class,
					mudraMoneyTransactionBean.getBeneficiaryId());
			WalletMastBean superDistributorMast=new WalletMastBean();
			if(walletMastBean!=null&&walletMastBean.getSuperdistributerid()!=null&&!walletMastBean.getSuperdistributerid().isEmpty()&&!walletMastBean.getSuperdistributerid().equalsIgnoreCase("-1")){
			 superDistributorMast=(WalletMastBean)session.get(WalletMastBean.class,	walletMastBean.getSuperdistributerid());
			}
			 
			// logic needs to be changed for verifing beneficiary and setting V in below try and catch inside if
			if(walletMastBean.getUsertype() != 2)
			{
				fundTransactionSummaryBean.setStatusCode("7000");
				fundTransactionSummaryBean.setStatusDesc("Invalid User.");
				return fundTransactionSummaryBean;	
			}
			
			if (mudraBeneficiaryBank != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim().equals(mudraBeneficiaryBank.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			// monthly Transfer Limit validation
			double transferLimit = 20000000.0;
//			AadharShilaDMTResponse dmtResponse=aadharShilaDMT.fetchSender(mudraSenderBank.getMobileNo(),mudraSenderBank.getAgentId());
//			if(dmtResponse!=null&&"SUCCESS".equalsIgnoreCase(dmtResponse.getErrorMsg())&& "00".equalsIgnoreCase(dmtResponse.getErrorCode())) {
//			//mudraSenderBank.setTransferLimit(dmtResponse.getData().getWalletbal());
//			transferLimit=dmtResponse.getData().getWalletbal();
//			}
			
//			if (transferLimit < mudraMoneyTransactionBean.getTxnAmount()) {
//				fundTransactionSummaryBean.setStatusCode("1122");
//				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
//				return fundTransactionSummaryBean;
//			}

//			fundTransactionSummaryBean.setBankName(mudraBeneficiaryBank.getBankName());
//			fundTransactionSummaryBean.setAgentName(walletMastBean.getName());
//			fundTransactionSummaryBean.setShopName(walletMastBean.getShopName());
//			fundTransactionSummaryBean.setAgentMobileNo(walletMastBean.getMobileno());
			/*// Yearly Transfer Limit validation
			if (mudraSenderBank.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderBank.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}*/

			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {

				    if (mudraMoneyTransactionBean.getTransType().equalsIgnoreCase("IMPS"))
					   ppiTxnIdfundTransfer = commanUtilDao.getTrxId("FBTX",walletMastBean.getAggreatorid());
					else 
					  ppiTxnIdfundTransfer = commanUtilDao.getTrxId("FBTXNEFT",walletMastBean.getAggreatorid());

			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();

				
				if (mudraMoneyTransactionBean != null && mudraMoneyTransactionBean.getAccHolderName() != null
						&& !mudraMoneyTransactionBean.getAccHolderName().isEmpty()) {
					try {
						System.out.println(mudraMoneyTransactionBean.getAccHolderName());
						mudraBeneficiaryBank.setVerified("V");
						mudraBeneficiaryBank.setVerifiedStatus("V");
						mudraBeneficiaryBank.setVerifiedon(new java.sql.Date(System.currentTimeMillis()));
						mudraBeneficiaryBank.setName(mudraMoneyTransactionBean.getAccHolderName());
						session.update(mudraBeneficiaryBank);
					} catch (Exception e) {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), "************************************* problem in update bene details "
								+ mudraBeneficiaryBank.getName());
						
					}
				}
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() 
					+ "fundTransfer start with fundtxnid:" + ppiTxnIdfundTransfer);
			
			String walletTxnCode = "1000";
			String walletSurChargeTxnCode = "1000";
			String walletAggTxnCode = "1000";
			String walletSuperDistributorTxnCode="1000";
			
			if (mudraSenderBank.getWalletBalance() - mudraMoneyTransactionBean.getTxnAmount() < 0) {
				agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount() - mudraSenderBank.getWalletBalance();
				refundAgt = agnWalletTxnAmount;
				surChargeAmount = 0;
				SurchargeBean surchargeBean = new SurchargeBean();
				// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(agnWalletTxnAmount);
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					surchargeBean.setTransType("PAYOUT");
				} else {
					surchargeBean.setTransType("PAYOUT");
				}
				surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

				if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
					surChargeAmount = surchargeBean.getSurchargeAmount();
					surChargeDist = surchargeBean.getDistSurCharge();
					wlSurcharge = surchargeBean.getCreditSurcharge();
					supDistSurcharge = surchargeBean.getSuDistSurcharge();
					agentRefund = surchargeBean.getAgentRefund();
					aggReturnVal = surchargeBean.getAggReturnVal();
					dtdsApply = surchargeBean.getDtdsApply();
				} else {
					fundTransactionSummaryBean.setStatusCode("1122");
					fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return fundTransactionSummaryBean;
				}

				if ((surChargeAmount + agnWalletTxnAmount + surChargeDist+supDistSurcharge) > new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid())) {
					fundTransactionSummaryBean.setStatusCode("7024");
					fundTransactionSummaryBean.setStatusDesc("Insufficient Agent Wallet Balance.");
					return fundTransactionSummaryBean;
				}

				if (walletmastAgg.getWhiteLabel().equalsIgnoreCase("1") &&(amtTxn +wlSurcharge) > new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())) {
					fundTransactionSummaryBean.setStatusCode("7024");
					fundTransactionSummaryBean.setStatusDesc("Something went wrong please try again later.");
					return fundTransactionSummaryBean;
				} else if (walletmastAgg.getWhiteLabel().equalsIgnoreCase("1")) {
					double countSur = 1;
					DecimalFormat df = new DecimalFormat("###");
					double modCharge= amtTxn % 20000000;
					double lastCreditSur = 0;
					if (amtTxn > 20000000.00) {
						if (modCharge == 0)
						{
							countSur = Integer.parseInt(df.format((amtTxn / 20000000)));
						}
						else
						{
							SurchargeBean surchargeBeanlast = new SurchargeBean();
							// surchargeBean.setAgentId(mudraSenderBank.getAgentId());
							surchargeBeanlast.setAgentId(mudraMoneyTransactionBean.getUserId());
							surchargeBeanlast.setAmount(modCharge);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								surchargeBeanlast.setTransType("PAYOUT");
							} else {
								surchargeBeanlast.setTransType("PAYOUT");
							}
							surchargeBeanlast = calculateSurcharge(surchargeBeanlast, serverName, requestId);

							if (surchargeBeanlast.getStatus().equalsIgnoreCase("1000")) {
								lastCreditSur=surchargeBeanlast.getCreditSurcharge();
							} else {
								fundTransactionSummaryBean.setStatusCode("1122");
								fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
								return fundTransactionSummaryBean;
							}
							countSur = Integer.parseInt(df.format(((amtTxn-modCharge) / 20000000)));
							
						}
					}
					
					
					
					
					walletAggTxnCode = transactionDao.walletToAggregatorTransfer(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),
							( amtTxn ),
							wlSurcharge, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),  aggReturnVal, dtdsApply );

				}
				

				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" 
						+ "fundTransfer :" + ppiTxnIdfundTransfer);


				planId=getPlanIdByAgentId(walletMastBean.getId());
				
				if (walletAggTxnCode.equalsIgnoreCase("1000")) {
					walletTxnCode = new TransactionDaoImpl().walletToAccount(walletMastBean.getId(),
							walletMastBean.getWalletid(), agnWalletTxnAmount, mudraBeneficiaryBank.getAccountNo(),
							ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(),
							walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(), 0.0);
				}
				if (walletTxnCode.equalsIgnoreCase("1000") && walletAggTxnCode.equalsIgnoreCase("1000")) {
					walletSurChargeTxnCode = new TransactionDaoImpl().walletToAccountSurchargeDist(
							walletMastBean.getId(), walletMastBean.getWalletid(), (surChargeAmount + surChargeDist+supDistSurcharge),
							mudraBeneficiaryBank.getAccountNo(), ppiTxnIdfundTransfer,
							mudraMoneyTransactionBean.getIpiemi(), walletMastBean.getAggreatorid(),
							mudraMoneyTransactionBean.getAgent(), 0.0, surChargeDist,
							walletMastBean.getDistributerid(),supDistSurcharge,(superDistributorMast.getCashDepositeWallet() == null ? walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund,dtdsApply);
				}
				
				
					if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")
							&& walletAggTxnCode.equalsIgnoreCase("1000")) {
						

						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
								.clone();

						mudraMoneyTransactionBeanSaveTxn.setIsBank(3);
						mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
						mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
						mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
						mudraMoneyTransactionBeanSaveTxn.setPlanId(planId);
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();
					} else {
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
								+ mudraMoneyTransactionBean.getAgentid() + "****"
								
								+ "velocity error :"
								+ walletTxnCode);

						
						transaction = session.beginTransaction();
						mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
								.clone();

						mudraMoneyTransactionBeanSaveTxn.setIsBank(3);
						mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
						mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
						mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
						mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
						mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
						mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
						mudraMoneyTransactionBeanSaveTxn.setPlanId(planId);
						session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
						transaction.commit();

					}

			}

			if (walletTxnCode.equalsIgnoreCase("1000") && walletSurChargeTxnCode.equalsIgnoreCase("1000")
					&& walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				int i = 1;
				do {
					String responseCode = null;
					String actCode=null;
					MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean
							.clone();
					fundSaveTxn.setIsBank(3);
					fundSaveTxn.setPlanId(planId);
					transaction = session.beginTransaction();
					fundSaveTxn.setId("AS" + i + ppiTxnIdfundTransfer);
					fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
					if (txnAmount > 20000000) {
						fundSaveTxn.setDrAmount(20000000);
						txnAmount = txnAmount - 20000000;

					} else {
						fundSaveTxn.setDrAmount(txnAmount);
						txnAmount = 0;
					}
					if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
						fundSaveTxn.setStatus("NEFT INITIATED");
					} else {
						fundSaveTxn.setStatus("PENDING");
					}
					session.saveOrUpdate(fundSaveTxn);
					transaction.commit();
					
					transaction = session.beginTransaction();
					List<String> respList=new ArrayList<>();
					if(!(walletMastBean.getNEFTBatch()==1 && mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT"))){
						respList=bankTransfer(mudraMoneyTransactionBean.getNarrartion(), fundSaveTxn.getId(), fundSaveTxn.getDrAmount(), mudraSenderBank.getAgentId(), mudraBeneficiaryBank);
					}
					else{
						respList.add("0");
						respList.add("S");
						respList.add("null");
					}
					
					
					
					responseCode = respList.get(0);
					actCode = respList.get(1);
						transaction = session.beginTransaction();
					
					 fundSaveTxn.setUserNarration("from service");
					
					if (!responseCode.equals("0")|| !(actCode.equals("0") ||actCode.equalsIgnoreCase("S")|| actCode.equals("26") || actCode.equals("11"))) {
						
						
					/*******************start Dynamic bank blocking **************************/	
					if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					try{	
					TableCache tableCache=CacheFactory.getCache();
					CacheModel model=tableCache.getCache(mudraBeneficiaryBank.getBankName());
					if(model==null){
						model=new CacheModel();
						model.setLastAccess(System.currentTimeMillis());
						model.setCount(1);
					}
					else
					{
					 int count=model.getCount();
					 if(count>=TableCacheImpl.TXN_FAILED_COUNT){
						Session sessionForCache=factory.openSession(); 
						Transaction transactionForCache=sessionForCache.beginTransaction();
						Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,3)");
						query.setParameter("bankName",mudraBeneficiaryBank.getBankName());
						query.executeUpdate();
						transactionForCache.commit();
					 }
					 count++;
					 model.setLastAccess(System.currentTimeMillis());
					 model.setCount(count);
					}
					tableCache.putCache(mudraBeneficiaryBank.getBankName(), model);
					}catch(Exception exception){
						exception.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
								+ mudraMoneyTransactionBean.getAgentid() 
								);
						
					}
					}
					/*******************end Dynamic bank blocking **************************/	
						
					if(responseCode.equals("1")){
							fundSaveTxn.setStatus("FAILED");
							fundSaveTxn.setRemark("Transaction FAILED");
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("FAILED");
						}
						 else{
								refundAgt = refundAgt - fundSaveTxn.getDrAmount();
								fundSaveTxn.setStatus("Confirmation Awaited");
								fundSaveTxn.setRemark("Transaction Pending");
								if(!respList.get(2).equals("null"))
									fundSaveTxn.setBankRrn(respList.get(2));
								fundSaveTxn.setBankResp("Confirmation Awaited");
							} 
	

						if (refundAgt > 0 && false) {
								
							smsFlag = true;
							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();

							mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setIsBank(3);
							mudraMoneyTransactionBeanSaveTxnRef.setAgentid(mudraMoneyTransactionBean.getAgentid());
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef
									.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(refundAgt);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("NEFT");
							} else {
								mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("IMPS");
							}
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("REFUNDED");
							mudraMoneyTransactionBeanSaveTxnRef.setPlanId(planId);
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To AGENT)");
							session.save(mudraMoneyTransactionBeanSaveTxnRef);

							SurchargeBean surchargeBean = new SurchargeBean();
							surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
							surchargeBean.setAmount(refundAgt);
							if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
								surchargeBean.setTransType("PAYOUT");
							} else {
								surchargeBean.setTransType("PAYOUT");
							}
							surchargeBean = calculateSurcharge(surchargeBean, serverName, requestId);

							if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
								surChargeAmount = surchargeBean.getSurchargeAmount();
								surChargeDist = surchargeBean.getDistSurCharge();
								supDistSurcharge=surchargeBean.getSuDistSurcharge();
								agentRefund=surchargeBean.getAgentRefund();
								aggReturnVal=surchargeBean.getAggReturnVal();
								dtdsApply=surchargeBean.getDtdsApply();
								wlSurcharge=surchargeBean.getCreditSurcharge();
							} else {
								fundTransactionSummaryBean.setStatusCode("1122");
								fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
								return fundTransactionSummaryBean;
							}

							String dmtSettlementCode = new TransactionDaoImpl().dmtSettlement(walletMastBean.getId(),
									walletMastBean.getWalletid(), /* agnWalletTxnAmount */refundAgt,
									ppiTxnIdfundTransfer, mudraMoneyTransactionBean.getIpiemi(), "",
									walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent());
							String surchargeRefundSettlementCode = new TransactionDaoImpl().surchargeRefundSettlement(
									walletMastBean.getId(), walletMastBean.getWalletid(),
									(surChargeAmount + surChargeDist+supDistSurcharge), ppiTxnIdfundTransfer,
									mudraMoneyTransactionBean.getIpiemi(), "", walletMastBean.getAggreatorid(),
									mudraMoneyTransactionBean.getAgent(), surChargeDist,
									walletMastBean.getDistributerid(),supDistSurcharge,(superDistributorMast.getCashDepositeWallet() == null ?walletMastBean.getSuperdistributerid():superDistributorMast.getCashDepositeWallet()),agentRefund,dtdsApply);
							if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
							{
								
								double countSur = 1;
								DecimalFormat df = new DecimalFormat("###");
								
								
								double modCharge= refundAgt % 20000000;
								double lastCreditSur = 0;
								if (refundAgt > 20000000.00) {
									if (modCharge == 0)
									{
										countSur = Integer.parseInt(df.format((refundAgt / 20000000)));
									}
									else
									{
										SurchargeBean surchargeBeanlast = new SurchargeBean();
										surchargeBeanlast.setAgentId(mudraMoneyTransactionBean.getUserId());
										surchargeBeanlast.setAmount(modCharge);
										if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
											surchargeBeanlast.setTransType("PAYOUT");
										} else {
											surchargeBeanlast.setTransType("PAYOUT");
										}
										surchargeBeanlast = calculateSurcharge(surchargeBeanlast, serverName, requestId);

										if (surchargeBeanlast.getStatus().equalsIgnoreCase("1000")) {
											lastCreditSur=surchargeBeanlast.getCreditSurcharge();
									
										} else {
											fundTransactionSummaryBean.setStatusCode("1122");
											fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
											return fundTransactionSummaryBean;
										}
										countSur = Integer.parseInt(df.format(((refundAgt-modCharge) / 20000000)));
										
									}
								}
								
								String AggRefundCode =  transactionDao.walletToAggregatorRefund(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),refundAgt,wlSurcharge,mudraMoneyTransactionBean.getIpiemi(),walletMastBean.getAggreatorid(), mudraMoneyTransactionBean.getAgent(),aggReturnVal,dtdsApply);
							}
							
						}
						

						txnAmount = 0;

					} else {
//dynamic routing.
						i++;
						refundAgt = refundAgt - fundSaveTxn.getDrAmount();
						
						if (actCode.equals("0") || actCode.equalsIgnoreCase("S")
								|| actCode.equals("26") || actCode.equals("11")) {
							if(mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")){
								fundSaveTxn.setStatus("NEFT INITIATED");
								fundSaveTxn.setRemark("NEFT INITIATED");
							}else if(actCode.equals("0")){
								fundSaveTxn.setStatus("SUCCESS");
								fundSaveTxn.setRemark("Transaction SUCCESSFUL");
							}else {
								fundSaveTxn.setStatus("PENDING");
								fundSaveTxn.setRemark("Transaction SUCCESSFUL");
							}
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("SUCCESS");
							
							/*******************start Dynamic bank blocking **************************/	
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
							try{	
							TableCache tableCache=CacheFactory.getCache();
							tableCache.removeCache(mudraBeneficiaryBank.getBankName());
							
							}catch(Exception exception){
								exception.printStackTrace();
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
										+ mudraMoneyTransactionBean.getAgentid() 
										);
								
							}
							}
							/*******************end Dynamic bank blocking **************************/	
							
							
						} else {
							smsFlag = true;
							fundSaveTxn.setStatus("Confirmation Awaited");
							fundSaveTxn.setRemark("Transaction Pending");
							if(!respList.get(2).equals("null"))
								fundSaveTxn.setBankRrn(respList.get(2));
							fundSaveTxn.setBankResp("Confirmation Awaited");
							
							/*******************start Dynamic bank blocking **************************/	
							if (!mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) 
							{
								try{	
								TableCache tableCache=CacheFactory.getCache();
								CacheModel model=tableCache.getCache(mudraBeneficiaryBank.getBankName());
								if(model==null)
								{
									model=new CacheModel();
									model.setLastAccess(System.currentTimeMillis());
									model.setCount(1);
								}
								else
								{
								 int count=model.getCount();
								 if(count>=TableCacheImpl.TXN_FAILED_COUNT)
								 {
									Session sessionForCache=factory.openSession(); 
									Transaction transactionForCache=sessionForCache.beginTransaction();
									Query query=sessionForCache.createSQLQuery("insert into blockbank(bankname,isbank) values(:bankName,3)");
									query.setParameter("bankName",mudraBeneficiaryBank.getBankName());
									query.executeUpdate();
									transactionForCache.commit();
								 }
								 count++;
								 model.setLastAccess(System.currentTimeMillis());
								 model.setCount(count);
								}
								tableCache.putCache(mudraBeneficiaryBank.getBankName(), model);
								}catch(Exception exception){
									exception.printStackTrace();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(),mudraBeneficiaryBank.getBankName(),"|" + requestId + "| problem in adding bank in cache. "
											+ mudraMoneyTransactionBean.getAgentid() 
											);
									
								}
							}
							/*******************end Dynamic bank blocking **************************/	
						}
					}
					session.update(fundSaveTxn);
					transaction.commit();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "|" + requestId + "|"
							+ mudraMoneyTransactionBean.getAgentid());
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
							+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
							+ "completed insert into dmtdetailsmast");
					

				} while (txnAmount != 0);

				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderBank.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryBank.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryBank.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryBank.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("As applicable");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());
				if ("NV".equalsIgnoreCase(mudraBeneficiaryBank.getVerifiedStatus())) {
					try {
						Transaction tx = session.beginTransaction();
						mudraBeneficiaryBank.setVerifiedStatus("V");
						session.saveOrUpdate(mudraBeneficiaryBank);
						tx.commit();
					} catch (Exception e) {

					}
				}
				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));


				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				fundTransactionSummaryBean
						.setAgentWalletAmount(new TransactionDaoImpl().getWalletBalance(walletMastBean.getWalletid()));
				MudraSenderBank mudraSenderNew = (MudraSenderBank) session.get(MudraSenderBank.class,
						mudraSenderBank.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());

//				String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer", mudraSenderBank.getAggreatorId());
//				if(smstemplet!=null && ! smsFlag) {
//					smstemplet = smstemplet.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getVerificationAmount())
//							.replaceAll("<<ACCNO>>", mudraBeneficiaryBank.getAccountNo())
//							.replaceAll("<<IFSCCODE>>", " "+getDate("yyyy/MM/dd HH:mm:ss"));
//						
//				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(), serverName + "****RequestId****" + requestId + "**********"
//						+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
//						+ "getMobileNo()``"
//						+ smstemplet);
//				
//				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderBank.getMobileNo(),
//						smstemplet,
//						"SMS", mudraSenderBank.getAggreatorId(), "", mudraSenderBank.getFirstName(), "NOTP");

				// ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				
				//Dear <<NAME>> you have made payment of Rs.<<AMOUNT>> to <<BENE NAME>> on mobile no. <<MOBILE>>. Transaction ID is <<TXNNO>>.
//				String sendMoneySmsTemplet = commanUtilDao.getsmsTemplet("sendMoney", mudraSenderBank.getAggreatorId());
//				sendMoneySmsTemplet = sendMoneySmsTemplet.replaceAll("<<NAME>>", walletMastBean.getId())
//						.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
//						.replaceAll("<<BENE NAME>>", mudraBeneficiaryBank.getName())
//						.replaceAll("<<MOBILE>>", mudraSenderBank.getMobileNo())
//						.replaceAll("<<TXNNO>>", ppiTxnIdfundTransfer);
//				
//				SmsAndMailUtility smsOnSendMoney = new SmsAndMailUtility("", "", "",walletMastBean.getMobileno(),
//						sendMoneySmsTemplet,
//						"SMS", mudraSenderBank.getAggreatorId(), "", walletMastBean.getName(), "NOTP");
				
				// ThreadUtil.getThreadPool().execute(smsOnSendMoney);
//				
//				}

			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "****RequestId****" + requestId + "**********"
						+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
						+ "velocity error :" + walletTxnCode);
			
				if(walletTxnCode.equalsIgnoreCase("1000"))
				{
					if(walletSurChargeTxnCode.equalsIgnoreCase("1000"))
					{
						if(!walletAggTxnCode.equalsIgnoreCase("1000"))
						{
							fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
						}
						else
						{
							fundTransactionSummaryBean.setStatusCode("1001");
						}
					}
					else
					{
						fundTransactionSummaryBean.setStatusCode(walletSurChargeTxnCode);
					}
						
				}
				else
				{
					fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				}
				//fundTransactionSummaryBean.setStatusCode(walletTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",mudraMoneyTransactionBean.getWalletId(), mudraMoneyTransactionBean.getUserId(), mudraMoneyTransactionBean.getSenderId(), mudraMoneyTransactionBean.getTxnId(),serverName + "****RequestId****" + requestId + "**********"
					+ mudraMoneyTransactionBean.getAgentid() + "****" + mudraMoneyTransactionBean.getSenderId()
					+ "problem in fundTransfer" + e.getMessage()+" "+e);
			
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(transaction.isActive())
				transaction.commit();
			session.close();
		}

		return fundTransactionSummaryBean;

	
	}


	public FundTransferResponse fundTransferWallet(MudraMoneyTransactionBean mudraMoneyTransactionBean,String operationType,String serverName, String requestId) {
		logger.info(serverName + "****RequestId****" + requestId+ "*Start excution *** fundTransfer method***:");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		FundTransferResponse fundTransactionSummaryBean = new FundTransferResponse();
		fundTransactionSummaryBean.setStatusCode("1001");
		fundTransactionSummaryBean.setStatusDesc("Error");

		try {
			logger.info(serverName + "**RequestId*" + requestId + "**** fundTransferWallet method**:"+ mudraMoneyTransactionBean.getSenderId());
			logger.info(serverName + "****RequestId****" + requestId + "**getBeneficiaryId()*** fundTransferWallet method****:"+ mudraMoneyTransactionBean.getBeneficiaryId());
			logger.info(serverName + "****RequestId****" + requestId + "** fundTransferWallet method*"+ mudraMoneyTransactionBean.getNarrartion());
			logger.info(serverName + "****RequestId****" + requestId + "**getTxnAmount()*** fundTransferWallet method****:"+ mudraMoneyTransactionBean.getTxnAmount());
			logger.info(serverName + "****RequestId****" + requestId + "** fundTransferWallet method***"+ mudraMoneyTransactionBean.getWalletId());

			TreeMap<String, String> treeMap = new TreeMap<String, String>();
			treeMap.put("txnAmount", "" + mudraMoneyTransactionBean.getTxnAmount());
			treeMap.put("userId", mudraMoneyTransactionBean.getUserId());
			treeMap.put("beneficiaryId", mudraMoneyTransactionBean.getBeneficiaryId());
			treeMap.put("CHECKSUMHASH", mudraMoneyTransactionBean.getCHECKSUMHASH());

			int statusCode = WalletSecurityUtility.checkSecurity(treeMap);
			if (statusCode != 1000) {
				fundTransactionSummaryBean.setStatusCode("" + statusCode);
				fundTransactionSummaryBean.setStatusDesc("Security error.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getSenderId() == null || mudraMoneyTransactionBean.getSenderId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1110");
				fundTransactionSummaryBean.setStatusDesc("Invalid Sender Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getBeneficiaryId() == null|| mudraMoneyTransactionBean.getBeneficiaryId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getNarrartion() == null || mudraMoneyTransactionBean.getNarrartion().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1111");
				fundTransactionSummaryBean.setStatusDesc("Invalid Transfer Type.");
				return fundTransactionSummaryBean;
			}
			if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("IMPS")) {
				fundTransactionSummaryBean.setStatusCode("1170");
				fundTransactionSummaryBean.setStatusDesc("IMPS transfer channel not open. ");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnAmount() <= 0) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Amount can't be zero.");
				return fundTransactionSummaryBean;
			}
			if (mudraMoneyTransactionBean.getWalletId() == null || mudraMoneyTransactionBean.getWalletId().isEmpty()) {
				fundTransactionSummaryBean.setStatusCode("1120");
				fundTransactionSummaryBean.setStatusDesc("Wallet Id can't be empty.");
				return fundTransactionSummaryBean;
			}

			transaction = session.beginTransaction();
			String ppiTxnIdfundTransfer;
			double agnWalletTxnAmount = 0;
			double surChargeAmount = 0;
			String walletAggTxnCode="1000";
			MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxn=null;
			
			MudraSenderWallet mudraSenderWallet = (MudraSenderWallet) session.get(MudraSenderWallet.class,mudraMoneyTransactionBean.getSenderId());
			WalletMastBean walletmastAgg = (WalletMastBean)session.get(WalletMastBean.class, mudraSenderWallet.getAggreatorId());
			MudraBeneficiaryWallet mudraBeneficiaryMastBean = (MudraBeneficiaryWallet) session.get(MudraBeneficiaryWallet.class, mudraMoneyTransactionBean.getBeneficiaryId());
			
			if (mudraBeneficiaryMastBean != null) {
				if (!mudraMoneyTransactionBean.getSenderId().trim()
						.equals(mudraBeneficiaryMastBean.getSenderId().trim())) {
					fundTransactionSummaryBean.setStatusCode("1130");
					fundTransactionSummaryBean.setStatusDesc("Beneficiary does not belong to mentioned Sender ID");
					return fundTransactionSummaryBean;
				}
			} else {
				fundTransactionSummaryBean.setStatusCode("1116");
				fundTransactionSummaryBean.setStatusDesc("Invalid Beneficiary Id.");
				return fundTransactionSummaryBean;
			}
			if (mudraSenderWallet.getTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()) {
				fundTransactionSummaryBean.setStatusCode("1122");
				fundTransactionSummaryBean.setStatusDesc("Your Monthly Transfer Limit exceeded.");
				return fundTransactionSummaryBean;
			}
			if (mudraSenderWallet.getYearlyTransferLimit() < mudraMoneyTransactionBean.getTxnAmount()
					&& mudraSenderWallet.getIsPanValidate() == 0) {
				fundTransactionSummaryBean.setStatusCode("1131");
				fundTransactionSummaryBean
						.setStatusDesc("Your Yearly Transfer Limit exceeded.Please Upload Sender PAN /Form 60");
				return fundTransactionSummaryBean;
			}

			if (mudraMoneyTransactionBean.getTxnId() == null || mudraMoneyTransactionBean.getTxnId().isEmpty()) {
				ppiTxnIdfundTransfer = commanUtilDao.getTrxId("MMTXN",mudraMoneyTransactionBean.getAggreatorid());
			} else {
				ppiTxnIdfundTransfer = mudraMoneyTransactionBean.getTxnId();
			}
			logger.info(serverName + "****RequestId****" + requestId + "*****fundTransferWallet start with fundtxnid*******:" + ppiTxnIdfundTransfer);
				agnWalletTxnAmount = mudraMoneyTransactionBean.getTxnAmount() ;
				surChargeAmount = 0;
				SurchargeBean surchargeBean = new SurchargeBean();
				surchargeBean.setAgentId(mudraMoneyTransactionBean.getUserId());
				surchargeBean.setAmount(agnWalletTxnAmount);
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					surchargeBean.setTransType("NEFT");
				} else {
					surchargeBean.setTransType("IMPS");
				}
				surchargeBean = mudraDao.calculateSurcharge(surchargeBean, serverName, requestId);

				if (surchargeBean.getStatus().equalsIgnoreCase("1000")) {
					surChargeAmount = surchargeBean.getSurchargeAmount();
				} else {
					fundTransactionSummaryBean.setStatusCode("1122");
					fundTransactionSummaryBean.setStatusDesc("Surcharge not define.Please contact admin.");
					return fundTransactionSummaryBean;
				}

			
				if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1")&&(surchargeBean.getCreditSurcharge()+agnWalletTxnAmount)>new TransactionDaoImpl().getWalletBalancebyId(walletmastAgg.getCashDepositeWallet())){
				     fundTransactionSummaryBean.setStatusCode("7024");
				     fundTransactionSummaryBean.setStatusDesc("Insufficient Aggregator Wallet Balance.");
				     return fundTransactionSummaryBean;
				}
				else if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
				{
					
					walletAggTxnCode = transactionDao.walletToAggregatorTransfer(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),(agnWalletTxnAmount),surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),mudraSenderWallet.getAggreatorId(),mudraMoneyTransactionBean.getAgent(),0,0);
			
				}
				
				logger.info(serverName + "****RequestId****" + requestId + "*** method****:" + ppiTxnIdfundTransfer);

				transaction = session.beginTransaction();
				mudraMoneyTransactionBeanSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				mudraMoneyTransactionBeanSaveTxn.setId("T" + ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				mudraMoneyTransactionBeanSaveTxn.setNarrartion("WALLET LOADING");
				mudraMoneyTransactionBeanSaveTxn.setCrAmount(agnWalletTxnAmount);
				mudraMoneyTransactionBeanSaveTxn.setDrAmount(0.0);
				mudraMoneyTransactionBeanSaveTxn.setStatus("PENDING");
				if (walletAggTxnCode.equalsIgnoreCase("1000")) {
					mudraMoneyTransactionBeanSaveTxn.setStatus("SUCCESS");
					mudraMoneyTransactionBeanSaveTxn.setRemark("SUCCESS");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
				} else {
					logger.info(serverName + "****RequestId****" + requestId + "***********velocity error******* findtransferwallet method****:"+ walletAggTxnCode);
					mudraMoneyTransactionBeanSaveTxn.setStatus("FAILED");
					mudraMoneyTransactionBeanSaveTxn.setRemark("FAILED");
					session.saveOrUpdate(mudraMoneyTransactionBeanSaveTxn);
					transaction.commit();
				}

			if ( walletAggTxnCode.equalsIgnoreCase("1000")) {
				double txnAmount = mudraMoneyTransactionBean.getTxnAmount();
				String beneName = new String();
				MudraMoneyTransactionBean fundSaveTxn = (MudraMoneyTransactionBean) mudraMoneyTransactionBean.clone();
				transaction = session.beginTransaction();
				fundSaveTxn.setId("TB"+ ppiTxnIdfundTransfer);
				fundSaveTxn.setTxnId(ppiTxnIdfundTransfer);
				
				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					fundSaveTxn.setStatus("NEFT INITIATED");
				} else {
					fundSaveTxn.setStatus("PENDING");
				}
				session.save(fundSaveTxn);

				if (mudraMoneyTransactionBean.getNarrartion().equalsIgnoreCase("NEFT")) {
					logger.info(serverName + "****RequestId****" + requestId + "****start insert into daily_neft_icici with txnid*******"+ fundSaveTxn.getId());
					SQLQuery insertQuery = session.createSQLQuery(""
							+ "insert into daily_neft_icici(mode,txnid,debitacc,creditacc,benname,amount,ifsc,status)VALUES(?,?,?,?,?,?,?,?)");
					insertQuery.setParameter(0, "N");
					insertQuery.setParameter(1, fundSaveTxn.getId());
					insertQuery.setParameter(2, "003105031195");
					insertQuery.setParameter(3, mudraBeneficiaryMastBean.getAccountNo());
					insertQuery.setParameter(4, mudraBeneficiaryMastBean.getName());
					insertQuery.setParameter(5, fundSaveTxn.getDrAmount());
					insertQuery.setParameter(6, mudraBeneficiaryMastBean.getIfscCode());
					insertQuery.setParameter(7, 0);
					insertQuery.executeUpdate();
					session.getTransaction().commit();
					logger.info(serverName + "****RequestId****" + requestId + "*****completed insert into daily_neft_icici with txnid***"+ fundSaveTxn.getId());
				} else { 
					IMPSIciciResponseBean respBean = new IMPSIciciResponseBean();
					if(!operationType.equalsIgnoreCase("verify"))
					{
						respBean =mudraDaoWallet.fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),fundSaveTxn.getDrAmount(),fundSaveTxn.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());
					}
					else
					{
						if (mudraBeneficiaryMastBean != null && mudraBeneficiaryMastBean.getVerified() != null
								&& !mudraBeneficiaryMastBean.getVerified().equalsIgnoreCase("V")) {
							System.out.println("++++++++++++++++++++++++++++++++calling finoimps for verify account++++++++++++++++++++++");

							//List<String> respList=finoImplementation(mudraMoneyTransactionBean.getNarrartion(),mudraMoneyTransactionBean.getVerificationAmount(),mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraBeneficiaryMastBean.getName(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getMobileNo(),mudraSenderWallet.getFirstName());
							respBean =mudraDaoWallet.fundTransferICICIBankIMPS(mudraBeneficiaryMastBean.getAccountNo(),mudraBeneficiaryMastBean.getIfscCode(),mudraMoneyTransactionBean.getVerificationAmount(),mudraMoneyTransactionBean.getId(),mudraSenderWallet.getFirstName(),mudraSenderWallet.getMobileNo());
							
							System.out.println("++++++++++++++++++++++++++++++++getting result from fino imps after verifying account ++++++++++++++++++++++");
							System.out.println("++++++++++++++++++++++++++++++++response is +++++++++++++++++++++"+respBean);
							
							transaction = session.beginTransaction();
							} 
						else {
							String finotrxId = commanUtilDao.getTrxId("ICICI",mudraMoneyTransactionBean.getAggreatorid());
							respBean.setActCode(200);
							beneName = mudraBeneficiaryMastBean.getName();
							
						}
					}
					transaction = session.beginTransaction();
					
					if(respBean.getActCode()!=200) {
						fundSaveTxn.setStatus("FAILED");
						fundSaveTxn.setRemark("Transaction FAILED");
						fundSaveTxn.setBankRrn(respBean.getBankRRN());
						MudraMoneyTransactionBean wallRefundTxnRef = new MudraMoneyTransactionBean();
						if (agnWalletTxnAmount > 0) {
							MudraMoneyTransactionBean mudraMoneyTransactionBeanSaveTxnRef = new MudraMoneyTransactionBean();
							mudraMoneyTransactionBeanSaveTxnRef.setId("TRA" + ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setTxnId(ppiTxnIdfundTransfer);
							mudraMoneyTransactionBeanSaveTxnRef.setWalletId(mudraMoneyTransactionBean.getWalletId());
							mudraMoneyTransactionBeanSaveTxnRef.setSenderId(mudraMoneyTransactionBean.getSenderId());
							mudraMoneyTransactionBeanSaveTxnRef.setBeneficiaryId(mudraMoneyTransactionBean.getBeneficiaryId());
							mudraMoneyTransactionBeanSaveTxnRef.setCrAmount(0.0);
							mudraMoneyTransactionBeanSaveTxnRef.setDrAmount(agnWalletTxnAmount);
							mudraMoneyTransactionBeanSaveTxnRef.setNarrartion("REFUND");
							mudraMoneyTransactionBeanSaveTxnRef.setStatus("SUCCESS");
							mudraMoneyTransactionBeanSaveTxnRef.setRemark("Transaction SUCCESSFUL(REFUND To Aggregator)");
							session.save(mudraMoneyTransactionBeanSaveTxnRef);
							if(walletmastAgg.getWhiteLabel().equalsIgnoreCase("1"))
							{
								String AggRefundCode =  transactionDao.walletToAggregatorRefund(ppiTxnIdfundTransfer,walletmastAgg.getCashDepositeWallet(),agnWalletTxnAmount,surchargeBean.getCreditSurcharge(),mudraMoneyTransactionBean.getIpiemi(),mudraSenderWallet.getAggreatorId(), mudraMoneyTransactionBean.getAgent(),0,0);
							}
						}
							

						txnAmount = 0;

					} else {
						fundSaveTxn.setStatus("SUCCESS");
						fundSaveTxn.setRemark("Transaction SUCCESSFUL");
						fundSaveTxn.setBankRrn(respBean.getBankRRN());
						if(operationType.equalsIgnoreCase("verify")){
							if (beneName.trim().equalsIgnoreCase(mudraBeneficiaryMastBean.getName().trim())) {
								mudraBeneficiaryMastBean.setVerified("V");
								session.update(mudraBeneficiaryMastBean);
								fundTransactionSummaryBean.setVerified("V");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(V)");
								fundTransactionSummaryBean.setVerificationDesc("Match");	
							} else {
								mudraBeneficiaryMastBean.setVerified("NV");
								session.update(mudraBeneficiaryMastBean);
								fundTransactionSummaryBean.setVerified("NV");
								fundTransactionSummaryBean.setAccHolderName(beneName + "(NV)");
								fundTransactionSummaryBean.setVerificationDesc("Not Matched");
							}
						}
					}
					fundSaveTxn.setBankResp(respBean.getBankRRN());
					session.update(fundSaveTxn);
					transaction.commit();
				}
				
				fundTransactionSummaryBean.setStatusCode("1000");
				fundTransactionSummaryBean.setStatusDesc("SUCCESS");
				fundTransactionSummaryBean.setSenderMobile(mudraSenderWallet.getMobileNo());
				fundTransactionSummaryBean.setBeneficiaryName(mudraBeneficiaryMastBean.getName());
				fundTransactionSummaryBean.setBeneficiaryIFSC(mudraBeneficiaryMastBean.getIfscCode());
				fundTransactionSummaryBean.setBeneficiaryAccNo(mudraBeneficiaryMastBean.getAccountNo());
				fundTransactionSummaryBean.setAmount(mudraMoneyTransactionBean.getTxnAmount());
				fundTransactionSummaryBean.setCharges("Min INR 15.00 Max upto 2%");
				fundTransactionSummaryBean.setMode(mudraMoneyTransactionBean.getNarrartion());

				// added on 4th Aug
				session.close();
				session = factory.openSession();
				Criteria cr = session.createCriteria(MudraMoneyTransactionBean.class);
				cr.add(Restrictions.eq("txnId", ppiTxnIdfundTransfer));

				List<MudraMoneyTransactionBean> results = cr.list();
				fundTransactionSummaryBean.setMudraMoneyTransactionBean(results);
				try
				{
				MudraSenderWallet mudraSenderNew=(MudraSenderWallet)session.get(MudraSenderWallet.class, mudraSenderWallet.getId());
				fundTransactionSummaryBean.setSenderLimit(mudraSenderNew.getTransferLimit());
				String smstemplet = commanUtilDao.getsmsTemplet("b2bMoneyTransfer", mudraSenderWallet.getAggreatorId());
				logger.info(serverName + "****RequestId****" + requestId +  smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()));
				SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", mudraSenderWallet.getMobileNo(),
						smstemplet.replaceAll("<<MOBILENO>>", mudraSenderWallet.getMobileNo())
								.replaceAll("<<AMOUNT>>", "" + (int) mudraMoneyTransactionBean.getTxnAmount())
								.replaceAll("<<TXNID>>", ppiTxnIdfundTransfer)
								.replaceAll("<<ACCNO>>", mudraBeneficiaryMastBean.getAccountNo())
								.replaceAll("<<IFSCCODE>>", mudraBeneficiaryMastBean.getIfscCode()),
						"SMS", mudraSenderWallet.getAggreatorId(), "", mudraSenderWallet.getFirstName(), "NOTP");

				ThreadUtil.getThreadPool().execute(smsAndMailUtility);


				}
				catch(Exception ex)
				{
					logger.info("*****problem in fundTransferWallet while sending message******e.message**"+ex.getMessage());
					ex.printStackTrace();
				}
			} else {
				logger.info(serverName + "****RequestId****" + requestId + "******" + mudraMoneyTransactionBean.getSenderId()+"**velocity error****  method****:" + walletAggTxnCode);
				fundTransactionSummaryBean.setStatusCode(walletAggTxnCode);
				fundTransactionSummaryBean.setStatusDesc("Velocity check error");
			}

		} catch (Exception e) {
			logger.debug(serverName + "****RequestId****" + requestId + "*fundTransferWallet====" + e.getMessage(), e);
			fundTransactionSummaryBean.setStatusCode("7000");
			fundTransactionSummaryBean.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return fundTransactionSummaryBean;

	}


	public String saveRemitApiRequest(String aggregatorId, String reqVal, String serverName, String apiName) {

		logger.info(serverName + "Start excution ************************************* saveRemitApiRequest agentId**:"
				+ aggregatorId);
		logger.info(serverName + "Start excution ************************************* saveRemitApiRequest reqVal**:"
				+ reqVal);

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		String requestId = "";
		try {
			transaction = session.beginTransaction();
			PayoutApiLog remApi = new PayoutApiLog();
		    //java.sql.Date date=new java.sql.Date(System.currentTimeMillis());
			 
			remApi.setRequestId(Long.toString(generateLongId()) + commanUtilDao.getTrxId("PAYREQ","OAGG001050"));
			remApi.setAggregatorId(aggregatorId);;
			remApi.setRequestValue(reqVal);
			remApi.setApiName(apiName);
			//remApi.setRequestdate(date);
			session.save(remApi);
			transaction.commit();
			requestId = remApi.getRequestId();
			logger.info(
					serverName + "Start excution ************************************* saveRemitApiRequest requestId**:"
							+ requestId);
		} catch (Exception e) {
			logger.debug(serverName + "problem in saveRemitApiRequest=========================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return requestId;
	}

	
	private static final long twepoch = 1288834974657L;
	private static final long sequenceBits = 17;
	private static final long sequenceMax = 65536;
	private static volatile long lastTimestamp = -1L;
	private static volatile long sequence = 0L;

	private static synchronized Long generateLongId() {
		long timestamp = System.currentTimeMillis();
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) % sequenceMax;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimestamp);
			}
		} else {
			sequence = 0;
		}
		lastTimestamp = timestamp;
		Long id = ((timestamp - twepoch) << sequenceBits) | sequence;
		return id;
	}

	private static long tilNextMillis(long lastTimestamp) {
		long timestamp = System.currentTimeMillis();
		while (timestamp <= lastTimestamp) {
			timestamp = System.currentTimeMillis();
		}
		return timestamp;
	}
	
	public List getBeneficiaryListBank(List<MudraBeneficiaryBank> beneList)
	{
		List respList = new ArrayList();
		Iterator<MudraBeneficiaryBank> itr = beneList.iterator();
		while(itr.hasNext())
		{
			MudraBeneficiaryBank benebank= itr.next();
			HashMap<String,Object> beneficiary = new HashMap<String,Object>();
			beneficiary.put("Id", benebank.getId());
			beneficiary.put("name", benebank.getName());
			beneficiary.put("transfertype", benebank.getTransferType());
			beneficiary.put("accountno", benebank.getAccountNo());
			beneficiary.put("ifsccode", benebank.getIfscCode());
			beneficiary.put("status", benebank.getStatus());
			respList.add(beneficiary);
		}
		return respList;
	}
	public List getBeneficiaryListWallet(List<MudraBeneficiaryWallet> beneList)
	{
		List respList = new ArrayList();
		Iterator<MudraBeneficiaryWallet> itr = beneList.iterator();
		while(itr.hasNext())
		{
			MudraBeneficiaryWallet benewallet= itr.next();
			HashMap<String,Object> beneficiary = new HashMap<String,Object>();
			beneficiary.put("Id", benewallet.getId());
			beneficiary.put("name", benewallet.getName());
			beneficiary.put("transfertype", benewallet.getTransferType());
			beneficiary.put("accountno", benewallet.getAccountNo());
			beneficiary.put("ifsccode", benewallet.getIfscCode());
			beneficiary.put("status", benewallet.getStatus());
			respList.add(beneficiary);
		}
		return respList;
	}
	
	public MudraSenderBank createSenderBankIfNotExt(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "", "", "Inside createSenderBankIfNotExt method");
		try {
			mudraSenderBank = getSenderDtlbank(mudraSenderBank, serverName, requestId);
			
			if (mudraSenderBank.getStatusDesc().equalsIgnoreCase("Y")) {
				return mudraSenderBank;
			}
//				else{
//				mudraSenderBank.setMpin("0000");
//				mudraSenderBank = registerSender(mudraSenderBank, serverName, requestId);
//			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraSenderBank.getAggreatorId(), "", "", "", "", "problem in"
					+ " createSenderBankIfNotExt method|e.message+"+e.getMessage());
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
		}

		return mudraSenderBank;
	}

	
	public MudraSenderBank registerSender(MudraSenderBank mudraSenderBank, String serverName, String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*****RequestId*******" + requestId
				+"|registerSender()|"
				+ mudraSenderBank.getMobileNo());
	
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		mudraSenderBank.setStatusCode("1001");
		mudraSenderBank.setStatusDesc("Error");
		Transaction transaction = null;
		String nmpin = "";
		try {
			if (mudraSenderBank.getMobileNo() == null || mudraSenderBank.getMobileNo().isEmpty()
					|| mudraSenderBank.getMobileNo().length() != 10) {
				mudraSenderBank.setStatusCode("1104");
				mudraSenderBank.setStatusDesc("Invalid Mobile Number.");
				return mudraSenderBank;
			}
			if (mudraSenderBank.getFirstName() == null || mudraSenderBank.getFirstName().isEmpty()) {
				mudraSenderBank.setStatusCode("1101");
				mudraSenderBank.setStatusDesc("First Name can't be empty.");
				return mudraSenderBank;
			}

			List<MudraSenderBank> senderList = session.createCriteria(MudraSenderBank.class)
					.add(Restrictions.eq("mobileNo", mudraSenderBank.getMobileNo())).list();
			if (senderList.size() == 0) {
						transaction = session.beginTransaction();
						mudraSenderBank.setId(commanUtilDao.getTrxId("MSENDER",mudraSenderBank.getAggreatorId()));
						mudraSenderBank.setMpin(CommanUtil.SHAHashing256(mudraSenderBank.getMpin()));
						mudraSenderBank.setStatus("ACTIVE");
						mudraSenderBank.setKycStatus("MIN KYC");
						mudraSenderBank.setTransferLimit(25000.00);
						mudraSenderBank.setFavourite("");
						session.save(mudraSenderBank);
						transaction.commit();
						List<MudraBeneficiaryBank> benfList = session.createCriteria(MudraBeneficiaryBank.class)
								.add(Restrictions.eq("senderId", mudraSenderBank.getId()))
								.add(Restrictions.ne("status", "Deleted"))
								.addOrder(Order.asc("status"))
								.list();
						mudraSenderBank.setBeneficiaryList(benfList);
						mudraSenderBank.setStatusCode("1000");
						mudraSenderBank.setStatusDesc("Sender has been registered successfully.");

					
			} else {
				mudraSenderBank.setStatusCode("11206");
				mudraSenderBank.setStatusDesc("Mobile Number Register With Us.");
				return mudraSenderBank;

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraSenderBank.getAggreatorId(),"","", "", "", serverName + "*****RequestId*******" + requestId
					+ "problem in registerSender" + e.getMessage()+" "+e);
			mudraSenderBank.setStatusCode("7000");
			mudraSenderBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}

		return mudraSenderBank;

	}
	
	public MudraBeneficiaryBank getBeneficiaryByAccountNo(String accountNo,String ifscCode,String senderId, String serverName) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName
				+ "|accountNo|"
				+ accountNo+"|senderId|"+senderId);
			factory = DBUtil.getSessionFactory();
			Session session = factory.openSession();

			Transaction transaction = null;

		MudraBeneficiaryBank bResult =null;
		try {
			bResult = (MudraBeneficiaryBank) session.createCriteria(MudraBeneficiaryBank.class)
					.add(Restrictions.eq("accountNo", accountNo)).add(Restrictions.eq("senderId", senderId)).add(Restrictions.eq("ifscCode", ifscCode)).uniqueResult();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",serverName + "problem in getBeneficiary" + e.getMessage()+" "+e);
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bResult;
	}
	
	public MudraBeneficiaryBank createBeneBankIfNotExt(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraBeneficiaryBank.getAggreatorId(), "", "", "", "", "Inside createBeneBankIfNotExt method|RequestId|"+requestId);
		Session session = null;

		Transaction transaction = null;

		try {
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			MudraBeneficiaryBank mudraBeneficiaryBankGet = getBeneficiaryByAccountNo(mudraBeneficiaryBank.getAccountNo(),mudraBeneficiaryBank.getIfscCode(),mudraBeneficiaryBank.getSenderId(),serverName);
			if(mudraBeneficiaryBankGet != null )
			{
				mudraBeneficiaryBank = (MudraBeneficiaryBank)mudraBeneficiaryBankGet.cloneMe();
			}
			if(mudraBeneficiaryBankGet == null || mudraBeneficiaryBank.getStatus().equals("Deactive"))
			{
				if(!session.isConnected())
				{
					factory = DBUtil.getSessionFactory();
					session = factory.openSession();
				}
				int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
						.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
						.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "",serverName + "****RequestId****" + requestId
						+ "Number ofactive Beneficiary :" + activeCount);

				if(mudraBeneficiaryBankGet == null){
					mudraBeneficiaryBank = registerBeneficiary(mudraBeneficiaryBank,serverName,requestId);
				}else if(mudraBeneficiaryBank.getStatus().equals("Deactive")){
					transaction = session.beginTransaction();
					mudraBeneficiaryBank.setStatus("Active");
					session.update(mudraBeneficiaryBank);
					transaction.commit();
				}
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), mudraBeneficiaryBank.getAggreatorId(), "", "", "", "", "problem in"
					+ " createBeneBankIfNotExt method|e.message+"+e.getMessage());
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");
			e.printStackTrace();
			transaction.rollback();
		} finally {
			if(session.isConnected())
			{session.close();}
			
		}

		return mudraBeneficiaryBank;
	}
	
	public MudraBeneficiaryBank registerBeneficiary(MudraBeneficiaryBank mudraBeneficiaryBank, String serverName,String requestId) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
				+ "|registerBeneficiary()|"
				+ mudraBeneficiaryBank.getAccountNo());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = null;

		try {

			if (mudraBeneficiaryBank.getSenderId() == null || mudraBeneficiaryBank.getSenderId().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1110");
				mudraBeneficiaryBank.setStatusDesc("Invalid Sender Id.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getName() == null || mudraBeneficiaryBank.getName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1101");
				mudraBeneficiaryBank.setStatusDesc("Invalid Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getBankName() == null || mudraBeneficiaryBank.getBankName().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1107");
				mudraBeneficiaryBank.setStatusDesc("Invalid Bank Name.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getAccountNo() == null || mudraBeneficiaryBank.getAccountNo().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1108");
				mudraBeneficiaryBank.setStatusDesc("Invalid Account Number.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getIfscCode() == null || mudraBeneficiaryBank.getIfscCode().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1109");
				mudraBeneficiaryBank.setStatusDesc("Invalid IFSC Code.");
				return mudraBeneficiaryBank;
			}

			if (mudraBeneficiaryBank.getTransferType() == null || mudraBeneficiaryBank.getTransferType().isEmpty()) {
				mudraBeneficiaryBank.setStatusCode("1111");
				mudraBeneficiaryBank.setStatusDesc("Invalid Transfer Type.");
				return mudraBeneficiaryBank;
			}

			List<MudraBeneficiaryBank> benList = session.createCriteria(MudraBeneficiaryBank.class)
				     .add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
				     .add(Restrictions.eq("accountNo", mudraBeneficiaryBank.getAccountNo()))
				     .add(Restrictions.ne("status","Deleted")).list();
			
			if (benList.size() > 0) {
				mudraBeneficiaryBank.setStatusCode("1124");
				mudraBeneficiaryBank.setStatusDesc("Your provided account number is already added with sender.");
				return benList.get(0);
			}

//			int activeCount = (int) session.createCriteria(MudraBeneficiaryBank.class)
//					.add(Restrictions.eq("senderId", mudraBeneficiaryBank.getSenderId()))
//					.add(Restrictions.eq("status", "Active")).setProjection(Projections.rowCount()).uniqueResult();
//			if (activeCount >= 15) {
//				mudraBeneficiaryBank.setStatusCode("1123");
//				mudraBeneficiaryBank.setStatusDesc(
//						"You have already 15 active beneficiaries. Please deactivate anyone before adding a new beneficiary.");
//				return mudraBeneficiaryBank;
//			}

			transaction = session.beginTransaction();
			mudraBeneficiaryBank.setId(commanUtilDao.getTrxId("MBENF",mudraBeneficiaryBank.getAggreatorId()));
			mudraBeneficiaryBank.setStatus("Active");
			mudraBeneficiaryBank.setVerified("NV");
			mudraBeneficiaryBank.setVerifiedStatus("NV");
			mudraBeneficiaryBank.setRecipientId("NA");
			session.save(mudraBeneficiaryBank);
			transaction.commit();
			mudraBeneficiaryBank.setStatusCode("1000");
			mudraBeneficiaryBank.setStatusDesc("Beneficiary has been registered successfully.");

			MudraSenderBank mudraSenderBank = (MudraSenderBank) session.get(MudraSenderBank.class,
					mudraBeneficiaryBank.getSenderId());


		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),mudraBeneficiaryBank.getAggreatorId(),"","", mudraBeneficiaryBank.getSenderId(), "", serverName + "*********RequestId*******" + requestId
					+ "problem in registerBeneficiary" + e.getMessage()+" "+e);
			mudraBeneficiaryBank.setStatusCode("7000");
			mudraBeneficiaryBank.setStatusDesc("Error");

			transaction.rollback();
		} finally {
			session.close();
		}
		return mudraBeneficiaryBank;
	}
	
	public SurchargeBean calculateSurcharge(SurchargeBean surchargeBean, String serverName, String requestId) {
		MudraSenderBank mudraSenderBanktest=new MudraSenderBank();
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName + "****RequestId******" + requestId
				+ "|calculateSurcharge()|"
				+ surchargeBean.getAgentId()+ "|calculateSurcharge()|"
						+ surchargeBean.getAmount());
		
		List objectList = null;
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		double surcharge = 0.0;
		double distsurcharge = 0.0;
		double creditSurcharge = 0.0;
		double suDistSurcharge=0.0;
		double agentRefund=0.0;
		double aggReturnVal=0.0;
		double dtdsApply=0.0;
		try {
			if (surchargeBean.getAgentId() == null || surchargeBean.getAgentId().isEmpty()) {
				surchargeBean.setStatus("4321");
				return surchargeBean;
			}

			if (surchargeBean.getAmount() <= 0) {
				surchargeBean.setStatus("4322");
				return surchargeBean;
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" + requestId
					+ "|excution  surcharge :" + surchargeBean.getAgentId()+ "|excution  surcharge :" + surchargeBean.getAmount());
		
			surchargeBean.setTxnCode(15);
			Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
			query.setParameter("id", surchargeBean.getAgentId());
			query.setParameter("txnAmount", surchargeBean.getAmount());
			if (surchargeBean.getTransType() == null)
				query.setParameter("transType", "");
			else
				query.setParameter("transType", surchargeBean.getTransType());
			query.setParameter("isBank", "3");
			objectList = query.list();
			Object obj[] = (Object[]) objectList.get(0);
			surcharge = Double.parseDouble("" + obj[0]);
			distsurcharge = Double.parseDouble("" + obj[1]);
			if (obj[2] != null)
				creditSurcharge = Double.parseDouble("" + obj[2]);
			else
				creditSurcharge =0.0;
			if(obj[3]!=null)
				suDistSurcharge=Double.parseDouble(""+obj[3]);
			else
				suDistSurcharge=0.0;
			if(obj[4]!=null)
				agentRefund=Double.parseDouble(""+obj[4]);
			else
				agentRefund=0.0;
			if(obj[5]!=null)
				aggReturnVal=Double.parseDouble(""+obj[5]);
			if(obj[6]!=null)
				dtdsApply=Double.parseDouble(""+obj[6]);
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", "****RequestId******" + requestId
					+ " excution surcharge :" + surcharge);
			
			/*logger.info("****RequestId******" + requestId
					+ "******** excution *********************************** surcharge :" + surcharge);*/
			surchargeBean.setSurchargeAmount(surcharge);
			surchargeBean.setDistSurCharge(distsurcharge);
			surchargeBean.setCreditSurcharge(creditSurcharge);
			surchargeBean.setSuDistSurcharge(suDistSurcharge);
			surchargeBean.setAgentRefund(agentRefund);
			surchargeBean.setAggReturnVal(aggReturnVal);
			surchargeBean.setDtdsApply(dtdsApply);
			surchargeBean.setStatus("1000");
		} catch (Exception e) {
			surchargeBean.setStatus("7000");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +"|Agentid :"+surchargeBean.getAgentId()+ "*******RequestId******" + requestId
				+ "problem in calculateCashinSurcharge" + e.getMessage()+" "+e);
			/*logger.debug(serverName + "*******RequestId******" + requestId
					+ "********problem in calculateCashinSurcharge**********************************" + e.getMessage(),
					e);*/
			e.printStackTrace();
		} finally {
			session.close();
		}
		return surchargeBean;
	}
	
	public String getPlanIdByAgentId(String agentId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getPlanIdByAgentId IFSCCode :-"+agentId);

		factory = DBUtil.getSessionFactory();
		Session session=null;
		try
		{
			session =factory.openSession();
			WalletMastBean walletMast=(WalletMastBean)session.get(WalletMastBean.class, agentId);
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			walletMast=(WalletMastBean)session.get(WalletMastBean.class, walletMast.getDistributerid());
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			walletMast=(WalletMastBean)session.get(WalletMastBean.class, walletMast.getAggreatorid());
			if(walletMast!=null&&walletMast.getPlanId()!=null&&walletMast.getPlanId().length()>4) {
				return walletMast.getPlanId();
			}
			
			
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "|Method Name :getPlanIdByAgentId | problem found" + e.getMessage()+" "+e);
			
		}
		finally
		{
			if(session!=null)
			session.close();
		}
		return "N/A";
	}
	
public List<String> bankTransfer(String transferMode,String orderId, double amount,String agentId,MudraBeneficiaryBank beneficiaryBank){
		
		PayoutMaster pay =  new PayoutMaster();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "calling bank transfer : "+agentId);		
		List<String> response = new ArrayList<String>();
		
		AadharShilaRequestResponse reqResp=new AadharShilaRequestResponse();
		String responseCode=null;
		String actcode=null;
		SessionFactory factory = null;
		Session session=null;
		Transaction transaction =null;
		
		
		try
		{
			
			
			factory = DBUtil.getSessionFactory();
			session = factory.openSession();
			transaction = session.beginTransaction();
			
			

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
			String date = dtf.format(now);  
			
			
			JSONObject jsonText = new JSONObject();
			jsonText.put("orderId", orderId);//"ORDERID_98765");
			jsonText.put("beneficiaryAccount", beneficiaryBank.getAccountNo());//"918008484891");
			jsonText.put("beneficiaryIFSC", beneficiaryBank.getIfscCode() );//"PYTM0123456");
			jsonText.put("amount", ""+amount);//"1.00");
			jsonText.put("purpose", "OTHERS");//"SALARY_DISBURSEMENT");
			jsonText.put("date", date);//"2020-06-01");
			jsonText.put("transferMode", transferMode);
			
			reqResp.setRequestJson(jsonText.toString());
			reqResp.setTxnId(orderId);
			session.save(reqResp);
			transaction.commit();
			transaction = session.beginTransaction();
			
			String bankRespXml = "Fail";
			
			
			String payResponse = pay.bankTransfer("", orderId, beneficiaryBank.getAccountNo(), beneficiaryBank.getIfscCode(), ""+amount, "OTHERS", date, transferMode);
			
			
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "response after calling payout banktransfer service"+payResponse);
			
			String status = null;
			String statusCode = null;
			String statusMessage = null;
			
			if (payResponse==null) {
				responseCode="0";
				actcode="7000";
			} else {
				
				try {
				JSONObject json = new JSONObject(payResponse);
				status = json.getString("status");
				statusCode = json.getString("statusCode");
				statusMessage = json.getString("statusMessage");
				
				reqResp.setResponseJson(payResponse);
				reqResp.setErrorCode(statusCode);
				reqResp.setErrorMsg(statusMessage);
				session.saveOrUpdate(reqResp);
				transaction.commit();
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				if (statusCode != null && "DE_001".equalsIgnoreCase(statusCode)
						&& "SUCCESS".equalsIgnoreCase(status)) {
				responseCode="0";
				actcode="0";
				} else if (statusCode != null && "DE_002".equalsIgnoreCase(statusCode)
						&&  "ACCEPTED".equalsIgnoreCase(status)) {
				responseCode="0";
				actcode="S";
				} else if (statusCode != null && ("DE_101".equalsIgnoreCase(statusCode) || "DE_102".equalsIgnoreCase(statusCode))
						&& "PENDING".equalsIgnoreCase(status)) {
				responseCode="0";
				actcode="7000";
				} else if (statusCode != null
						&& ("DE_010".equalsIgnoreCase(statusCode) || "DE_011".equalsIgnoreCase(statusCode)
								|| "DE_012".equalsIgnoreCase(statusCode) || "DE_013".equalsIgnoreCase(statusCode)
								|| "DE_014".equalsIgnoreCase(statusCode) || "DE_015".equalsIgnoreCase(statusCode)
								|| "DE_016".equalsIgnoreCase(statusCode) || "DE_017".equalsIgnoreCase(statusCode)
								|| "DE_018".equalsIgnoreCase(statusCode) || "DE_019".equalsIgnoreCase(statusCode)
								|| "DE_021".equalsIgnoreCase(statusCode) || "DE_022".equalsIgnoreCase(statusCode)
								|| "DE_023".equalsIgnoreCase(statusCode) || "DE_024".equalsIgnoreCase(statusCode)
								|| "DE_025".equalsIgnoreCase(statusCode) || "DE_034".equalsIgnoreCase(statusCode)
								|| "DE_039".equalsIgnoreCase(statusCode) || "DE_040".equalsIgnoreCase(statusCode)
								|| "DE_041".equalsIgnoreCase(statusCode) || "DE_042".equalsIgnoreCase(statusCode)
								|| "DE_050".equalsIgnoreCase(statusCode) || "DE_051".equalsIgnoreCase(statusCode)
								|| "DE_052".equalsIgnoreCase(statusCode) || "DE_053".equalsIgnoreCase(statusCode)
								|| "DE_054".equalsIgnoreCase(statusCode) || "DE_055".equalsIgnoreCase(statusCode)
								|| "DE_056".equalsIgnoreCase(statusCode) || "DE_057".equalsIgnoreCase(statusCode)
								|| "DE_602".equalsIgnoreCase(statusCode) || "DE_400".equalsIgnoreCase(statusCode)
								|| "DE_401".equalsIgnoreCase(statusCode) || "DE_402".equalsIgnoreCase(statusCode)
								|| "DE_403".equalsIgnoreCase(statusCode) || "DE_404".equalsIgnoreCase(statusCode)
								|| "DE_405".equalsIgnoreCase(statusCode) || "DE_406".equalsIgnoreCase(statusCode)
								|| "DE_407".equalsIgnoreCase(statusCode) || "DE_408".equalsIgnoreCase(statusCode)
								|| "DE_409".equalsIgnoreCase(statusCode) || "DE_500".equalsIgnoreCase(statusCode)
								|| "DE_606".equalsIgnoreCase(statusCode) || "DE_607".equalsIgnoreCase(statusCode)
								|| "DE_603".equalsIgnoreCase(statusCode) || "DE_609".equalsIgnoreCase(statusCode)
								|| "DE_612".equalsIgnoreCase(statusCode) || "DE_613".equalsIgnoreCase(statusCode)
								|| "DE_614".equalsIgnoreCase(statusCode) || "DE_615".equalsIgnoreCase(statusCode)
								|| "DE_616".equalsIgnoreCase(statusCode) || "DE_617".equalsIgnoreCase(statusCode)
								|| "DE_618".equalsIgnoreCase(statusCode) || "DE_619".equalsIgnoreCase(statusCode)
								|| "DE_620".equalsIgnoreCase(statusCode) || "DE_621".equalsIgnoreCase(statusCode)
								|| "DE_622".equalsIgnoreCase(statusCode) || "DE_623".equalsIgnoreCase(statusCode)
								|| "DE_625".equalsIgnoreCase(statusCode) || "DE_626".equalsIgnoreCase(statusCode)
								|| "DE_627".equalsIgnoreCase(statusCode) || "DE_628".equalsIgnoreCase(statusCode)
								|| "DE_629".equalsIgnoreCase(statusCode) || "DE_631".equalsIgnoreCase(statusCode)
								|| "DE_632".equalsIgnoreCase(statusCode) || "DE_634".equalsIgnoreCase(statusCode)
								|| "DE_636".equalsIgnoreCase(statusCode) || "DE_640".equalsIgnoreCase(statusCode)
								|| "DE_641".equalsIgnoreCase(statusCode) || "DE_643".equalsIgnoreCase(statusCode)
								|| "DE_646".equalsIgnoreCase(statusCode) || "DE_647".equalsIgnoreCase(statusCode)
								|| "DE_648".equalsIgnoreCase(statusCode) || "DE_649".equalsIgnoreCase(statusCode)
								|| "DE_650".equalsIgnoreCase(statusCode) || "DE_651".equalsIgnoreCase(statusCode)
								|| "DE_652".equalsIgnoreCase(statusCode) || "DE_653".equalsIgnoreCase(statusCode)
								|| "DE_656".equalsIgnoreCase(statusCode) || "DE_657".equalsIgnoreCase(statusCode)
								|| "DE_658".equalsIgnoreCase(statusCode) || "DE_659".equalsIgnoreCase(statusCode)
								|| "DE_660".equalsIgnoreCase(statusCode) || "DE_661".equalsIgnoreCase(statusCode)
								|| "DE_662".equalsIgnoreCase(statusCode) || "DE_663".equalsIgnoreCase(statusCode)
								|| "DE_664".equalsIgnoreCase(statusCode) || "DE_665".equalsIgnoreCase(statusCode)
								|| "DE_666".equalsIgnoreCase(statusCode) || "DE_667".equalsIgnoreCase(statusCode)
								|| "DE_668".equalsIgnoreCase(statusCode) || "DE_669".equalsIgnoreCase(statusCode)
								|| "DE_670".equalsIgnoreCase(statusCode) || "DE_671".equalsIgnoreCase(statusCode)
								|| "DE_672".equalsIgnoreCase(statusCode) || "DE_673".equalsIgnoreCase(statusCode)
								|| "DE_674".equalsIgnoreCase(statusCode) || "DE_675".equalsIgnoreCase(statusCode)
								|| "DE_676".equalsIgnoreCase(statusCode) || "DE_677".equalsIgnoreCase(statusCode)
								|| "DE_678".equalsIgnoreCase(statusCode) || "DE_679".equalsIgnoreCase(statusCode)
								|| "DE_680".equalsIgnoreCase(statusCode) || "DE_681".equalsIgnoreCase(statusCode)
								|| "DE_682".equalsIgnoreCase(statusCode) || "DE_683".equalsIgnoreCase(statusCode)
								|| "DE_701".equalsIgnoreCase(statusCode) || "DE_703".equalsIgnoreCase(statusCode)
								|| "DE_704".equalsIgnoreCase(statusCode) || "DE_705".equalsIgnoreCase(statusCode))) {
					responseCode="1";
				actcode="7000";
			}else {
				responseCode="0";
				actcode="7000";
			}
			}
			
		}
		catch(Exception e){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","",  "payout banktransfer "+"problem found" + e.getMessage()+" "+e);
			e.printStackTrace();
			responseCode="0";
			actcode="7000";
			if(transaction.isActive())
				transaction.rollback();
		}
		try {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","", "inside try block after aadadharshilremit commited ");
			
			
//				"errorMsg":"SUCCESS","errorCode":"00",
//				"data":{"customerId":"9999215888","name":"MOBILEWARE PVT LTD","bankName":"HDFC Bank Ltd",
//				"clientRefId":"FBTXTD10003","txnId":"925823485059","impsRespCode":"00","accountNumber":"123456123456",
//				"ifscCode":"HDFC0001898","txnStatus":"00"},"Reason":"SUCCESS"}
			
			response.add(responseCode);
			response.add(actcode);
			response.add("null");
			response.add("null");
			
		}
		catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","|payout banktransfer()|"+ "excepiton occur while adding response value in list " + e.getMessage()+" "+e);
			
			
			e.printStackTrace();
			response.add("0");
			response.add("7000");
		}
		finally
		{
			session.close();
		}
		return response;
	}
	
private String getDate(String string) {
	
	//TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat df = new SimpleDateFormat(string); // Quoted "Z" to indicate UTC, no timezone offset
	//df.setTimeZone(tz);
	return df.format(new Date());
}

	
	public static void main(String s[])
	{
		MudraSenderBank bk = new MudraSenderBank();
		bk.setMobileNo("9999215888");
		bk.setAggreatorId("OAGG001062");
		new ExtremmitapiDaoImpl().getSenderDtlbank(bk,"","");
	}
}
