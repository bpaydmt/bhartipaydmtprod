package com.bhartipay.payoutapi.payu.persistnace;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionDetail {

@SerializedName("txnId")
@Expose
private Integer txnId;
@SerializedName("batchId")
@Expose
private String batchId;
@SerializedName("merchantRefId")
@Expose
private String merchantRefId;
@SerializedName("purpose")
@Expose
private String purpose;
@SerializedName("amount")
@Expose
private Double amount;
@SerializedName("txnStatus")
@Expose
private String txnStatus;
@SerializedName("txnSubStatus")
@Expose
private Object txnSubStatus;
@SerializedName("txnDate")
@Expose
private String txnDate;
@SerializedName("scheduledTxnDate")
@Expose
private String scheduledTxnDate;
@SerializedName("payuTransactionRefNo")
@Expose
private String payuTransactionRefNo;
@SerializedName("beneficiaryName")
@Expose
private String beneficiaryName;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("responseCode")
@Expose
private String responseCode;
@SerializedName("transferType")
@Expose
private String transferType;
@SerializedName("bankTransactionRefNo")
@Expose
private Object bankTransactionRefNo;
@SerializedName("nameWithBank")
@Expose
private Object nameWithBank;
@SerializedName("lastStatusUpdateDate")
@Expose
private String lastStatusUpdateDate;
@SerializedName("succeedOn")
@Expose
private Object succeedOn;
@SerializedName("fee")
@Expose
private Object fee;
@SerializedName("tax")
@Expose
private Object tax;

public Integer getTxnId() {
return txnId;
}

public void setTxnId(Integer txnId) {
this.txnId = txnId;
}

public String getBatchId() {
return batchId;
}

public void setBatchId(String batchId) {
this.batchId = batchId;
}

public String getMerchantRefId() {
return merchantRefId;
}

public void setMerchantRefId(String merchantRefId) {
this.merchantRefId = merchantRefId;
}

public String getPurpose() {
return purpose;
}

public void setPurpose(String purpose) {
this.purpose = purpose;
}

public Double getAmount() {
return amount;
}

public void setAmount(Double amount) {
this.amount = amount;
}

public String getTxnStatus() {
return txnStatus;
}

public void setTxnStatus(String txnStatus) {
this.txnStatus = txnStatus;
}

public Object getTxnSubStatus() {
return txnSubStatus;
}

public void setTxnSubStatus(Object txnSubStatus) {
this.txnSubStatus = txnSubStatus;
}

public String getTxnDate() {
return txnDate;
}

public void setTxnDate(String txnDate) {
this.txnDate = txnDate;
}

public String getScheduledTxnDate() {
return scheduledTxnDate;
}

public void setScheduledTxnDate(String scheduledTxnDate) {
this.scheduledTxnDate = scheduledTxnDate;
}

public String getPayuTransactionRefNo() {
return payuTransactionRefNo;
}

public void setPayuTransactionRefNo(String payuTransactionRefNo) {
this.payuTransactionRefNo = payuTransactionRefNo;
}

public String getBeneficiaryName() {
return beneficiaryName;
}

public void setBeneficiaryName(String beneficiaryName) {
this.beneficiaryName = beneficiaryName;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public String getResponseCode() {
return responseCode;
}

public void setResponseCode(String responseCode) {
this.responseCode = responseCode;
}

public String getTransferType() {
return transferType;
}

public void setTransferType(String transferType) {
this.transferType = transferType;
}

public Object getBankTransactionRefNo() {
return bankTransactionRefNo;
}

public void setBankTransactionRefNo(Object bankTransactionRefNo) {
this.bankTransactionRefNo = bankTransactionRefNo;
}

public Object getNameWithBank() {
return nameWithBank;
}

public void setNameWithBank(Object nameWithBank) {
this.nameWithBank = nameWithBank;
}

public String getLastStatusUpdateDate() {
return lastStatusUpdateDate;
}

public void setLastStatusUpdateDate(String lastStatusUpdateDate) {
this.lastStatusUpdateDate = lastStatusUpdateDate;
}

public Object getSucceedOn() {
return succeedOn;
}

public void setSucceedOn(Object succeedOn) {
this.succeedOn = succeedOn;
}

public Object getFee() {
return fee;
}

public void setFee(Object fee) {
this.fee = fee;
}

public Object getTax() {
return tax;
}

public void setTax(Object tax) {
this.tax = tax;
}

}