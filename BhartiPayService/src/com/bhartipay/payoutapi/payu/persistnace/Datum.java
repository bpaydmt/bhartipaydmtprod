package com.bhartipay.payoutapi.payu.persistnace;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

@SerializedName("batchId")
@Expose
private String batchId;
@SerializedName("merchantRefId")
@Expose
private String merchantRefId;
@SerializedName("error")
@Expose
private String error;
@SerializedName("code")
@Expose
private List<Integer> code = null;

public String getBatchId() {
return batchId;
}

public void setBatchId(String batchId) {
this.batchId = batchId;
}

public String getMerchantRefId() {
return merchantRefId;
}

public void setMerchantRefId(String merchantRefId) {
this.merchantRefId = merchantRefId;
}

public String getError() {
return error;
}

public void setError(String error) {
this.error = error;
}

public List<Integer> getCode() {
return code;
}

public void setCode(List<Integer> code) {
this.code = code;
}

}