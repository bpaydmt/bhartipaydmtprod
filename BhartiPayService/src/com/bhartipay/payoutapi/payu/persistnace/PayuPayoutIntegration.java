package com.bhartipay.payoutapi.payu.persistnace;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.google.gson.Gson;

public class PayuPayoutIntegration {

//	public static final String BASE_URL = "https://payout.payumoney.com";
//	public static final String TOKEN_URL = "https://uat-accounts.payu.in/oauth/token";
//	public static final String PAYOUT_MERCHANT_ID = "1111117";
//	
//	public static final String USER_NAME = "garimachauhan24@gmail.com";
//	public static final String PASSWORD = "Tester@123"; 
//	public static final String CLIENT_ID = "6f8bb4951e030d4d7349e64a144a534778673585f86039617c167166e9154f7e";
	
	
	public static final String BASE_URL = "https://payout.payumoney.com";
	public static final String TOKEN_URL = "https://accounts.payu.in/oauth/token";
	public static final String PAYOUT_MERCHANT_ID = "1111361";
	
	public static final String USER_NAME = "lokesh.sharma@bhartipay.com";
	public static final String PASSWORD = "PayU@123"; 
	public static final String CLIENT_ID = "ccbb70745faad9c06092bb5c79bfd919b6f45fd454f34619d83920893e90ae6b";
	
	
	
	
	public static String getToken() {
	    try {
	        URL url = new URL(TOKEN_URL);
	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");

	        Map<String, String> params = new HashMap<>();
			    params.put("grant_type", "password");
			    params.put("scope", "create_payout_transactions");
			    params.put("username", USER_NAME);
			    params.put("password", PASSWORD);
			    params.put("client_id", CLIENT_ID);
			    
			    StringBuilder postData = new StringBuilder();
			    for (Map.Entry<String, String> param : params.entrySet()) {
			        if (postData.length() != 0) {
			            postData.append('&');
			        }
			        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			        postData.append('=');
			        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			    }

	        OutputStream os = conn.getOutputStream();
	        os.write(postData.toString().getBytes("UTF-8"));
	        os.flush();

	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));

	        String output;
	        String jsonResponse = null;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	            jsonResponse = output;
	        }

	        JSONObject jsonObject = new JSONObject(jsonResponse);
	        conn.disconnect();

	        return jsonObject.getString("access_token");
	      } catch (Exception e) {

	        e.printStackTrace();

	      }
	    return null;

	}
	
	public static void main(String[] args) {
//		String str = getToken();
//		System.out.println(str);
		PayuPayoutIntegration pi = new PayuPayoutIntegration();
		
		pi.fundTransfer("50100102602137", "HDFC0001898", "Ambuj KUmar", 10, "Test123112", "IMPS");
		pi.getTransactionStatus("Test123112");
	}
	
	
	
	public  ImpsResponse fundTransfer(String beneficiaryAccountNumber, String beneficiaryIfscCode, String beneficiaryName, double amount, String merchantRefId
			, String paymentType ) {
		try {

	        URL url = new URL(BASE_URL+"/payout/payment");
	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Authorization", "Bearer "+getToken());
	        conn.setRequestProperty("payoutMerchantId", PAYOUT_MERCHANT_ID);
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Cache-Control", "no-cache");
//	        conn.hea
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject  = new JSONObject();
			jsonObject.put("beneficiaryAccountNumber", beneficiaryAccountNumber);
			jsonObject.put("beneficiaryIfscCode", beneficiaryIfscCode);
			jsonObject.put("beneficiaryName", beneficiaryName);
			jsonObject.put("amount", amount);
			jsonObject.put("merchantRefId", merchantRefId);
			jsonObject.put("paymentType", paymentType);
			jsonObject.put("purpose", "Transafer Request");
			jsonObject.put("batchId", 1);

			jsonArray.add(jsonObject);
			
			System.out.println(jsonArray.toString());
	        OutputStream os = conn.getOutputStream();
	        os.write(jsonArray.toString().getBytes());
	        os.flush();

	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));

	        String output;
	        String jsonResponse = null;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	            jsonResponse = output;
	        }

	        Gson gson = new Gson();
	      //  JSONObject jsonObj = new JSONObject(jsonResponse);
	       ImpsResponse impsResponse = gson.fromJson(jsonResponse, ImpsResponse.class);
	       conn.disconnect();

	        return impsResponse;
	      } catch (Exception e) {

	        e.printStackTrace();

	      } 
		return null;
	}
	
	
	public static StatusResponse getTransactionStatus(String merchantRefId) {
	    try {
	        URL url = new URL(BASE_URL+"/payout/payment/listTransactions");
	        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Authorization", "Bearer "+getToken());
	        conn.setRequestProperty("payoutMerchantId", PAYOUT_MERCHANT_ID);
	        conn.setRequestProperty("Cache-Control", "no-cache");
	        
			
			 Map<String, String> params = new HashMap<>();
			    params.put("merchantRefId", merchantRefId);
			    params.put("batchId", "1");
			   
			    StringBuilder postData = new StringBuilder();
			    for (Map.Entry<String, String> param : params.entrySet()) {
			        if (postData.length() != 0) {
			            postData.append('&');
			        }
			        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			        postData.append('=');
			        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			    }

	        OutputStream os = conn.getOutputStream();
	        os.write(postData.toString().getBytes("UTF-8"));
	        os.flush();

	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));

	        String output;
	        String jsonResponse = null;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	            jsonResponse = output;
	        }
	        
	        Gson gson = new Gson();
	        StatusResponse statusResponse = gson.fromJson(jsonResponse, StatusResponse.class);
	        System.out.println(statusResponse);
	        conn.disconnect();
	        return statusResponse;

	      } catch (Exception e) {

	        e.printStackTrace();

	      }
	    return null;

	}
}
