package com.bhartipay.payoutapi.payu.persistnace;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImpsResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("msg")
@Expose
private Object msg;
@SerializedName("code")
@Expose
private Object code;
@SerializedName("data")
@Expose
private List<Datum> data = null;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public Object getMsg() {
return msg;
}

public void setMsg(Object msg) {
this.msg = msg;
}

public Object getCode() {
return code;
}

public void setCode(Object code) {
this.code = code;
}

public List<Datum> getData() {
return data;
}

public void setData(List<Datum> data) {
this.data = data;
}

}