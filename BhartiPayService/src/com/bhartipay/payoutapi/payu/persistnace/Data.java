package com.bhartipay.payoutapi.payu.persistnace;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

@SerializedName("payoutMerchantId")
@Expose
private Object payoutMerchantId;
@SerializedName("noOfPages")
@Expose
private Integer noOfPages;
@SerializedName("totalElements")
@Expose
private Integer totalElements;
@SerializedName("currentPage")
@Expose
private Integer currentPage;
@SerializedName("totalAmount")
@Expose
private Double totalAmount;
@SerializedName("succesTxn")
@Expose
private Integer succesTxn;
@SerializedName("pendingTxn")
@Expose
private Integer pendingTxn;
@SerializedName("transactionDetails")
@Expose
private List<TransactionDetail> transactionDetails = null;

public Object getPayoutMerchantId() {
return payoutMerchantId;
}

public void setPayoutMerchantId(Object payoutMerchantId) {
this.payoutMerchantId = payoutMerchantId;
}

public Integer getNoOfPages() {
return noOfPages;
}

public void setNoOfPages(Integer noOfPages) {
this.noOfPages = noOfPages;
}

public Integer getTotalElements() {
return totalElements;
}

public void setTotalElements(Integer totalElements) {
this.totalElements = totalElements;
}

public Integer getCurrentPage() {
return currentPage;
}

public void setCurrentPage(Integer currentPage) {
this.currentPage = currentPage;
}

public Double getTotalAmount() {
return totalAmount;
}

public void setTotalAmount(Double totalAmount) {
this.totalAmount = totalAmount;
}

public Integer getSuccesTxn() {
return succesTxn;
}

public void setSuccesTxn(Integer succesTxn) {
this.succesTxn = succesTxn;
}

public Integer getPendingTxn() {
return pendingTxn;
}

public void setPendingTxn(Integer pendingTxn) {
this.pendingTxn = pendingTxn;
}

public List<TransactionDetail> getTransactionDetails() {
return transactionDetails;
}

public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
this.transactionDetails = transactionDetails;
}

}