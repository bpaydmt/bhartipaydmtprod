package com.bhartipay.report;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.bhartipay.report.bean.TravelDetailsBean;
import com.bhartipay.report.bean.TravelTxn;
import com.bhartipay.report.bean.TxnDetailBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;

import org.apache.log4j.Logger;

public class ReportService 
{
	private static final Logger logger= Logger.getLogger(ReportService.class.getName());
	public List<TxnDetailBean> getTxnReport( List<TravelTxn> travelTxn)
	{
		logger.info("*************Start Execution ****************getTxnReport");
		List<TxnDetailBean> travelTxnDetlList = new ArrayList<TxnDetailBean>();
		Iterator<TravelTxn> itr = travelTxn.iterator();
		try
		{
		 while(itr.hasNext())
		 {
			 TravelTxn trvlTxn  =(TravelTxn)itr.next();
			 TxnDetailBean txnDetailBean = new TxnDetailBean();
			 txnDetailBean.setType(trvlTxn.getType());
			 txnDetailBean.setTransId(trvlTxn.getTransid());
			 txnDetailBean.setTxnDate(trvlTxn.getTxndate().toString());
			 txnDetailBean.setStatus(trvlTxn.getStatus());
			 txnDetailBean.setAmount(trvlTxn.getAmount());
			 if(trvlTxn.getType().equalsIgnoreCase("Flight"))
			 {
				 List<TravelDetailsBean> travelDtl= new ArrayList<TravelDetailsBean>(); 
				 StringTokenizer strHash = new StringTokenizer(trvlTxn.getDetails(), "#");
				 while(strHash.countTokens() > 0)
				 {
					 TravelDetailsBean trvlDtl = new TravelDetailsBean();
					 StringTokenizer strPipe = new StringTokenizer(strHash.nextToken(),"|");
					 if(strPipe.countTokens() > 0)
					 {
						 StringTokenizer strComma = new StringTokenizer(strPipe.nextToken(),",");
						 if(strComma.countTokens() > 0)
						 {
							 trvlDtl.setAirlineCode(strComma.nextToken());
							 if(strComma.countTokens() > 0)
							 {
								 trvlDtl.setFlightName(strComma.nextToken());
								 if(strComma.countTokens() > 0)
									 trvlDtl.setFlightCode(strComma.nextToken());
							 }
						 }
						 if(strPipe.countTokens() > 0)
						 {
							 StringTokenizer strComma2 = new StringTokenizer(strPipe.nextToken(),",");
							 if(strComma2.countTokens() > 0 )
							 {
								 trvlDtl.setSource(strComma2.nextToken());
							 }
							 if(strPipe.countTokens() > 0)
							 {
								 StringTokenizer strComma3 = new StringTokenizer(strPipe.nextToken(),",");
								 if(strComma3.countTokens() > 0 )
								 {
									 trvlDtl.setDestination(strComma3.nextToken());
								 }
								 if(strPipe.countTokens() > 0)
								 {
									 StringTokenizer strComma4 = new StringTokenizer(strPipe.nextToken(),",");
									 if(strComma4.countTokens() > 0 )
									 {
										 trvlDtl.setFlightAmt(Double.parseDouble(strComma4.nextToken()));
									 }
									 if(strPipe.countTokens() > 0)
									 {
										 StringTokenizer strComma5 = new StringTokenizer(strPipe.nextToken(),",");
										 if(strComma5.countTokens() > 0)
										 {
											 trvlDtl.setDeptDate(strComma5.nextToken());
										 }
										 if(strPipe.countTokens() > 0)
										 {
											 StringTokenizer strComma6 = new StringTokenizer(strPipe.nextToken(),",");
											 if(strComma6.countTokens() > 0)
											 {
												 trvlDtl.setArrDate(strComma6.nextToken());
											 }
											 if(strPipe.countTokens() > 0)
											 {
												 StringTokenizer strComma7 = new StringTokenizer(strPipe.nextToken(),",");
												 if(strComma7.countTokens() > 0 )
												 {
													 trvlDtl.setStatus(strComma7.nextToken());
												 }
												 if(strPipe.countTokens() > 0)
												 {
													 StringTokenizer strComma8 = new StringTokenizer(strPipe.nextToken(),",");
													 if(strComma8.countTokens() > 0 )
													 {
														 trvlDtl.setPnr(strComma8.nextToken());
													 }
												 }
											 }
										 }
									 }
								 }
							 }
						 }
					 }
					 //StringTokenizer strPipe = new StringTokenizer(trvlTxn.getDetails(),"|");
					 travelDtl.add(trvlDtl);
					 if(travelDtl.size() == 2)
					 {
						String firstStatus = travelDtl.get(0).getStatus();
						String secondStatus = travelDtl.get(1).getStatus();
						if(firstStatus.equalsIgnoreCase("TicketBooked") && !secondStatus.equalsIgnoreCase("TicketBooked"))
						{
							txnDetailBean.setStatus("PartiallyBooked");
						}
						else if(firstStatus.equalsIgnoreCase("FareQuoteDone") && secondStatus.equalsIgnoreCase("FareQuoteDone"))
						{
							txnDetailBean.setStatus("Cancel");
						}
					 }
					 else 
					 {
						 if(travelDtl.get(0).getStatus().equalsIgnoreCase("FareQuoteDone"))
							{
								txnDetailBean.setStatus("Cancel");
							}
					 }
					 
					 
				 }
				 txnDetailBean.setTravelDetails(travelDtl);
				 travelTxnDetlList.add(txnDetailBean);
			 }
			 else if(trvlTxn.getType().equalsIgnoreCase("Recharge"))
			 {
				 StringTokenizer strPipe = new StringTokenizer(trvlTxn.getDetails(),"|");
				 if(strPipe.countTokens() > 0)
				 {
					 RechargeTxnBean rechargeTxn = new RechargeTxnBean();
					 rechargeTxn.setRechargeType(strPipe.nextToken());
					 if(strPipe.countTokens() > 0)
					 {
				 		rechargeTxn.setRechargeCircle(strPipe.nextToken());
				 		if(strPipe.countTokens() > 0)
				 		{
				 			rechargeTxn.setRechargeOperator(strPipe.nextToken());
				 			if(strPipe.countTokens() > 0)	
				 				rechargeTxn.setRechargeNumber(strPipe.nextToken());
				 		}
					 }
					 txnDetailBean.setRechargeTxn(rechargeTxn);
				 }
			 travelTxnDetlList.add(txnDetailBean); 
		 }
		 }
		 
		}
		catch(Exception e)
		{
			logger.info("*****************Problem in ************getTxnReport*********e.message"+e.getMessage());
			e.printStackTrace();
		}
		return travelTxnDetlList;
	}

	}
