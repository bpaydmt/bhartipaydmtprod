package com.bhartipay.report.persistence;

import java.util.List;
import java.util.Map;

import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.lean.bean.CmsBean;
import com.bhartipay.lean.bean.DistributorIdAllot;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.report.bean.AgentBalDetailResponse;
import com.bhartipay.report.bean.AgentBalDetailsBean;
import com.bhartipay.report.bean.AgentClosingBalBean;
import com.bhartipay.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.report.bean.AgentCurrentSummary;
import com.bhartipay.report.bean.CommSummaryBean;
import com.bhartipay.report.bean.CustomerDetailsBean;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.report.bean.EscrowBean;
import com.bhartipay.report.bean.PartnerLedgerBean;
import com.bhartipay.report.bean.ReconciliationReport;
import com.bhartipay.report.bean.RefundTransactionBean;
import com.bhartipay.report.bean.ReportBean;
import com.bhartipay.report.bean.RevenueReportBean;
import com.bhartipay.report.bean.SenderClosingBalBean;
import com.bhartipay.report.bean.SenderProfileBean;
import com.bhartipay.report.bean.TravelTxn;
import com.bhartipay.report.bean.TxnReportByAdmin;
import com.bhartipay.report.bean.WalletHistoryBean;
import com.bhartipay.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.user.bean.AepsSettlementBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.wallet.aeps.AEPSAggregatorSettlement;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AEPSsettlement;
import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.bhartipay.wallet.matm.MATMLedger;

public interface ReportDao {

	@SuppressWarnings("rawtypes")
	public List wallettoBankReport(String userId);

	public List<RechargeTxnBean> rechargeReport(ReportBean reportBean);
	
	public List<BbpsPayment> bbpsReport(TxnInputBean inputBean);

	public List<TransactionBean> walletHistroyReport(WalletHistoryBean reportBean);

	public Map<String, String> getAggreator();
	
	public Map<String, String> getAggreatorList();
	
	public Map<String, String> getWLAggreator();

	public Map<String, String> getDistributerByAggId(String aggId);

	public Map<String, String> getSuperDistributerByAggId(String aggId);

	public Map<String, String> getDistributerBySuDistId(String aggId);

	public Map<String, String> getAgentByDistId(String distId);

	public Map<String, String> getAgentBySupDistId(String distId);

	public Map<String, String> getAgentByAggId(String distId);

	public List searchUser(String aggId, String dist, String agent, String subagentId);

	public List<SenderProfileBean> getSenderProfile(String userId);

	public Map<String, String> getSubAgentByAgentId(String agentId);

	public List<CustomerDetailsBean> getCustomerDetail(String aggid);

	public Map<String, String> getActiveAgentByDistId(String distId);
	
	public Map<String, String> getActiveAgentByAgg(String aggId);

	public Map<String, String> getActiveDistributerByAggId(String aggId);

	public Map<String, String> getSuperActiveDistributerByAggId(String aggId);

	public List getKycList(String aggId, String distId, String agentId, String subagentId);

	public List getSMSRepot(SMSSendDetails sMSSendDetails);

	public List<TxnReportByAdmin> getTxnReportByAdmin();

	public boolean acceptKyc(String refId);

	public boolean rejecttKyc(String refId);

	public Double getEscrowAccBal(String aggreatorid);

	public List<EscrowBean> getEscrowTxnList(EscrowBean escrowBean);

	public ReconciliationReport getPrePiadCardReConReport(String aggreatorid, String stDate, String endDate);

	public ReconciliationReport getRechargeReConReport(String aggreatorid, String stDate, String endDate);

	public ReconciliationReport getDMTReConReport(String aggreatorid, String stDate, String endDate);

	public ReconciliationReport getPGReConReport(String aggreatorid, String stDate, String endDate);

	public List<CommSummaryBean> getCommSummaryReport(String aggreatorid, String stDate, String endDate);

	public List<RechargeTxnBean> rechargeReportApp(String userId);

	public List<RevenueReportBean> getRevenueReportList(WalletMastBean walletMastBean);

	public List<RevenueReportBean> getCommissionSummary(WalletMastBean walletMastBean);

	public List<DmtDetailsMastBean> getDmtDetailsReportList(WalletMastBean walletMastBean);

	public List<AEPSLedger> getAepsReport(WalletMastBean walletMastBean);
	
	public List<AepsSettlementBean> getAepsPayoutReport(WalletMastBean walletMastBean);
	
	public List<MATMLedger> getMatmReport(WalletMastBean walletMastBean);

	public List<DmtDetailsMastBean> GetSupDistDmtDetailsReport(WalletMastBean walletMastBean);

	public List<AgentBalDetailsBean> getAgentBalDetail(String aggid);

	public AgentBalDetailResponse getAgentBalDetailBySuperDistributor(WalletMastBean walletBean);

	public List<AgentCurrentSummary> getAgentCurrentSummary(String distributerId);

	public Map<String, String> getAgentDister(String aggreatorId);

	public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(String agentId, String stDate, String endDate);

	public List<RechargeTxnBean> rechargeReportAggreator(ReportBean reportBean);
	
	public List<BbpsPayment> bbpsReportAggreator(ReportBean reportBean);
	
	public List<PaymentGatewayTxnBean> pgReportAggreator(ReportBean reportBean);

	public List<SenderClosingBalBean> getSenderClosingBalReport(String stDate);

	public List<RefundTransactionBean> getRefundTransactionReport(String stDate);

	public List<TravelTxn> getTravelTxn(/* String userId */ReportBean reportBean);

	public List<TravelTxn> getTravelTxn(String userId);

	public List<DmtDetailsMastBean> getCustomerDmtDetails(ReportBean reportBean);
	
	
	public List<AEPSLedger> getCustAepsReport(ReportBean reportBean);

	public List<BbpsPayment> getCustBBPSReport(ReportBean reportBean);

	public List<MATMLedger> getCustMATMReport(ReportBean reportBean);
	

	public List<B2CMoneyTxnMast> getCustDmtDetailsReport(ReportBean reportBean);

	public List<PartnerLedgerBean> getPartnerLedger(ReportBean reportBean);

	public List<AgentClosingBalBean> getAgentClosingBalReport(String stDate, String aggreatorid);

	public List<WalletTxnDetailsRpt> getWalletTxnDetailsReport(String stDate, String endDate, String aggreatorid);

	public List<B2CMoneyTxnMast> getBtocDmtReport(String stDate, String endDate, String aggreatorid);

	public List<EkycResponseBean> getKycReport(String stDate, String endDate, String aggreatorid);

	List<AEPSLedger> getAepsReportForAgent(WalletMastBean walletMastBean);
	
	List<AepsSettlementBean> getAepsPayoutReportForAgent(WalletMastBean walletMastBean);
	
	List<MATMLedger> getMatmReportForAgent(WalletMastBean walletMastBean);

	public Map<String, String> getSettledAggreator();
	
	
	public Map<String, String> getAepsSettlementCharge(String agentId);
	

	public Map<String, String> getTotalSettlementAmount(String aggregatorid);
	
	public Map<String, String> getTotalAepsTransactionAmount(String aggregatorid);

	public Map<String, String> postAggreatorSettlement(AEPSAggregatorSettlement aepsAggregatorSettlement);

	public Map<String, Object> postAepsSettlement(String aeAgentId);

	public Map<String, Object> postAepsSettlementData(String aeAgentId,String account);
	
	public Map<String, Object> isSaveAepsSettlement(AEPSsettlement aeps);
	
	public Map<String, Object> isSaveAepsSettlementAgent(AEPSsettlement aeps);
	
	// change by mani
	
	public List<LeanAccount> leanedAccountReport();
	public List<BbpsPayment> bbpsReportDistAggregator(WalletMastBean walletMastBean);
	
	public List<RechargeTxnBean> rechargeReportDist(WalletMastBean walletMastBean);
	
	public Map<String, String> getAllUser(String aggId);
	
	public List<LeanAccount> leanReport(String aggId);
	public Map<String, String> agentAccountList(String userId);
	public String validateAepsMerchant(AepsTokenRequest req);
	
	public Map<String,String> getManagerId(String distributorId,String aggId);
	public Map<String,String> getDistId(String distributorId,String aggId);
	public Map<String,String> getSoId(String distributorId,String aggId);
	public Map<String,String> getSoId(String aggId);
	public Map<String,String> getDistId(String aggId);
	public Map<String,String> getSdId(String sdid,String aggId);
	public Map<String,String> getSdId(String aggId);
	public Map<String,String> getSuperDist(String aggId);
	public Map<String,String> getSoDist(String aggId);
	public Map<String,String> getDistributorUnderSd(String id);
	public DistributorIdAllot getDistributorAllotid(String id, String aggId);
	public DistributorIdAllot updateTokenId(WalletMastBean mast);
	public Map<String, String> getAgentBySuper(String id,String aggId,int type);
	public List<AEPSLedger> getAepsReportSuper(WalletMastBean walletMastBean);
	public List<WalletMastBean> getUserEnable();
	public List<CmsBean> getCmsReportForAgent(WalletMastBean walletBean);
}
