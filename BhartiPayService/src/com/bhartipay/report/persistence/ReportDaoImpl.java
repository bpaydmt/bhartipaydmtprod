package com.bhartipay.report.persistence;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.lean.bean.CmsBean;
import com.bhartipay.lean.bean.DistributorIdAllot;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.mudra.bank3.payoutapi.PayoutApiMaster;
import com.bhartipay.mudra.bean.EkycResponseBean;
import com.bhartipay.report.bean.AgentBalDetailResponse;
import com.bhartipay.report.bean.AgentBalDetailsBean;
import com.bhartipay.report.bean.AgentClosingBalBean;
import com.bhartipay.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.report.bean.AgentCurrentSummary;
import com.bhartipay.report.bean.CommSummaryBean;
import com.bhartipay.report.bean.CustomerDetailsBean;
import com.bhartipay.report.bean.DmtDetailsMastBean;
import com.bhartipay.report.bean.EscrowBean;
import com.bhartipay.report.bean.PartnerLedgerBean;
import com.bhartipay.report.bean.ReconciliationDetailsBean;
import com.bhartipay.report.bean.ReconciliationReport;
import com.bhartipay.report.bean.ReconciliationSummaryBean;
import com.bhartipay.report.bean.RefundTransactionBean;
import com.bhartipay.report.bean.ReportBean;
import com.bhartipay.report.bean.RevenueReportBean;
import com.bhartipay.report.bean.SenderClosingBalBean;
import com.bhartipay.report.bean.SenderProfileBean;
import com.bhartipay.report.bean.TravelTxn;
import com.bhartipay.report.bean.TxnReportByAdmin;
import com.bhartipay.report.bean.WalletHistoryBean;
import com.bhartipay.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.transaction.bean.B2CMoneyTxnMast;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.TransactionBean;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.transaction.bean.WalletToBankTxnMast;
import com.bhartipay.user.bean.AepsSettlementBean;
import com.bhartipay.user.bean.WalletBalanceBean;
import com.bhartipay.user.bean.WalletKYCBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aeps.AEPSAggregatorSettlement;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AEPSsettlement;
import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.bhartipay.wallet.matm.MATMLedger;
import com.google.gson.Gson;

public class ReportDaoImpl implements ReportDao {

	private static final Logger logger = Logger.getLogger(ReportDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	private HttpServletRequest request;

	public List<WalletToBankTxnMast> wallettoBankReport(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "wallettoBankReport()");

		// logger.info("start excution ===================== method
		// wallettoBankReport(userId)"+userId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<WalletToBankTxnMast> results = null;
		try {
			Criteria cr = session.createCriteria(WalletToBankTxnMast.class);
			if (!(userId == ""))
				cr.add(Restrictions.eq("userId", userId));

			results = cr.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
					"Start Date" + "|" + results.size());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return results;

	}

	public List<RechargeTxnBean> rechargeReport(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "rechargeReport()" + "|" + reportBean.getAgentId());

		// logger.info("start excution ===================== method
		// rechargeReport(userId)"+reportBean.getUserId());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<RechargeTxnBean> results = new ArrayList<RechargeTxnBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(" from RechargeTxnBean where userId=:userId");
			if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty()) {
				serchQuery.append(" and DATE(rechargeDate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
			} else {

				serchQuery.append(" and DATE(rechargeDate) = STR_TO_DATE(REPLACE('" + formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
				// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
			}
			serchQuery.append(" order by DATE(rechargeDate) desc");

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			query.setString("userId", reportBean.getUserId());

			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in rechargeReportAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// rechargeReportAggreator*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method rechargeReport
		// Result "+results.size());
		return results;
	}

	
	
	public List<BbpsPayment> bbpsReport(TxnInputBean inputBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), inputBean.getAggreatorid(), "",
				inputBean.getUserId(), "", "", "rechargeReport()");

		// logger.info("start excution ===================== method
		// rechargeReport(userId)"+reportBean.getUserId());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<BbpsPayment> results = new ArrayList<BbpsPayment>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(" from BbpsPayment where agentid=:userId");
			if (inputBean.getStDate() != null && !inputBean.getStDate().isEmpty() && inputBean.getEndDate() != null
					&& !inputBean.getEndDate().isEmpty()) {
				serchQuery.append(" and DATE(requestdate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(inputBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(inputBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
			} else {

				serchQuery.append(" and DATE(requestdate) = STR_TO_DATE(REPLACE('" + formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
				// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
			}
			serchQuery.append(" order by DATE(requestdate) desc");

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), inputBean.getAggreatorid(), "",
					inputBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			query.setString("userId", inputBean.getUserId());

			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), inputBean.getAggreatorid(), "",
					inputBean.getUserId(), "", "", "problem in rechargeReportAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// rechargeReportAggreator*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), inputBean.getAggreatorid(), "",
				inputBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method rechargeReport
		// Result "+results.size());
		return results;
	}

	
	
	
	public List<TransactionBean> walletHistroyReport(WalletHistoryBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "RechargehistroyReports()" + "|" + reportBean.getAgentId());

		// logger.info("start excution ===================== method
		// rechargeReport(userId)"+reportBean.getUserId());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<TransactionBean> results = new ArrayList<TransactionBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(" from wallettransactionledger where userId=:userId");
			if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty()) {
				serchQuery.append(" and DATE(rechargeDate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
			} else {
				// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
			}
			serchQuery.append(" order by DATE(rechargeDate) desc");

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			query.setString("userId", reportBean.getUserId());

			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in WalletHistroyReports" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// rechargeReportAggreator*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method rechargeReport
		// Result "+results.size());
		return results;
	}

	public List<RechargeTxnBean> rechargeReportAggreator(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "rechargeReportAggreator()" + "|" + reportBean.getAgentId());

		// logger.info("start excution ===================== method
		// rechargeReportAggreator(userId)"+reportBean.getAggreatorid());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<RechargeTxnBean> results = new ArrayList<RechargeTxnBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if ("All".equalsIgnoreCase(reportBean.getAggreatorid())) {

				serchQuery.append(" from RechargeTxnBean where");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" DATE(rechargeDate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
				}
			} else {
				serchQuery.append(" from RechargeTxnBean where aggreatorid=:aggreatorid");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(rechargeDate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
				}
			}
			serchQuery.append("  ORDER BY STR_TO_DATE(rechargeDate, '%Y-%m-%d %H:%i:%s ') DESC");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			if (!"All".equalsIgnoreCase(reportBean.getAggreatorid())) {
				query.setString("aggreatorid", reportBean.getAggreatorid());
			}
			if (!(reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty())) {
				query.setMaxResults(20);
			}
			results = query.list();
			Collections.reverse(results);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in rechargeReportAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// rechargeReportAggreator*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method
		// rechargeReportAggreator Result "+results.size());
		return results;
	}

	
	
	public List<BbpsPayment> bbpsReportAggreator(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "bbpsReportAggreator()" + "|" + reportBean.getAgentId());


		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<BbpsPayment> results = new ArrayList<BbpsPayment>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if ("All".equalsIgnoreCase(reportBean.getAggreatorid())) {

				serchQuery.append(" from BbpsPayment where  status!='Cancel'");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(requestdate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
				}
			} else {
				serchQuery.append(" from BbpsPayment where aggreatorid=:aggreatorid and status!='Cancel'");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(requestdate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
				}
			}
			serchQuery.append("  ORDER BY STR_TO_DATE(requestdate, '%Y-%m-%d %H:%i:%s ') DESC");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			if (!"All".equalsIgnoreCase(reportBean.getAggreatorid())) {
				query.setString("aggreatorid", reportBean.getAggreatorid());
			}
			if (!(reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty())) {
				query.setMaxResults(20);
			}
			results = query.list();
			Collections.reverse(results);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in bbpsReportAggreator" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		return results;
	}


	public List<BbpsPayment> bbpsReportDistAggregator(WalletMastBean walletMastBean) {

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<BbpsPayment> results = new ArrayList<BbpsPayment>();
		try {
			if (walletMastBean.getUsertype() == 3) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Criteria criteria = session.createCriteria(BbpsPayment.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txndate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txndate", formatter.parse(walletMastBean.getStDate()), endDate));

				}
				if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null
						&& walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentid", agentList));
				} else {
					criteria.add(Restrictions.eq("agentid", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query ");

				//criteria.add(Restrictions.ne("status", "Pending"));
				results = criteria.list();
//			 }

			} else if (walletMastBean.getUsertype() != 2 && walletMastBean.getUsertype() != 3) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(BbpsPayment.class);

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null
						&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txndate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txndate", formatter.parse(walletMastBean.getStDate()), endDate));
					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggreatorid", walletMastBean.getAggreatorid()));

					results = criteria.list();
				} else {
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txndate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txndate", formatter.parse(walletMastBean.getStDate()), endDate));

					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggreatorid", walletMastBean.getAggreatorid()));
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query ");
					
					results = criteria.list();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;
		
	}
	
	

	
	public List<RechargeTxnBean> rechargeReportDist(WalletMastBean walletMastBean) {

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<RechargeTxnBean> results = new ArrayList<RechargeTxnBean>();
		try {
			if (walletMastBean.getUsertype() == 3) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Criteria criteria = session.createCriteria(RechargeTxnBean.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("rechargeDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("rechargeDate", formatter.parse(walletMastBean.getStDate()), endDate));

				}
				if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null
						&& walletMastBean.getId().equalsIgnoreCase("-1")  
						) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("userId", agentList));
				} else {
					 criteria.add(Restrictions.eq("userId", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query ");

				//criteria.add(Restrictions.ne("status", "Pending"));
				results = criteria.list();
//			 }

			} else if (walletMastBean.getUsertype() != 2 && walletMastBean.getUsertype() != 3) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(RechargeTxnBean.class);

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null
						&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("rechargeDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("rechargeDate", formatter.parse(walletMastBean.getStDate()), endDate));
					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggreatorid", walletMastBean.getAggreatorid()));

					results = criteria.list();
				} else {
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("rechargeDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("rechargeDate", formatter.parse(walletMastBean.getStDate()), endDate));

					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggreatorid", walletMastBean.getAggreatorid()));
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query ");
					
					results = criteria.list();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;
		
	}
	

	
	
	
	
	public List<PaymentGatewayTxnBean> pgReportAggreator(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "pgReportAggreator()" + "|" + reportBean.getAgentId());

		// logger.info("start excution ===================== method
		// rechargeReportAggreator(userId)"+reportBean.getAggreatorid());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<PaymentGatewayTxnBean> results = new ArrayList<PaymentGatewayTxnBean>();
		List<PaymentGatewayTxnBean> results1 = new ArrayList<PaymentGatewayTxnBean>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			if ("All".equalsIgnoreCase(reportBean.getAggreatorid())) {

				serchQuery.append(" from PaymentGatewayTxnBean where  trxResult!='Cancel'");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(pgtxnDate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					serchQuery.append(" and DATE(pgtxnDate)=STR_TO_DATE(REPLACE('" + formate.format(new Date())
							+ "', '/', '-'),'%d-%m-%Y')");
				}
			} else {
				serchQuery.append(" from PaymentGatewayTxnBean where aggreatorid=:aggreatorid and trxResult!='Cancel'");
				if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty()
						&& reportBean.getEndDate() != null && !reportBean.getEndDate().isEmpty()) {
					serchQuery.append(" and DATE(pgtxnDate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
					// serchQuery.append(" order by rechargeId ");
				} else {
					serchQuery.append(" and DATE(pgtxnDate)=STR_TO_DATE(REPLACE('" + formate.format(new Date())
							+ "', '/', '-'),'%d-%m-%Y')");
				}
			}
			serchQuery.append("  ORDER BY STR_TO_DATE(pgtxnDate, '%Y-%m-%d %H:%i:%s ') DESC");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			if (!"All".equalsIgnoreCase(reportBean.getAggreatorid())) {
				query.setString("aggreatorid", reportBean.getAggreatorid());
			}
			if (!(reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty())) {
				query.setMaxResults(20);
			}
			results = query.list();
				if(results.size()>0) {
					for(PaymentGatewayTxnBean res:results)
					{
						String txn = res.getTrxId();
						try {
							SQLQuery queryCharge = null;
							
							queryCharge = session.createSQLQuery("select resptxnid,txndebit from wallettransaction where txncode=181 and resptxnid=:resptxnid ");
							
							queryCharge.setString("resptxnid", txn);
							List<Object[]> rows = queryCharge.list();
							if(rows.size()>0) {
								for (Object[] row : rows) {
								  res.setTrxCurrency(""+row[1].toString());
								}
							}else
								 res.setTrxCurrency("NA");
								
							results1.add(res);
						} catch (Exception e) {
							e.printStackTrace();
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
									"problem in get PG charge from wallettransaction " + e.getMessage() + " " + e);
							 
						}
						
					}
				}
				
			Collections.reverse(results1);
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in pgReportAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// rechargeReportAggreator*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method
		// rechargeReportAggreator Result "+results.size());
		return results1;
	}

	public List<RechargeTxnBean> rechargeReportApp(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "rechargeReportApp()");
		// logger.info("start excution ===================== method
		// rechargeReport(userId)"+userId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Criteria cr = session.createCriteria(RechargeTxnBean.class);
		if (!(userId == ""))
			cr.add(Restrictions.eq("userId", userId));
		cr.addOrder(Order.desc("rechargeId"));
		cr.setMaxResults(20);

		List<RechargeTxnBean> results = cr.list();
		// Collections.reverse( results);
		session.close();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
				"+results size" + results.size());
		// logger.info("start excution ===================== method rechargeReport
		// Result "+results.size());
		return results;
	}

	public List<CustomerDetailsBean> getCustomerDetail(String aggid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "", "getCustomerDetail()");
		// logger.info("start excution
		// ******************************************************** method
		// getCustomerDetail()"+aggid);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<CustomerDetailsBean> results = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT id,mobileno,a.walletid,emailid,NAME,userstatus,kycstatus,finalBalance,DATE_FORMAT(lastlogintime, '%d/%m/%Y') AS lastlogintime   FROM walletmast a, walletbalance b WHERE  a.walletid=b.walletid  and aggreatorid=:aggid and distributerid='-1' and agentid='-1'");
			query.setParameter("aggid", aggid);
			query.setResultTransformer(Transformers.aliasToBean(CustomerDetailsBean.class));
			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "",
					"| problem in getCustomerDetail " + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getCustomerDetail*****************************************" + e.getMessage(),
			// e);

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "",
				"+results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;
	}

	public List<TxnReportByAdmin> getTxnReportByAdmin() {

		System.out.println("------reportDaoImpl.java-------method----List<TxnReportByAdmin> getTxnReportByAdmin()");

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getTxnReportByAdmin()");
		// logger.info("start excution
		// ******************************************************** method
		// getTxnReportByAdmin()");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<TxnReportByAdmin> results = null;
		try {
			// SQLQuery query=session.createSQLQuery("SELECT a.aggreatorid AS aggreatorid,''
			// AS txndesc,DATE_FORMAT(DATE(b.txndate),'%d/%m/%Y') AS txndate,SUM(txncredit)
			// AS txncredit,SUM(txndebit) AS txndebit FROM walletmast a INNER JOIN
			// wallettransaction b ON a.walletid=b.walletid INNER JOIN transactionmastercode
			// c ON b.txncode=c.txnid GROUP BY a.aggreatorid,c.txndesc,DATE(b.txndate) order
			// by txndate desc");
			// SQLQuery query=session.createSQLQuery("SELECT a.aggreatorid AS aggreatorid,''
			// AS txndesc,DATE_FORMAT(DATE(b.txndate),'%d/%m/%Y') AS txndate,SUM(txncredit)
			// AS txncredit,SUM(txndebit) AS txndebit FROM walletmast a INNER JOIN
			// wallettransaction b ON a.walletid=b.walletid AND txndate BETWEEN CURDATE() -
			// INTERVAL 3 DAY AND CURDATE() GROUP BY a.aggreatorid,DATE(b.txndate) ORDER BY
			// txndate DESC");
			SQLQuery query = session.createSQLQuery(
					"SELECT A.aggreatorid ,A.txndate ,SUM(A.txncredit) AS 'txncredit' ,SUM(A.txndebit) AS 'txndebit' FROM ( SELECT a.aggreatorid AS aggreatorid ,'' AS txndesc ,DATE_FORMAT(DATE (b.txndate), '%d/%m/%Y') AS txndate ,txncredit AS txncredit ,txndebit AS txndebit FROM walletmast a INNER JOIN wallettransaction b ON a.walletid = b.walletid AND txndate BETWEEN CURDATE() - INTERVAL 3 DAY AND CURDATE() ) A GROUP BY A.aggreatorid ,A.txndesc ,DATE (A.txndate) ORDER BY txndate DESC");
			query.setResultTransformer(Transformers.aliasToBean(TxnReportByAdmin.class));

			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getTxnReportByAdmin" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getTxnReportByAdmin*****************************************" +
			// e.getMessage(), e);

			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"+results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;
	}

	/*
	 * public PGPayeeBean profilebyPGtxnId(String txnId){ logger.
	 * info("start excution ===================== method profilebytxnId(txnId)"
	 * +txnId); factory=DBUtil.getSessionFactory(); Session session =
	 * factory.openSession(); Transaction transaction=session.beginTransaction();
	 * PGPayeeBean gGPayeeBean=null; try{ SQLQuery query=session.
	 * createSQLQuery("SELECT a.txnid AS txnid,txnamount,walletid,mobileno,paytxnid,merchantid,merchantuser,amount,merchanttxnid,callbackurl FROM paymentgatewaytxn a ,merchanttxnmast b WHERE a.paytxnid=b.txnid and a.txnid=:txn"
	 * ); query.setString("txn", txnId);
	 * query.setResultTransformer(Transformers.aliasToBean(PGPayeeBean.class));
	 * gGPayeeBean=(PGPayeeBean)query.list().get(0); }catch(Exception e){
	 * logger.debug("problem in Add  PG Txn=========================" +
	 * e.getMessage(), e);
	 * 
	 * e.printStackTrace(); transaction.rollback(); } finally { session.close(); }
	 * return gGPayeeBean; }
	 */

	public Map<String, String> getAggreator() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getAggreator()");
		// logger.info("Start excution ===================== method getAggreator");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=4 and id like 'OAGG%' ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				aggMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggreator==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);

	}


	public Map<String, String> getAggreatorList() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getAggreator()");
		// logger.info("Start excution ===================== method getAggreator");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=4 and id like 'OAGG%' ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				aggMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggreator==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);

	}

	
	
	public Map<String, String> getWLAggreator() {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getWLAggreator()");
		// logger.info("Start excution ===================== method getAggreator");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype = 4 and whitelabel = 1");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				aggMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getWLAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggreator==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);

	}

	public Map<String, String> getSuperDistributerByAggId(String aggId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"getSuperDistributerByAggId()");
		// logger.info("Start excution ===================== method
		// getDistributerByAggId(String aggId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> distMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE   usertype=7 and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				distMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in getAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggreator==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(distMap);

	}

	public Map<String, String> getDistributerByAggId(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"getDistributerByAggId()");
		// logger.info("Start excution ===================== method
		// getDistributerByAggId(String aggId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> distMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE   usertype=3 and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				distMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in getAggreator" + e.getMessage() + " " + e);
			// logger.debug("problem in getAggreator==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(distMap);

	}

	public Map<String, String> getDistributerBySuDistId(String superDistributor) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getDistributerBySuDistId()" + "|SuperDis." + superDistributor);
		logger.info("Start excution ===================== method getDistributerBySuDistId(String aggId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> distMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE   usertype=3 and superdistributerid=:superdistributerid ");
			query.setString("superdistributerid", superDistributor);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				distMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getDistributerBySuDistId" + e.getMessage() + " " + e);
			// logger.debug("problem in getDistributerBySuDistId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(distMap);

	}

	public Map<String, String> getSuperActiveDistributerByAggId(String aggId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"getSuperActiveDistributerByAggId()");
		// logger.info("Start excution ===================== method
		// getActiveDistributerByAggId(String aggId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> distMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE   usertype=7 and userstatus='A' and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				distMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in getActiveDistributerByAggId" + e.getMessage() + " " + e);
			// logger.debug("problem in getActiveDistributerByAggId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(distMap);

	}

	public Map<String, String> getActiveDistributerByAggId(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"getActiveDistributerByAggId()");
		// logger.info("Start excution ===================== method
		// getActiveDistributerByAggId(String aggId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> distMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE   usertype=3 and userstatus='A' and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				distMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in getActiveDistributerByAggId" + e.getMessage() + " " + e);
			// logger.debug("problem in getActiveDistributerByAggId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(distMap);

	}

	public Map<String, String> getAgentDister(String aggreatorId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", "getAgentDister()");
		// logger.info("Start excution ===================== method
		// getAgentDister(String aggreatorId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE  usertype in (2,3,6) and aggreatorid=:aggreatorId ");
			query.setString("aggreatorId", aggreatorId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "",
					"problem in getAgentDister" + e.getMessage() + " " + e);
			// logger.debug("problem in getAgentDister==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	public Map<String, String> getAgentByDistId(String distId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentByDistId()" + "|distId " + distId);
		// logger.info("Start excution ===================== method
		// getAgentByDistId(String distId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session
					.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and distributerid=:distId ");
			query.setString("distId", distId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in distributerid" + e.getMessage() + " " + e);
			// logger.debug("problem in distributerid==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	
	
	public Map<String, String> getAllUser(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentByDistId()" + "|distId " + "");
		
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session
					.createSQLQuery("SELECT id,NAME,walletid,aggreatorid FROM walletmast WHERE  usertype in(2,3,4) and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				agentMap.put(row[0].toString(), row[0].toString());  // + "-" + row[1].toString()
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in distributerid" + e.getMessage() + " " + e);
			
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	
	
	public Map<String, String> getAgentByAggId(String aggid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentByAggId()" + "|distId " + aggid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and aggreatorid=:aggid ");
			query.setString("aggid", aggid);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getAgentByAggId" + e.getMessage() + " " + e);
			// logger.debug("problem in distributerid==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	public Map<String, String> getAgentBySupDistId(String supdistId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentBySupDistId()" + "|distId " + supdistId);
		// logger.info("Start excution ===================== method
		// getAgentByDistId(String distId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE  usertype=2 and superdistributerid=:sudistid ");
			query.setString("sudistid", supdistId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getAgentBySupDistId" + e.getMessage() + " " + e);
			// logger.debug("problem in distributerid==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	public Map<String, String> getActiveAgentByDistId(String distId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getActiveAgentByDistId()" + "|distId " + distId);
		// logger.info("Start excution ===================== method
		// getActiveAgentByDistId(String distId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE  usertype=2 and userstatus='A' and distributerid=:distId ");
			query.setString("distId", distId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			// logger.debug("problem in getActiveAgentByDistId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	
	public Map<String, String> getManagerId(String mId, String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getManagerId()" + "|mId " + mId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			  query = session.createSQLQuery( "SELECT id,NAME FROM walletmast WHERE  usertype=9 and userstatus='A' and aggreatorid=:aggId ");	
			query.setString("aggId", aggId);
			//query.setString("distId", mId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			// logger.debug("problem in getActiveAgentByDistId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	
	
	
	
	public Map<String, String> getDistId(String dId , String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getDistId()" + "|distId " + dId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			if("BPMU001000".equalsIgnoreCase(dId)) {
				query = session.createSQLQuery( "SELECT id,NAME FROM walletmast WHERE  usertype=3 and userstatus='A'  and aggreatorid=:aggId ");
			}else {
			query = session.createSQLQuery( "SELECT id,NAME FROM walletmast WHERE  usertype=3 and userstatus='A' and aggreatorid=:aggId and managerId=(select managerId from walletmast where id=:id) ");
			query.setString("id", dId);
			}
			query.setString("aggId", aggId);
			//query.setString("distId", dId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
		 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	public Map<String, String> getDistId( String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getDistId()" + " ");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery( "SELECT id,NAME FROM walletmast WHERE  usertype=3 and userstatus='A'  and aggreatorid=:aggId ");
			 
			query.setString("aggId", aggId);
			//query.setString("distId", dId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
		 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	
	public Map<String, String> getSdId(String subAgg , String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getActiveAgentByDistId()" + "|distId " + subAgg);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			if("BPMU001000".equalsIgnoreCase(subAgg)) {
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=7 and userstatus='A' and aggreatorid=:aggId ");
			}else
			{
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=7 and userstatus='A' and aggreatorid=:aggId and salesId=(select salesId from walletmast where id=:id) ");	
			query.setString("id", subAgg);
			}				
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	public Map<String, String> getSdId( String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getActiveAgentByDistId()" );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=7 and userstatus='A' and aggreatorid=:aggId ");
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	
	public Map<String, String> getSoId(String subAgg , String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getActiveAgentByDistId()" + "|distId " + subAgg);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			if("BPMU001000".equalsIgnoreCase(subAgg)) {
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=8 and userstatus='A' and aggreatorid=:aggId ");
			}else
			{
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=8 and userstatus='A' and aggreatorid=:aggId and managerId=(select managerId from walletmast where id=:id) ");	
			query.setString("id", subAgg);
			}				
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	public Map<String, String> getSoId(String aggId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getSoId()" + "AggId "+aggId );
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			 
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=8 and userstatus='A' and aggreatorid=:aggId ");
			 				
			query.setString("aggId", aggId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	
	
	
	
	public Map<String, String> getActiveAgentByAgg(String aggId){


		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getActiveAgentByAgg()" + "|distId " + aggId);
		// logger.info("Start excution ===================== method
		// getActiveAgentByDistId(String distId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			if("OAGG001050".equalsIgnoreCase(aggId)) {
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE  usertype=2 and userstatus='A' and id like 'MERCSPAY%'");
			}else {
				query = session.createSQLQuery(
						"SELECT id,NAME FROM walletmast WHERE  usertype=2 and userstatus='A' and aggreatorid=:aggId and id like 'MERCSPAY%'");
				query.setString("aggId", aggId);
			}
			
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getActiveAgentByAgg" + e.getMessage() + " " + e);
			// logger.debug("problem in getActiveAgentByDistId==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	
	}
	
	public Map<String, String> getSubAgentByAgentId(String agentId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getSubAgentByAgentId()" + "|agentId " + agentId);
		logger.info("Start excution ===================== method getSubAgentByAgentId(String agentId)");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> subAgentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT id,NAME FROM walletmast WHERE  usertype=5 and agentid=:agentId  and userstatus='A'");
			query.setString("agentId", agentId);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				subAgentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in distributerid" + e.getMessage() + " " + e);
			// logger.debug("problem in distributerid==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(subAgentMap);
	}

	public List searchUser(String aggId, String distId, String agentId, String subagentId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"searchUser()" + "|agentId " + agentId + "|distId " + distId + "|subagentId " + subagentId);
		/*
		 * Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","",
		 * "", "",methodname +"|distId "+distId ); Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),aggId,"","", "", "",methodname
		 * +"|subagentId "+subagentId );
		 */
		/*
		 * logger.
		 * info("Start excution ===================== method searchUser(aggId,dist,agent,subagentId)--aggId"
		 * +aggId); logger.
		 * info("Start excution ===================== method searchUser(aggId,dist,agent,subagentId)--distId"
		 * +distId); logger.
		 * info("Start excution ===================== method searchUser(aggId,dist,agent,subagentId)--agentId"
		 * +agentId); logger.
		 * info("Start excution ===================== method searchUser(aggId,dist,agent,subagentId)--subagentId"
		 * +subagentId);
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<WalletMastBean> userList = null;
		try {
			Query query = null;
			if (aggId.equals("-1")) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "", "Show Agg List");
				// logger.info("Start excution ===================== Show Agg List");
				query = session.createQuery("FROM WalletMastBean WHERE usertype=4 ");
			} else if (!(aggId.equals("-1")) && (distId.equals("-1")) && (agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
						"Show Dist List by agg id");
				// logger.info("Start excution ===================== Show Dist List by agg id");
				query = session.createQuery("FROM WalletMastBean WHERE usertype=3 and aggreatorid=:aggId ");
				query.setString("aggId", aggId);
			} else if (!(aggId.equals("-1")) && !(distId.equals("-1")) && (agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
						"Show Agent List  List by Dist id");
				// logger.info("Start excution ===================== Show Agent List List by
				// Dist id");
				query = session.createQuery("FROM WalletMastBean WHERE usertype=2 and distributerid=:distId ");
				query.setString("distId", distId);
			} else if (!(aggId.equals("-1")) && !(distId.equals("-1")) && !(agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				query = session.createQuery("FROM WalletMastBean WHERE  usertype in (1,5) and agentid=:agentId ");
				query.setString("agentId", agentId);
			} else {
				query = session.createQuery("FROM WalletMastBean WHERE  usertype =1 and subAgentId=:subagentId ");
				query.setString("subagentId", subagentId);
			}
			userList = query.list();

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in searchUser" + e.getMessage() + " " + e);
			// logger.debug("problem in searchUser==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return userList;
	}

	public List getKycList(String aggId, String distId, String agentId, String subagentId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"getKycList()" + "|agentId " + agentId + "|distId " + distId + "|subagentId " + subagentId);
		/*
		 * Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggId,"","",
		 * "", "",methodname +"|distId "+distId ); Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),aggId,"","", "", "",methodname
		 * +"|subagentId "+subagentId );
		 */
		/*
		 * logger.
		 * info("Start excution ============1========= method getKycList(aggId,dist,agent,subagentId)--aggId"
		 * +aggId); logger.
		 * info("Start excution =============2======== method getKycList(aggId,dist,agent,subagentId)--distId"
		 * +distId); logger.
		 * info("Start excution ==============3======= method getKycList(aggId,dist,agent,subagentId)--agentId"
		 * +agentId); logger.
		 * info("Start excution ===============4====== method getKycList(aggId,dist,agent,subagentId)--subagentId"
		 * +subagentId);
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<WalletKYCBean> userList = null;
		try {
			Query query = null;
			if (aggId.equals("-1")) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "", "Show Agg List");
				// logger.info("Start excution ===================== Show Agg List");
				query = session.createQuery(
						"FROM WalletKYCBean WHERE UserId in (select id FROM WalletMastBean WHERE usertype=4) and status='Pending' ");
			} else if (!(aggId.equals("-1")) && (distId.equals("-1")) && (agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
						"Show Dist List by agg id");
				// logger.info("Start excution ===================== Show Dist List by agg id");
				query = session.createQuery(
						"FROM WalletKYCBean WHERE UserId in (select id FROM WalletMastBean WHERE usertype in (3,1) and aggreatorid=:aggId and distributerid='-1' ) and status='Pending'");
				query.setString("aggId", aggId);
			} else if (!(aggId.equals("-1")) && !(distId.equals("-1")) && (agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
						"Show Agent List  List by Dist id");
				// logger.info("Start excution ===================== Show Agent List List by
				// Dist id");
				query = session.createQuery(
						"FROM WalletKYCBean WHERE UserId in (select id FROM WalletMastBean WHERE usertype=2 and distributerid=:distId ) and status='Pending'");
				query.setString("distId", distId);
			} else if (!(aggId.equals("-1")) && !(distId.equals("-1")) && !(agentId.equals("-1"))
					&& (subagentId.equals("-1"))) {
				query = session.createQuery(
						"FROM WalletKYCBean WHERE UserId in (select id FROM WalletMastBean WHERE usertype in (1,5) and agentid=:agentId ) and status='Pending'");
				query.setString("agentId", agentId);
			} else if (!(aggId.equals("-1")) && !(distId.equals("-1")) && !(agentId.equals("-1"))
					&& !((subagentId.equals("-1")))) {
				query = session.createQuery(
						"FROM WalletKYCBean WHERE UserId in (select id FROM WalletMastBean WHERE  usertype =1 and subAgentId=:subagentId ) and status='Pending'");
				query.setString("subagentId", subagentId);
			} else {
				query = session.createQuery(
						"FROM WalletKYCBean WHERE WHERE UserId in (select id FROM WalletMastBean WHERE usertype=1 and aggreatorid=:aggId and distributerid='-1' ) and status='Pending' ");
				query.setString("aggId", aggId);
			}
			userList = query.list();

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
					"problem in getKycList==================" + e.getMessage() + " " + e);
			// logger.debug("problem in getKycList==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggId, "", "", "", "",
				"+userList size" + userList.size());
		// logger.info("Start excution ====================2323=
		// ===================================="+userList.size());
		return userList;
	}

	public boolean acceptKyc(String refId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag = false;

		try {
			Transaction transaction = session.beginTransaction();
			WalletKYCBean walletKYCBean = (WalletKYCBean) session.get(WalletKYCBean.class, refId);
			walletKYCBean.setStatus("Accepted");
			session.saveOrUpdate(walletKYCBean);

			// logger.info("*******************************UserId*****************************"+walletKYCBean.getUserId());
			WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
					walletKYCBean.getUserId());
			// logger.info("*******************************getKycstatus*****************************"+walletMastBean.getKycstatus());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", walletKYCBean.getUserId(), "", "",
					"acceptKyc()" + "|" + walletMastBean.getKycstatus());
			walletMastBean.setKycstatus("A");
			session.saveOrUpdate(walletMastBean);
			transaction.commit();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in acceptKyc" + e.getMessage() + " " + e);
			// logger.debug("problem in acceptKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}

	public boolean rejecttKyc(String refId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		boolean flag = false;
		try {
			Transaction transaction = session.beginTransaction();
			WalletKYCBean walletKYCBean = (WalletKYCBean) session.get(WalletKYCBean.class, refId);
			walletKYCBean.setStatus("Rejected");
			session.saveOrUpdate(walletKYCBean);
			transaction.commit();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in rejecttKyc" + e.getMessage() + " " + e);
			// logger.debug("problem in rejecttKyc==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return flag;
	}

//SELECT txndesc,SUM(txncredit) AS credit ,SUM(txndebit) AS debit FROM wallettransaction a INNER JOIN transactionmastercode b ON a.txncode=b.txnid AND b.Escrow=1  AND aggreatorid ='AGGR001035'  AND DATE(txnDate) BETWEEN STR_TO_DATE(REPLACE('14/11/2016', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('14/12/2016', '/', '-'),'%d-%m-%Y') GROUP BY txndesc

	public List<EscrowBean> getEscrowTxnList(EscrowBean escrowBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), escrowBean.getAggreatorid(), "", "", "", "",
				"getEscrowTxnList()" + "Start Date" + escrowBean.getStDate() + "End Date " + escrowBean.getEndDate());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),escrowBean.getAggreatorid(),"","", "",
		 * "","Start Date"+escrowBean.getStDate() ); Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),escrowBean.getAggreatorid(),"","", "",
		 * "","End Date "+escrowBean.getEndDate() );
		 */
		/*
		 * logger.
		 * info("Start excution ===================== method getEscrowTxnList(escrowBean)"
		 * +escrowBean.getAggreatorid());
		 * logger.info("Start excution ===================== method stdate"+escrowBean.
		 * getStDate());
		 * logger.info("Start excution ===================== method endDate"+escrowBean.
		 * getEndDate());
		 */
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<EscrowBean> escrowTxnList = new ArrayList<EscrowBean>();
		if (escrowBean.getAggreatorid() == null || escrowBean.getAggreatorid().isEmpty()) {
			return escrowTxnList;
		} else {
			try {

				StringBuilder serchQuery = new StringBuilder();
				serchQuery.append(
						"SELECT txndesc,SUM(txncredit) AS credit ,SUM(txndebit) AS debit FROM wallettransaction a INNER JOIN transactionmastercode b ON a.txncode=b.txnid AND b.Escrow=1  AND");
				serchQuery.append(" aggreatorid ='" + escrowBean.getAggreatorid() + "'");
				if (!(escrowBean.getStDate() == null || escrowBean.getStDate().isEmpty())
						&& !(escrowBean.getEndDate() == null || escrowBean.getEndDate().isEmpty())) {
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
					serchQuery.append(" AND DATE(txnDate) BETWEEN STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(escrowBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(escrowBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y') ");
				}
				serchQuery.append(" GROUP BY txndesc");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), escrowBean.getAggreatorid(), "", "", "",
						"", "serchQuery " + serchQuery);
				// logger.info("************getEscrowTxnList********serchQuery*************************"+serchQuery);
				EscrowBean escrowBeanR = null;
				Query query = session.createSQLQuery(serchQuery.toString());
				List<Object[]> rows = query.list();
				for (Object[] row : rows) {
					escrowBeanR = new EscrowBean();
					escrowBeanR.setTxndesc(row[0].toString());
					escrowBeanR.setCredit((Double) row[1]);
					escrowBeanR.setDebit((Double) row[2]);
					escrowTxnList.add(escrowBeanR);
				}
			} catch (Exception e) {
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), escrowBean.getAggreatorid(), "", "", "",
						"", "problem in getEscrowTxnList" + e.getMessage() + " " + e);
				// logger.debug("problem in getEscrowTxnList==================" +
				// e.getMessage(), e);
			} finally {
				session.close();
			}
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), escrowBean.getAggreatorid(), "", "", "", "",
				"escrowTxnList.size() " + escrowTxnList.size());
		// logger.info("Start excution ===================== getEscrowTxnList
		// List"+escrowTxnList.size());
		return escrowTxnList;
	}

	public List<SenderProfileBean> getSenderProfile(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "getSenderProfile()");
		// logger.info("Start excution ===================== method
		// getSenderProfile(userId)"+userId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<SenderProfileBean> senderProfile = new ArrayList<SenderProfileBean>();
		try {
			Query query = null;

			if (userId == null || userId.isEmpty()) {
				query = session.createSQLQuery(
						"SELECT userid,payermobile,payername,payerEmail ,SUM(amount) AS amount FROM wallettobanktxnmast  GROUP BY userid,payermobile  ORDER BY userid,payername");
			} else {
				query = session.createSQLQuery(
						"SELECT userid,payermobile,payername,payerEmail ,SUM(amount) AS amount FROM wallettobanktxnmast where userid=:userid  GROUP BY userid,payermobile ORDER BY userid,payername ");
				query.setString("userid", userId);
			}
			SenderProfileBean senderProfileBean = null;
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				senderProfileBean = new SenderProfileBean();

				senderProfileBean.setUserid(row[0].toString());
				senderProfileBean.setPayermobile(row[1].toString());
				senderProfileBean.setPayerEmail(row[2].toString());
				senderProfileBean.setPayername(row[3].toString());
				senderProfileBean.setAmount(Double.parseDouble(row[4].toString()));
				senderProfile.add(senderProfileBean);
			}

		} catch (Exception e) {
			e.printStackTrace();

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
					"problem in getSenderProfile" + e.getMessage() + " " + e);
			// logger.debug("problem in getSenderProfile==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "",
				"senderProfile size" + senderProfile.size());
		// logger.info("Start excution ===================== senderProfile
		// List"+senderProfile.size());
		return senderProfile;
	}

	public List getSMSRepot(SMSSendDetails sMSSendDetails) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), sMSSendDetails.getAggreatorid(),
				sMSSendDetails.getWalletid(), "", "", "",
				"getSMSRepot" + "Start Date" + sMSSendDetails.getStDate() + "End Date" + sMSSendDetails.getEndDate());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),sMSSendDetails.getAggreatorid(),sMSSendDetails.
		 * getWalletid(),"", "", "", "Start Date"+sMSSendDetails.getStDate());
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),sMSSendDetails.getAggreatorid(),sMSSendDetails.
		 * getWalletid(),"", "", "", "End Date"+sMSSendDetails.getEndDate());
		 */
		/*
		 * logger.
		 * info("start excution ===========getAggreatorid========== method getSMSRepot(sMSSendDetails)"
		 * +sMSSendDetails.getAggreatorid()); logger.
		 * info("start excution ===========getStDate========== method getSMSRepot(sMSSendDetails)"
		 * +sMSSendDetails.getStDate()); logger.
		 * info("start excution ===========getEndDate========== method getSMSRepot(sMSSendDetails)"
		 * +sMSSendDetails.getEndDate());
		 */
		List<SMSSendDetails> smsRepot = new ArrayList<SMSSendDetails>();
		List<SMSSendDetails> smsRepot1 = new ArrayList<SMSSendDetails>();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(" from SMSSendDetails where aggreatorid=:aggreatorid");
			if (sMSSendDetails.getStDate() != null && !sMSSendDetails.getStDate().isEmpty()
					&& sMSSendDetails.getEndDate() != null && !sMSSendDetails.getEndDate().isEmpty()) {
				serchQuery.append(" and DATE(entryon) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(sMSSendDetails.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(sMSSendDetails.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
			} else {
				// serchQuery.append(" LIMIT 200");
			}
			serchQuery.append(" order by entryon DESC");
			System.out.println("~~~~" + serchQuery);
			Query query = session.createQuery(serchQuery.toString());
			query.setString("aggreatorid", sMSSendDetails.getAggreatorid());
			smsRepot = query.list();
			Iterator<SMSSendDetails> ite = smsRepot.iterator();

			while (ite.hasNext()) {
				SMSSendDetails sendDetails = ite.next();
				Date entryOn = sendDetails.getEntryon();
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String fDate = dt1.format(entryOn);
				sendDetails.setStDate(fDate);
				smsRepot1.add(sendDetails);
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), sMSSendDetails.getAggreatorid(),
					sMSSendDetails.getWalletid(), "", "", "",
					"SMS report" + ((SMSSendDetails) smsRepot.get(0)).getEntryon());
			// System.out.println("*********************SMS report*********************"+
			// ((SMSSendDetails)smsRepot.get(0)).getEntryon());

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), sMSSendDetails.getAggreatorid(),
					sMSSendDetails.getWalletid(), "", "", "", "Problem in sMSSendDetails" + e.getMessage() + " " + e);
			e.printStackTrace();
			transaction.rollback();
			System.out.println(e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), sMSSendDetails.getAggreatorid(),
				sMSSendDetails.getWalletid(), "", "", "", "smsRepotsize" + smsRepot.size());
		// logger.info("stop excution ====================getSMSRepot===
		// return===="+smsRepot.size());
		return smsRepot1;
	}

	public Double getEscrowAccBal(String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getEscrowAccBal()");
		// logger.info("Start excution ===================== method
		// getEscrowAccBal(aggId)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Double escrowAccBal = 0.0;
		try {
			escrowAccBal = (Double) session.createSQLQuery(
					"select sum(txncredit)- sum(txndebit) from wallettransaction a inner join transactionmastercode b on a.txncode=b.txnid and b.Escrow=1 and aggreatorid=:aggreatorid ")
					.setString("aggreatorid", aggreatorid).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getEscrowAccBal" + e.getMessage() + " " + e);
			// logger.debug("problem in getEscrowAccBal==================" + e.getMessage(),
			// e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"escrowAccBal" + escrowAccBal);
		// logger.info("Start excution ===================== getEscrowAccBal
		// "+escrowAccBal);
		return escrowAccBal;
	}

//SELECT txnid, DATE_FORMAT(txnDate, '%d/%m/%y %H:%i:%s') txnDate,userid,aggreatorid,txnAmount, aggamount , distamount, agentamount, subagentamount, payoutamount FROM commsummary	

	public List<CommSummaryBean> getCommSummaryReport(String aggreatorid, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getCommSummaryReport()");
		// logger.info("Start excution ===================== method
		// getCommSummaryReport(String aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<CommSummaryBean> commSummaryList = new ArrayList<CommSummaryBean>();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			StringBuilder commSummaryquery = new StringBuilder(
					"SELECT txnid, DATE_FORMAT(txnDate, '%d/%m/%y %H:%i:%s') txnDate,userid,aggreatorid,txnAmount, aggamount , distamount, agentamount, subagentamount, payoutamount FROM commsummary where aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				commSummaryquery.append(" AND DATE(txnDate)=CURDATE()-INTERVAL 1 DAY");
			} else {
				commSummaryquery.append(
						" AND DATE(txnDate) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"commSummaryquery " + commSummaryquery.toString());
			// logger.info("****** query ===================== prePiadquery
			// "+commSummaryquery.toString());
			SQLQuery query = session.createSQLQuery(commSummaryquery.toString());
			query.setString("aggreatorid", aggreatorid);
			query.setResultTransformer(Transformers.aliasToBean(CommSummaryBean.class));
			commSummaryList = query.list();

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getCommSummaryReport" + e.getMessage() + " " + e);
			// logger.debug("problem in getCommSummaryReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getCommSummaryReport " + commSummaryList);
		// logger.info("completed excution ===================== getCommSummaryReport
		// ");
		return commSummaryList;
	}

	public ReconciliationReport getPrePiadCardReConReport(String aggreatorid, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getPrePiadCardReConReport()");
		// logger.info("Start excution ===================== method
		// getPrePiadCardReConReport(String aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		ReconciliationReport prePiadCardReCon = new ReconciliationReport();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			ReconciliationSummaryBean reconciliationSummaryBean = new ReconciliationSummaryBean();
			SQLQuery query = null;

			StringBuilder prePiadquery = new StringBuilder(
					"SELECT COUNT(*) as txnCout ,IFNULL(SUM(transaction_amount),0) as txnAmount FROM prepaidreconview WHERE STATUS='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				prePiadquery.append(" AND DATE(settlement_date)=CURDATE()-INTERVAL 1 DAY");
			} else {
				prePiadquery.append(" AND DATE(settlement_date) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadquery" + prePiadquery.toString());
			// logger.info("****** query ===================== prePiadquery
			// "+prePiadquery.toString());
			query = session.createSQLQuery(prePiadquery.toString());
			query.setString("aggreatorid", aggreatorid);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				reconciliationSummaryBean.setTotalTxn(Integer.parseInt(row[0].toString()));
				// String totAmount=row[1].toString();
				if (row[1] == null) {
					reconciliationSummaryBean.setTotalAmount(0.0);
				} else {
					reconciliationSummaryBean.setTotalAmount(Double.parseDouble(row[1].toString()));
				}
			}

			StringBuilder prePiadDetailsquery = new StringBuilder(
					"SELECT aggreatorid,id,prepaidcardnumber,txnreqid,card_id,user_id,transaction_ref_no,transaction_amount,merchant_name,merchant_id,terminal_id,channel,transaction_type,cashback_amount FROM prepaidreconview WHERE STATUS='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				prePiadDetailsquery.append(" AND DATE(settlement_date)=CURDATE()-INTERVAL 1 DAY");
			} else {
				prePiadDetailsquery.append(" AND DATE(settlement_date) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadquery " + prePiadquery.toString());
			// logger.info("****** query ===================== prePiadquery
			// "+prePiadquery.toString());

			query = session.createSQLQuery(prePiadDetailsquery.toString());
			query.setString("aggreatorid", aggreatorid);
			query.setResultTransformer(Transformers.aliasToBean(ReconciliationDetailsBean.class));
			List<ReconciliationDetailsBean> prePiadDetailsList = query.list();
			prePiadCardReCon.setReconciliationSummaryBean(reconciliationSummaryBean);
			prePiadCardReCon.setPrePiadDetailsList(prePiadDetailsList);

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getPrePiadCardReConReport" + e.getMessage() + " " + e);
			// logger.debug("problem in getPrePiadCardReConReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getPrePiadCardReConReport");
		// logger.info("completed excution =====================
		// getPrePiadCardReConReport ");
		return prePiadCardReCon;

	}

	public ReconciliationReport getRechargeReConReport(String aggreatorid, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getRechargeReConReport()");
		// logger.info("Start excution ===================== method
		// getRechargeReConReport(String aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		ReconciliationReport prePiadCardReCon = new ReconciliationReport();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			ReconciliationSummaryBean reconciliationSummaryBean = new ReconciliationSummaryBean();
			SQLQuery query = null;

			StringBuilder prePiadquery = new StringBuilder(
					"SELECT COUNT(*) as txnCout ,IFNULL(SUM(rechargeamt),0) as txnAmount FROM rechargemast WHERE STATUS='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				prePiadquery.append(" AND DATE(rechargeDate)=CURDATE()-INTERVAL 1 DAY");
			} else {
				prePiadquery.append(" AND DATE(rechargeDate) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadquery" + prePiadquery.toString());
			// logger.info("****** query ===================== prePiadquery
			// "+prePiadquery.toString());
			query = session.createSQLQuery(prePiadquery.toString());
			query.setString("aggreatorid", aggreatorid);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				reconciliationSummaryBean.setTotalTxn(Integer.parseInt(row[0].toString()));
				// String totAmount=row[1].toString();
				if (row[1] == null) {
					reconciliationSummaryBean.setTotalAmount(0.0);
				} else {
					reconciliationSummaryBean.setTotalAmount(Double.parseDouble(row[1].toString()));
				}
			}

			StringBuilder prePiadDetailsquery = new StringBuilder(
					"from RechargeTxnBean WHERE status='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				prePiadDetailsquery.append(" AND DATE(rechargeDate)=current_date-1");
			} else {
				prePiadDetailsquery.append(" AND DATE(rechargeDate) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadDetailsquery" + prePiadDetailsquery.toString());
			// logger.info("****** query ===================== prePiadDetailsquery
			// "+prePiadDetailsquery.toString());

			Query dtlQuery = session.createQuery(prePiadDetailsquery.toString());
			dtlQuery.setString("aggreatorid", aggreatorid);
			List<RechargeTxnBean> rechargeTxnList = dtlQuery.list();

			prePiadCardReCon.setRechargeTxnList(rechargeTxnList);
			prePiadCardReCon.setReconciliationSummaryBean(reconciliationSummaryBean);

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getRechargeReConReport" + e.getMessage() + " " + e);
			// logger.debug("problem in getRechargeReConReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"completed excution  getRechargeReConReport ");
		// logger.info("completed excution ===================== getRechargeReConReport
		// ");
		return prePiadCardReCon;
	}

	public ReconciliationReport getDMTReConReport(String aggreatorid, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getDMTReConReport()");
		// logger.info("Start excution ===================== method
		// getDMTReConReport(String aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		ReconciliationReport prePiadCardReCon = new ReconciliationReport();
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			ReconciliationSummaryBean reconciliationSummaryBean = new ReconciliationSummaryBean();
			SQLQuery query = null;

			StringBuilder prePiadquery = new StringBuilder(
					"SELECT COUNT(*) as txnCout ,IFNULL(SUM(amount),0) as txnAmount FROM wallettobanktxnmast WHERE paymentstatus='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				prePiadquery.append(" AND DATE(txndate)=CURDATE()-INTERVAL 1 DAY");
			} else {
				prePiadquery.append(
						" AND DATE(txndate) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadquery " + prePiadquery.toString());
			// logger.info("****** query ===================== prePiadquery
			// "+prePiadquery.toString());
			query = session.createSQLQuery(prePiadquery.toString());
			query.setString("aggreatorid", aggreatorid);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				reconciliationSummaryBean.setTotalTxn(Integer.parseInt(row[0].toString()));
				// String totAmount=row[1].toString();
				if (row[1] == null) {
					reconciliationSummaryBean.setTotalAmount(0.0);
				} else {
					reconciliationSummaryBean.setTotalAmount(Double.parseDouble(row[1].toString()));
				}
			}

			StringBuilder dmtDetailsquery = new StringBuilder(
					"from WalletToBankTxnMast WHERE paymentStatus='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				dmtDetailsquery.append(" AND DATE(txndate)=current_date-1");
			} else {
				dmtDetailsquery.append(
						" AND DATE(txndate) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"prePiadDetailsquery" + dmtDetailsquery.toString());
			// logger.info("****** query ===================== prePiadDetailsquery
			// "+dmtDetailsquery.toString());

			Query dmtQuery = session.createQuery(dmtDetailsquery.toString());
			dmtQuery.setString("aggreatorid", aggreatorid);
			List<WalletToBankTxnMast> dmtTxnList = dmtQuery.list();

			prePiadCardReCon.setReconciliationSummaryBean(reconciliationSummaryBean);
			prePiadCardReCon.setWalletToBankTxnList(dmtTxnList);

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getDMTReConReport" + e.getMessage() + " " + e);
			// logger.debug("problem in getDMTReConReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"completed excution getDMTReConReport ");
		logger.info("completed excution ===================== getDMTReConReport ");
		return prePiadCardReCon;
	}

	public ReconciliationReport getPGReConReport(String aggreatorid, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getPGReConReport()");
		// logger.info("Start excution ===================== method
		// getPGReConReport(String aggreatorid)"+aggreatorid);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		ReconciliationReport prePiadCardReCon = new ReconciliationReport();
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			ReconciliationSummaryBean reconciliationSummaryBean = new ReconciliationSummaryBean();
			SQLQuery query = null;

			StringBuilder pgquery = new StringBuilder(
					"SELECT COUNT(*) AS txnCout ,IFNULL(SUM(txnamount),0) AS txnAmount FROM paymentgatewaytxn WHERE  aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				pgquery.append(" AND DATE(pgtxnDate)=CURDATE()-INTERVAL 1 DAY");
			} else {
				pgquery.append(
						" AND DATE(pgtxnDate) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"pgquery " + pgquery.toString());
			// logger.info("****** query ===================== pgquery
			// "+pgquery.toString());
			query = session.createSQLQuery(pgquery.toString());
			query.setString("aggreatorid", aggreatorid);
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				reconciliationSummaryBean.setTotalTxn(Integer.parseInt(row[0].toString()));
				// String totAmount=row[1].toString();
				if (row[1] == null) {
					reconciliationSummaryBean.setTotalAmount(0.0);
				} else {
					reconciliationSummaryBean.setTotalAmount(Double.parseDouble(row[1].toString()));
				}
			}

			StringBuilder pgDetailsquery = new StringBuilder(
					"from PaymentGatewayTxnBean WHERE txnresult='SUCCESS' AND aggreatorid=:aggreatorid");
			if (stDate == null || stDate.isEmpty() || endDate == null || endDate.isEmpty()) {
				pgDetailsquery.append(" AND DATE(pgtxnDate)=current_date-1");
			} else {
				pgDetailsquery.append(
						" AND DATE(pgtxnDate) BETWEEN STR_TO_DATE(REPLACE('" + formate.format(formatter.parse(stDate))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"pgDetailsquery " + pgDetailsquery.toString());
			// logger.info("****** query ===================== pgDetailsquery
			// "+pgDetailsquery.toString());
			Query dmtQuery = session.createQuery(pgDetailsquery.toString());
			dmtQuery.setString("aggreatorid", aggreatorid);
			List<PaymentGatewayTxnBean> pgTxnList = dmtQuery.list();
			prePiadCardReCon.setReconciliationSummaryBean(reconciliationSummaryBean);
			prePiadCardReCon.setPaymentGatewayTxnList(pgTxnList);

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getPGReConReport" + e.getMessage() + " " + e);
			// logger.debug("problem in getPGReConReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"completed excution getPGReConReport ");
		// logger.info("completed excution ===================== getPGReConReport ");
		return prePiadCardReCon;
	}

	public List<RevenueReportBean> getRevenueReportList(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getRevenueReportList()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());
				factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<RevenueReportBean> results = new ArrayList<RevenueReportBean>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				StringBuilder rRevenuequery = new StringBuilder(
						"SELECT aggreatorid,txndate,agentid,txnamount,aepstxnamount,matmtxnamount,managername FROM allrevenuereport");
//				if (walletMastBean.getAddress1() == null || walletMastBean.getAddress1().isEmpty()) {
//					rRevenuequery.append(" revenuereport ");
//				}else if(walletMastBean.getAddress1().equalsIgnoreCase("2")) {
//					rRevenuequery.append(" aepsrevenuereport ");
//				}else if(walletMastBean.getAddress1().equalsIgnoreCase("3")) {
//					rRevenuequery.append(" matmrevenuereport ");
//				}else if(walletMastBean.getAddress1().equalsIgnoreCase("1")) {
//					rRevenuequery.append(" evenuereport ");
//				}
						
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					rRevenuequery.append(" where txndate=DATE_FORMAT(CURRENT_DATE, '%d/%m/%Y')");
				} else {
					rRevenuequery.append(" where STR_TO_DATE(txndate,'%d/%m/%Y') BETWEEN STR_TO_DATE('"
							+ formate.format(formatter.parse(walletMastBean.getStDate()))
							+ "','%d/%m/%Y')  AND STR_TO_DATE('"
							+ formate.format(formatter.parse(walletMastBean.getEndDate())) + "','%d/%m/%Y')");
				}

				if (walletMastBean.getAggreatorid() != null && !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
					rRevenuequery.append(" and aggreatorid='" + walletMastBean.getAggreatorid() + "'");

				SQLQuery query = session.createSQLQuery(rRevenuequery.toString());

				query.setResultTransformer(Transformers.aliasToBean(RevenueReportBean.class));
				results = query.list();

			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "",
					"problem in getRevenueReportList" + e.getMessage() + " " + e);
			// logger.debug("problem in getRevenueReportList==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size " + results.size());
		// logger.info("start excution ===================== method getRevenueReportList
		// Result "+results.size());
		return results;

	}

	public List<RevenueReportBean> getCommissionSummary(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getCommissionSummary()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","Start Date " +walletMastBean.getStDate() );
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","End Date"+walletMastBean.getEndDate() );
		 */
		// logger.info("start excution ============12========= method
		// getCommissionSummary(userId)"+walletMastBean.getStDate());
		// logger.info("start excution ============12========= method
		// getCommissionSummary(userId)"+walletMastBean.getEndDate());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<RevenueReportBean> results = new ArrayList<RevenueReportBean>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				StringBuilder rRevenuequery = new StringBuilder(
						"SELECT aggreatorid,txndate,txncount,agentid,txnamount,servicecharge FROM revenuereport");
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					rRevenuequery.append(" where txndate=DATE_FORMAT(CURRENT_DATE, '%d/%m/%Y')");
				} else {
					rRevenuequery.append(" where STR_TO_DATE(txndate,'%d/%m/%Y') BETWEEN STR_TO_DATE('"
							+ formate.format(formatter.parse(walletMastBean.getStDate()))
							+ "','%d/%m/%Y')  AND STR_TO_DATE('"
							+ formate.format(formatter.parse(walletMastBean.getEndDate())) + "','%d/%m/%Y')");
				}

				if (walletMastBean.getAggreatorid() != null && !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
					rRevenuequery.append(" and aggreatorid='" + walletMastBean.getAggreatorid() + "'");

				SQLQuery query = session.createSQLQuery(rRevenuequery.toString());

				query.setResultTransformer(Transformers.aliasToBean(RevenueReportBean.class));
				results = query.list();

			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "",
					"problem in getCommissionSummary" + e.getMessage() + " " + e);
			// logger.debug("problem in getCommissionSummary==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method getCommissionSummary
		// Result "+results.size());
		return results;

	}

	public List<DmtDetailsMastBean> getDmtDetailsReportList(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getDmtDetailsReportList()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<DmtDetailsMastBean> results = new ArrayList<DmtDetailsMastBean>();
		try {
			if(walletMastBean.getBankType() == null ) {
				walletMastBean.setBankType("All");
			}
			if(walletMastBean.getTxnStatus() == null) {
				walletMastBean.setTxnStatus("All");
			}
			
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null) {
						
						StringBuilder rRevenuequery = new StringBuilder(
								"SELECT aggreatorid,senderid,agentid,txnid,sendermobileno,sendername,name,bankname,benemobileno,beneaccountno,beneifsccode,txnamount,remittancetype,remittancemode,remittancestatus,paymentinfo,DATE_FORMAT(txndatetime, '%d/%m/%Y %T') AS txndatetime,banktranrefno,bankrrn,bankresponse,beneid,kyctype,DATE_FORMAT(updatedatetime, '%d/%m/%Y %T') AS updatedatetime,CASE WHEN distributerid = '-1' THEN ' ' ELSE distributerid END  distributerid  FROM dmtdetailsmast");
						
						if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
								|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {

							rRevenuequery.append(" where date(txndatetime)=CURDATE()");
							// rRevenuequery.append(" where date(b.txndate)=CURDATE()");
						} else {
							rRevenuequery.append(" WHERE DATE(txndatetime) BETWEEN STR_TO_DATE(REPLACE('"
									+ formate.format(formatter.parse(walletMastBean.getStDate()))
									+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
									+ formate.format(formatter.parse(walletMastBean.getEndDate()))
									+ "', '/', '-'),'%d-%m-%Y')");

						}
						
					/*	
						if(! "All".equalsIgnoreCase(walletMastBean.getBankType())) {
							if("Bank1".equalsIgnoreCase(walletMastBean.getBankType())) {
								rRevenuequery.append(" and txnid like '%FB%'");
							}else {
								rRevenuequery.append(" and txnid not like '%FB%'");
							}
						}
					*/
						if(! "All".equalsIgnoreCase(walletMastBean.getBankType())) {
							if("Bank1".equalsIgnoreCase(walletMastBean.getBankType())) {
								rRevenuequery.append(" and txnid like '%AS%'");
							}else if("Bank2".equalsIgnoreCase(walletMastBean.getBankType())) {
								rRevenuequery.append(" and txnid like '%FB%'");
							}
							else if("Bank3".equalsIgnoreCase(walletMastBean.getBankType())){
								rRevenuequery.append(" and txnid LIKE 'PU%' or txnid LIKE 'PT%' " );
							}
				        }
						
						if(! "All".equalsIgnoreCase(walletMastBean.getTxnStatus())) {
							
							String strMain = walletMastBean.getTxnStatus();
						    String[] arrSplit = strMain.split(", ");
						    int len=arrSplit.length;
						    if(len==1)
						    {
						      rRevenuequery.append(" and remittancestatus ='"+walletMastBean.getTxnStatus()+"'");	
						    }else if(len==2)
						    {
						      rRevenuequery.append(" and remittancestatus ='"+arrSplit[0]+"' or remittancestatus ='"+arrSplit[1]+"' ");	
						    }else if(len==3)
						    {
							  rRevenuequery.append(" and remittancestatus ='"+arrSplit[0]+"' or remittancestatus ='"+arrSplit[1]+"' or remittancestatus ='"+arrSplit[2]+"' ");	
							}
						    
							//rRevenuequery.append(" and remittancestatus ='"+walletMastBean.getTxnStatus()+"'");
						}
						
						if(!walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						rRevenuequery.append(" and aggreatorid='" + walletMastBean.getAggreatorid() + "'");
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
								walletMastBean.getWalletid(), "", "", "", "query " + rRevenuequery);

						SQLQuery query = session.createSQLQuery(rRevenuequery.toString())
								.addScalar("aggreatorid", Hibernate.STRING).addScalar("senderid", Hibernate.STRING)
								.addScalar("agentid", Hibernate.STRING).addScalar("txnid", Hibernate.STRING)
								.addScalar("name", Hibernate.STRING).addScalar("bankname", Hibernate.STRING)
								.addScalar("beneaccountno", Hibernate.STRING)
								.addScalar("beneifsccode", Hibernate.STRING).addScalar("txnamount", Hibernate.DOUBLE)
								.addScalar("remittancetype", Hibernate.STRING)
								.addScalar("remittancestatus", Hibernate.STRING)
								.addScalar("paymentinfo", Hibernate.STRING).addScalar("txndatetime", Hibernate.STRING)
								.addScalar("sendername", Hibernate.STRING).addScalar("sendermobileno", Hibernate.STRING)
								.addScalar("benemobileno", Hibernate.STRING)
								.addScalar("remittancemode", Hibernate.STRING)
								.addScalar("banktranrefno", Hibernate.STRING).addScalar("bankrrn", Hibernate.STRING)
								.addScalar("bankresponse", Hibernate.STRING).addScalar("beneid", Hibernate.STRING)
								.addScalar("updatedatetime", Hibernate.STRING).addScalar("kyctype", Hibernate.STRING)
								.addScalar("distributerid", Hibernate.STRING);
						query.setResultTransformer(Transformers.aliasToBean(DmtDetailsMastBean.class));
						results = query.list();
					
				} else {
					List<DmtDetailsMastBean> results1 = new ArrayList<DmtDetailsMastBean>();
					StringBuilder rRevenuequery = new StringBuilder(
							"select b.wtbtxnid as txnid,b.aggreatorid as aggreatorid,b.walletid,b.userid as senderid,b.userid as agentid,b.accountno as beneaccountno,b.accountno,b.ifsccode as beneifsccode,b.transfertype as remittancetype,b.amount as txnamount,b.wallettxnstatus,b.status as remittancestatus,b.remarks as paymentinfo,b.ipimei,b.useragent,DATE_FORMAT(b.txndate, '%d/%m/%Y %T') AS txndatetime,w.mobileno as sendermobileno, w.name as sendername from b2cmoneytxnmast b join walletmast w on b.walletid=w.walletid");
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						rRevenuequery.append(" where date(b.txndate)=CURDATE()");
					} else {
						rRevenuequery.append(" WHERE DATE(b.txndate) BETWEEN STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(walletMastBean.getStDate()))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(walletMastBean.getEndDate()))
								+ "', '/', '-'),'%d-%m-%Y')");
					}
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query " + rRevenuequery);

					/*
					if(! "All".equalsIgnoreCase(walletMastBean.getBankType())) {
						if("Bank1".equalsIgnoreCase(walletMastBean.getBankType())) {
							rRevenuequery.append(" and txnid like '%FB%'");
						}else {
							rRevenuequery.append(" and txnid not like '%FB%'");
						}
					}
					*/

					if(! "All".equalsIgnoreCase(walletMastBean.getBankType())) {
						if("Bank1".equalsIgnoreCase(walletMastBean.getBankType())) {
							rRevenuequery.append(" and txnid like '%AS%'");
						}else if("Bank2".equalsIgnoreCase(walletMastBean.getBankType())) {
							rRevenuequery.append(" and txnid like '%FB%'");
						}
						else if("Bank3".equalsIgnoreCase(walletMastBean.getBankType())){
							rRevenuequery.append(" and txnid LIKE 'PU%' or txnid LIKE 'PT%' " );
						}
					}

					
					
					
					if(! "All".equalsIgnoreCase(walletMastBean.getTxnStatus())) {
						
						String strMain = walletMastBean.getTxnStatus();
					    String[] arrSplit = strMain.split(", ");
					    int len=arrSplit.length;
					    if(len==1)
					    {
					      rRevenuequery.append(" and remittancestatus ='"+walletMastBean.getTxnStatus()+"'");	
					    }else if(len==2)
					    {
					      rRevenuequery.append(" and remittancestatus ='"+arrSplit[0]+"' or remittancestatus ='"+arrSplit[1]+"' ");	
					    }else if(len==3)
					    {
						  rRevenuequery.append(" and remittancestatus ='"+arrSplit[0]+"' or remittancestatus ='"+arrSplit[1]+"' or remittancestatus ='"+arrSplit[2]+"' ");	
						}
						
					//	rRevenuequery.append(" and remittancestatus ='"+walletMastBean.getTxnStatus()+"'");
					}
					
					
					
					SQLQuery query = session.createSQLQuery(rRevenuequery.toString())
							.addScalar("aggreatorid", Hibernate.STRING).addScalar("senderid", Hibernate.STRING)
							.addScalar("agentid", Hibernate.STRING).addScalar("txnid", Hibernate.STRING)
							.addScalar("beneaccountno", Hibernate.STRING).addScalar("beneifsccode", Hibernate.STRING)
							.addScalar("txnamount", Hibernate.DOUBLE).addScalar("remittancetype", Hibernate.STRING)
							.addScalar("remittancestatus", Hibernate.STRING).addScalar("paymentinfo", Hibernate.STRING)
							.addScalar("txndatetime", Hibernate.STRING).addScalar("sendername", Hibernate.STRING)
							.addScalar("sendermobileno", Hibernate.STRING);
					query.setResultTransformer(Transformers.aliasToBean(DmtDetailsMastBean.class));
					results = query.list();

					StringBuilder rRevenuequery1 = new StringBuilder(
							"SELECT aggreatorid,senderid,agentid,txnid,sendermobileno,sendername,name,bankname,benemobileno,beneaccountno,beneifsccode,txnamount,remittancetype,remittancemode,remittancestatus,paymentinfo,DATE_FORMAT(txndatetime, '%d/%m/%Y %T') AS txndatetime,banktranrefno,bankrrn,bankresponse,beneid,kyctype,DATE_FORMAT(updatedatetime, '%d/%m/%Y %T') AS updatedatetime,CASE WHEN distributerid = '-1' THEN ' ' ELSE distributerid END  distributerid  FROM dmtdetailsmast");
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {

						rRevenuequery1.append(" where date(txndatetime)=CURDATE()");
					} else {
						rRevenuequery1.append(" WHERE DATE(txndatetime) BETWEEN STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(walletMastBean.getStDate()))
								+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
								+ formate.format(formatter.parse(walletMastBean.getEndDate()))
								+ "', '/', '-'),'%d-%m-%Y')");
						
					}
					
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query " + rRevenuequery1);
					
					SQLQuery query1 = session.createSQLQuery(rRevenuequery1.toString())
							.addScalar("aggreatorid", Hibernate.STRING).addScalar("senderid", Hibernate.STRING)
							.addScalar("agentid", Hibernate.STRING).addScalar("txnid", Hibernate.STRING)
							.addScalar("name", Hibernate.STRING).addScalar("bankname", Hibernate.STRING)
							.addScalar("beneaccountno", Hibernate.STRING).addScalar("beneifsccode", Hibernate.STRING)
							.addScalar("txnamount", Hibernate.DOUBLE).addScalar("remittancetype", Hibernate.STRING)
							.addScalar("remittancestatus", Hibernate.STRING).addScalar("paymentinfo", Hibernate.STRING)
							.addScalar("txndatetime", Hibernate.STRING).addScalar("sendername", Hibernate.STRING)
							.addScalar("sendermobileno", Hibernate.STRING).addScalar("benemobileno", Hibernate.STRING)
							.addScalar("remittancemode", Hibernate.STRING).addScalar("banktranrefno", Hibernate.STRING)
							.addScalar("bankrrn", Hibernate.STRING).addScalar("bankresponse", Hibernate.STRING)
							.addScalar("beneid", Hibernate.STRING).addScalar("updatedatetime", Hibernate.STRING)
							.addScalar("kyctype", Hibernate.STRING).addScalar("distributerid", Hibernate.STRING);
					query1.setResultTransformer(Transformers.aliasToBean(DmtDetailsMastBean.class));
					results1 = query1.list();

					results.addAll(results1);

				}
			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}

	
	
	public List<AepsSettlementBean> getAepsPayoutReport(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getAepsReport()" + "Start Date " + walletMastBean.getStDate()
						+ "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AepsSettlementBean> results = new ArrayList<AepsSettlementBean>();
		try {
			if (walletMastBean.getUsertype() == 3) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Criteria criteria = session.createCriteria(AepsSettlementBean.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("settlementDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("settlementDate", formatter.parse(walletMastBean.getStDate()), endDate));

				}
				if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null
						&& walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
				} else {
					criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query ");

				results = criteria.list();

			} else if (walletMastBean.getUsertype() != 2 && walletMastBean.getUsertype() != 3) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(AepsSettlementBean.class);

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null
						&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
					
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("settlementDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("settlementDate", formatter.parse(walletMastBean.getStDate()), endDate));
					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregatorId", walletMastBean.getAggreatorid()));

					
					results = criteria.list();
				} else {
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("settlementDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("settlementDate", formatter.parse(walletMastBean.getStDate()), endDate));

					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregatorId", walletMastBean.getAggreatorid()));
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query ");
					
					results = criteria.list();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}

	
	public List<AEPSLedger> getAepsReport(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getAepsReport()" + "Start Date " + walletMastBean.getStDate()
						+ "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AEPSLedger> results = new ArrayList<AEPSLedger>();
		try {
			if (walletMastBean.getUsertype() == 3) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Criteria criteria = session.createCriteria(AEPSLedger.class);

//			 if(walletMastBean!=null&&walletMastBean.getAggreatorid()!=null&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")){
//			// StringBuilder rRevenuequery=new StringBuilder("select b.wtbtxnid as txnid,b.aggreatorid as aggreatorid,b.walletid,b.userid as senderid,b.userid as agentid,b.accountno as beneaccountno,b.accountno,b.ifsccode as beneifsccode,b.transfertype as remittancetype,b.amount as txnamount,b.wallettxnstatus,b.status as remittancestatus,b.remarks as paymentinfo,b.ipimei,b.useragent,DATE_FORMAT(b.txndate, '%d/%m/%Y %T') AS txndatetime,w.mobileno as sendermobileno, w.name as sendername from b2cmoneytxnmast b join walletmast w on b.walletid=w.walletid");
//			 if(walletMastBean.getStDate()==null ||walletMastBean.getStDate().isEmpty() ||walletMastBean.getEndDate()==null ||walletMastBean.getEndDate().isEmpty()){
//				 criteria.add(Restrictions.ge("txnDate",formate.parse(formate.format(new Date()))));
//
//			 }else{
//				 Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
//				 criteria.add(Restrictions.between("txnDate",formatter.parse(walletMastBean.getStDate()),endDate));
//			 }
//			 if(walletMastBean.getAggreatorid()!=null&&!walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
//			 criteria.add(Restrictions.eq("aggregator", walletMastBean.getAggreatorid()));
//			
//			 criteria.add(Restrictions.ne("status", "Pending"));
//			
//			 results=criteria.list();
//			 }else{
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));

				}
				if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null
						&& walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
				} else {
					criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query ");

				//criteria.add(Restrictions.ne("status", "Pending"));
				results = criteria.list();
//			 }

			} else if (walletMastBean.getUsertype() != 2 && walletMastBean.getUsertype() != 3) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(AEPSLedger.class);

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null
						&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
					// StringBuilder rRevenuequery=new StringBuilder("select b.wtbtxnid as
					// txnid,b.aggreatorid as aggreatorid,b.walletid,b.userid as senderid,b.userid
					// as agentid,b.accountno as beneaccountno,b.accountno,b.ifsccode as
					// beneifsccode,b.transfertype as remittancetype,b.amount as
					// txnamount,b.wallettxnstatus,b.status as remittancestatus,b.remarks as
					// paymentinfo,b.ipimei,b.useragent,DATE_FORMAT(b.txndate, '%d/%m/%Y %T') AS
					// txndatetime,w.mobileno as sendermobileno, w.name as sendername from
					// b2cmoneytxnmast b join walletmast w on b.walletid=w.walletid");
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregator", walletMastBean.getAggreatorid()));

					//criteria.add(Restrictions.ne("status", "Pending"));
					criteria.add(Restrictions.ne("txnType", "BALANCE ENQUIRY"));
					results = criteria.list();
				} else {
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));

					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregator", walletMastBean.getAggreatorid()));
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query ");
					criteria.add(Restrictions.ne("txnType", "BALANCE ENQUIRY"));
					//criteria.add(Restrictions.ne("status", "Pending"));
					results = criteria.list();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}

	public List<MATMLedger> getMatmReport(WalletMastBean walletMastBean) {
		/*
		 * 
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","getMatmReport()" +"Start Date "
		 * +walletMastBean.getStDate()+"End Date"+walletMastBean.getEndDate() );
		 * 
		 * factory=DBUtil.getSessionFactory(); Session session = factory.openSession();
		 * List<MATMLedger> results=new ArrayList<MATMLedger>(); try{
		 * if(!(walletMastBean.getId()=="")){ SimpleDateFormat formatter = new
		 * SimpleDateFormat("dd-MMM-yyyy"); SimpleDateFormat formate = new
		 * SimpleDateFormat("yyyy-MM-dd");
		 * 
		 * Calendar cal = Calendar.getInstance();
		 * 
		 * 
		 * 
		 * Criteria criteria= session.createCriteria(MATMLedger.class);
		 * 
		 * if(walletMastBean!=null&&walletMastBean.getAggreatorid()!=null&&(
		 * walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001052")||
		 * walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001053")||
		 * walletMastBean.getAggreatorid().equalsIgnoreCase("AGGR001035"))){ //
		 * StringBuilder rRevenuequery=new
		 * StringBuilder("select b.wtbtxnid as txnid,b.aggreatorid as aggreatorid,b.walletid,b.userid as senderid,b.userid as agentid,b.accountno as beneaccountno,b.accountno,b.ifsccode as beneifsccode,b.transfertype as remittancetype,b.amount as txnamount,b.wallettxnstatus,b.status as remittancestatus,b.remarks as paymentinfo,b.ipimei,b.useragent,DATE_FORMAT(b.txndate, '%d/%m/%Y %T') AS txndatetime,w.mobileno as sendermobileno, w.name as sendername from b2cmoneytxnmast b join walletmast w on b.walletid=w.walletid"
		 * ); if(walletMastBean.getStDate()==null ||walletMastBean.getStDate().isEmpty()
		 * ||walletMastBean.getEndDate()==null ||walletMastBean.getEndDate().isEmpty()){
		 * criteria.add(Restrictions.ge("txnDate",formate.parse(formate.format(new
		 * Date()))));
		 * 
		 * }else{ Date endDate = new
		 * Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
		 * criteria.add(Restrictions.between("txnDate",formatter.parse(walletMastBean.
		 * getStDate()),endDate)); }
		 * if(walletMastBean.getAggreatorid()!=null&&!walletMastBean.getAggreatorid().
		 * equalsIgnoreCase("All")) criteria.add(Restrictions.eq("aggregator",
		 * walletMastBean.getAggreatorid()));
		 * 
		 * criteria.add(Restrictions.ne("status", "Pending"));
		 * 
		 * results=criteria.list(); }else{ if(walletMastBean.getStDate()==null
		 * ||walletMastBean.getStDate().isEmpty() ||walletMastBean.getEndDate()==null
		 * ||walletMastBean.getEndDate().isEmpty()){
		 * criteria.add(Restrictions.ge("txnDate",formate.parse(formate.format(new
		 * Date()))));
		 * 
		 * }else{ Date endDate = new
		 * Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
		 * criteria.add(Restrictions.between("txnDate",formatter.parse(walletMastBean.
		 * getStDate()),endDate));
		 * 
		 * } if(walletMastBean.getAggreatorid()!=null&&!walletMastBean.getAggreatorid().
		 * equalsIgnoreCase("All")) criteria.add(Restrictions.eq("aggregator",
		 * walletMastBean.getAggreatorid())); Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","query " );
		 * 
		 * criteria.add(Restrictions.ne("status", "Pending")); results=criteria.list();
		 * }
		 * 
		 * 
		 * }
		 * 
		 * }catch (Exception e) {
		 * 
		 * e.printStackTrace(); Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","problem in DmtDetailsMastBean" + e.getMessage() +e
		 * ); //logger.debug("problem in DmtDetailsMastBean==================" +
		 * e.getMessage(), e); } finally { session.close(); }
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","results size"+results.size() ); // logger.
		 * info("start excution ===================== method DmtDetailsMastBean Result "
		 * +results.size()); return results;
		 * 
		 */
		
	


		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getmatmReport()" + "Start Date " + walletMastBean.getStDate()
						+ "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<MATMLedger> results = new ArrayList<MATMLedger>();
		try {
			if (walletMastBean.getUsertype() == 3) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Criteria criteria = session.createCriteria(MATMLedger.class);


				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));

				}
				if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null
						&& walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
				} else {
					criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query ");

				//criteria.add(Restrictions.ne("status", "Pending"));
				results = criteria.list();
//			 }

			} else if (walletMastBean.getUsertype() != 2 && walletMastBean.getUsertype() != 3) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(MATMLedger.class);

				if (walletMastBean != null && walletMastBean.getAggreatorid() != null
						&& !walletMastBean.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
				
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregator", walletMastBean.getAggreatorid()));

					//criteria.add(Restrictions.ne("status", "Pending"));

					results = criteria.list();
				} else {
					if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
							|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
						criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

					} else {
						Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
						criteria.add(
								Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));

					}
					if (walletMastBean.getAggreatorid() != null
							&& !walletMastBean.getAggreatorid().equalsIgnoreCase("All"))
						criteria.add(Restrictions.eq("aggregator", walletMastBean.getAggreatorid()));
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
							walletMastBean.getWalletid(), "", "", "", "query ");

					//criteria.add(Restrictions.ne("status", "Pending"));
					results = criteria.list();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in MATMLedger" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	
		
		
	}

	
	@Override
	public List<AEPSLedger> getAepsReportForAgent(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getId(),
				walletMastBean.getAggreatorid(), "", "", "", "getAepsReportForAgent()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AEPSLedger> results = new ArrayList<AEPSLedger>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(AEPSLedger.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", walletMastBean.getId()))
						.add(Restrictions.ne("txnType", "BALANCE ENQUIRY"));
		       
				System.out.println(criteria.toString());
				results = criteria.list();

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}
	
	@Override
	public List<AepsSettlementBean> getAepsPayoutReportForAgent(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getId(),
				walletMastBean.getAggreatorid(), "", "", "", "getAepsPayoutReportForAgent()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AepsSettlementBean> results = new ArrayList<AepsSettlementBean>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(AepsSettlementBean.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("settlementDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("settlementDate", formatter.parse(walletMastBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
						//.add(Restrictions.ne("txnType", "BALANCE ENQUIRY"));
		       
				System.out.println(criteria.toString());
				results = criteria.list();

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in aepspayoutsettlement" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}

	@Override
	public List<MATMLedger> getMatmReportForAgent(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getId(),
				walletMastBean.getAggreatorid(), "", "", "", "getAepsReportForAgent()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<MATMLedger> results = new ArrayList<MATMLedger>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(MATMLedger.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", walletMastBean.getId()))
						.add(Restrictions.ne("status", "Pending"));

				results = criteria.list();

			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
			// logger.debug("problem in DmtDetailsMastBean==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info("start excution ===================== method DmtDetailsMastBean
		// Result "+results.size());
		return results;

	}

	public List<DmtDetailsMastBean> GetSupDistDmtDetailsReport(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "GetSupDistDmtDetailsReport()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","Start Date " +walletMastBean.getStDate() );
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.
		 * getWalletid(),"", "", "","End Date"+walletMastBean.getEndDate() );
		 */
		// logger.info("start excution ============12========= method
		// GetSupDistDmtDetailsReport(userId)"+walletMastBean.getStDate());
		// logger.info("start excution ============12========= method
		// GetSupDistDmtDetailsReport(userId)"+walletMastBean.getEndDate());
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<DmtDetailsMastBean> results = new ArrayList<DmtDetailsMastBean>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				StringBuilder rRevenuequery = new StringBuilder(
						"SELECT aggreatorid,senderid,agentid,txnid,sendermobileno,sendername,benemobileno,beneaccountno,beneifsccode,txnamount,remittancetype,remittancemode,remittancestatus,paymentinfo,DATE_FORMAT(txndatetime, '%d/%m/%Y %T') AS txndatetime,banktranrefno,bankrrn,bankresponse,beneid,kyctype,DATE_FORMAT(updatedatetime, '%d/%m/%Y %T') AS updatedatetime,CASE WHEN distributerid = '-1' THEN ' ' ELSE distributerid END  distributerid  FROM dmtdetailsmast");
//				 StringBuilder rRevenuequery=new StringBuilder("select b.wtbtxnid as txnid,b.aggreatorid as aggreatorid,b.walletid,b.userid as senderid,b.userid as agentid,b.accholdername as beneaccountno,b.accountno,b.ifsccode as beneifsccode,b.transfertype as remittancetype,b.amount as txnamount,b.wallettxnstatus,b.status as remittancestatus,b.remarks as paymentinfo,b.ipimei,b.useragent,DATE_FORMAT(b.txndate, '%d/%m/%Y %T') AS txndatetime,w.mobileno as sendermobileno, w.name as sendername from b2cmoneytxnmast b join walletmast w on b.walletid=w.walletid");
				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					rRevenuequery.append(" where date(txndatetime)=CURDATE()");
//					 rRevenuequery.append(" and distributerid=:distributerid");
				} else {
					rRevenuequery.append(" WHERE DATE(txndatetime) BETWEEN STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(walletMastBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(walletMastBean.getEndDate()))
							+ "', '/', '-'),'%d-%m-%Y')");
					// rRevenuequery.append(" WHERE DATE(b.txndate) BETWEEN
					// STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getStDate()))+"',
					// '/', '-'),'%d-%m-%Y') AND
					// STR_TO_DATE(REPLACE('"+formate.format(formatter.parse(walletMastBean.getEndDate()))+"',
					// '/', '-'),'%d-%m-%Y')");

				}
				if (walletMastBean.getDistributerid() == null
						|| walletMastBean.getDistributerid().equalsIgnoreCase("All"))
					rRevenuequery.append(
							" and distributerid in(select id from walletmast where usertype=3 and superdistributerid='"
									+ walletMastBean.getSuperdistributerid() + "')");
				else
					rRevenuequery.append(" and distributerid='" + walletMastBean.getDistributerid() + "'");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
						walletMastBean.getWalletid(), "", "", "", "query " + rRevenuequery);
				// logger.info("start excution ============12========= query***"+rRevenuequery);

				SQLQuery query = session.createSQLQuery(rRevenuequery.toString())
						.addScalar("aggreatorid", Hibernate.STRING).addScalar("senderid", Hibernate.STRING)
						.addScalar("agentid", Hibernate.STRING).addScalar("txnid", Hibernate.STRING)
						.addScalar("beneaccountno", Hibernate.STRING).addScalar("beneifsccode", Hibernate.STRING)
						.addScalar("txnamount", Hibernate.DOUBLE).addScalar("remittancetype", Hibernate.STRING)
						.addScalar("remittancestatus", Hibernate.STRING).addScalar("paymentinfo", Hibernate.STRING)
						.addScalar("txndatetime", Hibernate.STRING).addScalar("sendername", Hibernate.STRING)
						.addScalar("sendermobileno", Hibernate.STRING).addScalar("benemobileno", Hibernate.STRING)
						.addScalar("remittancemode", Hibernate.STRING).addScalar("banktranrefno", Hibernate.STRING)
						.addScalar("bankrrn", Hibernate.STRING).addScalar("bankresponse", Hibernate.STRING)
						.addScalar("beneid", Hibernate.STRING).addScalar("updatedatetime", Hibernate.STRING)
						.addScalar("kyctype", Hibernate.STRING).addScalar("distributerid", Hibernate.STRING);
				query.setResultTransformer(Transformers.aliasToBean(DmtDetailsMastBean.class));
				results = query.list();
			}

		} catch (Exception e) {

			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "",
					"problem in GetSupDistDmtDetailsReport" + e.getMessage() + " " + e);
			// logger.debug("problem in GetSupDistDmtDetailsReport==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "",
				"start excution method GetSupDistDmtDetailsReport Result " + results.size());
		// logger.info("start excution ===================== method
		// GetSupDistDmtDetailsReport Result "+results.size());
		return results;

	}

	public List<AgentBalDetailsBean> getAgentBalDetail(String aggid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "", "getAgentBalDetail()");
		// logger.info("start excution
		// ******************************************************** method
		// getCustomerDetail()"+aggid);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AgentBalDetailsBean> results = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT id,usertype,approveDate,name,credit,debit,finalBalance FROM walletmast ,walletbalance WHERE walletmast.walletid=walletbalance.walletid AND walletmast.usertype in (2,3,6,7,8,9) AND walletmast.aggreatorid=:aggid");
			query.setParameter("aggid", aggid);
			query.setResultTransformer(Transformers.aliasToBean(AgentBalDetailsBean.class));
			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "",
					"problem in getAgentBalDetail" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getAgentBalDetail*****************************************" + e.getMessage(),
			// e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggid, "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;
	}

	public AgentBalDetailResponse getAgentBalDetailBySuperDistributor(WalletMastBean walletBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletBean.getAggreatorid(),
				walletBean.getWalletid(), "", "", "", "getAgentBalDetailBySuperDistributor()" + "|"
						+ walletBean.getAgentid() + walletBean.getDistributerid() + walletBean.getSuperdistributerid());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AgentBalDetailsBean> results = null;
		AgentBalDetailResponse response = new AgentBalDetailResponse();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("yyyyMMdd");
			StringBuffer stringQuery = new StringBuffer();

			stringQuery.append(
					"select agentdetails(id,:startDate,:endDate) as details,id,name,walletid from walletmast where distributerid=:distId and id!=:distId");

			SQLQuery query = session.createSQLQuery(stringQuery.toString());

			query.setParameter("distId", walletBean.getDistributerid());
			if (walletBean.getStDate() == null || walletBean.getStDate().isEmpty() || walletBean.getEndDate() == null
					|| walletBean.getEndDate().isEmpty()) {
				Date date = new Date();
				query.setParameter("startDate", formate.format(date));
				query.setParameter("endDate", formate.format(date));
				
				System.out.println("startDate "+ formate.format(date));
				System.out.println("endDate"+ formate.format(date));
			} else {
				query.setParameter("startDate", formate.format(formatter.parse(walletBean.getStDate())));
				query.setParameter("endDate", formate.format(formatter.parse(walletBean.getEndDate())));
			
				System.out.println("Agent start "+formate.format(formatter.parse(walletBean.getStDate())));
				System.out.println("Agent end "+formate.format(formatter.parse(walletBean.getEndDate())));
			}

//			}

			query.setResultTransformer(Transformers.aliasToBean(AgentBalDetailsBean.class));
			results = query.list();
			SQLQuery query1 = session.createSQLQuery("select distagentdetails(:distId,:startDate,:endDate)");
			query1.setParameter("distId", walletBean.getDistributerid());
			if (walletBean.getStDate() == null || walletBean.getStDate().isEmpty() || walletBean.getEndDate() == null
					|| walletBean.getEndDate().isEmpty()) {
				Date date = new Date();
				query1.setParameter("startDate", formate.format(date));
				query1.setParameter("endDate", formate.format(date));
				
				System.out.println("startDate "+ formate.format(date));
				System.out.println("endDate"+ formate.format(date));
			} else {
				query1.setParameter("startDate", formate.format(formatter.parse(walletBean.getStDate())));
				query1.setParameter("endDate", formate.format(formatter.parse(walletBean.getEndDate())));
			
				System.out.println("start "+formate.format(formatter.parse(walletBean.getStDate())));
				System.out.println("end "+formate.format(formatter.parse(walletBean.getEndDate())));
			}
			String sumDetails = (String) query1.uniqueResult();
			response.setBalList(results);
			response.setDistributorTotal(sumDetails);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletBean.getAggreatorid(),
					walletBean.getWalletid(), "", "", "",
					"problem in getAgentBalDetailBySuperDistributor" + e.getMessage());
			// logger.debug("problem in
			// getAgentBalDetailBySuperDistributor*****************************************"
			// + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletBean.getAggreatorid(),
				walletBean.getWalletid(), "", "", "", "results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return response;
	}

	public List<AgentCurrentSummary> getAgentCurrentSummary(String distributerId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentCurrentSummary()" + "|" + distributerId);
		// logger.info("start excution
		// ******************************************************** method
		// gatAgentCurrentSummary()"+distributerId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AgentCurrentSummary> results = null;
		try {
			SQLQuery query = session.createSQLQuery(
					"SELECT id agentId,name,agenttype type,SUM(txncredit) txnCredit,SUM(txndebit) txnDebit,c.finalBalance finalBalance   FROM walletmast  a,wallettransaction b,walletbalance c WHERE a.walletid=b.walletid  AND b.walletid=c.walletid AND a.id IN (SELECT id FROM walletmast WHERE distributerid=:distributerId )AND DATE(txnDate)=DATE(NOW()) GROUP BY id  ORDER BY id");
			// SQLQuery query=session.createSQLQuery("SELECT id agentId,name,agenttype
			// type,SUM(txncredit) txnCredit,SUM(txndebit) txnDebit,c.finalBalance
			// finalBalance FROM walletmast a,wallettransaction b,walletbalance c WHERE
			// a.walletid=b.walletid AND b.walletid=c.walletid AND a.id IN (SELECT id FROM
			// walletmast WHERE distributerid=:distributerId ) GROUP BY agentId ORDER BY
			// agentId");
			query.setParameter("distributerId", distributerId);
			query.setResultTransformer(Transformers.aliasToBean(AgentCurrentSummary.class));
			query.addScalar("agentId").addScalar("name").addScalar("type").addScalar("txnCredit").addScalar("txnDebit")
					.addScalar("finalBalance");
			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in gatAgentCurrentSummary" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// gatAgentCurrentSummary*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"results size" + results.size() + "|" + distributerId);
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;
	}

	public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(String agentId, String stDate, String endDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getAgentConsolidatedReport()" + "|" + agentId);
		// logger.info("start excution
		// ******************************************************** method
		// gatAgentCurrentSummary()"+agentId);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AgentConsolidatedReportBean> results = new ArrayList<AgentConsolidatedReportBean>();
		try {
			if (!(agentId == null || stDate == null || endDate == null)) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				SQLQuery query = session.createSQLQuery(
						"SELECT DATE_FORMAT(txnDate,'%d/%m/%Y')  txnDate,a.walletid walletid,a.id agentId,name,a.agenttype type,SUM(b.txncredit) txnCredit,SUM(b.txndebit) txnDebit  FROM walletmast a,wallettransaction b  WHERE a.walletid=b.walletid AND DATE(txnDate) BETWEEN STR_TO_DATE('"
								+ formate.format(formatter.parse(stDate)) + "','%d/%m/%Y') AND STR_TO_DATE('"
								+ formate.format(formatter.parse(endDate)) + "','%d/%m/%Y') AND a.id='" + agentId
								+ "'  GROUP BY DATE(txnDate) ORDER BY DATE(txnDate)");
				query.addScalar("txnDate").addScalar("walletid").addScalar("agentId").addScalar("name")
						.addScalar("type").addScalar("txnCredit").addScalar("txnDebit");
				query.setResultTransformer(Transformers.aliasToBean(AgentConsolidatedReportBean.class));
				results = query.list();
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in gatAgentCurrentSummary" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// gatAgentCurrentSummary*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;
	}

	public List<SenderClosingBalBean> getSenderClosingBalReport(String stDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getSenderClosingBalReport()" + "|" + stDate);

		// logger.info("start excution
		// ******************************************************** method
		// getSenderClosingBalReport()"+stDate);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<SenderClosingBalBean> results = new ArrayList<SenderClosingBalBean>();
		try {

			if (!(stDate == null)) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				SQLQuery query = session.createSQLQuery(
						"SELECT id,aggreatorid,mobileno,firstname,DATE_FORMAT(auditdate,'%d/%m/%Y') auditdate,walletbalance,transferlimit,kycstatus,ispanvalidate FROM sendermast_audit  WHERE  DATE(auditdate) = STR_TO_DATE('"
								+ formate.format(formatter.parse(stDate)) + "','%d/%m/%Y')  ORDER BY id");
				query.setResultTransformer(Transformers.aliasToBean(SenderClosingBalBean.class));
				results = query.list();

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getSenderClosingBalReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getSenderClosingBalReport*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public List<RefundTransactionBean> getRefundTransactionReport(String stDate) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getRefundTransactionReport()" + "|" + stDate);

		// logger.info("start excution
		// ******************************************************** method
		// getRefundTransactionReport()"+stDate);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<RefundTransactionBean> results = new ArrayList<RefundTransactionBean>();
		try {

			if (!(stDate == null)) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				SQLQuery query = session.createSQLQuery(
						"SELECT a.id,refundid,a.status,DATE_FORMAT(txndate,'%d/%m/%Y') txndate,b.remark,amount,servicecharge,agentorsenderid,refundtosenderoragent FROM refundtransaction a, remittanceledger b    WHERE  a.id=b.id and   DATE(txndate) = STR_TO_DATE('"
								+ formate.format(formatter.parse(stDate)) + "','%d/%m/%Y')  ORDER BY refundid");
				query.setResultTransformer(Transformers.aliasToBean(RefundTransactionBean.class));
				results = query.list();

			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getRefundTransactionReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getRefundTransactionReport*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public List<TravelTxn> getTravelTxn(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", userId, "", "", "getTravelTxn()"

		);
		logger.info("start excution ===================== method getTravelTxn()");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<TravelTxn> travelTxn = new ArrayList<TravelTxn>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

		try {

			SQLQuery query = session.createSQLQuery(
					"select 'Recharge' as type,rechargeid as transid,rechargeamt as amount,rechargeDate as txndate,status,1 as cancellationstatus,concat(concat(concat(concat(concat(concat(rechargetype,'|'),rechargecircle),'|'),rechargeoperator,'|'),rechargenumber)) as details  from rechargemast where userid= '"
							+ userId
							+ "' union select 'Flight'  as type,tripid as transid,totalfare as amount,fareqdate as txndate,status,cancellationstatus,getFlightdetails(tripid)  as details from airfare where airfareid  in (select max(airfareid ) from airfare where userid='"
							+ userId + "' group by tripid) order by txndate desc");

			query.setResultTransformer(Transformers.aliasToBean(TravelTxn.class));
			travelTxn = query.list();
		} catch (Exception e) {

			logger.debug("problem in getTravelTxn()=======================" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return travelTxn;
	}

	public List<TravelTxn> getTravelTxn(/* String userId */ReportBean reportBean) {
		class Local {
		}
		;
		String methodname = Local.class.getEnclosingMethod().getName();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", reportBean.getUserId(), "", "",
				methodname);
		logger.info("start excution ===================== method getTravelTxn()");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<TravelTxn> travelTxn = new ArrayList<TravelTxn>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

		try {
			String stdate = null;
			String enddate = null;
			StringBuilder str = new StringBuilder(
					"select 'Recharge' as type,rechargeid as transid,rechargeamt as amount,rechargeDate as txndate,status,1 as cancellationstatus,concat(concat(concat(concat(concat(concat(rechargetype,'|'),rechargecircle),'|'),rechargeoperator,'|'),rechargenumber)) as details  from rechargemast where userid= '"
							+ reportBean.getUserId() + "' ");
			if (reportBean.getStDate() != null && reportBean.getEndDate() != null) {
				stdate = formate.format(formatter.parse(reportBean.getStDate()));
				enddate = formate.format(formatter.parse(reportBean.getEndDate()));
				str.append("AND DATE(rechargeDate) BETWEEN STR_TO_DATE(REPLACE('" + stdate
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('" + enddate
						+ "', '/', '-'),'%d-%m-%Y') ");
			}
			str.append(
					"union select 'Flight'  as type,tripid as transid,totalfare as amount,fareqdate as txndate,status,cancellationstatus,getFlightdetails(tripid)  as details from airfare where airfareid  in (select max(airfareid ) from airfare where userid='"
							+ reportBean.getUserId() + "' group by tripid)");
			if (reportBean.getStDate() != null && reportBean.getEndDate() != null) {

				str.append(" AND DATE(fareqdate) BETWEEN STR_TO_DATE(REPLACE('" + stdate
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('" + enddate + "', '/', '-'),'%d-%m-%Y')");
			}
			SQLQuery query = session.createSQLQuery(str.append(" order by txndate desc").toString());
			query.setResultTransformer(Transformers.aliasToBean(TravelTxn.class));
			travelTxn = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", reportBean.getUserId(), "", "",
					"problem in getTravelTxn()" + e.getMessage() + " " + e);
			// logger.debug("problem in getTravelTxn()=======================" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return travelTxn;
	}

	public List<DmtDetailsMastBean> getCustomerDmtDetails(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "getCustomerDmtDetails()" + "Start Date" + reportBean.getStDate()
						+ "End Date" + reportBean.getEndDate() + "|" + reportBean.getAgentId());
		
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<DmtDetailsMastBean> results = new ArrayList<DmtDetailsMastBean>();
		try {
			if (!(reportBean.getUserId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				StringBuilder rRevenuequery = new StringBuilder(
						"SELECT senderid,agentid,txnid,sendermobileno,sendername,benemobileno,beneaccountno,beneifsccode,txnamount,remittancetype,remittancemode,remittancestatus,paymentinfo,DATE_FORMAT(txndatetime, '%d/%m/%Y %T') AS txndatetime,banktranrefno,bankrrn,bankresponse,beneid,kyctype,DATE_FORMAT(updatedatetime, '%d/%m/%Y %T') AS updatedatetime,CASE WHEN distributerid = '-1' THEN ' ' ELSE distributerid END  distributerid  FROM dmtdetailsmast ");
				if (reportBean.getStDate() == null || reportBean.getStDate().isEmpty()
						|| reportBean.getEndDate() == null || reportBean.getEndDate().isEmpty()) {
					rRevenuequery.append(" where date(txndatetime)=CURDATE()");
				} else {
					rRevenuequery.append(" WHERE DATE(txndatetime) BETWEEN STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
				}
				rRevenuequery.append(" and agentid=:agentid");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
						reportBean.getUserId(), "", "", "query " + rRevenuequery);
				// logger.info("start excution ===========getCustomerDmtDetails=========
				// query***"+rRevenuequery);

				SQLQuery query = session.createSQLQuery(rRevenuequery.toString())
						.addScalar("senderid", Hibernate.STRING).addScalar("agentid", Hibernate.STRING)
						.addScalar("txnid", Hibernate.STRING).addScalar("sendermobileno", Hibernate.STRING)
						.addScalar("sendername", Hibernate.STRING).addScalar("benemobileno", Hibernate.STRING)
						.addScalar("beneaccountno", Hibernate.STRING).addScalar("beneifsccode", Hibernate.STRING)
						.addScalar("txnamount", Hibernate.DOUBLE).addScalar("remittancetype", Hibernate.STRING)
						.addScalar("remittancemode", Hibernate.STRING).addScalar("remittancestatus", Hibernate.STRING)
						.addScalar("paymentinfo", Hibernate.STRING).addScalar("txndatetime", Hibernate.STRING)
						.addScalar("banktranrefno", Hibernate.STRING).addScalar("bankrrn", Hibernate.STRING)
						.addScalar("bankresponse", Hibernate.STRING).addScalar("beneid", Hibernate.STRING)
						.addScalar("updatedatetime", Hibernate.STRING).addScalar("kyctype", Hibernate.STRING)
						.addScalar("distributerid", Hibernate.STRING);
				// .addScalar("aggreatorid",Hibernate.STRING);
				query.setParameter("agentid", reportBean.getUserId());
				query.setResultTransformer(Transformers.aliasToBean(DmtDetailsMastBean.class));
				results = query.list();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in getCustomerDmtDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in getCustomerDmtDetails==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "results size" + results.size());
		// logger.info("start excution ===================== method
		// getCustomerDmtDetails Result "+results.size());
		return results;

	}

	
	
	
	public List<AEPSLedger> getCustAepsReport(ReportBean reportBean) {


		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AEPSLedger> results = new ArrayList<AEPSLedger>();
		try {
			if (!(reportBean.getUserId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(AEPSLedger.class);

				if (reportBean.getStDate() == null || reportBean.getStDate().isEmpty()
						|| reportBean.getEndDate() == null || reportBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(reportBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(reportBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", reportBean.getUserId()))
						.add(Restrictions.ne("txnType", "BALANCE ENQUIRY"));
		       
				System.out.println(criteria.toString());
				results = criteria.list();

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return results;
	
	}
	
	
	public List<BbpsPayment> getCustBBPSReport(ReportBean reportBean) {

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
		List<BbpsPayment> results = new ArrayList<BbpsPayment>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(" from BbpsPayment where agentid=:userId");
			if (reportBean.getStDate() != null && !reportBean.getStDate().isEmpty() && reportBean.getEndDate() != null
					&& !reportBean.getEndDate().isEmpty()) {
				serchQuery.append(" and DATE(requestdate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
			} else {

				serchQuery.append(" and DATE(requestdate) = STR_TO_DATE(REPLACE('" + formate.format(new Date())
						+ "', '/', '-'),'%d-%m-%Y')");
				// serchQuery.append(" and DATE(rechargeDate)=DATE(NOW())");
			}
			serchQuery.append(" order by DATE(requestdate) desc");

			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "",
					reportBean.getUserId(), "", "", "serchQuery" + serchQuery);
			// System.out.println("~~~~"+ serchQuery);

			Query query = session.createQuery(serchQuery.toString());
			query.setString("userId", reportBean.getUserId());

			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "",
					reportBean.getUserId(), "", "", "problem in rechargeReportAggreator" + e.getMessage() + " " + e);
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "+results size" + results.size());
		// logger.info("start excution ===================== method rechargeReport
		// Result "+results.size());
		return results;
	
	}
	
	
	
	public List<MATMLedger> getCustMATMReport(ReportBean reportBean) {
		

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<MATMLedger> results = new ArrayList<MATMLedger>();
		try {
			if (!(reportBean.getUserId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(MATMLedger.class);

				if (reportBean.getStDate() == null || reportBean.getStDate().isEmpty()
						|| reportBean.getEndDate() == null || reportBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(reportBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(reportBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", reportBean.getUserId()))
						.add(Restrictions.ne("status", "Pending"));

				results = criteria.list();

			}

		} catch (Exception e) {
			e.printStackTrace();
		
		} finally {
			session.close();
		}
		return results;
		
	}
	
	public List<B2CMoneyTxnMast> getCustDmtDetailsReport(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "getCustDmtDetailsReport()" + "Start Date" + reportBean.getStDate()
						+ "End Date" + reportBean.getEndDate() + "|" + reportBean.getAgentId());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId
		 * (), "", "","Start Date"+reportBean.getStDate() );
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId
		 * (), "", "","End Date" +reportBean.getEndDate() );
		 */
		/*
		 * logger.
		 * info("start excution ============12========= method getCustDmtDetailsReport(userId)"
		 * +reportBean.getUserId()); logger.
		 * info("start excution ============12========= method getCustDmtDetailsReport(userId)"
		 * +reportBean.getStDate()); logger.
		 * info("start excution ============12========= method getCustDmtDetailsReport(userId)"
		 * +reportBean.getEndDate());
		 */
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<B2CMoneyTxnMast> results = new ArrayList<B2CMoneyTxnMast>();
		try {
			if (!(reportBean.getUserId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				StringBuilder rRevenuequery = new StringBuilder("FROM B2CMoneyTxnMast");
				if (reportBean.getStDate() == null || reportBean.getStDate().isEmpty()
						|| reportBean.getEndDate() == null || reportBean.getEndDate().isEmpty()) {
					rRevenuequery.append(" where date(txndate)=CURDATE()");
				} else {
					rRevenuequery.append(" WHERE DATE(txndate) BETWEEN STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getStDate()))
							+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
				}
				rRevenuequery.append(" and userid=:userid");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
						reportBean.getUserId(), "", "", "query " + rRevenuequery);
				// logger.info("start excution ===========getCustDmtDetailsReport=========
				// query***"+rRevenuequery);

				Query query = session.createQuery(rRevenuequery.toString())
				/*
				 * .addScalar("senderid",Hibernate.STRING)
				 * .addScalar("agentid",Hibernate.STRING) .addScalar("txnid",Hibernate.STRING)
				 * .addScalar("sendermobileno",Hibernate.STRING)
				 * .addScalar("sendername",Hibernate.STRING)
				 * .addScalar("benemobileno",Hibernate.STRING)
				 * .addScalar("beneaccountno",Hibernate.STRING)
				 * .addScalar("beneifsccode",Hibernate.STRING)
				 * .addScalar("txnamount",Hibernate.DOUBLE)
				 * .addScalar("remittancetype",Hibernate.STRING)
				 * .addScalar("remittancemode",Hibernate.STRING)
				 * .addScalar("remittancestatus",Hibernate.STRING)
				 * .addScalar("paymentinfo",Hibernate.STRING)
				 * .addScalar("txndatetime",Hibernate.STRING)
				 * .addScalar("banktranrefno",Hibernate.STRING)
				 * .addScalar("bankrrn",Hibernate.STRING)
				 * .addScalar("bankresponse",Hibernate.STRING)
				 * .addScalar("beneid",Hibernate.STRING)
				 * .addScalar("updatedatetime",Hibernate.STRING)
				 * .addScalar("kyctype",Hibernate.STRING)
				 * .addScalar("distributerid",Hibernate.STRING)
				 */;
				query.setParameter("userid", reportBean.getUserId());
				// query.setResultTransformer(Transformers.aliasToBean(B2CMoneyTxnMast.class));
				results = query.list();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in getCustomerDmtDetails" + e.getMessage() + " " + e);
			// logger.debug("problem in getCustomerDmtDetails==================" +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "results size " + results.size());
		// logger.info("start excution ===================== method
		// getCustomerDmtDetails Result "+results.size());
		return results;

	}

	public List<PartnerLedgerBean> getPartnerLedger(ReportBean reportBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "",
				"getPartnerLedger()" + "PName" + reportBean.getPartnerName() + "Start Date" + reportBean.getStDate()
						+ "End Date" + reportBean.getEndDate() + "|" + reportBean.getAgentId());
		/*
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId
		 * (), "", "","End Date" +reportBean.getPartnerName() );
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId
		 * (), "", "","Start Date"+reportBean.getStDate() );
		 * Commonlogger.log(Commonlogger.INFO,
		 * this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId
		 * (), "", "","End Date" +reportBean.getEndDate() );
		 */
		/*
		 * logger.
		 * info("start excution*********************************** method getPartnerLedger(PartnerName)"
		 * +reportBean.getPartnerName()); logger.
		 * info("start excution *********************************** method getPartnerLedger(stDate)"
		 * +reportBean.getStDate()); logger.
		 * info("start excution *********************************** method getPartnerLedger(endDate)"
		 * +reportBean.getEndDate()); logger.
		 * info("start excution *********************************** method getPartnerLedger(endDate)"
		 * +reportBean.getAggreatorid());
		 */
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<PartnerLedgerBean> results = new ArrayList<PartnerLedgerBean>();
		try {
			if (reportBean.getPartnerName().length > 0) {

				String[] menuCode = reportBean.getPartnerName();

				StringBuilder partnerName = new StringBuilder();
				for (int i = 0; i < reportBean.getPartnerName().length; i++) {
					if (i == 0) {
						partnerName.append("'" + menuCode[i] + "'");
					} else {
						partnerName.append(",'" + menuCode[i] + "'");
					}
				}

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
				StringBuilder partnerLedgerquery = new StringBuilder(
						"SELECT id,partnername,DATE_FORMAT(txndate, '%d/%m/%Y') AS txndate,moneyload,moneyconsume,balances FROM partnerledger ");
				partnerLedgerquery.append(" WHERE DATE(txndate) BETWEEN STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y')  AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(reportBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");
				partnerLedgerquery.append(" and partnername in (" + partnerName + ")");
				partnerLedgerquery.append(" and aggreatorid = '" + reportBean.getAggreatorid() + "'");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
						reportBean.getUserId(), "", "", "query " + partnerLedgerquery);
				// logger.info("start excution ************************* getPartnerLedger
				// *********** query***"+partnerLedgerquery);
				SQLQuery query = session.createSQLQuery(partnerLedgerquery.toString());

				// query.setParameter("partnername",
				// StringUtils.join(reportBean.getPartnerName(), ','));
				query.setResultTransformer(Transformers.aliasToBean(PartnerLedgerBean.class));
				results = query.list();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
					reportBean.getUserId(), "", "", "problem in getPartnerLedger " + e.getMessage() + " " + e);
			// logger.debug("problem in getPartnerLedger ************************* " +
			// e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), reportBean.getAggreatorid(), "",
				reportBean.getUserId(), "", "", "results size" + results.size());
		// logger.info("start excution ************************* method getPartnerLedger
		// Result "+results.size());
		return results;

	}

	public List<AgentClosingBalBean> getAgentClosingBalReport(String stDate, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getAgentClosingBalReport()" + "|" + stDate);

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AgentClosingBalBean> results = new ArrayList<AgentClosingBalBean>();
		try {

			if (!(stDate == null)) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

				if ("All".equalsIgnoreCase(aggreatorid)) {
					SQLQuery query = session.createSQLQuery(
							"SELECT id,NAME,credit,debit,finalbalance,DATE_FORMAT(auditdate,'%d/%m/%Y') auditdate FROM walletbalance_audit WHERE  DATE(auditdate) = STR_TO_DATE('"
									+ formate.format(formatter.parse(stDate))
									+ "','%d/%m/%Y')  AND usertype IN (2,3,6) ORDER BY id");
					query.setResultTransformer(Transformers.aliasToBean(AgentClosingBalBean.class));
					results = query.list();
				} else {
					SQLQuery query = session.createSQLQuery(
							"SELECT id,NAME,credit,debit,finalbalance,DATE_FORMAT(auditdate,'%d/%m/%Y') auditdate FROM walletbalance_audit WHERE  DATE(auditdate) = STR_TO_DATE('"
									+ formate.format(formatter.parse(stDate))
									+ "','%d/%m/%Y')  AND usertype IN (2,3,6) AND aggreatorid='" + aggreatorid
									+ "' ORDER BY id");
					query.setResultTransformer(Transformers.aliasToBean(AgentClosingBalBean.class));
					results = query.list();
				}
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getAgentClosingBalReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getAgentClosingBalReport*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public List<WalletTxnDetailsRpt> getWalletTxnDetailsReport(String stDate, String endDate, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getWalletTxnDetailsReport()" + "|" + stDate);

		// logger.info("start excution
		// ******************************************************** method
		// getWalletTxnDetailsReport()"+stDate);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<WalletTxnDetailsRpt> results = new ArrayList<WalletTxnDetailsRpt>();
		try {

			StringBuilder sqlQuery = new StringBuilder(
					"SELECT id,name,DATE_FORMAT(txndate,'%d/%m/%Y') txndate,txnid,resptxnid,txncredit,txndebit,payeedtl,closingbal,txndesc  FROM passbook ,walletmast  WHERE passbook.walletid=walletmast.walletid ");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

			if ("All".equalsIgnoreCase(aggreatorid)) {
				if (!(stDate == null && endDate == null)) {
					sqlQuery.append("AND DATE(txndate) between STR_TO_DATE('" + formate.format(formatter.parse(stDate))
							+ "','%d/%m/%Y') and STR_TO_DATE('" + formate.format(formatter.parse(endDate))
							+ "','%d/%m/%Y')  ORDER BY id");
				} else {
					sqlQuery.append("AND DATE(txndate) = STR_TO_DATE('" + formate.format(new Date())
							+ "','%d/%m/%Y') ORDER BY id");
				}
			} else {
				if (!(stDate == null && endDate == null)) {
					sqlQuery.append("AND DATE(txndate) between STR_TO_DATE('" + formate.format(formatter.parse(stDate))
							+ "','%d/%m/%Y') and STR_TO_DATE('" + formate.format(formatter.parse(endDate))
							+ "','%d/%m/%Y')  AND aggreatorid='" + aggreatorid + "' ORDER BY id");
				} else {
					sqlQuery.append("AND DATE(txndate) = STR_TO_DATE('" + formate.format(new Date())
							+ "','%d/%m/%Y') AND aggreatorid='" + aggreatorid + "' ORDER BY id");
				}
			}
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			query.setResultTransformer(Transformers.aliasToBean(WalletTxnDetailsRpt.class));
			results = query.list();
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getWalletTxnDetailsReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getWalletTxnDetailsReport*****************************************" +
			// e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public List<B2CMoneyTxnMast> getBtocDmtReport(String stDate, String endDate, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getBtocDmtReport()" + "|" + stDate);
		// logger.info("start excution
		// ******************************************************** method
		// getBtocDmtReport()"+stDate);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<B2CMoneyTxnMast> results = new ArrayList<B2CMoneyTxnMast>();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			// SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			if (stDate == null) {
				stDate = formatter.format(new Date());
				Date minDate = formatter.parse(stDate);
				Date eDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
				endDate = formatter.format(eDate);
			}
			if (!(stDate == null)) {

				Date minDate = formatter.parse(endDate);
				Date eDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
				endDate = formatter.format(eDate);

//		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
//	    SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");	
//		SQLQuery  query=session.createSQLQuery("SELECT id,name,DATE_FORMAT(txndate,'%d/%m/%Y') txndate,txnid,resptxnid,txncredit,txndebit,payeedtl,closingbal,txndesc  FROM passbook ,walletmast  WHERE passbook.walletid=walletmast.walletid AND DATE(txndate) = STR_TO_DATE('"+formate.format(formatter.parse(stDate))+"','%d/%m/%Y')  AND aggreatorid='"+aggreatorid+"' ORDER BY id");
//		query.setResultTransformer(Transformers.aliasToBean(WalletTxnDetailsRpt.class));
				Criteria criteria = session.createCriteria(B2CMoneyTxnMast.class);
				criteria.add(Restrictions.eq("aggreatorId", aggreatorid));
				criteria.add(Restrictions.ge("txndate", formatter.parse(stDate)));
				criteria.add(Restrictions.le("txndate", formatter.parse(endDate)));
				results = criteria.list();
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getBtocDmtReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getBtocDmtReport*****************************************" + e.getMessage(),
			// e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public List<EkycResponseBean> getKycReport(String stDate, String endDate, String aggreatorid) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"getKycReport()" + "|" + stDate);

		// logger.info("start excution
		// ******************************************************** method
		// getKycReport()"+stDate);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<EkycResponseBean> results = new ArrayList<EkycResponseBean>();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			// SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			if (stDate == null) {
				stDate = formatter.format(new Date());
				Date minDate = formatter.parse(stDate);
				Date eDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
				endDate = formatter.format(eDate);
			}
			if (!(stDate == null)) {

				Date minDate = formatter.parse(endDate);
				Date eDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
				endDate = formatter.format(eDate);
				Criteria criteria = session.createCriteria(EkycResponseBean.class);
				criteria.add(Restrictions.eq("aggreator", aggreatorid));
				criteria.add(Restrictions.ge("time", formatter.parse(stDate)));
				criteria.add(Restrictions.le("time", formatter.parse(endDate)));
				results = criteria.list();
			}

		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
					"problem in getKycReport" + e.getMessage() + " " + e);
			// logger.debug("problem in
			// getKycReport*****************************************" + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid, "", "", "", "",
				"results size" + results.size());
		// logger.info(" **********************return
		// **********************************************************"+results.size());
		return results;

	}

	public static void main(String[] args) {
		ReportDaoImpl reportDaoImpl = new ReportDaoImpl();
		// SMSSendDetails sMSSendDetails=new SMSSendDetails();
		// sMSSendDetails.setAggreatorid("AGGR001027");
		// reportDaoImpl.getEscrowAccBal("AGGR001035");
		// reportDaoImpl.getAgentConsolidatedReport("AGEN001070","01-Aug-2017","29-Aug-2017");
		// EscrowBean escrowBean=new EscrowBean();
		// escrowBean.setAggreatorid("AGGR001035");
		// reportDaoImpl.getEscrowTxnList(escrowBean);
		// reportDaoImpl.getPrePiadCardReConReport("AGGR001035",
		// "01-Dec-2016","14-Feb-2017");
		// reportDaoImpl.getDmtDetailsReportList(new WalletMastBean());

		// reportDaoImpl.getWalletTxnDetailsReport("31-Oct-2017","AGGR001035");
		
		AEPSsettlement aeps = new AEPSsettlement();
		aeps.setAgentid("BPA001014");
		aeps.setDebitAmount("274.5");
		
		reportDaoImpl.isSaveAepsSettlement(aeps);
	}
	

	@Override
	public Map<String, String> getSettledAggreator() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getSettledAggreator()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT distinct(aggreatorid) FROM walletmast WHERE whitelabel = 1");
			List<String> rows = query.list();
			for (String row : rows) {
				System.out.println(row);
				aggMap.put(row, row);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getSettledAggreator" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);
	}

	
	@Override
	public Map<String, String> getAepsSettlementCharge(String agentId) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		List objectList=new ArrayList<>();
		try {
		 Query query = session.createSQLQuery("call surchargemodel(:id,:txnAmount,:transType,:isBank)");
		 query.setParameter("id", agentId);
		 query.setParameter("txnAmount", "100");
		 query.setParameter("transType", "AEPSPAYOUT");
		 query.setParameter("isBank", 1);	
		 
		 objectList=query.list();
		 Object[] obj = (Object[])objectList.get(0);
		 double surcharge = Double.parseDouble(""+obj[0]);
		 aggMap.put("settlementCharge", String.valueOf(surcharge));
		 System.out.println(obj[0]+"SURCHARGE  "+surcharge+ "   AGENTID=  "+agentId+"  AGENT");
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getSettledAggreator" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);
	}
	
	
	
	
	@Override
	public Map<String, String> getTotalSettlementAmount(String aggregatorid) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getTotalSettlementAmount()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			String queryStr = "SELECT sum(txncredit) FROM wallettransaction WHERE walletid IN (SELECT walletid FROM walletmast WHERE aggreatorid='"
					+ aggregatorid + "') and txncode in(80,88);";

			SQLQuery query = session.createSQLQuery(queryStr);

			Object obj = query.uniqueResult();

			aggMap.put("totalSettlementAmount", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getTotalSettlementAmount" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);
	}

	@Override
	public Map<String, String> getTotalAepsTransactionAmount(String aggregatorid) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
				"getTotalAepsTransactionAmount()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			String queryStr = "SELECT sum(txncredit)-sum(txndebit)  FROM wallettransaction WHERE walletid IN (SELECT walletid FROM walletmast WHERE aggreatorid='"
					+ aggregatorid + "') and txncode in (110,115);";

			SQLQuery query = session.createSQLQuery(queryStr);

			Object obj = query.uniqueResult();

			aggMap.put("totalAepsTransactionAmount", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in getTotalAepsTransactionAmount" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);
	}

	@Override
	public Map<String, String> postAggreatorSettlement(AEPSAggregatorSettlement aepsAggregatorSettlement) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "postAggreatorSettlement()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Map<String, String> aggMap = new HashMap<String, String>();
		try {
			SQLQuery walletIDQuery = null;
			SQLQuery creditWalletQuery = null;
			SQLQuery nextQuery = null;

			String queryStr = "SELECT walletid FROM walletmast WHERE id IN (SELECT cashdepositewallet  FROM walletmast WHERE id='"
					+ aepsAggregatorSettlement.getAggregatorid() + "');";

			walletIDQuery = session.createSQLQuery(queryStr);
			Object obj = walletIDQuery.uniqueResult();
			System.out.println("obj : " + obj);

			String queryNext = "SELECT nextval(:ad,:aggregatorid)";
			nextQuery = session.createSQLQuery(queryNext);
			nextQuery.setParameter("aggregatorid", aepsAggregatorSettlement.getAggregatorid());
			nextQuery.setParameter("ad", "ADDED");

			Object nextObj = nextQuery.uniqueResult();
			System.out.println("nextObj " + nextObj);

			if (obj.toString() != null) {

				try {
					String queryStr2 = "call creditwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";

					double amt = Double.valueOf(aepsAggregatorSettlement.getAvailableSettlementAmount());

					creditWalletQuery = session.createSQLQuery(queryStr2);
					creditWalletQuery.setParameter("aggregatorid", aepsAggregatorSettlement.getAggregatorid());
					creditWalletQuery.setParameter("walletid", obj.toString());
					creditWalletQuery.setParameter("amount", amt);
					creditWalletQuery.setParameter("narration", "AEPS Settlement");
					creditWalletQuery.setParameter("resptxnid", "" + nextObj);
					creditWalletQuery.setParameter("txncode", 110);

					creditWalletQuery.executeUpdate();
					transaction.commit();

					aggMap.put("creditwallet", "Amount " + amt + " has been settled.");
				} catch (Exception e) {
					e.printStackTrace();
					aggMap.put("error", "Something happend wrong!!!");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "",
					"problem in postAggreatorSettlement" + e.getMessage() + " " + e);
			aggMap.put("error", "Data not saved!!!");
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(aggMap);
	}

	@Override
	public Map<String, Object> postAepsSettlement(String aeAgentMobile) {
		// TODO Auto-generated method stub
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "postAepsSettlement()");
		
		Map<String, Object> aggObjMap = new HashMap<String, Object>();
		try {
			
			String walletid = null;
			String name = null;
			
			for(WalletMastBean walletBean : getWalletIdByAgentIdORMobile(aeAgentMobile)) {
				walletid = walletBean.getWalletid();
				name = walletBean.getName();
				aggObjMap.put("accountNumber",walletBean.getAccountNumber());
				aggObjMap.put("ifscCode",walletBean.getIfscCode());
				aggObjMap.put("bankName",walletBean.getBankName());
				
				
			}
	        
	        if(walletid !=null && !walletid.equalsIgnoreCase("")) {
	        	
	        	factory = DBUtil.getSessionFactory();
	    		session = factory.openSession();
	    		Transaction transaction = session.beginTransaction();
	    		
//		        String query = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit, sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,124) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String query80_81 = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89,133) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String query124 = "select txn.txncode as txncode, sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate)<=date(now())";
		        
		        Query qry = session.createQuery(query80_81);
		        List txn80_81List = qry.list();
		        
		        qry = session.createQuery(query124);
		        List txn124List = qry.list();
		        
		        int mapIndex = 0;
		        for(int i=0; i<txn80_81List.size();i++) {
	    			Object[] obj=(Object[])txn80_81List.get(i);
	    			
		    			Map<String, String> txnBeanMap = new HashMap<String, String>();
		    			txnBeanMap.put("txncode",obj[0].toString());
		    			txnBeanMap.put("sumCredit",obj[1].toString());
	    			
	    			aggObjMap.put(""+ mapIndex++, txnBeanMap);
	    		}
		        
		        for(int i=0; i<txn124List.size();i++) {
	    			Object[] obj=(Object[])txn124List.get(i);
	    			
		    			Map<String, String> txnBeanMap = new HashMap<String, String>();
		    			txnBeanMap.put("txncode",obj[0] !=null ? obj[0].toString() : "0");
		    			txnBeanMap.put("sumDebit",obj[1] != null ? obj[1].toString() : "0");
	    			
	    			aggObjMap.put(""+ mapIndex++, txnBeanMap);
	    		}
		        
		        
	    		try {
					String finalBalQuery = "select wbb.finalBalance from WalletBalanceBean wbb where wbb.walletid='"+walletid+"'";
					Query finalBalSql = session.createQuery(finalBalQuery);
					Double finalBalance =(Double)finalBalSql.uniqueResult();
					aggObjMap.put("finalBalance", finalBalance);
				} catch (Exception e) {
					e.printStackTrace();
					aggObjMap.put("finalBalance", 0.0);
				}
	    		aggObjMap.put("name",name);
	        }
	        else {
	        	aggObjMap.put("statusCode", "err01");
	        	aggObjMap.put("status", "ERROR");
	        	aggObjMap.put("statusMsg", "Agent Id/Mobile No does not exist.");
	        }
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","problem in postAepsSettlement" + e.getMessage() + " " + e);
			
			aggObjMap.put("statusCode", "err00");
			aggObjMap.put("status", "ERROR");
        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
		} finally {
			session.close();
		}
		return aggObjMap;
	}

	
	@Override
	public Map<String, Object> postAepsSettlementData(String userId,String account) {
		// TODO Auto-generated method stub
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "postAepsSettlement()");
		  List<AddBankAccount> results=new ArrayList<AddBankAccount>();
		Map<String, Object> aggObjMap = new HashMap<String, Object>();
		try {
			
			String walletid = null;
			String name = null;
			System.out.println("Account   "+account);
			WalletUserDao walletUserDao=new WalletUserDaoImpl();
			Map<String, String> data = walletUserDao.agentAccounts(account,userId);
			
			aggObjMap.put("accountNumber",data.get("accountNumber"));
			aggObjMap.put("ifscCode",data.get("ifscCode"));
			aggObjMap.put("bankName",data.get("bankName"));	
			walletid=data.get("walletId");
			name=data.get("name");
			
	        if(walletid !=null && !walletid.equalsIgnoreCase("")) {
	        	
	        	factory = DBUtil.getSessionFactory();
	    		session = factory.openSession();
	    		Transaction transaction = session.beginTransaction();
	    		
//		        String query = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit, sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,124) and date(txn.txnDate)<=date(now()) group by txn.txncode";
// change on 30-01-2021
	    	  /* 
	    		String query80_81 = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89,133) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String query124 = "select txn.txncode as txncode, sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate)<=date(now())";
		      */ 
/*	    		
String query80_81 = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit from wallettransaction txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89,133) and date(txn.txnDate) between DATE_SUB(date(now()) , INTERVAL 7 DAY) and date(now()) group by txn.txncode";
String query124 = "select txn.txncode as txncode, sum(txn.txndebit) as sumDebit from wallettransaction txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate) between DATE_SUB(date(now()) , INTERVAL 7 DAY) and date(now())";
Query qry = session.createSQLQuery(query80_81);
List txn80_81List = qry.list();
qry = session.createSQLQuery(query124);
List txn124List = qry.list();
*/
	    		
   //System.out.println(query80_81+" \n"+query124);
   //System.out.println("80  81 "+txn80_81List.size());	        
   // System.out.println(" 124  "+txn124List.size());      
		       
		        String query80_81 = "select txn.txncode as txncode, sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89,133) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String query124 = "select txn.txncode as txncode, sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate)<=date(now())";
		        
		        Query qry = session.createQuery(query80_81);
		        List txn80_81List = qry.list();
		        
		        qry = session.createQuery(query124);
		        List txn124List = qry.list();
		        
		        
		        int mapIndex = 0;
		        for(int i=0; i<txn80_81List.size();i++) {
	    			Object[] obj=(Object[])txn80_81List.get(i);
	    			
		    			Map<String, String> txnBeanMap = new HashMap<String, String>();
		    			txnBeanMap.put("txncode",obj[0].toString());
		    			txnBeanMap.put("sumCredit",obj[1].toString());
	    			
	    			aggObjMap.put(""+ mapIndex++, txnBeanMap);
	    		}
		        
		        for(int i=0; i<txn124List.size();i++) {
	    			Object[] obj=(Object[])txn124List.get(i);
	    			
		    			Map<String, String> txnBeanMap = new HashMap<String, String>();
		    			txnBeanMap.put("txncode",obj[0] !=null ? obj[0].toString() : "0");
		    			txnBeanMap.put("sumDebit",obj[1] != null ? obj[1].toString() : "0");
	    			
	    			aggObjMap.put(""+ mapIndex++, txnBeanMap);
	    		}
		        
		        
	    		try {
					String finalBalQuery = "select wbb.finalBalance from WalletBalanceBean wbb where wbb.walletid='"+walletid+"'";
					Query finalBalSql = session.createQuery(finalBalQuery);
					Double finalBalance =(Double)finalBalSql.uniqueResult();
					aggObjMap.put("finalBalance", finalBalance);
				} catch (Exception e) {
					e.printStackTrace();
					aggObjMap.put("finalBalance", 0.0);
				}
	    		aggObjMap.put("name",name);
	        }
	        else {
	        	aggObjMap.put("statusCode", "err01");
	        	aggObjMap.put("status", "ERROR");
	        	aggObjMap.put("statusMsg", "Agent Id/Mobile No does not exist.");
	        }
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","problem in postAepsSettlement" + e.getMessage() + " " + e);
			
			aggObjMap.put("statusCode", "err00");
			aggObjMap.put("status", "ERROR");
        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
		} finally {
			session.close();
		}
		return aggObjMap;
	}

	public List<AddBankAccount> getWalletIdByAgentIdORMobileData(String account) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List agentList=new ArrayList();
		try {
			 String sqlquery="select * from addbankaccount where accountNumber='"+account+"' ";
			 Query query=session.createSQLQuery(sqlquery);
		     agentList = query.list();
			/*
		    Criteria walletCri = session.createCriteria(AddBankAccount.class);
			Criterion id = Restrictions.eq("userId",agentORmobile);
			Criterion mob = Restrictions.eq("mobileNo",agentORmobile);
            LogicalExpression orExp = Restrictions.or(id, mob);
			walletCri.add( orExp );
			return walletCri.list();
			*/
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return agentList;
	}
	
	
	
	public List<WalletMastBean> getWalletIdByAgentIdORMobile(String agentORmobile) {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			Criteria walletCri = session.createCriteria(WalletMastBean.class);
			
			Criterion id = Restrictions.eq("id",agentORmobile);
			Criterion mob = Restrictions.eq("mobileno",agentORmobile);

			LogicalExpression orExp = Restrictions.or(id, mob);
			walletCri.add( orExp );
			
			return walletCri.list();
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	@Override
	public Map<String, Object> isSaveAepsSettlement(AEPSsettlement aeps) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement()");
		Map<String, Object> aggObjMap = new HashMap<String, Object>();
		String mailTemp = "";
		try {
			mailTemp =
					
					"<p>Settlement Team,</p>\r\n" + 
					"<p>There is request for AEPS/m-ATM settlement. Kindly do the needful. Below is request details for settlement.</p>\r\n" + 
					"<p>Agent id:-&nbsp; <<agentid>></p>\r\n" + 
					"<p>Agent Name:- <<agentname>></p>\r\n" + 
					"<p>Respective Txnid:- <<resptxnid>></p>\r\n" +
					"<p>Settlement Amount:- <<settlementamount>></p>\r\n" +
					"<p>Account No:- <<accountno>></p>\r\n" +
					"<p>IFSC:- <<ifsc>></p>\r\n" +
					"<p>Bank Name:- <<bankname>></p>\r\n" + 
							/* "<p>Bank Name:- <<bankname>></p>\r\n" + */
					"<p>IMPS/NEFT Team. Kindaly take a confirmation from acount team before processing.</p>\r\n" + 
					"<p>&nbsp;</p>\r\n" + 
					"<p><em><strong>Note</strong></em>:- This is auto generated mail. Please do not reply.</p>\r\n" + 
					"<p>Thanks,</p>\r\n" + 
					"<p><strong><<source>></strong></p>";
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() "+mailTemp);
			
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() "+"problem in mail templates");
			e.printStackTrace();
		}
		try {
			
			if(aeps.getCreditAmount()!=null && !aeps.getCreditAmount().equalsIgnoreCase(""))
				aeps.setCreditAmount(new DecimalFormat("0.00").format(Double.valueOf(aeps.getCreditAmount())));
			
			if(aeps.getDebitAmount()!=null && !aeps.getDebitAmount().equalsIgnoreCase(""))
				aeps.setDebitAmount(new DecimalFormat("0.00").format(Double.valueOf(aeps.getDebitAmount())));

			if(aeps.getServiceCharges()!=null && !aeps.getServiceCharges().equalsIgnoreCase(""))
				aeps.setServiceCharges(new DecimalFormat("0.00").format(Double.valueOf(aeps.getServiceCharges())));
			
			String walletid = null;
			String aggregatorID = null;
			WalletMastBean walletMast =  null;
			for(WalletMastBean walletBean : getWalletIdByAgentIdORMobile(aeps.getAgentid())) {
				walletid = walletBean.getWalletid();
				aggregatorID = walletBean.getAggreatorid();
				walletMast = walletBean;
			}
	        if(walletid !=null && !walletid.equalsIgnoreCase("")) {
	        	
	        	Double aepsAmount = 0.0;
				Double canAmount = 0.0;
				Double finalBalance = 0.0;
				Double toSettledAmt= 0.0;
				Double serviceAmt= 0.0;
				
				toSettledAmt = Double.valueOf(aeps.getDebitAmount());
				
				serviceAmt = Double.valueOf(aeps.getServiceCharges());
				
				factory = DBUtil.getSessionFactory();
	    		session = factory.openSession();
	    		Transaction transaction = session.beginTransaction();
	    		
	    		String sumCredit80_81Str = "select sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String sumDebit124Str = "select sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate)<=date(now())";
		        
		        Query sumCredit80_81Qry = session.createQuery(sumCredit80_81Str);
		        List sumCredit80_81List = sumCredit80_81Qry.list();
		        
		        sumCredit80_81Qry = session.createQuery(sumDebit124Str);
		        Double preAmount = (Double) sumCredit80_81Qry.uniqueResult();
		        
				for(int i=0; i<sumCredit80_81List.size();i++) {
					 aepsAmount +=(Double)sumCredit80_81List.get(i);
				}
				
				if(aepsAmount>0.0) {
				 canAmount = aepsAmount - (preAmount != null ? preAmount : 0);
				}
				
				Double totalSettle =0.0; 
				
				if(toSettledAmt > serviceAmt && serviceAmt >= 0) {
					totalSettle = toSettledAmt + serviceAmt;
					
					try {
						String finalBalQuery = "select wbb.finalBalance from WalletBalanceBean wbb where wbb.walletid='"+walletid+"'";
						Query finalBalSql = session.createQuery(finalBalQuery);
						finalBalance =(Double)finalBalSql.uniqueResult();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				 
				if(totalSettle < 0 || totalSettle == 0){
					aggObjMap.put("statusCode", "err01");
					aggObjMap.put("status", "ERROR");
					aggObjMap.put("statusMsg", "You want to settle "+toSettledAmt+" + "+serviceAmt+", We can not proceed.");
				}
				else if(totalSettle > finalBalance){
					aggObjMap.put("statusCode", "err01");
					aggObjMap.put("status", "ERROR");
					aggObjMap.put("statusMsg", "You want to settle "+totalSettle+" amount, But Agent's current balance is less, which is "+finalBalance);
				}
				else {
					if(false/*toSettledAmt > canAmount*/){
						aggObjMap.put("statusCode", "err01");
						aggObjMap.put("status", "ERROR");
						aggObjMap.put("statusMsg", "You can not settle "+totalSettle+" amount, Because you entered more than "+canAmount);
					}
					else {
			    		
			    		SQLQuery nextQuery=null;
			    		
			        	String queryNext = "SELECT nextval(:ad,:aggregatorId)";
						nextQuery = session.createSQLQuery(queryNext);
						nextQuery.setParameter("aggregatorId", aggregatorID);
						nextQuery.setParameter("ad", "REVOKE");
						Object nextObj = nextQuery.uniqueResult();
						
						String queryStr = "call debitwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";
						String creditStr = "call creditwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";
						
						SQLQuery debitWalletQuery=null;
						debitWalletQuery = session.createSQLQuery(queryStr);
						debitWalletQuery.setParameter("aggregatorid", aggregatorID);
						debitWalletQuery.setParameter("walletid", walletid);
						debitWalletQuery.setParameter("amount", new DecimalFormat("0.00").format(toSettledAmt));
						debitWalletQuery.setParameter("narration", "aepssettlements");
						debitWalletQuery.setParameter("resptxnid", ""+nextObj);
						debitWalletQuery.setParameter("txncode", 124);
		
						//int i = debitWalletQuery.executeUpdate();
						
						if(debitWalletQuery.executeUpdate()>0) {
							transaction = session.beginTransaction();
							if(serviceAmt>0) {
							SQLQuery debitWallet125Query=null;
							debitWallet125Query = session.createSQLQuery(queryStr);
							debitWallet125Query.setParameter("aggregatorid", aggregatorID);
							debitWallet125Query.setParameter("walletid", walletid);
							debitWallet125Query.setParameter("amount", new DecimalFormat("0.00").format(serviceAmt));
							debitWallet125Query.setParameter("narration", "aepssettlements");
							debitWallet125Query.setParameter("resptxnid", ""+nextObj);
							debitWallet125Query.setParameter("txncode", 125);
							debitWallet125Query.executeUpdate();							
							 }
							
//							transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(wBean.getDistributerid(), aggregatorId), distsurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,82);
							//int j = debitWallet125Query.executeUpdate(); 
							
							if(true){
								
								if(walletMast != null && "1".equalsIgnoreCase(walletMast.getWhiteLabel())) {
									try {
									WalletMastBean walletMastAggBean = (WalletMastBean)session.get(WalletMastBean.class, walletMast.getAggreatorid());
									WalletMastBean walletMastCashAggBean = (WalletMastBean)session.get(WalletMastBean.class, walletMastAggBean.getCashDepositeWallet());
									
									SQLQuery debitWallet157Query=null;
									debitWallet157Query = session.createSQLQuery(queryStr);
									debitWallet157Query.setParameter("aggregatorid", aggregatorID);
									debitWallet157Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
									debitWallet157Query.setParameter("amount", new DecimalFormat("0.00").format(toSettledAmt));
									debitWallet157Query.setParameter("narration", "aepssettlements");
									debitWallet157Query.setParameter("resptxnid", ""+nextObj);
									debitWallet157Query.setParameter("txncode", 157);
									if (debitWallet157Query.executeUpdate() > 0) {
										try {
											if(serviceAmt > 0) {
											SQLQuery creditWallet158Query = null;
											creditWallet158Query = session.createSQLQuery(creditStr);
											creditWallet158Query.setParameter("aggregatorid", aggregatorID);
											creditWallet158Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
											creditWallet158Query.setParameter("amount",
													new DecimalFormat("0.00").format(serviceAmt));
											creditWallet158Query.setParameter("narration", "aepssettlements charge");
											creditWallet158Query.setParameter("resptxnid", "" + nextObj);
											creditWallet158Query.setParameter("txncode", 158);
											creditWallet158Query.executeUpdate();
											}
//											transaction.commit();
//											transaction = session.beginTransaction();
											SQLQuery debitWallet159Query = null;
											debitWallet159Query = session.createSQLQuery(queryStr);
											debitWallet159Query.setParameter("aggregatorid", aggregatorID);
											debitWallet159Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
											debitWallet159Query.setParameter("amount",7.08);
											debitWallet159Query.setParameter("narration", "aepssettlements charge");
											debitWallet159Query.setParameter("resptxnid", "" + nextObj);
											debitWallet159Query.setParameter("txncode", 159);
											debitWallet159Query.executeUpdate();
										} catch (Exception e) {
											e.printStackTrace();
										}
									}else {
										aggObjMap.put("statusCode", "err00");
										aggObjMap.put("status", "ERROR");
							        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
							        	transaction.rollback();
							        	return aggObjMap;
									}
									transaction.commit();
								}catch (Exception e) {
									aggObjMap.put("statusCode", "err00");
									aggObjMap.put("status", "ERROR");
						        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
						        	transaction.rollback();
						        	return aggObjMap;
								}	
									
								}else {
									transaction.commit();
								}
								
								aggObjMap.put("statusCode", "Suc01");
								aggObjMap.put("status", "Success");
								aggObjMap.put("statusMsg", "Agent("+aeps.getAgentid()+")'s "+aeps.getDebitAmount()+" amount with "+aeps.getServiceCharges()+" service charge has been successfully Settled.");
								
								transaction = session.beginTransaction();
								AepsSettlementBean aepsBean = new AepsSettlementBean();
								aepsBean.setAccountNumber(walletMast.getAccountNumber());
								aepsBean.setAgentId(walletMast.getId());
								aepsBean.setAgentName(walletMast.getName());
								aepsBean.setAgentWalletId(walletMast.getWalletid());
								aepsBean.setAmount(toSettledAmt);
								aepsBean.setIfscCode(walletMast.getIfscCode());
								aepsBean.setRespTxnId(nextObj.toString());
								aepsBean.setStatus("PENDING");
								aepsBean.setSettlementDate(new Date());
								aepsBean.setAggregatorId(walletMast.getAggreatorid());
								aepsBean.setPaymentMode("IMPS");
								aepsBean.setCharges(serviceAmt);
								session.save(aepsBean);
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() commit start for aepssettlement(Movetobank)");
								transaction.commit();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() commit done for aepssettlement(Movetobank)");

								try {
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() start replacing in mail temp for aepssettlement(Movetobank)  "+mailTemp);
									mailTemp = mailTemp.replace("<<agentid>>", aeps.getAgentid())
											.replace("<<bankname>>", walletMast.getBankName()!=null?walletMast.getBankName():"")
											.replace("<<agentname>>", walletMast.getName()).replace("<<resptxnid>>", "" + nextObj)
											.replace("<<settlementamount>>", "" + toSettledAmt)
											.replace("<<accountno>>", walletMast.getAccountNumber()).replace("<<ifsc>>",walletMast.getIfscCode())
											;
//									                                                          String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId,String walletid,String sendername,String type
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() done replacing in mail temp for aepssettlement(Movetobank)  "+mailTemp);
									
										
										
										  try {
										  
										  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
										  LocalDateTime now = LocalDateTime.now(); String date = dtf.format(now);
										  
										  PayoutApiMaster apiMaster = new PayoutApiMaster(); 
										  String payResponse =apiMaster.bankTransfer("", nextObj.toString(), walletMast.getAccountNumber(),
										  walletMast.getIfscCode(), ""+toSettledAmt, "OTHERS", date, "IMPS");
										  
										 
										  String status = null; String statusCode = null; String statusMessage = null;
										  
										  if (payResponse==null) {
										  System.out.println("RESPONSE  NULL");
										  } else {
										  
										  try
										  {
										  JSONObject json = new JSONObject(payResponse); 
										  status =json.getString("status"); 
										  statusCode = json.getString("statusCode");
										  statusMessage = json.getString("statusMessage");
										  
										  System.out.println(status+"    "+statusCode+"   "+statusMessage);
										  
										  transaction = session.beginTransaction();
										  
										  aepsBean.setOthers(statusCode+":-- "+statusMessage);
										  aepsBean.setPaymentMode("IMPS"); 
										  aepsBean.setRrn("");
										  //aepsBean.setPaymentStatus(status);
										  aepsBean.setPaymentStatus("Pending");
										  
										  aepsBean.setUtr("");
										  
										  session.saveOrUpdate(aepsBean); transaction.commit();
										  
										  }catch (Exception e) { e.printStackTrace(); }
										  
										  if (statusCode != null && "DE_001".equalsIgnoreCase(statusCode) &&
										  "SUCCESS".equalsIgnoreCase(status)) {
										  
										  } else if (statusCode != null && "DE_002".equalsIgnoreCase(statusCode) &&
										  "ACCEPTED".equalsIgnoreCase(status)) {
										  
										  } else if (statusCode != null && ("DE_101".equalsIgnoreCase(statusCode) ||
										  "DE_102".equalsIgnoreCase(statusCode)) && "PENDING".equalsIgnoreCase(status))
										  {
										  
										  } else if (statusCode != null && ("DE_010".equalsIgnoreCase(statusCode) ||
										  "DE_011".equalsIgnoreCase(statusCode) ||
										  "DE_012".equalsIgnoreCase(statusCode) ||
										  "DE_013".equalsIgnoreCase(statusCode) ||
										  "DE_014".equalsIgnoreCase(statusCode) ||
										  "DE_015".equalsIgnoreCase(statusCode) ||
										  "DE_016".equalsIgnoreCase(statusCode) ||
										  "DE_017".equalsIgnoreCase(statusCode) ||
										  "DE_018".equalsIgnoreCase(statusCode) ||
										  "DE_019".equalsIgnoreCase(statusCode) ||
										  "DE_021".equalsIgnoreCase(statusCode) ||
										  "DE_022".equalsIgnoreCase(statusCode) ||
										  "DE_023".equalsIgnoreCase(statusCode) ||
										  "DE_024".equalsIgnoreCase(statusCode) ||
										  "DE_025".equalsIgnoreCase(statusCode) ||
										  "DE_034".equalsIgnoreCase(statusCode) ||
										  "DE_039".equalsIgnoreCase(statusCode) ||
										  "DE_040".equalsIgnoreCase(statusCode) ||
										  "DE_041".equalsIgnoreCase(statusCode) ||
										  "DE_042".equalsIgnoreCase(statusCode) ||
										  "DE_050".equalsIgnoreCase(statusCode) ||
										  "DE_051".equalsIgnoreCase(statusCode) ||
										  "DE_052".equalsIgnoreCase(statusCode) ||
										  "DE_053".equalsIgnoreCase(statusCode) ||
										  "DE_054".equalsIgnoreCase(statusCode) ||
										  "DE_055".equalsIgnoreCase(statusCode) ||
										  "DE_056".equalsIgnoreCase(statusCode) ||
										  "DE_057".equalsIgnoreCase(statusCode) ||
										  "DE_602".equalsIgnoreCase(statusCode) ||
										  "DE_400".equalsIgnoreCase(statusCode) ||
										  "DE_401".equalsIgnoreCase(statusCode) ||
										  "DE_402".equalsIgnoreCase(statusCode) ||
										  "DE_403".equalsIgnoreCase(statusCode) ||
										  "DE_404".equalsIgnoreCase(statusCode) ||
										  "DE_405".equalsIgnoreCase(statusCode) ||
										  "DE_406".equalsIgnoreCase(statusCode) ||
										  "DE_407".equalsIgnoreCase(statusCode) ||
										  "DE_408".equalsIgnoreCase(statusCode) ||
										  "DE_409".equalsIgnoreCase(statusCode) ||
										  "DE_500".equalsIgnoreCase(statusCode) ||
										  "DE_606".equalsIgnoreCase(statusCode) ||
										  "DE_607".equalsIgnoreCase(statusCode) ||
										  "DE_603".equalsIgnoreCase(statusCode) ||
										  "DE_609".equalsIgnoreCase(statusCode) ||
										  "DE_612".equalsIgnoreCase(statusCode) ||
										  "DE_613".equalsIgnoreCase(statusCode) ||
										  "DE_614".equalsIgnoreCase(statusCode) ||
										  "DE_615".equalsIgnoreCase(statusCode) ||
										  "DE_616".equalsIgnoreCase(statusCode) ||
										  "DE_617".equalsIgnoreCase(statusCode) ||
										  "DE_618".equalsIgnoreCase(statusCode) ||
										  "DE_619".equalsIgnoreCase(statusCode) ||
										  "DE_620".equalsIgnoreCase(statusCode) ||
										  "DE_621".equalsIgnoreCase(statusCode) ||
										  "DE_622".equalsIgnoreCase(statusCode) ||
										  "DE_623".equalsIgnoreCase(statusCode) ||
										  "DE_625".equalsIgnoreCase(statusCode) ||
										  "DE_626".equalsIgnoreCase(statusCode) ||
										  "DE_627".equalsIgnoreCase(statusCode) ||
										  "DE_628".equalsIgnoreCase(statusCode) ||
										  "DE_629".equalsIgnoreCase(statusCode) ||
										  "DE_631".equalsIgnoreCase(statusCode) ||
										  "DE_632".equalsIgnoreCase(statusCode) ||
										  "DE_634".equalsIgnoreCase(statusCode) ||
										  "DE_636".equalsIgnoreCase(statusCode) ||
										  "DE_640".equalsIgnoreCase(statusCode) ||
										  "DE_641".equalsIgnoreCase(statusCode) ||
										  "DE_643".equalsIgnoreCase(statusCode) ||
										  "DE_646".equalsIgnoreCase(statusCode) ||
										  "DE_647".equalsIgnoreCase(statusCode) ||
										  "DE_648".equalsIgnoreCase(statusCode) ||
										  "DE_649".equalsIgnoreCase(statusCode) ||
										  "DE_650".equalsIgnoreCase(statusCode) ||
										  "DE_651".equalsIgnoreCase(statusCode) ||
										  "DE_652".equalsIgnoreCase(statusCode) ||
										  "DE_653".equalsIgnoreCase(statusCode) ||
										  "DE_656".equalsIgnoreCase(statusCode) ||
										  "DE_657".equalsIgnoreCase(statusCode) ||
										  "DE_658".equalsIgnoreCase(statusCode) ||
										  "DE_659".equalsIgnoreCase(statusCode) ||
										  "DE_660".equalsIgnoreCase(statusCode) ||
										  "DE_661".equalsIgnoreCase(statusCode) ||
										  "DE_662".equalsIgnoreCase(statusCode) ||
										  "DE_663".equalsIgnoreCase(statusCode) ||
										  "DE_664".equalsIgnoreCase(statusCode) ||
										  "DE_665".equalsIgnoreCase(statusCode) ||
										  "DE_666".equalsIgnoreCase(statusCode) ||
										  "DE_667".equalsIgnoreCase(statusCode) ||
										  "DE_668".equalsIgnoreCase(statusCode) ||
										  "DE_669".equalsIgnoreCase(statusCode) ||
										  "DE_670".equalsIgnoreCase(statusCode) ||
										  "DE_671".equalsIgnoreCase(statusCode) ||
										  "DE_672".equalsIgnoreCase(statusCode) ||
										  "DE_673".equalsIgnoreCase(statusCode) ||
										  "DE_674".equalsIgnoreCase(statusCode) ||
										  "DE_675".equalsIgnoreCase(statusCode) ||
										  "DE_676".equalsIgnoreCase(statusCode) ||
										  "DE_677".equalsIgnoreCase(statusCode) ||
										  "DE_678".equalsIgnoreCase(statusCode) ||
										  "DE_679".equalsIgnoreCase(statusCode) ||
										  "DE_680".equalsIgnoreCase(statusCode) ||
										  "DE_681".equalsIgnoreCase(statusCode) ||
										  "DE_682".equalsIgnoreCase(statusCode) ||
										  "DE_683".equalsIgnoreCase(statusCode) ||
										  "DE_701".equalsIgnoreCase(statusCode) ||
										  "DE_703".equalsIgnoreCase(statusCode) ||
										  "DE_704".equalsIgnoreCase(statusCode) ||
										  "DE_705".equalsIgnoreCase(statusCode))) {
										  
										  }else {
										  
										  }
										  
										  
										  } }catch (Exception e) {
											  e.printStackTrace();
											  }
										  
									/*	  
									if ("OAGG001050".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "Bhartipay Service PVT. LTD.");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"gagandeep@bhartipay.com,vikas.kumar@bhartipay.com",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);
										
									} else if ("OAGG001057".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "PROMONEY");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"sachin.verma@promoney.in,rajat.verma@promoney.in",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);

									} else if ("OAGG001058".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "ATSV E-SOLUTION INDIA PRIVATE INDIA LIMITED");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"vinayprabhakarsingh@gmail.com,Anandtiwari@workmail.com",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);

									}
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() started thread for sending mail temp for aepssettlement(Movetobank)  ");
                              */
								     
								}catch (Exception e) {
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() problem in replacing mailtemp or in sending mail temp for aepssettlement(Movetobank)  ");
									e.printStackTrace();
								}
								
							}
							else {
								transaction.rollback();
								aggObjMap.put("statusCode", "err00");
								aggObjMap.put("status", "ERROR");
					        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
							}
								
						}
						else {
							aggObjMap.put("statusCode", "err00");
							aggObjMap.put("status", "ERROR");
				        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
						}
					}
				}
			}
	        else {
	        	aggObjMap.put("statusCode", "err01");
	        	aggObjMap.put("status", "ERROR");
	        	aggObjMap.put("statusMsg", "Agent Id/Mobile No does not exist.");
	        }
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","problem in postAepsSettlement" + e.getMessage() + " " + e);
			
			aggObjMap.put("statusCode", "err00");
			aggObjMap.put("status", "ERROR");
        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
        	
		} finally {
			session.close();
		}
		
		return aggObjMap;
	}
	
	

	
	
	
	 
	
	@Override
	public Map<String, Object> isSaveAepsSettlementAgent(AEPSsettlement aeps) {
		
		Map<String, Object> aggObjMap = new HashMap<String, Object>();
		String mailTemp = "";
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		
		Gson gsonAeps = new Gson();
		String jsonText=gsonAeps.toJson(aeps);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "isSaveAepsSettlementAgent()", "", "Json String ", ""+jsonText);
		try {
			mailTemp =
					
					"<p>Settlement Team,</p>\r\n" + 
					"<p>There is request for AEPS/m-ATM settlement. Kindly do the needful. Below is request details for settlement.</p>\r\n" + 
					"<p>Agent id:-&nbsp; <<agentid>></p>\r\n" + 
					"<p>Agent Name:- <<agentname>></p>\r\n" + 
					"<p>Respective Txnid:- <<resptxnid>></p>\r\n" +
					"<p>Settlement Amount:- <<settlementamount>></p>\r\n" +
					"<p>Account No:- <<accountno>></p>\r\n" +
					"<p>IFSC:- <<ifsc>></p>\r\n" +
					"<p>Bank Name:- <<bankname>></p>\r\n" + 
							/* "<p>Bank Name:- <<bankname>></p>\r\n" + */
					"<p>IMPS/NEFT Team. Kindaly take a confirmation from acount team before processing.</p>\r\n" + 
					"<p>&nbsp;</p>\r\n" + 
					"<p><em><strong>Note</strong></em>:- This is auto generated mail. Please do not reply.</p>\r\n" + 
					"<p>Thanks,</p>\r\n" + 
					"<p><strong><<source>></strong></p>";
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlementAgent() Mail  "+mailTemp);
			
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlementAgent() "+"problem in mail templates");
			e.printStackTrace();
		}
		try {
			
			if(aeps.getCreditAmount()!=null && !aeps.getCreditAmount().equalsIgnoreCase(""))
				aeps.setCreditAmount(new DecimalFormat("0.00").format(Double.valueOf(aeps.getCreditAmount())));
			
			if(aeps.getDebitAmount()!=null && !aeps.getDebitAmount().equalsIgnoreCase(""))
				aeps.setDebitAmount(new DecimalFormat("0.00").format(Double.valueOf(aeps.getDebitAmount())));

			if(aeps.getServiceCharges()!=null && !aeps.getServiceCharges().equalsIgnoreCase(""))
				aeps.setServiceCharges(new DecimalFormat("0.00").format(Double.valueOf(aeps.getServiceCharges())));
			
			String walletid = null;
			String aggregatorID = null;
			WalletMastBean walletMast =  null;
			
			
			
			for(WalletMastBean walletBean : getWalletIdByAgentIdORMobile(aeps.getAgentid())) {
			
				walletid = walletBean.getWalletid();
				aggregatorID = walletBean.getAggreatorid();
				walletMast = walletBean;
			}
			
			String sqlquery="select accountNumber,ifscCode from addbankaccount where accountNumber='"+aeps.getAccountNumber()+"' ";
			Query query=session.createSQLQuery(sqlquery);
		    List agentList = query.list();
		    Object[] obj1=(Object[])agentList.get(0);
			walletMast.setAccountNumber(String.valueOf(obj1[0]));
			walletMast.setIfscCode(String.valueOf(obj1[1]));
			
			
	        if(walletid !=null && !walletid.equalsIgnoreCase("")) {
	        	
	        	Double aepsAmount = 0.0;
				Double canAmount = 0.0;
				Double finalBalance = 0.0;
				Double toSettledAmt= 0.0;
				Double serviceAmt= 0.0;
				
				
				
				toSettledAmt = Double.valueOf(aeps.getDebitAmount());
				
				serviceAmt = Double.valueOf(aeps.getServiceCharges());
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","Settlement Amt-  "+toSettledAmt , "Service Amt- "+serviceAmt, "", "");
				
				factory = DBUtil.getSessionFactory();
	    		session = factory.openSession();
	    		Transaction transaction = session.beginTransaction();
	    		//String sumCredit80_81Str = "select sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,81,88,89) and date(txn.txnDate)<=date(now()) group by txn.txncode";
			       
	    		String sumCredit80_81Str = "select sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode in (80,88,133) and date(txn.txnDate)<=date(now()) group by txn.txncode";
		        String sumDebit124Str = "select sum(txn.txndebit) as sumDebit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=124 and date(txn.txnDate)<=date(now())";
		        
		        //String sumFailedCredit80_81Str = "select sum(txn.txncredit) as sumCredit from TransactionBean txn where txn.walletid='"+walletid+"' and txn.txncode=133 and date(txn.txnDate)<=date(now()) ";
		        
		        
		        Query sumCredit80_81Qry = session.createQuery(sumCredit80_81Str);
		        List sumCredit80_81List = sumCredit80_81Qry.list();
		        
		        sumCredit80_81Qry = session.createQuery(sumDebit124Str);
		        Double preAmount = (Double) sumCredit80_81Qry.uniqueResult();
		         
		        //sumCredit80_81Qry = session.createQuery(sumFailedCredit80_81Str);
		        //Double failedAmount = (Double) sumCredit80_81Qry.uniqueResult();
		        
		        
				for(int i=0; i<sumCredit80_81List.size();i++) {
					 aepsAmount +=(Double)sumCredit80_81List.get(i);
				}
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","Settlement Amt-  "+toSettledAmt , " Service Amt- "+serviceAmt, " preAmount - "+preAmount, " ");
				
				if(aepsAmount>0.0) {
				 canAmount = aepsAmount - (preAmount != null ? preAmount : 0);
				 
				 //canAmount = aepsAmount - (preAmount != null ? (preAmount-failedAmount) : 0);
				}
				
				Double totalSettle =0.0; 
				
				if(toSettledAmt > serviceAmt && serviceAmt >= 0) {
					totalSettle = toSettledAmt + serviceAmt;
					
					try {
						String finalBalQuery = "select wbb.finalBalance from WalletBalanceBean wbb where wbb.walletid='"+walletid+"'";
						Query finalBalSql = session.createQuery(finalBalQuery);
						finalBalance =(Double)finalBalSql.uniqueResult();
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","  " , "finalBalance - "+finalBalance, "", "");
							
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "Settlement Total "+totalSettle,"   Settlement Amt-  "+toSettledAmt , "Service Amt- "+serviceAmt, " canAmount "+canAmount, "");
				
				if(totalSettle < 0 || totalSettle == 0){
					aggObjMap.put("statusCode", "err01");
					aggObjMap.put("status", "ERROR");
					aggObjMap.put("statusMsg", "You want to settle "+toSettledAmt+" + "+serviceAmt+", We can not proceed."+"   totalSettle  "+totalSettle);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","  " , "Settlement Amount - ", " Zero or -", "totalSettle "+totalSettle);
					
				}
				else if(totalSettle > finalBalance){
					aggObjMap.put("statusCode", "err01");
					aggObjMap.put("status", "ERROR");
					aggObjMap.put("statusMsg", "You want to settle "+totalSettle+" amount, But Agent's current balance is less, which is "+finalBalance);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","  " , "Settlement Amount greater than final balance ", " finalBalance "+finalBalance, "  totalSettle "+totalSettle);
					
				}
				else {
					if(toSettledAmt > canAmount){
						aggObjMap.put("statusCode", "err01");
						aggObjMap.put("status", "ERROR");
						aggObjMap.put("statusMsg", "You can not settle "+totalSettle+" amount, Because you entered more than "+canAmount+"   toSettledAmt "+toSettledAmt);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()","  " , " toSettledAmt Settlement Amount greater than canbeSettle ", " canAmount  "+canAmount, " toSettledAmt "+toSettledAmt);
						
					}
					else {
			    		
			    		SQLQuery nextQuery=null;
			    		
			        	String queryNext = "SELECT nextval(:ad,:aggregatorId)";
						nextQuery = session.createSQLQuery(queryNext);
						nextQuery.setParameter("aggregatorId", aggregatorID);
						nextQuery.setParameter("ad", "REVOKE");
						Object nextObj = nextQuery.uniqueResult();
						
						String queryStr = "call debitwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";
						String creditStr = "call creditwallet(:aggregatorid,:walletid,:amount,:narration,:resptxnid,:txncode)";
						
						SQLQuery debitWalletQuery=null;
						debitWalletQuery = session.createSQLQuery(queryStr);
						debitWalletQuery.setParameter("aggregatorid", aggregatorID);
						debitWalletQuery.setParameter("walletid", walletid);
						debitWalletQuery.setParameter("amount", new DecimalFormat("0.00").format(toSettledAmt));
						debitWalletQuery.setParameter("narration", "aepssettlements");
						debitWalletQuery.setParameter("resptxnid", ""+nextObj);
						debitWalletQuery.setParameter("txncode", 124);
		
						//int i = debitWalletQuery.executeUpdate();
						
						if(debitWalletQuery.executeUpdate()>0) {
							transaction = session.beginTransaction();
							if(serviceAmt>0) {
							SQLQuery debitWallet125Query=null;
							debitWallet125Query = session.createSQLQuery(queryStr);
							debitWallet125Query.setParameter("aggregatorid", aggregatorID);
							debitWallet125Query.setParameter("walletid", walletid);
							debitWallet125Query.setParameter("amount", new DecimalFormat("0.00").format(serviceAmt));
							debitWallet125Query.setParameter("narration", "aepssettlements");
							debitWallet125Query.setParameter("resptxnid", ""+nextObj);
							debitWallet125Query.setParameter("txncode", 125);
							debitWallet125Query.executeUpdate();							
							 }
							
//							transactionDao.creditMoney("D"+txnId, agentId, transactionDao.getWalletIdByUserId(wBean.getDistributerid(), aggregatorId), distsurcharge, aepsResponseData.getPayerId(),txnId,userIpimei,aggregatorId,userAgent,82);
							//int j = debitWallet125Query.executeUpdate(); 
							
							if(true){
								
								if(walletMast != null && "1".equalsIgnoreCase(walletMast.getWhiteLabel())) {
									try {
									WalletMastBean walletMastAggBean = (WalletMastBean)session.get(WalletMastBean.class, walletMast.getAggreatorid());
									WalletMastBean walletMastCashAggBean = (WalletMastBean)session.get(WalletMastBean.class, walletMastAggBean.getCashDepositeWallet());
									
									SQLQuery debitWallet157Query=null;
									debitWallet157Query = session.createSQLQuery(queryStr);
									debitWallet157Query.setParameter("aggregatorid", aggregatorID);
									debitWallet157Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
									debitWallet157Query.setParameter("amount", new DecimalFormat("0.00").format(toSettledAmt));
									debitWallet157Query.setParameter("narration", "aepssettlements");
									debitWallet157Query.setParameter("resptxnid", ""+nextObj);
									debitWallet157Query.setParameter("txncode", 157);
									if (debitWallet157Query.executeUpdate() > 0) {
										try {
											if(serviceAmt > 0) {
											SQLQuery creditWallet158Query = null;
											creditWallet158Query = session.createSQLQuery(creditStr);
											creditWallet158Query.setParameter("aggregatorid", aggregatorID);
											creditWallet158Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
											creditWallet158Query.setParameter("amount",
													new DecimalFormat("0.00").format(serviceAmt));
											creditWallet158Query.setParameter("narration", "aepssettlements charge");
											creditWallet158Query.setParameter("resptxnid", "" + nextObj);
											creditWallet158Query.setParameter("txncode", 158);
											creditWallet158Query.executeUpdate();
											}
//											transaction.commit();
//											transaction = session.beginTransaction();
											SQLQuery debitWallet159Query = null;
											debitWallet159Query = session.createSQLQuery(queryStr);
											debitWallet159Query.setParameter("aggregatorid", aggregatorID);
											debitWallet159Query.setParameter("walletid", walletMastCashAggBean.getWalletid());
											debitWallet159Query.setParameter("amount",7.08);
											debitWallet159Query.setParameter("narration", "aepssettlements charge");
											debitWallet159Query.setParameter("resptxnid", "" + nextObj);
											debitWallet159Query.setParameter("txncode", 159);
											debitWallet159Query.executeUpdate();
										} catch (Exception e) {
											e.printStackTrace();
										}
									}else {
										aggObjMap.put("statusCode", "err00");
										aggObjMap.put("status", "ERROR");
							        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
							        	transaction.rollback();
							        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()"," !!! can not proceed!!! " , "Settlement Amount not debit with 157 ", "  ", " ");
										
							        	return aggObjMap;
									}
									transaction.commit();
								}catch (Exception e) {
									aggObjMap.put("statusCode", "err00");
									aggObjMap.put("status", "ERROR");
						        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
						        	transaction.rollback();
						        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "isSaveAepsSettlementAgent()"," !!! can not proceed!!!  Catch Block " , "  Problem in Move to bank  ", "  ", " "+e.getMessage());
									
						        	return aggObjMap;
								}	
									
								}else {
									transaction.commit();
								}
								
								aggObjMap.put("statusCode", "Suc01");
								aggObjMap.put("status", "Success");
								aggObjMap.put("statusMsg", "Agent("+aeps.getAgentid()+")'s "+aeps.getDebitAmount()+" amount with "+aeps.getServiceCharges()+" service charge has been successfully Settled.");
								
								transaction = session.beginTransaction();
								AepsSettlementBean aepsBean = new AepsSettlementBean();
								aepsBean.setAccountNumber(walletMast.getAccountNumber());
								aepsBean.setAgentId(walletMast.getId());
								aepsBean.setAgentName(walletMast.getName());
								aepsBean.setAgentWalletId(walletMast.getWalletid());
								aepsBean.setAmount(toSettledAmt);
								aepsBean.setIfscCode(walletMast.getIfscCode());
								aepsBean.setRespTxnId(nextObj.toString());
								aepsBean.setStatus("PENDING");
								aepsBean.setSettlementDate(new Date());
								aepsBean.setAggregatorId(walletMast.getAggreatorid());
								aepsBean.setPaymentMode(aeps.getTxnMode());
								aepsBean.setCharges(serviceAmt);
								session.save(aepsBean);
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() commit start for aepssettlement(Movetobank)");
								transaction.commit();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() commit done for aepssettlement(Movetobank)");

								try {
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() start replacing in mail temp for aepssettlement(Movetobank)  "+mailTemp);
									mailTemp = mailTemp.replace("<<agentid>>", aeps.getAgentid())
											.replace("<<bankname>>", walletMast.getBankName()!=null?walletMast.getBankName():"")
											.replace("<<agentname>>", walletMast.getName()).replace("<<resptxnid>>", "" + nextObj)
											.replace("<<settlementamount>>", "" + toSettledAmt)
											.replace("<<accountno>>", walletMast.getAccountNumber()).replace("<<ifsc>>",walletMast.getIfscCode())
											;
//									                                                          String email,String emailMsg, String mailSub, String mobileNo, String smsMsg, String flag,String aggId,String walletid,String sendername,String type
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() done replacing in mail temp for aepssettlement(Movetobank)  "+mailTemp);
									
										
										
										  try {
										  
										  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
										  LocalDateTime now = LocalDateTime.now(); String date = dtf.format(now);
										  
										  PayoutApiMaster apiMaster = new PayoutApiMaster(); 
										  String payResponse =apiMaster.bankTransfer("", nextObj.toString(), walletMast.getAccountNumber(),
										  walletMast.getIfscCode(), ""+toSettledAmt, "OTHERS", date, aeps.getTxnMode()); 
										  
										 
										  String status = null; String statusCode = null; String statusMessage = null;
										  
										  if (payResponse==null) {
										  System.out.println("RESPONSE  NULL");
										  } else {
										  
										  try
										  {
										  JSONObject json = new JSONObject(payResponse); 
										  status =json.getString("status"); 
										  statusCode = json.getString("statusCode");
										  statusMessage = json.getString("statusMessage");
										  
										  System.out.println(status+"    "+statusCode+"   "+statusMessage);
										  
										  transaction = session.beginTransaction();
										  
										  aepsBean.setOthers(statusCode+":-- "+statusMessage);
										  aepsBean.setPaymentMode(aeps.getTxnMode());
										  aepsBean.setRrn("");
										  //aepsBean.setPaymentStatus(status);
										  aepsBean.setPaymentStatus("Pending");
										  aepsBean.setUtr("");
										  
										  session.saveOrUpdate(aepsBean); transaction.commit();
										  
										  }catch (Exception e) { e.printStackTrace(); }
										  
										  if (statusCode != null && "DE_001".equalsIgnoreCase(statusCode) &&
										  "SUCCESS".equalsIgnoreCase(status)) {
										  
										  } else if (statusCode != null && "DE_002".equalsIgnoreCase(statusCode) &&
										  "ACCEPTED".equalsIgnoreCase(status)) {
										  
										  } else if (statusCode != null && ("DE_101".equalsIgnoreCase(statusCode) ||
										  "DE_102".equalsIgnoreCase(statusCode)) && "PENDING".equalsIgnoreCase(status))
										  {
										  
										  } else if (statusCode != null && ("DE_010".equalsIgnoreCase(statusCode) ||
										  "DE_011".equalsIgnoreCase(statusCode) ||
										  "DE_012".equalsIgnoreCase(statusCode) ||
										  "DE_013".equalsIgnoreCase(statusCode) ||
										  "DE_014".equalsIgnoreCase(statusCode) ||
										  "DE_015".equalsIgnoreCase(statusCode) ||
										  "DE_016".equalsIgnoreCase(statusCode) ||
										  "DE_017".equalsIgnoreCase(statusCode) ||
										  "DE_018".equalsIgnoreCase(statusCode) ||
										  "DE_019".equalsIgnoreCase(statusCode) ||
										  "DE_021".equalsIgnoreCase(statusCode) ||
										  "DE_022".equalsIgnoreCase(statusCode) ||
										  "DE_023".equalsIgnoreCase(statusCode) ||
										  "DE_024".equalsIgnoreCase(statusCode) ||
										  "DE_025".equalsIgnoreCase(statusCode) ||
										  "DE_034".equalsIgnoreCase(statusCode) ||
										  "DE_039".equalsIgnoreCase(statusCode) ||
										  "DE_040".equalsIgnoreCase(statusCode) ||
										  "DE_041".equalsIgnoreCase(statusCode) ||
										  "DE_042".equalsIgnoreCase(statusCode) ||
										  "DE_050".equalsIgnoreCase(statusCode) ||
										  "DE_051".equalsIgnoreCase(statusCode) ||
										  "DE_052".equalsIgnoreCase(statusCode) ||
										  "DE_053".equalsIgnoreCase(statusCode) ||
										  "DE_054".equalsIgnoreCase(statusCode) ||
										  "DE_055".equalsIgnoreCase(statusCode) ||
										  "DE_056".equalsIgnoreCase(statusCode) ||
										  "DE_057".equalsIgnoreCase(statusCode) ||
										  "DE_602".equalsIgnoreCase(statusCode) ||
										  "DE_400".equalsIgnoreCase(statusCode) ||
										  "DE_401".equalsIgnoreCase(statusCode) ||
										  "DE_402".equalsIgnoreCase(statusCode) ||
										  "DE_403".equalsIgnoreCase(statusCode) ||
										  "DE_404".equalsIgnoreCase(statusCode) ||
										  "DE_405".equalsIgnoreCase(statusCode) ||
										  "DE_406".equalsIgnoreCase(statusCode) ||
										  "DE_407".equalsIgnoreCase(statusCode) ||
										  "DE_408".equalsIgnoreCase(statusCode) ||
										  "DE_409".equalsIgnoreCase(statusCode) ||
										  "DE_500".equalsIgnoreCase(statusCode) ||
										  "DE_606".equalsIgnoreCase(statusCode) ||
										  "DE_607".equalsIgnoreCase(statusCode) ||
										  "DE_603".equalsIgnoreCase(statusCode) ||
										  "DE_609".equalsIgnoreCase(statusCode) ||
										  "DE_612".equalsIgnoreCase(statusCode) ||
										  "DE_613".equalsIgnoreCase(statusCode) ||
										  "DE_614".equalsIgnoreCase(statusCode) ||
										  "DE_615".equalsIgnoreCase(statusCode) ||
										  "DE_616".equalsIgnoreCase(statusCode) ||
										  "DE_617".equalsIgnoreCase(statusCode) ||
										  "DE_618".equalsIgnoreCase(statusCode) ||
										  "DE_619".equalsIgnoreCase(statusCode) ||
										  "DE_620".equalsIgnoreCase(statusCode) ||
										  "DE_621".equalsIgnoreCase(statusCode) ||
										  "DE_622".equalsIgnoreCase(statusCode) ||
										  "DE_623".equalsIgnoreCase(statusCode) ||
										  "DE_625".equalsIgnoreCase(statusCode) ||
										  "DE_626".equalsIgnoreCase(statusCode) ||
										  "DE_627".equalsIgnoreCase(statusCode) ||
										  "DE_628".equalsIgnoreCase(statusCode) ||
										  "DE_629".equalsIgnoreCase(statusCode) ||
										  "DE_631".equalsIgnoreCase(statusCode) ||
										  "DE_632".equalsIgnoreCase(statusCode) ||
										  "DE_634".equalsIgnoreCase(statusCode) ||
										  "DE_636".equalsIgnoreCase(statusCode) ||
										  "DE_640".equalsIgnoreCase(statusCode) ||
										  "DE_641".equalsIgnoreCase(statusCode) ||
										  "DE_643".equalsIgnoreCase(statusCode) ||
										  "DE_646".equalsIgnoreCase(statusCode) ||
										  "DE_647".equalsIgnoreCase(statusCode) ||
										  "DE_648".equalsIgnoreCase(statusCode) ||
										  "DE_649".equalsIgnoreCase(statusCode) ||
										  "DE_650".equalsIgnoreCase(statusCode) ||
										  "DE_651".equalsIgnoreCase(statusCode) ||
										  "DE_652".equalsIgnoreCase(statusCode) ||
										  "DE_653".equalsIgnoreCase(statusCode) ||
										  "DE_656".equalsIgnoreCase(statusCode) ||
										  "DE_657".equalsIgnoreCase(statusCode) ||
										  "DE_658".equalsIgnoreCase(statusCode) ||
										  "DE_659".equalsIgnoreCase(statusCode) ||
										  "DE_660".equalsIgnoreCase(statusCode) ||
										  "DE_661".equalsIgnoreCase(statusCode) ||
										  "DE_662".equalsIgnoreCase(statusCode) ||
										  "DE_663".equalsIgnoreCase(statusCode) ||
										  "DE_664".equalsIgnoreCase(statusCode) ||
										  "DE_665".equalsIgnoreCase(statusCode) ||
										  "DE_666".equalsIgnoreCase(statusCode) ||
										  "DE_667".equalsIgnoreCase(statusCode) ||
										  "DE_668".equalsIgnoreCase(statusCode) ||
										  "DE_669".equalsIgnoreCase(statusCode) ||
										  "DE_670".equalsIgnoreCase(statusCode) ||
										  "DE_671".equalsIgnoreCase(statusCode) ||
										  "DE_672".equalsIgnoreCase(statusCode) ||
										  "DE_673".equalsIgnoreCase(statusCode) ||
										  "DE_674".equalsIgnoreCase(statusCode) ||
										  "DE_675".equalsIgnoreCase(statusCode) ||
										  "DE_676".equalsIgnoreCase(statusCode) ||
										  "DE_677".equalsIgnoreCase(statusCode) ||
										  "DE_678".equalsIgnoreCase(statusCode) ||
										  "DE_679".equalsIgnoreCase(statusCode) ||
										  "DE_680".equalsIgnoreCase(statusCode) ||
										  "DE_681".equalsIgnoreCase(statusCode) ||
										  "DE_682".equalsIgnoreCase(statusCode) ||
										  "DE_683".equalsIgnoreCase(statusCode) ||
										  "DE_701".equalsIgnoreCase(statusCode) ||
										  "DE_703".equalsIgnoreCase(statusCode) ||
										  "DE_704".equalsIgnoreCase(statusCode) ||
										  "DE_705".equalsIgnoreCase(statusCode))) {
										  
										  }else {
										  
										  }
										  
										  
										  } }catch (Exception e) {
											  e.printStackTrace();
											  }
								/*	
										  
								  if ("OAGG001050".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "Bhartipay Service PVT. LTD.");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"gagandeep@bhartipay.com,vikas.kumar@bhartipay.com",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);
										
									} else if ("OAGG001057".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "PROMONEY");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"sachin.verma@promoney.in,rajat.verma@promoney.in",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);

									} else if ("OAGG001058".equalsIgnoreCase(walletMast.getAggreatorid())) {
										mailTemp=mailTemp.replace("<<source>>", "ATSV E-SOLUTION INDIA PRIVATE INDIA LIMITED");
										SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility(
												"vinayprabhakarsingh@gmail.com,Anandtiwari@workmail.com",
												mailTemp,
												"AEPS/m-ATM settlement request for Agent " + walletMast.getId() + "("
														+ walletMast.getName() + ")",
												"", "", "Mail", walletMast.getAggreatorid(), walletMast.getWalletid(),
												walletMast.getId(), "NOTP");
										Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "",
												"",
												"isSaveAepsSettlement() starting thread for sending mail temp for aepssettlement(Movetobank)  ");
										ThreadUtil.getThreadPool().execute(smsAndMailUtility);

									}
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() started thread for sending mail temp for aepssettlement(Movetobank)  ");
     */
								     
								}catch (Exception e) {
									Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "isSaveAepsSettlement() problem in replacing mailtemp or in sending mail temp for aepssettlement(Movetobank)  ");
									e.printStackTrace();
								}
								
							}
							else {
								transaction.rollback();
								aggObjMap.put("statusCode", "err00");
								aggObjMap.put("status", "ERROR");
					        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
					        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "Problem with debit service charge with  125", "", "","problem in postAepsSettlementAgent");
										        	
							}
								
						}
						else {
							aggObjMap.put("statusCode", "err00");
							aggObjMap.put("status", "ERROR");
				        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
				        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "Problem with debit   124", "", "","problem in postAepsSettlementAgent");
							
						}
					}
				}
			}
	        else {
	        	aggObjMap.put("statusCode", "err01");
	        	aggObjMap.put("status", "ERROR");
	        	aggObjMap.put("statusMsg", "Agent Id/Mobile No does not exist.");
	        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "Problem with wallet id null", "", "","problem in postAepsSettlementAgent");
				
	        }
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","problem in postAepsSettlement" + e.getMessage() + " " + e);
			
			aggObjMap.put("statusCode", "err00");
			aggObjMap.put("status", "ERROR");
        	aggObjMap.put("statusMsg", "!!! can not proceed!!!");
        	
		} finally {
			session.close();
		}
		
		return aggObjMap;
	}
	

	
	
	
	
	// change by mani
	
	public List<LeanAccount> leanedAccountReport() {

	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
	
		List<LeanAccount> results = new ArrayList<LeanAccount>();
		try {

			//StringBuilder serchQuery = new StringBuilder();
			//serchQuery.append("from LeanAccount order by id DESC");
            //Query query = session.createQuery(serchQuery.toString());
			Query query = session.createQuery("from LeanAccount");
			results = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","leanedAccountReport()","checking leanedAccountReport "+results.size());
			 
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","leanedAccountReport()","checking leanedAccountReport error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return results;
	}
	

	public List<LeanAccount> leanReport(String aggId) {

	    factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
	
		List<LeanAccount> results = new ArrayList<LeanAccount>();
		try {
             if(aggId.equalsIgnoreCase(""))
             {
            	Query query = session.createQuery("from LeanAccount");
     			results = query.list();
     			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","leanReport()","checking leanReport "+results.size());
                  
             }else {
			Query query = session.createQuery("from LeanAccount where aggregatorId=:aggId");
			query.setString("aggId", aggId);
			results = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","leanReport()","checking leanReport "+results.size());
             }
		} catch (Exception e) {
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","leanReport()","checking leanReport error"+ e.getMessage()+" "+e);
			  logger.debug("*****************error************** " + e.getMessage(), e);
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		
		return results;
	}
	
	
	public Map<String, String> agentAccountList(String userId) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "agentAccountList()" + "|userId " + userId);
	    factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			SQLQuery query = null;
			query = session.createSQLQuery("SELECT accountNumber,bankName FROM addbankaccount WHERE  userId=:userId and status=true ");
			//query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and distributerid=:distId ");
			query.setString("userId", userId);
			List rowss = query.list();
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				//System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString()+" - "+row[1].toString());
			}
        } catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in userId" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);
    }

	@Override
	public String validateAepsMerchant(AepsTokenRequest req) {
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "validateAepsMerchant()");
	    factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		String status="1001";
		try{
			SQLQuery query = null;
			query = session.createSQLQuery(
					"SELECT * FROM aepsclientconfig WHERE   agentauthid=:authId and agentauthpassword=:pass ");
			query.setString("authId", req.getAgentAuthId());
			query.setString("pass", req.getAgentAuthPassword());
			List<Object[]> rows = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "Row "+rows.size());
			   
			if(rows.size()>0)
			{
				//WalletUserDao walletUserDao=new WalletUserDaoImpl();
				//OnboardStatusResponse st = walletUserDao.onboardStatus(req.getRetailerId());
				
				status="1000";	
			}else
			{
				status="1001";
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			status="7000";

		}finally {
			session.close();
		}
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "status "+status);
		
		return status;
	}

	
	public Map<String, String> getSuperDist(String id) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getSuperDist()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, id);
			if(mast!=null) 
			{	SQLQuery query = null;
				 
					query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=7 and userstatus='A' and aggreatorid=:aggId and salesId=:soId ");
					query.setString("soId", mast.getId());
					query.setString("aggId", mast.getAggreatorid());
					List<Object[]> rows = query.list();
					for (Object[] row : rows) {
						System.out.println(row[0].toString());
						agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
					}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	
	public Map<String, String> getSoDist(String id) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getSuperDist()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, id);
			if(mast!=null) 
			{	SQLQuery query = null;
				 
					query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=3 and userstatus='A' and aggreatorid=:aggId and salesId=:soId ");
					query.setString("soId", mast.getId());
					query.setString("aggId", mast.getAggreatorid());
					List<Object[]> rows = query.list();
					for (Object[] row : rows) {
						System.out.println(row[0].toString());
						agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
					}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	public Map<String, String> getDistributorUnderSd(String id) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "","getDistributorUnderSd()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		try {
			WalletMastBean mast = (WalletMastBean)session.get(WalletMastBean.class, id);
			if(mast!=null) 
			{	SQLQuery query = null;
				 
					query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=3 and userstatus='A' and aggreatorid=:aggId and superdistributerid=:sdId ");
					query.setString("sdId", mast.getId());
					query.setString("aggId", mast.getAggreatorid());
					List<Object[]> rows = query.list();
					for (Object[] row : rows) {
						System.out.println(row[0].toString());
						agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
					}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getActiveAgentByDistId" + e.getMessage() + " " + e);
			 } finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}

	
	public DistributorIdAllot getDistributorAllotid(String aggreatorId, String distributerId ) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", "getDistributorAllotid()" + "|distributerId " + distributerId);
		 
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		DistributorIdAllot distAllot =new DistributorIdAllot();

		try {
           distAllot = (DistributorIdAllot)session.get(DistributorIdAllot.class, distributerId);
             
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorId, "", "", "", "", ""
					+ "*****" + distributerId + "problem in getDistributorAllotid" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return distAllot;
	}


	public DistributorIdAllot updateTokenId(WalletMastBean mast ) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "updateTokenId()" );
		 
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		
		DistributorIdAllot superDistAllot =new DistributorIdAllot();
		int assignId = Integer.parseInt(mast.getUtrNo());
		int idCharge = Integer.parseInt(mast.getMobileno());
		Double txnamount= new Double(assignId*idCharge);
	    try {
           superDistAllot = (DistributorIdAllot)session.get(DistributorIdAllot.class, mast.getSuperdistributerid());
           if(superDistAllot.getRemainingToken()>=assignId)
           {
        	   Transaction transaction = session.beginTransaction();
        	   superDistAllot.setRemainingToken(superDistAllot.getRemainingToken()-assignId);
        	   superDistAllot.setConsumeToken(superDistAllot.getConsumeToken()+assignId);
        	   session.saveOrUpdate(superDistAllot);
        	   transaction.commit();
        	
        	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "assignId "+assignId, "superDistAllot" ); 
        	   
        	   DistributorIdAllot distAllot = (DistributorIdAllot)session.get(DistributorIdAllot.class, mast.getDistributerid());
        	   {

               String res = creditDebitIdCharge(mast.getDistributerid(), mast.getSuperdistributerid(),txnamount);
               Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "assignId "+assignId, "    creditDebitIdCharge  Response  "+res ); 
              	   
        	   Transaction tx = session.beginTransaction();
    		   distAllot.setAllToken(distAllot.getAllToken()+assignId);
    		   distAllot.setRemainingToken(distAllot.getRemainingToken()+assignId);
    		  // distAllot.setConsumeToken(distAllot.getConsumeToken()+assignId);
        	   session.saveOrUpdate(superDistAllot);
        	   tx.commit();	
        	   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "assignId "+assignId, "DistributorIdAllot" ); 
         	  
        	   
        	   if(res.equalsIgnoreCase("SUCCESS")) {
        	     superDistAllot.setAggregatorId("SUCCESS");
        	     superDistAllot.setUserId(distAllot.getUserId());
        	   }
        	   else if(res.equalsIgnoreCase("FAIL")) {
        		  superDistAllot.setAggregatorId("FAIL");
        	      superDistAllot.setUserId(distAllot.getUserId());
        	   }
        	   else {
        		  superDistAllot.setAggregatorId("Please contact to admin");
        		  superDistAllot.setUserId(distAllot.getUserId());
        	   }
        	    
              }
           } 
         
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "" 	+ "*****" + "" + "problem in updateAssignId" + e.getMessage() + " " + e);
		} finally {
			session.close();
		}
		return superDistAllot;
	}

	
	public String creditDebitIdCharge(String distId,String superDistId, double txnamount)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "distId "+distId, "", "creditDebitIdCharge ", "  Response  "+superDistId ); 
     	  
		factory = DBUtil.getSessionFactory();
		Session sessions = factory.openSession();
		
        String res="FAIL";
		try {
        WalletMastBean supDisWallet = (WalletMastBean) sessions.get(WalletMastBean.class, superDistId); 
        WalletMastBean disWallet = (WalletMastBean) sessions.get(WalletMastBean.class, distId);   
     	if(supDisWallet!=null  && disWallet!=null) {
        String disWalletId = disWallet.getWalletid();
        WalletBalanceBean distbal = (WalletBalanceBean)sessions.get(WalletBalanceBean.class, disWalletId);
	        if(distbal.getFinalBalance()>=txnamount)
	        {
	        Transaction transaction = sessions.beginTransaction();	
	        String queryNext = "SELECT nextval(:ad,:aggregatorid)";
			SQLQuery nextQuery = session.createSQLQuery(queryNext);
			   nextQuery.setParameter("aggregatorid", disWallet.getAggreatorid());
			   nextQuery.setParameter("ad", "REVOKE");
	        Object nextObj = nextQuery.uniqueResult();
	        String txnId=""+nextObj;
	        
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "creditDebitIdCharge ", "  txnId  "+txnId ); 
	     	 
	        String str = "CALL debitwallet('" + disWallet.getAggreatorid() + "','" + disWallet.getWalletid() + "'," + txnamount + ",'Allot ID Charge','"+ txnId + "',186)";
	        System.out.println(" str   "+str);
	        Query query = sessions.createSQLQuery(str);
	    	query.executeUpdate();
	    	 
	    	transaction.commit();
	    	
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", " debitwallet ", "", "creditDebitIdCharge ", "  Query  "+str ); 
	     	
	    	
	        Transaction tx = sessions.beginTransaction();
	    	String str1 = "CALL creditwallet('" + supDisWallet.getAggreatorid() + "','" + supDisWallet.getWalletid() + "'," + txnamount + ",'Allot ID Charge','"+ txnId + "',187)";
	  	   System.out.println(" str1   "+str1);
	    	Query query1 = sessions.createSQLQuery(str1);
	  	    query1.executeUpdate();	
	  	  
	  	    tx.commit();
	  	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", " creditwallet ", "", "creditDebitIdCharge ", "  Query  "+str1 ); 
	     	 
	  	    res= "SUCCESS";
	  	    
	        }else
	        	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", " ", "Problem in distributor wallet balance ", "", " ", " distbal.getFinalBalance() "+distbal.getFinalBalance());
			
	        	return res;
	        	
     	}else
     	{
     		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", " ", "Problem in creditDebitIdCharge ", "", " supDisWallet &  DisWallet can not be blank", "");
			
     		return res;
     	}
  	    
		}catch(Exception e)
		{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", " ", "Problem in creditDebitIdCharge ", "", "", "" + e.getMessage() + " " + e);
			 
		}finally {
			sessions.close();	
		}
		
	 return res;
	}
	
	
	public Map<String, String> getAgentBySuper(String id,String aggId,int type) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "getAgentBySuper()" + "type " + type);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		Map<String, String> agentMap = new HashMap<String, String>();
		 
		try {
			SQLQuery query = null;
			if(type==3) 
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and distributerid=:distId ");
			else if(type==7) 
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and superdistributerid=:distId ");
			else if(type==8) 
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and salesid=:distId ");
			else if(type==9) 
			query = session.createSQLQuery("SELECT id,NAME FROM walletmast WHERE  usertype=2 and managerid=:distId ");
			 
			query.setString("distId", id);
			
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				System.out.println(row[0].toString());
				agentMap.put(row[0].toString(), row[0].toString() + "-" + row[1].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), "", "", "", "", "", "problem in getAgentBySuper" + e.getMessage() + " " + e);
			} finally {
			session.close();
		}
		return new CommanUtilDaoImpl().sortByValue(agentMap);

	}
	
	
	
	
	public List<AEPSLedger> getAepsReportSuper(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "getAepsReport()" + "Start Date " + walletMastBean.getStDate()
						+ "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<AEPSLedger> results = new ArrayList<AEPSLedger>();
		try {
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			Criteria criteria = session.createCriteria(AEPSLedger.class);

			if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
					|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
				criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

			} else {
				Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
				criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
            }
		 
			 if (walletMastBean.getUsertype() == 3 && walletMastBean.getId() != null && walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where distributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
			  }else if (walletMastBean.getUsertype() == 7 && walletMastBean.getId() != null && walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where superdistributerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
			 } else if (walletMastBean.getUsertype() == 8 && walletMastBean.getId() != null && walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where salesid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
			 } else if (walletMastBean.getUsertype() == 9 && walletMastBean.getId() != null && walletMastBean.getId().equalsIgnoreCase("-1")) {
					Query query = session.createSQLQuery(
							"select id from walletmast where managerid=:distId and usertype=:userType");
					query.setParameter("distId", walletMastBean.getDistributerid());
					query.setParameter("userType", 2);
					List agentList = query.list();
					criteria.add(Restrictions.in("agentId", agentList));
			 
			 } else {
					criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(), walletMastBean.getWalletid(), "", "", "", "query ");
 
				results = criteria.list();
 
		} catch (Exception e) {
            e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(), walletMastBean.getWalletid(), "", "", "", "problem in DmtDetailsMastBean" + e.getMessage() + e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(), walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		return results;
	}

	@Override
	public List<WalletMastBean> getUserEnable() {
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		List<WalletMastBean> list=new ArrayList<>();
		try {
			Query query = session.createQuery("from WalletMastBean where usertype in(2,3,7,8,9) ");
			List<WalletMastBean> str=query.list();
			if(str.size()>0)
			{
				for(WalletMastBean ll:str)
				{
					WalletMastBean  mast=new WalletMastBean();
					mast.setId(ll.getId());
					mast.setSalesId(ll.getSalesId());
					mast.setManagerId(ll.getManagerId());
					mast.setBank3(ll.getBank3());
					mast.setOnlineMoney(ll.getOnlineMoney());
					list.add(mast);
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			session.close();
		}
		

		return list;
	}
	
	
	@Override
	public List<CmsBean> getCmsReportForAgent(WalletMastBean walletMastBean) {

		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getId(),
				walletMastBean.getAggreatorid(), "", "", "", "getCmsReportForAgent()" + "Start Date "
						+ walletMastBean.getStDate() + "End Date" + walletMastBean.getEndDate());

		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<CmsBean> results = new ArrayList<CmsBean>();
		try {
			if (!(walletMastBean.getId() == "")) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy");

				Calendar cal = Calendar.getInstance();

				Criteria criteria = session.createCriteria(CmsBean.class);

				if (walletMastBean.getStDate() == null || walletMastBean.getStDate().isEmpty()
						|| walletMastBean.getEndDate() == null || walletMastBean.getEndDate().isEmpty()) {
					criteria.add(Restrictions.ge("txnDate", formate.parse(formate.format(new Date()))));

				} else {
					Date endDate = new Date(formatter.parse(walletMastBean.getEndDate()).getTime() + 86400000);
					criteria.add(Restrictions.between("txnDate", formatter.parse(walletMastBean.getStDate()), endDate));
				}

				criteria.add(Restrictions.eq("agentId", walletMastBean.getId()));
		       
				System.out.println(criteria.toString());
				results = criteria.list();

			}

		} catch (Exception e) {
            e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
					walletMastBean.getWalletid(), "", "", "", "problem in getCmsReportForAgent" + e.getMessage() + e);
		 } finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), walletMastBean.getAggreatorid(),
				walletMastBean.getWalletid(), "", "", "", "results size" + results.size());
		return results;
    }
	
}
