package com.bhartipay.report.bean;

import java.util.List;

import com.bhartipay.commission.bean.CommPercMaster;
import com.bhartipay.transaction.bean.PaymentGatewayTxnBean;
import com.bhartipay.transaction.bean.RechargeTxnBean;
import com.bhartipay.transaction.bean.WalletToBankTxnMast;

public class ReconciliationReport {
	
	private ReconciliationSummaryBean reconciliationSummaryBean;
	private List <ReconciliationDetailsBean> prePiadDetailsList;
	private List <RechargeTxnBean> rechargeTxnList;
	private List <WalletToBankTxnMast> walletToBankTxnList;
	private List <PaymentGatewayTxnBean> paymentGatewayTxnList;
	
	
	public ReconciliationSummaryBean getReconciliationSummaryBean() {
		return reconciliationSummaryBean;
	}
	public void setReconciliationSummaryBean(ReconciliationSummaryBean reconciliationSummaryBean) {
		this.reconciliationSummaryBean = reconciliationSummaryBean;
	}
	public List<ReconciliationDetailsBean> getPrePiadDetailsList() {
		return prePiadDetailsList;
	}
	public void setPrePiadDetailsList(List<ReconciliationDetailsBean> prePiadDetailsList) {
		this.prePiadDetailsList = prePiadDetailsList;
	}
	public List<RechargeTxnBean> getRechargeTxnList() {
		return rechargeTxnList;
	}
	
	public void setRechargeTxnList(List<RechargeTxnBean> rechargeTxnList) {
		this.rechargeTxnList = rechargeTxnList;
	}
	public List<WalletToBankTxnMast> getWalletToBankTxnList() {
		return walletToBankTxnList;
	}
	public void setWalletToBankTxnList(List<WalletToBankTxnMast> walletToBankTxnList) {
		this.walletToBankTxnList = walletToBankTxnList;
	}
	public List<PaymentGatewayTxnBean> getPaymentGatewayTxnList() {
		return paymentGatewayTxnList;
	}
	public void setPaymentGatewayTxnList(List<PaymentGatewayTxnBean> paymentGatewayTxnList) {
		this.paymentGatewayTxnList = paymentGatewayTxnList;
	}
	
	
	
	
	


}
