package com.bhartipay.report.bean;

import java.io.Serializable;

public class AgentCurrentSummary implements Serializable{
	
	private String distributerId;
	private String agentId;
	private String name;
	private String type;
	private double txnCredit;
	private double txnDebit;
	private double finalBalance;
	
	
	
	
	public String getDistributerId() {
		return distributerId;
	}
	public void setDistributerId(String distributerId) {
		this.distributerId = distributerId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getTxnCredit() {
		return txnCredit;
	}
	public void setTxnCredit(double txnCredit) {
		this.txnCredit = txnCredit;
	}
	public double getTxnDebit() {
		return txnDebit;
	}
	public void setTxnDebit(double txnDebit) {
		this.txnDebit = txnDebit;
	}
	public double getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(double finalBalance) {
		this.finalBalance = finalBalance;
	}
	
	
	
	
	

}
