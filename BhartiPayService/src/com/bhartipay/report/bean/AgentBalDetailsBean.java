package com.bhartipay.report.bean;

import java.sql.Timestamp;
import java.util.StringTokenizer;

public class AgentBalDetailsBean {
	
	private String id;
	private String name;
	private double credit;
	private double debit;
	private double finalBalance;
	private String aeps;
	private String matm;
	private String bbps;
	private String details;
	private String walletid;
	private String cashdeposit;
	private String DmtAmt;
	private String commission;
	private String recharge;
	private int usertype;
	private String approveDate;

	public String getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Timestamp approveDate) {
		this.approveDate = ""+approveDate;
	}
	
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getCashdeposit() {
		return cashdeposit;
	}
	public String getDmtAmt() {
		return DmtAmt;
	}
	public String getCommission() {
		return commission;
	}
	public String getRecharge() {
		return recharge;
	}
	public void setCashdeposit(String cashdeposit) {
		this.cashdeposit = cashdeposit;
	}
	public void setDmtAmt(String dmtAmt) {
		DmtAmt = dmtAmt;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public void setRecharge(String recharge) {
		this.recharge = recharge;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
		if(details!=null){
			StringTokenizer tokeniser=new StringTokenizer(details,"|");
			if(tokeniser.hasMoreTokens())
				setCashdeposit(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setDmtAmt(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setCommission(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setRecharge(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setAeps(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setMatm(tokeniser.nextToken());
			if(tokeniser.hasMoreTokens())
				setBbps(tokeniser.nextToken());
		}
		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public double getDebit() {
		return debit;
	}
	public void setDebit(double debit) {
		this.debit = debit;
	}
	public double getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(double finalBalance) {
		this.finalBalance = finalBalance;
	}
	public String getAeps() {
		return aeps;
	}
	public void setAeps(String aeps) {
		this.aeps = aeps;
	}
	public String getMatm() {
		return matm;
	}
	public void setMatm(String matm) {
		this.matm = matm;
	}
	public String getBbps() {
		return bbps;
	}
	public void setBbps(String bbps) {
		this.bbps = bbps;
	}
	
	
	

}
