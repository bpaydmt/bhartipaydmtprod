package com.bhartipay.report.bean;

import java.util.Date;
import java.util.List;

import com.bhartipay.transaction.bean.RechargeTxnBean;

public class TxnDetailBean 
{
	private String type;
	
	private String transId;
	
	private String txnDate;
	
	private String status;
	
	private double amount;
	
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	private List<TravelDetailsBean> travelDetails;
	
	private RechargeTxnBean rechargeTxn;

	public RechargeTxnBean getRechargeTxn() {
		return rechargeTxn;
	}

	public void setRechargeTxn(RechargeTxnBean rechargeTxn) {
		this.rechargeTxn = rechargeTxn;
	}

	public List<TravelDetailsBean> getTravelDetails() {
		return travelDetails;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTravelDetails(List<TravelDetailsBean> travelDetails) {
		this.travelDetails = travelDetails;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}
