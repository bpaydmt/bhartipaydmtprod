package com.bhartipay.report.bean;

import java.io.Serializable;

public class RefundTransactionBean implements Serializable{
	
	private String  id;
	private String  refundid;
	private String  status;
	private String  txndate;
	private String  remark;
	private double  amount;
	private double servicecharge;
	private String agentorsenderid;
	private String refundtosenderoragent;
	
	
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	
	public String getRefundid() {
		return refundid;
	}
	public void setRefundid(String refundid) {
		this.refundid = refundid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getServicecharge() {
		return servicecharge;
	}
	public void setServicecharge(double servicecharge) {
		this.servicecharge = servicecharge;
	}
	public String getAgentorsenderid() {
		return agentorsenderid;
	}
	public void setAgentorsenderid(String agentorsenderid) {
		this.agentorsenderid = agentorsenderid;
	}
	public String getRefundtosenderoragent() {
		return refundtosenderoragent;
	}
	public void setRefundtosenderoragent(String refundtosenderoragent) {
		this.refundtosenderoragent = refundtosenderoragent;
	}
	
	
	

}
