package com.bhartipay.report.bean;

import java.io.Serializable;

public class AgentConsolidatedReportBean implements Serializable{
	
	private  String userId;
	private String stDate;
	private String endDate;
	
	private String txnDate;
	private String walletid;
	private String agentId;
	private String name;
	private String type;
	private double txnCredit;
	private double txnDebit;
	
	
	
	
		
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getTxnCredit() {
		return txnCredit;
	}
	public void setTxnCredit(double txnCredit) {
		this.txnCredit = txnCredit;
	}
	public double getTxnDebit() {
		return txnDebit;
	}
	public void setTxnDebit(double txnDebit) {
		this.txnDebit = txnDebit;
	}
	
	
	
	 

}
