package com.bhartipay.report.bean;

public class UserKycListBean {
	
	private  String refid; 
	private  String userid; 
	private  String name;
	private  String addprofkycid; 
	private  String addprofdesc; 
	private  byte[] addprofkycpic;
	private  String idprofkycid; 
	private  String idprofdesc; 
	private  byte[] idprofkycpic;
	private  String STATUS; 
	private  String loaddate;
	private int usertype;
	
	
	


	
	
	
	
	public byte[] getAddprofkycpic() {
		return addprofkycpic;
	}
	public void setAddprofkycpic(byte[] addprofkycpic) {
		this.addprofkycpic = addprofkycpic;
	}
	public byte[] getIdprofkycpic() {
		return idprofkycpic;
	}
	public void setIdprofkycpic(byte[] idprofkycpic) {
		this.idprofkycpic = idprofkycpic;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getRefid() {
		return refid;
	}
	public void setRefid(String refid) {
		this.refid = refid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

	
	
	public String getAddprofkycid() {
		return addprofkycid;
	}
	public void setAddprofkycid(String addprofkycid) {
		this.addprofkycid = addprofkycid;
	}
	public String getAddprofdesc() {
		return addprofdesc;
	}
	public void setAddprofdesc(String addprofdesc) {
		this.addprofdesc = addprofdesc;
	}

	public String getIdprofkycid() {
		return idprofkycid;
	}
	public void setIdprofkycid(String idprofkycid) {
		this.idprofkycid = idprofkycid;
	}
	public String getIdprofdesc() {
		return idprofdesc;
	}
	public void setIdprofdesc(String idprofdesc) {
		this.idprofdesc = idprofdesc;
	}

	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getLoaddate() {
		return loaddate;
	}
	public void setLoaddate(String loaddate) {
		this.loaddate = loaddate;
	}
	
	
	
	
	
	
	

}
