package com.bhartipay.report.bean;

import java.io.Serializable;

public class PartnerLedgerBean implements Serializable{
	
	private int	id;
	private String partnername;
	private String txndate;
	private double moneyload;
	private double moneyconsume;
	private double balances;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPartnername() {
		return partnername;
	}
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	public double getMoneyload() {
		return moneyload;
	}
	public void setMoneyload(double moneyload) {
		this.moneyload = moneyload;
	}
	public double getMoneyconsume() {
		return moneyconsume;
	}
	public void setMoneyconsume(double moneyconsume) {
		this.moneyconsume = moneyconsume;
	}
	public double getBalances() {
		return balances;
	}
	public void setBalances(double balances) {
		this.balances = balances;
	}
	
	
	
	
	

}
