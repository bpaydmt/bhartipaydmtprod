package com.bhartipay.report.bean;

public class TravelDetailsBean 
{
	private String flightCode;
	
	private String flightName;
	
	private String AirlineCode;
	
	private String source;
	
	private String destination;
	
	private String status;
	
	private double flightAmt;
	
	private String arrDate;
	
	private String deptDate;
	
	private String pnr;

	
	public String getPnr() {
		return pnr;
	}


	public void setPnr(String pnr) {
		this.pnr = pnr;
	}


	public String getFlightCode() {
		return flightCode;
	}

	
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}

	public String getFlightName() {
		return flightName;
	}

	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}

	public String getAirlineCode() {
		return AirlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		AirlineCode = airlineCode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public double getFlightAmt() {
		return flightAmt;
	}

	public void setFlightAmt(double flightAmt) {
		this.flightAmt = flightAmt;
	}



	public String getArrDate() {
		return arrDate;
	}


	public void setArrDate(String arrDate) {
		this.arrDate = arrDate;
	}


	public String getDeptDate() {
		return deptDate;
	}

	public void setDeptDate(String deptDate) {
		this.deptDate = deptDate;
	}
	
	
}
