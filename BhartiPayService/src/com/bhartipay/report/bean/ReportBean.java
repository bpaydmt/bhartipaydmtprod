package com.bhartipay.report.bean;

import javax.persistence.Column;

public class ReportBean {
	
	
	private String userId;

	private String aggId;
	private String createdBy;
	private String distId;
	private String suDistId;
	private String agentId;
	private String subagentId;
	private String planId;
	private String statusCode;
	private String txnType;
	private String aggreatorid;
	private String refid;
	
	private String [] partnerName;
	
	private String endDate;
	private String stDate;
	private int userType;
	
	




	public String getSuDistId() {
		return suDistId;
	}

	public void setSuDistId(String suDistId) {
		this.suDistId = suDistId;
	}

	public String[] getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String[] partnerName) {
		this.partnerName = partnerName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getRefid() {
		return refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getSubagentId() {
		return subagentId;
	}

	public void setSubagentId(String subagentId) {
		this.subagentId = subagentId;
	}

	public String getAggId() {
		return aggId;
	}

	public void setAggId(String aggId) {
		this.aggId = aggId;
	}

	public String getDistId() {
		return distId;
	}

	public void setDistId(String distId) {
		this.distId = distId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
	
	
	
	

}
