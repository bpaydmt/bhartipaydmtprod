package com.bhartipay.report.bean;

import java.io.Serializable;
import java.math.BigInteger;

public class RevenueReportBean implements Serializable{
	
	private String txndate;
	private BigInteger txncount;
	private String agentid;
	private double txnamount;
	private double aepstxnamount;
	private double matmtxnamount;
	private double servicecharge;
	private String aggreatorid;
	private String managername;
	private String type;
	
	
	
	
	public String getManagername() {
		return managername;
	}
	public void setManagername(String managername) {
		this.managername = managername;
	}
	public double getAepstxnamount() {
		return aepstxnamount;
	}
	public void setAepstxnamount(double aepstxnamount) {
		this.aepstxnamount = aepstxnamount;
	}
	public double getMatmtxnamount() {
		return matmtxnamount;
	}
	public void setMatmtxnamount(double matmtxnamount) {
		this.matmtxnamount = matmtxnamount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}

	
	public BigInteger getTxncount() {
		return txncount;
	}
	public void setTxncount(BigInteger txncount) {
		this.txncount = txncount;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public double getTxnamount() {
		return txnamount;
	}
	public void setTxnamount(double txnamount) {
		this.txnamount = txnamount;
	}
	public double getServicecharge() {
		return servicecharge;
	}
	public void setServicecharge(double servicecharge) {
		this.servicecharge = servicecharge;
	}
	
	
	
	
	
	
	
	

}
