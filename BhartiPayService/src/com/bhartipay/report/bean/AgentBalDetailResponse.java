package com.bhartipay.report.bean;

import java.util.List;

public class AgentBalDetailResponse 
{
	private List<AgentBalDetailsBean> balList ;
	
	private String distributorTotal;
	
	public List<AgentBalDetailsBean> getBalList() {
		return balList;
	}
	public void setBalList(List<AgentBalDetailsBean> balList) {
		this.balList = balList;
	}
	public String getDistributorTotal() {
		return distributorTotal;
	}
	public void setDistributorTotal(String distributorTotal) {
		this.distributorTotal = distributorTotal;
	} 
}
