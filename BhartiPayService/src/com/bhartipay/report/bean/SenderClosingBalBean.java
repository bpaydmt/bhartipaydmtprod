package com.bhartipay.report.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SenderClosingBalBean implements Serializable{
	

private String id;
private String  aggreatorid;
private String mobileno;
private String firstname;
private String auditdate;
private BigDecimal walletbalance;
private BigDecimal transferlimit;
private String kycstatus;
private String ispanvalidate;





public String getIspanvalidate() {
	return ispanvalidate;
}
public void setIspanvalidate(String ispanvalidate) {
	this.ispanvalidate = ispanvalidate;
}
public String getKycstatus() {
	return kycstatus;
}
public void setKycstatus(String kycstatus) {
	this.kycstatus = kycstatus;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getAggreatorid() {
	return aggreatorid;
}
public void setAggreatorid(String aggreatorid) {
	this.aggreatorid = aggreatorid;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getAuditdate() {
	return auditdate;
}
public void setAuditdate(String auditdate) {
	this.auditdate = auditdate;
}
public BigDecimal getWalletbalance() {
	return walletbalance;
}
public void setWalletbalance(BigDecimal walletbalance) {
	this.walletbalance = walletbalance;
}
public BigDecimal getTransferlimit() {
	return transferlimit;
}
public void setTransferlimit(BigDecimal transferlimit) {
	this.transferlimit = transferlimit;
}







}
