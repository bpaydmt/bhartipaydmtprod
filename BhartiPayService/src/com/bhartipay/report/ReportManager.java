package com.bhartipay.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.config.WalletUtilService;
import com.bhartipay.lean.bean.AddBankAccount;
import com.bhartipay.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.report.bean.AgentCurrentSummary;
import com.bhartipay.report.bean.EscrowBean;
import com.bhartipay.report.bean.ReconciliationReport;
import com.bhartipay.report.bean.ReportBean;
import com.bhartipay.report.bean.RequestBean;
import com.bhartipay.report.bean.TravelTxn;
import com.bhartipay.report.bean.TxnDetailBean;
import com.bhartipay.report.bean.WalletHistoryBean;
import com.bhartipay.report.persistence.ReportDao;
import com.bhartipay.report.persistence.ReportDaoImpl;
import com.bhartipay.transaction.bean.Response;
import com.bhartipay.transaction.bean.TransactionResponse;
import com.bhartipay.transaction.bean.TxnInputBean;
import com.bhartipay.user.bean.WalletBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.BPJWTSignUtil;
import com.bhartipay.util.bean.SMSSendDetails;
import com.bhartipay.util.bean.SupportTicketBean;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.wallet.aeps.AEPSAggregatorSettlement;
import com.bhartipay.wallet.aeps.AEPSsettlement;
import com.bhartipay.wallet.aepsApi.bean.AepsTokenRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.jsonwebtoken.Claims;


@Path("/ReportManager")
public class ReportManager {
	
	private static final Logger logger = Logger.getLogger(WalletUtilService.class.getName());
	ReportDao reportDao=new ReportDaoImpl();
	CommanUtilDao commanUtilDao=new CommanUtilDaoImpl();
	
	ObjectMapper oMapper = new ObjectMapper();
	Gson gson = new Gson();
	BPJWTSignUtil oxyJWTSignUtil=new BPJWTSignUtil();
	
	
	@POST
	@Path("/wallettoBankReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String wallettoBankReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"wallettoBankReport()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.wallettoBankReport(reportBean.getUserId()));
	}

	@POST
	@Path("/rechargeReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String rechargeReport(ReportBean reportBean){
			      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"rechargeReport()|"
				+ reportBean.getAgentId());
		//logger.info("Start execution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.rechargeReport(reportBean));
	}
	
	@POST
	@Path("/bbpsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String bbpsReport(TxnInputBean inputBean){
			      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),inputBean.getAggreatorid(),"",inputBean.getUserId(), "", "", 
			"rechargeReport()|");
		//logger.info("Start execution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.bbpsReport(inputBean));
	}
	
	@POST
	@Path("/walletHistroyReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String walletHistroyReport(WalletHistoryBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"walletHistroyReport()|"
				+ reportBean.getAgentId());
		//logger.info("Start execution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.walletHistroyReport(reportBean));
	}
	
	
	@POST
	@Path("/rechargeReportAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String rechargeReportAggreator(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"rechargeReportAggreator()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.rechargeReportAggreator(reportBean));
	}
	
	
	
	@POST
	@Path("/bbpsReportAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String bbpsReportAggreator(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"bbpsReportAggreator()"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.bbpsReportAggreator(reportBean));
	}

	
	@POST
	@Path("/bbpsReportDistAggregator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String bbpsReportDistAggregator(WalletMastBean walletMastBean){
		
	      
	//	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
	//			"bbpsReportAggreator()"
	//			+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.bbpsReportDistAggregator(walletMastBean));
	}
	
	
	@POST
	@Path("/rechargeReportDist")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String rechargeReportDist(WalletMastBean walletMastBean){
		
	      
	//	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
	//			"bbpsReportAggreator()"
	//			+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.rechargeReportDist(walletMastBean));
	}
	
	
	@POST
	@Path("/pgReportAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String pgReportAggreator(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"pgReportAggreator()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.pgReportAggreator(reportBean));
	}
	
	@POST
	@Path("/rechargeReportApp")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String rechargeReportApp(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"rechargeReportApp()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCountry()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.rechargeReportApp(reportBean.getUserId()));
	}
	
	@POST
	@Path("/getCustomerDetail")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustomerDetail(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getCustomerDetail()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getCustomerDetail()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustomerDetail(reportBean.getAggId()));
	}
	
	
	
	
	
	
	@POST
	@Path("/getAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAggreator(){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getAggreator()");
		//logger.info("Start excution ===================== method getAggreator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAggreator());
	}
	
	
	@POST
	@Path("/getAggreatorList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAggreatorList(){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getAggreator()");
		//logger.info("Start excution ===================== method getAggreator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAggreatorList());
	}
	
	@POST
	@Path("/getWLAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getWLAggreator(){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getAggreator()");
		//logger.info("Start excution ===================== method getAggreator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getWLAggreator());
	}
	
	
	@POST
	@Path("/getSettledAggreator")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSettledAggreator(){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getSettledAggreator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getSettledAggreator());
	}
	
	
	
	@POST
	@Path("/getAepsSettlementCharge")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsSettlementCharge(String agentId){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getSettledAggreator()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsSettlementCharge(agentId));
	    
	}
	
	
	
	@POST
	@Path("/getAggreatorSettlement")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String fetchAggreatorSettlement(String aggregatorid){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","fetchAggreatorSettlement()");
	    JSONObject jObject = null;
	    
	    try{
		      JSONObject totalSettlementAmountJsonObject = new JSONObject(reportDao.getTotalSettlementAmount(aggregatorid));
		      JSONObject totalAepsTransactionAmountJsonObject = new JSONObject(reportDao.getTotalAepsTransactionAmount(aggregatorid));
		      
		      jObject = new JSONObject();
		      jObject.put("total",totalSettlementAmountJsonObject);
		      jObject.put("aeps", totalAepsTransactionAmountJsonObject); 
	      }catch(Exception eIn){
	      }
	    return jObject.toString();
	}
	
	@POST
	@Path("/saveAggreatorSettlement")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String postAggreatorSettlement(String aggSettlement){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","postAggreatorSettlement()");
	    JSONObject jObject = null;
	    
	    try{
			Gson gson = new Gson();
			AEPSAggregatorSettlement aepsAggregatorSettlementDeails = gson.fromJson(aggSettlement, AEPSAggregatorSettlement.class);
			
			JSONObject aggreatorSettlementJsonObject = new JSONObject(reportDao.postAggreatorSettlement(aepsAggregatorSettlementDeails));
			jObject = new JSONObject();
			jObject.put("status",aggreatorSettlementJsonObject);
	      }catch(Exception eIn){
	      }
	    return jObject.toString();
	}
	
	@POST
	@Path("/saveAepsSettlement")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String postAepsSettlement(String aeAgentId){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","postAepsSettlement()");
	    JSONObject jsonObject = null;
	    
	    try{jsonObject = new JSONObject(reportDao.postAepsSettlement(aeAgentId));}catch(Exception eIn){}
	    return jsonObject.toString();
	}
	
	@POST
	@Path("/saveAepsSettlementData")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String postAepsSettlementData(AddBankAccount account){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","postAepsSettlement()");
	    JSONObject jsonObject = null;
	    
	    try{
	    	Map<String, Object> rp = reportDao.postAepsSettlementData(account.getUserId(),account.getAccountNumber());
	    	
	    	jsonObject = new JSONObject(
	    		reportDao.postAepsSettlementData(account.getUserId(),account.getAccountNumber())
	    	  );
	    	}
	     catch(Exception eIn){}
	    
	    return jsonObject.toString();
	}
	
	
	
	@POST
	@Path("/getSettlementDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSettlementDetails(String jsonString){
	      
		Gson g = new Gson();
		AEPSsettlement aeps = g.fromJson(jsonString, AEPSsettlement.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","getSettlementDetails()");
	    JSONObject jsonObject = null;
	    
	    try{jsonObject = new JSONObject(reportDao.postAepsSettlement(aeps.getAgentid()));}catch(Exception eIn){}
	    return jsonObject.toString();
	}
	
	
	@POST
	@Path("/saveAmountAepsSettlementAgent")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String postAmountAepsSettlementAgent(String settlementRequest){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","postAmountAepsSettlement()");
	    JSONObject jsonObject = null;
	    
	    Gson g = new Gson();
	    AEPSsettlement aeps = g.fromJson(settlementRequest, AEPSsettlement.class);
		
	    if(aeps != null) {
	    	try{jsonObject = new JSONObject(reportDao.isSaveAepsSettlementAgent(aeps));}catch(Exception eIn){eIn.printStackTrace();}
	    }
	    return jsonObject.toString();
	}
	
	
	@POST
	@Path("/saveAmountAepsSettlement")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String postAmountAepsSettlement(String settlementRequest){
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","","","","postAmountAepsSettlement()");
	    JSONObject jsonObject = null;
	    
	    Gson g = new Gson();
	    AEPSsettlement aeps = g.fromJson(settlementRequest, AEPSsettlement.class);
		
	    if(aeps != null) {
	    	try{jsonObject = new JSONObject(reportDao.isSaveAepsSettlement(aeps));}catch(Exception eIn){eIn.printStackTrace();}
	    }
	    return jsonObject.toString();
	}
	
	@POST
	@Path("/getDistributerByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistributerByAggId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getDistributerByAggId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getDistributerByAggId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getDistributerByAggId(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSuperDistributerByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSuperDistributerByAggId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getDistributerByAggId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getDistributerByAggId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSuperDistributerByAggId(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getDistributerBySuDistId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistributerBySuDistId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getDistributerBySuDistId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getDistributerByAggId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getDistributerBySuDistId(reportBean.getSuDistId()));
	}
	
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
	@Path("/aggPlan")
	public String aggPlan(String aggId){
		
	  logger.info("******************************Onboard Status Check***********************************");
	  WalletUserDao walletUserDao=new WalletUserDaoImpl();
	  GsonBuilder builder = new GsonBuilder();
	  Gson gson = builder.create();
	  return gson.toJson(walletUserDao.aggPlan(aggId));
	 
	}
	
	
	
	@POST
	@Path("/getSuperActiveDistributerByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSuperActiveDistributerByAggId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getSuperActiveDistributerByAggId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getDistributerByAggId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getActiveDistributerByAggId(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getActiveDistributerByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getActiveDistributerByAggId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getActiveDistributerByAggId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getDistributerByAggId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getActiveDistributerByAggId(reportBean.getAggId()));
	}
	
	

	
	
	@POST
	@Path("/getActiveAgentByDistId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getActiveAgentByDistId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getActiveAgentByDistId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getActiveAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getActiveAgentByDistId(reportBean.getDistId()));
	}
	
	
	@POST
	@Path("/getManagerId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getManagerId(ReportBean reportBean){
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "getDistId()|" + reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getManagerId(reportBean.getDistId(),reportBean.getAggId()));
	}
	
	
	@POST
	@Path("/getDistId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistId(ReportBean reportBean){
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "getDistId()|" + reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getDistId(reportBean.getDistId(),reportBean.getAggId()));
	}
	
	
	@POST
	@Path("/getDistIdd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistIdd(ReportBean reportBean){
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"","", "", "", "getDistId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getDistId(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSdId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSdId(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"",reportBean.getUserId(), "", "", "getSdId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSdId(reportBean.getDistId(),reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSdIdd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSdIdd(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"",reportBean.getUserId(), "", "", "getSdId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSdId(reportBean.getAggId()));
	}
	
	
	@POST
	@Path("/getSoId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSoId(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "getSoId()|"+ reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSoId(reportBean.getDistId(),reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSoIdd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSoIdd(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"","", "", "", "getSoId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSoId(reportBean.getAggId()));
	}
	
	
	

	@POST
	@Path("/getActiveAgentByAgg")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getActiveAgentByAgg(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getActiveAgentByDistId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getActiveAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getActiveAgentByAgg(reportBean.getAggId()));
	}
	
	
	
	@POST
	@Path("/getAgentByDistId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentByDistId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getAgentByDistId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAgentByDistId(reportBean.getDistId()));
	}
	
	
	@POST
	@Path("/getAllUser")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAllUser(String aggId){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", 
				"getAgentByDistId()|"+ "");
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAllUser(aggId));
	}
	
	
	
	@POST
	@Path("/getAgentBySupDistId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentBySupDistId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getAgentBySupDistId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAgentBySupDistId(reportBean.getSuDistId()));
	}
	
	
	
	@POST
	@Path("/getAgentByAggId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentByAggId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getAgentByAggId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAgentByAggId(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSubAgentByAgentId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSubAgentByAgentId(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getSubAgentByAgentId()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method getSubAgentByAgentId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSubAgentByAgentId(reportBean.getAgentId()));
	}
	
	@POST
	@Path("/searchUser")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String searchUser(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"searchUser()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution ===================== method searchUser(reportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.searchUser(reportBean.getAggId(),reportBean.getDistId(),reportBean.getAgentId(),reportBean.getSubagentId()));
	}
	
	@POST
	@Path("/getKycList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getKycList(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getKycList()|"
				+ reportBean.getAgentId());
		//logger.info("Start excution =========getKycList============ method searchUser(reportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getKycList(reportBean.getAggId(),reportBean.getDistId(),reportBean.getAgentId(),reportBean.getSubagentId()));
	}
	
	
	@POST
	@Path("/getSenderProfile")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSenderProfile(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"getSenderProfile()|"
				+ reportBean.getAgentId());
	//	logger.info("Start excution ****************************************************** method searchUser(reportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getSenderProfile(reportBean.getUserId()));
		
	}
	
	@POST
	@Path("/getSMSRepot")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSMSRepot(SMSSendDetails sMSSendDetails){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),sMSSendDetails.getAggreatorid(),sMSSendDetails.getWalletid(),"", "", "", 
				"getSMSRepot()");
		//logger.info("Start excution ****************************************************** method getSMSRepot(sMSSendDetails)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getSMSRepot(sMSSendDetails));
	}
	
	@POST
	@Path("/saveUpdateSupportTicket")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public SupportTicketBean saveUpdateSupportTicket(@HeaderParam("IPIMEI") String imei,@HeaderParam("AGENT") String agent,SupportTicketBean supportTicketBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),supportTicketBean.getAggreatorId(),"",supportTicketBean.getUserId(), "", "", 
			"saveUpdateSupportTicket()|"+agent	);
		logger.info("Start excution ****************************************************** method saveUpdateSupportTicket(supportTicketBean)");
		 return commanUtilDao.saveUpdateSupportTicket(supportTicketBean, imei, agent);
	 }
	
	@POST
	@Path("/getSupportTicket")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getSupportTicket(SupportTicketBean supportTicketBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),supportTicketBean.getAggreatorId(),"",supportTicketBean.getUserId(), "", "","getSupportTicket()" 
				);
		//logger.info("Start excution ****************************************************** method getSupportTicket(supportTicketBean)"+supportTicketBean.getAggreatorId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getSupportTicket(supportTicketBean.getAggreatorId()));
	}
	
	
	
	
	@POST
	@Path("/getSupportTicketList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getSupportTicketList(SupportTicketBean supportTicketBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),supportTicketBean.getAggreatorId(),"",supportTicketBean.getUserId(), "", "","getSupportTicketList()" 
				);
		//logger.info("Start excution ****************************************************** method getSupportTicket(supportTicketBean)"+supportTicketBean.getAggreatorId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(commanUtilDao.getSupportTicketList(supportTicketBean.getAggreatorId(),supportTicketBean.getUserId(),supportTicketBean.getStDate(),supportTicketBean.getEndDate()));
	}
	
	
	@POST
	@Path("/updateSupportTicket")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public SupportTicketBean updateSupportTicket(SupportTicketBean supportTicketBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),supportTicketBean.getAggreatorId(),"",supportTicketBean.getUserId(), "", "", 
			"updateSupportTicket()|"+supportTicketBean.getId());
		//logger.info("Start excution ****************************************************** method updateSupportTicket(supportTicketBean)"+supportTicketBean.getId());
		 return commanUtilDao.updateSupportTicket(supportTicketBean.getId(),supportTicketBean.getRemarks());
	 }
	
	
	@POST
	@Path("/getTxnReportByAdmin")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getTxnReportByAdmin(){
		
		System.out.println("-------- Inside reportManager.java---- method -----getTxnReportByAdmin()-----");
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getTxnReportByAdmin()" 
			);
		//logger.info("Start excution ****************************************************** method getTxnReportByAdmin()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getTxnReportByAdmin());
		
	}
	
	@POST
	@Path("/acceptKyc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String acceptKyc(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"acceptKyc()|"+reportBean.getRefid());
		//logger.info("Start excution ****************************************************** method acceptKyc()"+reportBean.getRefid());
		return ""+reportDao.acceptKyc(reportBean.getRefid());
	}
	
	@POST
	@Path("/rejecttKyc")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String rejecttKyc(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"rejecttKyc()|"+reportBean.getRefid());
		//logger.info("Start excution ****************************************************** method rejecttKyc()"+reportBean.getRefid());
		return ""+reportDao.rejecttKyc(reportBean.getRefid());
		
	}
	
	@POST
	@Path("/getEscrowAccBal")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getEscrowAccBal( EscrowBean escrowBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),escrowBean.getAggreatorid(),"","", "", "","getEscrowAccBal()" 
			);
		//logger.info("Start excution ****************************************************** method getEscrowAccBal()"+escrowBean.getAggreatorid());
		Double d = reportDao.getEscrowAccBal(escrowBean.getAggreatorid());
		
		if(d == null){
			return "0.00";
		}
		return Double.toString(reportDao.getEscrowAccBal(escrowBean.getAggreatorid()));
		//return Double.toString(reportDao.getEscrowAccBal(escrowBean.getAggreatorid()));
	}
	
	
	@POST
	@Path("/getEscrowTxnList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getEscrowTxnList(EscrowBean escrowBean){
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),escrowBean.getAggreatorid(),"","", "", "","getEscrowTxnList()" 
			);
		//logger.info("Start excution ****************************************************** method getEscrowTxnList(escrowBean)"+escrowBean.getAggreatorid());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getEscrowTxnList(escrowBean));
		
	}
	
	
	
	
	@POST
	@Path("/getPrePiadCardReConReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ReconciliationReport getPrePiadCardReConReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","getPrePiadCardReConReport()" +"Sart Date :"+reportBean.getStDate()+"End Date :"+reportBean.getEndDate()
			);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Sart Date :"+reportBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","End Date :"+reportBean.getEndDate()
				);*/
		//logger.info("Start excution ===================== method getPrePiadCardReConReport(reportBean)"+reportBean.getAggreatorid());
		//logger.info("Start excution ===============ST====== method getPrePiadCardReConReport(reportBean)"+reportBean.getStDate());
		//logger.info("Start excution ================END===== method getPrePiadCardReConReport(reportBean)"+reportBean.getEndDate());
		//GsonBuilder builder = new GsonBuilder();
		//Gson gson = builder.create();
		//return gson.toJson(reportDao.getPrePiadCardReConReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate()));
		return reportDao.getPrePiadCardReConReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate());
	}
	
	@POST
	@Path("/getRechargeReConReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ReconciliationReport getRechargeReConReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","getRechargeReConReport" +"Sart Date :"+reportBean.getStDate()+"End Date :"+reportBean.getEndDate()
			);
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Sart Date :"+reportBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","End Date :"+reportBean.getEndDate()
				);*/
		/*logger.info("Start excution ===================== method getRechargeReConReport(reportBean)"+reportBean.getAggreatorid());
		logger.info("Start excution ===============ST====== method getRechargeReConReport(reportBean)"+reportBean.getStDate());
		logger.info("Start excution ================END===== method getRechargeReConReport(reportBean)"+reportBean.getEndDate());*/
		return reportDao.getRechargeReConReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate());
	}
	
	
	
	@POST
	@Path("/getDMTReConReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ReconciliationReport getDMTReConReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","getDMTReConReport()" +"Sart Date :"+reportBean.getStDate()+"End Date :"+reportBean.getEndDate()
			);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Sart Date :"+reportBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","End Date :"+reportBean.getEndDate()
				);*/
		/*logger.info("Start excution ===================== method getDMTReConReport(reportBean)"+reportBean.getAggreatorid());
		logger.info("Start excution ===============ST====== method getDMTReConReport(reportBean)"+reportBean.getStDate());
		logger.info("Start excution ================END===== method getDMTReConReport(reportBean)"+reportBean.getEndDate());*/
		return reportDao.getDMTReConReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate());
	}
	
	@POST
	@Path("/getPGReConReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ReconciliationReport getPGReConReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","getPGReConReport()" +"Sart Date :"+reportBean.getStDate()+"End Date :"+reportBean.getEndDate()
			);
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Sart Date :"+reportBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","End Date :"+reportBean.getEndDate()
				);*/
		/*logger.info("Start excution ===================== method getPGReConReport(reportBean)"+reportBean.getAggreatorid());
		logger.info("Start excution ===============ST====== method getPGReConReport(reportBean)"+reportBean.getStDate());
		logger.info("Start excution ================END===== method getPGReConReport(reportBean)"+reportBean.getEndDate());*/
		return reportDao.getPGReConReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate());
	}

	@POST
	@Path("/getCommSummaryReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCommSummaryReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","getCommSummaryReport" +"Sart Date :"+reportBean.getStDate()+"End Date :"+reportBean.getEndDate()
			);
	/*	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Sart Date :"+reportBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","End Date :"+reportBean.getEndDate()
				);*/
		/*logger.info("Start excution ===================== method getPGReConReport(reportBean)"+reportBean.getAggreatorid());
		logger.info("Start excution ===============ST====== method getPGReConReport(reportBean)"+reportBean.getStDate());
		logger.info("Start excution ================END===== method getPGReConReport(reportBean)"+reportBean.getEndDate());*/
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCommSummaryReport(reportBean.getAggreatorid(),reportBean.getStDate(),reportBean.getEndDate()));
	}
	
	
	
	@POST
	@Path("/getRevenueReportList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getRevenueReportList(WalletMastBean walletMastBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getRevenueReportList()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getRevenueReportList(walletMastBean));
	}
	
	@POST
	@Path("/getCommissionSummary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCommissionSummary(WalletMastBean walletMastBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getCommissionSummary()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getRevenueReportList(walletMastBean));
	}
	
	
	
	@POST
	@Path("/getDmtDetailsReportList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDmtDetailsReportList(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getDmtDetailsReportList()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getDmtDetailsReportList(walletMastBean));
		
	}
	
	
	
	@POST
	@Path("/getAepsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsReport(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReport()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsReport(walletMastBean));
		
	}
	
	
	@POST
	@Path("/getAepsPayoutReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsPayoutReport(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReport()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsPayoutReport(walletMastBean));
		
	}
	
	
	@POST
	@Path("/getMatmReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getMatmReport(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getMatmReport()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getMatmReport(walletMastBean));
		
	}
	
	@POST
	@Path("/getAepsReportForAgent")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsReportForAgent(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReportForAgent()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsReportForAgent(walletMastBean));
		
	}
	
	
	@POST
	@Path("/getAepsPayoutReportForAgent")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsPayoutReportForAgent(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReportForAgent()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsPayoutReportForAgent(walletMastBean));
		
	}
	
	@POST
	@Path("/getMatmReportForAgent")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getMatmReportForAgent(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReportForAgent()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getMatmReportForAgent(walletMastBean));
		
	}
	
	@POST
	@Path("/GetSupDistDmtDetailsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String GetSupDistDmtDetailsReport(WalletMastBean walletMastBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","GetSupDistDmtDetailsReport()" 
			);
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.GetSupDistDmtDetailsReport(walletMastBean));
		
	}
	
	
	@POST
	@Path("/getAgentBalDetail")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentBalDetail(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getAgentBalDetail()|"+reportBean.getAgentId());
		
		//logger.info("Start excution =========getAgentBalDetail============ method getAgentBalDetail(reportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAgentBalDetail(reportBean.getAggId()));
	}
	
	
	
	@POST
	@Path("/getAgentBalDetailBySuperDistributor")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentBalDetailBySuperDistributor(WalletMastBean walletBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletBean.getAggreatorid(),walletBean.getWalletid(),"", "", "", 
			"getAgentBalDetailBySuperDistributor()|"+walletBean.getAgentid());
		
		//logger.info("Start excution =========getAgentBalDetail============ method getAgentBalDetail(reportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAgentBalDetailBySuperDistributor(walletBean));
	}
	
	
	@POST
	@Path("/gatAgentCurrentSummary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentCurrentSummary(AgentCurrentSummary agentCurrentSummary){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",
			"getAgentCurrentSummary()|"+agentCurrentSummary.getAgentId());
		//logger.info("Start excution =========gatAgentCurrentSummary============ method gatAgentCurrentSummary(AgentCurrentSummary)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAgentCurrentSummary(agentCurrentSummary.getDistributerId()));
		
	}
	
	
	@POST
	@Path("/getAgentDister")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentDister(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getAgentDister()|"+reportBean.getAgentId());
		//logger.info("Start excution ===================== method getAgentDister(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAgentDister(reportBean.getAggId()));
	}
	
	
	
	@POST
	@Path("/getAgentConsolidatedReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentConsolidatedReport(AgentConsolidatedReportBean agentConsolidatedReportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",agentConsolidatedReportBean.getWalletid(),agentConsolidatedReportBean.getUserId(), "", "", 
			"getAgentConsolidatedReport()|"+agentConsolidatedReportBean.getAgentId());
		//logger.info("Start excution =========getAgentConsolidatedReport============ method getAgentConsolidatedReport(agentConsolidatedReportBean)");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAgentConsolidatedReport(agentConsolidatedReportBean.getUserId(),agentConsolidatedReportBean.getStDate(),agentConsolidatedReportBean.getEndDate()));
		
	}
	
	
	@POST
	@Path("/getSenderClosingBalReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSenderClosingBalReport(ReportBean reportBean){
		
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getSenderClosingBalReport()|"+reportBean.getAgentId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"|"+reportBean.getStDate());
		//logger.info("Start excution ===================== method getSenderClosingBalReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getSenderClosingBalReport(reportBean.getStDate()));
		
	}
	
	
	
	
	@POST
	@Path("/getRefundTransactionReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getRefundTransactionReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getRefundTransactionReport()|"+reportBean.getAgentId()+"|"+reportBean.getStDate());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
				"|"+reportBean.getStDate());*/
		//logger.info("Start excution ===================== method getRefundTransactionReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getRefundTransactionReport(reportBean.getStDate()));
		
	}
	
	
	  @POST
	  @Path("/getTxnDetails")
	  @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	  @Consumes(MediaType.APPLICATION_JSON)
	  public Response getTxnDetails(RequestBean requestBean ,@Context HttpServletRequest request)
	  
	
	  {  
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),"","", "", "","getTxnDetails()" 
			);
	  // logger.info("***********Start Execution *******getTxnDetails*******");
	   TransactionResponse response=new TransactionResponse(); 
	   
	   List<TravelTxn> travelTxn = new ArrayList<TravelTxn>();
	   List<TxnDetailBean> travelTxnDetlList = new ArrayList<TxnDetailBean>();
	   
	   Response resp = new Response();
	   
	   if(requestBean.getAggreatorId()==null ||requestBean.getAggreatorId().isEmpty()){
	    response.setStatus("FAILED");
	    response.setStatusCode("1");
	    response.setStatusMsg("Invalid Aggreator ID."); 
	    resp.setResponse(gson.toJson(response));
	   return resp;
	  }
	   try
	   {
	   String mKey="e391ab57590132714ad32da9acf3013eb88c";
	   Claims claim=oxyJWTSignUtil.parseToken(requestBean.getRequest(),mKey);
	  JSONObject claimJson = new JSONObject(claim);
	 
	   ReportBean reportBean = gson.fromJson(claimJson.toString(), ReportBean.class);
	   travelTxn = reportDao.getTravelTxn(reportBean);
	   travelTxnDetlList = new ReportService().getTxnReport( travelTxn);
	   
	   response.setTxnDetlList(travelTxnDetlList);
	   response.setStatus("SUCCESS");
	   response.setStatusCode("300");
	   response.setStatusMsg("SUCCESS");
	   resp.setResponse(oxyJWTSignUtil.generateToken(oMapper.convertValue(response, HashMap.class), mKey));
	   return resp;
	   
	   }
	   catch(Exception ex)
	   {
		  
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),requestBean.getAggreatorId(),"","", "", "",
			"problem in **********getTxnDetails"+ex.getMessage() +" "+ex);
	    logger.info("************problem in **********getTxnDetails*********");
	    ex.printStackTrace();
	    response.setStatus("FAILED");
	    response.setStatusCode("1");
	    response.setStatusMsg("FAILED");
	    resp.setResponse(gson.toJson(response));
	    return resp;
	   }
	   
	  }
	
	
	@POST
	@Path("/getCustomerDmtDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustomerDmtDetails(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getCustomerDmtDetails()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustomerDmtDetails(reportBean));
		
	}
	
	
	@POST
	@Path("/getCustAepsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustAepsReport(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getCustAepsReport()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustAepsReport(reportBean));
		
	}
	@POST
	@Path("/getCustBBPSReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustBBPSReport(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getCustBBPSReport()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustBBPSReport(reportBean));
		
	}
	@POST
	@Path("/getCustMATMReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustMATMReport(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getCustMATMReport()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustMATMReport(reportBean));
		
	}
	
	@POST
	@Path("/getCustDmtDetailsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getCustDmtDetailsReport(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getCustDmtDetailsReport()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getCustDmtDetailsReport(reportBean));
		
	}




/*	@POST


	@Path("/getTxnDtls")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getTxnDtls(String userId)
	  {  
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "","getTxnDtls()" 
);
	   //logger.info("***********Start Execution *******getTxnDetails*******");
	   GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getTravelTxn(userId));
	
	  }*/
	

	
	@POST
	 @Path("/getTxnDtls")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	  public String getTxnDtls(ReportBean reportBean)
	   {  
	  class Local {};
	       String methodname = Local.class.getEnclosingMethod().getName();
	  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",reportBean.getUserId(), "", "",methodname 
	);
	    //logger.info("***********Start Execution *******getTxnDetails*******");
	    GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(reportDao.getTravelTxn(reportBean));
	 
	   }
	
	@POST
	@Path("/getPartnerLedger")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getPartnerLedger(ReportBean reportBean) {
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "",
			"getPartnerLedger()|"+reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getPartnerLedger(reportBean));
		
	}
	
	
	
	@POST
	@Path("/getAgentClosingBalReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentClosingBalReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getAgentClosingBalReport()|"+reportBean.getAgentId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Start Date" 
				+"|"+reportBean.getStDate());
		//logger.info("Start excution ===================== method getAgentClosingBalReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAgentClosingBalReport(reportBean.getStDate(),reportBean.getAggreatorid()));
		
	}
	

	@POST
	@Path("/getWalletTxnDetailsReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getWalletTxnDetailsReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getWalletTxnDetailsReport()|"+reportBean.getAgentId()+"Start Date" 
					+"|"+reportBean.getStDate());
		/*Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Start Date" 
				+"|"+reportBean.getStDate());*/
		//logger.info("Start excution ===================== method getWalletTxnDetailsReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getWalletTxnDetailsReport(reportBean.getStDate(),reportBean.getEndDate(), reportBean.getAggreatorid()));
	}
	

	@POST
	@Path("/getBtocDmtReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getBtocDmtReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getBtocDmtReport()|"+reportBean.getAgentId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Start Date" 
				+"|"+reportBean.getStDate());
		//logger.info("Start excution ===================== method getBtocDmtReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getBtocDmtReport(reportBean.getStDate(),reportBean.getEndDate(),reportBean.getAggreatorid()));
	}
	
	
	@POST
	@Path("/getKycReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getKycReport(ReportBean reportBean){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", 
			"getKycReport()|"+reportBean.getAgentId());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "","Start Date" 
				+"|"+reportBean.getStDate());
		//logger.info("Start excution ===================== method getBtocDmtReport(reportBean)"+reportBean.getStDate());
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getKycReport(reportBean.getStDate(),reportBean.getEndDate(),reportBean.getAggreatorid()));
	}
	
	@POST
	@Path("/getCashDepositBank")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getListOfCashdepositBankByAggregator(String aggregatorId)
	{
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggregatorId,"","", "", "","getListOfCashdepositBankByAggregator");
	    GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(new CommanUtilDaoImpl().getListOfCashdepositBankByAggregator(aggregatorId));
	}
	
	// change by mani
	
	@GET
	@Path("/leanedAccountReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String leanedAccountReport(){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","leanedAccountReport");
		   
		logger.info("Start execution ===================== method leanedAccountReport()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	   // System.out.println("BBPS REPORT DATA  "+reportDao.bbpsReport(inputBean).get(0).getAggreatorid());
	    //JSONObject jObject=new JSONObject(inputBean);
		//System.out.println("====ok===="+jObject);
	    System.out.println("-----ok------"+reportDao.leanedAccountReport());
	    
	    return gson.toJson(reportDao.leanedAccountReport());
	}
	
	@POST
	@Path("/leanReport")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String leanReport(String aggId){
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","leanReport");
		   
		logger.info("Start execution ===================== method leanReport()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	   
	    return gson.toJson(reportDao.leanReport(aggId));
	}
	
	
	@POST
	@Path("/agentAccountList")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String agentAccountList(AddBankAccount account){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),account.getUserId(),"","", "", "",  "getAgentByDistId()|" );
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.agentAccountList(account.getUserId()));
	}
	
	@POST
	@Path("/validateAepsMerchant")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String validateAepsMerchant(AepsTokenRequest req){
		
	      
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",  "getAgentByDistId()|" );
		//logger.info("Start excution ===================== method getAgentByDistId(reportBean)");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.validateAepsMerchant(req));
	}
	
	
	@POST
	@Path("/getSuperDist")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSuperDist(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"",reportBean.getUserId(), "", "", "getSdId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSuperDist(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getSoDist")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getSoDist(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"",reportBean.getUserId(), "", "", "getSdId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getSoDist(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getDistributorUnderSd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistributorUnderSd(ReportBean reportBean){
		 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggId(),"",reportBean.getUserId(), "", "", "getSdId()");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getDistributorUnderSd(reportBean.getAggId()));
	}
	
	@POST
	@Path("/getDistributorAllotid")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getDistributorAllotid(ReportBean reportBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getDistributorAllotid()" );
 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getDistributorAllotid(reportBean.getAggreatorid(),reportBean.getUserId()));
		
	}
	
	
	@POST
	@Path("/updateTokenId")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String updateTokenId(WalletMastBean mast){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","updateTokenId()" );
 
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.updateTokenId(mast));
		
	}
	
	@POST
	@Path("/getAgentBySuper")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAgentBySuper(ReportBean reportBean){
		
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),reportBean.getAggreatorid(),"",reportBean.getUserId(), "", "", "getAgentBySuper()"+ reportBean.getAgentId());
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(reportDao.getAgentBySuper(reportBean.getDistId(),reportBean.getAggId(),reportBean.getUserType()));
	}
	
	@POST
	@Path("/getAepsReportSuper")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAepsReportSuper(WalletMastBean walletMastBean) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getAepsReportSuper()" );
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(reportDao.getAepsReportSuper(walletMastBean));
		
	}
	
	
	
	@GET
	@Path("/getUserEnable")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<WalletMastBean> getUserEnable() {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "","getUserEnable()" );
		
	    return reportDao.getUserEnable();
		
	}
	
	
	@POST
    @Path("/getCmsReportForAgent")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String getCmsReportForAgent(WalletMastBean walletMastBean) {

	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),walletMastBean.getAggreatorid(),walletMastBean.getWalletid(),"", "", "","getCmsReportForAgent()" );
     GsonBuilder builder = new GsonBuilder();
     Gson gson = builder.create();

	 return gson.toJson(reportDao.getCmsReportForAgent(walletMastBean));
   }
 
	
}
