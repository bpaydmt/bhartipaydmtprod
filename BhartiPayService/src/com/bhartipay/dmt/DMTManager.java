package com.bhartipay.dmt;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.dmt.bean.DMTInputBean;
import com.bhartipay.dmt.persistence.DMTDao;
import com.bhartipay.dmt.persistence.DMTDaoImpl;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.util.thirdParty.bean.WalletExist;

@Path("/DMTManager")
public class DMTManager {
	private static final Logger logger = Logger.getLogger(DMTManager.class.getName());
	DMTDao dMTDao=new DMTDaoImpl();
	
	@POST
	@Path("/CustomerValidation")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DMTInputBean CustomerValidation(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","CustomerValidation()" );

		//logger.info("Start excution ===================== method CustomerValidation(dMTInputBean)");
		return dMTDao.CustomerValidation(dMTInputBean);
	}
	

	
	@POST
	@Path("/CustomerRegistration")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DMTInputBean CustomerRegistration(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","CustomerRegistration()" );

		//logger.info("Start excution ===================== method CustomerRegistration(dMTInputBean)");
		return dMTDao.CustomerRegistration(dMTInputBean);
	}
	
	
	
	@POST
	@Path("/OTCConfirmation")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DMTInputBean OTCConfirmation(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","OTCConfirmation()" );

		//logger.info("Start excution ===================== method CustomerRegistration(dMTInputBean)");
		return dMTDao.OTCConfirmation(dMTInputBean);
	}
	
	
	@POST
	@Path("/AddingBeneficiary")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DMTInputBean AddingBeneficiary(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","AddingBeneficiary()" );

		//logger.info("Start excution ===================== method AddingBeneficiary(dMTInputBean)");
		return dMTDao.AddingBeneficiary(dMTInputBean);
	}
	

	
	@POST
	@Path("/checkWalletExistRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletExist checkWalletExistRequest(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","checkWalletExistRequest()" );

		//logger.info("Start excution ===================== method checkWalletExistRequest(dMTInputBean)");
		return dMTDao.checkWalletExistRequest(dMTInputBean.getMobile());
	}
	
	@POST
	@Path("/createWalletRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletExist createWalletRequest(DMTInputBean dMTInputBean){	
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","createWalletRequest()" );

	//logger.info("Start excution ===================== method createWalletRequest(dMTInputBean)");
	return dMTDao.createWalletRequest(dMTInputBean.getMobile(),dMTInputBean.getName());
	}
	
	@POST
	@Path("/verifyRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletExist verifyRequest(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","verifyRequest()" );

		//logger.info("Start excution ===================== method verifyRequest(dMTInputBean)");
		return dMTDao.verifyRequest(dMTInputBean.getMobile(),dMTInputBean.getRequestNo(),dMTInputBean.getOtp());
	}
	
	@POST
	@Path("/resendOtp")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public WalletExist resendOtp(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","resendOtp()" );

		//logger.info("Start excution ===================== method resendOtp(dMTInputBean)");
		return dMTDao.resendOtp(dMTInputBean.getMobile(),dMTInputBean.getRequestNo());
	}
	
	
	@POST
	@Path("/getUserDetails")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public  UserDetails getUserDetails(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","getUserDetails()" );

		//logger.info("Start excution ===================== method getUserDetails(dMTInputBean)");
		return dMTDao.getUserDetails(dMTInputBean.getMobile());
	}
	
	@POST
	@Path("/addBeneficiaryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails addBeneficiaryRequest(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","addBeneficiaryRequest()" );

		//logger.info("Start excution ===================== method addBeneficiaryRequest(dMTInputBean)");
		return dMTDao.addBeneficiaryRequest(dMTInputBean.getMobile(),dMTInputBean.getBenName(),dMTInputBean.getBenAccount(),dMTInputBean.getAccountType(),dMTInputBean.getBenIFSC());
	}
	
	@POST
	@Path("/deleteBeneficiaryRequest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails deleteBeneficiaryRequest(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","deleteBeneficiaryRequest()" );

		//logger.info("Start excution ===================== method deleteBeneficiaryRequest(dMTInputBean)");
		return dMTDao.deleteBeneficiaryRequest(dMTInputBean.getMobile(),dMTInputBean.getBenCode(),dMTInputBean.getBenName(),dMTInputBean.getBenType(),dMTInputBean.getBenAccount(),dMTInputBean.getBenIFSC());
	}
	
	
	@POST
	@Path("/mrTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails mrTransfer(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","mrTransfer()" +dMTInputBean.getSurChargeAmount());

		//logger.info("Start excution ===================== method mrTransfer(dMTInputBean)***SurChargeAmount"+dMTInputBean.getSurChargeAmount());
		return dMTDao.mrTransfer(dMTInputBean,ipiemi,agent);
	}
	
	
	@POST
	@Path("/reinitiateMrTransfer")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails reinitiateMrTransfer(@HeaderParam("IPIMEI") String ipiemi,@HeaderParam("AGENT") String agent,DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","reinitiateMrTransfer()" +dMTInputBean.getSurChargeAmount());

		//logger.info("Start excution ===================== method mrTransfer(dMTInputBean)***SurChargeAmount"+dMTInputBean.getSurChargeAmount());
		return dMTDao.reinitiateMrTransfer(dMTInputBean,ipiemi,agent);
	}
	
	
	
	
	
	@POST
	@Path("/getUserBalance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails getUserBalance(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","getUserBalance()" );

		//logger.info("Start excution ===================== method getUserDetails(dMTInputBean)");
		return dMTDao.getUserBalance(dMTInputBean.getMobile());
	}
	
	
	@POST
	@Path("/getTransStatus")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails getTransStatus(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","getTransStatus()" );

		//logger.info("Start excution ===================== method getTransStatus(dMTInputBean)");
		return dMTDao.getTransStatus(dMTInputBean.getAgentTransId());
	}
	
	
	@POST
	@Path("/getTransHistory")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public UserDetails getTransHistory(DMTInputBean dMTInputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTInputBean.getAggreatorid(),dMTInputBean.getWalletId(),dMTInputBean.getUserId(), "", "","getTransHistory()" );

		//logger.info("Start excution ===================== method getTransHistory(dMTInputBean)");
		return dMTDao.getTransHistory(dMTInputBean.getMobile());
	}
	
	
}
