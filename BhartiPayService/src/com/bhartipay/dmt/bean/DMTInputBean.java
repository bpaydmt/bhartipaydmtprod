package com.bhartipay.dmt.bean;

import javax.persistence.Column;
import javax.persistence.Transient;

public class DMTInputBean {
	
	public String fname;
	public String lname;
	private String	otc;
	private String	otcRefCode;
	private String	routingType;
	
	
	public String mobile;
	private String	benAccount;
	private String	benIFSC;
	private String	benName;
	public String name;
	private String	otp;
	private String	requestNo;
	private String	accountType;
	private String	benCode;
	private String	benType;
	
	private double	amount;
	private double surChargeAmount;
	
	
	public String transferType;
	public String benMobile;
	public String agentTransId;
	public String type;
	public String reinitiate;
	public String previousAgentTransId;
	public String description;
	
	private String aggreatorid;
	private String walletId;
	private String userId;
	
	
	
	@Transient
	public String statusCode;
	
	@Transient
	public String statusDesc;
	
	
	
	
	
	
	
	
	
	
	


	public double getSurChargeAmount() {
		return surChargeAmount;
	}

	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getBenMobile() {
		return benMobile;
	}

	public void setBenMobile(String benMobile) {
		this.benMobile = benMobile;
	}

	public String getAgentTransId() {
		return agentTransId;
	}

	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReinitiate() {
		return reinitiate;
	}

	public void setReinitiate(String reinitiate) {
		this.reinitiate = reinitiate;
	}

	public String getPreviousAgentTransId() {
		return previousAgentTransId;
	}

	public void setPreviousAgentTransId(String previousAgentTransId) {
		this.previousAgentTransId = previousAgentTransId;
	}

	public String getBenType() {
		return benType;
	}

	public void setBenType(String benType) {
		this.benType = benType;
	}

	public String getBenCode() {
		return benCode;
	}

	public void setBenCode(String benCode) {
		this.benCode = benCode;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoutingType() {
		return routingType;
	}

	public void setRoutingType(String routingType) {
		this.routingType = routingType;
	}

	public String getBenAccount() {
		return benAccount;
	}

	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}

	public String getBenIFSC() {
		return benIFSC;
	}

	public void setBenIFSC(String benIFSC) {
		this.benIFSC = benIFSC;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public String getOtc() {
		return otc;
	}

	public void setOtc(String otc) {
		this.otc = otc;
	}

	public String getOtcRefCode() {
		return otcRefCode;
	}

	public void setOtcRefCode(String otcRefCode) {
		this.otcRefCode = otcRefCode;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
