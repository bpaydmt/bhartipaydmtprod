package com.bhartipay.dmt.persistence;

import org.json.JSONException;

import com.bhartipay.dmt.bean.DMTInputBean;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.util.thirdParty.bean.WalletExist;

public interface DMTDao {
	
	public DMTInputBean CustomerValidation(DMTInputBean dMTImputBean);
	public DMTInputBean CustomerRegistration(DMTInputBean dMTImputBean);
	public DMTInputBean OTCConfirmation(DMTInputBean dMTImputBean);
	public DMTInputBean AddingBeneficiary(DMTInputBean dMTImputBean);
	
	public WalletExist checkWalletExistRequest(String mobileno);
	public WalletExist createWalletRequest(String mobileno,String Name);
	public WalletExist verifyRequest(String mobileno,String requestno,String otp);
	public  UserDetails getUserDetails(String MoblieNo);
	public WalletExist resendOtp(String mobileno,String requestno);
	public UserDetails getUserBalance(String mobileno);
	public UserDetails addBeneficiaryRequest(String mobileno,String BeneficiaryName,String AccountNo,String AccountType,String IFSC);
	public UserDetails deleteBeneficiaryRequest(String MobileNo,String BeneficiaryCode,String BeneficiaryName,String BeneficiaryType,String AccountNo,String IFSC);
	public UserDetails mrTransfer(DMTInputBean dMTImputBean,String ipiemi,String agent);
	public UserDetails reinitiateMrTransfer(DMTInputBean dMTImputBean,String ipiemi,String agent);
	
	public UserDetails getTransStatus(String agentTransId);
	public UserDetails getTransHistory(String mobileno);
	

}
