package com.bhartipay.dmt.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import io.jsonwebtoken.Claims;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.commission.persistence.CommissionDaoImpl;
import com.bhartipay.dmt.bean.DMTInputBean;
import com.bhartipay.transaction.bean.WalletToBankTxnMast;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.bhartipay.util.thirdParty.CyberPlatDMTInteration;
import com.bhartipay.util.thirdParty.bean.Beneficiary;
import com.bhartipay.util.thirdParty.bean.CardDetail;
import com.bhartipay.util.thirdParty.bean.DMTBean;
import com.bhartipay.util.thirdParty.bean.TAG0;
import com.bhartipay.util.thirdParty.bean.UserDetails;
import com.bhartipay.util.thirdParty.bean.WalletExist;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;

public class DMTDaoImpl implements DMTDao {
	

	
	private static final Logger logger = Logger.getLogger(DMTDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	DMTBean dMTBean=new DMTBean();
	CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
	Properties pverificationProp=new Properties();
	Properties pPaymentProp=new Properties();
	
	HashMap<String, String> hmap = new HashMap<String, String>();
	
	TransactionDao transactionDao=new TransactionDaoImpl();
	
	private transient Properties paymentProp = null;
	String mPaisURL="http://115.249.4.214:8080/testenv/mypaycash/DistributorWalletApi";
	String agentId="19";
	
	
	public DMTInputBean CustomerValidation(DMTInputBean dMTImputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  CustomerValidation()");
		//logger.info("Start excution ********************************************************* method CustomerValidation()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		dMTImputBean.setStatusCode("1001");
		try{
			String	dmttxnid = commanUtilDao.getTrxId("CDMT",dMTImputBean.getAggreatorid());
			dMTBean.setSession("dmttxnid");
			dMTBean.setNumber(dMTImputBean.getMobile());
			dMTBean.setType("5");
			dMTBean.setAmount(1.0);
			dMTBean.setAmountAll(1.0);
			String verificationResp=cyberPlatDMTInteration.DMTVerification(dMTBean);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  verifyresp first time"+verificationResp);
			
			//logger.debug("*******************************************verifyresp first time*****************************`"+verificationResp );
			StringTokenizer st=new StringTokenizer(verificationResp,"|"); 
			while (st.hasMoreTokens()) {  
				 String tok = (String) st.nextToken();
				 pverificationProp.put(tok.substring(0, tok.indexOf('=')),
				     tok.substring(tok.indexOf('=') + 1)); 
		     }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","",""+pverificationProp.get("ERROR"));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","",""+pverificationProp.get("RESULT"));
			
			
			//logger.debug("~~~*********************************~~~`"+pverificationProp.get("ERROR") );
			//logger.debug("~~~~~*********************************~~~`"+pverificationProp.get("RESULT") );
			if(Integer.parseInt(pverificationProp.get("ERROR").toString())==0 && Integer.parseInt(pverificationProp.get("RESULT").toString())==0){
				String paymentResp=cyberPlatDMTInteration.DMTPayment(dMTBean);	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," paymentResp"+paymentResp );
				
				//logger.debug("*******************************************paymentResp *****************************`"+paymentResp );
				 st=new StringTokenizer(paymentResp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 pPaymentProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     }
				if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==0 && Integer.parseInt(pPaymentProp.get("RESULT").toString())==0){
					dMTImputBean.setStatusCode("0");
					dMTImputBean.setStatusDesc(pPaymentProp.getProperty("ADDINFO"));
					
				}else if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==23){
					dMTImputBean.setStatusCode("23");
					dMTImputBean.setStatusDesc("The customer is not registered");
				}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("Operator Server Down");
				}
			}else{
				dMTImputBean.setStatusCode("224");
				dMTImputBean.setStatusDesc("Operator Server Down");
			}
			
		}catch (Exception e) {
			dMTImputBean.setStatusCode("7000");
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  problem in CustomerValidation"+e.getMessage()+" "+e);
			
			//logger.debug("problem in CustomerValidation******************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return dMTImputBean;	
	}
	
	
	
	public DMTInputBean CustomerRegistration(DMTInputBean dMTImputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  CustomerRegistration()");
		
		//logger.info("Start excution ********************************************************* method CustomerRegistration()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		dMTImputBean.setStatusCode("1001");
		try{
			String	dmttxnid = commanUtilDao.getTrxId("CDMT",dMTImputBean.getAggreatorid());
			dMTBean.setSession("dmttxnid");
			dMTBean.setNumber(dMTImputBean.getMobile());
			dMTBean.setfName(dMTImputBean.getFname());
			dMTBean.setlName(dMTImputBean.getLname());
			dMTBean.setType("0");
			dMTBean.setAmount(1.0);
			dMTBean.setAmountAll(1.0);
			String verificationResp=cyberPlatDMTInteration.DMTVerification(dMTBean);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","verifyresp first time CustomerRegistration "+verificationResp);
			
			//logger.debug("*******************************************verifyresp first time**********CustomerRegistration*******************`"+verificationResp );
			StringTokenizer st=new StringTokenizer(verificationResp,"|"); 
			while (st.hasMoreTokens()) {  
				 String tok = (String) st.nextToken();
				 pverificationProp.put(tok.substring(0, tok.indexOf('=')),
				     tok.substring(tok.indexOf('=') + 1)); 
		     }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," "+pverificationProp.get("ERROR"));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  "+pverificationProp.get("RESULT"));
			
			//logger.debug("~~~*******************CustomerRegistration**************~~~`"+pverificationProp.get("ERROR") );
			//logger.debug("~~~~~*****************CustomerRegistration****************~~~`"+pverificationProp.get("RESULT") );
			if(Integer.parseInt(pverificationProp.get("ERROR").toString())==0 && Integer.parseInt(pverificationProp.get("RESULT").toString())==0){
				String paymentResp=cyberPlatDMTInteration.DMTPayment(dMTBean);	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  paymentResp"+paymentResp);
				
				//logger.debug("**************************CustomerRegistration*****************paymentResp *****************************`"+paymentResp );
				 st=new StringTokenizer(paymentResp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 pPaymentProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     }
				
				if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==0 && Integer.parseInt(pPaymentProp.get("RESULT").toString())==0){
					dMTImputBean.setStatusCode("0");
					dMTImputBean.setStatusDesc(pPaymentProp.getProperty("ADDINFO"));
					
				}else if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==23){
					dMTImputBean.setStatusCode("23");
					dMTImputBean.setStatusDesc("The customer is registered already.");
				}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("Operator Server Down");
				}
				}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("Operator Server Down");
				}
			
		}catch (Exception e) {
			dMTImputBean.setStatusCode("7000");
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  problem in CustomerRegistration"+e.getMessage()+" "+e);
			
			//logger.debug("problem in CustomerRegistration******************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return dMTImputBean;
}
	
	
	
	
	public DMTInputBean OTCConfirmation(DMTInputBean dMTImputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  OTCConfirmation()");
		
		//logger.info("Start excution ********************************************************* method OTCConfirmation()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		dMTImputBean.setStatusCode("1001");
		try{
			String	dmttxnid = commanUtilDao.getTrxId("CDMT",dMTImputBean.getAggreatorid());
			dMTBean.setSession("dmttxnid");
			dMTBean.setNumber(dMTImputBean.getMobile());
			dMTBean.setOtc(dMTImputBean.getOtc());
			dMTBean.setOtcRefCode(dMTImputBean.getOtcRefCode());
			dMTBean.setType("2");
			dMTBean.setAmount(1.0);
			dMTBean.setAmountAll(1.0);
			String verificationResp=cyberPlatDMTInteration.DMTVerification(dMTBean);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","verifyresp first time OTCConfirmation"+verificationResp );
			
			//logger.debug("*******************************************verifyresp first time**********OTCConfirmation*******************`"+verificationResp );
			StringTokenizer st=new StringTokenizer(verificationResp,"|"); 
			while (st.hasMoreTokens()) {  
				 String tok = (String) st.nextToken();
				 pverificationProp.put(tok.substring(0, tok.indexOf('=')),
				     tok.substring(tok.indexOf('=') + 1)); 
		     }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  "+pverificationProp.get("ERROR"));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  "+pverificationProp.get("RESULT"));
			
			
			//logger.debug("~~~*******************OTCConfirmation**************~~~`"+pverificationProp.get("ERROR") );
			//logger.debug("~~~~~*****************OTCConfirmation****************~~~`"+pverificationProp.get("RESULT") );
			if(Integer.parseInt(pverificationProp.get("ERROR").toString())==0 && Integer.parseInt(pverificationProp.get("RESULT").toString())==0){
				String paymentResp=cyberPlatDMTInteration.DMTPayment(dMTBean);	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  paymentResp"+paymentResp);
				
				//logger.debug("**************************OTCConfirmation*****************paymentResp *****************************`"+paymentResp );
				 st=new StringTokenizer(paymentResp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 pPaymentProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     }
				
				if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==0 && Integer.parseInt(pPaymentProp.get("RESULT").toString())==0){
					dMTImputBean.setStatusCode("0");
					dMTImputBean.setStatusDesc(pPaymentProp.getProperty("ADDINFO"));
					
				}else if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==224){
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("INVALID REQUEST");
				}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("Operator Server Down");
				}
				}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("INVALID REQUEST PARAMETERS");
				}
			
		}catch (Exception e) {
			dMTImputBean.setStatusCode("7000");
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  problem in OTCConfirmation"+e.getMessage()+" "+e);
			
			//logger.debug("problem in OTCConfirmation******************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return dMTImputBean;
}
	
	
		
	public DMTInputBean AddingBeneficiary(DMTInputBean dMTImputBean){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  AddingBeneficiary()");
	
		//logger.info("Start excution ********************************************************* method AddingBeneficiary()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		dMTImputBean.setStatusCode("1001");
		try{
			String	dmttxnid = commanUtilDao.getTrxId("CDMT",dMTImputBean.getAggreatorid());
			dMTBean.setSession("dmttxnid");
			dMTBean.setNumber(dMTImputBean.getMobile());
			dMTBean.setBenAccount(dMTImputBean.getBenAccount());
			dMTBean.setRoutingType(dMTImputBean.getRoutingType());
			dMTBean.setBenIFSC(dMTImputBean.getBenIFSC());
			dMTBean.setBenName(dMTImputBean.getBenName());
			dMTBean.setType("4");
			dMTBean.setAmount(1.0);
			dMTBean.setAmountAll(1.0);
			String verificationResp=cyberPlatDMTInteration.DMTVerification(dMTBean);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  verifyresp first time AddingBeneficiary "+verificationResp);
			
			//logger.debug("*******************************************verifyresp first time**********AddingBeneficiary*******************`"+verificationResp );
			StringTokenizer st=new StringTokenizer(verificationResp,"|"); 
			while (st.hasMoreTokens()) {  
				 String tok = (String) st.nextToken();
				 pverificationProp.put(tok.substring(0, tok.indexOf('=')),
				     tok.substring(tok.indexOf('=') + 1)); 
		     }
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  )"+pverificationProp.get("ERROR"));
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  "+pverificationProp.get("RESULT"));
			
			//logger.debug("~~~*******************AddingBeneficiary**************~~~`"+pverificationProp.get("ERROR") );
			//logger.debug("~~~~~*****************AddingBeneficiary****************~~~`"+pverificationProp.get("RESULT") );
			if(Integer.parseInt(pverificationProp.get("ERROR").toString())==0 && Integer.parseInt(pverificationProp.get("RESULT").toString())==0){
				String paymentResp=cyberPlatDMTInteration.DMTPayment(dMTBean);	
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","  paymentResp"+paymentResp);
				
				//logger.debug("**************************AddingBeneficiary*****************paymentResp *****************************`"+paymentResp );
				 st=new StringTokenizer(paymentResp,"|"); 
				while (st.hasMoreTokens()) {  
					 String tok = (String) st.nextToken();
					 pPaymentProp.put(tok.substring(0, tok.indexOf('=')),
					     tok.substring(tok.indexOf('=') + 1)); 
			     }
			if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==0 && Integer.parseInt(pPaymentProp.get("RESULT").toString())==0){
					dMTImputBean.setStatusCode("0");
					dMTImputBean.setStatusDesc(pPaymentProp.getProperty("ADDINFO"));
					
			}else if(Integer.parseInt(pPaymentProp.get("ERROR").toString())==23){
					dMTImputBean.setStatusCode("23");
					dMTImputBean.setStatusDesc("CARD IS NOT CREATED BY THIS MOBILE NO.");
			}else{
					dMTImputBean.setStatusCode("224");
					dMTImputBean.setStatusDesc("INVALID REQUEST PARAMETERS.");
			}
			}else{
				dMTImputBean.setStatusCode("224");
				dMTImputBean.setStatusDesc("Operator Server Down");
			}
			
		}catch (Exception e) {
			dMTImputBean.setStatusCode("7000");
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," problem in AddingBeneficiary"+e.getMessage()+" "+e);
			
			//logger.debug("problem in AddingBeneficiary******************" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return dMTImputBean;
	}
	
	
	public WalletExist checkWalletExistRequest(String mobileno) {
		hmap.put("MobileNo", mobileno);
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+mPaisURL);
		
	//	logger.debug("************************************Request URL*********************************"+mPaisURL+"/checkWalletExistRequest");
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/checkWalletExistRequest");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+walletexist.getResponse());
		
		//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
		 Claims claim=jwts.parseToken(walletexist.getResponse());
		 walletexist.setCode(claim.get("Code").toString());
		 walletexist.setCardExists(claim.get("CardExists").toString());
		 walletexist.setMessage(claim.get("Message").toString());
		 walletexist.setResponse(claim.get("Response").toString());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+walletexist.getCardExists());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+walletexist.getCode());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  checkWalletExistRequest()"+walletexist.getMessage());
			
		 /*logger.debug("***********************************************************************walletexist.getCardExists "+walletexist.getCardExists());
		 logger.debug("*****************************************************************walletexist.getCode() "+walletexist.getCode());
		 logger.debug("********************************************************************walletexistgetMessage() "+walletexist.getMessage());*/
		return walletexist;
  }
	
	
	public WalletExist createWalletRequest(String mobileno,String Name){
		hmap.put("MobileNo", mobileno);
		hmap.put("Name", Name);
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/createWalletRequest");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  createWalletRequest()"+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  createWalletRequest()"+walletexist.getResponse());
		
		//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
		 Claims claim=jwts.parseToken(walletexist.getResponse());
		 walletexist.setCode(claim.get("Code").toString());
		 walletexist.setCardExists(claim.get("CardExists").toString());
		 walletexist.setMessage(claim.get("Message").toString());
		 walletexist.setResponse(claim.get("Response").toString());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCardExists());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCode());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getMessage());
			
		//logger.debug("********************************************************************walletexist.getCardExists "+walletexist.getCardExists());
		//logger.debug("********************************************************************walletexist.getCode() "+walletexist.getCode());
		//logger.debug("********************************************************************walletexistgetMessage() "+walletexist.getMessage());
		return walletexist;
	}	
	
	
	public WalletExist verifyRequest(String mobileno,String requestno,String otp){
		hmap.put("MobileNo", mobileno);
		hmap.put("RequestNo", requestno);
		hmap.put("Otp", otp);
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/verifyRequest");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," verifyRequest() "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getResponse());
		
		//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		 walletexist.setCode(claim.get("Code").toString());
		 //walletexist.setCardExists(claim.get("CardExists").toString());
		 walletexist.setMessage(claim.get("Message").toString());
		 walletexist.setOtpVerify(claim.get("OtpVerify").toString());
		 walletexist.setResponse(claim.get("Response").toString());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCardExists());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCode());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getMessage());
			
		 /*logger.debug("********************************************************************walletexist.getCardExists "+walletexist.getCardExists());
		 logger.debug("********************************************************************walletexist.getCode() "+walletexist.getCode());
		logger.debug("********************************************************************walletexistgetMessage() "+walletexist.getMessage());*/
		return walletexist;
	}
	
	
	
	public WalletExist resendOtp(String mobileno,String requestno){
		hmap.put("MobileNo", mobileno);
		hmap.put("RequestNo", requestno);
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/resendOtp");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," resendOtp() "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getResponse());
		
		//logger.debug("***************resendOtp*********************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("******************resendOtp******************walletexist.getResponse()*********************************"+walletexist.getResponse());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		 walletexist.setCode(claim.get("Code").toString());
		 walletexist.setMessage(claim.get("Message").toString());
		 walletexist.setResponse(claim.get("Response").toString());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCardExists());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getCode());
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getMessage());
			
		 //logger.debug("**********************resendOtp**********************************************walletexist.getCode() "+walletexist.getCode());
		 //logger.debug("************************resendOtp********************************************walletexistgetMessage() "+walletexist.getMessage());
		return walletexist;
	}
	
	
 public UserDetails getTransHistory(String mobileno){
		hmap.put("MobileNo", mobileno);
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/getTransHistory");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","getTransHistory()| Output :"+output);
		
		//logger.debug("**************getTransHistory*****************************************************Output :"+output);
		UserDetails  userDetails = new UserDetails();
		try{
			WalletExist walletexist = gson.fromJson(output, WalletExist.class);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," getTransHistory() "+walletexist.getRequestNo());
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getResponse());
			
			//logger.debug("********getTransHistory***********************************************************RequestNo "+walletexist.getRequestNo());
			//logger.debug("********getTransHistory***********************************************************Response "+walletexist.getResponse());
			userDetails.setRequestNo(walletexist.getRequestNo());
			Claims claim=jwts.parseToken(walletexist.getResponse());
			JsonParser parser = new JsonParser();
			JSONObject cc = new JSONObject(claim);
			userDetails=gson.fromJson(cc.toString(), UserDetails.class);
						
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in getUserDetails "+e.getMessage()+" "+e);
			
			//logger.debug("problem in getUserDetails******************" + e.getMessage(), e);
		}
		return userDetails;
		
	}
	
	
public  UserDetails getUserDetails(String MoblieNo){
		hmap.put("MobileNo", MoblieNo);
		paymentProp = new Properties();
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/getUserDetails");
		Gson gson = new Gson();
		String output = response.getEntity(String.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","getUserDetails()| Output :"+output);
		
		//logger.debug("*******************************************************************Output :"+output);
		UserDetails  userDetails = new UserDetails();
		try{
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  "+walletexist.getResponse());
		
		//logger.debug("*******************************************************************RequestNo "+walletexist.getRequestNo());
		//logger.debug("*******************************************************************Response "+walletexist.getResponse());
		userDetails.setRequestNo(walletexist.getRequestNo());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		JsonParser parser = new JsonParser();
		String beneficiary=claim.get("Beneficiary").toString();
	
		JSONObject cc = new JSONObject(claim);
		List<TAG0> tag0s = new ArrayList<>();
	
		if(!beneficiary.equalsIgnoreCase("[]")){
		JSONObject bene_obj = cc.getJSONObject("Beneficiary");
			Iterator<String> tag_keys	=bene_obj.keys();
		while (tag_keys.hasNext()) {
			String key = (String) tag_keys.next();
			JSONObject tag_obj=bene_obj.getJSONObject(key);
			TAG0 tag0 = gson.fromJson(tag_obj.toString(), TAG0.class);
			tag0s.add(tag0);
			}
		}
		
		Beneficiary beneficiary2 = new Beneficiary();
		beneficiary2.settAG0(tag0s);
		userDetails.setCardDetail(gson.fromJson(cc.getJSONObject("CardDetail").toString(), CardDetail.class));//add("CardDetail", cc.getJSONObject("CardDetail"));
		userDetails.setCardExists(cc.getString("CardExists"));
		userDetails.setBeneficiary(beneficiary2);
		userDetails.setResponse(cc.getString("Response"));
		userDetails.setMessage(cc.getString("Message"));
		userDetails.setCode(cc.getString("Code"));
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RequestNo "+userDetails.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  Response "+userDetails.getResponse());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Message "+userDetails.getMessage());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  Code "+userDetails.getCode());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RemitLimitAvailable "+walletexist.getRemitLimitAvailable());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  Balance "+walletexist.getBalance());
		
		/* logger.debug("*******************************************************************RequestNo "+userDetails.getRequestNo());
		 logger.debug("*******************************************************************Response "+userDetails.getResponse());
		 logger.debug("*******************************************************************Message "+userDetails.getMessage());
		 logger.debug("*******************************************************************Code "+userDetails.getCode());
		logger.debug("*******************************************************************RemitLimitAvailable "+walletexist.getRemitLimitAvailable());
		logger.debug("*******************************************************************Balance "+walletexist.getBalance());*/
		}catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  problem in getUserDetails "+e.getMessage()+" "+e);
			
			//logger.debug("problem in getUserDetails******************" + e.getMessage(), e);
		}
		return userDetails;
	}
	
	

public UserDetails addBeneficiaryRequest(String mobileno,String BeneficiaryName,String AccountNo,String AccountType,String IFSC){
	hmap.put("MobileNo", mobileno);
	hmap.put("BeneficiaryName", BeneficiaryName);
	hmap.put("AccountNo", AccountNo);
	hmap.put("AccountType", AccountType);
	hmap.put("IFSC", IFSC);
	
	JWTSignVerify jwts=new JWTSignVerify();
	String token=jwts.generateToken(hmap);
	JsonObject obj = new JsonObject();
	obj.addProperty("AgentId", agentId);
	obj.addProperty("Request", token);
	PostJSonRequest pjr=new PostJSonRequest();
	ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/addBeneficiaryRequest");
	Gson gson = new Gson();
	String output = response.getEntity(String.class); 
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  addBeneficiaryRequest()|Output :"+output);
	
	//logger.debug("*******************************************************************Output :"+output);
	UserDetails  userDetails = new UserDetails();
	try{
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  RequestNo "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+walletexist.getResponse());
		
		//logger.debug("*******************************************************************RequestNo "+walletexist.getRequestNo());
		//logger.debug("*******************************************************************Response "+walletexist.getResponse());
		
	
		//System.out.println(walletexist.getResponse());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		JSONObject cc = new JSONObject(claim);
		
		userDetails.setRequestNo(walletexist.getRequestNo());
		userDetails.setResponse(cc.getString("Response"));
		userDetails.setMessage(cc.getString("Message"));
		userDetails.setCode(cc.getString("Code"));
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RequestNo "+userDetails.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+userDetails.getResponse());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Message "+userDetails.getMessage());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Code "+userDetails.getCode());
		
		 /*logger.debug("*******************************************************************RequestNo "+userDetails.getRequestNo());
		 logger.debug("*******************************************************************Response "+userDetails.getResponse());
		 logger.debug("*******************************************************************Message "+userDetails.getMessage());
		 logger.debug("*******************************************************************Code "+userDetails.getCode());*/
		
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in addBeneficiaryRequest "+e.getMessage()+" "+e);
		
	//	logger.debug("problem in addBeneficiaryRequest******************" + e.getMessage(), e);
	}
	return userDetails;
}

public UserDetails deleteBeneficiaryRequest(String MobileNo,String BeneficiaryCode,String BeneficiaryName,String BeneficiaryType,String AccountNo,String IFSC){
	hmap.put("MobileNo", MobileNo);
	hmap.put("BeneficiaryName", BeneficiaryName);
	hmap.put("BeneficiaryCode", BeneficiaryCode);
	hmap.put("BeneficiaryType", BeneficiaryType);
	hmap.put("AccountNo", AccountNo);
	hmap.put("IFSC", IFSC);
	JWTSignVerify jwts=new JWTSignVerify();
	String token=jwts.generateToken(hmap);
	JsonObject obj = new JsonObject();
	obj.addProperty("AgentId", agentId);
	obj.addProperty("Request", token);
	PostJSonRequest pjr=new PostJSonRequest();
	ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/deleteBeneficiaryRequest");
	Gson gson = new Gson();
	String output = response.getEntity(String.class); 
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," deleteBeneficiaryRequest()|Output :"+output );
	
	//logger.debug("*******************************************************************Output :"+output);
	UserDetails  userDetails = new UserDetails();
	try{
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  RequestNo "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+walletexist.getResponse());
		
		/*logger.debug("*******************************************************************RequestNo "+walletexist.getRequestNo());
		logger.debug("*******************************************************************Response "+walletexist.getResponse());*/
	
	 Claims claim=jwts.parseToken(walletexist.getResponse());
	 JSONObject cc = new JSONObject(claim);
	 userDetails.setRequestNo(walletexist.getRequestNo());
	 userDetails.setResponse(cc.getString("Response"));
	 userDetails.setMessage(cc.getString("Message"));
	 userDetails.setCode(cc.getString("Code"));
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RequestNo "+userDetails.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+userDetails.getResponse());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Message "+userDetails.getMessage());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Code "+userDetails.getCode());
		
	/* logger.debug("*******************************************************************RequestNo "+userDetails.getRequestNo());
	 logger.debug("*******************************************************************Response "+userDetails.getResponse());
	 logger.debug("*******************************************************************Message "+userDetails.getMessage());
	 logger.debug("*******************************************************************Code "+userDetails.getCode());
	*/
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in deleteBeneficiaryRequest"+e.getMessage()+" "+e );
		
		//logger.debug("problem in deleteBeneficiaryRequest******************" + e.getMessage(), e);
	}
	return userDetails;

}



public UserDetails getUserBalance(String mobileno){
	hmap.put("MobileNo", mobileno);
	JWTSignVerify jwts=new JWTSignVerify();
	String token=jwts.generateToken(hmap);
	JsonObject obj = new JsonObject();
	obj.addProperty("AgentId", agentId);
	obj.addProperty("Request", token);
	PostJSonRequest pjr=new PostJSonRequest();
	ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/getUserBalance");
	Gson gson = new Gson();
	String output = response.getEntity(String.class); 
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," getUserBalance()|Output :"+output );
	
	//logger.debug("*******************************************************************Output :"+output);
	UserDetails  userDetails = new UserDetails();
	try{
	WalletExist walletexist = gson.fromJson(output, WalletExist.class);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  RequestNo "+walletexist.getRequestNo());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+walletexist.getResponse());
	
	//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
	//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
	Claims claim=jwts.parseToken(walletexist.getResponse());
	 JSONObject cc = new JSONObject(claim);
	 userDetails.setRequestNo(walletexist.getRequestNo());
	 userDetails.setMobileNo(cc.getString("MobileNo"));
	 userDetails.setBalance(cc.getDouble("Balance"));
	 userDetails.setResponse(cc.getString("Response"));
	 userDetails.setMessage(cc.getString("Message"));
	 userDetails.setCode(cc.getString("Code"));
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," MobileNo "+userDetails.getMobileNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Balance "+userDetails.getBalance());
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RequestNo "+userDetails.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+userDetails.getResponse());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Message "+userDetails.getMessage());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Code "+userDetails.getCode());
		
	/* logger.debug("*******************************************************************MobileNo "+userDetails.getMobileNo());
	 logger.debug("*******************************************************************Balance "+userDetails.getBalance());
	 logger.debug("*******************************************************************RequestNo "+userDetails.getRequestNo());
	 logger.debug("*******************************************************************Response "+userDetails.getResponse());
	 logger.debug("*******************************************************************Message "+userDetails.getMessage());
	 logger.debug("*******************************************************************Code "+userDetails.getCode());*/
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in deleteBeneficiaryRequest"+e.getMessage()+" "+e );
		
		//logger.debug("problem in deleteBeneficiaryRequest******************" + e.getMessage(), e);
	}
	return userDetails;
}




public UserDetails getTransStatus(String agentTransId){
	hmap.put("AgentTransId", agentTransId);
	JWTSignVerify jwts=new JWTSignVerify();
	String token=jwts.generateToken(hmap);
	JsonObject obj = new JsonObject();
	obj.addProperty("AgentId", agentId);
	obj.addProperty("Request", token);
	PostJSonRequest pjr=new PostJSonRequest();
	ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/getTransStatus");
	Gson gson = new Gson();
	String output = response.getEntity(String.class); 
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," getTransStatus()|Output :"+output );
	
	//logger.debug("*******************************************************************Output :"+output);
	UserDetails  userDetails = new UserDetails();
	try{
	WalletExist walletexist = gson.fromJson(output, WalletExist.class);
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "","","  RequestNo "+walletexist.getRequestNo());
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+walletexist.getResponse());
	
	//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
	//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
	Claims claim=jwts.parseToken(walletexist.getResponse());
	 JSONObject cc = new JSONObject(claim);
	 userDetails=gson.fromJson(cc.toString(), UserDetails.class);
	 
	 
	/* userDetails.setRequestNo(walletexist.getRequestNo());
	 userDetails.setMobileNo(cc.getString("MobileNo"));
	 userDetails.setBalance(cc.getDouble("Balance"));
	 userDetails.setResponse(cc.getString("Response"));
	 userDetails.setMessage(cc.getString("Message"));
	 userDetails.setCode(cc.getString("Code"));*/
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," MobileNo "+userDetails.getMobileNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Balance "+userDetails.getBalance());
	 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," RequestNo "+userDetails.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Response "+userDetails.getResponse());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Message "+userDetails.getMessage());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," Code "+userDetails.getCode());
		
	 /*logger.debug("*******************************************************************MobileNo "+userDetails.getMobileNo());
	 logger.debug("*******************************************************************Balance "+userDetails.getBalance());
	 logger.debug("*******************************************************************RequestNo "+userDetails.getRequestNo());
	 logger.debug("*******************************************************************Response "+userDetails.getResponse());
	 logger.debug("*******************************************************************Message "+userDetails.getMessage());
	 logger.debug("*******************************************************************Code "+userDetails.getCode());*/
	}catch (Exception e) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "",""," problem in deleteBeneficiaryRequest"+e.getMessage()+" "+e );
		
		
		//logger.debug("problem in deleteBeneficiaryRequest******************" + e.getMessage(), e);
	}
	return userDetails;
}






public UserDetails mrTransfer(DMTInputBean dMTImputBean,String ipiemi,String agent){
	
	String agentTransId="";
	WalletToBankTxnMast walletToBankTxnMast=new WalletToBankTxnMast();

	walletToBankTxnMast.setAggreatorid(dMTImputBean.getAggreatorid());
	walletToBankTxnMast.setUserId(dMTImputBean.getUserId());
	walletToBankTxnMast.setWalletId(dMTImputBean.getWalletId());
	walletToBankTxnMast.setPayeecode(dMTImputBean.getBenCode());
	walletToBankTxnMast.setPayeename(dMTImputBean.getBenName());
	walletToBankTxnMast.setAccno(dMTImputBean.getBenAccount());
	walletToBankTxnMast.setAcctype(dMTImputBean.getAccountType());
	walletToBankTxnMast.setIfsccode(dMTImputBean.getBenIFSC());
	walletToBankTxnMast.setAmount(dMTImputBean.getAmount());
	walletToBankTxnMast.setSurCharge(dMTImputBean.getSurChargeAmount());
	walletToBankTxnMast.setTransferType(dMTImputBean.getTransferType());
	walletToBankTxnMast.setMobileno(dMTImputBean.getBenMobile());
	walletToBankTxnMast.setDescription(dMTImputBean.getDescription());
	walletToBankTxnMast.setType(dMTImputBean.getType());
	walletToBankTxnMast.setReinitiate(dMTImputBean.getReinitiate());
	walletToBankTxnMast.setPreviousTxnId(dMTImputBean.getPreviousAgentTransId());
	walletToBankTxnMast.setIpimei(ipiemi);
	walletToBankTxnMast.setUseragent(agent);
	
	walletToBankTxnMast=transactionDao.dmtTransfer(walletToBankTxnMast);
	
	UserDetails  userDetails = new UserDetails();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," mrTransfer()|StatusCode :"+walletToBankTxnMast.getStatusCode() );
	
	//logger.debug("*******************************************************************walletToBankTxnMast.getStatusCode() :"+walletToBankTxnMast.getStatusCode());
	if(walletToBankTxnMast.getStatusCode().equals("1000")){
		
		agentTransId=walletToBankTxnMast.getWtbtxnid();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," MobileNo :"+dMTImputBean.getMobile());
		
		//logger.debug("*******************************************************************dMTImputBean.getMobile :"+dMTImputBean.getMobile());
		hmap.put("MobileNo", dMTImputBean.getMobile());
		hmap.put("BeneficiaryCode", dMTImputBean.getBenCode());
		hmap.put("BeneficiaryName", dMTImputBean.getBenName());
		hmap.put("AccountNo", dMTImputBean.getBenAccount());
		hmap.put("AccountType", dMTImputBean.getAccountType());
		hmap.put("IFSC", dMTImputBean.getBenIFSC());
		hmap.put("Amount", Integer.toString((int)dMTImputBean.getAmount()));
		hmap.put("TransferType", dMTImputBean.getTransferType());
		hmap.put("BeneficiaryMobile", dMTImputBean.getBenMobile());
		hmap.put("AgentTransId", agentTransId);
		//hmap.put("Description", dMTImputBean.getDescription());
		hmap.put("Type", dMTImputBean.getType());
		hmap.put("Reinitiate", dMTImputBean.getReinitiate());
		hmap.put("PreviousAgentTransId", dMTImputBean.getPreviousAgentTransId());
		
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/mrTransfer");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," Output :"+output);
		
		//logger.debug("*******************************************************************Output :"+output);
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","RequestNo "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," Response "+walletexist.getResponse());
		
		//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		JSONObject cc = new JSONObject(claim);
		
		
		userDetails=gson.fromJson(cc.toString(), UserDetails.class);
		
		
		String dmtResponse=cc.toString();
		transactionDao.dmtTransferUpdate(userDetails,dmtResponse);
	}else{
		 userDetails.setResponse("ERROR");
		 userDetails.setMessage("ERROR");
		 userDetails.setCode(walletToBankTxnMast.getStatusCode());
		 userDetails.setMobileNo(walletToBankTxnMast.getMobileno());
	}
	
	return userDetails;
	
}



public UserDetails reinitiateMrTransfer(DMTInputBean dMTImputBean,String ipiemi,String agent){
	
	String agentTransId="";
	WalletToBankTxnMast walletToBankTxnMast=new WalletToBankTxnMast();

	walletToBankTxnMast.setAggreatorid(dMTImputBean.getAggreatorid());
	walletToBankTxnMast.setUserId(dMTImputBean.getUserId());
	walletToBankTxnMast.setWalletId(dMTImputBean.getWalletId());
	walletToBankTxnMast.setPayeecode(dMTImputBean.getBenCode());
	walletToBankTxnMast.setPayeename(dMTImputBean.getBenName());
	walletToBankTxnMast.setAccno(dMTImputBean.getBenAccount());
	walletToBankTxnMast.setAcctype(dMTImputBean.getAccountType());
	walletToBankTxnMast.setIfsccode(dMTImputBean.getBenIFSC());
	walletToBankTxnMast.setAmount(dMTImputBean.getAmount());
	walletToBankTxnMast.setSurCharge(dMTImputBean.getSurChargeAmount());
	walletToBankTxnMast.setTransferType(dMTImputBean.getTransferType());
	walletToBankTxnMast.setMobileno(dMTImputBean.getBenMobile());
	walletToBankTxnMast.setDescription(dMTImputBean.getDescription());
	walletToBankTxnMast.setType(dMTImputBean.getType());
	walletToBankTxnMast.setReinitiate(dMTImputBean.getReinitiate());
	walletToBankTxnMast.setPreviousTxnId(dMTImputBean.getPreviousAgentTransId());
	walletToBankTxnMast.setIpimei(ipiemi);
	walletToBankTxnMast.setUseragent(agent);
	//walletToBankTxnMast=transactionDao.dmtTransfer(walletToBankTxnMast);

	
	
	walletToBankTxnMast=transactionDao.dmtReinitiateTransfer(walletToBankTxnMast);
	UserDetails  userDetails = new UserDetails();
	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," reinitiateMrTransfer()|StatusCode "+walletToBankTxnMast.getStatusCode());
	
	//logger.debug("*******************************************************************walletToBankTxnMast.getStatusCode() :"+walletToBankTxnMast.getStatusCode());
	if(walletToBankTxnMast.getStatusCode().equals("1000")){
		 agentTransId=walletToBankTxnMast.getWtbtxnid();
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," MobileNO "+dMTImputBean.getMobile());
			
		// logger.debug("*******************************************************************dMTImputBean.getMobile :"+dMTImputBean.getMobile());
		
		hmap.put("MobileNo", dMTImputBean.getMobile());
		hmap.put("BeneficiaryCode", dMTImputBean.getBenCode());
		hmap.put("BeneficiaryName", dMTImputBean.getBenName());
		hmap.put("AccountNo", dMTImputBean.getBenAccount());
		hmap.put("AccountType", dMTImputBean.getAccountType());
		hmap.put("IFSC", dMTImputBean.getBenIFSC());
		//hmap.put("Amount", Double.toString(dMTImputBean.getAmount()));
		hmap.put("Amount", Integer.toString((int)dMTImputBean.getAmount()));
		hmap.put("TransferType", dMTImputBean.getTransferType());
		hmap.put("BeneficiaryMobile", dMTImputBean.getBenMobile());
		hmap.put("AgentTransId", agentTransId);
		//hmap.put("Description", dMTImputBean.getDescription());
		hmap.put("Type", dMTImputBean.getType());
		hmap.put("Reinitiate", dMTImputBean.getReinitiate());
		hmap.put("PreviousAgentTransId", dMTImputBean.getPreviousAgentTransId());
		
		JWTSignVerify jwts=new JWTSignVerify();
		String token=jwts.generateToken(hmap);
		JsonObject obj = new JsonObject();
		obj.addProperty("AgentId", agentId);
		obj.addProperty("Request", token);
		PostJSonRequest pjr=new PostJSonRequest();
		ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/mrTransfer");
		Gson gson = new Gson();
		String output = response.getEntity(String.class); 
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," output "+output);
		
		//logger.debug("*******************************************************************Output :"+output);
		WalletExist walletexist = gson.fromJson(output, WalletExist.class);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "","","RequestNo "+walletexist.getRequestNo());
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),dMTImputBean.getAggreatorid(),dMTImputBean.getWalletId(),dMTImputBean.getUserId(), "",""," Response "+walletexist.getResponse());
		
		//logger.debug("************************************walletexist.getRequestNo()*********************************"+walletexist.getRequestNo());
		//logger.debug("************************************walletexist.getResponse()*********************************"+walletexist.getResponse());
		Claims claim=jwts.parseToken(walletexist.getResponse());
		JSONObject cc = new JSONObject(claim);
		userDetails=gson.fromJson(cc.toString(), UserDetails.class);
		String dmtResponse=cc.toString();
		transactionDao.dmtTransferUpdate(userDetails,dmtResponse);
	}else{
		 userDetails.setResponse("ERROR");
		 userDetails.setMessage("ERROR");
		 userDetails.setCode(walletToBankTxnMast.getStatusCode());
		 userDetails.setMobileNo(walletToBankTxnMast.getMobileno());
	}
	return userDetails;
	
}


public WalletExist getBankDetails(String BankName,String IFSC,String  BranchName){
	JWTSignVerify jwts=new JWTSignVerify();
	String token=jwts.generateToken(hmap);
	JsonObject obj = new JsonObject();
	obj.addProperty("AgentId", agentId);
	obj.addProperty("Request", token);
	PostJSonRequest pjr=new PostJSonRequest();
	ClientResponse response=pjr.submitRequest(obj.toString(),mPaisURL+"/getBankDetails");
	Gson gson = new Gson();
	String output = response.getEntity(String.class);
	System.out.println("Output :"+output);
	WalletExist walletexist = gson.fromJson(output, WalletExist.class);
	Claims claim=jwts.parseToken(walletexist.getResponse());
	
	return walletexist;
	
}







	public static void main(String args[]) throws Exception{
		new DMTDaoImpl().checkWalletExistRequest("9935041287");
		//  new DMTDaoImpl().createWalletRequest("9935041287" ,"skumar");
        // new DMTDaoImpl().verifyRequest("9935041287" ,"582addf96677c","927005");
//new DMTDaoImpl().getUserDetails("9935041287");
		//new DMTDaoImpl().addBeneficiaryRequest("9935041287","Sandeep","50100058431673","Savings","HDFC0001898");
		//new DMTDaoImpl().resendOtp("9935041287" ,"582addf96677c");
		//new DMTDaoImpl().deleteBeneficiaryRequest("9935041287","ntKbq","Sandeep","NEFT/IMPS","50100058431672","HDFC0001898");
		//new DMTDaoImpl().getUserBalance("9935041287");
		
		/*DMTInputBean dMTInputBean=new DMTInputBean();
		dMTInputBean.setMobile("9935041287");
		dMTInputBean.setAggreatorid("AGGR001035");
		dMTInputBean.setWalletId("865456531103112016162157");
		dMTInputBean.setUserId("AGEN001016");
		dMTInputBean.setBenCode("NheQa");
		dMTInputBean.setBenName("Santosh");
		dMTInputBean.setBenAccount("50100058433274");
		dMTInputBean.setAccountType("Savings");
		dMTInputBean.setBenIFSC("HDFC0001898");
		dMTInputBean.setAmount(15);
		dMTInputBean.setTransferType("NEFT");
		dMTInputBean.setBenMobile("9935041287");
		//dMTImputBean.setDescription("test..");
		dMTInputBean.setType("VALIDATEBENEF");
		dMTInputBean.setReinitiate("FALSE");
		dMTInputBean.setPreviousAgentTransId("");*/
		//new DMTDaoImpl().mrTransfer(dMTImputBean);
		//new DMTDaoImpl().getTransHistory("9935041287");
		//new DMTDaoImpl().getTransStatus("WTAT001065");
		
		
		
		
	}
	
}
