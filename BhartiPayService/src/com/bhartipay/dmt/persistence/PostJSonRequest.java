package com.bhartipay.dmt.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class PostJSonRequest {	
	    
	public ClientResponse submitRequest(String params,String redirectUrl){
		String output=null;
		try{
			ClientConfig config = new DefaultClientConfig();			
			Client client=Client.create(config);
			//client.addFilter(new LoggingFilter());
			//client.addFilter(new HTTPBasicAuthFilter(secureObject.getTranportalID(), secureObject.getTranportalPwd()));
			WebResource webResource=client.resource(redirectUrl);
			String jsonText = params;
			System.out.println("jsonText"+jsonText);
			//System.out.println(webResource.post(JSONObject.class,jsonText));
			ClientResponse response = webResource.accept("application/json").type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, jsonText);
			
			//ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
			
			System.out.println(response.getStatus());
			if (response.getStatus() != 200) {
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			/*String output1 = response.getEntity(String.class);
			System.out.println(output1);*/
			
			return response;
			
		}catch (Exception e) {
				e.printStackTrace();
		}
		return null;
	}
}
