package com.bhartipay.dmt.persistence;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class JWTSignVerify{
      public Claims parseToken(String token) {
        try {
        	String str = new String(DatatypeConverter.printBase64Binary(new String("e391ab57590132714ad32da9acf3013eb88c").getBytes()));
            System.out.println("Token :"+token);
        	Claims body = Jwts.parser()
                    .setSigningKey(str)
                    .parseClaimsJws(token)
                    .getBody();

           //System.out.println(Jwts.parser().setSigningKey(str).parse(token).getBody().toString().substring(1, Jwts.parser().setSigningKey(str).parse(token).getBody().toString().length())); 
        	return body;

        } catch (JwtException | ClassCastException e) {
        	System.out.println(e);
            return null;
        }
		//return token;
    }

    /**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     * 
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(HashMap<String, String> valueObj ) {
        
    	Claims claims = Jwts.claims();
        
        //Jwts.header();
        Set set = valueObj.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
           System.out.println(mentry.getValue());
           claims.put((String) mentry.getKey(), mentry.getValue());
        }

        String str = new String(DatatypeConverter.printBase64Binary(new String("e391ab57590132714ad32da9acf3013eb88c").getBytes()));
		//Claims claims = Jwts.claims();
		//claims.put("MobileNo", "1111111111");
		return Jwts.builder()
		    .setHeaderParam("typ", "JWT").setHeaderParam("alg", "HS256")
		    .setClaims(claims).signWith(SignatureAlgorithm.HS256, str)   
		    .compact();
                
    }
    
  public static void main(String args[]){
	  JWTSignVerify sv=new JWTSignVerify();
	  System.out.println(sv.parseToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJNb2JpbGVObyI6IjExMTExMTExMTEifQ.GRr5R0gsaNxB-hOrEax6uqoPhy88_AeMeNCwe2fwhPw"));
	  System.out.println(sv.parseToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJNb2JpbGVObyI6IjExMTExMTExMTEifQ.Z_4k91rQX_QUqeUZTZT81yZqLZMpWt-EkId6oIPd0pg"));
	  
  }
}