package com.bhartipay.rechargeService.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="rechargeoperatormaster")

public class RechargeOperatorMasterBean {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column( name ="operatorid", length=5, nullable = false)
	private int operatorId;
	
	@Column( name ="name", length=100, nullable = false)
	private String name;
	
	@Column( name ="servicetype", length=100, nullable = false)
	private String serviceType;
	
	@Column( name ="rechargetype", length=100, nullable = false)
	private String rechargeType;
	
	@Column( name ="code", length=10, nullable = false)
	private String code;
	
	@Column( name ="client", length=50, nullable = false)
	private String client;
	
	@Column( name ="url", length=200, nullable = false)
	private String url;
	
	@Column( name ="flag", length=1, nullable = false)
	private int flag;
   
	@Column(name="operatorcode")
	private String operatorCode;
	
	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}
	
	
	
	
	
	
}
