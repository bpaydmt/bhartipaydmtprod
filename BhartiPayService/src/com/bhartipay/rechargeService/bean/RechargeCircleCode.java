package com.bhartipay.rechargeService.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rechargecirclecode")
public class RechargeCircleCode
{

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sno")
	private int sno;
	
	@Column(name="circlename")
	private String circleName;
	
	@Column(name="circlecode")
	private int circleCode;
	
	@Column(name="payworldcode")
	private int payWorldCode;

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public int getCircleCode() {
		return circleCode;
	}

	public void setCircleCode(int circleCode) {
		this.circleCode = circleCode;
	}

	public int getPayWorldCode() {
		return payWorldCode;
	}

	public void setPayWorldCode(int payWorldCode) {
		this.payWorldCode = payWorldCode;
	}
	
	
}
