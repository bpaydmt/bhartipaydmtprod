package com.bhartipay.rechargeService.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.lean.bean.RechargeBillRequest;
import com.bhartipay.util.DBUtil;

public class RechargeDaoImpl implements RechargeDao {

	public static final Logger LOGGER = Logger.getLogger(RechargeDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	
	@Override
	public boolean getRechargeAgentRequest(RechargeBillRequest req) {
		// TODO Auto-generated method stub
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		boolean bbpsRequest = false;
		String resp="FAIL";
		try {
			 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			 String dateString = format.format( new Date(System.currentTimeMillis()));
			SQLQuery query = session.createSQLQuery("select count(*) from rechargebillrequest where userId='" + req.getUserId()
					+ "' and operatorName='"+req.getOperatorName()+"'and mobileNumber='"+req.getMobileNumber()+"' and requestDate='"+dateString+"' and amount='"+req.getAmount()+"' and creationDate  > DATE_SUB(NOW(), INTERVAL 10 MINUTE) ");
		   int count = ((BigInteger) query.uniqueResult()).intValue();
		   if (count > 0) {
				resp = "You have already processed transaction with same details please wait for 10 minutes.";
				bbpsRequest=false;
			} else {
				resp = "SUCCESS";
			bbpsRequest=true;
			}
		   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",req.getUserId(),"","","Record exist getRechargeAgentRequest  ");
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",req.getUserId(),"","","|problem in getRechargeAgentRequest "+e.getMessage()+" "+e);
			e.printStackTrace();
			transaction.rollback();
			bbpsRequest=false;
		} finally {
			session.close();
		}
		
		return bbpsRequest;
	}

	@Override
	public boolean saveRechargeAgentRequest(RechargeBillRequest request) {
		// TODO Auto-generated method stub
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		boolean bbpsRequest = false;
		try {
			if(request!=null)
			{
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",request.getUserId(),"","","saveRechargeAgentRequest ");
			session.save(request);	
			transaction.commit();
			bbpsRequest = true;
			}
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",request.getUserId(),"","","|problem in saveRechargeAgentRequest "+e.getMessage()+" "+e);
			e.printStackTrace();
			transaction.rollback();
			bbpsRequest=false;
			
		} finally {
			session.close();
		}
		
		return bbpsRequest;
	}

}
