package com.bhartipay.rechargeService.dao;

import com.bhartipay.lean.bean.RechargeBillRequest;

public interface RechargeDao {

	public boolean getRechargeAgentRequest(RechargeBillRequest bbpsPayment);
	public boolean saveRechargeAgentRequest(RechargeBillRequest bbpsPaymentReq);
	
}
