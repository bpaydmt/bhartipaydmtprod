package com.bhartipay.customerCare.bean;

import java.io.Serializable;
import java.util.List;

import com.bhartipay.transaction.bean.CashBackPassbookBean;
import com.bhartipay.transaction.bean.PassbookBean;

public class CustomerCareResponse implements Serializable {
	
	private String statusCode;
	private String statusMsg;
	
	private double walletBalance;
	private double oxyCashBalance;
	
	private AgentCustDetails agentCustDetails;
	
	private List <PassbookBean> passbook;
	private List <CashBackPassbookBean> cashbackpassbook;
	
	
	
	
	
	
	
	
	
	
	public List<CashBackPassbookBean> getCashbackpassbook() {
		return cashbackpassbook;
	}
	public void setCashbackpassbook(List<CashBackPassbookBean> cashbackpassbook) {
		this.cashbackpassbook = cashbackpassbook;
	}
	public List<PassbookBean> getPassbook() {
		return passbook;
	}
	public void setPassbook(List<PassbookBean> passbook) {
		this.passbook = passbook;
	}
	public double getWalletBalance() {
		return walletBalance;
	}
	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}
	public double getOxyCashBalance() {
		return oxyCashBalance;
	}
	public void setOxyCashBalance(double oxyCashBalance) {
		this.oxyCashBalance = oxyCashBalance;
	}
	public AgentCustDetails getAgentCustDetails() {
		return agentCustDetails;
	}
	public void setAgentCustDetails(AgentCustDetails agentCustDetails) {
		this.agentCustDetails = agentCustDetails;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	
	
	
	

}
