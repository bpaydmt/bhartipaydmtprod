package com.bhartipay.customerCare.persistence;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

import com.bhartipay.commission.persistence.CommissionDaoImpl;
import com.bhartipay.customerCare.bean.AgentCustDetails;
import com.bhartipay.customerCare.bean.CustomerCareResponse;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.util.DBUtil;



public class CustomerCareDaoImpl implements CustomerCareDao {
	
/*	private static CustomerCareDaoImpl customerCareDaoImpl=null;
	private CustomerCareDaoImpl(){
		
	}
	
	public static CustomerCareDaoImpl getCustomerCareDaoImpl(){
		if(customerCareDaoImpl==null){
			customerCareDaoImpl=new CustomerCareDaoImpl();
		}
		return customerCareDaoImpl;
	}
	*/
	private static final Logger logger = Logger.getLogger(CustomerCareDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	TransactionDao transactionDao=new TransactionDaoImpl();
	
	
	@SuppressWarnings("unchecked")
	public CustomerCareResponse getUserDtl(String userId,String aggreatorId,String serverName ){
		logger.info(serverName+"****serverName****"+serverName+"*******"+userId+"***Start excution ****************************** getUserDtl method**mobileNo/email/id***:"	+ userId);
		logger.info(serverName+"****serverName****"+serverName+"*******"+userId+"***Start excution ****************************** getUserDtl method**aggreatorid***:"	+ aggreatorId);
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		 List<AgentCustDetails> results=null;
		 AgentCustDetails agentCustDetails=null;
		 CustomerCareResponse customerCareResponse=new CustomerCareResponse();
		try{
			SQLQuery  query=null;
			WalletMastBean wBean=(WalletMastBean)session.get(WalletMastBean.class,aggreatorId);
			if(wBean!=null && wBean.getWhiteLabel()!=null&&wBean.getWhiteLabel().equalsIgnoreCase("1")){
				query=session.createSQLQuery("SELECT usertype,kycstatus,id, approvalRequired, walletid,mobileno,emailid,name,userstatus,distributerid,aggreatorid,(SELECT `mas`.`name` FROM `walletmast` `mas` WHERE (`mas`.`id` = `walletmast`.`distributerid`))  `distributername`,(select ws.userStatus from walletsecurity ws where ws.walletid=walletmast.walletid) blockStatus,address1,address2, city,state,pin,pan, addressprooftype,adhar,agenttype,shopname,DATE_FORMAT(creationDate,'%d/%m/%Y %h:%i:%s') creationDate, DATE_FORMAT(approveDate,'%d/%m/%Y %h:%i:%s') approveDate,managername,soname,agentcode,asagentcode  FROM `walletmast`  WHERE (emailid=:userId OR mobileno=:userId OR id=:userId) AND aggreatorid=:aggreatorId and usertype in (1,2,3,4,6,7,8,9)");
				query.setParameter("aggreatorId", aggreatorId);
			}else{
				query=session.createSQLQuery("SELECT usertype,kycstatus,id, approvalRequired, walletid,mobileno,emailid,name,userstatus,distributerid,aggreatorid,(SELECT `mas`.`name` FROM `walletmast` `mas` WHERE (`mas`.`id` = `walletmast`.`distributerid`))  `distributername`,(select ws.userStatus from walletsecurity ws where ws.walletid=walletmast.walletid) blockStatus,address1,address2, city,state,pin,pan, addressprooftype,adhar,agenttype,shopname,DATE_FORMAT(creationDate,'%d/%m/%Y %h:%i:%s') creationDate, DATE_FORMAT(approveDate,'%d/%m/%Y %h:%i:%s') approveDate,managername,soname,agentcode,asagentcode  FROM `walletmast`  WHERE (emailid=:userId OR mobileno=:userId OR id=:userId) and usertype in (1,2,3,4,6,7,8,9)");
			}
			query.setParameter("userId", userId);
			
			query.setResultTransformer(Transformers.aliasToBean(AgentCustDetails.class));
			results=query.list();
			if (results != null && results.size() != 0) {
				agentCustDetails = (AgentCustDetails)results.get(0);
				agentCustDetails.setPan(EncryptionDecryption.getdecrypted(agentCustDetails.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
				agentCustDetails.setAdhar(EncryptionDecryption.getdecrypted(agentCustDetails.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
			}else{
				
				customerCareResponse.setStatusCode("1");
				customerCareResponse.setStatusMsg("No Data Found.");
				return customerCareResponse;
				
			}
			customerCareResponse.setAgentCustDetails(agentCustDetails);
			customerCareResponse.setWalletBalance(transactionDao.getWalletBalancebyId(agentCustDetails.getId()));
			customerCareResponse.setOxyCashBalance(transactionDao.getCashBackWalletBalancebyId(agentCustDetails.getId()));
			//01/10/2017
			
			/* DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 Date date = new Date();
		     String stDate=sdf.format(date);
			 Calendar cal = Calendar.getInstance();
		     cal.setTime(date);
		     cal.add(Calendar.DATE, -10);
		     Date dateBefore10Days = cal.getTime();
		     String endDate=sdf.format(dateBefore10Days);
		     customerCareResponse.setPassbook(transactionDao.showPassbook(agentCustDetails.getWalletid(),stDate,endDate));
		     customerCareResponse.setCashbackpassbook(transactionDao.showCashBackPassbook(agentCustDetails.getWalletid(),stDate,endDate));*/
		     
		   
			
		}catch (Exception e) {
				logger.debug(serverName+"****serverName****"+serverName+"*******"+userId+"*******problem in getUserDtl***************" + e.getMessage(), e);
				customerCareResponse.setStatusCode("1");
				customerCareResponse.setStatusMsg("Error");
				e.printStackTrace();
				transaction.rollback();
			} finally {
				session.close();
			}
			
		return customerCareResponse;
		
	}

}
