package com.bhartipay.customerCare.persistence;

import com.bhartipay.customerCare.bean.CustomerCareResponse;

public interface CustomerCareDao {
	
	public CustomerCareResponse getUserDtl(String userId,String aggreatorId,String serverName );

}
