package com.bhartipay.customerCare;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.customerCare.bean.CustomerCareRequest;
import com.bhartipay.customerCare.bean.CustomerCareResponse;
import com.bhartipay.customerCare.persistence.CustomerCareDao;
import com.bhartipay.customerCare.persistence.CustomerCareDaoImpl;



@Path("/CustomerCareManager")
public class CustomerCareManager {
	private static final Logger logger = Logger.getLogger(CustomerCareManager.class.getName());
	CustomerCareDao customerCareDao=new CustomerCareDaoImpl();
	
//	CustomerCareDao customerCareDao=CustomerCareDaoImpl.getCustomerCareDaoImpl();
	
	@POST
	@Path("/CustomerCareResponse")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public CustomerCareResponse getUserDtl(CustomerCareRequest customerCareRequest,@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		logger.info("serverName****"+serverName+"********Start excution ******************************************** method CustomerCareResponse(customerCareRequest)");
		return customerCareDao.getUserDtl(customerCareRequest.getUserId(),customerCareRequest.getAggreatorId(),serverName);
	}
	
	
	
	

}
