package com.bhartipay.cme.persistence;

import java.util.List;
import java.util.Map;

import com.bhartipay.cme.bean.CMEAccountStsBean;
import com.bhartipay.cme.bean.CMEMoneyAgentTransferBean;
import com.bhartipay.cme.bean.CMERequestBean;
import com.bhartipay.cme.bean.CMERespBean;
import com.bhartipay.cme.bean.CMEResponse;

public interface CMEDao {
	
	/**
	 * 
	 * @param serverName
	 * @return
	 */
	public String getBankDtl(String serverName,String userId);
	
	/**
	 * 
	 * @param serverName
	 * @return
	 */
	public String getDeposittype(String serverName);
	
	
	
	/**
	 * 
	 * @param cMERequestBean
	 * @return
	 */
	public CMEResponse saveCMERequest(String serverName,CMERequestBean cMERequestBean);
	
	
	
	/**
	 * 
	 * @param serverName
	 * @param cMERequestBean
	 * @return
	 */
	public String getCMERequestList(String serverName,CMERequestBean cMERequestBean);
	
	/**
	 * 
	 * @param serverName
	 * @param walletId
	 * @return
	 */
	public String getDMTBalance(String serverName,String walletId);
	
	
	/**
	 * 
	 * @param serverName
	 * @param mobileNo
	 * @param aggreatorId
	 * @return
	 */
	
	public CMERespBean getAgnDtlByUserId(String serverName,String unId,String aggreatorId,String distributorId);
	
	public CMERespBean getAgnDtlByUserIdSo(String serverName,String unId,String aggreatorId,String distributorId,String usertype,String userId);
	
	/**
	 * 
	 * @param serverName
	 * @param mobileNo
	 * @param aggreatorId
	 * @return
	 */
	
	public CMERespBean getDistributorDtlByUserId(String serverName,String unId,String aggreatorId,String distributorId);
	/**
	 * 
	 * @param serverName
	 * @param mobileNo
	 * @param aggreatorId
	 * @param mpin
	 * @return
	 */
	public CMERespBean validateMPIN(String serverName,String mobileNo,String aggreatorId,String mpin,String chechksum,String userId);
	
	/**
	 * 
	 * @param serverName
	 * @param mobileNo
	 * @param aggreatorId
	 * @param mpin
	 * @return
	 */
	public CMERespBean changeMPIN(String serverName,String mobileNo,String aggreatorId,String mpin,String chechksum,String userId,String newmpin);
	
	public CMERespBean forgotMPIN(String serverName,String mobileNo,String aggreatorId,String otp,String chechksum,String userId,String newmpin);
	
	
	/**
	 * 
	 * @param serverName
	 * @param cMEMoneyAgentTransferBean
	 * @return
	 */
	public CMERespBean moneyTrnsfer(String serverName,CMEMoneyAgentTransferBean cMEMoneyAgentTransferBean);
	
	/**
	 * 
	 * @param walletId
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public CMERespBean getCMEAccountStsList(String serverName,String walletId,String stDate,String endDate);
	
	
	/**
	 * 
	 * @param walletId
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public CMERespBean getMovementReportBySupDist(String serverName,String walletId,String stDate,String endDate);
	
	
	
	//added for CME checker
	 /**
	  * 
	  * @param aggreatorId
	  * @return
	  */
	 public List<CMERequestBean> getCMECheckerListByAggId(String aggreatorId , String status,String stDate,String endDate);
	 /**
	  * 
	  * @param aggreatorId
	  * @return
	  */
	 public boolean acceptCMERequest(String RequestId,String remarksReason,String ipIemi,String agent);
	 /**
	  * 
	  * @param aggreatorId
	  * @return
	  */
	  public boolean rejectCMERequest(String RequestId,String remarksReason);
	  
	  /**
	   * 
	   * @return
	   */
	  public Map<String,String> getCMEUserList();
	
	  
	  /**
	   * 
	   * @param aggreatorId
	   * @param status
	   * @param stDate
	   * @param endDate
	   * @return
	   */
	  public List<CMERequestBean> getCMEApproveListByAggId(String aggreatorId, String status, String stDate,	String endDate) ;
	  
	  /**
	   * 
	   * @param RequestId
	   * @param remarkApprover
	   * @param ipIemi
	   * @param agent
	   * @return
	   */
	  public boolean approveCMERequest(String RequestId, String remarkApprover, String ipIemi, String agent);
	  
	  
	  /**
	   * 
	   * @param RequestId
	   * @param remarkApprover
	   * @return
	   */
	  public boolean disapproverCMERequest(String RequestId, String remarkApprover);

}
