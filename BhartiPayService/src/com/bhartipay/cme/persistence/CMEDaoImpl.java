package com.bhartipay.cme.persistence;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.json.JSONException;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cme.bean.BankdtlBean;
import com.bhartipay.cme.bean.CMEAccountStsBean;
import com.bhartipay.cme.bean.CMEAgentDetails;
import com.bhartipay.cme.bean.CMEMoneyAgentTransferBean;
import com.bhartipay.cme.bean.CMERequestBean;
import com.bhartipay.cme.bean.CMERespBean;
import com.bhartipay.cme.bean.CMEResponse;
import com.bhartipay.cme.bean.DepositDtlBean;
import com.bhartipay.cme.bean.PayLoadBean;
import com.bhartipay.cme.bean.ResPayload;
import com.bhartipay.mudra.bank.persistence.MudraDaoBank;
import com.bhartipay.mudra.bank.persistence.MudraDaoBankImpl;
import com.bhartipay.transaction.bean.CashDepositMast;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.user.bean.DeclinedListBean;
import com.bhartipay.user.bean.WalletMastBean;
import com.bhartipay.user.persistence.WalletUserDao;
import com.bhartipay.user.persistence.WalletUserDaoImpl;
import com.bhartipay.util.CommanUtil;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.WalletSecurityUtility;
import com.bhartipay.util.bean.UserRoleMapping;
import com.bhartipay.util.bean.WalletConfiguration;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.google.gson.Gson;

public class CMEDaoImpl implements CMEDao {
	
	

	private static final Logger logger = Logger.getLogger(CMEDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	CommanUtilDaoImpl commanUtilDao = new CommanUtilDaoImpl();
	// added for CME Checker
	WalletUserDao walletUserDao = new WalletUserDaoImpl();
	TransactionDaoImpl transactionDao = new TransactionDaoImpl();
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public String getBankDtl(String serverName, String userId) {
		JSONObject json = new JSONObject();
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", serverName +  "|getBankDtl()"
					);
		//logger.info(serverName + "******" + userId + "Start excution ===================== method getBankDtl()");
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		ArrayList bankList = new ArrayList();
		ArrayList depositList = new ArrayList();
		try {
			WalletMastBean user = (WalletMastBean)session.get(WalletMastBean.class, userId);
			if(user != null)
			{
			String bankAcc = null;
			SQLQuery query = session
					.createSQLQuery("SELECT bank_name,account_no FROM cashdepositbankdetail WHERE aggregator_id='"+user.getAggreatorid()+"'");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				BankdtlBean bankdtlBean = new BankdtlBean();
				 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", serverName +  "|bankAcc Details "+ bankAcc
							);
				//logger.info(serverName + "******" + userId + "bankAcc Details ===================== " + bankAcc);
				bankdtlBean.setAccountNumber(row[1].toString());
				bankdtlBean.setBankName(row[0].toString());
				bankList.add(bankdtlBean);

			}
			json.put("statusCode", "1000");
			json.put("statusMsg", "SUCCESS");
			json.put("payload", bankList);

			/*****************
			 * start changes by ambuj singh
			 *************************/
			try {
				String deposit = null;
				SQLQuery query1 = session.createSQLQuery("SELECT id,type FROM deposittypedtl WHERE STATUS=1 ");
				List<Object[]> rows1 = query1.list();
				for (Object[] row : rows1) {
					DepositDtlBean depositDtlBean = new DepositDtlBean();
					deposit = row[1].toString();
					depositDtlBean.setDepositeId(deposit);
					depositDtlBean.setDepositeType(deposit);
					 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", serverName +  "|bankAcc Details "+ deposit
								);
					//logger.info(serverName + "bankAcc Details ===================== " + deposit);
					depositList.add(depositDtlBean);
				}
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			json.put("depositList", depositList);
			/*****************
			 * end changes by ambuj singh
			 *************************/
			}
			else
			{
				try {
					json.put("statusCode", "0");
					json.put("statusMsg", "FAILED ");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", serverName +  "|problem in getBankDtl "+ e.getMessage()+" "+e
					);
			/*logger.debug(serverName + "******" + userId + "problem in getBankDtl==================" + e.getMessage(),
					e);*/
			try {
				json.put("statusCode", "0");
				json.put("statusMsg", "FAILED " + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","",userId, "", "", serverName +  "|Total Bank Details "
				);
		//logger.info(serverName + "Total Bank Details ===================== method getBankDtl()");
		return json.toString();

	}

	@Override
	public String getDeposittype(String serverName) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +  "|getDeposittype() "
				);
		//logger.info(serverName + "Start excution ===================== method getDeposittype()");
		JSONObject json = new JSONObject();
		ArrayList depositList = new ArrayList();
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();

		try {
			String deposit = null;
			SQLQuery query = session.createSQLQuery("SELECT id,type FROM deposittypedtl WHERE STATUS=1 ");
			List<Object[]> rows = query.list();
			for (Object[] row : rows) {
				DepositDtlBean depositDtlBean = new DepositDtlBean();
				deposit = row[1].toString();
				depositDtlBean.setDepositeId(deposit);
				depositDtlBean.setDepositeType(deposit);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +  "|bankAcc Details "+deposit
						);
				//logger.info(serverName + "bankAcc Details ===================== " + deposit);
				depositList.add(depositDtlBean);
			}
			json.put("statusCode", "1000");
			json.put("statusMsg", "SUCCESS");
			json.put("payload", depositList);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				json.put("statusCode", "0");
				json.put("statusMsg", "FAILED " + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +  "|problem in getDeposittype "+e.getMessage()+" "+e
					);
			logger.debug(serverName + "problem in getDeposittype==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "", serverName +  "|Total Bank Details "
				);
		//logger.info(serverName + "Total Bank Details ===================== method getDeposittype()");

		return json.toString();

	}

	@Override
	public CMEResponse saveCMERequest(String serverName, CMERequestBean cMERequestBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|saveCMERequest()"
				);
		
	/*	logger.info(serverName
				+ "start excution ***************************************************** method saveCMERequest()");*/
		CMEResponse cMEResponse = new CMEResponse();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			
			TreeMap<String,String>  object=new TreeMap<String,String>(); 
			object.put("aggreatorId", cMERequestBean.getAggreatorId());
            object.put("userId", cMERequestBean.getUserId());
            object.put("walletId", cMERequestBean.getWalletId());
            object.put("bankName",cMERequestBean.getBankName());
            object.put("branchName",cMERequestBean.getBranchName());
            object.put("docType",cMERequestBean.getDocType());
            object.put("txnRefNo",cMERequestBean.getTxnRefNo());
            object.put("amount",""+cMERequestBean.getAmount());
            object.put("depositeDate",cMERequestBean.getDepositeDate());
            object.put("remarks",cMERequestBean.getRemarks());
            object.put("CHECKSUMHASH",cMERequestBean.getCHECKSUMHASH());
			int statusCode= WalletSecurityUtility.checkSecurity(object);

		    if(statusCode!=1000){
		    	cMEResponse.setStatusCode(""+statusCode);
				cMEResponse.setStatusMsg("Security error.");
				return cMEResponse;
		    }
			
		    
			
			if (cMERequestBean.getAggreatorId() == null || cMERequestBean.getAggreatorId().isEmpty()) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|Aggreator Id is empty"
						);
				/*logger.info(serverName + "Inside *********************************************Aggreator Id is empty"
						+ cMERequestBean.getAggreatorId());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Aggreator Id Can't  be empty ");
				return cMEResponse;
			}

			if (cMERequestBean.getBankName() == null || cMERequestBean.getBankName().isEmpty()) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|Aggreator Id is empty"+ cMERequestBean.getBankName()
						);
				/*logger.info(serverName + "Inside *********************************************Aggreator Id is empty"
						+ cMERequestBean.getBankName());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Bank Name Can't  be empty ");
				return cMEResponse;
			}

			if (cMERequestBean.getBranchName() == null || cMERequestBean.getBranchName().isEmpty()) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|BranchName Id is empty"+cMERequestBean.getBranchName()
						);
				/*logger.info(serverName + "Inside *********************************************BranchName Id is empty"
						+ cMERequestBean.getBranchName());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Branch Name Can't  be empty ");
				return cMEResponse;
			}

			if (cMERequestBean.getDocType() == null || cMERequestBean.getDocType().isEmpty()) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|DocType Id is empty "+cMERequestBean.getDocType()
						);
				/*logger.info(serverName + "Inside *********************************************DocType Id is empty"
						+ cMERequestBean.getDocType());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Document Type Can't  be empty ");
				return cMEResponse;
			}

			if (cMERequestBean.getTxnRefNo() == null || cMERequestBean.getTxnRefNo().isEmpty()) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|TxnRefNo Id is empty"+cMERequestBean.getTxnRefNo()
						);
				/*logger.info(serverName + "Inside *********************************************TxnRefNo Id is empty"
						+ cMERequestBean.getTxnRefNo());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Txn Ref Number Can't  be empty ");
				return cMEResponse;
			}

			if (cMERequestBean.getAmount() <= 0) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getAmount Id is empty"+ cMERequestBean.getAmount()
						);
				/*logger.info(serverName + "Inside *********************************************getAmount Id is empty"
						+ cMERequestBean.getAmount());*/
				cMEResponse.setStatusCode("0");
				cMEResponse.setStatusMsg("Amount can't be 0 or less the 0");
				return cMEResponse;
			}

			/*
			 * if( cMERequestBean.getDepositeDate()==null
			 * ||cMERequestBean.getDepositeDate().isEmpty()){ logger.info(
			 * serverName+"Inside *********************************************Aggreator Id is empty"
			 * + cMERequestBean.getTxnRefNo()); cMEResponse.setStatusCode("0");
			 * cMEResponse.setStatusMsg("Deposite Date be 0 or less the 0");
			 * return cMEResponse; }
			 */

			/*
			 * if( cMERequestBean.getReciptDoc()==null
			 * ||cMERequestBean.getReciptDoc().isEmpty()){ logger.info(
			 * serverName+"Inside *********************************************Aggreator Id is empty"
			 * + cMERequestBean.getTxnRefNo()); cMEResponse.setStatusCode("0");
			 * cMEResponse.setStatusMsg("Upload the reciept Document."); return
			 * cMEResponse; }
			 */
			WalletMastBean wBean=(WalletMastBean)session.get(WalletMastBean.class,cMERequestBean.getUserId());
			
			cMERequestBean.setCmeRequestid(commanUtilDao.getTrxId("CMEREQ",cMERequestBean.getAggreatorId()));
			cMERequestBean.setStatus("PENDING");
			cMERequestBean.setCheckerStatus("PENDING");
			cMERequestBean.setApproverStatus("PENDING");
			CashDepositMast cMast=new CashDepositMast();

cMast.setDepositId(cMERequestBean.getCmeRequestid()); 
//if(wBean!=null&&wBean.getSuperdistributerid()!=null&&!wBean.getSuperdistributerid().isEmpty()&&!wBean.getSuperdistributerid().equalsIgnoreCase("-1")){
//cMast.setAggreatorId(wBean.getSuperdistributerid());	
//}else{
cMast.setAggreatorId(cMERequestBean.getAggreatorId());
//}
cMast.setUserId(cMERequestBean.getUserId());    
cMast.setWalletId(cMERequestBean.getWalletId());                 
cMast.setType(cMERequestBean.getDocType());    
cMast.setAmount(cMERequestBean.getAmount());  
cMast.setNeftRefNo(cMERequestBean.getTxnRefNo());  
cMast.setReciptPic(cMERequestBean.getReciptDoc());  
cMast.setStatus(cMERequestBean.getStatus());    
cMast.setIpIemi(cMERequestBean.getIpIemi());        
cMast.setAgent(cMERequestBean.getAgent());
SimpleDateFormat df=null;
if(cMERequestBean.getDepositeDate()!=null&&cMERequestBean.getDepositeDate().indexOf("-")>=0){
df=new SimpleDateFormat("dd-MMM-yyyy");
}else{
df=new SimpleDateFormat("dd/MM/yyyy");	
}
Date depositDate=df.parse(cMERequestBean.getDepositeDate());
cMast.setDepositDate(new java.sql.Date(depositDate.getTime()));
cMast.setBankName(cMERequestBean.getBankName());              
cMast.setBranchName(cMERequestBean.getBranchName());  
//remark  
//remarkapprover  
cMast.setCheckerStatus(cMERequestBean.getCheckerStatus()); 
cMast.setApproverStatus(cMERequestBean.getApproverStatus());        
//requestdate



//----------  -----------  ----------  ------------------------  ------  -------  ---------  ---------  --------  ------------  -------------------------------------------------------------------------------------------------------------------  -------------------  --------------------  ----------  ------  --------------  -------------  --------------  ---------------------
//CASH001108  AGGR001047   AGGR001047  978777787729062017182612  IMPS    1234.00  ddkkd      (NULL)     Approved  192.168.0.17  Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36  2018-03-08 00:00:00  AXIS-916020076556799  -           fdfre   efsrse          Accepted       Accepted          2018-03-08 17:49:24			
			
			session.save(cMast);
			transaction.commit();
			cMEResponse.setStatusCode("1000");
			cMEResponse.setStatusMsg("SUCCESS");
			PayLoadBean payload = new PayLoadBean();
			payload.setcMERequestBean(cMERequestBean);
			cMEResponse.setResPayload(payload);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|problem in saveCMERequest"+ e.getMessage()+" "+e
					);
			//logger.debug(serverName + "problem in saveCMERequest=========================" + e.getMessage(), e);
			cMEResponse.setStatusCode("7000");
			cMEResponse.setStatusMsg("FAILED " + e.getMessage());
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return cMEResponse;
	}

	@Override
	public String getCMERequestList(String serverName, CMERequestBean cMERequestBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getCMERequestList()"
				);
		/*logger.info(serverName
				+ "start excution ***************************************************** method getCMERequestList()");
		logger.info(serverName
				+ "start excution ***************************************************** method getAggreatorId()"
				+ cMERequestBean.getAggreatorId());
		logger.info(
				serverName + "start excution ***************************************************** method getUserId()"
						+ cMERequestBean.getUserId());*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<CashDepositMast> cashDepositMast = new ArrayList<CashDepositMast>();
		JSONObject json = new JSONObject();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
			StringBuilder serchQuery = new StringBuilder();
			//serchQuery.append(" from CashDepositMast where aggreatorId =:aggreatorId and userId=:userId and depositId like :depositId");
			serchQuery.append(" from CashDepositMast where aggreatorId =:aggreatorId and userId=:userId");

			if (cMERequestBean.getStDate() != null && !cMERequestBean.getStDate().isEmpty()
					&& cMERequestBean.getEndDate() != null && !cMERequestBean.getEndDate().isEmpty()) {
				serchQuery.append(" and  DATE(requestdate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(cMERequestBean.getStDate()))
						+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(cMERequestBean.getEndDate())) + "', '/', '-'),'%d-%m-%Y')");

			} else {
				serchQuery.append(" and date(requestdate)=CURDATE()");
				// serchQuery.append(" and reqestdate=DATE_FORMAT(CURRENT_DATE,
				// '%d/%m/%Y')");
				// serchQuery.append(" and date(reqestdate)=date(CURDATE())");
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|serchQuery"+serchQuery
					);
			//logger.info(serverName + "serchQuery ***************************************************** " + serchQuery);
			Query query = session.createQuery(serchQuery.toString());
			query.setString("aggreatorId", cMERequestBean.getAggreatorId());
			query.setString("userId", cMERequestBean.getUserId());
			//query.setString("depositId","CMER%");
			cashDepositMast = query.list();
			List<CMERequestBean> cmeRequestList=new ArrayList<CMERequestBean>();
			if(cashDepositMast!=null&&cashDepositMast.size()>0){
				for(CashDepositMast cdm:cashDepositMast){
					CMERequestBean cme=new CMERequestBean();
					cme.setAgent(cdm.getAgent());
					cme.setAggreatorId(cdm.getAggreatorId());
					cme.setAmount(cdm.getAmount());
					cme.setApproverStatus(cdm.getApproverStatus());
					cme.setBankName(cdm.getBankName());
					cme.setBranchName(cdm.getBranchName());
					cme.setCheckerStatus(cdm.getCheckerStatus());
					cme.setCmeRequestid(cdm.getDepositId());
					//cme.setDepositeDate(cdm.getDepositDate());
					//cme.setDistributerId(cdm.getde);
					cme.setDocType(cdm.getType());
					cme.setRemarkApprover(cdm.getRemarkApprover());
					cme.setRemarks(cdm.getRemark());
					cme.setReqestdate(cdm.getRequestdate());
					cme.setStatus(cdm.getStatus());
					cme.setTxnRefNo(cdm.getNeftRefNo());
					cme.setUserId(cdm.getUserId());
					cme.setWalletId(cdm.getWalletId());
					
					cmeRequestList.add(cme);
				}
			}
			

			json.put("statusCode", "1000");
			json.put("statusMsg", "SUCCESS");
			json.put("count", cmeRequestList.size());
			JSONObject json1 = new JSONObject();
			json1.put("cmeRequestList", cmeRequestList);
			json.put("resPayload", json1);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				json.put("statusCode", "0");
				json.put("statusMsg", "FAILED " + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|problem in getCMERequestList"+e.getMessage()+" "+e
					);
			logger.debug(serverName + "problem in getCMERequestList==================" + e.getMessage(), e);
		} finally {
			session.close();
		}
		return json.toString();
	}

	public String getDMTBalance(String serverName, String walletId) {
		JSONObject json = new JSONObject();
		try {
			NumberFormat formatter = new DecimalFormat("#0.00");
			double dmtBalance = new TransactionDaoImpl().getWalletBalance(walletId);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|getDMTBalance()|"+ formatter.format(dmtBalance)
					);
			/*logger.info(
					serverName + "**************************************************" + formatter.format(dmtBalance));*/
			json.put("statusCode", "1000");
			json.put("statusMsg", "SUCCESS");
			JSONObject json1 = new JSONObject();
			json1.put("dmtBalance", formatter.format(dmtBalance));
			json.put("resPayload", json1);
		} catch (Exception e1) {
			try {
				json.put("statusCode", "0");
				json.put("statusMsg", "Failed");
			} catch (JSONException e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|problem in getDeposittype"+ e.getMessage()+" "+e
						);
				//logger.debug(serverName + "problem in getDeposittype==================" + e.getMessage(), e);
			}

			e1.printStackTrace();
		}
		return json.toString();

	}

	public CMERespBean getAgnDtlByUserId(String serverName, String mobileNo, String aggreatorId, String distributorId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|getAgnDtlByUserId()"+ mobileNo
				);
		//logger.info(serverName + "**************************************getAgnDtlByUserId************" + mobileNo);
		// JSONObject json=new JSONObject();
		CMERespBean cMERespBean = new CMERespBean();
		CMEAgentDetails cMEAgentDetails = null;
		try {

			WalletMastBean agentDtl = (WalletMastBean) new TransactionDaoImpl().getAgnDtlByUserIdCME(mobileNo,
					aggreatorId, distributorId);

			if (agentDtl == null) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id does not exist"+ mobileNo
						);
				/*logger.info(serverName + "**************************************Agent Id does not exist************"
						+ mobileNo);*/
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Agent Id does not exist");
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id exist"
						);
				//logger.info(serverName + "**************************************Agent Id exist************");
				cMEAgentDetails = new CMEAgentDetails();
				cMEAgentDetails.setId(agentDtl.getId());
				cMEAgentDetails.setMobileno(agentDtl.getMobileno());
				cMEAgentDetails.setEmailid(agentDtl.getEmailid());
				cMEAgentDetails.setName(agentDtl.getName());
				cMEAgentDetails.setAddress1(agentDtl.getAddress1());
				cMEAgentDetails.setAddress2(agentDtl.getAddress2());
				cMEAgentDetails.setCity(agentDtl.getCity());
				cMEAgentDetails.setState(agentDtl.getState());
				cMEAgentDetails.setPin(agentDtl.getPin());
				/****************
				 * start change by ambuj singh
				 *******************/
				cMEAgentDetails.setAvailableLimit(0);
				cMEAgentDetails.setAssignedLimit(0);

				cMEAgentDetails.setClosingBal("" + new TransactionDaoImpl()
						.getWalletBalance(new TransactionDaoImpl().getWalletIdByUserId(mobileNo, aggreatorId)));

				/**************** end change by ambuj singh *******************/
				cMERespBean.setStatusCode("1000");
				cMERespBean.setStatusMsg("SUCCESS");
				ResPayload resPayload = new ResPayload();
				resPayload.setAgentDtl(cMEAgentDetails);
				cMERespBean.setResPayload(resPayload);

			}
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|problem in getAgnDtlByUserId"+ e.getMessage()+" "+e
					);
			//logger.debug(serverName + "problem in getAgnDtlByUserId==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		return cMERespBean;

	}
	
	public CMERespBean getDistributorDtlByUserId(String serverName, String mobileNo, String aggreatorId, String distributorId){
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|getDistributorDtlByUserId()"+ mobileNo
				);
		//logger.info(serverName + "**************************************getDistributorDtlByUserId************" + mobileNo);
		// JSONObject json=new JSONObject();
		CMERespBean cMERespBean = new CMERespBean();
		CMEAgentDetails cMEAgentDetails = null;
		try {

			WalletMastBean agentDtl = (WalletMastBean) new TransactionDaoImpl().getDistributorDtlByUserId(mobileNo,
					aggreatorId, distributorId);

			if (agentDtl == null) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id does not exist)"+ mobileNo
						);
				/*logger.info(serverName + "*********************************getDistributorDtlByUserId*****Agent Id does not exist************"
						+ mobileNo);*/
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Agent Id does not exist");
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id exist"
						);
				//logger.info(serverName + "**************************************Agent Id exist************");
				cMEAgentDetails = new CMEAgentDetails();
				cMEAgentDetails.setId(agentDtl.getId());
				cMEAgentDetails.setMobileno(agentDtl.getMobileno());
				cMEAgentDetails.setEmailid(agentDtl.getEmailid());
				cMEAgentDetails.setName(agentDtl.getName());
				cMEAgentDetails.setAddress1(agentDtl.getAddress1());
				cMEAgentDetails.setAddress2(agentDtl.getAddress2());
				cMEAgentDetails.setCity(agentDtl.getCity());
				cMEAgentDetails.setState(agentDtl.getState());
				cMEAgentDetails.setPin(agentDtl.getPin());
				/****************
				 * start change by ambuj singh
				 *******************/
				cMEAgentDetails.setAvailableLimit(0);
				cMEAgentDetails.setAssignedLimit(0);

				cMEAgentDetails.setClosingBal("" + new TransactionDaoImpl()
						.getWalletBalance(new TransactionDaoImpl().getWalletIdByUserId(mobileNo, aggreatorId)));

				/**************** end change by ambuj singh *******************/
				cMERespBean.setStatusCode("1000");
				cMERespBean.setStatusMsg("SUCCESS");
				ResPayload resPayload = new ResPayload();
				resPayload.setAgentDtl(cMEAgentDetails);
				cMERespBean.setResPayload(resPayload);

			}
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|problem in getAgnDtlByUserId"+ e.getMessage()+" "+e
					);
			//logger.debug(serverName + "problem in getAgnDtlByUserId==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		return cMERespBean;

	
	}

	public CMERespBean validateMPIN(String serverName, String mobileNo, String aggreatorId, String mpin,String chechksum,String userId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|validateMPIN()|mobileNo"+ mobileNo+"|mpin "+mpin
				);
	/*	logger.info(serverName + "**************************************validateMPIN************" + mobileNo);
		logger.info(serverName + "**************************************validateMPIN****mpin********" + mpin);*/
		CMERespBean cMERespBean = new CMERespBean();
		try {

			TreeMap<String,String>  map=new TreeMap<String,String>(); 
			map.put("mobileNo",mobileNo);
	        map.put("aggreatorId",aggreatorId);
	        map.put("mpin",mpin);
	        map.put("userId",userId);
	        map.put("CHECKSUMHASH",chechksum);
			int statusCode= WalletSecurityUtility.checkSecurity(map);

		    if(statusCode!=1000){
		    	cMERespBean.setStatusCode(""+statusCode);
				cMERespBean.setStatusMsg("Security error.");
				return cMERespBean;
		    }
			
			if (mpin == null || mpin.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid MPIN.");
				return cMERespBean;
			}

			String hashMpin = new WalletUserDaoImpl().getMpin(mobileNo, aggreatorId);
			
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|hashMpin"+hashMpin
							);
					
					//logger.info(serverName + "**************************************validateMPIN*****hashMpin*******" + hashMpin);
			if (hashMpin == null || hashMpin.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid MPIN.");
			} else {
				if (CommanUtil.SHAHashing256(mpin).equals(hashMpin)) {
					cMERespBean.setStatusCode("1000");
					cMERespBean.setStatusMsg("MPIN Validate");
				} else {
					cMERespBean.setStatusCode("0");
					cMERespBean.setStatusMsg("Invalid MPIN.");
				}
			}
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|problem in validateMPIN"+ e.getMessage()+" "+e
					);
			//logger.debug(serverName + "problem in validateMPIN==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		return cMERespBean;
	}

	
	public CMERespBean changeMPIN(String serverName, String mobileNo, String aggreatorId, String mpin,String chechksum,String userId,String newmpin) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|changeMPIN()"+ mobileNo
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|mpin"+mpin
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|newmpin"+newmpin
				);
		/*logger.info(serverName + "**************************************changeMPIN************" + mobileNo);
		logger.info(serverName + "**************************************changeMPIN****mpin********" + mpin);
		logger.info(serverName + "**************************************changeMPIN****mpin********" + newmpin);*/
		CMERespBean cMERespBean = new CMERespBean();
		try {

			TreeMap<String,String>  map=new TreeMap<String,String>(); 
			map.put("mobileNo",mobileNo);
	        map.put("aggreatorId",aggreatorId);
	        map.put("mpin",mpin);
	        map.put("userId",userId);
	        map.put("newmpin",newmpin);
	        map.put("CHECKSUMHASH",chechksum);
			int statusCode= WalletSecurityUtility.checkSecurity(map);

		    if(statusCode!=1000){
		    	cMERespBean.setStatusCode(""+statusCode);
				cMERespBean.setStatusMsg("Security error.");
				return cMERespBean;
		    }
			
			if (mpin == null || mpin.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid MPIN.");
				return cMERespBean;
			}
			
			String hashMpin = new WalletUserDaoImpl().getMpin(mobileNo, aggreatorId);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|hashMpin"+ hashMpin
					);
			/*logger.info(
					serverName + "**************************************changeMPIN*****hashMpin*******" + hashMpin);*/
			if (hashMpin == null || hashMpin.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid MPIN.");
			} else {
				if (CommanUtil.SHAHashing256(mpin).equals(hashMpin)) {
					String result= new WalletUserDaoImpl().changeMpin(mobileNo, aggreatorId, CommanUtil.SHAHashing256(newmpin));
					if(result!=null&&result.equalsIgnoreCase("1000")){
						cMERespBean.setStatusCode("1000");
						cMERespBean.setStatusMsg("MPIN Changed.");	
					
					}else if(result!=null&&result.equalsIgnoreCase("1001")){
						cMERespBean.setStatusCode("0");
						cMERespBean.setStatusMsg("MPIN not changed please try again.");	
					}
				} else {
					cMERespBean.setStatusCode("0");
					cMERespBean.setStatusMsg("Invalid MPIN.");
				}
			}
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|problem in validateMPIN"+ e.getMessage()+" "+e
					);
			logger.debug(serverName + "problem in validateMPIN==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		return cMERespBean;
	}
	

	public CMERespBean forgotMPIN(String serverName, String mobileNo, String aggreatorId, String mpin,String chechksum,String userId,String newmpin) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|changeMPIN()"+ mobileNo
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|otp"+mpin
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|newmpin"+newmpin
				);
		/*logger.info(serverName + "**************************************changeMPIN************" + mobileNo);
		logger.info(serverName + "**************************************changeMPIN****mpin********" + mpin);
		logger.info(serverName + "**************************************changeMPIN****mpin********" + newmpin);*/
		CMERespBean cMERespBean = new CMERespBean();
		try {

			TreeMap<String,String>  map=new TreeMap<String,String>(); 
			map.put("mobileNo",mobileNo);
	        map.put("aggreatorId",aggreatorId);
	        map.put("mpin",mpin);
	        map.put("userId",userId);
	        map.put("newmpin",newmpin);
	        map.put("CHECKSUMHASH",chechksum);
			int statusCode= WalletSecurityUtility.checkSecurity(map);

		    if(statusCode!=1000){
		    	cMERespBean.setStatusCode(""+statusCode);
				cMERespBean.setStatusMsg("Security error.");
				return cMERespBean;
		    }
			
			if (newmpin == null || newmpin.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid MPIN.");
				return cMERespBean;
			}
			
			WalletUserDao walletUserDao=new WalletUserDaoImpl();
			if( walletUserDao.validateOTP(userId, aggreatorId, mpin))
			{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "Forgot MPIN", "", serverName +  " "+mpin);
				
			 	String result= new WalletUserDaoImpl().changeMpin(mobileNo, aggreatorId, CommanUtil.SHAHashing256(newmpin));
				if(result!=null&&result.equalsIgnoreCase("1000")){
					cMERespBean.setStatusCode("1000");
					cMERespBean.setStatusMsg("MPIN Changed.");	
				
				}else if(result!=null&&result.equalsIgnoreCase("1001")){
					cMERespBean.setStatusCode("0");
					cMERespBean.setStatusMsg("MPIN not changed please try again.");	
				}
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "Forgot MPIN OTP validated", "", serverName +  " "+mpin);
			}else
			{
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Otp");		
			}
			 
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"",userId, "", "", serverName +  "|problem in validateMPIN"+ e.getMessage()+" "+e
					);
			logger.debug(serverName + "problem in validateMPIN==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		return cMERespBean;
	}
	
	
	public CMERespBean moneyTrnsfer(String serverName, CMEMoneyAgentTransferBean cMEMoneyAgentTransferBean) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|moneyTrnsfer()|"+  cMEMoneyAgentTransferBean.getAgentMobile()
				);
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();

		CMERespBean cMERespBean = new CMERespBean();
		try {
			
			
			
TreeMap<String,String>  object=new TreeMap<String,String>(); 
object.put("userId",cMEMoneyAgentTransferBean.getUserId());
object.put("walletId",cMEMoneyAgentTransferBean.getWalletId());
object.put("aggreatorId", cMEMoneyAgentTransferBean.getAggreatorId());
object.put("agentMobile",cMEMoneyAgentTransferBean.getAgentMobile());
object.put("amount",""+cMEMoneyAgentTransferBean.getAmount());
object.put("remark",cMEMoneyAgentTransferBean.getRemark());
object.put("CHECKSUMHASH",cMEMoneyAgentTransferBean.getCHECKSUMHASH());
int statusCode= WalletSecurityUtility.checkSecurity(object);

if(statusCode!=1000){
	cMERespBean.setStatusCode(""+statusCode);
	cMERespBean.setStatusMsg("Security Error.");
	return cMERespBean;
}

			SQLQuery query = session.createSQLQuery("select * from blockedcme where id=:id");
			query.setString("id", cMEMoneyAgentTransferBean.getUserId());
			
			List<String> count = query.list();
			if(count != null && count.size() > 0)
			{
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("User is temporarily blocked for Cash-out!!");
				return cMERespBean;
			}

			if (cMEMoneyAgentTransferBean.getUserId() == null || cMEMoneyAgentTransferBean.getUserId().isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Sender User ID.");
				return cMERespBean;
			}

			if (cMEMoneyAgentTransferBean.getWalletId() == null || cMEMoneyAgentTransferBean.getWalletId().isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Sender Wallet ID.");
				return cMERespBean;
			}

			if (cMEMoneyAgentTransferBean.getAggreatorId() == null
					|| cMEMoneyAgentTransferBean.getAggreatorId().isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Aggreator ID.");
				return cMERespBean;
			}

			if (cMEMoneyAgentTransferBean.getAgentMobile() == null
					|| cMEMoneyAgentTransferBean.getAgentMobile().isEmpty()
					|| !(Pattern.matches("^([6-9]{1})([0-9]{9})$", cMEMoneyAgentTransferBean.getAgentMobile()))) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Agent Mobile number.");
				return cMERespBean;
			}

			if (cMEMoneyAgentTransferBean.getAmount() <= 0) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Amount.");
				return cMERespBean;

			}

			String status = new TransactionDaoImpl().cmeToAgent(cMEMoneyAgentTransferBean.getUserId(),
					cMEMoneyAgentTransferBean.getWalletId(), cMEMoneyAgentTransferBean.getAgentMobile(),
					cMEMoneyAgentTransferBean.getAmount(), cMEMoneyAgentTransferBean.getIpImei(),
					cMEMoneyAgentTransferBean.getAggreatorId(), cMEMoneyAgentTransferBean.getAgent());

			if (status.equalsIgnoreCase("1000")) {
				cMERespBean.setStatusCode("1000");
				cMERespBean.setStatusMsg("Amount transferred Successfully.");
				CMEAgentDetails cMEAgentDetails = new CMEAgentDetails();

				
				WalletMastBean agentDtl = (WalletMastBean) new TransactionDaoImpl().getAgnDtlByUserId(
						cMEMoneyAgentTransferBean.getAgentMobile(), cMEMoneyAgentTransferBean.getAggreatorId());
				cMEAgentDetails.setId(agentDtl.getId());
				cMEAgentDetails.setTransferAmount("" + cMEMoneyAgentTransferBean.getAmount());
				cMEAgentDetails.setClosingBal("" + new TransactionDaoImpl().getWalletBalance(
						new TransactionDaoImpl().getWalletIdByUserId(cMEMoneyAgentTransferBean.getAgentMobile(),
								cMEMoneyAgentTransferBean.getAggreatorId())));
				cMEAgentDetails.setCmeWalletBal(
						new TransactionDaoImpl().getWalletBalance(cMEMoneyAgentTransferBean.getWalletId()));
				ResPayload resPayload = new ResPayload();
				resPayload.setAgentDtl(cMEAgentDetails);
				cMERespBean.setResPayload(resPayload);
				try {
					WalletMastBean cmeUser = (WalletMastBean) session.get(WalletMastBean.class,	cMEMoneyAgentTransferBean.getUserId());
					
					String smstemplet = commanUtilDao.getsmsTemplet("CMETOAGENT",cMEMoneyAgentTransferBean.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|CMETOAGENT smstemplet"+  smstemplet
							);
					//logger.info("~~~~~~~~~moneyTrnsfer~~~~~~~~~~~~CMETOAGENT smstemplet~~~~~~~~~~~~~~~~~~~````"+ smstemplet);
					//Dear CME, you have transferred Rs. <<AMOUNT>> to <<AGEN_NAME>> on <<DATE>>.Your balance is Rs. <<BAL_AMOUNT>>.
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", cmeUser.getMobileno(),smstemplet.replace("<<NAME>>", "" + cmeUser.getName()).replace("<<AMOUNT>>", "" + cMEMoneyAgentTransferBean.getAmount())
									.replace("<<AGEN_NAME>>", agentDtl.getId())
									.replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"))
//									.replace("<<BAL_AMOUNT>>","" + cMEAgentDetails.getClosingBal()),
									.replace("<<BAL_AMOUNT>>","" + new TransactionDaoImpl().getWalletBalance(cmeUser.getWalletid())),
									"SMS", cMEMoneyAgentTransferBean.getAggreatorId(), cmeUser.getWalletid(), cmeUser.getName(),"NOTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
					//Dear Agent <<AGENTID>>, value of Rs. <<AMOUNT>> has been transferred from CME <<CMEID>> on <<DATE>>.Current Bal is <<BAL>>.
					String cmetoagett = commanUtilDao.getsmsTemplet("CMETOAGENTT",cMEMoneyAgentTransferBean.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|CMETOAGENTT smstemplet"+  cmetoagett
							);
					//logger.info("~~~~~~~~~moneyTrnsfer~~~~~~~~~~~~CMETOAGENTT smstemplet~~~~~~~~~~~~~~~~~~~````"+ cmetoagett);
					smsAndMailUtility = new SmsAndMailUtility("", "", "", cMEMoneyAgentTransferBean.getAgentMobile(),cmetoagett.replace("<<AGENT>>", agentDtl.getName())
							.replace("<<AMOUNT>>", "" + cMEMoneyAgentTransferBean.getAmount())
							.replace("<<NAME>>", cmeUser.getId())
							.replace("<<DATE>>", getDate("yyyy/MM/dd HH:mm:ss"))
							.replace("<<BAL>>","" + new TransactionDaoImpl().getWalletBalance(agentDtl.getWalletid())),
							"SMS", cMEMoneyAgentTransferBean.getAggreatorId(), cmeUser.getWalletid(), cmeUser.getName(),"NOTP");
//					 t = new Thread(smsAndMailUtility);
//					 t.start();
					 
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);					
					
					
				} catch (Exception e) {
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|problem in moneyTrnsfer message send"+  e.getMessage()+" "+e
							);
					/*logger.debug(serverName + "problem in moneyTrnsfer===message send===============" + e.getMessage(),
							e);*/
				}

				return cMERespBean;

			} else if (status.equalsIgnoreCase("7032")) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Amount.");
				return cMERespBean;
			} else if (status.equalsIgnoreCase("7036")) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Money not transfer in same wallet.");
				return cMERespBean;
			} else if (status.equalsIgnoreCase("7023")) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid Agent Mobile number.");
				return cMERespBean;
			} else if (status.equalsIgnoreCase("7024")) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Insufficient wallet Balance.");
				return cMERespBean;
			} else {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("ERROR");
				return cMERespBean;
			}

		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|problem in moneyTrnsfe"+  e.getMessage()+" "+e
					);
			//logger.debug(serverName + "problem in moneyTrnsfer==================" + e.getMessage(), e);
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		return cMERespBean;

	}

	public CMERespBean getCMEAccountStsList(String serverName, String walletId, String stDate, String endDate) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|getCMEAccountStsList()"
				);
		/*logger.info(serverName
				+ "start excution ******************************************************** method getCMEAccountStsList()"
				+ walletId);*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<CMEAccountStsBean> cMEAccountStsList = null;
		CMERespBean cMERespBean = new CMERespBean();

		try {

			if (walletId == null || walletId.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid wallet Id.");
				return cMERespBean;
			}

			/*
			 * StringBuilder serchQuery=new StringBuilder(); serchQuery.
			 * append("SELECT DATE_FORMAT(txndate, '%d/%m/%Y %h:%m:%s')  AS txndate,txnid,txncredit,txndebit,payeedtl,closingbal,txndesc FROM passbook WHERE walletid=:walletId"
			 * ); if(stDate!=null&&!stDate.isEmpty()
			 * &&endDate!=null&&!endDate.isEmpty()){ serchQuery.
			 * append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"
			 * +formate.format(formatter.parse(stDate))
			 * +"', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"+formate.
			 * format(formatter.parse(endDate))+"', '/', '-'),'%d-%m-%Y')"); }
			 *//*****************
				 * start changes by ambuj singh
				 *************************//*
										 * else{ //serchQuery.
										 * append(" ORDER BY DATE(txndate) DESC LIMIT 50"
										 * ); serchQuery.
										 * append(" ORDER BY STR_TO_DATE(txndate, '%Y-%m-%d %H:%i:%s ')  DESC LIMIT 50"
										 * ); }
										 */

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(txndate, '%d/%m/%Y %H:%i:%s')  AS txndate,txnid,txncredit,txndebit,payeedtl,closingbal,txndesc,b.name name FROM passbook a,walletmast b WHERE a.payeedtl=b.id and a.walletid=:walletId");
			if (stDate != null && !stDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
				serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate)) + "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate))
						+ "', '/', '-'),'%d-%m-%Y') ORDER BY STR_TO_DATE(txndate, '%Y-%m-%d %H:%i:%s ')");
			}
			/*****************
			 * start changes by ambuj singh
			 *************************/
			else {

				serchQuery.append(" ORDER BY STR_TO_DATE(txndate, '%Y-%m-%d %H:%i:%s ')  DESC LIMIT 50");
			}
			/*****************
			 * end changes by ambuj singh
			 *************************/

			/*****************
			 * end changes by ambuj singh
			 *************************/
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("walletId", walletId);

			query/*
					 * .addScalar("txndate").addScalar("txnid").addScalar(
					 * "txncredit") .addScalar("txndebit")
					 * .addScalar("payeedtl") .addScalar("closingbal")
					 * .addScalar("txndesc")
					 */

					.setResultTransformer(Transformers.aliasToBean(CMEAccountStsBean.class));
			cMEAccountStsList = query.list();
			cMERespBean.setStatusCode("1000");
			cMERespBean.setStatusMsg("SUCCESS.");
			ResPayload resPayload = new ResPayload();
			resPayload.setcMEAccountStsList(cMEAccountStsList);
			cMERespBean.setResPayload(resPayload);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|problem in getCMEAccountStsList"+e.getMessage()+" "+e
					);
			/*logger.debug(serverName + "problem in getCMEAccountStsList*****************************************"
					+ e.getMessage(), e);*/
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("Error.");
			e.printStackTrace();

		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|return"+ cMEAccountStsList.size()
				);
		/*logger.info(serverName
				+ " **********************return ***********getCMEAccountStsList***********************************************"
				+ cMEAccountStsList.size());*/
		return cMERespBean;
	}

	
	
	public CMERespBean getMovementReportBySupDist(String serverName, String walletId, String stDate, String endDate) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|getMovementReportBySupDist()"
				);
		/*logger.info(serverName
				+ "start excution ******************************************************** method getMovementReportBySupDist()"
				+ walletId);*/
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		List<CMEAccountStsBean> cMEAccountStsList = null;
		CMERespBean cMERespBean = new CMERespBean();
        System.out.println(walletId+"  "+stDate+"  "+endDate);
		try {

			if (walletId == null || walletId.isEmpty()) {
				cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Invalid wallet Id.");
				return cMERespBean;
			}

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append(
					"SELECT DATE_FORMAT(txndate, '%d/%m/%Y %H:%i:%s')  AS txndate,txnid,txncredit,txndebit,payeedtl,closingbal,txndesc,b.name name FROM passbook a,walletmast b WHERE a.payeedtl=b.id and a.walletid=:walletId");
			if (stDate != null && !stDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
				serchQuery.append(" and DATE(txndate) BETWEEN   STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(stDate)) + "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
						+ formate.format(formatter.parse(endDate))
						+ "', '/', '-'),'%d-%m-%Y') ORDER BY STR_TO_DATE(txndate, '%Y-%m-%d %H:%i:%s ')");
			}
			
			else {

				serchQuery.append(" ORDER BY STR_TO_DATE(txndate, '%Y-%m-%d %H:%i:%s ')  DESC LIMIT 50");
			}
			
			SQLQuery query = session.createSQLQuery(serchQuery.toString());
			query.setString("walletId", walletId);

			query.setResultTransformer(Transformers.aliasToBean(CMEAccountStsBean.class));
			cMEAccountStsList = query.list();
			cMERespBean.setStatusCode("1000");
			cMERespBean.setStatusMsg("SUCCESS.");
			ResPayload resPayload = new ResPayload();
			resPayload.setcMEAccountStsList(cMEAccountStsList);
			cMERespBean.setResPayload(resPayload);
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|problem in getMovementReportBySupDist()"+ e.getMessage()+" "+e
					);
			/*logger.debug(serverName + "problem in getMovementReportBySupDist*****************************************"
					+ e.getMessage(), e);*/
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("Error.");
			e.printStackTrace();

		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"",walletId,"", "", "", serverName +  "|return"+ cMEAccountStsList.size()
				);
		/*logger.info(serverName
				+ " **********************return ***********getMovementReportBySupDist***********************************************"
				+ cMEAccountStsList.size());*/
		return cMERespBean;
	}
	
	// added for CME Checker
	public List<CMERequestBean> getCMECheckerListByAggId(String aggreatorId, String status, String stDate,
			String endDate) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|getCMECheckerListByAggId()"
				);
		//logger.info("start excution ===================== method getCMECheckerListByAggId(aggreatorId)" + aggreatorId);
		List<CMERequestBean> cmeRequestList = new ArrayList<CMERequestBean>();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		try {
			//String stmt = "from CMERequestBean where aggreatorId=:aggreatorId and status='PENDING' and checkerstatus='PENDING' and approverstatus='PENDING'";
			String stmt = "from CMERequestBean where aggreatorId=:aggreatorId ";
			if (status.trim().equalsIgnoreCase("ALL")) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|stDate"
						);
				//logger.info("stDate **********************************************" + aggreatorId);
				if (stDate != null && !stDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
					stmt = stmt + " and DATE(reqestdate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(stDate))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')";
				} else {

					stmt = stmt + "  ";

				}

			} else {
				stmt = stmt + "and status='" + status.trim() + "' ";
			}

			stmt = stmt + "order by cmeRequestid DESC";
			Query query = session.createQuery(stmt);
			query.setString("aggreatorId", aggreatorId);
			cmeRequestList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|problem in getCMECheckerListByAggId"+e.getMessage()+" "+e
					);
			/*logger.debug(
					"problem in getCMECheckerListByAggId***********************************************************"
							+ e.getMessage(),
					e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|return"+ cmeRequestList.size()
				);
		/*logger.info("stop excution ********************************getCashDepositReqByUserId******* return**********"
				+ cmeRequestList.size());*/
		return cmeRequestList;
	}

	public boolean acceptCMERequest(String RequestId, String remarksReason, String ipIemi, String agent) {
		
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tr = session.beginTransaction();
		boolean flag = false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|acceptCMERequest()|"+ agent +"|RequestId"+RequestId
				);
		try {
			CMERequestBean cmeRequest = (CMERequestBean) session.get(CMERequestBean.class, RequestId);
			// String
			// status=transactionDao.cashDepositSettlement(cmeRequest.getUserId(),cmeRequest.getWalletId(),cmeRequest.getAmount(),cmeRequest.getCmeRequestid(),ipIemi,walletUserDao.showUserProfile(cmeRequest.getUserId()).getMobileno(),cmeRequest.getAggreatorId(),agent);
			// if(status.equals("1000")){
			cmeRequest.setStatus("ACCEPTED");
			cmeRequest.setCheckerStatus("ACCEPTED");
			cmeRequest.setRemarksReason(remarksReason);
			session.saveOrUpdate(cmeRequest);
			tr.commit();
			flag = true;
			// DDear CME, Your limit Replenishment request with <<TXNID>> of Rs.
			// <<AMOUNT>> has been accepted by Bhartipay.
			/*
			 * try{ WalletMastBean
			 * walletMastBean=(WalletMastBean)session.get(WalletMastBean.class,
			 * cmeRequest.getUserId()); String smstemplet =
			 * commanUtilDao.getsmsTemplet("CMELIMITREPL",cmeRequest.
			 * getAggreatorId()); logger.
			 * info("~~~~~~~~~acceptCMERequest~~~~~~~~~~~~CMERequest smstemplet~~~~~~~~~~~~~~~~~~~````"
			 * + smstemplet); SmsAndMailUtility smsAndMailUtility = new
			 * SmsAndMailUtility("", "","", walletMastBean.getMobileno(),
			 * smstemplet.replaceAll("<<TXNID>",
			 * RequestId).replaceAll("<<AMOUNT>>",
			 * cmeRequest.getAmount().toString()),
			 * "SMS",cmeRequest.getAggreatorId(),walletMastBean.getWalletid(),
			 * walletMastBean.getName(),"NOTP"); Thread t = new
			 * Thread(smsAndMailUtility); t.start(); } catch (Exception e) {
			 * e.printStackTrace(); logger.
			 * debug("problem in acceptCMERequest===message send==============="
			 * + e.getMessage(), e);
			 * 
			 * }
			 */

			// }
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in acceptCMERequest"+ e.getMessage()+" "+e
					);
			/*logger.info("problem in acceptCMERequest***********************************************************"
					+ e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|return"+ flag 
				);
		//logger.info("stop excution ********************************acceptCMERequest******* return**********");
		return flag;
	}

	public boolean rejectCMERequest(String RequestId, String remarksReason) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tr = session.beginTransaction();
		boolean flag = false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|rejectCMERequest() |RequestId"+RequestId
				);
		try {
			CMERequestBean cmeRequest = (CMERequestBean) session.get(CMERequestBean.class, RequestId);
			cmeRequest.setStatus("REJECTED");
			cmeRequest.setCheckerStatus("REJECTED");
			cmeRequest.setRemarksReason(remarksReason);
			session.saveOrUpdate(cmeRequest);
			tr.commit();
			flag = true;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in rejectCMERequest"+e.getMessage()+" "+e
					);
		/*	logger.info("problem in rejectCMERequest***********************************************************"
					+ e.getMessage(), e);*/
			tr.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|return"+flag
				);
		//logger.info("stop excution ********************************rejectCMERequest******* return**********");
		return flag;
	}

	public Map<String, String> getCMEUserList() {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|getCMEUserList() "
				);
		//logger.info("start *********************************getCMEUserList*************");
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Map<String, String> cmeList = new HashMap<String, String>();
		try {

			Query query = session.createQuery("from UserRoleMapping");
			List<UserRoleMapping> list = query.list();
			if (list != null && list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					String roleId = list.get(i).getRoleId();
					if (roleId.contains("9004")) {
						String cmeUserId = list.get(i).getUserId();
						cmeList.put(cmeUserId, cmeUserId);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in getCMEUserList "+e.getMessage()+" "+e
					);
			/*logger.info("problem in getCMEUserList***********************************************************"
					+ e.getMessage(), e);*/
		} finally {
			session.close();
		}
		return cmeList;
	}

	// public String cmeToAgent(String userId,String sendWalletid,String
	// recMobile,double amount,String ipImei,String aggreatorid,String agent);
	private String getDate(String format) {
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat df = new SimpleDateFormat(format); // Quoted "Z" to indicate
														// UTC, no timezone
														// offset
		df.setTimeZone(tz);
		return df.format(new Date());
	}

	// added for CME Approver
	public List<CMERequestBean> getCMEApproveListByAggId(String aggreatorId, String status, String stDate,	String endDate) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|getCMEApproveListByAggId() "
				);
		//logger.info("start excution ===================== method getCMEApproveListByAggId(aggreatorId)" + aggreatorId);
		List<CMERequestBean> cmeRequestList = new ArrayList<CMERequestBean>();
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		try {
			String stmt = "from CMERequestBean where aggreatorId=:aggreatorId and status='ACCEPTED' and checkerstatus='ACCEPTED' and approverstatus='PENDING'";
			/*if (status.trim().equalsIgnoreCase("ALL")) {
				logger.info("stDate **********************************************" + aggreatorId);
				if (stDate != null && !stDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
					stmt = stmt + " and DATE(reqestdate) BETWEEN   STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(stDate))
							+ "', '/', '-'),'%d-%m-%Y') AND STR_TO_DATE(REPLACE('"
							+ formate.format(formatter.parse(endDate)) + "', '/', '-'),'%d-%m-%Y')";
				} else {

					stmt = stmt + "  ";

				}

			} else {
				stmt = stmt + "and status='" + status.trim() + "' ";
			}*/

			stmt = stmt + "order by cmeRequestid DESC";
			Query query = session.createQuery(stmt);
			query.setString("aggreatorId", aggreatorId);
			cmeRequestList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|problem in getCMEApproveListByAggId"+e.getMessage()+" "+e
					);
		/*	logger.debug(
					"problem in getCMEApproveListByAggId***********************************************************"
							+ e.getMessage(),
					e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "",   "|return "+ cmeRequestList.size()
				);
	/*	logger.info("stop excution ********************************getCMEApproveListByAggId******* return**********"
				+ cmeRequestList.size());*/
		return cmeRequestList;
	}

	public boolean approveCMERequest(String RequestId, String remarkApprover, String ipIemi, String agent) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tr = session.beginTransaction();
		boolean flag = false;
		try {
			CMERequestBean cmeRequest = (CMERequestBean) session.get(CMERequestBean.class, RequestId);
			String status = transactionDao.cashDepositSettlement(cmeRequest.getUserId(), cmeRequest.getWalletId(),
					cmeRequest.getAmount(), cmeRequest.getCmeRequestid(), ipIemi,
					walletUserDao.showUserProfile(cmeRequest.getUserId()).getMobileno(), cmeRequest.getAggreatorId(),
					agent);
			if (status.equals("1000")) {
				cmeRequest.setStatus("APPROVED");
				cmeRequest.setApproverStatus("ACCEPTED");
				cmeRequest.setRemarkApprover(remarkApprover);
				session.saveOrUpdate(cmeRequest);
				tr.commit();
				flag = true;
				// DDear CME, Your limit Replenishment request with <<TXNID>> of
				// Rs. <<AMOUNT>> has been accepted by Bhartipay.
				try {
					WalletMastBean walletMastBean = (WalletMastBean) session.get(WalletMastBean.class,
							cmeRequest.getUserId());
					String smstemplet = commanUtilDao.getsmsTemplet("CMELIMITREPL", cmeRequest.getAggreatorId());
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|approveCMERequest()|smstemplet "+ smstemplet
							);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|RequestId "+RequestId +"agent "+agent
							);
					/*logger.info("~~~~~~~~~approveCMERequest~~~~~~~~~~~~CMERequest smstemplet~~~~~~~~~~~~~~~~~~~````"
							+ smstemplet);*/
					SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "",
							walletMastBean.getMobileno(),
							smstemplet.replaceAll("<<NAME>", walletMastBean.getName()).replaceAll("<<AMOUNT>>",
									cmeRequest.getAmount().toString()),
							"SMS", cmeRequest.getAggreatorId(), walletMastBean.getWalletid(), walletMastBean.getName(),
							"NOTP");
//					Thread t = new Thread(smsAndMailUtility);
//					t.start();
					ThreadUtil.getThreadPool().execute(smsAndMailUtility);
				} catch (Exception e) {
					e.printStackTrace();
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in approveCMERequest message send"+e.getMessage()+" "+e
							);
					//logger.debug("problem in approveCMERequest===message send===============" + e.getMessage(), e);

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in approveCMERequest"+e.getMessage()+" "+e
					);
			/*logger.info("problem in approveCMERequest***********************************************************"
					+ e.getMessage(), e);*/
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|return"
				);
		//logger.info("stop excution ********************************approveCMERequest******* return**********");
		return flag;
	}

	public boolean disapproverCMERequest(String RequestId, String remarkApprover) {
		factory = DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tr = session.beginTransaction();
		boolean flag = false;
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|disapproverCMERequest()|RequestId "+RequestId
				);
		try {
			CMERequestBean cmeRequest = (CMERequestBean) session.get(CMERequestBean.class, RequestId);
			cmeRequest.setStatus("REJECTED");
			cmeRequest.setApproverStatus("REJECTED");
			cmeRequest.setRemarkApprover(remarkApprover);
			session.saveOrUpdate(cmeRequest);
			tr.commit();
			flag = true;
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|problem in disapproverCMERequest"+e.getMessage()+" "+e
					);
			/*logger.info("problem in disapproverCMERequest***********************************************************"
					+ e.getMessage(), e);*/
			tr.rollback();
		} finally {
			session.close();
		}
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|return"
				);
		//logger.info("stop excution ********************************rejectCMERequest******* return**********");
		return flag;
	}

	
	public CMERespBean getAgnDtlByUserIdSo(String serverName, String mobileNo, String aggreatorId, String distributorId,String type,String userId) {
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|getAgnDtlByUserId()"+ mobileNo);
		CMERespBean cMERespBean = new CMERespBean();
		CMEAgentDetails cMEAgentDetails = null;
		try {

			WalletMastBean agentDtl = (WalletMastBean) new TransactionDaoImpl().getAgnDtlByUserIdCMENew(mobileNo,aggreatorId, distributorId,type,userId);

			if (agentDtl == null) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id does not exist"+ mobileNo);
	 			cMERespBean.setStatusCode("0");
				cMERespBean.setStatusMsg("Agent Id does not exist");
			} else {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|Agent Id exist");
 				cMEAgentDetails = new CMEAgentDetails();
				cMEAgentDetails.setId(agentDtl.getId());
				cMEAgentDetails.setMobileno(agentDtl.getMobileno());
				cMEAgentDetails.setEmailid(agentDtl.getEmailid());
				cMEAgentDetails.setName(agentDtl.getName());
				cMEAgentDetails.setAddress1(agentDtl.getAddress1());
				cMEAgentDetails.setAddress2(agentDtl.getAddress2());
				cMEAgentDetails.setCity(agentDtl.getCity());
				cMEAgentDetails.setState(agentDtl.getState());
				cMEAgentDetails.setPin(agentDtl.getPin());
				/****************
				 * start change by ambuj singh
				 *******************/
				cMEAgentDetails.setAvailableLimit(0);
				cMEAgentDetails.setAssignedLimit(0);

				cMEAgentDetails.setClosingBal("" + new TransactionDaoImpl()
						.getWalletBalance(new TransactionDaoImpl().getWalletIdByUserId(mobileNo, aggreatorId)));

				/**************** end change by ambuj singh *******************/
				cMERespBean.setStatusCode("1000");
				cMERespBean.setStatusMsg("SUCCESS");
				ResPayload resPayload = new ResPayload();
				resPayload.setAgentDtl(cMEAgentDetails);
				cMERespBean.setResPayload(resPayload);
 			}
		} catch (Exception e) {
			cMERespBean.setStatusCode("0");
			cMERespBean.setStatusMsg("FAILED");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorId,"","", "", "", serverName +  "|problem in getAgnDtlByUserId"+ e.getMessage()+" "+e);
	 		e.printStackTrace();
		}
		return cMERespBean;

	}
	
	
	
	public static void main(String[] args) {
		CMERequestBean cMERequestBean = new CMERequestBean();
		cMERequestBean.setAggreatorId("AGGR001035");
		cMERequestBean.setUserId("AGGR001035");
		// System.out.println(new CMEDaoImpl().getCMERequestList("aaa",
		// cMERequestBean));

		//System.out.println(new CMEDaoImpl().getCMEAccountStsList("aaa", "870072350524102016170940", "", ""));
	}

}
