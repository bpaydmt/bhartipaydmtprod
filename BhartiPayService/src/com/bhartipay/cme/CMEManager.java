package com.bhartipay.cme;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.cme.bean.CMEMoneyAgentTransferBean;
import com.bhartipay.cme.bean.CMERequestBean;
import com.bhartipay.cme.bean.CMERespBean;
import com.bhartipay.cme.bean.CMEResponse;
import com.bhartipay.cme.persistence.CMEDao;
import com.bhartipay.cme.persistence.CMEDaoImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



@Path("/CMEManager")
public class CMEManager {
	
	private static final Logger logger = Logger.getLogger(CMEManager.class.getName());
	CMEDao cmeDao=new CMEDaoImpl();
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	
	@POST
	@Path("/getBankDtl")
	@Produces({ MediaType.APPLICATION_JSON })
	 public String getBankDtl(@Context HttpServletRequest request,@HeaderParam("USERID") String userId){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", userId, "", "", serverName +  "|getBankDtl()"
				);
		//logger.info(serverName +"**********"+userId +"Start excution *********** method getBankDtl()");
		return cmeDao.getBankDtl(serverName,userId);
	 }
	
	
	
	
	@POST
	@Path("/getDeposittype")
	@Produces({ MediaType.APPLICATION_JSON })
	 public String getDeposittype(@Context HttpServletRequest request){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +  "|getDeposittype()"
				);
		//logger.info(serverName +"Start excution *********** method getDeposittype()");
		return cmeDao.getDeposittype(serverName);
	 }
	
	
	
	/**
	 * 
	 * @param ipIemi
	 * @param agent
	 * @param cMERequestBean
	 * @return
	 */
	
	@POST
	@Path("/saveCMERequest")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public CMEResponse saveCMERequest(@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +  "|saveCMERequest()"
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +  "|ipIemi"+ipIemi
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","", "", "", "", serverName +  "|agent"+agent
				);
		/*logger.info(serverName +"Start excution ******************************************************* method createPlan(commPlanMaster)");
		logger.info(serverName +"Start excution *********************ipIemi********************************* method createPlan(commPlanMaster)"+ipIemi);
		logger.info(serverName +"Start excution *********************agent********************************* method createPlan(commPlanMaster)"+agent);*/
		cMERequestBean.setIpIemi(ipIemi);
		cMERequestBean.setAgent(agent);
		return cmeDao.saveCMERequest(serverName,cMERequestBean);
		
	}
	
	
	@POST
	@Path("/getCMERequestList")
	@Produces({ MediaType.APPLICATION_JSON })
	 public String getCMERequestList(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getCMERequestList()"
				);
		//logger.info(serverName +"Start excution ******************************************************* method getDeposittype()");
		return cmeDao.getCMERequestList(serverName,cMERequestBean);
	 }
	
	
	@POST
	@Path("/getDMTBalance")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getDMTBalance(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getdmtBalance()"
				);
		//logger.info(serverName +"Start excution ******************************************************* method getdmtBalance()");
		return cmeDao.getDMTBalance(serverName,cMERequestBean.getWalletId());
	}
	
	/**
	 * 
	 * @param request
	 * @param cMERequestBean
	 * @return
	 */
	@POST
	@Path("/getAgnDtlByUserId")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean getAgnDtlByUserId(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getAgnDtlByUserId()"
				);
		//logger.info(serverName +"Start excution ******************************************************* method getAgnDtlByUserId()");
		
		if(cMERequestBean.getDistributerId()==null ||cMERequestBean.getDistributerId().isEmpty()){
			return cmeDao.getAgnDtlByUserId(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),"-1");
		}else{
			return cmeDao.getAgnDtlByUserId(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getDistributerId());
		}
		
		
	}
	
	@POST
	@Path("/getDistributorDtlByUserId")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean getDistributorDtlByUserId(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getDistributorDtlByUserId()"
				);
		//logger.info(serverName +"Start excution ******************************************************* method getDistributorDtlByUserId()");
		
			return cmeDao.getDistributorDtlByUserId(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getDistributerId());
		
		
	}
	
	/**
	 * 
	 * @param request
	 * @param cMERequestBean
	 * @return
	 */
	@POST
	@Path("/validateMPIN")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean validateMPIN(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|validateMPIN()"+cMERequestBean.getMpin()
				);
		
		//logger.info(serverName +"Start excution ******************************************************* method validateMPIN()"+cMERequestBean.getMpin());
		return cmeDao.validateMPIN(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getMpin(),cMERequestBean.getCHECKSUMHASH(),cMERequestBean.getUserId());

		
	}
	
	
	/**
	 * 
	 * @param request
	 * @param cMERequestBean
	 * @return
	 */
	@POST
	@Path("/changeMPIN")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean changeMPIN(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|changeMPIN()"+cMERequestBean.getNewmpin()
				);
		//logger.info(serverName +"Start excution ******************************************************* method changeMPIN()"+cMERequestBean.getMpin());
		return cmeDao.changeMPIN(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getMpin(),cMERequestBean.getCHECKSUMHASH(),cMERequestBean.getUserId(),cMERequestBean.getNewmpin());

		
	}
	

	@POST
	@Path("/forgotMPIN")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean forgotMPIN(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|changeMPIN()"+cMERequestBean.getNewmpin()
				);
		//logger.info(serverName +"Start excution ******************************************************* method changeMPIN()"+cMERequestBean.getMpin());
		return cmeDao.forgotMPIN(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getMpin(),cMERequestBean.getCHECKSUMHASH(),cMERequestBean.getUserId(),cMERequestBean.getNewmpin());

		
	}
	
	
/**
 * 
 * @param request
 * @param ipIemi
 * @param agent
 * @param cMEMoneyAgentTransferBean
 * @return
 */
	@POST
	@Path("/moneyTrnsfer")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean moneyTrnsfer(@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CMEMoneyAgentTransferBean cMEMoneyAgentTransferBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|moneyTrnsfer()"
				);
		//logger.info(serverName +"Start excution ******************************************************* method moneyTrnsfer()");
		cMEMoneyAgentTransferBean.setIpImei(ipIemi);
		cMEMoneyAgentTransferBean.setAgent(agent);
		return cmeDao.moneyTrnsfer(serverName,cMEMoneyAgentTransferBean);
		
	}
	
	/**
	 * 
	 * @param request
	 * @param cMEMoneyAgentTransferBean
	 * @return
	 */
	@POST
	@Path("/getCMEAccountStsList")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean getCMEAccountStsList(@Context HttpServletRequest request,CMEMoneyAgentTransferBean cMEMoneyAgentTransferBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  "|getCMEAccountStsList()"
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName   +cMEMoneyAgentTransferBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName +  cMEMoneyAgentTransferBean.getEndDate()
				);
		/*logger.info(serverName +"Start excution ******************************************************* method getCMEAccountStsList()");
		logger.info(serverName +"Start excution ******************************************************* method getCMEAccountStsList()"+cMEMoneyAgentTransferBean.getStDate());
		logger.info(serverName +"Start excution ******************************************************* method getCMEAccountStsList()"+cMEMoneyAgentTransferBean.getEndDate());
	*/	return cmeDao.getCMEAccountStsList(serverName,cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getStDate(),cMEMoneyAgentTransferBean.getEndDate());
		
	}
	
	
	
	@POST
	@Path("/getMovementReportBySupDist")
	@Produces({ MediaType.APPLICATION_JSON })
	public CMERespBean getMovementReportBySupDist(@Context HttpServletRequest request,CMEMoneyAgentTransferBean cMEMoneyAgentTransferBean){
		String serverName=request.getServletContext().getInitParameter("serverName");
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName + "|getMovementReportBySupDist()|"+ cMEMoneyAgentTransferBean.getStDate()
				);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMEMoneyAgentTransferBean.getAggreatorId(),cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getUserId(), "", "", serverName + "|getMovementReportBySupDist()|"+ cMEMoneyAgentTransferBean.getEndDate()
				);
/*		logger.info(serverName +"Start excution ******************************************************* method getMovementReportBySupDist()");
		logger.info(serverName +"Start excution ******************************************************* method getMovementReportBySupDist()"+cMEMoneyAgentTransferBean.getStDate());
		logger.info(serverName +"Start excution ******************************************************* method getMovementReportBySupDist()"+cMEMoneyAgentTransferBean.getEndDate());*/
		return cmeDao.getMovementReportBySupDist(serverName,cMEMoneyAgentTransferBean.getWalletId(),cMEMoneyAgentTransferBean.getStDate(),cMEMoneyAgentTransferBean.getEndDate());
		
	}
	//added for CME checker 
	 /**
	  * 
	  * @param request
	  * @param cMEMoneyAgentTransferBean
	  * @return
	  */
	 @POST
	 @Path("/getCMECheckerListByAggId")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String getCMECheckerListByAggId(CMERequestBean cmeRequest){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|getCMECheckerListByAggId()|" +cmeRequest.getUserId()
					);
	 // logger.info("Start excution ===================== method getCMECheckerListByAggId(CMERequestBean)"+cmeRequest.getUserId());
	  GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(cmeDao.getCMECheckerListByAggId( cmeRequest.getAggreatorId(),cmeRequest.getStatus(),cmeRequest.getStDate(),cmeRequest.getEndDate()));
	 }
	 
	 @POST
	 @Path("/acceptCMERequest")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String acceptCashDeposit(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CMERequestBean cmeRequest){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|acceptCashDeposit()|" +cmeRequest.getCmeRequestid()
					);
		// logger.info("Start excution ************************************ method acceptCMERequest(CMERequestBean)"+cmeRequest.getCmeRequestid());
	  return ""+cmeDao.acceptCMERequest(cmeRequest.getCmeRequestid(),cmeRequest.getRemarksReason(),ipIemi,agent);
	  }
	 
	 
	 @POST
	 @Path("/rejectCMERequest")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String rejectCashDeposit(CMERequestBean cmeRequest){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|rejectCashDeposit()|" +cmeRequest.getCmeRequestid()
					);
	 // logger.info("Start excution ************************************ method rejectCMERequest(CMERequestBean)"+cmeRequest.getCmeRequestid());
	  return ""+cmeDao.rejectCMERequest(cmeRequest.getCmeRequestid(),cmeRequest.getRemarksReason());
	  }
	
	 
	 @POST
	 @Path("/getCMEUserList")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public String getCMEUserList(){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", "",   "|getCMEUserList()" 
					);
	//logger.info("Start excution ===================== method getCMEUserList()");
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    return gson.toJson(cmeDao.getCMEUserList());
			
	}
	
	
	 
	 
	 
	 
	 @POST
	 @Path("/getCMEApproveListByAggId")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String getCMEApproveListByAggId(CMERequestBean cmeRequest){
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|getCMEApproveListByAggId()|"+cmeRequest.getUserId()
					);
	  //logger.info("Start excution ===================== method getCMEApproveListByAggId(CMERequestBean)"+cmeRequest.getUserId());
	  GsonBuilder builder = new GsonBuilder();
	     Gson gson = builder.create();
	     return gson.toJson(cmeDao.getCMEApproveListByAggId( cmeRequest.getAggreatorId(),cmeRequest.getStatus(),cmeRequest.getStDate(),cmeRequest.getEndDate()));
	 }
	 
	 @POST
	 @Path("/approveCMERequest")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String approveCMERequest(@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent,CMERequestBean cmeRequest){
	 
		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|approveCMERequest()|"+cmeRequest.getCmeRequestid()
					);
		 //logger.info("Start excution ************************************ method approveCMERequest(CMERequestBean)"+cmeRequest.getCmeRequestid());
	  return ""+cmeDao.approveCMERequest(cmeRequest.getCmeRequestid(),cmeRequest.getRemarkApprover(),ipIemi,agent);
	  }
	 
	 
	 @POST
	 @Path("/disapproverCMERequest")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 @Consumes(MediaType.APPLICATION_JSON)
	 public String disapproverCMERequest(CMERequestBean cmeRequest){

		 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cmeRequest.getAggreatorId(),cmeRequest.getWalletId(),cmeRequest.getUserId(), "", "",   "|disapproverCMERequest()|"+cmeRequest.getCmeRequestid()
					);
	 // logger.info("Start excution ************************************ method disapproverCMERequest(CMERequestBean)"+cmeRequest.getCmeRequestid());
	  return ""+cmeDao.disapproverCMERequest(cmeRequest.getCmeRequestid(),cmeRequest.getRemarkApprover());
	  }
	 
	 
	 @POST
		@Path("/getAgnDtlByUserIdSo")
		@Produces({ MediaType.APPLICATION_JSON })
		public CMERespBean getAgnDtlByUserIdSo(@Context HttpServletRequest request,CMERequestBean cMERequestBean){
			String serverName=request.getServletContext().getInitParameter("serverName");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),cMERequestBean.getAggreatorId(),cMERequestBean.getWalletId(),cMERequestBean.getUserId(), "", "", serverName +  "|getAgnDtlByUserId()");
	        
	        return cmeDao.getAgnDtlByUserIdSo(serverName,cMERequestBean.getMobileNo(),cMERequestBean.getAggreatorId(),cMERequestBean.getDistributerId(),cMERequestBean.getTxnRefNo(),cMERequestBean.getUserId());  	 
		}
	 
	
}
