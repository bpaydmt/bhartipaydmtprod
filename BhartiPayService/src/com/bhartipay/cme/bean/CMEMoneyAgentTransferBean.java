package com.bhartipay.cme.bean;

import java.io.Serializable;

public class CMEMoneyAgentTransferBean implements Serializable{
	
	private String userId;
	private String walletId;
	private String agentMobile;
	private double amount;
	private String ipImei;
	private String aggreatorId;
	private String agent;
	private String remark;
	
	private String stDate;
	private String endDate;
	private String agentId;
	private String CHECKSUMHASH;
	
	
	
	
	
	
		
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}
	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getAgentMobile() {
		return agentMobile;
	}
	public void setAgentMobile(String agentMobile) {
		this.agentMobile = agentMobile;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getIpImei() {
		return ipImei;
	}
	public void setIpImei(String ipImei) {
		this.ipImei = ipImei;
	}
	
	
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	

}
