package com.bhartipay.cme.bean;

public class CMERespBean {
	
	private String statusCode;
	private String statusMsg;
	private ResPayload resPayload;
	
	
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public ResPayload getResPayload() {
		return resPayload;
	}
	public void setResPayload(ResPayload resPayload) {
		this.resPayload = resPayload;
	}
	
	
	

}
