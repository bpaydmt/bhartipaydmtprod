package com.bhartipay.cme.bean;

import java.util.List;

public class ResPayload {
	
	private CMEAgentDetails agentDtl;
	
	private List<CMEAccountStsBean> cMEAccountStsList=null;

	public CMEAgentDetails getAgentDtl() {
		return agentDtl;
	}

	public void setAgentDtl(CMEAgentDetails agentDtl) {
		this.agentDtl = agentDtl;
	}

	public List<CMEAccountStsBean> getcMEAccountStsList() {
		return cMEAccountStsList;
	}

	public void setcMEAccountStsList(List<CMEAccountStsBean> cMEAccountStsList) {
		this.cMEAccountStsList = cMEAccountStsList;
	}
	
	
	
	
	

}
