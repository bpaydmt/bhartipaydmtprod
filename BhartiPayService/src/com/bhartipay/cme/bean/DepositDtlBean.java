package com.bhartipay.cme.bean;

public class DepositDtlBean {
	
	private String depositeId;
	private String depositeType;
	public String getDepositeId() {
		return depositeId;
	}
	public void setDepositeId(String depositeId) {
		this.depositeId = depositeId;
	}
	public String getDepositeType() {
		return depositeType;
	}
	public void setDepositeType(String depositeType) {
		this.depositeType = depositeType;
	}
	
	
	

}
