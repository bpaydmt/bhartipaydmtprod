package com.bhartipay.cme.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="cmerequestmast")
public class CMERequestBean implements Serializable{
	
	
	@Id
	@Column (name = "requestid",updatable = false,nullable = false,  length=20)
	private String cmeRequestid;
	
	@Column(name="aggreatorid", length=100, nullable = true)
	private String aggreatorId;
	
	@Column( name = "userid", length=100, nullable = true)
	private String userId;
	
	@Column( name = "walletid", length=25, nullable = true)
	private String walletId;
	
	@Column(name = "amount", nullable = false, length=10)
	private Double amount;
	
	
	@Column( name = "doctype", length=25, nullable = true)
	private String docType;
	
	@Column( name = "txnrefno", length=25, nullable = true)
	private String txnRefNo;
	
	@Column( name = "bankname", length=50)
	private String bankName;
	
	
	
	
	@Column( name = "branchname", length=250)
	private String branchName;
	
	@Column( name = "depositedate", length=50)
	private String depositeDate;
	
	
	@Column( name = "remarks", length=250)
	private String remarks;
	
	@Column(name = "reciptdoc", length = 100000)
    private String reciptDoc;
	
	@Column( name = "status", length=50)
	private String status;
	
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date reqestdate;
	
	@Column(name="ipiemi", length=50)
	private String ipIemi;
	
	@Column(name="agent" ,length = 500)
	private String agent;
	
	
	//added for CME checker
	 @Column( name = "remarksReason", length=250)
	 private String remarksReason;
	
	 
	 
	 @Column( name = "remarkapprover", length=250)
	private String remarkApprover;
		
		
	@Column( name = "checkerstatus", length=50)
		private String checkerStatus;
		
	@Column( name = "approverstatus", length=50)
	private String approverStatus;
	 
	 
	 
	@Transient
	private String stDate;
	
	@Transient
	private String endDate;
	
	@Transient
	private String mobileNo;
	
	@Transient
	private String mpin;
	@Transient
	private String newmpin;
	
	@Transient
	private String distributerId;
	@Transient
	private String CHECKSUMHASH;
	
	

	public String getNewmpin() {
		return newmpin;
	}

	public void setNewmpin(String newmpin) {
		this.newmpin = newmpin;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getRemarkApprover() {
		return remarkApprover;
	}

	public void setRemarkApprover(String remarkApprover) {
		this.remarkApprover = remarkApprover;
	}

	public String getCheckerStatus() {
		return checkerStatus;
	}

	public void setCheckerStatus(String checkerStatus) {
		this.checkerStatus = checkerStatus;
	}

	public String getApproverStatus() {
		return approverStatus;
	}

	public void setApproverStatus(String approverStatus) {
		this.approverStatus = approverStatus;
	}

	public String getDistributerId() {
		return distributerId;
	}

	public void setDistributerId(String distributerId) {
		this.distributerId = distributerId;
	}

	public String getRemarksReason() {
		return remarksReason;
	}

	public void setRemarksReason(String remarksReason) {
		this.remarksReason = remarksReason;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCmeRequestid() {
		return cmeRequestid;
	}

	public void setCmeRequestid(String cmeRequestid) {
		this.cmeRequestid = cmeRequestid;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getTxnRefNo() {
		return txnRefNo;
	}

	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getDepositeDate() {
		return depositeDate;
	}

	public void setDepositeDate(String depositeDate) {
		this.depositeDate = depositeDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReciptDoc() {
		return reciptDoc;
	}

	public void setReciptDoc(String reciptDoc) {
		this.reciptDoc = reciptDoc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public Date getReqestdate() {
		return reqestdate;
	}

	public void setReqestdate(Date reqestdate) {
		this.reqestdate = reqestdate;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}



	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
	
	

}
