package com.bhartipay.cme.bean;

import javax.persistence.Transient;

public class CMEResponse {
	
	
	private String statusCode;
	private String statusMsg;
	private PayLoadBean resPayload=new PayLoadBean();
	
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public PayLoadBean getResPayload() {
		return resPayload;
	}
	public void setResPayload(PayLoadBean resPayload) {
		this.resPayload = resPayload;
	}
	
	
	
	
	
	
	
	

}
