package com.bhartipay.bbps.billfetch.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="customerInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerInfo {

	@XmlElement(name="customerMobile")
	private String customerMobile;
	
	@XmlElement(name="customerEmail")
	private String customerEmail;
	
	@XmlElement(name="customerAdhaar")
	private String customerAdhaar;
	
	@XmlElement(name="customerPan")
	private String customerPan;
	
	
	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerAdhaar() {
		return customerAdhaar;
	}

	public void setCustomerAdhaar(String customerAdhaar) {
		this.customerAdhaar = customerAdhaar;
	}

	public String getCustomerPan() {
		return customerPan;
	}

	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}

	
}
