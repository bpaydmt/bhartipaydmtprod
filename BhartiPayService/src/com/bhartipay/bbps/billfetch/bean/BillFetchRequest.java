package com.bhartipay.bbps.billfetch.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billFetchRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillFetchRequest {
	
	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "billerId")
	private String billerId;

	@XmlElement(name = "customerInfo")
	CustomerInfo customerInfo = new CustomerInfo();

	@XmlElement(name = "agentDeviceInfo")
	AgentDeviceInfo agentDeviceInfo = new AgentDeviceInfo();

	@XmlElement(name = "inputParams")
	InputParams inputParams = new InputParams();

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public AgentDeviceInfo getAgentDeviceInfo() {
		return agentDeviceInfo;
	}

	public void setAgentDeviceInfo(AgentDeviceInfo agentDeviceInfo) {
		this.agentDeviceInfo = agentDeviceInfo;
	}

	public InputParams getInputParams() {
		return inputParams;
	}

	public void setInputParams(InputParams inputParams) {
		this.inputParams = inputParams;
	}
	
}
