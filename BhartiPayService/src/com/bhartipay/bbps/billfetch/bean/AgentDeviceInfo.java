package com.bhartipay.bbps.billfetch.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "agentDeviceInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgentDeviceInfo {
	
	@XmlElement(name = "ip")
	private String ip;

	@XmlElement(name = "initChannel")
	private String initChannel;
	
	@XmlElement(name = "mac")
	private String mac;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getInitChannel() {
		return initChannel;
	}

	public void setInitChannel(String initChannel) {
		this.initChannel = initChannel;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
}
