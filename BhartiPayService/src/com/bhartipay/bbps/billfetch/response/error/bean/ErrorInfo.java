package com.bhartipay.bbps.billfetch.response.error.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "errorInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorInfo
{
	@XmlElement(name = "error")
	private Error error;

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

}