package com.bhartipay.bbps.billfetch.response.error.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billFetchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillFetchResponse
{
	@XmlElement(name = "errorInfo")
	private ErrorInfo errorInfo;

	@XmlElement(name = "responseCode")
	private String responseCode;

    public ErrorInfo getErrorInfo ()
    {
        return errorInfo;
    }

    public void setErrorInfo (ErrorInfo errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }
}
