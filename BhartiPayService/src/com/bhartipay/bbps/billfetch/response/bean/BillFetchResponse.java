package com.bhartipay.bbps.billfetch.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billFetchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillFetchResponse {
	
	@XmlElement(name = "responseCode")
	private String responseCode;

	@XmlElement(name = "inputParams")
	InputParams inputParams = new InputParams();

	@XmlElement(name = "billerResponse")
	BillerResponse billerResponse = new BillerResponse();

	@XmlElement(name = "additionalInfo")
	AdditionalInfo additionalInfo = new AdditionalInfo();

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public InputParams getInputParams() {
		return inputParams;
	}

	public void setInputParams(InputParams inputParams) {
		this.inputParams = inputParams;
	}

	public BillerResponse getBillerResponse() {
		return billerResponse;
	}

	public void setBillerResponse(BillerResponse billerResponse) {
		this.billerResponse = billerResponse;
	}

	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
}
