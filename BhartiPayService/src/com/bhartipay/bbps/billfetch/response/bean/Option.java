package com.bhartipay.bbps.billfetch.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "option")
@XmlAccessorType(XmlAccessType.FIELD)
public class Option {

	@XmlElement(name = "amountName")
	private String amountName;

	@XmlElement(name = "amountValue")
	private String amountValue;

	public String getAmountName() {
		return amountName;
	}

	public void setAmountName(String amountName) {
		this.amountName = amountName;
	}

	public String getAmountValue() {
		return amountValue;
	}

	public void setAmountValue(String amountValue) {
		this.amountValue = amountValue;
	}

}
