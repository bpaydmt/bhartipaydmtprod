package com.bhartipay.bbps.billfetch.response.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "additionalInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalInfo {
	
	@XmlElement(name = "info")
	List<Info> inf = new ArrayList<Info>();

	public List<Info> getInf() {
		return inf;
	}

	public void setInf(List<Info> inf) {
		this.inf = inf;
	}
	
	

}
