package com.bhartipay.bbps.transactionstatus.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "txnList")
@XmlAccessorType(XmlAccessType.FIELD)
public class TxnList {
	
	@XmlElement(name = "billerId")
	private String billerId;

	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "amount")
	private String amount;

	@XmlElement(name = "txnStatus")
	private String txnStatus;

	@XmlElement(name = "txnReferenceId")
	private String txnReferenceId;

	@XmlElement(name = "txnDate")
	private String txnDate;

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getTxnStatus ()
    {
        return txnStatus;
    }

    public void setTxnStatus (String txnStatus)
    {
        this.txnStatus = txnStatus;
    }

    public String getTxnReferenceId ()
    {
        return txnReferenceId;
    }

    public void setTxnReferenceId (String txnReferenceId)
    {
        this.txnReferenceId = txnReferenceId;
    }

    public String getTxnDate ()
    {
        return txnDate;
    }

    public void setTxnDate (String txnDate)
    {
        this.txnDate = txnDate;
    }
}
