package com.bhartipay.bbps.transactionstatus.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transactionStatusResp")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionStatusResp {
	
	@XmlElement(name = "responseReason")
	private String responseReason;

	@XmlElement(name = "txnList")
	private TxnList txnList;

	@XmlElement(name = "responseCode")
	private String responseCode;

    public String getResponseReason ()
    {
        return responseReason;
    }

    public void setResponseReason (String responseReason)
    {
        this.responseReason = responseReason;
    }

    public TxnList getTxnList ()
    {
        return txnList;
    }

    public void setTxnList (TxnList txnList)
    {
        this.txnList = txnList;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

}