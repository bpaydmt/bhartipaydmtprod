package com.bhartipay.bbps.transactionstatus.response.error.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transactionStatusResp")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionStatusResp {
    
	@XmlElement(name = "responseReason")
	private String responseReason;

	@XmlElement(name = "errorInfo")
	private ErrorInfo errorInfo;

	@XmlElement(name = "responseCode")
	private String responseCode;

    public String getResponseReason ()
    {
        return responseReason;
    }

    public void setResponseReason (String responseReason)
    {
        this.responseReason = responseReason;
    }

    public ErrorInfo getErrorInfo ()
    {
        return errorInfo;
    }

    public void setErrorInfo (ErrorInfo errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }
}
