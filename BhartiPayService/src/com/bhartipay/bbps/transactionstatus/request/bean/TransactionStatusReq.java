package com.bhartipay.bbps.transactionstatus.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transactionStatusReq")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionStatusReq
{
	@XmlElement(name = "trackType")
	private String trackType;

	@XmlElement(name = "trackValue")
	private String trackValue;

    public String getTrackType ()
    {
        return trackType;
    }

    public void setTrackType (String trackType)
    {
        this.trackType = trackType;
    }

    public String getTrackValue ()
    {
        return trackValue;
    }

    public void setTrackValue (String trackValue)
    {
        this.trackValue = trackValue;
    }

}
	