package com.bhartipay.bbps;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.billerinfo.bean.BillerBbpsResponseBean;
import com.bhartipay.bbps.dao.PaymentDao;
import com.bhartipay.bbps.dao.impl.PaymentDaoImpl;
import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.bbps.service.BbpsBillerService;
import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;
import com.bhartipay.lean.bean.AgentBillPayRequest;
import com.bhartipay.lean.bean.BbpsComplaint;
import com.bhartipay.lean.bean.BbpsRequestData;
import com.bhartipay.lean.bean.BbpsStatusUpdateResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/BbpsManager")
public class BbpsManager {
	
	@POST
	@Path("/getBillerCategoryTest")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getBillerCategory(){
		
		System.out.println("Try for get biller category ");
		
		return null;
	}
	
	//********************************** BBPS API Start ****************************************
	
	
		@POST
		@Path("/loadBillerCategory")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean loadBillerCategory(@Context HttpServletRequest request){
			System.out.println("Try for load biller category ");
			BillerBbpsResponseBean billerCategoryResponseBean = null;
			
			BbpsBillerService billerCategory = new BillerBbpsServiceImpl();
			String loadStatus = billerCategory.loadAllBillerCategory();
			
			JSONObject billerCategories = new JSONObject();
	
		    try {
		        billerCategories.put("status", loadStatus);
		        billerCategoryResponseBean = new BillerBbpsResponseBean();
		        billerCategoryResponseBean.setResponse(billerCategories.toString());
		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
			return billerCategoryResponseBean;
		}
	
		@POST
		@Path("/getBillerCategory")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean getBillerCategory(@Context HttpServletRequest request){
			System.out.println("Try for get biller category ");
			BillerBbpsResponseBean billerCategoryResponseBean = null;
			
			BbpsBillerService billerCategoryDao = new BillerBbpsServiceImpl();
			Set<String> allCaregorySet = billerCategoryDao.getAllBillerCategory();
			
			JSONObject billerCategories = new JSONObject();

		    try {

		        JSONArray categoryJSONObjectArray = new JSONArray();

		        for (String category : allCaregorySet) {
		            if (category != null) {
		            	
		            	JSONObject localeAsJSONObject = new JSONObject();
		                localeAsJSONObject.put("category", category);
		            	
		                categoryJSONObjectArray.put(localeAsJSONObject);
		            }
		        }

		        billerCategories.put("billerCategories", categoryJSONObjectArray);
		        
		        billerCategoryResponseBean = new BillerBbpsResponseBean();
		        billerCategoryResponseBean.setResponse(billerCategories.toString());

		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
			
			return billerCategoryResponseBean;
		}
		
		
		//getBillerByCategory
		@POST
		@Path("/getBillerByCategory")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean getBillerByCategory(String category,@Context HttpServletRequest request){
			System.out.println("Try for get biller BY category ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,String> allCaregoryMap = bbpsBillerDao.getBillerListByCategory(category);
			
			JSONObject billerCategories = new JSONObject(allCaregoryMap);

			try {
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(billerCategories.toString());

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
		
		//getCustomParam
		@POST
		@Path("/getCustomParam")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean getCustomParam(String billerId,@Context HttpServletRequest request){
			System.out.println("Try for get biller Custom Parameters ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			String customParamByBiller = bbpsBillerDao.getCustomParam(billerId);
			
			try {
				
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(customParamByBiller);

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
		
		//getCustomParam
		@POST
		@Path("/fetchBillDetails")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean fetchBillDetails(@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent, String billFetchRequest){
			System.out.println("Try for fetch bill details ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> fetchedBillMap = bbpsBillerDao.fetchBillDetails(billFetchRequest, ipIemi, agent);
			
			JSONObject fetchedBillJObj = new JSONObject(fetchedBillMap);

			try {
				
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
		
		
		//billPaymentRequest
		@POST
		@Path("/billPaymentRequest")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean billPaymentRequest(@Context HttpServletRequest request,@HeaderParam("IPIMEI") String ipIemi,@HeaderParam("AGENT") String agent, String billRequest){
			System.out.println("Try for bill payment details ");
			
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> fetchedBillMap = bbpsBillerDao.billPaymentRequest(billRequest, agent, ipIemi);
			
			JSONObject fetchedBillJObj = new JSONObject(fetchedBillMap);

			try {
				
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
			
		
		@POST
		@Path("/bbpsTxtIDRequest")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean checkTxtStatusService(String TxnStatusRequest,@Context HttpServletRequest request){
			
			System.out.println("Checking Txt Status .... ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> getTxtStatusMap = bbpsBillerDao.getTxtStatusRequest(TxnStatusRequest);
			
			JSONObject fetchedBillJObj = new JSONObject(getTxtStatusMap);

			try {
				
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
		
		
		@POST
		@Path("/complaintStatusRequest")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean complaintStatusService(String complaintStatusRequest,@Context HttpServletRequest request){
			
			System.out.println("Checking complaint status request .... ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> getTxtStatusMap = bbpsBillerDao.getcomplaintStatusRequest(complaintStatusRequest);
			
			JSONObject fetchedBillJObj = new JSONObject(getTxtStatusMap);

			try {
				
		        billerBbpsResponseBean = new BillerBbpsResponseBean();
		        billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());

		    } catch (Exception e) {
		        e.printStackTrace();
		    }
			
			return billerBbpsResponseBean;
		}
		
		
		@POST
		@Path("/complaintRegistrationRequest")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean complaintRegistrationService(String complaintRegistrationRequest, @Context HttpServletRequest request){
			
			System.out.println("Checking complaint Registration request .... ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> getTxtStatusMap = bbpsBillerDao.registrationComplaintRequest(complaintRegistrationRequest);
			
			JSONObject fetchedBillJObj = new JSONObject(getTxtStatusMap);
			
			try {
				
				billerBbpsResponseBean = new BillerBbpsResponseBean();
				billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return billerBbpsResponseBean;
		}
		
		
		
		@POST
		@Path("/getUserId")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public BillerBbpsResponseBean getUserId(BbpsRequestData complaintRegistrationRequest, @Context HttpServletRequest request){
			
			String id = complaintRegistrationRequest.getUser();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),id,"","", "", "","getUserId");
			System.out.println("Checking complaint Registration request .... ");
			BillerBbpsResponseBean billerBbpsResponseBean = null;
			
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			Map<String,Object> getTxtStatusMap = bbpsBillerDao.getUserId(complaintRegistrationRequest.getRequest(),id);
			
			JSONObject fetchedBillJObj = new JSONObject(getTxtStatusMap);
			
			try {
				
				billerBbpsResponseBean = new BillerBbpsResponseBean();
				billerBbpsResponseBean.setResponse(fetchedBillJObj.toString());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return billerBbpsResponseBean;
		}
		
		
		
		@POST
		@Path("/getComplaintDetails")
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public String getComplaintDetails(String userId){
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userId,"","", "", "","getComplaintDetails");
			   
			System.out.println("Complaint  "+userId);
			 GsonBuilder builder = new GsonBuilder();
		     Gson gson = builder.create();
		     BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
		     List<BbpsComplaint> list = bbpsBillerDao.getComplaintDetails(userId);
		     Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userId,"","", "", "","getComplaintDetails"+list.size());
		     return gson.toJson(list);
			//return list;
		}
		
		
		
		
		
		//********************************** BBPS API End ****************************************
		@POST
		@Path("/getBbpsAgentRequest") 
		@Consumes({MediaType.APPLICATION_JSON})
		public Boolean getBbpsAgentRequest(BbpsPayment pay){
		System.out.println("====================");
			PaymentDao payDao = new PaymentDaoImpl();
		     boolean agentBillRequest = payDao.getBbpsAgentRequest(pay);
			System.out.println("okkkkkkkkkkkkkk  "+agentBillRequest);
			return agentBillRequest;
		}

		@POST
		@Path("/saveBbpsAgentRequest") 
		@Consumes({MediaType.APPLICATION_JSON})
		public Boolean saveBbpsAgentRequest(AgentBillPayRequest pay){
		System.out.println("====================");
			PaymentDao payDao = new PaymentDaoImpl();
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			pay.setCreationDate(timestamp);
		     boolean agentBillRequest = payDao.saveBbpsAgentRequest(pay);
			System.out.println("okkkkkkkkkkkkkk  "+agentBillRequest);
			return agentBillRequest;
		}
		
		
		
	// BBPS Transaction Status Check/Update
		
		
		@POST
		@Path("/bbpsStatusUpdate")
		@Produces({ MediaType.APPLICATION_JSON})
		public BbpsStatusUpdateResponse bbpsStatusUpdate(String txnRequest,@Context HttpServletRequest request){
			 System.out.println("Checking Txt Status .... ");
			BbpsStatusUpdateResponse res=new BbpsStatusUpdateResponse();
			BbpsBillerService bbpsBillerDao = new BillerBbpsServiceImpl();
			String getTxtStatus = bbpsBillerDao.bbpsStatusUpdate(txnRequest);
			res.setStatus(getTxtStatus);
			res.setTxnId(txnRequest);
	
			return res;
		}
		 
		
		
		
		
		
		
}
