package com.bhartipay.bbps.complaint.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "complaintRegistrationReq")
@XmlAccessorType(XmlAccessType.FIELD)
public class ComplaintRegistrationReq {
	
	@XmlElement(name = "billerId")
	private String billerId;

	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "participationType")
	private String participationType;

	@XmlElement(name = "complaintType")
	private String complaintType;

	@XmlElement(name = "complaintDesc")
	private String complaintDesc;

	@XmlElement(name = "servReason")
	private String servReason;

	@XmlElement(name = "txnRefId")
	private String txnRefId;

	@XmlElement(name = "complaintDisposition")
	private String complaintDisposition;

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getParticipationType() {
		return participationType;
	}

	public void setParticipationType(String participationType) {
		this.participationType = participationType;
	}

	public String getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(String complaintType) {
		this.complaintType = complaintType;
	}

	public String getComplaintDesc() {
		return complaintDesc;
	}

	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}

	public String getServReason() {
		return servReason;
	}

	public void setServReason(String servReason) {
		this.servReason = servReason;
	}

	public String getTxnRefId() {
		return txnRefId;
	}

	public void setTxnRefId(String txnRefId) {
		this.txnRefId = txnRefId;
	}

	public String getComplaintDisposition() {
		return complaintDisposition;
	}

	public void setComplaintDisposition(String complaintDisposition) {
		this.complaintDisposition = complaintDisposition;
	}
}
