package com.bhartipay.bbps.complaint.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "complaintTrackingReq")
@XmlAccessorType(XmlAccessType.FIELD)
public class ComplaintTrackingReq
{
	@XmlElement(name = "complaintId")
	private String complaintId;

	@XmlElement(name = "complaintType")
	private String complaintType;

    public String getComplaintId ()
    {
        return complaintId;
    }

    public void setComplaintId (String complaintId)
    {
        this.complaintId = complaintId;
    }

    public String getComplaintType ()
    {
        return complaintType;
    }

    public void setComplaintType (String complaintType)
    {
        this.complaintType = complaintType;
    }
}