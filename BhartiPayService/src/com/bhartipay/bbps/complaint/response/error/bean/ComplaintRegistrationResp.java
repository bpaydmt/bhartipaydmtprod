package com.bhartipay.bbps.complaint.response.error.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "complaintRegistrationResp")
@XmlAccessorType(XmlAccessType.FIELD)
public class ComplaintRegistrationResp
{
	@XmlElement(name = "responseReason")
	private String responseReason;

	@XmlElement(name = "complaintId")
	private String complaintId;

	@XmlElement(name = "errorInfo")
	private ErrorInfo errorInfo;

	@XmlElement(name = "complaintAssigned")
	private String complaintAssigned;

	@XmlElement(name = "responseCode")
	private String responseCode;

    public String getResponseReason ()
    {
        return responseReason;
    }

    public void setResponseReason (String responseReason)
    {
        this.responseReason = responseReason;
    }

    public String getComplaintId ()
    {
        return complaintId;
    }

    public void setComplaintId (String complaintId)
    {
        this.complaintId = complaintId;
    }

    public ErrorInfo getErrorInfo ()
    {
        return errorInfo;
    }

    public void setErrorInfo (ErrorInfo errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public String getComplaintAssigned ()
    {
        return complaintAssigned;
    }

    public void setComplaintAssigned (String complaintAssigned)
    {
        this.complaintAssigned = complaintAssigned;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }
}