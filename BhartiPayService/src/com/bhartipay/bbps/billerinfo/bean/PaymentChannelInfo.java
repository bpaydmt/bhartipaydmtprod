package com.bhartipay.bbps.billerinfo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType (XmlAccessType.FIELD)
public class PaymentChannelInfo {
	
	@XmlElement(name="paymentChannelName")
	String paymentChannelName="";
	@XmlElement(name="minAmount")
	String minAmount="";
	@XmlElement(name="maxAmount")
	String maxAmount="";
	
	public String getPaymentChannelName() {
		return paymentChannelName;
	}
	public void setPaymentChannelName(String paymentChannelName) {
		this.paymentChannelName = paymentChannelName;
	}
	public String getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}
	public String getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}
	
	

}
