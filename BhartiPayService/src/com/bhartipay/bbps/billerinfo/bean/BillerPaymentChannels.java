package com.bhartipay.bbps.billerinfo.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="billerPaymentChannels")
@XmlAccessorType (XmlAccessType.FIELD)
public class BillerPaymentChannels {
	
	@XmlElement(name="paymentChannelInfo")
	private List<PaymentChannelInfo> paymentChannelInfo=new ArrayList<PaymentChannelInfo>();

	public List<PaymentChannelInfo> getPaymentChannelInfo() {
		return paymentChannelInfo;
	}

	public void setPaymentChannelInfo(List<PaymentChannelInfo> paymentChannelInfo) {
		this.paymentChannelInfo = paymentChannelInfo;
	}
	

}
