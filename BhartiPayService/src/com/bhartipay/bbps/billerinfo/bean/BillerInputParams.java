package com.bhartipay.bbps.billerinfo.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="billerInputParams")
@XmlAccessorType (XmlAccessType.FIELD)
public class BillerInputParams {

	
	@XmlElement(name="paramInfo")
	 private List<ParamInfo> paramInfo=new ArrayList<ParamInfo>();

	public List<ParamInfo> getParamInfo() {
		return paramInfo;
	}

	public void setParamInfo(List<ParamInfo> paramInfo) {
		this.paramInfo = paramInfo;
	}
	
	
}
