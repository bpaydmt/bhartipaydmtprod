package com.bhartipay.bbps.billerinfo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="biller")
@XmlAccessorType (XmlAccessType.FIELD)
public class Biller {
	
	@XmlElement(name="billerId")
	String billerId="";
	@XmlElement(name="billerName")
	String billerName="";
	@XmlElement(name="billerCategory")
	String billerCategory="";
	@XmlElement(name="billerAdhoc")
	String billerAdhoc="";
	@XmlElement(name="billerCoverage")
	String billerCoverage="";
	@XmlElement(name="billerFetchRequiremet")
	String billerFetchRequiremet="";
	@XmlElement(name="billerPaymentExactness")
	String billerPaymentExactness="";
	@XmlElement(name="billerSupportBillValidation")
	String billerSupportBillValidation="";
	@XmlElement(name="supportPendingStatus")
	String supportPendingStatus="";
	@XmlElement(name="supportDeemed")
	String supportDeemed="";
	@XmlElement(name="billerTimeout")
	String billerTimeout="";
	
	@XmlElement(name="billerInputParams")
	BillerInputParams billerInputParams=new BillerInputParams();
	
	@XmlElement(name="billerAmountOptions")
	String billerAmountOptions="";
	@XmlElement(name="billerPaymentModes")
	String billerPaymentModes="";
	@XmlElement(name="billerDescription")
	String billerDescription="";
	@XmlElement(name="rechargeAmountInValidationRequest")
	String rechargeAmountInValidationRequest="";
	
	@XmlElement(name="billerPaymentChannels")
	BillerPaymentChannels billerPaymentChannels=new BillerPaymentChannels();

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getBillerName() {
		return billerName;
	}

	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}

	public String getBillerCategory() {
		return billerCategory;
	}

	public void setBillerCategory(String billerCategory) {
		this.billerCategory = billerCategory;
	}

	public String getBillerAdhoc() {
		return billerAdhoc;
	}

	public void setBillerAdhoc(String billerAdhoc) {
		this.billerAdhoc = billerAdhoc;
	}

	public String getBillerCoverage() {
		return billerCoverage;
	}

	public void setBillerCoverage(String billerCoverage) {
		this.billerCoverage = billerCoverage;
	}

	public String getBillerFetchRequiremet() {
		return billerFetchRequiremet;
	}

	public void setBillerFetchRequiremet(String billerFetchRequiremet) {
		this.billerFetchRequiremet = billerFetchRequiremet;
	}

	public String getBillerPaymentExactness() {
		return billerPaymentExactness;
	}

	public void setBillerPaymentExactness(String billerPaymentExactness) {
		this.billerPaymentExactness = billerPaymentExactness;
	}

	public String getBillerSupportBillValidation() {
		return billerSupportBillValidation;
	}

	public void setBillerSupportBillValidation(String billerSupportBillValidation) {
		this.billerSupportBillValidation = billerSupportBillValidation;
	}

	public String getSupportPendingStatus() {
		return supportPendingStatus;
	}

	public void setSupportPendingStatus(String supportPendingStatus) {
		this.supportPendingStatus = supportPendingStatus;
	}

	public String getSupportDeemed() {
		return supportDeemed;
	}

	public void setSupportDeemed(String supportDeemed) {
		this.supportDeemed = supportDeemed;
	}

	public String getBillerTimeout() {
		return billerTimeout;
	}

	public void setBillerTimeout(String billerTimeout) {
		this.billerTimeout = billerTimeout;
	}

	public BillerInputParams getBillerInputParams() {
		return billerInputParams;
	}

	public void setBillerInputParams(BillerInputParams billerInputParams) {
		this.billerInputParams = billerInputParams;
	}

	public String getBillerAmountOptions() {
		return billerAmountOptions;
	}

	public void setBillerAmountOptions(String billerAmountOptions) {
		this.billerAmountOptions = billerAmountOptions;
	}

	public String getBillerPaymentModes() {
		return billerPaymentModes;
	}

	public void setBillerPaymentModes(String billerPaymentModes) {
		this.billerPaymentModes = billerPaymentModes;
	}

	public String getBillerDescription() {
		return billerDescription;
	}

	public void setBillerDescription(String billerDescription) {
		this.billerDescription = billerDescription;
	}

	public String getRechargeAmountInValidationRequest() {
		return rechargeAmountInValidationRequest;
	}

	public void setRechargeAmountInValidationRequest(String rechargeAmountInValidationRequest) {
		this.rechargeAmountInValidationRequest = rechargeAmountInValidationRequest;
	}

	public BillerPaymentChannels getBillerPaymentChannels() {
		return billerPaymentChannels;
	}

	public void setBillerPaymentChannels(BillerPaymentChannels billerPaymentChannels) {
		this.billerPaymentChannels = billerPaymentChannels;
	}
	
	
	
	
	

}
