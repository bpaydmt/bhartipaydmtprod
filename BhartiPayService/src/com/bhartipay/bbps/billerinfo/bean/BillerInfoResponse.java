package com.bhartipay.bbps.billerinfo.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="billerInfoResponse")
@XmlAccessorType (XmlAccessType.FIELD)
public class BillerInfoResponse {
	
	@XmlElement(name="responseCode")
	private String responseCode=null;
	
	@XmlElement(name="biller")
	 private List<Biller> biller=new ArrayList<Biller>();

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public List<Biller> getBiller() {
		return biller;
	}

	public void setBiller(List<Biller> biller) {
		this.biller = biller;
	}
	
	

}
