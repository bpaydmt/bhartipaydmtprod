package com.bhartipay.bbps.deposit.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "depositEnquiryResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class DepositEnquiryResponse {
	
	@XmlElement(name = "currentBalance")
	private String currentBalance;

	@XmlElement(name = "instituteId")
	private String instituteId;

	@XmlElement(name = "currency")
	private String currency;

	@XmlElement(name = "transaction")
	private Transaction transaction;

    public String getCurrentBalance ()
    {
        return currentBalance;
    }

    public void setCurrentBalance (String currentBalance)
    {
        this.currentBalance = currentBalance;
    }

    public String getInstituteId ()
    {
        return instituteId;
    }

    public void setInstituteId (String instituteId)
    {
        this.instituteId = instituteId;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public Transaction getTransaction ()
    {
        return transaction;
    }

    public void setTransaction (Transaction transaction)
    {
        this.transaction = transaction;
    }

}
