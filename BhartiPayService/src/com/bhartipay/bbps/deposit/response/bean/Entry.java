package com.bhartipay.bbps.deposit.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "entry")
@XmlAccessorType(XmlAccessType.FIELD)
public class Entry {
	
	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "amount")
	private String amount;

	@XmlElement(name = "datetime")
	private String datetime;

	@XmlElement(name = "transType")
	private String transType;

	@XmlElement(name = "requestId")
	private String requestId;

	@XmlElement(name = "transactionId")
	private String transactionId;

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getDatetime ()
    {
        return datetime;
    }

    public void setDatetime (String datetime)
    {
        this.datetime = datetime;
    }

    public String getTransType ()
    {
        return transType;
    }

    public void setTransType (String transType)
    {
        this.transType = transType;
    }

    public String getRequestId ()
    {
        return requestId;
    }

    public void setRequestId (String requestId)
    {
        this.requestId = requestId;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }
}
