package com.bhartipay.bbps.deposit.response.bean;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {
	
	@XmlElement(name = "entry")
	private List<Entry> entry;

    public List<Entry> getEntry ()
    {
        return entry;
    }

    public void setEntry (List<Entry> entry)
    {
        this.entry = entry;
    }
}
