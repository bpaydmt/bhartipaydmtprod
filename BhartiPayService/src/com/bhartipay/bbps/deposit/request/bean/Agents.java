package com.bhartipay.bbps.deposit.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "agents")
@XmlAccessorType(XmlAccessType.FIELD)
public class Agents {

	@XmlElement(name = "agentId")
	private String agentId;

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }
}
