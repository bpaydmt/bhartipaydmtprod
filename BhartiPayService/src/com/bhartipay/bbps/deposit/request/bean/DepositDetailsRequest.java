package com.bhartipay.bbps.deposit.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "depositDetailsRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class DepositDetailsRequest {
	
	@XmlElement(name = "fromDate")
	private String fromDate;

	@XmlElement(name = "transType")
	private String transType;

	@XmlElement(name = "toDate")
	private String toDate;

	@XmlElement(name = "agents")
	private Agents agents;

    public String getFromDate ()
    {
        return fromDate;
    }

    public void setFromDate (String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getTransType ()
    {
        return transType;
    }

    public void setTransType (String transType)
    {
        this.transType = transType;
    }

    public String getToDate ()
    {
        return toDate;
    }

    public void setToDate (String toDate)
    {
        this.toDate = toDate;
    }

    public Agents getAgents ()
    {
        return agents;
    }

    public void setAgents (Agents agents)
    {
        this.agents = agents;
    }

}
