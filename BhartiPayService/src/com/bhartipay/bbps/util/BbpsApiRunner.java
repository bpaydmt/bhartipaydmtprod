package com.bhartipay.bbps.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.bhartipay.Logger.Commonlogger;

@SuppressWarnings("deprecation")
public class BbpsApiRunner {
	
		public static void main(String args[]) {
			
			BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
			bbpsApiRunner.complaintRegistration();
			
		}
		
		public String getBbpsApiResponse(PreRequestParam requestParam) {
		
		String decryptedResponse = null;
		
		String urlParameters = requestParam.getUrlParams();
		
		System.out.println("urlParameters : "+urlParameters);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "urlParameters  : "+urlParameters);
		
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		String request = requestParam.getRequest();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "request  : "+request);
		
		try {
			URL url = new URL(request);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);

			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String responseStatus = conn.getResponseMessage();
			System.out.println("responseStatus :" + responseStatus);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseStatus  : "+responseStatus);
			
			int responseCode = conn.getResponseCode();
			System.out.println("responseCode :" + responseCode);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseCode  : "+responseCode);
			String readLine = null;

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
				in.close();

				String responseAsString = response.toString();
				System.out.println("responseAsString : "+responseAsString);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseAsString  : "+responseAsString);
				if(responseAsString.contains("<html>"))
					decryptedResponse = responseAsString;
				else
					decryptedResponse = CCAVdecrypt.getDecryptedString(responseAsString);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "decryptedResponse  : "+decryptedResponse);
			} else {
				System.out.println("POST NOT WORKED");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[else] POST NOT WORKED");
				decryptedResponse = "POST NOT WORKED";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[Exception]:"+e.getMessage());
			decryptedResponse = "Unknown Host";
		}
		return decryptedResponse;
	}
		


		
		public String getBbpsApiResponse22(PreRequestParam requestParam) {
			
			String decryptedResponse = null;
			
			String urlParameters = requestParam.getNewUrlParams();
			
			System.out.println("urlParameters : "+urlParameters);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "urlParameters  : "+urlParameters);
			
			byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			String request = requestParam.getRequest();
			//String encReq = "&encRequest=" + requestParam.getEncRequest();
			String encReq = requestParam.getEncRequest();
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "request  : "+request);
			
			try {
				 
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost(request+"?"+urlParameters);
		         
		        //Set the API media type in http content-type header
		        postRequest.addHeader("content-type", "application/x-www-form-urlencoded");
		        StringEntity userEntity = new StringEntity(encReq);
		        postRequest.setEntity(userEntity);
		        HttpResponse responses = httpClient.execute(postRequest);
		        int statusCode = responses.getStatusLine().getStatusCode();
		       System.out.println("RESPONSE STRING "+responses.toString());
		       HttpParams conn = responses.getParams();
			   System.out.println(conn.toString());
			   decryptedResponse = CCAVdecrypt.getDecryptedString(responses.toString());
			   System.out.println("Decrypted String "+decryptedResponse);
		
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[Exception]:"+e.getMessage());
				decryptedResponse = "Unknown Host";
			}
			return decryptedResponse;
		}

	public String callBillerApiResponse(PreRequestParam requestParam)
	{
		String urlParameters = requestParam.getNewUrlParams();
		String encReq = requestParam.getEncRequest();
		String request = requestParam.getRequest();
		String encryptedResponse ="";
        try {
        	  HttpClient httpclient = new DefaultHttpClient();
	          URIBuilder builder = new URIBuilder();
              HttpPost httppost = new HttpPost(request+"?"+urlParameters);
	          HttpEntity entity= new  ByteArrayEntity(encReq.getBytes("UTF-8"));
              httppost.setEntity(entity);
              HttpResponse response = httpclient.execute(httppost);
              encryptedResponse = EntityUtils.toString(response.getEntity());
            
        } catch (IOException e) {
            
            e.printStackTrace();
        }
	 
	 return  encryptedResponse;
	}
		
		
		
      public String getBbpsNewApiResponse(PreRequestParam requestParam) {
			
			String decryptedResponse = null;
			try {
					
					String responseAsString = callBillerApiResponse(requestParam);;
					System.out.println("responseAsString : "+responseAsString);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseAsString  : "+responseAsString);
					if(responseAsString.contains("<html>"))
						decryptedResponse = responseAsString;
					else
						decryptedResponse = CCAVdecrypt.getDecryptedString(responseAsString);
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "decryptedResponse  : "+decryptedResponse);
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[Exception]:"+e.getMessage());
				decryptedResponse = "Unknown Host";
			}
			return decryptedResponse;
		}
		
		
		
		public String getRechargeApiResponse(PrerequstRechargeParams requestParam) {
			
		String decryptedResponse = null;
		
		String urlParameters = requestParam.getUrlParams();
		
		System.out.println("urlParameters : "+urlParameters);
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "urlParameters  : "+urlParameters);
		
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		String request = requestParam.getRequest();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "request  : "+request);
		
		try {
			URL url = new URL(request);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);

			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String responseStatus = conn.getResponseMessage();
			System.out.println("responseStatus :" + responseStatus);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseStatus  : "+responseStatus);
			
			int responseCode = conn.getResponseCode();
			System.out.println("responseCode :" + responseCode);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseCode  : "+responseCode);
			String readLine = null;

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
				in.close();

				String responseAsString = response.toString();
				System.out.println("responseAsString : "+responseAsString);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "responseAsString  : "+responseAsString);
				if(responseAsString.contains("<html>"))
					decryptedResponse = responseAsString;
				else
					decryptedResponse = CCAVdecrypt.getDecryptedRechargeString(responseAsString);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "decryptedResponse  : "+decryptedResponse);
			} else {
				System.out.println("POST NOT WORKED");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[else] POST NOT WORKED");
				decryptedResponse = "POST NOT WORKED";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getBbpsApiResponse()","","", "","", "[Exception]:"+e.getMessage());
			decryptedResponse = "Unknown Host";
		}
		return decryptedResponse;
	}
		
		
		
	public void billValidate() {
		
		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		
		
//		String request = "https://stgapi.billavenue.com/billpay/extComplaints/track/xml";
		
		String request = "https://stgapi.billavenue.com/billpay/extBillValCntrl/billValidationRequest/xml";
		String request2 = "https://stgapi.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
		
//		String request = "https://stgapi.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml";
		
		String reqId = TransactionManager.getNewTransactionId();
		
		String xmlRequestToString2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<billPaymentRequest>\r\n" + 
				"    <agentId>CC01CC01513515340681</agentId>\r\n" + 
				"    <billerAdhoc>true</billerAdhoc>\r\n" + 
				"    <agentDeviceInfo>\r\n" + 
				"        <ip>192.168.2.73</ip>\r\n" + 
				"        <initChannel>AGT</initChannel>\r\n" + 
				"        <mac>01-23-45-67-89-ab</mac>\r\n" + 
				"    </agentDeviceInfo>\r\n" + 
				"    <customerInfo>\r\n" + 
				"        <customerMobile>9898990084</customerMobile>\r\n" + 
				"        <customerEmail></customerEmail>\r\n" + 
				"        <customerAdhaar></customerAdhaar>\r\n" + 
				"        <customerPan></customerPan>\r\n" + 
				"    </customerInfo>\r\n" + 
				"    <billerId>OTME00005XXZ43</billerId>\r\n" + 
				"   <inputParams>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a</paramName>\r\n" + 
				"         <paramValue>10</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b</paramName>\r\n" + 
				"         <paramValue>20</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c</paramName>\r\n" + 
				"         <paramValue>30</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d</paramName>\r\n" + 
				"         <paramValue>40</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d e</paramName>\r\n" + 
				"         <paramValue>50</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"   </inputParams>\r\n" + 
				"   <billerResponse>\r\n" + 
				"        <billAmount>100000</billAmount>\r\n" + 
				"        <billDate>2015-06-14</billDate>\r\n" + 
				"        <billNumber>12303</billNumber>\r\n" + 
				"        <billPeriod>june</billPeriod>\r\n" + 
				"        <customerName>BBPS</customerName>\r\n" + 
				"        <dueDate>2015-06-20</dueDate>\r\n" + 
				"        <amountOptions>\r\n" + 
				"            <option>\r\n" + 
				"                <amountName>Late Payment Fee</amountName>\r\n" + 
				"                <amountValue>40</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"            <option>\r\n" + 
				"                <amountName>Fixed Charges</amountName>\r\n" + 
				"                <amountValue>50</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"	    <option>\r\n" + 
				"                <amountName>Additional Charges</amountName>\r\n" + 
				"                <amountValue>60</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"        </amountOptions>\r\n" + 
				"    </billerResponse>\r\n" + 
				"    <additionalInfo>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a</infoName>\r\n" + 
				"            <infoValue>10</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b</infoName>\r\n" + 
				"            <infoValue>20</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b c</infoName>\r\n" + 
				"            <infoValue>30</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b c d</infoName>\r\n" + 
				"            <infoValue>40</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"    </additionalInfo>\r\n" + 
				"    <amountInfo>\r\n" + 
				"        <amount>100000</amount>\r\n" + 
				"        <currency>356</currency>\r\n" + 
				"        <custConvFee>0</custConvFee>\r\n" + 
				"        <amountTags></amountTags>\r\n" + 
				"    </amountInfo>\r\n" + 
				"    <paymentMethod>\r\n" + 
				"        <paymentMode>Cash</paymentMode>\r\n" + 
				"        <quickPay>N</quickPay>\r\n" + 
				"        <splitPay>N</splitPay>\r\n" + 
				"    </paymentMethod>\r\n" + 
				"    <paymentInfo>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>Remarks</infoName>\r\n" + 
				"            <infoValue>Received</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"    </paymentInfo>\r\n" + 
				"</billPaymentRequest>";
		
		String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<billValidationRequest>\n" + 
				"<agentId>CC01CC01513515340681</agentId>\n" + 
				"<billerId>OTME00005XXZ43</billerId>\n" + 
				"<inputParams>\n" + 
				"<input>\n" + 
				"<paramName>a</paramName>\n" + 
				"<paramValue>10</paramValue>\n" + 
				"</input>\n" + 
				"<input>\n" + 
				"<paramName>a b</paramName>\n" + 
				"<paramValue>20</paramValue>\n" + 
				"</input>\n" + 
				"<input>\n" + 
				"<paramName>a b c</paramName>\n" + 
				"<paramValue>30</paramValue>\n" + 
				"</input>\n" + 
				"<input>\n" + 
				"<paramName>a b c d</paramName>\n" + 
				"<paramValue>40</paramValue>\n" + 
				"</input>\n" + 
				"<input>\n" + 
				"<paramName>a b c d e</paramName>\n" + 
				"<paramValue>50</paramValue>\n" + 
				"</input>\n" + 
				"</inputParams>\n" + 
				"</billValidationRequest>";
		
		
		String xmlRequestToString123 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<billPaymentRequest>\n" + 
				"    <agentId>CC01CC01513515340681</agentId>\n" + 
				"    <billerAdhoc>true</billerAdhoc>\n" + 
				"    <agentDeviceInfo>\n" + 
				"        <ip>192.168.2.73</ip>\n" + 
				"        <initChannel>AGT</initChannel>\n" + 
				"        <mac>01-23-45-67-89-ab</mac>\n" + 
				"    </agentDeviceInfo>\n" + 
				"    <customerInfo>\n" + 
				"        <customerMobile>9898990083</customerMobile>\n" + 
				"        <customerEmail></customerEmail>\n" + 
				"        <customerAdhaar></customerAdhaar>\n" + 
				"        <customerPan></customerPan>\n" + 
				"    </customerInfo>\n" + 
				"    <billerId>OTNS00005XXZ43</billerId>\n" + 
				"   <inputParams>\n" + 
				"      <input>\n" + 
				"         <paramName>a</paramName>\n" + 
				"         <paramValue>10</paramValue>\n" + 
				"      </input>\n" + 
				"      <input>\n" + 
				"         <paramName>a b</paramName>\n" + 
				"         <paramValue>20</paramValue>\n" + 
				"      </input>\n" + 
				"      <input>\n" + 
				"         <paramName>a b c</paramName>\n" + 
				"         <paramValue>30</paramValue>\n" + 
				"      </input>\n" + 
				"      <input>\n" + 
				"         <paramName>a b c d</paramName>\n" + 
				"         <paramValue>40</paramValue>\n" + 
				"      </input>\n" + 
				"      <input>\n" + 
				"         <paramName>a b c d e</paramName>\n" + 
				"         <paramValue>50</paramValue>\n" + 
				"      </input>\n" + 
				"   </inputParams>\n" + 
				"   <amountInfo>\n" + 
				"       <amount>100000</amount>\n" + 
				"       <currency>356</currency>\n" + 
				"       <custConvFee>0</custConvFee>\n" + 
				"       <amountTags></amountTags>\n" + 
				"   </amountInfo>\n" + 
				"   <paymentMethod>\n" + 
				"       <paymentMode>Cash</paymentMode>\n" + 
				"       <quickPay>Y</quickPay>\n" + 
				"       <splitPay>N</splitPay>\n" + 
				"   </paymentMethod>\n" + 
				"   <paymentInfo>\n" + 
				"       <info>\n" + 
				"           <infoName>Remarks</infoName>\n" + 
				"           <infoValue>Received</infoValue>\n" + 
				"       </info>\n" + 
				"   </paymentInfo>\n" + 
				"</billPaymentRequest>";
		
	/*
	 * try{
	 * 
	 * JAXBContext jContext = JAXBContext.newInstance(BillerInfoRequest.class);
	 * Marshaller marshallObj = jContext.createMarshaller();
	 * marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	 * BillerInfoRequest billInfo = new BillerInfoRequest(); StringWriter sw = new
	 * StringWriter(); marshallObj.marshal(billInfo, sw); xmlRequestToString =
	 * sw.toString(); } catch(Exception e) { e.printStackTrace(); }
	 */

		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
		
		String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
		System.out.println("decryptedResponse\n" + decryptedResponse);
		
	
	  String reqId2 = TransactionManager.getNewTransactionId();
	  
	  PreRequestParam requestParam2 = new PreRequestParam();
	  requestParam2.setRequest(request2); 
	  requestParam2.setRequestId(reqId);
	  requestParam2.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString2));
	  
	  String decryptedResponse2 = bbpsApiRunner.getBbpsApiResponse(requestParam2);
	  System.out.println("decryptedResponse2\n" + decryptedResponse2);
		
	}
		
		
public void billPayment(String reqId) {
		
		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		
		
		String request = "https://stgapi.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";

		//String reqId = TransactionManager.getNewTransactionId();
		
		//String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><billPaymentRequest><agentId>CC01CC01513515340681</agentId><billerAdhoc>true</billerAdhoc><agentDeviceInfo><ip>192.168.2.73</ip><initChannel>AGT</initChannel><mac>01-23-45-67-89-ab</mac></agentDeviceInfo><customerInfo><customerMobile>9898990084</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>OTME00005XXZ43</billerId><inputParams><input><paramName>a</paramName><paramValue>10</paramValue></input><input><paramName>a b</paramName><paramValue>20</paramValue></input><input><paramName>a b c</paramName><paramValue>30</paramValue></input><input><paramName>a b c d</paramName><paramValue>40</paramValue></input><input><paramName>a b c d e</paramName><paramValue>50</paramValue></input></inputParams><billerResponse><billAmount>100000</billAmount><billDate>2015-06-14</billDate><billNumber>12303</billNumber><billPeriod>june</billPeriod><customerName>BBPS</customerName><dueDate>2015-06-20</dueDate><amountOptions><option><amountName>Late Payment Fee</amountName><amountValue>40</amountValue></option><option><amountName>Fixed Charges</amountName><amountValue>50</amountValue></option><option><amountName>Additional Charges</amountName><amountValue>60</amountValue></option></amountOptions></billerResponse><additionalInfo><info><infoName>a</infoName><infoValue>10</infoValue></info><info><infoName>a b</infoName><infoValue>20</infoValue></info><info><infoName>a b c</infoName><infoValue>30</infoValue></info><info><infoName>a b c d</infoName><infoValue>40</infoValue></info></additionalInfo><amountInfo><amount>100000</amount><currency>356</currency><custConvFee>0</custConvFee><amountTags></amountTags>/amountInfo><paymentMethod><paymentMode>Cash</paymentMode><quickPay>N</quickPay><splitPay>N</splitPay></paymentMethod><paymentInfo><info><infoName>Remarks</infoName><infoValue>Received</infoValue></info></paymentInfo></billPaymentRequest>";
		
		String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<billPaymentRequest>\r\n" + 
				"    <agentId>CC01CC01513515340681</agentId>\r\n" + 
				"    <billerAdhoc>true</billerAdhoc>\r\n" + 
				"    <agentDeviceInfo>\r\n" + 
				"        <ip>192.168.2.73</ip>\r\n" + 
				"        <initChannel>AGT</initChannel>\r\n" + 
				"        <mac>01-23-45-67-89-ab</mac>\r\n" + 
				"    </agentDeviceInfo>\r\n" + 
				"    <customerInfo>\r\n" + 
				"        <customerMobile>9898990084</customerMobile>\r\n" + 
				"        <customerEmail></customerEmail>\r\n" + 
				"        <customerAdhaar></customerAdhaar>\r\n" + 
				"        <customerPan></customerPan>\r\n" + 
				"    </customerInfo>\r\n" + 
				"    <billerId>OTME00005XXZ43</billerId>\r\n" + 
				"   <inputParams>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a</paramName>\r\n" + 
				"         <paramValue>10</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b</paramName>\r\n" + 
				"         <paramValue>20</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c</paramName>\r\n" + 
				"         <paramValue>30</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d</paramName>\r\n" + 
				"         <paramValue>40</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d e</paramName>\r\n" + 
				"         <paramValue>50</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"   </inputParams>\r\n" + 
				"   <billerResponse>\r\n" + 
				"        <billAmount>100000</billAmount>\r\n" + 
				"        <billDate>2015-06-14</billDate>\r\n" + 
				"        <billNumber>12303</billNumber>\r\n" + 
				"        <billPeriod>june</billPeriod>\r\n" + 
				"        <customerName>BBPS</customerName>\r\n" + 
				"        <dueDate>2015-06-20</dueDate>\r\n" + 
				"        <amountOptions>\r\n" + 
				"            <option>\r\n" + 
				"                <amountName>Late Payment Fee</amountName>\r\n" + 
				"                <amountValue>40</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"            <option>\r\n" + 
				"                <amountName>Fixed Charges</amountName>\r\n" + 
				"                <amountValue>50</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"	    <option>\r\n" + 
				"                <amountName>Additional Charges</amountName>\r\n" + 
				"                <amountValue>60</amountValue>\r\n" + 
				"            </option>\r\n" + 
				"        </amountOptions>\r\n" + 
				"    </billerResponse>\r\n" + 
				"    <additionalInfo>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a</infoName>\r\n" + 
				"            <infoValue>10</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b</infoName>\r\n" + 
				"            <infoValue>20</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b c</infoName>\r\n" + 
				"            <infoValue>30</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>a b c d</infoName>\r\n" + 
				"            <infoValue>40</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"    </additionalInfo>\r\n" + 
				"    <amountInfo>\r\n" + 
				"        <amount>100000</amount>\r\n" + 
				"        <currency>356</currency>\r\n" + 
				"        <custConvFee>0</custConvFee>\r\n" + 
				"        <amountTags></amountTags>\r\n" + 
				"    </amountInfo>\r\n" + 
				"    <paymentMethod>\r\n" + 
				"        <paymentMode>Cash</paymentMode>\r\n" + 
				"        <quickPay>N</quickPay>\r\n" + 
				"        <splitPay>N</splitPay>\r\n" + 
				"    </paymentMethod>\r\n" + 
				"    <paymentInfo>\r\n" + 
				"        <info>\r\n" + 
				"            <infoName>Remarks</infoName>\r\n" + 
				"            <infoValue>Received</infoValue>\r\n" + 
				"        </info>\r\n" + 
				"    </paymentInfo>\r\n" + 
				"</billPaymentRequest>";
	  
	  PreRequestParam requestParam = new PreRequestParam();
	  requestParam.setRequest(request); 
	  requestParam.setRequestId(reqId);
	  requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
	  
	  String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
	  System.out.println("decryptedResponse\n" + decryptedResponse);
		
	}


	public String billFetch() {
		
		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		
		
		String request = "https://stgapi.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml";
	
		String reqId = TransactionManager.getNewTransactionId();
		
		//String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><billPaymentRequest><agentId>CC01CC01513515340681</agentId><billerAdhoc>true</billerAdhoc><agentDeviceInfo><ip>192.168.2.73</ip><initChannel>AGT</initChannel><mac>01-23-45-67-89-ab</mac></agentDeviceInfo><customerInfo><customerMobile>9898990084</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>OTME00005XXZ43</billerId><inputParams><input><paramName>a</paramName><paramValue>10</paramValue></input><input><paramName>a b</paramName><paramValue>20</paramValue></input><input><paramName>a b c</paramName><paramValue>30</paramValue></input><input><paramName>a b c d</paramName><paramValue>40</paramValue></input><input><paramName>a b c d e</paramName><paramValue>50</paramValue></input></inputParams><billerResponse><billAmount>100000</billAmount><billDate>2015-06-14</billDate><billNumber>12303</billNumber><billPeriod>june</billPeriod><customerName>BBPS</customerName><dueDate>2015-06-20</dueDate><amountOptions><option><amountName>Late Payment Fee</amountName><amountValue>40</amountValue></option><option><amountName>Fixed Charges</amountName><amountValue>50</amountValue></option><option><amountName>Additional Charges</amountName><amountValue>60</amountValue></option></amountOptions></billerResponse><additionalInfo><info><infoName>a</infoName><infoValue>10</infoValue></info><info><infoName>a b</infoName><infoValue>20</infoValue></info><info><infoName>a b c</infoName><infoValue>30</infoValue></info><info><infoName>a b c d</infoName><infoValue>40</infoValue></info></additionalInfo><amountInfo><amount>100000</amount><currency>356</currency><custConvFee>0</custConvFee><amountTags></amountTags>/amountInfo><paymentMethod><paymentMode>Cash</paymentMode><quickPay>N</quickPay><splitPay>N</splitPay></paymentMethod><paymentInfo><info><infoName>Remarks</infoName><infoValue>Received</infoValue></info></paymentInfo></billPaymentRequest>";
		
		String xmlRequestToString = "<billFetchRequest>\r\n" + 
				"   <agentId>CC01CC01513515340681</agentId>\r\n" + 
				"   <agentDeviceInfo>\r\n" + 
				"      <ip>192.168.2.73</ip>\r\n" + 
				"      <initChannel>AGT</initChannel>\r\n" + 
				"      <mac>01-23-45-67-89-ab</mac>\r\n" + 
				"   </agentDeviceInfo>\r\n" + 
				"   <customerInfo>\r\n" + 
				"      <customerMobile>9898990084</customerMobile>\r\n" + 
				"      <customerEmail></customerEmail>\r\n" + 
				"      <customerAdhaar></customerAdhaar>\r\n" + 
				"      <customerPan></customerPan>\r\n" + 
				"   </customerInfo>\r\n" + 
				"   <billerId>OTME00005XXZ43</billerId>\r\n" + 
				"   <inputParams>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a</paramName>\r\n" + 
				"         <paramValue>10</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b</paramName>\r\n" + 
				"         <paramValue>20</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c</paramName>\r\n" + 
				"         <paramValue>30</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d</paramName>\r\n" + 
				"         <paramValue>40</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"      <input>\r\n" + 
				"         <paramName>a b c d e</paramName>\r\n" + 
				"         <paramValue>50</paramValue>\r\n" + 
				"      </input>\r\n" + 
				"   </inputParams>\r\n" + 
				"</billFetchRequest>";
	  
	  PreRequestParam requestParam = new PreRequestParam();
	  requestParam.setRequest(request); 
	  requestParam.setRequestId(reqId);
	  requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
	  
	  String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
	  System.out.println("decryptedResponse\n" + decryptedResponse);
		
	
	return reqId;
	}

public void complaintRegistration() {
		
		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		
		
		String request = "https://stgapi.billavenue.com/billpay/extComplaints/register/xml";
		
		String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><complaintRegistrationReq>    <complaintType>Transaction</complaintType>    <complaintDesc>showing sucess but not update bill at biller portel</complaintDesc>    <txnRefId>CC01ABS70001</txnRefId>    <complaintDisposition>Transaction Successful, account not updated</complaintDisposition></complaintRegistrationReq>";
		  
	  String reqId = TransactionManager.getNewTransactionId();
	  PreRequestParam requestParam = new PreRequestParam();
	  requestParam.setRequest(request); 
	  requestParam.setRequestId(reqId);
	  requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
	  
	  String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
	  System.out.println("decryptedResponse\n" + decryptedResponse);
		
	}
	
}