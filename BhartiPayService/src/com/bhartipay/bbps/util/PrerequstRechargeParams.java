package com.bhartipay.bbps.util;

import com.bhartipay.bbps.BbpsProperties;

public class PrerequstRechargeParams {


	
//	agentid=CC01CC01513515340681
	private String request;
	final private String accessCode=BbpsProperties.getAccessCode();
	private String requestId;
	private String encRequest;
	final private String ver=BbpsProperties.getVer();
	final private String instituteId=BbpsProperties.getAgentInstitutionID();
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getAccessCode() {
		return accessCode;
	}

	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getEncRequest() {
		return encRequest;
	}
	public void setEncRequest(String encRequest) {
		this.encRequest = encRequest;
	}
	public String getVer() {
		return ver;
	}

	public String getInstituteId() {
		return instituteId;
	}
	
	public String getUrlParams() {
//		accessCode=XXXX&encRequest=XXXX&requestId=XXXX&ver=XXXX&instituteId=XXXX
		StringBuffer baseUrl = new StringBuffer();
		baseUrl.append("accessCode=" + accessCode);
		baseUrl.append("&");
		baseUrl.append("requestId=BPA"+requestId+requestId);
		baseUrl.append("&");
		baseUrl.append("encRequest=" + encRequest);
		baseUrl.append("&");
		baseUrl.append("ver=" + ver);
		baseUrl.append("&");
		baseUrl.append("instituteId=" + instituteId);
		
		String urlParameters = baseUrl.toString();
		
		return urlParameters;
	}
	
}
