package com.bhartipay.bbps.util;

import java.util.TreeMap;

import com.bhartipay.bbps.BbpsProperties;
import com.ccavenue.security.AesCryptUtil;

public class CCAVencrypt {
	
	public static String getEncryptedString(String strXML) {
			
		final String key = BbpsProperties.getWorkingKey();

		System.out.println("encryptData Length : "+strXML.length());
        
        AesCryptUtil acu = new AesCryptUtil(key);
        String strEncrypt = acu.encrypt(strXML);
        System.out.println("Request encrypted successfully.");
        
        return strEncrypt;
	}
	
	public static String getRechargeEncryptedString(String strXML) {
		
//		final String key = "9012ED5ABB24360BA57178E0BF41BD4D";
		final String key = BbpsProperties.getWorkingKey();
		System.out.println("encryptData Length : "+strXML.length());
        
        AesCryptUtil acu = new AesCryptUtil(key);
        String strEncrypt = acu.encrypt(strXML);
        System.out.println("Request encrypted successfully.");
        
        return strEncrypt;
	}
	public static void main(String[] args) {
		TreeMap<String, String> map=new TreeMap<String, String>();
		map.put("Kolkata","Kolkata");
		map.put("Uttar Pradesh(West)","Uttar Pradesh (West)");
		map.put("Mumbai","Mumbai");
		map.put("Uttar Pradesh (East)","Uttar Pradesh (East)");
		map.put("Delhi","Delhi");
		map.put("Rajasthan","Rajasthan");
		map.put("Chennai","Chennai");
		map.put("Madhya Pradesh","Madhya Pradesh");
		map.put("Maharashtra","Maharashtra");
		map.put("WestBengal & AN Island","WestBengal & AN Island");
		map.put("Gujrat","Gujrat");
		map.put("Bihar & Jharkhand","Bihar & Jharkhand");
		map.put("Andhra Pradesh","Andhra Pradesh");
		map.put("Orissa","Orissa");
		map.put("Karnataka","Karnataka");
		map.put("Assam","Assam");
		map.put("Tamilnadu","Tamilnadu");
		map.put("NorthEast","NorthEast");
		map.put("Kerala","Kerala");
		map.put("J&K","J&K");
		map.put("Punjab","Punjab");
		map.put("Uttaranchal","Uttaranchal");
		map.put("Haryana","Haryana");
		map.put("Himachal Pradesh","Himachal Pradesh");
		
		System.out.println(map);
	}
}
