package com.bhartipay.bbps.util;

import com.bhartipay.bbps.BbpsProperties;

public class PreRequestParam {
	
	private String request;
	final private String accessCode=BbpsProperties.getAccessCode();
	private String requestId;
	private String encRequest;
	final private String ver=BbpsProperties.getVer();
	final private String instituteId=BbpsProperties.getAgentInstitutionID();
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getAccessCode() {
		return accessCode;
	}

	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getEncRequest() {
		return encRequest;
	}
	public void setEncRequest(String encRequest) {
		this.encRequest = encRequest;
	}
	public String getVer() {
		return ver;
	}

	public String getInstituteId() {
		return instituteId;
	}
	
	public String getUrlParams() {
		
		StringBuffer baseUrl = new StringBuffer();
		baseUrl.append("accessCode=" + accessCode);
		baseUrl.append("&");
		baseUrl.append("requestId=9012ED5ABB24360BA57" + requestId);
		baseUrl.append("&");
		baseUrl.append("encRequest=" + encRequest);
		baseUrl.append("&");
		baseUrl.append("ver=" + ver);
		baseUrl.append("&");
		baseUrl.append("instituteId=" + instituteId);
		
		String urlParameters = baseUrl.toString();
		
		return urlParameters;
	}
	
	public String getCompleteRequestId() {
		return "9012ED5ABB24360BA57"+requestId;
	}
	
	
	public String getNewUrlParams() {
		
		StringBuffer baseUrl = new StringBuffer();
		baseUrl.append("accessCode=" + accessCode);
		baseUrl.append("&");
		baseUrl.append("requestId=9012ED5ABB24360BA57" + requestId);
		baseUrl.append("&");
		baseUrl.append("ver=" + ver);
		baseUrl.append("&");
		baseUrl.append("instituteId=" + instituteId);
		//baseUrl.append("&");
		//baseUrl.append("encRequest=" + encRequest);
		
		String urlParameters = baseUrl.toString();
		
		return urlParameters;
	}
	
	
}
