package com.bhartipay.bbps.util.Scheduler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzSchedulerListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
		String groupName = "callBillerAPI";
		
		JobDetail job = JobBuilder.newJob(SchedulerJob.class).withIdentity("loadBillerListXml", groupName).build();

			try {

				Trigger trigger = TriggerBuilder.newTrigger().withIdentity("loadBillerListXml Every 6 hours starting at 00am", groupName).withSchedule(
				     CronScheduleBuilder.cronSchedule("0 0 0,6,12,18 ? * * *"))
					//	CronScheduleBuilder.cronSchedule("0 32 0,19 ? * * *"))
						.build();  // 0 13 * * *
				
				Scheduler scheduler = new StdSchedulerFactory().getScheduler();
				scheduler.start();
				scheduler.scheduleJob(job, trigger);

			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		
	}

	
}
