package com.bhartipay.bbps.util.Scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.bhartipay.bbps.service.BbpsBillerService;
import com.bhartipay.bbps.service.impl.BillerBbpsServiceImpl;

public class SchedulerJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub

		try {
			BbpsBillerService billerBbpsService = new BillerBbpsServiceImpl();
			billerBbpsService.loadAllBillerCategory();
			
			System.out.println("SchedulerJob quartz");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
