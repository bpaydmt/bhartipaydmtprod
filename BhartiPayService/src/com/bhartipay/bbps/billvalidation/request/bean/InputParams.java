package com.bhartipay.bbps.billvalidation.request.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "inputParams")
@XmlAccessorType(XmlAccessType.FIELD)
public class InputParams {
	
	@XmlElement(name = "input")
	private List<Input> input;

	public List<Input> getInput() {
		return input;
	}

	public void setInput(List<Input> input) {
		this.input = input;
	}
	
}