package com.bhartipay.bbps.billvalidation.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "input")
@XmlAccessorType(XmlAccessType.FIELD)
public class Input {
	
	@XmlElement(name = "paramName")
	private String paramName;

	@XmlElement(name = "paramValue")
	private String paramValue;

    public String getParamName ()
    {
        return paramName;
    }

    public void setParamName (String paramName)
    {
        this.paramName = paramName;
    }

    public String getParamValue ()
    {
        return paramValue;
    }

    public void setParamValue (String paramValue)
    {
        this.paramValue = paramValue;
    }
}
