package com.bhartipay.bbps.billvalidation.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billValidationRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillValidationRequest {
    
	@XmlElement(name = "billerId")
	private String billerId;

	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "inputParams")
	private InputParams inputParams = new InputParams();

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }

    public InputParams getInputParams ()
    {
        return inputParams;
    }

    public void setInputParams (InputParams inputParams)
    {
        this.inputParams = inputParams;
    }
}
