package com.bhartipay.bbps.billvalidation.response.error.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billValidationResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillValidationResponse {
	
	@XmlElement(name = "responseReason")
	private String responseReason;

	@XmlElement(name = "complianceReason")
	private String complianceReason;

	@XmlElement(name = "complianceCode")
	private String complianceCode;

	@XmlElement(name = "responseCode")
	private String responseCode;

    public String getResponseReason ()
    {
        return responseReason;
    }

    public void setResponseReason (String responseReason)
    {
        this.responseReason = responseReason;
    }

    public String getComplianceReason ()
    {
        return complianceReason;
    }

    public void setComplianceReason (String complianceReason)
    {
        this.complianceReason = complianceReason;
    }

    public String getComplianceCode ()
    {
        return complianceCode;
    }

    public void setComplianceCode (String complianceCode)
    {
        this.complianceCode = complianceCode;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }
    
}
