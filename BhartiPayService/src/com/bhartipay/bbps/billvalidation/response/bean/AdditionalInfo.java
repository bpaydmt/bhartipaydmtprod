package com.bhartipay.bbps.billvalidation.response.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "additionalInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalInfo {
	
	@XmlElement(name = "info")
	private List<Info> info;

	public List<Info> getInfo() {
		return info;
	}

	public void setInfo(List<Info> info) {
		this.info = info;
	}

}
