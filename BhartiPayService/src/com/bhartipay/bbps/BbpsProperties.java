package com.bhartipay.bbps;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class BbpsProperties {
	
	private static Properties prop = loadProperties();
	
	private static Properties loadProperties() {
		Properties prop = new Properties();
		String filePath = "/bhartipay/bbps/bbpsConfig.properties";	//For Live OR UAT
 		//String filePath = "D://bhartipay/bbpsConfig.properties";	//For Local
 	
		try{
			InputStream in = new FileInputStream(filePath);
			prop.load(in);
			in.close();
		}catch(Exception e){
			System.out.println(e);
		}
		return prop;
	}
	
	public static String getWorkingKey(){
		return prop.getProperty("WorkingKey");
	}
	
	public static String getAgentInstitutionID(){
		return prop.getProperty("AgentInstitutionID");
	}
	
	public static String getAccessCode(){
		return prop.getProperty("AccessCode");
	}
	
	public static String getVer(){
		return prop.getProperty("ver");
	}
	
	public static String getBillerMDM(){
		return prop.getProperty("BillerMDM");
	}
	
	public static String getBillFetch(){
		return prop.getProperty("BillFetch");
	}
	
	public static String getBillValidation(){
		return prop.getProperty("BillValidation");
	}

	public static String getBillPayment(){
		return prop.getProperty("BillPayment");
	}
	
	public static String getComplaintRegistration(){
		return prop.getProperty("ComplaintRegistration");
	}
	
	public static String getComplaintTracking(){
		return prop.getProperty("ComplaintTracking");
	}
	
	public static String getTransactionStatus(){
		return prop.getProperty("TransactionStatus");
	}
	
	public static String getDepositEnquiry(){
		return prop.getProperty("DepositEnquiry");
	}
	
	public static String getBillersXmlFilePath(){
		return prop.getProperty("BillersXmlFilePath");
	}
	
	public static String getBillerInfoAPI(){
		return prop.getProperty("biller.info");
	}
	
	public static String getBillPaymentAPI(){
		return prop.getProperty("bill.payment");
	}
	
	public static String getBillerTransactionAPI(){
		return prop.getProperty("transaction.status");
	}
	
	public static String getBillerDepositEnquiry(){
		return prop.getProperty("deposit.enquiry");
	}
	
	
	
}