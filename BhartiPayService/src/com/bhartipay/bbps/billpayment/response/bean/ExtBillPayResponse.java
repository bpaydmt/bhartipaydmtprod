package com.bhartipay.bbps.billpayment.response.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ExtBillPayResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtBillPayResponse {

	@XmlElement(name = "responseCode")
	private String responseCode;

	@XmlElement(name = "responseReason")
	private String responseReason;
	
	@XmlElement(name = "txnRefId")
	private String txnRefId;
	
	@XmlElement(name = "txnRespType")
	private String txnRespType;
	
	@XmlElement(name = "inputParams")
	InputParams inputParams = new InputParams();
	
	@XmlElement(name = "CustConvFee")
	private String CustConvFee;
	
	@XmlElement(name = "RespAmount")
	private String RespAmount;
	
	@XmlElement(name = "RespBillDate")
	private String RespBillDate;
	
	@XmlElement(name = "RespBillNumber")
	private String RespBillNumber;
	
	@XmlElement(name = "RespBillPeriod")
	private String RespBillPeriod;
	
	@XmlElement(name = "RespCustomerName")
	private String RespCustomerName;
	
	@XmlElement(name = "RespDueDate")
	private String RespDueDate;
	
	@XmlElement(name = "approvalRefNumber")
	private String approvalRefNumber;
	

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseReason() {
		return responseReason;
	}

	public void setResponseReason(String responseReason) {
		this.responseReason = responseReason;
	}

	public String getTxnRefId() {
		return txnRefId;
	}

	public void setTxnRefId(String txnRefId) {
		this.txnRefId = txnRefId;
	}

	public String getTxnRespType() {
		return txnRespType;
	}

	public void setTxnRespType(String txnRespType) {
		this.txnRespType = txnRespType;
	}

	public InputParams getInputParams() {
		return inputParams;
	}

	public void setInputParams(InputParams inputParams) {
		this.inputParams = inputParams;
	}

	public String getCustConvFee() {
		return CustConvFee;
	}

	public void setCustConvFee(String custConvFee) {
		CustConvFee = custConvFee;
	}

	public String getRespAmount() {
		return RespAmount;
	}

	public void setRespAmount(String respAmount) {
		RespAmount = respAmount;
	}

	public String getRespBillDate() {
		return RespBillDate;
	}

	public void setRespBillDate(String respBillDate) {
		RespBillDate = respBillDate;
	}

	public String getRespBillNumber() {
		return RespBillNumber;
	}

	public void setRespBillNumber(String respBillNumber) {
		RespBillNumber = respBillNumber;
	}

	public String getRespBillPeriod() {
		return RespBillPeriod;
	}

	public void setRespBillPeriod(String respBillPeriod) {
		RespBillPeriod = respBillPeriod;
	}

	public String getRespCustomerName() {
		return RespCustomerName;
	}

	public void setRespCustomerName(String respCustomerName) {
		RespCustomerName = respCustomerName;
	}

	public String getRespDueDate() {
		return RespDueDate;
	}

	public void setRespDueDate(String respDueDate) {
		RespDueDate = respDueDate;
	}

	public String getApprovalRefNumber() {
		return approvalRefNumber;
	}

	public void setApprovalRefNumber(String approvalRefNumber) {
		this.approvalRefNumber = approvalRefNumber;
	}

}
