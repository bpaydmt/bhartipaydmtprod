package com.bhartipay.bbps.billpayment.response.bean;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "inputParams")
@XmlAccessorType(XmlAccessType.FIELD)
public class InputParams {

	@XmlElement(name = "input")
	List<Input> inputList = new ArrayList<Input>();

	public List<Input> getInputList() {
		return inputList;
	}

	public void setInputList(List<Input> inputList) {
		this.inputList = inputList;
	}

}
