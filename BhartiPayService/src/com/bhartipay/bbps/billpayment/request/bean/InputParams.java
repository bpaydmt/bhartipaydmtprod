package com.bhartipay.bbps.billpayment.request.bean;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "inputParams")
@XmlAccessorType(XmlAccessType.FIELD)
public class InputParams {

	@XmlElement(name = "input")
	List<Input> input = new ArrayList<Input>();

	public List<Input> getInput() {
		return input;
	}

	public void setInput(List<Input> input) {
		this.input = input;
	}

}
