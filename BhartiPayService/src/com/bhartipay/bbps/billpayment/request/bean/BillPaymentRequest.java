package com.bhartipay.bbps.billpayment.request.bean;

public class BillPaymentRequest extends BillPaymentSuperRequest{
	
	private String aggreatorId;
	
	private String welletId;
	
	private String agentidDmt;
	
	private String billerName;
	
	private String billerType;

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getWelletId() {
		return welletId;
	}

	public void setWelletId(String welletId) {
		this.welletId = welletId;
	}

	public String getAgentidDmt() {
		return agentidDmt;
	}

	public void setAgentidDmt(String agentidDmt) {
		this.agentidDmt = agentidDmt;
	}

	public String getBillerName() {
		return billerName;
	}

	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}

	public String getBillerType() {
		return billerType;
	}

	public void setBillerType(String billerType) {
		this.billerType = billerType;
	}
}
