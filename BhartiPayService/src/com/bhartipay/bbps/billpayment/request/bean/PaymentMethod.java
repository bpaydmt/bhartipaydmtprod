package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PaymentMethod")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethod {

	@XmlElement(name = "paymentMode")
	private String paymentMode;

	@XmlElement(name = "quickPay")
	private String quickPay;

	@XmlElement(name = "splitPay")
	private String splitPay;

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getQuickPay() {
		return quickPay;
	}

	public void setQuickPay(String quickPay) {
		this.quickPay = quickPay;
	}

	public String getSplitPay() {
		return splitPay;
	}

	public void setSplitPay(String splitPay) {
		this.splitPay = splitPay;
	}

}
