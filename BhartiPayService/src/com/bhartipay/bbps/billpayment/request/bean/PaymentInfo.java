package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paymentInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentInfo {

	@XmlElement(name = "info")
	Info info = new Info();

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}
}
