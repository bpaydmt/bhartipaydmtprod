package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billerResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillerResponse {

	@XmlElement(name = "billAmount")
	private String billAmount;

	@XmlElement(name = "billDate")
	private String billDate;

	@XmlElement(name = "billNumber")
	private String billNumber;

	@XmlElement(name = "billPeriod")
	private String billPeriod;

	@XmlElement(name = "customerName")
	private String customerName;

	@XmlElement(name = "dueDate")
	private String dueDate;

	@XmlElement(name = "amountOptions")
	AmountOptions amountOptions = new AmountOptions();

	public String getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillPeriod() {
		return billPeriod;
	}

	public void setBillPeriod(String billPeriod) {
		this.billPeriod = billPeriod;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public AmountOptions getAmountOptions() {
		return amountOptions;
	}

	public void setAmountOptions(AmountOptions amountOptions) {
		this.amountOptions = amountOptions;
	}

}
