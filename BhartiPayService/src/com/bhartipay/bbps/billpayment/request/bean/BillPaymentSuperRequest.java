package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billPaymentRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillPaymentSuperRequest {

	@XmlElement(name = "agentId")
	private String agentId;

	@XmlElement(name = "billerAdhoc")
	private String billerAdhoc;

	@XmlElement(name = "agentDeviceInfo")
	AgentDeviceInfo agentDeviceInfo = new AgentDeviceInfo();

	@XmlElement(name = "customerInfo")
	CustomerInfo customerInfo = new CustomerInfo();

	@XmlElement(name = "billerId")
	private String billerId;

	@XmlElement(name = "inputParams")
	InputParams inputParams = new InputParams();

	@XmlElement(name = "billerResponse")
	BillerResponse billerResponse = new BillerResponse();

	@XmlElement(name = "additionalInfo")
	AdditionalInfo additionalInfo = new AdditionalInfo();

	@XmlElement(name = "amountInfo")
	AmountInfo amountInfo = new AmountInfo();

	@XmlElement(name = "paymentInfo")
	PaymentInfo paymentInfo = new PaymentInfo();

	@XmlElement(name = "paymentMethod")
	PaymentMethod paymentMethod = new PaymentMethod();
	
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBillerAdhoc() {
		return billerAdhoc;
	}

	public void setBillerAdhoc(String billerAdhoc) {
		this.billerAdhoc = billerAdhoc;
	}

	public AgentDeviceInfo getAgentDeviceInfo() {
		return agentDeviceInfo;
	}

	public void setAgentDeviceInfo(AgentDeviceInfo agentDeviceInfo) {
		this.agentDeviceInfo = agentDeviceInfo;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public InputParams getInputParams() {
		return inputParams;
	}

	public void setInputParams(InputParams inputParams) {
		this.inputParams = inputParams;
	}

	public BillerResponse getBillerResponse() {
		return billerResponse;
	}

	public void setBillerResponse(BillerResponse billerResponse) {
		this.billerResponse = billerResponse;
	}

	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public AmountInfo getAmountInfo() {
		return amountInfo;
	}

	public void setAmountInfo(AmountInfo amountInfo) {
		this.amountInfo = amountInfo;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}
