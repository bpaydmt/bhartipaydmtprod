package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "info")
@XmlAccessorType(XmlAccessType.FIELD)
public class Info {

	@XmlElement(name = "infoName")
	private String infoName;

	@XmlElement(name = "infoValue")
	private String infoValue;

	public String getInfoName() {
		return infoName;
	}

	public void setInfoName(String infoName) {
		this.infoName = infoName;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

}
