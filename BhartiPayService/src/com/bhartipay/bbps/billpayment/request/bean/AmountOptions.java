package com.bhartipay.bbps.billpayment.request.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bhartipay.bbps.billpayment.request.bean.Option;

@XmlRootElement(name = "amountOptions")
@XmlAccessorType(XmlAccessType.FIELD)
public class AmountOptions {

	@XmlElement(name = "option")
	List<Option> option = new ArrayList<Option>();

	public List<Option> getOption() {
		return option;
	}

	public void setOption(List<Option> option) {
		this.option = option;
	}

}
