package com.bhartipay.bbps.billpayment.request.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "amountInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AmountInfo {

	@XmlElement(name = "amount")
	private String amount;

	@XmlElement(name = "currency")
	private String currency;

	@XmlElement(name = "custConvFee")
	private String custConvFee;

	@XmlElement(name = "amountTags")
	private String amountTags;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCustConvFee() {
		return custConvFee;
	}

	public void setCustConvFee(String custConvFee) {
		this.custConvFee = custConvFee;
	}

	public String getAmountTags() {
		return amountTags;
	}

	public void setAmountTags(String amountTags) {
		this.amountTags = amountTags;
	}

}