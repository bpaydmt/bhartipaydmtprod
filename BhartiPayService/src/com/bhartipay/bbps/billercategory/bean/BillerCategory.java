package com.bhartipay.bbps.billercategory.bean;

public class BillerCategory {
	
	private String billerCategory;
	
	private String billerId;

	public String getBillerCategory() {
		return billerCategory;
	}

	public void setBillerCategory(String billerCategory) {
		this.billerCategory = billerCategory;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}
	
}
