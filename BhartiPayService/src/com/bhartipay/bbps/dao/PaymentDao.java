package com.bhartipay.bbps.dao;

import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.lean.bean.AgentBillPayRequest;

public interface PaymentDao {
	
	public boolean saveOrUpdatePaymentDetails(BbpsPayment bbpsPayment);
	
	public boolean getBbpsAgentRequest(BbpsPayment bbpsPayment);
	public boolean saveBbpsAgentRequest(AgentBillPayRequest bbpsPaymentReq);

	public boolean getBbpsAgentRequest1(BbpsPayment bbpsPayment);
}
