package com.bhartipay.bbps.dao.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.dao.PaymentDao;
import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.lean.bean.AgentBillPayRequest;
import com.bhartipay.remittence.service.dao.RemittenceDaoImpl;
import com.bhartipay.util.DBUtil;

public class PaymentDaoImpl implements PaymentDao {

	public static final Logger LOGGER = Logger.getLogger(PaymentDaoImpl.class.getName());
	SessionFactory factory;
	Transaction transaction = null;
	Session session = null;
	
	@Override
	public boolean saveOrUpdatePaymentDetails(BbpsPayment bbpsPayment) {
		// TODO Auto-generated method stub
		
		boolean isBbpsPaymentSave = false;
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|");
			
		if(bbpsPayment != null) {
			try{
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|Try to save payment Details.");
				factory = DBUtil.getSessionFactory();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|Got the SessionFactory.");
				session = factory.openSession();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|Session open.");
				transaction=session.beginTransaction();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|Transaction begin.");
				session.saveOrUpdate(bbpsPayment);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|payment Details saved.");
				transaction.commit();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(), "",bbpsPayment.getTxnid(), "|savePaymentDetails()|Transaction commited.");
				isBbpsPaymentSave = true;
			} catch (Exception e) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(),"",bbpsPayment.getTxnid(),"|problem in savePaymentDetails "+e.getMessage()+" "+e);
				e.printStackTrace();
				transaction.rollback();
			} finally {
				session.close();
			}
		}
		return isBbpsPaymentSave;
	}

	@Override
	public boolean getBbpsAgentRequest1(BbpsPayment bbpsPayment) {
		// TODO Auto-generated method stub
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		boolean bbpsRequest = false;
		String resp="FAIL";
		try {System.out.println("HelloooooooooooooSSS");
		   SQLQuery query = session.createSQLQuery("select count(*) from agentbillpayrequest where userId='" + bbpsPayment.getAgentid()
					+ "' and billerId='"+bbpsPayment.getBillerid()+"' and txnAmount='"+bbpsPayment.getTxnamount()+"'  and creationDate  > DATE_SUB(NOW(), INTERVAL 10 MINUTE) ");
		   System.out.println("HelloooooooooooooSSS before");
		   int count = ((BigInteger) query.uniqueResult()).intValue();
		   System.out.println("HelloooooooooooooSSS after "+count);
		   if (count > 0) {
				resp = "You have already processed transaction with same details please wait for 10 minutes.";
				 System.out.println("HelloooooooooooooSSS after inside");
				bbpsRequest=false;
			} else {
				resp = "SUCCESS";
				 System.out.println("HelloooooooooooooSSS after inside  new new");
			bbpsRequest=true;
			}
			
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(),"",bbpsPayment.getTxnid(),"|problem in savePaymentDetails "+e.getMessage()+" "+e);
			e.printStackTrace();
			transaction.rollback();
			bbpsRequest=false;
			System.out.println("HelloooooooooooooSSS errrrrrrr");
		} finally {
			session.close();
		}
		
		
		return bbpsRequest;
	}

	@Override
	public boolean saveBbpsAgentRequest(AgentBillPayRequest request) {
		// TODO Auto-generated method stub
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		boolean bbpsRequest = false;
		try {
			if(request!=null)
			{
			session.save(request);	
			transaction.commit();
			bbpsRequest = true;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			bbpsRequest=false;
		} finally {
			session.close();
		}
		
		
		return bbpsRequest;
	}

	
	@Override
	public boolean getBbpsAgentRequest(BbpsPayment bbpsPayment) {
		// TODO Auto-generated method stub
		factory = DBUtil.getSessionFactory();
		session = factory.openSession();
		transaction=session.beginTransaction();
		boolean bbpsRequest = false;
		String resp="FAIL";
		try {
			 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			 String dateString = format.format( new Date(System.currentTimeMillis()));
			SQLQuery query = session.createSQLQuery("select count(*) from agentbillpayrequest where userId='" + bbpsPayment.getAgentid()
					+ "' and billerId='"+bbpsPayment.getBillerid()+"' and requestDate='"+dateString+"' and txnAmount='"+bbpsPayment.getTxnamount()+"'  and caNumber='"+bbpsPayment.getConsumerNumber()+"'   and creationDate  > DATE_SUB(NOW(), INTERVAL 10 MINUTE) ");
		   int count = ((BigInteger) query.uniqueResult()).intValue();
		   if (count > 0) {
				resp = "You have already processed transaction with same details please wait for 10 minutes.";
				bbpsRequest=false;
			} else {
				resp = "SUCCESS";
			bbpsRequest=true;
			}
			
			
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),bbpsPayment.getAggreatorid(),bbpsPayment.getWalletid(),bbpsPayment.getAgentid(),"",bbpsPayment.getTxnid(),"|problem in savePaymentDetails "+e.getMessage()+" "+e);
			e.printStackTrace();
			transaction.rollback();
			bbpsRequest=false;
		} finally {
			session.close();
		}
		
		
		return bbpsRequest;
	}

	
	
	
}
