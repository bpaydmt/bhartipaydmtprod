package com.bhartipay.bbps.service.impl;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.bhartipay.Logger.Commonlogger;
import com.bhartipay.bbps.BbpsProperties;
import com.bhartipay.bbps.billercategory.bean.BillerCategory;
import com.bhartipay.bbps.billerinfo.bean.Biller;
import com.bhartipay.bbps.billerinfo.bean.BillerInfoRequest;
import com.bhartipay.bbps.billerinfo.bean.BillerInfoResponse;
import com.bhartipay.bbps.billerinfo.bean.ParamInfo;
import com.bhartipay.bbps.billerinfo.bean.PaymentChannelInfo;
import com.bhartipay.bbps.billfetch.bean.BillFetchRequest;
import com.bhartipay.bbps.billfetch.response.bean.BillFetchResponse;
import com.bhartipay.bbps.billfetch.response.bean.BillerResponse;
import com.bhartipay.bbps.billfetch.response.bean.Info;
import com.bhartipay.bbps.billfetch.response.bean.Input;
import com.bhartipay.bbps.billfetch.response.bean.Option;
import com.bhartipay.bbps.billpayment.request.bean.AmountInfo;
import com.bhartipay.bbps.billpayment.request.bean.BillPaymentRequest;
import com.bhartipay.bbps.billpayment.request.bean.BillPaymentSuperRequest;
import com.bhartipay.bbps.billpayment.request.bean.PaymentMethod;
import com.bhartipay.bbps.billpayment.response.bean.ExtBillPayResponse;
import com.bhartipay.bbps.billvalidation.request.bean.BillValidationRequest;
import com.bhartipay.bbps.billvalidation.request.bean.InputParams;
import com.bhartipay.bbps.billvalidation.response.bean.BillValidationResponse;
import com.bhartipay.bbps.complaint.request.bean.ComplaintRegistrationReq;
import com.bhartipay.bbps.complaint.request.bean.ComplaintTrackingReq;
import com.bhartipay.bbps.complaint.response.bean.ComplaintRegistrationResp;
import com.bhartipay.bbps.complaint.response.bean.ComplaintTrackingResp;
import com.bhartipay.bbps.dao.PaymentDao;
import com.bhartipay.bbps.dao.impl.PaymentDaoImpl;
import com.bhartipay.bbps.entities.BbpsPayment;
import com.bhartipay.bbps.service.BbpsBillerService;
import com.bhartipay.bbps.transactionstatus.request.bean.TransactionStatusReq;
import com.bhartipay.bbps.transactionstatus.response.bean.TransactionStatusResp;
import com.bhartipay.bbps.transactionstatus.response.bean.TxnList;
import com.bhartipay.bbps.util.BbpsApiRunner;
import com.bhartipay.bbps.util.CCAVencrypt;
import com.bhartipay.bbps.util.PreRequestParam;
import com.bhartipay.bbps.util.PrerequstRechargeParams;
import com.bhartipay.bbps.util.TransactionManager;
import com.bhartipay.biller.util.XmlToStringAndStringToXml;
import com.bhartipay.lean.bean.AgentBillPayRequest;
import com.bhartipay.lean.bean.BbpsComplaint;
import com.bhartipay.lean.bean.BbpsStatusUpdateResponse;
import com.bhartipay.lean.bean.LeanAccount;
import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.transaction.persistence.TransactionDao;
import com.bhartipay.transaction.persistence.TransactionDaoImpl;
import com.bhartipay.util.DBUtil;
import com.bhartipay.util.SmsAndMailUtility;
import com.bhartipay.util.ThreadUtil;
import com.bhartipay.util.persistence.CommanUtilDao;
import com.bhartipay.util.persistence.CommanUtilDaoImpl;
import com.google.gson.Gson;

public class BillerBbpsServiceImpl implements BbpsBillerService {

	static BillerInfoResponse billerInfoResponseForCategory = null;
	
	static String fetchedRequestID = null;

	@Override
	public Set<String> getAllBillerCategory() {
		// TODO Auto-generated method stub

		Set<String> billerCategoryList = null;
		
		XmlToStringAndStringToXml xmlToString = new XmlToStringAndStringToXml();
		System.out.println(" BILLER XML FILE PATH  "+BbpsProperties.getBillersXmlFilePath());
		String decryptedResponse = xmlToString.getBillersListFromXml(BbpsProperties.getBillersXmlFilePath());
		System.out.println("decryptedResponse   "+ decryptedResponse);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(BillerInfoResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			billerInfoResponseForCategory = (BillerInfoResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));

			billerCategoryList = new TreeSet<String>();
			for (Biller biller : billerInfoResponseForCategory.getBiller()) {
				if(biller.getBillerCategory().equalsIgnoreCase("DTH"))
					billerCategoryList.add(biller.getBillerCategory());
				else
					billerCategoryList.add(capitalizer(biller.getBillerCategory()));
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return billerCategoryList;
	}

	@Override
	public Map<String, String> getBillerListByCategory(String category) {
		// TODO Auto-generated method stub

		Map<String, String> billerListByCategory = null;
		billerListByCategory = new TreeMap<String, String>();
		
		Gson g = new Gson();
		BillerCategory billerCategory = g.fromJson(category, BillerCategory.class);
		
		for (Biller biller : billerInfoResponseForCategory.getBiller()) {

			if (biller.getBillerCategory().equalsIgnoreCase(billerCategory.getBillerCategory()))
				billerListByCategory.put(biller.getBillerId(), biller.getBillerName());
		}
		return billerListByCategory;
	}

	@Override
	public String getCustomParam(String billerID) {
		// TODO Auto-generated method stub

		String billerParam = null;

		try {
			
			Gson g = new Gson();
			BillerCategory billerCategory = g.fromJson(billerID, BillerCategory.class);

			JSONObject jObj = new JSONObject();

			for (Biller biller : billerInfoResponseForCategory.getBiller()) {
				if (biller.getBillerId().equals(billerCategory.getBillerId())) {

					jObj.put("billerId", biller.getBillerId());
					jObj.put("billerName", biller.getBillerName());
					jObj.put("billerCategory", biller.getBillerCategory());
					jObj.put("billerAdhoc", biller.getBillerAdhoc());
					jObj.put("billerCoverage", biller.getBillerCoverage());
					jObj.put("billerFetchRequiremet", biller.getBillerFetchRequiremet());

					jObj.put("billerPaymentExactness", biller.getBillerPaymentExactness());
					jObj.put("billerSupportBillValidation", biller.getBillerSupportBillValidation());
					jObj.put("supportPendingStatus", biller.getSupportPendingStatus());
					jObj.put("supportDeemed", biller.getSupportDeemed());
					jObj.put("billerTimeout", biller.getBillerTimeout());

					JSONArray paramInfoArray = new JSONArray();
					for (ParamInfo paramInfo : biller.getBillerInputParams().getParamInfo()) {
						JSONObject paramInfoJSONObj = new JSONObject();
						paramInfoJSONObj.put("paramName", paramInfo.getParamName());
						paramInfoJSONObj.put("dataType", paramInfo.getDataType());
						paramInfoJSONObj.put("isOptional", paramInfo.getIsOptional());
						paramInfoJSONObj.put("minLength", paramInfo.getMinLength());
						paramInfoJSONObj.put("maxLength", paramInfo.getMaxLength());
						paramInfoJSONObj.put("regEx", paramInfo.getRegEx());
						paramInfoArray.put(paramInfoJSONObj.toString());
					}
					jObj.put("billerInputParams", paramInfoArray);

//					jObj.put("billerPaymentModes", biller.getBillerPaymentModes());
					jObj.put("billerPaymentModes", "Cash");
					
					jObj.put("billerDescription", biller.getBillerDescription());
					jObj.put("rechargeAmountInValidationRequest", biller.getRechargeAmountInValidationRequest());

					JSONArray channelInfoArray = new JSONArray();
					for (PaymentChannelInfo paymentChannelInfo : biller.getBillerPaymentChannels()
							.getPaymentChannelInfo()) {
						JSONObject channelInfoJSONObj = new JSONObject();
						channelInfoJSONObj.putOnce("paymentChannelName", paymentChannelInfo.getPaymentChannelName());
						channelInfoJSONObj.putOnce("minAmount", paymentChannelInfo.getMinAmount());
						channelInfoJSONObj.putOnce("maxAmount", paymentChannelInfo.getMaxAmount());

						channelInfoArray.put(channelInfoJSONObj.toString());
					}
					jObj.put("billerPaymentChannels", channelInfoArray);
					jObj.put("statusCode", "OK");
					
					return jObj.toString();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return billerParam;
	}

	@Override
	public Map<String, Object> fetchBillDetails(String billFetchRequest, String userAgent, String ipAddress) {
		// TODO Auto-generated method stub
		
		Map<String, Object> billFetchMap = new HashMap<String, Object>();
		BbpsApiRunner billInfoRunner = new BbpsApiRunner();
		String request = BbpsProperties.getBillFetch();
		fetchedRequestID = TransactionManager.getNewTransactionId();
		//String reqId = fetchedRequestID;
		String xmlRequestToString = "";

		try {

			JAXBContext jContext = JAXBContext.newInstance(BillFetchRequest.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			BillFetchRequest billFetch = g.fromJson(billFetchRequest, BillFetchRequest.class);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(billFetch, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"fetchBillDetails()","","", "","", "fetchBillDetails_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(fetchedRequestID);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = billInfoRunner.getBbpsApiResponse(requestParam);

		JAXBContext jaxbContext;
		
		try {
			
			if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
				jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.billfetch.response.error.bean.BillFetchResponse.class);
			else
				jaxbContext = JAXBContext.newInstance(BillFetchResponse.class);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			if(decryptedResponse.contains("<errorCode>")) {
				com.bhartipay.bbps.billfetch.response.error.bean.BillFetchResponse billFetchErrorResponse = new com.bhartipay.bbps.billfetch.response.error.bean.BillFetchResponse();
				billFetchErrorResponse = (com.bhartipay.bbps.billfetch.response.error.bean.BillFetchResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
				billFetchMap.put("responseCode", billFetchErrorResponse.getResponseCode());
				billFetchMap.put("errorCode", billFetchErrorResponse.getErrorInfo().getError().getErrorCode());
				billFetchMap.put("errorMessage", billFetchErrorResponse.getErrorInfo().getError().getErrorMessage());
			}
			else if(decryptedResponse.contains("<html>")) {
				billFetchMap.put("responseCode", "001");
				billFetchMap.put("errorCode", "err01");
				
				int titleStartIndex = decryptedResponse.indexOf("<title>");
				int titleEndIndex = decryptedResponse.indexOf("</title>");
				int titleStartIndexWithLength = titleStartIndex + "<title>".length();
				
				String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
				
				
				billFetchMap.put("errorMessage", titleMessage);
			}
			else {
				
				BillFetchResponse billFetchResponse = new BillFetchResponse();
				billFetchResponse = (BillFetchResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
				billFetchMap.put("responseCode", billFetchResponse.getResponseCode());
	
				List<Map<String,String>> inputsList = new ArrayList<Map<String,String>>();
				
				for (Input inputsForBillFetch : billFetchResponse.getInputParams().getInputList()) {
					Map<String, String> inputMap = new HashMap<String, String>();
					
					inputMap.put("paramName", inputsForBillFetch.getParamName());
					
					inputMap.put("paramValue", inputsForBillFetch.getParamValue());
					
					inputsList.add(inputMap);
				}
				billFetchMap.put("inputParams", inputsList);
				
				BillerResponse billerResponseForBillFetch = billFetchResponse.getBillerResponse();
				Map<String, Object> billerResponseMap = new HashMap<String, Object>();
				
				double amtD = 0;
					try {
						amtD = Double.valueOf(billerResponseForBillFetch.getBillAmount());
						amtD = amtD/100;
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				billerResponseMap.put("billAmount", amtD);
				billerResponseMap.put("billDate", billerResponseForBillFetch.getBillDate());
				billerResponseMap.put("billNumber", billerResponseForBillFetch.getBillNumber());
				billerResponseMap.put("billPeriod", billerResponseForBillFetch.getBillPeriod());
				billerResponseMap.put("customerName", billerResponseForBillFetch.getCustomerName());
				billerResponseMap.put("dueDate", billerResponseForBillFetch.getDueDate());
				
				List<Map<String,String>> amountOptionsList = new ArrayList<Map<String,String>>();
				
				for (Option amountOptions : billerResponseForBillFetch.getAmountOptions().getOption()) {
					Map<String, String> amountOptionsMap = new HashMap<String, String>();
					
					amountOptionsMap.put("amountName", amountOptions.getAmountName());
					
					double amtOptD = 0;
						try {
							amtOptD = Double.valueOf(amountOptions.getAmountValue());
							//amtOptD = amtOptD/100;
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					
					amountOptionsMap.put("amountValue", ""+amtOptD);
					amountOptionsList.add(amountOptionsMap);
					
				}
				billerResponseMap.put("amountOptions", amountOptionsList);
				billFetchMap.put("billerResponse", billerResponseMap);
				
				
				List<Map<String,String>> additionalInfoList = new ArrayList<Map<String,String>>();
				
				for (Info additionalInfo : billFetchResponse.getAdditionalInfo().getInf()) {
					Map<String, String> infoMap = new HashMap<String, String>();
					
					infoMap.put("infoName", additionalInfo.getInfoName());
					infoMap.put("infoValue", additionalInfo.getInfoValue());
					additionalInfoList.add(infoMap);
				}
				billFetchMap.put("additionalInfo", additionalInfoList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return billFetchMap;
	}
	
	public static  String capitalizer(String word){

        String[] words = word.split(" ");
        StringBuilder sb = new StringBuilder();
        if (words[0].length() > 0) {
            sb.append(Character.toUpperCase(words[0].charAt(0)) + words[0].subSequence(1, words[0].length()).toString().toLowerCase());
            for (int i = 1; i < words.length; i++) {
                sb.append(" ");
                sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString().toLowerCase());
            }
        }
        return  sb.toString();

    }
	
	@Override
	public Map<String, Object> billPaymentRequest(String billPayRequest, String userAgent, String ipAddress) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPaymentRequest()","","", "","", "billPaymentRequest() | billPayRequest "+billPayRequest);
		
		Map<String, Object> validationMap = billValidationRequest(billPayRequest);
		JSONObject validationMapObj = new JSONObject(validationMap);
		
		try {
			
			String responseCode = "";
			String responseQuickPay = "";
			String responseReason = "";

			if(validationMapObj.has("responseCode"))
				responseCode = validationMapObj.getString("responseCode");
			if(validationMapObj.has("responseReason"))
				responseReason = validationMapObj.getString("responseReason");
			if(validationMapObj.has("quickPay"))
				responseQuickPay = validationMapObj.getString("quickPay");
			
			if(("000".equalsIgnoreCase(responseCode) && "Successful".equalsIgnoreCase(responseReason)) || "N".equalsIgnoreCase(responseQuickPay)) {
				String reqId = (String)validationMapObj.get("reqId");
				return billPayRequest(billPayRequest, reqId, userAgent, ipAddress);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return validationMap;
	}
	
	public Map<String, Object> billValidationRequest(String billPayRequest) {
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billValidationRequest()","","", "","", "billValidationRequest() | billPayRequest "+billPayRequest);
		
		Map<String, Object> billPayMap = new HashMap<String, Object>();
		BbpsApiRunner billInfoRunner = new BbpsApiRunner();
		
		String request = BbpsProperties.getBillValidation();
		
		String reqId = TransactionManager.getNewTransactionId();
		
		billPayMap.put("reqId", reqId);
		
		String xmlRequestToString = "";
		
		try {

			
			Gson g = new Gson();
			BillPaymentRequest billPaymentRequest = g.fromJson(billPayRequest, BillPaymentRequest.class);
			
			BillPaymentSuperRequest billPay = billPaymentRequest;
			PaymentMethod paymentMethod = billPay.getPaymentMethod();
			
			if("Y".equalsIgnoreCase(paymentMethod.getQuickPay())){
				billPayMap.put("quickPay", "Y");
			
				BillValidationRequest billValidationRequest = new BillValidationRequest();
				billValidationRequest.setAgentId(billPay.getAgentId());
				billValidationRequest.setBillerId(billPay.getBillerId());
				
				List<com.bhartipay.bbps.billvalidation.request.bean.Input> inputList = new ArrayList<com.bhartipay.bbps.billvalidation.request.bean.Input>();
				
				
				for(com.bhartipay.bbps.billpayment.request.bean.Input inputs : billPay.getInputParams().getInput()) {
					com.bhartipay.bbps.billvalidation.request.bean.Input input = new com.bhartipay.bbps.billvalidation.request.bean.Input();
					input.setParamName(inputs.getParamName());
					input.setParamValue(inputs.getParamValue());
					inputList.add(input);
				}
				InputParams inputParams = new InputParams();
				inputParams.setInput(inputList);
				
				billValidationRequest.setInputParams(inputParams);
				
				JAXBContext jContext = JAXBContext.newInstance(BillValidationRequest.class);
				Marshaller marshallObj = jContext.createMarshaller();
				marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				
				StringWriter sWriter = new StringWriter();
				marshallObj.marshal(billValidationRequest, sWriter);
				xmlRequestToString = sWriter.toString();
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billValidationRequest()","","", "","", "billValidationRequest_xmlRequestToString : "+xmlRequestToString);
			}
			else {
				billPayMap.put("quickPay", "N");
				return billPayMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = billInfoRunner.getBbpsApiResponse(requestParam);
		
		
		JAXBContext jaxbContext;
		
		try {
			
			if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
				jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.billvalidation.response.error.bean.BillValidationResponse.class);
			else
				jaxbContext = JAXBContext.newInstance(BillValidationResponse.class);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			if(decryptedResponse.contains("<html>")) {
				billPayMap.put("responseCode", "001");
				billPayMap.put("errorCode", "err01");
				
				int titleStartIndex = decryptedResponse.indexOf("<title>");
				int titleEndIndex = decryptedResponse.indexOf("</title>");
				int titleStartIndexWithLength = titleStartIndex + "<title>".length();
				
				String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
				
				billPayMap.put("errorMessage", titleMessage);
			}
			else {
				
				BillValidationResponse billValidationResponse = new BillValidationResponse();
				billValidationResponse = (BillValidationResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
				billPayMap.put("responseCode", billValidationResponse.getResponseCode());
				billPayMap.put("responseReason", billValidationResponse.getResponseReason());
				billPayMap.put("complianceCode", billValidationResponse.getComplianceCode());
				billPayMap.put("complianceReason", billValidationResponse.getComplianceReason());
				billPayMap.put("approvalRefNo", billValidationResponse.getApprovalRefNo());
				
				if(billValidationResponse.getAdditionalInfo() != null) {
				
					List<Map<String, String>> infoList = new ArrayList<Map<String, String>>();
					
					for (com.bhartipay.bbps.billvalidation.response.bean.Info info : billValidationResponse.getAdditionalInfo().getInfo()) {
						Map<String, String> infoMap = new HashMap<String, String>();
						infoMap.put("infoName", info.getInfoName());
						infoMap.put("infoValue", info.getInfoValue());
						infoList.add(infoMap);
					}
					billPayMap.put("additionalInfo", infoList);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return billPayMap;
	}
	
	public Map<String, Object> billPayRequest(String billPayRequest, String reqId, String userAgent, String ipAddress) {
		Gson gson = new Gson();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "billPayRequest() | reqId "+reqId+"| billPayRequest "+billPayRequest);
		
		Map<String, Object> billPayMap = new HashMap<String, Object>();
		BbpsApiRunner billInfoRunner = new BbpsApiRunner();

		String request = BbpsProperties.getBillPayment();
		String xmlRequestToString = "";
		String aggreatorid = "";
		String debitAmout = "";
		
		BillPaymentSuperRequest billPay = new BillPaymentSuperRequest();
		BillPaymentRequest billPaymentRequest = new BillPaymentRequest();
		BbpsPayment bbpsPay = new BbpsPayment();
		PaymentDao payDao = new PaymentDaoImpl();
		
		String paramName = null;
		String paramValue = null;
		String custMobile = null;
		StringBuffer sb=new StringBuffer();
		try {

			JAXBContext jContext = JAXBContext.newInstance(BillPaymentSuperRequest.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			billPaymentRequest = g.fromJson(billPayRequest, BillPaymentRequest.class);
			billPay = billPaymentRequest;
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPaymentRequest()","","", "","", "billPaymentRequest : "+gson.toJson(billPaymentRequest));
			
			custMobile = billPay.getCustomerInfo().getCustomerMobile();
			
			com.bhartipay.bbps.billpayment.request.bean.InputParams inputParams = billPay.getInputParams();
			List<com.bhartipay.bbps.billpayment.request.bean.Input> inputList = inputParams.getInput();
			
			
			for(com.bhartipay.bbps.billpayment.request.bean.Input input : inputList) {
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"inputParams()","",input.getParamName()+"  "+input.getParamValue(), "","", "inputParams : "+gson.toJson(input));
						
				if(input.getParamName().toUpperCase().contains("ID")) {
					paramName = input.getParamName();
					paramValue = input.getParamValue();
					sb.append(input.getParamValue()+" ");
				}
				if(input.getParamName().toUpperCase().contains("NUMBER")) {
					paramName = input.getParamName();
					paramValue = input.getParamValue();
					sb.append(input.getParamValue()+" ");
				}
				if(input.getParamName().toUpperCase().contains("NO")) {
					paramName = input.getParamName();
					paramValue = input.getParamValue();
					sb.append(input.getParamValue()+" ");
				}
				if(input.getParamName().toUpperCase().contains("ACCOUNT")) {
					paramName = input.getParamName();
					paramValue = input.getParamValue();
					sb.append(input.getParamValue()+" ");
				}
			}
			System.out.println("PARAM VALUE "+sb);
			com.bhartipay.bbps.billpayment.request.bean.BillerResponse billerResponse = billPay.getBillerResponse();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billerResponse()","","", "","", "billerResponse : "+billerResponse);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billerResponse()","","", "","", "billerResponse : "+gson.toJson(billerResponse));
			
			if(billerResponse.getBillAmount() != null) {

				double amountBillAmount = 0;
				try {
					amountBillAmount = Double.valueOf(billerResponse.getBillAmount());
					amountBillAmount = amountBillAmount*100;
					int intAmout = (int) amountBillAmount;
					billerResponse.setBillAmount(""+intAmout);
					
				}
				catch(Exception e) {
					e.printStackTrace();
				}

				List<com.bhartipay.bbps.billpayment.request.bean.Option> amountOptionsList = billerResponse.getAmountOptions().getOption();
				com.bhartipay.bbps.billpayment.request.bean.AmountOptions amountOptions = new com.bhartipay.bbps.billpayment.request.bean.AmountOptions();
				List<com.bhartipay.bbps.billpayment.request.bean.Option> newAmountOptionsList = new ArrayList<com.bhartipay.bbps.billpayment.request.bean.Option>(); 
				
				for(com.bhartipay.bbps.billpayment.request.bean.Option opt : amountOptionsList) {
					double amountValue = 0;
					try {
						amountValue = Double.valueOf(opt.getAmountValue());
						//amountValue = amountValue*100;
						int intAmout = (int) amountValue;
						opt.setAmountValue(""+intAmout);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
					newAmountOptionsList.add(opt);
				}
				
				amountOptions.setOption(newAmountOptionsList);
				billerResponse.setAmountOptions(amountOptions);
				billPay.setBillerResponse(billerResponse);
			
			}
			
			AmountInfo amountInfo = billPay.getAmountInfo();
			
			double amountInfoAmount = 0;
			try {
				debitAmout = amountInfo.getAmount();
				amountInfoAmount = Double.valueOf(debitAmout);
				amountInfoAmount = amountInfoAmount*100;
				int intAmout = (int) amountInfoAmount;
				amountInfo.setAmount(""+intAmout);
				
				amountInfoAmount = Double.valueOf(amountInfo.getCustConvFee());
				amountInfoAmount = amountInfoAmount*100;
				intAmout = (int) amountInfoAmount;
				amountInfo.setCustConvFee(""+intAmout);
				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			billPay.setAmountInfo(amountInfo);
			aggreatorid = billPaymentRequest.getAggreatorId();
			
			PaymentMethod paymentMethod = billPay.getPaymentMethod();
			paymentMethod.setPaymentMode("Cash");
			
			billPay.setPaymentMethod(paymentMethod);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(billPay, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "xmlRequestToString : "+xmlRequestToString);
		
		/*	
			List<com.bhartipay.bbps.billpayment.request.bean.Input> paramList = billPaymentRequest.getInputParams().getInput();
			for(int i=0;i<paramList.size();i++)
			{	
				sb.append(paramList.get(i).getParamValue());
				sb.append("_");
			}
		*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		
		PaymentMethod paymentMethod = billPay.getPaymentMethod();
		
		if("Y".equalsIgnoreCase(paymentMethod.getQuickPay())){
			requestParam.setRequestId(reqId);
		}
		
		if("N".equalsIgnoreCase(paymentMethod.getQuickPay())){
			requestParam.setRequestId(fetchedRequestID);
		}
		

		CommanUtilDao commanUtilDao = new CommanUtilDaoImpl();

		String dmtTxnID = commanUtilDao.getTrxId("BBPS", billPaymentRequest.getAggreatorId());
		System.out.println(" RESTXNID  "+dmtTxnID);
		bbpsPay.setAgentid(billPaymentRequest.getAgentidDmt());
		bbpsPay.setAggreatorid(billPaymentRequest.getAggreatorId());
		bbpsPay.setWalletid(billPaymentRequest.getWelletId());
		bbpsPay.setResptxnid(dmtTxnID);
		bbpsPay.setBillerid(billPaymentRequest.getBillerId());
		bbpsPay.setBillername(billPaymentRequest.getBillerName());
		bbpsPay.setBillertype(billPaymentRequest.getBillerType());
		
		bbpsPay.setRequest(xmlRequestToString);
		bbpsPay.setRequestid(requestParam.getCompleteRequestId());
		bbpsPay.setRequestdate(new java.util.Date());	//		Calendar.getInstance().getTimeInMillis()
		bbpsPay.setResponsedate(new java.util.Date());
		bbpsPay.setQuickpaytype(paymentMethod.getQuickPay());
	   
		
		bbpsPay.setConsumerNumber(sb.toString());
		
		bbpsPay.setTxnamount((Double.parseDouble(debitAmout)));
		bbpsPay.setTxndate(new java.util.Date());
		bbpsPay.setStatus("Pending");
		bbpsPay.setRemark("Pending");
		java.sql.Date date = java.sql.Date.valueOf(LocalDate.now());
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatt.format( new Date(System.currentTimeMillis())   );
		
		AgentBillPayRequest agentBillPay=new AgentBillPayRequest(bbpsPay.getAgentid(),timestamp,dateString,bbpsPay.getTxnamount(),bbpsPay.getRequestid(),bbpsPay.getBillerid(),bbpsPay.getConsumerNumber());
		if(payDao.getBbpsAgentRequest(bbpsPay))
		{
		boolean saveReq = payDao.saveBbpsAgentRequest(agentBillPay);
	    System.out.println("Save Bill Request  "+saveReq);		
	    TransactionDao transactionDao = new TransactionDaoImpl();
	    Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","payment not debit from wallet", "","", "Status Pending Saved : ");
		
	    if(transactionDao.debitWalletForBBPSTransacitons(billPaymentRequest.getAgentidDmt(), billPaymentRequest.getWelletId(),billPaymentRequest.getAggreatorId(),dmtTxnID, Double.valueOf(debitAmout), paramValue, userAgent, ipAddress)) {
	    	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","payment  debit from wallet", "","", "Status not Saved : ");
			boolean savePendingStatus = payDao.saveOrUpdatePaymentDetails(bbpsPay);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "payment detail save with Pending : "+savePendingStatus);
			if(savePendingStatus) {
				
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","paymentdebitfromwallet","", "","", "with Pending status : "+savePendingStatus);
				
				requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
				
				String decryptedResponse = billInfoRunner.getBbpsApiResponse(requestParam);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"decryptedresponse ","","", "","", "payment detail save with Pending : "+savePendingStatus+"  --  decryptedresponse  "+decryptedResponse);
				System.out.println("decryptedResponse= "+decryptedResponse);		
				bbpsPay.setResponse(decryptedResponse);
				bbpsPay.setResponsedate(new java.util.Date());
				
				JAXBContext jaxbContext;
				JAXBContext jaxbContext1;
				
				try {
					//decryptedResponse+="<errorCode>";
					//decryptedResponse+="<html>";
					if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
						jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.billpayment.response.error.bean.ExtBillPayResponse.class);
					else
						jaxbContext = JAXBContext.newInstance(ExtBillPayResponse.class);
					
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				 	
					ExtBillPayResponse extBillPayResponse = new ExtBillPayResponse();
					/*
						try {
							jaxbContext1 = JAXBContext.newInstance(ExtBillPayResponse.class);
							Unmarshaller jaxbUnmarshaller1 = jaxbContext1.createUnmarshaller();
							extBillPayResponse = (ExtBillPayResponse) jaxbUnmarshaller1.unmarshal(new StringReader(decryptedResponse));
							if(extBillPayResponse!=null)
							{
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"extBillPayResponse()","","", "","", "payment detail save with Error FinalBillPayResponseError : "+extBillPayResponse);
							//bbpsPay.setStatus(extBillPayResponse.getResponseReason());
							bbpsPay.setTxnid(extBillPayResponse.getTxnRefId());
							}
						 }catch(Exception e)
						 {
						   Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"extBillPayResponse()","","", "","", "payment detail TxnIDResponse : "+extBillPayResponse);
						 }
				    */
					if(decryptedResponse.contains("<errorCode>")) {
						
						
						
						
	
						com.bhartipay.bbps.billpayment.response.error.bean.ExtBillPayResponse extBillPayErrorResponse = new com.bhartipay.bbps.billpayment.response.error.bean.ExtBillPayResponse();
						extBillPayErrorResponse = (com.bhartipay.bbps.billpayment.response.error.bean.ExtBillPayResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
						
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"ErrorResponse()","","", "","", "payment detail TxnIDResponse : "+extBillPayErrorResponse);
						
						
						if("PNR001".equalsIgnoreCase(extBillPayErrorResponse.getErrorInfo().getError().getErrorCode()))
						{
							billPayMap.put("errorMessage", "Currently the request is in progress, please check the status after some time.");
							bbpsPay.setStatus("Pending");
						}else 
						{
						transactionDao.creditWalletForBBPSTransacitons(billPaymentRequest.getAgentidDmt(), billPaymentRequest.getWelletId(),billPaymentRequest.getAggreatorId(),dmtTxnID,Double.valueOf(debitAmout),paramValue,userAgent,ipAddress);
						
						String errorCode = extBillPayErrorResponse.getErrorInfo().getError().getErrorCode();
						
							if("E701".equalsIgnoreCase(extBillPayErrorResponse.getErrorInfo().getError().getErrorCode()))
								billPayMap.put("errorMessage", "Internal Problem - Please Contact Admin for futher enquiry.");
							else if("DE001".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Invalid ENC request.");
							else if("DE202".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "AI authentication failed.");
							else if("E016".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Biller id required.");
							else if("E017".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Biller id invalid.");
							else if("E018".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Biller mode invalid.");
							else if("E097".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Biller adhoc configuration mismatch.");
							else if("E042".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Payment method required.");
							else if("E043".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "payment mode required.");
							else if("E044".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "payment mode invalid.");
							else if("E045".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Quickpay required.");
							else if("E046".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Quickpay invalid.");
							else if("E047".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Payment method required / Splitpay required.");
							else if("E048".equalsIgnoreCase(errorCode))
								billPayMap.put("errorMessage", "Payment info invalid / Splitpay invalid.");
							else
								billPayMap.put("errorMessage", extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage());
								
						
							bbpsPay.setStatus("Failed");
						}
						
						
						
						billPayMap.put("errorCode", extBillPayErrorResponse.getErrorInfo().getError().getErrorCode());
						billPayMap.put("responseCode", extBillPayErrorResponse.getResponseCode());
						//bbpsPay.setStatus(extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage());
						 
							if("Failure".equalsIgnoreCase(extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage()))
								bbpsPay.setRemark("Failed");
							else if("Pending".equalsIgnoreCase(extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage()))
								bbpsPay.setRemark("Pending");
							else if("Successful".equalsIgnoreCase(extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage()))
						        bbpsPay.setRemark("Successful");
						    else
						    	bbpsPay.setRemark(extBillPayErrorResponse.getErrorInfo().getError().getErrorMessage());
						    	
						    //bbpsPay.setRemark("Pending");
							
						bbpsPay.setTxnamount((Double.parseDouble(debitAmout)));
						bbpsPay.setRequestdate(new java.util.Date());
						bbpsPay.setTxndate(new java.util.Date());
						boolean saveErrorStatus = payDao.saveOrUpdatePaymentDetails(bbpsPay);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "payment detail save with Error saveErrorStatuserrorCode: "+saveErrorStatus);
					}
					else if(decryptedResponse.contains("<html>")) {
						
						transactionDao.creditWalletForBBPSTransacitons(billPaymentRequest.getAgentidDmt(), billPaymentRequest.getWelletId(),billPaymentRequest.getAggreatorId(),dmtTxnID,Double.valueOf(debitAmout),paramValue,userAgent,ipAddress);
						
						billPayMap.put("responseCode", "001");
						billPayMap.put("errorCode", "err01");
						
						int titleStartIndex = decryptedResponse.indexOf("<title>");
						int titleEndIndex = decryptedResponse.indexOf("</title>");
						int titleStartIndexWithLength = titleStartIndex + "<title>".length();
						
						String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
						
						billPayMap.put("errorMessage", titleMessage);
						
						bbpsPay.setTxnamount((Double.parseDouble(debitAmout)));
						bbpsPay.setRequestdate(new java.util.Date());
						bbpsPay.setTxndate(new java.util.Date());
						bbpsPay.setStatus(titleMessage);
						
							if("Failure".equalsIgnoreCase(titleMessage))
								bbpsPay.setRemark("Failed");
							else if("Pending".equalsIgnoreCase(titleMessage))
								bbpsPay.setRemark("Pending");
							else if("Successful".equalsIgnoreCase(titleMessage))
						       bbpsPay.setRemark("Successful");
						    else
								bbpsPay.setRemark("Failed");
							
						
						boolean saveErrorStatus = payDao.saveOrUpdatePaymentDetails(bbpsPay);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "payment detail save with Error saveErrorStatushtml : "+saveErrorStatus);
						
					}
					else {
						
						//ExtBillPayResponse extBillPayResponse = new ExtBillPayResponse();
						extBillPayResponse = (ExtBillPayResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"extBillPayResponse()","","", "","", "payment detail save with Error FinalBillPayResponse : "+extBillPayResponse);
						
						bbpsPay.setStatus(extBillPayResponse.getResponseReason());
						bbpsPay.setTxnid(extBillPayResponse.getTxnRefId());
			
							if("Successful".equalsIgnoreCase(extBillPayResponse.getResponseReason()))
								bbpsPay.setRemark("Successful");
							else if("Pending".equalsIgnoreCase(extBillPayResponse.getResponseReason()))
								bbpsPay.setRemark("Pending");
							else if("Failure".equalsIgnoreCase(extBillPayResponse.getResponseReason()))
						         bbpsPay.setRemark("Failed");
						    else
							 	bbpsPay.setRemark("Failed");
							
						
						List<Map<String, String>> inputsList = new ArrayList<Map<String, String>>();
						for (com.bhartipay.bbps.billpayment.response.bean.Input inputsForBillPay : extBillPayResponse.getInputParams().getInputList()) {
							Map<String, String> inputsMap = new HashMap<String, String>();
							inputsMap.put("paramName", inputsForBillPay.getParamName());
							inputsMap.put("paramValue", inputsForBillPay.getParamValue());
							inputsList.add(inputsMap);
						}
						
						double respAmount = 0;
							try {
								respAmount = Double.valueOf(extBillPayResponse.getRespAmount());
								respAmount = respAmount/100;
							}
							catch(Exception e) {
								e.printStackTrace();
							}
						bbpsPay.setTxnamount(respAmount);
						
						Date txndate = new java.util.Date();
						
					    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");   
					    String strTxnDate = formatter.format(txndate);  
						
					    bbpsPay.setTxndate(txndate);
						
						double CustConvFee = 0;
							try {
								CustConvFee = Double.valueOf(extBillPayResponse.getCustConvFee());
								CustConvFee = CustConvFee/100;
							}
							catch(Exception e) {
								e.printStackTrace();
							}
						
						boolean saveSuccessStatus = payDao.saveOrUpdatePaymentDetails(bbpsPay);
						Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"billPayRequest()","","", "","", "payment detail save with saveSuccessStatus : "+saveSuccessStatus);
						
						//TODO for SMS API
						
						if(saveSuccessStatus) {
							
							billPayMap.put("responseCode", extBillPayResponse.getResponseCode());
							billPayMap.put("responseReason", extBillPayResponse.getResponseReason());
							billPayMap.put("txnRefId", extBillPayResponse.getTxnRefId());
							billPayMap.put("txnDateTime", strTxnDate);
							billPayMap.put("txnRespType", extBillPayResponse.getTxnRespType());
							billPayMap.put("approvalRefNumber", extBillPayResponse.getApprovalRefNumber());
							billPayMap.put("inputParams", inputsList);
							billPayMap.put("RespAmount", respAmount);
							billPayMap.put("CustConvFee", CustConvFee);
							billPayMap.put("RespBillDate", extBillPayResponse.getRespBillDate());
							billPayMap.put("RespBillNumber", extBillPayResponse.getRespBillNumber());
							billPayMap.put("RespBillPeriod", extBillPayResponse.getRespBillPeriod());
							billPayMap.put("RespCustomerName", extBillPayResponse.getRespCustomerName());
							billPayMap.put("RespDueDate", extBillPayResponse.getRespDueDate());
							billPayMap.put("orderID", dmtTxnID);
							billPayMap.put("consumerNumber", sb.toString());
							try {

								String smstemplet = commanUtilDao.getsmsTemplet("bbpsPay",
										billPaymentRequest.getAggreatorId());
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),
										billPaymentRequest.getAggreatorId(), billPaymentRequest.getWelletId(),
										billPaymentRequest.getAgentidDmt(), "", dmtTxnID,
										"billPayRequest()| try to get smstemplet >>" + smstemplet);

								smstemplet = smstemplet.replaceAll("<<AMOUNT>>", "" + respAmount);
								smstemplet = smstemplet.replaceAll("<<BILLERNAME>>",
										billPaymentRequest.getBillerName());
								smstemplet = smstemplet.replaceAll("<<CONSUMERPARAM>>",
										paramName != null ? paramName : "");
								smstemplet = smstemplet.replaceAll("<<CONSUMERNO>>",
										paramValue != null ? paramValue : "");
								smstemplet = smstemplet.replaceAll("<<TXNID>>", extBillPayResponse.getTxnRefId());
								smstemplet = smstemplet.replaceAll("<<DATETIME>>", strTxnDate);

								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid,
										billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "",
										dmtTxnID, "billPayRequest()| Try to send sms " + smstemplet);

								/*
								SmsAndMailUtility smsAndMailUtility = new SmsAndMailUtility("", "", "", custMobile,
										smstemplet, "SMS", aggreatorid, billPaymentRequest.getWelletId(), "BRTPAY",
										"NOTP");
								ThreadUtil.getThreadPool().execute(smsAndMailUtility);
								
								
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid,
										billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "",
										dmtTxnID, "billPayRequest()| sms sent successfully.");
							   */
							
							} catch (Exception e) {
								e.printStackTrace();
								Commonlogger.log(Commonlogger.INFO, this.getClass().getName(), aggreatorid,
										billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "",
										dmtTxnID, "billPayRequest()| sms sending failed.");
							}
						}
						else {
							billPayMap.put("responseCode", "004");
							billPayMap.put("errorCode", "err04");
							billPayMap.put("errorMessage", "Something went worng. Please Contact Admin for futher enquiry.");
							Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "", dmtTxnID, "billPayRequest()| responseCode--004| errorCode--err04| errorMessage--Something went worng. Please Contact Admin for futher enquiry.");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					billPayMap.put("responseCode", "003");
					billPayMap.put("errorCode", "err03");
					billPayMap.put("errorMessage", "Error durring generating response. Please Contact Admin for futher enquiry.");
					Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,billPaymentRequest.getWelletId(),billPaymentRequest.getAgentidDmt(), "",dmtTxnID, "billPayRequest()| responseCode--003| errorCode--err03| errorMessage-- Exception durring generating response : "+e.getMessage());
				}
			 }
			else {
				billPayMap.put("responseCode", "001");
				billPayMap.put("errorCode", "err01");
				billPayMap.put("errorMessage", "Unable to processing. Please Contact Admin for futher enquiry.");
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "", dmtTxnID, "billPayRequest()| responseCode--001| errorCode--err01| errorMessage--Unable to processing. Please Contact Admin for futher enquiry.");
			  }
			}
			else {
				billPayMap.put("responseCode", "002");
				billPayMap.put("errorCode", "err02");
				billPayMap.put("errorMessage", "Unable to debit from your wallet balance. Please check wallet balance.");
				//bbpsPay.setStatus("Failed");
				//bbpsPay.setRemark("Failed");
				//boolean saveUnableDebit = payDao.saveOrUpdatePaymentDetails(bbpsPay);
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "", dmtTxnID, "billPayRequest()| responseCode--002| errorCode--err02| errorMessage--Unable to debit from your wallet balance. Please check wallet balance.");
			}
		
		}else
		{
			System.out.println("You have already processed transaction with same details please wait for 10 minutes.");	
			billPayMap.put("responseCode", "005");
			billPayMap.put("errorCode", "err05");
			billPayMap.put("errorMessage", "You have already processed transaction with same details please wait for 10 minutes.");
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),aggreatorid,billPaymentRequest.getWelletId(), billPaymentRequest.getAgentidDmt(), "", dmtTxnID, "billPayRequest()| responseCode--005| errorCode--err05| errorMessage--You have already processed transaction with same details please wait for 10 minutes.");
		
		}
		return billPayMap;
		
	}

	@Override
	public Map<String, Object> getTxtStatusRequest(String txtIDRequest) {
		// TODO Auto-generated method stub
		
		Map<String, Object> txtStatusMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();

		String request = BbpsProperties.getTransactionStatus();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		
		try {

			JAXBContext jContext = JAXBContext.newInstance(TransactionStatusReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			TransactionStatusReq txtStatusReq = g.fromJson(txtIDRequest, TransactionStatusReq.class);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(txtStatusReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getTxtStatusRequest()","","", "","", "getTxtStatusRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(TransactionStatusResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp transactionStatusErrorResponse = new com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp();
			  transactionStatusErrorResponse = (com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  txtStatusMap.put("responseCode", transactionStatusErrorResponse.getResponseCode());
			  txtStatusMap.put("responseReason", transactionStatusErrorResponse.getResponseReason());
			  txtStatusMap.put("errorCode",transactionStatusErrorResponse.getErrorInfo().getError().getErrorCode());
			  txtStatusMap.put("errorMessage",transactionStatusErrorResponse.getErrorInfo().getError().getErrorMessage());
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			  txtStatusMap.put("responseCode","001");
			  txtStatusMap.put("errorCode", "err01");
			  
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  
			  txtStatusMap.put("errorMessage", titleMessage);
		  } else {
			  
			  TransactionStatusResp transactionStatusResp = new TransactionStatusResp();
			  transactionStatusResp = (TransactionStatusResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  txtStatusMap.put("responseCode", transactionStatusResp.getResponseCode());
			  txtStatusMap.put("responseReason", transactionStatusResp.getResponseReason());
			  
			  TxnList txnListObj = transactionStatusResp.getTxnList();
			  Map<String, String> txnListMap = new HashMap<String, String>();
			  
			  txnListMap.put("agentId", txnListObj.getAgentId());
			  
			  double txnAmount = 0;
			  try {
				  txnAmount = Double.valueOf(txnListObj.getAmount());
				  txnAmount = txnAmount/100;
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			  
			  txnListMap.put("amount", ""+txnAmount);
			  txnListMap.put("billerId", txnListObj.getBillerId());
			  txnListMap.put("txnDate", txnListObj.getTxnDate());
			  txnListMap.put("txnReferenceId", txnListObj.getTxnReferenceId());
			  txnListMap.put("txnStatus", txnListObj.getTxnStatus());

			  txtStatusMap.put("txnList", txnListMap);
			  
		  }
		 
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }

		  return txtStatusMap;
	}
	
	
	@Override
	public Map<String, Object> registrationComplaintRequest(String complaintRequest) {
		// TODO Auto-generated method stub
		
		Map<String, Object> complaintRegRequestMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();

		String request = BbpsProperties.getComplaintRegistration();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		System.out.println(complaintRequest+"       "+reqId);
		try {

			JAXBContext jContext = JAXBContext.newInstance(ComplaintRegistrationReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			ComplaintRegistrationReq complaintRegistrationReq = g.fromJson(complaintRequest, ComplaintRegistrationReq.class);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(complaintRegistrationReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"registrationComplaintRequest()","","", "","", "registrationComplaintRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		System.out.println("decryptedResponse : "+decryptedResponse);

		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(ComplaintRegistrationResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp complaintRegistrationErrorResponse = new com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp();
			  complaintRegistrationErrorResponse = (com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintRegRequestMap.put("responseCode", complaintRegistrationErrorResponse.getResponseCode());
			  complaintRegRequestMap.put("responseReason", complaintRegistrationErrorResponse.getResponseReason());
			  
			  complaintRegRequestMap.put("complaintAssigned", complaintRegistrationErrorResponse.getComplaintAssigned());
			  complaintRegRequestMap.put("complaintId", complaintRegistrationErrorResponse.getComplaintId());
			  
			  complaintRegRequestMap.put("errorCode",complaintRegistrationErrorResponse.getErrorInfo().getError().getErrorCode());
			  complaintRegRequestMap.put("errorMessage",complaintRegistrationErrorResponse.getErrorInfo().getError().getErrorMessage());
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			  complaintRegRequestMap.put("responseCode","001");
			  complaintRegRequestMap.put("errorCode", "err01");
			  
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  complaintRegRequestMap.put("errorMessage", titleMessage);
		  } else {
			  
			  ComplaintRegistrationResp complaintRegistrationResp = new ComplaintRegistrationResp();
			  complaintRegistrationResp = (ComplaintRegistrationResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintRegRequestMap.put("responseCode", complaintRegistrationResp.getResponseCode());
			  complaintRegRequestMap.put("responseReason", complaintRegistrationResp.getResponseReason());
			  complaintRegRequestMap.put("complaintId", complaintRegistrationResp.getComplaintId());
			  complaintRegRequestMap.put("complaintAssigned", complaintRegistrationResp.getComplaintAssigned());
			  
		  }
		 
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }

		  return complaintRegRequestMap;
		
	}
	

	@Override
	public Map<String, Object> getcomplaintStatusRequest(String complaintID) {
		// TODO Auto-generated method stub
		
		Map<String, Object> complaintStatusMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();

		String request = BbpsProperties.getComplaintTracking();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		System.out.println(request+"  getcomplaintStatusRequest  "+reqId);
		try {

			JAXBContext jContext = JAXBContext.newInstance(ComplaintTrackingReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			ComplaintTrackingReq complaintStatusReq = g.fromJson(complaintID, ComplaintTrackingReq.class);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(complaintStatusReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getcomplaintStatusRequest()","","", "","", "getcomplaintStatusRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		System.out.println("decryptedResponse : "+decryptedResponse);

		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.complaint.response.error.bean.ComplaintTrackingResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(ComplaintTrackingResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.complaint.response.error.bean.ComplaintTrackingResp complaintStatusErrorResponse = new com.bhartipay.bbps.complaint.response.error.bean.ComplaintTrackingResp();
			  complaintStatusErrorResponse = (com.bhartipay.bbps.complaint.response.error.bean.ComplaintTrackingResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintStatusMap.put("responseCode", complaintStatusErrorResponse.getResponseCode());
			  complaintStatusMap.put("responseReason", complaintStatusErrorResponse.getResponseReason());
			  
			  complaintStatusMap.put("complaintAssigned", complaintStatusErrorResponse.getComplaintAssigned());
			  complaintStatusMap.put("complaintId", complaintStatusErrorResponse.getComplaintId());
			  complaintStatusMap.put("complaintRemarks", complaintStatusErrorResponse.getComplaintRemarks());
			  complaintStatusMap.put("complaintStatus", complaintStatusErrorResponse.getComplaintStatus());
			  
			  
			  complaintStatusMap.put("errorCode",complaintStatusErrorResponse.getErrorInfo().getError().getErrorCode());
			  complaintStatusMap.put("errorMessage",complaintStatusErrorResponse.getErrorInfo().getError().getErrorMessage());
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			  complaintStatusMap.put("responseCode","001");
			  complaintStatusMap.put("errorCode", "err01");
			  
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  
			  complaintStatusMap.put("errorMessage", titleMessage);
		  } else {
			  
			  ComplaintTrackingResp complaintStatusResp = new ComplaintTrackingResp();
			  complaintStatusResp = (ComplaintTrackingResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintStatusMap.put("responseCode", complaintStatusResp.getResponseCode());
			  complaintStatusMap.put("responseReason", complaintStatusResp.getResponseReason());
			  complaintStatusMap.put("complaintId", complaintStatusResp.getComplaintId());
			  complaintStatusMap.put("complaintAssigned", complaintStatusResp.getComplaintAssigned());
			  
		  }
		 
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }

		  return complaintStatusMap;
		
	}

	@Override
	public String loadAllBillerCategory() {
		// TODO Auto-generated method stub
		
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "");
		
		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		String isBillerLoaded = "";

		String request = BbpsProperties.getBillerMDM();
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
System.out.println(request+"   Request -------   reqId   "+reqId);
		try {

			//Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "try to jContext to BillerInfoRequest.class");
			//JAXBContext jContext = JAXBContext.newInstance(BillerInfoRequest.class);
			//Marshaller marshallObj = jContext.createMarshaller();
			//marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//BillerInfoRequest billInfo = new BillerInfoRequest();
			//StringWriter sw = new StringWriter();
			//marshallObj.marshal(billInfo, sw);
			//xmlRequestToString = sw.toString();
		/*	xmlRequestToString="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
					"<billerInfoRequest>\n" + 
					//"<billerId>OTME00005XXZ43</billerId>\n" + 
					"<billerId>AAVA00000NATMF</billerId>\n" + 
					"</billerInfoRequest>"; 
			*/		
			
			xmlRequestToString= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
					"<billerInfoRequest>\n" + 
					"<billerId>AAVA00000NATMF</billerId>\n" + 
					"<billerId>ACT000000NAT01</billerId>\n" + 
					"<billerId>ADIT00000NATRA</billerId>\n" + 
					"<billerId>AGL000000MAP01</billerId>\n" + 
					"<billerId>AIRT00000NAT87</billerId>\n" + 
					"<billerId>AMC000000GUJ01</billerId>\n" + 
					"<billerId>APDCL0000ASM01</billerId>\n" + 
					"<billerId>APDCL0000ASM02</billerId>\n" +
					"<billerId>AROH00000WBL0I</billerId>\n" + 
					"<billerId>ASIABB000NAT01</billerId>\n" + 
					"<billerId>ASSA00000ASMA5</billerId>\n" + 
					"<billerId>ATBROAD00NAT01</billerId>\n" + 
					"<billerId>ATBROAD00NAT02</billerId>\n" + 
					"<billerId>ATLLI0000NAT01</billerId>\n" + 
					"<billerId>ATLLI0000NAT02</billerId>\n" + 
					"<billerId>ATPOST000NAT01</billerId>\n" + 
					"<billerId>ATPOST000NAT02</billerId>\n" + 
					"<billerId>AVAI00000NAT7J</billerId>\n" + 
					"<billerId>AVVNL0000RAJ01</billerId>\n" + 
					"<billerId>AXIS00000NATSN</billerId>\n" + 
					"<billerId>BAJA00000NATV1</billerId>\n" + 
					"<billerId>BANK00000NATDH</billerId>\n" + 
					"<billerId>BESCOM000KAR01</billerId>\n" + 
					"<billerId>BESLOB000RAJ02</billerId>\n" + 
					"<billerId>BEST00000MUM01</billerId>\n" + 
					"<billerId>BFL000000NAT01</billerId>\n" + 
					"<billerId>BHAG00000NATBJ</billerId>\n" + 
					"<billerId>BHAR00000NATR4</billerId>\n" + 
					"<billerId>BILAVAIRTEL001</billerId>\n" + 
					"<billerId>BILAVBSNL00001</billerId>\n" + 
					"<billerId>BILAVIDEA00001</billerId>\n" + 
					"<billerId>BILAVJIO000001</billerId>\n" + 
					"<billerId>BILAVMTNL00001</billerId>\n" + 
					"<billerId>BILAVMTNL00002</billerId>\n" + 
					"<billerId>BILAVVODA00001</billerId>\n" + 
					"<billerId>BKESL0000RAJ02</billerId>\n" + 
					"<billerId>BMC000000MAP01</billerId>\n" + 
					"<billerId>BSESRAJPLDEL01</billerId>\n" + 
					"<billerId>BSESYAMPLDEL01</billerId>\n" + 
					"<billerId>BSNL00000NAT5C</billerId>\n" + 
					"<billerId>BSNL00000NATPZ</billerId>\n" + 
					"<billerId>BSNLLLCORNAT01</billerId>\n" + 
					"<billerId>BSNLLLINDNAT01</billerId>\n" + 
					"<billerId>BSNLMOB00NAT01</billerId>\n" + 
					"<billerId>BWSSB0000KAR01</billerId>\n" + 
					"<billerId>CAMD00000NATAI</billerId>\n" + 
					"<billerId>CAPR00000NATC0</billerId>\n" + 
					"<billerId>CAPR00000NATUB</billerId>\n" + 
					"<billerId>CESC00000KOL01</billerId>\n" + 
					"<billerId>CESCOM000KAR01</billerId>\n" + 
					"<billerId>CESU00000ODI01</billerId>\n" + 
					"<billerId>CGSM00000GUJ01</billerId>\n" + 
					"<billerId>CLIX00000NATST</billerId>\n" + 
					"<billerId>COMWBB000NAT01</billerId>\n" + 
					"<billerId>CONBB0000PUN01</billerId>\n" + 
					"<billerId>CSPDCL000CHH01</billerId>\n" + 
					"<billerId>CUGL00000UTP01</billerId>\n" + 
					"<billerId>DDED00000DAD01</billerId>\n" + 
					"<billerId>DELH00000DEL6Q</billerId>\n" + 
					"<billerId>DENB00000NATIO</billerId>\n" + 
					"<billerId>DEPA00000MIZ9U</billerId>\n" + 
					"<billerId>DGVCL0000GUJ01</billerId>\n" + 
					"<billerId>DHBVN0000HAR01</billerId>\n" + 
					"<billerId>DISH00000NAT01</billerId>\n" + 
					"<billerId>DLJB00000DEL01</billerId>\n" + 
					"<billerId>DMIF00000NATMN</billerId>\n" + 
					"<billerId>DNHPDCL0DNH001</billerId>\n" + 
					"<billerId>DOPN00000NAG01</billerId>\n" + 
					"<billerId>DVOIS0000NAT02</billerId>\n" + 
					"<billerId>ELEC00000CHA3L</billerId>\n" + 
					"<billerId>EPDCLOB00ANP01</billerId>\n" + 
					"<billerId>ESSK00000NATFR</billerId>\n" + 
					"<billerId>EXID00000NAT25</billerId>\n" + 
					"<billerId>FAIR00000NAT6Z</billerId>\n" + 
					"<billerId>FLAS00000NATVZ</billerId>\n" + 
					"<billerId>FLEX00000NATJL</billerId>\n" + 
					"<billerId>FUSNBB000NAT01</billerId>\n" + 
					"<billerId>FUTU00000NAT09</billerId>\n" + 
					"<billerId>GAIL00000IND01</billerId>\n" + 
					"<billerId>GED000000GOA01</billerId>\n" + 
					"<billerId>GESCOM000KAR01</billerId>\n" + 
					"<billerId>GGL000000UTP01</billerId>\n" + 
					"<billerId>GMC000000MAP01</billerId>\n" + 
					"<billerId>GOVE00000PUDN0</billerId>\n" + 
					"<billerId>GUJGAS000GUJ01</billerId>\n" + 
					"<billerId>GWMC00000WGL01</billerId>\n" + 
					"<billerId>HATH00000NATRZ</billerId>\n" + 
					"<billerId>HATHWAY00NAT01</billerId>\n" + 
					"<billerId>HCG000000HAR01</billerId>\n" + 
					"<billerId>HCG000000HAR02</billerId>\n" + 
					"<billerId>HDFC00000NATV4</billerId>\n" + 
					"<billerId>HERO00000NAT7F</billerId>\n" + 
					"<billerId>HESCOM000KAR01</billerId>\n" + 
					"<billerId>HMWSS0000HYD01</billerId>\n" + 
					"<billerId>HPCL00000NAT01</billerId>\n" + 
					"<billerId>HPSEB0000HIP02</billerId>\n" + 
					"<billerId>HUDA00000HAR01</billerId>\n" + 
					"<billerId>I2IF00000NAT6K</billerId>\n" + 
					"<billerId>IDEA00000NAT01</billerId>\n" + 
					"<billerId>IDFC00000NATCK</billerId>\n" + 
					"<billerId>IEPL00000GUJ01</billerId>\n" + 
					"<billerId>IMC000000MAP01</billerId>\n" + 
					"<billerId>INDI00000NAT2P</billerId>\n" + 
					"<billerId>INDI00000NATT5</billerId>\n" + 
					"<billerId>INDI00000NATTR</billerId>\n" + 
					"<billerId>INDI00000NATYG</billerId>\n" + 
					"<billerId>INDRAPGASDEL02</billerId>\n" + 
					"<billerId>INDU00000NATR2</billerId>\n" + 
					"<billerId>INST00000BIHKL</billerId>\n" + 
					"<billerId>INST00000CHHZV</billerId>\n" + 
					"<billerId>IOAG00000DEL01</billerId>\n" + 
					"<billerId>IPRU00000NAT01</billerId>\n" + 
					"<billerId>JANA00000NATO4</billerId>\n" + 
					"<billerId>JBVNL0000JHA01</billerId>\n" + 
					"<billerId>JDVVNL000RAJ01</billerId>\n" + 
					"<billerId>JIO000000NAT01</billerId>\n" + 
					"<billerId>JIO000000NAT02</billerId>\n" + 
					"<billerId>JMC000000MAP01</billerId>\n" + 
					"<billerId>JUSC00000JAM01</billerId>\n" + 
					"<billerId>JVVNL0000RAJ01</billerId>\n" + 
					"<billerId>KALY00000THA3E</billerId>\n" + 
					"<billerId>KALY00000THAO1</billerId>\n" + 
					"<billerId>KEDLOB000RAJ02</billerId>\n" + 
					"<billerId>KERA00000KERMO</billerId>\n" + 
					"<billerId>KESCO0000UTP01</billerId>\n" + 
					"<billerId>KSEBL0000KER01</billerId>\n" + 
					"<billerId>LAMP00000NAT7E</billerId>\n" + 
					"<billerId>LAND00000NATRD</billerId>\n" + 
					"<billerId>LOAN00000NATVP</billerId>\n" + 
					"<billerId>LOKS00000NATC9</billerId>\n" + 
					"<billerId>MAHA00000MAH01</billerId>\n" + 
					"<billerId>MAHA00000MUM01</billerId>\n" + 
					"<billerId>MANA00000NATWG</billerId>\n" + 
					"<billerId>MCA000000PUN01</billerId>\n" + 
					"<billerId>MCC000000KAR01</billerId>\n" + 
					"<billerId>MCG000000GUR01</billerId>\n" + 
					"<billerId>MCJ000000PUN01</billerId>\n" + 
					"<billerId>MCL000000PUN01</billerId>\n" + 
					"<billerId>MESCOM000KAR01</billerId>\n" + 
					"<billerId>MGVCL0000GUJ01</billerId>\n" + 
					"<billerId>MNET00000ASM5W</billerId>\n" + 
					"<billerId>MNGL00000MAH02</billerId>\n" + 
					"<billerId>MONE00000NATX1</billerId>\n" + 
					"<billerId>MOTI00000NATHD</billerId>\n" + 
					"<billerId>MOUN00000DEL3N</billerId>\n" + 
					"<billerId>MPCZ00000MAP01</billerId>\n" + 
					"<billerId>MPCZ00000MAP02</billerId>\n" + 
					"<billerId>MPDC00000MEG01</billerId>\n" + 
					"<billerId>MPEZ00000MAP01</billerId>\n" + 
					"<billerId>MPEZ00000MAP02</billerId>\n" + 
					"<billerId>MPPK00000MAP01</billerId>\n" + 
					"<billerId>MPPO00000MAP0Y</billerId>\n" + 
					"<billerId>MTNL00000DEL01</billerId>\n" + 
					"<billerId>MTNL00000MUM01</billerId>\n" + 
					"<billerId>MUNI00000CHANI</billerId>\n" + 
					"<billerId>NBPDCL000BHI01</billerId>\n" + 
					"<billerId>NDMC00000DEL01</billerId>\n" + 
					"<billerId>NDMC00000DEL02</billerId>\n" + 
					"<billerId>NESCO0000ODI01</billerId>\n" + 
					"<billerId>NETP00000PUNS8</billerId>\n" + 
					"<billerId>NEXTRA000NAT01</billerId>\n" + 
					"<billerId>NPCL00000NOI01</billerId>\n" + 
					"<billerId>PAIS00000NATCV</billerId>\n" + 
					"<billerId>PEDM00000MIZ01</billerId>\n" + 
					"<billerId>PGVCL0000GUJ01</billerId>\n" + 
					"<billerId>PMC000000PUN01</billerId>\n" + 
					"<billerId>PRAM00000NATYI</billerId>\n" + 
					"<billerId>PSPCL0000PUN01</billerId>\n" + 
					"<billerId>PUNE00000MAHSE</billerId>\n" + 
					"<billerId>RELI00000MUM03</billerId>\n" + 
					"<billerId>RELI00000NAT3O</billerId>\n" + 
					"<billerId>RELI00000NATQ9</billerId>\n" + 
					"<billerId>RMC000000JHA01</billerId>\n" + 
					"<billerId>SANW00000UTPFB</billerId>\n" + 
					"<billerId>SBPDCL000BHI01</billerId>\n" + 
					"<billerId>SFLFEE000PUN01</billerId>\n" + 
					"<billerId>SGL000000GUJ01</billerId>\n" + 
					"<billerId>SHRI00000NAT7D</billerId>\n" + 
					"<billerId>SHRI00000NAT7E</billerId>\n" + 
					"<billerId>SHRI00000NATRI</billerId>\n" + 
					"<billerId>SICR00000NATDG</billerId>\n" + 
					"<billerId>SITI00000UTP03</billerId>\n" + 
					"<billerId>SKPR00000SIK01</billerId>\n" + 
					"<billerId>SKPR00000SIK02</billerId>\n" + 
					"<billerId>SMC000000DNH01</billerId>\n" + 
					"<billerId>SMC000000GUJ01</billerId>\n" + 
					"<billerId>SNAP00000NAT61</billerId>\n" + 
					"<billerId>SOUTHCO00ODI01</billerId>\n" + 
					"<billerId>SPDCLOB00ANP01</billerId>\n" + 
					"<billerId>SPENET000NAT01</billerId>\n" + 
					"<billerId>STAR00000NATXZ</billerId>\n" + 
					"<billerId>SUND00000NAT02</billerId>\n" + 
					"<billerId>TATA00000NATGS</billerId>\n" + 
					"<billerId>TATADCDMANAT01</billerId>\n" + 
					"<billerId>TATADGSM0NAT01</billerId>\n" + 
					"<billerId>TATADLLI0NAT01</billerId>\n" + 
					"<billerId>TATAPWR00DEL01</billerId>\n" + 
					"<billerId>TATAPWR00MUM01</billerId>\n" +
					"<billerId>TATASKY00NAT01</billerId>\n" + 
					"<billerId>TIKO00000NAT01</billerId>\n" + 
					"<billerId>TIMB00000NATRQ</billerId>\n" + 
					"<billerId>TNEB00000TND01</billerId>\n" + 
					"<billerId>TNGCLOB00TRI01</billerId>\n" + 
					"<billerId>TOLL00000NAT72</billerId>\n" + 
					"<billerId>TORR00000AGR01</billerId>\n" + 
					"<billerId>TORR00000AHM02</billerId>\n" + 
					"<billerId>TORR00000BHW03</billerId>\n" + 
					"<billerId>TORR00000NATLX</billerId>\n" + 
					"<billerId>TORR00000SUR04</billerId>\n" + 
					"<billerId>TPADL0000AJM02</billerId>\n" +
					"<billerId>TSEC00000TRI01</billerId>\n" + 
					"<billerId>TTN000000NAT01</billerId>\n" + 
					"<billerId>UCPGPL000MAH01</billerId>\n" + 
					"<billerId>UGVCL0000GUJ01</billerId>\n" + 
					"<billerId>UHBVN0000HAR01</billerId>\n" + 
					"<billerId>UITWOB000BHW02</billerId>\n" + 
					"<billerId>UJS000000UTT01</billerId>\n" + 
					"<billerId>UNN000000MAP01</billerId>\n" + 
					"<billerId>UPCL00000UTT01</billerId>\n" + 
					"<billerId>UPPCL0000UTP01</billerId>\n" + 
					"<billerId>UPPCL0000UTP02</billerId>\n" + 
					"<billerId>VART00000NATHC</billerId>\n" +
					"<billerId>VAST00000NATLW</billerId>\n" + 
					"<billerId>VFIB00000NATJJ</billerId>\n" + 
					"<billerId>VGL000000GUJ01</billerId>\n" + 
					"<billerId>VIDEOCON0NAT01</billerId>\n" + 
					"<billerId>VISA00000NAT5A</billerId>\n" + 
					"<billerId>VODA00000NAT01</billerId>\n" + 
					"<billerId>VODA00000NAT02</billerId>\n" + 
					"<billerId>WBSEDCL00WBL01</billerId>\n" + 
					"<billerId>WESCO0000ODI01</billerId>\n" + 
					"<billerId>OTME00005XXZ43</billerId>\n" +
					"</billerInfoRequest>";
			
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "xmlRequestToString : "+xmlRequestToString);
            System.out.println("xmlRequestToString  ->   "+xmlRequestToString);
			PreRequestParam requestParam = new PreRequestParam();
			requestParam.setRequest(request);
			requestParam.setRequestId(reqId);
			requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));
	        System.out.println("enc Request "+CCAVencrypt.getEncryptedString(xmlRequestToString));
			
	        Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "Encrypted Request  : "+requestParam.getEncRequest());
	       
	        String decryptedResponse = bbpsApiRunner.getBbpsNewApiResponse(requestParam);
	        //String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
			System.out.println("dec response  "+decryptedResponse);
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "decryptedResponse  : "+decryptedResponse);

			if("POST NOT WORKED".equalsIgnoreCase(decryptedResponse)) {
				isBillerLoaded = "[false, POST NOT WORKED] " + decryptedResponse;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "[false, POST NOT WORKED] decryptedResponse  : "+decryptedResponse);
			}
			else if("Unknown Host".equalsIgnoreCase(decryptedResponse)) {
				isBillerLoaded = "[false, Unknown Host] " + decryptedResponse;
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "[false, Unknown Host] decryptedResponse  : "+decryptedResponse);
			}else {
				XmlToStringAndStringToXml stringToXml = new XmlToStringAndStringToXml();
				isBillerLoaded = ""+stringToXml.setBillersListToXml(decryptedResponse, BbpsProperties.getBillersXmlFilePath());
				Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "[True, Success] decryptedResponse  : "+decryptedResponse);
			}
		}catch(Exception e){
			e.printStackTrace();
			isBillerLoaded = "[false, Exception] " + e.getMessage();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"loadAllBillerCategory()","","", "","", "[false, Exception] "+e.getMessage());
		}
		return isBillerLoaded;
	}
	
	@Override
	public Set<String> getAllBillerCategory123() {
		// TODO Auto-generated method stub

		Set<String> billerCategoryList = null;

		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();

		String request = BbpsProperties.getBillerMDM();
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";

		try {

			JAXBContext jContext = JAXBContext.newInstance(BillerInfoRequest.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			BillerInfoRequest billInfo = new BillerInfoRequest();
			StringWriter sw = new StringWriter();
			marshallObj.marshal(billInfo, sw);
			xmlRequestToString = sw.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsApiRunner.getBbpsApiResponse(requestParam);
		
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(BillerInfoResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			billerInfoResponseForCategory = (BillerInfoResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));

			billerCategoryList = new TreeSet<String>();
			for (Biller biller : billerInfoResponseForCategory.getBiller()) {
				if(biller.getBillerCategory().equalsIgnoreCase("DTH"))
					billerCategoryList.add(biller.getBillerCategory());
				else
					billerCategoryList.add(capitalizer(biller.getBillerCategory()));
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return billerCategoryList;
	}
	
	
	public com.bhartipay.recharge.response.BillerInfoResponse getBillInfo(BillInfoRequest bill) {

		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		String request = BbpsProperties.getBillerInfoAPI();
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";

		try {
			JAXBContext jContext = JAXBContext.newInstance(com.bhartipay.recharge.request.BillerInfoRequest.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			com.bhartipay.recharge.request.BillerInfoRequest billInfo = new com.bhartipay.recharge.request.BillerInfoRequest();
			billInfo.setBillerId(bill.getBillerInfoRequest().getBillerId());
			StringWriter sw = new StringWriter();
			
			marshallObj.marshal(billInfo, sw);
			xmlRequestToString = sw.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		com.bhartipay.recharge.response.BillerInfoResponse billerInfoResponseForCategory = null;
		PrerequstRechargeParams requestParam = new PrerequstRechargeParams();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getRechargeEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsApiRunner.getRechargeApiResponse(requestParam);
		
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(com.bhartipay.recharge.response.BillerInfoResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();		
			billerInfoResponseForCategory = (com.bhartipay.recharge.response.BillerInfoResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return billerInfoResponseForCategory;
	
		
	}
	
	public com.bhartipay.recharge.response.ExtBillPayResponse prepaidBillPayment(com.bhartipay.recharge.request.BillPaymentRequest bill) {

		BbpsApiRunner bbpsApiRunner = new BbpsApiRunner();
		String request = BbpsProperties.getBillPaymentAPI();
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
				"<billPaymentRequest>\n" + 
				"   <agentId>CC01CC01513515340681</agentId>\n" + 
				"   <billerAdhoc>true</billerAdhoc>\n" + 
				"   <agentDeviceInfo>\n" + 
				"      <ip>198.136.54.132</ip>\n" + 
				"      <initChannel>AGT</initChannel>\n" + 
				"      <mac>ed:37:8d:10:5a:f8</mac>\n" + 
				"   </agentDeviceInfo>\n" + 
				"   <customerInfo>\n" + 
				"      <customerMobile>9876543210</customerMobile>\n" + 
				"      <customerEmail />\n" + 
				"      <customerAdhaar />\n" + 
				"      <customerPan />\n" + 
				"   </customerInfo>\n" + 
				"   <billerId>BILAVAIRTEL001</billerId>\n" + 
				"   <inputParams>\n" + 
				"      <input>\n" + 
				"         <paramName>Location</paramName>\n" + 
				"         <paramValue>Mumbai</paramValue>\n" + 
				"      </input>\n" + 
				"      <input>\n" + 
				"         <paramName>Mobile Number</paramName>\n" + 
				"         <paramValue>9898981000</paramValue>\n" + 
				"      </input>\n" + 
				"   </inputParams>\n" + 
				"   <amountInfo>\n" + 
				"      <amount>100000</amount>\n" + 
				"      <currency>356</currency>\n" + 
				"      <custConvFee>0</custConvFee>\n" + 
				"      <amountTags />\n" + 
				"   </amountInfo>\n" + 
				"   <paymentMethod>\n" + 
				"      <paymentMode>Cash</paymentMode>\n" + 
				"      <quickPay>Y</quickPay>\n" + 
				"      <splitPay>N</splitPay>\n" + 
				"   </paymentMethod>\n" + 
				"   <paymentInfo>\n" + 
				"      <info>\n" + 
				"         <infoName>Remarks</infoName>\n" + 
				"         <infoValue>Received</infoValue>\n" + 
				"      </info>\n" + 
				"   </paymentInfo>\n" + 
				"</billPaymentRequest>";
		
//		<agentId>CC01CC01513515340681</agentId>
//		<ip>198.136.54.132</ip>
//		<customerMobile>9876543210</customerMobile>
//		<billerId>BILAVAIRTEL001</billerId>
//		<paramValue>Mumbai</paramValue>
//		<paramValue>9898981000</paramValue>
//		<amount>100000</amount>
		
		xmlRequestToString = xmlRequestToString.replace("CC01CC01513515340681", bill.getAgentId());
		xmlRequestToString = xmlRequestToString.replace("198.136.54.132", bill.getAgentDeviceInfo().getIp());
		xmlRequestToString = xmlRequestToString.replace("9876543210", bill.getCustomerInfo().getCustomerMobile());
		xmlRequestToString = xmlRequestToString.replace("BILAVAIRTEL001", bill.getBillerId());
		xmlRequestToString = xmlRequestToString.replace("Mumbai", bill.getInputParams().getInput().get(1).getParamValue());
		xmlRequestToString = xmlRequestToString.replaceAll("9898981000", bill.getInputParams().getInput().get(0).getParamValue());
		xmlRequestToString = xmlRequestToString.replaceAll("100000", bill.getAmountInfo().getAmount());
		
		try {
			JAXBContext jContext = JAXBContext.newInstance(com.bhartipay.recharge.request.BillPaymentRequest.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//			com.bhartipay.recharge.request.BillPaymentRequest billInfo = new com.bhartipay.recharge.request.BillPaymentRequest();
//			billInfo.setBillerId(bill.getBillerInfoRequest().getBillerId());
			StringWriter sw = new StringWriter();
			
			marshallObj.marshal(bill, sw);
			//xmlRequestToString = sw.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		com.bhartipay.recharge.response.ExtBillPayResponse billerPaymentResponse = null;
		PrerequstRechargeParams requestParam = new PrerequstRechargeParams();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getRechargeEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsApiRunner.getRechargeApiResponse(requestParam);
		
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(com.bhartipay.recharge.response.ExtBillPayResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();		
			billerPaymentResponse = (com.bhartipay.recharge.response.ExtBillPayResponse) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return billerPaymentResponse;
	}
	
	
	@Override
	public Map<String, Object> getUserId(String complaintRequest,String id) {
		// TODO Auto-generated method stub
		
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction=session.beginTransaction();
		Map<String, Object> complaintRegRequestMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();

		String request = BbpsProperties.getComplaintRegistration();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		ComplaintRegistrationReq complaintRegistrationReq =null;
		System.out.println(complaintRequest+"       "+reqId);
		try {

			JAXBContext jContext = JAXBContext.newInstance(ComplaintRegistrationReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			complaintRegistrationReq = g.fromJson(complaintRequest, ComplaintRegistrationReq.class);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(complaintRegistrationReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"registrationComplaintRequest()","","", "","", "registrationComplaintRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		System.out.println("decryptedResponse : "+decryptedResponse);

		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(ComplaintRegistrationResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp complaintRegistrationErrorResponse = new com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp();
			  complaintRegistrationErrorResponse = (com.bhartipay.bbps.complaint.response.error.bean.ComplaintRegistrationResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintRegRequestMap.put("responseCode", complaintRegistrationErrorResponse.getResponseCode());
			  complaintRegRequestMap.put("responseReason", complaintRegistrationErrorResponse.getResponseReason());
			  
			  complaintRegRequestMap.put("complaintAssigned", complaintRegistrationErrorResponse.getComplaintAssigned());
			  complaintRegRequestMap.put("complaintId", complaintRegistrationErrorResponse.getComplaintId());
			  
			  complaintRegRequestMap.put("errorCode",complaintRegistrationErrorResponse.getErrorInfo().getError().getErrorCode());
			  complaintRegRequestMap.put("errorMessage",complaintRegistrationErrorResponse.getErrorInfo().getError().getErrorMessage());
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			  complaintRegRequestMap.put("responseCode","001");
			  complaintRegRequestMap.put("errorCode", "err01");
			  
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  complaintRegRequestMap.put("errorMessage", titleMessage);
		  } else {
			  
			  ComplaintRegistrationResp complaintRegistrationResp = new ComplaintRegistrationResp();
			  complaintRegistrationResp = (ComplaintRegistrationResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  complaintRegRequestMap.put("responseCode", complaintRegistrationResp.getResponseCode());
			  complaintRegRequestMap.put("responseReason", complaintRegistrationResp.getResponseReason());
			  complaintRegRequestMap.put("complaintId", complaintRegistrationResp.getComplaintId());
			  complaintRegRequestMap.put("complaintAssigned", complaintRegistrationResp.getComplaintAssigned());
			
			  BbpsComplaint complaint=new BbpsComplaint(id,complaintRegistrationReq.getTxnRefId(),reqId,
					  complaintRegistrationReq.getComplaintDisposition(),complaintRegistrationReq.getComplaintDesc(),
					  complaintRegistrationResp.getComplaintId(),complaintRegistrationResp.getComplaintAssigned(),
					  complaintRegistrationResp.getResponseCode(),complaintRegistrationResp.getResponseReason());
			 session.save(complaint);
			 transaction.commit();
			 Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"","","", "", ""," save getComplaintRegistration()");
		  }
		 
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }

		  return complaintRegRequestMap;
		
	}
	
	
	@Override
	public  List<BbpsComplaint> getComplaintDetails(String userId) 
	{
		System.out.println("----UserId  "+userId);
		SessionFactory factory=DBUtil.getSessionFactory();
		Session session = factory.openSession();
		Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userId,"","", "", "","getComplaintDetails");
		   
		List<BbpsComplaint> results = new ArrayList<BbpsComplaint>();
		try {

			StringBuilder serchQuery = new StringBuilder();
			serchQuery.append("from BbpsComplaint  where userId='"+userId+"' ");
            Query query = session.createQuery(serchQuery.toString());
			results = query.list();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userId,"","", "", "","getComplaintDetails "+results.size());
		} catch (Exception e) {
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),userId,"","", "", ""," errors getComplaintDetails"+e.getMessage()+"   "+e);
			e.printStackTrace();
			
		} finally {
			session.close();
		}
	    
		return results;
		
	}
	
	
	
	
  // BBPS Transaction Status Check/Update
	
	
	@Override
	public String bbpsStatusUpdate(String txnId) {
		// TODO Auto-generated method stub
		
		Map<String, Object> txtStatusMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();

		String request = BbpsProperties.getTransactionStatus();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		String status="";
		try {

			JAXBContext jContext = JAXBContext.newInstance(TransactionStatusReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			//TransactionStatusReq txtStatusReq = g.fromJson(txtIDRequest, TransactionStatusReq.class);
			
			TransactionStatusReq txnReq=new TransactionStatusReq();
			txnReq.setTrackType("TRANS_REF_ID");
			txnReq.setTrackValue(txnId);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(txnReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getTxtStatusRequest()","","", "","", "getTxtStatusRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(TransactionStatusResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp transactionStatusErrorResponse = new com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp();
			  transactionStatusErrorResponse = (com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  
			  txtStatusMap.put("errorMessage",transactionStatusErrorResponse.getErrorInfo().getError().getErrorMessage());
		      status=transactionStatusErrorResponse.getErrorInfo().getError().getErrorMessage();
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			   
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  txtStatusMap.put("errorMessage", titleMessage);
              status=titleMessage;
		  } else {
			  
			  TransactionStatusResp transactionStatusResp = new TransactionStatusResp();
			  transactionStatusResp = (TransactionStatusResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			   
			  TxnList txnListObj = transactionStatusResp.getTxnList();
			  Map<String, String> txnListMap = new HashMap<String, String>();
			  txnListMap.put("txnStatus", txnListObj.getTxnStatus());
              status=txnListObj.getTxnStatus();
		  }
		 
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }

		  return status;
	}
	
	
 
	public BbpsStatusUpdateResponse bbpsStatusUpdateByReqId(String txnId) {
		// TODO Auto-generated method stub
		
		Map<String, Object> txtStatusMap = new HashMap<String, Object>();
		BbpsApiRunner bbpsRunner = new BbpsApiRunner();
		BbpsStatusUpdateResponse res=new BbpsStatusUpdateResponse();
		String request = BbpsProperties.getTransactionStatus();
		
		String reqId = TransactionManager.getNewTransactionId();
		String xmlRequestToString = "";
		String status="";
		try {

			JAXBContext jContext = JAXBContext.newInstance(TransactionStatusReq.class);
			Marshaller marshallObj = jContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Gson g = new Gson();
			//TransactionStatusReq txtStatusReq = g.fromJson(txtIDRequest, TransactionStatusReq.class);
			
			TransactionStatusReq txnReq=new TransactionStatusReq();
			txnReq.setTrackType("REQUEST_ID");
			txnReq.setTrackValue(txnId);
			
			StringWriter sWriter = new StringWriter();
			marshallObj.marshal(txnReq, sWriter);
			xmlRequestToString = sWriter.toString();
			Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getTxtStatusRequest()","","", "","", "getTxtStatusRequest_xmlRequestToString : "+xmlRequestToString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PreRequestParam requestParam = new PreRequestParam();
		requestParam.setRequest(request);
		requestParam.setRequestId(reqId);
		requestParam.setEncRequest(CCAVencrypt.getEncryptedString(xmlRequestToString));

		String decryptedResponse = bbpsRunner.getBbpsApiResponse(requestParam);
		
		  JAXBContext jaxbContext;
		  
		  try {
		  
		  if(decryptedResponse.contains("<errorCode>") || decryptedResponse.contains("<html>"))
			  jaxbContext = JAXBContext.newInstance(com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp.class);
		  else
			  jaxbContext = JAXBContext.newInstance(TransactionStatusResp.class);
		  
		  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		  
		  if(decryptedResponse.contains("<errorCode>")) {
			  com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp transactionStatusErrorResponse = new com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp();
			  transactionStatusErrorResponse = (com.bhartipay.bbps.transactionstatus.response.error.bean.TransactionStatusResp)jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  
			  txtStatusMap.put("errorMessage",transactionStatusErrorResponse.getErrorInfo().getError().getErrorMessage());
		      status=transactionStatusErrorResponse.getErrorInfo().getError().getErrorMessage();
		  } 
		  else if(decryptedResponse.contains("<html>")) { 
			   
			  int titleStartIndex = decryptedResponse.indexOf("<title>");
			  int titleEndIndex = decryptedResponse.indexOf("</title>");
			  int titleStartIndexWithLength = titleStartIndex + "<title>".length();
			  
			  String titleMessage = decryptedResponse.substring(titleStartIndexWithLength, titleEndIndex);
			  txtStatusMap.put("errorMessage", titleMessage);
              status=titleMessage;
		   } else {
			  
			  TransactionStatusResp transactionStatusResp = new TransactionStatusResp();
			  transactionStatusResp = (TransactionStatusResp) jaxbUnmarshaller.unmarshal(new StringReader(decryptedResponse));
			  
			  TxnList txnListObj = transactionStatusResp.getTxnList();
			  if(txnListObj!=null) {
			  res.setStatus(txnListObj.getTxnStatus());
			  res.setTxnId(txnListObj.getTxnReferenceId());
			  Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"getTxtStatusRequest()","","", "","", "Transaction status found : "+xmlRequestToString+"  \n  "+decryptedResponse);
			  }
		  }
		 } catch (Exception e) {
		 	
		 	Commonlogger.log(Commonlogger.INFO, this.getClass().getName(),"Problem in BbpsStatusCheck","","", "","", "Transaction status with error : "+e.getMessage());
		 	e.printStackTrace();  
		 }

	  return res;
	}
	
	
}
