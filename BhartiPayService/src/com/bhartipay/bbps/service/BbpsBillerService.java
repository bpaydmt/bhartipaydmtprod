package com.bhartipay.bbps.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bhartipay.lean.bean.BbpsComplaint;
import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;

public interface BbpsBillerService {
	
	public String loadAllBillerCategory();
	
	public Set<String> getAllBillerCategory();
	
	public Set<String> getAllBillerCategory123();
	
	public Map<String,String> getBillerListByCategory(String category);
	
	public String getCustomParam(String billerID);
	
	public Map<String,Object> fetchBillDetails(String fetchBill, String ipIemi, String agent);
	
	public Map<String,Object> billPaymentRequest(String paymentBill, String userAgent, String ipAddress);
	
	public Map<String,Object> getTxtStatusRequest(String txtID);
	
	public Map<String,Object> getcomplaintStatusRequest(String complaintID);

	public Map<String, Object> registrationComplaintRequest(String complaintRequest);
	
	public Map<String, Object> getUserId(String complaintRequest,String id);
	
	//********************************** For prepaid bill ******************************//
	
	public BillerInfoResponse getBillInfo(BillInfoRequest billInfo);
	
	public List<BbpsComplaint> getComplaintDetails(String userId);
	
	public String bbpsStatusUpdate(String txtIDRequest);

}
