import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.CyberPlat.IPriv;
import org.CyberPlat.IPrivKey;
//import org.apache.log4j.Logger;



//import com.wallet.digitalPurse.util.thirdParty.bean.DMTBean;

public class CyberPlatDMTInteration {
	//public static final Logger LOGGER = Logger.getLogger(CyberPlatDMTInteration.class.getName());
	
	 private static final String ENC = "windows-1251";
     private static final String SD = "326587";
     private static final String AP = "327322";
     private static final String OP = "327323";
    // private static final String KEYS = "/home/sandeep/iprivpg/src/java/test/pdtest/KEYS";
     private static final String KEYS = "./KEYS";
    private static final String PASS = "powerpay@123";
     
     private static final String ACCEPT_KEYS="40";
     private static final String NO_ROUTE="0";
     

	private IPrivKey sec = null;
	private IPrivKey pub = null;
	
	
	public String DMTVerification(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay_check.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 System.out.println("********DMTVerification**********apiResp: " + apiResp);
			}catch (Exception e)
		{
			//System.out.println("******************problem in DMTVerification**********: " +  e.getMessage(), e);
				e.printStackTrace();
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	

	public String DMTPayment(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 System.out.println("********DMTPayment*************************************************************apiResp:\r\n " + apiResp);
			}catch (Exception e)
		{
				//System.out.println("******************problem in DMTPayment********************************************: " +  e.getMessage(), e);
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	
	
	public String DMTStatus(DMTBean dMTBean){
		String apiResp=null;
		String verificationURL="https://in.cyberplat.com/cgi-bin/pwdmt/pwdmt_pay_status.cgi";
		String reqparams=getRequestParams(dMTBean);
		CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
		IPriv.setCodePage(ENC);
		try{
			cyberPlatDMTInteration.sec= IPriv.openSecretKey(KEYS + "/secret.key", PASS);	
			 apiResp = cyberPlatDMTInteration.sendRequest(verificationURL,reqparams);
			 apiResp =parseValidationResponse(apiResp);
			 System.out.println("********DMTStatus********************************************************apiResp:\r\n " + apiResp);
			}catch (Exception e)
		{
				//System.out.println("******************problem in DMTStatus**********************************************: " +  e.getMessage(), e);
		}
		cyberPlatDMTInteration.done();
		return apiResp;
	}
	
	
	
	private String getRequestParams(DMTBean dMTBean){
		String reqparams ="SD=" + SD + "\r\n" 
						+"AP=" + AP + "\r\n"
						+"OP=" + OP + "\r\n" 
						+"ACCEPT_KEYS="+ACCEPT_KEYS+"\r\n"
						+"NO_ROUTE="+NO_ROUTE+"\r\n"
						+"SESSION="+dMTBean.getSession()+"\r\n" 
						+"pin="+dMTBean.getPin()+"\r\n" 
						+"otc="+dMTBean.getOtc()+"\r\n" 
						+"fName="+dMTBean.getfName()+"\r\n"
						+"routingType="+dMTBean.getRoutingType()+"\r\n" 
						+"mothersMaidenName="+dMTBean.getMothersMaidenName()+"\r\n" 
						+"state="+dMTBean.getState()+"\r\n" 
						+"benAccount="+dMTBean.getBenAccount()+"\r\n" 
						+"TERM_ID="+dMTBean.getTermId()+"\r\n" 
						+"benMobile="+dMTBean.getBenMobile()+"\r\n" 
						+"address="+dMTBean.getAddress()+"\r\n" 
						+"birthday="+dMTBean.getBirthday()+"\r\n" 
						+"NUMBER="+dMTBean.getNumber()+"\r\n" 
						+"gender="+dMTBean.getGender()+"\r\n" 
						+"otcRefCode="+dMTBean.getOtcRefCode()+"\r\n" 
						+"benNick="+dMTBean.getBenNick()+"\r\n" 
						+"benIFSC="+dMTBean.getBenIFSC()+"\r\n" 
						+"lName="+dMTBean.getlName()+"\r\n" 
						+"benName="+dMTBean.getBenName()+"\r\n" 
						+"benCode="+dMTBean.getBenCode()+"\r\n" 
						+"Type="+dMTBean.getType()+"\r\n" 
						+"ACCOUNT="+dMTBean.getAccount()+"\r\n" 
						+"AMOUNT="+dMTBean.getAmount()+"\r\n" 
						+"AMOUNT_ALL="+dMTBean.getAmountAll()+"\r\n" 
						+"COMMENT="+dMTBean.getComment()+"\r\n" ;
				
		System.out.println("**********************************************************reqparams: " + reqparams);
		return reqparams;
		
	}
	
	
	private void done()
	{
		if (sec != null)
			sec.closeKey();
		
	}
	
	
private String sendRequest (String url,String requestparm) throws Exception {
		
		String req =requestparm;
		req = "inputmessage=" + URLEncoder.encode(sec.signText(req));
		System.out.println("******************REQUEST: " + req);
		URL u = new URL(url);
		URLConnection con = u.openConnection();
		con.setDoOutput(true);
		con.getOutputStream().write(req.getBytes());
		con.getOutputStream().close();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), ENC));
		char[] raw_resp = new char[1024];
		int raw_resp_len = in.read(raw_resp);
		StringBuffer s = new StringBuffer();
		s.append(raw_resp, 0, raw_resp_len);
		String resp = s.toString();
		System.out.println("******************RESPONSE: " + resp);
		return resp;
		
	}
	
	
	
	
private String parseValidationResponse(String apiResp){
	StringBuilder apiRespRest=new StringBuilder() ;
	System.out.println("******************apiResp: " + apiResp);
	String[] str=apiResp.substring(apiResp.indexOf("BEGIN")+5, apiResp.indexOf("END")).split("\\r?\\n");
	for (String string : str) {
		if(string.contains("="))
			apiRespRest.append(string+"|");
			
	}
	System.out.println("******************apiRespRest: " + apiRespRest.subSequence(0, apiRespRest.length()-1));
	return apiRespRest.subSequence(0, apiRespRest.length()-1).toString();
	
}	


public static void main(String[] args) {
	CyberPlatDMTInteration cyberPlatDMTInteration=new CyberPlatDMTInteration();
	DMTBean dmTBean =new DMTBean ();
	dmTBean.setSession("user123");
	dmTBean.setTermId(OP);
	dmTBean.setNumber("9935041287");
	dmTBean.setType("5");
	dmTBean.setAmount(1.0);
	dmTBean.setAmountAll(1.0);
	cyberPlatDMTInteration.DMTVerification(dmTBean);
	cyberPlatDMTInteration.DMTPayment(dmTBean);
	cyberPlatDMTInteration.DMTStatus(dmTBean);
}


	
}


 class DMTBean {
	
	private String	session="";
	private String	pin="";
	private String	otc="";
	private String	fName="";
	private String	routingType="";
	private String	mothersMaidenName="";
	private String	state="";
	private String	benAccount="";
	private String	termId="";
	private String	benMobile="";
	private String	address="";
	private String	birthday="";
	private String	number="";
	private String	gender="";
	private String	otcRefCode="";
	private String	benNick="";
	private String	benIFSC="";
	private String	lName="";
	private String	benName="";
	private String	benCode="";
	private String	type="";
	private String	account="";
	private double	amount;
	private double	amountAll;
	private String  comment="Bhartipay";
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getOtc() {
		return otc;
	}
	public void setOtc(String otc) {
		this.otc = otc;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getRoutingType() {
		return routingType;
	}
	public void setRoutingType(String routingType) {
		this.routingType = routingType;
	}
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getBenAccount() {
		return benAccount;
	}
	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getBenMobile() {
		return benMobile;
	}
	public void setBenMobile(String benMobile) {
		this.benMobile = benMobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOtcRefCode() {
		return otcRefCode;
	}
	public void setOtcRefCode(String otcRefCode) {
		this.otcRefCode = otcRefCode;
	}
	public String getBenNick() {
		return benNick;
	}
	public void setBenNick(String benNick) {
		this.benNick = benNick;
	}
	public String getBenIFSC() {
		return benIFSC;
	}
	public void setBenIFSC(String benIFSC) {
		this.benIFSC = benIFSC;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getBenName() {
		return benName;
	}
	public void setBenName(String benName) {
		this.benName = benName;
	}
	public String getBenCode() {
		return benCode;
	}
	public void setBenCode(String benCode) {
		this.benCode = benCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmountAll() {
		return amountAll;
	}
	public void setAmountAll(double amountAll) {
		this.amountAll = amountAll;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}

