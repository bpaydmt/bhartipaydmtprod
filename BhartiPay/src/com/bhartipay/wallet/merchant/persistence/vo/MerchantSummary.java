package com.bhartipay.wallet.merchant.persistence.vo;

public class MerchantSummary{
	
	 private String merchantID;
	 private String merchantUser;
	 private String merchantName;
	 private String merchantEmail;
	 private String merchantMobile;
	 private String merchantSite;
	 private String merchantStatus;
	 private String merchantType;
	 private String merchantPassword;
	 private String merchantConfirmPassword;
	 private String encryptionKey;
	 private String aggreatorid;
	 private String  statusCode;
	 private String createDate;
	 private String createBy;
	 
	 
	 
	 
	 
	 
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getMerchantConfirmPassword() {
		return merchantConfirmPassword;
	}
	public void setMerchantConfirmPassword(String merchantConfirmPassword) {
		this.merchantConfirmPassword = merchantConfirmPassword;
	}
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getMerchantUser() {
		return merchantUser;
	}
	public void setMerchantUser(String merchantUser) {
		this.merchantUser = merchantUser;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMerchantEmail() {
		return merchantEmail;
	}
	public void setMerchantEmail(String merchantEmail) {
		this.merchantEmail = merchantEmail;
	}
	public String getMerchantMobile() {
		return merchantMobile;
	}
	public void setMerchantMobile(String merchantMobile) {
		this.merchantMobile = merchantMobile;
	}
	public String getMerchantSite() {
		return merchantSite;
	}
	public void setMerchantSite(String merchantSite) {
		this.merchantSite = merchantSite;
	}
	public String getMerchantStatus() {
		return merchantStatus;
	}
	public void setMerchantStatus(String merchantStatus) {
		this.merchantStatus = merchantStatus;
	}
	public String getMerchantPassword() {
		return merchantPassword;
	}
	public void setMerchantPassword(String merchantPassword) {
		this.merchantPassword = merchantPassword;
	}
	public String getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	 
	 
}
