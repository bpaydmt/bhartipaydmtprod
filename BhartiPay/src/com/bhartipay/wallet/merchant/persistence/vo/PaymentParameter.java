package com.bhartipay.wallet.merchant.persistence.vo;

public class PaymentParameter {
private String mid;
private String txnId;
private String txnAmount;
private String merchantUserName;
private String callBackURL;
private String others;
private String userAgent;
private String reqIp;
private  String statusCode;
private  String statusDesc;
private String merchantTxnId;
private double amount;
private String merchantName;
private  String walletMobileNo;
private  String UserStatus;
private  String validateBy;
private String aggreatorId;




public String getAggreatorId() {
	return aggreatorId;
}
public void setAggreatorId(String aggreatorId) {
	this.aggreatorId = aggreatorId;
}
public String getWalletMobileNo() {
	return walletMobileNo;
}
public void setWalletMobileNo(String walletMobileNo) {
	this.walletMobileNo = walletMobileNo;
}
public String getUserStatus() {
	return UserStatus;
}
public void setUserStatus(String userStatus) {
	UserStatus = userStatus;
}
public String getValidateBy() {
	return validateBy;
}
public void setValidateBy(String validateBy) {
	this.validateBy = validateBy;
}
public String getMerchantName() {
	return merchantName;
}
public void setMerchantName(String merchantName) {
	this.merchantName = merchantName;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public String getMerchantTxnId() {
	return merchantTxnId;
}
public void setMerchantTxnId(String merchantTxnId) {
	this.merchantTxnId = merchantTxnId;
}
public String getStatusCode() {
	return statusCode;
}
public void setStatusCode(String statusCode) {
	this.statusCode = statusCode;
}
public String getStatusDesc() {
	return statusDesc;
}
public void setStatusDesc(String statusDesc) {
	this.statusDesc = statusDesc;
}
public String getUserAgent() {
	return userAgent;
}
public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
}
public String getReqIp() {
	return reqIp;
}
public void setReqIp(String reqIp) {
	this.reqIp = reqIp;
}
public String getMid() {
	return mid;
}
public void setMid(String mid) {
	this.mid = mid;
}
public String getTxnId() {
	return txnId;
}
public void setTxnId(String txnId) {
	this.txnId = txnId;
}
public String getTxnAmount() {
	return txnAmount;
}
public void setTxnAmount(String txnAmount) {
	this.txnAmount = txnAmount;
}
public String getMerchantUserName() {
	return merchantUserName;
}
public void setMerchantUserName(String merchantUserName) {
	this.merchantUserName = merchantUserName;
}
public String getCallBackURL() {
	return callBackURL;
}
public void setCallBackURL(String callBackURL) {
	this.callBackURL = callBackURL;
}
public String getOthers() {
	return others;
}
public void setOthers(String others) {
	this.others = others;
}
}
