package com.bhartipay.wallet.merchant.persistence.vo;



public class MerchentBean {
	
	private String userId;
	private String walletId;
	private double amount;
	private String mid;
	private String txnId;
	private String ipImei;
	private String otp;
	private String aggreatorid;

	
	
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getIpImei() {
		return ipImei;
	}
	public void setIpImei(String ipImei) {
		this.ipImei = ipImei;
	}
	
	
	
	
	

}
