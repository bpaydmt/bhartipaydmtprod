package com.bhartipay.wallet.merchant.action;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.views.freemarker.tags.SetModel;

import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.kyc.Utils.ObjectFactory;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.MerchentBean;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.bhartipay.wallet.merchant.service.impl.MerchantServiceImpl;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;

public class PaymentAction extends BaseAction{
	
	MerchantService mService=new MerchantServiceImpl();
	UserService service=new UserServiceImpl();
	Logger logger=Logger.getLogger(PaymentAction.class);
	WalletConfiguration config=new WalletConfiguration();
	ObjectFactory oFactory = new ObjectFactory();
	
	
	public WalletConfiguration getConfig() {
		return config;
	}

	public void setConfig(WalletConfiguration config) {
		this.config = config;
	}

	public boolean generateCsrfToken(){
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

            session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
        }

        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

        session.put("csrfPreventionSalt", salt);
        return true;
	}
	public String webPaymentRequest(){
		generateCsrfToken();
	logger.debug("start web payment request calling.");
		String mid;
		String txnId;
		String txnAmount;
		String merchantUserName;
		String callBackURL;
		String others;
		String billingDtls;
		String shippingDtls;
		String country;
		String currency;
		String walletMobileNo;
		
	mid=request.getParameter("mid");
	logger.debug("*********************************************************************Mid: "+mid);
	txnId=request.getParameter("txnId");
	logger.debug("*********************************************************************txnId: "+txnId);
	txnAmount=request.getParameter("txnAmount");
	logger.debug("*********************************************************************txnAmount	: "+txnAmount);
	merchantUserName=request.getParameter("merchantUserName");
	logger.debug("*********************************************************************merchantUserName: "+merchantUserName);
	callBackURL=request.getParameter("callBackURL");
	logger.debug("*********************************************************************callBackURL: "+callBackURL);
	others=request.getParameter("others");
	logger.debug("*********************************************************************others: "+others);
	billingDtls=request.getParameter("billingDtls");
	logger.debug("*********************************************************************billingDtls: "+billingDtls);
	shippingDtls=request.getParameter("shippingDtls");
	logger.debug("*********************************************************************shippingDtls: "+shippingDtls);
	walletMobileNo=request.getParameter("walletMobileNo");
	logger.debug("*********************************************************************walletMobileNo: "+walletMobileNo);
	
	
	/*try
	{
		String AggreatorId = "I4dSyPlIIw1AB6iXi2sQwQ==";
				mid="MERC001009";
				txnId="yzYSjYtvvLoInKsvxcarrQ==";
				txnAmount="72zddUQiNGD84P+Tgc+iYA==";
				merchantUserName="yq7rrwD0CVO3yXjSQC+LeGqNRaYLcGkJ4ntMglyZLUk=";
				callBackURL="EOzcM9DIHjmAlSbVaHIW5Z6PGDL8dGc1EBg2/j94GOFcriCcsnSwYS3sMaCph98y0+U3hp+1dlMgWFj5PpGl1flIy4NBzf/H/JUW7mvuXpc=";
				others="hBf7VWUHJY3+pugoJsTymQ==";
				walletMobileNo="lzRqeEGfCGnl9G2WILGTKg==";
	}
	finally
	{
		
	}*/
	try
	
	//delete
	{
	
	
//		mid="MERC001009";
//		txnId=EncryptionDecryption.getEncrypted("Txn123","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"Txn123";
//		txnAmount=EncryptionDecryption.getEncrypted("10000","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"10";
//		merchantUserName=EncryptionDecryption.getEncrypted("santosh3","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"Abcd";
//		callBackURL="EOzcM9DIHjmAlSbVaHIW5Z6PGDL8dGc1EBg2/j94GOFcriCcsnSwYS3sMaCph98y0+U3hp+1dlMgWFj5PpGl1flIy4NBzf/H/JUW7mvuXpc=";//EncryptionDecryption.getEncrypted("https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=6-45Wq2qIvGcX9zKtdAG","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"123";
//		others=EncryptionDecryption.getEncrypted("others","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"others";
//		billingDtls=EncryptionDecryption.getEncrypted("billingDtls","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"billingDtls";
//		shippingDtls=EncryptionDecryption.getEncrypted("shippingDtls","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"shippingDtls";
//		walletMobileNo=EncryptionDecryption.getEncrypted("7011605528","mA3AMr0wcGLsnktQun9BDuBkRolWgviZpcgtnb24wWs=");//"8505830303";
		//String stCall = EncryptionDecryption.getdecrypted("EOzcM9DIHjmAlSbVaHIW5Z6PGDL8dGc1EBg2/j94GOFcriCcsnSwYS3sMaCph98y0+U3hp+1dlMgWFj5PpGl1flIy4NBzf/H/JUW7mvuXpc=", key)
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	//delete

	
	
	logger.debug("**********************************Setting paymentParameter for calling validateSaveTxn service***********************************");
	PaymentParameter pmt=new PaymentParameter();
	pmt.setMid(mid);
	pmt.setMerchantTxnId(txnId);
	pmt.setTxnAmount(txnAmount);
	pmt.setMerchantUserName(merchantUserName);
	pmt.setCallBackURL(callBackURL);
	pmt.setOthers(others);
	pmt.setWalletMobileNo(walletMobileNo);
	
	String domainName=request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).substring(0, request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).indexOf("/"));
	session.put("domainName",domainName);
	config.setDomainName(domainName);
	String aggId=service.getAggIdByDomain(config);
	pmt.setAggreatorId(aggId);
	if(aggId.equalsIgnoreCase("error")){
		return "Error";
	}else{
		
	}
	config.setAggreatorid(aggId);
	session	.put("aggId", aggId);
	Map<String,String> mapResult=service.getWalletConfigPG(config);
	if(mapResult!=null){
		session.put("mapResult",mapResult);
		System.out.println(mapResult);
		session.put("logo",mapResult.get("logopic"));
		session.put("banner",mapResult.get("bannerpic"));
	}
	
	logger.debug("*********************************before calling validateSaveTxn************************************");
	String userAgent=request.getHeader("User-Agent");
	String ipAddress =  request.getRemoteAddr();
if (ipAddress == null) {
	ipAddress = request.getHeader("X-FORWARDED-FOR");
}
	/*if(pmt != null)
	{
		String objGson = new Gson().toJson(pmt);
		try
		{
			boolean flag = oFactory.checkScript(objGson);
			if(!flag)
			{
				addActionError("Oops  something went wronge. Please try after sometime !!");
				return "fail";
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			addActionError("Oops  something went wronge. Please try after sometime !!");
			return "fail";
		}
	
	}*/
	PaymentParameter result=mService.validateSaveTxn(pmt,ipAddress,userAgent);
	logger.debug("************Getting result after calling validateSavetXn service "+result.getStatusCode()+"**********************"+result.getStatusDesc()+"**************");
	System.out.println("======================================================"+result.getStatusCode()+result.getStatusDesc()+result.getTxnId());
	session.put("mobileNo", "*******"+result.getWalletMobileNo().substring(7, result.getWalletMobileNo().length()));
	session.put("PaymentParameter", result);
	if(result.getStatusCode().equalsIgnoreCase("1000")){
		
		if(result.getValidateBy().equalsIgnoreCase("OTP"))
		{
			return "otp";
		}
		return "success";
	}
	if(result.getStatusCode().equalsIgnoreCase("1001")){
		addActionError("Failed due to some technical error.");
		return "fail";
	}
	if(result.getStatusCode().equalsIgnoreCase("7000")){
		addActionError("An error has occurred.");
		return "fail";
}
	if(result.getStatusCode().equalsIgnoreCase("7034")){
		addActionError("Invalid username mid");
		return "fail";
	}
	if(result.getStatusCode().equalsIgnoreCase("7033")){
		addActionError("Invalid mid");
		return "fail";
	}
	logger.info("end web payment request calling. no any result code matches so return default as fail.");	
		return "fail";
	}
	
	public String walletPaymentConfirm(){
		
		logger.debug("*********************************calling  walletPaymentConfirm()************************************");
		PaymentParameter paymentParameter=(PaymentParameter)session.get("PaymentParameter");
		User user=new User();//(User) session.get("User");
		
		String aggId = (String)session.get("aggId");
		WalletMastBean bean = new UserServiceImpl().getUserByMobileNo(paymentParameter.getWalletMobileNo(), aggId);
		/*User user = new User();
		user.setId("AGEN001042");
		user.setWalletid("993504128705062017183756");
		*/
		
		user.setId(bean.getId());
		user.setWalletid(bean.getWalletid());
		logger.debug("*********************************builing merchantBean parameters for calling merchantPayment Service************************************");
		MerchentBean merchentBean=new MerchentBean();
		merchentBean.setWalletId(user.getWalletid());
		logger.debug("wallet id****************************"+user.getWalletid());
		merchentBean.setUserId(user.getId());
		logger.debug("user id****************************"+user.getId());
		merchentBean.setTxnId(paymentParameter.getTxnId());
		logger.debug("txn id****************************"+paymentParameter.getTxnId());
		merchentBean.setIpImei("");
		logger.debug("IpImei****************************blank string");
		merchentBean.setMid(paymentParameter.getMid());
		logger.debug("MID****************************"+paymentParameter.getMid());
		merchentBean.setAmount(paymentParameter.getAmount());
		logger.debug("Amount****************************"+paymentParameter.getAmount());
		merchentBean.setOtp(request.getParameter("otp"));
		merchentBean.setAggreatorid(/*"AGGR001035"*/aggId);
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress =  request.getRemoteAddr();
	if (ipAddress == null) {
		ipAddress = request.getHeader("X-FORWARDED-FOR");
	}
		if(merchentBean != null)
		{
			String objGson = new Gson().toJson(merchentBean);
			try
			{
				boolean flag = oFactory.checkScript(objGson);
				if(!flag)
				{
					addActionError("Oops  something went wronge. Please try after sometime !!");
					return "fail";
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				addActionError("Oops  something went wronge. Please try after sometime !!");
				return "fail";
			}
		
		}
		String paymentResult=mService.merchentPaymet(merchentBean,ipAddress,userAgent);
		logger.debug("getting result after calling service merchantPaymet****************************"+paymentResult);
		if(paymentResult!=null&&paymentResult.equalsIgnoreCase("1000")){
			paymentParameter.setStatusCode(paymentResult);
			paymentParameter.setStatusDesc("success");
			session.put("PaymentParameter", paymentParameter);
			request.getSession().putValue("PaymentParameter", paymentParameter);
			logger.debug("return succecss after calling service****************************");
			return "success";
		}else{	
			paymentParameter.setStatusCode(paymentResult);
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			logger.debug("return fail after calling service****************************");
			return "fail";
		}
	
	}
	
	public String walletPaymentCancel(){
		logger.debug("************************calling walletPaymentCancel****************************");
		PaymentParameter paymentParameter=(PaymentParameter)session.get("PaymentParameter");
		User user=(User) session.get("User");
		
			paymentParameter.setStatusCode("9999");
			paymentParameter.setStatusDesc("payment cancel by user");
			session.put("PaymentParameter", paymentParameter);
			logger.debug("*********************return succecss on calling walletPaymentCancel status code 9999 for cancel****************************");
			return "success";
		
	}
}
