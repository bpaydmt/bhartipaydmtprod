package com.bhartipay.wallet.merchant.action;

import java.io.InputStream;
import java.security.SecureRandom;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.MerchantSummary;
import com.bhartipay.wallet.merchant.persistence.vo.MerchentBean;
import com.bhartipay.wallet.merchant.service.impl.MerchantServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class ManageMerchant extends BaseAction{
	
   public static Logger logger=Logger.getLogger(ManageMerchant.class);	
	MerchantSummary merchantBean=new MerchantSummary();
	Map<String,String> statusList=new LinkedHashMap<String, String>();
	Map<String,String> merchantTypeList=new LinkedHashMap<String, String>();
	MerchantService mService=new MerchantServiceImpl();
	Properties prop =null;
	public ManageMerchant(){
		/* added by neeraj call error code using properties file. */
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}}
		/*end by neeraj*/
	
	
	public Map<String, String> getStatusList() {
		return statusList;
	}

	public void setStatusList(Map<String, String> statusList) {
		this.statusList = statusList;
	}

	public MerchantSummary getMerchantBean() {
		return merchantBean;
	}

	public void setMerchantBean(MerchantSummary merchantBean) {
		this.merchantBean = merchantBean;
	}
	
	
	
	

	public Map<String, String> getMerchantTypeList() {
		return merchantTypeList;
	}


	public void setMerchantTypeList(Map<String, String> merchantTypeList) {
		this.merchantTypeList = merchantTypeList;
	}


	public String addMerchant(){
		generateCsrfToken();
		logger.debug("************************calling addMerchant()************************");
		statusList.put("A","Active");
		statusList.put("D","Deactive");
		
		merchantTypeList.put("WM","Wallet merchant");
		merchantTypeList.put("DMT","DMT merchant");
		merchantTypeList.put("Apartments","Apartments");
		
		System.out.println("add merchant service");
		logger.debug("************************calling addMerchant() retunr success************************");
		return "success";
	}
	
	
	
	
	public String saveAddMerchant(){
		User user=(User)session.get("User");
		logger.debug("************************calling saveAddMerchant()************************");
		statusList.put("A","Active");
		statusList.put("D","Deactive");
		
		merchantTypeList.put("WM","Wallet merchant");
		merchantTypeList.put("DMT","DMT merchant");
		
		
		if(merchantBean==null){
			logger.debug("************************getting merchhantBean null************************");
			addActionError("An error has occurred.");
			return "fail";
		}
		if(merchantBean.getMerchantUser()==null||merchantBean.getMerchantUser().isEmpty()){
			logger.debug("************************merchant user="+merchantBean.getMerchantUser()+"************************");
			addActionError("Please enter merchant username.");
			return "fail";
		}
		if(merchantBean.getMerchantName()==null||merchantBean.getMerchantName().isEmpty()){
			logger.debug("************************merchant name="+merchantBean.getMerchantName()+"************************");
			addActionError("Please enter merchant name.");
			return "fail";
		}
		if(merchantBean.getMerchantEmail()==null||merchantBean.getMerchantEmail().isEmpty()){
			logger.debug("************************merchant email="+merchantBean.getMerchantEmail()+"************************");
			addActionError("Please enter merchant email.");
			return "fail";
		}
		if(merchantBean.getMerchantMobile()==null||merchantBean.getMerchantMobile().isEmpty()){
			logger.debug("************************merchant mobile="+merchantBean.getMerchantMobile()+"************************");
			addActionError("Please enter merchant mobile number.");
			return "fail";
		}
		/*if(merchantBean.getMerchantSite()==null||merchantBean.getMerchantSite().isEmpty()){
			logger.debug("************************merchant site="+merchantBean.getMerchantSite()+"************************");
			addActionError("Please enter merchant business web URL.");
			return "fail";
		}*/
		if(merchantBean.getMerchantPassword()==null||merchantBean.getMerchantPassword().isEmpty()){
			logger.debug("************************merchant merchantPassword="+merchantBean.getMerchantPassword()+"************************");
			addActionError("Please enter password.");
			return "fail";
		}
		if(merchantBean.getMerchantConfirmPassword()==null||merchantBean.getMerchantConfirmPassword().isEmpty()){
			logger.debug("************************merchant merchantConfirmPassword="+merchantBean.getMerchantConfirmPassword()+"************************");
			addActionError("Please enter confirm password.");
			return "fail";
		}
		if(!merchantBean.getMerchantPassword().equals(merchantBean.getMerchantConfirmPassword())){
			logger.debug("************************Password and confirm password must be same.************************");
			addActionError("Password and confirm password must be same.");
			return "fail";
		}
		logger.debug("************************calling saveMerchant Service ************************");
		merchantBean.setCreateBy(user.getId());
		merchantBean.setAggreatorid(user.getAggreatorid());
		
		
		String userAgent=request.getHeader("User-Agent");
		
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		MerchantSummary m=mService.saveMerchant(merchantBean,ipAddress,userAgent);
		setMerchantBean(m);
		logger.debug("************************getting result on calling service saveMerchant.************************result is :::: "+m.getStatusCode());
		if(m.getStatusCode().equalsIgnoreCase("1000")){
			addActionMessage("Merchant added successfully.");
		return "success";	
		}
		if(m.getStatusCode().equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("110011"));
			return "fail";
			
		}
		if(m.getStatusCode().equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("7000"));
			return "fail";
		}
		if(m.getStatusCode().equalsIgnoreCase("8501")){
			addActionError(prop.getProperty("8501"));
			return "fail";
		}
		if(m.getStatusCode().equalsIgnoreCase("8503")){
			addActionError(prop.getProperty("8503"));
			return "fail";
		}
		if(m.getStatusCode().equalsIgnoreCase("8504")){
			addActionError(prop.getProperty("8504"));
			return "fail";
		}
		if(m.getStatusCode().equalsIgnoreCase("8505")){
			addActionError(prop.getProperty("8505"));
			return "fail";
		}
		
		
		if(m.getStatusCode().equalsIgnoreCase("7001")){
			addActionError(prop.getProperty("7001"));
			return "fail";
		}
		else if(m.getStatusCode().equalsIgnoreCase("7002")){
			addActionError(prop.getProperty("7002"));
			return "fail";
		}
		else if(m.getStatusCode().equalsIgnoreCase("7038")){
			addActionError(prop.getProperty("7038"));
			return "fail";
		}
		else if(m.getStatusCode().equalsIgnoreCase("7041")){
			addActionError(prop.getProperty("7041"));
			return "fail";
		}
		else if(m.getStatusCode().equalsIgnoreCase("7040")){
			addActionError(prop.getProperty("7040"));
			return "fail";
		}
		
		
		
		
		
		
		
		
		logger.debug("************************result not match any condition returnig success by default.************************");

		return "success";
	}
	
	
	public String tPMerchantList(){
		User  user=(User)session.get("User");
		merchantBean.setAggreatorid(user.getAggreatorid());
		List<MerchantSummary> mList=mService.getMerchantList(merchantBean);
		request.setAttribute("mList",mList);
		return "success";
	}
	
	public boolean generateCsrfToken(){
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

            session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
        }

        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

        session.put("csrfPreventionSalt", salt);
        return true;
	}
}
