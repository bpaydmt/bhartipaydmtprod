package com.bhartipay.wallet.merchant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.MerchantSummary;
import com.bhartipay.wallet.merchant.persistence.vo.MerchentBean;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class MerchantServiceImpl implements MerchantService{

	public static Logger logger=Logger.getLogger(MerchantServiceImpl.class);
	@Override
	public MerchantSummary saveMerchant(MerchantSummary merchantSummary,String ipimei,String userAgent) {
		logger.debug("*************************calling method saveMerchnat*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(merchantSummary);		
		WebResource webResource=ServiceManager.getWebResource("MerchantManager/saveMerchant");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::"+response.getStatus()+"*********************************");
			 throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
		}
		logger.debug("getting response from service*********************************"+response);

		String output = response.getEntity(String.class);	
		merchantSummary = gson.fromJson(output, MerchantSummary.class);
		logger.debug("output result is**********"+output);
		return merchantSummary;
	}

	@Override
	public PaymentParameter validateSaveTxn(PaymentParameter parameter,String ipimei,String userAgent) {
		logger.debug("*************************calling method validateSaveTxn*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(parameter);
		
		WebResource webResource=ServiceManager.getWebResource("MerchantManager/validateSaveTxn");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::"+response.getStatus()+"*********************************");
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("getting response from service*********************************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("output result is**********"+output);
		PaymentParameter parameter2 = gson.fromJson(output, PaymentParameter.class);		
		return parameter2;
	}

	@Override
	public String merchentPaymet(MerchentBean merchantBean,String ipimei,String userAgent) {
		logger.debug("*************************calling method merchentPaymet*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(merchantBean);
		
		WebResource webResource=ServiceManager.getWebResource("MerchantManager/merchentPaymet");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::"+response.getStatus()+"*********************************");
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("getting response from service*********************************"+response);
		String output = response.getEntity(String.class);			
//		PaymentParameter parameter2 = gson.fromJson(output, PaymentParameter.class);
		logger.debug("output result is**********"+output);
		return output;
	}

	@Override
	public List<MerchantSummary> getMerchantList(MerchantSummary merchantBean) {
		logger.debug("*************************calling method getMerchantList*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(merchantBean);
		
		WebResource webResource=ServiceManager.getWebResource("MerchantManager/getMerchantList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::"+response.getStatus()+"*********************************");
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("getting response from service*********************************"+response);
		String output = response.getEntity(String.class);			
		List<MerchantSummary> parameter2 = gson.fromJson(output, OTG1.class);
		logger.debug("output result is**********"+output);
		return parameter2;
	}

	
	
	
	
}
class OTG1 extends ArrayList<MerchantSummary>{
	
}