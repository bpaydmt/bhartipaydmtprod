package com.bhartipay.wallet.aeps;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aepsresponse")
public class AepsResponseData {

	@Id
	@Column(name = "refno")
	private String refno;
	@Column(name = "payerId")
	private String payerId;
	@Column(name = "payertype")
	private String payertype;
	@Column(name = "payeeId")
	private String payeeId;
	@Column(name = "payeetype")
	private String payeetype;
	@Column(name = "txnType")
	private String txnType;
	@Column(name = "orderId")
	private String orderId;
	@Column(name = "amount")
	private int amount;
	@Column(name = "txnId")
	private String txnId;
	@Column(name = "balance")
	private String balance;
	@Column(name = "orderStatus")
	private String orderStatus;
	@Column(name = "paymentStatus")
	private String paymentStatus;
	@Column(name = "requestId")
	private String requestId;
	@Column(name = "stan")
	private String stan;
	@Column(name = "rrn")
	private String rrn;
	@Column(name = "bankAuth")
	private String bankAuth;
	@Column(name = "processingCode")
	private String processingCode;
	@Column(name = "accountBalance")
	private String accountBalance;
	@Column(name = "bankResponseCode")
	private String bankResponseCode;
	@Column(name = "bankResponseMsg")
	private String bankResponseMsg;
	@Column(name = "terminalId")
	private String terminalId;
	@Column(name = "agentId")
	private String agentId;
	@Column(name = "aadharNumber")
	private String aadharNumber;
	@Column(name = "dateTime")
	private long dateTime;
	@Column(name = "statusCode")
	private String statusCode;
	@Column(name = "statusMessage")
	private String statusMessage;
	@Column(name = "commissionAmt")
	private Double commissionAmt;
	@Column(name = "gstAmt")
	private Double gstAmt;
	@Column(name = "tdsAmt")
	private Double tdsAmt;
	@Column(name = "walletMessage")
	private String walletMessage;
	@Column(name = "isWalletFailed")
	private Boolean isWalletFailed;
	@Column(name = "bcname")
	private String bcname;
	@Column(name = "bcaddress")
	private String bcaddress;
	@Column(name = "name")
	private String name;
	@Column(name = "redirectionUrl")
	private String redirectionUrl;

	public String getRefno() {
		return refno;
	}

	public String getName() {
		return name;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

	public String getPayertype() {
		return payertype;
	}

	public void setPayertype(String payertype) {
		this.payertype = payertype;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getPayeetype() {
		return payeetype;
	}

	public void setPayeetype(String payeetype) {
		this.payeetype = payeetype;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getBankAuth() {
		return bankAuth;
	}

	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getBankResponseCode() {
		return bankResponseCode;
	}

	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}

	public String getBankResponseMsg() {
		return bankResponseMsg;
	}

	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public long getDateTime() {
		return dateTime;
	}

	public void setDateTime(long dateTime) {
		this.dateTime = dateTime;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Double getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(Double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public Double getGstAmt() {
		return gstAmt;
	}

	public void setGstAmt(Double gstAmt) {
		this.gstAmt = gstAmt;
	}

	public Double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(Double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	public String getWalletMessage() {
		return walletMessage;
	}

	public void setWalletMessage(String walletMessage) {
		this.walletMessage = walletMessage;
	}

	public Boolean getIsWalletFailed() {
		return isWalletFailed;
	}

	public void setIsWalletFailed(Boolean isWalletFailed) {
		this.isWalletFailed = isWalletFailed;
	}

	public String getBcname() {
		return bcname;
	}

	public void setBcname(String bcname) {
		this.bcname = bcname;
	}

	public String getBcaddress() {
		return bcaddress;
	}

	public void setBcaddress(String bcaddress) {
		this.bcaddress = bcaddress;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return refno+"";
	}

}
