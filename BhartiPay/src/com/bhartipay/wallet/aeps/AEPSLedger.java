package com.bhartipay.wallet.aeps;

import java.sql.Timestamp;

public class AEPSLedger {
	
	
	private String mid;
	private String agentCode;
	private String metaData;
	
	private String txnId;
	
	private String ptyTransDt;
	private int amount;
	private String status;
	private String statusCode;
	private String responseHash;
	private String  aggregator;
	private String agentId;
	private String walletId;
	private String userAgnet;
	private String ipimei;
	private String redirectionUrl;
	private String txnType;
	private String orderId;
	private String orderStatus;
	private String paymentStatus;
	private String requestId;
	private String stan;
	private String rrn;
	private String bankAuth;
	private String processingCode;
	private String bankResponseCode;
	private String bankResponseMsg;
	private String aadharNumber;
	private String statusMessage;
	private Timestamp txnDate;
	private String token;
	private String bcname;
	
	
	
	
	
	public String getBcname() {
		return bcname;
	}
	public void setBcname(String bcname) {
		this.bcname = bcname;
	}
	AEPSConfig aepsConfig;
	private String aepsChannel;
	private String type;
	
	private double commissionAmt;
	
	
	public double getCommissionAmt() {
		return commissionAmt;
	}
	public void setCommissionAmt(double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAepsChannel() {
		return aepsChannel;
	}
	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}
	
	
	
	public String getOrderId() {
		return orderId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public String getRequestId() {
		return requestId;
	}
	public String getStan() {
		return stan;
	}
	public String getRrn() {
		return rrn;
	}
	public String getBankAuth() {
		return bankAuth;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public String getBankResponseCode() {
		return bankResponseCode;
	}
	public String getBankResponseMsg() {
		return bankResponseMsg;
	}
	public String getAadharNumber() {
		return aadharNumber;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public Timestamp getTxnDate() {
		return txnDate;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}
	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}
	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getRedirectionUrl() {
		return redirectionUrl;
	}
	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}
	public AEPSConfig getAepsConfig() {
		return aepsConfig;
	}
	public void setAepsConfig(AEPSConfig aepsConfig) {
		this.aepsConfig = aepsConfig;
	}
	public String getMid() {
		return mid;
	}
	public String getMetaData() {
		return metaData;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
	public String getTxnId() {
		return txnId;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public String getPtyTransDt() {
		return ptyTransDt;
	}
	public int getAmount() {
		return amount;
	}
	public String getStatus() {
		return status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public String getResponseHash() {
		return responseHash;
	}
	public String getAggregator() {
		return aggregator;
	}
	public String getAgentId() {
		return agentId;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public void setPtyTransDt(String ptyTransDt) {
		this.ptyTransDt = ptyTransDt;
	}
	public void setAmount(int d) {
		this.amount = d;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void setResponseHash(String responseHash) {
		this.responseHash = responseHash;
	}
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getUserAgnet() {
		return userAgnet;
	}
	public String getIpimei() {
		return ipimei;
	}
	public void setUserAgnet(String userAgnet) {
		this.userAgnet = userAgnet;
	}
	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}
	
	

}
