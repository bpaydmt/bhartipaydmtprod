package com.bhartipay.wallet.aeps;

public class Data {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
