package com.bhartipay.wallet.aeps;

public class FinoHeader {

	private String ClientId;
	private String AuthKey;
	
	public String getClientId() {
		return ClientId;
	}
	public void setClientId(String clientId) {
		ClientId = clientId;
	}
	public String getAuthKey() {
		return AuthKey;
	}
	public void setAuthKey(String authKey) {
		AuthKey = authKey;
	}
	
	
}
