package com.bhartipay.wallet.aeps;


public class AEPSConfig {
	
	private long id;
	private String aggregatorId;
	private String mid;
	private String secretKey;
	private String redirectionUrl;
	private String agentAuthId;
	private String agentAuthPassword;
	private String aepsCallbackURL;
	private String aepsChannel;
	private String type;

	

	
	
	public String getAepsChannel() {
		return aepsChannel;
	}


	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}


	

	public String getAgentAuthId() {
		return agentAuthId;
	}


	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}


	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}


	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}


	public String getAepsCallbackURL() {
		return aepsCallbackURL;
	}


	public void setAepsCallbackURL(String aepsCallbackURL) {
		this.aepsCallbackURL = aepsCallbackURL;
	}


	
	
	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}
	public long getId() {
		return id;
	}
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	public String getMid() {
		return mid;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public void setMid(String mid) {
		this.mid = mid;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
