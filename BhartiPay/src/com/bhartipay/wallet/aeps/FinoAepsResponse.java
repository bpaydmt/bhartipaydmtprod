package com.bhartipay.wallet.aeps;

public class FinoAepsResponse {
	
	private String ClientRefID;
	private String DisplayMessage;
	private String ResponseCode;
	private String ClientRes;
	
	public String getClientRefID() {
		return ClientRefID;
	}
	public void setClientRefID(String clientRefID) {
		ClientRefID = clientRefID;
	}

	public String getDisplayMessage() {
		return DisplayMessage;
	}
	public void setDisplayMessage(String displayMessage) {
		DisplayMessage = displayMessage;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getClientRes() {
		return ClientRes;
	}
	public void setClientRes(String clientRes) {
		ClientRes = clientRes;
	}

}
