package com.bhartipay.wallet.transactionbk2.service;

import java.util.List;
import java.util.Map;

import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.FinoAepsResponse;
import com.bhartipay.wallet.aeps.TokenResponse;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;


/**
 * 
 * @author Ambuj Singh
 *
 */
public interface TransactionService {


	/**
	 * 
	 * @param trxAmount
	 * @param walletId
	 * @param mobileNo
	 * @param trxReasion
	 * @return
	 */
	public String savePGTrx(double trxAmount,String walletId,String mobileNo,String trxReasion,String payTxnid,String aggreatorid,String ipimei,String userAgent);
	 /**
	  * 
	  * @param userId
	  * @param txnId
	  * @param pgTxnId
	  * @param txnAmount
	  * @param status
	  * @param imei
	  * @return
	  */
	public String updatePGTrx(String hash,String responseparams,String userId, String txnId,String pgTxnId, double txnAmount,String status,String imei);
	
	/**
	 * 
	 * @param walletid
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public List<PassbookBean> showPassbook(TxnInputBean inputBean);
	
	/**
	 * 
	 * @param walletid
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public List<PassbookBean> showOrdersView(TxnInputBean inputBean);
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String walletToWalletTransfer(TxnInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String cashIn(TxnInputBean inputBean,String ipimei,String userAgent);
	
	
	/**
	 * 
	 * @param wtob
	 * @return
	 */
	public WalletToBankTxnMast walletToBankTransfer(WalletToBankTxnMast wtob);
	
	/**
	 * 
	 * @param imputBean
	 * @return
	 */
	public DMTBean customerValidation(DMTInputBean imputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public DMTInputBean customerRegistration(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public DMTInputBean oTCConfirmation(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String saveAskMoney(TxnInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public AskMoneyBean getCashOut(TxnInputBean inputBean,String ip,String ua);
	
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public AskMoneyBean cashOutUpdate(AskMoneyBean inputBean);
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist checkWalletExistRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist createWalletRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist verifyRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist resendOtp(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getUserDetails(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails addBeneficiaryRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails deleteBeneficiaryRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails mrTransfer(DMTInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails reinitiateMrTransfer(DMTInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getUserBalance(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getTransStatus(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getTransHistory(DMTInputBean inputBean);
	
	/***
	 * 
	 * @param bean
	 * @return
	 */
	public String getEscrowAccBal(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	List<EscrowBean> getEscrowTxnList(EscrowBean bean);
	
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public List<CashDepositMast> getCashDepositReqByUserId(CashDepositMast depositMast,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param cashDepositMast
	 * @return
	 */
	public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent);
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public List<CashDepositMast> getCashDepositReqByAggId(CashDepositMast depositMast);
	
	
	/**
	 * 
	 * @param cashDepositMast
	 * @return
	 */
	public String acceptCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent);
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public String rejectCashDeposit(CashDepositMast depositMast);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public RefundMastBean requestRefund(RefundMastBean bean,String ipimei,String userAgent);

	/**
	 * 
	 * @param bean
	 * @return
	 */
	public List<RefundMastBean> getRefundReqByAggId(RefundMastBean bean);
	
	/**
	 * 
	 * @param refundMastBean
	 * @return
	 */
	public String rejectRefundReq(RefundMastBean refundMastBean);
	
	/**
	 * 
	 * @param refundMastBean
	 * @return
	 */
	public String acceptRefundReq(RefundMastBean refundMastBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getPrePiadCardReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getRechargeReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getDMTReConReport(EscrowBean bean);
	
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getPGReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param surchargeBean
	 * @return
	 */
	public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean);
	
	/**
	 * 
	 * @param surchargeBean
	 * @return
	 */
	public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public SurchargeBean calculateDMTSurcharge(SurchargeBean bean);
	
	
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	public String getWalletBalance(String walletId);
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	public String getWalletBalanceById(String walletId);
	
	
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	
	public List<CashDepositMast> getCashDepositReportByAggId(CashDepositMast depositMast);
	
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	
	 public List<PassbookBean> showPassbookById(TxnInputBean inputBean);
	 
	 
	 /**
	  * 
	  * @param inputBean
	  * @return
	  */
	 public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(TxnInputBean inputBean);
	 
	 
		/**
		 * 
		 * @param walletid
		 * @param stDate
		 * @param endDate
		 * @return
		 */
		public List<PassbookBean> showCashBackPassbook(TxnInputBean inputBean);
		
		/**
		 * 
		 * @param inputBean
		 * @return
		 */
		public List<PassbookBean> showCashBackPassbookById(TxnInputBean inputBean) ;
		
		
		/**
		 * 
		 * @param depositMast
		 * @return
		 */
		public List<CashDepositMast> getCDAReqByAggId(CashDepositMast depositMast) ;
		
		/**
		 * 
		 * @param cashDepositMast
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public String approvedCashDeposit(CashDepositMast cashDepositMast,String ipimei, String userAgent);
		
		/**
		 * 
		 * @param depositMast
		 * @return
		 */
		public String rejectApproverCashDeposit(CashDepositMast depositMast);
		
		/**
		 * 
		 * @return
		 */
		public Map<String, String> getPartnerList() ;
		
		
		/**
		 * 
		 * @return
		 */
		public Map<String, String> getTxnSourceList();
		
		/**
		 * 
		 * @param bean
		 * @return
		 */
		public PartnerPrefundBean savePartnerPrefund(PartnerPrefundBean bean);
	 
		/**
		 * 
		 * @param b2cMoneyTxnMast
		 * @return
		 */
		 public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2cMoneyTxnMast);
		 
		 /**
		  * 
		  * @param aepsLedger
		  * @return
		  */
		 public AEPSLedger saveAepsRequest(AEPSLedger aepsLedger);
		 
		 public MATMLedger saveMatmRequest(MATMLedger matmLedger);
		 
		 /**
		  * 
		  * @param aepsLedger
		  * @return
		  */
		 public TokenResponse generateToken(AEPSLedger aepsLedger);
		 
		 /**
		  * 
		  * @param aepsLedger
		  * @return
		  */
		 public AEPSLedger updateAepsRequest(AEPSLedger aepsLedger);
		 
		 /**
		  * 
		  * @param jsonText
		  * @return
		  */
		 public String finoAepsResponse(String jsonText);
		 
		 /**
		  * 
		  * @param aepsLedger
		  * @return
		  * @throws Exception
		  */
		 public FinoAepsResponse finoAEPSCall(AEPSLedger aepsLedger)throws Exception;
		
		
		 
		 public String finoMatmResponse(String jsonText);
		public List<CashDepositMast> getCashDepositReqByUserIdPending(CashDepositMast depositMast, String ipAddress,
				String userAgent);
		 
		 
	 
}
