package com.bhartipay.wallet.cme.action;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.bhartipay.lean.DistributorIdAllot;
import com.bhartipay.wallet.cme.service.CMEService;
import com.bhartipay.wallet.cme.service.impl.CMEServiceImpl;
import com.bhartipay.wallet.cme.vo.CMERequestBean;
import com.bhartipay.wallet.cme.vo.CMEResponseBean;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.gson.Gson;

import appnit.com.crypto.CheckSumHelper;

public class ManageCME extends BaseAction implements ServletResponseAware {

	HttpServletResponse response;
	CMERequestBean cmeBean = new CMERequestBean();
	Map<String, String> documentTypeList = new LinkedHashMap<String, String>();
	Map<String, String> bankDetailList = new LinkedHashMap<String, String>();
	CMEService cmeService = new CMEServiceImpl();
TransactionService tService=new TransactionServiceImpl();

  WalletMastBean walletBean = new WalletMastBean();

  
  
	public WalletMastBean getWalletBean() {
	return walletBean;
}

public void setWalletBean(WalletMastBean walletBean) {
	this.walletBean = walletBean;
}

	public CMERequestBean getCmeBean() {
		return cmeBean;
	}

	public void setCmeBean(CMERequestBean cmeRequestBean) {
		this.cmeBean = cmeRequestBean;
	}

	public Map<String, String> getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(Map<String, String> documentTypeList) {
		this.documentTypeList = documentTypeList;
	}

	public Map<String, String> getBankDetailList() {
		return bankDetailList;
	}

	public void setBankDetailList(Map<String, String> bankDetailList) {
		this.bankDetailList = bankDetailList;
	}

	public String cMERequest() {

		User user = (User) session.get("User");
		setBankDetailList(cmeService.getBankDtl(user.getId()));
		setDocumentTypeList(cmeService.getDeposittype());

		return "success";
	}

	public String saveCMERequest() {
		User user = (User) session.get("User");

		setBankDetailList(cmeService.getBankDtl(user.getId()));
		setDocumentTypeList(cmeService.getDeposittype());
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("CME");
		File dir = new File(filePath, user.getId());
		boolean mkdir = dir.mkdir();
		filePath = filePath.concat("/" + user.getId());

		
		if (cmeBean.getMyFile1() != null || cmeBean.getMyFile1FileName() != null
				|| cmeBean.getMyFile1ContentType() != null) {
			try {
			
				if ((cmeBean.getMyFile1ContentType().equalsIgnoreCase("image/png")
						|| cmeBean.getMyFile1ContentType().equalsIgnoreCase("image/jpeg")
						|| cmeBean.getMyFile1ContentType().equalsIgnoreCase("application/pdf"))
						&&(cmeBean.getMyFile1FileName().toLowerCase().endsWith(".png")
								||cmeBean.getMyFile1FileName().toLowerCase().endsWith(".jpeg")
								||cmeBean.getMyFile1FileName().toLowerCase().endsWith(".pdf"))
						&& cmeBean.getMyFile1().length() <= 1000000) {
					File fileToCreate1 = new File(filePath, cmeBean.getMyFile1FileName());
					FileUtils.copyFile(cmeBean.getMyFile1(), fileToCreate1);
					cmeBean.setReciptDoc("./CME/" + user.getId() + "/" + cmeBean.getMyFile1FileName());

				} else {
					addActionError(
							"Only png, jpeg and pdf file format are allowed and file size must not exceed 1 MB.");
					return "fail";
				}
				if (cmeBean.getMyFile1().length() > 1000000) {
					addActionError(
							"Only png, jpeg and pdf file format are allowed and file size must not exceed 1 MB.");
					cmeBean.setReciptDoc("");
					return "fail";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		cmeBean.setAggreatorId(user.getAggreatorid());

		if (user.getUsertype() == 6) {
			cmeBean.setUserId(user.getSubAggregatorId());
		} else {
			cmeBean.setUserId(user.getId());
		}

		TreeMap<String,String>  object=new TreeMap<String,String>(); 
		object.put("aggreatorId", cmeBean.getAggreatorId());
        object.put("userId", cmeBean.getUserId());
        object.put("walletId", user.getWalletid());
        object.put("bankName",cmeBean.getBankName());
        object.put("branchName",cmeBean.getBranchName());
        object.put("docType",cmeBean.getDocType());
        object.put("txnRefNo",cmeBean.getTxnRefNo());
        object.put("amount",""+cmeBean.getAmount());
        object.put("depositeDate",cmeBean.getDepositeDate());
        object.put("remarks",cmeBean.getRemarks());
        object.put("token",user.getToken());
        
        String checkSumString="";
		try {
			checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmeBean.setCHECKSUMHASH(checkSumString);
		
		cmeBean.setWalletId(user.getWalletid());
		CMEResponseBean cme = cmeService.saveCMERequest(cmeBean, ipAddress, userAgent);

		if (cme != null && cme.getStatusCode().equalsIgnoreCase("1000")) {

			addActionMessage("Request has been submitted successfully.");
			cmeBean = new CMERequestBean();
			return "success";
		} else {
			addActionError(cme.getStatusMsg());
			return "fail";
		}

		// return "success";
	}

	public String viewCMERequests() {
		return "success";
	}

	public String cMETransactionHistory() {
		User user = (User) session.get("User");
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		cmeBean.setAggreatorId(user.getAggreatorid());
		if (user.getUsertype() == 6) {
			cmeBean.setUserId(user.getSubAggregatorId());
		} else {
			cmeBean.setUserId(user.getId());
		}

		CMEResponseBean respBean = cmeService.getCMERequestList(cmeBean, ipAddress, userAgent);
		if (respBean.getStatusCode().equalsIgnoreCase("1000")) {
			request.setAttribute("requestList", respBean.getResPayload().getCmeRequestList());
		}

		return "success";
	}

	public String cMEAcountStatement() {
		User user = (User) session.get("User");
		cmeBean.setWalletId(user.getWalletid());
		cmeBean.setAggreatorId(user.getAggreatorid());
		if (user.getUsertype() == 6) {
			cmeBean.setUserId(user.getSubAggregatorId());
		} else {
			cmeBean.setUserId(user.getId());
		}
		CMEResponseBean cmeResponseBean = cmeService.getCMEAccountStsList(cmeBean);
		if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
			request.setAttribute("cmeAccountStmt", cmeResponseBean.getResPayload().getcMEAccountStsList());
		}
		return "success";
	}
	
	
	/*public String movementReportBySupDist() {
		User user = (User) session.get("User");
		cmeBean.setWalletId(user.getWalletid());
		cmeBean.setAggreatorId(user.getAggreatorid());
		
			cmeBean.setUserId(user.getId());
		
		CMEResponseBean cmeResponseBean = cmeService.movementReportBySupDist(cmeBean);
		if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
			request.setAttribute("cmeAccountStmt", cmeResponseBean.getResPayload().getcMEAccountStsList());
		}
		return "success";
	}*/
	
	public String movementReportByDist() {
		User user = (User) session.get("User");
		cmeBean.setWalletId(user.getWalletid());
		cmeBean.setAggreatorId(user.getAggreatorid());
		
			cmeBean.setUserId(user.getId());
		
		CMEResponseBean cmeResponseBean = cmeService.movementReportBySupDist(cmeBean);
		if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
			request.setAttribute("cmeAccountStmt", cmeResponseBean.getResPayload().getcMEAccountStsList());
		}
		return "success";
	}
	
	
	

	public String cMETOAgentTransfer() {
		return "success";
	}

	public String dMTBalance() {
		User user = (User) session.get("User");
		cmeBean.setWalletId(user.getWalletid());
		CMEResponseBean respBean = cmeService.getDMTBalance(cmeBean);
		cmeBean.setAmount(respBean.getResPayload().getDmtBalance());
		request.setAttribute("balance", "" + cmeBean.getAmount());
		return "success";
	}

	public String getAgnDtlByUserId() {
		try {
			User user = (User) session.get("User");
			if (user.getUsertype() == 3) {
				cmeBean.setDistributerId(user.getId());
			}
			cmeBean.setAggreatorId(user.getAggreatorid());

			CMEResponseBean cmeResponseBean = cmeService.getAgnDtlByUserId(cmeBean);
			JSONObject json = new JSONObject();
			Gson gson = new Gson();
			if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
				JSONParser parser = new JSONParser();

				String agent = gson.toJson(cmeResponseBean.getResPayload().getAgentDtl());
				JSONObject js = (JSONObject) parser.parse(agent);
				json.put("agentDetail", js);
				response.setContentType("application/json");
				response.getWriter().println(json);
			} else {
				json.put("msg", cmeResponseBean.getStatusMsg());
				response.setContentType("application/json");
				response.getWriter().println(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	
	public String getDistributorDtlByUserId() {
		try {
			User user = (User) session.get("User");
			cmeBean.setAggreatorId(user.getAggreatorid());

			CMEResponseBean cmeResponseBean = cmeService.getDistributorDtlByUserId(cmeBean);
			JSONObject json = new JSONObject();
			Gson gson = new Gson();
			if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
				JSONParser parser = new JSONParser();

				String agent = gson.toJson(cmeResponseBean.getResPayload().getAgentDtl());
				JSONObject js = (JSONObject) parser.parse(agent);
				json.put("agentDetail", js);
				response.setContentType("application/json");
				response.getWriter().println(json);
			} else {
				json.put("msg", cmeResponseBean.getStatusMsg());
				response.setContentType("application/json");
				response.getWriter().println(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String openMPinValidation() {
		session.put("cmeRequest", cmeBean);
		return "success";
	}

	public String validateMPIN() {
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		User user = (User) session.get("User");
		CMERequestBean cBean = (CMERequestBean) session.get("cmeRequest");
		cmeBean.setMobileNo(user.getMobileno());
		cmeBean.setAggreatorId(user.getAggreatorid());
		cmeBean.setUserId(user.getId());
		
		TreeMap<String,String>  map=new TreeMap<String,String>(); 
		map.put("mobileNo",cmeBean.getMobileNo());
        map.put("aggreatorId",cmeBean.getAggreatorId());
        map.put("mpin",cmeBean.getMpin());
        map.put("userId",cmeBean.getUserId());
        map.put("token",user.getToken());
              
        String checkSumString="";
		try {
			checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmeBean.setCHECKSUMHASH(checkSumString);
		
		CMEResponseBean cmeResponse = cmeService.validateMPIN(cmeBean);
		if (cmeResponse != null && cmeResponse.getStatusCode().equalsIgnoreCase("1000")) {
			CMERequestBean requestBean = new CMERequestBean();
			if (user.getUsertype() == 6) {
				requestBean.setUserId(user.getSubAggregatorId());
			} else {
				requestBean.setUserId(user.getId());
			}
			requestBean.setWalletId(user.getWalletid());
			requestBean.setAgentMobile(cBean.getMobileNo());
			requestBean.setAmount(cBean.getAmount());
			requestBean.setAggreatorId(user.getAggreatorid());
			requestBean.setRemark("Remark");
			
			TreeMap<String,String>  object=new TreeMap<String,String>(); 
			object.put("userId",requestBean.getUserId());
			object.put("walletId",requestBean.getWalletId());
			object.put("aggreatorId", requestBean.getAggreatorId());
			object.put("agentMobile",requestBean.getAgentMobile());
			object.put("amount",""+requestBean.getAmount());
			object.put("remark",requestBean.getRemark());
			object.put("token",user.getToken());
	     
			try {
				checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),object);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			requestBean.setCHECKSUMHASH(checkSumString);

			CMEResponseBean bean = cmeService.moneyTrnsfer(requestBean, ipAddress, userAgent);
			if (bean != null && bean.getStatusCode().equalsIgnoreCase("1000")) {
				addActionMessage(bean.getStatusMsg());
				request.setAttribute("agentDtl", bean.getResPayload().getAgentDtl());
				String walletBal=tService.getWalletBalance(user.getWalletid());
				if(walletBal!=null&&!walletBal.isEmpty()){
					try{
						user.setFinalBalance(Double.parseDouble(walletBal));
						session.put("User",user);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				return "success";
			}
			if (bean != null && bean.getStatusCode().equalsIgnoreCase("0")) {
				addActionError(bean.getStatusMsg());
				return "fail";
			}
			if (bean != null && bean.getStatusCode().equalsIgnoreCase("8001")) {
				addActionError(bean.getStatusMsg());
				return "fail";
			}
			if (bean != null && bean.getStatusCode().equalsIgnoreCase("8002")) {
				addActionError(bean.getStatusMsg());
				return "fail";
			}
			return "success";
		} else {
			addActionError(cmeResponse.getStatusMsg());
			return "fail";
		}

	}

	public String validateTxn() {
		return "success";

	}

	// added for CME checker action
	public String cMERequestListChecker() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		cmeBean.setStatus("PENDING");
		List<CMERequestBean> list = cmeService.getCMECheckerListByAggId(cmeBean);
		request.setAttribute("eList", list);
		return "success";
	}
	
	
	
	
	

	public String acceptCMERequestChecker() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());

		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String result = cmeService.acceptCMERequest(cmeBean, ipAddress, userAgent);
		cmeBean.setStatus("PENDING");
		List<CMERequestBean> list = cmeService.getCMECheckerListByAggId(cmeBean);
		request.setAttribute("eList", list);
		if (result.equalsIgnoreCase("True")) {
			addActionMessage("CME Request Accepted. Request Id:" + cmeBean.getCmeRequestid());
		} else {
			addActionError("Please try again latter.");
		}

		return "success";
	}

	public String rejectCMERequestChecker() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String result = cmeService.rejectCMERequest(cmeBean);
		cmeBean.setStatus("PENDING");
		List<CMERequestBean> list = cmeService.getCMECheckerListByAggId(cmeBean);
		if (result.equalsIgnoreCase("True")) {
			addActionMessage("CME Request Rejected. Request Id:" + cmeBean.getCmeRequestid());
		} else {
			addActionError("Please try again latter.");
		}
		request.setAttribute("eList", list);
		return "success";
	}

	public String cMERequestReport() {
		User user = (User) session.get("User");
		System.out.println("**********************22*********************" + cmeBean.getStDate());
		System.out.println("*************************33******************" + cmeBean.getEndDate());

		cmeBean.setAggreatorId(user.getAggreatorid());
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		cmeBean.setStatus("ALL");
		List<CMERequestBean> list = cmeService.getCMECheckerListByAggId(cmeBean);
		request.setAttribute("eList", list);
		return "success";
	}

	public String fundTransferByDist() {
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		User user = (User) session.get("User");

		System.out.println("*******" + cmeBean.getMobileNo());
		System.out.println("*******" + cmeBean.getAgentId());
		System.out.println("*******" + cmeBean.getAmount());
		CMERequestBean requestBean = new CMERequestBean();

		requestBean.setUserId(user.getId());
		requestBean.setWalletId(user.getWalletid());
		requestBean.setAgentMobile(cmeBean.getMobileNo());
		requestBean.setAmount(cmeBean.getAmount());
		requestBean.setAggreatorId(user.getAggreatorid());
		requestBean.setRemark("Remark");
		
		TreeMap<String,String>  object=new TreeMap<String,String>(); 
		object.put("userId",requestBean.getUserId());
		object.put("walletId",requestBean.getWalletId());
		object.put("aggreatorId", requestBean.getAggreatorId());
		object.put("agentMobile",requestBean.getAgentMobile());
		object.put("amount",""+requestBean.getAmount());
		object.put("remark",requestBean.getRemark());
		object.put("token",user.getToken());
      String checkSumString="";
		try {
			checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		requestBean.setCHECKSUMHASH(checkSumString);
        
		CMEResponseBean bean = cmeService.moneyTrnsfer(requestBean, ipAddress, userAgent);
		JSONObject jObject = new JSONObject();

		if (bean != null && bean.getStatusCode().equalsIgnoreCase("1000")) {
			jObject.put("status", "SUCCESS");
			jObject.put("statusMsg", bean.getStatusMsg());
			String walletBal = new TransactionServiceImpl().getWalletBalanceById(user.getId());
			user.setFinalBalance(Double.parseDouble(walletBal!=null?walletBal:"0.0"));
			session.put("User",user);
			
		}
		if (bean != null && bean.getStatusCode().equalsIgnoreCase("0")) {
			jObject.put("status", "FAILED");
			jObject.put("statusMsg", bean.getStatusMsg());

		}
		if (bean != null && bean.getStatusCode().equalsIgnoreCase("8001")) {
			jObject.put("status", "FAILED");
			jObject.put("statusMsg", bean.getStatusMsg());

		}
		if (bean != null && bean.getStatusCode().equalsIgnoreCase("8002")) {
			jObject.put("status", "FAILED");
			jObject.put("statusMsg", bean.getStatusMsg());

		}
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;

	}
	
	
	
	public String getCMEApproveListByAggId() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		List<CMERequestBean> list = cmeService.getCMEApproveListByAggId(cmeBean);
		request.setAttribute("eList", list);
		return "success";
	}
	
	
	
	public String approveCMERequestChecker() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());

		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String result = cmeService.approveCMERequest(cmeBean, ipAddress, userAgent);
		
		List<CMERequestBean> list = cmeService.getCMEApproveListByAggId(cmeBean);
		request.setAttribute("eList", list);
		if (result.equalsIgnoreCase("True")) {
			addActionMessage("CME Request Accepted. Request Id:" + cmeBean.getCmeRequestid());
		} else {
			addActionError("Please try again latter.");
		}

		return "success";
	}
	
	//disapproverCMERequest
	
	public String disapproverCMERequestChecker() {
		User user = (User) session.get("User");

		cmeBean.setAggreatorId(user.getAggreatorid());

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String result = cmeService.disapproverCMERequestChecker(cmeBean);
		List<CMERequestBean> list = cmeService.getCMEApproveListByAggId(cmeBean);
		if (result.equalsIgnoreCase("True")) {
			addActionMessage("CME Request Rejected. Request Id:" + cmeBean.getCmeRequestid());
		} else {
			addActionError("Please try again latter.");
		}
		request.setAttribute("eList", list);
		return "success";
	}
	
	
	public String getAgnDtlByUserIdSo() {
		try {
			User user = (User) session.get("User");
			 
			cmeBean.setDistributerId(user.getId());
			cmeBean.setTxnRefNo(""+user.getUsertype());
			cmeBean.setAggreatorId(user.getAggreatorid());
			 
			
			CMEResponseBean cmeResponseBean = cmeService.getAgnDtlByUserIdSo(cmeBean);
			JSONObject json = new JSONObject();
			Gson gson = new Gson();
			if (cmeResponseBean != null && cmeResponseBean.getStatusCode().equalsIgnoreCase("1000")) {
				JSONParser parser = new JSONParser();

				String agent = gson.toJson(cmeResponseBean.getResPayload().getAgentDtl());
				JSONObject js = (JSONObject) parser.parse(agent);
				json.put("agentDetail", js);
				response.setContentType("application/json");
				response.getWriter().println(json);
			} else {
				json.put("msg", cmeResponseBean.getStatusMsg());
				response.setContentType("application/json");
				response.getWriter().println(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	

	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response = arg0;
	}

	


	public String addToken()
	{
	 User user=(User)session.get("User");
	 UserService service=new UserServiceImpl();
	 String mob = cmeBean.getMobileNo();
	 String userId = cmeBean.getUserId();
	 
	 System.out.println(mob+"    "+userId);
	 
	 DistributorIdAllot allotId = service.getDistributorAllotid(user.getId(),user.getAggreatorid());
	 allotId.setUserId(userId);
	 request.setAttribute("allotId",allotId);
	  
	 return "success";
	}
	
	
	public String updateTokenId()
	{
	 User user=(User)session.get("User"); 
	 UserService service=new UserServiceImpl();
	 
	 walletBean.setSuperdistributerid(user.getId());
	 walletBean.setDistributerid(walletBean.getName());
	 walletBean.setMobileno(walletBean.getMobileno());
	 walletBean.setUtrNo(walletBean.getUtrNo());
	 
	 System.out.println(walletBean.getName()+"   "+walletBean.getUtrNo());
	 DistributorIdAllot status = service.updateTokenId(walletBean);
	 
	   
	 DistributorIdAllot allotId = service.getDistributorAllotid(user.getId(),user.getAggreatorid());
	 allotId.setAggregatorId(status.getAggregatorId());
	 allotId.setUserId(status.getUserId());
	 
	 request.setAttribute("allotId",allotId);
	  
	 return "success";
	}
	
	
	
}
