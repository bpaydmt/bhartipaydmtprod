package com.bhartipay.wallet.cme.vo;

import java.io.Serializable;
import java.util.List;

public class ResPayload implements Serializable{
	private CMERequestBean cMERequestBean;
	private List<CMERequestBean> cmeRequestList = null;
	private double dmtBalance;
	private CMEAgentDetails agentDtl;
	private List<CMEAccountStatements> cMEAccountStsList=null;
	
	
	public List<CMEAccountStatements> getcMEAccountStsList() {
		return cMEAccountStsList;
	}
	public void setcMEAccountStsList(List<CMEAccountStatements> cMEAccountStsList) {
		this.cMEAccountStsList = cMEAccountStsList;
	}
	public CMEAgentDetails getAgentDtl() {
		return agentDtl;
	}
	public void setAgentDtl(CMEAgentDetails agentDtl) {
		this.agentDtl = agentDtl;
	}

	
	public double getDmtBalance() {
		return dmtBalance;
	}

	public void setDmtBalance(double dmtBalance) {
		this.dmtBalance = dmtBalance;
	}

	public List<CMERequestBean> getCmeRequestList() {
		return cmeRequestList;
	}

	public void setCmeRequestList(List<CMERequestBean> cmeRequestList) {
		this.cmeRequestList = cmeRequestList;
	}

	public CMERequestBean getcMERequestBean() {
		return cMERequestBean;
	}

	public void setcMERequestBean(CMERequestBean cMERequestBean) {
		this.cMERequestBean = cMERequestBean;
	}
	
}
