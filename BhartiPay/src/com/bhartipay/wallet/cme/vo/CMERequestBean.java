package com.bhartipay.wallet.cme.vo;

import java.io.File;
import java.io.Serializable;

import javax.persistence.Column;

public class CMERequestBean implements Serializable{

			private String cmeRequestid;
			 private String aggreatorId;
			 private String userId;
			 private String walletId;
			 private Double amount;
			 private String docType;
			 private String txnRefNo;
			 private String bankName;
			 private String branchName;
			 private String depositeDate;
			 private String remark;
			 private String remarks;
			 private String reciptDoc;
			 private String status;
			 private String reqestdate;
			 private String ipIemi;
			 private String agent;
			 private String stDate;
			 private String endDate;
			 private String mobileNo;
			 private String agentId;
			 private String agentMobile;
			 //added for CME checker
			 private String remarksReason;
			 
			 private String mpin;
			private File myFile1;
			private String myFile1FileName;
			private String myFile1ContentType;
			
			private String distributerId;
			
			
			private String remarkApprover;
			private String checkerStatus;
			private String approverStatus;
			private String CHECKSUMHASH;
			
			 
			public String getRemarks() {
				return remarks;
			}
			public void setRemarks(String remarks) {
				this.remarks = remarks;
			}
			public String getCHECKSUMHASH() {
				return CHECKSUMHASH;
			}
			public void setCHECKSUMHASH(String cHECKSUMHASH) {
				CHECKSUMHASH = cHECKSUMHASH;
			}
			public String getRemarkApprover() {
				return remarkApprover;
			}
			public void setRemarkApprover(String remarkApprover) {
				this.remarkApprover = remarkApprover;
			}
			public String getCheckerStatus() {
				return checkerStatus;
			}
			public void setCheckerStatus(String checkerStatus) {
				this.checkerStatus = checkerStatus;
			}
			public String getApproverStatus() {
				return approverStatus;
			}
			public void setApproverStatus(String approverStatus) {
				this.approverStatus = approverStatus;
			}
			public String getDistributerId() {
				return distributerId;
			}
			public void setDistributerId(String distributerId) {
				this.distributerId = distributerId;
			}
			public String getAgentMobile() {
				return agentMobile;
			}
			public void setAgentMobile(String agentMobile) {
				this.agentMobile = agentMobile;
			}
			public String getMpin() {
				return mpin;
			}
			public void setMpin(String mpin) {
				this.mpin = mpin;
			}
			public String getAgentId() {
				return agentId;
			}
			public void setAgentId(String agentId) {
				this.agentId = agentId;
			}
			public String getMobileNo() {
				return mobileNo;
			}
			public void setMobileNo(String mobileNo) {
				this.mobileNo = mobileNo;
			}
			public String getStDate() {
				return stDate;
			}
			public void setStDate(String stDate) {
				this.stDate = stDate;
			}
			public String getEndDate() {
				return endDate;
			}
			public void setEndDate(String endDate) {
				this.endDate = endDate;
			}
			public File getMyFile1() {
					return myFile1;
				}
				public void setMyFile1(File myFile1) {
					this.myFile1 = myFile1;
				}
				public String getMyFile1FileName() {
					return myFile1FileName;
				}
				public void setMyFile1FileName(String myFile1FileName) {
					this.myFile1FileName = myFile1FileName;
				}
				public String getMyFile1ContentType() {
					return myFile1ContentType;
				}
				public void setMyFile1ContentType(String myFile1ContentType) {
					this.myFile1ContentType = myFile1ContentType;
				}
			public String getBranchName() {
				return branchName;
			}
			public void setBranchName(String branchName) {
				this.branchName = branchName;
			}
			public String getCmeRequestid() {
				return cmeRequestid;
			}
			public void setCmeRequestid(String cmeRequestid) {
				this.cmeRequestid = cmeRequestid;
			}
			public String getAggreatorId() {
				return aggreatorId;
			}
			public void setAggreatorId(String aggreatorId) {
				this.aggreatorId = aggreatorId;
			}
			public String getUserId() {
				return userId;
			}
			public void setUserId(String userId) {
				this.userId = userId;
			}
			public String getWalletId() {
				return walletId;
			}
			public void setWalletId(String walletId) {
				this.walletId = walletId;
			}
			public Double getAmount() {
				return amount;
			}
			public void setAmount(Double amount) {
				this.amount = amount;
			}
			public String getDocType() {
				return docType;
			}
			public void setDocType(String docType) {
				this.docType = docType;
			}
			public String getTxnRefNo() {
				return txnRefNo;
			}
			public void setTxnRefNo(String txnRefNo) {
				this.txnRefNo = txnRefNo;
			}
			public String getBankName() {
				return bankName;
			}
			public void setBankName(String bankName) {
				this.bankName = bankName;
			}
			public String getDepositeDate() {
				return depositeDate;
			}
			public void setDepositeDate(String depositeDate) {
				this.depositeDate = depositeDate;
			}
			
			public String getRemark() {
				return remark;
			}
			public void setRemark(String remark) {
				this.remark = remark;
			}
			public String getReciptDoc() {
				return reciptDoc;
			}
			public void setReciptDoc(String reciptDoc) {
				this.reciptDoc = reciptDoc;
			}
			public String getStatus() {
				return status;
			}
			public void setStatus(String status) {
				this.status = status;
			}
			public String getReqestdate() {
				return reqestdate;
			}
			public void setReqestdate(String reqestdate) {
				this.reqestdate = reqestdate;
			}
			public String getIpIemi() {
				return ipIemi;
			}
			public void setIpIemi(String ipIemi) {
				this.ipIemi = ipIemi;
			}
			public String getAgent() {
				return agent;
			}
			public void setAgent(String agent) {
				this.agent = agent;
			}
			public String getRemarksReason() {
				return remarksReason;
			}
			public void setRemarksReason(String remarksReason) {
				this.remarksReason = remarksReason;
			}
		
			 
			 
}
