package com.bhartipay.wallet.cme.vo;

public class CMEAccountStatements {
	private String txndate;
	 private String txnid;
	
	 private String payeedtl;
	 private String name;
	 private String closingbal;
	 private String txndesc;
	 private double txncredit;
	 private double txndebit;
	 
	 
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public double getTxncredit() {
		return txncredit;
	}
	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}
	public double getTxndebit() {
		return txndebit;
	}
	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}
	public String getPayeedtl() {
		return payeedtl;
	}
	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}
	public String getClosingbal() {
		return closingbal;
	}
	public void setClosingbal(String closingbal) {
		this.closingbal = closingbal;
	}
	public String getTxndesc() {
		return txndesc;
	}
	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}
	 
	 
}
