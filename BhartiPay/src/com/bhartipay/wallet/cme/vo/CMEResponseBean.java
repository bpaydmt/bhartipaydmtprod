package com.bhartipay.wallet.cme.vo;
import java.io.Serializable;
import java.util.List;
public class CMEResponseBean implements Serializable{

	private String statusCode;
	private List<Payload> payload = null;
	private String statusMsg;
	private Integer count;
	private ResPayload resPayload;
	private CMEAgentDetails agentDtl;
	
	
	
	public CMEAgentDetails getAgentDtl() {
		return agentDtl;
	}
	public void setAgentDtl(CMEAgentDetails agentDtl) {
		this.agentDtl = agentDtl;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public ResPayload getResPayload() {
		return resPayload;
	}
	public void setResPayload(ResPayload resPayload) {
		this.resPayload = resPayload;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<Payload> getPayload() {
		return payload;
	}
	public void setPayload(List<Payload> payload) {
		this.payload = payload;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
 
}
