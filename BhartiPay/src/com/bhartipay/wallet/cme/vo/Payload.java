package com.bhartipay.wallet.cme.vo;

import java.io.Serializable;

public class Payload implements Serializable{
	private String accountNumber;
	private String bankName;
	private String depositeType;
	private String depositeId;
	
	
	public String getDepositeType() {
		return depositeType;
	}
	public void setDepositeType(String depositeType) {
		this.depositeType = depositeType;
	}
	public String getDepositeId() {
		return depositeId;
	}
	public void setDepositeId(String depositeId) {
		this.depositeId = depositeId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
}
