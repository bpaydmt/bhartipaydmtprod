package com.bhartipay.wallet.cme.service;

import java.util.List;
import java.util.Map;

import com.bhartipay.wallet.cme.vo.CMEAgentDetails;
import com.bhartipay.wallet.cme.vo.CMERequestBean;
import com.bhartipay.wallet.cme.vo.CMEResponseBean;

public interface CMEService {

	/**
	 * 
	 * @return
	 */
	public Map<String,String> getBankDtl(String userId);
	/**
	 * 
	 * @return
	 */
	public Map<String, String> getDeposittype();
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public CMEResponseBean saveCMERequest(CMERequestBean bean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public CMEResponseBean getCMERequestList(CMERequestBean bean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public CMEResponseBean getDMTBalance(CMERequestBean bean);
	
	
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean getAgnDtlByUserId(CMERequestBean requestBean);
	public CMEResponseBean getAgnDtlByUserIdSo(CMERequestBean requestBean);
	
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean getDistributorDtlByUserId(CMERequestBean requestBean);
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean validateMPIN(CMERequestBean requestBean);
	
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean moneyTrnsfer(CMERequestBean requestBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean getCMEAccountStsList(CMERequestBean requestBean);
	
	/**
	 * 
	 * @param requestBean
	 * @return
	 */
	public CMEResponseBean movementReportBySupDist(CMERequestBean requestBean);
	//added for CME checker 
	 /**
	  * 
	  * @param CMERequestBean
	  * @return
	  */
	 public List<CMERequestBean> getCMECheckerListByAggId(CMERequestBean requestBean);
	 /**
	  * 
	  * @param cashDepositMast
	  * @return
	  */
	 public String acceptCMERequest(CMERequestBean cmeBean,String ipimei,String userAgent);
	 /**
	  * 
	  * @param depositMast
	  * @return
	  */
	 public String rejectCMERequest(CMERequestBean cmeBean);
	
	 
	 /**
	  * 
	  * @param requestBean
	  * @return
	  */
	 public List<CMERequestBean> getCMEApproveListByAggId(CMERequestBean requestBean);
	 
	 /**
	  * 
	  * @param cmeBean
	  * @param ipimei
	  * @param userAgent
	  * @return
	  */
	 public String approveCMERequest(CMERequestBean cmeBean,String ipimei,String userAgent);
	 
	 
	 /**
	  * 
	  * @param cmeBean
	  * @return
	  */
	 public String disapproverCMERequestChecker(CMERequestBean cmeBean);
	
	
}
