package com.bhartipay.wallet.cme.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.wallet.cme.service.CMEService;
import com.bhartipay.wallet.cme.vo.CMEAgentDetails;
import com.bhartipay.wallet.cme.vo.CMERequestBean;
import com.bhartipay.wallet.cme.vo.CMEResponseBean;
import com.bhartipay.wallet.cme.vo.Payload;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CMEServiceImpl implements CMEService {
	public static Logger logger = Logger.getLogger(CMEServiceImpl.class);

	@Override
	public Map<String, String> getBankDtl(String userId) {
		logger.debug("*************************calling method getBankDtl*********************************");
		Gson gson = new Gson();
		// String jsonText=gson.toJson(merchantSummary);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getBankDtl");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("USERID",userId).post(ClientResponse.class);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean cmeResponseBean = gson.fromJson(output, CMEResponseBean.class);
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Payload> payloadList = cmeResponseBean.getPayload();
		for (Payload pay : payloadList) {
			map.put(pay.getBankName() + " - " + pay.getAccountNumber() + "",
					pay.getBankName() + " - " + pay.getAccountNumber() + "");
		}

		return map;    
	}

	@Override
	public Map<String, String> getDeposittype() {
		logger.debug("*************************calling method getDeposittype *********************************");
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getDeposittype");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		logger.debug("*************************getting response from service*********************************");
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);
		String output = response.getEntity(String.class);
		logger.debug("output result is**********" + output);
		CMEResponseBean cmeResponseBean = gson.fromJson(output, CMEResponseBean.class);
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Payload> payloadList = cmeResponseBean.getPayload();
		for (Payload pay : payloadList) {
			map.put(pay.getDepositeId(),pay.getDepositeType());
		}

		return map;
	}

	@Override
	public CMEResponseBean saveCMERequest(CMERequestBean bean,String ipimei,String userAgent) {
		logger.debug("*************************calling method getBankDtl*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		logger.debug("*************************ipimei*********************************"+ipimei);
		logger.debug("*************************userAgent*********************************"+userAgent);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/saveCMERequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean getCMERequestList(CMERequestBean bean, String ipimei, String userAgent) {
		logger.debug("*************************calling method getCMERequestList*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getCMERequestList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean getDMTBalance(CMERequestBean bean) {
		logger.debug("*************************calling method getDMTBalance*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getDMTBalance");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean getAgnDtlByUserId(CMERequestBean requestBean) {
		logger.debug("*************************calling method getAgnDtlByUserId*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getAgnDtlByUserId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}
	
	
	
	@Override
	public CMEResponseBean getDistributorDtlByUserId(CMERequestBean requestBean) {
		logger.debug("*************************calling method getAgnDtlByUserId*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getDistributorDtlByUserId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean validateMPIN(CMERequestBean requestBean) {
		logger.debug("*************************calling method validateMPIN*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/validateMPIN");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean moneyTrnsfer(CMERequestBean requestBean,String ipimei,String userAgent) {
		logger.debug("*************************calling method moneyTrnsfer*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/moneyTrnsfer");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}

	@Override
	public CMEResponseBean getCMEAccountStsList(CMERequestBean requestBean) {
		logger.debug("*************************calling method getCMEAccountStsList*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getCMEAccountStsList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}
	
	@Override
	public CMEResponseBean movementReportBySupDist(CMERequestBean requestBean) {
		logger.debug("*************************calling method getCMEAccountStsList*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(requestBean);
		WebResource webResource = ServiceManager.getWebResource("CMEManager/getMovementReportBySupDist");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");

		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);

		String output = response.getEntity(String.class);
		CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
		logger.debug("output result is**********" + output);
		return resBean;
	}
	
	
	//added for CME Checker
	 public List<CMERequestBean> getCMECheckerListByAggId(CMERequestBean requestBean)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(requestBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/getCMECheckerListByAggId");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  List<CMERequestBean> parameter2 = (List<CMERequestBean>) gson.fromJson(output, JsonToArrayList.class);
	  return parameter2;
	 }
	 
	 public String acceptCMERequest(CMERequestBean cmeBean,String ipimei,String userAgent)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(cmeBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/acceptCMERequest");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  return output;
	 }
	 
	 public String rejectCMERequest(CMERequestBean cmeBean)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(cmeBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/rejectCMERequest");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  return output;
	 }
	
	
	 
	 public List<CMERequestBean> getCMEApproveListByAggId(CMERequestBean requestBean)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(requestBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/getCMEApproveListByAggId");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  List<CMERequestBean> parameter2 = (List<CMERequestBean>) gson.fromJson(output, JsonToArrayList.class);
	  return parameter2;
	 }
	 
	
	 
	 public String approveCMERequest(CMERequestBean cmeBean,String ipimei,String userAgent)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(cmeBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/approveCMERequest");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  return output;
	 }
	
	 
	 
	 public String disapproverCMERequestChecker(CMERequestBean cmeBean)
	 {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(cmeBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("CMEManager/disapproverCMERequest");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  return output;
	 }
	
	 
	 @Override
		public CMEResponseBean getAgnDtlByUserIdSo(CMERequestBean requestBean) {
			logger.debug("*************************calling method getAgnDtlByUserId*********************************");
			Gson gson = new Gson();
			String jsonText=gson.toJson(requestBean);
			WebResource webResource = ServiceManager.getWebResource("CMEManager/getAgnDtlByUserIdSo");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
			logger.debug("*************************getting response from service*********************************");

			if (response.getStatus() != 200) {
				logger.debug("*************************getting response from service::" + response.getStatus()
						+ "*********************************");
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			logger.debug("getting response from service*********************************" + response);

			String output = response.getEntity(String.class);
			CMEResponseBean resBean = gson.fromJson(output, CMEResponseBean.class);
			logger.debug("output result is**********" + output);
			return resBean;
		}
	 
	 
}


class JsonToArrayList extends ArrayList<CMERequestBean>{

}
