package com.bhartipay.wallet.matm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class MatmResponseData {

	private String refno;
	
	private String payerId;
	
	private String payertype;

	private String payeeId;
	
	private String payeetype;
	
	private String txnType;
	
	private String orderId;
	
	private int amount;
	
	private String txnId;
	
	private String balance;
	
	private String orderStatus;
	
	private String paymentStatus;
	
	private String requestId;
	
	private String stan;
	
	private String rrn;
	
	private String bankAuth;	
	
	private String processingCode;
	
	private String accountBalance;
	
	private String bankResponseCode;
	
	private String bankResponseMsg;
	
	private String terminalId;
	
	private String agentId;
	
	private String aadharNumber;
	
	
	private String dateTime;
	
	private String statusCode;
	
	private String statusMessage;
	
	private Double commissionAmt;
	
	private Double gstAmt;
	
	private Double tdsAmt;
	
	private String walletMessage;
	
	private Boolean isWalletFailed;
	
	private String bcname;
	
	private String bcaddress;
	
	private String name;
	
	private String redirectionUrl;
	
	private Timestamp txnDate;
	
	private String cardNumber;
	
	
	
	
	
	public Timestamp getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}

	public String getRefno() {
		return refno;
	}

	public String getName() {
		return name;
	}

	

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	
	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	
	

	public String getBankResponseCode() {
		return bankResponseCode;
	}

	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}

	public String getBankResponseMsg() {
		return bankResponseMsg;
	}

	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}
	
	public Boolean getIsWalletFailed() {
		return isWalletFailed;
	}

	public void setIsWalletFailed(Boolean isWalletFailed) {
		this.isWalletFailed = isWalletFailed;
	}

	
	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return refno+"";
	}

	public String getPayertype() {
		return payertype;
	}

	public void setPayertype(String payertype) {
		this.payertype = payertype;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getPayeetype() {
		return payeetype;
	}

	public void setPayeetype(String payeetype) {
		this.payeetype = payeetype;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getBankAuth() {
		return bankAuth;
	}

	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public Double getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(Double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public Double getGstAmt() {
		return gstAmt;
	}

	public void setGstAmt(Double gstAmt) {
		this.gstAmt = gstAmt;
	}

	public Double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(Double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	public String getWalletMessage() {
		return walletMessage;
	}

	public void setWalletMessage(String walletMessage) {
		this.walletMessage = walletMessage;
	}

	public String getBcname() {
		return bcname;
	}

	public void setBcname(String bcname) {
		this.bcname = bcname;
	}

	public String getBcaddress() {
		return bcaddress;
	}

	public void setBcaddress(String bcaddress) {
		this.bcaddress = bcaddress;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	
}
