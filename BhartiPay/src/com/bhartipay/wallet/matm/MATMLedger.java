package com.bhartipay.wallet.matm;

import java.sql.Timestamp;

import javax.persistence.Transient;

import com.bhartipay.wallet.aeps.AEPSConfig;

public class MATMLedger {

	private String mid;
	private String agentCode;
	private String metaData;
	private String txnId;
	private String ptyTransDt;

	private Timestamp responseTime;

	private double amount;
	private String status;
	private String statusCode;
	private String aggregator;
	private String agentId;
	private String walletId;
	private String userAgnet;
	private String ipimei;
	private String txnType;
	private String orderId;
	private String orderStatus;
	private String paymentStatus;
	private String requestId;
	private String rrn;
	private String processingCode;
	private String bankResponseCode;
	private String bankResponseMsg;
	private String statusMessage;
	private Timestamp txnDate;
	private String token;
	private String date;
	// Extra Feilds
	private String accountBalance;
	private String terminalId;
	private String cardNumber;

	// private String aepsChannel;

	AEPSConfig aepsConfig;
	private String type;

	private String aepsChannel;
	private String responseHash;
	private String redirectionUrl;
	private String stan;
	private String bankAuth;
	private String aadharNumber;

	private double commissionAmt;

	
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public String getPtyTransDt() {
		return ptyTransDt;
	}

	public void setPtyTransDt(String ptyTransDt) {
		this.ptyTransDt = ptyTransDt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRrn() {
		return rrn;
	}

	public Timestamp getTxnDate() {
		return txnDate;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public void setTxnDate(Timestamp txnDate) {
		this.txnDate = txnDate;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public AEPSConfig getAepsConfig() {
		return aepsConfig;
	}

	public void setAepsConfig(AEPSConfig aepsConfig) {
		this.aepsConfig = aepsConfig;
	}

	public String getMid() {
		return mid;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getTxnId() {
		return txnId;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public double getAmount() {
		return amount;
	}

	public String getStatus() {
		return status;
	}

	public String getAggregator() {
		return aggregator;
	}

	public String getAgentId() {
		return agentId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserAgnet() {
		return userAgnet;
	}

	public String getIpimei() {
		return ipimei;
	}

	public void setUserAgnet(String userAgnet) {
		this.userAgnet = userAgnet;
	}

	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getBankResponseCode() {
		return bankResponseCode;
	}

	public void setBankResponseCode(String bankResponseCode) {
		this.bankResponseCode = bankResponseCode;
	}

	public String getBankResponseMsg() {
		return bankResponseMsg;
	}

	public void setBankResponseMsg(String bankResponseMsg) {
		this.bankResponseMsg = bankResponseMsg;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getAepsChannel() {
		return aepsChannel;
	}

	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}

	public String getResponseHash() {
		return responseHash;
	}

	public void setResponseHash(String responseHash) {
		this.responseHash = responseHash;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getBankAuth() {
		return bankAuth;
	}

	public void setBankAuth(String bankAuth) {
		this.bankAuth = bankAuth;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

}
