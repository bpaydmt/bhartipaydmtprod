package com.bhartipay.wallet.matm;

public class FinoAepsResponse {
	
	private String ClientRefID;
	private String DispalyMessage;
	private String ResponseCode;
	private String ClientRes;
	
	public String getClientRefID() {
		return ClientRefID;
	}
	public void setClientRefID(String clientRefID) {
		ClientRefID = clientRefID;
	}
	public String getDispalyMessage() {
		return DispalyMessage;
	}
	public void setDispalyMessage(String dispalyMessage) {
		DispalyMessage = dispalyMessage;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getClientRes() {
		return ClientRes;
	}
	public void setClientRes(String clientRes) {
		ClientRes = clientRes;
	}

}
