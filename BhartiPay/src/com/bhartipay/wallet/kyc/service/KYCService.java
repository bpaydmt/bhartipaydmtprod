package com.bhartipay.wallet.kyc.service;

import com.bhartipay.wallet.kyc.bean.EkycResponseBean;
import com.bhartipay.wallet.kyc.bean.KycConfigBean;
import com.bhartipay.wallet.kyc.bean.KycXmlResponse;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.SenderKYCBean;

public interface KYCService {
	
	/**
	 * 
	 * @return
	 */
	
	public void setSendereKYCResponse(EkycResponseBean ekycResponseBean,String ipAddress,String userAgent);
	public EkycResponseBean getEKycSenderData(EkycResponseBean bean);
	public KycConfigBean getSendereKYCRequest(SenderKYCBean mastBean, String ipAddress, String userAgent);
	public EkycResponseBean getResponseXml(KycXmlResponse bean);

}
