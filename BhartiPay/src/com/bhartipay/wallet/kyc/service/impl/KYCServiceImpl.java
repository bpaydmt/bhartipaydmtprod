package com.bhartipay.wallet.kyc.service.impl;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.bean.EkycResponseBean;
import com.bhartipay.wallet.kyc.bean.KycConfigBean;
import com.bhartipay.wallet.kyc.bean.KycXmlResponse;
import com.bhartipay.wallet.kyc.service.KYCService;
import com.bhartipay.wallet.report.service.impl.ReportServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.SenderKYCBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class KYCServiceImpl implements KYCService{
	public static Logger logger=Logger.getLogger(KYCServiceImpl.class);

	public KycConfigBean getSendereKYCRequest(SenderKYCBean mastBean, String ipAddress, String userAgent){
		Gson gson = new Gson();
		logger.debug("*****************calling method  getSendereKYCRequest ***********************");
		String jsonText=gson.toJson(mastBean);
		KycConfigBean kycconfigbean=new KycConfigBean();
		WebResource webResource=ServiceManager.getWebResource("MudraManager/sendereKYC");
		logger.debug("*****************response come from service***********************");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipAddress).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		logger.debug("*****************response come from service***********************"+response.getStatus());
		
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		kycconfigbean = gson.fromJson(output, KycConfigBean.class);
		
		logger.debug("*****************return html configuration data for redirecting ***********************"+kycconfigbean.authxml);
		return kycconfigbean;
		
	}
	
	public void setSendereKYCResponse(EkycResponseBean ekycResponseBean,String ipAddress,String userAgent){
		Gson gson = new Gson();
		String jsonText=gson.toJson(ekycResponseBean);
		EkycResponseBean ekycresponse=new EkycResponseBean();
		WebResource webResource=ServiceManager.getWebResource("MudraManager/eKYCResponse");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipAddress).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		ekycresponse = gson.fromJson(output, EkycResponseBean.class);
		
		
	}

	@Override
	public EkycResponseBean getEKycSenderData(EkycResponseBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		EkycResponseBean ekycresponse=new EkycResponseBean();
		WebResource webResource=ServiceManager.getWebResource("MudraManager/getEKycSenderData");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		ekycresponse = gson.fromJson(output, EkycResponseBean.class);
		return ekycresponse;
	}
	
	@Override
	public EkycResponseBean getResponseXml(KycXmlResponse bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		EkycResponseBean ekycresponse=new EkycResponseBean();
		WebResource webResource=ServiceManager.getWebResource("MudraManager/getResponseXml");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		ekycresponse = gson.fromJson(output, EkycResponseBean.class);
		return ekycresponse;
	}
	
}
