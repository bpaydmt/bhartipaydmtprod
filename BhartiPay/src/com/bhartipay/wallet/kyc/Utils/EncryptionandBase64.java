package com.bhartipay.wallet.kyc.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;

public class EncryptionandBase64 {

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");
	String time = dateFormat.format(new Date());

	// params from encodedFinalString
	public String getLogic(String slk, String deviceId, String ServiceType, String rrn, String backupurl,
			String service, String stack) {

		String encodedFinalString = "";
		byte[] finaldata = null;
		StringBuffer xmlStr = null;

		try {
			xmlStr = new StringBuffer();

			if (service.equalsIgnoreCase("auth")) {
				xmlStr.append("<AUTHXML>").append("<SLK>").append(slk).append("</SLK>").append("<SERVICETYPE>")
						.append(ServiceType).append("</SERVICETYPE>").append("<RRN>").append(rrn).append("</RRN>")
						.append("<DEVICEID>").append(deviceId).append("</DEVICEID>").append("<STACK>").append("01")
						.append("</STACK>").append("<REDIRECTURL>").append(backupurl).append("</REDIRECTURL>")
						.append("<RFKEY>").append("").append("</RFKEY>").append("<UUID>").append("").append("</UUID>")
						.append("</AUTHXML>");
			} else if (service.equalsIgnoreCase("kyc")) {
				xmlStr.append("<KYCXML>").append("<SLK>").append(slk).append("</SLK>").append("<SERVICETYPE>")
						.append(ServiceType).append("</SERVICETYPE>").append("<RRN>").append(rrn).append("</RRN>")
						.append("<DEVICEID>").append(deviceId).append("</DEVICEID>").append("<STACK>").append("01")
						.append("</STACK>").append("<REDIRECTURL>").append(backupurl).append("</REDIRECTURL>")
						.append("<RFKEY>").append("").append("</RFKEY>").append("<UUID>").append("").append("</UUID>")
						.append("</KYCXML>");
			}

			finaldata = xmlStr.toString().getBytes();
		} catch (Exception ext) {
			ext.printStackTrace();
		}
		encodedFinalString = new String(Base64.encodeBase64(finaldata));
		return encodedFinalString;
	}

}