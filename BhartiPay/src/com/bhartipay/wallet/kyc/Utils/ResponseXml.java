package com.bhartipay.wallet.kyc.Utils;

	import javax.xml.bind.annotation.XmlAccessType;
	import javax.xml.bind.annotation.XmlAccessorType;
	import javax.xml.bind.annotation.XmlElement;
	import javax.xml.bind.annotation.XmlRootElement;
	import javax.xml.bind.annotation.XmlType;


	/**
	 * <p>Java class for anonymous complex type.
	 * 
	 * <p>The following schema fragment specifies the expected content contained within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="SERVICETYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="UUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="RRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ERRCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ERRMSG" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="DKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="RESPONSE" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="RFKEY" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {
	    "servicetype",
	    "uuid",
	    "rrn",
	    "status",
	    "errcode",
	    "errmsg",
	    "dkey",
	    "response",
	    "rfkey"
	})
	@XmlRootElement(name = "RESPONSEXML")
	public class ResponseXml {

	    @XmlElement(name = "SERVICETYPE", required = true)
	    protected String servicetype;
	    @XmlElement(name = "UUID", required = true)
	    protected String uuid;
	    @XmlElement(name = "RRN", required = true)
	    protected String rrn;
	    @XmlElement(name = "STATUS", required = true)
	    protected String status;
	    @XmlElement(name = "ERRCODE", required = true)
	    protected String errcode;
	    @XmlElement(name = "ERRMSG", required = true)
	    protected String errmsg;
	    @XmlElement(name = "DKEY", required = true)
	    protected String dkey;
	    @XmlElement(name = "RESPONSE", required = true)
	    protected String response;
	    @XmlElement(name = "RFKEY", required = true)
	    protected String rfkey;

	    /**
	     * Gets the value of the servicetype property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getSERVICETYPE() {
	        return servicetype;
	    }

	    /**
	     * Sets the value of the servicetype property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setSERVICETYPE(String value) {
	        this.servicetype = value;
	    }

	    /**
	     * Gets the value of the uuid property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getUUID() {
	        return uuid;
	    }

	    /**
	     * Sets the value of the uuid property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setUUID(String value) {
	        this.uuid = value;
	    }

	    /**
	     * Gets the value of the rrn property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getRRN() {
	        return rrn;
	    }

	    /**
	     * Sets the value of the rrn property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setRRN(String value) {
	        this.rrn = value;
	    }

	    /**
	     * Gets the value of the status property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getSTATUS() {
	        return status;
	    }

	    /**
	     * Sets the value of the status property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setSTATUS(String value) {
	        this.status = value;
	    }

	    /**
	     * Gets the value of the errcode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getERRCODE() {
	        return errcode;
	    }

	    /**
	     * Sets the value of the errcode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setERRCODE(String value) {
	        this.errcode = value;
	    }

	    /**
	     * Gets the value of the errmsg property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getERRMSG() {
	        return errmsg;
	    }

	    /**
	     * Sets the value of the errmsg property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setERRMSG(String value) {
	        this.errmsg = value;
	    }

	    /**
	     * Gets the value of the dkey property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getDKEY() {
	        return dkey;
	    }

	    /**
	     * Sets the value of the dkey property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setDKEY(String value) {
	        this.dkey = value;
	    }

	    /**
	     * Gets the value of the response property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getRESPONSE() {
	        return response;
	    }

	    /**
	     * Sets the value of the response property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setRESPONSE(String value) {
	        this.response = value;
	    }

	    /**
	     * Gets the value of the rfkey property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getRFKEY() {
	        return rfkey;
	    }

	    /**
	     * Sets the value of the rfkey property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setRFKEY(String value) {
	        this.rfkey = value;
	    }

	}

