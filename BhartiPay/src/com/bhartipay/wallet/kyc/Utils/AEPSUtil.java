package com.bhartipay.wallet.kyc.Utils;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import com.sun.jersey.core.util.Base64;

public class AEPSUtil {
	
	public static String calculateHmac(String payload, String secret){
		SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes(),"HmacSHA256");
		Mac mac = null;
		try {
		mac = Mac.getInstance("HmacSHA256");
		mac.init(keySpec);
		byte[] result = mac.doFinal(payload.getBytes());
		return new String(Base64.encode(result));
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
		throw new RuntimeException("Exception hashing payload", e);
		}
		}
	
	
	 public static String getMD5EncryptedValue(String password) {
	        final byte[] defaultBytes = password.getBytes();
	        try {
	            final MessageDigest md5MsgDigest = MessageDigest.getInstance("MD5");
	            md5MsgDigest.reset();
	            md5MsgDigest.update(defaultBytes);
	            final byte messageDigest[] = md5MsgDigest.digest();
	            final StringBuffer hexString = new StringBuffer();
	            for (final byte element : messageDigest) {
	                final String hex = Integer.toHexString(0xFF & element);
	                if (hex.length() == 1) {
	                    hexString.append('0');
	                }
	                hexString.append(hex);
	            }
	            password = hexString + "";
	        } catch (final NoSuchAlgorithmException nsae) {
	            nsae.printStackTrace();
	        }
	        return password;
	    }

	 public static void main(String[] args) {
		
		 System.out.println(getMD5EncryptedValue("ambuj singh"));
		 System.out.println(getMD5EncryptedValue("ambuj singh"));
	}
}
