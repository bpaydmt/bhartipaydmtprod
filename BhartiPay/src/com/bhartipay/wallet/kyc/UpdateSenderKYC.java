package com.bhartipay.wallet.kyc;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts2.interceptor.ServletResponseAware;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.kyc.Utils.Constants;
import com.bhartipay.wallet.kyc.Utils.DecryptionBase64;
import com.bhartipay.wallet.kyc.Utils.KYCRES;
import com.bhartipay.wallet.kyc.Utils.KYCUtil;
import com.bhartipay.wallet.kyc.Utils.ResponseXml;
import com.bhartipay.wallet.kyc.bean.EkycResponseBean;
import com.bhartipay.wallet.kyc.bean.KycConfigBean;
import com.bhartipay.wallet.kyc.bean.KycXmlResponse;
import com.bhartipay.wallet.kyc.service.KYCService;
import com.bhartipay.wallet.kyc.service.impl.KYCServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.SenderKYCBean;
import com.bhartipay.wallet.user.persistence.vo.User;

import appnit.com.crypto.CheckSumHelper;

public class UpdateSenderKYC  extends BaseAction implements ServletResponseAware{

	private static final long serialVersionUID = -203193667575448996L;
	SenderKYCBean mastBean=new SenderKYCBean();
	EkycResponseBean kycbean=new EkycResponseBean();
	KYCRES result = new KYCRES();	
	KycConfigBean kycconfigbean=new KycConfigBean();
	HttpServletResponse response=null;
	private KYCService service=new KYCServiceImpl();
	public static String ekycuid = "";	
	
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		
		
	}
	public String getSenderKycData(){
		
		try{
			
			kycbean.setKyctxnid(request.getParameter("kyctxnid"));
			kycbean=service.getEKycSenderData(kycbean);
			request.getSession().setAttribute("kycbean", kycbean);
		}catch (Exception e) {
			System.out.println(e);
			// TODO: handle exception
		}
		return SUCCESS;
	}
	
	public String updateSenderKYC(){
		System.out.println("***********************************************updatesenderkyc*********************************");
		ServletContext context=request.getServletContext();
		String serverName=context.getInitParameter("serverName");
		
		logger.debug(serverName+"************************************updateSenderKYC() action starting");
		String userAgent=request.getHeader("User-Agent");
		
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
			   
		}
		if (mastBean != null && mastBean.getDeviceId() != null) {
			String type = mastBean.getDeviceId();
			if (type.equalsIgnoreCase("Mantra")) {
				mastBean.setDeviceId("Mantra(MFS100)");
			} else if (type.equalsIgnoreCase("Startek")) {
				mastBean.setDeviceId("Startek(FM220)");
			} else if (type.equalsIgnoreCase("Morpho")) {
				mastBean.setDeviceId("Morpho(1300 E3)");
			} else if (type.equalsIgnoreCase("Secugen")) {
				mastBean.setDeviceId("Secugen(HU20)");
			} else if (type.equalsIgnoreCase("Precision")) {
				mastBean.setDeviceId("Precision (PB510)");
			} else if (type.equalsIgnoreCase("IriShield")) {
				mastBean.setDeviceId("IriShield (MK212OU)");
			} else if (type.equalsIgnoreCase("Cogent")){
				mastBean.setDeviceId("Cogent (3M(CSD200i)");
			}
		} else {
			mastBean.setDeviceId("Mantra(MFS100)");
		}
		mastBean.setServicetype("05");
		logger.debug(serverName+"************************************Request IP"+ipAddress);
		
		User us=(User)session.get("User");
		mastBean.setAgentid(us.getId());
		
		logger.debug(serverName+"************************************User ID"+us.getId());
		
		
		System.out.println(mastBean.getAgentid());
		mastBean.setAggreatorid(us.getAggreatorid());
		
		logger.debug(serverName+"************************************User ID"+us.getAggreatorid());
		
		logger.debug(serverName+"******************updateSenderKYC() service start***********************");
		
		 TreeMap<String,String> obj = new TreeMap<String,String>();
		 	   obj.put("userId", mastBean.getAgentid());
		 	   obj.put("agentid", mastBean.getAgentid());
		 	   obj.put("aggreatorid",mastBean.getAggreatorid());
		       obj.put("servicetype",mastBean.getServicetype());
		       obj.put("mobileno",mastBean.getMobileno());
		       obj.put("token",us.getToken());
         String checkSum;
		try {
			checkSum = CheckSumHelper.getCheckSumHelper().genrateCheckSum(us.getTokenSecurityKey(), obj);
			 mastBean.setCHECKSUMHASH(checkSum);
			 mastBean.setRequestFrom("WEB");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

       
		
		kycconfigbean=service.getSendereKYCRequest(mastBean,ipAddress,userAgent);
	    
		request.getSession().setAttribute("kycconfigbean",kycconfigbean);
	
		return SUCCESS;
	}
	public KycConfigBean getKycconfigbean() {
		return kycconfigbean;
	}
	public void setKycconfigbean(KycConfigBean kycconfigbean) {
		this.kycconfigbean = kycconfigbean;
	}
	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	
	
	
	public String kycresponse(){

	String kycInfo=request.getParameter("KycInfo");
	String aadhaarTxnId=request.getParameter("AadhaarTxnId");
	String initVector=request.getParameter("Iv");
	String uuid=request.getParameter("Uuid");
	String status=request.getParameter("Status");
	String transType=request.getParameter("TransType");
	String aadhaarTs=request.getParameter("AadhaarTs");
	String txnId=request.getParameter("TxnId");
	
	KYCUtil kycUtil=new KYCUtil();
	
	
	String plainText=kycUtil.decrypt("cc3418fea553d9b0201cde8a207176a2", initVector, kycInfo);
	try{
	DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
	DocumentBuilder builder = factory.newDocumentBuilder();
	ByteArrayInputStream input = new ByteArrayInputStream(plainText.toString().getBytes("UTF-8"));
    Document doc = builder.parse(input);
    doc.getDocumentElement().normalize();
   Node node= doc.getFirstChild();
   
NamedNodeMap KycRes=node.getAttributes();
//[code="3caab1f1bbed4919ab85c30280f7ddbb", ret="Y", ts="2018-08-31T15:10:29.520+05:30", ttl="2019-08-31T15:10:29", txn="UKC:OTPEKYC20180831151011782"]

Node node1= node.getFirstChild();
//[tkn="01001873ZIkZdo82JnhtPMVS2Yw3/TV0OQSfQXYAODGM3DuYE10Prw4R+OdvtKFa7LsEgLWP", uid="xxxxxxxx4698"]
NamedNodeMap UidData=node1.getAttributes();
//[tkn="01001873ZIkZdo82JnhtPMVS2Yw3/TV0OQSfQXYAODGM3DuYE10Prw4R+OdvtKFa7LsEgLWP", uid="xxxxxxxx4698"]
NamedNodeMap poi=node1.getFirstChild().getAttributes();
//[dob="15-02-1986", gender="M", name="Ambuj Kumar Singh"]
NamedNodeMap poa=node1.getFirstChild().getNextSibling().getAttributes();
//[co="S/O: Arjun Singh", country="India", dist="Allahabad", loc="Banka Semari", pc="212301", state="Uttar Pradesh", vtc="Akodha"]
Object pht=node1.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getNodeValue();
System.out.println(pht);


//KycRes
String code=KycRes.getNamedItem("code").getNodeValue();
String ret=KycRes.getNamedItem("ret").getNodeValue();
String ts=KycRes.getNamedItem("ts").getNodeValue();
String ttl=KycRes.getNamedItem("ttl").getNodeValue();
String txn=KycRes.getNamedItem("txn").getNodeValue();

//UidData
String tkn=UidData.getNamedItem("tkn").getNodeValue();
String uid=UidData.getNamedItem("uid").getNodeValue();

//POI
String dob=poi.getNamedItem("dob").getNodeValue();
String gender=poi.getNamedItem("gender").getNodeValue();
String name=poi.getNamedItem("name").getNodeValue();

//POA
String co=poa.getNamedItem("co").getNodeValue();
String country=poa.getNamedItem("country").getNodeValue();
String dist=poa.getNamedItem("dist").getNodeValue();
String loc=poa.getNamedItem("loc").getNodeValue();
String pc=poa.getNamedItem("pc").getNodeValue();
String state=poa.getNamedItem("state").getNodeValue();
String vtc=poa.getNamedItem("vtc").getNodeValue();

//pht
System.out.println(pht.toString());

/*
Rcode = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("code")								
Rtxn = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("txn")								
Rts = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts").getTextContent();
Uuid = doc.getElementsByTagName("UidData").item(0).getAttributes().getNamedItem("uuid")
dob = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob")
gender = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("gender")
name = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name")
co = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co")
country = doc.getElementsByTagName("Poa").item(0).getAttributes()
dist = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist")
lm = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm")
loc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc")
pc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc")
po = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po")
state = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state")
vtc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc")
pht = doc.getElementsByTagName("Pht").item(0).getTextContent();
*/

KycXmlResponse respBean=new KycXmlResponse();
respBean.setTxnID(aadhaarTxnId);
respBean.setUID(uuid);
respBean.setServicetype(transType);
respBean.setPht(pht.toString());
respBean.setRCode(code);

respBean.setRtxn(aadhaarTxnId);
respBean.setRts(ts);;
respBean.setUuid(uuid);;

respBean.setPoidob(dob);
respBean.setPoigender(gender);
respBean.setPoiname(name);
respBean.setPoaco(co);
respBean.setPoadist(dist);
respBean.setPoalm(country);
respBean.setPoaloc(loc);
respBean.setPoapc(pc);
//respBean.setPoapo(po);
respBean.setPoastate(state);
respBean.setPoavtc(vtc);
respBean.setStatus(status);
respBean.setKyctxnid(txnId);
respBean.setRTimeStamp(aadhaarTs);

    

KYCService kycService=new KYCServiceImpl();
kycService.getResponseXml(respBean);



result.setSERVICETYPE(transType);
result.setUUID(uuid);
result.setRRN(txnId);
if (status!=null&&status.equalsIgnoreCase("Y"))
	result.setERRMSG("SUCCESS");
else
	result.setERRMSG("FAILED");



		
		
		
		System.out.println("resultuuid"+ result.getUUID());
		System.out.println("resulterrcode"+ result.getERRCODE());
		System.out.println("resulterrmsg"+ result.getERRMSG());
		System.out.println("resultservicetype"+ result.getSERVICETYPE());
		System.out.println("resultrrn"+ result.getRRN());
		
		
		request.setAttribute("result",result); 
		
	 
	 } catch (Exception e) {
		    e.printStackTrace();
		 }
	   if (status!=null&&status.equalsIgnoreCase("Y")){
	    	return "kycresp";
	    }else{
	    	return "error";
	    }
	}
	public String kycresponseAndriod(){
		


		String kycInfo=request.getParameter("KycInfo");
		String aadhaarTxnId=request.getParameter("AadhaarTxnId");
		String initVector=request.getParameter("Iv");
		String uuid=request.getParameter("Uuid");
		String status=request.getParameter("Status");
		String transType=request.getParameter("TransType");
		String aadhaarTs=request.getParameter("AadhaarTs");
		String txnId=request.getParameter("TxnId");
		
		KYCUtil kycUtil=new KYCUtil();
		
		
		String plainText=kycUtil.decrypt("cc3418fea553d9b0201cde8a207176a2", initVector, kycInfo);
		try{
		DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		ByteArrayInputStream input = new ByteArrayInputStream(plainText.toString().getBytes("UTF-8"));
	    Document doc = builder.parse(input);
	    doc.getDocumentElement().normalize();
	   Node node= doc.getFirstChild();
	   
	NamedNodeMap KycRes=node.getAttributes();
	//[code="3caab1f1bbed4919ab85c30280f7ddbb", ret="Y", ts="2018-08-31T15:10:29.520+05:30", ttl="2019-08-31T15:10:29", txn="UKC:OTPEKYC20180831151011782"]

	Node node1= node.getFirstChild();
	//[tkn="01001873ZIkZdo82JnhtPMVS2Yw3/TV0OQSfQXYAODGM3DuYE10Prw4R+OdvtKFa7LsEgLWP", uid="xxxxxxxx4698"]
	NamedNodeMap UidData=node1.getAttributes();
	//[tkn="01001873ZIkZdo82JnhtPMVS2Yw3/TV0OQSfQXYAODGM3DuYE10Prw4R+OdvtKFa7LsEgLWP", uid="xxxxxxxx4698"]
	NamedNodeMap poi=node1.getFirstChild().getAttributes();
	//[dob="15-02-1986", gender="M", name="Ambuj Kumar Singh"]
	NamedNodeMap poa=node1.getFirstChild().getNextSibling().getAttributes();
	//[co="S/O: Arjun Singh", country="India", dist="Allahabad", loc="Banka Semari", pc="212301", state="Uttar Pradesh", vtc="Akodha"]
	Object pht=node1.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getNodeValue();
	System.out.println(pht);


	//KycRes
	String code=KycRes.getNamedItem("code").getNodeValue();
	String ret=KycRes.getNamedItem("ret").getNodeValue();
	String ts=KycRes.getNamedItem("ts").getNodeValue();
	String ttl=KycRes.getNamedItem("ttl").getNodeValue();
	String txn=KycRes.getNamedItem("txn").getNodeValue();

	//UidData
	String tkn=UidData.getNamedItem("tkn").getNodeValue();
	String uid=UidData.getNamedItem("uid").getNodeValue();

	//POI
	String dob=poi.getNamedItem("dob").getNodeValue();
	String gender=poi.getNamedItem("gender").getNodeValue();
	String name=poi.getNamedItem("name").getNodeValue();

	//POA
	String co=poa.getNamedItem("co").getNodeValue();
	String country=poa.getNamedItem("country").getNodeValue();
	String dist=poa.getNamedItem("dist").getNodeValue();
	String loc=poa.getNamedItem("loc").getNodeValue();
	String pc=poa.getNamedItem("pc").getNodeValue();
	String state=poa.getNamedItem("state").getNodeValue();
	String vtc=poa.getNamedItem("vtc").getNodeValue();

	//pht
	System.out.println(pht.toString());

	/*
	Rcode = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("code")								
	Rtxn = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("txn")								
	Rts = doc.getElementsByTagName("KycRes").item(0).getAttributes().getNamedItem("ts").getTextContent();
	Uuid = doc.getElementsByTagName("UidData").item(0).getAttributes().getNamedItem("uuid")
	dob = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("dob")
	gender = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("gender")
	name = doc.getElementsByTagName("Poi").item(0).getAttributes().getNamedItem("name")
	co = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("co")
	country = doc.getElementsByTagName("Poa").item(0).getAttributes()
	dist = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("dist")
	lm = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("lm")
	loc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("loc")
	pc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("pc")
	po = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("po")
	state = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("state")
	vtc = doc.getElementsByTagName("Poa").item(0).getAttributes().getNamedItem("vtc")
	pht = doc.getElementsByTagName("Pht").item(0).getTextContent();
	*/

	KycXmlResponse respBean=new KycXmlResponse();
	respBean.setTxnID(aadhaarTxnId);
	respBean.setUID(uuid);
	respBean.setServicetype(transType);
	respBean.setPht(pht.toString());
	respBean.setRCode(code);

	respBean.setRtxn(aadhaarTxnId);
	respBean.setRts(ts);;
	respBean.setUuid(uuid);;

	respBean.setPoidob(dob);
	respBean.setPoigender(gender);
	respBean.setPoiname(name);
	respBean.setPoaco(co);
	respBean.setPoadist(dist);
	respBean.setPoalm(country);
	respBean.setPoaloc(loc);
	respBean.setPoapc(pc);
	//respBean.setPoapo(po);
	respBean.setPoastate(state);
	respBean.setPoavtc(vtc);
	respBean.setStatus(status);
	respBean.setKyctxnid(txnId);
	respBean.setRTimeStamp(aadhaarTs);

	    

	KYCService kycService=new KYCServiceImpl();
	kycService.getResponseXml(respBean);



	result.setSERVICETYPE(transType);
	result.setUUID(uuid);
	result.setRRN(txnId);

	if (status!=null&&status.equalsIgnoreCase("Y"))
		result.setERRMSG("SUCCESS");
	else
		result.setERRMSG("FAILED");

			
			
			
			System.out.println("resultuuid"+ result.getUUID());
			System.out.println("resulterrcode"+ result.getERRCODE());
			System.out.println("resulterrmsg"+ result.getERRMSG());
			System.out.println("resultservicetype"+ result.getSERVICETYPE());
			System.out.println("resultrrn"+ result.getRRN());
			
			
			request.setAttribute("result",result); 
			
		 
		 } catch (Exception e) {
			    e.printStackTrace();
			 }
		
		if (status!=null&&status.equalsIgnoreCase("Y")){
	    	return "kycrespandriod";
	    }else{
	    	return "error";
	    }
	    
	}

	public SenderKYCBean getMastBean() {
		return mastBean;
	}
	public void setMastBean(SenderKYCBean mastBean) {
		this.mastBean = mastBean;
	}
	public EkycResponseBean getKycbean() {
		return kycbean;
	}
	public void setKycbean(EkycResponseBean kycbean) {
		this.kycbean = kycbean;
	}
	public KYCRES getResult() {
		return result;
	}
	public void setResult(KYCRES result) {
		this.result = result;
	}
}
