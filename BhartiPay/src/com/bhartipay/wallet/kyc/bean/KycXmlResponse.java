package com.bhartipay.wallet.kyc.bean;


public class KycXmlResponse {
	private String kyctxnid;
	
	private String senderid;
	
	private String status; 
	
	private String agentid; 
	
	private String others; 
	
	private String aggreator; 
	
	private String deviceid; 
	
	private String time; 
	
	private String servicetype; 
	
	private String UID ;
	
	private String Poiname; 
	
	private String Poidob;
	
	private String Poigender; 
	
	private String Poiphone;
	
	private String Poiemail; 
	
	private String Poaco; 
	
	private String Poadist; 
	
	private String Poahouse; 
	
	private String Poaloc;
	
	private String Poapc; 
	
	private String Poastate; 

	private String Poavtc; 
	
	private String PoavtcCode; 

	private String Poastreet;
	
	private String Poalm;
	
	private String Poasubdist; 
	
	private String Poapo; 
	
	private String Pht;
	
	private String CreatedDate;
	
	private String UserID;
	
	private String TxnID;
	
	private String RCode; 
	
	private String RTimeStamp;
	
	
	private String Rtxn;
	
	private String Rts;
	
	private String Uuid;
	
	public String getKyctxnid() {
		return kyctxnid;
	}
	public void setKyctxnid(String kyctxnid) {
		this.kyctxnid = kyctxnid;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public String getAggreator() {
		return aggreator;
	}
	public void setAggreator(String aggreator) {
		this.aggreator = aggreator;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getServicetype() {
		return servicetype;
	}
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getPoiname() {
		return Poiname;
	}
	public void setPoiname(String poiname) {
		Poiname = poiname;
	}
	public String getPoidob() {
		return Poidob;
	}
	public void setPoidob(String poidob) {
		Poidob = poidob;
	}
	public String getPoigender() {
		return Poigender;
	}
	public void setPoigender(String poigender) {
		Poigender = poigender;
	}
	public String getPoiphone() {
		return Poiphone;
	}
	public void setPoiphone(String poiphone) {
		Poiphone = poiphone;
	}
	public String getPoiemail() {
		return Poiemail;
	}
	public void setPoiemail(String poiemail) {
		Poiemail = poiemail;
	}
	public String getPoaco() {
		return Poaco;
	}
	public void setPoaco(String poaco) {
		Poaco = poaco;
	}
	public String getPoadist() {
		return Poadist;
	}
	public void setPoadist(String poadist) {
		Poadist = poadist;
	}
	public String getPoahouse() {
		return Poahouse;
	}
	public void setPoahouse(String poahouse) {
		Poahouse = poahouse;
	}
	public String getPoaloc() {
		return Poaloc;
	}
	public void setPoaloc(String poaloc) {
		Poaloc = poaloc;
	}
	public String getPoapc() {
		return Poapc;
	}
	public void setPoapc(String poapc) {
		Poapc = poapc;
	}
	public String getPoastate() {
		return Poastate;
	}
	public void setPoastate(String poastate) {
		Poastate = poastate;
	}
	public String getPoavtc() {
		return Poavtc;
	}
	public void setPoavtc(String poavtc) {
		Poavtc = poavtc;
	}
	public String getPoavtcCode() {
		return PoavtcCode;
	}
	public void setPoavtcCode(String poavtcCode) {
		PoavtcCode = poavtcCode;
	}
	public String getPoastreet() {
		return Poastreet;
	}
	public void setPoastreet(String poastreet) {
		Poastreet = poastreet;
	}
	public String getPoalm() {
		return Poalm;
	}
	public void setPoalm(String poalm) {
		Poalm = poalm;
	}
	public String getPoasubdist() {
		return Poasubdist;
	}
	public void setPoasubdist(String poasubdist) {
		Poasubdist = poasubdist;
	}
	public String getPoapo() {
		return Poapo;
	}
	public void setPoapo(String poapo) {
		Poapo = poapo;
	}
	public String getPht() {
		return Pht;
	}
	public void setPht(String pht) {
		Pht = pht;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getTxnID() {
		return TxnID;
	}
	public void setTxnID(String txnID) {
		TxnID = txnID;
	}
	public String getRCode() {
		return RCode;
	}
	public void setRCode(String rCode) {
		RCode = rCode;
	}
	public String getRTimeStamp() {
		return RTimeStamp;
	}
	public void setRTimeStamp(String rTimeStamp) {
		RTimeStamp = rTimeStamp;
	}
	public String getRtxn() {
		return Rtxn;
	}
	public void setRtxn(String rtxn) {
		Rtxn = rtxn;
	}
	public String getRts() {
		return Rts;
	}
	public void setRts(String rts) {
		Rts = rts;
	}
	public String getUuid() {
		return Uuid;
	}
	public void setUuid(String uuid) {
		Uuid = uuid;
	}
	


}
