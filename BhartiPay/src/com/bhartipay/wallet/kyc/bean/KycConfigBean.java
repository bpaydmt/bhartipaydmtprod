package com.bhartipay.wallet.kyc.bean;
import java.io.Serializable;

import javax.persistence.Transient;

public class KycConfigBean implements Serializable{
	
	private String aggreator;
	private String slk;
	private String servicetype;
	private String deviceid;
	private String stack;	
	private String redirecturl;
	private String callbackurl;
	public String statusCode;
	private String servicevalue;
	private String xmlstring;
    private String requestFrom;
    private String iv;
    

	
    
    
	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getRequestFrom() {
		return requestFrom;
	}

	public void setRequestFrom(String requestFrom) {
		this.requestFrom = requestFrom;
	}

	public String statusDesc;
	
	
	public String authxml;
	
	public String getAggreator() {
		return aggreator;
	}
	
	public void setAggreator(String aggreator) {
		this.aggreator = aggreator;
	}
	
	
	public String getSlk() {
		return slk;
	}
	
	public void setSlk(String slk) {
		this.slk = slk;
	}
	
	public String getServicetype() {
		return servicetype;
	}
	
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	
	public String getDeviceid() {
		return deviceid;
	}
	
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	
	public String getStack() {
		return stack;
	}
	
	public void setStack(String stack) {
		this.stack = stack;
	}
	
	public String getRedirecturl() {
		return redirecturl;
	}
	
	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}

	public String getAuthxml() {
		return authxml;
	}

	public void setAuthxml(String authxml) {
		this.authxml = authxml;
	}

	public String getCallbackurl() {
		return callbackurl;
	}

	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getServicevalue() {
		return servicevalue;
	}

	public void setServicevalue(String servicevalue) {
		this.servicevalue = servicevalue;
	}

	public String getXmlstring() {
		return xmlstring;
	}

	public void setXmlstring(String xmlstring) {
		this.xmlstring = xmlstring;
	}
	
	

}
