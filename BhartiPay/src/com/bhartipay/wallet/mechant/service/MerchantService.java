package com.bhartipay.wallet.mechant.service;

import java.util.List;

import com.bhartipay.wallet.merchant.persistence.vo.MerchantSummary;
import com.bhartipay.wallet.merchant.persistence.vo.MerchentBean;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;

public interface MerchantService {

	public MerchantSummary saveMerchant(MerchantSummary merchantSummary,String ipimei,String userAgent);
	
	public PaymentParameter validateSaveTxn(PaymentParameter parameter,String ipimei,String userAgent);
	
	public String merchentPaymet(MerchentBean merchantBean,String ipimei,String userAgent);
	
	public List<MerchantSummary> getMerchantList(MerchantSummary merchantSummary);
}
