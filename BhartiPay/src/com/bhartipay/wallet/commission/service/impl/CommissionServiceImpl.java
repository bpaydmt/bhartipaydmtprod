package com.bhartipay.wallet.commission.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import com.bhartipay.wallet.commission.persistence.vo.CommInputBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommPercViewMast;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommRequest;
import com.bhartipay.wallet.commission.persistence.vo.CommissionBean;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CommissionServiceImpl implements CommissionService{

	@Override
	public CommPlanMaster createPlan(CommPlanMaster master,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(master);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/createPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommPlanMaster commMaster = gson.fromJson(output, CommPlanMaster.class);		
		return commMaster;
	}

	@Override
	public Map<String, HashMap<String, String>> getPlanDtl(CommPlanMaster commBean) {
		
		System.out.println("____________Inside getplanDtl____________");
		Gson gson = new Gson();
		String jsonText=gson.toJson(commBean);
		
		System.out.println("________jsonText_____________"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/getPlanDtl");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, Map.class);
		return myMap;
	}

	
	@Override
	public Map<String, HashMap<String, String>> getAllPlans(CommPlanMaster commBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(commBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/getAllPlans");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, Map.class);
		return myMap;
	}

	@Override
	public CommInputBean getCommPerc(CommInputBean masterKey) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(masterKey);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/getCommPercDtl");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommInputBean commMaster = gson.fromJson(output, CommInputBean.class);		
		return commMaster;
	}

	@Override
	public CommInputBean saveUpdateCommPerc(CommInputBean masterBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(masterBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/saveUpdateCommPerc");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommInputBean commMaster = gson.fromJson(output, CommInputBean.class);		
		return commMaster;
	}

	@Override
	public ReportBean assignCommission(ReportBean reportBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/assignCommission");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		reportBean= gson.fromJson(output, ReportBean.class);		
		return reportBean;
	}
	
	@Override
	public Map<String, String> getCommission(ReportBean reportBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		WebResource webResource = ServiceManager
				.getWebResource("CommissionManager/getCommission");
		ClientResponse response = webResource.type(
				MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}

	public Map<String, String> convertToMap(JsonObject jsonObject) {
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(jsonObject, Map.class);
		return myMap;
	}

	@Override
	public List<CommPercViewMast> showCommByPlanId(ReportBean reportBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/ShowCommByPlanId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CommPercViewMast> comPercMaster= gson.fromJson(output, OTOList.class);		
		return comPercMaster;
		


	}

	@Override
	public CommPlanMaster showPlan(ReportBean reportBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/ShowPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommPlanMaster comPlanMaster= gson.fromJson(output, CommPlanMaster.class);		
		return comPlanMaster;
	}

	@Override
	public CommPlanMaster updatePlan(CommPlanMaster master,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(master);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/updatePlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommPlanMaster comPlanMaster= gson.fromJson(output, CommPlanMaster.class);		
		return comPlanMaster;
	}

	@Override
	public CommInputBean editShowCommByCommId(CommInputBean commbean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(commbean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/ShowCommByCommId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CommInputBean commInputBean= gson.fromJson(output, CommInputBean.class);		
		return commInputBean;
	}
	@Override	
public List<CommissionBean> getCommissionByAgentId(CommissionBean commissionBean){
		Gson gson = new Gson();
		String jsonText=gson.toJson(commissionBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/GetCommissionByAgentId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200&&response.getStatus() != 204){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		List<CommissionBean> commInputBean=null;
		if(response.getStatus() != 204){
		String output = response.getEntity(String.class);
		commInputBean= gson.fromJson(output, OTOList1.class);	
		}
		return commInputBean;
	}
	@Override
	public String assignCommission(CommRequest commissionBean){
		Gson gson = new Gson();
		String jsonText=gson.toJson(commissionBean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/AssignCommission");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		//CommInputBean commInputBean= gson.fromJson(output, CommInputBean.class);		
		return output;
	}
	@Override
	public String CommService(CommissionBean assignCommbean){
		Gson gson = new Gson();
		String jsonText=gson.toJson(assignCommbean);
		
		WebResource webResource=ServiceManager.getWebResource("CommissionManager/assignCommisson");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		//CommInputBean commInputBean= gson.fromJson(output, CommInputBean.class);		
		return output;
		
	}

}
class OTOList extends ArrayList<CommPercViewMast>{
	
}
class OTOList1 extends ArrayList<CommissionBean>{
	
}