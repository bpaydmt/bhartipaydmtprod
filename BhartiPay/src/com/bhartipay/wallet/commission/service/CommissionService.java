package com.bhartipay.wallet.commission.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhartipay.wallet.commission.persistence.vo.CommInputBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMasterKey;
import com.bhartipay.wallet.commission.persistence.vo.CommPercViewMast;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommRequest;
import com.bhartipay.wallet.commission.persistence.vo.CommissionBean;
import com.bhartipay.wallet.report.bean.ReportBean;

public interface CommissionService {
	/**
	 * 
	 * @param master
	 * @return
	 */
	public CommPlanMaster createPlan(CommPlanMaster master,String ipimei,String userAgent);
	
	/**
	 * 
	 * @return
	 */
	public Map<String,HashMap<String,String>> getPlanDtl(CommPlanMaster commBean);
	
	/**
	 * 
	 * @return
	 */
	public Map<String,HashMap<String,String>> getAllPlans(CommPlanMaster commBean);
	
	/**
	 * 
	 * @param masterKey
	 * @return
	 */
	public CommInputBean getCommPerc(CommInputBean masterKey);
	
	/**
	 * 
	 * @param masterKey
	 * @return
	 */
	public CommInputBean saveUpdateCommPerc(CommInputBean masterBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public ReportBean assignCommission(ReportBean reportBean);

/**
 * 
 * @param id
 * @return
 */
	public Map<String, String> getCommission(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<CommPercViewMast> showCommByPlanId(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public CommPlanMaster showPlan(ReportBean reportBean);
	
	/**
	 * 
	 * @param master
	 * @return
	 */
	public CommPlanMaster updatePlan(CommPlanMaster master,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param commbean
	 * @return
	 */
	public CommInputBean editShowCommByCommId(CommInputBean commbean);
	
	public String CommService(CommissionBean assignCommbean); 
	
	public List<CommissionBean> getCommissionByAgentId(CommissionBean commissionBean);
	
	public String assignCommission(CommRequest commissionBean);
	
}
