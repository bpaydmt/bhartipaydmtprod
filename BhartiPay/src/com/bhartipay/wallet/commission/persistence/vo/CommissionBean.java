package com.bhartipay.wallet.commission.persistence.vo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

public class CommissionBean {
	
	private int commid;
	private String planId;
	private String distributorId;
	private String agentId;
	private double agentPer;
	private double maxCharges;	
	private int maxChargesCheck;	
	private double disPer;
	private double superDisPer;
	private double serviceCharges;
	private double rangeTo;
	private double rangeFrom;
	private double commperc;
	private double fixedCharges;
	private int fixedChargesCheck;
	private String ipiemi;
	private String agent;
	private String txntype;
	private String fixedorperc;
	private int bankrefid;
	
	
	
	
	public int getBankrefid() {
		return bankrefid;
	}

	public void setBankrefid(int bankrefid) {
		this.bankrefid = bankrefid;
	}

	public double getCommperc() {
		return commperc;
	}

	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}

	public String getFixedorperc() {
		return fixedorperc;
	}

	public void setFixedorperc(String fixedorperc) {
		this.fixedorperc = fixedorperc;
	}

	public String getTxntype() {
		return txntype;
	}

	public void setTxntype(String txntype) {
		this.txntype = txntype;
	}

	public int getCommid() {
		return commid;
	}

	public void setCommid(int commid) {
		this.commid = commid;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public double getAgentPer() {
		return agentPer;
	}

	public void setAgentPer(double agentPer) {
		this.agentPer = agentPer;
	}

	public double getMaxCharges() {
		return maxCharges;
	}

	public void setMaxCharges(double maxCharges) {
		this.maxCharges = maxCharges;
	}

	public int getMaxChargesCheck() {
		return maxChargesCheck;
	}

	public void setMaxChargesCheck(int maxChargesCheck) {
		this.maxChargesCheck = maxChargesCheck;
	}

	public double getDisPer() {
		return disPer;
	}

	public void setDisPer(double disPer) {
		this.disPer = disPer;
	}

	public double getSuperDisPer() {
		return superDisPer;
	}

	public void setSuperDisPer(double superDisPer) {
		this.superDisPer = superDisPer;
	}

	public double getServiceCharges() {
		return serviceCharges;
	}

	public void setServiceCharges(double serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public double getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(double rangeTo) {
		this.rangeTo = rangeTo;
	}

	public double getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(double rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	public double getFixedCharges() {
		return fixedCharges;
	}

	public void setFixedCharges(double fixedCharges) {
		this.fixedCharges = fixedCharges;
	}

	public int getFixedChargesCheck() {
		return fixedChargesCheck;
	}

	public void setFixedChargesCheck(int fixedChargesCheck) {
		this.fixedChargesCheck = fixedChargesCheck;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	
	
}
