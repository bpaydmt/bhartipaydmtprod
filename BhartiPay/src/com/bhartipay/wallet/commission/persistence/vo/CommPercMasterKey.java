package com.bhartipay.wallet.commission.persistence.vo;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

public class CommPercMasterKey implements Serializable{
	private String planId ;
	private int txnid ;
	
	
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public int getTxnid() {
		return txnid;
	}
	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}
	
	

}
