package com.bhartipay.wallet.commission.persistence.vo;

import java.io.Serializable;
import java.util.List;

public class CommPercMaster implements Serializable{
	
	private CommPercMasterKey id;
	private double fixedCharge;
	private double serviceTax;
	private int bankRefID;
	private int rangeFrom;
	private int rangeTo;
	private String desc;
	private double sbcess;
	private double kycess;
	private double aggregatorperc;
	private double distributorperc;
	private double superdistperc;
	private double agentperc;
	private double subagentperc;
	private double commperc;
	private String statusCode ;	
	private String planId ;
	private int txnid ;
	private List<CommPercRangeMaster> commPercRangeMaster;
	
	private int commid ;
	

	
	public List<CommPercRangeMaster> getCommPercRangeMaster() {
		return commPercRangeMaster;
	}
	public void setCommPercRangeMaster(List<CommPercRangeMaster> commPercRangeMaster) {
		this.commPercRangeMaster = commPercRangeMaster;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public int getTxnid() {
		return txnid;
	}
	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}
	
	
	
	

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public CommPercMasterKey getId() {
		return id;
	}

	public void setId(CommPercMasterKey id) {
		this.id = id;
	}

	public double getFixedCharge() {
		return fixedCharge;
	}

	public void setFixedCharge(double fixedCharge) {
		this.fixedCharge = fixedCharge;
	}

	public double getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}

	public int getBankRefID() {
		return bankRefID;
	}

	public void setBankRefID(int bankRefID) {
		this.bankRefID = bankRefID;
	}

	public int getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(int rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	public int getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(int rangeTo) {
		this.rangeTo = rangeTo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getSbcess() {
		return sbcess;
	}

	public void setSbcess(double sbcess) {
		this.sbcess = sbcess;
	}

	public double getKycess() {
		return kycess;
	}

	public void setKycess(double kycess) {
		this.kycess = kycess;
	}

	public double getAggregatorperc() {
		return aggregatorperc;
	}

	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}

	public double getDistributorperc() {
		return distributorperc;
	}

	public void setDistributorperc(double distributorperc) {
		this.distributorperc = distributorperc;
	}

	public double getAgentperc() {
		return agentperc;
	}

	public void setAgentperc(double agentperc) {
		this.agentperc = agentperc;
	}

	public double getSubagentperc() {
		return subagentperc;
	}

	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}

	public double getCommperc() {
		return commperc;
	}

	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}
	public double getSuperdistperc() {
		return superdistperc;
	}
	public void setSuperdistperc(double superdistperc) {
		this.superdistperc = superdistperc;
	}
	public int getCommid() {
		return commid;
	}
	public void setCommid(int commid) {
		this.commid = commid;
	}
	
	

	     

	

}
