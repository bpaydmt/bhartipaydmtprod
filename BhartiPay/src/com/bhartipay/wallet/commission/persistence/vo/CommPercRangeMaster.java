package com.bhartipay.wallet.commission.persistence.vo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="commpercrangemaster")

public class CommPercRangeMaster {
	
	private int commrangeid ;
	private double fixedcharge;
	private int rangefrom;
	private int rangeto;
	private int commid ;
	public int getCommrangeid() {
		return commrangeid;
	}
	public void setCommrangeid(int commrangeid) {
		this.commrangeid = commrangeid;
	}
	public double getFixedcharge() {
		return fixedcharge;
	}
	public void setFixedcharge(double fixedcharge) {
		this.fixedcharge = fixedcharge;
	}
	public int getRangefrom() {
		return rangefrom;
	}
	public void setRangefrom(int rangefrom) {
		this.rangefrom = rangefrom;
	}
	public int getRangeto() {
		return rangeto;
	}
	public void setRangeto(int rangeto) {
		this.rangeto = rangeto;
	}
	public int getCommid() {
		return commid;
	}
	public void setCommid(int commid) {
		this.commid = commid;
	}
	
	

}
