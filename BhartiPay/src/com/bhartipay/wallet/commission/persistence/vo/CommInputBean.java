package com.bhartipay.wallet.commission.persistence.vo;

import java.util.List;
import java.util.Set;

public class CommInputBean {
	
	private String planid;
	private int txnid ;
	private double servicetax;
	private int bankrefiD;
	private List<Double> fixedcharge;
	private List<Integer> rangefrom;
	private List<Integer> rangeto;
	private String description;
	private double sbcess;
	private double kycess;
	private double aggregatorperc;
	private double distributorperc;
	private double agentperc;
	private double subagentperc;
	private double commperc;
	private String statuscode;
	private int commid;
	private Set<CommPercRangeMaster> commPercRangeMaster;
	

	

	public Set<CommPercRangeMaster> getCommPercRangeMaster() {
		return commPercRangeMaster;
	}
	public void setCommPercRangeMaster(Set<CommPercRangeMaster> commPercRangeMaster) {
		this.commPercRangeMaster = commPercRangeMaster;
	}
	public int getCommid() {
		return commid;
	}
	public void setCommid(int commId) {
		this.commid = commId;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getPlanid() {
		return planid;
	}
	public void setPlanid(String planid) {
		this.planid = planid;
	}
	public int getTxnid() {
		return txnid;
	}
	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}

	public double getServicetax() {
		return servicetax;
	}
	public void setServicetax(double servicetax) {
		this.servicetax = servicetax;
	}
	public int getBankrefiD() {
		return bankrefiD;
	}
	public void setBankrefiD(int bankrefiD) {
		this.bankrefiD = bankrefiD;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getSbcess() {
		return sbcess;
	}
	public void setSbcess(double sbcess) {
		this.sbcess = sbcess;
	}
	public double getKycess() {
		return kycess;
	}
	public void setKycess(double kycess) {
		this.kycess = kycess;
	}
	public double getAggregatorperc() {
		return aggregatorperc;
	}
	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}
	public double getDistributorperc() {
		return distributorperc;
	}
	public void setDistributorperc(double distributorperc) {
		this.distributorperc = distributorperc;
	}
	public double getAgentperc() {
		return agentperc;
	}
	public void setAgentperc(double agentperc) {
		this.agentperc = agentperc;
	}
	public double getSubagentperc() {
		return subagentperc;
	}
	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}
	public double getCommperc() {
		return commperc;
	}
	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}
	public List<Double> getFixedcharge() {
		return fixedcharge;
	}
	public void setFixedcharge(List<Double> fixedcharge) {
		this.fixedcharge = fixedcharge;
	}
	public List<Integer> getRangefrom() {
		return rangefrom;
	}
	public void setRangefrom(List<Integer> rangefrom) {
		this.rangefrom = rangefrom;
	}
	public List<Integer> getRangeto() {
		return rangeto;
	}
	public void setRangeto(List<Integer> rangeto) {
		this.rangeto = rangeto;
	}

}
