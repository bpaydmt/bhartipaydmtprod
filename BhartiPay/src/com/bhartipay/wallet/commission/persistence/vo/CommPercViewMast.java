package com.bhartipay.wallet.commission.persistence.vo;


public class CommPercViewMast {
	
	

	
	
	
	private String planid;
	private int txnid;
	private double  fixedcharge;
	private int  rangefrom;
	private int rangeto ;
	private String   description;
	private double  servicetax ;
	private double  sbcess;
	private double  kycess;
	private double  aggregatorperc;
	private double  distributorperc;
	private double  agentperc;
	private double  subagentperc;
	private double  commperc;
	public String getPlanid() {
		return planid;
	}
	public void setPlanid(String planid) {
		this.planid = planid;
	}
	public int getTxnid() {
		return txnid;
	}
	public void setTxnid(int txnid) {
		this.txnid = txnid;
	}
	public double getFixedcharge() {
		return fixedcharge;
	}
	public void setFixedcharge(double fixedcharge) {
		this.fixedcharge = fixedcharge;
	}
	public int getRangefrom() {
		return rangefrom;
	}
	public void setRangefrom(int rangefrom) {
		this.rangefrom = rangefrom;
	}
	public int getRangeto() {
		return rangeto;
	}
	public void setRangeto(int rangeto) {
		this.rangeto = rangeto;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getServicetax() {
		return servicetax;
	}
	public void setServicetax(double servicetax) {
		this.servicetax = servicetax;
	}
	public double getSbcess() {
		return sbcess;
	}
	public void setSbcess(double sbcess) {
		this.sbcess = sbcess;
	}
	public double getKycess() {
		return kycess;
	}
	public void setKycess(double kycess) {
		this.kycess = kycess;
	}
	public double getAggregatorperc() {
		return aggregatorperc;
	}
	public void setAggregatorperc(double aggregatorperc) {
		this.aggregatorperc = aggregatorperc;
	}
	public double getDistributorperc() {
		return distributorperc;
	}
	public void setDistributorperc(double distributorperc) {
		this.distributorperc = distributorperc;
	}
	public double getAgentperc() {
		return agentperc;
	}
	public void setAgentperc(double agentperc) {
		this.agentperc = agentperc;
	}
	public double getSubagentperc() {
		return subagentperc;
	}
	public void setSubagentperc(double subagentperc) {
		this.subagentperc = subagentperc;
	}
	public double getCommperc() {
		return commperc;
	}
	public void setCommperc(double commperc) {
		this.commperc = commperc;
	}
	
	
	
	
	
	
	
	
	

}
