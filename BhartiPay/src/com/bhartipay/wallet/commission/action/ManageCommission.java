package com.bhartipay.wallet.commission.action;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.RandomStringUtils;

import com.bhartipay.wallet.commission.persistence.vo.CommInputBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMasterKey;
import com.bhartipay.wallet.commission.persistence.vo.CommPercRangeMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommPercViewMast;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.persistence.vo.CommRequest;
import com.bhartipay.wallet.commission.persistence.vo.CommissionBean;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class ManageCommission extends BaseAction{

	private static final long serialVersionUID = 8535182832566240343L;
	Properties prop =null;
	public ManageCommission(){
		/* added by neeraj call error code using properties file. */
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		/*end by neeraj*/
	}

	private String agentId;
	CommissionBean assignCommission = new CommissionBean();
	List<CommissionBean> cBean=new ArrayList<CommissionBean>();
	CommissionService comService=new CommissionServiceImpl();
	CommPlanMaster commBean=new CommPlanMaster();
	CommInputBean cpmaster=new CommInputBean();
	CommPercMasterKey ckBean=new CommPercMasterKey();
	UserService service=new UserServiceImpl();
	
	Map<String, String> plans=new HashMap<String, String>();
	Map<String, String> txnType=new HashMap<String, String>();
	
	
	TxnInputBean inputBean=new TxnInputBean();
	Map<String, String> aggrigatorList=new LinkedHashMap<String, String>();
	Map<String, String> distributorList=new LinkedHashMap<String, String>();
	Map<String, String> agentList=new LinkedHashMap<String, String>();
	Map<String,String> statusList=new LinkedHashMap<String, String>();
 	public Map<String,String> subagentList=new LinkedHashMap<String, String>();
	public ReportBean reportBean=new ReportBean();
	
	
	
	
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public List<CommissionBean> getcBean() {
		return cBean;
	}

	public void setcBean(List<CommissionBean> cBean) {
		this.cBean = cBean;
	}

	public CommissionBean getAssignCommission() {
		return assignCommission;
	}

	public void setAssignCommission(CommissionBean assignCommission) {
		this.assignCommission = assignCommission;
	}

	public Map<String, String> getStatusList() {
		return statusList;
	}

	public void setStatusList(Map<String, String> statusList) {
		this.statusList = statusList;
	}

	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}

	public Map<String, String> getAggrigatorList() {
		return aggrigatorList;
	}

	public void setAggrigatorList(Map<String, String> aggrigatorList) {
		this.aggrigatorList = aggrigatorList;
	}

	public Map<String, String> getDistributorList() {
		return distributorList;
	}

	public void setDistributorList(Map<String, String> distributorList) {
		this.distributorList = distributorList;
	}

	public Map<String, String> getAgentList() {
		return agentList;
	}

	public void setAgentList(Map<String, String> agentList) {
		this.agentList = agentList;
	}

	public Map<String, String> getSubagentList() {
		return subagentList;
	}

	public void setSubagentList(Map<String, String> subagentList) {
		this.subagentList = subagentList;
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public CommissionService getComService() {
		return comService;
	}

	public void setComService(CommissionService comService) {
		this.comService = comService;
	}

	public CommInputBean getCpmaster() {
		return cpmaster;
	}

	public void setCpmaster(CommInputBean cpmaster) {
		this.cpmaster = cpmaster;
	}

	public CommPercMasterKey getCkBean() {
		return ckBean;
	}

	public void setCkBean(CommPercMasterKey ckBean) {
		this.ckBean = ckBean;
	}

	public Map<String, String> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, String> plans) {
		this.plans = plans;
	}

	public Map<String, String> getTxnType() {
		return txnType;
	}

	public void setTxnType(Map<String, String> txnType) {
		this.txnType = txnType;
	}

	public CommPlanMaster getCommBean() {
		return commBean;
	}

	public void setCommBean(CommPlanMaster commBean) {
		this.commBean = commBean;
	}
	
	public boolean generateCsrfToken(){
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

            session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
        }

        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

        session.put("csrfPreventionSalt", salt);
        return true;
	}
	public String createPlan(){		
		generateCsrfToken();
		return "success";
	}
	
	public String saveCreatePlan(){
		String userAgent=request.getHeader("User-Agent");
		String ipAddress =  request.getRemoteAddr();
	if (ipAddress == null) {
		ipAddress = request.getHeader("X-FORWARDED-FOR");
	}
		if(commBean==null){
			addActionError("An error has occurred please try again after some timie.");
			return "fail";
		}
		if(commBean.getPlanType()==null||commBean.getPlanType().isEmpty()){
			addActionError("Plan type can not be blank.");
			return "fail";
		}
		if(commBean.getPlanDescription()==null||commBean.getPlanDescription().isEmpty()){
			addActionError("Plan description can not be blank.");
			return "fail";
		}
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		commBean=comService.createPlan(commBean,ipAddress,userAgent);
		if(commBean!=null&&commBean.getStatusCode().equalsIgnoreCase("1000")){
			commBean.setPlanType(null);
			commBean.setPlanDescription(null);
			addActionMessage("Plan created successfully. plan id is "+commBean.getPlanId());
			return "success";
		}
		if(commBean!=null&&commBean.getStatusCode().equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("11001"));
			return "fail";
		}
		if(commBean!=null&&commBean.getStatusCode().equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("7000"));
			return "fail";
		}
		if(commBean!=null&&commBean.getStatusCode().equalsIgnoreCase("9004")){
			addActionError(prop.getProperty("9004"));
			return "fail";
		}
		return "success";
	}
	
	public String defineCommission(){
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		return "success";
	}
	
	
	public String getCommPerc(){
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		
		if(reportBean==null||reportBean.getPlanId()==null||reportBean.getPlanId().equalsIgnoreCase("-1")){
			if(cpmaster==null||cpmaster.getPlanid()==null||cpmaster.getPlanid().equalsIgnoreCase("-1")){
			addActionError("Please select a Plan.");
			return "fail";
			}else if(reportBean!=null&&reportBean.getPlanId()!=null){
				cpmaster.setPlanid(reportBean.getPlanId());
			}
		}
		
		if(reportBean.getTxnType()==null||reportBean.getTxnType().equalsIgnoreCase("-1")){
			if(cpmaster==null||cpmaster.getTxnid()==-1){
			addActionError("Please select a Transaction Type.");
			return "fail";
			}else if(reportBean.getTxnType()!=null){
				cpmaster.setTxnid(Integer.parseInt(reportBean.getTxnType()));
			}
		}
		
		/*if(cpmaster==null||cpmaster.getPlanid()==null||cpmaster.getPlanid().equalsIgnoreCase("-1")){
		addActionError("Please select a plan.");
		return "fail";
	}
	if(cpmaster.getTxnid()==-1){
		addActionError("Please select a transaction type.");
		return "fail";
	}*/
		CommInputBean cominBean=comService.getCommPerc(cpmaster);
		setCpmaster(cominBean);		
		ckBean.setPlanId(cpmaster.getPlanid());
		ckBean.setTxnid(cpmaster.getTxnid());
		request.setAttribute("commList",cominBean);
		request.setAttribute("show","true");		
		return "success";
	}
	
	public String saveAssignCommission(){
		String userAgent=request.getHeader("User-Agent");
		String ipAddress =  request.getRemoteAddr();
	if (ipAddress == null) {
		ipAddress = request.getHeader("X-FORWARDED-FOR");
	}
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		request.setAttribute("show","true");	
		/*if(cpmaster.getRangefrom()>=cpmaster.getRangeto()){
			addActionError("Please check range. Range from must be less thant range to");
			return "fail";
		}*/
		double sum=cpmaster.getAggregatorperc()+cpmaster.getDistributorperc()+cpmaster.getAgentperc()+cpmaster.getSubagentperc();
		if(sum>100||sum<100){
			addActionError("please check user percentage bifurcation");
			return "fail";
		}
		
		
		
		if(cpmaster.getFixedcharge()!=null){
			Set<CommPercRangeMaster> commList=new LinkedHashSet<CommPercRangeMaster>();
			for (int i=0;i<cpmaster.getFixedcharge().size();i++) {
				CommPercRangeMaster rangeMaster=new CommPercRangeMaster();
				rangeMaster.setFixedcharge(cpmaster.getFixedcharge().get(i));
				rangeMaster.setRangefrom(cpmaster.getRangefrom().get(i));
				rangeMaster.setRangeto(cpmaster.getRangeto().get(i));
				commList.add(rangeMaster);
				
			}
			cpmaster.setCommPercRangeMaster(commList);
		}
		
		request.setAttribute("commList",cpmaster);
		cpmaster=comService.saveUpdateCommPerc(cpmaster,ipAddress,userAgent);
		setCpmaster(cpmaster);
		ckBean.setPlanId(cpmaster.getPlanid());
		ckBean.setTxnid(cpmaster.getTxnid());
		if(cpmaster.getStatuscode().equalsIgnoreCase("1000")){
			addActionMessage("Commission Plan defined successfully.");
			if(cpmaster.getTxnid()!=0)			
			reportBean.setTxnType(""+cpmaster.getTxnid());
			if(cpmaster.getPlanid()!=null&&!cpmaster.getPlanid().isEmpty())
			reportBean.setPlanId(cpmaster.getPlanid());
			
			//List<CommPercViewMast> list=comService.showCommByPlanId(reportBean);
			CommPlanMaster cpMaster=comService.showPlan(reportBean);
			
			if(cpMaster==null||cpMaster.getPlanId()==null||cpMaster.getPlanType()==null){
				addActionError("Problem with fetchening result from server please try again.");
				return "fail";
			}
			setCommBean(cpMaster);
			request.setAttribute("commList",cpmaster);
			//request.setAttribute("cpList",list);
			return "success";
			
		}else if(cpmaster.getStatuscode().equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("111001"));
		}else if(cpmaster.getStatuscode().equalsIgnoreCase("9005")){
			addActionError(prop.getProperty("9005"));
		}else if(cpmaster.getStatuscode().equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("7000"));
		}else{
			addActionError(prop.getProperty("7000"));
		}
		return "fail";
		
	}
	
	
	
	public String assignCommission(){	
		generateCsrfToken();
		User user=(User)session.get("User");		
		if(user.getUsertype()==99){
		if(reportBean.getAggId()==null){
			reportBean.setAggId("-1");
		}	
		if(reportBean.getDistId()==null){
			reportBean.setDistId("-1");
		}
		if(reportBean.getAgentId()==null){
			reportBean.setAgentId("-1");
		}
		if(reportBean.getSubagentId()==null){
			reportBean.setSubagentId("-1");
		}
		}
		if(user.getUsertype()==4){		
			
			reportBean.setAggId(user.getId());	
			if(reportBean.getDistId()==null){
				reportBean.setDistId("-1");	
			}
			if(reportBean.getAgentId()==null){
				reportBean.setAgentId("-1");
			}
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}
			
		}
		if(user.getUsertype()==3){
			reportBean.setAggId("agg");		
			reportBean.setDistId(user.getId());	
			if(reportBean.getAgentId()==null){
				reportBean.setAgentId("-1");
			}	
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}	
		}
		
		if(user.getUsertype()==2){
			reportBean.setAggId("agg");		
			reportBean.setDistId("dis");	
			reportBean.setAgentId(user.getId());
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}	
			
		}
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		Map<String, String> aggrigatorList=service.getAggreator();	
		if(aggrigatorList!=null&&aggrigatorList.size()>0){
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList=service.getDistributerByAggId(reportBean.getAggId());
		if(distributorList!=null&&distributorList.size()>0){
			setDistributorList(distributorList);
		}
		
		Map<String, String> agentList=service.getAgentByDistId(reportBean.getDistId());
		if(agentList!=null&&agentList.size()>0){
			setAgentList(agentList);
		}
		
		Map<String, String> subAgentList=service.getSubAgentByAgentId(reportBean.getAgentId());
		if(subAgentList!=null&&subAgentList.size()>0){
			setSubagentList(subAgentList);
		}
		
		return "success";
	}
	
	
	public String commPlans(){
		generateCsrfToken();
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getAllPlans(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		statusList.put("1", "Active");
		statusList.put("0","Inactive");
		
		return "success";
	}
	
	
	public String showCommByPlanId(){
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getAllPlans(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		statusList.put("1", "Active");
		statusList.put("0","Inactive");
		
		if(reportBean==null||reportBean.getPlanId()==null||reportBean.getPlanId().isEmpty()){
			addActionError("Invalid request.");
			return "fail";
		}
		if(reportBean.getPlanId().equalsIgnoreCase("-1")){
			addActionError("Please select a Plan.");
			return "fail";
		}
//		if(reportBean.getTxnType().equalsIgnoreCase("-1")){
//			addActionError("Please select a transaction type.");
//			return "fail";
//		}
		
		List<CommPercViewMast> list=comService.showCommByPlanId(reportBean);
		CommPlanMaster cpMaster=comService.showPlan(reportBean);
		
		if(cpMaster==null||cpMaster.getPlanId()==null||cpMaster.getPlanType()==null){
			addActionError("Problem with fetchening result from server please try again.");
			return "fail";
		}
		setCommBean(cpMaster);
		request.setAttribute("cpList",list);
		return "success";
	}
	
	public String updatePlan(){
		String userAgent=request.getHeader("User-Agent");
		String ipAddress =  request.getRemoteAddr();
	if (ipAddress == null) {
		ipAddress = request.getHeader("X-FORWARDED-FOR");
	}
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getAllPlans(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		statusList.put("1", "Active");
		statusList.put("0","Inactive");
		
		List<CommPercViewMast> list=comService.showCommByPlanId(reportBean);
		//CommPlanMaster cpMaster=comService.showPlan(reportBean);
		request.setAttribute("cpList",list);
	   commBean=comService.updatePlan(commBean,ipAddress,userAgent);
	   if(commBean==null){
		   addActionError("Invalid request.");
		   return "fail";
	   }
	   if(commBean.getStatusCode().equalsIgnoreCase("1001")){
		   addActionError(prop.getProperty("111001"));
		   return "fail";
	   }
	   if(commBean.getStatusCode().equalsIgnoreCase("9003")){
		   addActionError(prop.getProperty("9003"));
		   return "fail";
	   }
	   if(commBean.getStatusCode().equalsIgnoreCase("1000")){
		   addActionError("Status updated successfully.");
		   return "success";
	   }
	   if(commBean.getStatusCode().equalsIgnoreCase("7000")){
		   addActionError(prop.getProperty("7000"));
		   return "fail";
	   }
	   else{
		   addActionError(prop.getProperty("7000"));
		   return "fail";
	   }
		
		
		
	}
	public String saveCommissionPlan(){
	
		User user=(User)session.get("User");		
		if(user.getUsertype()==99){
		if(reportBean.getAggId()==null){
			reportBean.setAggId("-1");
		}	
		if(reportBean.getDistId()==null){
			reportBean.setDistId("-1");
		}
		if(reportBean.getAgentId()==null){
			reportBean.setAgentId("-1");
		}
		if(reportBean.getSubagentId()==null){
			reportBean.setSubagentId("-1");
		}
		}
		if(user.getUsertype()==4){		
			
			reportBean.setAggId(user.getId());	
			if(reportBean.getDistId()==null){
				reportBean.setDistId("-1");	
			}
			if(reportBean.getAgentId()==null){
				reportBean.setAgentId("-1");
			}
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}
			
		}
		if(user.getUsertype()==3){
			reportBean.setAggId("agg");		
			reportBean.setDistId(user.getId());	
			if(reportBean.getAgentId()==null){
				reportBean.setAgentId("-1");
			}	
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}	
		}
		
		if(user.getUsertype()==2){
			reportBean.setAggId("agg");		
			reportBean.setDistId("dis");	
			reportBean.setAgentId(user.getId());
			if(reportBean.getSubagentId()==null){
				reportBean.setSubagentId("-1");
			}	
			
		}
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		
		Map<String, String> aggrigatorList=service.getAggreator();	
		if(aggrigatorList!=null&&aggrigatorList.size()>0){
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList=service.getDistributerByAggId(reportBean.getAggId());
		if(distributorList!=null&&distributorList.size()>0){
			setDistributorList(distributorList);
		}
		
		Map<String, String> agentList=service.getAgentByDistId(reportBean.getDistId());
		if(agentList!=null&&agentList.size()>0){
			setAgentList(agentList);
		}
		
		Map<String, String> subAgentList=service.getSubAgentByAgentId(reportBean.getAgentId());
		if(subAgentList!=null&&subAgentList.size()>0){
			setSubagentList(subAgentList);
		}
		
		if(reportBean==null||reportBean.getPlanId()==null||reportBean.getPlanId().equalsIgnoreCase("-1")){
			addActionError("Please select a Plan.");
			return "fail";
		}
		if(reportBean.getDistId().equalsIgnoreCase("-1")){
			addActionError("Please select a Distributor.");
			return "fail";
		}
		
		reportBean=comService.assignCommission(reportBean);
		if(reportBean!=null&&reportBean.getStatusCode().equalsIgnoreCase("1000")){
			addActionMessage("Plan successfully assigned.");
		}
		else{
			addActionError("An error has occurred please try again.");
		}
		return "success";
		
	}
	
	public String editShowCommByCommId(){
		CommInputBean cominBean=comService.editShowCommByCommId(cpmaster);
		setCpmaster(cominBean);		
		ckBean.setPlanId(cpmaster.getPlanid());
		ckBean.setTxnid(cpmaster.getTxnid());
		
		request.setAttribute("show","true");		
		return "success";
	}
		
	public String getCommissionByAgentId(){
		List<CommissionBean> list=comService.getCommissionByAgentId(assignCommission);
		if(list!=null)
		request.setAttribute("commList", list);
		return "success";
	}
	
	public String saveDefineCommission(){
		
		System.out.println(cBean);
		System.out.println("dfsfsd");
		
		
		CommRequest commRequest=new CommRequest();
		commRequest.setUserType(3);
		commRequest.setAgentid(agentId);
		commRequest.setList(cBean);
		
		String result=comService.assignCommission(commRequest);
		if(result!=null&&result.equalsIgnoreCase("success")){
			addActionMessage("Plan assigned successfully to "+agentId+".");
			request.setAttribute("status","success");
		}else{
			addActionError("Something went wrong please try again later.");
			request.setAttribute("status","fail");
		}
		
		return "success";
	}
}
