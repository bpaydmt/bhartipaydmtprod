package com.bhartipay.wallet.recharge.bean;



import java.io.Serializable;
/**
 * RechargeBean is bean class for all properties of recharge services or payments
 * @author Bhartipay
 *
 */
public class RechargeBean implements Serializable{

private static final long serialVersionUID = 1L;
private String walletId;
private String trxId;
private String rechargeType;
private int ammount;
private String serviceProvider;
private String region;
private String spResponse;
private String mobileNo;
private String stdCode;
private String accountNumber;
private String dateTime;
private String imei;
private String appName;
private String userMobile;
private String aggreatorid;



public String getAggreatorid() {
	return aggreatorid;
}
public void setAggreatorid(String aggreatorid) {
	this.aggreatorid = aggreatorid;
}
public String getUserMobile() {
	return userMobile;
}
public void setUserMobile(String userMobile) {
	this.userMobile = userMobile;
}
public String getAppName() {
	return appName;
}
public void setAppName(String appName) {
	this.appName = appName;
}
public String getImei() {
return imei;
}
public void setImei(String imei) {
this.imei = imei;
}

public String getDateTime() {
	return dateTime;
}
public void setDateTime(String dateTime) {
	this.dateTime = dateTime;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
public String getStdCode() {
	return stdCode;
}
public void setStdCode(String stdCode) {
	this.stdCode = stdCode;
}
public String getAccountNumber() {
	return accountNumber;
}
public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
}
public String getWalletId() {
	return walletId;
}
public void setWalletId(String walletId) {
	this.walletId = walletId;
}
public String getTrxId() {
	return trxId;
}
public void setTrxId(String trxId) {
	this.trxId = trxId;
}
public String getRechargeType() {
	return rechargeType;
}
public void setRechargeType(String rechargeType) {
	this.rechargeType = rechargeType;
}
public int getAmmount() {
	return ammount;
}
public void setAmmount(int ammount) {
	this.ammount = ammount;
}
public String getServiceProvider() {
	return serviceProvider;
}
public void setServiceProvider(String serviceProvider) {
	this.serviceProvider = serviceProvider;
}
public String getRegion() {
	return region;
}
public void setRegion(String region) {
	this.region = region;
}
public String getSpResponse() {
	return spResponse;
}
public void setSpResponse(String spResponse) {
	this.spResponse = spResponse;
}
}
