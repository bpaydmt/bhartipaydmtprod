package com.bhartipay.wallet.recharge.bean;


public class UserWishListBean {
	private int wishId;
	private String userId;
	private String walletId;
	private String rechargeNumber;
	private String rechargeType;
	private String rechargeCircle;
	private String rechargeOperator;
	private Double rechargeAmt;
	public String status;
	
	
	
	
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getWishId() {
		return wishId;
	}

	public void setWishId(int wishId) {
		this.wishId = wishId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getRechargeNumber() {
		return rechargeNumber;
	}

	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeCircle() {
		return rechargeCircle;
	}

	public void setRechargeCircle(String rechargeCircle) {
		this.rechargeCircle = rechargeCircle;
	}

	public String getRechargeOperator() {
		return rechargeOperator;
	}

	public void setRechargeOperator(String rechargeOperator) {
		this.rechargeOperator = rechargeOperator;
	}

	public Double getRechargeAmt() {
		return rechargeAmt;
	}

	public void setRechargeAmt(Double rechargeAmt) {
		this.rechargeAmt = rechargeAmt;
	}
	
	
	
	
	
	
}
