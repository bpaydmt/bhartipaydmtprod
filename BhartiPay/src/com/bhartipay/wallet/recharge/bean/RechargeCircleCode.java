package com.bhartipay.wallet.recharge.bean;


/**
 * RechargeCircleCode class is bean class for table recharge_circle_code
 * @author Bhartipay
 *
 */
public class RechargeCircleCode {
private int sno;
private String circleName;
private int circleCode;
public int getSno() {
	return sno;
}
public void setSno(int sno) {
	this.sno = sno;
}
public String getCircleName() {
	return circleName;
}
public void setCircleName(String circleName) {
	this.circleName = circleName;
}
public int getCircleCode() {
	return circleCode;
}
public void setCircleCode(int circleCode) {
	this.circleCode = circleCode;
}

}
