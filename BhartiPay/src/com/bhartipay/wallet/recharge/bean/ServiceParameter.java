package com.bhartipay.wallet.recharge.bean;



import java.io.Serializable;
/**
 * ServiceParameter class is bean class it is use for setting and getting paramter
 * which are required by recharge service
 * @author Bhartipay
 *
 */
public class ServiceParameter implements Serializable {
	private String serviceType;
	private String serviceOperator;
	private String mobileNo;
	private int amount;
	private String circle;
	private String stdCode;
	private String accountNumber;
public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getServiceOperator() {
		return serviceOperator;
	}
	public void setServiceOperator(String serviceOperator) {
		this.serviceOperator = serviceOperator;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getStdCode() {
		return stdCode;
	}
	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


}
