package com.bhartipay.wallet.recharge.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

	public class RechargeTxnBean {
		
	private String rechargeId;
	private String userId;
	private String walletId;
	private String rechargeNumber;
	private String rechargeType;
	private String rechargeCircle;
	private String rechargeOperator;
	private Double rechargeAmt;
	private Double coupanAmt;
	private String wallettxnStatus;
	private String status;
	private String other;
	private String txnAgent;
	private String ipIemi;
	private String rechargeDate;
	private String statusCode;
	private String stdCode;
	private String accountNumber;
	private String aggreatorid;
	private String operatorTxnId;
	private String operatorName;
	private String numberSeries;
	private String CHECKSUMHASH;
	private String mobileNumber;
	
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getNumberSeries() {
		return numberSeries;
	}

	public void setNumberSeries(String numberSeries) {
		this.numberSeries = numberSeries;
	}

	public String getOperatorTxnId() {
		return operatorTxnId;
	}

	public void setOperatorTxnId(String operatorTxnId) {
		this.operatorTxnId = operatorTxnId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getRechargeId() {
		return rechargeId;
	}

	public void setRechargeId(String rechargeId) {
		this.rechargeId = rechargeId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getRechargeNumber() {
		return rechargeNumber;
	}

	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeCircle() {
		return rechargeCircle;
	}

	public void setRechargeCircle(String rechargeCircle) {
		this.rechargeCircle = rechargeCircle;
	}

	public String getRechargeOperator() {
		return rechargeOperator;
	}

	public void setRechargeOperator(String rechargeOperator) {
		this.rechargeOperator = rechargeOperator;
	}

	public Double getRechargeAmt() {
		return rechargeAmt;
	}

	public void setRechargeAmt(Double rechargeAmt) {
		this.rechargeAmt = rechargeAmt;
	}

	public Double getCoupanAmt() {
		return coupanAmt;
	}

	public void setCoupanAmt(Double coupanAmt) {
		this.coupanAmt = coupanAmt;
	}



	public String getWallettxnStatus() {
		return wallettxnStatus;
	}

	public void setWallettxnStatus(String wallettxnStatus) {
		this.wallettxnStatus = wallettxnStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getTxnAgent() {
		return txnAgent;
	}

	public void setTxnAgent(String txnAgent) {
		this.txnAgent = txnAgent;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}

	public String getRechargeDate() {
		return rechargeDate;
	}

	public void setRechargeDate(String rechargeDate) {
		this.rechargeDate = rechargeDate;
	}

	
	
	
	
	
	
	

}
