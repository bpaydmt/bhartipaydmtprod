package com.bhartipay.wallet.recharge.action;

import java.io.InputStream;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;

import com.bhartipay.biller.bean.BillDetails;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.pg.PGDetails;
import com.bhartipay.wallet.framework.security.AES128Bit;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.Utils.ObjectFactory;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.recharge.service.impl.RechargeServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.CouponsBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;

import appnit.com.crypto.CheckSumHelper;

public class RechargeAction extends BaseAction implements ServletResponseAware{
	
	
public static Logger logger=Logger.getLogger(RechargeAction.class);


	UserService uService=new UserServiceImpl();
	RechargeTxnBean rechargeBean=new RechargeTxnBean();
	RechargeService rService=new RechargeServiceImpl();
	
	HashMap<String, String> prePaid = new HashMap<String, String>();
	HashMap<String, String> postPaid = new HashMap<String, String>();
	HashMap<String, String> dth = new HashMap<String, String>();
	HashMap<String, String> landLine = new HashMap<String, String>();
	HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
	HashMap<String, String> postPaidDataCard = new HashMap<String, String>();
	HashMap<String, String> circle = new HashMap<String, String>();
	HashMap<String, String> electricity = new HashMap<String, String>();
	HashMap<String, String> gas = new HashMap<String, String>();
	HashMap<String, String> insurance = new HashMap<String, String>();
	
	Map<String,String> couponCategory=new HashMap<String, String>();
	List<CouponsBean> couponList=new ArrayList<CouponsBean>();
	HttpServletResponse response=null;
	TransactionService tService=new TransactionServiceImpl();
	CommPlanMaster commBean=new CommPlanMaster();
	CommissionService commService=new CommissionServiceImpl();
	Map<String, String> plans=new HashMap<String, String>();
	Map<String, String> txnType=new HashMap<String, String>();
	private UserService service=new UserServiceImpl();
	ObjectFactory oFactory = new ObjectFactory();
	TxnInputBean inputBean=new TxnInputBean();
	BillDetails billDetails = new BillDetails();
	Properties prop =null;
	
	
	
	
	public BillDetails getBillDetails() {
		return billDetails;
	}



	public void setBillDetails(BillDetails billDetails) {
		this.billDetails = billDetails;
	}



	public RechargeAction()
	{
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
	

	
	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}

	public Map<String, String> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, String> plans) {
		this.plans = plans;
	}

	public Map<String, String> getTxnType() {
		return txnType;
	}

	public void setTxnType(Map<String, String> txnType) {
		this.txnType = txnType;
	}

	public Map<String, String> getCouponCategory() {
		return couponCategory;
	}

	public void setCouponCategory(Map<String, String> couponCategory) {
		this.couponCategory = couponCategory;
	}

	public List<CouponsBean> getCouponList() {
		return couponList;
	}

	public void setCouponList(List<CouponsBean> couponList) {
		this.couponList = couponList;
	}

	public HashMap<String, String> getPrePaid() {
		return prePaid;
	}

	public void setPrePaid(HashMap<String, String> prePaid) {
		this.prePaid = prePaid;
	}

	public HashMap<String, String> getPostPaid() {
		return postPaid;
	}

	public void setPostPaid(HashMap<String, String> postPaid) {
		this.postPaid = postPaid;
	}

	public HashMap<String, String> getDth() {
		return dth;
	}

	public void setDth(HashMap<String, String> dth) {
		this.dth = dth;
	}

	public HashMap<String, String> getLandLine() {
		return landLine;
	}

	public void setLandLine(HashMap<String, String> landLine) {
		this.landLine = landLine;
	}

	public HashMap<String, String> getPrePaidDataCard() {
		return prePaidDataCard;
	}

	public void setPrePaidDataCard(HashMap<String, String> prePaidDataCard) {
		this.prePaidDataCard = prePaidDataCard;
	}

	public HashMap<String, String> getPostPaidDataCard() {
		return postPaidDataCard;
	}

	public void setPostPaidDataCard(HashMap<String, String> postPaidDataCard) {
		this.postPaidDataCard = postPaidDataCard;
	}

	public HashMap<String, String> getCircle() {
		return circle;
	}

	public void setCircle(HashMap<String, String> circle) {
		this.circle = circle;
	}

	public RechargeTxnBean getRechargeBean() {
		return rechargeBean;
	}

	public void setRechargeBean(RechargeTxnBean rechargeBean) {
		this.rechargeBean = rechargeBean;
	}

	
	
	
	public HashMap<String, String> getElectricity() {
		return electricity;
	}



	public void setElectricity(HashMap<String, String> electricity) {
		this.electricity = electricity;
	}



	public HashMap<String, String> getGas() {
		return gas;
	}



	public void setGas(HashMap<String, String> gas) {
		this.gas = gas;
	}



	public HashMap<String, String> getInsurance() {
		return insurance;
	}



	public void setInsurance(HashMap<String, String> insurance) {
		this.insurance = insurance;
	}



	public String recharge(){	
		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		String billerType = (String)request.getParameter("billerType");
		 request.getSession().putValue("billerType", rechargeBean.getRechargeType());
	        session.put("billerType", rechargeBean.getRechargeType());
		logger.debug("******************calling recharge() action and return success***********************");
		Map<String, HashMap<String,String>> mapResult= rService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		

		if(mapResult!=null){
			try{
			setPrePaid(mapResult.get("PREPAID"));
			setPostPaid(mapResult.get("POSTPAID"));
			setDth(mapResult.get("DTH"));
			setLandLine(mapResult.get("LANDLINE"));
			setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
			setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
			setElectricity(mapResult.get("ELECTRICITY"));
			setGas(mapResult.get("GAS"));
			setInsurance(mapResult.get("INSURANCE"));
			setCircle(mapResult.get("CIRCLE"));
			}catch(Exception e){
				e.printStackTrace();
				addActionError("Oops! something went wronge.");
				return "fail";
			}
		}
		else
		{
			addActionError("Oops! something went wronge.");
			return "fail";
		}
		return "success";
		}else{
			return "error";
		}
	}
	
	public String getBillerDetails() {

		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		logger.debug("******************calling recharge() action and return success***********************");
//		RechargeTxnBean rechargeBean = new RechargeTxnBean();
		System.out.println(rechargeBean.getRechargeOperator());
		System.out.println(rechargeBean.getRechargeNumber());
		
		//rechargeBean.setRechargeOperator("UPPCL (URBAN) - UTTAR PRADESH");
		
		String result= rService.getBillerDetails(rechargeBean);
		JSONObject jsonObject = new JSONObject();
		try {
			org.json.JSONObject json = new org.json.JSONObject(result);
			response.setContentType("application/json");
			response.getWriter().println(json);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
		return null;
	}
	
	//**************developed By Ankit Budhiraja******************

	public String getPostPaidBillerDetails() {

		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		logger.debug("******************calling recharge() action and return success***********************");
//		RechargeTxnBean rechargeBean = new RechargeTxnBean();
		System.out.println(rechargeBean.getRechargeOperator());
		System.out.println(rechargeBean.getRechargeNumber());
		
		String result= rService.getPostPaidBillerDetails(rechargeBean);
		JSONObject jsonObject = new JSONObject();
		try {
			org.json.JSONObject json = new org.json.JSONObject(result);
			response.setContentType("application/json");
			response.getWriter().println(json);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
		return null;
	}

	
	public String getInsuranceBillerDetails() {

		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		logger.debug("******************calling recharge() action and return success***********************");
//		RechargeTxnBean rechargeBean = new RechargeTxnBean();
		System.out.println(rechargeBean.getRechargeOperator());
		System.out.println(rechargeBean.getRechargeNumber());
		
		String result= rService.getInsuranceBillerDetails(rechargeBean);
		JSONObject jsonObject = new JSONObject();
		try {
			org.json.JSONObject json = new org.json.JSONObject(result);
			response.setContentType("application/json");
			response.getWriter().println(json);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
		return null;
	}

		
	public String getGasBillerDetails() {

		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		logger.debug("******************calling recharge() action and return success***********************");
//		RechargeTxnBean rechargeBean = new RechargeTxnBean();
		System.out.println(rechargeBean.getRechargeOperator());
		System.out.println(rechargeBean.getRechargeNumber());
		
		String result= rService.getGasBillerDetails(rechargeBean);
		JSONObject jsonObject = new JSONObject();
		try {
			org.json.JSONObject json = new org.json.JSONObject(result);
			response.setContentType("application/json");
			response.getWriter().println(json);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
		return null;
	}
	
	//******************************End By Ankit Budhiraja**************************
	public String prepaidRecharge() {
		
		User user=(User)session.get("User");
		if(user==null||user.getUsertype()<=2){
		String billerType = (String)request.getParameter("billerType");
		 request.getSession().putValue("billerType", rechargeBean.getRechargeType());
	        session.put("billerType", rechargeBean.getRechargeType());
		logger.debug("******************calling recharge() action and return success***********************");
		Map<String, HashMap<String,String>> mapResult= rService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		

		if(mapResult!=null){
				try {

//					HashMap<String, String> prepaidRecharge = new HashMap<String, String>();
//					prepaidRecharge.put("BILAVAIRTEL001","AIRTEL");
//					prepaidRecharge.put("BILAVIDEA00001","Idea");
//					prepaidRecharge.put("BILAVJIO000001","Jio");
//					prepaidRecharge.put("BILAVMTNL00001","MTNL Mumbai");
//					prepaidRecharge.put("BILAVMTNL00002","MTNL Delhi");
//					prepaidRecharge.put("BILAVVODA00001","Vodafone");
//					setPrePaid(prepaidRecharge);
//					
//					LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
//					 map.put("Andhra Pradesh","Andhra Pradesh"); 
//					 map.put("Assam","Assam"); 
//					 map.put("Bihar & Jharkhand","Bihar & Jharkhand"); 
//					 map.put("Chennai","Chennai"); 
//					 map.put("Delhi","Delhi"); 
//					 map.put("Gujrat","Gujrat"); 
//					 map.put("Haryana","Haryana"); 
//					 map.put("Himachal Pradesh","Himachal Pradesh"); 
//					 map.put("J&K","J&K"); map.put("Karnataka","Karnataka"); 
//					 map.put("Kerala","Kerala"); 
//					 map.put("Kolkata","Kolkata"); 
//					 map.put("Madhya Pradesh","Madhya Pradesh"); 
//					 map.put("Maharashtra","Maharashtra"); 
//					 map.put("Mumbai","Mumbai"); 
//					 map.put("NorthEast","NorthEast"); 
//					 map.put("Orissa","Orissa"); 
//					 map.put("Punjab","Punjab"); 
//					 map.put("Rajasthan","Rajasthan"); 
//					 map.put("Tamilnadu","Tamilnadu"); 
//					 map.put("Uttar Pradesh (East)","Uttar Pradesh (East)"); 
//					 map.put("Uttar Pradesh(West)","Uttar Pradesh (West)"); 
//					 map.put("Uttaranchal","Uttaranchal"); 
//					 map.put("WestBengal & AN Island","WestBengal & AN Island");
					 
						setPrePaid(mapResult.get("PREPAID"));
						setPostPaid(mapResult.get("POSTPAID"));
						setDth(mapResult.get("DTH"));
						setLandLine(mapResult.get("LANDLINE"));
						setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
						setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
						setCircle(mapResult.get("CIRCLE"));
						setGas(mapResult.get("ELECTRICITY"));
						setInsurance(mapResult.get("GAS"));
						setCircle(mapResult.get("INSURANCE"));


				}catch(Exception e){
				e.printStackTrace();
				addActionError("Oops! something went wronge.");
				return "fail";
			}
		}
		else
		{
			addActionError("Oops! something went wronge.");
			return "fail";
		}
		return "success";
		}else{
			return "error";
		}
	
	}
	
	public String rechargeB2C(){	
		generateCsrfToken();
		String billerType = (String)request.getParameter("billerType");
		 request.getSession().putValue("billerType", rechargeBean.getRechargeType());
	        session.put("billerType", rechargeBean.getRechargeType());
		logger.debug("******************calling recharge() action and return success***********************");
		Map<String, HashMap<String,String>> mapResult= rService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		

		if(mapResult!=null){
			try{
				session.put("dth", mapResult.get("DTH"));
			setPrePaid(mapResult.get("PREPAID"));
			setPostPaid(mapResult.get("POSTPAID"));
			setDth(mapResult.get("DTH"));
			setLandLine(mapResult.get("LANDLINE"));
			setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
			setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
			setCircle(mapResult.get("CIRCLE"));
			setGas(mapResult.get("ELECTRICITY"));
			setInsurance(mapResult.get("GAS"));
			setCircle(mapResult.get("INSURANCE"));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return "success";
	}
	
	/*public static HashMap<String, String> convert(String str) {
	    String[] tokens = str.split(",");
	    HashMap<String, String> map = new HashMap<>();
	    for(int i=0; i<tokens.length-1; ){
	    	String nToken=tokens[i];
	    	String[] nt=nToken.split("=");
	    	map.put(nt[0],nt[1]);
	    }
	    return map;
	}*/
	
	public String walletToWallet(){
		generateCsrfToken();
		logger.debug("*********************************calling wallettowallet************************************");
		return "success";
	}

	
	public String sendWtoWMoney(){
		User user=(User)session.get("User");
		logger.debug("*********************************calling sendwtowmoney************************************");
		String aggId=(String)session.get("aggId");
		Map<String, HashMap<String,String>> mapResult= rService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		session.put("transferTo","toWallet");
		
		if(mapResult!=null){
			try{
			setPrePaid(mapResult.get("PREPAID"));
			setPostPaid(mapResult.get("POSTPAID"));
			setDth(mapResult.get("DTH"));
			setLandLine(mapResult.get("LANDLINE"));
			setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
			setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
			setCircle(mapResult.get("CIRCLE"));
			setGas(mapResult.get("ELECTRICITY"));
			setInsurance(mapResult.get("GAS"));
			setCircle(mapResult.get("INSURANCE"));
			}catch(Exception e){
				e.printStackTrace();
				//addActionError("Oops! something went wronge.");
				session.put("errorMsg", "Oops! something went wronge.");
				
				return "fail";
			}
		}
		else
		{
			//addActionError("Oops! something went wronge.");
			session.put("errorMsg", "Oops! something went wronge.");
			return "fail";
		}
		if(user == null || user.getId() == null)
		{
			request.getSession().putValue("WToWMoneyBean", inputBean);
			session.put("loginStatus", "fail");
			return "login";
		}
		if(inputBean == null || inputBean.getTrxAmount() == 0)
		{
			inputBean = (TxnInputBean)session.get("WToWMoneyBean");
		}
		session.remove("WToWMoneyBean");
		inputBean.setAggreatorid(aggId);
		//txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), imei,txnInputBean.getAggreatorid(),agent,txnInputBean.getCHECKSUMHASH()
		inputBean.setUserId(user.getId());
		inputBean.setWalletId(user.getWalletid());
		
		if(inputBean.getTrxAmount()==0){
			//addActionError("Amount can not be zero or blank.");
			session.put("errorMsg", "Amount can not be zero or blank.");
			return "fail";
		}
		if(inputBean.getMobileNo()==null||inputBean.getMobileNo().isEmpty()){
			//addActionError("Invalid mobile number.");
			session.put("errorMsg", "Invalid mobile number.");
			return "fail";
		}
		logger.debug("*********************************calling service wallettowallettransfer************************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		TreeMap<String,String> obj=new TreeMap<String,String>();
		obj.put("userId",user.getId());
        obj.put("walletId",user.getWalletid());
        obj.put("mobileNo",inputBean.getMobileNo());
        obj.put("trxAmount",""+inputBean.getTrxAmount());
        obj.put("ipImei",ipAddress);
        obj.put("aggreatorid",aggId);
        obj.put("token",user.getToken());
       try {
		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),obj);
		inputBean.setCHECKSUMHASH(checkSumString);
       } catch (Exception e) {
		e.printStackTrace();
       }
        //obj.put("CHECKSUMHASH",checkSum);
		
		String result=tService.walletToWalletTransfer(inputBean,ipAddress,userAgent);
		logger.debug("*********************************getting response from service  "+result+"************************************");
		if(result==null||result.isEmpty()){
			//addActionError("An error has occurred.");
			session.put("errorMsg", "An error has occurred.");
			return "fail";
		}
		if(result.equalsIgnoreCase("1001")){
			//addActionError(prop.getProperty("17000"));
			session.put("errorMsg",prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("1000")){
			inputBean.setMobileNo("");
			inputBean.setTrxAmount(0.0);
			addActionMessage("Successfully sent.");
			session.put("successMsg","Successfully sent.");
			
			logger.debug("*********************************calling service profilebyloginid************************************");
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			logger.debug("*********************************getting result from service "+resultMap+"************************************");

			updateWalletDetails(resultMap);
			return "success";
		}
		if(result.equalsIgnoreCase("7023")){
			session.put("errorMsg",prop.getProperty("7011"));
			//addActionError(prop.getProperty("7011"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			session.put("errorMsg",prop.getProperty("17014"));
			//addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			session.put("errorMsg",prop.getProperty("17017"));
			//addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			session.put("errorMsg",prop.getProperty("17018"));
			//addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			session.put("errorMsg",prop.getProperty("17019"));
			//addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			session.put("errorMsg",prop.getProperty("17020"));
			//addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			session.put("errorMsg",prop.getProperty("17021"));
			//addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			session.put("errorMsg",prop.getProperty("17022"));
			//addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			session.put("errorMsg",prop.getProperty("17045"));
			//addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			session.put("errorMsg",prop.getProperty("17046"));
			//addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			session.put("errorMsg",prop.getProperty("17047"));
			//addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			session.put("errorMsg",prop.getProperty("17000"));
			//addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			session.put("errorMsg",prop.getProperty("17023"));
			//addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			session.put("errorMsg",prop.getProperty("17024"));
			//addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			session.put("errorMsg",prop.getProperty("17042"));
			//addActionError(prop.getProperty("17042"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7036")){
			session.put("errorMsg",prop.getProperty("70361"));
			//addActionError(prop.getProperty("70361"));
			return "fail";
		}
		logger.debug("*********************************no condition matches return success by default***********************************");

		return "fail";
		
	}
	
	public String rechargeService(){
		
		System.out.println("________________________calling______rechargeService()_________________________");
		
		User user = (User)session.get("User");
		
		System.out.println("___________________________________User____________________"+user);
 
		session.remove("rechargeFlow");
		session.put("loginStatus", "");
		String aggId=(String)session.get("aggId");
		rechargeBean.setAggreatorid(aggId);
		
		System.out.println("____________________________________aggId____________________"+aggId);
		
		String userAgent;
		String ipAddress;
		//logger.debug("******************calling rechargeService() action **********************");
		System.out.println("==========================================================="+rechargeBean.getRechargeAmt());
	//	User user=(User)session.get("User");
		try
		{		
		if(rechargeBean != null)
		{
			if("PREPAID".equalsIgnoreCase(rechargeBean.getRechargeType())) {
				String op = rechargeBean.getRechargeOperator();
				String[] arr = op.split(",");
				rechargeBean.setRechargeOperator(arr[0]);
			  }
			String objGson = new Gson().toJson(rechargeBean);
			boolean flag = oFactory.checkScript(objGson);
			
			System.out.println("__________________flag____________________"+flag);
			if(!flag)
			{
				addActionError("Oops  something went wronge. Please try after sometime !!");
				return "fail";
			}
		}
		if(user==null||user.getId()==null||user.getId().isEmpty()||user.getWalletid()==null||user.getWalletid().isEmpty()){
			request.setAttribute("login","login");
			session.put("rechargeDetails",rechargeBean);
			session.put("loginStatus", "fail");
			return "login";
		}
		
		RechargeTxnBean rtb=(RechargeTxnBean)session.get("rechargeDetails");
		Double amt;
		
		if(rtb!=null&&rtb.getRechargeNumber()!=null&&!rtb.getRechargeNumber().isEmpty()){
			
			rechargeBean=rtb;
			if(rechargeBean != null)
			{
				String objGson = new Gson().toJson(rechargeBean);
				boolean flag = oFactory.checkScript(objGson);
				if(!flag)
				{
					addActionError("Oops  something went wronge. Please try after sometime !!");
					return "fail";
				}
			}
			rechargeBean.setUserId(user.getId());
			rechargeBean.setWalletId(user.getWalletid());
			
			System.out.println("userGetID_________"+user.getId());
			System.out.println("getWalletid_________"+user.getWalletid());

			session.remove("rechargeDetails");
		}else{
			rechargeBean.setUserId(user.getId());
			rechargeBean.setWalletId(user.getWalletid());
		}		
		if((user.getFinalBalance()-rechargeBean.getRechargeAmt()>=0)||(user.getCashBackfinalBalance()-rechargeBean.getRechargeAmt()>=0)){
			userAgent=request.getHeader("User-Agent");
			ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			/*
			if(rechargeBean.getRechargeAmt() > 3000 && rechargeBean.getRechargeType().toUpperCase().equals("DTH") ){
				session.put("rechargeError", "Amount Cannot be greater than 3000 Rs");
				return "recharge";
			}
			else if(rechargeBean.getRechargeAmt() > 1500 ){
				session.put("rechargeError", "Amount Cannot be greater than 1500 Rs");
				return "recharge";
			}
			*/
			TreeMap<String,String>  jsonObj=new TreeMap<String,String>(); 
			jsonObj.put("walletId",rechargeBean.getWalletId());
	        jsonObj.put("userId",rechargeBean.getUserId());
	        jsonObj.put("rechargeNumber",rechargeBean.getRechargeNumber());
	        jsonObj.put("rechargeType",rechargeBean.getRechargeType());
	        jsonObj.put("rechargeOperator",rechargeBean.getRechargeOperator());
	        jsonObj.put("rechargeCircle",rechargeBean.getRechargeCircle()!=null ? rechargeBean.getRechargeCircle():"NA");
	        jsonObj.put("rechargeAmt",""+rechargeBean.getRechargeAmt());
	        jsonObj.put("aggreatorid",rechargeBean.getAggreatorid());
	        jsonObj.put("token",user.getToken());
	        request.getSession().putValue("billerType", rechargeBean.getRechargeType());
	        session.put("billerType", rechargeBean.getRechargeType());
	        
	        System.out.println("Token_____________"+user.getToken());
	        System.out.println("TokenSecurity______"+user.getTokenSecurityKey());

	     
	        try {
	    		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),jsonObj);
	    		System.out.println("checkSumString__________________________"+checkSumString);
	    		rechargeBean.setCHECKSUMHASH(checkSumString);
	    		System.out.println("checkSumString__________________________"+checkSumString);

	           } catch (Exception e) {
	    		e.printStackTrace();
	           }
			
			
			rechargeBean=rService.goForPayment(rechargeBean,ipAddress,userAgent);
			if(rechargeBean.getStatusCode().equalsIgnoreCase("0"))
			{
			addActionMessage("Duplicate Transaction");
			return "dupRecharge";	
			}
			request.setAttribute("rechargeBean", rechargeBean);
			session.put("rechargeBean", rechargeBean);
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			updateWalletDetails(resultMap);
			
			Map<String,Object> mapObj=uService.getCouponsCategory();
			
			Map<String,String> couponCategory=(Map<String, String>)mapObj.get("category");
			List<CouponsBean> couponList=(List<CouponsBean>)mapObj.get("couponList");
			
			setCouponCategory(couponCategory);
			setCouponList(couponList);
			
			session.put("couponList",couponList);
			session.put("couponCategory", couponCategory);
			
			System.out.println("coupon category+++++++++++++++++++"+couponCategory);
			System.out.println("coupon list **********************"+couponList);
			logger.debug("******************rechargeService action completed and updatedWalletDetails**********************");
			
		}else{
			if(user.getUsertype() == 1)
			{
				userAgent=request.getHeader("User-Agent");
				ipAddress = request.getHeader("X-FORWARDED-FOR") ;
				session.put("rechargeFlow", "TRUE");
				amt = rechargeBean.getRechargeAmt() - user.getFinalBalance();
				String flag = paymentGatewayB2C(user,aggId,amt,userAgent,ipAddress);
				if(flag.equalsIgnoreCase("addMoneyb2c"))
				{
					return "addMoneyb2c";
				}
				return "successb2c";
			}
			DecimalFormat d=new DecimalFormat("0.00");
			logger.debug("******************balance low redirect to addmoney page.**********************");
			addActionMessage("Your wallet balance is low. Please add remaining "+user.getCountrycurrency()+" "+d.format((rechargeBean.getRechargeAmt()-user.getFinalBalance()))+" to your wallet first.");
			return "addMoney";
		}
		
		//if(reTxnBean.getStatusCode())
		if(user.getUsertype() == 1)
		{
			session.put("rechargeFlow", "TRUE");
			
			return "successb2c";
		}	
			
		return "success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "fail";
		}
	}
	
	
	
	public String prepaidRechargeService() {
		

		
		System.out.println("________________________calling______rechargeService()_________________________");
		
		User user = (User)session.get("User");
		
		System.out.println("___________________________________User____________________"+user);
 
		session.remove("rechargeFlow");
		session.put("loginStatus", "");
		String aggId=(String)session.get("aggId");
		rechargeBean.setAggreatorid(aggId);
		
		System.out.println("____________________________________aggId____________________"+aggId);
		
		String userAgent;
		String ipAddress;
		//logger.debug("******************calling rechargeService() action **********************");
		System.out.println("==========================================================="+rechargeBean.getRechargeAmt());
	//	User user=(User)session.get("User");
		try
		{		
		if(rechargeBean != null)
		{
			String objGson = new Gson().toJson(rechargeBean);
			boolean flag = oFactory.checkScript(objGson);
			
			System.out.println("__________________flag____________________"+flag);
			if(!flag)
			{
				addActionError("Oops  something went wronge. Please try after sometime !!");
				return "fail";
			}
		}
		if(user==null||user.getId()==null||user.getId().isEmpty()||user.getWalletid()==null||user.getWalletid().isEmpty()){
			request.setAttribute("login","login");
			session.put("rechargeDetails",rechargeBean);
			session.put("loginStatus", "fail");
			return "login";
		}
		
		RechargeTxnBean rtb=(RechargeTxnBean)session.get("rechargeDetails");
		Double amt;
		
		if(rtb!=null&&rtb.getRechargeNumber()!=null&&!rtb.getRechargeNumber().isEmpty()){
			
			rechargeBean=rtb;
			if(rechargeBean != null)
			{
				String objGson = new Gson().toJson(rechargeBean);
				boolean flag = oFactory.checkScript(objGson);
				if(!flag)
				{
					addActionError("Oops  something went wronge. Please try after sometime !!");
					return "fail";
				}
			}
			rechargeBean.setUserId(user.getId());
			rechargeBean.setWalletId(user.getWalletid());
			
			System.out.println("userGetID_________"+user.getId());
			System.out.println("getWalletid_________"+user.getWalletid());

			session.remove("rechargeDetails");
		}else{
			rechargeBean.setUserId(user.getId());
			rechargeBean.setWalletId(user.getWalletid());
		}		
		if((user.getFinalBalance()-rechargeBean.getRechargeAmt()>=0)||(user.getCashBackfinalBalance()-rechargeBean.getRechargeAmt()>=0)){
			userAgent=request.getHeader("User-Agent");
			ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
			if(rechargeBean.getRechargeAmt() > 3000 && rechargeBean.getRechargeType().toUpperCase().equals("DTH") ){
				session.put("rechargeError", "Amount Cannot be greater than 3000 Rs");
				return "recharge";
			}
			else if(rechargeBean.getRechargeAmt() > 1500 ){
				session.put("rechargeError", "Amount Cannot be greater than 1500 Rs");
				return "recharge";
			}
			TreeMap<String,String>  jsonObj=new TreeMap<String,String>(); 
			jsonObj.put("walletId",rechargeBean.getWalletId());
	        jsonObj.put("userId",rechargeBean.getUserId());
	        jsonObj.put("rechargeNumber",rechargeBean.getRechargeNumber());
	        jsonObj.put("rechargeType",rechargeBean.getRechargeType());
	        jsonObj.put("rechargeOperator",rechargeBean.getRechargeOperator());
	        jsonObj.put("rechargeCircle",rechargeBean.getRechargeCircle());
	        jsonObj.put("rechargeAmt",""+rechargeBean.getRechargeAmt());
	        jsonObj.put("aggreatorid",rechargeBean.getAggreatorid());
	        jsonObj.put("token",user.getToken());
	        request.getSession().putValue("billerType", rechargeBean.getRechargeType());
	        session.put("billerType", rechargeBean.getRechargeType());
	        
	        System.out.println("Token_____________"+user.getToken());
	        System.out.println("TokenSecurity______"+user.getTokenSecurityKey());

	     
	        try {
	    		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),jsonObj);
	    		System.out.println("checkSumString__________________________"+checkSumString);
	    		rechargeBean.setCHECKSUMHASH(checkSumString);
	    		System.out.println("checkSumString__________________________"+checkSumString);

	           } catch (Exception e) {
	    		e.printStackTrace();
	           }
			
			
			rechargeBean=rService.goForPayment(rechargeBean,ipAddress,userAgent);
			request.setAttribute("rechargeBean", rechargeBean);
			session.put("rechargeBean", rechargeBean);
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			updateWalletDetails(resultMap);
			
			Map<String,Object> mapObj=uService.getCouponsCategory();
			
			Map<String,String> couponCategory=(Map<String, String>)mapObj.get("category");
			List<CouponsBean> couponList=(List<CouponsBean>)mapObj.get("couponList");
			
			setCouponCategory(couponCategory);
			setCouponList(couponList);
			
			session.put("couponList",couponList);
			session.put("couponCategory", couponCategory);
			
			System.out.println("coupon category+++++++++++++++++++"+couponCategory);
			System.out.println("coupon list **********************"+couponList);
			logger.debug("******************rechargeService action completed and updatedWalletDetails**********************");
			
		}else{
			if(user.getUsertype() == 1)
			{
				userAgent=request.getHeader("User-Agent");
				ipAddress = request.getHeader("X-FORWARDED-FOR") ;
				session.put("rechargeFlow", "TRUE");
				amt = rechargeBean.getRechargeAmt() - user.getFinalBalance();
				String flag = paymentGatewayB2C(user,aggId,amt,userAgent,ipAddress);
				if(flag.equalsIgnoreCase("addMoneyb2c"))
				{
					return "addMoneyb2c";
				}
				return "successb2c";
			}
			DecimalFormat d=new DecimalFormat("0.00");
			logger.debug("******************balance low redirect to addmoney page.**********************");
			addActionMessage("Your wallet balance is low. Please add remaining "+user.getCountrycurrency()+" "+d.format((rechargeBean.getRechargeAmt()-user.getFinalBalance()))+" to your wallet first.");
			return "addMoney";
		}
		
		//if(reTxnBean.getStatusCode())
		if(user.getUsertype() == 1)
		{
			session.put("rechargeFlow", "TRUE");
			
			return "successb2c";
		}	
			
		return "success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "fail";
		}
	
		
//		System.out.println("________________________calling______rechargeService()_________________________");
//		User user = (User)session.get("User");
//		
//		String userAgent;
//		String ipAddress;
//		try
//		{		
//		if(rechargeBean != null)
//		{
//			String objGson = new Gson().toJson(rechargeBean);
//			boolean flag = oFactory.checkScript(objGson);
//			
//			System.out.println("__________________flag____________________"+flag);
//			if(!flag)
//			{
//				addActionError("Oops  something went wronge. Please try after sometime !!");
//				return "fail";
//			}
//		}
//		
//		
//		if(user==null||user.getId()==null||user.getId().isEmpty()||user.getWalletid()==null||user.getWalletid().isEmpty()){
//			request.setAttribute("login","login");
//			session.put("rechargeDetails",rechargeBean);
//			session.put("loginStatus", "fail");
//			return "login";
//		}
//		userAgent=request.getHeader("User-Agent");
//		ipAddress = request.getHeader("X-FORWARDED-FOR") ;	
//		BillInfoRequest billInfoRequest = new BillInfoRequest();
//		BillerInfoRequest info = new BillerInfoRequest();
//		info.setBillerId(rechargeBean.getRechargeOperator());
//		billInfoRequest.setBillerInfoRequest(info);
//		BillerInfoResponse billerInfoResponse = rService.getBillerInfo(billInfoRequest,ipAddress,userAgent);
//			
//		return "success";
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//			return "fail";
//		}
//	
//	
	
}
	
	public void updateWalletDetails(Map<String,String> resultMap){
		logger.debug("******************calling updateWalletDetails **********************");
		User user=(User)session.get("User");
		
		String userImg=uService.getProfilePic(resultMap.get("id"));
		user.setUserImg(userImg);
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setKycStatus(resultMap.get("kycStatus"));
		user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
		user.setWhiteLabel(Integer.parseInt(resultMap.get("whiteLabel")!=null?resultMap.get("whiteLabel"):"0"));
		user.setWallet(Integer.parseInt(resultMap.get("wallet")));
		user.setBank(Integer.parseInt(resultMap.get("bank")));
		user.setPortalMessage(resultMap.get("portalMessage")!=null?resultMap.get("portalMessage"):"");
		user.setAsCode(resultMap.get("asCode")!=null?resultMap.get("asCode"):"");
		user.setAgentCode(resultMap.get("agentCode")!=null?resultMap.get("agentCode"):"");
		user.setAsAgentCode(resultMap.get("asAgentCode")!=null?resultMap.get("asAgentCode"):"");
		user.setShopName(resultMap.get("shopName")!=null?resultMap.get("shopName"):"");
		user.setAepsChannel(resultMap.get("aepsChannel")!=null?resultMap.get("aepsChannel"):"");
		user.setBank1(resultMap.get("bank1")!=null?resultMap.get("bank1"):"1");
		user.setBank2(resultMap.get("bank2")!=null?resultMap.get("bank2"):"1");
		user.setBank3(resultMap.get("bank3")!=null?resultMap.get("bank3"):"1");
		user.setBank3_via(resultMap.get("bank3_via")!=null?resultMap.get("bank3_via"):"1");
		user.setOnlineMoney(resultMap.get("onlineMoney")!= null ? Integer.parseInt(String.valueOf(resultMap.get("onlineMoney")).substring(0, 1)) : Integer.parseInt("0"));
		user.setRecharge(resultMap.get("recharge")!= null ? Integer.parseInt(String.valueOf(resultMap.get("recharge")).substring(0, 1)) : Integer.parseInt("0") );
		user.setBbps(resultMap.get("bbps")!= null ? Integer.parseInt(String.valueOf(resultMap.get("bbps")).substring(0, 1)) : Integer.parseInt("0") );

		session.put("User",user);
//		session.put("serviceMaster",resultMap.get("serviceMaster"));
		logger.debug("******************updation successfully bye updateWalletDetails **********************");
	}
	
	public String rechargeInvoice(){
		Map<String,String> couponCategory=(Map<String,String>) session.get("couponCategory");
		setCouponCategory(couponCategory);
		return "success";
	}
	
	public String rechargeInvoiceB2C(){
		User user = (User)session.get("User");
		if(user == null || user.getId() == null)
		{
			session.put("loginStatus", "fail");
			return "login";
		}
		Map<String,String> couponCategory=(Map<String,String>) session.get("couponCategory");
		setCouponCategory(couponCategory);
		rechargeB2C();
		return "success";
	}
	
	public boolean generateCsrfToken(){
		try
		{
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

            session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
        }

        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

        session.put("csrfPreventionSalt", salt);
        return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	public String getOperatorCircle(){
		JSONObject jObject = new JSONObject();
		//rechargeBean.setNumberSeries(rechargeBean.getRechargeNumber());
	    String operatorCircle= rService.getOperatorCircle(rechargeBean);
	   
	    jObject.put("operator",operatorCircle.substring(0, operatorCircle.indexOf("|")));
	    jObject.put("circle",operatorCircle.substring(operatorCircle.indexOf("|")+1));
		
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}
	
	
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response = arg0;
	}
	

	public String paymentGatewayB2C(User user,String aggId,double amount,String userAgent,String ipAddress){		
		logger.debug("**********************  calling paymentGateway() **************************");
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			return "fail";
		}		
		
		if(amount <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+amount);
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		/*String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;*/
		if (ipAddress == null) {
			   ipAddress = "0.0.0.0";
		}
		
		String result=tService.savePGTrx(amount, user.getWalletid(), user.getMobileno(), "b2c customer","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError("There is some problem please try again.");
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid mobile no.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError("Transaction failed, Wallet balance violated.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError("Transaction fail.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid reciver mobile number.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError("Transaction failed due to insufficient balance.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PaymentGateway.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		WalletConfiguration config1=new WalletConfiguration();
		config1.setAggreatorid(aggId);
		
		
		Map<String,String> mapResult=service.getWalletConfigPG(config1);
		
		
		//Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
		String appName=mapResult.get("pgAgId");
		String userName=mapResult.get("pgAgId");
		String MID=mapResult.get("pgMid");
		
		String successURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/cPGRes.action";
		String	failURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/cPGRes.action";
		
		logger.debug("**********************  CallBackFailURL**************************"+successURL);
		logger.debug("**********************  failURL**************************"+failURL);
		
		String submitTo=mapResult.get("pgUrl");
		String securitykey=mapResult.get("pgEncKey");
//		String TxnType=mapResult.get("pgTxnType");
//		String country=mapResult.get("pgCountry");
//		String currency=mapResult.get("pgCurrency");
		//String Channel="WEB";
		
//		String operatingMode=prop.getProperty("operatingMode");
//		String otherDetails=prop.getProperty("otherDetails");
//		String collaborator=prop.getProperty("collaborator");
				
				
				String requestParameter="Appname="+appName+"|TransID="+txnId+"|Amount="+amount+"|Appuser="+userName+"|CallBackURL="+successURL+"|MID="+MID;
				PGDetails pgDetails=new PGDetails();
				String billingDtls = pgDetails.getBillingDtls();
logger.debug("***********************Before encryption parameters **********************************");				
logger.debug("request parameter for paymentgateway  "+requestParameter);
logger.debug("pgdetails    "+pgDetails);
logger.debug("billing details    "+billingDtls);

			    if(user.getEmailid()!=null&&user.getEmailid().length()>0){
			    	billingDtls=billingDtls.replace("sandeep.prajapati@appnittech.com",user.getEmailid());
			    }
			   /* if(user.getName()!=null&&user.getName().length()>0){
			    	billingDtls=billingDtls.replace("Appnit Technologies",user.getName());
			    }*/
			    if(user.getMobileno()!=null&&user.getMobileno().length()>0){
			    	billingDtls=billingDtls.replace("9335235731",user.getMobileno());
			    }
			    
				String shippingDtls = pgDetails.getShippingDtls();
				//String key = Encryption.getEncryptionKey("rechappPG");
				
				System.out.println(pgDetails.getBillingDtls());
				System.out.println(pgDetails.getShippingDtls());
				System.out.println(requestParameter);
				requestParameter = AES128Bit.encrypt(requestParameter, securitykey);
				billingDtls = AES128Bit.encrypt(billingDtls, securitykey);
				shippingDtls = AES128Bit.encrypt(shippingDtls, securitykey);
				requestParameter = requestParameter.replaceAll("\n", "");
				billingDtls = billingDtls.replaceAll("\n", "");
				shippingDtls = shippingDtls.replaceAll("\n", "");
				logger.debug("***********************after encryption parameters **********************************");				
				logger.debug("request parameter for paymentgateway  "+requestParameter);
				logger.debug("pgdetails    "+pgDetails);
				logger.debug("billing details    "+billingDtls);
				logger.debug("MID is  "+MID);
				request.setAttribute("requestparameter",requestParameter);
				request.setAttribute("billingDtls",billingDtls);
				request.setAttribute("shippingDtls",shippingDtls);
				request.setAttribute("MID",MID);
				request.setAttribute("submitTo",submitTo);
				
				
				
		return "addMoneyb2c";
		
	}
	
	
	public String pGatewayB2CAddMoney()
	{
		
		logger.debug("**********************  calling paymentGateway() **************************");
		
		User user=(User)session.get("User");
		Map<String, HashMap<String,String>> mapResult= rService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		

		if(mapResult!=null){
			try{
			setPrePaid(mapResult.get("PREPAID"));
			setPostPaid(mapResult.get("POSTPAID"));
			setDth(mapResult.get("DTH"));
			setLandLine(mapResult.get("LANDLINE"));
			setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
			setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
			setCircle(mapResult.get("CIRCLE"));
			}catch(Exception e){
				e.printStackTrace();
				addActionError("Oops! something went wronge.");
				session.put("cAddMnyErr", "Oops! something went wronge.");
				return "fail";
			}
		}
		else
		{
			session.put("cAddMnyErr", "Oops! something went wronge.");
			addActionError("Oops! something went wronge.");
			return "fail";
		}
		if(user == null || user.getId() == null)
		{
			session.put("inputBean", inputBean);
			session.put("loginStatus", "fail");
			return "fail";
		}
		if( inputBean.getTrxAmount() == 0)
		{
			inputBean = (TxnInputBean)session.get("inputBean");
		}
		String aggId=(String)session.get("aggId");
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			session.put("cAddMnyErr", "Please try again..");
			return "fail";
		}		
		
		if(inputBean.getTrxAmount() <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
			addActionError("Please Enter Valid Amount.");
			session.put("cAddMnyErr", "Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError("There is some problem please try again.");
			session.put("cAddMnyErr", "There is some problem please try again.");
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid mobile no.");
			session.put("cAddMnyErr", "Invalid mobile no.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError("Transaction failed, Wallet balance violated.");
			session.put("cAddMnyErr", "Transaction failed, Wallet balance violated.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError("Transaction fail.");
			session.put("cAddMnyErr", "Transaction fail.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid reciver mobile number.");
			session.put("cAddMnyErr", "Invalid reciver mobile number.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError("Transaction failed due to insufficient balance.");
			session.put("cAddMnyErr", "Transaction failed due to insufficient balance.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			session.put("cAddMnyErr", "Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PaymentGateway.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		WalletConfiguration config1=new WalletConfiguration();
		config1.setAggreatorid(aggId);
		
		
		Map<String,String> mapResult1=service.getWalletConfigPG(config1);
		
		
		//Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
		String appName=mapResult1.get("pgAgId");
		String userName=mapResult1.get("pgAgId");
		String MID=mapResult1.get("pgMid");
		
		String successURL=mapResult1.get("pgCallBackUrl")+"/BhartiPay/cPGRes.action";
		String	failURL=mapResult1.get("pgCallBackUrl")+"/BhartiPay/cPGRes.action";
		
		logger.debug("**********************  CallBackFailURL**************************"+successURL);
		logger.debug("**********************  failURL**************************"+failURL);
		
		String submitTo=mapResult1.get("pgUrl");
		String securitykey=mapResult1.get("pgEncKey");
//		String TxnType=mapResult.get("pgTxnType");
//		String country=mapResult.get("pgCountry");
//		String currency=mapResult.get("pgCurrency");
		//String Channel="WEB";
		
//		String operatingMode=prop.getProperty("operatingMode");
//		String otherDetails=prop.getProperty("otherDetails");
//		String collaborator=prop.getProperty("collaborator");
				
				
				String requestParameter="Appname="+appName+"|TransID="+txnId+"|Amount="+inputBean.getTrxAmount()+"|Appuser="+userName+"|CallBackURL="+successURL+"|MID="+MID;
				PGDetails pgDetails=new PGDetails();
				String billingDtls = pgDetails.getBillingDtls();
logger.debug("***********************Before encryption parameters **********************************");				
logger.debug("request parameter for paymentgateway  "+requestParameter);
logger.debug("pgdetails    "+pgDetails);
logger.debug("billing details    "+billingDtls);

			    if(user.getEmailid()!=null&&user.getEmailid().length()>0){
			    	billingDtls=billingDtls.replace("sandeep.prajapati@appnittech.com",user.getEmailid());
			    }
			   /* if(user.getName()!=null&&user.getName().length()>0){
			    	billingDtls=billingDtls.replace("Appnit Technologies",user.getName());
			    }*/
			    if(user.getMobileno()!=null&&user.getMobileno().length()>0){
			    	billingDtls=billingDtls.replace("9335235731",user.getMobileno());
			    }
			    
				String shippingDtls = pgDetails.getShippingDtls();
				//String key = Encryption.getEncryptionKey("rechappPG");
				
				System.out.println(pgDetails.getBillingDtls());
				System.out.println(pgDetails.getShippingDtls());
				System.out.println(requestParameter);
				requestParameter = AES128Bit.encrypt(requestParameter, securitykey);
				billingDtls = AES128Bit.encrypt(billingDtls, securitykey);
				shippingDtls = AES128Bit.encrypt(shippingDtls, securitykey);
				requestParameter = requestParameter.replaceAll("\n", "");
				billingDtls = billingDtls.replaceAll("\n", "");
				shippingDtls = shippingDtls.replaceAll("\n", "");
				logger.debug("***********************after encryption parameters **********************************");				
				logger.debug("request parameter for paymentgateway  "+requestParameter);
				logger.debug("pgdetails    "+pgDetails);
				logger.debug("billing details    "+billingDtls);
				logger.debug("MID is  "+MID);
				request.setAttribute("requestparameter",requestParameter);
				request.setAttribute("billingDtls",billingDtls);
				request.setAttribute("shippingDtls",shippingDtls);
				request.setAttribute("MID",MID);
				request.setAttribute("submitTo",submitTo);
				
				
				
		return "success";
		
	}
	
	
	public String pgResponseB2C() {
		logger.debug("********************************************response come from payment gateway********************************************");
		User user=null;
		StringBuffer mobileAppResponse = new StringBuffer();
		String responseparams = request.getParameter("responseparams");
		String hash=request.getParameter("hash");
		Properties paymentProp = null;

		String result = "fail";
		String msg="";

		Logger logger = Logger.getLogger(this.getClass().getName());
		logger.debug("********************************************** response come from Payment gateway = "
				+ responseparams);
		if (responseparams == null) {
			logger.debug("********************************************** getting resp Parameter as null so recharge could not be done   ****************************************");
			msg="Payment fialed.";
			return result;
		} else {

			paymentProp = new Properties();
			StringTokenizer stokz = new StringTokenizer(responseparams, "|");
			while (stokz.hasMoreTokens()) {
				String tok = (String) stokz.nextToken();
				paymentProp.put(tok.substring(0, tok.indexOf('=')),
						tok.substring(tok.indexOf('=') + 1));
			}
			String id=null;
			String trxId=null;
			String amount="0.0";
			String method = request.getMethod();
			if (method.equalsIgnoreCase("GET")) {
				logger.debug("********************************************** request coming from get method    ****************************************");
				msg="Payment fialed.";
				result = "fail";
				
			} else {
				logger.debug("********************************************response come from post method ********************************************");
				id = paymentProp.getProperty("plutusTxnId");
				String status = paymentProp.getProperty("status");
				String code = paymentProp.getProperty("responseCode");
				String otherDetails = paymentProp.getProperty("appName");
				trxId = paymentProp.getProperty("appTransId");
				amount = paymentProp.getProperty("amount");  
				
				if (status != null && status.contains("success")|| code.equals("01")) {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
					msg="Amount successfully added to your wallet.";
					result="success";
					//return "success";
				} else {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");

					msg="Payment failed.";
					result = "fail";
					//return "fail";
				}
				logger.debug("*******************************calling service profileBytxnid and getwalletconfig*************************************");

				Map<String, String> resultMap=uService.ProfilebytxnId(trxId);
				WalletConfiguration conf=new WalletConfiguration();
				if(Double.parseDouble(String.valueOf(resultMap.get("usertype")))==4){
				conf.setAggreatorid(resultMap.get("id"));
				}
				else{
					conf.setAggreatorid(resultMap.get("aggreatorid"));
				}
				Map<String,String> config=uService.getWalletConfig(conf);
				if(config==null||config.size()==0){
					logger.debug("*******************************getting wallet config as null******************************");
					msg="An error has occurred please try again.";
					return "fail";
				}
				logger.debug("**********************************creating user session*************************************");
				
				createUserSession(resultMap, config,conf.getAggreatorid());
			    user=(User)session.get("User");
			    logger.debug("**********************************calling service updatePGTrx*************************************");
				String updateResult=tService.updatePGTrx(hash,responseparams,user.getId(),trxId,id,Double.parseDouble(amount), result, "");
				logger.debug("********************************getting response from service ************************************"+updateResult);
				if(updateResult.equalsIgnoreCase("1001")){
				msg="Payment failed.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7029")){
				msg="Payment failed from payment gateway.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("1000")){
				msg="Amount successfully added";
				result="success";
				}
				if(updateResult.equalsIgnoreCase("7000")){
				msg="An error has occurred.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7015")){
				msg="Payment failed.";
				result="fail";
				}
			}
			if(result.equalsIgnoreCase("success")){
				if(user!=null&&user.getEmailid()!=null){
					UserSummary usummery=new UserSummary();
					usummery.setUserId(user.getEmailid());
					usummery.setAggreatorid(user.getAggreatorid());
				Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
				updateWalletDetails(resultMap);
				}
				addActionMessage(msg);
				session.put("cAddMnyMsg", msg);
			}
			else{
				if(user!=null&&user.getEmailid()!=null){
					UserSummary usummery=new UserSummary();
					usummery.setUserId(user.getEmailid());
					usummery.setAggreatorid(user.getAggreatorid());
				Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
				updateWalletDetails(resultMap);
				}
				addActionError(msg);
				session.put("cAddMnyErr", msg);
			}
			logger.debug("**********************************returning result *************************************"+result);
			return result;
		}
	

	}
	

	public void createUserSession(Map<String,String> resultMap,Map<String,String> config,String aggId){
		logger.debug("*********************************user session creating************************************");
		
		
		/*************************for menu options***************************/
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> res=commService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(res.get("PLAN"));
			setTxnType(res.get("TXNTYPE"));
		}
		
		session.put("menuMap", res.get("TXNTYPE"));
		session.put("aggId",aggId);
		/*************************end menu options***************************/
		
		
		User user=new User();
		
		String userImg=uService.getProfilePic(resultMap.get("id"));
		user.setUserImg(userImg);
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setAgentid(resultMap.get("agentid"));
		user.setSubAgentId(resultMap.get("subAgentId"));
		user.setDistributerid(resultMap.get("distributerid"));
		user.setSuperdistributerid(resultMap.get("superdistributerid")!=null?resultMap.get("superdistributerid"):"-1");
		user.setAggreatorid(resultMap.get("aggreatorid"));
		
		user.setCountry(config.get("country"));
		user.setCountrycurrency(config.get("countrycurrency"));
		user.setCountryid(Double.parseDouble(String.valueOf(config.get("countryid"))));
		user.setEmailvalid(Double.parseDouble(String.valueOf(config.get("emailvalid"))));
		user.setKycvalid(Double.parseDouble(String.valueOf(config.get("kycvalid"))));
		user.setOtpsendtomail(Double.parseDouble(String.valueOf(config.get("otpsendtomail"))));
		user.setSmssend(Double.parseDouble(String.valueOf(config.get("smssend"))));
		user.setStatus(Double.parseDouble(String.valueOf(config.get("status"))));
		
		user.setLogo(config.get("logopic"));
		user.setCustomerCare(config.get("customerCare")!=null?config.get("customerCare"):"0120-4000004");
		user.setSupportEmailId(config.get("supportEmailId")!=null?config.get("supportEmailId"):"");
		user.setBanner(config.get("bannerpic"));
		session.put("logo",config.get("logopic"));
		session.put("banner",config.get("bannerpic"));
		
		session.put("User",user);
		session.put("mapResult",config);
		logger.debug("*********************************user session created successfully************************************");
	}
 
	
	public String getPanAndAdhar()
	{
		User user=(User)session.get("User");
		WalletMastBean mast=new WalletMastBean();
		
		TreeMap<String,String>  jsonObj=new TreeMap<String,String>();
        jsonObj.put("userId",rechargeBean.getUserId());
        jsonObj.put("aggreatorid",rechargeBean.getAggreatorid());
        jsonObj.put("token",user.getToken()); 
        try {
    		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),jsonObj);
    		rechargeBean.setCHECKSUMHASH(checkSumString);
    		
           } catch (Exception e) {
    		e.printStackTrace();
           }
        
        String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		WalletMastBean panAdhar = rService.getPanAndAdhar(rechargeBean,ipAddress,userAgent);
		request.setAttribute("panAdhar", panAdhar);
		
		
		
	return "success";
	}
	
	 
	public String pendingRechargeTxn()
	{
	  User user=(User)session.get("User");
	   
	  String s = rechargeBean.getRechargeId();
	  System.out.println("TxnId "+s);
	  List<RechargeTxnBean> list = rService.markPending(rechargeBean.getRechargeId());
	  
	  request.setAttribute("markPending", list);
	   
	 return "success";
	}
	
	 
	
	public String pendingDmtTxn()
	{
		  User user=(User)session.get("User");
		  
		  String s = rechargeBean.getRechargeId();
		  System.out.println("TxnId "+s);
		  List<MudraMoneyTransactionBean> list = rService.markDmtPending(rechargeBean.getRechargeId());
		  
		  request.setAttribute("markDmtPending", list);
		
	  return "success";
	}
	
	public String rechargeMaintainance()
	{
		
		return "success";
	}
	
	public String enablePg()
	{
	  User user=(User)session.get("User");
	  String status = rechargeBean.getStatus();
	  String txnId = rechargeBean.getRechargeId();
	  System.out.println("TxnId "+txnId);
	  List<WalletMastBean> list = rService.enablePg(rechargeBean);
	  rechargeBean=null;
	  request.setAttribute("markPending", list);
	   
	 return "success";
	}
	
	
}
