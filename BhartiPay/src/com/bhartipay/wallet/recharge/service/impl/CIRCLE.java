
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class CIRCLE {

    @SerializedName("UTTAR PRADESH (W) & UTTARAKHAND  ")
    @Expose
    private String uTTARPRADESHWUTTARAKHAND;
    @SerializedName("KERALA")
    @Expose
    private String kERALA;
    @SerializedName("BIHAR & JHARKHAND")
    @Expose
    private String bIHARJHARKHAND;
    @SerializedName("Delhi")
    @Expose
    private String delhi;
    @SerializedName("MADHYA PRADESH & CHHATISGARH")
    @Expose
    private String mADHYAPRADESHCHHATISGARH;
    @SerializedName("MAHARASHTRA")
    @Expose
    private String mAHARASHTRA;
    @SerializedName("MUMBAI")
    @Expose
    private String mUMBAI;
    @SerializedName("UTTAR PRADESH (E)")
    @Expose
    private String uTTARPRADESHE;
    @SerializedName("HIMACHAL PRADESH")
    @Expose
    private String hIMACHALPRADESH;
    @SerializedName("RAJASTHAN")
    @Expose
    private String rAJASTHAN;
    @SerializedName("PUNJAB")
    @Expose
    private String pUNJAB;
    @SerializedName("HARYANA")
    @Expose
    private String hARYANA;
    @SerializedName("Jharkhand")
    @Expose
    private String jharkhand;
    @SerializedName("WEST BENGAL")
    @Expose
    private String wESTBENGAL;
    @SerializedName("TAMILNADU")
    @Expose
    private String tAMILNADU;
    @SerializedName("ORISSA")
    @Expose
    private String oRISSA;
    @SerializedName("ANDHRA PRADESH")
    @Expose
    private String aNDHRAPRADESH;
    @SerializedName("ASSAM")
    @Expose
    private String aSSAM;
    @SerializedName("CHENNAI")
    @Expose
    private String cHENNAI;
    @SerializedName("NORTH EAST")
    @Expose
    private String nORTHEAST;
    @SerializedName("GUJARAT")
    @Expose
    private String gUJARAT;
    @SerializedName("KOLKATA")
    @Expose
    private String kOLKATA;
    @SerializedName("JAMMU & KASHMIR")
    @Expose
    private String jAMMUKASHMIR;
    @SerializedName("KARNATAKA")
    @Expose
    private String kARNATAKA;

    /**
     * 
     * @return
     *     The uTTARPRADESHWUTTARAKHAND
     */
    public String getUTTARPRADESHWUTTARAKHAND() {
        return uTTARPRADESHWUTTARAKHAND;
    }

    /**
     * 
     * @param uTTARPRADESHWUTTARAKHAND
     *     The UTTAR PRADESH (W) & UTTARAKHAND  
     */
    public void setUTTARPRADESHWUTTARAKHAND(String uTTARPRADESHWUTTARAKHAND) {
        this.uTTARPRADESHWUTTARAKHAND = uTTARPRADESHWUTTARAKHAND;
    }

    /**
     * 
     * @return
     *     The kERALA
     */
    public String getKERALA() {
        return kERALA;
    }

    /**
     * 
     * @param kERALA
     *     The KERALA
     */
    public void setKERALA(String kERALA) {
        this.kERALA = kERALA;
    }

    /**
     * 
     * @return
     *     The bIHARJHARKHAND
     */
    public String getBIHARJHARKHAND() {
        return bIHARJHARKHAND;
    }

    /**
     * 
     * @param bIHARJHARKHAND
     *     The BIHAR & JHARKHAND
     */
    public void setBIHARJHARKHAND(String bIHARJHARKHAND) {
        this.bIHARJHARKHAND = bIHARJHARKHAND;
    }

    /**
     * 
     * @return
     *     The delhi
     */
    public String getDelhi() {
        return delhi;
    }

    /**
     * 
     * @param delhi
     *     The Delhi
     */
    public void setDelhi(String delhi) {
        this.delhi = delhi;
    }

    /**
     * 
     * @return
     *     The mADHYAPRADESHCHHATISGARH
     */
    public String getMADHYAPRADESHCHHATISGARH() {
        return mADHYAPRADESHCHHATISGARH;
    }

    /**
     * 
     * @param mADHYAPRADESHCHHATISGARH
     *     The MADHYA PRADESH & CHHATISGARH
     */
    public void setMADHYAPRADESHCHHATISGARH(String mADHYAPRADESHCHHATISGARH) {
        this.mADHYAPRADESHCHHATISGARH = mADHYAPRADESHCHHATISGARH;
    }

    /**
     * 
     * @return
     *     The mAHARASHTRA
     */
    public String getMAHARASHTRA() {
        return mAHARASHTRA;
    }

    /**
     * 
     * @param mAHARASHTRA
     *     The MAHARASHTRA
     */
    public void setMAHARASHTRA(String mAHARASHTRA) {
        this.mAHARASHTRA = mAHARASHTRA;
    }

    /**
     * 
     * @return
     *     The mUMBAI
     */
    public String getMUMBAI() {
        return mUMBAI;
    }

    /**
     * 
     * @param mUMBAI
     *     The MUMBAI
     */
    public void setMUMBAI(String mUMBAI) {
        this.mUMBAI = mUMBAI;
    }

    /**
     * 
     * @return
     *     The uTTARPRADESHE
     */
    public String getUTTARPRADESHE() {
        return uTTARPRADESHE;
    }

    /**
     * 
     * @param uTTARPRADESHE
     *     The UTTAR PRADESH (E)
     */
    public void setUTTARPRADESHE(String uTTARPRADESHE) {
        this.uTTARPRADESHE = uTTARPRADESHE;
    }

    /**
     * 
     * @return
     *     The hIMACHALPRADESH
     */
    public String getHIMACHALPRADESH() {
        return hIMACHALPRADESH;
    }

    /**
     * 
     * @param hIMACHALPRADESH
     *     The HIMACHAL PRADESH
     */
    public void setHIMACHALPRADESH(String hIMACHALPRADESH) {
        this.hIMACHALPRADESH = hIMACHALPRADESH;
    }

    /**
     * 
     * @return
     *     The rAJASTHAN
     */
    public String getRAJASTHAN() {
        return rAJASTHAN;
    }

    /**
     * 
     * @param rAJASTHAN
     *     The RAJASTHAN
     */
    public void setRAJASTHAN(String rAJASTHAN) {
        this.rAJASTHAN = rAJASTHAN;
    }

    /**
     * 
     * @return
     *     The pUNJAB
     */
    public String getPUNJAB() {
        return pUNJAB;
    }

    /**
     * 
     * @param pUNJAB
     *     The PUNJAB
     */
    public void setPUNJAB(String pUNJAB) {
        this.pUNJAB = pUNJAB;
    }

    /**
     * 
     * @return
     *     The hARYANA
     */
    public String getHARYANA() {
        return hARYANA;
    }

    /**
     * 
     * @param hARYANA
     *     The HARYANA
     */
    public void setHARYANA(String hARYANA) {
        this.hARYANA = hARYANA;
    }

    /**
     * 
     * @return
     *     The jharkhand
     */
    public String getJharkhand() {
        return jharkhand;
    }

    /**
     * 
     * @param jharkhand
     *     The Jharkhand
     */
    public void setJharkhand(String jharkhand) {
        this.jharkhand = jharkhand;
    }

    /**
     * 
     * @return
     *     The wESTBENGAL
     */
    public String getWESTBENGAL() {
        return wESTBENGAL;
    }

    /**
     * 
     * @param wESTBENGAL
     *     The WEST BENGAL
     */
    public void setWESTBENGAL(String wESTBENGAL) {
        this.wESTBENGAL = wESTBENGAL;
    }

    /**
     * 
     * @return
     *     The tAMILNADU
     */
    public String getTAMILNADU() {
        return tAMILNADU;
    }

    /**
     * 
     * @param tAMILNADU
     *     The TAMILNADU
     */
    public void setTAMILNADU(String tAMILNADU) {
        this.tAMILNADU = tAMILNADU;
    }

    /**
     * 
     * @return
     *     The oRISSA
     */
    public String getORISSA() {
        return oRISSA;
    }

    /**
     * 
     * @param oRISSA
     *     The ORISSA
     */
    public void setORISSA(String oRISSA) {
        this.oRISSA = oRISSA;
    }

    /**
     * 
     * @return
     *     The aNDHRAPRADESH
     */
    public String getANDHRAPRADESH() {
        return aNDHRAPRADESH;
    }

    /**
     * 
     * @param aNDHRAPRADESH
     *     The ANDHRA PRADESH
     */
    public void setANDHRAPRADESH(String aNDHRAPRADESH) {
        this.aNDHRAPRADESH = aNDHRAPRADESH;
    }

    /**
     * 
     * @return
     *     The aSSAM
     */
    public String getASSAM() {
        return aSSAM;
    }

    /**
     * 
     * @param aSSAM
     *     The ASSAM
     */
    public void setASSAM(String aSSAM) {
        this.aSSAM = aSSAM;
    }

    /**
     * 
     * @return
     *     The cHENNAI
     */
    public String getCHENNAI() {
        return cHENNAI;
    }

    /**
     * 
     * @param cHENNAI
     *     The CHENNAI
     */
    public void setCHENNAI(String cHENNAI) {
        this.cHENNAI = cHENNAI;
    }

    /**
     * 
     * @return
     *     The nORTHEAST
     */
    public String getNORTHEAST() {
        return nORTHEAST;
    }

    /**
     * 
     * @param nORTHEAST
     *     The NORTH EAST
     */
    public void setNORTHEAST(String nORTHEAST) {
        this.nORTHEAST = nORTHEAST;
    }

    /**
     * 
     * @return
     *     The gUJARAT
     */
    public String getGUJARAT() {
        return gUJARAT;
    }

    /**
     * 
     * @param gUJARAT
     *     The GUJARAT
     */
    public void setGUJARAT(String gUJARAT) {
        this.gUJARAT = gUJARAT;
    }

    /**
     * 
     * @return
     *     The kOLKATA
     */
    public String getKOLKATA() {
        return kOLKATA;
    }

    /**
     * 
     * @param kOLKATA
     *     The KOLKATA
     */
    public void setKOLKATA(String kOLKATA) {
        this.kOLKATA = kOLKATA;
    }

    /**
     * 
     * @return
     *     The jAMMUKASHMIR
     */
    public String getJAMMUKASHMIR() {
        return jAMMUKASHMIR;
    }

    /**
     * 
     * @param jAMMUKASHMIR
     *     The JAMMU & KASHMIR
     */
    public void setJAMMUKASHMIR(String jAMMUKASHMIR) {
        this.jAMMUKASHMIR = jAMMUKASHMIR;
    }

    /**
     * 
     * @return
     *     The kARNATAKA
     */
    public String getKARNATAKA() {
        return kARNATAKA;
    }

    /**
     * 
     * @param kARNATAKA
     *     The KARNATAKA
     */
    public void setKARNATAKA(String kARNATAKA) {
        this.kARNATAKA = kARNATAKA;
    }

}
