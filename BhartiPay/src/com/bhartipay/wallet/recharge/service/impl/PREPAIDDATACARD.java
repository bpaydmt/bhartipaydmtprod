
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PREPAIDDATACARD {

    @SerializedName("Vodafone")
    @Expose
    private String vodafone;
    @SerializedName("Reliance GSM")
    @Expose
    private String relianceGSM;
    @SerializedName("Tata Photon")
    @Expose
    private String tataPhoton;
    @SerializedName("Idea")
    @Expose
    private String idea;
    @SerializedName("MTNL")
    @Expose
    private String mTNL;
    @SerializedName("Tata Indicom")
    @Expose
    private String tataIndicom;
    @SerializedName("MTS Mbrowse")
    @Expose
    private String mTSMbrowse;
    @SerializedName("Aircel")
    @Expose
    private String aircel;
    @SerializedName("Airtel")
    @Expose
    private String airtel;
    @SerializedName("Reliance Netconnect")
    @Expose
    private String relianceNetconnect;
    @SerializedName("MTS Mblaze")
    @Expose
    private String mTSMblaze;
    @SerializedName("Tata Docomo")
    @Expose
    private String tataDocomo;
    @SerializedName("BSNL")
    @Expose
    private String bSNL;

    /**
     * 
     * @return
     *     The vodafone
     */
    public String getVodafone() {
        return vodafone;
    }

    /**
     * 
     * @param vodafone
     *     The Vodafone
     */
    public void setVodafone(String vodafone) {
        this.vodafone = vodafone;
    }

    /**
     * 
     * @return
     *     The relianceGSM
     */
    public String getRelianceGSM() {
        return relianceGSM;
    }

    /**
     * 
     * @param relianceGSM
     *     The Reliance GSM
     */
    public void setRelianceGSM(String relianceGSM) {
        this.relianceGSM = relianceGSM;
    }

    /**
     * 
     * @return
     *     The tataPhoton
     */
    public String getTataPhoton() {
        return tataPhoton;
    }

    /**
     * 
     * @param tataPhoton
     *     The Tata Photon
     */
    public void setTataPhoton(String tataPhoton) {
        this.tataPhoton = tataPhoton;
    }

    /**
     * 
     * @return
     *     The idea
     */
    public String getIdea() {
        return idea;
    }

    /**
     * 
     * @param idea
     *     The Idea
     */
    public void setIdea(String idea) {
        this.idea = idea;
    }

    /**
     * 
     * @return
     *     The mTNL
     */
    public String getMTNL() {
        return mTNL;
    }

    /**
     * 
     * @param mTNL
     *     The MTNL
     */
    public void setMTNL(String mTNL) {
        this.mTNL = mTNL;
    }

    /**
     * 
     * @return
     *     The tataIndicom
     */
    public String getTataIndicom() {
        return tataIndicom;
    }

    /**
     * 
     * @param tataIndicom
     *     The Tata Indicom
     */
    public void setTataIndicom(String tataIndicom) {
        this.tataIndicom = tataIndicom;
    }

    /**
     * 
     * @return
     *     The mTSMbrowse
     */
    public String getMTSMbrowse() {
        return mTSMbrowse;
    }

    /**
     * 
     * @param mTSMbrowse
     *     The MTS Mbrowse
     */
    public void setMTSMbrowse(String mTSMbrowse) {
        this.mTSMbrowse = mTSMbrowse;
    }

    /**
     * 
     * @return
     *     The aircel
     */
    public String getAircel() {
        return aircel;
    }

    /**
     * 
     * @param aircel
     *     The Aircel
     */
    public void setAircel(String aircel) {
        this.aircel = aircel;
    }

    /**
     * 
     * @return
     *     The airtel
     */
    public String getAirtel() {
        return airtel;
    }

    /**
     * 
     * @param airtel
     *     The Airtel
     */
    public void setAirtel(String airtel) {
        this.airtel = airtel;
    }

    /**
     * 
     * @return
     *     The relianceNetconnect
     */
    public String getRelianceNetconnect() {
        return relianceNetconnect;
    }

    /**
     * 
     * @param relianceNetconnect
     *     The Reliance Netconnect
     */
    public void setRelianceNetconnect(String relianceNetconnect) {
        this.relianceNetconnect = relianceNetconnect;
    }

    /**
     * 
     * @return
     *     The mTSMblaze
     */
    public String getMTSMblaze() {
        return mTSMblaze;
    }

    /**
     * 
     * @param mTSMblaze
     *     The MTS Mblaze
     */
    public void setMTSMblaze(String mTSMblaze) {
        this.mTSMblaze = mTSMblaze;
    }

    /**
     * 
     * @return
     *     The tataDocomo
     */
    public String getTataDocomo() {
        return tataDocomo;
    }

    /**
     * 
     * @param tataDocomo
     *     The Tata Docomo
     */
    public void setTataDocomo(String tataDocomo) {
        this.tataDocomo = tataDocomo;
    }

    /**
     * 
     * @return
     *     The bSNL
     */
    public String getBSNL() {
        return bSNL;
    }

    /**
     * 
     * @param bSNL
     *     The BSNL
     */
    public void setBSNL(String bSNL) {
        this.bSNL = bSNL;
    }

}
