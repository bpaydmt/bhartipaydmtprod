
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PREPAID {

    @SerializedName("TATA WALKY")
    @Expose
    private String tATAWALKY;
    @SerializedName("RELIANCE CDMA")
    @Expose
    private String rELIANCECDMA;
    @SerializedName("UNINOR")
    @Expose
    private String uNINOR;
    @SerializedName("VODAFONE")
    @Expose
    private String vODAFONE;
    @SerializedName("MTS")
    @Expose
    private String mTS;
    @SerializedName("VIDEOCON")
    @Expose
    private String vIDEOCON;
    @SerializedName("IDEA")
    @Expose
    private String iDEA;
    @SerializedName("MTNL")
    @Expose
    private String mTNL;
    @SerializedName("AIRCEL")
    @Expose
    private String aIRCEL;
    @SerializedName("RELIANCE")
    @Expose
    private String rELIANCE;
    @SerializedName("BSNL")
    @Expose
    private String bSNL;
    @SerializedName("VIRGIN CDMA")
    @Expose
    private String vIRGINCDMA;
    @SerializedName("VIRGIN GSM")
    @Expose
    private String vIRGINGSM;
    @SerializedName("AIRTEL")
    @Expose
    private String aIRTEL;
    @SerializedName("TATA DOCOMO")
    @Expose
    private String tATADOCOMO;

    /**
     * 
     * @return
     *     The tATAWALKY
     */
    public String getTATAWALKY() {
        return tATAWALKY;
    }

    /**
     * 
     * @param tATAWALKY
     *     The TATA WALKY
     */
    public void setTATAWALKY(String tATAWALKY) {
        this.tATAWALKY = tATAWALKY;
    }

    /**
     * 
     * @return
     *     The rELIANCECDMA
     */
    public String getRELIANCECDMA() {
        return rELIANCECDMA;
    }

    /**
     * 
     * @param rELIANCECDMA
     *     The RELIANCE CDMA
     */
    public void setRELIANCECDMA(String rELIANCECDMA) {
        this.rELIANCECDMA = rELIANCECDMA;
    }

    /**
     * 
     * @return
     *     The uNINOR
     */
    public String getUNINOR() {
        return uNINOR;
    }

    /**
     * 
     * @param uNINOR
     *     The UNINOR
     */
    public void setUNINOR(String uNINOR) {
        this.uNINOR = uNINOR;
    }

    /**
     * 
     * @return
     *     The vODAFONE
     */
    public String getVODAFONE() {
        return vODAFONE;
    }

    /**
     * 
     * @param vODAFONE
     *     The VODAFONE
     */
    public void setVODAFONE(String vODAFONE) {
        this.vODAFONE = vODAFONE;
    }

    /**
     * 
     * @return
     *     The mTS
     */
    public String getMTS() {
        return mTS;
    }

    /**
     * 
     * @param mTS
     *     The MTS
     */
    public void setMTS(String mTS) {
        this.mTS = mTS;
    }

    /**
     * 
     * @return
     *     The vIDEOCON
     */
    public String getVIDEOCON() {
        return vIDEOCON;
    }

    /**
     * 
     * @param vIDEOCON
     *     The VIDEOCON
     */
    public void setVIDEOCON(String vIDEOCON) {
        this.vIDEOCON = vIDEOCON;
    }

    /**
     * 
     * @return
     *     The iDEA
     */
    public String getIDEA() {
        return iDEA;
    }

    /**
     * 
     * @param iDEA
     *     The IDEA
     */
    public void setIDEA(String iDEA) {
        this.iDEA = iDEA;
    }

    /**
     * 
     * @return
     *     The mTNL
     */
    public String getMTNL() {
        return mTNL;
    }

    /**
     * 
     * @param mTNL
     *     The MTNL
     */
    public void setMTNL(String mTNL) {
        this.mTNL = mTNL;
    }

    /**
     * 
     * @return
     *     The aIRCEL
     */
    public String getAIRCEL() {
        return aIRCEL;
    }

    /**
     * 
     * @param aIRCEL
     *     The AIRCEL
     */
    public void setAIRCEL(String aIRCEL) {
        this.aIRCEL = aIRCEL;
    }

    /**
     * 
     * @return
     *     The rELIANCE
     */
    public String getRELIANCE() {
        return rELIANCE;
    }

    /**
     * 
     * @param rELIANCE
     *     The RELIANCE
     */
    public void setRELIANCE(String rELIANCE) {
        this.rELIANCE = rELIANCE;
    }

    /**
     * 
     * @return
     *     The bSNL
     */
    public String getBSNL() {
        return bSNL;
    }

    /**
     * 
     * @param bSNL
     *     The BSNL
     */
    public void setBSNL(String bSNL) {
        this.bSNL = bSNL;
    }

    /**
     * 
     * @return
     *     The vIRGINCDMA
     */
    public String getVIRGINCDMA() {
        return vIRGINCDMA;
    }

    /**
     * 
     * @param vIRGINCDMA
     *     The VIRGIN CDMA
     */
    public void setVIRGINCDMA(String vIRGINCDMA) {
        this.vIRGINCDMA = vIRGINCDMA;
    }

    /**
     * 
     * @return
     *     The vIRGINGSM
     */
    public String getVIRGINGSM() {
        return vIRGINGSM;
    }

    /**
     * 
     * @param vIRGINGSM
     *     The VIRGIN GSM
     */
    public void setVIRGINGSM(String vIRGINGSM) {
        this.vIRGINGSM = vIRGINGSM;
    }

    /**
     * 
     * @return
     *     The aIRTEL
     */
    public String getAIRTEL() {
        return aIRTEL;
    }

    /**
     * 
     * @param aIRTEL
     *     The AIRTEL
     */
    public void setAIRTEL(String aIRTEL) {
        this.aIRTEL = aIRTEL;
    }

    /**
     * 
     * @return
     *     The tATADOCOMO
     */
    public String getTATADOCOMO() {
        return tATADOCOMO;
    }

    /**
     * 
     * @param tATADOCOMO
     *     The TATA DOCOMO
     */
    public void setTATADOCOMO(String tATADOCOMO) {
        this.tATADOCOMO = tATADOCOMO;
    }

}
