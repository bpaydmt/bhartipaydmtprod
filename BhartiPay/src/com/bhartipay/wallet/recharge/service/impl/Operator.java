
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Operator {

    @SerializedName("DTH")
    @Expose
    private DTH dTH;
    @SerializedName("POSTPAIDDATACARD")
    @Expose
    private POSTPAIDDATACARD pOSTPAIDDATACARD;
    @SerializedName("POSTPAID")
    @Expose
    private POSTPAID pOSTPAID;
    @SerializedName("LANDLINE")
    @Expose
    private LANDLINE lANDLINE;
    @SerializedName("PREPAID")
    @Expose
    private PREPAID pREPAID;
    @SerializedName("CIRCLE")
    @Expose
    private CIRCLE cIRCLE;
    @SerializedName("PREPAIDDATACARD")
    @Expose
    private PREPAIDDATACARD pREPAIDDATACARD;

    /**
     * 
     * @return
     *     The dTH
     */
    public DTH getDTH() {
        return dTH;
    }

    /**
     * 
     * @param dTH
     *     The DTH
     */
    public void setDTH(DTH dTH) {
        this.dTH = dTH;
    }

    /**
     * 
     * @return
     *     The pOSTPAIDDATACARD
     */
    public POSTPAIDDATACARD getPOSTPAIDDATACARD() {
        return pOSTPAIDDATACARD;
    }

    /**
     * 
     * @param pOSTPAIDDATACARD
     *     The POSTPAIDDATACARD
     */
    public void setPOSTPAIDDATACARD(POSTPAIDDATACARD pOSTPAIDDATACARD) {
        this.pOSTPAIDDATACARD = pOSTPAIDDATACARD;
    }

    /**
     * 
     * @return
     *     The pOSTPAID
     */
    public POSTPAID getPOSTPAID() {
        return pOSTPAID;
    }

    /**
     * 
     * @param pOSTPAID
     *     The POSTPAID
     */
    public void setPOSTPAID(POSTPAID pOSTPAID) {
        this.pOSTPAID = pOSTPAID;
    }

    /**
     * 
     * @return
     *     The lANDLINE
     */
    public LANDLINE getLANDLINE() {
        return lANDLINE;
    }

    /**
     * 
     * @param lANDLINE
     *     The LANDLINE
     */
    public void setLANDLINE(LANDLINE lANDLINE) {
        this.lANDLINE = lANDLINE;
    }

    /**
     * 
     * @return
     *     The pREPAID
     */
    public PREPAID getPREPAID() {
        return pREPAID;
    }

    /**
     * 
     * @param pREPAID
     *     The PREPAID
     */
    public void setPREPAID(PREPAID pREPAID) {
        this.pREPAID = pREPAID;
    }

    /**
     * 
     * @return
     *     The cIRCLE
     */
    public CIRCLE getCIRCLE() {
        return cIRCLE;
    }

    /**
     * 
     * @param cIRCLE
     *     The CIRCLE
     */
    public void setCIRCLE(CIRCLE cIRCLE) {
        this.cIRCLE = cIRCLE;
    }

    /**
     * 
     * @return
     *     The pREPAIDDATACARD
     */
    public PREPAIDDATACARD getPREPAIDDATACARD() {
        return pREPAIDDATACARD;
    }

    /**
     * 
     * @param pREPAIDDATACARD
     *     The PREPAIDDATACARD
     */
    public void setPREPAIDDATACARD(PREPAIDDATACARD pREPAIDDATACARD) {
        this.pREPAIDDATACARD = pREPAIDDATACARD;
    }

}
