
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class POSTPAID {

    @SerializedName("RELIANCE CDMA")
    @Expose
    private String rELIANCECDMA;
    @SerializedName("IDEA")
    @Expose
    private String iDEA;
    @SerializedName("AIRCEL")
    @Expose
    private String aIRCEL;
    @SerializedName("TATA INDICOM")
    @Expose
    private String tATAINDICOM;
    @SerializedName("RELIANCE")
    @Expose
    private String rELIANCE;
    @SerializedName("VODAFONE")
    @Expose
    private String vODAFONE;
    @SerializedName("AIRTEL")
    @Expose
    private String aIRTEL;
    @SerializedName("TATA DOCOMO")
    @Expose
    private String tATADOCOMO;

    /**
     * 
     * @return
     *     The rELIANCECDMA
     */
    public String getRELIANCECDMA() {
        return rELIANCECDMA;
    }

    /**
     * 
     * @param rELIANCECDMA
     *     The RELIANCE CDMA
     */
    public void setRELIANCECDMA(String rELIANCECDMA) {
        this.rELIANCECDMA = rELIANCECDMA;
    }

    /**
     * 
     * @return
     *     The iDEA
     */
    public String getIDEA() {
        return iDEA;
    }

    /**
     * 
     * @param iDEA
     *     The IDEA
     */
    public void setIDEA(String iDEA) {
        this.iDEA = iDEA;
    }

    /**
     * 
     * @return
     *     The aIRCEL
     */
    public String getAIRCEL() {
        return aIRCEL;
    }

    /**
     * 
     * @param aIRCEL
     *     The AIRCEL
     */
    public void setAIRCEL(String aIRCEL) {
        this.aIRCEL = aIRCEL;
    }

    /**
     * 
     * @return
     *     The tATAINDICOM
     */
    public String getTATAINDICOM() {
        return tATAINDICOM;
    }

    /**
     * 
     * @param tATAINDICOM
     *     The TATA INDICOM
     */
    public void setTATAINDICOM(String tATAINDICOM) {
        this.tATAINDICOM = tATAINDICOM;
    }

    /**
     * 
     * @return
     *     The rELIANCE
     */
    public String getRELIANCE() {
        return rELIANCE;
    }

    /**
     * 
     * @param rELIANCE
     *     The RELIANCE
     */
    public void setRELIANCE(String rELIANCE) {
        this.rELIANCE = rELIANCE;
    }

    /**
     * 
     * @return
     *     The vODAFONE
     */
    public String getVODAFONE() {
        return vODAFONE;
    }

    /**
     * 
     * @param vODAFONE
     *     The VODAFONE
     */
    public void setVODAFONE(String vODAFONE) {
        this.vODAFONE = vODAFONE;
    }

    /**
     * 
     * @return
     *     The aIRTEL
     */
    public String getAIRTEL() {
        return aIRTEL;
    }

    /**
     * 
     * @param aIRTEL
     *     The AIRTEL
     */
    public void setAIRTEL(String aIRTEL) {
        this.aIRTEL = aIRTEL;
    }

    /**
     * 
     * @return
     *     The tATADOCOMO
     */
    public String getTATADOCOMO() {
        return tATADOCOMO;
    }

    /**
     * 
     * @param tATADOCOMO
     *     The TATA DOCOMO
     */
    public void setTATADOCOMO(String tATADOCOMO) {
        this.tATADOCOMO = tATADOCOMO;
    }

}
