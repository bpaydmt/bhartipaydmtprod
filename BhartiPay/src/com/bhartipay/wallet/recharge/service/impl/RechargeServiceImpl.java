package com.bhartipay.wallet.recharge.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RechargeServiceImpl implements RechargeService {

	public static Logger logger=Logger.getLogger(RechargeServiceImpl.class);
	@Override
	public RechargeTxnBean goForPayment(RechargeTxnBean rechargeBean,String ipimei,String userAgent) {
		//logger.debug("***********************calling goforpayment service***************************");
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(rechargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/goForPayment");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).
				header("IPIMEI",ipimei).
				header("AGENT", userAgent).
				post(ClientResponse.class,jsonText);
		
	//	logger.debug("***********************getting response from goforpayment service***************************");
		
		if (response.getStatus() != 200)
		{
			//	logger.debug("***********************getting response code from goforpayment service***************************"+response.getStatus());

			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		//logger.debug("***********************getting response from goforpayment service***************************"+response);

		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);

		RechargeTxnBean rechargeBean1 = gson.fromJson(output, RechargeTxnBean.class);
		
		return rechargeBean1;
	}
	
	@Override
	public BillerInfoResponse getBillerInfo(BillInfoRequest billInfo,String ipimei,String userAgent) {

		//logger.debug("***********************calling goforpayment service***************************");
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(billInfo);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getBillerInfo");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).
				header("IPIMEI",ipimei).
				header("AGENT", userAgent).
				post(ClientResponse.class,jsonText);
		
		if (response.getStatus() != 200)
		{
			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);

		BillerInfoResponse rechargeBean1 = gson.fromJson(output, BillerInfoResponse.class);
		
		return rechargeBean1;
	
	}
	
	@Override
	public Map<String, HashMap<String, String>> getRechargeOperator() {
		Gson gson = new Gson();
		logger.info("************start calling getRechargeOperator*******");
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getRechargeOperator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, MyMap.class);
		return myMap;
	}
	
	public String getBillerDetails(RechargeTxnBean rechargeTxnBean) {

		Gson gson = new Gson();
		String requestJson = gson.toJson(rechargeTxnBean);
		logger.info("************start calling getBillerDetails*******");
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getBillerDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,requestJson);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
//		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, MyMap.class);
		return output;
	
	}
	public String getPostPaidBillerDetails(RechargeTxnBean rechargeTxnBean) {

		Gson gson = new Gson();
		String requestJson = gson.toJson(rechargeTxnBean);
		logger.info("************start calling getPostPaidBillerDetails*******");
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getPostPaidBillerDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,requestJson);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
//		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, MyMap.class);
		return output;
	
	}

	public String getInsuranceBillerDetails(RechargeTxnBean rechargeTxnBean) {

		Gson gson = new Gson();
		String requestJson = gson.toJson(rechargeTxnBean);
		logger.info("************start calling getPostPaidBillerDetails*******");
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getInsuranceBillerDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,requestJson);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
//		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, MyMap.class);
		return output;
	
	}

	
	public String getGasBillerDetails(RechargeTxnBean rechargeTxnBean) {

		Gson gson = new Gson();
		String requestJson = gson.toJson(rechargeTxnBean);
		logger.info("************start calling getPostPaidBillerDetails*******");
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getGasBillerDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,requestJson);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
//		Map<String,HashMap<String,String>> myMap = gson.fromJson(jo, MyMap.class);
		return output;
	
	}

	
	public String getOperatorCircle(RechargeTxnBean rechargeBean) {
		logger.debug("***********************calling getOperatorCircle service***************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(rechargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getOperatorCircle");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("***********************getting response from getOperatorCircle service***************************");
		
		if (response.getStatus() != 200) {
			logger.debug("***********************getting response code from getOperatorCircle service***************************"+response.getStatus());

			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("***********************getting response from getOperatorCircle service***************************"+response);

		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);

			return output;
	}
	
	 

	@Override
	public WalletMastBean getPanAndAdhar(RechargeTxnBean rechargeBean, String ipimei, String userAgent) {

		Gson gson = new Gson();
		String jsonText=gson.toJson(rechargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUser/getPanAndAdhar");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).
				header("IPIMEI",ipimei).
				header("AGENT", userAgent).
				post(ClientResponse.class,jsonText);
			
		if (response.getStatus() != 200)
		{
			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);
        WalletMastBean mast = gson.fromJson(output, WalletMastBean.class);
		
		return mast;
		
	}
	
	
	@Override
	public List<RechargeTxnBean> markPending(String str) {

		Gson gson = new Gson();
		 
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/markPending");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,str);
			
		if (response.getStatus() != 200)
		{
			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);
         PendingRc mast = gson.fromJson(output, PendingRc.class);
		
		return mast;
		
	}

	@Override
	public List<MudraMoneyTransactionBean> markDmtPending(String str) {

		Gson gson = new Gson();
		 
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/markDmtPending");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,str);
			
		if (response.getStatus() != 200)
		{
			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);
        PendingDmt mast = gson.fromJson(output, PendingDmt.class);
		
		return mast;
		
	}
	
	@Override
	public List<WalletMastBean> enablePg(RechargeTxnBean str) {

		Gson gson = new Gson();
		String jsonText=gson.toJson(str);
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/enablePg");
		ClientResponse response = webResource.
				type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
			
		if (response.getStatus() != 200)
		{
			   throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		logger.debug("*******************output sent***************************"+output);
		PgEnable mast = gson.fromJson(output, PgEnable.class);
		
		return mast;
		
	}
	

}
class MyMap extends HashMap<String,HashMap<String,String>>{
	
}

class PendingRc extends ArrayList<RechargeTxnBean>
{

}

class PendingDmt extends ArrayList<MudraMoneyTransactionBean>
{

}

class PgEnable extends ArrayList<WalletMastBean>
{

}
