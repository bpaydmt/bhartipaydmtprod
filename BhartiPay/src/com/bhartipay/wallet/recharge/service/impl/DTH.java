
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DTH {

    @SerializedName("Dish TV")
    @Expose
    private String dishTV;
    @SerializedName("Videocon D2h")
    @Expose
    private String videoconD2h;
    @SerializedName("Tata Sky")
    @Expose
    private String tataSky;
    @SerializedName("Airtel Digital TV")
    @Expose
    private String airtelDigitalTV;
    @SerializedName("Sun Direct")
    @Expose
    private String sunDirect;
    @SerializedName("Reliance Digital TV")
    @Expose
    private String relianceDigitalTV;

    /**
     * 
     * @return
     *     The dishTV
     */
    public String getDishTV() {
        return dishTV;
    }

    /**
     * 
     * @param dishTV
     *     The Dish TV
     */
    public void setDishTV(String dishTV) {
        this.dishTV = dishTV;
    }

    /**
     * 
     * @return
     *     The videoconD2h
     */
    public String getVideoconD2h() {
        return videoconD2h;
    }

    /**
     * 
     * @param videoconD2h
     *     The Videocon D2h
     */
    public void setVideoconD2h(String videoconD2h) {
        this.videoconD2h = videoconD2h;
    }

    /**
     * 
     * @return
     *     The tataSky
     */
    public String getTataSky() {
        return tataSky;
    }

    /**
     * 
     * @param tataSky
     *     The Tata Sky
     */
    public void setTataSky(String tataSky) {
        this.tataSky = tataSky;
    }

    /**
     * 
     * @return
     *     The airtelDigitalTV
     */
    public String getAirtelDigitalTV() {
        return airtelDigitalTV;
    }

    /**
     * 
     * @param airtelDigitalTV
     *     The Airtel Digital TV
     */
    public void setAirtelDigitalTV(String airtelDigitalTV) {
        this.airtelDigitalTV = airtelDigitalTV;
    }

    /**
     * 
     * @return
     *     The sunDirect
     */
    public String getSunDirect() {
        return sunDirect;
    }

    /**
     * 
     * @param sunDirect
     *     The Sun Direct
     */
    public void setSunDirect(String sunDirect) {
        this.sunDirect = sunDirect;
    }

    /**
     * 
     * @return
     *     The relianceDigitalTV
     */
    public String getRelianceDigitalTV() {
        return relianceDigitalTV;
    }

    /**
     * 
     * @param relianceDigitalTV
     *     The Reliance Digital TV
     */
    public void setRelianceDigitalTV(String relianceDigitalTV) {
        this.relianceDigitalTV = relianceDigitalTV;
    }

}
