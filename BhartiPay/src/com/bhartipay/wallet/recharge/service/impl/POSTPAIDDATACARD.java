
package com.bhartipay.wallet.recharge.service.impl;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class POSTPAIDDATACARD {

    @SerializedName("Aircel")
    @Expose
    private String aircel;
    @SerializedName("Tata Indicom")
    @Expose
    private String tataIndicom;
    @SerializedName("Airtel")
    @Expose
    private String airtel;
    @SerializedName("Reliance Netconnect")
    @Expose
    private String relianceNetconnect;
    @SerializedName("Tata Docomo")
    @Expose
    private String tataDocomo;
    @SerializedName("Reliance GSM")
    @Expose
    private String relianceGSM;
    @SerializedName("Vodafoe")
    @Expose
    private String vodafoe;
    @SerializedName("Idea")
    @Expose
    private String idea;

    /**
     * 
     * @return
     *     The aircel
     */
    public String getAircel() {
        return aircel;
    }

    /**
     * 
     * @param aircel
     *     The Aircel
     */
    public void setAircel(String aircel) {
        this.aircel = aircel;
    }

    /**
     * 
     * @return
     *     The tataIndicom
     */
    public String getTataIndicom() {
        return tataIndicom;
    }

    /**
     * 
     * @param tataIndicom
     *     The Tata Indicom
     */
    public void setTataIndicom(String tataIndicom) {
        this.tataIndicom = tataIndicom;
    }

    /**
     * 
     * @return
     *     The airtel
     */
    public String getAirtel() {
        return airtel;
    }

    /**
     * 
     * @param airtel
     *     The Airtel
     */
    public void setAirtel(String airtel) {
        this.airtel = airtel;
    }

    /**
     * 
     * @return
     *     The relianceNetconnect
     */
    public String getRelianceNetconnect() {
        return relianceNetconnect;
    }

    /**
     * 
     * @param relianceNetconnect
     *     The Reliance Netconnect
     */
    public void setRelianceNetconnect(String relianceNetconnect) {
        this.relianceNetconnect = relianceNetconnect;
    }

    /**
     * 
     * @return
     *     The tataDocomo
     */
    public String getTataDocomo() {
        return tataDocomo;
    }

    /**
     * 
     * @param tataDocomo
     *     The Tata Docomo
     */
    public void setTataDocomo(String tataDocomo) {
        this.tataDocomo = tataDocomo;
    }

    /**
     * 
     * @return
     *     The relianceGSM
     */
    public String getRelianceGSM() {
        return relianceGSM;
    }

    /**
     * 
     * @param relianceGSM
     *     The Reliance GSM
     */
    public void setRelianceGSM(String relianceGSM) {
        this.relianceGSM = relianceGSM;
    }

    /**
     * 
     * @return
     *     The vodafoe
     */
    public String getVodafoe() {
        return vodafoe;
    }

    /**
     * 
     * @param vodafoe
     *     The Vodafoe
     */
    public void setVodafoe(String vodafoe) {
        this.vodafoe = vodafoe;
    }

    /**
     * 
     * @return
     *     The idea
     */
    public String getIdea() {
        return idea;
    }

    /**
     * 
     * @param idea
     *     The Idea
     */
    public void setIdea(String idea) {
        this.idea = idea;
    }

}
