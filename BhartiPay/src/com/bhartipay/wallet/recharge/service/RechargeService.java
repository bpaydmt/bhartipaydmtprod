package com.bhartipay.wallet.recharge.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhartipay.recharge.request.BillInfoRequest;
import com.bhartipay.recharge.response.BillerInfoResponse;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;

public interface RechargeService {

	/**
	 * 
	 * @return
	 */
	public RechargeTxnBean goForPayment(RechargeTxnBean rechargeBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @return
	 */
	public BillerInfoResponse getBillerInfo(BillInfoRequest rechargeBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @return
	 */
	public Map<String,HashMap<String,String>> getRechargeOperator();
	
	/**
	 * 
	 * @return
	 */
	public String getBillerDetails(RechargeTxnBean rechargeTxnBean);
	
	/**
	 * 
	 * @param rechargeBean
	 * @return
	 */
	public String getOperatorCircle(RechargeTxnBean rechargeBean);

	public String getPostPaidBillerDetails(RechargeTxnBean rechargeBean);

	public String getGasBillerDetails(RechargeTxnBean rechargeBean);

	public String getInsuranceBillerDetails(RechargeTxnBean rechargeBean);
	public WalletMastBean getPanAndAdhar(RechargeTxnBean rechargeBean,String ipimei,String userAgent);
	
	public List<RechargeTxnBean> markPending(String str);
	public List<MudraMoneyTransactionBean> markDmtPending(String str);
	public List<WalletMastBean> enablePg(RechargeTxnBean txn );
	
}
