package com.bhartipay.wallet.report.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.bhartipay.aeps.AepsTokenRequest;
import com.bhartipay.bbps.vo.BbpsPayment;
import com.bhartipay.lean.LeanAccount;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.recharge.bean.RechargeBean;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.report.bean.AgentBalDetailResponse;
import com.bhartipay.wallet.report.bean.AgentBalDetailsBean;
import com.bhartipay.wallet.report.bean.AgentClosingBalBean;
import com.bhartipay.wallet.report.bean.AgentCurrentSummary;
import com.bhartipay.wallet.report.bean.AgentDetailsBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.CommSummaryBean;
import com.bhartipay.wallet.report.bean.ObjectTOList;
import com.bhartipay.wallet.report.bean.PartnerLedgerBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.SMSSendDetails;
import com.bhartipay.wallet.report.bean.SenderClosingBalBean;
import com.bhartipay.wallet.report.bean.SupportTicketBean;
import com.bhartipay.wallet.report.bean.TravelTxn;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.report.bean.WalletHistoryBean;
import com.bhartipay.wallet.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.wallet.report.service.ReportService;
import com.bhartipay.wallet.transaction.persistence.vo.JsonTOList;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.PaymentGatewayTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletHistoryTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ReportServiceImpl implements ReportService{
	public static Logger logger=Logger.getLogger(ReportServiceImpl.class);
	@Override
	public List<WalletToBankTxnMast> wallettoBankReport(ReportBean reportBean) {
		logger.debug("*****************calling method  walettobankreport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service wallettobankreport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/wallettoBankReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
		logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<WalletToBankTxnMast> list = gson.fromJson(output,ObjectTOList.class);
		System.out.println(list);
		for(WalletToBankTxnMast pb:list){
			System.out.println(pb.getWtbtxnid());
			System.out.println(pb.getPayeename());
		}
		return list;
	}

	@Override
	public List<RechargeTxnBean> rechargeReport(ReportBean reportBean) {
		logger.debug("*****************calling method  rechargeReport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service rechargeReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/rechargeReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<RechargeTxnBean> list = gson.fromJson(output,OTOList.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<BbpsPayment> bbpsReport(TxnInputBean inputBean) {
		logger.debug("*****************calling method  bbpsReport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		logger.debug("*****************json text as parameter to calling service bbpsReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/bbpsReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<BbpsPayment> list = gson.fromJson(output,OTOList24.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<WalletHistoryTxnBean> walletHistoryReport(WalletHistoryBean reportBean) {
		
		logger.debug("*****************calling method  walletHistoryReport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		
		logger.debug("*****************json text as parameter to calling service walletHistoryReport***********************"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/walletHistroyReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<WalletHistoryTxnBean> list = gson.fromJson(output,OTOList22.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
	
	@Override
	public List<RechargeTxnBean> rechargeReportAggreator(ReportBean reportBean) {
		logger.debug("*****************calling method  rechargeReportAggreator ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service rechargeReportAggreator***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/rechargeReportAggreator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service rechargeReportAggreator***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string rechargeReportAggreator***********************"+output);
		List<RechargeTxnBean> list = gson.fromJson(output,OTOList.class);
		System.out.println(list);
		
		return list;
	}
	
	
	@Override
	public List<BbpsPayment> bbpsReportAggreator(ReportBean reportBean) {
		logger.debug("*****************calling method  bbpsReportAggreator ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service bbpsReportAggreator***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/bbpsReportAggreator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service bbpsReportAggreator***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string bbpsReportAggreator***********************"+output);
		List<BbpsPayment> list = gson.fromJson(output,OTOList24.class);
		System.out.println(list);
		
		return list;
	}
	
	
	@Override
	public List<BbpsPayment> bbpsReportDistAggregator(WalletMastBean walletBean) {
		logger.debug("*****************calling method  bbpsReportDistAggregator ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(walletBean);
		logger.debug("*****************json text as parameter to calling service bbpsReportAggreator***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/bbpsReportDistAggregator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service bbpsReportDistAggreator***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string bbpsReportDistAggreator***********************"+output);
		List<BbpsPayment> list = gson.fromJson(output,OTOList24.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
	@Override
	public List<RechargeTxnBean> rechargeReportDist(WalletMastBean walletBean) {
		logger.debug("*****************calling method  bbpsReportDistAggregator ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(walletBean);
		logger.debug("*****************json text as parameter to calling service bbpsReportAggreator***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/rechargeReportDist");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service bbpsReportDistAggreator***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string bbpsReportDistAggreator***********************"+output);
		List<RechargeTxnBean> list = gson.fromJson(output,OTOList.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
	
	@Override
	public List<PaymentGatewayTxnBean> pgReportAggreator(ReportBean reportBean) {
		logger.debug("*****************calling method  pgReportAggreator ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service pgReportAggreator***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/pgReportAggreator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service pgReportAggreator***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string pgReportAggreator***********************"+output);
		List<PaymentGatewayTxnBean> list = gson.fromJson(output,OTOList23.class);
		System.out.println(list);
		
		return list;
	}
	
	

	@Override
	public List<SMSSendDetails> getSMSRepot(SMSSendDetails smsBean) {
		logger.debug("*****************calling method  getsmsreport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(smsBean);
		logger.debug("*****************json text as parameter to calling service getsendsmsreport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSMSRepot");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getsendsmsreport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<SMSSendDetails> list = gson.fromJson(output,OTOList1.class);
		System.out.println(list);
		
		return list;
	}

	@Override
	public SupportTicketBean saveUpdateSupportTicket(SupportTicketBean supBean,String ipimei,String userAgent) {
		logger.debug("*****************calling method  getsmsreport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(supBean);
		logger.debug("*****************json text as parameter to calling service getsendsmsreport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/saveUpdateSupportTicket");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getsendsmsreport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		SupportTicketBean sup= gson.fromJson(output,SupportTicketBean.class);
		
		
		return sup;
	}

	@Override
	public List<SupportTicketBean> getSupportTicket(SupportTicketBean supBean) {
		logger.debug("*****************calling method  getsmsreport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(supBean);
		logger.debug("*****************json text as parameter to calling service getSupportTicket***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSupportTicket");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getSupportTicket***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<SupportTicketBean> list = gson.fromJson(output,OTOList2.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<SupportTicketBean> supportTicketList(SupportTicketBean supBean){
		logger.debug("*****************calling method  supportTicketList ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(supBean);
		logger.debug("*****************json text as parameter to calling service supportTicketList***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSupportTicketList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getSupportTicket***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<SupportTicketBean> list = gson.fromJson(output,OTOList2.class);
		System.out.println(list);
		
		return list;	
	}

	@Override
	public SupportTicketBean updateSupportTicket(SupportTicketBean supBean) {
		logger.debug("*****************calling method  updateSupportTicket ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(supBean);
		logger.debug("*****************json text as parameter to calling service updateSupportTicket***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/updateSupportTicket");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getsendsmsreport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		SupportTicketBean sup= gson.fromJson(output,SupportTicketBean.class);
		return sup;
	}

	@Override
	public List<TxnReportByAdmin> getTxnReportByAdmin() {
		logger.debug("*****************calling method  getTxnReportByAdmin ***********************");
		Gson gson = new Gson();
//		String jsonText=gson.toJson();
		logger.debug("*****************json text as parameter to calling service getTxnReportByAdmin***********************");
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getTxnReportByAdmin");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		logger.debug("*****************response come from getSupportTicket***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<TxnReportByAdmin> list = gson.fromJson(output,OTOList3.class);
		System.out.println(list);
		
		return list;
	}
	@Override
	 public List<CommSummaryBean> getCommSummaryReport(TxnInputBean commSummaryBean) {
	  logger.debug("*****************calling method  getCommSummaryReport ***********************");
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(commSummaryBean);
	  logger.debug("*****************json text as parameter to calling service getCommSummaryReport***********************"+jsonText);
	  WebResource webResource=ServiceManager.getWebResource("ReportManager/getCommSummaryReport");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	  logger.debug("*****************response come from getCommSummaryReport***********************");
	  if (response.getStatus() != 200){
	   logger.debug("*****************response come***********************"+response.getStatus()); 
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  logger.debug("*****************response come from service***********************"+response);
	  String output = response.getEntity(String.class);  
	  logger.debug("*****************response come from service string***********************"+output);
	  List<CommSummaryBean> list = gson.fromJson(output,OTOList4.class);
	  System.out.println(list);
	  
	  return list;
	 }
	
	
	@Override
	public List<AgentBalDetailsBean> getAgentBalDetail(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentBalDetail ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentBalDetail***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentBalDetail");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getAgentBalDetail***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentBalDetailsBean> list = gson.fromJson(output,OTOList5.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public AgentBalDetailResponse getAgentBalDetailBySuperDistributor(WalletMastBean walletBean){
		logger.debug("*****************calling method  getAgentBalDetail ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(walletBean);
		logger.debug("*****************json text as parameter to calling service getAgentBalDetailBySuperDistributor***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentBalDetailBySuperDistributor");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getAgentBalDetailBySuperDistributor***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		AgentBalDetailResponse list = gson.fromJson(output,AgentBalDetailResponse.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<AgentDetailsBean> getAgentDtlsByDistId(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentDtlsByDistId***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAgentDtlsByDistId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getAgentDtlsByDistId***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentDetailsBean> list = gson.fromJson(output,OTOList6.class);
		System.out.println(list);
		
		return list;
	}
	
	public List<AgentDetailsBean> getAgentDtlsBySupDistId(ReportBean reportBean){

		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentDtlsBySupDistId***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAgentDtlsBySupDistId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getAgentDtlsByDistId***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentDetailsBean> list = gson.fromJson(output,OTOList6.class);
		System.out.println(list);
		
		return list;
	
	}
	
	@Override
	public List<AgentCurrentSummary> getAgentCurrentSummary(AgentCurrentSummary agentCurrentSummary) {
		logger.debug("*****************calling method  getAgentCurrentSummary ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(agentCurrentSummary);
		logger.debug("*****************json text as parameter to calling service getAgentCurrentSummary***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/gatAgentCurrentSummary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getAgentCurrentSummary***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentCurrentSummary> list = gson.fromJson(output,OTOList7.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
	
	@Override
	public List<SenderClosingBalBean> getSenderClosingBalReport(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getSenderClosingBalReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSenderClosingBalReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from SenderClosingBalBean***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<SenderClosingBalBean> list = gson.fromJson(output,OTOList8.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
	@Override
	public List<RefundTransactionBean> getRefundTransactionReport(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getRefundTransactionReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getRefundTransactionReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getRefundTransactionReport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<RefundTransactionBean> list = gson.fromJson(output,OTOList9.class);
		System.out.println(list);
		
		return list;
	}
	
	
	@Override
	public List<DmtDetailsMastBean> getDmtDetailsReportList(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustomerDmtDetails");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<DmtDetailsMastBean> list = gson.fromJson(output,OTOList15.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public List<AEPSLedger> getCustAepsReport(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustAepsReport");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<AEPSLedger> list = gson.fromJson(output,OTOList25.class);
		 System.out.println(list);
		 return list;
	}
	
	@Override
	public List<BbpsPayment> getCustBBPSReport(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustBBPSReport");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<BbpsPayment> list = gson.fromJson(output,OTOList24.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public List<MATMLedger> getCustMATMReport(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustMATMReport");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<MATMLedger> list = gson.fromJson(output,OTOList26.class);
		 System.out.println(list);
		 return list;
	}
	
	@Override
	public List<B2CMoneyTxnMast> getCustDmtDetailsReport(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustDmtDetailsReport");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<B2CMoneyTxnMast> list = gson.fromJson(output,OTOList21.class);
		 System.out.println(list);
		 return list;
	}
	
	@Override
	public List<PartnerLedgerBean> getPartnerLedger(ReportBean reportBean) {
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		 WebResource webResource=ServiceManager.getWebResource("ReportManager/getPartnerLedger");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<PartnerLedgerBean> list = gson.fromJson(output,OTOList16.class);
		 System.out.println(list);
		 return list;
	}
	
	

	
	@Override
	public List<AgentClosingBalBean> getAgentClosingBalReport(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentClosingBalReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentClosingBalReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from SenderClosingBalBean***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentClosingBalBean> list = gson.fromJson(output,OTOList17.class);
		System.out.println(list);
		
		return list;
	}
	
	
	@Override
	public List<WalletTxnDetailsRpt> getWalletTxnDetailsReport(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getWalletTxnDetailsReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getWalletTxnDetailsReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getWalletTxnDetailsReport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<WalletTxnDetailsRpt> list = gson.fromJson(output,OTOList18.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<TravelTxn> getTxnDetails(String userId) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(userId);
		logger.debug("*****************json text as parameter to calling service getWalletTxnDetailsReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getTxnDtls");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,userId/*jsonText*/);
		logger.debug("*****************response come from getWalletTxnDetailsReport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<TravelTxn> list = gson.fromJson(output,OTOList20.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<TravelTxn> getflightTxnDetails(ReportBean reportBean) {
		logger.debug("*****************calling method  getAgentDtlsByDistId ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getWalletTxnDetailsReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getTxnDtls");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getWalletTxnDetailsReport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<TravelTxn> list = gson.fromJson(output,OTOList20.class);
		System.out.println(list);
		
		return list;
	}
	
	@Override
	public List<B2CMoneyTxnMast> getBtocDmtReport(ReportBean reportBean) {
		logger.debug("*****************calling method  getBtocDmtReport ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getBtocDmtReport***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getBtocDmtReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getWalletTxnDetailsReport***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<B2CMoneyTxnMast> list = gson.fromJson(output,OTOList19.class);
		System.out.println(list);
		
		return list;
	}
	
	
	// change by mani
	
	@Override
	public List<LeanAccount> leanReport(String aggId) {
		logger.debug("*****************calling method  bbpsReport ***********************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		logger.debug("*****************calling service leanReport***********************");
		WebResource webResource=ServiceManager.getWebResource("ReportManager/leanReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<LeanAccount> list = gson.fromJson(output,OTOList27.class);
		
		
		return list;
	}
	
	
	
	@Override
	public String leanedAccountReport() {
		logger.debug("*****************calling method  bbpsReport ***********************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(inputBean);
		logger.debug("*****************calling service leanedAccountReport***********************");
		WebResource webResource=ServiceManager.getWebResource("ReportManager/leanedAccountReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		logger.debug("*****************response come from service***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<LeanAccount> list = gson.fromJson(output,OTOList27.class);
		
		
		return output;
	}

	@Override
	public String validateAepsMerchant(AepsTokenRequest req) {
		
		logger.debug("*****************calling method  validateAepsMerchant ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(req);
		logger.debug("*****************json text as parameter to calling service validateAepsMerchant***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/validateAepsMerchant");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from validateAepsMerchant***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);
		
		System.out.println(response.toString());
		 
		return output;
	}
	
	
	public List<AgentDetailsBean> getAgentDtlsBySo(ReportBean reportBean){

		logger.debug("*****************calling method  GetAgentDtlsBySo ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentDtlsBySupDistId***********************"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAgentDtlsBySo");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		logger.debug("*****************response come from getAgentDtlsByDistId***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentDetailsBean> list = gson.fromJson(output,OTOList6.class);
		System.out.println(list);
		
		return list;
	 }
	
	public List<AgentDetailsBean> getAgentDtlsBySoList(ReportBean reportBean){

		logger.debug("*****************calling method  GetAgentDtlsBySo ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(reportBean);
		logger.debug("*****************json text as parameter to calling service getAgentDtlsBySupDistId***********************"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAgentDtlsBySoList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		logger.debug("*****************response come from getAgentDtlsByDistId***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		List<AgentDetailsBean> list = gson.fromJson(output,OTOList6.class);
		System.out.println(list);
		
		return list;
	 }
	
	
}

class OTOList27 extends ArrayList<LeanAccount>{
	
}

class OTOList extends ArrayList<RechargeTxnBean>{
	
}
class OTOList1 extends ArrayList<SMSSendDetails>{
	
}
class OTOList2 extends ArrayList<SupportTicketBean>{
	
}
class OTOList3 extends ArrayList<TxnReportByAdmin>{
	
}
class OTOList4 extends ArrayList<CommSummaryBean>{
	
}

class OTOList5 extends ArrayList<AgentBalDetailsBean>{
	
}

class OTOList6 extends ArrayList<AgentDetailsBean>{
	
}

class OTOList7 extends ArrayList<AgentCurrentSummary>{
	
}


class OTOList8 extends ArrayList<SenderClosingBalBean>{
	
}

class OTOList9 extends ArrayList<RefundTransactionBean>{
	
}

class OTOList15 extends ArrayList<DmtDetailsMastBean>{
	
}

class OTOList16 extends ArrayList<PartnerLedgerBean>{
	
}

class OTOList17 extends ArrayList<AgentClosingBalBean>{
	
}

class OTOList18 extends ArrayList<WalletTxnDetailsRpt>{
	
}
class OTOList19 extends ArrayList<B2CMoneyTxnMast>{
	
}

class OTOList20 extends ArrayList<TravelTxn>{
	
}

class OTOList21 extends ArrayList<B2CMoneyTxnMast>{
	
}

class OTOList22 extends ArrayList<WalletHistoryTxnBean>{
	
}
class OTOList23 extends ArrayList<PaymentGatewayTxnBean>{
	
}
class OTOList24 extends ArrayList<BbpsPayment>{
	
}
class OTOList25 extends ArrayList<AEPSLedger>{
	
}
class OTOList26 extends ArrayList<MATMLedger>{
	
}
