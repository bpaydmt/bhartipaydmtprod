package com.bhartipay.wallet.report.service;

import java.util.List;

import com.bhartipay.aeps.AepsTokenRequest;
import com.bhartipay.bbps.vo.BbpsPayment;
import com.bhartipay.lean.LeanAccount;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.recharge.bean.RechargeBean;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.report.bean.AgentBalDetailResponse;
import com.bhartipay.wallet.report.bean.AgentBalDetailsBean;
import com.bhartipay.wallet.report.bean.AgentClosingBalBean;
import com.bhartipay.wallet.report.bean.AgentCurrentSummary;
import com.bhartipay.wallet.report.bean.AgentDetailsBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.CommSummaryBean;
import com.bhartipay.wallet.report.bean.PartnerLedgerBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.SMSSendDetails;
import com.bhartipay.wallet.report.bean.SenderClosingBalBean;
import com.bhartipay.wallet.report.bean.SupportTicketBean;
import com.bhartipay.wallet.report.bean.TravelTxn;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.report.bean.WalletHistoryBean;
import com.bhartipay.wallet.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.wallet.transaction.persistence.vo.PaymentGatewayTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletHistoryTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;

public interface ReportService {

	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<WalletToBankTxnMast> wallettoBankReport(ReportBean reportBean);
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<RechargeTxnBean> rechargeReport(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<BbpsPayment> bbpsReport(TxnInputBean inputBean);
	
	public List<WalletHistoryTxnBean> walletHistoryReport(WalletHistoryBean reportBean);
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<RechargeTxnBean> rechargeReportAggreator(ReportBean reportBean) ;
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<BbpsPayment> bbpsReportAggreator(ReportBean reportBean) ;

	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<PaymentGatewayTxnBean> pgReportAggreator(ReportBean reportBean);
	
	
	/**
	 * 
	 * @param smsBean
	 * @return
	 */
	public List<SMSSendDetails> getSMSRepot(SMSSendDetails smsBean);
	
	/**
	 * 
	 * @param supBean
	 * @return
	 */
	public SupportTicketBean saveUpdateSupportTicket(SupportTicketBean supBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @return
	 */
	public List<SupportTicketBean> getSupportTicket(SupportTicketBean supBean);
	
	/**
	 * 
	 * @return
	 */
	public List<SupportTicketBean> supportTicketList(SupportTicketBean supBean);
	/**
	 * 
	 * @param supBean
	 * @return
	 */
	public SupportTicketBean updateSupportTicket(SupportTicketBean supBean);
	
	/**
	 * 
	 * @return
	 */
	public List<TxnReportByAdmin> getTxnReportByAdmin(); 
	
	/**
	 * 
	 * @param commSummaryBean
	 * @return
	 */
	 public List<CommSummaryBean> getCommSummaryReport(TxnInputBean bean);
	 
	 /**
	  * 
	  * @param reportBean
	  * @return
	  */
	public List<AgentBalDetailsBean> getAgentBalDetail(ReportBean reportBean) ;
	
	
	 /**
	  * 
	  * @param reportBean
	  * @return
	  */
	public AgentBalDetailResponse getAgentBalDetailBySuperDistributor(WalletMastBean walletBean) ;
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<AgentDetailsBean> getAgentDtlsByDistId(ReportBean reportBean) ;
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<AgentDetailsBean> getAgentDtlsBySupDistId(ReportBean reportBean);
	
	public List<AgentDetailsBean> getAgentDtlsBySo(ReportBean reportBean);
	public List<AgentDetailsBean> getAgentDtlsBySoList(ReportBean reportBean);
	/**
	 * 
	 * @param agentCurrentSummary
	 * @return
	 */
	public List<AgentCurrentSummary> getAgentCurrentSummary(AgentCurrentSummary agentCurrentSummary) ;
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<SenderClosingBalBean> getSenderClosingBalReport(ReportBean reportBean) ;
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<RefundTransactionBean> getRefundTransactionReport(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<DmtDetailsMastBean> getDmtDetailsReportList(ReportBean reportBean);
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<AEPSLedger> getCustAepsReport(ReportBean reportBean);
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<BbpsPayment> getCustBBPSReport(ReportBean reportBean);
	
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<MATMLedger> getCustMATMReport(ReportBean reportBean);
	
	
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<B2CMoneyTxnMast> getCustDmtDetailsReport(ReportBean reportBean);
	
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<PartnerLedgerBean> getPartnerLedger(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<AgentClosingBalBean> getAgentClosingBalReport(ReportBean reportBean) ;
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<WalletTxnDetailsRpt> getWalletTxnDetailsReport(ReportBean reportBean);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<B2CMoneyTxnMast> getBtocDmtReport(ReportBean reportBean);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<TravelTxn> getTxnDetails(String userId);
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<TravelTxn> getflightTxnDetails(ReportBean reportBean);
	
	
	public String leanedAccountReport();
	
	public List<BbpsPayment> bbpsReportDistAggregator(WalletMastBean walletBean) ;
	
	public List<RechargeTxnBean> rechargeReportDist(WalletMastBean walletBean) ;
	public List<LeanAccount> leanReport(String aggId);
	
	public String validateAepsMerchant(AepsTokenRequest req);
	
}
