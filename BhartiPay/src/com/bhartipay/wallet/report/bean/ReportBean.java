package com.bhartipay.wallet.report.bean;

public class ReportBean {
	
	
	private String userId;
	private String aggId;
	private String distId;
	private String agentId;
    private String subagentId;
    private String planId;
    private String statusCode;
	private String txnType;
	private String refid;
	private String txnDate;
	private String aggreatorid;
	private String distributerId;
	private String superdistributorid;
	private String [] partnerName;
	private String stDate;
	private String endDate;
	
	


	public String getSuperdistributorid() {
		return superdistributorid;
	}

	public void setSuperdistributorid(String superdistributorid) {
		this.superdistributorid = superdistributorid;
	}

	public String[] getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String[] partnerName) {
		this.partnerName = partnerName;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getDistributerId() {
		return distributerId;
	}

	public void setDistributerId(String distributerId) {
		this.distributerId = distributerId;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getRefid() {
		return refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getSubagentId() {
		return subagentId;
	}

	public void setSubagentId(String subagentId) {
		this.subagentId = subagentId;
	}

	public String getAggId() {
		return aggId;
	}

	public void setAggId(String aggId) {
		this.aggId = aggId;
	}

	public String getDistId() {
		return distId;
	}

	public void setDistId(String distId) {
		this.distId = distId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	

}
