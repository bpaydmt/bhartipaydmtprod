package com.bhartipay.wallet.report.bean;

import java.io.Serializable;

public class AgentClosingBalBean implements Serializable{
	
	
	private String  id;
	private String name;
	private double credit;
	private double debit;
	private double finalbalance;
	private String auditdate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public double getDebit() {
		return debit;
	}
	public void setDebit(double debit) {
		this.debit = debit;
	}
	public double getFinalbalance() {
		return finalbalance;
	}
	public void setFinalbalance(double finalbalance) {
		this.finalbalance = finalbalance;
	}
	public String getAuditdate() {
		return auditdate;
	}
	public void setAuditdate(String auditdate) {
		this.auditdate = auditdate;
	}
	
	
	
	

}
