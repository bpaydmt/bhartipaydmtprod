package com.bhartipay.wallet.report.bean;

import java.util.List;

public class ResponseBean {
	
	private String response;
	private String message;
	private String code;
	private List<RefundAgent> refundList;
	
	
	public List<RefundAgent> getRefundList() {
		return refundList;
	}
	public void setRefundList(List<RefundAgent> refundList) {
		this.refundList = refundList;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
	
	

}
