package com.bhartipay.wallet.report.bean;

import java.util.Date;

public class TravelTxn 
{
	private String type;
	
	private String transid;
	
	private Date txndate;
	
	private double amount;
	
	private String status;
	
	private String details;

	private String cancellationstatus;
	
	
	public String getCancellationstatus() {
		return cancellationstatus;
	}

	public void setCancellationstatus(String cancellationstatus) {
		this.cancellationstatus = cancellationstatus;
	}

	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTransid() {
		return transid;
	}

	public void setTransid(String transid) {
		this.transid = transid;
	}

	public Date getTxndate() {
		return txndate;
	}

	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public static void main(String[] s)
	{
		String str="6E,Indigo,126|DEL,Delhi|BLR,Bangalore|2622.00|2018-04-26 16:00:00|2018-04-26 18:45:00|TicketBooked|ZCWBVZ#SG,SpiceJet,136|BLR,Bangalore|DEL,Delhi|3613.77|2018-04-26 05:55:00|2018-04-26 08:30:00|TicketBooked|G1SL7L#";
		String[] strToken1 =  str.split("#");
		System.out.println(strToken1.length);
		for(int i=0;i<strToken1.length;i++)
		{
			System.out.println(strToken1[i]);
		}
	}
}
