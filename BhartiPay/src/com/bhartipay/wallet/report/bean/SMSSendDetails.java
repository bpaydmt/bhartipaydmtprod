package com.bhartipay.wallet.report.bean;
import java.sql.Date;
public class SMSSendDetails {
	
	private int id;
	private String aggreatorid;
	private String recipientno;
	private String walletid;
	private String sendername;
	private Date entryon;
	public String stDate;
	public String endDate;
	
		

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRecipientno() {
		return recipientno;
	}

	public void setRecipientno(String recipientno) {
		this.recipientno = recipientno;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getSendername() {
		return sendername;
	}

	public void setSendername(String sendername) {
		this.sendername = sendername;
	}

	public Date getEntryon() {
		return entryon;
	}

	public void setEntryon(Date entryon) {
		this.entryon = entryon;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	
	
	
	
	
	

}
