package com.bhartipay.wallet.report.bean;


public class TxnReportByAdmin {
	
	private  String aggreatorid;
	private  String txndesc;
	private  String txndate;
	private  double txncredit;
	private  double txndebit;
	
	
	
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getTxndesc() {
		return txndesc;
	}
	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	public double getTxncredit() {
		return txncredit;
	}
	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}
	public double getTxndebit() {
		return txndebit;
	}
	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}
	
	
	
	
	
	
	
	

}
