package com.bhartipay.wallet.report.bean;

public class RefundAgent {
	private String id;
	
	private String status;
	
	private String dramount;
	
	private String txnamount;
	
	private String narrartion;
	
	private String mobileno;

    private String otp;

	
	
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	public String getTxnamount() {
		return txnamount;
	}

	public void setTxnamount(String txnamount) {
		this.txnamount = txnamount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDramount() {
		return dramount;
	}

	public void setDramount(String dramount) {
		this.dramount = dramount;
	}

	public String getNarrartion() {
		return narrartion;
	}

	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

}
