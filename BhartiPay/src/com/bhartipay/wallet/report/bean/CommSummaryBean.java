package com.bhartipay.wallet.report.bean;

import java.math.BigDecimal;

public class CommSummaryBean {

	private String txnid;
	private String txnDate;
	private String userid;
	private String aggreatorid;
	private BigDecimal txnAmount;
	private BigDecimal  aggamount;
	private BigDecimal  distamount;
	private BigDecimal  agentamount;
	private BigDecimal  subagentamount;
	private BigDecimal  payoutamount;
	
	private String stDate;
	private String endDate;
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public BigDecimal getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}
	public BigDecimal getAggamount() {
		return aggamount;
	}
	public void setAggamount(BigDecimal aggamount) {
		this.aggamount = aggamount;
	}
	public BigDecimal getDistamount() {
		return distamount;
	}
	public void setDistamount(BigDecimal distamount) {
		this.distamount = distamount;
	}
	public BigDecimal getAgentamount() {
		return agentamount;
	}
	public void setAgentamount(BigDecimal agentamount) {
		this.agentamount = agentamount;
	}
	public BigDecimal getSubagentamount() {
		return subagentamount;
	}
	public void setSubagentamount(BigDecimal subagentamount) {
		this.subagentamount = subagentamount;
	}
	public BigDecimal getPayoutamount() {
		return payoutamount;
	}
	public void setPayoutamount(BigDecimal payoutamount) {
		this.payoutamount = payoutamount;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
	
	
	
	
	
	
}
