package com.bhartipay.wallet.report.bean;
import java.util.Date;

public class SupportTicketBean {
	private int id;
	
	private String aggreatorId;
	private String userId;
	private String mobileNo;
	private String emailId;
	private String ticketType;
	private String description;
	
	private String entryon;
	private String statusCode;
	private String status;
	
	private String remarks;
	private String ipiemi;
	private String userAgent;
	
	private String stDate;
	private String endDate;

	
	
	
	public String getStDate() {
		return stDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setStDate(String stDate) {
		this.stDate = stDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAggreatorId() {
		return aggreatorId;
	}


	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getTicketType() {
		return ticketType;
	}


	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}





	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}





	public String getEntryon() {
		return entryon;
	}


	public void setEntryon(String entryon) {
		this.entryon = entryon;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getIpiemi() {
		return ipiemi;
	}


	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}


	public String getUserAgent() {
		return userAgent;
	}


	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	
		
	

}
