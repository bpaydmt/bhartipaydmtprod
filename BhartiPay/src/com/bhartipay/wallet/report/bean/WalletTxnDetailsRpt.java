package com.bhartipay.wallet.report.bean;

import java.io.Serializable;

public class WalletTxnDetailsRpt implements Serializable{
	
	private String id;
	private String  name;
	private String txndate;
	private String txnid;
	private String resptxnid;
	private double txncredit;
	private double txndebit;
	private String payeedtl;
	private double closingbal;
	private String txndesc;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getResptxnid() {
		return resptxnid;
	}
	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}
	public double getTxncredit() {
		return txncredit;
	}
	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}
	public double getTxndebit() {
		return txndebit;
	}
	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}
	public String getPayeedtl() {
		return payeedtl;
	}
	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}
	public double getClosingbal() {
		return closingbal;
	}
	public void setClosingbal(double closingbal) {
		this.closingbal = closingbal;
	}
	public String getTxndesc() {
		return txndesc;
	}
	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}
	
	
	
	
	
	

}
