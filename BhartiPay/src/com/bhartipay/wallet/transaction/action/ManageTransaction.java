package com.bhartipay.wallet.transaction.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.util.ServerInfo;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.bhartipay.bbps.vo.BbpsPayment;
import com.bhartipay.lean.AddBankAccount;
import com.bhartipay.lean.LeanAccount;
import com.bhartipay.lean.PgReturnResponse;
import com.bhartipay.util.ChecksumUtils;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.pg.PGDetails;
import com.bhartipay.wallet.framework.security.AES128Bit;
import com.bhartipay.wallet.framework.security.EncryptionByEnc256;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.Utils.ObjectFactory;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.bhartipay.wallet.merchant.service.impl.MerchantServiceImpl;
import com.bhartipay.wallet.payment.vo.CustomerDtls;
import com.bhartipay.wallet.payment.vo.PayGateHandler;
import com.bhartipay.wallet.payment.vo.ShippingDtls;
import com.bhartipay.wallet.payment.vo.TDSecurePayGateObject;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.recharge.service.impl.RechargeServiceImpl;
import com.bhartipay.wallet.report.bean.AgentClosingBalBean;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.CommSummaryBean;
import com.bhartipay.wallet.report.bean.PartnerLedgerBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.SMSSendDetails;
import com.bhartipay.wallet.report.bean.SenderClosingBalBean;
import com.bhartipay.wallet.report.bean.SupportTicketBean;
import com.bhartipay.wallet.report.bean.TravelTxn;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.report.bean.WalletHistoryBean;
import com.bhartipay.wallet.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.wallet.report.service.ReportService;
import com.bhartipay.wallet.report.service.impl.ReportServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.MoneyRequestBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGAggregatorDetailBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.PaymentGatewayTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.UserWalletConfigBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletHistoryTxnBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;

import appnit.com.crypto.CheckSumHelper;

public class ManageTransaction extends BaseAction implements ServletResponseAware{
	
	WalletMastBean walletBean=new WalletMastBean();
	MoneyRequestBean moneyRequest=new MoneyRequestBean();
	HttpServletResponse response=null;
	public static Logger logger=Logger.getLogger(ManageTransaction.class);	
	TransactionService tService=new TransactionServiceImpl();
	UserService uService=new UserServiceImpl();
	TxnInputBean inputBean=new TxnInputBean();
	DMTInputBean dmtInputBean=new DMTInputBean();
	WalletToBankTxnMast wtob=new WalletToBankTxnMast();
	Map<String,String> customerList=new TreeMap<String, String>();
	UserWalletConfigBean config=new UserWalletConfigBean();
	MerchantService mService=new MerchantServiceImpl();
	ReportService rService=new ReportServiceImpl();
	Map<String, String> plans=new HashMap<String, String>(); 
	Map<String, String> txnType=new HashMap<String, String>();
	CommissionService comService=new CommissionServiceImpl();	
	CommPlanMaster commBean=new CommPlanMaster();
	SMSSendDetails smsBean=new SMSSendDetails();
	SupportTicketBean supBean=new SupportTicketBean();
	CommissionService commService=new CommissionServiceImpl();
	UserDetails userDetails=new UserDetails();
	Map<String,String> accountTypeList=new LinkedHashMap<String, String>();
	CashDepositMast depositMast=new CashDepositMast();
	Map<String,String> txnTypeList=new HashMap<String,String>();
	RefundMastBean refundBean=new RefundMastBean();
	AskMoneyBean amb=new AskMoneyBean();
	EscrowBean bean=new EscrowBean();
	UserSummary user=new UserSummary();
	SurchargeBean suBean=new SurchargeBean();
	public String statusMessage;
	Properties prop =null;
	Map<String, String> aggrigatorList=new LinkedHashMap<String, String>();
	Map<String,String> bankNameList=new HashMap<String,String>();
	Map<String, String> agentList=new LinkedHashMap<String, String>();
	private UserService service=new UserServiceImpl();
	
	PartnerPrefundBean partnerPrefundBean=new PartnerPrefundBean();
	
	public Map<String,String> txnSourceList=new LinkedHashMap<String, String>();
	public Map<String,String> partnerList=new LinkedHashMap<String, String>();
	public Map<String,String> tranType=new LinkedHashMap<String, String>();
	B2CMoneyTxnMast b2cMoneyTxnMast = new B2CMoneyTxnMast();
	 ObjectFactory oFactory = new ObjectFactory();
	 RechargeService reService=new RechargeServiceImpl();
		HashMap<String, String> prePaid = new HashMap<String, String>();
		HashMap<String, String> postPaid = new HashMap<String, String>();
		HashMap<String, String> dth = new HashMap<String, String>();
		HashMap<String, String> landLine = new HashMap<String, String>();
		HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
		HashMap<String, String> postPaidDataCard = new HashMap<String, String>();
		HashMap<String, String> circle = new HashMap<String, String>();
	 
	 
		Map<String, String> requestMap =new HashMap<String,String>();
		Map<String,String> pgpayementPageParams=new HashMap<String,String>();
		String pgPaymentUrl=null;
		String TransactionStatus=null;
		
		String customerId="";
		String distributorId = "";
	
		String agentName = "";
		String agentMobile = "";
		String agentMail = "";
		
		Map<String,String> distributorList=new HashMap<String,String>();
		Map<String,String> subAggregatorList=new HashMap<String,String>();
		
		String depositBank = "";
	 
		public String getAgentName() {
			return agentName;
		}

		public void setAgentName(String agentName) {
			this.agentName = agentName;
		}

		public String getAgentMobile() {
			return agentMobile;
		}
		
		public WalletMastBean getWalletBean() {
			return walletBean;
		}

		public void setWalletBean(WalletMastBean walletBean) {
			this.walletBean = walletBean;
		}

		public void setAgentMobile(String agentMobile) {
			this.agentMobile = agentMobile;
		}

		public String getAgentMail() {
			return agentMail;
		}

		public void setAgentMail(String agentMail) {
			this.agentMail = agentMail;
		}
		
		public Map<String, String> getDistributorList() {
			return distributorList;
		}

		public void setDistributorList(Map<String, String> distributorList) {
			this.distributorList = distributorList;
		}

		public Map<String, String> getSubAggregatorList() {
			return subAggregatorList;
		}

		public void setSubAggregatorList(Map<String, String> subAggregatorList) {
			this.subAggregatorList = subAggregatorList;
		}
		
		public String getDepositBank() {
			return depositBank;
		}

		public void setDepositBank(String depositBank) {
			this.depositBank = depositBank;
		}

		public String getDistributorId() {
			return distributorId;
		}

		public void setDistributorId(String distributorId) {
			this.distributorId = distributorId;
		}

		public String getTransactionStatus() {
			return TransactionStatus;
		}

		public void setTransactionStatus(String transactionStatus) {
			TransactionStatus = transactionStatus;
		}
		
		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public String getPgPaymentUrl() {
			return pgPaymentUrl;
		}

		public void setPgPaymentUrl(String pgPaymentUrl) {
			this.pgPaymentUrl = pgPaymentUrl;
		}

		public Map<String, String> getPgpayementPageParams() {
			return pgpayementPageParams;
		}

		public void setPgpayementPageParams(Map<String, String> pgpayementPageParams) {
			this.pgpayementPageParams = pgpayementPageParams;
		}


		public Map<String, String> getRequestMap() {
			return requestMap;
		}

		public void setRequestMap(Map<String, String> requestMap) {
			this.requestMap = requestMap;
		}
	 
	 
	 
	 public Map<String, String> getAggrigatorList() {
			return aggrigatorList;
		}

		public void setAggrigatorList(Map<String, String> aggrigatorList) {
			this.aggrigatorList = aggrigatorList;
		}

	public MoneyRequestBean getMoneyRequest() {
			return moneyRequest;
		}

		public void setMoneyRequest(MoneyRequestBean moneyRequest) {
			this.moneyRequest = moneyRequest;
		}
	 public HashMap<String, String> getPrePaid() {
			return prePaid;
		}

		public void setPrePaid(HashMap<String, String> prePaid) {
			this.prePaid = prePaid;
		}

		public HashMap<String, String> getPostPaid() {
			return postPaid;
		}

		public void setPostPaid(HashMap<String, String> postPaid) {
			this.postPaid = postPaid;
		}

		public HashMap<String, String> getDth() {
			return dth;
		}

		public void setDth(HashMap<String, String> dth) {
			this.dth = dth;
		}

		public HashMap<String, String> getLandLine() {
			return landLine;
		}

		public void setLandLine(HashMap<String, String> landLine) {
			this.landLine = landLine;
		}

		public HashMap<String, String> getPrePaidDataCard() {
			return prePaidDataCard;
		}

		public void setPrePaidDataCard(HashMap<String, String> prePaidDataCard) {
			this.prePaidDataCard = prePaidDataCard;
		}

		public HashMap<String, String> getPostPaidDataCard() {
			return postPaidDataCard;
		}

		public void setPostPaidDataCard(HashMap<String, String> postPaidDataCard) {
			this.postPaidDataCard = postPaidDataCard;
		}

		public HashMap<String, String> getCircle() {
			return circle;
		}

		public void setCircle(HashMap<String, String> circle) {
			this.circle = circle;
		}

	public B2CMoneyTxnMast getB2cMoneyTxnMast() {
	  return b2cMoneyTxnMast;
	 }

	 public void setB2cMoneyTxnMast(B2CMoneyTxnMast b2cMoneyTxnMast) {
	  this.b2cMoneyTxnMast = b2cMoneyTxnMast;
	 }
	
	
	
	
	
	
	
	
	public Map<String, String> getTranType() {
		tranType.put("DEBIT", "DEBIT");
		tranType.put("CREDIT", "CREDIT");
		return tranType;
	}

	public void setTranType(Map<String, String> tranType) {
		this.tranType = tranType;
	}


	
	
	public Map<String, String> getTxnSourceList() {
		return txnSourceList;
	}
	public void setTxnSourceList(Map<String, String> txnSourceList) {
		this.txnSourceList = txnSourceList;
	}
	public Map<String, String> getPartnerList() {
		return partnerList;
	}
	public void setPartnerList(Map<String, String> partnerList) {
		this.partnerList = partnerList;
	}
	
	
	
	public PartnerPrefundBean getPartnerPrefundBean() {
		return partnerPrefundBean;
	}
	public void setPartnerPrefundBean(PartnerPrefundBean partnerPrefundBean) {
		this.partnerPrefundBean = partnerPrefundBean;
	}
	public Map<String, String> getAgentList() {
		return agentList;
	}
	public void setAgentList(Map<String, String> agentList) {
		this.agentList = agentList;
	}
	
	
	
	
	public ManageTransaction(){
		/* added by neeraj call error code using properties file. */
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}}
		/*end by neeraj*/
	public SurchargeBean getSuBean() {
		return suBean;
	}

	public String getStatusMessage() {
		return statusMessage;
	}


	public String b2CMoneyTxn()
	 {
	  logger.debug("**********b2CMoneyTxn**************");
	  try
	  {
	   User user = (User)session.get("User");
	   String aggId = (String)session.get("aggId");
	   Map<String, HashMap<String,String>> mapResult= reService.getRechargeOperator();
		request.setAttribute("mapResult",mapResult);
		session.put("transferTo","toBank");
		

		if(mapResult!=null){
			try{
			setPrePaid(mapResult.get("PREPAID"));
			setPostPaid(mapResult.get("POSTPAID"));
			setDth(mapResult.get("DTH"));
			setLandLine(mapResult.get("LANDLINE"));
			setPrePaidDataCard(mapResult.get("PREPAIDDATACARD"));
			setPostPaidDataCard(mapResult.get("POSTPAIDDATACARD"));
			setCircle(mapResult.get("CIRCLE"));
			}catch(Exception e){
				e.printStackTrace();
				session.put("errorMsg", "Oops! something went wronge.");
				//addActionError("Oops! something went wronge.");
				return "fail";
			}
		}
		else
		{
			session.put("errorMsg", "Oops! something went wronge.");
			//addActionError("Oops! something went wronge.");
			return "fail";
		}
	   if(user == null || user.getId() == null)
	   {
	    request.getSession().putValue("loginStatus", "fail");
		session.put("loginStatus", "fail");
	    request.getSession().putValue("b2cMoneyTxnMast", b2cMoneyTxnMast);
	    return "login";
	   }
	   if(b2cMoneyTxnMast == null || b2cMoneyTxnMast.getAmount() == 0 || b2cMoneyTxnMast.getAccountNo() == null)
	   {
	    b2cMoneyTxnMast = (B2CMoneyTxnMast)session.get("b2cMoneyTxnMast");
	   }
	   if(b2cMoneyTxnMast != null)
	   {

	    String objGson = new Gson().toJson(b2cMoneyTxnMast);
	    boolean flag = oFactory.checkScript(objGson);
	    if(!flag)
	    {
	    	session.put("errorMsg", "Oops! something went wronge.");
	    // addActionError("Oops  something went wronge. Please try after sometime !!");
	     return "fail";
	    }
	   
	   }
	   String userAgent=request.getHeader("User-Agent");
	   
	   String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	   if (ipAddress == null) {
	       ipAddress = request.getRemoteAddr();
	   }
	   b2cMoneyTxnMast.setUserId(user.getId());
	   b2cMoneyTxnMast.setUserId(user.getId());
	   b2cMoneyTxnMast.setWalletId(user.getWalletid());
	   b2cMoneyTxnMast.setIpimei(ipAddress);
	   b2cMoneyTxnMast.setAggreatorId(aggId);
	   b2cMoneyTxnMast.setTransferType("NEFT");
	   TreeMap<String,String> obj=new TreeMap<String,String>();
	   obj.put("userId",b2cMoneyTxnMast.getUserId());
	         obj.put("walletId",b2cMoneyTxnMast.getWalletId());
	         obj.put("amount",""+b2cMoneyTxnMast.getAmount());
	         obj.put("ipimei",b2cMoneyTxnMast.getIpimei());
	         obj.put("aggreatorId",b2cMoneyTxnMast.getAggreatorId());
	         obj.put("accHolderName",b2cMoneyTxnMast.getAccHolderName());
	         obj.put("accountNo",b2cMoneyTxnMast.getAccountNo());
	         obj.put("ifscCode",b2cMoneyTxnMast.getIfscCode());
	         obj.put("remarks",b2cMoneyTxnMast.getRemarks());
	         obj.put("transferType",b2cMoneyTxnMast.getTransferType());
	         obj.put("token",user.getToken());                
	         try {
	       String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),obj);
	       b2cMoneyTxnMast.setCHECKSUMHASH(checkSumString);
	            } catch (Exception e) {
	       e.printStackTrace();
	            }
	   B2CMoneyTxnMast respBean =tService.b2CMoneyTxn(b2cMoneyTxnMast);
	   if(respBean.getStatusCode().equals("1000") && respBean.getStatusMsg().contains("NEFT"))
	   {
		   session.put("successMsg", "Successfully transfered.");
	    //addActionMessage("Successfully transfered.");
	    return "success";
	   }
	   else
	   {
	    switch(respBean.getStatusCode())
	    {
	    case "1001" :
	    {
	    	 session.put("errorMsg", "There is some problem please try again.");
	     //addActionError("There is some problem please try again.");
	     return "fail";
	    }
	    case "7014":
	    {
	    	 session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
	    // addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
	     return "fail";
	    }
	    case "7017":
	    {
	    	 session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
	 	    //  addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
	     return "fail";
	    }
	    case "7018":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
	    // addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
	     return "fail";
	    }
	    case "7019":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
	     return "fail";
	    }
	    case "7020":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
	     return "fail";
	    }
	    case "7021":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
	     return "fail";
	    }
	    case "7022":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
	     return "fail";
	    }
	    case "7045":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
	     return "fail";
	    }
	    case "7046":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
	     return "fail";
	    }
	    case "7047":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
	     return "fail";
	    }
	    case "7048":
	    { session.put("errorMsg", "Transaction failed, Wallet balance violated.");
	    //addActionError("Transaction failed, Wallet balance violated.");
	     return "fail";
	    }
	    case "7000":
	    { session.put("errorMsg", "Transaction fail.");
	    //addActionError("Transaction fail.");
	     return "fail";
	    }
	    case "7024":
	    { session.put("errorMsg", "Transaction failed due to insufficient balance.");
	    //addActionError("Transaction failed due to insufficient balance.");
	     return "fail";
	    }
	    case "7042":
	    { session.put("errorMsg", "Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
	    //addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
	     return "fail";
	    }
	    case "8001":
	    { session.put("errorMsg", "You are not an active user.");
	    //addActionError("You are not an active user.");
	     return "fail";
	    }
	    case "8002":
	    { session.put("errorMsg", "Security Error.");
	    //addActionError("Security Error.");
	     return "fail";
	    }
	    default :
	    { session.put("errorMsg", "Transaction failed.");
	    //addActionError("Transaction failed.");
	     return "fail";
	    }
	    }
	   }
	  }
	  catch(Exception e)
	  {
	   logger.debug("*******problem in b2CMoneyTxn*********e.message**"+e.getMessage());
	   e.printStackTrace();
	  }
	  return "fail";
	 }
	
	public Map<String, String> getBankNameList() {
		/*bankNameList.put("AXIS-916020076556799","Axis Bank-916020076556799");
		bankNameList.put("ICICI-003105031101","ICICI BANK-003105031101");
     	bankNameList.put("Saraswat-358100100000163","Saraswat Bank-358100100000163");
		bankNameList.put("SBI-36882771971","State Bank of India-36882771971");
		bankNameList.put("BB-10170003130745","Bandhan Bank-10170003130745");
		bankNameList.put("BOB-05860200001476","Bank Of Baroda-05860200001476");
		*/
		User user=(User)session.get("User");
		List<String> bankList = new ArrayList<String>();
		if(user.getUsertype() == 4)
		{
			bankList = uService.getCashDepositBank("OAGG001050");
		}
		else
		{
			bankList =uService.getCashDepositBank((String)session.get("aggId"));
		}
		for(String bank : bankList)
		{
			bankNameList.put(bank,bank);
		}
		return bankNameList;
	}
	public void setBankNameList(Map<String, String> bankNameList) {
		this.bankNameList = bankNameList;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public void setSuBean(SurchargeBean suBean) {
		this.suBean = suBean;
	}

	public UserSummary getUser() {
		return user;
	}

	public void setUser(UserSummary user) {
		this.user = user;
	}

	public AskMoneyBean getAmb() {
		return amb;
	}

	public void setAmb(AskMoneyBean amb) {
		this.amb = amb;
	}

	public RefundMastBean getRefundBean() {
		return refundBean;
	}

	public void setRefundBean(RefundMastBean refundBean) {
		this.refundBean = refundBean;
	}

	public Map<String, String> getTxnTypeList() {
		
		txnTypeList.put("NEFT","NEFT");
		txnTypeList.put("RTGS","RTGS");
		txnTypeList.put("IMPS","IMPS");
		txnTypeList.put("CASH", "CASH");
		txnTypeList.put("CREDIT","CREDIT");
		return txnTypeList;
	}

	public void setTxnTypeList(Map<String, String> txnTypeList) {
		this.txnTypeList = txnTypeList;
	}

	public CashDepositMast getDepositMast() {
		return depositMast;
	}

	public void setDepositMast(CashDepositMast depositMast) {
		this.depositMast = depositMast;
	}

	public EscrowBean getBean() {
		return bean;
	}

	public void setBean(EscrowBean bean) {
		this.bean = bean;
	}

	public Map<String, String> getAccountTypeList() {
		return accountTypeList;
	}

	public void setAccountTypeList(Map<String, String> accountTypeList) {
		accountTypeList.put("Savings","Saving");
		accountTypeList.put("Current","Current");
		this.accountTypeList = accountTypeList;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public SupportTicketBean getSupBean() {
		return supBean;
	}

	public void setSupBean(SupportTicketBean supBean) {
		this.supBean = supBean;
	}

	public SMSSendDetails getSmsBean() {
		return smsBean;
	}

	public void setSmsBean(SMSSendDetails smsBean) {
		this.smsBean = smsBean;
	}

	public CommPlanMaster getCommBean() {
		return commBean;
	}

	public void setCommBean(CommPlanMaster commBean) {
		this.commBean = commBean;
	}

	public Map<String, String> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, String> plans) {
		this.plans = plans;
	}

	public Map<String, String> getTxnType() {
		return txnType;
	}

	public void setTxnType(Map<String, String> txnType) {
		this.txnType = txnType;
	}

	public DMTInputBean getDmtInputBean() {
		return dmtInputBean;
	}

	public void setDmtInputBean(DMTInputBean dmtInputBean) {
		this.dmtInputBean = dmtInputBean;
	}

	public UserWalletConfigBean getConfig() {
		return config;
	}

	public void setConfig(UserWalletConfigBean config) {
		this.config = config;
	}

	public Map<String, String> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(Map<String, String> customerList) {
		this.customerList = customerList;
	}

	public WalletToBankTxnMast getWtob() {
		return wtob;
	}

	public void setWtob(WalletToBankTxnMast wtob) {
		this.wtob = wtob;
	}

	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}

	/*public String addMoney(){
		logger.debug("**********************  addmoney calling  **************************");
		User user=(User)session.get("User");
		if(user!=null&&user.getUsertype()==1){
			logger.debug("**********************  addmoney calling  returning customer**************************");
			return "customer";
		}
		logger.debug("**********************  addmoney calling  returning success**************************");
	return "success";	
	}*/
	
	public String addMoney(){
		logger.debug("**********************  addmoney calling  **************************");
		User user=(User)session.get("User");
		if(user.getOnlineMoney()!=1) {
			return "error";
		}
		String orderId=request.getParameter("ORDER_ID");
		String amount=request.getParameter("AMOUNT");
		TransactionStatus=request.getParameter("STATUS");
		
		if(orderId !=null && orderId !="") {
			pgpayementPageParams.put("ORDER_ID", orderId);
		}
		if(amount !=null && amount !="") {
			pgpayementPageParams.put("AMOUNT", amount);
		}
		if(TransactionStatus !=null && TransactionStatus !="") {
			pgpayementPageParams.put("STATUS", TransactionStatus);
		}
		if(user!=null&&user.getUsertype()==1){
			logger.debug("**********************  addmoney calling  returning customer**************************");
			return "customer";
		}
		logger.debug("**********************  addmoney calling  returning success**************************");
		return "success";	
	}
	
	public String moneyRequest(){
		User user=(User)session.get("User");
		logger.debug("**********************  moneyRequest calling  returning success**************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		logger.debug("**********************  moneyRequest.getBank()**************************"+moneyRequest.getBank());
		logger.debug("**********************  moneyRequest.getTxnType()**************************"+moneyRequest.getTxnType());
		logger.debug("**********************  moneyRequest.getAmount()**************************"+moneyRequest.getAmount());
		logger.debug("**********************  moneyRequest.getUtrNo()**************************"+moneyRequest.getUtrNo());
		
		moneyRequest.setAggId(user.getAggreatorid());
		moneyRequest.setIpImei(ipAddress);
		moneyRequest.setUserAgent(userAgent);
		moneyRequest.setWalletId(user.getWalletid());
		moneyRequest.setSenderName(user.getName());
		moneyRequest.setRequestStatus("0");
		
    

		CashDepositMast cdm=new CashDepositMast();
		cdm.setAggreatorId(user.getAggreatorid());
		cdm.setUserId(user.getId());
		cdm.setWalletId(user.getWalletid());
		cdm.setType(moneyRequest.getTxnType());
		cdm.setAmount(moneyRequest.getAmount());
		cdm.setNeftRefNo(moneyRequest.getUtrNo());
		cdm.setIpIemi(ipAddress);
		cdm.setAgent(userAgent);
		cdm.setBankName(moneyRequest.getBank());
		cdm.setBranchName("-");
		
		String status=tService.moneyRequest(cdm);
		
		JSONObject jObject = new JSONObject();
		jObject.put("status",status);
		response.setContentType("application/json");
		try {
			response.getWriter().println(jObject);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String paymentGateway(){	
		
		logger.debug("**********************  calling paymentGateway() **************************");
		
		System.out.println("__________Inside ManageTransaction.java____________paymentGateway_________");
		
		User user=(User)session.get("User");
		String aggId=(String)session.get("aggId");
		
		System.out.println("______User____"+user+"_______aggId___________"+aggId);
		
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			
			System.out.println("___________User is Null______________");
			addActionError("Please try again..");
			return "fail";
		}		
		
		if(inputBean.getTrxAmount() <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
			
			System.out.println("_________________Transaction Amt >0 ______inputBean.getTrxAmount()"+inputBean.getTrxAmount());
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
		
		System.out.println("__________________result______________"+result);
		
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError("There is some problem please try again.");
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid mobile no.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError("Transaction failed, Wallet balance violated.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError("Transaction fail.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid reciver mobile number.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError("Transaction failed due to insufficient balance.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PaymentGateway.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		WalletConfiguration config1=new WalletConfiguration();
		config1.setAggreatorid(aggId);
		
		System.out.println("----aggreatorId------"+config1.getAggreatorid());
		
		
		Map<String,String> mapResult=service.getWalletConfigPG(config1);
		
		
		//Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
		String appName=mapResult.get("pgAgId");
		String userName=mapResult.get("pgAgId");
		String MID=mapResult.get("pgMid");
		
		String successURL=mapResult.get("pgCallBackUrl")+"/PGResponse.action";
		String	failURL=mapResult.get("pgCallBackUrl")+"/PGResponse.action";
		
		logger.debug("**********************  CallBackFailURL**************************"+successURL);
		logger.debug("**********************  failURL**************************"+failURL);
		
		String submitTo=mapResult.get("pgUrl");
		String securitykey=mapResult.get("pgEncKey");
//		String TxnType=mapResult.get("pgTxnType");
//		String country=mapResult.get("pgCountry");
//		String currency=mapResult.get("pgCurrency");
		//String Channel="WEB";
		
//		String operatingMode=prop.getProperty("operatingMode");
//		String otherDetails=prop.getProperty("otherDetails");
//		String collaborator=prop.getProperty("collaborator");
				
				
				String requestParameter="Appname="+appName+"|TransID="+txnId+"|Amount="+inputBean.getTrxAmount()+"|Appuser="+userName+"|CallBackURL="+successURL+"|MID="+MID;
				PGDetails pgDetails=new PGDetails();
				String billingDtls = pgDetails.getBillingDtls();
logger.debug("***********************Before encryption parameters **********************************");				
logger.debug("request parameter for paymentgateway  "+requestParameter);
logger.debug("pgdetails    "+pgDetails);
logger.debug("billing details    "+billingDtls);

			    if(user.getEmailid()!=null&&user.getEmailid().length()>0){
			    	billingDtls=billingDtls.replace("sandeep.prajapati@appnittech.com",user.getEmailid());
			    }
			   /* if(user.getName()!=null&&user.getName().length()>0){
			    	billingDtls=billingDtls.replace("Appnit Technologies",user.getName());
			    }*/
			    if(user.getMobileno()!=null&&user.getMobileno().length()>0){
			    	billingDtls=billingDtls.replace("9335235731",user.getMobileno());
			    }
			    
				String shippingDtls = pgDetails.getShippingDtls();
				//String key = Encryption.getEncryptionKey("rechappPG");
				
				System.out.println(pgDetails.getBillingDtls());
				System.out.println(pgDetails.getShippingDtls());
				System.out.println(requestParameter);
				requestParameter = AES128Bit.encrypt(requestParameter, securitykey);
				billingDtls = AES128Bit.encrypt(billingDtls, securitykey);
				shippingDtls = AES128Bit.encrypt(shippingDtls, securitykey);
				requestParameter = requestParameter.replaceAll("\n", "");
				billingDtls = billingDtls.replaceAll("\n", "");
				shippingDtls = shippingDtls.replaceAll("\n", "");
				logger.debug("***********************after encryption parameters **********************************");				
				logger.debug("request parameter for paymentgateway  "+requestParameter);
				logger.debug("pgdetails    "+pgDetails);
				logger.debug("billing details    "+billingDtls);
				logger.debug("MID is  "+MID);
				request.setAttribute("requestparameter",requestParameter);
				request.setAttribute("billingDtls",billingDtls);
				request.setAttribute("shippingDtls",shippingDtls);
				request.setAttribute("MID",MID);
				request.setAttribute("submitTo",submitTo);
				
				
				
		return "success";
		
	}
	
	
	public String paymentGatewayB2C(){		
		logger.debug("**********************  calling paymentGateway() **************************");
		User user=(User)session.get("User");
		if(user == null || user.getId() == null)
		{
			session.put("loginStatus", "fail");
			return "fail";
		}
		String aggId=(String)session.get("aggId");
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			return "fail";
		}		
		
		if(inputBean.getTrxAmount() <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError("There is some problem please try again.");
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid mobile no.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError("Transaction failed, Wallet balance violated.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError("Transaction fail.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid reciver mobile number.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError("Transaction failed due to insufficient balance.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PaymentGateway.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		WalletConfiguration config1=new WalletConfiguration();
		config1.setAggreatorid(aggId);
		
		
		Map<String,String> mapResult=service.getWalletConfigPG(config1);
		
		
		//Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
		String appName=mapResult.get("pgAgId");
		String userName=mapResult.get("pgAgId");
		String MID=mapResult.get("pgMid");
		
		String successURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/PGResponseB2CRecharge.action";
		String	failURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/PGResponseB2CRecharge.action";
		
		logger.debug("**********************  CallBackFailURL**************************"+successURL);
		logger.debug("**********************  failURL**************************"+failURL);
		
		String submitTo=mapResult.get("pgUrl");
		String securitykey=mapResult.get("pgEncKey");
//		String TxnType=mapResult.get("pgTxnType");
//		String country=mapResult.get("pgCountry");
//		String currency=mapResult.get("pgCurrency");
		//String Channel="WEB";
		
//		String operatingMode=prop.getProperty("operatingMode");
//		String otherDetails=prop.getProperty("otherDetails");
//		String collaborator=prop.getProperty("collaborator");
				
				
				String requestParameter="Appname="+appName+"|TransID="+txnId+"|Amount="+inputBean.getTrxAmount()+"|Appuser="+userName+"|CallBackURL="+successURL+"|MID="+MID;
				PGDetails pgDetails=new PGDetails();
				String billingDtls = pgDetails.getBillingDtls();
logger.debug("***********************Before encryption parameters **********************************");				
logger.debug("request parameter for paymentgateway  "+requestParameter);
logger.debug("pgdetails    "+pgDetails);
logger.debug("billing details    "+billingDtls);

			    if(user.getEmailid()!=null&&user.getEmailid().length()>0){
			    	billingDtls=billingDtls.replace("sandeep.prajapati@appnittech.com",user.getEmailid());
			    }
			   /* if(user.getName()!=null&&user.getName().length()>0){
			    	billingDtls=billingDtls.replace("Appnit Technologies",user.getName());
			    }*/
			    if(user.getMobileno()!=null&&user.getMobileno().length()>0){
			    	billingDtls=billingDtls.replace("9335235731",user.getMobileno());
			    }
			    
				String shippingDtls = pgDetails.getShippingDtls();
				//String key = Encryption.getEncryptionKey("rechappPG");
				
				System.out.println(pgDetails.getBillingDtls());
				System.out.println(pgDetails.getShippingDtls());
				System.out.println(requestParameter);
				requestParameter = AES128Bit.encrypt(requestParameter, securitykey);
				billingDtls = AES128Bit.encrypt(billingDtls, securitykey);
				shippingDtls = AES128Bit.encrypt(shippingDtls, securitykey);
				requestParameter = requestParameter.replaceAll("\n", "");
				billingDtls = billingDtls.replaceAll("\n", "");
				shippingDtls = shippingDtls.replaceAll("\n", "");
				logger.debug("***********************after encryption parameters **********************************");				
				logger.debug("request parameter for paymentgateway  "+requestParameter);
				logger.debug("pgdetails    "+pgDetails);
				logger.debug("billing details    "+billingDtls);
				logger.debug("MID is  "+MID);
				request.setAttribute("requestparameter",requestParameter);
				request.setAttribute("billingDtls",billingDtls);
				request.setAttribute("shippingDtls",shippingDtls);
				request.setAttribute("MID",MID);
				request.setAttribute("submitTo",submitTo);
				
				
				
		return "success";
		
	}
	
	
	public String pgResponse() {
		logger.debug("********************************************response come from payment gateway  pgResponse()********************************************");
		User user=null;
		StringBuffer mobileAppResponse = new StringBuffer();
		String responseparams = request.getParameter("responseparams");
		String hash=request.getParameter("hash");
		Properties paymentProp = null;

		logger.debug("************* response received from Payment gateway  pgResponse()  returnCallPyementGateway = "+responseparams);
		
		String result = "fail";
		String msg="";

		Logger logger = Logger.getLogger(this.getClass().getName());
		logger.debug("********************************************** response come from Payment gateway = "
				+ responseparams);
		if (responseparams == null) {
			logger.debug("********************************************** getting resp Parameter as null so recharge could not be done   ****************************************");
			msg="Payment fialed.";
			return result;
		} else {

			paymentProp = new Properties();
			StringTokenizer stokz = new StringTokenizer(responseparams, "|");
			while (stokz.hasMoreTokens()) {
				String tok = (String) stokz.nextToken();
				paymentProp.put(tok.substring(0, tok.indexOf('=')),
						tok.substring(tok.indexOf('=') + 1));
			}
			String id=null;
			String trxId=null;
			String amount="0.0";
			String method = request.getMethod();
			if (method.equalsIgnoreCase("GET")) {
				logger.debug("********************************************** request coming from get method    ****************************************");
				msg="Payment fialed.";
				result = "fail";
				
			} else {
				logger.debug("********************************************response come from post method ********************************************");
				id = paymentProp.getProperty("plutusTxnId");
				String status = paymentProp.getProperty("status");
				String code = paymentProp.getProperty("responseCode");
				String otherDetails = paymentProp.getProperty("appName");
				trxId = paymentProp.getProperty("appTransId");
				amount = paymentProp.getProperty("amount");
				
				if (status != null && status.contains("success")|| code.equals("01")) {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
					msg="Amount successfully added to your wallet.";
					result="success";
					//return "success";
				} else {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");

					msg="Payment failed.";
					result = "fail";
					//return "fail";
				}
				logger.debug("*******************************calling service profileBytxnid and getwalletconfig*************************************");

				Map<String, String> resultMap=uService.ProfilebytxnId(trxId);
				WalletConfiguration conf=new WalletConfiguration();
				if(Double.parseDouble(String.valueOf(resultMap.get("usertype")))==4){
				conf.setAggreatorid(resultMap.get("id"));
				}
				else{
					conf.setAggreatorid(resultMap.get("aggreatorid"));
				}
				Map<String,String> config=uService.getWalletConfig(conf);
				if(config==null||config.size()==0){
					logger.debug("*******************************getting wallet config as null******************************");
					msg="An error has occurred please try again.";
					return "fail";
				}
				logger.debug("**********************************creating user session*************************************");
				
				createUserSession(resultMap, config,conf.getAggreatorid());
			    user=(User)session.get("User");
			    logger.debug("**********************************calling service updatePGTrx*************************************");
				String updateResult=tService.updatePGTrx(hash,responseparams,user.getId(),trxId,id,Double.parseDouble(amount), result, "");
				logger.debug("********************************getting response from service ************************************"+updateResult);
				if(updateResult.equalsIgnoreCase("1001")){
				msg="Payment failed.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7029")){
				msg="Payment failed from payment gateway.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("1000")){
				msg="Amount successfully added";
				result="success";
				}
				if(updateResult.equalsIgnoreCase("7000")){
				msg="An error has occurred.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7015")){
				msg="Payment failed.";
				result="fail";
				}
			}
			if(result.equalsIgnoreCase("success")){
				if(user!=null&&user.getEmailid()!=null){
					UserSummary usummery=new UserSummary();
					usummery.setUserId(user.getEmailid());
					usummery.setAggreatorid(user.getAggreatorid());
				Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
				updateWalletDetails(resultMap);
				}
				addActionMessage(msg);
			}
			else{
				addActionError(msg);
			}
			logger.debug("**********************************returning result *************************************"+result);
			return result;
		}
	

	}
	
	
	
	
	
	public String pGOpen(){
		
		logger.debug("**********************  calling paymentGateway() **************************");
		User user=(User)session.get("User");
		String aggId=(String)session.get("aggId");
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			return "fail";
		}		
		
		if(inputBean.getTrxAmount() <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("100111"));
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("70231"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError(prop.getProperty("17048"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}
		
		
		
		
		inputBean.setTxnId(result);		
		
		logger.debug("**********************  pGOpen calling  returning success**************************");
	return "success";		
	}	
	
	public String openPaymentGateway(){
		PaymentParameter paymentParameter=(PaymentParameter)session.get("PaymentParameter");                                    
		User user=(User)session.get("User");
		inputBean.setTxnId(paymentParameter.getTxnId());
		request.setAttribute("client","tparty");
		return "success";
	}
	
	public String paygatePaymentGateway(){	
		/*if(inputBean.getBankid()==null||inputBean.getBankid().equalsIgnoreCase("-1")||inputBean.getBankid().isEmpty()){
			addActionError("please select bank.");
			return "fail";
		}*/
		
		logger.debug("**********************  calling paygate paymentGateway() **************************");
		User user=(User)session.get("User");
		String aggId=(String)session.get("aggId");
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			return "fail";
		}		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("100111"));
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError(prop.getProperty("17048"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}
				
		inputBean.setTxnId(result);		

		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PayGate.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
				Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
				
						String AGID=mapResult.get("pgAgId");
						String MID=mapResult.get("pgMid");
						String CallBackFailURL=mapResult.get("pgCallBackUrl")+"/DigitalWallet/PaygatePgResponse.action";
						String	CallBackSuccessURL=mapResult.get("pgCallBackUrl")+"/DigitalWallet/PaygatePgResponse.action";
						
						logger.debug("**********************  CallBackFailURL**************************"+CallBackFailURL);
						logger.debug("**********************  CallBackSuccessURL**************************"+CallBackSuccessURL);
						
						String SubmitTo=mapResult.get("pgUrl");
						String EncKey=mapResult.get("pgEncKey");
						String TxnType=mapResult.get("pgTxnType");
						String Country=mapResult.get("pgCountry");
						String Currency=mapResult.get("pgCurrency");
						String Channel="WEB";
						
						

						TDSecurePayGateObject secureObject=new TDSecurePayGateObject();
						
						CustomerDtls cusDtls=new CustomerDtls();
						cusDtls.setCustName(user.getName());
						cusDtls.setCustEmailId(user.getEmailid());
						cusDtls.setCustMobileNo(user.getMobileno());
						cusDtls.setCustAddress("Sector XX");
						cusDtls.setCustCity("NOIDA");
						cusDtls.setCustCountry("INDIA");
						cusDtls.setCustPinCode("201308");
						
						ShippingDtls shiDtls=new ShippingDtls();
						
						shiDtls.setDeliveryName("Digital Wallet");
						shiDtls.setDeliveryMobileNo(user.getMobileno());
						shiDtls.setDeliveryPinCode("201301");
						shiDtls.setDeliveryAddress("");
						shiDtls.setDeliveryCity("");
						shiDtls.setDeliveryCountry("");
						shiDtls.setDeliveryPhNo1("");
						shiDtls.setDeliveryState("");
						
						secureObject.setCustdtls(cusDtls);
						secureObject.setShipdtls(shiDtls);
						
						PayGateHandler d2Phandler=new PayGateHandler();				
						
						secureObject.setAg_id(AGID);
						secureObject.setMe_id(MID);
						secureObject.setOrder_no(inputBean.getTxnId());
						secureObject.setAmount(inputBean.getTrxAmount());
						secureObject.setCurrency(Currency);
						secureObject.setCountry(Country);
						secureObject.setTxn_type(TxnType);
						secureObject.setSuccess_url(CallBackSuccessURL);
						secureObject.setFailure_url(CallBackFailURL);
						secureObject.setChannel(Channel);
						secureObject.setEncKey(EncKey);
						secureObject.setTargetUrl(SubmitTo);
//						secureObject.setPg_id(Integer.parseInt(inputBean.getBankid()));
//						secureObject.setPaymode("NB");
						//secureObject.setRequestParams(d2Phandler.generateTransStr4NB(secureObject));
						
						secureObject.setTxn_details(d2Phandler.generateTxnDetails4NB(secureObject));
						secureObject.setPg_details(d2Phandler.generatePgDetails4NB(secureObject));
						secureObject.setCard_details(d2Phandler.generateCardDetails4NB(secureObject));
						secureObject.setCust_details(d2Phandler.generateCustDetails4NB(secureObject));
						secureObject.setBill_details(d2Phandler.generateBillDetails4NB(secureObject));
						secureObject.setShip_details(d2Phandler.generateShipDetails4NB(secureObject));
						secureObject.setItem_details(d2Phandler.generateItemDetails4NB(secureObject));
						secureObject.setOther_details(d2Phandler.generateOtherDetails4NB(secureObject));			
				
				
				logger.debug("***********************after encryption parameters **********************************");				
				
				session.put("secureObject",secureObject);
		return "success";
		
	}
	
	
	public String walletPaymentGateway(){
		
		logger.debug("****************************walletPaymentGateway************************************");
		logger.debug(inputBean.getTrxAmount()+"====================================================================");
		System.out.println(inputBean.getTrxAmount()+"====================================================================");
		String aggId=(String)session.get("aggId");
        PaymentParameter paymentParameter=(PaymentParameter)session.get("PaymentParameter");
        User user=(User)session.get("User");
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("******************user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()********************");
			addActionError("Please try again..");
			return "fail";
		}
		
		logger.debug("***************calling service savePGTrx*******************************");
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		if(inputBean.getTrxAmount() <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money",paymentParameter.getTxnId(),aggId,ipAddress,userAgent);
		logger.debug("***************getting response from service***************************"+result);
		if(result.equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("100111"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError(prop.getProperty("17048"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PayGate.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");

//		WalletCallBackFailURL=http://125.99.51.226:82/DigitalWallet/WalletPGResponse.action
//			WalletCallBackSuccessURL=http://125.99.51.226:82/DigitalWallet/WalletPGResponse.action
				String AGID=mapResult.get("pgAgId");
				String MID=mapResult.get("pgMid");
				String CallBackFailURL=mapResult.get("pgCallBackUrl")+"/DigitalWallet/WalletPGResponse.action";
				String	CallBackSuccessURL=mapResult.get("pgCallBackUrl")+"/DigitalWallet/WalletPGResponse.action";
				String SubmitTo=mapResult.get("pgUrl");
				String EncKey=mapResult.get("pgEncKey");
				String TxnType=mapResult.get("pgTxnType");
				String Country=mapResult.get("pgCountry");
				String Currency=mapResult.get("pgCurrency");
				String Channel="WEB";

		TDSecurePayGateObject secureObject=new TDSecurePayGateObject();
		
		CustomerDtls cusDtls=new CustomerDtls();
		cusDtls.setCustName(user.getName());
		cusDtls.setCustEmailId(user.getEmailid());
		cusDtls.setCustMobileNo(user.getMobileno());
		cusDtls.setCustAddress("Sector XX");
		cusDtls.setCustCity("NOIDA");
		cusDtls.setCustCountry("INDIA");
		cusDtls.setCustPinCode("201308");
		
		ShippingDtls shiDtls=new ShippingDtls();
		
		shiDtls.setDeliveryName("Digital Wallet");
		shiDtls.setDeliveryMobileNo(user.getMobileno());
		shiDtls.setDeliveryPinCode("201301");
		shiDtls.setDeliveryAddress("");
		shiDtls.setDeliveryCity("");
		shiDtls.setDeliveryCountry("");
		shiDtls.setDeliveryPhNo1("");
		shiDtls.setDeliveryState("");
		
		secureObject.setCustdtls(cusDtls);
		secureObject.setShipdtls(shiDtls);
		
		PayGateHandler d2Phandler=new PayGateHandler();				
		
		secureObject.setAg_id(AGID);
		secureObject.setMe_id(MID);
		secureObject.setOrder_no(txnId);
		secureObject.setAmount(inputBean.getTrxAmount());
		secureObject.setCurrency(Currency);
		secureObject.setCountry(Country);
		secureObject.setTxn_type(TxnType);
		secureObject.setSuccess_url(CallBackSuccessURL);
		secureObject.setFailure_url(CallBackFailURL);
		secureObject.setChannel(Channel);
		secureObject.setEncKey(EncKey);
		secureObject.setTargetUrl(SubmitTo);
		secureObject.setPg_id(Integer.parseInt(inputBean.getBankid()));
		secureObject.setPaymode("NB");
		//secureObject.setRequestParams(d2Phandler.generateTransStr4NB(secureObject));
	
		secureObject.setTxn_details(d2Phandler.generateTxnDetails4NB(secureObject));
		secureObject.setPg_details(d2Phandler.generatePgDetails4NB(secureObject));
		secureObject.setCard_details(d2Phandler.generateCardDetails4NB(secureObject));
		secureObject.setCust_details(d2Phandler.generateCustDetails4NB(secureObject));
		secureObject.setBill_details(d2Phandler.generateBillDetails4NB(secureObject));
		secureObject.setShip_details(d2Phandler.generateShipDetails4NB(secureObject));
		secureObject.setItem_details(d2Phandler.generateItemDetails4NB(secureObject));
		secureObject.setOther_details(d2Phandler.generateOtherDetails4NB(secureObject));			

System.out.println(secureObject.getTxn_details());
System.out.println(secureObject.getPg_details());
System.out.println(secureObject.getCard_details());
System.out.println(secureObject.getCust_details());
System.out.println(secureObject.getBill_details());
System.out.println(secureObject.getShip_details());
System.out.println(secureObject.getItem_details());
System.out.println(secureObject.getOther_details());
logger.debug("***********************after encryption parameters **********************************");				

session.put("secureObject",secureObject);
				
		return "success";
		
	}
	
		
	public String paygatePgResponse() {
		return "";
		/*
		WalletConfiguration config3=new WalletConfiguration();
		String domainName=request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).substring(0, request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).indexOf("/"));
		session.put("domainName",domainName);
		config3.setDomainName(domainName);
		String aggId=uService.getAggIdByDomain(config3);
		config3.setAggreatorid(aggId);
		Map MapResult3=uService.getWalletConfig(config3);
		String encKey=(String)MapResult3.get("pgEncKey");
		
		logger.debug("********************************************response come from paygate payment gateway********************************************");
		User user = null;
		String result = "fail";
		String msg = "";
		Logger logger = Logger.getLogger(this.getClass().getName());
		String txn_response = request.getParameter("txn_response");
		String pg_details = request.getParameter("pg_details");
		String fraud_details = request.getParameter("fraud_details");
		String other_details = request.getParameter("other_details");
		logger.debug("**********************************************txn_response:-"
				+ txn_response);
		logger.debug("**********************************************pg_details:-"
				+ pg_details);
		logger.debug("**********************************************fraud_details:-"
				+ fraud_details);
		logger.debug("**********************************************other_details:-"
				+ other_details);

		if (txn_response != null) {
			txn_response = decryptString(txn_response,encKey);
		}
		if (pg_details != null) {
			pg_details = decryptString(pg_details,encKey);
		}
		if (fraud_details != null) {
			fraud_details = decryptString(fraud_details,encKey);
		}
		if (other_details != null) {
			other_details = decryptString(other_details,encKey);
		}
		logger.debug("**********************************************txn_response:-"
				+ txn_response);
		logger.debug("**********************************************pg_details:-"
				+ pg_details);
		logger.debug("**********************************************fraud_details:-"
				+ fraud_details);
		logger.debug("**********************************************other_details:-"
				+ other_details);

		// paygate|201608030010|10207|10.0|IND|INR|2016-08-12|12:46:37|7937916890741261|0|Failed|NB|00026|Invalid
		// Payment Gateway
		String ag_id = "";
		String me_id = "";
		String order_no = "";
		String amount = "";
		String country = "";
		String currency = "";
		String txn_date = "";
		String txn_time = "";
		String ag_ref = "";
		String pg_ref = "";
		String statusfrompaygate = "";// �Successful�, �Failed�, �Hold� or
										// �Pending�
		String res_code = "";// 0 for Success.
		String res_message = "";
		StringTokenizer token = new StringTokenizer(txn_response, "|");
		if (token.hasMoreElements())
			ag_id = token.nextToken();
		if (token.hasMoreElements())
			me_id = token.nextToken();
		if (token.hasMoreElements())
			order_no = token.nextToken();
		if (token.hasMoreElements())
			amount = token.nextToken();
		if (token.hasMoreElements())
			country = token.nextToken();
		if (token.hasMoreElements())
			currency = token.nextToken();
		if (token.hasMoreElements())
			txn_date = token.nextToken();
		if (token.hasMoreElements())
			txn_time = token.nextToken();
		if (token.hasMoreElements())
			ag_ref = token.nextToken();
		if (token.hasMoreElements())
			pg_ref = token.nextToken();
		if (token.hasMoreElements())
			statusfrompaygate = token.nextToken();
		if (token.hasMoreElements())
			res_code = token.nextToken();
		if (token.hasMoreElements())
			res_message = token.nextToken();

		String authorisationStatus = statusfrompaygate;
		if (authorisationStatus.indexOf("Success") > -1) {
			msg = "Amount successfully added to your wallet.";
			result = "success";
		} else if (authorisationStatus.indexOf("Failed") > -1) {
			msg = "Transaction failed.";
			result="fail";
		}

		logger.debug("********************************************response come from post method ********************************************");

		logger.debug("*******************************calling service profileBytxnid and getwalletconfig*************************************");

		Map<String, String> resultMap = uService.ProfilebytxnId(order_no);
		WalletConfiguration conf=new WalletConfiguration();
		if(Double.parseDouble(String.valueOf(resultMap.get("usertype")))==4){
			conf.setAggreatorid(resultMap.get("id"));
			}
			else{
				conf.setAggreatorid(resultMap.get("aggreatorid"));
			}
		Map<String, String> config = uService.getWalletConfig(conf);
		if (config == null || config.size() == 0) {
			logger.debug("*******************************getting wallet config as null******************************");
			msg = "An error has occurred please try again.";
			return "fail";
		}
		logger.debug("**********************************creating user session*************************************");
		createUserSession(resultMap, config,conf.getAggreatorid());
		user = (User) session.get("User");
		logger.debug("**********************************calling service updatePGTrx*************************************");
		String updateResult = tService.updatePGTrx(user.getId(), order_no,
				pg_ref, Double.parseDouble(amount), result, "");
		logger.debug("********************************getting response from service ************************************"
				+ updateResult);
		if (updateResult.equalsIgnoreCase("1001")) {
			msg = prop.getProperty("11117015");
			result = "fail";
		}
		if (updateResult.equalsIgnoreCase("7029")) {
			msg = prop.getProperty("11117029");
			result = "fail";
		}
		if (updateResult.equalsIgnoreCase("1000")) {
			msg = "Amount successfully added";
			result = "success";
		}
		if (updateResult.equalsIgnoreCase("7000")) {
			msg = prop.getProperty("7000");
			result = "fail";
		}
		if (updateResult.equalsIgnoreCase("7015")) {
			msg = prop.getProperty("11117015");
			result = "fail";
		}

		if (result.equalsIgnoreCase("success")) {
			if (user != null && user.getEmailid() != null) {
				UserSummary usummery=new UserSummary();
				usummery.setUserId(user.getEmailid());
				usummery.setAggreatorid(user.getAggreatorid());
				Map<String, String> resultMap1 = uService.ProfilebyloginId(usummery);
				updateWalletDetails(resultMap1);
			}
			addActionMessage(msg);
		} else {
			addActionError(msg);
		}
		logger.debug("**********************************returning result *************************************"
				+ result);
		return result;
	*/}
	


	
	public String walletPgResponse() {
		
		return "";/*

		WalletConfiguration config3=new WalletConfiguration();
		String domainName=request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).substring(0, request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).indexOf("/"));
		session.put("domainName",domainName);
		config3.setDomainName(domainName);
		String aggId=uService.getAggIdByDomain(config3);
		config3.setAggreatorid(aggId);
		Map MapResult3=uService.getWalletConfig(config3);
		String encKey=(String)MapResult3.get("pgEncKey");
		
		
		logger.debug("********************************************response come from paygate payment gateway********************************************");
		User user = null;
		String result = "fail";
		String msg = "";
		Logger logger = Logger.getLogger(this.getClass().getName());
		String txn_response = request.getParameter("txn_response");
		String pg_details = request.getParameter("pg_details");
		String fraud_details = request.getParameter("fraud_details");
		String other_details = request.getParameter("other_details");
		logger.debug("**********************************************txn_response:-"
				+ txn_response);
		logger.debug("**********************************************pg_details:-"
				+ pg_details);
		logger.debug("**********************************************fraud_details:-"
				+ fraud_details);
		logger.debug("**********************************************other_details:-"
				+ other_details);

		if (txn_response != null) {
			txn_response = decryptString(txn_response,encKey);
		}
		if (pg_details != null) {
			pg_details = decryptString(pg_details, encKey);
		}
		if (fraud_details != null) {
			fraud_details = decryptString(fraud_details, encKey);
		}
		if (other_details != null) {
			other_details = decryptString(other_details, encKey);
		}
		logger.debug("**********************************************txn_response:-"
				+ txn_response);
		logger.debug("**********************************************pg_details:-"
				+ pg_details);
		logger.debug("**********************************************fraud_details:-"
				+ fraud_details);
		logger.debug("**********************************************other_details:-"
				+ other_details);

		// paygate|201608030010|10207|10.0|IND|INR|2016-08-12|12:46:37|7937916890741261|0|Failed|NB|00026|Invalid
		// Payment Gateway
		String ag_id = "";
		String me_id = "";
		String order_no = "";
		String amount = "";
		String country = "";
		String currency = "";
		String txn_date = "";
		String txn_time = "";
		String ag_ref = "";
		String pg_ref = "";
		String statusfrompaygate = "";// �Successful�, �Failed�, �Hold� or
										// �Pending�
		String res_code = "";// 0 for Success.
		String res_message = "";
		StringTokenizer token = new StringTokenizer(txn_response, "|");
		if (token.hasMoreElements())
			ag_id = token.nextToken();
		if (token.hasMoreElements())
			me_id = token.nextToken();
		if (token.hasMoreElements())
			order_no = token.nextToken();
		if (token.hasMoreElements())
			amount = token.nextToken();
		if (token.hasMoreElements())
			country = token.nextToken();
		if (token.hasMoreElements())
			currency = token.nextToken();
		if (token.hasMoreElements())
			txn_date = token.nextToken();
		if (token.hasMoreElements())
			txn_time = token.nextToken();
		if (token.hasMoreElements())
			ag_ref = token.nextToken();
		if (token.hasMoreElements())
			pg_ref = token.nextToken();
		if (token.hasMoreElements())
			statusfrompaygate = token.nextToken();
		if (token.hasMoreElements())
			res_code = token.nextToken();
		if (token.hasMoreElements())
			res_message = token.nextToken();

		String authorisationStatus = statusfrompaygate;
		if (authorisationStatus.indexOf("Success") > -1) {
			logger.debug("**********************************getting response from payment gateway as"+authorisationStatus+"************************************");
			msg="Amount successfully added to your wallet.";
			result="success";
		} else if (authorisationStatus.indexOf("Failed") > -1) {
			logger.debug("**********************************getting response from payment gateway as "+authorisationStatus+"************************************");
			msg="Payment failed.";
			result = "fail";
		}

		logger.debug("********************************************response come from post method ********************************************");

		logger.debug("*******************************calling service profileBytxnid and getwalletconfig*************************************");

	
				logger.debug("**********************************calling service profilebytxnid and profilebypgtxnid and getwalletconfig************************************");
				Map<String, String> resultMap=uService.ProfilebytxnId(order_no);
				PGPayeeBean payeeBean=uService.profilebyPGtxnId(order_no);
				WalletConfiguration conf=new WalletConfiguration();
				if(Double.parseDouble(String.valueOf(resultMap.get("usertype")))==4){
					conf.setAggreatorid(resultMap.get("id"));
					}
					else{
						conf.setAggreatorid(resultMap.get("aggreatorid"));
					}
				Map<String,String> config=uService.getWalletConfig(conf);
				
				PaymentParameter pp=new PaymentParameter();
				
				
				pp.setMid(payeeBean.getMerchantid());
				pp.setTxnId(payeeBean.getPaytxnid());
				pp.setAmount(payeeBean.getAmount());
				pp.setMerchantUserName(payeeBean.getMerchantuser());
				pp.setCallBackURL(payeeBean.getCallbackurl());
				pp.setMerchantTxnId(payeeBean.getMerchanttxnid());
				pp.setStatusCode("9999");
				pp.setStatusDesc("failed");
				
				if(config==null||config.size()==0){
					logger.debug("**********************************getting config as null***********************************************");
					msg="An error has occurred please try again.";
					result= "fail";
				}
				createUserSession(resultMap, config,conf.getAggreatorid());
			    user=(User)session.get("User");
			    logger.debug("**********************************calling service updatePGTrx************************************");
				String updateResult=tService.updatePGTrx(user.getId(),order_no,pg_ref,Double.parseDouble(amount), result, "");
				logger.debug("**********************************getting result from service="+updateResult+"************************************");
				if(updateResult.equalsIgnoreCase("1000")){
					
					MerchentBean merchentBean=new MerchentBean();
					merchentBean.setWalletId(user.getWalletid());
					merchentBean.setUserId(user.getId());
					merchentBean.setTxnId(pp.getTxnId());
					merchentBean.setIpImei("");
					merchentBean.setMid(pp.getMid());
					merchentBean.setAmount(pp.getAmount());
					*//**deducting amount from wallet**//*
					String userAgent=request.getHeader("User-Agent");
					String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
					if (ipAddress == null) {
						   ipAddress = request.getRemoteAddr();
					}
					String paymentResult=mService.merchentPaymet(merchentBean,ipAddress,userAgent);
					if(paymentResult!=null&&paymentResult.equalsIgnoreCase("1000")){
						pp.setStatusCode(paymentResult);
						pp.setStatusDesc("success");
						session.put("PaymentParameter", pp);
						result="success";
					}else{	
						pp.setStatusCode(paymentResult);
						pp.setStatusDesc("failed");
						session.put("PaymentParameter", pp);
						result="fail";
					}
				
				
				}else{
				pp.setStatusCode(updateResult);
				pp.setStatusDesc("failed");
				result="fail";
				}
			
			logger.debug("**********************************returing result is "+result+"************************************");
		return result;*/	
		

	}
	
	public void createUserSession(Map<String,String> resultMap,Map<String,String> config,String aggId){
		logger.debug("*********************************user session creating************************************");
		
		
		/*************************for menu options***************************/
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> res=commService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(res.get("PLAN"));
			setTxnType(res.get("TXNTYPE"));
		}
		
		session.put("menuMap", res.get("TXNTYPE"));
		session.put("aggId",aggId);
		/*************************end menu options***************************/
		
		
		User user=new User();
		
		String userImg=uService.getProfilePic(resultMap.get("id"));
		user.setUserImg(userImg);
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setAgentid(resultMap.get("agentid"));
		user.setSubAgentId(resultMap.get("subAgentId"));
		user.setDistributerid(resultMap.get("distributerid"));
		user.setSuperdistributerid(resultMap.get("superdistributerid")!=null?resultMap.get("superdistributerid"):"-1");
		user.setAggreatorid(resultMap.get("aggreatorid"));
		user.setToken(resultMap.get("token"));
		user.setTokenSecurityKey(resultMap.get("tokenSecurityKey"));
		user.setKycStatus(resultMap.get("kycStatus"));
		user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
		user.setWhiteLabel(Integer.parseInt(resultMap.get("whiteLabel")));
		user.setWallet(Integer.parseInt(resultMap.get("wallet")));
		user.setBank(Integer.parseInt(resultMap.get("bank")));
		user.setPortalMessage(resultMap.get("portalMessage")!=null?resultMap.get("portalMessage"):"");
		user.setAgentCode(resultMap.get("agentCode")!=null?resultMap.get("agentCode"):"");
		user.setAsCode(resultMap.get("asCode")!=null?resultMap.get("asCode"):"");
		user.setAsAgentCode(resultMap.get("asAgentCode")!=null?resultMap.get("asAgentCode"):"");
		user.setShopName(resultMap.get("shopName")!=null?resultMap.get("shopName"):"");
		user.setAepsChannel(resultMap.get("aepsChannel")!=null?resultMap.get("aepsChannel"):"");
		user.setBank1(resultMap.get("bank1")!=null?resultMap.get("bank1"):"1");
		user.setBank2(resultMap.get("bank2")!=null?resultMap.get("bank2"):"1");
		user.setBank3(resultMap.get("bank3")!=null?resultMap.get("bank3"):"1");
		user.setBank3_via(resultMap.get("bank3_via")!=null?resultMap.get("bank3_via"):"1");
		user.setOnlineMoney(resultMap.get("onlineMoney")!= null ? Integer.parseInt(String.valueOf(resultMap.get("onlineMoney")).substring(0, 1)) : Integer.parseInt("0"));
		user.setRecharge(resultMap.get("recharge")!= null ? Integer.parseInt(String.valueOf(resultMap.get("recharge")).substring(0, 1)) : Integer.parseInt("0") );
		user.setBbps(resultMap.get("bbps")!= null ? Integer.parseInt(String.valueOf(resultMap.get("bbps")).substring(0, 1)) : Integer.parseInt("0") );

		user.setCountry(config.get("country"));
		user.setCountrycurrency(config.get("countrycurrency"));
		user.setCountryid(Double.parseDouble(String.valueOf(config.get("countryid"))));
		user.setEmailvalid(Double.parseDouble(String.valueOf(config.get("emailvalid"))));
		user.setKycvalid(Double.parseDouble(String.valueOf(config.get("kycvalid"))));
		user.setOtpsendtomail(Double.parseDouble(String.valueOf(config.get("otpsendtomail"))));
		user.setSmssend(Double.parseDouble(String.valueOf(config.get("smssend"))));
		user.setStatus(Double.parseDouble(String.valueOf(config.get("status"))));
		
		user.setLogo(config.get("logopic"));
		user.setCustomerCare(config.get("customerCare")!=null?config.get("customerCare"):"0120-4000004");
		user.setSupportEmailId(config.get("supportEmailId")!=null?config.get("supportEmailId"):"");
		user.setBanner(config.get("bannerpic"));
		session.put("logo",config.get("logopic"));
		session.put("banner",config.get("bannerpic"));
		
		session.put("User",user);
//		session.put("serviceMaster",config.get("serviceMaster"));
		session.put("mapResult",config);
		logger.debug("*********************************user session created successfully************************************");
	}
	
	public void updateWalletDetails(Map<String,String> resultMap){
		logger.debug("*********************************updateing walletDetails************************************");
		User user=(User)session.get("User");
		
		
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setAgentid(resultMap.get("agentid"));
		user.setSubAgentId(resultMap.get("subAgentId"));
		user.setDistributerid(resultMap.get("distributerid"));
		user.setSuperdistributerid(resultMap.get("superdistributerid")!=null?resultMap.get("superdistributerid"):"-1");
		user.setAggreatorid(resultMap.get("aggreatorid"));
		user.setKycStatus(resultMap.get("kycStatus"));
		user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
		user.setWhiteLabel(Integer.parseInt(resultMap.get("whiteLabel")!=null?resultMap.get("whiteLabel"):"0"));
		user.setWallet(Integer.parseInt(resultMap.get("wallet")));
		user.setBank(Integer.parseInt(resultMap.get("bank")));
		user.setPortalMessage(resultMap.get("portalMessage")!=null?resultMap.get("portalMessage"):"");
		user.setAgentCode(resultMap.get("agentCode")!=null?resultMap.get("agentCode"):"");
		user.setAsCode(resultMap.get("asCode")!=null?resultMap.get("asCode"):"");
		user.setShopName(resultMap.get("shopName")!=null?resultMap.get("shopName"):"");
		user.setAsAgentCode(resultMap.get("asAgentCode")!=null?resultMap.get("asAgentCode"):"");
		user.setAepsChannel(resultMap.get("aepsChannel")!=null?resultMap.get("aepsChannel"):"");
		user.setBank1(resultMap.get("bank1")!=null?resultMap.get("bank1"):"1");
		user.setBank2(resultMap.get("bank2")!=null?resultMap.get("bank2"):"1");
		user.setBank3(resultMap.get("bank3")!=null?resultMap.get("bank3"):"1");
		user.setBank3_via(resultMap.get("bank3_via")!=null?resultMap.get("bank3_via"):"1");
		user.setOnlineMoney(resultMap.get("onlineMoney")!= null ? Integer.parseInt(String.valueOf(resultMap.get("onlineMoney")).substring(0, 1)) : Integer.parseInt("0"));
		user.setRecharge(resultMap.get("recharge")!= null ? Integer.parseInt(String.valueOf(resultMap.get("recharge")).substring(0, 1)) : Integer.parseInt("0") );
		user.setBbps(resultMap.get("bbps")!= null ? Integer.parseInt(String.valueOf(resultMap.get("bbps")).substring(0, 1)) : Integer.parseInt("0") );

		session.put("User",user);
//		session.put("serviceMaster",resultMap.get("serviceMaster"));
		logger.debug("*********************************successfully updated walletdetails************************************");
	}
	
	
	public String showPassbook(){
		generateCsrfToken();
		User us=(User)session.get("User");
		inputBean.setWalletId(us.getWalletid());
		logger.debug("*********************************calling show passbook************************************");
		//tService.showPassbook(inputBean);
		List<PassbookBean> listResult=tService.showPassbook(inputBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}

public String getAmountPopup() {
		String txnIdPassed=request.getParameter("txnId");
		String res=tService.getAmountPopup(txnIdPassed);
		if(res.equals("something went wrong.")) {
			try {
				response.getWriter().write(res);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		else{
		JSONObject json =new JSONObject();
		String updatedResponse=res.replace("[", " ").replace("]", "");
		
		//["dfssf","100.0"]
				int in=res.indexOf(",");
				
		String agentId=res.substring(1, in-1);
		String txnAmount=res.substring(in+2, res.length()-2);

		System.out.println(res);
		json.put("AgentId", agentId);
		json.put("TxnAmount", txnAmount);
		
		try {
			response.getWriter().write(json.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
		return null;
	}
	
	public String pgTransactionsUpdate() {
		String responseMsg;
		User user = (User) session.get("User");
		String aggregatorID=user.getAggreatorid();
		TreeMap<String,String> obj=new TreeMap<String,String>();
		obj.put("userId",user.getId());
        obj.put("walletId",user.getWalletid());
        obj.put("aggregatorId",user.getAggreatorid());
        obj.put("token",user.getToken());
       
        
       try {
		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),obj);
		inputBean.setCHECKSUMHASH(checkSumString);
		
		System.out.println("map in starting : "+ obj);
		System.out.println("checksumString :  "+ checkSumString);
       } catch (Exception e) {
    	   responseMsg="Something went wrong.";
    	   request.setAttribute("response", response);
		return "success";
       }
		
		
//		if(aggregatorID.equals("OAGG001050")) {
			String txnId = (String) request.getParameter("txnId");
			inputBean.setTxnId(txnId);
			inputBean.setAggreatorid(aggregatorID);
			inputBean.setUserId(user.getId());
			inputBean.setWalletId(user.getWalletid());
			
			responseMsg = tService.pgTransactionsUpdate(inputBean);
//		}else {
//			responseMsg="You are not allowed to do this operation.";	
//			}
		System.out.println(user.getAgentCode());
		
		try {
//			JSONObject objJson=new JSONObject();
//			objJson.put("response",responseMsg);
			response.setContentType("text/plain");
			this.response.getWriter().print(responseMsg);
			this.response.getWriter().flush();
			this.response.getWriter().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getTxnDetails(){
		generateCsrfToken();
		User us=(User)session.get("User");
		String userId = us.getId();
		logger.debug("*********************************calling show passbook************************************");
		//tService.showPassbook(inputBean);
		List<TravelTxn> listResult=rService.getTxnDetails(userId);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	public String getflightTxnDetails(){
		generateCsrfToken();
		User us=(User)session.get("User");
		ReportBean reportBean = new ReportBean();
		String userId = us.getId();
		reportBean.setUserId(userId);
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling Flight Details************************************");
		//tService.showPassbook(inputBean);
		List<TravelTxn> listResult=rService.getflightTxnDetails(reportBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	public String passbookSummary(){
		logger.debug("*********************************calling passbook summary service************************************");
		User us=(User)session.get("User");
		inputBean.setWalletId(us.getWalletid());
		List<PassbookBean> listResult=tService.showPassbook(inputBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	
	public String ordersView(){
		generateCsrfToken();
		User us=(User)session.get("User");
		inputBean.setWalletId(us.getWalletid());
		logger.debug("*********************************calling orderview************************************");
		//tService.showPassbook(inputBean);
		List<PassbookBean> listResult=tService.showOrdersView(inputBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	
	public String requestRefund(){
		generateCsrfToken();
		User us=(User)session.get("User");
		inputBean.setWalletId(us.getWalletid());
		logger.debug("*********************************calling request refund************************************");
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		refundBean.setAggreatorId(us.getAggreatorid());
		refundBean.setUserId(us.getId());
		refundBean.setWalletId(us.getWalletid());
		
		refundBean=tService.requestRefund(refundBean,ipAddress, userAgent);		
		if(refundBean.getStatusCode().equalsIgnoreCase("1000")){
			addActionMessage("Request completed successfully.");	
		}
		if(refundBean.getStatusCode().equalsIgnoreCase("1001")){
			addActionError("Request not completed please try again.");	
		}
		if(refundBean.getStatusCode().equalsIgnoreCase("7000")){
			addActionError("There is some problem please try again.");	
		}
		if(refundBean.getStatusCode().equalsIgnoreCase("5504")){
			addActionError("Transaction id can not be blank");	
		}
		List<PassbookBean> listResult=tService.showOrdersView(inputBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	
	public String ordersViewSummary(){
		logger.debug("*********************************calling orderview summary************************************");
		List<PassbookBean> listResult=tService.showOrdersView(inputBean);
		request.setAttribute("listResult",listResult);
		return "success";
	}
	
	public String cashIn(){
		generateCsrfToken();
		logger.debug("*********************************calling cash in************************************");
		return "success";
	}

	public String cashOut(){
		generateCsrfToken();
		logger.debug("*********************************calling cash Out************************************");
		return "success";
	}
	
	public String cashInRequest(){
		logger.debug("*********************************calling cashInRequest************************************");
		String aggId=(String)session.get("aggId");
		inputBean.setAggreatorid(aggId);
		if(inputBean.getTrxAmount()==0){
			addActionError("Amount can not be zero or blank.");
			return "fail";
		}
		if(inputBean.getMobileNo()==null||inputBean.getMobileNo().isEmpty()){
			addActionError("Invalid mobile number.");
			return "fail";
		}
		logger.debug("*********************************calling service cashin************************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		
		String result=tService.cashIn(inputBean,ipAddress,userAgent);
		logger.debug("*********************************getting response from service  "+result+"************************************");
		if(result==null||result.isEmpty()){
			addActionError("An error has occurred.");
			return "fail";
		}
		if(result.equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("1000")){
			inputBean.setMobileNo("");
			inputBean.setTrxAmount(0.0);
			addActionMessage("Successfully sent.");
			User user=(User)session.get("User");
			logger.debug("*********************************calling service profilebyloginid************************************");
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			logger.debug("*********************************getting result from service "+resultMap+"************************************");

			updateWalletDetails(resultMap);
			return "success";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("7011"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7036")){
			addActionError(prop.getProperty("17036"));
			return "fail";
		}
		logger.debug("*********************************no condition matches return success by default***********************************");

		return "fail";
		
	}
	
	
	
	 public String cashOutRequest(){
		 generateCsrfToken();
	        logger.debug("**********************  cashOutRequest calling  **************************");
	        if(inputBean.getTrxAmount()==0){
	               addActionError("Amount can not be zero or blank.");
	               return "fail";
	        }
	        if(inputBean.getMobileNo()==null||inputBean.getMobileNo().isEmpty()){
	               addActionError("This Mobile number is not registered with us.");
	               return "fail";
	        }
	        
	        User user=(User)session.get("User");
	 if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
	               logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
	               addActionError("Please try again..");
	               return "customer";
	        }
	        
	        
	        logger.debug("**********************  calling service cashOutRequest **************************");
	        inputBean.setUserId(user.getId());
	        inputBean.setWalletId(user.getWalletid());
	        String aggId=(String)session.get("aggId");
	        inputBean.setAggreatorid(aggId);
	        
	        String userAgent=request.getHeader("User-Agent");
	        String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
	        AskMoneyBean result=tService.getCashOut(inputBean,ipAddress,userAgent);
	        
	        
	        
	        logger.debug("*********************************getting response from service  "+result+"************************************");
	        if(result.getStatusCode()==null||result.getStatusCode().isEmpty()){
	               addActionError(prop.getProperty("7000"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("1001")){
	               addActionError(prop.getProperty("17000"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("1000")){
	        	   setAmb(result);
	               addActionMessage("Enter the OTP which has been sent on Payer's mobile number.");
	               return "otpvalidate";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7032")){
	               addActionError(prop.getProperty("7032"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7025")){
	            addActionError(prop.getProperty("7025"));
	            return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7000")){
	               addActionError(prop.getProperty("17000"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7023")){
	               addActionError(prop.getProperty("7011"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7024")){
	               addActionError(prop.getProperty("17024"));
	               return "fail";
	        }
	        if(result.getStatusCode().equalsIgnoreCase("7042")){
	               addActionError(prop.getProperty("17024"));
	               return "fail";
	        }  
	        logger.debug("*********************************no condition matches return success by default***********************************");
	        
	        logger.debug("**********************  saveAskMoney calling  returning success**************************");
	        return "success";   
	 }
	 
	 public String cashOutUpdate(){
		 generateCsrfToken();
		 User us=(User)session.get("User");
		 amb.setId(us.getId());
		 AskMoneyBean ambResult=tService.cashOutUpdate(amb);
		 if(ambResult.getStatusCode().equalsIgnoreCase("1000")){
			
			 addActionMessage(" You have received Rs. "+ambResult.getReqAmount()+" from "+ambResult.getResMobile()+". Ref. No: "+ambResult.getTrxid());
				WalletConfiguration conf=new WalletConfiguration();
				conf.setAggreatorid(us.getAggreatorid());
				Map<String,String> config=uService.getWalletConfig(conf);
				user.setUserId(us.getMobileno());
				user.setAggreatorid(us.getAggreatorid());
				Map<String,String> resultMap=uService.ProfilebyloginId(user);
				createUserSession(resultMap, config, us.getAggreatorid());
			 
			 return "success";
		 }
		 if(ambResult.getStatusCode().equalsIgnoreCase("8802")){
			 addActionError(prop.getProperty("8802"));
			 return "fail";
		 }
 if(ambResult.getStatusCode().equalsIgnoreCase("8801")){
	 addActionError(prop.getProperty("8801"));
	 return "otpvalidate";
 }
 if(ambResult.getStatusCode().equalsIgnoreCase("7024")){
	 addActionError(prop.getProperty("70241"));
	 return "fail";
 }
 if(ambResult.getStatusCode().equalsIgnoreCase("7000")){
	 addActionError(prop.getProperty("70011"));
	 return "fail";
 }
 if(ambResult.getStatusCode().equalsIgnoreCase("1001")){
	 addActionError(prop.getProperty("10011111"));
	 return "fail";
 }
 if(ambResult.getStatusCode().equalsIgnoreCase("7014")){
		addActionError(prop.getProperty("17014"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7017")){
		addActionError(prop.getProperty("17017"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7018")){
		addActionError(prop.getProperty("17018"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7019")){
		addActionError(prop.getProperty("17019"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7020")){
		addActionError(prop.getProperty("17020"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7021")){
		addActionError(prop.getProperty("17021"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7022")){
		addActionError(prop.getProperty("17022"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7045")){
		addActionError(prop.getProperty("17045"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7046")){
		addActionError(prop.getProperty("17046"));
		return "fail";
	}
	if(ambResult.getStatusCode().equalsIgnoreCase("7047")){
		addActionError(prop.getProperty("17047"));
		return "fail";
	}
		 return "success";
	 }
	
	
	
	public String walletToWallet(){
		generateCsrfToken();
		logger.debug("*********************************calling wallettowallet************************************");
		return "success";
	}

	
	public String sendWtoWMoney(){
		User user=(User)session.get("User");
		logger.debug("*********************************calling sendwtowmoney************************************");
		String aggId=(String)session.get("aggId");

		inputBean.setAggreatorid(aggId);
		if(inputBean.getTrxAmount()==0){
			addActionError("Amount can not be zero or blank.");
			return "fail";
		}
		if(inputBean.getMobileNo()==null||inputBean.getMobileNo().isEmpty()){
			addActionError("Invalid mobile number.");
			return "fail";
		}
		logger.debug("*********************************calling service wallettowallettransfer************************************");
		
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		TreeMap<String,String> obj=new TreeMap<String,String>();
		obj.put("userId",user.getId());
        obj.put("walletId",user.getWalletid());
        obj.put("mobileNo",inputBean.getMobileNo());
        obj.put("trxAmount",""+inputBean.getTrxAmount());
        obj.put("ipImei",ipAddress);
        obj.put("aggreatorid",aggId);
        obj.put("token",user.getToken());
       try {
		String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),obj);
		inputBean.setCHECKSUMHASH(checkSumString);
       } catch (Exception e) {
		e.printStackTrace();
       }
        //obj.put("CHECKSUMHASH",checkSum);
		
		String result=tService.walletToWalletTransfer(inputBean,ipAddress,userAgent);
		logger.debug("*********************************getting response from service  "+result+"************************************");
		if(result==null||result.isEmpty()){
			addActionError("An error has occurred.");
			return "fail";
		}
		if(result.equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("1000")){
			inputBean.setMobileNo("");
			inputBean.setTrxAmount(0.0);
			addActionMessage("Successfully sent.");
			
			logger.debug("*********************************calling service profilebyloginid************************************");
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			logger.debug("*********************************getting result from service "+resultMap+"************************************");

			updateWalletDetails(resultMap);
			return "success";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("7011"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("17023"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}
		if(result.equalsIgnoreCase("7036")){
			addActionError(prop.getProperty("70361"));
			return "fail";
		}
		logger.debug("*********************************no condition matches return success by default***********************************");

		return "fail";
		
	}
	
	
	
	
	
	public String walletToBank(){
		logger.debug("*********************************calling wallettobank************************************");

		return "success";
	}
	
	
	public String sendWtoBMoney(){
		logger.debug("*********************************calling sendwtobmoney************************************");

		if(wtob.getAmount()==0){
			addActionError("Amount can not be zero or blank.");
			return "fail";
		}
		if(wtob.getAccno()==null||wtob.getAccno().isEmpty()){
			addActionError("Invalid account number.");
			return "fail";
		}
		logger.debug("*********************************calling service wallettobanktransfer************************************");
		String aggId=(String)session.get("aggId");
		wtob.setAggreatorid(aggId);
		WalletToBankTxnMast result=tService.walletToBankTransfer(wtob);
		logger.debug("*********************************getting response from service "+result+"************************************");

		session.put("wallettobank",result);
		if(result.getStatusCode()==null||result.getStatusCode().isEmpty()){
			addActionError("An error has occurred.");
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("1001")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("1000")){
			wtob=new WalletToBankTxnMast();
			addActionMessage("Successfully sent.");
			User user=(User)session.get("User");
			logger.debug("*********************************calling service profilebyloginid************************************");
			UserSummary usummery=new UserSummary();
			usummery.setUserId(user.getEmailid());
			usummery.setAggreatorid(user.getAggreatorid());
			Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
			logger.debug("*********************************getting response from service  "+resultMap+"************************************");

			updateWalletDetails(resultMap);
			
			return "success";
		}
		if(result.getStatusCode().equalsIgnoreCase("7031")){
			addActionError(prop.getProperty("7031"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("70231"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7014")){
			addActionError(prop.getProperty("17014"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7017")){
			addActionError(prop.getProperty("17017"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7018")){
			addActionError(prop.getProperty("17018"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7019")){
			addActionError(prop.getProperty("17019"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7020")){
			addActionError(prop.getProperty("17020"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7021")){
			addActionError(prop.getProperty("17021"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7022")){
			addActionError(prop.getProperty("17022"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7045")){
			addActionError(prop.getProperty("17045"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7046")){
			addActionError(prop.getProperty("17046"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7047")){
			addActionError(prop.getProperty("17047"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7000")){
			addActionError(prop.getProperty("17000"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7023")){
			addActionError(prop.getProperty("702311"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7024")){
			addActionError(prop.getProperty("17024"));
			return "fail";
		}
		if(result.getStatusCode().equalsIgnoreCase("7042")){
			addActionError(prop.getProperty("17042"));
			return "fail";
		}else{
			logger.debug("*********************************conditions not matches return success ************************************");

		return "success";
		}
	}
	
	public String setLimit(){
		
		logger.debug("*********************************calling setLimit************************************");
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		User user=(User)session.get("User");
		
		if(user!=null){
					
			if(user!=null&&!config.getId().equalsIgnoreCase("-1")){
				config.setTxnType(0);
				logger.debug("*********************************calling service getWalletConfigByUserId************************************");
				config=uService.getWalletConfigByUserId(config);
				logger.debug("*********************************getting response by service "+config+"************************************");
			}
			
			
			
			logger.debug("*********************************calling service getcustomerbyagentID************************************");
	
		//Map<String,String> mapResult=uService.getCustomerByAgentId(user.getUsertype(),user.getId());	
		//logger.debug("*********************************getting result from service "+mapResult+"************************************");

		//setCustomerList(mapResult);
		}		
		return "success";
	}
	
	public String getWalletConfigByUserId(){
		logger.debug("*********************************calling getWalletConfigByUserId************************************");
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		User user=(User)session.get("User");		
		if(user!=null&&!config.getId().equalsIgnoreCase("-1")){
			logger.debug("*********************************calling service getWalletConfigByUserId************************************");
			config=uService.getWalletConfigByUserId(config);
			logger.debug("*********************************getting response by service "+config+"************************************");
		}
		return "success";
	}
	public String saveLimit(){
		logger.debug("*********************************calling saveLimit************************************");
		logger.debug("*********************************calling getWalletConfigByUserId************************************");
		String aggId=(String)session.get("aggId");
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> resultMap=comService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(resultMap.get("PLAN"));
			setTxnType(resultMap.get("TXNTYPE"));
		}
		User user=(User)session.get("User");
		if(user!=null){
			logger.debug("*********************************calling service getCustomerByAgentId************************************");
			Map<String,String> mapResult=uService.getCustomerByAgentId(user.getUsertype(),user.getId());	
			logger.debug("*********************************getting response from service "+mapResult+"***********************************");

			setCustomerList(mapResult);
			}
			
		if(config==null){
			addActionError("An error has occurred.");
			return "fail";
		}
		logger.debug("*********************************calling service saveLimit************************************");
		config.setAggreatorid(aggId);
		String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			   ipAddress = request.getRemoteAddr();
		}
		
		UserWalletConfigBean result=uService.saveLimit(config,ipAddress,userAgent);
		logger.debug("*********************************getting response from service "+result.getStatus()+"************************************");
		config=result;
		if(result.getStatus().equalsIgnoreCase("1000")){
			addActionMessage("Transaction limit set successfully.");
			return "success";
		}
		if(result.getStatus().equalsIgnoreCase("1001")){
			addActionError("Updation failed please try again.");
			return "success";
		}
		
		if(result.getStatus().equalsIgnoreCase("7000")){
			addActionError("An error has occurred.");
			return "success";
		}
		return "success";
	}
	
	public String paymentReport(){
		logger.debug("*********************************calling paymentreport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		if(user.getUsertype()!=99){
			reportBean.setUserId(user.getId());
			}
			else{
			reportBean.setUserId("");
		}
		logger.debug("*********************************calling service wallettobankreport************************************");
		List<WalletToBankTxnMast>	list=rService.wallettoBankReport(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	public String rechargeReport(){
		logger.debug("*********************************calling rechargeReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		reportBean.setUserId(user.getId());
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service rechargeReport************************************");
		List<RechargeTxnBean>	list=rService.rechargeReport(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	
	public String bbpsReport(){
		logger.debug("*********************************calling bbpsReport************************************");
		User user=(User)session.get("User");
		inputBean.setUserId(user.getId());
		logger.debug("*********************************calling service bbpsReport************************************");
		List<BbpsPayment>	list=rService.bbpsReport(inputBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	public String walletHistory(){
		logger.debug("*********************************calling WalletHistory************************************");
		User user=(User)session.get("User");
		//ReportBean reportBean=new ReportBean();
		WalletHistoryBean walletHistoryBean = new WalletHistoryBean();
		walletHistoryBean.setUserId(user.getId());
		walletHistoryBean.setStDate(inputBean.getStDate());
		walletHistoryBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service WalletHistoryreport************************************");
		List<WalletHistoryTxnBean>	list=rService.walletHistoryReport(walletHistoryBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	
	public String rechargeReportAggreator(){
		logger.debug("*********************************calling rechargeReportAggreator************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (inputBean.getAggreatorid() == null || user.getWhiteLabel() == 1) {
			reportBean.setAggreatorid(user.getAggreatorid());
		} else {
			reportBean.setAggreatorid(inputBean.getAggreatorid());
		}
		
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service rechargeReportAggreator************************************");
		List<RechargeTxnBean>	list=rService.rechargeReportAggreator(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	
	public String bbpsReportAggreator(){
		logger.debug("*********************************calling bbpsReportAggreator************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (inputBean.getAggreatorid() == null || user.getWhiteLabel() == 1) {
			reportBean.setAggreatorid(user.getAggreatorid());
		} else {
			reportBean.setAggreatorid(inputBean.getAggreatorid());
		}
		
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service bbpsReportAggreator************************************");
		List<BbpsPayment>	list=rService.bbpsReportAggreator(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	
	
	
	public String bbpsReportDistAggregator(){
		User user=(User)session.get("User");
		//List <AEPSLedger> revnList=new ArrayList<>();
			if (user.getUsertype() == 3) {
				Map<String, String> agentList = service.getAgentByDistId(user.getId());
				if (agentList != null && agentList.size() > 0) {
					setAgentList(agentList);
				}
				walletBean.setDistributerid(user.getId());
			}else if(user.getUsertype()!=2&&user.getUsertype()!=3) {
				if(user.getWhiteLabel()!=1) {
				Map<String, String> aggrigatorList=service.getAggreator();	
				if(aggrigatorList!=null&&aggrigatorList.size()>0){
					setAggrigatorList(aggrigatorList);
				}
				}
				if(walletBean.getAggreatorid()==null||user.getWhiteLabel()==1)
					walletBean.setAggreatorid(user.getAggreatorid());
			}
		if(inputBean!=null&&inputBean.getStDate()!=null&&inputBean.getEndDate()!=null) {
			walletBean.setStDate(inputBean.getStDate());
			walletBean.setEndDate(inputBean.getEndDate());
		}

			
			walletBean.setUsertype((int)user.getUsertype());
			if(inputBean.getUserId()!=null) {
				walletBean.setId(inputBean.getUserId());
			}
			if(inputBean==null|| inputBean.getUserId()==null||inputBean.getUserId().equalsIgnoreCase("-1")){
				walletBean.setId("-1");
			}
			List<BbpsPayment> list =rService.bbpsReportDistAggregator(walletBean);
			request.setAttribute("agentList", list);
		
		return "success";	
	}

	
	public String rechargeReportDist(){
		User user=(User)session.get("User");
			if (user.getUsertype() == 3) {
				Map<String, String> agentList = service.getAgentByDistId(user.getId());
				if (agentList != null && agentList.size() > 0) {
					setAgentList(agentList);
				}
				walletBean.setDistributerid(user.getId());
			}else if(user.getUsertype()!=2&&user.getUsertype()!=3) {
				if(user.getWhiteLabel()!=1) {
				Map<String, String> aggrigatorList=service.getAggreator();	
				if(aggrigatorList!=null&&aggrigatorList.size()>0){
					setAggrigatorList(aggrigatorList);
				}
				}
				if(walletBean.getAggreatorid()==null||user.getWhiteLabel()==1)
					walletBean.setAggreatorid(user.getAggreatorid());
			}
		if(inputBean!=null&&inputBean.getStDate()!=null&&inputBean.getEndDate()!=null) {
			walletBean.setStDate(inputBean.getStDate());
			walletBean.setEndDate(inputBean.getEndDate());
		}

			
			walletBean.setUsertype((int)user.getUsertype());
			if(inputBean.getUserId()!=null) {
				walletBean.setId(inputBean.getUserId());
			}
			if(inputBean==null|| inputBean.getUserId()==null||inputBean.getUserId().equalsIgnoreCase("-1")){
				walletBean.setId("-1");
			}
			List<RechargeTxnBean> list =rService.rechargeReportDist(walletBean);
			request.setAttribute("resultList", list);
		
		return "success";	
	}



	
	
	
	
	public String pgReportAggreator(){
		logger.debug("*********************************calling rechargeReportAggreator************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (inputBean.getAggreatorid() == null || user.getWhiteLabel() == 1) {
			reportBean.setAggreatorid(user.getAggreatorid());
		} else {
			reportBean.setAggreatorid(inputBean.getAggreatorid());
		}
		
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service rechargeReportAggreator************************************");
		List<PaymentGatewayTxnBean>	list=rService.pgReportAggreator(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	public String validateCustomer(){
		if(dmtInputBean==null||dmtInputBean.getMobile()==null||dmtInputBean.getMobile().isEmpty()){
			addActionError("Please enter mobile number.");
			return "fail";
		}
		
		DMTBean inBean=tService.customerValidation(dmtInputBean);
		
		if(inBean.getStatusCode().equalsIgnoreCase("0")){			
			return "showCustomerList";
		}else  if(inBean.getStatusCode().equalsIgnoreCase("23")){			
			return "customerRegistration";
		}
		else if(inBean.getStatusCode().equalsIgnoreCase("224")){
			addActionError("");
			return "fail";
		}
		else{
			addActionError("An error has occurred.");
			return "fail";
		}
	}
	
	public String saveRegisterCustomer(){
		
		if(dmtInputBean==null){
			addActionError("An error has occurred please try again.");
			return "fail";
		}
		if(dmtInputBean.getMobile()==null||dmtInputBean.getMobile().isEmpty()){
			addActionError("Please enter mobile number.");
			return "fail";
		}
		if(dmtInputBean.getFname()==null||dmtInputBean.getFname().isEmpty()){
			addActionError("Please enter first name.");
			return "fail";
		}
		if(dmtInputBean.getLname()==null||dmtInputBean.getLname().isEmpty()){
			addActionError("Please enter last name.");
			return "fail";
		}
		
		
		dmtInputBean=tService.customerRegistration(dmtInputBean);
		
		if(dmtInputBean.getStatusCode().equalsIgnoreCase("0")){
			
			return "validateOtp";
		}else  if(dmtInputBean.getStatusCode().equalsIgnoreCase("23")){
			addActionMessage("Customer already registered.");
			return "alreadyRegisterd";
		}
		else if(dmtInputBean.getStatusCode().equalsIgnoreCase("224")){
			addActionError(prop.getProperty("224"));
			return "fail";
		}
		else{
			addActionError("An error has occurred.");
			return "fail";
		}
	}
	
	public String dmtOTPValidation(){
		if(dmtInputBean==null){
			
		}
		if(dmtInputBean.getMobile()==null||dmtInputBean.getMobile().isEmpty()){
			addActionError("Invalid request.");
			return "fail";
		}
		if(dmtInputBean.getRequestNo()==null||dmtInputBean.getRequestNo().isEmpty()){
			addActionError("Invalid request.");
			return "fail";
		}
		if(dmtInputBean.getOtp()==null||dmtInputBean.getOtp().isEmpty()){
			addActionError("Please enter OTP");
			return "fail";
		}
		WalletExist walletExist=tService.verifyRequest(dmtInputBean);
		
		if(walletExist!=null&&walletExist.getResponse().equalsIgnoreCase("SUCCESS")&&walletExist.getCode().equalsIgnoreCase("300")){
			setUserDetails(tService.getUserDetails(dmtInputBean));
			request.setAttribute("userDetails",getUserDetails());
			addActionMessage(walletExist.getMessage());
			return "validateOtp";
		}else {
			addActionError(walletExist.getMessage());
			return "fail";
		}
		
		
	}
	
	public String dmtResendOTP(){
		WalletExist walletExist=tService.resendOtp(dmtInputBean);
		if(walletExist==null){
			addActionError("An error has occurred.");
			return "fail";
		}
		if(walletExist.getResponse().equalsIgnoreCase("success")){
			addActionMessage(walletExist.getMessage());
			return "success";
		}
		else{
			addActionError("OTP not sent please try again.");
			return "fail";
		}
		
	}
	
	public String decryptString(String encString,String secureKey){
		
		Properties prop = new Properties();
		InputStream in = ServiceManager.class
				.getResourceAsStream("PayGate.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		//secureKey=prop.getProperty("EncKey");
		String decryptString=new String();
		try{
    	decryptString=EncryptionByEnc256.decrypt(encString,secureKey);
    	}catch(Exception e){
			e.printStackTrace();
		}
    	return decryptString;
    }
	
	
	
	public String askMoney(){
        logger.debug("**********************  askMoney calling  **************************");
        generateCsrfToken();
        User user=(User)session.get("User");
        if(user!=null&&user.getUsertype()==1){
               logger.debug("**********************  askMoney calling  returning customer**************************");
               return "customer";
        }
        logger.debug("**********************  askMoney calling  returning success**************************");
 return "success";   
 }
 
 
 public String saveAskMoney(){
        logger.debug("**********************  saveAskMoney calling  **************************");
        if(inputBean.getTrxAmount()==0){
               addActionError("Amount can not be zero or blank.");
               return "fail";
        }
        if(inputBean.getMobileNo()==null||inputBean.getMobileNo().isEmpty()){
               addActionError("This Mobile number is not registered with us.");
               return "fail";
        }
        
        User user=(User)session.get("User");
 if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
               logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
               addActionError("Please try again..");
               return "customer";
        }
        
        
        logger.debug("**********************  calling service saveAskMoney **************************");
        inputBean.setUserId(user.getId());
        inputBean.setWalletId(user.getWalletid());
        String aggId=(String)session.get("aggId");
        inputBean.setAggreatorid(aggId);
        String result=tService.saveAskMoney(inputBean);
        
        
        
        logger.debug("*********************************getting response from service  "+result+"************************************");
        if(result==null||result.isEmpty()){
               addActionError("An error has occurred.");
               return "fail";
        }
        if(result.equalsIgnoreCase("1001")){
               addActionError(prop.getProperty("17000"));
               return "fail";
        }
        if(result.equalsIgnoreCase("1000")){
        generateCsrfToken();
               addActionMessage("Successfully sent.");
               return "success";
        }
        if(result.equalsIgnoreCase("7032")){
               addActionError(prop.getProperty("7032"));
               return "fail";
        }
        if(result.equalsIgnoreCase("7025")){
            addActionError(prop.getProperty("17025"));
            return "fail";
        }
        if(result.equalsIgnoreCase("7000")){
               addActionError(prop.getProperty("17000"));
               return "fail";
        }
        if(result.equalsIgnoreCase("7023")){
               addActionError(prop.getProperty("7011"));
               return "fail";
        }
        if(result.equalsIgnoreCase("7024")){
               addActionError(prop.getProperty("17024"));
               return "fail";
        }
        if(result.equalsIgnoreCase("7042")){
               addActionError(prop.getProperty("17042"));
               return "fail";
        }
        logger.debug("*********************************no condition matches return success by default***********************************");
        
        logger.debug("**********************  saveAskMoney calling  returning success**************************");
        return "success";   
 }
 
 public String smsReport(){
	 String aggId=(String)session.get("aggId");
		smsBean.setAggreatorid(aggId);
		List<SMSSendDetails> smsResult=rService.getSMSRepot(smsBean);
		request.setAttribute("smsResult",smsResult);
	 return "success";
 }

public String getSMSRepot(){
	String aggId=(String)session.get("aggId");
	smsBean.setAggreatorid(aggId);
	List<SMSSendDetails> smsResult=rService.getSMSRepot(smsBean);
	request.setAttribute("smsResult",smsResult);
	return "success";
}
public String customerSupport(){
	User user=(User)session.get("User");
	String aggId=(String)session.get("aggId");
	supBean.setAggreatorId(aggId);
	supBean.setUserId(user.getId());
	List<SupportTicketBean> list=rService.supportTicketList(supBean);
	request.setAttribute("supportList",list);
return "success";

}

public String saveSupportDetails(){
	String aggId=(String)session.get("aggId");
	User user=(User)session.get("User");
	supBean.setAggreatorId(aggId);
	supBean.setUserId(user.getId());
	List<SupportTicketBean> list=rService.supportTicketList(supBean);
	request.setAttribute("supportList",list);
	
	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	supBean=rService.saveUpdateSupportTicket(supBean,ipAddress, userAgent);
	if(supBean.getStatusCode().equalsIgnoreCase("1000")){
		addActionMessage("Support ticket generated successfully. Reference number is "+supBean.getId());
		return "success";
	}
	
	//as per santosh sir.
	else{
		addActionError("Support ticket not generated please try again later.");
		return "success";
	}
}

public String supportTickets(){
	String aggId=(String)session.get("aggId");
	supBean.setAggreatorId(aggId);
	List<SupportTicketBean> list=rService.getSupportTicket(supBean);
	request.setAttribute("supportList",list);
	return "success";
}

public String supportTicketList(){
	
	String aggId=(String)session.get("aggId");
	supBean.setAggreatorId(aggId);
	List<SupportTicketBean> list=rService.supportTicketList(supBean);
	request.setAttribute("supportList",list);
	return "success";
	
}

public String closeTickets(){
	
	
	supBean=rService.updateSupportTicket(supBean);
	
	String aggId=(String)session.get("aggId");
	supBean.setAggreatorId(aggId);
	List<SupportTicketBean> list=rService.getSupportTicket(supBean);
	request.setAttribute("supportList",list);
	
	if(supBean.getStatusCode().equalsIgnoreCase("1000")){
		addActionMessage("Support ticket with reference no "+supBean.getId()+" closed successfully.");
		return "success";
	}
	
	//as per santosh sir.
	else{
		addActionError("Please try again.");
		return "success";
	}
}

public String transactionReport(){
	List<TxnReportByAdmin> list=rService.getTxnReportByAdmin();
	request.setAttribute("txnList",list);
	return "success";
}

public String dmt(){
	DMTInputBean dBean=new DMTInputBean();
	User user=(User)session.get("User");
	/****************************aggregator mobile number*******************************/
//	WalletMastBean wmb=uService.showUserProfile(user.getAggreatorid());
//	dBean.setMobile(wmb.getMobileno());
	if(user.getUsertype()==1){
		dBean.setMobile(user.getMobileno());
		session.put("dmtMobile",user.getMobileno());
	}else{
		if((dmtInputBean.getMobile()!=null&&!dmtInputBean.getMobile().isEmpty())){
			dBean.setMobile(dmtInputBean.getMobile());
			session.put("dmtMobile",dmtInputBean.getMobile());
			}
		else if(session.get("dmtMobile")!=null&&!((String)session.get("dmtMobile")).isEmpty()){
			dBean.setMobile(((String)session.get("dmtMobile")));
		}
		else{
				addActionError("Mobile number can not be blank.");
				return "emptyMobile";
		}
	
	}
	WalletExist walletExist=tService.checkWalletExistRequest(dBean);
	if(walletExist!=null){
		if(walletExist.getResponse()!=null&&walletExist.getCode()!=null&&walletExist.getCode().equalsIgnoreCase("300")){
			if(walletExist.getCardExists()!=null&&walletExist.getCardExists().equalsIgnoreCase("Y")){
				setUserDetails(tService.getUserDetails(dBean));
				request.setAttribute("userDetails",getUserDetails());
				return "success";
			}
			/*else{
				addActionMessage("Please contact to your aggregator.");
				return "success";
			}*/
			else{
				dBean.setMobile((String)session.get("dmtMobile"));
				dBean.setName(user.getName());
				WalletExist walletExist2=tService.createWalletRequest(dBean);
				if(walletExist2.getResponse()!=null&&walletExist2.getCode()!=null&&walletExist2.getCode().equalsIgnoreCase("300")){
					dmtInputBean.setMobile((String)session.get("dmtMobile"));
					dmtInputBean.setRequestNo(walletExist2.getRequestNo());
					addActionMessage("Please enter the OTP that has been sent to your mobile number.");
					return "otpValidation";
				}else{
					return "fail";
				}
				
			}
		}else{
			return "fail";
		}
	}
	else{
		return "fail";
	}
	
}

public String addBeneficiary(){	
	accountTypeList.put("Savings","Savings");
	accountTypeList.put("Current","Current");
	setAccountTypeList(accountTypeList);
	User user=(User)session.get("User");
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	return "success";
}

public String addBeneficiaryRequest(){
	User user=(User)session.get("User");
	accountTypeList.put("Saving","Saving");
	accountTypeList.put("Current","Current");
	setAccountTypeList(accountTypeList);	
	UserDetails userDetails=tService.addBeneficiaryRequest(dmtInputBean);
	if(userDetails.getResponse()!=null&&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		String mobile=(String)session.get("dmtMobile");
//		dmtInputBean.setMobile(user.getMobileno());
		dmtInputBean.setMobile(mobile);
		dmtInputBean.setRequestNo(userDetails.getRequestNo());
		addActionMessage("Please enter the OTP that has been sent to your mobile number.");
		return "success";
	}else{
		addActionError(userDetails.getMessage());
		return "fail";
	}
	
}


public String deleteBeneficiaryRequest(){
	User user=(User)session.get("User");
	UserDetails userDetails=tService.deleteBeneficiaryRequest(dmtInputBean);
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	if(userDetails.getResponse()!=null&&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		dmtInputBean.setRequestNo(userDetails.getRequestNo());
		addActionMessage("Please enter the OTP that has been sent to your mobile number.");
		return "success";
	}else{
		addActionError(userDetails.getMessage());
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	
}	
public String fundTransfer(){
	User user=(User)session.get("User");
	if(dmtInputBean.getReinitiate()!=null&&dmtInputBean.getReinitiate().equalsIgnoreCase("true")){
		String transId=dmtInputBean.getPreviousAgentTransId();
		dmtInputBean=(DMTInputBean)session.get("dmtInputBean");
		dmtInputBean.setReinitiate("TRUE");
		dmtInputBean.setPreviousAgentTransId(transId);
		session.remove("dmtInputBean");
	}else{
//		dmtInputBean.setType("VALIDATEBENEF");
		dmtInputBean.setType("MR");
		dmtInputBean.setReinitiate("FALSE");
		dmtInputBean.setPreviousAgentTransId("");
	}
	
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	dmtInputBean.setAggreatorid(user.getAggreatorid());
	dmtInputBean.setWalletId(user.getWalletid());
	dmtInputBean.setUserId(user.getId());
	
	/*dmtInputBean.setBenCode("NheQa");
	dmtInputBean.setBenName("Santosh");
	dmtInputBean.setBenAccount("50100058433274");
	dmtInputBean.setAccountType("Savings");
	dmtInputBean.setBenIFSC("HDFC0001898");
	dmtInputBean.setAmount(15);*/
	dmtInputBean.setTransferType(dmtInputBean.getBenType());
	
	dmtInputBean.setBenMobile(user.getMobileno());
	dmtInputBean.setDescription("test..");
//	dmtInputBean.setType("VALIDATEBENEF");
	session.put("dmtInputBean",dmtInputBean);
	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
UserDetails userDetails=null;
if(dmtInputBean.getReinitiate()!=null&&dmtInputBean.getReinitiate().equalsIgnoreCase("true")){
	 userDetails=tService.reinitiateMrTransfer(dmtInputBean,ipAddress,userAgent);
}else{
	 userDetails=tService.mrTransfer(dmtInputBean,ipAddress,userAgent);
}
//String mobile=(String)session.get("dmtMobile");
//dmtInputBean.setMobile(user.getMobileno());
dmtInputBean.setMobile(mobile);
	if(userDetails.getResponse()!=null&&userDetails.getResponse().equalsIgnoreCase("Success")&&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		dmtInputBean.setRequestNo(userDetails.getRequestNo());
		if(userDetails.getRecharge()!=null&&userDetails.getRecharge().getStatus().equalsIgnoreCase("SUCCESS")&&userDetails.getMoneyRemittance()!=null&&userDetails.getMoneyRemittance().getTransferStatus().equalsIgnoreCase("FAILED")){
//			dmtInputBean.setMobile(user.getMobileno());
			dmtInputBean.setMobile(mobile);
			UserDetails userDtls=tService.getUserBalance(dmtInputBean);			
			if(userDtls.getResponse()!=null&&userDtls.getResponse().equalsIgnoreCase("success") &&userDtls.getCode()!=null&&userDtls.getCode().equalsIgnoreCase("300")){
				if(Double.parseDouble(userDtls.getBalance())>=userDetails.getMoneyRemittance().getAmount()){
// 	if(true){
				request.setAttribute("reinitflag","true");
				}
			}
		}
		request.setAttribute("userDetails",userDetails);
		return "success";
	}
	
	if(userDetails.getCode().equalsIgnoreCase("7023")){
		addActionError(prop.getProperty("70231"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7014")){
		addActionError(prop.getProperty("17014"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7017")){
		addActionError(prop.getProperty("17017"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7018")){
		addActionError(prop.getProperty("17018"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7019")){
		addActionError(prop.getProperty("17019"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7020")){
		addActionError(prop.getProperty("17020"));
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7021")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17021"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7022")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17022"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7045")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17045"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7046")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17046"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7047")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17047"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7000")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17000"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7023")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("702311"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7024")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17024"));
		return "fail";
	}
	if(userDetails.getCode().equalsIgnoreCase("7042")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionError(prop.getProperty("17042"));
		return "fail";
	}
	else{
		addActionError(userDetails.getMessage());
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		return "fail";
	}
}

public String userBalance(){
	
	User user=(User)session.get("User");
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	UserDetails userDetails=tService.getUserBalance(dmtInputBean);
	
	if(userDetails.getResponse()!=null&&userDetails.getResponse().equalsIgnoreCase("success") &&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		setUserDetails(tService.getUserDetails(dmtInputBean));
		request.setAttribute("userDetails",getUserDetails());
		addActionMessage("Your current balance is "+userDetails.getBalance());
		return "success";
	}else{
		addActionError(userDetails.getMessage());
		return "fail";
	}
	
}

public String transStatus(){
	User user=(User)session.get("User");
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	UserDetails userDetails=tService.getTransStatus(dmtInputBean);
	
	if(userDetails.getResponse()!=null&&userDetails.getResponse().equalsIgnoreCase("success") &&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		UserDetails userDetails2=tService.getTransHistory(dmtInputBean);
		request.setAttribute("userDetails",userDetails2);
		addActionMessage("Current status for wallet id "+userDetails.getAgentTransId()+" is "+userDetails.getStatus());
		return "success";
	}else{
		addActionError(userDetails.getMessage());
		return "fail";
	}

}
public String userTransHistory(){
	User user=(User)session.get("User");
	String mobile=(String)session.get("dmtMobile");
//	dmtInputBean.setMobile(user.getMobileno());
	dmtInputBean.setMobile(mobile);
	UserDetails userDetails=tService.getTransHistory(dmtInputBean);
	
	if(userDetails.getResponse()!=null&&userDetails.getResponse().equalsIgnoreCase("success") &&userDetails.getCode()!=null&&userDetails.getCode().equalsIgnoreCase("300")){
		
		request.setAttribute("userDetails",userDetails);
		return "success";
	}else{
		addActionError(userDetails.getMessage());
		return "fail";
	}	
}

public String escrowReport(){
	User u=(User)session.get("User");	
	bean.setAggreatorid(u.getId());
String eBalance=tService.getEscrowAccBal(bean);
List<EscrowBean> eList=tService.getEscrowTxnList(bean);

request.setAttribute("eBalance",eBalance);
request.setAttribute("eList",eList);

	return "success";
}

public boolean generateCsrfToken(){
	Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

    if (csrfPreventionSaltCache == null){
        csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

        session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
    }

    // Generate the salt and store it in the users cache
    String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
    csrfPreventionSaltCache.put(salt, Boolean.TRUE);

    session.put("csrfPreventionSalt", salt);
    return true;
}

public String cashDeposit(){
	
	
ServerInfo s=new ServerInfo();
System.out.println("s.getServerBuilt()"+s.getServerBuilt());
System.out.println("s.getServerinfo()"+s.getServerInfo());
System.out.println("s.getServerNumber()"+s.getServerNumber());
	
System.out.println(request.getServerPort());
System.out.println(request.getServletContext().getServerInfo());

System.out.println(request.getServletContext().getInitParameter("name"));
	
	setActionMessages(null);


	User user=(User)session.get("User");
	if(inputBean!=null && inputBean.getStDate()!=null && inputBean.getEndDate()!=null) {
		depositMast.setStDate(inputBean.getStDate());
		depositMast.setEndDate(inputBean.getEndDate());
	}
//	else {
//		String pattern = "dd-MMM-yyyy";
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//		String date = simpleDateFormat.format(new Date());
//		depositMast.setStDate(date);
//		depositMast.setEndDate(date);
//	}
 
	depositMast.setAggreatorId(user.getAggreatorid());
	depositMast.setUserId(user.getId());
	depositMast.setWalletId(user.getWalletid());
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR");
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
//		CashDepositMast deposit = tService.requsetCashDeposit(depositMast,ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByUserId(depositMast, ipAddress, userAgent);
		request.setAttribute("eList",list);
		return "success";
}
public String cashDepositPending(){
	
	
ServerInfo s=new ServerInfo();
System.out.println("s.getServerBuilt()"+s.getServerBuilt());
System.out.println("s.getServerinfo()"+s.getServerInfo());
System.out.println("s.getServerNumber()"+s.getServerNumber());
	
System.out.println(request.getServerPort());
System.out.println(request.getServletContext().getServerInfo());

System.out.println(request.getServletContext().getInitParameter("name"));
	
	setActionMessages(null);


	User user=(User)session.get("User");
	

	depositMast.setAggreatorId(user.getAggreatorid());
	depositMast.setUserId(user.getId());
	depositMast.setWalletId(user.getWalletid());
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
//		CashDepositMast deposit = tService.requsetCashDeposit(depositMast,ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByUserIdPending(depositMast, ipAddress, userAgent);
		request.setAttribute("eList",list);
		return "success";
}

public String saveCashDeposit(){
	User user=(User)session.get("User");
	
	if(depositMast==null){
		return "fail";
	}
	if(depositMast.getType()==null||depositMast.getType().equalsIgnoreCase("-1")){
		addActionError("Please select transaction type.");
		return "fail";
	}
	if(depositMast.getAmount()==0){
		addActionMessage("Please enter amount.");
		return "fail";
	}
	/*
	if(depositMast.getCashDepositDate()==null){
		addActionMessage("Please enter date.");
		return "fail";
	}
	*/
	if(depositMast.getType().equalsIgnoreCase("NEFT")){
		if(depositMast.getNeftRefNo()==null||depositMast.getNeftRefNo().isEmpty()){
			addActionError("Please enter your NEFT/IMPS/RTGS transaction id.");
		}
	}else{
		if(depositMast.getMyFile1()==null){
			//addActionError("Please upload receipt file.");
		}else{
			try{
			depositMast.setReciptPic(encodeFileToBase64Binary(depositMast.getMyFile1()));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
//	if(user.getSuperdistributerid()!=null&&!user.getSuperdistributerid().isEmpty()&&!user.getSuperdistributerid().equalsIgnoreCase("-1")&&!user.getAggreatorid().equals("OAGG001050")){
//	depositMast.setAggreatorId(user.getSuperdistributerid());	
//	}else{
	depositMast.setAggreatorId(user.getAggreatorid());
//	}
	
	depositMast.setUserId(user.getId());
	depositMast.setWalletId(user.getWalletid());
	
	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
		CashDepositMast deposit = tService.requsetCashDeposit(depositMast,
				ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByUserIdPending(deposit, ipAddress, userAgent);
		request.setAttribute("eList",list);
		if (deposit.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Request successfully completed.");
			depositMast=new CashDepositMast();
			return "success";
		}
		if (deposit.getStatusCode().equalsIgnoreCase("5501")) {
			addActionMessage("Amount can not be 0.");
			return "fail";
		}
		if (deposit.getStatusCode().equalsIgnoreCase("5502")) {
			addActionMessage("Please select a transaction type.");
			return "fail";
		}
		if (deposit.getStatusCode().equalsIgnoreCase("5503")) {
			addActionMessage("Please enter NEFT/IMPS/RTGS transaction id or upload transaction receipt.");
			return "fail";
		}
		if (deposit.getStatusCode().equalsIgnoreCase("7000")) {
			addActionMessage("There are some problem please try again.");
			return "fail";

		}
		if (deposit.getStatusCode().equalsIgnoreCase("1001")) {
			addActionMessage("Request not completed. Please try again.");
			return "fail";
		}
		return "success";
}




public String cashDepositList(){
	generateCsrfToken();
	User user=(User)session.get("User");
	

	if(user.getUsertype()==7){
	depositMast.setAggreatorId(user.getId());
	}else{
	depositMast.setAggreatorId(user.getAggreatorid());
	}
	if(user.getWhiteLabel()==1){
		depositMast.setType("1");
	}else{
		depositMast.setType("0");
	}

	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
//		CashDepositMast deposit = tService.requsetCashDeposit(depositMast,ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByAggId(depositMast);
		request.setAttribute("eList",list);
		return "success";
}


public String cashDepositListByDist(){
	generateCsrfToken();
	User user=(User)session.get("User");
	
//	Map<String, String> aggrigatorList=uService.getAggreator();	
//	if(aggrigatorList!=null&&aggrigatorList.size()>0){
//		setAggrigatorList(aggrigatorList);
//	}
	if(user.getUsertype()==7){
	depositMast.setAggreatorId(user.getId());
	depositMast.setType("1");
	}else{
	depositMast.setAggreatorId(user.getAggreatorid());
	if(user.getWhiteLabel()==1){
		depositMast.setType("1");
	}else{
		depositMast.setType("0");
	}
	}
	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
//		CashDepositMast deposit = tService.requsetCashDeposit(depositMast,ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByAggId(depositMast);
		request.setAttribute("eList",list);
		return "success";
}

public String acceptCashDeposit(){
	generateCsrfToken();
	User user=(User)session.get("User");
	
	if(user.getUsertype()==7 && !user.getAggreatorid().equals("OAGG001050")){
		depositMast.setAggreatorId(user.getId());
		}else{
	depositMast.setAggreatorId(user.getAggreatorid());
		}
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
		
		
		String result=tService.acceptCashDeposit(depositMast, ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByAggId(depositMast);
		request.setAttribute("eList",list);
		if(result.equalsIgnoreCase("True")){
			addActionMessage("Cash deposit accepted. ref number is :"+depositMast.getDepositId());
		}
		else{
			addActionError("Please try again latter.");
		}
		
		return "success";
}

public String acceptCashDepositByDist(){
	generateCsrfToken();
	User user=(User)session.get("User");
	
	if(user.getUsertype()==7&&!user.getAggreatorid().equals("OAGG001050")){
		depositMast.setAggreatorId(user.getId());
		}else{
	depositMast.setAggreatorId(user.getAggreatorid());
		}
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
		
		
		String result=tService.acceptCashDepositByDist(depositMast, ipAddress, userAgent);
		List<CashDepositMast> list=tService.getCashDepositReqByAggId(depositMast);
		request.setAttribute("eList",list);
		if(result.equalsIgnoreCase("1000")){
			addActionMessage("Cash deposit accepted. ref number is :"+depositMast.getDepositId());
		}else if(result.equalsIgnoreCase("7032")){
			addActionMessage("Cash deposit can't be 0.");
		}else if(result.equalsIgnoreCase("7036")){
			addActionMessage("Cash deposit can't done with same wallet.");
		}else if(result.equalsIgnoreCase("7023")){
			addActionMessage("Distributor wallet not found.");
		}else if(result.equalsIgnoreCase("7025")){
			addActionMessage("Insufficient wallet balance.");
		}else if(result.equalsIgnoreCase("7024")){
			addActionMessage("Something went wrong please try again later.");
		}
		else{
			addActionError("Please try again later.");
		}
		
		return "success";
}


public String rejectCashDeposit(){
	generateCsrfToken();
	User user=(User)session.get("User");
	
	if(user.getUsertype()==7&&!user.getAggreatorid().equals("OAGG001050")){
		depositMast.setAggreatorId(user.getId());
		}else{
	depositMast.setAggreatorId(user.getAggreatorid());
		}
	
		if(user.getWhiteLabel()==1){
			depositMast.setType("1");
		}else{
			depositMast.setType("0");
		}
	
	
	
	
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	
		
		String result=tService.rejectCashDeposit(depositMast);
		List<CashDepositMast> list=tService.getCashDepositReqByAggId(depositMast);
		if(result.equalsIgnoreCase("True")){
			addActionMessage("Cash deposit rejected. ref number is :"+depositMast.getDepositId());
		}
		else{
			addActionError("Please try again latter.");
		}
		request.setAttribute("eList",list);
		return "success";
}

public String acceptRefund(){
	String userAgent=request.getHeader("User-Agent");
	String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
	if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
	}
	String result=tService.acceptRefundReq(refundBean,ipAddress,userAgent);
	User user=(User)session.get("User");
	refundBean.setAggreatorId(user.getAggreatorid());
	
	List<RefundMastBean> eList=tService.getRefundReqByAggId(refundBean);
	request.setAttribute("eList",eList);
	if(result.equalsIgnoreCase("True")){
		addActionMessage("Request completed successfully.");
	}else{
		addActionError("Please try again.");
	}
	return "success";
}
public String rejectRefund(){
	
	String result=tService.rejectRefundReq(refundBean);
	User user=(User)session.get("User");
	refundBean.setAggreatorId(user.getAggreatorid());
	List<RefundMastBean> eList=tService.getRefundReqByAggId(refundBean);
	request.setAttribute("eList",eList);
	if(result.equalsIgnoreCase("True")){
		addActionMessage("Request completed successfully.");
	}else{
		addActionError("Please try again.");
	}
	return "success";
}



public String refundRequestList(){
	User user=(User)session.get("User");
	refundBean.setAggreatorId(user.getAggreatorid());
	List<RefundMastBean> eList=tService.getRefundReqByAggId(refundBean);
	request.setAttribute("eList",eList);
	return "success";
}


public String prepaidCardReconciliationReport(){
	
	User us=(User)session.get("User");
	bean.setAggreatorid(us.getId());
	ReconciliationReport mapObject=tService.getPrePiadCardReConReport(bean);
	request.setAttribute("mapObject",mapObject);
	return "success";
	
}


public String rechargeReconciliationReport(){
	User us=(User)session.get("User");
	bean.setAggreatorid(us.getId());
	ReconciliationReport mapObject=tService.getRechargeReConReport(bean);
	request.setAttribute("mapObject",mapObject);
	return "success";
	
}

public String dMTReconciliationReport(){
	User us=(User)session.get("User");
	bean.setAggreatorid(us.getId());
	ReconciliationReport mapObject=tService.getDMTReConReport(bean);
	request.setAttribute("mapObject",mapObject);
	return "success";
}

public String pGReconciliationReport(){
	User us=(User)session.get("User");
	bean.setAggreatorid(us.getId());
	ReconciliationReport mapObject=tService.getPGReConReport(bean);
	request.setAttribute("mapObject",mapObject);
	return "success";
}
public String getCommSummaryReport(){
	  String aggId=(String)session.get("aggId");
	  inputBean.setAggreatorid(aggId);
	  List<CommSummaryBean> commSummaryResult=rService.getCommSummaryReport(inputBean);
	  request.setAttribute("commSummaryBean",commSummaryResult);
	  return "success";
	}

private String encodeFileToBase64Binary(File file)throws IOException {

	  byte[] bytes = loadFile(file);
	  byte[] encoded = Base64.encodeBase64(bytes);
	  String encodedString = new String(encoded);

	  return encodedString;
	 }

	public String calculateCashinSurcharge() {
		User us = (User) session.get("User");
		suBean.setId(us.getId());
		suBean = tService.calculateCashinSurcharge(suBean);
		JSONObject jObject = new JSONObject();
		if (suBean.getStatus().equalsIgnoreCase("1000")) {
			jObject.put("amount", suBean.getAmount());
			jObject.put("status", "1000");
			jObject.put("samount", suBean.getSurchargeAmount());
		} else {
			jObject.put("status", "7000");
			// jObject.put("amount",suBean.getSarchargeAmount());
		}
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}
	public String calculateCashOutSurcharge() {
		User us = (User) session.get("User");
		suBean.setId(us.getId());
		suBean = tService.calculateCashOutSurcharge(suBean);
		JSONObject jObject = new JSONObject();
		if (suBean.getStatus().equalsIgnoreCase("1000")) {
			jObject.put("amount", suBean.getAmount());
			jObject.put("status", "1000");
			jObject.put("samount", suBean.getSurchargeAmount());
		} else {
			jObject.put("status", "7000");
			// jObject.put("amount",suBean.getSarchargeAmount());
		}
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}
	
	public String calculateDMTSurcharge() {
		User us = (User) session.get("User");
		suBean.setId(us.getId());
		suBean = tService.calculateDMTSurcharge(suBean);
		JSONObject jObject = new JSONObject();
		if (suBean.getStatus().equalsIgnoreCase("1000")) {
			jObject.put("amount", suBean.getAmount());
			jObject.put("status", "1000");
			jObject.put("samount", suBean.getSurchargeAmount());
		} else {
			jObject.put("status", "7000");
			// jObject.put("amount",suBean.getSarchargeAmount());
		}
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}
	
	 private static byte[] loadFile(File file) throws IOException {
	     InputStream is = new FileInputStream(file);

	     long length = file.length();
	     if (length > Integer.MAX_VALUE) {
	         // File is too large
	     }
	     byte[] bytes = new byte[(int)length];
	     
	     int offset = 0;
	     int numRead = 0;
	     while (offset < bytes.length
	            && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	         offset += numRead;
	     }

	     if (offset < bytes.length) {
	         throw new IOException("Could not completely read file "+file.getName());
	     }

	     is.close();
	     return bytes;
	 }

	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response=arg0;
		
	}
	
	
//	code edited by amardeep on 28-mar-17
	
	 public String moneyTransfer(){
		 
//			SenderFavouriteBean  fBean=new SenderFavouriteBean();
		 	SenderFavouriteBean senderFavouriteBean=new SenderFavouriteBean();
			User u=(User)session.get("User");
			if(u!=null&&u.getUsertype()!=2){
				return "error";
			}
			String data=request.getParameter("data");
			Gson gson=new Gson();
			
			senderFavouriteBean.setAgentId(u.getId());
			
			String userAgent = request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}

			//User user = (User) session.get("User");

		List<SenderFavouriteViewBean>	fListBean = uService.getFavouriteList(senderFavouriteBean, ipAddress, userAgent);
			try {
				Gson gs = new Gson();

				String jsonString = gs.toJson(fListBean);
				JSONParser jsonParser = new JSONParser();
				JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
		 return "success";
	 }
	 
	
		public String getWalletBalance() {
			User us = (User) session.get("User");
			if(user!=null&&user.getUsertype()==1){
				return "null";
			}else{
			
			
			String walletBal = tService.getWalletBalance(us.getWalletid());
			JSONObject jObject = new JSONObject();
			jObject.put("amount", walletBal);
			us.setFinalBalance(Double.parseDouble(walletBal!=null?walletBal:"0.0"));
							
			try {
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
			}
			return null;
		}
	 
	 
		
		public String cashDepositRptList(){
			generateCsrfToken();
			User user=(User)session.get("User");
			
			if(user.getUsertype()==7){
			depositMast.setAggreatorId(user.getId());
			depositMast.setType("1");
			}else{
			depositMast.setAggreatorId(user.getAggreatorid());
			if(user.getWhiteLabel()==0){
				depositMast.setType("0");
			}else{
				depositMast.setType("1");
			}
			}
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
//				CashDepositMast deposit = tService.requsetCashDeposit(depositMast,ipAddress, userAgent);
				List<CashDepositMast> list=tService.getCashDepositReportByAggId(depositMast);
				request.setAttribute("eList",list);
				return "success";
		}
		
		
		public String showPassbookById(){
			  generateCsrfToken();
			  User us=(User)session.get("User");
			   Map<String, String> agentList=service.getAgentDister(us.getAggreatorid());
				if(agentList!=null&&agentList.size()>0){
					setAgentList(agentList);
				}
				inputBean.setAggreatorid(us.getAggreatorid());
			  inputBean.setWalletId(us.getWalletid());
			  logger.debug("*********************************calling show passbook************************************");
			  //tService.showPassbook(inputBean);
			  List<PassbookBean> listResult=tService.showPassbookById(inputBean);
			  request.setAttribute("listResult",listResult);
			  return "success";
			 }
		
		
		
		public String showPassbookForDist(){
			  generateCsrfToken();
			  User us=(User)session.get("User");
			   Map<String, String> agentList=service.getAgentByDistId(us.getId());
				if(agentList!=null&&agentList.size()>0){
					setAgentList(agentList);
				}
			  inputBean.setWalletId(us.getWalletid());
			  logger.debug("*********************************calling show passbook************************************");
			  //tService.showPassbook(inputBean);
			  List<PassbookBean> listResult=tService.showPassbookById(inputBean);
			  request.setAttribute("listResult",listResult);
			  return "success";
			 }
		
		
		
		public String getAgentConsolidatedReport(){
			  generateCsrfToken();
			  User us=(User)session.get("User");
			   Map<String, String> agentList=service.getAgentByDistId(us.getId());
				if(agentList!=null&&agentList.size()>0){
					setAgentList(agentList);
				}
			
			  logger.debug("*********************************calling show passbook************************************");
			
			  List<AgentConsolidatedReportBean> listResult=tService.getAgentConsolidatedReport(inputBean);
			  request.setAttribute("listResult",listResult);
			  return "success";
			 }
		
	
		
		public String showCashBackPassbook(){
			generateCsrfToken();
			User us=(User)session.get("User");
			inputBean.setWalletId(us.getWalletid());
			logger.debug("*********************************calling show showCashBackPassbook************************************");
			//tService.showPassbook(inputBean);
			List<PassbookBean> listResult=tService.showCashBackPassbook(inputBean);
			request.setAttribute("listResult",listResult);
			return "success";
		}
		
		public String cashBackPassbookSummary(){
			logger.debug("*********************************calling showCashBackPassbook summary service************************************");
			List<PassbookBean> listResult=tService.showCashBackPassbook(inputBean);
			request.setAttribute("listResult",listResult);
			return "success";
		}
	 
		
		
		public String showCashBackPassbookById(){
			  generateCsrfToken();
			  User us=(User)session.get("User");
			   Map<String, String> agentList=service.getAgentByAggId(us.getAggreatorid());
				if(agentList!=null&&agentList.size()>0){
					setAgentList(agentList);
				}
			  inputBean.setWalletId(us.getWalletid());
			  if(inputBean.getUserId() != null && !inputBean.getUserId().isEmpty() && inputBean.getUserId().equalsIgnoreCase("All"))
			  {
				  inputBean.setUserId("All|"+us.getUsertype()+"|"+us.getId());
			  }
			  logger.debug("*********************************calling show passbook************************************");
			  //tService.showPassbook(inputBean);
			  List<PassbookBean> listResult=tService.showCashBackPassbookById(inputBean);
			  request.setAttribute("listResult",listResult);
			  return "success";
			 }
		
		
		
		
		public String cashDepositApproveList(){
			generateCsrfToken();
			User user=(User)session.get("User");
			if(user.getUsertype()==7){
			depositMast.setAggreatorId(user.getId());
			}else{
			depositMast.setAggreatorId(user.getAggreatorid());
			}
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			if(user.getWhiteLabel()==0){
				depositMast.setType("0");
			}else{
				depositMast.setType("1");
			}
			List<CashDepositMast> list=tService.getCDAReqByAggId(depositMast);
				request.setAttribute("eList",list);
				return "success";
		}
		
		
		
		public String approveCashDeposit(){
			generateCsrfToken();
			User user=(User)session.get("User");
			
			if(user.getUsertype()==7 && !user.getAggreatorid().equals("OAGG001050")){
				depositMast.setAggreatorId(user.getId());
				}else{
			depositMast.setAggreatorId(user.getAggreatorid());
				}
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
				
				
				String result=tService.approvedCashDeposit(depositMast, ipAddress, userAgent);
				List<CashDepositMast> list=tService.getCDAReqByAggId(depositMast);
				request.setAttribute("eList",list);
				if(result.equalsIgnoreCase("True")){
					addActionMessage("Cash deposit accepted. ref number is :"+depositMast.getDepositId());
				}
				else{
					addActionError("Please try again latter.");
				}
				
				return "success";
		}
		
		
		
		
		public String rejectApproverCashDeposit(){
			generateCsrfToken();
			User user=(User)session.get("User");
			
			if(user.getUsertype()==7&&!user.getAggreatorid().equals("OAGG001050")){
				depositMast.setAggreatorId(user.getId());
				}else{
			depositMast.setAggreatorId(user.getAggreatorid());
				}
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
				
				String result=tService.rejectApproverCashDeposit(depositMast);
				List<CashDepositMast> list=tService.getCDAReqByAggId(depositMast);
				if(result.equalsIgnoreCase("True")){
					addActionMessage("Cash deposit rejected. ref number is :"+depositMast.getDepositId());
				}
				else{
					addActionError("Please try again latter.");
				}
				request.setAttribute("eList",list);
				return "success";
		}
		
		
		
		
		
		
		public String getSenderClosingBalReport(){
			logger.debug("*********************************calling rechargeReport************************************");
			User user=(User)session.get("User");
			ReportBean reportBean=new ReportBean();
			reportBean.setAggreatorid(user.getAggreatorid());
			reportBean.setStDate(inputBean.getStDate());
			logger.debug("*********************************calling service rechargeReport************************************");
			List<SenderClosingBalBean>	list=rService.getSenderClosingBalReport(reportBean);
			request.setAttribute("resultList",list);
			return "success";
		}
		
		
		
		public String getRefundTransactionReport(){
			logger.debug("*********************************calling getRefundTransactionReport************************************");
			User user=(User)session.get("User");
			ReportBean reportBean=new ReportBean();
			reportBean.setAggreatorid(user.getAggreatorid());
			reportBean.setStDate(inputBean.getStDate());
			logger.debug("*********************************calling service rechargeReport************************************"+inputBean.getStDate());
			List<RefundTransactionBean>	list=rService.getRefundTransactionReport(reportBean);
			request.setAttribute("resultList",list);
			return "success";
		}
		
			
		public String startPrefund(){
				generateCsrfToken();
				User us=(User)session.get("User");
				setPartnerList(tService.getPartnerList());
				setTxnSourceList(tService.getTxnSourceList());
				return "success";
		}
		
		
		public String savePrefund(){
			generateCsrfToken();
			User us=(User)session.get("User");
			setPartnerList(tService.getPartnerList());
			setTxnSourceList(tService.getTxnSourceList());
			partnerPrefundBean.setAggreatorid(us.getAggreatorid());
			if(us.getUsertype()==6){
			partnerPrefundBean.setUserId(us.getSubAggregatorId());
			}else{
				partnerPrefundBean.setUserId(us.getId());	
			}
			
			partnerPrefundBean=tService.savePartnerPrefund(partnerPrefundBean);
			
			if(partnerPrefundBean.getStatusCode().equals("300")){
				addActionMessage(partnerPrefundBean.getStatusMsg());
			}else{
				addActionError(partnerPrefundBean.getStatusMsg());	
			}
			
			return "success";
	}
		
	
		
		
		public String getPartnerLedger(){
			
			generateCsrfToken();
			User us=(User)session.get("User");
			setPartnerList(tService.getPartnerList());
			ReportBean reportBean=new ReportBean();
			reportBean.setPartnerName(inputBean.getPartnerName());
			reportBean.setStDate(inputBean.getStDate());
			reportBean.setEndDate(inputBean.getEndDate());
			reportBean.setAggreatorid(us.getAggreatorid());
			List<PartnerLedgerBean> listResult=rService.getPartnerLedger(reportBean);
			request.setAttribute("listResult",listResult);
			return "success";
		}
		
		
		
		
		public String getAgentClosingBalReport(){
			logger.debug("*********************************calling getAgentClosingBalReport************************************");
			User user=(User)session.get("User");
			ReportBean reportBean=new ReportBean();
			
			Map<String, String> aggrigatorList = service.getAggreator();
			if (aggrigatorList != null && aggrigatorList.size() > 0) {
				setAggrigatorList(aggrigatorList);
			}

			if (inputBean.getAggreatorid() == null || user.getWhiteLabel() == 1) {
				reportBean.setAggreatorid(user.getAggreatorid());
			} else {
				reportBean.setAggreatorid(inputBean.getAggreatorid());
			}
			
			reportBean.setStDate(inputBean.getStDate());
			logger.debug("*********************************calling service getAgentClosingBalReport************************************");
			List<AgentClosingBalBean>	list=rService.getAgentClosingBalReport(reportBean);
			request.setAttribute("resultList",list);
			return "success";
		}
		
		
		
	public String getWalletTxnDetailsReport() {
		logger.debug(
				"*********************************calling getWalletTxnDetailsReport************************************");
		User user = (User) session.get("User");
		ReportBean reportBean = new ReportBean();

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (inputBean.getAggreatorid() == null || user.getWhiteLabel() == 1) {
			reportBean.setAggreatorid(user.getAggreatorid());
		} else {
			reportBean.setAggreatorid(inputBean.getAggreatorid());
		}
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug(
				"*********************************calling service getWalletTxnDetailsReport************************************");
		List<WalletTxnDetailsRpt> list = rService.getWalletTxnDetailsReport(reportBean);
		request.setAttribute("resultList", list);
		return "success";
	}
		
		
		public String btocDmtReport(){
			logger.debug("*********************************calling btocDmtReport************************************");
			User user=(User)session.get("User");
			ReportBean reportBean=new ReportBean();
			
			reportBean.setAggreatorid(user.getAggreatorid());
			
			reportBean.setStDate(inputBean.getStDate());
			reportBean.setEndDate(inputBean.getEndDate());
			logger.debug("*********************************calling service btocDmtReport************************************");
			List<B2CMoneyTxnMast>	list=rService.getBtocDmtReport(reportBean);
			request.setAttribute("resultList",list);
			return "success";
		}
		
		public String showCashDeposite(){
			   generateCsrfToken();
			   User us=(User)session.get("User");
			   inputBean.setWalletId(us.getWalletid());
			   logger.debug("*********************************calling show showCashDeposite************************************");
			   //tService.showPassbook(inputBean);
			   List<PassbookBean> listResult=tService.showCashDeposite(inputBean);
			   request.setAttribute("listResult",listResult);
			   return "success";
			  }
	 /*public String getListOfRevokeUser()
	 {
		 logger.debug("*********************************calling getListOfRevokeUser************************************");
		 User user = (User)session.get("User");
			 try
			 {
				 String message = 
			 }
			 catch(Exception ex)
			 {
				 logger.debug("*******problem in getListOfRevokeUser*********"+ex.getMessage());
				 ex.printStackTrace();
				 return "fail";
			 }
	 }*/
		
		
		public String bhartipayPayementService() throws NoSuchAlgorithmException {
			
			if(inputBean.getTrxAmount() <=0 ){
				logger.debug("********************** inputBean.getTrxAmount() **************************"+inputBean.getTrxAmount());
				addActionError("Please Enter Valid Amount.");
				return "fail";
			}
			StringBuilder httpRequest = new StringBuilder();
			 
			 
			User user=(User)session.get("User");
			String aggId=(String)session.get("aggId");
			double amount = inputBean.getTrxAmount();
			
			int amountVal=(int) (amount*100);
			String dicemalAmount = String.valueOf(amountVal);
			
			 dicemalAmount = String.valueOf(dicemalAmount).split("\\.")[0];
			

			
			
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
			
			PGAggregatorDetailBean bean=	tService.getPGAGGId(aggId,userAgent,ipAddress);
			
			
			
			//String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
			String result=tService.savePGTrx(inputBean.getTrxAmount(), user.getWalletid(), user.getMobileno(), "add money","",aggId,ipAddress,userAgent);
			
			
			
			System.out.println("__________________result______________"+result);
			
			logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

			if(result.equalsIgnoreCase("1001")){
				addActionError("There is some problem please try again.");
				return "fail";
			}
			
			if(result.equalsIgnoreCase("7023")){
				addActionError("Invalid mobile no.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7014")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7017")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7018")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7019")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7020")){
				addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7021")){
				addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7022")){
				addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7045")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7046")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7047")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7048")){
				addActionError("Transaction failed, Wallet balance violated.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7000")){
				addActionError("Transaction fail.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7023")){
				addActionError("Invalid reciver mobile number.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7024")){
				addActionError("Transaction failed due to insufficient balance.");
				return "fail";
			}
			if(result.equalsIgnoreCase("7042")){
				addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
				return "fail";
			}
			
			
			String orderId=result;
			String pay_id=bean.getMid();
			String salt=bean.getTransactionkey();
			
			String name=user.getName();
			String mail=user.getEmailid();
			String contactNumber=user.getMobileno();
			//String returnurl="http://localhost:8080/BhartiPay/paymentGatewayResponse";
			String returnurl=bean.getDomainname()+"/paymentGatewayResponse";
			
			
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("AMOUNT", dicemalAmount);
			map.put("PAY_ID", pay_id);
			map.put("ORDER_ID", orderId);
			map.put("TXNTYPE", "SALE");
			map.put("CUST_NAME", name);
			map.put("CUST_PHONE", contactNumber);
			map.put("CUST_EMAIL", mail);
			map.put("CURRENCY_CODE", "356");
			map.put("RETURN_URL", returnurl);
			
			pgpayementPageParams=map;

			// Secret Key eec2fd91c96b4ebb
		//	String hash = ChecksumUtils.generateCheckSum(map, "eec2fd91c96b4ebb");


			//Secret Key eec2fd91c96b4ebb
			String  hash=null;
			
			try {
				hash = ChecksumUtils.generateCheckSum(map,salt);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			pgpayementPageParams.put("HASH",hash);
			
		  //  pgPaymentUrl="https://merchant.bhartipay.com/crm/jsp/paymentrequest";
		    pgPaymentUrl=bean.getPgurl();

		
			return "input";
		
			
		}	
		
		
		public String returnCallPyementGatewayPrevious() {
			
			User user=(User)session.get("User");
			String aggId=(String)session.get("aggId");
			Map<String, String[]> fieldMapObj = request.getParameterMap();
			 requestMap = new HashMap<String, String>();
			 
			 String result = "fail";
				String msg="";
				
				String hash="";
				String responseparams="";
				
				String trxId=null;
				String id=null;
				String amount="";
				String status="";

				Logger logger = Logger.getLogger(this.getClass().getName());
				logger.debug("********************************************** response come from Payment gateway = "
						+ response);
				if (response == null) {
					logger.debug("********************************************* getting resp Parameter as null so recharge could not be done   ***************************************");
					msg="Payment fialed.";
					return result;

				}
				else {
					for (Entry<String, String[]> entry : fieldMapObj.entrySet()) {
						try {
							requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

						} catch (ClassCastException classCastException) {
							logger.error("Exception", classCastException);
						}
					}
					
					trxId=(String)requestMap.get("ORDER_ID");
					id=(String)requestMap.get("TXN_ID");
					amount=(String)requestMap.get("AMOUNT");
					/*double doubleAmount=Double.valueOf(amount);
					
					int amountVal=(int) (doubleAmount/100);
					String dicemalAmount = String.valueOf(amountVal);
					
					 dicemalAmount = String.valueOf(dicemalAmount).split("\\.")[0];
					 amount=dicemalAmount;*/
					 
					status=(String)requestMap.get("STATUS");
					TransactionStatus =status;
					
					String issueBank=(String)requestMap.get("CARD_ISSUER_BANK");
					if(issueBank !=null ) {
					issueBank = issueBank.replaceAll(",", "");
					requestMap.remove("CARD_ISSUER_BANK");
					requestMap.put("CARD_ISSUER_BANK", issueBank);
					}
					
					if (status != null && status.contains("Captured")) {
						logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
						msg="Amount successfully added to your wallet.";
						addActionMessage(msg);
						result="success";
						String updateResult=tService.updatePGTrx(hash,responseparams,user.getId(),trxId,id,Double.parseDouble(amount), result, "");
					} else {
						logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
						msg="Payment failed.";
						addActionError(msg);
					
					}
					
				}
		
			return "success";

		}
		
		public String moneyWaysCallBack() {
			
			logger.info("-------------------#########################################################################----------------from moneyWaysCallBack---------------------------------");
			
			String userAgent=request.getHeader("User-Agent");
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			
			Map<String, String[]> fieldMapObj = request.getParameterMap();
			 requestMap = new HashMap<String, String>();
			
			for (Entry<String, String[]> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			logger.info("-------------------------requestMap---------------------------");
			logger.info(requestMap);
			
			String rechargeid=requestMap.get("recharge_tid");
			String rechargeStatus=requestMap.get("recharge_status");

			RechargeTxnBean rechargeBean=	tService.updateRechargeTxnBean(rechargeid,rechargeStatus,userAgent,ipAddress);
			
			/*if (rechargeStatus.equalsIgnoreCase("REFUND")) {
				tService.callCreditWallet(rechargeBean);
			}*/
			
			System.out.println(rechargeBean.getStatus());
			return "input";
			
		}
		
	public String subAggregatorUtility() {

		User user = (User) session.get("User");

		String aggId = (String) session.get("aggId");

		distributorList = tService.distributorList(aggId);
		subAggregatorList = tService.subAggregatorUtility(aggId);

		return "success";

	}
	
	
	public String distMapping() {

		User user = (User) session.get("User");

		String aggId = (String) session.get("aggId");
		
		distributorList = tService.superDistributerList(aggId);
		subAggregatorList = tService.distributorList(aggId);
		
		
		return "success";

	}

	public String showUtilUserDetailList() {
		User user = (User) session.get("User");

		String aggId = (String) session.get("aggId");
		subAggregatorList = tService.utilProfileChange(aggId);
		return "success";
	}
	
	public String utilProfileChange() {
		User user = (User) session.get("User");

		String aggId = (String) session.get("aggId");
		subAggregatorList = tService.utilProfileChange(aggId);
		return "success";
	}

	public String subAggregatorUtilityChange() throws IOException {

		System.out.println(customerId);

		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		String hj = tService.subAggregatorUtilityChange(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(hj);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}

	
	public String superDistributorUtilityChange() throws IOException {

		System.out.println(customerId);

		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		String hj = tService.superDistributorUtilityChange(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(hj);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}
	public String utilProfileAgentDetails() throws IOException {

		System.out.println(customerId);

		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		String hj = tService.utilProfileAgentDetails(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(hj);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}

	public String changeUserDistributor() throws IOException {

		System.out.println(customerId);

		System.out.println(distributorId);
		String message = tService.changeDistributor(customerId, distributorId);
		System.out.println(message);
		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		// String hj=tService.subAggregatorUtilityChange(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		System.out.println(message);
		response.getWriter().write(message);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}
	
	public String changeUserSuperDistributor() throws IOException {

		System.out.println(customerId);

		System.out.println(distributorId);
		String message = tService.changeSuperDistributor(customerId, distributorId);
		System.out.println(message);
		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		// String hj=tService.subAggregatorUtilityChange(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		System.out.println(message);
		response.getWriter().write(message);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}

	public String utilChangeAgentProfile() throws IOException {

		System.out.println(customerId);
		System.out.println(agentName);
		System.out.println(agentMobile);
		System.out.println(agentMail);

		String message = tService.utilChangeAgentProfile(customerId, agentName, agentMobile, agentMail);
		// subAggregatorUtilityDetails= tService.subAggregatorUtilityChange(customerId);
		// String hj=tService.subAggregatorUtilityChange(customerId);
		// request.setAttribute("subAggregatorUtilityDetails",
		// subAggregatorUtilityDetails);
		// PrintWriter pw= response.getWriter();

		// String responseToClient= "<tdcp><cmd><ack
		// cmd=�Init�><panelistid>3849303</panelistid></ack></cmd></tdcp>";

		// response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(message);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}
	
	@SuppressWarnings("unchecked")
	public String getMenuBankList() {
		System.out.println("Wellcome to getMenuBankList()!!!");
		logger.debug("*********** Wellcome to getMenuBankList()!!! *****************");
		try{
		
			User user=(User)session.get("User");
			List<String> bankList = new ArrayList<String>();
			Map<String,String> menuBankNameList=new HashMap<String,String>();
			
			if(user.getUsertype() == 4)
				bankList = uService.getCashDepositBank("OAGG001050");
			else
				bankList =uService.getCashDepositBank((String)session.get("aggId"));
			
			for(String bank : bankList)
				menuBankNameList.put(bank,bank);

			JSONObject jObjectBank =new JSONObject();
		      
			jObjectBank.put("statusCode","000");
			jObjectBank.put("status", "success"); 
			jObjectBank.put("statusMsg", menuBankNameList); 
		      
		      logger.debug("*********** jObject *****************");
			  logger.debug(jObjectBank);
			  
			
			response.getWriter().flush();
			response.getWriter().write(new Gson().toJson(jObjectBank));
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 
			      
			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
				  response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
			      
		      }catch(Exception eIn){
		      }
		}
		
		return "success";
	}
	
	
	// change by mani
	

	public String leanAccountUtility() {
		User user = (User) session.get("User");
		logger.debug("*********************************calling leanAccountUtility************************************");
		
		String aggId = (String) session.get("aggId");
		System.out.println("aggID "+aggId);
		subAggregatorList = tService.leanAccountUtility(aggId);
		System.out.println(subAggregatorList.size()+"******** "+subAggregatorList);
		
		return "success";
	}
	
	
	public String leanAccountDetails() throws IOException {

		logger.debug("*********************************calling leanAccountDetails************************************");
		
		System.out.println("Agent details -----");
        String hj = tService.leanAccountDetails(customerId);
	
		response.getWriter().write(hj);
		response.getWriter().flush();
		response.getWriter().close();
		return "success";

	}
	
	public String leanedAccountReport(){
		logger.debug("*********************************calling leanedAccountReport************************************");
		User user=(User)session.get("User");
		//System.out.println("ID   "+user.getId());
		//leanAccount.setAgentId(user.getId());
		//logger.debug("*********************************calling service bbpsReport************************************");
	
		String	output=rService.leanedAccountReport();


		try {
			response.getWriter().write(output);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "success";
	}
	
	public String saveLeanAccount() throws Exception {
		User user = (User) session.get("User");
		LeanAccount lean = new LeanAccount();
		logger.debug("*********************************calling saveLeanAccount************************************");
		
		lean.setName(request.getParameter("name"));
        lean.setAggregatorId(request.getParameter("aggregatorId"));
		lean.setWalletId(request.getParameter("walletId"));
        lean.setUserId(request.getParameter("userId"));
		lean.setAmount(Double.parseDouble(request.getParameter("amount")));

		String saveDtl = tService.saveLeanAccount(lean);

		response.getWriter().write(saveDtl);
		response.getWriter().flush();
		response.getWriter().close();

		List<LeanAccount> result = rService.leanReport(request.getParameter("aggregatorId"));
		request.setAttribute("leanResult",result);
		//lean();
		
		return "success";
	}
	
	
	
	public String updateLeanAmount() throws Exception 
	{
		
		logger.debug("*********************************calling updateLeanAmount************************************");
		
		User user = (User) session.get("User");
		LeanAccount lean = new LeanAccount();
		lean.setUserId(request.getParameter("userId"));
		lean.setAmount(Double.parseDouble(request.getParameter("amount")));
		String updateDtl = tService.updateLeanAmount(lean);
		
		response.getWriter().write(updateDtl);
		response.getWriter().flush();
		response.getWriter().close();
		
		return null;
	}
	
	
	public String deleteLeanRecord() throws Exception {
		
		logger.debug("*********************************calling deleteLeanRecord************************************");
		
		User user = (User) session.get("User");
		LeanAccount lean = new LeanAccount();
		lean.setUserId(request.getParameter("userId"));
		String deleteRecord = tService.deleteLeanRecord(lean);
		
		response.getWriter().write(deleteRecord);
		response.getWriter().flush();
		response.getWriter().close();
		
		return null;
	}
	
	
	
	public String findLeanRecord() throws Exception {
		User user = (User) session.get("User");
		LeanAccount lean = new LeanAccount();
		logger.debug("*********************************calling findLeanRecord************************************");
		lean.setUserId(request.getParameter("userId"));
		lean.setAggregatorId(request.getParameter("aggregatorId"));
		String leanRecord = tService.findLeanRecord(lean);
		
		response.getWriter().write(leanRecord);
		response.getWriter().flush();
		response.getWriter().close();
		
		return null;
	}
	
	
	public String lean() {
		User user = (User) session.get("User");
		logger.debug("*********************************calling leanAccountUtility************************************");
		String aggId = (String) session.get("aggId");
		
		Map<String, String> agentList=service.getAllUser(aggId);
		if(agentList!=null&&agentList.size()>0){
			setAgentList(agentList);
		}
		
		List<LeanAccount> result = rService.leanReport(aggId);
		request.setAttribute("leanResult",result);
		
	
		return "success";
	}

	public String leanReport()
	{
		Map<String, String> aggrigatorList=service.getAggreator();	
		if(aggrigatorList!=null&&aggrigatorList.size()>0){
			setAggrigatorList(aggrigatorList);
		}
		//walletBean.getAggreatorid()
		List<LeanAccount> result = rService.leanReport(walletBean.getAggreatorid());
		request.setAttribute("leanResult",result);
		
		return "success";	
		
	}
	
	public String accountDetail() throws Exception 
	{
	User user = (User) session.get("User");
	logger.debug("*********************************calling accountDetail************************************");
	
	 String userId = request.getParameter("userId");
	 //AddBankAccount accountDetail = tService.accountDetail(userId);
	 //request.setAttribute("accountDetail",accountDetail);
	
	
    String hj = tService.accountDetail(userId);
    response.getWriter().write(hj);
	response.getWriter().flush();
	response.getWriter().close();
	
	return "success";
    }
	
	
	//New PG Amount Add with Payment mode
	
   public String returnCallPyementGateway() {
		
	   logger.debug("************* response received from Payment gateway returnCallPyementGateway =");
	   
		User user=(User)session.get("User");
		String aggId=(String)session.get("aggId");
		Map<String, String[]> fieldMapObj = request.getParameterMap();
		 requestMap = new HashMap<String, String>();

		 System.out.println("Response from PG  fieldMapObj    "+fieldMapObj);
		 logger.debug("************* response come from Payment gateway = "+fieldMapObj);		
		 
		 String objGson = new Gson().toJson(fieldMapObj);
		 
		 
		 
		 System.out.println("returnCallPyementGateway Response String  "+objGson);
		 logger.debug("************* response come from Payment gateway = "+objGson);					
		
		 String result = "fail";
			String msg="";
			
			String hash="";
			String responseparams="";
			
			String trxId=null;
			String id=null;
			String amount="";
			String status="";
			String mode="";
			
	// changes on 15-05-2021
			
			String pgTxnDate="";
			String pgTxnResCode="";
			String custPhone="";
			String mopType="";
			String cardmask="";
			String currencyCode="";
			String pgTxnRrn="";
			String pgStatus="";
			String pgAmount="";
			String pgResMsg="";
			String paymentId="";
			String custEmail="";
			String pgGateway="";
			String pgTxnId="";
			String acqId="";
			String pgTxnType="";
			String dupYn="";
			String pgHash="";
			String pgPaymentType="";
			String pgReturnUrl="";
			String pgPayId="";
			String pgOrderId="";
			String custName="";
			String agentId="";
			StringBuffer other=new StringBuffer();			
			
			
			Logger logger = Logger.getLogger(this.getClass().getName());
			logger.debug("********************************************** response come from Payment gateway = "
					+ response);
			if (response == null) {
				logger.debug("********************************************* getting resp Parameter as null so recharge could not be done   ***************************************");
				msg="Payment fialed.";
				return result;

			}
			else {
				for (Entry<String, String[]> entry : fieldMapObj.entrySet()) {
					try {
						requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

						String pgResKey = entry.getKey();
						String pgResValue = ((String[]) entry.getValue())[0];
						
						    if("RESPONSE_DATE_TIME".equalsIgnoreCase(pgResKey))
						    {
						    	pgTxnDate= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway RESPONSE_DATE_TIME = "+pgTxnDate);
						    }
						    else if("RESPONSE_CODE".equalsIgnoreCase(pgResKey))
						    {
						    	pgTxnResCode= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway RESPONSE_CODE = "+pgTxnResCode);	
						    }
							else if("CUST_PHONE".equalsIgnoreCase(pgResKey))
							{
								custPhone= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway CUST_PHONE = "+custPhone);
							}
							else if("MOP_TYPE".equalsIgnoreCase(pgResKey))
							{
								mopType= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway MOP_TYPE = "+mopType);
							}
							else if("CARD_MASK".equalsIgnoreCase(pgResKey))
							{
								cardmask= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway CARD_MASK = "+cardmask);
							}
							else if("CURRENCY_CODE".equalsIgnoreCase(pgResKey))
							{
								currencyCode= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway CURRENCY_CODE = "+currencyCode);
							}
							else if("RRN".equalsIgnoreCase(pgResKey))
							{
								pgTxnRrn= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway RRN = "+pgTxnRrn);
							}
							else if("STATUS".equalsIgnoreCase(pgResKey))
							{
								pgStatus= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway STATUS = "+pgStatus);
							}
							else if("AMOUNT".equalsIgnoreCase(pgResKey))
							{
								pgAmount= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway AMOUNT = "+pgAmount);
							}
							else if("RESPONSE_MESSAGE".equalsIgnoreCase(pgResKey))
							{
								pgResMsg= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway RESPONSE_MESSAGE = "+pgResMsg);
							}
							else if("PAYMENT_ID".equalsIgnoreCase(pgResKey))
							{
								paymentId= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway PAYMENT_ID = "+paymentId);
							}
							else if("CUST_EMAIL".equalsIgnoreCase(pgResKey))
							{
								custEmail= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway CUST_EMAIL = "+custEmail);
							}
							else if("PG_GATEWAY".equalsIgnoreCase(pgResKey))
							{
								pgGateway= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway PG_GATEWAY = "+pgGateway);
							}
							else if("TXN_ID".equalsIgnoreCase(pgResKey))
							{
								pgTxnId= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway TXN_ID = "+pgTxnId);
							}
							else if("ACQ_ID".equalsIgnoreCase(pgResKey))
							{
								acqId= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway ACQ_ID = "+acqId);
							}
							else if("TXNTYPE".equalsIgnoreCase(pgResKey))
							{
								pgTxnType= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway TXNTYPE = "+pgTxnType);
							}
							else if("DUPLICATE_YN".equalsIgnoreCase(pgResKey))
							{
								dupYn= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway DUPLICATE_YN = "+dupYn);
							}
							else if("HASH".equalsIgnoreCase(pgResKey))
							{
								pgHash= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway HASH = "+pgHash);
							}
							else if("PAYMENT_TYPE".equalsIgnoreCase(pgResKey))
							{
								pgPaymentType= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway PAYMENT_TYPE = "+pgPaymentType);
							}
							else if("RETURN_URL".equalsIgnoreCase(pgResKey))
							{
								pgReturnUrl= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway RETURN_URL = "+pgReturnUrl);
							}
							else if("PAY_ID".equalsIgnoreCase(pgResKey))
							{
								pgPayId= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway PAY_ID = "+pgPayId);
							}
							else if("ORDER_ID".equalsIgnoreCase(pgResKey))
							{
								pgOrderId= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway ORDER_ID = "+pgOrderId);
							}
							else if("CUST_NAME".equalsIgnoreCase(pgResKey))
							{
								custName= pgResValue;
						    	logger.debug("************* response received returnCallPyementGateway CUST_NAME = "+custName);
							}
							else
							{
								other.append(pgResKey+" "+pgResValue+" ");
						    	logger.debug("************* response received ELSE returnCallPyementGateway "+pgResKey+" PG_GATEWAY = "+other);
							}
						 	
						
					} catch (ClassCastException classCastException) {
						logger.debug("*************Errors in  response received returnCallPyementGateway Problem in = "+classCastException.getMessage());
						logger.error("Exception", classCastException);
					}
				
				System.out.println("returnCallPyementGateway  date "+pgTxnDate+" resCode "+pgTxnResCode+" custPhone "+custPhone+" moptype "+mopType+" cardMask "+cardmask+
						" currencyCode "+currencyCode+"  pgTxnRrn  "+pgTxnRrn+"  pgStatus "+pgStatus+"  pgAmount "+pgAmount+" pgResMsg "+pgResMsg+" paymentId "+paymentId+
						"  custEmail "+custEmail+" pgGateway  "+pgGateway+" pgTxnId "+pgTxnId+" acqId  "+acqId+" pgTxnType "+pgTxnType+" dupYn  "+dupYn+"  pgHash "+pgHash+
						" pgPaymentType  "+pgPaymentType+"  pgReturnUrl "+pgReturnUrl+" pgPayId  "+pgPayId+"  pgOrderId "+pgOrderId+" custName  "+custName+
						"  userId  "+user.getId()+" other response with key & value  "+other);
				 }
				
			   PgReturnResponse pgReturnRes=new PgReturnResponse(pgTxnDate,pgTxnResCode,custPhone,mopType,cardmask,currencyCode,pgTxnRrn,pgStatus,pgAmount,pgResMsg,
					paymentId,custEmail,pgGateway,pgTxnId,acqId,pgTxnType,dupYn,pgHash,pgPaymentType,pgReturnUrl,pgPayId,pgOrderId,custName,user.getId());
				
			   String reresponse = tService.savePgReturnResponse(pgReturnRes);
			   logger.debug("************* returnCallPyementGateway save Payment gateway response = "+reresponse);	
			   
				String responseKey = new Gson().toJson(fieldMapObj);
				System.out.println("returnCallPyementGateway Response responseKey  "+responseKey);
				logger.debug("************* responseKey come from Payment gateway = "+responseKey);		
				
				
				mode=(String)requestMap.get("PAYMENT_TYPE");
				System.out.println(pgPaymentType+"    returnCallPyementGateway   mode   "+mode);
				
				trxId=(String)requestMap.get("ORDER_ID");
				id=(String)requestMap.get("TXN_ID");
				amount=(String)requestMap.get("AMOUNT");
				/*double doubleAmount=Double.valueOf(amount);
				
				int amountVal=(int) (doubleAmount/100);
				String dicemalAmount = String.valueOf(amountVal);
				
				 dicemalAmount = String.valueOf(dicemalAmount).split("\\.")[0];
				 amount=dicemalAmount;*/
				 
				status=(String)requestMap.get("STATUS");
				TransactionStatus =status;
			
				System.out.println(mode+"  mode  "+trxId+" trxId "+id+" id "+amount+"  amount "+status+"  status ");
				
				String issueBank=(String)requestMap.get("CARD_ISSUER_BANK");
				if(issueBank !=null ) {
				issueBank = issueBank.replaceAll(",", "");
				requestMap.remove("CARD_ISSUER_BANK");
				requestMap.put("CARD_ISSUER_BANK", issueBank);
				}
				
				if (status != null && status.contains("Captured")) {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
					msg="Amount successfully added to your wallet.";
					addActionMessage(msg);
					result="success";
					String updateResult=tService.updatePGTrxTest(hash,responseparams,user.getId(),trxId,id,Double.parseDouble(amount), result, "",pgPaymentType,cardmask);
				} else {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
					msg="Payment failed.";
					addActionError(msg);
				
				}
				
			}
	
		return "success";

	}
	
	 
   public String addAccountSelf()
    {
	   User user = (User) session.get("User");
	   
	   try {
	   
	    AddBankAccount account=new AddBankAccount();
	    account.setUserId(user.getId());
		account.setAggregatorId(user.getAggreatorid());
		String agg = depositMast.getAgent();
		System.out.println("AGG  "+agg);
		if(depositMast.getAgent()!=null && !depositMast.getAgent().equalsIgnoreCase("")) {
		account.setName(depositMast.getAgentname().trim());
		account.setBankName(depositMast.getBankName());
		account.setAccountNumber(depositMast.getAgent());
		account.setIfscCode(depositMast.getRemark());
		account.setMobileNo(user.getMobileno());
		account.setWalletId(user.getWalletid());
	   
		AddBankAccount acc = tService.addAccountSelf(account);
		   if("5".equalsIgnoreCase(acc.getAccountNumber()))
			  addActionMessage("You have added maximum account.");
			else if("0".equalsIgnoreCase(acc.getAccountNumber()))
			  addActionMessage("Account already exist.");	
			else if("1".equalsIgnoreCase(acc.getAccountNumber()))
			{	
			  addActionMessage("Account Details Saved.");
			  depositMast=null;
			}	
		}
		
		List<AddBankAccount> list = tService.getAllAccount(account);
		
		request.setAttribute("eList", list);
	   
	   }catch(Exception e)
	   {
		   
	   }
	   
	 return "success";
	}
	
   public String approveAccountSelf()
   {
        User user = (User) session.get("User");
	    AddBankAccount account=new AddBankAccount();
	    account.setUserId(user.getId());
		account.setAggregatorId(user.getAggreatorid());
	    //List<AddBankAccount> list = tService.getAllAccount(account);
		List<AddBankAccount> list = tService.getPendingAccount();
		request.setAttribute("eList", list); 
		
	   return "success";
   }
   
   public String updateAccountSelf()
   {
      User user = (User) session.get("User");
        
	  AddBankAccount account=new AddBankAccount();
	  account.setId(Integer.parseInt(depositMast.getDepositId()));
	  account.setAccountNumber(depositMast.getStatus());
      account.setComment(depositMast.getRemark());
	  tService.updateAccountSelf(account);
	  
	  List<AddBankAccount> list = tService.getPendingAccount();
	  request.setAttribute("eList", list); 
		
	  return "success";
   }
   
  
   
	
}

