package com.bhartipay.wallet.transaction.persistence.vo;




import java.io.Serializable;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TAG0 implements Serializable {

    @SerializedName("beneficiaryCode")
    @Expose
    private String beneficiaryCode;
    @SerializedName("beneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("beneficiaryType")
    @Expose
    private String beneficiaryType;
    @SerializedName("accountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("accountType")
    @Expose
    private String accountType;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    @SerializedName("active")
    @Expose
    private String active;

    /**
     * 
     * @return
     *     The beneficiaryCode
     */
    public String getBeneficiaryCode() {
        return beneficiaryCode;
    }

    /**
     * 
     * @param beneficiaryCode
     *     The BeneficiaryCode
     */
    public void setBeneficiaryCode(String beneficiaryCode) {
        this.beneficiaryCode = beneficiaryCode;
    }

    /**
     * 
     * @return
     *     The beneficiaryName
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * 
     * @param beneficiaryName
     *     The BeneficiaryName
     */
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    /**
     * 
     * @return
     *     The beneficiaryType
     */
    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    /**
     * 
     * @param beneficiaryType
     *     The BeneficiaryType
     */
    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    /**
     * 
     * @return
     *     The accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 
     * @param accountNumber
     *     The AccountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * 
     * @return
     *     The accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * 
     * @param accountType
     *     The AccountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * 
     * @return
     *     The iFSC
     */
    public String getIFSC() {
        return iFSC;
    }

    /**
     * 
     * @param iFSC
     *     The IFSC
     */
    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    /**
     * 
     * @return
     *     The active
     */
    public String getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The Active
     */
    public void setActive(String active) {
        this.active = active;
    }

}
