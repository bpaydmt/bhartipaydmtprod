package com.bhartipay.wallet.transaction.persistence.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class TransDetail {

	@SerializedName("agentTransId")
	 @Expose
	 private String agentTransId;
	 @SerializedName("mrTransId")
	 @Expose
	 private String mrTransId;
	 @SerializedName("topupTransId")
	 @Expose
	 private String topupTransId;
	 @SerializedName("transDateTime")
	 @Expose
	 private String transDateTime;
	 @SerializedName("amount")
	 @Expose
	 private String amount;
	 @SerializedName("status")
	 @Expose
	 private String status;
	 @SerializedName("reinitiate")
	 @Expose
	 private String reinitiate;
	 @SerializedName("benefAccNo")
	 @Expose
	 private String benefAccNo;
	 @SerializedName("originalTransId")
	 @Expose
	 private String originalTransId;
	 @SerializedName("remark")
	 @Expose
	 private String remark;
	public String getAgentTransId() {
		return agentTransId;
	}
	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}
	public String getMrTransId() {
		return mrTransId;
	}
	public void setMrTransId(String mrTransId) {
		this.mrTransId = mrTransId;
	}
	public String getTopupTransId() {
		return topupTransId;
	}
	public void setTopupTransId(String topupTransId) {
		this.topupTransId = topupTransId;
	}
	public String getTransDateTime() {
		return transDateTime;
	}
	public void setTransDateTime(String transDateTime) {
		this.transDateTime = transDateTime;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReinitiate() {
		return reinitiate;
	}
	public void setReinitiate(String reinitiate) {
		this.reinitiate = reinitiate;
	}
	public String getBenefAccNo() {
		return benefAccNo;
	}
	public void setBenefAccNo(String benefAccNo) {
		this.benefAccNo = benefAccNo;
	}
	public String getOriginalTransId() {
		return originalTransId;
	}
	public void setOriginalTransId(String originalTransId) {
		this.originalTransId = originalTransId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
