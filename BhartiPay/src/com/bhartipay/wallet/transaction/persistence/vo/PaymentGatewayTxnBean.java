package com.bhartipay.wallet.transaction.persistence.vo;

import javax.persistence.Column;

public class PaymentGatewayTxnBean {
	

	private String trxId;
	private String pgId;
	private double trxAmount;
	private String trxResult;
	private String trxReasion;
	private String trxCountry;
	private String trxCurrency;
	private String walletId;
	private String mobileNo;
	private String aggreatorid;
	private String trxOtherDetails;
	private String payTxnid;
	private String email;
	private String appName;
	private String imeiIP;
	private String userId;
	private String useragent;
	private String pgtxnDate;
	
	private String paymentMode;
	
	private String cardMask;
	
	private String creditStatus;
	 
	 
	public String getPgtxnDate() {
		return pgtxnDate;
	}

	public void setPgtxnDate(String pgtxnDate) {
		this.pgtxnDate = pgtxnDate;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getImeiIP() {
		return imeiIP;
	}

	public void setImeiIP(String imeiIP) {
		this.imeiIP = imeiIP;
	}

	public String getPayTxnid() {
		return payTxnid;
	}

	public void setPayTxnid(String payTxnid) {
		this.payTxnid = payTxnid;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public String getPgId() {
		return pgId;
	}

	public void setPgId(String pgId) {
		this.pgId = pgId;
	}

	public double getTrxAmount() {
		return trxAmount;
	}

	public void setTrxAmount(double trxAmount) {
		this.trxAmount = trxAmount;
	}

	public String getTrxResult() {
		return trxResult;
	}

	public void setTrxResult(String trxResult) {
		this.trxResult = trxResult;
	}

	public String getTrxReasion() {
		return trxReasion;
	}

	public void setTrxReasion(String trxReasion) {
		this.trxReasion = trxReasion;
	}

	public String getTrxCountry() {
		return trxCountry;
	}

	public void setTrxCountry(String trxCountry) {
		this.trxCountry = trxCountry;
	}

	public String getTrxCurrency() {
		return trxCurrency;
	}

	public void setTrxCurrency(String trxCurrency) {
		this.trxCurrency = trxCurrency;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getTrxOtherDetails() {
		return trxOtherDetails;
	}

	public void setTrxOtherDetails(String trxOtherDetails) {
		this.trxOtherDetails = trxOtherDetails;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCardMask() {
		return cardMask;
	}

	public void setCardMask(String cardMask) {
		this.cardMask = cardMask;
	}

	public String getCreditStatus() {
		return creditStatus;
	}

	public void setCreditStatus(String creditStatus) {
		this.creditStatus = creditStatus;
	}
	
	
	
	

}
