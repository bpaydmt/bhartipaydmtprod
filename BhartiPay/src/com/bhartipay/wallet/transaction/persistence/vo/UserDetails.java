package com.bhartipay.wallet.transaction.persistence.vo;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserDetails {

    @SerializedName("cardExists")
    @Expose
    private String cardExists;
    @SerializedName("cardDetail")
    @Expose
    private CardDetail cardDetail;
    @SerializedName("beneficiary")
    @Expose
    private Beneficiary beneficiary;
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    
    @SerializedName("balance")
    @Expose
    private String balance;
    
    @SerializedName("transDetails")
	@Expose
	private List<TransDetail> transDetails = new ArrayList<TransDetail>();
	
	@SerializedName("Count")
	@Expose
	private int Count;
	
	
	@SerializedName("moneyRemittance")
	@Expose
	private MoneyRemittance moneyRemittance;

	@SerializedName("bankDetail")
	@Expose
	private BankDetail bankDetail;
	@SerializedName("recharge")
	@Expose
	private Recharge recharge;
	
/*	@SerializedName("MobileNo")
	@Expose
	private String mobileNo;*/
	@SerializedName("agentTransId")
	@Expose
	private String agentTransId;
	 private String status;

	 
		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
    
    
    
    public List<TransDetail> getTransDetails() {
		return transDetails;
	}

	public void setTransDetails(List<TransDetail> transDetails) {
		this.transDetails = transDetails;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public MoneyRemittance getMoneyRemittance() {
		return moneyRemittance;
	}

	public void setMoneyRemittance(MoneyRemittance moneyRemittance) {
		this.moneyRemittance = moneyRemittance;
	}

	public BankDetail getBankDetail() {
		return bankDetail;
	}

	public void setBankDetail(BankDetail bankDetail) {
		this.bankDetail = bankDetail;
	}

	public Recharge getRecharge() {
		return recharge;
	}

	public void setRecharge(Recharge recharge) {
		this.recharge = recharge;
	}

	public String getAgentTransId() {
		return agentTransId;
	}

	public void setAgentTransId(String agentTransId) {
		this.agentTransId = agentTransId;
	}

	public String getCardExists() {
		return cardExists;
	}

	public void setCardExists(String cardExists) {
		this.cardExists = cardExists;
	}

	public CardDetail getCardDetail() {
		return cardDetail;
	}

	public void setCardDetail(CardDetail cardDetail) {
		this.cardDetail = cardDetail;
	}

	public Beneficiary getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(Beneficiary beneficiary) {
		this.beneficiary = beneficiary;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	private String requestNo;
    
    
    
    }
