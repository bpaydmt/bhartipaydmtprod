package com.bhartipay.wallet.transaction.persistence.vo;


import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recharge {

	@SerializedName("agentId")
	@Expose
	private String agentId;
	@SerializedName("paycTransId")
	@Expose
	private String paycTransId;
	@SerializedName("transDate")
	@Expose
	private String transDate;
	@SerializedName("amount")
	@Expose
	private String amount;
	@SerializedName("topupCharge")
	@Expose
	private String topupCharge;
	@SerializedName("product")
	@Expose
	private String product;
	@SerializedName("status")
	@Expose
	private String status;
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getPaycTransId() {
		return paycTransId;
	}
	public void setPaycTransId(String paycTransId) {
		this.paycTransId = paycTransId;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTopupCharge() {
		return topupCharge;
	}
	public void setTopupCharge(String topupCharge) {
		this.topupCharge = topupCharge;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
