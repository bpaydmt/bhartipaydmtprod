
package com.bhartipay.wallet.transaction.persistence.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DMTBean {

    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusDesc")
    @Expose
    private StatusDesc statusDesc;

    /**
     * 
     * @return
     *     The mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 
     * @param mobile
     *     The mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 
     * @return
     *     The statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * 
     * @param statusCode
     *     The statusCode
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * 
     * @return
     *     The statusDesc
     */
    public StatusDesc getStatusDesc() {
        return statusDesc;
    }

    /**
     * 
     * @param statusDesc
     *     The statusDesc
     */
    public void setStatusDesc(StatusDesc statusDesc) {
        this.statusDesc = statusDesc;
    }

}
