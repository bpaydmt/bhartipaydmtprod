package com.bhartipay.wallet.transaction.persistence.vo;

public class TxnInputBean {	
	
	private double trxAmount;
	private  String walletId;
	private  String  mobileNo;
	private  String  trxReasion;
	private  String userId;
	private  String  txnId;
	private  String  pgtxn;
	private  String  status;
	private  String  ipImei;
	private  String  stDate;
	private  String  endDate;
	private String bankid;
	private String txnType;
	private String aggreatorid;
	private double surChargeAmount;
	private String CHECKSUMHASH;
	
	
	private  String [] partnerName;
	
	
	
	
	
	
	

	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}
	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	public String[] getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String[] partnerName) {
		this.partnerName = partnerName;
	}
	public double getSurChargeAmount() {
		return surChargeAmount;
	}
	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getBankid() {
		return bankid;
	}
	public void setBankid(String bankid) {
		this.bankid = bankid;
	}
	public double getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(double trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getTrxReasion() {
		return trxReasion;
	}
	public void setTrxReasion(String trxReasion) {
		this.trxReasion = trxReasion;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getPgtxn() {
		return pgtxn;
	}
	public void setPgtxn(String pgtxn) {
		this.pgtxn = pgtxn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIpImei() {
		return ipImei;
	}
	public void setIpImei(String ipImei) {
		this.ipImei = ipImei;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	

}
