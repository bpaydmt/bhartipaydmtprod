package com.bhartipay.wallet.transaction.persistence.vo;

import java.io.Serializable;
import java.util.Date;



public class PartnerPrefundBean implements Serializable{
	
	
	private int id;
	private String aggreatorid;
	private String userId;
	private String partnerName;
	private String txnType;
	private double amount;
	private String txnSource;
	private String txnRefNo;
	private String partnerRefNo;
	private String description;
	private Date txnDate;
	private String statusCode;
	private String statusMsg;
	private String stDate;
	
	
	
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTxnSource() {
		return txnSource;
	}
	public void setTxnSource(String txnSource) {
		this.txnSource = txnSource;
	}
	public String getTxnRefNo() {
		return txnRefNo;
	}
	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}
	public String getPartnerRefNo() {
		return partnerRefNo;
	}
	public void setPartnerRefNo(String partnerRefNo) {
		this.partnerRefNo = partnerRefNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	
	
	
	
	
	
	
	

}
