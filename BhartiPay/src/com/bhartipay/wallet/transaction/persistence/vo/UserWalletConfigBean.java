package com.bhartipay.wallet.transaction.persistence.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="userwalletconfig")

public class UserWalletConfigBean {
	
	private String id;
	private String walletid;
	private double minbal;
	private double maxamtpertx;
	private double maxamtperday;
	private double maxamtperweek;
	private double maxamtpermonth;
	private int maxtxperday;
	private int maxtxperweek;
	private int maxtxpermonth;
	private double maxamtperacc;
	private double maxamtperquarter;
	private double maxamtperhalfyearly;
	private double maxamtperyear;
	private String status;
	private int txnType;
	private String aggreatorid;
	
	
	
	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public int getTxnType() {
		return txnType;
	}

	public void setTxnType(int txnType) {
		this.txnType = txnType;
	}

	public double getMaxamtperquarter() {
		return maxamtperquarter;
	}

	public void setMaxamtperquarter(double maxamtperquarter) {
		this.maxamtperquarter = maxamtperquarter;
	}

	public double getMaxamtperhalfyearly() {
		return maxamtperhalfyearly;
	}

	public void setMaxamtperhalfyearly(double maxamtperhalfyearly) {
		this.maxamtperhalfyearly = maxamtperhalfyearly;
	}

	public double getMaxamtperyear() {
		return maxamtperyear;
	}

	public void setMaxamtperyear(double maxamtperyear) {
		this.maxamtperyear = maxamtperyear;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public double getMinbal() {
		return minbal;
	}

	public void setMinbal(double minbal) {
		this.minbal = minbal;
	}

	public double getMaxamtpertx() {
		return maxamtpertx;
	}

	public void setMaxamtpertx(double maxamtpertx) {
		this.maxamtpertx = maxamtpertx;
	}

	public double getMaxamtperday() {
		return maxamtperday;
	}

	public void setMaxamtperday(double maxamtperday) {
		this.maxamtperday = maxamtperday;
	}

	public double getMaxamtperweek() {
		return maxamtperweek;
	}

	public void setMaxamtperweek(double maxamtperweek) {
		this.maxamtperweek = maxamtperweek;
	}

	public double getMaxamtpermonth() {
		return maxamtpermonth;
	}

	public void setMaxamtpermonth(double maxamtpermonth) {
		this.maxamtpermonth = maxamtpermonth;
	}

	public int getMaxtxperday() {
		return maxtxperday;
	}

	public void setMaxtxperday(int maxtxperday) {
		this.maxtxperday = maxtxperday;
	}

	public int getMaxtxperweek() {
		return maxtxperweek;
	}

	public void setMaxtxperweek(int maxtxperweek) {
		this.maxtxperweek = maxtxperweek;
	}

	public int getMaxtxpermonth() {
		return maxtxpermonth;
	}

	public void setMaxtxpermonth(int maxtxpermonth) {
		this.maxtxpermonth = maxtxpermonth;
	}

	public double getMaxamtperacc() {
		return maxamtperacc;
	}

	public void setMaxamtperacc(double maxamtperacc) {
		this.maxamtperacc = maxamtperacc;
	}
	
	
	
	
	

}
