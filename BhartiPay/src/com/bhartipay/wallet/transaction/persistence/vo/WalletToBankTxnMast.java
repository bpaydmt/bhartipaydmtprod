package com.bhartipay.wallet.transaction.persistence.vo;



import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class WalletToBankTxnMast {

	private String wtbtxnid;
	private String walletId;
	private String userId;
	private String payeename;
	private String payeecode;
	private String accno;
	private String ifsccode;
	private double amount;
	private String mobileno;
	private String email;
	private String status;
	private String wallettxnstatus;
	private String bankrefNo;
	private String banktxnstatus;
	private String ipimei;
	private String txndate;
	private String sendarname;
	private String sendarmobile;
	private String sendaremail;
	private  String statusCode;;
	private String aggreatorid;
	 private String acctype;
	 private String transferType;
	
	
	
	public String getAcctype() {
		return acctype;
	}
	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getPayeecode() {
		return payeecode;
	}
	public void setPayeecode(String payeecode) {
		this.payeecode = payeecode;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSendarname() {
		return sendarname;
	}
	public void setSendarname(String sendarname) {
		this.sendarname = sendarname;
	}
	public String getSendarmobile() {
		return sendarmobile;
	}
	public void setSendarmobile(String sendarmobile) {
		this.sendarmobile = sendarmobile;
	}
	public String getSendaremail() {
		return sendaremail;
	}
	public void setSendaremail(String sendaremail) {
		this.sendaremail = sendaremail;
	}
	public String getWtbtxnid() {
		return wtbtxnid;
	}
	public void setWtbtxnid(String wtbtxnid) {
		this.wtbtxnid = wtbtxnid;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletid) {
		this.walletId = walletid;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userid) {
		this.userId = userid;
	}
	public String getPayeename() {
		return payeename;
	}
	public void setPayeename(String payeename) {
		this.payeename = payeename;
	}
	public String getAccno() {
		return accno;
	}
	public void setAccno(String accno) {
		this.accno = accno;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getWallettxnstatus() {
		return wallettxnstatus;
	}
	public void setWallettxnstatus(String wallettxnstatus) {
		this.wallettxnstatus = wallettxnstatus;
	}
	public String getBankrefNo() {
		return bankrefNo;
	}
	public void setBankrefNo(String bankrefNo) {
		this.bankrefNo = bankrefNo;
	}
	public String getBanktxnstatus() {
		return banktxnstatus;
	}
	public void setBanktxnstatus(String banktxnstatus) {
		this.banktxnstatus = banktxnstatus;
	}
	public String getIpimei() {
		return ipimei;
	}
	public void setIpimei(String ipimei) {
		this.ipimei = ipimei;
	}
	public String getTxndate() {
		return txndate;
	}
	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}
	
	
	
	
	
}