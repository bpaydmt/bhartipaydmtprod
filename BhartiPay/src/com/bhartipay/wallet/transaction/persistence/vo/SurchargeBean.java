package com.bhartipay.wallet.transaction.persistence.vo;


import java.io.Serializable;

public class SurchargeBean implements Serializable{
	
	private String id;
	private String agentId;
	private  double amount; 
	private  double surchargeAmount; 
	private  String status;
	private  String txnCode;
	private double agentRefund;
	
	private double aggRefund;
	private double tdsApply;
	
	

	
	
	public double getTdsApply() {
		return tdsApply;
	}
	public void setTdsApply(double tdsApply) {
		this.tdsApply = tdsApply;
	}

	
	
	public double getAggRefund() {
		return aggRefund;
	}
	public void setAggRefund(double aggRefund) {
		this.aggRefund = aggRefund;
	}
	public double getAgentRefund() {
		return agentRefund;
	}
	public void setAgentRefund(double agentRefund) {
		this.agentRefund = agentRefund;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getSurchargeAmount() {
		return surchargeAmount;
	}
	public void setSurchargeAmount(double surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	
	
	
	
}
