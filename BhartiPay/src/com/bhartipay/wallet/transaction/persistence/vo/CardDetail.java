
package com.bhartipay.wallet.transaction.persistence.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class CardDetail {

    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("remitLimitAvailable")
    @Expose
    private Integer remitLimitAvailable;

    /**
     * 
     * @return
     *     The mobileno
     */
    public String getMobileno() {
        return mobileno;
    }

    /**
     * 
     * @param mobileno
     *     The Mobileno
     */
    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    /**
     * 
     * @return
     *     The balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * 
     * @param balance
     *     The Balance
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    /**
     * 
     * @return
     *     The remitLimitAvailable
     */
    public Integer getRemitLimitAvailable() {
        return remitLimitAvailable;
    }

    /**
     * 
     * @param remitLimitAvailable
     *     The RemitLimitAvailable
     */
    public void setRemitLimitAvailable(Integer remitLimitAvailable) {
        this.remitLimitAvailable = remitLimitAvailable;
    }

}
