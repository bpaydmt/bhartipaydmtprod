package com.bhartipay.wallet.transaction.persistence.vo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Beneficiary {

	   
    private List<TAG0> tAG0;

    @SerializedName("beneficiaryCode")
    @Expose
    private String beneficiaryCode;
    @SerializedName("beneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("accountType")
    @Expose
    private String accountType;
    @SerializedName("iFSC")
    @Expose
    private String iFSC;
    
    
    
    
    
	public String getBeneficiaryCode() {
		return beneficiaryCode;
	}

	public void setBeneficiaryCode(String beneficiaryCode) {
		this.beneficiaryCode = beneficiaryCode;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getiFSC() {
		return iFSC;
	}

	public void setiFSC(String iFSC) {
		this.iFSC = iFSC;
	}

	public List<TAG0> gettAG0() {
		return tAG0;
	}

	public void settAG0(List<TAG0> tAG0) {
		this.tAG0 = tAG0;
	}

   

}
