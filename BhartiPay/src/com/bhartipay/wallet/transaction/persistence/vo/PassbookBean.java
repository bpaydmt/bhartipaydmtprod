package com.bhartipay.wallet.transaction.persistence.vo;

import java.sql.Date;
public class PassbookBean {

	private String txnid;	
	private String txndate;
	private String walletid;
	private double txncredit;
	private double txndebit;
	private String payeedtl;
	private String txndesc;
	private String closingBal;
	private int status;
	private String resptxnid;
	private String closingbal;
	private String agentname;
	

	
	
	
	
	
	
	

	public String getAgentname() {
		return agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public String getClosingbal() {
		return closingbal;
	}

	public void setClosingbal(String closingbal) {
		this.closingbal = closingbal;
	}

	public String getResptxnid() {
		return resptxnid;
	}

	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getClosingBal() {
		return closingBal;
	}

	public void setClosingBal(String closingBal) {
		this.closingBal = closingBal;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}



	public String getTxndate() {
		return txndate;
	}

	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}

	public double getTxncredit() {
		return txncredit;
	}

	public void setTxncredit(double txncredit) {
		this.txncredit = txncredit;
	}

	public double getTxndebit() {
		return txndebit;
	}

	public void setTxndebit(double txndebit) {
		this.txndebit = txndebit;
	}

	public String getPayeedtl() {
		return payeedtl;
	}

	public void setPayeedtl(String payeedtl) {
		this.payeedtl = payeedtl;
	}

	public String getTxndesc() {
		return txndesc;
	}

	public void setTxndesc(String txndesc) {
		this.txndesc = txndesc;
	}
	
	
	
	
	
}
