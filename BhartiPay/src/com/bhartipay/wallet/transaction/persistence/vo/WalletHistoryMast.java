package com.bhartipay.wallet.transaction.persistence.vo;

import java.io.File;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
public class WalletHistoryMast {
	
	private String depositId;
	private String aggreatorId;
	private String userId;
	private String walletId;
	private String type;
	private Double amount;
	private String neftRefNo;
    private String reciptPic;
	private String status;
	private String depositDate;
	private String requestdate;
	private String ipIemi;
	private String agent;
	private String statusCode;
	private File myFile1;
	private String myFile1FileName;
	private String myFile1ContentType;
	private String usertype;
	private String bankName;
	private String branchName;
	private String remark;
	private String stDate;
	private String endDate;
	private String remarkApprover;
	private String checkerStatus;
	private String approverStatus;
	private String agentname;
	
	
	public String getAgentname() {
		return agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(String requestdate) {
		this.requestdate = requestdate;
	}

	public String getRemarkApprover() {
		return remarkApprover;
	}

	public void setRemarkApprover(String remarkApprover) {
		this.remarkApprover = remarkApprover;
	}

	public String getCheckerStatus() {
		return checkerStatus;
	}

	public void setCheckerStatus(String checkerStatus) {
		this.checkerStatus = checkerStatus;
	}

	public String getApproverStatus() {
		return approverStatus;
	}

	public void setApproverStatus(String approverStatus) {
		this.approverStatus = approverStatus;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}



	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public File getMyFile1() {
		return myFile1;
	}

	public void setMyFile1(File myFile1) {
		this.myFile1 = myFile1;
	}

	public String getMyFile1FileName() {
		return myFile1FileName;
	}

	public void setMyFile1FileName(String myFile1FileName) {
		this.myFile1FileName = myFile1FileName;
	}

	public String getMyFile1ContentType() {
		return myFile1ContentType;
	}

	public void setMyFile1ContentType(String myFile1ContentType) {
		this.myFile1ContentType = myFile1ContentType;
	}

	public String getIpIemi() {
		return ipIemi;
	}

	public void setIpIemi(String ipIemi) {
		this.ipIemi = ipIemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column( name = "desc", length=500)
	private String desc;

	public String getDepositId() {
		return depositId;
	}

	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getNeftRefNo() {
		return neftRefNo;
	}

	public void setNeftRefNo(String neftRefNo) {
		this.neftRefNo = neftRefNo;
	}

	public String getReciptPic() {
		return reciptPic;
	}

	public void setReciptPic(String reciptPic) {
		this.reciptPic = reciptPic;
	}

	

	public String getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(String depositDate) {
		this.depositDate = depositDate;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	

}
