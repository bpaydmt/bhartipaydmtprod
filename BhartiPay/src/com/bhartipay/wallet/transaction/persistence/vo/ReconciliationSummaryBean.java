package com.bhartipay.wallet.transaction.persistence.vo;


public class ReconciliationSummaryBean {
	private int totalTxn;
	private double totalAmount;
	
	
	public int getTotalTxn() {
		return totalTxn;
	}
	public void setTotalTxn(int totalTxn) {
		this.totalTxn = totalTxn;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
	
	
	
	

}
