package com.bhartipay.wallet.transaction.persistence.vo;


public class ReconciliationDetailsBean {
	

	
	private String aggreatorid;
	private String id;
	private String prepaidcardnumber;
	private String txnreqid;
	private String card_id;
	private String user_id;
	private String transaction_ref_no;
	private double transaction_amount;
	private String merchant_name;
	private String channel;
	private String transaction_type;
	 private String  merchant_id;
	 private String  terminal_id;
	 private double cashback_amount;
	
	
	
	public double getCashback_amount() {
		return cashback_amount;
	}
	public void setCashback_amount(double cashback_amount) {
		this.cashback_amount = cashback_amount;
	}
	public String getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getTerminal_id() {
		return terminal_id;
	}
	public void setTerminal_id(String terminal_id) {
		this.terminal_id = terminal_id;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrepaidcardnumber() {
		return prepaidcardnumber;
	}
	public void setPrepaidcardnumber(String prepaidcardnumber) {
		this.prepaidcardnumber = prepaidcardnumber;
	}
	public String getTxnreqid() {
		return txnreqid;
	}
	public void setTxnreqid(String txnreqid) {
		this.txnreqid = txnreqid;
	}
	public String getCard_id() {
		return card_id;
	}
	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTransaction_ref_no() {
		return transaction_ref_no;
	}
	public void setTransaction_ref_no(String transaction_ref_no) {
		this.transaction_ref_no = transaction_ref_no;
	}
	public double getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(double transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public String getMerchant_name() {
		return merchant_name;
	}
	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	
	
	
	
	
	
	
	

}
