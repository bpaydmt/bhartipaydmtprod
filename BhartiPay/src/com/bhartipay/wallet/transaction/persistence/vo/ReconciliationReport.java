package com.bhartipay.wallet.transaction.persistence.vo;


import java.util.List;

import com.bhartipay.wallet.commission.persistence.vo.CommInputBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMaster;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;

public class ReconciliationReport {
	
	private ReconciliationSummaryBean reconciliationSummaryBean;
	private List <ReconciliationDetailsBean> prePiadDetailsList;
	private List <RechargeTxnBean> rechargeTxnList;
	private List <WalletToBankTxnMast> walletToBankTxnList;
	List <CommInputBean> percList;
	private List<PaymentGatewayTxnBean> paymentGatewayTxnList;
	private String pgtxnDate;
	 
	 
	
	public String getPgtxnDate() {
		return pgtxnDate;
	}
	public void setPgtxnDate(String pgtxnDate) {
		this.pgtxnDate = pgtxnDate;
	}
	public List<PaymentGatewayTxnBean> getPaymentGatewayTxnList() {
		return paymentGatewayTxnList;
	}
	public void setPaymentGatewayTxnList(
			List<PaymentGatewayTxnBean> paymentGatewayTxnList) {
		this.paymentGatewayTxnList = paymentGatewayTxnList;
	}
	public ReconciliationSummaryBean getReconciliationSummaryBean() {
		return reconciliationSummaryBean;
	}
	public void setReconciliationSummaryBean(ReconciliationSummaryBean reconciliationSummaryBean) {
		this.reconciliationSummaryBean = reconciliationSummaryBean;
	}
	public List<ReconciliationDetailsBean> getPrePiadDetailsList() {
		return prePiadDetailsList;
	}
	public void setPrePiadDetailsList(List<ReconciliationDetailsBean> prePiadDetailsList) {
		this.prePiadDetailsList = prePiadDetailsList;
	}
	public List<RechargeTxnBean> getRechargeTxnList() {
		return rechargeTxnList;
	}
	public void setRechargeTxnList(List<RechargeTxnBean> rechargeTxnList) {
		this.rechargeTxnList = rechargeTxnList;
	}
	public List<WalletToBankTxnMast> getWalletToBankTxnList() {
		return walletToBankTxnList;
	}
	public void setWalletToBankTxnList(List<WalletToBankTxnMast> walletToBankTxnList) {
		this.walletToBankTxnList = walletToBankTxnList;
	}
	public List<CommInputBean> getPercList() {
		return percList;
	}
	public void setPercList(List<CommInputBean> percList) {
		this.percList = percList;
	}
	
	
	
	
	
	
	

}
