package com.bhartipay.wallet.transaction.persistence.vo;
public class WalletExist {
	
	
	  private String requestNo;
	  private String response;// STRING Y SUCCESS/ERROR
	  private String cardExists;// STRING Y Y If wallet exist else N
	  private String message;// STRING Y Response message
	  private String code;// SUCCESS: 300 FAILED: 1
	  private String balance;
	  private String remitLimitAvailable;
	  private String mobileno;
	  private String  otpVerify;
	public String getRequestNo() {
		return requestNo;
	}
	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getCardExists() {
		return cardExists;
	}
	public void setCardExists(String cardExists) {
		this.cardExists = cardExists;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getRemitLimitAvailable() {
		return remitLimitAvailable;
	}
	public void setRemitLimitAvailable(String remitLimitAvailable) {
		this.remitLimitAvailable = remitLimitAvailable;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getOtpVerify() {
		return otpVerify;
	}
	public void setOtpVerify(String otpVerify) {
		this.otpVerify = otpVerify;
	}
	 
	
}
