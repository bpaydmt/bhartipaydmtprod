package com.bhartipay.wallet.transaction.persistence.vo;



public class PGAggregatorDetailBean {
	

	
	private String id;
		
	
	private String mid;
	

	private String password;
	

	private String transactionkey;
	
	private String domainname;
	
	private String pgurl;
	
	

	public String getPgurl() {
		return pgurl;
	}

	public void setPgurl(String pgurl) {
		this.pgurl = pgurl;
	}
	

	public String getDomainname() {
		return domainname;
	}

	public void setDomainname(String domainname) {
		this.domainname = domainname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTransactionkey() {
		return transactionkey;
	}

	public void setTransactionkey(String transactionkey) {
		this.transactionkey = transactionkey;
	}
	

}
