package com.bhartipay.wallet.transaction.persistence.vo;

public class MoneyRequestBean {
private int id;
private String bank;
private String txnType;
private double amount;
private String utrNo;
private String aggId;
private String walletId;
private String senderName;
private String ipImei;
private String userAgent;
private String status;
private String requestStatus;
private String aggreatorid;
private String requestDate;
private String remark;




public String getRemark() {
	return remark;
}
public void setRemark(String remark) {
	this.remark = remark;
}
public String getAggreatorid() {
	return aggreatorid;
}
public String getRequestDate() {
	return requestDate;
}
public void setAggreatorid(String aggreatorid) {
	this.aggreatorid = aggreatorid;
}
public void setRequestDate(String requestDate) {
	this.requestDate = requestDate;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getRequestStatus() {
	return requestStatus;
}
public void setRequestStatus(String requestStatus) {
	this.requestStatus = requestStatus;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getIpImei() {
	return ipImei;
}
public String getUserAgent() {
	return userAgent;
}
public void setIpImei(String ipImei) {
	this.ipImei = ipImei;
}
public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
}
public String getAggId() {
	return aggId;
}
public String getWalletId() {
	return walletId;
}
public String getSenderName() {
	return senderName;
}
public void setAggId(String aggId) {
	this.aggId = aggId;
}
public void setWalletId(String walletId) {
	this.walletId = walletId;
}
public void setSenderName(String senderName) {
	this.senderName = senderName;
}
public String getBank() {
	return bank;
}
public String getTxnType() {
	return txnType;
}
public double getAmount() {
	return amount;
}
public String getUtrNo() {
	return utrNo;
}
public void setBank(String bank) {
	this.bank = bank;
}
public void setTxnType(String txnType) {
	this.txnType = txnType;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public void setUtrNo(String utrNo) {
	this.utrNo = utrNo;
}



}
