
package com.bhartipay.wallet.transaction.persistence.vo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class StatusDesc {

    @SerializedName("RequestNo")
    @Expose
    private String requestNo;
    @SerializedName("CardExists")
    @Expose
    private String cardExists;
    @SerializedName("CardDetail")
    @Expose
    private CardDetail cardDetail;
    @SerializedName("Beneficiary")
    @Expose
    private List<Object> beneficiary = new ArrayList<Object>();
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Code")
    @Expose
    private String code;

    /**
     * 
     * @return
     *     The requestNo
     */
    public String getRequestNo() {
        return requestNo;
    }

    /**
     * 
     * @param requestNo
     *     The RequestNo
     */
    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    /**
     * 
     * @return
     *     The cardExists
     */
    public String getCardExists() {
        return cardExists;
    }

    /**
     * 
     * @param cardExists
     *     The CardExists
     */
    public void setCardExists(String cardExists) {
        this.cardExists = cardExists;
    }

    /**
     * 
     * @return
     *     The cardDetail
     */
    public CardDetail getCardDetail() {
        return cardDetail;
    }

    /**
     * 
     * @param cardDetail
     *     The CardDetail
     */
    public void setCardDetail(CardDetail cardDetail) {
        this.cardDetail = cardDetail;
    }

    /**
     * 
     * @return
     *     The beneficiary
     */
    public List<Object> getBeneficiary() {
        return beneficiary;
    }

    /**
     * 
     * @param beneficiary
     *     The Beneficiary
     */
    public void setBeneficiary(List<Object> beneficiary) {
        this.beneficiary = beneficiary;
    }

    /**
     * 
     * @return
     *     The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The Response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The Code
     */
    public void setCode(String code) {
        this.code = code;
    }

}
