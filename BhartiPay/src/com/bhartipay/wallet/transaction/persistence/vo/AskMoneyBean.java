package com.bhartipay.wallet.transaction.persistence.vo;

public class AskMoneyBean {
	private String trxid;
	private String reqWalletId;
	private String aggreatorid;
	private String resMobile;
	private double reqAmount;
	private String status;
	private String askUserIp;
	private String askUserAgent;
	private String statusCode;
	private String otp;
	private String id;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getTrxid() {
		return trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}

	public String getReqWalletId() {
		return reqWalletId;
	}

	public void setReqWalletId(String reqWalletId) {
		this.reqWalletId = reqWalletId;
	}

	public String getResMobile() {
		return resMobile;
	}

	public void setResMobile(String resMobile) {
		this.resMobile = resMobile;
	}

	public double getReqAmount() {
		return reqAmount;
	}

	public void setReqAmount(double reqAmount) {
		this.reqAmount = reqAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAskUserIp() {
		return askUserIp;
	}

	public void setAskUserIp(String askUserIp) {
		this.askUserIp = askUserIp;
	}

	public String getAskUserAgent() {
		return askUserAgent;
	}

	public void setAskUserAgent(String askUserAgent) {
		this.askUserAgent = askUserAgent;
	}
	
	
	
	
	

}
