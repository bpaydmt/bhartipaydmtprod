package com.bhartipay.wallet.transaction.service;

import java.util.List;
import java.util.Map;

import com.bhartipay.lean.AddBankAccount;
import com.bhartipay.lean.LeanAccount;
import com.bhartipay.lean.PgReturnResponse;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.MoneyRequestBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGAggregatorDetailBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;


/**
 * 
 * @author Ambuj Singh
 *
 */
public interface TransactionService {


	/**
	 * 
	 * @param trxAmount
	 * @param walletId
	 * @param mobileNo
	 * @param trxReasion
	 * @return
	 */
	public String savePGTrx(double trxAmount,String walletId,String mobileNo,String trxReasion,String payTxnid,String aggreatorid,String ipimei,String userAgent);
	 /**
	  * 
	  * @param userId
	  * @param txnId
	  * @param pgTxnId
	  * @param txnAmount
	  * @param status
	  * @param imei
	  * @return
	  */
	public String updatePGTrx(String hash,String responseparams,String userId, String txnId,String pgTxnId, double txnAmount,String status,String imei);
	
	public String updatePGTrxTest(String hash,String responseparams,String userId, String txnId,String pgTxnId, double txnAmount,String status,String imei,String mode,String card);
	
	public String savePgReturnResponse(PgReturnResponse res);
	
	
	/**
	 * 
	 * @param mbean
	 * @return
	 */
	public String moneyRequest(CashDepositMast mbean);
	
	/**
	 * 
	 * @param walletid
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public List<PassbookBean> showPassbook(TxnInputBean inputBean);
	
	/**
	 * 
	 * @param walletid
	 * @param stDate
	 * @param endDate
	 * @return
	 */
	public List<PassbookBean> showOrdersView(TxnInputBean inputBean);
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String walletToWalletTransfer(TxnInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String cashIn(TxnInputBean inputBean,String ipimei,String userAgent);
	
	
	/**
	 * 
	 * @param wtob
	 * @return
	 */
	public WalletToBankTxnMast walletToBankTransfer(WalletToBankTxnMast wtob);
	
	/**
	 * 
	 * @param imputBean
	 * @return
	 */
	public DMTBean customerValidation(DMTInputBean imputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public DMTInputBean customerRegistration(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public DMTInputBean oTCConfirmation(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public String saveAskMoney(TxnInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public AskMoneyBean getCashOut(TxnInputBean inputBean,String ip,String ua);
	
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public AskMoneyBean cashOutUpdate(AskMoneyBean inputBean);
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist checkWalletExistRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist createWalletRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist verifyRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public WalletExist resendOtp(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getUserDetails(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails addBeneficiaryRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails deleteBeneficiaryRequest(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails mrTransfer(DMTInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails reinitiateMrTransfer(DMTInputBean inputBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getUserBalance(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getTransStatus(DMTInputBean inputBean);
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	public UserDetails getTransHistory(DMTInputBean inputBean);
	
	/***
	 * 
	 * @param bean
	 * @return
	 */
	public String getEscrowAccBal(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	List<EscrowBean> getEscrowTxnList(EscrowBean bean);
	
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public List<CashDepositMast> getCashDepositReqByUserId(CashDepositMast depositMast,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param cashDepositMast
	 * @return
	 */
	public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent);
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public List<CashDepositMast> getCashDepositReqByAggId(CashDepositMast depositMast);
	
	
	/**
	 * 
	 * @param cashDepositMast
	 * @return
	 */
	public String acceptCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param cashDepositMast
	 * @return
	 */
	public String acceptCashDepositByDist(CashDepositMast cashDepositMast,String ipimei,String userAgent);
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	public String rejectCashDeposit(CashDepositMast depositMast);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public RefundMastBean requestRefund(RefundMastBean bean,String ipimei,String userAgent);

	/**
	 * 
	 * @param bean
	 * @return
	 */
	public List<RefundMastBean> getRefundReqByAggId(RefundMastBean bean);
	
	/**
	 * 
	 * @param refundMastBean
	 * @return
	 */
	public String rejectRefundReq(RefundMastBean refundMastBean);
	
	/**
	 * 
	 * @param refundMastBean
	 * @return
	 */
	public String acceptRefundReq(RefundMastBean refundMastBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getPrePiadCardReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getRechargeReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getDMTReConReport(EscrowBean bean);
	
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public ReconciliationReport getPGReConReport(EscrowBean bean);
	
	/**
	 * 
	 * @param surchargeBean
	 * @return
	 */
	public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean);
	
	/**
	 * 
	 * @param surchargeBean
	 * @return
	 */
	public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public SurchargeBean calculateDMTSurcharge(SurchargeBean bean);
	
	
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	public String getWalletBalance(String walletId);
	
	/**
	 * 
	 * @param walletId
	 * @return
	 */
	public String getWalletBalanceById(String walletId);
	
	
	/**
	 * 
	 * @param depositMast
	 * @return
	 */
	
	public List<CashDepositMast> getCashDepositReportByAggId(CashDepositMast depositMast);
	
	
	/**
	 * 
	 * @param inputBean
	 * @return
	 */
	
	 public List<PassbookBean> showPassbookById(TxnInputBean inputBean);
	 
	 
	 /**
	  * 
	  * @param inputBean
	  * @return
	  */
	 public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(TxnInputBean inputBean);
	 
	 
		/**
		 * 
		 * @param walletid
		 * @param stDate
		 * @param endDate
		 * @return
		 */
		public List<PassbookBean> showCashBackPassbook(TxnInputBean inputBean);
		
		/**
		 * 
		 * @param inputBean
		 * @return
		 */
		public List<PassbookBean> showCashBackPassbookById(TxnInputBean inputBean) ;
		
		
		/**
		 * 
		 * @param depositMast
		 * @return
		 */
		public List<CashDepositMast> getCDAReqByAggId(CashDepositMast depositMast) ;
		
		/**
		 * 
		 * @param cashDepositMast
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public String approvedCashDeposit(CashDepositMast cashDepositMast,String ipimei, String userAgent);
		
		/**
		 * 
		 * @param depositMast
		 * @return
		 */
		public String rejectApproverCashDeposit(CashDepositMast depositMast);
		
		/**
		 * 
		 * @return
		 */
		public Map<String, String> getPartnerList() ;
		
		
		/**
		 * 
		 * @return
		 */
		public Map<String, String> getTxnSourceList();
		
		/**
		 * 
		 * @param bean
		 * @return
		 */
		public PartnerPrefundBean savePartnerPrefund(PartnerPrefundBean bean);
	 
		/**
		 * 
		 * @param b2cMoneyTxnMast
		 * @return
		 */
		 public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2cMoneyTxnMast);
		 
		 public List<PassbookBean> showCashDeposite(TxnInputBean inputBean);
		 
		 public PGAggregatorDetailBean getPGAGGId(String aggId,String ipimei,String userAgent);
		 
		 public RechargeTxnBean getRechargeTxnBean(String rechargeid, String userAgent, String ipAddress);
		public RechargeTxnBean updateRechargeTxnBean(String rechargeid,String rechargeStatus, String userAgent, String ipAddress);
		List<CashDepositMast> getCashDepositReqByUserIdPending(CashDepositMast depositMast, String ipimei,
				String userAgent);
		
		public Map<String, String> subAggregatorUtility(String aggId);
		
		public Map<String, String> superDistributerList(String aggId);
	
		public Map<String, String> distributorList(String aggId);
	
		public String subAggregatorUtilityChange(String customerId);
		
		public String superDistributorUtilityChange(String customerId);
	
		public String changeDistributor(String customerId, String distributorId);

		public String changeSuperDistributor(String customerId, String distributorId);
		
		public Map<String, String> utilProfileChange(String aggId);
	
		public String utilProfileAgentDetails(String customerId);
	
		public String utilChangeAgentProfile(String customerId, String agentName, String agentMobile, String agentMail);
		
		public void callCreditWallet(RechargeTxnBean rechargeBean);
		
		
		//change by mani
		
        public Map<String, String> leanAccountUtility(String aggId);
		public String leanAccountDetails(String customerId);
		public String saveLeanAccount(LeanAccount leaned) throws Exception;
		public String updateLeanAmount(LeanAccount leaned) throws Exception;
		public String deleteLeanRecord(LeanAccount lean) throws Exception;
		public String findLeanRecord(LeanAccount lean) throws Exception;
		
		public String pgTransactionsUpdate(TxnInputBean inputBean);
		public String getAmountPopup(String txnIdPassed);
		public String accountDetail(String agentId);
		public AddBankAccount addAccountSelf(AddBankAccount account); 
		public List<AddBankAccount> getAllAccount(AddBankAccount account); 
		public AddBankAccount updateAccountSelf(AddBankAccount account); 
		public List<AddBankAccount> getPendingAccount(); 
}
