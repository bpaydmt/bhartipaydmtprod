
package com.bhartipay.wallet.transaction.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.bhartipay.lean.AddBankAccount;
import com.bhartipay.lean.LeanAccount;
import com.bhartipay.lean.PgReturnResponse;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.JsonTOList;
import com.bhartipay.wallet.transaction.persistence.vo.MoneyRequestBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGAggregatorDetailBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class TransactionServiceImpl implements TransactionService{
public static Logger logger=Logger.getLogger(TransactionServiceImpl.class);	

	/*public String savePGTrx(double trxAmount,String walletId,String mobileNo,String trxReasion,String payTxnid,String aggreatorid,String ipimei,String userAgent){
		
		
		logger.debug("**************************** calling savePGTrx()************************************");
		
		System.out.println("______________________________savePGTrx____________________________");
		
		//private String payTxnid;
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("trxAmount",trxAmount);   
		requestPayload.put("walletId",walletId);
		requestPayload.put("mobileNo",mobileNo);
		requestPayload.put("trxReasion",trxAmount);
		requestPayload.put("payTxnid",payTxnid);
		requestPayload.put("aggreatorid",aggreatorid);
		
		System.out.println("_____trxAmount______"+trxAmount);
		System.out.println("_____walletId______"+walletId);
		System.out.println("_____mobileNo______"+mobileNo);
		System.out.println("_____trxReasion______"+trxReasion);
		System.out.println("_____trxReasion______"+trxAmount);
		System.out.println("_____payTxnid______"+payTxnid);
		System.out.println("_____aggreatorid______"+aggreatorid);
		
		
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		logger.debug("**************************** calling savePGTrx service with jsonString************************************");
		
		System.out.println("____________Calling savePGTrx Services with jsonString________________________");
		
		logger.debug("jsontext  :"+jsonText);	
		
		System.out.println("jsonText______________"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/savePGTrx");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		logger.debug("**************************** getting response from service ************************************"+response);
		
		if (response.getStatus() != 200) {
			
			logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

			   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("**************************** getting response from service ************************************"+output);
		return output;
	}*/

public String savePGTrx(double trxAmount,String walletId,String mobileNo,String trxReasion,String payTxnid,String aggreatorid,String ipimei,String userAgent){
	
	
	logger.debug("**************************** calling savePGTrx()************************************");
	
	System.out.println("______________________________savePGTrx____________________________");
	
	//private String payTxnid;
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("trxAmount",trxAmount);   
	requestPayload.put("walletId",walletId);
	requestPayload.put("mobileNo",mobileNo);
	requestPayload.put("trxReasion",trxAmount);
	requestPayload.put("payTxnid",payTxnid);
	requestPayload.put("aggreatorid",aggreatorid);
	
	System.out.println("_____trxAmount______"+trxAmount);
	System.out.println("_____walletId______"+walletId);
	System.out.println("_____mobileNo______"+mobileNo);
	System.out.println("_____trxReasion______"+trxReasion);
	System.out.println("_____trxReasion______"+trxAmount);
	System.out.println("_____payTxnid______"+payTxnid);
	System.out.println("_____aggreatorid______"+aggreatorid);
	
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	logger.debug("**************************** calling savePGTrx service with jsonString************************************");
	
	System.out.println("____________Calling savePGTrx Services with jsonString________________________");
	
	logger.debug("jsontext  :"+jsonText);	
	
	System.out.println("jsonText______________"+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/savePGTrx");
	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	logger.debug("**************************** getting response from service ************************************"+response);
	
	if (response.getStatus() != 200) {
		
		logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

		   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("**************************** getting response from service ************************************"+output);
	return output;
}
	
	
	public String moneyRequest(CashDepositMast mbean){
		
		Gson gson=new Gson();
		logger.debug("**************************** calling moneyRequest()************************************");
		String jsonText=gson.toJson(mbean);
		
		logger.debug("**************************** calling moneyRequest service with jsonString************************************");
	logger.debug("jsontext  :"+jsonText);	
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/moneyRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("**************************** getting response from service ************************************"+response);
		if (response.getStatus() != 200) {
			logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		MoneyRequestBean mBean=gson.fromJson(output,MoneyRequestBean.class);
		logger.debug("**************************** getting response from service ************************************"+output);
		return mBean.getStatus();
	}
	
	
	public Map<String,String> convertToMap(JsonObject jsonObject){
		Gson gson=new Gson();
		Map<String, String> myMap = gson.fromJson(jsonObject, Map.class);		
		return myMap;
	}



	/*@Override
	public String updatePGTrx(String hash,String responseparams,String userId, String txnId, String pgTxnId,double txnAmount, String status, String imei) {
		logger.debug("**************************** calling updatePGTrx ************************************");

		JSONObject  requestPayload=new JSONObject ();
		requestPayload.put("hash",hash); 
		requestPayload.put("responseParam",responseparams); 
		requestPayload.put("trxAmount",txnAmount);   
		requestPayload.put("userId",userId);
		requestPayload.put("txnId",txnId);
		requestPayload.put("pgtxn",pgTxnId);
		requestPayload.put("status",status);
		requestPayload.put("ipImei",imei);
		
		String jsonText = JSONValue.toJSONString(requestPayload);
		logger.debug("**************************** calling service updatePGTrx*********************************with jsonString***");
		logger.debug("json stirng "+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/updatePGTrx");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("**************************** getting response status ************************************"+response.getStatus());
		if (response.getStatus() != 200) {
		
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		logger.debug("**************************** getting output ************************************"+output);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
		return output;
	}*/
	
	
	@Override
	public String updatePGTrx(String hash,String responseparams,String userId, String txnId, String pgTxnId,double txnAmount, String status, String imei) {
	logger.debug("**************************** calling updatePGTrx ************************************");

	JSONObject  requestPayload=new JSONObject ();
	requestPayload.put("hash",hash); 
	requestPayload.put("responseParam",responseparams); 
	requestPayload.put("trxAmount",txnAmount);   
	requestPayload.put("userId",userId);
	requestPayload.put("txnId",txnId);
	requestPayload.put("pgtxn",pgTxnId);
	requestPayload.put("status",status);
	requestPayload.put("ipImei",imei);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	logger.debug("**************************** calling service updatePGTrx*********************************with jsonString***");
	logger.debug("json stirng "+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/updatePGTrx");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("**************************** getting response status ************************************"+response.getStatus());
	if (response.getStatus() != 200) {
	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	logger.debug("**************************** getting output ************************************"+output);
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	return output;
}

	
	@Override
	public String updatePGTrxTest(String hash,String responseparams,String userId, String txnId, String pgTxnId,double txnAmount, String status, String imei,String mode,String card) {
	logger.debug("**************************** calling updatePGTrx ************************************");

	JSONObject  requestPayload=new JSONObject ();
	requestPayload.put("hash",hash); 
	requestPayload.put("responseParam",responseparams); 
	requestPayload.put("trxAmount",txnAmount);   
	requestPayload.put("userId",userId);
	requestPayload.put("txnId",txnId);
	requestPayload.put("pgtxn",pgTxnId);
	requestPayload.put("status",status);
	requestPayload.put("ipImei",imei);
	requestPayload.put("paymentMode",mode);
	requestPayload.put("cardMask",card);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	logger.debug("**************************** calling service updatePGTrx*********************************with jsonString***"+"  mode  "+mode);
	logger.debug("json stirng "+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/updatePGTrxTest");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("**************************** getting response status ************************************"+response.getStatus());
	if (response.getStatus() != 200) {
	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	logger.debug("**************************** getting output ************************************"+output);
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	return output;
}

	@Override
	public String savePgReturnResponse(PgReturnResponse res) {

   	 logger.debug("**************************** calling updatePGTrx ************************************");

	 Gson gson=new Gson();
	 String jsonText=gson.toJson(res);
		
	 logger.debug("json stirng "+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/savePgReturnResponse");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("**************************** getting response status ************************************"+response.getStatus());
	if (response.getStatus() != 200) {
	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	logger.debug("**************************** getting output ************************************"+output);
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	return output;
}


	@Override
	public List<PassbookBean> showPassbook(TxnInputBean inputBean) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showPassbook");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		return list;
	}



	@Override
	public String walletToWalletTransfer(TxnInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/walletToWallet");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	@Override
	public String cashIn(TxnInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/cashIn");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	@Override
	public WalletToBankTxnMast walletToBankTransfer(WalletToBankTxnMast inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/tansferInAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletToBankTxnMast parameter2 = gson.fromJson(output, WalletToBankTxnMast.class);		
		return parameter2;
	}



	@Override
	public List<PassbookBean> showOrdersView(TxnInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showOrder");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		System.out.println(list);
		for(PassbookBean pb:list){
			System.out.println(pb.getWalletid());
			System.out.println(pb.getTxnid());
		}
		return list;
	}



	@Override
	public DMTBean customerValidation(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerValidation");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
	  
		String statusDesc = new JsonParser().parse(output).getAsJsonObject().get("statusDesc").toString().replace("\\", "").replace("\r", "");
		 JsonObject object3 = new JsonObject();
		 statusDesc=statusDesc.substring(1,statusDesc.length()-1);
	
		 if(new JsonParser().parse(output).getAsJsonObject().get("statusCode")!=null&& new JsonParser().parse(output).getAsJsonObject().get("statusCode").toString().equalsIgnoreCase("0")){
		 object3.add("statusDesc", new JsonParser().parse(statusDesc).getAsJsonObject());
		 }
		 else{
//		object3.add("statusDesc", new JsonParser().parse(output).getAsJsonObject().get("statusDesc")); 
		 }
		 object3.add("mobile",new JsonParser().parse(output).getAsJsonObject().get("mobile"));
		 object3.add("statusCode",new JsonParser().parse(output).getAsJsonObject().get("statusCode"));
		
		 
		DMTBean inBean = gson.fromJson(object3, DMTBean.class);		
		 return inBean;
	}



	@Override
	public DMTInputBean customerRegistration(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerRegistration");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		DMTInputBean parameter2 = gson.fromJson(output, DMTInputBean.class);
		String otcRefCode=new JsonParser().parse(parameter2.getStatusDesc()).getAsJsonObject().get("RequestNo").toString();
		parameter2.setOtcRefCode(otcRefCode.substring(1,otcRefCode.length()-1));
		return parameter2;
	}



	@Override
	public DMTInputBean oTCConfirmation(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerRegistration");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		DMTInputBean parameter2 = gson.fromJson(output, DMTInputBean.class);
		return parameter2;
	}

	 @Override
	 public String saveAskMoney(TxnInputBean inputBean) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/askMoney");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  
	  return output;
	 }

	 @Override
	 public AskMoneyBean getCashOut(TxnInputBean inputBean,String ipimei,String userAgent) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashOut");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  	 
	  return  gson.fromJson(output,AskMoneyBean.class);
	 }
	 
	 
	 @Override
	 public AskMoneyBean cashOutUpdate(AskMoneyBean inputBean) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/cashOutUpdate");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  	 
	  return  gson.fromJson(output,AskMoneyBean.class);
	 }
	@Override
	public WalletExist checkWalletExistRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/checkWalletExistRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist createWalletRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/createWalletRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist verifyRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/verifyRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist resendOtp(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/resendOtp");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public UserDetails getUserDetails(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getUserDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails addBeneficiaryRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/addBeneficiaryRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails deleteBeneficiaryRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/deleteBeneficiaryRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails mrTransfer(DMTInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/mrTransfer");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}
	
	@Override
	public UserDetails 	reinitiateMrTransfer(DMTInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/reinitiateMrTransfer");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}


	@Override
	public UserDetails getUserBalance(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getUserBalance");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails getTransStatus(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getTransStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails getTransHistory(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getTransHistory");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public String getEscrowAccBal(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getEscrowAccBal");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		//UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return output;
	}


	@Override
	public List<EscrowBean> getEscrowTxnList(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getEscrowTxnList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<EscrowBean> parameter2 = gson.fromJson(output, OTG1.class);
		return parameter2;
	}



	@Override
	public List<CashDepositMast> getCashDepositReqByUserId(
			CashDepositMast depositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByUserId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
	}

	@Override
	public List<CashDepositMast> getCashDepositReqByUserIdPending(CashDepositMast depositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByUserIdPending");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
	}


	@Override
	public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/requsetCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CashDepositMast parameter2 = gson.fromJson(output, CashDepositMast.class);
		return parameter2;
	}



	@Override
	public List<CashDepositMast> getCashDepositReqByAggId(
			CashDepositMast depositMast) {
			Gson gson = new Gson();
			String jsonText=gson.toJson(depositMast);
			
			WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByAggId");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200){
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			String output = response.getEntity(String.class);		
			List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
			return parameter2;
	}



	@Override
	public String acceptCashDeposit(CashDepositMast cashDepositMast,
			String ipimei, String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/acceptCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	@Override
	public String acceptCashDepositByDist(CashDepositMast cashDepositMast,
			String ipimei, String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/acceptCashDepositByDist");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public String rejectCashDeposit(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public RefundMastBean requestRefund(RefundMastBean bean, String ipimei,
			String userAgent) {
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/requestRefund");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		RefundMastBean parameter2 = gson.fromJson(output, RefundMastBean.class);
		return parameter2;	
		}



	@Override
	public List<RefundMastBean> getRefundReqByAggId(RefundMastBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getRefundReqByAggId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<RefundMastBean> parameter2 = gson.fromJson(output, OTG3.class);
		return parameter2;	
	}



	@Override
	public String rejectRefundReq(RefundMastBean refundMastBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(refundMastBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectRefundReq");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public String acceptRefundReq(RefundMastBean refundMastBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(refundMastBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/acceptRefundReq");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public ReconciliationReport getPrePiadCardReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getPrePiadCardReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public ReconciliationReport getRechargeReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getRechargeReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public ReconciliationReport getDMTReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getDMTReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}
	
	public ReconciliationReport getPGReConReport(EscrowBean bean){
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getPGReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(surchargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashinSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		surchargeBean = gson.fromJson(output, SurchargeBean.class);

		return surchargeBean;
	}



	@Override
	public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(surchargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashOutSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		surchargeBean = gson.fromJson(output, SurchargeBean.class);

		return surchargeBean;
	}



	@Override
	public SurchargeBean calculateDMTSurcharge(SurchargeBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashOutSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		bean = gson.fromJson(output, SurchargeBean.class);

		return bean;
	}
	
	
	
public String getWalletBalance(String walletId){
		logger.debug("**************************** calling getWalletBalance()************************************");
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("walletId",walletId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		logger.debug("**************************** calling getWalletBalance service with jsonString************************************");
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getWalletBalance");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("**************************** getting response from service ************************************"+output);
		return output;
	}


public String getWalletBalanceById(String walletId){
	logger.debug("**************************** calling getWalletBalanceById()************************************");
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("walletId",walletId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	logger.debug("**************************** calling getWalletBalanceById service with jsonString************************************");
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/getWalletBalancebyId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("**************************** getting response from service ************************************"+output);
	return output;
}

	
	

@Override
public List<CashDepositMast> getCashDepositReportByAggId(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReportByAggId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
}


	@Override
	public List<PassbookBean> showPassbookById(TxnInputBean inputBean) {
		List<PassbookBean> list = null;
		try {
			logger.debug("*************************** calling showPassbook ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("TransactionManager/showPassbookById");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
					jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			list = gson.fromJson(output, JsonTOList.class);
		/*	System.out.println(list);
			for (PassbookBean pb : list) {
				System.out.println(pb.getWalletid());
				System.out.println(pb.getTxnid());
			}*/
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<PassbookBean> showCashBackPassbookById(TxnInputBean inputBean) {
		List<PassbookBean> list = null;
		try {
			logger.debug("*************************** calling showPassbook ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("TransactionManager/showCashBackPassbookById");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
					jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			list = gson.fromJson(output, JsonTOList.class);
			System.out.println(list);
			for (PassbookBean pb : list) {
				System.out.println(pb.getWalletid());
				System.out.println(pb.getTxnid());
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(TxnInputBean inputBean) {
		List<AgentConsolidatedReportBean> list = null;
		try {
			logger.debug("*************************** calling getAgentConsolidatedReport ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("ReportManager/getAgentConsolidatedReport");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);		
			 list = gson.fromJson(output, OTG5.class);
		
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	
	@Override
	public List<PassbookBean> showCashBackPassbook(TxnInputBean inputBean) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showCashBackPassbook");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		return list;
	}
	
	
	
	
	@Override
	public List<CashDepositMast> getCDAReqByAggId(CashDepositMast depositMast) {
			Gson gson = new Gson();
			String jsonText=gson.toJson(depositMast);
			
			WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCDAReqByAggId");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200){
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			String output = response.getEntity(String.class);		
			List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
			return parameter2;
	}
	
	
	@Override
	public String approvedCashDeposit(CashDepositMast cashDepositMast,String ipimei, String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/approvedCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	@Override
	public String rejectApproverCashDeposit(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectApproverCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	
	
	@Override
	public Map<String, String> getPartnerList() {
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getPartnerList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public Map<String, String> getTxnSourceList() {
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getTxnSourceList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public PartnerPrefundBean savePartnerPrefund(PartnerPrefundBean bean) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/savePartnerPrefund");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		bean = gson.fromJson(output, PartnerPrefundBean.class);

		return bean;
	}
	
	@Override
	 public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2cMoneyTxnMast) {
	  logger.debug("*************************** calling showPassbook ***********************************");
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(b2cMoneyTxnMast);
	  
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/b2CMoneyTxn");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",b2cMoneyTxnMast.getIpimei()).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  B2CMoneyTxnMast resp = gson.fromJson(output, B2CMoneyTxnMast.class);
	  return resp;
	 }
	
	
	@Override
	 public List<PassbookBean> showCashDeposite(TxnInputBean inputBean) {
	  logger.debug("*************************** calling showCashDeposite ***********************************");
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/showCashDeposite");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
	  return list;
	 }
	
	
	
public PGAggregatorDetailBean getPGAGGId(String aggreatorid,String ipimei,String userAgent){
		
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("aggreatorid",aggreatorid); 
		String jsonText = JSONValue.toJSONString(requestPayload);
		Gson gson = new Gson();
        System.out.println("jsonText______________"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getPGAGGId");
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		logger.debug("**************************** getting response from service ************************************"+response);
		
		if (response.getStatus() != 200) {
			
			logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

			   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("**************************** getting response from service ************************************"+output);
	//	return output;
		
				
		PGAggregatorDetailBean parameter2 = gson.fromJson(output, PGAggregatorDetailBean.class);
		
		return parameter2;
		
	}

public RechargeTxnBean getRechargeTxnBean(String rechargeId,String ipimei,String userAgent){
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("rechargeId",rechargeId); 
	String jsonText = JSONValue.toJSONString(requestPayload);
	Gson gson = new Gson();
    System.out.println("jsonText______________"+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/getRechargeTxnBean");
	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	logger.debug("**************************** getting response from service ************************************"+response);
	
	if (response.getStatus() != 200) {
		
		logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

		   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("**************************** getting response from service ************************************"+output);
//	return output;
	
			
	RechargeTxnBean parameter2 = gson.fromJson(output, RechargeTxnBean.class);
	
	return parameter2;
	
}


public RechargeTxnBean updateRechargeTxnBean(String rechargeId,String rechargeStatus,String ipimei,String userAgent){
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("rechargeId",rechargeId); 
	requestPayload.put("status",rechargeStatus);
	String jsonText = JSONValue.toJSONString(requestPayload);
	Gson gson = new Gson();
    System.out.println("jsonText______________"+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/updateRechargeTxnBean");
	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	logger.debug("**************************** getting response from service ************************************"+response);
	
	if (response.getStatus() != 200) {
		
		logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

		   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("**************************** getting response from service ************************************"+output);
//	return output;
	
			
	RechargeTxnBean parameter2 = gson.fromJson(output, RechargeTxnBean.class);
	
	return parameter2;
	
}

	@Override
	public Map<String,String> distributorList(String aggId) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/distributorList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return pp;
	}
	
	@Override
	public Map<String,String> subAggregatorUtility(String aggId) {
		logger.debug("**************************** calling subAggregatorUtility ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/subAggregatorUtility");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return pp;
	}
	
	@Override
	public Map<String,String> superDistributerList(String aggId) {
		logger.debug("**************************** calling subAggregatorUtility ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/superDistributerListChange");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return pp;
	}
	
	@Override
	public String subAggregatorUtilityChange(String customerId) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/subAggregatorUtilityChange");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		


	String jsonStr = gson.toJson(pp);
		return jsonStr;
	}
	
	

	@Override
	public String superDistributorUtilityChange(String customerId) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/superDistributorUtilityChange");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		


	String jsonStr = gson.toJson(pp);
		return jsonStr;
	}
	
	@Override
	public Map<String,String> utilProfileChange(String aggId) {
		logger.debug("**************************** calling utilProfileChange ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/utilProfileChange");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return pp;
	}
	
	@Override
	public String utilProfileAgentDetails(String customerId) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/utilProfileAgentDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
	
	
	String jsonStr = gson.toJson(pp);
		return jsonStr;
	}
	
	@Override
	public String changeDistributor(String customerId,String distributorId) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("id",customerId); 
		requestPayload.put("distributerid",distributorId); 
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/changeDistributor");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		//Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return output;
	}
	
	@Override
	public String changeSuperDistributor(String customerId,String distributorId) {
		logger.debug("**************************** calling changeSuperDistributor ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("id",customerId); 
		requestPayload.put("distributerid",distributorId); 
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/changeSuperDistributor");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		return output;
	}

	@Override
	public String utilChangeAgentProfile(String customerId,String agentName,String agentMobile,String agentMail) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("id",customerId); 
		requestPayload.put("name",agentName); 
		requestPayload.put("mobileno",agentMobile); 
		requestPayload.put("emailid",agentMail); 
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/utilChangeAgentProfile");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	

		
		
		return output;
	}

	
	@Override
	public String getAmountPopup(String txnIdPassed) {
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(txnIdPassed);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getAmountPopup");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		String Output=response.getEntity(String.class);
		//List<PassbookBean> list = gson.fromJson(Output, JsonTOList.class);
		return Output;
	}

	
	@Override
	public String pgTransactionsUpdate(TxnInputBean inputBean) {
		String resp = null;
		logger.debug("**************************** calling pgTransactions ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/pgTransactionsUpdate");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		String Output=response.getEntity(String.class);
		if(response.getStatus() == 200) {
			resp= "success";
		}
	
		if (response.getStatus() != 200){
			resp="error";
	
		}
		/*
		 * String output = response.getEntity(String.class); return
		 * gson.fromJson(output, String.class);
		 */
	return Output;
		
	}
	
	
	@Override
	public void callCreditWallet(RechargeTxnBean rechargeBean) {

		JSONObject requestPayload = new JSONObject();
		
		if(rechargeBean.getRechargeId() != null)
			requestPayload.put("rechargeId", rechargeBean.getRechargeId());
		if(rechargeBean.getStatus() != null)
			requestPayload.put("status", rechargeBean.getStatus());
		if(rechargeBean.getUserId() != null)
			requestPayload.put("userId", rechargeBean.getUserId());
		if(rechargeBean.getWalletId() != null)
			requestPayload.put("walletId", rechargeBean.getWalletId());
		if(rechargeBean.getRechargeNumber() != null)
			requestPayload.put("rechargeNumber", rechargeBean.getRechargeNumber());
		if(rechargeBean.getRechargeType() != null)
			requestPayload.put("rechargeType", rechargeBean.getRechargeType());
		if(rechargeBean.getRechargeCircle() != null)
			requestPayload.put("rechargeCircle", rechargeBean.getRechargeCircle());
		if(rechargeBean.getRechargeOperator() != null)
			requestPayload.put("rechargeOperator", rechargeBean.getRechargeOperator());
		if(rechargeBean.getRechargeAmt() != null)
			requestPayload.put("rechargeAmt", rechargeBean.getRechargeAmt());
		if(rechargeBean.getCoupanAmt() != null)
			requestPayload.put("coupanAmt", rechargeBean.getCoupanAmt());
		if(rechargeBean.getWallettxnStatus() != null)
			requestPayload.put("wallettxnStatus", rechargeBean.getWallettxnStatus());
		if(rechargeBean.getOther() != null)
			requestPayload.put("other", rechargeBean.getOther());
		if(rechargeBean.getTxnAgent() != null)
			requestPayload.put("txnAgent", rechargeBean.getTxnAgent());
		if(rechargeBean.getIpIemi() != null)
			requestPayload.put("ipIemi", rechargeBean.getIpIemi());
		if(rechargeBean.getRechargeDate() != null)
			requestPayload.put("rechargeDate", rechargeBean.getRechargeDate());
		if(rechargeBean.getStatusCode() != null)
			requestPayload.put("statusCode", rechargeBean.getStatusCode());
		if(rechargeBean.getStdCode() != null)
			requestPayload.put("stdCode", rechargeBean.getStdCode());
		if(rechargeBean.getAccountNumber() != null)
			requestPayload.put("accountNumber", rechargeBean.getAccountNumber());
		if(rechargeBean.getAggreatorid() != null)
			requestPayload.put("aggreatorid", rechargeBean.getAggreatorid());
		if(rechargeBean.getOperatorTxnId() != null)
			requestPayload.put("operatorTxnId", rechargeBean.getOperatorTxnId());
		if(rechargeBean.getOperatorName() != null)
			requestPayload.put("operatorName", rechargeBean.getOperatorName());
		if(rechargeBean.getNumberSeries() != null)
			requestPayload.put("numberSeries", rechargeBean.getNumberSeries());
		if(rechargeBean.getCHECKSUMHASH() != null)
			requestPayload.put("checksumhash", rechargeBean.getCHECKSUMHASH());
		
		
		
		String jsonText = JSONValue.toJSONString(requestPayload);
		Gson gson = new Gson();
		 
	    System.out.println("jsonText______________"+jsonText);
	    logger.debug("callCreditWallet jsonText______________"+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/callCreditWallet");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",rechargeBean.getIpIemi()).post(ClientResponse.class,jsonText);
		logger.debug("**************************** getting response from service ************************************"+response);
		
		if (response.getStatus() != 200) {
			logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);
			   throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
		}
	}
	
	
	
	//change by mani

	
	@Override
	public Map<String,String> leanAccountUtility(String aggId) {
		logger.debug("**************************** calling leanAccountUtility ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/leanAccountUtility");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		
		return pp;
	}
	
	
	@Override
	public String leanAccountDetails(String customerId) {
		logger.debug("**************************** calling leanAccountDetails ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/leanAccountDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
	    String jsonStr = gson.toJson(pp);
		
	    return jsonStr;
	}
	
	
	
	@Override
	public String saveLeanAccount(LeanAccount lean)  {
		logger.debug("**************************** calling saveLeanAccount ************************************");
		Gson gson = new Gson();
	    //ObjectMapper Obj = new ObjectMapper();
	    //String jsonText = Obj.writeValueAsString(lean);
		System.out.println(lean.getUserId()+" 33   "+lean.getWalletId()+"     "+lean.getAmount());
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("userId",lean.getUserId()); 
		requestPayload.put("walletId",lean.getWalletId()); 
		requestPayload.put("amount",lean.getAmount());
		requestPayload.put("name",lean.getName());
		requestPayload.put("aggregatorId",lean.getAggregatorId());
		String jsonText = JSONValue.toJSONString(requestPayload);
	    //String jsonText=gson.toJson(lean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/saveLeanAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	
	@Override
	public String updateLeanAmount(LeanAccount lean) throws Exception {
		logger.debug("**************************** calling updateLeanAmount ************************************");
		Gson gson = new Gson();
	    ObjectMapper Obj = new ObjectMapper();
	    String jsonText = Obj.writeValueAsString(lean);
	    System.out.println(lean.getAmount()+"    222222  "+lean.getUserId());
		//String jsonText=gson.toJson(lean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/updateLeanAmount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	
	@Override
	public String deleteLeanRecord(LeanAccount lean) throws Exception  {
		logger.debug("**************************** calling deleteLeanRecord ************************************");
		Gson gson = new Gson();
	    ObjectMapper Obj = new ObjectMapper();
	    String jsonText = Obj.writeValueAsString(lean);
		//String jsonText=gson.toJson(id);
		System.out.println("22222 "+lean.getUserId());
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/deleteLeanRecord");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	

	@Override
	public String findLeanRecord(LeanAccount lean) throws Exception  {
		logger.debug("**************************** calling findLeanRecord ************************************");
		Gson gson = new Gson();
	    ObjectMapper Obj = new ObjectMapper();
	    String jsonText = Obj.writeValueAsString(lean);
		//String jsonText=gson.toJson(id);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/findLeanRecord");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	

	@Override
	public String accountDetail(String customerId) {
		logger.debug("**************************** calling leanAccountDetails ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/accountDetail");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		//AddBankAccount pp = gson.fromJson(output, AddBankAccount.class);
	    //String jsonStr = gson.toJson(pp);
		
	    return output;
	}


	@Override
	public AddBankAccount addAccountSelf(AddBankAccount account) {

		logger.debug("**************************** calling addAccountSelf ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/addAccountSelf");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		AddBankAccount accounts = gson.fromJson(output, AddBankAccount.class);
		
	  return accounts;
	}
	
	@Override
	public List<AddBankAccount> getAllAccount(AddBankAccount account) {

		logger.debug("**************************** calling addAccountSelf ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getAllAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		List<AddBankAccount> accounts = gson.fromJson(output, OTG6.class);
		
	  return accounts;
	}
	
	@Override
	public List<AddBankAccount> getPendingAccount() {

		logger.debug("**************************** calling getPendingAccount ************************************");
		Gson gson = new Gson();
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getPendingAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		List<AddBankAccount> accounts = gson.fromJson(output, OTG6.class);
		
	  return accounts;
	}


	@Override
	public AddBankAccount updateAccountSelf(AddBankAccount account) {
		logger.debug("**************************** calling updateAccountSelf ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/updateAccountSelf");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		AddBankAccount accounts = gson.fromJson(output, AddBankAccount.class);
		return accounts;
	}
	
	
	
	
}

class OTG1 extends ArrayList<EscrowBean>{
	
}
class OTG2 extends ArrayList<CashDepositMast>{
	
}
class OTG3 extends ArrayList<RefundMastBean>{
	
}
class OTG4 extends HashMap<String,Object>{
	 
}

class OTG5 extends ArrayList<AgentConsolidatedReportBean>{
	 
}
class OTG6 extends ArrayList<AddBankAccount>{
	
}
