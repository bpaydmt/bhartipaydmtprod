
package com.bhartipay.wallet.transaction.servicebk.impl;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.bhartipay.lean.AepsRedirection;
import com.bhartipay.lean.CmsEnquiryResponse;
import com.bhartipay.security.EncryptionDecryption;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.FinoAepsRequest;
import com.bhartipay.wallet.aeps.FinoAepsResponse;
import com.bhartipay.wallet.aeps.FinoHeader;
import com.bhartipay.wallet.aeps.TokenResponse;
import com.bhartipay.wallet.framework.security.EncryptionByEnc256;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.Utils.AEPSUtil;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.JsonTOList;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.transactionbk.service.TransactionService;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class TransactionServiceImpl implements TransactionService{
public static Logger logger=Logger.getLogger(TransactionServiceImpl.class);	
	public String savePGTrx(double trxAmount,String walletId,String mobileNo,String trxReasion,String payTxnid,String aggreatorid,String ipimei,String userAgent){
		
		
		logger.debug("**************************** calling savePGTrx()************************************");
		
		//private String payTxnid;
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("trxAmount",trxAmount);   
		requestPayload.put("walletId",walletId);
		requestPayload.put("mobileNo",mobileNo);
		requestPayload.put("trxReasion",trxAmount);
		requestPayload.put("payTxnid",payTxnid);
		requestPayload.put("aggreatorid",aggreatorid);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		logger.debug("**************************** calling savePGTrx service with jsonString************************************");
	logger.debug("jsontext  :"+jsonText);	
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/savePGTrx");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		logger.debug("**************************** getting response from service ************************************"+response);
		if (response.getStatus() != 200) {
			logger.debug("**************************** getting response from service status="+response.getStatus()+" ************************************"+response);

			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("**************************** getting response from service ************************************"+output);
		return output;
	}
	
	
	
	public Map<String,String> convertToMap(JsonObject jsonObject){
		Gson gson=new Gson();
		Map<String, String> myMap = gson.fromJson(jsonObject, Map.class);		
		return myMap;
	}



	@Override
	public String updatePGTrx(String hash,String responseparams,String userId, String txnId, String pgTxnId,double txnAmount, String status, String imei) {
		logger.debug("**************************** calling updatePGTrx ************************************");

		JSONObject  requestPayload=new JSONObject ();
		requestPayload.put("hash",hash); 
		requestPayload.put("responseParam",responseparams); 
		requestPayload.put("trxAmount",txnAmount);   
		requestPayload.put("userId",userId);
		requestPayload.put("txnId",txnId);
		requestPayload.put("pgtxn",pgTxnId);
		requestPayload.put("status",status);
		requestPayload.put("ipImei",imei);
		
		String jsonText = JSONValue.toJSONString(requestPayload);
		logger.debug("**************************** calling service updatePGTrx*********************************with jsonString***");
		logger.debug("json stirng "+jsonText);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/updatePGTrx");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("**************************** getting response status ************************************"+response.getStatus());
		if (response.getStatus() != 200) {
		
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		logger.debug("**************************** getting output ************************************"+output);
//		JsonParser parser = new JsonParser();
//		JsonObject jo = (JsonObject) parser.parse(output);
		return output;
	}



	@Override
	public List<PassbookBean> showPassbook(TxnInputBean inputBean) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showPassbook");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		return list;
	}



	@Override
	public String walletToWalletTransfer(TxnInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/walletToWallet");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	@Override
	public String cashIn(TxnInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/cashIn");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	@Override
	public WalletToBankTxnMast walletToBankTransfer(WalletToBankTxnMast inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/tansferInAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletToBankTxnMast parameter2 = gson.fromJson(output, WalletToBankTxnMast.class);		
		return parameter2;
	}



	@Override
	public List<PassbookBean> showOrdersView(TxnInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showOrder");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		System.out.println(list);
		for(PassbookBean pb:list){
			System.out.println(pb.getWalletid());
			System.out.println(pb.getTxnid());
		}
		return list;
	}



	@Override
	public DMTBean customerValidation(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerValidation");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
	  
		String statusDesc = new JsonParser().parse(output).getAsJsonObject().get("statusDesc").toString().replace("\\", "").replace("\r", "");
		 JsonObject object3 = new JsonObject();
		 statusDesc=statusDesc.substring(1,statusDesc.length()-1);
	
		 if(new JsonParser().parse(output).getAsJsonObject().get("statusCode")!=null&& new JsonParser().parse(output).getAsJsonObject().get("statusCode").toString().equalsIgnoreCase("0")){
		 object3.add("statusDesc", new JsonParser().parse(statusDesc).getAsJsonObject());
		 }
		 else{
//		object3.add("statusDesc", new JsonParser().parse(output).getAsJsonObject().get("statusDesc")); 
		 }
		 object3.add("mobile",new JsonParser().parse(output).getAsJsonObject().get("mobile"));
		 object3.add("statusCode",new JsonParser().parse(output).getAsJsonObject().get("statusCode"));
		
		 
		DMTBean inBean = gson.fromJson(object3, DMTBean.class);		
		 return inBean;
	}



	@Override
	public DMTInputBean customerRegistration(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerRegistration");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		DMTInputBean parameter2 = gson.fromJson(output, DMTInputBean.class);
		String otcRefCode=new JsonParser().parse(parameter2.getStatusDesc()).getAsJsonObject().get("RequestNo").toString();
		parameter2.setOtcRefCode(otcRefCode.substring(1,otcRefCode.length()-1));
		return parameter2;
	}



	@Override
	public DMTInputBean oTCConfirmation(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/CustomerRegistration");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		DMTInputBean parameter2 = gson.fromJson(output, DMTInputBean.class);
		return parameter2;
	}

	 @Override
	 public String saveAskMoney(TxnInputBean inputBean) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/askMoney");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  
	  return output;
	 }

	 @Override
	 public AskMoneyBean getCashOut(TxnInputBean inputBean,String ipimei,String userAgent) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashOut");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  	 
	  return  gson.fromJson(output,AskMoneyBean.class);
	 }
	 
	 
	 @Override
	 public AskMoneyBean cashOutUpdate(AskMoneyBean inputBean) {
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(inputBean);
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/cashOutUpdate");
	  //return transactionDao.askMoney(txnInputBean.getUserId(), txnInputBean.getWalletId(),txnInputBean.getMobileNo(), txnInputBean.getTrxAmount(), txnInputBean.getIpImei());
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  
	  String output = response.getEntity(String.class);  	 
	  return  gson.fromJson(output,AskMoneyBean.class);
	 }
	@Override
	public WalletExist checkWalletExistRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/checkWalletExistRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist createWalletRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/createWalletRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist verifyRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/verifyRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public WalletExist resendOtp(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/resendOtp");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		WalletExist parameter2 = gson.fromJson(output, WalletExist.class);
		return parameter2;
	}



	@Override
	public UserDetails getUserDetails(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getUserDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails addBeneficiaryRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/addBeneficiaryRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails deleteBeneficiaryRequest(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/deleteBeneficiaryRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails mrTransfer(DMTInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/mrTransfer");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}
	
	@Override
	public UserDetails 	reinitiateMrTransfer(DMTInputBean inputBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/reinitiateMrTransfer");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}


	@Override
	public UserDetails getUserBalance(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getUserBalance");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails getTransStatus(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getTransStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public UserDetails getTransHistory(DMTInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("DMTManager/getTransHistory");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return parameter2;
	}



	@Override
	public String getEscrowAccBal(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getEscrowAccBal");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		//UserDetails parameter2 = gson.fromJson(output, UserDetails.class);
		return output;
	}


	@Override
	public List<EscrowBean> getEscrowTxnList(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getEscrowTxnList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<EscrowBean> parameter2 = gson.fromJson(output, OTG1.class);
		return parameter2;
	}



	@Override
	public List<CashDepositMast> getCashDepositReqByUserId(
			CashDepositMast depositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByUserId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
	}

	@Override
	public List<CashDepositMast> getCashDepositReqByUserIdPending(
		CashDepositMast depositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByUserIdPending");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
	}

	public String sendSMSAlert(TxnInputBean inputBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/sendSMSAlert");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		//List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return output;
	}

	@Override
	public CashDepositMast requsetCashDeposit(CashDepositMast cashDepositMast,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/requsetCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		CashDepositMast parameter2 = gson.fromJson(output, CashDepositMast.class);
		return parameter2;
	}



	@Override
	public List<CashDepositMast> getCashDepositReqByAggId(
			CashDepositMast depositMast) {
			Gson gson = new Gson();
			String jsonText=gson.toJson(depositMast);
			
			WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReqByAggId");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200){
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			String output = response.getEntity(String.class);		
			List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
			return parameter2;
	}



	@Override
	public String acceptCashDeposit(CashDepositMast cashDepositMast,
			String ipimei, String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/acceptCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public String rejectCashDeposit(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public RefundMastBean requestRefund(RefundMastBean bean, String ipimei,
			String userAgent) {
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/requestRefund");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		RefundMastBean parameter2 = gson.fromJson(output, RefundMastBean.class);
		return parameter2;	
		}



	@Override
	public List<RefundMastBean> getRefundReqByAggId(RefundMastBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getRefundReqByAggId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<RefundMastBean> parameter2 = gson.fromJson(output, OTG3.class);
		return parameter2;	
	}



	@Override
	public String rejectRefundReq(RefundMastBean refundMastBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(refundMastBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectRefundReq");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public String acceptRefundReq(RefundMastBean refundMastBean,String ipimei,String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(refundMastBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/acceptRefundReq");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}



	@Override
	public ReconciliationReport getPrePiadCardReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getPrePiadCardReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public ReconciliationReport getRechargeReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getRechargeReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public ReconciliationReport getDMTReConReport(EscrowBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getDMTReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}
	
	public ReconciliationReport getPGReConReport(EscrowBean bean){
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getPGReConReport");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		return gson.fromJson(output,ReconciliationReport.class);
	}



	@Override
	public SurchargeBean calculateCashinSurcharge(SurchargeBean surchargeBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(surchargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashinSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		surchargeBean = gson.fromJson(output, SurchargeBean.class);

		return surchargeBean;
	}



	@Override
	public SurchargeBean calculateCashOutSurcharge(SurchargeBean surchargeBean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(surchargeBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashOutSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		surchargeBean = gson.fromJson(output, SurchargeBean.class);

		return surchargeBean;
	}



	@Override
	public SurchargeBean calculateDMTSurcharge(SurchargeBean bean) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/calculateCashOutSurcharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		bean = gson.fromJson(output, SurchargeBean.class);

		return bean;
	}
	
	
	
public String getWalletBalance(String walletId){
		logger.debug("**************************** calling getWalletBalance()************************************");
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("walletId",walletId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		logger.debug("**************************** calling getWalletBalance service with jsonString************************************");
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getWalletBalance");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("**************************** getting response from service ************************************"+output);
		return output;
	}


public String getWalletBalanceById(String walletId){
	logger.debug("**************************** calling getWalletBalanceById()************************************");
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("walletId",walletId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	logger.debug("**************************** calling getWalletBalanceById service with jsonString************************************");
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/getWalletBalancebyId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("**************************** getting response from service ************************************"+output);
	return output;
}

	
	

@Override
public List<CashDepositMast> getCashDepositReportByAggId(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCashDepositReportByAggId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
		return parameter2;
}


	@Override
	public List<PassbookBean> showPassbookById(TxnInputBean inputBean) {
		List<PassbookBean> list = null;
		try {
			logger.debug("*************************** calling showPassbook ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("TransactionManager/showPassbookById");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
					jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			list = gson.fromJson(output, JsonTOList.class);
		/*	System.out.println(list);
			for (PassbookBean pb : list) {
				System.out.println(pb.getWalletid());
				System.out.println(pb.getTxnid());
			}*/
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<PassbookBean> showCashBackPassbookById(TxnInputBean inputBean) {
		List<PassbookBean> list = null;
		try {
			logger.debug("*************************** calling showPassbook ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("TransactionManager/showCashBackPassbookById");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
					jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			list = gson.fromJson(output, JsonTOList.class);
			System.out.println(list);
			for (PassbookBean pb : list) {
				System.out.println(pb.getWalletid());
				System.out.println(pb.getTxnid());
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public List<AgentConsolidatedReportBean> getAgentConsolidatedReport(TxnInputBean inputBean) {
		List<AgentConsolidatedReportBean> list = null;
		try {
			logger.debug("*************************** calling getAgentConsolidatedReport ***********************************");
			Gson gson = new Gson();
			String jsonText = gson.toJson(inputBean);

			WebResource webResource = ServiceManager.getWebResource("ReportManager/getAgentConsolidatedReport");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);		
			 list = gson.fromJson(output, OTG5.class);
		
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	
	@Override
	public List<PassbookBean> showCashBackPassbook(TxnInputBean inputBean) {
		logger.debug("**************************** calling showPassbook ************************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(inputBean);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/showCashBackPassbook");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		List<PassbookBean> list = gson.fromJson(output, JsonTOList.class);
		return list;
	}
	
	
	
	
	@Override
	public List<CashDepositMast> getCDAReqByAggId(CashDepositMast depositMast) {
			Gson gson = new Gson();
			String jsonText=gson.toJson(depositMast);
			
			WebResource webResource=ServiceManager.getWebResource("TransactionManager/getCDAReqByAggId");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200){
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			String output = response.getEntity(String.class);		
			List<CashDepositMast> parameter2 = gson.fromJson(output, OTG2.class);
			return parameter2;
	}
	
	
	@Override
	public String approvedCashDeposit(CashDepositMast cashDepositMast,String ipimei, String userAgent) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(cashDepositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/approvedCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	@Override
	public String rejectApproverCashDeposit(CashDepositMast depositMast) {
		Gson gson = new Gson();
		String jsonText=gson.toJson(depositMast);
		
		WebResource webResource=ServiceManager.getWebResource("TransactionManager/rejectApproverCashDeposit");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		return output;
	}
	
	
	
	
	@Override
	public Map<String, String> getPartnerList() {
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getPartnerList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public Map<String, String> getTxnSourceList() {
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getTxnSourceList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public PartnerPrefundBean savePartnerPrefund(PartnerPrefundBean bean) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/savePartnerPrefund");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);		
		bean = gson.fromJson(output, PartnerPrefundBean.class);

		return bean;
	}
	
	@Override
	 public B2CMoneyTxnMast b2CMoneyTxn(B2CMoneyTxnMast b2cMoneyTxnMast) {
	  logger.debug("*************************** calling showPassbook ***********************************");
	  Gson gson = new Gson();
	  String jsonText=gson.toJson(b2cMoneyTxnMast);
	  
	  WebResource webResource=ServiceManager.getWebResource("TransactionManager/b2CMoneyTxn");
	  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",b2cMoneyTxnMast.getIpimei()).post(ClientResponse.class,jsonText);

	  if (response.getStatus() != 200){
	      throw new RuntimeException("Failed : HTTP error code : "
	    + response.getStatus());
	  }
	  String output = response.getEntity(String.class);  
	  B2CMoneyTxnMast resp = gson.fromJson(output, B2CMoneyTxnMast.class);
	  return resp;
	 }
	
	
	 public AEPSLedger saveAepsRequest(AEPSLedger aepsLedger){

		  logger.debug("*************************** calling saveAepsRequest ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/saveAepsRequest");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		  AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }
	 
	 public AEPSLedger getAepsLedger(AEPSLedger aepsLedger){

		  logger.debug("*************************** calling getAepsLedger ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/getAepsLedger");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		  AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }
	 
	 
	 public MATMLedger saveMatmRequest(MATMLedger matmLedger){

		  logger.debug("*************************** calling saveMatmRequest ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(matmLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/saveMatmRequest");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",matmLedger.getIpimei()).header("AGENT",matmLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		   MATMLedger resp = gson.fromJson(output, MATMLedger.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }
	 
	 
	 public TokenResponse generateToken(String agentAuthId, String agentAuthPassword, String agentid, String apiId, String pipe){

		  logger.debug("*************************** calling aadharshila generateToken ***********************************");
		  Gson gson = new Gson();
		  JSONObject jsonText=new JSONObject();
		  jsonText.put("agentAuthId",AEPSUtil.getMD5EncryptedValue(agentAuthId));
		  jsonText.put("agentAuthPassword",AEPSUtil.getMD5EncryptedValue(agentAuthPassword));
		  jsonText.put("retailerId",agentid);
		  jsonText.put("apiId",apiId);
		  jsonText.put("pipe",pipe);
		  logger.debug("*************************** calling aadharshila generateToken  request***********************************"+jsonText);

		  WebResource webResource=ServiceManager.getAsAepsTokenUrl();
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  logger.debug("*************************** calling aadharshila generateToken response ***********************************"+output);

		  try{
		  TokenResponse resp = gson.fromJson(output, TokenResponse.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }
	 
	 public FinoAepsResponse finoAEPSCall(AEPSLedger aepsLedger)throws Exception{

		  logger.debug("*************************** calling finoAEPSCall  ***********************************");
		  Gson gson = new Gson();
		  
		  FinoHeader finoHeader=new FinoHeader();
		  finoHeader.setAuthKey(aepsLedger.getAepsConfig().getAgentAuthPassword());
		  finoHeader.setClientId(aepsLedger.getAepsConfig().getAgentAuthId());
		  String jsonHeader=gson.toJson(finoHeader);
		  String encryptedJsonHeader=EncryptionDecryption.getEncrypted(jsonHeader, aepsLedger.getAepsConfig().getSecretKey());
		  FinoAepsRequest aepsRequest=new FinoAepsRequest();
		  aepsRequest.setAmount(""+aepsLedger.getAmount());
		  aepsRequest.setClientRefID(aepsLedger.getTxnId());
		  aepsRequest.setMerchantId(aepsLedger.getAgentCode());
		  aepsRequest.setRETURNURL(aepsLedger.getAepsConfig().getAepsCallbackURL());
		  aepsRequest.setSERVICEID(aepsLedger.getTxnType());
		  aepsRequest.setVersion("1000");
		  
		  String jsonText=gson.toJson(aepsRequest);
		  String encryptedJsonText=EncryptionDecryption.getEncrypted(jsonText, aepsLedger.getAepsConfig().getSecretKey());
		  WebResource webResource=ServiceManager.getFinoAepsUrl();
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("Authentication",encryptedJsonHeader).post(ClientResponse.class,encryptedJsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		  FinoAepsResponse resp = gson.fromJson(output, FinoAepsResponse.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }
	 
	
	 public AEPSLedger updateAepsRequest(AEPSLedger aepsLedger){

		  logger.debug("*************************** calling updateAepsRequest ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/updateAepsRequest");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return resp;
		 
		 
	 }
	
	 public String finoAepsResponse(String jsonText){

		  logger.debug("*************************** calling finoAepsResponse ***********************************");
		  Gson gson = new Gson();
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/AepsResponse");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  //AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return output;
		 
		 
	 }

	 public String finoAepsStatusResponse(String jsonText){

		  logger.debug("*************************** calling finoAepsResponse ***********************************");
		  Gson gson = new Gson();
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/AepsStatusResponse");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  //AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return output;
		 
		 
	 }

	 public String finoMATMStatusResponse(String jsonText){

		  logger.debug("*************************** calling finoAepsResponse ***********************************");
		  Gson gson = new Gson();
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/MatmStatusResponse");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  //AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return output;
		 
		 
	 }
	 
	 
	 public String finoMatmResponse(String jsonText){

		  logger.debug("*************************** calling finoMatmResponse ***********************************");
		  Gson gson = new Gson();
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/MatmResponse");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  //AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return output;
		 
		 
	 }
	 
	 
	 public MATMLedger getMatmLedger(String txnid) {
		 logger.debug("*************************** calling finoMatmResponse ***********************************");
		  Gson gson = new Gson();
		  JSONObject json=new JSONObject();
		  json.put("txnId", txnid);
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/getMatmLedger");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,json.toString());

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class); 
		  output = output.replaceAll("txnDate", "date");
		  MATMLedger resp = gson.fromJson(output, MATMLedger.class);
		  return resp;
	 }
	 public String asAadharShilaResponse(String jsonText){

		  logger.debug("*************************** calling finoMatmResponse ***********************************");
		  Gson gson = new Gson();
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/asAepsResponse");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("developer_key", "Ib0buhlDB3AIn505e3kJag==").post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  //AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return output;
		 
		 
	 }
	
	public static void main(String s[])
	{
//		String val="{\"rrn\":\"020915692390\",\"stanNo\":\"692390\",\"txnId\":\"100772007271504318715223\",\"aepstxnId\":\"439001817861\",\"action\":\"Credit\",\"device\":\"Mantra MFS 100\",\"status\":\"0\",\"txnStatus\":\"Success\",\"bankName\":\"ICICI Bank\",\"uId\":\"XXXXXXXX6645\",\"authCode\":\"0a62bdd4dcd04c7784bb69cc17b02261\",\"deviceNo\":\"3035813\",\"balance\":\"58659.26\",\"Agent_Id\":\"BPA001000\",\"Service\":\"Balance Enquiry\",\"Amount\":100.0}";
		String val="{\"rrn\":\"021016404428\",\"stanNo\":\"404428\",\"txnId\":\"100772007281657102232045\",\"aepstxnId\":\"843585499290\",\"action\":\"Credit\",\"device\":\"Mantra MFS 100\",\"status\":\"0\",\"txnStatus\":\"Success\",\"bankName\":\"Bank Of Baroda\",\"uId\":\"XXXXXXXX4698\",\"authCode\":\"c38efdeaea8142519db514742166219b\",\"deviceNo\":\"3035813\",\"balance\":\"116559.61\",\"Agent_Id\":\"BPA001000\",\"Service\":\"Balance Enquiry\",\"Amount\":0.0}";

		
		
		//		AEPSLedger aepsLedger = new Gson().fromJson(val, AEPSLedger.class);
//		MATMLedger matmLedger = new Gson().fromJson(val, MATMLedger.class);
//
//		new TransactionServiceImpl().saveAepsRequest(aepsLedger);
		new TransactionServiceImpl().asAadharShilaResponse(val);
		
		
	}

   
	public AEPSLedger saveAepsRequestApi(AEPSLedger aepsLedger){

		  logger.debug("*************************** calling saveAepsRequestApi ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("TransactionManager/saveAepsRequestApi");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		  AEPSLedger resp = gson.fromJson(output, AEPSLedger.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	 }



	@Override
	public AepsRedirection saveNewAepsRequest(AEPSLedger aepsLedger) {

		  logger.debug("*************************** calling getAepsLedger ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("AepsApi/aepsApiRequest");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		   AepsRedirection resp = gson.fromJson(output, AepsRedirection.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	}



	@Override
	public AepsRedirection saveNewAepsEnquiryRequest(AEPSLedger aepsLedger) {


		  logger.debug("*************************** calling getAepsLedger ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("AepsApi/aepsApiEnquiry");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		   AepsRedirection resp = gson.fromJson(output, AepsRedirection.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
		
	}



	@Override
	public AepsRedirection makeAepsTxnStatus(AEPSLedger aepsLedger) {
		 

		  logger.debug("*************************** calling getAepsLedger ***********************************");
		  Gson gson = new Gson();
		  String jsonText=gson.toJson(aepsLedger);
		  
		  WebResource webResource=ServiceManager.getWebResource("NewAepsTransactionManager/makeAepsTxnStatus");
		  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",aepsLedger.getIpimei()).header("AGENT",aepsLedger.getUserAgnet()).post(ClientResponse.class,jsonText);

		  if (response.getStatus() != 200){
		      throw new RuntimeException("Failed : HTTP error code : "
		    + response.getStatus());
		  }
		  String output = response.getEntity(String.class);  
		  try{
		   AepsRedirection resp = gson.fromJson(output, AepsRedirection.class);
		  return resp;
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return null;
	}

	
	@Override
	public TokenResponse generateCmsToken(String agentAuthId, String agentAuthPassword, String agentid, String apiId) {
			  logger.debug("*************************** calling generateCmsToken generateToken ***********************************");
			  Gson gson = new Gson();
			  JSONObject jsonText=new JSONObject();
			  jsonText.put("agentAuthId",AEPSUtil.getMD5EncryptedValue(agentAuthId));
			  jsonText.put("agentAuthPassword",AEPSUtil.getMD5EncryptedValue(agentAuthPassword));
			  jsonText.put("retailerId",agentid);
			  jsonText.put("apiId",apiId); 
			  logger.debug("*************************** calling  generateToken  request***********************************"+jsonText);

			  WebResource webResource=ServiceManager.getCmsTokenUrl();
			  ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText.toString());

			  if (response.getStatus() != 200){
			      throw new RuntimeException("Failed : HTTP error code : "
			    + response.getStatus());
			  }
			  String output = response.getEntity(String.class);  
			  logger.debug("*************************** calling aadharshila generateToken response ***********************************"+output);

			  try{
			  TokenResponse resp = gson.fromJson(output, TokenResponse.class);
			  return resp;
			  }catch(Exception e){
				  e.printStackTrace();
			  }
			  return null;
		 }



	@Override
	public CmsEnquiryResponse searchCmsTxn(CmsEnquiryResponse cms) {
        	 
		    Gson gson = new Gson();
		    String jsonText=gson.toJson(cms);
			
			WebResource webResource=ServiceManager.getWebResource("TransactionManager/searchCmsTxn");
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

			if (response.getStatus() != 200) {
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
			}
			String output = response.getEntity(String.class);
			CmsEnquiryResponse bean = gson.fromJson(output, CmsEnquiryResponse.class);
			 
		return bean;
	}
	
	
	
}

class OTG1 extends ArrayList<EscrowBean>{
	
}
class OTG2 extends ArrayList<CashDepositMast>{
	
}
class OTG3 extends ArrayList<RefundMastBean>{
	
}
class OTG4 extends HashMap<String,Object>{
	 
}

class OTG5 extends ArrayList<AgentConsolidatedReportBean>{
	 
}