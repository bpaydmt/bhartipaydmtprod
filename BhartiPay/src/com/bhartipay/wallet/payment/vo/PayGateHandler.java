
package com.bhartipay.wallet.payment.vo;

import org.apache.log4j.Logger;

import com.bhartipay.wallet.framework.security.EncryptionByEnc256;

@SuppressWarnings("unused")
public class PayGateHandler {
	protected static final Logger logger = Logger
			.getLogger(PayGateHandler.class);


	public PayGateHandler() {

	}


	

// 200904281000001|DOM|IND|INR|10|uniqueOrderNo-002|others|www.direcpay.com/sucess_page.jsp|www.direcpay.com/fail_page.jsp|TOML
public String generateTxnDetails4NB(TDSecurePayGateObject secureObject) {

		/*
		private String ag_id;  
		private String me_id;
		private String	order_no;
		private double amount;
		private String country="IND";
		private String currency="INR"; 
		private String txn_type="SALE";
		private String success_url; 
		private String failure_url;
		private String channel="WEB";
		ag_id|me_id|order_no|amount|country|currency|txn_type|success_url|failure_url|channel		
		*/
		String transString=secureObject.getAg_id()+"|"+secureObject.getMe_id()+"|"+secureObject.getOrder_no()+"|"+secureObject.getAmount()+"|"+secureObject.getCountry()+"|"+secureObject.getCurrency()+"|"+secureObject.getTxn_type()+"|"+secureObject.getSuccess_url()+"|"+secureObject.getFailure_url()+"|"+secureObject.getChannel();
		return EncString(transString, secureObject.getEncKey());
	}
public String generatePgDetails4NB(TDSecurePayGateObject secureObject) {
		
		/*private int pg_id; 
		private String paymode;// String NB = Net BankingCC = Credit CardDC = Debit CardPP = Prepaid CardWA = WalletCE = Credit Card EMI
		private  int scheme;
		private int emi_months;*/
//		String pgDetails=secureObject.getPg_id()+"|"+secureObject.getPaymode()+"|7||";	
		String pgDetails="0||7||";	
		return EncString(pgDetails, secureObject.getEncKey());
	}
public String generateCardDetails4NB(TDSecurePayGateObject secureObject) {
	/*private String card_details;//(CardDetails)
	private String card_no;
	private String exp_month;
	private String exp_year;
	private int cvv2;
	private String card_name;*/
	String cardDetails="||||";
	
	return EncString(cardDetails, secureObject.getEncKey());
	}
public String generateCustDetails4NB(TDSecurePayGateObject secureObject) {
	
	/*private String cust_details;//(CustomerDetails)
	private String cust_name;
	private String email_id;
	private String mobile_no;
	private String unique_id;            
	private String is_logged_in;*/
	CustomerDtls custDtls=secureObject.getCustdtls();
	
	String custDetails=custDtls.getCustName()+"|"+custDtls.getCustEmailId()+"|"+custDtls.getCustMobileNo()+"|"+custDtls.getCustEmailId()+"|N";
	return EncString(custDetails, secureObject.getEncKey());
}
public String generateBillDetails4NB(TDSecurePayGateObject secureObject) {
	
	/*private String bill_details;//(BillingDetails)
	private String bill_address; 
	private String bill_city;
	private String bill_state;
	private String bill_country;
	private String bill_zip;
	private int multicard;*/
	//ShippingDtls billDtls=secureObject.getShipdtls();
	CustomerDtls custDtls=secureObject.getCustdtls();
	String billDetails=custDtls.getCustAddress()+"|"+custDtls.getCustCity()+"|"+custDtls.getCustState()+"|"+custDtls.getCustCountry()+"|"+custDtls.getCustPinCode();
	return EncString(billDetails, secureObject.getEncKey());
}
public String generateShipDetails4NB(TDSecurePayGateObject secureObject) {
	
	/*private String bill_details;//(BillingDetails)
	private String bill_address; 
	private String bill_city;
	private String bill_state;
	private String bill_country;
	private String bill_zip;
	private int multicard;*/
	//ShippingDtls billDtls=secureObject.getShipdtls();
	String shipDetails="||||||";
	return EncString(shipDetails, secureObject.getEncKey());
}
public String generateItemDetails4NB(TDSecurePayGateObject secureObject) {
	
	/*private String bill_details;//(BillingDetails)
	private String bill_address; 
	private String bill_city;
	private String bill_state;
	private String bill_country;
	private String bill_zip;
	private int multicard;*/
	//ShippingDtls billDtls=secureObject.getShipdtls();
	String itemDetails="||";
	return EncString(itemDetails, secureObject.getEncKey());
}
public String generateOtherDetails4NB(TDSecurePayGateObject secureObject) {
	
	/*private String bill_details;//(BillingDetails)
	private String bill_address; 
	private String bill_city;
	private String bill_state;
	private String bill_country;
	private String bill_zip;
	private int multicard;*/
	//ShippingDtls billDtls=secureObject.getShipdtls();
	String otherDetails="||||";
	return EncString(otherDetails, secureObject.getEncKey());
}
	
	public String EncString(String plainString,String secureKey){
		String encString=new String();
		try{
    	encString=EncryptionByEnc256.encrypt(plainString,secureKey);
    	}catch(Exception e){
			e.printStackTrace();
		}
    	return encString;
    }
}
