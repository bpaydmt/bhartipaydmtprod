package com.bhartipay.wallet.payment.vo;
import java.io.Serializable;

import com.bhartipay.wallet.framework.persistence.vo.ValueObject;

public class CustomerDtls extends ValueObject implements Serializable{
	
	private String custName;
	private String custAddress;
	private String custCity;
	private String custState;
	private String custPinCode;
	private String custCountry;
	private String custPhoneNo1;
	private String custPhoneNo2;
	private String custPhoneNo3;
	private String custMobileNo;
	private String custEmailId;
	private String otherNotes;
	
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustAddress() {
		return custAddress;
	}
	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}
	public String getCustCity() {
		return custCity;
	}
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	public String getCustState() {
		return custState;
	}
	public void setCustState(String custState) {
		this.custState = custState;
	}
	public String getCustPinCode() {
		return custPinCode;
	}
	public void setCustPinCode(String custPinCode) {
		this.custPinCode = custPinCode;
	}
	public String getCustCountry() {
		return custCountry;
	}
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	public String getCustPhoneNo1() {
		return custPhoneNo1;
	}
	public void setCustPhoneNo1(String custPhoneNo1) {
		this.custPhoneNo1 = custPhoneNo1;
	}
	public String getCustPhoneNo2() {
		return custPhoneNo2;
	}
	public void setCustPhoneNo2(String custPhoneNo2) {
		this.custPhoneNo2 = custPhoneNo2;
	}
	public String getCustPhoneNo3() {
		return custPhoneNo3;
	}
	public void setCustPhoneNo3(String custPhoneNo3) {
		this.custPhoneNo3 = custPhoneNo3;
	}
	public String getCustMobileNo() {
		return custMobileNo;
	}
	public void setCustMobileNo(String custMobileNo) {
		this.custMobileNo = custMobileNo;
	}
	public String getCustEmailId() {
		return custEmailId;
	}
	public void setCustEmailId(String custEmailId) {
		this.custEmailId = custEmailId;
	}
	public String getOtherNotes() {
		return otherNotes;
	}
	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}
	
	
	
	}
