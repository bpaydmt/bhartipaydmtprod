package com.bhartipay.wallet.user.action.bk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.bhartipay.biller.bean.BillDetails;
import com.bhartipay.biller.bean.WalletBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.service.utility.PasswordValidator;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.framework.utility.EncryptionDecryption;
import com.bhartipay.wallet.framework.utility.ReadPropertyFile;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.bhartipay.wallet.merchant.service.impl.MerchantServiceImpl;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.bean.UserWishListBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.recharge.service.impl.RechargeServiceImpl;
import com.bhartipay.wallet.report.bean.AgentBalDetailsBean;
import com.bhartipay.wallet.report.bean.AgentCurrentSummary;
import com.bhartipay.wallet.report.bean.AgentDetailsBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.RefundAgent;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.ResponseBean;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.report.service.ReportService;
import com.bhartipay.wallet.report.service.impl.ReportServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.dao.PCUser;
import com.bhartipay.wallet.user.persistence.vo.AgentDetailsView;
import com.bhartipay.wallet.user.persistence.vo.BankDetailsBean;
import com.bhartipay.wallet.user.persistence.vo.BankMastBean;
import com.bhartipay.wallet.user.persistence.vo.Coupons;
import com.bhartipay.wallet.user.persistence.vo.CouponsBean;
import com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean;
import com.bhartipay.wallet.user.persistence.vo.DMTReportInOut;
import com.bhartipay.wallet.user.persistence.vo.DeclinedListBean;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.DmtUserDetails;
import com.bhartipay.wallet.user.persistence.vo.FundTransactionSummaryBean;
import com.bhartipay.wallet.user.persistence.vo.LoginResponse;
import com.bhartipay.wallet.user.persistence.vo.MailConfigMast;
import com.bhartipay.wallet.user.persistence.vo.MerchantOffersMast;
import com.bhartipay.wallet.user.persistence.vo.MudraBeneficiaryMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails;
import com.bhartipay.wallet.user.persistence.vo.RevenueReportBean;
import com.bhartipay.wallet.user.persistence.vo.SMSConfigMast;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.SenderProfileBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardOutputBean;
import com.bhartipay.wallet.user.persistence.vo.UploadKyc;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.UserBlockedBean;
import com.bhartipay.wallet.user.persistence.vo.UserMenuMapping;
import com.bhartipay.wallet.user.persistence.vo.UserRoleMapping;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletKYCBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.bk.UserService;
import com.bhartipay.wallet.user.service.impl.bk.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;

import appnit.com.crypto.CheckSumHelper;

public class ManageUser extends BaseAction implements ServletResponseAware {

	private static final long serialVersionUID = -203193667575448996L;
	Properties prop = null;

	public ManageUser() {
		/* added by neeraj call error code using properties file. */
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		/* end by neeraj */

	}

	HttpServletResponse response = null;
	CommissionService commService = new CommissionServiceImpl();
	public static Map<String, String> loggedinUserMap = new HashMap<String, String>();
	private Map<String, String> countryCode = new TreeMap<String, String>();
	UserSummary user = new UserSummary();
	private UserService service = new UserServiceImpl();
	WalletMastBean walletBean = new WalletMastBean();
	Map<String, String> kycOptions = new LinkedHashMap<String, String>();
	Map<String, String> kycIdOptions = new LinkedHashMap<String, String>();
	UploadKyc upload = new UploadKyc();
	MerchantService mService = new MerchantServiceImpl();
	TxnInputBean inputBean = new TxnInputBean();
	Map<String, String> aggrigatorList = new LinkedHashMap<String, String>();
	Map<String, String> distributorList = new LinkedHashMap<String, String>();
	Map<String, String> agentTypeList = new LinkedHashMap<String, String>();
	Map<String, String> agentList = new LinkedHashMap<String, String>();
	public Map<String, String> subagentList = new LinkedHashMap<String, String>();
	public Map<String, String> statusList = new LinkedHashMap<String, String>();
	ReportBean reportBean = new ReportBean();
	UserWishListBean wishBean = new UserWishListBean();
	WalletConfiguration config = new WalletConfiguration();
	MailConfigMast mconfig = new MailConfigMast();
	SMSConfigMast sconfig = new SMSConfigMast();
	CommPlanMaster commBean = new CommPlanMaster();
	AskMoneyBean amb = new AskMoneyBean();
	Map<String, String> plans = new HashMap<String, String>();
	Map<String, String> txnType = new HashMap<String, String>();
	private String countrycurrency;// not update
	public Map<String, String> smssendList = new LinkedHashMap<String, String>();// 1
																					// active
																					// 0
																					// inactive
	public Map<String, String> kycvalidList = new LinkedHashMap<String, String>();// 1
																					// active
																					// 0
																					// inactive
	public Map<String, String> emailvalidList = new LinkedHashMap<String, String>();// 1
																					// active
																					// 0
																					// inactive
	public Map<String, String> otpsendtomailList = new LinkedHashMap<String, String>();// 1
																						// active
																						// 0
																						// inactive
	public Map<String, String> reqAgentApprovalList = new LinkedHashMap<String, String>();// 1
																							// active
																							// 0
																							// inactive
	public Map<String, String> usertypeList = new LinkedHashMap<String, String>();
	public Map<String, String> idTypeList = new LinkedHashMap<String, String>();
	public Map<String, String> paymentModeList = new LinkedHashMap<String, String>();

	public Map<String, String> userStatusList = new LinkedHashMap<String, String>();
	public Map<String, String> txnTypeList = new LinkedHashMap<String, String>();
	public Map<String, String> subAggList = new LinkedHashMap<String, String>();
	public Map<String, String> aggMenuList = new LinkedHashMap<String, String>();

	public Map<String, String> aggRoleList = new LinkedHashMap<String, String>();

	public Map<String, String> ptxnList = new LinkedHashMap<String, String>();
	public Map<String, String> pcountryCode = new LinkedHashMap<String, String>();
	public Map<String, String> protocolList = new LinkedHashMap<String, String>();
	ReportService rService = new ReportServiceImpl();
	UserMenuMapping menuMapping = new UserMenuMapping();
	UserRoleMapping roleMapping = new UserRoleMapping();

	DmtDetailsMastBean dmtMast = new DmtDetailsMastBean();
	SmartCardBean smartCardBean = new SmartCardBean();
	PCUser pcuser = new PCUser();
	RechargeTxnBean rechargeBean = new RechargeTxnBean();
	HashMap<String, String> prePaid = new HashMap<String, String>();
	HashMap<String, String> postPaid = new HashMap<String, String>();
	HashMap<String, String> dth = new HashMap<String, String>();
	HashMap<String, String> landLine = new HashMap<String, String>();
	HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
	HashMap<String, String> postPaidDataCard = new HashMap<String, String>();
	HashMap<String, String> circle = new HashMap<String, String>();
	MudraSenderMastBean mastBean = new MudraSenderMastBean();
	CouponsBean couponsBean = new CouponsBean();
	MudraBeneficiaryMastBean mBean = new MudraBeneficiaryMastBean();
	BankDetailsBean bankDetail = new BankDetailsBean();

	UserBlockedBean userBlockedBean = new UserBlockedBean();

	MudraSenderPanDetails mudraSenderPanDetails = new MudraSenderPanDetails();

	public DmtDetailsMastBean getDmtMast() {
		return dmtMast;
	}

	public void setDmtMast(DmtDetailsMastBean dmtMast) {
		this.dmtMast = dmtMast;
	}

	
	
	public Map<String, String> getStatusList() {
		statusList.put("ACCEPTED","ACCEPTED");
		statusList.put("PENDING","PENDING");
		statusList.put("FAILED","FAILED");
		statusList.put("SUCCESS","SUCCESS");
		statusList.put("REFUNDED","REFUNDED");
		return statusList;
	}

	public void setStatusList(Map<String, String> statusList) {
		this.statusList = statusList;
	}

	public Map<String, String> getPaymentModeList() {
		paymentModeList.put("dd", "DD");
		paymentModeList.put("cash", "CASH");
		paymentModeList.put("netbanking", "NET BANKING");
		paymentModeList.put("agentwallet", "AGENT WALLET");
		paymentModeList.put("distributorwallet", "DISTRIBUTOR WALLET");
		return paymentModeList;
	}

	public void setPaymentModeList(Map<String, String> paymentModeList) {
		paymentModeList.put("dd", "DD");
		paymentModeList.put("cash", "CASH");
		paymentModeList.put("netbanking", "NET BANKING");
		paymentModeList.put("agentwallet", "AGENT WALLET");
		paymentModeList.put("distributorwallet", "DISTRIBUTOR WALLET");
		this.paymentModeList = paymentModeList;
	}

	public Map<String, String> getIdTypeList() {
		idTypeList.put("voterid", "Voter ID");
		idTypeList.put("aadhar", "Aadhar");
		idTypeList.put("passport", "Passport");
		idTypeList.put("others", "Others");
		return idTypeList;
	}

	public void setIdTypeList(Map<String, String> idTypeList) {
		idTypeList.put("voterid", "Voter ID");
		idTypeList.put("aadhar", "Aadhar");
		idTypeList.put("passport", "Passport");
		idTypeList.put("others", "Others");
		this.idTypeList = idTypeList;
	}

	public Map<String, String> getAgentTypeList() {
		agentTypeList.put("individual", "Individual");
		agentTypeList.put("firm", "Firm");
		return agentTypeList;
	}

	public void setAgentTypeList(Map<String, String> agentTypeList) {
		agentTypeList.put("individual", "Individual");
		agentTypeList.put("firm", "Firm");
		this.agentTypeList = agentTypeList;
	}

	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}

	public MudraSenderPanDetails getMudraSenderPanDetails() {
		return mudraSenderPanDetails;
	}

	public void setMudraSenderPanDetails(MudraSenderPanDetails mudraSenderPanDetails) {
		this.mudraSenderPanDetails = mudraSenderPanDetails;
	}

	public MudraSenderMastBean getMastBean() {
		return mastBean;
	}

	public void setMastBean(MudraSenderMastBean mastBean) {
		this.mastBean = mastBean;
	}

	public AskMoneyBean getAmb() {
		return amb;
	}

	public void setAmb(AskMoneyBean amb) {
		this.amb = amb;
	}

	public Map<String, String> getProtocolList() {
		protocolList.clear();
		protocolList.put("http", "http");
		protocolList.put("https", "https");
		return protocolList;
	}

	public void setProtocolList(Map<String, String> protocolList) {
		this.protocolList = protocolList;
	}

	public Map<String, String> getPcountryCode() {
		return pcountryCode;
	}

	public void setPcountryCode(Map<String, String> pcountryCode) {
		this.pcountryCode = pcountryCode;
	}

	public Map<String, String> getKycIdOptions() {
		return kycIdOptions;
	}

	public void setKycIdOptions(Map<String, String> kycIdOptions) {
		this.kycIdOptions = kycIdOptions;
	}

	public CouponsBean getCouponsBean() {
		return couponsBean;
	}

	public void setCouponsBean(CouponsBean couponsBean) {
		this.couponsBean = couponsBean;
	}

	public RechargeTxnBean getRechargeBean() {
		return rechargeBean;
	}

	public void setRechargeBean(RechargeTxnBean rechargeBean) {
		this.rechargeBean = rechargeBean;
	}

	public HashMap<String, String> getDth() {
		return dth;
	}

	public void setDth(HashMap<String, String> dth) {
		this.dth = dth;
	}

	public HashMap<String, String> getPrePaid() {
		return prePaid;
	}

	public void setPrePaid(HashMap<String, String> prePaid) {
		this.prePaid = prePaid;
	}

	public HashMap<String, String> getPostPaid() {
		return postPaid;
	}

	public void setPostPaid(HashMap<String, String> postPaid) {
		this.postPaid = postPaid;
	}

	public HashMap<String, String> getLandLine() {
		return landLine;
	}

	public void setLandLine(HashMap<String, String> landLine) {
		this.landLine = landLine;
	}

	public HashMap<String, String> getPrePaidDataCard() {
		return prePaidDataCard;
	}

	public void setPrePaidDataCard(HashMap<String, String> prePaidDataCard) {
		this.prePaidDataCard = prePaidDataCard;
	}

	public HashMap<String, String> getPostPaidDataCard() {
		return postPaidDataCard;
	}

	public void setPostPaidDataCard(HashMap<String, String> postPaidDataCard) {
		this.postPaidDataCard = postPaidDataCard;
	}

	public HashMap<String, String> getCircle() {
		return circle;
	}

	public void setCircle(HashMap<String, String> circle) {
		this.circle = circle;
	}

	public SmartCardBean getSmartCardBean() {
		return smartCardBean;
	}

	public void setSmartCardBean(SmartCardBean smartCardBean) {
		this.smartCardBean = smartCardBean;
	}

	public PCUser getPcuser() {
		return pcuser;
	}

	public void setPcuser(PCUser pcuser) {
		this.pcuser = pcuser;
	}

	public UserMenuMapping getMenuMapping() {
		return menuMapping;
	}

	public void setMenuMapping(UserMenuMapping menuMapping) {
		this.menuMapping = menuMapping;
	}

	public UserRoleMapping getRoleMapping() {
		return roleMapping;
	}

	public void setRoleMapping(UserRoleMapping roleMapping) {
		this.roleMapping = roleMapping;
	}

	public Map<String, String> getSubAggList() {
		return subAggList;
	}

	public void setSubAggList(Map<String, String> subAggList) {
		this.subAggList = subAggList;
	}

	public Map<String, String> getAggMenuList() {
		return aggMenuList;
	}

	public void setAggMenuList(Map<String, String> aggMenuList) {
		this.aggMenuList = aggMenuList;
	}

	public Map<String, String> getAggRoleList() {
		return aggRoleList;
	}

	public void setAggRoleList(Map<String, String> aggRoleList) {
		this.aggRoleList = aggRoleList;
	}

	public Map<String, String> getTxnType() {
		return txnType;
	}

	public void setTxnType(Map<String, String> txnType) {
		this.txnType = txnType;
	}

	public Map<String, String> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, String> plans) {
		this.plans = plans;
	}

	public Map<String, String> getTxnTypeList() {
		return txnTypeList;
	}

	public void setTxnTypeList(Map<String, String> txnTypeList) {
		this.txnTypeList = txnTypeList;
	}

	public Map<String, String> getUserStatusList() {
		return userStatusList;
	}

	public void setUserStatusList(Map<String, String> userStatusList) {
		this.userStatusList = userStatusList;
	}

	public SMSConfigMast getSconfig() {
		return sconfig;
	}

	public void setSconfig(SMSConfigMast sconfig) {
		this.sconfig = sconfig;
	}

	public MailConfigMast getMconfig() {
		return mconfig;
	}

	public void setMconfig(MailConfigMast mconfig) {
		this.mconfig = mconfig;
	}

	public Map<String, String> getSubagentList() {
		return subagentList;
	}

	public void setSubagentList(Map<String, String> subagentList) {
		this.subagentList = subagentList;
	}

	public Map<String, String> getUsertypeList() {
		return usertypeList;
	}

	public void setUsertypeList(Map<String, String> usertypeList) {
		this.usertypeList = usertypeList;
	}

	public Map<String, String> getSmssendList() {

		return smssendList;
	}

	public UserBlockedBean getUserBlockedBean() {
		return userBlockedBean;
	}

	public void setUserBlockedBean(UserBlockedBean userBlockedBean) {
		this.userBlockedBean = userBlockedBean;
	}

	public void setSmssendList(Map<String, String> smssendList) {
		smssendList.put("1", "YES");
		smssendList.put("0", "NO");
		this.smssendList = smssendList;
	}

	public Map<String, String> getKycvalidList() {
		return kycvalidList;
	}

	public void setKycvalidList(Map<String, String> kycvalidList) {
		kycvalidList.put("1", "YES");
		kycvalidList.put("0", "NO");
		this.kycvalidList = kycvalidList;
	}

	public Map<String, String> getEmailvalidList() {
		return emailvalidList;
	}

	public void setEmailvalidList(Map<String, String> emailvalidList) {
		emailvalidList.put("1", "YES");
		emailvalidList.put("0", "NO");
		this.emailvalidList = emailvalidList;
	}

	public Map<String, String> getPtxnList() {
		return ptxnList;
	}

	public void setPtxnList(Map<String, String> ptxnList) {
		ptxnList.put("SALE", "SALE");
		ptxnList.put("AUTH", "AUTH");
		this.ptxnList = ptxnList;
	}

	public Map<String, String> getOtpsendtomailList() {
		return otpsendtomailList;
	}

	public void setOtpsendtomailList(Map<String, String> otpsendtomailList) {
		otpsendtomailList.put("1", "YES");
		otpsendtomailList.put("0", "NO");
		this.otpsendtomailList = otpsendtomailList;
	}

	public Map<String, String> getReqAgentApprovalList() {
		return reqAgentApprovalList;
	}

	public void setReqAgentApprovalList(Map<String, String> reqAgentApprovalList) {
		reqAgentApprovalList.put("1", "YES");
		reqAgentApprovalList.put("0", "NO");
		this.reqAgentApprovalList = reqAgentApprovalList;
	}

	public WalletConfiguration getConfig() {
		return config;
	}

	public void setConfig(WalletConfiguration config) {
		this.config = config;
	}

	public UserWishListBean getWishBean() {
		return wishBean;
	}

	public void setWishBean(UserWishListBean wishBean) {
		this.wishBean = wishBean;
	}

	public Map<String, String> getAgentList() {
		return agentList;
	}

	public void setAgentList(Map<String, String> agentList) {
		this.agentList = agentList;
	}

	public Map<String, String> getDistributorList() {
		return distributorList;
	}

	public void setDistributorList(Map<String, String> distributorList) {
		this.distributorList = distributorList;
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public Map<String, String> getAggrigatorList() {
		return aggrigatorList;
	}

	public void setAggrigatorList(Map<String, String> aggrigatorList) {
		this.aggrigatorList = aggrigatorList;
	}

	public UploadKyc getUpload() {
		return upload;
	}

	public void setUpload(UploadKyc upload) {
		this.upload = upload;
	}

	public Map<String, String> getKycOptions() {
		return kycOptions;
	}

	public void setKycOptions(Map<String, String> kycOptions) {
		this.kycOptions = kycOptions;
	}

	public WalletMastBean getWalletBean() {
		return walletBean;
	}

	public void setWalletBean(WalletMastBean walletBean) {
		this.walletBean = walletBean;
	}

	public UserSummary getUser() {
		return user;
	}

	public void setUser(UserSummary user) {
		this.user = user;
	}

	public Map<String, String> getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(Map<String, String> countryCode) {
		this.countryCode = countryCode;
	}

	public String logOff() {

		String result = "success";
		int userType = 0;
		try {
			User user = (User) session.get("User");
			if (user == null) {
				return "error";
			}
			if (user.getUsertype() != 1) {
				result = "adminlogin";
			}
			if (user.getUsertype() == 1) {
				userType = 1;
			}

			if (session instanceof org.apache.struts2.dispatcher.SessionMap) {

				session.remove("User");
				((org.apache.struts2.dispatcher.SessionMap) session).clear();
			}

			if (session instanceof org.apache.struts2.dispatcher.SessionMap) {
				((org.apache.struts2.dispatcher.SessionMap) session).invalidate();
			}
		} catch (Exception e) {
			result = "error";
		}
		if (userType == 1) {
			request.getSession().putValue("loginStatus", "logoff");
		}
		return result;

	}

	public String logOut() {

		String result = "success";
		User user = (User) session.get("User");
		if (user == null) {
			return "error";
		}
		if (user.getUsertype() != 1) {
			result = "adminlogin";
		}

		if (session instanceof org.apache.struts2.dispatcher.SessionMap) {

			session.remove("User");
			((org.apache.struts2.dispatcher.SessionMap) session).clear();
		}

		if (session instanceof org.apache.struts2.dispatcher.SessionMap) {
			((org.apache.struts2.dispatcher.SessionMap) session).invalidate();
		}

		return result;

	}

	public String login() {

		System.out.println("User login requested.");
		session.put("loginStatus", "");
		/***********************************************
		 * start fetching recharge operators and circle
		 **********************************************************/
		RechargeService rs = new RechargeServiceImpl();
		logger.debug(
				"******************calling getrechargeoperator service from manageUser action and return success***********************");
		Map<String, HashMap<String, String>> mapResult2 = rs.getRechargeOperator();
		request.setAttribute("mapResult", mapResult2);

		if (mapResult2 != null) {
			try {
				setPrePaid(mapResult2.get("PREPAID"));
				setPostPaid(mapResult2.get("POSTPAID"));
				setDth(mapResult2.get("DTH"));
				setLandLine(mapResult2.get("LANDLINE"));
				setPrePaidDataCard(mapResult2.get("PREPAIDDATACARD"));
				setPostPaidDataCard(mapResult2.get("POSTPAIDDATACARD"));
				setCircle(mapResult2.get("CIRCLE"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/***********************************************
		 * end fetching recharge operators and circle
		 **********************************************************/
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String aggid = (String) session.get("aggId");
		user.setAggreatorid(aggid);
		logger.debug("******************calling login() service and return success***********************");
		LoginResponse loginResponse = service.login(user, ipAddress, userAgent);
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1010")) {
			// otp send to user
			if (session.get("custType") != null && ((String) session.get("custType")).equals("1")) {
				session.put("loginStatus", "first");
				request.getSession().putValue("custId", user.getUserId());
				request.getSession().putValue("custPwd", user.getPassword());
				return "custOtpVal";
			}
			return "otpvalidation";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1011")) {

			/*
			 * if(!checkLoggedinUser(this.user)){ addActionError(
			 * "You have already logged into Bhartipay from another machine. Do you want to reset the session?  <a href='#' onClick='removeSession(\""
			 * +this.user.getUserId()+ "\")' title='Remove Duplication Session'>Yes</a>");
			 * return "fail"; }
			 */

			// user successfully login
			logger.debug("******************calling ProfilebyloginId(user) service ***********************");
			Map<String, String> resultMap = service.ProfilebyloginId(user);
			if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4) {
				config.setAggreatorid(resultMap.get("id"));
			} else {
				config.setAggreatorid(resultMap.get("aggreatorid"));
			}

			/*************************
			 * for menu options
			 ***************************/
			logger.debug("******************calling getPlanDtl() service ***********************");
			commBean.setAggreatorid(config.getAggreatorid());
			Map<String, HashMap<String, String>> res = commService.getPlanDtl(commBean);
			if (resultMap != null) {
				setPlans(res.get("PLAN"));
				setTxnType(res.get("TXNTYPE"));
			}

			session.put("menuMap", res.get("TXNTYPE"));
			/*************************
			 * end menu options
			 ***************************/

			logger.debug("******************calling getWalletConfig() service***********************");
			Map<String, String> config1 = service.getWalletConfig(config);
			if (config1 == null || config1.size() == 0) {
				addActionError("An error has occurred please try again.");
				session.put("loginerr", "An error has occurred please try again.");
				session.put("loginStatus", "fail");

				return "fail";
			}
			ReportBean rBean = new ReportBean();
			rBean.setUserId(resultMap.get("id"));
			if (rBean.getUserId() != null && Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 1
					&& Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 99) {
				session.put("commission", new CommissionServiceImpl().getCommission(rBean));
			}
			User user = new User();
			logger.debug("******************calling getProfilePic() service***********************");
			String userImg = service.getProfilePic(resultMap.get("id"));

			user.setUserImg(userImg);
			user.setEmailid(resultMap.get("emailid"));
			user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
			user.setCashBackfinalBalance(Double.parseDouble(String.valueOf(resultMap.get("cashBackFinalBalance"))));
			user.setId(resultMap.get("id"));
			user.setMobileno(resultMap.get("mobileno"));
			user.setName(resultMap.get("name"));
			user.setWalletid(resultMap.get("walletid"));
			user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
			user.setAgentid(resultMap.get("agentid"));
			user.setSubAgentId(resultMap.get("subAgentId"));
			user.setDistributerid(resultMap.get("distributerid"));
			user.setSuperdistributerid(
					resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
			user.setAggreatorid(resultMap.get("aggreatorid"));
			user.setToken(loginResponse.getToken());
			user.setTokenSecurityKey(loginResponse.getTokenSecurityKey());
			user.setKycStatus(resultMap.get("kycStatus"));
			user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
			user.setWhiteLabel(
					Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));
			if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 3
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 7) {
				menuMapping.setUserId(user.getId());
				menuMapping = service.getUserMenuMapping(menuMapping);
				if (menuMapping != null && menuMapping.getMenuType() != null) {
					List<String> aggMenus = Arrays.asList(menuMapping.getMenuType());
					session.put("aggMenus", aggMenus);
				}

				roleMapping.setUserId(user.getId());
				roleMapping = service.getUserRoleMapping(roleMapping);
				if (roleMapping != null && roleMapping.getRoleType() != null) {
					List<String> aggRoles = Arrays.asList(roleMapping.getRoleType());
					session.put("aggRoles", aggRoles);
				}

				if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6) {
					user.setSubAggregatorId(user.getId());
					user.setId(user.getAggreatorid());
				}

			}
			user.setCountry(config1.get("country"));
			user.setCountrycurrency(config1.get("countrycurrency"));
			user.setCountryid(Double.parseDouble(String.valueOf(config1.get("countryid"))));
			user.setEmailvalid(Double.parseDouble(String.valueOf(config1.get("emailvalid"))));
			user.setKycvalid(Double.parseDouble(String.valueOf(config1.get("kycvalid"))));
			user.setOtpsendtomail(Double.parseDouble(String.valueOf(config1.get("otpsendtomail"))));
			user.setSmssend(Double.parseDouble(String.valueOf(config1.get("smssend"))));
			user.setStatus(Double.parseDouble(String.valueOf(config1.get("status"))));
			user.setLogo(config1.get("logopic"));
			user.setCustomerCare(config1.get("customerCare") != null ? config1.get("customerCare") : "0120-4000004");
			user.setSupportEmailId(config1.get("supportEmailId") != null ? config1.get("supportEmailId") : "");
			user.setBanner(config1.get("bannerpic"));

			session.put("logo", config1.get("logopic"));
			session.put("banner", config1.get("bannerpic"));
			session.put("User", user);
			if (user.getUsertype() == 1) {
				session.put("loginStatus", "success");
				TxnInputBean WToWMoneyBean = (TxnInputBean) session.get("WToWMoneyBean");
				if (WToWMoneyBean != null && WToWMoneyBean.getTrxAmount() != 0) {
					return "walletTowallet";
				}
				B2CMoneyTxnMast wToB = (B2CMoneyTxnMast) session.get("b2cMoneyTxnMast");
				if (wToB != null && wToB.getAmount() != 0) {
					return "walletToBank";
				}
				RechargeTxnBean rtb = (RechargeTxnBean) session.get("rechargeDetails");
				if (rtb != null && rtb.getRechargeNumber() != null && !rtb.getRechargeNumber().isEmpty()) {
					generateCsrfToken();
					return "recharge";
				}
				BillDetails billdetail = (BillDetails) session.get("BillDetails");
				if (billdetail != null && billdetail.getBillerId() != 0)
					return "customer";
				TxnInputBean inputBean = (TxnInputBean) session.get("inputBean");
				if (inputBean != null && inputBean.getTrxAmount() != 0) {
					return "addMoneyB2C";
				}
				return "input";
			}
			System.out.println("during login " + resultMap);

			if (user.getUsertype() == 99) {

				List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
				request.setAttribute("txnList", list);
			}
			return "success";

		}

		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7000")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7000"));
			session.put("	", prop.getProperty("7000"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7010")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7010"));
			session.put("loginerr", prop.getProperty("7010"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7011")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7011"));
			session.put("loginerr", prop.getProperty("7011"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7012")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7012"));
			session.put("loginerr", prop.getProperty("7012"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7039")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7039"));
			session.put("loginerr", prop.getProperty("7039"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7050")) {
			request.setAttribute("login", "loginpopup");
			addActionError(prop.getProperty("7050"));
			session.put("loginerr", prop.getProperty("7050"));
			session.put("loginStatus", "fail");

			return "fail";
		}
		request.setAttribute("login", "loginpopup");
		System.out.println("result is " + loginResponse.getStatusCode());
		return "input";

	}

	public String isCustomer() {
		JSONObject json = new JSONObject();
		try {
			String userAgent = request.getHeader("User-Agent");

			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}

			String aggid = (String) session.get("aggId");
			user.setAggreatorid(aggid);
			logger.debug("******************calling login() service and return success***********************");
			LoginResponse loginResponse = service.login(user, ipAddress, userAgent);
			if (loginResponse != null && loginResponse.getStatusCode() != null
					&& !loginResponse.getStatusCode().isEmpty()
					&& loginResponse.getStatusCode().equalsIgnoreCase("1011")) {

				Map<String, String> resultMap = service.ProfilebyloginId(user);
				if (resultMap == null) {
					json.put("isCustomer", "FALSE");
				}
				if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 1) {
					json.put("isCustomer", "TRUE");
				}
			} else {
				json.put("isCustomer", "FALSE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			json.put("isCustomer", "FALSE");
		}
		try {
			response.setContentType("application/json");
			response.getWriter().println(json);
		} catch (Exception e) {

		}
		return null;
	}

	public String userLogin() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		System.out.println("User login requested.");

		String aggid = (String) session.get("aggId");
		user.setAggreatorid(aggid);
		UserSummary userSummary = user;
		LoginResponse loginResponse = service.login(user, ipAddress, userAgent);
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1010")) {
			// otp send to user
			return "otpvalidation";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1011")) {
			// user successfully login

			String userId = "";
			String userData = "";
			Cookie cookie[] = request.getCookies();
			for (Cookie cookie2 : cookie) {

				if (cookie2.getName().equalsIgnoreCase("userdata")) {
					userData = cookie2.getName();
					String data = cookie2.getValue();
					try {
						userId = EncryptionDecryption.getdecrypted(data,
								"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

			Map<String, String> resultMap = service.ProfilebyloginId(user);
			if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4) {
				config.setAggreatorid(resultMap.get("id"));
			} else {
				config.setAggreatorid(resultMap.get("aggreatorid"));
			}
			Map<String, String> config1 = service.getWalletConfig(config);

			/*************************
			 * for menu options
			 ***************************/
			commBean.setAggreatorid(config.getAggreatorid());
			Map<String, HashMap<String, String>> res = commService.getPlanDtl(commBean);
			if (resultMap != null) {
				setPlans(res.get("PLAN"));
				setTxnType(res.get("TXNTYPE"));
			}

			session.put("menuMap", res.get("TXNTYPE"));
			/*************************
			 * end menu options
			 ***************************/

			if (config1 == null || config1.size() == 0) {
				addActionError(prop.getProperty("1011"));
				return "fail";
			}

			User user = new User();
			ReportBean rBean = new ReportBean();
			rBean.setUserId(resultMap.get("id"));
			if (rBean.getUserId() != null && Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 1
					&& Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 99) {
				session.put("commission", new CommissionServiceImpl().getCommission(rBean));
			}
			String userImg = service.getProfilePic(resultMap.get("id"));
			user.setUserImg(userImg);
			user.setEmailid(resultMap.get("emailid"));
			user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
			user.setCashBackfinalBalance(Double.parseDouble(String.valueOf(resultMap.get("cashBackFinalBalance"))));
			user.setId(resultMap.get("id"));
			user.setMobileno(resultMap.get("mobileno"));
			user.setName(resultMap.get("name"));
			user.setWalletid(resultMap.get("walletid"));
			user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
			user.setAgentid(resultMap.get("agentid"));
			user.setSubAgentId(resultMap.get("subAgentId"));
			user.setDistributerid(resultMap.get("distributerid"));
			user.setSuperdistributerid(
					resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
			user.setAggreatorid(resultMap.get("aggreatorid"));
			user.setToken(loginResponse.getToken());
			user.setTokenSecurityKey(loginResponse.getTokenSecurityKey());
			user.setKycStatus(resultMap.get("kycStatus"));
			user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
			user.setWhiteLabel(
					Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));
			this.user.setUserId(resultMap.get("id"));
			/*
			 * if(!checkLoggedinUser(this.user)){ addActionError(
			 * "You have already logged into Bhartipay from another machine. Do you want to reset the session?  <a href='#' onClick='removeSession(\""
			 * +this.user.getUserId()+ "\")' title='Remove Duplication Session'>Yes</a>");
			 * return "fail"; }
			 */

			if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 3
					|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 7) {
				menuMapping.setUserId(user.getId());
				menuMapping = service.getUserMenuMapping(menuMapping);
				if (menuMapping != null && menuMapping.getMenuType() != null) {
					List<String> aggMenus = Arrays.asList(menuMapping.getMenuType());
					session.put("aggMenus", aggMenus);
				}

				roleMapping.setUserId(user.getId());
				roleMapping = service.getUserRoleMapping(roleMapping);
				if (roleMapping != null && roleMapping.getRoleType() != null) {
					List<String> aggRoles = Arrays.asList(roleMapping.getRoleType());
					session.put("aggRoles", aggRoles);
				}

				if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6) {
					user.setSubAggregatorId(user.getId());
					user.setId(user.getAggreatorid());
				}
			}

			user.setCountry(config1.get("country"));
			user.setCountrycurrency(config1.get("countrycurrency"));
			user.setCountryid(Double.parseDouble(String.valueOf(config1.get("countryid"))));
			user.setEmailvalid(Double.parseDouble(String.valueOf(config1.get("emailvalid"))));
			user.setKycvalid(Double.parseDouble(String.valueOf(config1.get("kycvalid"))));
			user.setOtpsendtomail(Double.parseDouble(String.valueOf(config1.get("otpsendtomail"))));
			user.setSmssend(Double.parseDouble(String.valueOf(config1.get("smssend"))));
			user.setStatus(Double.parseDouble(String.valueOf(config1.get("status"))));
			user.setLogo(config1.get("logopic"));
			user.setCustomerCare(config1.get("customerCare") != null ? config1.get("customerCare") : "0120-4000004");
			user.setSupportEmailId(config1.get("supportEmailId") != null ? config1.get("supportEmailId") : "");
			user.setBanner(config1.get("bannerpic"));
			session.put("logo", config1.get("logopic"));
			session.put("banner", config1.get("bannerpic"));

			session.put("User", user);

			if ((userData.isEmpty() || !userId.equalsIgnoreCase(resultMap.get("id"))) && user.getUsertype() == 2
					&& user.getAggreatorid() != null && user.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
				service.otpResend(userSummary);
				session.put("User", null);
				session.put("loginResponse", loginResponse);
				return "uservalidation";
			}

			if (user.getUsertype() == 2 && user.getAggreatorid() != null
					&& user.getAggreatorid().equalsIgnoreCase("OAGG001050")) {

				try {
					Cookie cookie2 = new Cookie("userdata", EncryptionDecryption.getEncrypted(resultMap.get("id"),
							"km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw="));
					cookie2.setMaxAge(60 * 60 * 24 * 15);
					response.addCookie(cookie2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (user.getUsertype() == 1) {
				return "customer";
			}
			System.out.println("during login " + resultMap);
			if (user.getUsertype() == 99) {
				List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
				request.setAttribute("txnList", list);
			}
			return "success";

		}

		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("7000"));
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7010")) {
			addActionError(prop.getProperty("7010"));
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7011")) {
			addActionError(prop.getProperty("7011"));
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7012")) {
			addActionError(prop.getProperty("7012"));
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7039")) {
			addActionError(prop.getProperty("7039"));
			return "fail";
		}

		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7050")) {
			addActionError(prop.getProperty("7050"));
			return "fail";
		}
		System.out.println("result is " + loginResponse.getStatusCode());
		return "input";

	}

	public String forgotPasswordOTP() {
		System.out.println("++++++++user type ++++++++++++++++++++++++++++++++++++++++++++" + user.getUsertype());
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);

		if (user.getUserId() == null || user.getUserId().isEmpty()) {
			addActionError("Please enter valid Email/Mobile number");
			request.setAttribute("forgot", "forgot");
			return "fail";
		}
		logger.debug("************** calling service forgotPassword() and returning success ******************");
		String result = service.forgotPassword(user);
		if (result.equalsIgnoreCase("1000")) {
			if (session.get("custType") != null) {
				String custType = (String) session.get("custType");
				if (custType.equals("1")) {
					session.put("fogetPwdOtp", "true");
					return "customer";
				}
			}
			return "success";
		}
		if (result.equalsIgnoreCase("1001")) {
			addActionError(prop.getProperty("1001"));
		}
		if (result.equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("7000"));
		}
		if (result.equalsIgnoreCase("7010")) {
			addActionError(prop.getProperty("7010"));
		}
		if (result.equalsIgnoreCase("7011")) {
			addActionError(prop.getProperty("7011"));
		}
		if (result.equalsIgnoreCase("7036")) {
			addActionError(prop.getProperty("7036"));
		}
		if (user.getUsertype() == 1) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("forgot", "forgot");
			return "failone";
		} else {
			return "fail";
		}
	}

	public String changePassword() {
		System.out.println("++++++++user type ++++++++++++++++++++++++++++++++++++++++++++" + user.getUsertype());
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);

		if (user.getUserId() == null || user.getUserId().isEmpty()) {
			addActionError("An error has occurred.");
			return "fail";
		}
		if (user.getPassword() == null || user.getPassword().isEmpty()) {
			addActionError("Please enter new password.");
			return "fail";
		}
		if (user.getConfirmPassword() == null || user.getPassword().isEmpty()) {
			addActionError("Please enter confirm new password.");
			return "fail";
		}
		if (!user.getPassword().equalsIgnoreCase(user.getConfirmPassword())) {
			addActionError("New password and confirm new password must be same.");
			return "fail";
		}
		String result = service.setForgotPassword(user);

		if (result != null && result.equalsIgnoreCase("1000")) {
			addActionMessage("Password changed successfully.");
			if (user.getUsertype() == 1) {
				session.put("errorsignup", "errorsignup");
				request.setAttribute("login", "login");
				user = new UserSummary();
				return "successone";
			}
			user = new UserSummary();
			return "success";
		}
		// user=new UserSummary();
		if (result != null && result.equalsIgnoreCase("1000")) {
			addActionError(prop.getProperty("1000"));
			return "fail";
		} else if (result.equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "fail";
		} else if (result.equalsIgnoreCase("7013")) {
			// addActionError("Current Password didn't match or New Password is
			// one of the previous 5 used passwords.");
			addActionError(prop.getProperty("7013"));
			return "fail";
		}

		return "fail";
	}

	public String changeFirstPassword() {

		System.out.println("++++++++aggId ++++++++++++++++++++++++++++++++++++++++++++" + session.get("aggId"));
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		System.out.println("++++++++aggId ++++++++++++++++++++++++++++++++++++++++++++" + user.getAggreatorid());
		if (user.getUserId() == null || user.getUserId().isEmpty()) {
			addActionError("An error has occurred.");
			return "fail";
		}

		if (user.getOldpassword() == null || user.getOldpassword().isEmpty()) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError("Please enter current password.");
			return "fail";
		}
		if (user.getPassword() == null || user.getPassword().isEmpty()) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError("Please enter new password.");
			return "fail";
		}
		if (user.getConfirmPassword() == null || user.getPassword().isEmpty()) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError("Please enter confirm new password.");
			return "fail";
		}
		if (!user.getPassword().equalsIgnoreCase(user.getConfirmPassword())) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError("New Password and Confirm New Password must be same.");
			return "fail";
		}
		if (user.getOldpassword().equals(user.getPassword())) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError("Current Password and New Password can not be same.");
			return "fail";
		}
		String result = service.setFirstTimePassword(user);
		if (result != null && result.equalsIgnoreCase("1000")) {
			addActionMessage("Password changed successfully.");
			if (user.getUsertype() == 1) {
				session.put("errorsignup", "errorsignup");
				user = new UserSummary();
				return "successone";
			}
			user = new UserSummary();
			return "success";
		}
		// user=new UserSummary();
		if (result != null && result.equalsIgnoreCase("1000")) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError(prop.getProperty("1000"));
			return "fail";
		} else if (result != null && result.equalsIgnoreCase("1000")) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			return "fail";
		} else if (result.equalsIgnoreCase("7038")) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError(prop.getProperty("7038"));
			return "fail";
		}
		if (result.equalsIgnoreCase("7013")) {
			user.setOldpassword("");
			user.setPassword("");
			user.setConfirmPassword("");
			addActionError(prop.getProperty("70131"));
			return "fail";

		}
		if (result.equalsIgnoreCase("7702")) {
			addActionError(prop.getProperty("7702"));
			return "success";
		}
		if (result.equalsIgnoreCase("7703")) {
			addActionError(prop.getProperty("7703"));
			return "success";
		}
		return "fail";
	}

	public String getImage() {
		User user = (User) session.get("User");
		System.out.println(request.getParameter("userId"));
		String userImg = service.getProfilePic(user.getId());
		System.out.println("=============================================================dfdfsf userImg" + userImg);
		request.setAttribute("userImg", userImg);
		return "success";
	}

	public String walletLogin() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		System.out.println("User login requested.");

		String domainName = request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).substring(0,
				request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).indexOf("/"));
		session.put("domainName", domainName);
		config.setDomainName(domainName);
		String aggId = service.getAggIdByDomain(config);
		if (aggId.equalsIgnoreCase("error")) {
			return "Error";
		} else {

		}
		config.setAggreatorid(aggId);
		session.put("aggId", aggId);
		Map<String, String> mapResult = service.getWalletConfig(config);
		if (mapResult != null) {
			session.put("mapResult", mapResult);
			System.out.println(mapResult);
			session.put("logo", mapResult.get("logopic"));
			session.put("banner", mapResult.get("bannerpic"));
		}

		user.setAggreatorid(aggId);
		LoginResponse loginResponse = service.login(user, ipAddress, userAgent);

		PaymentParameter paymentParameter = (PaymentParameter) session.get("PaymentParameter");
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1010")) {
			// otp send to user
			return "otpvalidation";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("1011")) {
			// user successfully login
			Map<String, String> resultMap = service.ProfilebyloginId(user);
			if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4) {
				config.setAggreatorid(resultMap.get("id"));
			} else {
				config.setAggreatorid(resultMap.get("aggreatorid"));
			}

			/*************************
			 * for menu options
			 ***************************/
			commBean.setAggreatorid(config.getAggreatorid());
			Map<String, HashMap<String, String>> res = commService.getPlanDtl(commBean);
			if (resultMap != null) {
				setPlans(res.get("PLAN"));
				setTxnType(res.get("TXNTYPE"));
			}

			session.put("menuMap", res.get("TXNTYPE"));
			/*************************
			 * end menu options
			 ***************************/

			Map<String, String> config1 = service.getWalletConfig(config);

			if (config1 == null || config1.size() == 0) {
				addActionError(prop.getProperty("1011"));
				return "fail";
			}
			User user = new User();

			String userImg = service.getProfilePic(resultMap.get("id"));
			user.setUserImg(userImg);
			user.setEmailid(resultMap.get("emailid"));
			user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
			user.setId(resultMap.get("id"));
			user.setMobileno(resultMap.get("mobileno"));
			user.setName(resultMap.get("name"));
			user.setWalletid(resultMap.get("walletid"));
			user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
			user.setAgentid(resultMap.get("agentid"));
			user.setSubAgentId(resultMap.get("subAgentId"));
			user.setDistributerid(resultMap.get("distributerid"));
			user.setSuperdistributerid(
					resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
			user.setAggreatorid(resultMap.get("aggreatorid"));
			user.setToken(resultMap.get("token"));
			user.setTokenSecurityKey(resultMap.get("tokenSecurityKey"));
			user.setKycStatus(resultMap.get("kycStatus"));
			user.setCountry(config1.get("country"));
			user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
			user.setWhiteLabel(
					Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));

			user.setCountrycurrency(config1.get("countrycurrency"));
			user.setCountryid(Double.parseDouble(String.valueOf(config1.get("countryid"))));
			user.setEmailvalid(Double.parseDouble(String.valueOf(config1.get("emailvalid"))));
			user.setKycvalid(Double.parseDouble(String.valueOf(config1.get("kycvalid"))));
			user.setOtpsendtomail(Double.parseDouble(String.valueOf(config1.get("otpsendtomail"))));
			user.setSmssend(Double.parseDouble(String.valueOf(config1.get("smssend"))));
			user.setStatus(Double.parseDouble(String.valueOf(config1.get("status"))));
			user.setLogo(config1.get("logopic"));
			user.setCustomerCare(config1.get("customerCare") != null ? config1.get("customerCare") : "0120-4000004");
			user.setSupportEmailId(config1.get("supportEmailId") != null ? config1.get("supportEmailId") : "");
			user.setBanner(config1.get("bannerpic"));
			session.put("logo", config1.get("logopic"));
			session.put("banner", config1.get("bannerpic"));

			session.put("User", user);

			return "success";

		}

		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7000")) {
			paymentParameter.setStatusCode(loginResponse.getStatusCode());
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7010")) {
			paymentParameter.setStatusCode(loginResponse.getStatusCode());
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7011")) {
			paymentParameter.setStatusCode(loginResponse.getStatusCode());
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7012")) {
			paymentParameter.setStatusCode(loginResponse.getStatusCode());
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			return "fail";
		}
		if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
				&& loginResponse.getStatusCode().equalsIgnoreCase("7039")) {
			paymentParameter.setStatusCode(loginResponse.getStatusCode());
			paymentParameter.setStatusDesc("failed");
			session.put("PaymentParameter", paymentParameter);
			// addActionError("Your Aggregator has not approved your
			// authentication yet. Please contact to your Aggregator.");
			return "fail";
		}
		paymentParameter.setStatusCode(loginResponse.getStatusCode());
		paymentParameter.setStatusDesc("failed");
		session.put("PaymentParameter", paymentParameter);
		return "input";

	}

	public String validateOTP() {

		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		if (user != null && user.getOtp() != null && !user.getOtp().isEmpty()) {
			String result = service.validateOTP(user);
			System.out.println(result);
			if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {

				Map<String, String> resultMap = service.ProfilebyloginId(user);

				if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4) {
					config.setAggreatorid(resultMap.get("id"));
				} else {
					config.setAggreatorid(resultMap.get("aggreatorid"));
				}

				Map<String, String> config1 = service.getWalletConfig(config);

				if (config1 == null || config1.size() == 0) {
					addActionError("An error has occurred please try again.");
					return "fail";
				}
				User user = new User();

				String userImg = service.getProfilePic(resultMap.get("id"));
				user.setUserImg(userImg);
				user.setEmailid(resultMap.get("emailid"));
				user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
				user.setId(resultMap.get("id"));
				user.setMobileno(resultMap.get("mobileno"));
				user.setName(resultMap.get("name"));
				user.setWalletid(resultMap.get("walletid"));
				user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
				user.setAgentid(resultMap.get("agentid"));
				user.setSubAgentId(resultMap.get("subAgentId"));
				user.setDistributerid(resultMap.get("distributerid"));
				user.setSuperdistributerid(
						resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
				user.setAggreatorid(resultMap.get("aggreatorid"));
				user.setKycStatus(resultMap.get("kycStatus"));
				user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
				user.setWhiteLabel(
						Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));

				user.setCountry(config1.get("country"));
				user.setCountrycurrency(config1.get("countrycurrency"));
				user.setCountryid(Double.parseDouble(String.valueOf(config1.get("countryid"))));
				user.setEmailvalid(Double.parseDouble(String.valueOf(config1.get("emailvalid"))));
				user.setKycvalid(Double.parseDouble(String.valueOf(config1.get("kycvalid"))));
				user.setOtpsendtomail(Double.parseDouble(String.valueOf(config1.get("otpsendtomail"))));
				user.setSmssend(Double.parseDouble(String.valueOf(config1.get("smssend"))));
				user.setStatus(Double.parseDouble(String.valueOf(config1.get("status"))));
				user.setLogo(config1.get("logopic"));
				user.setCustomerCare(
						config1.get("customerCare") != null ? config1.get("customerCare") : "0120-4000004");
				user.setSupportEmailId(config1.get("supportEmailId") != null ? config1.get("supportEmailId") : "");
				user.setBanner(config1.get("bannerpic"));
				session.put("logo", config1.get("logopic"));
				session.put("banner", config1.get("bannerpic"));

				session.put("User", user);
				if (user.getUsertype() == 1) {
					session.put("registration", "true");
					return "customer";
				}
				return "success";
			} else {
				addActionError("Please enter valid OTP.");
				return "fail";
			}
		} else {
			addActionError("Please enter a valid OTP.");
			return "fail";
		}

	}

	public String verifyOTP() {

		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		if (user != null && user.getOtp() != null && !user.getOtp().isEmpty()) {
			String result = service.verifyOTP(user);
			System.out.println(result);
			if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {
				return "success";
			} else {
				addActionError("Please enter valid OTP.");
				return "fail";
			}
		} else {
			addActionError("Please enter a valid OTP.");
			return "fail";
		}

	}

	public String otpResend() {
		generateCsrfToken();
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		if (user != null && user.getUserId() != null && !user.getUserId().isEmpty()) {
			String result = service.otpResend(user);
			System.out.println(result);
			if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {
				addActionMessage("OTP has been sent successfully.");
				if (user.getUserstatus() != null && user.getUserstatus().equalsIgnoreCase("fpo")) {
					return "fpo";
				} else {
					return "success";
				}
			} else {
				addActionError("Please try again.");
				return "success";
			}
		} else {
			addActionError("Please try again.");
			return "success";
		}
	}

	public String cashOtpResend() {
		generateCsrfToken();
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		if (user != null && user.getUserId() != null && !user.getUserId().isEmpty()) {
			String result = service.otpResend(user);
			System.out.println(result);
			if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {
				addActionMessage("OTP has been sent successfully on Payer's mobile number.");
				if (user.getUserstatus() != null && user.getUserstatus().equalsIgnoreCase("fpo")) {
					return "fpo";
				} else {
					return "success";
				}
			} else {
				addActionError("Please try again.");
				return "success";
			}
		} else {
			addActionError("Please try again.");
			return "success";
		}
	}

	public String home() {
		System.out.println("home action calling");
		generateCsrfToken();
		RechargeService rs = new RechargeServiceImpl();
		logger.debug("******************calling recharge() action and return success***********************");
		Map<String, HashMap<String, String>> mapResult2 = rs.getRechargeOperator();
		request.setAttribute("mapResult", mapResult2);
		// session.put("loginStatus", "");

		if (mapResult2 != null) {
			try {
				setPrePaid(mapResult2.get("PREPAID"));
				setPostPaid(mapResult2.get("POSTPAID"));
				setDth(mapResult2.get("DTH"));
				setLandLine(mapResult2.get("LANDLINE"));
				setPrePaidDataCard(mapResult2.get("PREPAIDDATACARD"));
				setPostPaidDataCard(mapResult2.get("POSTPAIDDATACARD"));
				setCircle(mapResult2.get("CIRCLE"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String domainName = request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).substring(0,
				request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).indexOf("/"));
		session.put("domainName", domainName);
		config.setDomainName(domainName);
		String aggId = service.getAggIdByDomain(config);
		if (aggId.equalsIgnoreCase("error")) {
			return "Error";
		} else {
			if (user.getUsertype() == 99) {
				List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
				request.setAttribute("txnList", list);
			}

			// Map<String, String> map=service.getCountry();
			config.setAggreatorid(aggId);
			session.put("aggId", aggId);
			Map<String, String> mapResult = service.getWalletConfig(config);
			if (mapResult != null) {
				// setCountryCode(map);
				session.put("sessionid", request.getSession().getId());
				session.put("mapResult", mapResult);
				System.out.println(mapResult);
				session.put("logo", mapResult.get("logopic"));
				session.put("banner", mapResult.get("bannerpic"));
			} else {
				addActionError("An error has occurred please try again.");
			}
			User user = (User) session.get("User");

			if (user != null && user.getUsertype() == 99) {
				List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
				request.setAttribute("txnList", list);
			}
			if (user != null && user.getUsertype() != 1) {

				return "admin";
			} else {
				return "success";
			}
		}

	}

	public String checkUserSession() {
		JSONObject jObject = new JSONObject();
		jObject.put("status", "FALSE");
		try {
			if (session != null && session.get("sessionid") != null) {
				String currentSessionId = request.getSession().getId();
				String oldSessionId = (String) session.get("sessionid");
				if (currentSessionId.equalsIgnoreCase(oldSessionId)) {
					jObject.put("status", "TRUE");
				}
				// session.put("sessionid", request.getSession().getId());
			}
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String userHome() {
		generateCsrfToken();
		System.out.println("home action calling");

		// Map<String, String> map=service.getCountry();
		String domainName = request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).substring(0,
				request.getRequestURL().substring(request.getRequestURL().indexOf("//") + 2).indexOf("/"));
		session.put("domainName", domainName);
		config.setDomainName(domainName);

		String aggId = service.getAggIdByDomain(config);

		if (aggId.equalsIgnoreCase("error")) {
			return "Error";
		} else {

			config.setAggreatorid(aggId);
			Map<String, String> mapResult = service.getWalletConfig(config);

			if (mapResult != null) {
				// setCountryCode(map);
				session.put("sessionid", request.getSession().getId());
				session.put("mapResult", mapResult);
				System.out.println(mapResult);
				session.put("aggId", aggId);
				session.put("logo", mapResult.get("logopic"));
				session.put("banner", mapResult.get("bannerpic"));

			} else {
				addActionError("An error has occurred please try again.");
			}
			User user = (User) session.get("User");
			if (user != null && user.getUsertype() == 99) {
				List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
				request.setAttribute("txnList", list);
			}
			return "success";
		}

	}

	public String signUp() {
		System.out.println("signup action calling");

		String aggId = (String) session.get("aggId");
		config.setAggreatorid(aggId);
		Map<String, String> mapResult = service.getWalletConfig(config);
		if (mapResult != null) {
			// setCountryCode(map);
			session.put("mapResult", mapResult);
			System.out.println(mapResult);
		} else {
			addActionError("An error has occurred please try again.");
		}
		return "success";
	}

	public String saveSignUpUser() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		// Map<String, String> map=service.getCountry();
		// if(map!=null){
		// setCountryCode(map);
		// }
		System.out.println(user.getName());
		System.out.println(user.getMobileno());
		System.out.println(user.getEmailid());
		System.out.println(user.getCountrycode());
		System.out.println(user.getPassword());
		System.out.println(user.getConfirmPassword());

		if (user.getPassword() != null && user.getConfirmPassword() != null
				&& !user.getPassword().equals(user.getConfirmPassword())) {
			addActionError("Password and Confirm Password not match");
			return "success";
		}

		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		String result = service.saveSignUpUser(user, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.equalsIgnoreCase("1000")) {
			addActionMessage("User created successfully.");
			user = new UserSummary();
			return "success";
		} else if (result.equalsIgnoreCase("7001")) {
			addActionError(prop.getProperty("7001"));
			return "success";
		} else if (result.equalsIgnoreCase("7002")) {
			addActionError(prop.getProperty("7002"));
			return "success";
		} else if (result.equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "success";
		} else if (result.equalsIgnoreCase("7041")) {
			addActionError(prop.getProperty("7041"));
			return "success";
		} else if (result.equalsIgnoreCase("7040")) {
			addActionError(prop.getProperty("7040"));
			return "success";
		} else {

			addActionError("There is some problem please try again.");
			return "success";
		}
	}

	public String saveSignUpUserByAgent() {
		request.setAttribute("userType", "" + walletBean.getUsertype());
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++savesignupuser");
		User user1 = (User) session.get("User");
		String aggId = (String) session.get("aggId");
		System.out.println(walletBean.getAgentid());
		if (user1.getUsertype() == 99) {
			usertypeList.put("4", "Aggregator");
			usertypeList.put("3", "Distributor");
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
		}
		if (user1.getUsertype() == 4 || user1.getUsertype() == 6) {

			if (user1.getUsertype() == 4) {
				usertypeList.put("6", "Sub-aggregator");
			}
			usertypeList.put("3", "Distributor");
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
			reportBean.setAggId(user1.getId());
			reportBean.setDistId("-1");
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}

		}
		if (user1.getUsertype() == 3) {
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
			reportBean.setAggId("-1");
			reportBean.setDistId(user1.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
		}

		if (user1.getUsertype() == 2) {
			reportBean.setAggId("-1");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user1.getId());
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
		}

		if (user1.getUsertype() == 5) {
			usertypeList.put("1", "Customer");
		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getActiveDistributerByAggId(walletBean.getAggreatorid());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getActiveAgentByDistId(walletBean.getDistributerid());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}
		Map<String, String> subagentList = service.getSubAgentByAgentId(walletBean.getAgentid());
		if (subagentList != null && agentList.size() > 0) {
			setSubagentList(subagentList);
		}

		System.out.println(
				"******************************************************************" + walletBean.getDistributerid());
		System.out.println(
				"******************************************************************" + walletBean.getAggreatorid());
		System.out.println(
				"******************************************************************" + walletBean.getAgentid());
		System.out.println(
				"******************************************************************" + walletBean.getSubAgentId());
		if (walletBean.getUsertype() == -1) {
			addActionError("Please select usertype.");
			return "success";
		}
		if (walletBean.getUsertype() == 1 || walletBean.getUsertype() == 5) {
			if (walletBean.getDistributerid().equalsIgnoreCase("-1")) {
				addActionError("Please select Distributor.");
				return "success";
			}
			if (walletBean.getAgentid().equalsIgnoreCase("-1")) {
				addActionError("Please select Agent.");
				return "success";
			}
		}
		if (walletBean.getUsertype() == 2) {
			if (walletBean.getDistributerid().equalsIgnoreCase("-1")) {
				addActionError("Please select Distributor.");
				return "success";
			}
		}

		boolean b = walletBean.getName().contains(" +");
		if (b) {
			addActionError("More than one space not allowed in name.");
			return "success";
		}

		if (walletBean.getUsertype() == 3) {
			walletBean.setDistributerid("-1");
			walletBean.setAgentid("-1");
			walletBean.setSubAgentId("-1");
		}
		if (walletBean.getUsertype() == 2) {
			walletBean.setAgentid("-1");
			walletBean.setSubAgentId("-1");
		}
		if (walletBean.getUsertype() == 5) {
			walletBean.setSubAgentId("-1");
		}

		if (walletBean.getPassword() != null && walletBean.getConfirmPassword() != null
				&& !walletBean.getPassword().equals(walletBean.getConfirmPassword())) {
			addActionError("Password and Confirm Password not match");
			return "success";
		}

		walletBean.setAggreatorid(aggId);

		String result = service.saveSignUpUserByAgent(walletBean, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.equalsIgnoreCase("1000")) {
			addActionMessage("User created successfully.");
			walletBean = new WalletMastBean();
			request.setAttribute("userType", "-1");
			return "success";
		} else if (result.equalsIgnoreCase("7001")) {
			addActionError(prop.getProperty("7001"));
			return "success";
		} else if (result.equalsIgnoreCase("7002")) {
			addActionError(prop.getProperty("7002"));
			return "success";
		} else if (result.equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "success";
		} else if (result.equalsIgnoreCase("7041")) {
			addActionError(prop.getProperty("7041"));
			return "success";
		} else if (result.equalsIgnoreCase("7040")) {
			addActionError(prop.getProperty("7040"));
			return "success";
		} else {
			addActionError("There is some problem please try again.");
			return "success";
		}
	}

	public String saveSignUpUser1() {
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		if (user.getPassword() != null && user.getConfirmPassword() != null
				&& !user.getPassword().equals(user.getConfirmPassword())) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", "Password and Confirm Password not match.");
			// addActionError("Password and Confirm Password not match");
			return "success";
		}

		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);

		String result = service.saveSignUpUser(user, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.equalsIgnoreCase("1000")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("login", "login");

			Map<String, String> mapResult = (Map<String, String>) request.getSession().getAttribute("mapResult");
			if (mapResult != null && mapResult.get("emailvalid") != null
					&& Double.parseDouble(String.valueOf(mapResult.get("emailvalid"))) == 0) {
				session.put("loginStatus", "fail");
				// addActionMessage("Your account has been created successfully.
				// Please login.");
				session.put("msg", "Your account has been created successfully. Please login.");
			} else {
				addActionMessage(
						"Your account has been created successfully. Please login with the password which has been sent to your email id.");
			}
			user = new UserSummary();
			return "success";
		} else if (result.equalsIgnoreCase("7001")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", prop.getProperty("7001"));
			// addActionError(prop.getProperty("7001"));
			return "success";
		} else if (result.equalsIgnoreCase("7002")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", prop.getProperty("7002"));
			// addActionError(prop.getProperty("7002"));
			return "success";
		} else if (result.equalsIgnoreCase("7038")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", prop.getProperty("7038"));
			// addActionError(prop.getProperty("7038"));
			return "success";
		} else if (result.equalsIgnoreCase("7041")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", prop.getProperty("7041"));
			// addActionError(prop.getProperty("7041"));
			return "success";
		} else if (result.equalsIgnoreCase("7040")) {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", prop.getProperty("7040"));
			// addActionError(prop.getProperty("7040"));
			return "success";
		} else {
			session.put("errorsignup", "errorsignup");
			request.setAttribute("register", "register");
			session.put("errmsg", "There is some problem please try again.");
			// addActionError("There is some problem please try again.");
			return "success";
		}
	}

	public String createNewUser() {
		generateCsrfToken();
		User user = (User) session.get("User");

		if (user.getUsertype() == 99) {
			usertypeList.put("4", "Aggregator");
			usertypeList.put("3", "Distributor");
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
		}
		if (user.getUsertype() == 4 || user.getUsertype() == 6) {
			if (user.getUsertype() == 4) {
				usertypeList.put("6", "Sub-aggregator");
			}
			usertypeList.put("3", "Distributor");
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
			reportBean.setAggId(user.getId());
			reportBean.setDistId("-1");
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
		}
		if (user.getUsertype() == 3) {
			usertypeList.put("2", "Agent");
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
			reportBean.setAggId("agg");
			reportBean.setDistId(user.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
		}

		if (user.getUsertype() == 2) {
			reportBean.setAggId("agg");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user.getId());
			usertypeList.put("5", "Sub-agent");
			usertypeList.put("1", "Customer");
		}

		if (user.getUsertype() == 5) {
			usertypeList.put("1", "Customer");
		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getActiveDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getActiveAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}
		Map<String, String> subagentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subagentList != null && agentList.size() > 0) {
			setSubagentList(subagentList);
		}

		return "success";

	}

	public String agentOnBoard() {
		generateCsrfToken();
		User user = (User) session.get("User");

		Map<String, String> distributorList = service.getActiveDistributerByAggId(user.getAggreatorid());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		return "success";

	}

	public String saveAgentOnBoard() {
		request.setAttribute("userType", "" + walletBean.getUsertype());
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		System.out.println("++++++++++++++++++++++++++++++++++++++++++++saveAgentOnBoard");
		User user1 = (User) session.get("User");
		String aggId = (String) session.get("aggId");
		System.out.println(walletBean.getAgentid());

		if (walletBean.getState().equalsIgnoreCase("-1")) {
			addActionError(prop.getProperty("224001"));
			return "fail";
		}

		Map<String, String> distributorList = service.getActiveDistributerByAggId(user1.getAggreatorid());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		System.out.println(
				"******************************************************************" + walletBean.getDistributerid());
		System.out.println(
				"******************************************************************" + walletBean.getAggreatorid());

		boolean b = walletBean.getName().contains(" +");
		if (b) {
			addActionError("More than one space not allowed in name.");
			return "success";
		}

		walletBean.setAggreatorid(aggId);
		walletBean.setUsertype(2);
		WalletMastBean result = service.saveAgentOnBoard(walletBean, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("User created successfully please upload related documents.");

			walletBean = new WalletMastBean();
			walletBean.setId(result.getId());
			return "success";
		} else if (result.getStatusCode().equalsIgnoreCase("7001")) {
			addActionError(prop.getProperty("7001"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7002")) {
			addActionError(prop.getProperty("7002"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7041")) {
			addActionError(prop.getProperty("7041"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7040")) {
			addActionError(prop.getProperty("7040"));
			return "fail";
		} else {
			addActionError("There is some problem please try again.");
			return "fail";
		}
	}

	public String updateAgentOnBoad() {
		request.setAttribute("userType", "" + walletBean.getUsertype());
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("AGENTDOC");
		File dir = new File(filePath, walletBean.getId());
		boolean mkdir = dir.mkdir();
		filePath = filePath.concat("/" + walletBean.getId());

		if (walletBean.getFile1() != null || walletBean.getFile1FileName() != null
				|| walletBean.getFile1ContentType() != null) {
			try {

				if ((walletBean.getFile1ContentType().equalsIgnoreCase("image/png")
						|| walletBean.getFile1ContentType().equalsIgnoreCase("image/gif")
						|| walletBean.getFile1ContentType().equalsIgnoreCase("image/jpeg"))
						&& (walletBean.getFile1FileName().toLowerCase().endsWith(".png")
								|| walletBean.getFile1FileName().toLowerCase().endsWith(".gif")
								|| walletBean.getFile1FileName().toLowerCase().endsWith(".jpeg"))
						&& walletBean.getFile1().length() <= 2000000) {
					File fileToCreate1 = new File(filePath, walletBean.getFile1FileName());
					FileUtils.copyFile(walletBean.getFile1(), fileToCreate1);
					walletBean.setIdLoc("./AGENTDOC/" + walletBean.getId() + "/" + walletBean.getFile1FileName());

				} else {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}
				if (walletBean.getFile1().length() > 2000000) {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					walletBean.setIdLoc("");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (walletBean.getFile2() != null || walletBean.getFile2FileName() != null
				|| walletBean.getFile2ContentType() != null) {
			try {

				if ((walletBean.getFile2ContentType().equalsIgnoreCase("image/png")
						|| walletBean.getFile2ContentType().equalsIgnoreCase("image/gif")
						|| walletBean.getFile2ContentType().equalsIgnoreCase("image/jpeg"))
						&& walletBean.getFile2().length() <= 2000000) {
					File fileToCreate1 = new File(filePath, walletBean.getFile2FileName());
					FileUtils.copyFile(walletBean.getFile2(), fileToCreate1);
					walletBean.setAddressLoc("./AGENTDOC/" + walletBean.getId() + "/" + walletBean.getFile2FileName());

				} else {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}
				if (walletBean.getFile2().length() > 2000000) {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					walletBean.setAddressLoc("");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (walletBean.getFile3() != null || walletBean.getFile3FileName() != null
				|| walletBean.getFile3ContentType() != null) {
			try {

				if ((walletBean.getFile3ContentType().equalsIgnoreCase("application/pdf"))
						&& walletBean.getFile3().length() <= 5000000) {
					File fileToCreate1 = new File(filePath, walletBean.getFile3FileName());
					FileUtils.copyFile(walletBean.getFile3(), fileToCreate1);
					walletBean.setFormLoc("./AGENTDOC/" + walletBean.getId() + "/" + walletBean.getFile3FileName());

				} else {
					addActionError("Document are allowed only pdf file format and file size must not exceed 4 MB.");
					return "success";
				}
				if (walletBean.getFile3().length() > 5000000) {
					addActionError("Document are allowed only pdf file format and file size must not exceed 4 MB.");
					walletBean.setFormLoc("");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String aggId = (String) session.get("aggId");
		walletBean.setAggreatorid(aggId);
		WalletMastBean result = service.updateAgentOnBoad(walletBean, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Document uploaded successfully with agent id " + result.getId());

			walletBean = new WalletMastBean();
			walletBean.setId(result.getId());
			return "success";
		} else if (result.getStatusCode().equalsIgnoreCase("7001")) {
			addActionError(prop.getProperty("7001"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7002")) {
			addActionError(prop.getProperty("7002"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7041")) {
			addActionError(prop.getProperty("7041"));
			return "fail";
		} else if (result.getStatusCode().equalsIgnoreCase("7040")) {
			addActionError(prop.getProperty("7040"));
			return "fail";
		} else {
			addActionError("There is some problem please try again.");
			return "fail";
		}

	}

	public String searchUser() {

		User user = (User) session.get("User");

		if (user.getUsertype() == 99) {
			if (reportBean.getAggId() == null) {
				reportBean.setAggId("-1");
			}
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}
		if (user.getUsertype() == 4 || user.getUsertype() == 6) {

			reportBean.setAggId(user.getId());
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}
		if (user.getUsertype() == 3) {
			reportBean.setAggId("agg");
			reportBean.setDistId(user.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}

		if (user.getUsertype() == 2) {
			reportBean.setAggId("agg");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user.getId());
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		Map<String, String> subAgentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subAgentList != null && subAgentList.size() > 0) {
			setSubagentList(subAgentList);
		}

		List<WalletMastBean> userList = service.searchUser(reportBean);
		request.setAttribute("userL", userList);
		return "success";
	}

	public String userKycList() {

		User user = (User) session.get("User");
		if (user.getUsertype() == 99) {
			if (reportBean.getAggId() == null) {
				reportBean.setAggId("-1");
			}
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}
		if (user.getUsertype() == 4 || user.getUsertype() == 6) {

			reportBean.setAggId(user.getId());
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}
		if (user.getUsertype() == 3) {
			reportBean.setAggId("agg");
			reportBean.setDistId(user.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}

		if (user.getUsertype() == 2) {
			reportBean.setAggId("agg");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user.getId());
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		Map<String, String> subAgentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subAgentList != null && subAgentList.size() > 0) {
			setSubagentList(subAgentList);
		}

		List<WalletKYCBean> userList = service.getKycList(reportBean);
		request.setAttribute("userL", userList);
		return "success";

	}

	public String getDistributerByAggId() {
		System.out.println("==================================================" + reportBean.getAggId());
		Map<String, String> distributorList = service.getDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		return "success";
	}

	public String getAgentByDistId() {
		System.out.println("==================================================" + reportBean.getDistId());
		Map<String, String> agentList = service.getAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}
		return "success";
	}

	public String getActiveDistributerByAggId() {
		System.out.println("==================================================" + reportBean.getAggId());
		Map<String, String> distributorList = service.getActiveDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		return "success";
	}

	public String getActiveAgentByDistId() {
		System.out.println("==================================================" + reportBean.getDistId());
		Map<String, String> agentList = service.getActiveAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}
		return "success";
	}

	public String getSubagentByAgentId() {
		System.out.println("==================================================" + reportBean.getAgentId());
		Map<String, String> subagentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subagentList != null && subagentList.size() > 0) {
			setSubagentList(subagentList);
		}
		return "success";
	}

	public String senderProfile() {
		User user = (User) session.get("User");
		if (user.getUsertype() == 99) {
			reportBean.setUserId(null);
		} else {
			reportBean.setUserId(user.getId());
		}
		List<SenderProfileBean> senderList = service.getSenderProfile(reportBean);
		request.setAttribute("senderList", senderList);
		return "success";
	}

	public String getWalletConfig() {

		if (user != null && user.getCountrycode() != null && !user.getCountrycode().isEmpty()) {
			config.setAggreatorid("0");
			Map<String, String> mapResult = service.getWalletConfig(config);
			System.out.println(mapResult);
		}

		return "success";
	}

	public String showProfile() {
		generateCsrfToken();
		User user = (User) session.get("User");
		WalletMastBean mastBean = null;

		if (user != null && user.getSubAggregatorId() != null && !user.getSubAggregatorId().isEmpty()) {
			mastBean = service.showUserProfile(user.getSubAggregatorId());
		} else if (user != null && user.getId() != null) {
			mastBean = service.showUserProfile(user.getId());
		}
		if (mastBean != null) {
			setWalletBean(mastBean);
			return "success";
		}
		addActionError("An error has occurred.");
		return "fail";
	}

	public String custProfile() {
		generateCsrfToken();
		User user = (User) session.get("User");
		WalletMastBean mastBean = null;

		if (user != null && user.getSubAggregatorId() != null && !user.getSubAggregatorId().isEmpty()) {
			mastBean = service.showUserProfile(user.getSubAggregatorId());
		} else if (user != null && user.getId() != null) {
			mastBean = service.showUserProfile(user.getId());
		}
		if (mastBean != null) {
			session.put("custProfile", mastBean);
			return "success";
		}
		addActionError("An error has occurred.");
		return "fail";
	}

	public String updateUser() {
		session.put("userEdit", "userEdit");
		userStatusList.put("A", "Active");
		/* userStatusList.put("D", "Deactive"); */
		userStatusList.put("S", "Suspended");

		if (reportBean.getUserId() == null || reportBean.getUserId() == null) {
			addActionError("An error has occurred.");
			return "fail";
		}

		/*
		 * if(user!=null&&user.getSubAggregatorId()!=null&&!user.
		 * getSubAggregatorId().isEmpty()){
		 * mastBean=service.showUserProfile(user.getSubAggregatorId()); } else
		 * if(user!=null&&user.getId()!=null){
		 * mastBean=service.showUserProfile(user.getId()); }
		 */
		WalletMastBean mastBean = service.showUserProfile(reportBean.getUserId());

		if (mastBean != null) {
			setWalletBean(mastBean);
		}

		request.setAttribute("statusflag", "true");
		return "success";

	}

	public String acceptKyc() {
		System.out.println("****************************************************" + reportBean.getRefid());
		if (reportBean.getRefid() == null || reportBean.getRefid() == null) {
			addActionError("An error has occurred.");
			return "fail";
		}
		String status = service.acceptKyc(reportBean.getRefid());

		if (Boolean.parseBoolean(status)) {
			addActionMessage("KYC Accepted for Ref. Number " + reportBean.getRefid());
		} else {
			addActionError("KYC Not Accepted, Please Try Again.");
		}

		User user = (User) session.get("User");
		if (user.getUsertype() == 99) {
			if (reportBean.getAggId() == null) {
				reportBean.setAggId("-1");
			}
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}
		if (user.getUsertype() == 4 || user.getUsertype() == 6) {

			reportBean.setAggId(user.getId());
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}
		if (user.getUsertype() == 3) {
			reportBean.setAggId("agg");
			reportBean.setDistId(user.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}

		if (user.getUsertype() == 2) {
			reportBean.setAggId("agg");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user.getId());
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		Map<String, String> subAgentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subAgentList != null && subAgentList.size() > 0) {
			setSubagentList(subAgentList);
		}

		List<WalletKYCBean> userList = service.getKycList(reportBean);
		request.setAttribute("userL", userList);
		return "success";

	}

	public String rejectKyc() {
		System.out.println("****************************************************" + reportBean.getRefid());
		if (reportBean.getRefid() == null || reportBean.getRefid() == null) {
			addActionError("An error has occurred.");
			return "fail";
		}
		String status = service.rejectKyc(reportBean.getRefid());

		if (Boolean.parseBoolean(status)) {
			addActionMessage("KYC Rejected for Ref. Number  " + reportBean.getRefid());
		} else {
			addActionError("KYC Not Rejected, Please Try Again.");
		}

		User user = (User) session.get("User");
		if (user.getUsertype() == 99) {
			if (reportBean.getAggId() == null) {
				reportBean.setAggId("-1");
			}
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}
		if (user.getUsertype() == 4 || user.getUsertype() == 6) {

			reportBean.setAggId(user.getId());
			if (reportBean.getDistId() == null) {
				reportBean.setDistId("-1");
			}
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}
		if (user.getUsertype() == 3) {
			reportBean.setAggId("agg");
			reportBean.setDistId(user.getId());
			if (reportBean.getAgentId() == null) {
				reportBean.setAgentId("-1");
			}
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}
		}

		if (user.getUsertype() == 2) {
			reportBean.setAggId("agg");
			reportBean.setDistId("dis");
			reportBean.setAgentId(user.getId());
			if (reportBean.getSubagentId() == null) {
				reportBean.setSubagentId("-1");
			}

		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> distributorList = service.getDistributerByAggId(reportBean.getAggId());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		Map<String, String> agentList = service.getAgentByDistId(reportBean.getDistId());
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		Map<String, String> subAgentList = service.getSubAgentByAgentId(reportBean.getAgentId());
		if (subAgentList != null && subAgentList.size() > 0) {
			setSubagentList(subAgentList);
		}

		List<WalletKYCBean> userList = service.getKycList(reportBean);
		request.setAttribute("userL", userList);
		return "success";

	}

	public String editProfile() {
		String statusflag = request.getParameter("statusflag");
		if (statusflag != null && !statusflag.isEmpty() && statusflag.equalsIgnoreCase("true")) {
			userStatusList.put("A", "Active");
			/* userStatusList.put("D", "Inactive"); */
			userStatusList.put("S", "Suspended");
			request.setAttribute("statusflag", "true");
		}

		WalletMastBean mastBean = service.editProfile(walletBean);
		if (mastBean != null) {
			setWalletBean(mastBean);
		}
		User user = (User) session.get("User");
		user.setName(mastBean.getName());
		session.put("User", user);
		request.setAttribute("editMsg", "Profile updated successfully.");
		return "success";
	}

	public String saveProfilePic() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		User user = (User) session.get("User");
		WalletMastBean mastBean = null;
		if (user != null && user.getId() != null) {
			mastBean = service.showUserProfile(user.getId());
		}
		if (mastBean != null) {
			setWalletBean(mastBean);
			// "success";
		}

		if (upload.getMyFile1() == null || upload.getMyFile1FileName() == null
				|| upload.getMyFile1ContentType() == null) {
			addActionError("Please select a profile picture.");
			return "success";
		} else {
			try {
				if (user.getSubAggregatorId() != null && !user.getSubAggregatorId().isEmpty()) {
					walletBean.setUserId(user.getSubAggregatorId());
				} else {
					walletBean.setUserId(user.getId());
				}

				if (upload.getMyFile1ContentType().equalsIgnoreCase("image/png")
						|| upload.getMyFile1ContentType().equalsIgnoreCase("image/gif")
						|| upload.getMyFile1ContentType().equalsIgnoreCase("image/jpeg")) {
					walletBean.setProfilePic(encodeFileToBase64Binary(upload.getMyFile1()));

				} else {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}
				if (upload.getMyFile1().length() > 1000000) {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		String result = service.saveProfilePic(walletBean, ipAddress, userAgent);
		if (result.equalsIgnoreCase("True")) {
			addActionMessage("Profile pic updated successfully.");
		} else {
			addActionError("Please try again latter.");
		}
		return "success";
	}

	public String kycUpload() {
		generateCsrfToken();
		Map<String, String> mapResult1 = service.getKyc();
		Map<String, String> mapResult2 = service.getKycId();

		setKycOptions(mapResult1);
		setKycIdOptions(mapResult2);
		return "success";
	}

	public String uploadUserOffers() {
		generateCsrfToken();
		return "success";
	}

	public String saveUploadoffers() {
		List<MerchantOffersMast> list = new ArrayList<MerchantOffersMast>();
		MerchantOffersMast bean = new MerchantOffersMast();
		User user = (User) session.get("User");
		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("BULKCREATION");
		System.out.println("Image Location:" + filePath);
		filePath = filePath.concat("/" + user.getId());
		if (upload.getMyFile1() == null || upload.getMyFile1FileName() == null
				|| upload.getMyFile1ContentType() == null) {
			addActionError("Please select offer details excel file.");
			return "fail";
		} else {

			try {

				File fileToCreate1 = new File(filePath, upload.getMyFile1FileName());
				FileUtils.copyFile(upload.getMyFile1(), fileToCreate1);

				/*
				 * *************************************************************
				 * *************************************************************
				 * ************************
				 */

				if (upload.getMyFile1FileName().startsWith(user.getAggreatorid())
						|| upload.getMyFile1FileName().startsWith(user.getId())) {

					try {
						FileInputStream inputStream = new FileInputStream(fileToCreate1);
						Workbook workbook = new XSSFWorkbook(inputStream);
						Sheet firstSheet = workbook.getSheetAt(0);
						Iterator<Row> iterator = firstSheet.iterator();

						if (iterator.hasNext()) {
							iterator.next();
						}
						while (iterator.hasNext()) {
							bean = new MerchantOffersMast();
							Row nextRow = iterator.next();

							bean.setAggreatorId(user.getId());

							Cell cell;
							cell = nextRow.getCell(0);
							bean.setShopName(cell.getStringCellValue());

							cell = nextRow.getCell(1);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setMerchantId(str);
							} else {
								bean.setMerchantId(cell.getStringCellValue());
							}

							cell = nextRow.getCell(2);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setTerminalId(str);
							} else {
								bean.setTerminalId(cell.getStringCellValue());
							}

							cell = nextRow.getCell(3);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setCashBack(Integer.parseInt(str));
							} else {
								bean.setCashBack(Integer.parseInt(cell.getStringCellValue()));
							}

							list.add(bean);

						}
						String result = service.uploadMerchantOffer(user.getId(), list);
						if (result.equalsIgnoreCase("1000")) {
							list = service.getMerchantOffer(user.getId());
							addActionMessage(prop.getProperty("1000111"));
						} else {
							addActionError("Please try again later.");
							return "fail";
						}
						workbook.close();
						inputStream.close();
						System.out.println(bean);

					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
					}

				} else {
					addActionError("Incorrect file name.");
					return "fail";
				}

				/*
				 * *************************************************************
				 * *************************************************************
				 * ************************
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("userUploadList", list);
		return "success";
	}

	public String getMerchantOffer() {
		generateCsrfToken();
		User user = (User) session.get("User");

		List<MerchantOffersMast> list = service.getMerchantOffer(user.getId());
		request.setAttribute("userUploadList", list);

		return "success";
	}

	public String uploadUsers() {
		generateCsrfToken();
		return "success";
	}

	public String saveUploadUsers() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		WalletMastBean bean = new WalletMastBean();
		List<WalletMastBean> userUploadList = new ArrayList<WalletMastBean>();
		User user = (User) session.get("User");
		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("BULKCREATION");
		System.out.println("Image Location:" + filePath);
		filePath = filePath.concat("/" + user.getId());
		if (upload.getMyFile1() == null || upload.getMyFile1FileName() == null
				|| upload.getMyFile1ContentType() == null) {
			addActionError("Please select user details excel file.");
			return "fail";
		} else {
			try {

				File fileToCreate1 = new File(filePath, upload.getMyFile1FileName());
				FileUtils.copyFile(upload.getMyFile1(), fileToCreate1);

				/*
				 * *************************************************************
				 * *************************************************************
				 * ************************
				 */

				if (upload.getMyFile1FileName().startsWith(user.getAggreatorid())
						|| upload.getMyFile1FileName().startsWith(user.getId())) {

					try {
						FileInputStream inputStream = new FileInputStream(fileToCreate1);
						Workbook workbook = new XSSFWorkbook(inputStream);
						Sheet firstSheet = workbook.getSheetAt(0);
						Iterator<Row> iterator = firstSheet.iterator();
						int cellcount = 0;
						if (iterator.hasNext()) {
							Row nextRow = iterator.next();
							Iterator<Cell> cellIterator = nextRow.cellIterator();
							while (cellIterator.hasNext()) {
								cellIterator.next();
								cellcount++;
							}
						}
						int count = 1;
						while (iterator.hasNext()) {
							bean = new WalletMastBean();
							Row nextRow = iterator.next();

							// Iterator<Cell> cellIterator =
							// nextRow.cellIterator();

							bean.setCreatedby(user.getId());
							if (user.getUsertype() == 4) {
								bean.setAggreatorid(user.getId());
							} else {
								bean.setAggreatorid(user.getAggreatorid());
							}

							Cell cell = nextRow.getCell(0);
							cell = nextRow.getCell(1);
							bean.setName(cell.getStringCellValue());

							cell = nextRow.getCell(2);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setMobileno(str);
							} else {
								bean.setMobileno(cell.getStringCellValue());
							}

							cell = nextRow.getCell(3);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setEmailid(str);
							} else {
								bean.setEmailid(cell.getStringCellValue());
							}

							cell = nextRow.getCell(4);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								String str = NumberToTextConverter.toText(cell.getNumericCellValue());
								bean.setUsertype(Integer.parseInt(str));
							} else {
								bean.setUsertype(Integer.parseInt(cell.getStringCellValue()));
							}

							cell = nextRow.getCell(5);
							if (cell != null) {
								String distributor = cell.getStringCellValue();
								bean.setDistributerid(distributor);
							} else {
								bean.setDistributerid("-1");
							}

							cell = nextRow.getCell(6);
							if (cell != null) {
								String agent = cell.getStringCellValue();
								bean.setAgentid(agent);
							} else {
								bean.setAgentid("-1");
							}
							cell = nextRow.getCell(7);

							if (cell != null) {
								String subAgent = cell.getStringCellValue();
								bean.setSubAgentId(subAgent);
							} else {
								bean.setSubAgentId("-1");
							}
							cell = nextRow.getCell(8);

							cell = nextRow.getCell(9);

							cell = nextRow.getCell(10);
							if (cell != null)
								bean.setAddress1(cell.getStringCellValue());
							cell = nextRow.getCell(11);
							if (cell != null)
								bean.setAddress2(cell.getStringCellValue());
							cell = nextRow.getCell(12);
							if (cell != null)
								bean.setState(cell.getStringCellValue());
							cell = nextRow.getCell(13);
							if (cell != null)
								bean.setCity(cell.getStringCellValue());
							cell = nextRow.getCell(14);
							if (cell != null)
								bean.setPin(cell.getStringCellValue());

							String result = service.signUpUserByUpload(bean, ipAddress, userAgent);

							System.out.println("result " + result);
							if (result.equalsIgnoreCase("1000")) {
								bean.setUserstatus("User created successfully.");

							} else if (result.equalsIgnoreCase("7001")) {
								bean.setUserstatus(prop.getProperty("7001"));
							} else if (result.equalsIgnoreCase("7002")) {
								bean.setUserstatus(prop.getProperty("7002"));
							} else if (result.equalsIgnoreCase("7038")) {
								bean.setUserstatus(prop.getProperty("7038"));
							} else if (result.equalsIgnoreCase("7041")) {
								bean.setUserstatus(prop.getProperty("7041"));
							} else if (result.equalsIgnoreCase("7040")) {
								bean.setUserstatus(prop.getProperty("7040"));
							} else if (result.equalsIgnoreCase("7000")) {
								bean.setUserstatus(prop.getProperty("7000"));
							} else {
								bean.setUserstatus("There is some problem please try again.");
							}
							bean.setId("" + count++);
							userUploadList.add(bean);
						}

						workbook.close();
						inputStream.close();

					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
					}

				} else {
					addActionError("Incorrect file name.");
					return "fail";
				}

				/*
				 * *************************************************************
				 * *************************************************************
				 * ************************
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("userUploadList", userUploadList);
		return "success";
	}

	public String saveKycUpload() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User user = (User) session.get("User");

		Map<String, String> mapResult1 = service.getKyc();
		Map<String, String> mapResult2 = service.getKycId();

		setKycOptions(mapResult1);
		setKycIdOptions(mapResult2);
		if (upload == null || upload.getUserId() == null) {
			addActionError("An error has occurred.");
			return "fail";
		}
		if (upload.getAddpkycid() == -1) {
			addActionError("Please select your address proof type.");
			return "fail";
		}

		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("KYC");
		System.out.println("Image Location:" + filePath);
		File dir = new File(filePath, upload.getUserId());
		boolean mkdir = dir.mkdir();
		filePath = filePath.concat("/" + upload.getUserId());

		if (upload.getMyFile1() == null || upload.getMyFile1FileName() == null
				|| upload.getMyFile1ContentType() == null) {
			addActionError("Please select your address proof.");
			return "fail";
		} else {
			try {

				File fileToCreate1 = new File(filePath, upload.getMyFile1FileName());
				FileUtils.copyFile(upload.getMyFile1(), fileToCreate1);

				upload.setAddpkycpic(encodeFileToBase64Binary(upload.getMyFile1()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (upload.getIdpkycid() == -1) {
			addActionError("Please select your id proof type.");
			return "fail";
		}
		if (upload.getMyFile2() == null || upload.getMyFile2FileName() == null
				|| upload.getMyFile2ContentType() == null) {
			addActionError("Please select your id proof.");
			return "fail";
		} else {
			try {

				File fileToCreate2 = new File(filePath, upload.getMyFile2FileName());
				FileUtils.copyFile(upload.getMyFile2(), fileToCreate2);
				upload.setIdpkycpic(encodeFileToBase64Binary(upload.getMyFile2()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		upload.setUserName(user.getName());
		upload.setMobileNo(user.getMobileno());
		upload.setEmailId(user.getEmailid());
		String result = service.saveKycDocuments(upload, ipAddress, userAgent);
		if (result.equalsIgnoreCase("1000")) {
			upload = new UploadKyc();
			addActionMessage("Successfully uploaded.");
			return "success";
		}
		if (result.equalsIgnoreCase("1001")) {
			addActionError(prop.getProperty("DU1001"));
			return "fail";
		}
		if (result.equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("7000"));
			return "fail";
		}
		// System.out.println(upload.getMyFile1());
		// System.out.println(upload.getMyFileFileName1());
		// System.out.println(upload.getMyFileContentType1());
		return "fail";
	}

	public String askMoney() {
		return "success";
	}

	public String resetPassword() {
		generateCsrfToken();
		return "success";
	}

	public String saveResetPassword() {
		if (walletBean == null) {
			addActionError("An error has occurred.");
			return "success";
		}
		if (walletBean != null && walletBean.getOldpassword() == null || walletBean.getOldpassword().isEmpty()) {
			addActionError("Please enter current password.");
			return "success";
		}
		if (walletBean != null && walletBean.getPassword() == null || walletBean.getPassword().isEmpty()) {
			addActionError("Please enter new password.");
			return "success";
		}
		if (walletBean != null && walletBean.getConfirmPassword() == null
				|| walletBean.getConfirmPassword().isEmpty()) {
			addActionError("Please enter confirm new password.");
			return "success";
		}
		if (!walletBean.getPassword().equals(walletBean.getConfirmPassword())) {
			addActionError("New password and confirm new password must be same");
			return "success";
		}
		if (walletBean.getOldpassword().equals(walletBean.getPassword())) {
			addActionError("Current Password and New Password can not be same.");
			return "success";
		}
		User u = (User) session.get("User");
		if (u.getSubAggregatorId() != null && !u.getSubAggregatorId().isEmpty()) {
			walletBean.setUserId(u.getSubAggregatorId());
		}
		String result = service.resetPassword(walletBean);
		if (result.equalsIgnoreCase("1000")) {
			if (u.getUsertype() == 1) {
				session.remove("User");
				addActionMessage("Your password has been reset. Please login again.");
				request.setAttribute("login", "loginpopup");
				return "successCust";
			} else {
				addActionMessage("Your password has been reset. Please login again.");
				session.remove("User");

				return "successPass";
			}
		}
		if (result.equalsIgnoreCase("1001")) {
			addActionError(prop.getProperty("10011"));
			return "success";
		}
		if (result.equalsIgnoreCase("7013")) {
			// addActionError("Current Password didn't match or New Password is
			// one of the previous 5 used passwords.");
			addActionError(prop.getProperty("70131"));
			return "success";

		}
		if (result.equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("7000"));
			return "success";
		}
		if (result.equalsIgnoreCase("7702")) {
			addActionError(prop.getProperty("7702"));
			return "success";
		}
		if (result.equalsIgnoreCase("7703")) {
			addActionError(prop.getProperty("7703"));
			return "success";
		} else if (result.equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "success";
		}
		return "success";
	}

	public String prepaidService() {
		User user = (User) session.get("Userr");
		// if(user==null||user.getEmailid()==null||user.getEmailid().isEmpty()){
		// return "login";
		// }
		return "success";
	}

	private String encodeFileToBase64Binary(File file) throws IOException {

		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}

	private static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

	public String saveWishList() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		wishBean = service.saveWishList(wishBean, ipAddress, userAgent);
		if (wishBean.getStatus() != null && wishBean.getStatus().equalsIgnoreCase("1000")) {
			addActionMessage("This transaction summary is added to wish list.");
			// response.getWriter().print("This transaction summary is added to
			// wish list.");
			// return "success";
		}
		if (wishBean.getStatus() != null && wishBean.getStatus().equalsIgnoreCase("1001")) {

			// return "success";
		}
		if (wishBean.getStatus() != null && wishBean.getStatus().equalsIgnoreCase("7000")) {
			// return "success";
		}
		try {
			response.getWriter().print("This transaction summary is added to wish list.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String wishList() {
		generateCsrfToken();
		User user = (User) session.get("User");
		wishBean.setUserId(user.getId());
		List<UserWishListBean> list = service.getWishList(wishBean);
		request.setAttribute("wishList", list);
		return "success";
	}

	public String deleteWishList() {

		String result = service.deleteWishList(wishBean);
		if (result.equalsIgnoreCase("TRUE")) {
			addActionMessage("Record deleted successfully.");
		} else {
			addActionError("An error has occurred please try again.");
		}
		User user = (User) session.get("User");
		wishBean.setUserId(user.getId());
		List<UserWishListBean> list = service.getWishList(wishBean);
		request.setAttribute("wishList", list);

		return "success";
	}

	public String walletConfiguration() {
		setPtxnList(ptxnList);
		setEmailvalidList(emailvalidList);
		setKycvalidList(kycvalidList);
		setOtpsendtomailList(otpsendtomailList);
		setSmssendList(smssendList);
		setReqAgentApprovalList(reqAgentApprovalList);
		setCountryCode(service.getCountry());
		setPcountryCode(service.getCountryCode());
		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		Map<String, String> txnTypelist = service.getTxnTypeDtl();
		if (txnTypelist != null) {
			setTxnTypeList(txnTypelist);
		}
		WalletConfiguration conf = new WalletConfiguration();
		String aggId = (String) session.get("aggId");
		conf.setAggreatorid(aggId);
		config = service.getConfiguration(conf);
		request.setAttribute("themes", config.getThemes());
		return "success";
	}

	public String getWConfig() {
		setPtxnList(ptxnList);
		setEmailvalidList(emailvalidList);
		setKycvalidList(kycvalidList);
		setOtpsendtomailList(otpsendtomailList);
		setSmssendList(smssendList);
		setReqAgentApprovalList(reqAgentApprovalList);
		setCountryCode(service.getCountry());
		setPcountryCode(service.getCountryCode());
		Map<String, String> aggrigatorList = service.getAggreator();
		Map<String, String> txnTypelist = service.getTxnTypeDtl();
		if (txnTypelist != null) {
			setTxnTypeList(txnTypelist);
		}
		// String aggId=(String)session.get("aggId");
		// config.setAggreatorid(aggId);
		config = service.getConfiguration(config);
		request.setAttribute("themes", config.getThemes());
		return "success";
	}

	public String emailConfiguration() {
		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		String aggId = (String) session.get("aggId");
		mconfig.setAggreatorid(aggId);
		mconfig = service.emailConfiguration(mconfig);
		return "success";
	}

	public String getEConfig() {

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}
		// String aggId=(String)session.get("aggId");
		// mconfig.setAggreatorid(aggId);
		mconfig = service.emailConfiguration(mconfig);
		return "success";

	}

	public String saveEmailConfiguration() {
		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (mconfig == null) {
			addActionError("Invalid request");
			return "success";
		}
		if (mconfig.getHosturl() == null || mconfig.getHosturl().isEmpty() || mconfig.getHosturl().contains(" ")) {
			addActionError("Please enter valid host URL.");
			return "success";
		}
		if (mconfig.getFrom_mail() == null || mconfig.getFrom_mail().isEmpty()
				|| mconfig.getFrom_mail().contains(" ")) {
			addActionError("Please enter valid from email.");
			return "success";
		}
		if (mconfig.getFromname() == null || mconfig.getFromname().isEmpty() || mconfig.getFromname().contains(" ")) {
			addActionError("Please enter valid from name.");
			return "success";
		}
		if (mconfig.getUsername() == null || mconfig.getUsername().isEmpty() || mconfig.getUsername().contains(" ")) {
			addActionError("Please enter valid username");
			return "success";
		}
		if (mconfig.getPassword() == null || mconfig.getPassword().isEmpty() || mconfig.getPassword().contains(" ")) {
			addActionError("Please enter valid password.");
			return "success";
		}
		if (mconfig.getPort() == null || mconfig.getPort().isEmpty() || mconfig.getPassword().contains(" ")) {
			addActionError("Please enter valid port number");
			return "success";
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		mconfig = service.saveMailConfiguration(mconfig, ipAddress, userAgent);

		if (mconfig.getActionResult().equalsIgnoreCase("1000")) {
			addActionMessage("Email configuration updated successfully.");
		} else {
			addActionError("An error has occurred.");
		}
		return "success";
	}

	public String smsConfiguration() {

		User user = (User) session.get("User");
		if (user.getUsertype() == 99) {
			usertypeList.put("4", "Aggregator");
			usertypeList.put("2", "Agent");
		}

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		String aggId = (String) session.get("aggId");
		sconfig.setAggreatorid(aggId);
		sconfig = service.smsConfiguration(sconfig);
		return "success";
	}

	public String getSConfig() {

		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		// String aggId=(String)session.get("aggId");
		// sconfig.setAggreatorid(aggId);
		sconfig = service.smsConfiguration(sconfig);
		return "success";

	}

	public String saveSMSConfiguration() {
		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		if (sconfig == null) {
			addActionError("Invalid request");
			return "success";
		}
		if (sconfig.getSmsurl() == null || sconfig.getSmsurl().isEmpty() || sconfig.getSmsurl().contains(" ")) {
			addActionError("Please enter valid SMS URL.");
			return "success";
		}
		if (sconfig.getSmsfeedid() == null || sconfig.getSmsfeedid().isEmpty()
				|| sconfig.getSmsfeedid().contains(" ")) {
			addActionError("Please enter valid SMS feedid.");
			return "success";
		}
		if (sconfig.getSmsusername() == null || sconfig.getSmsusername().isEmpty()
				|| sconfig.getSmsusername().contains(" ")) {
			addActionError("Please enter valid SMS username.");
			return "success";
		}
		if (sconfig.getSmssenderid() == null || sconfig.getSmssenderid().isEmpty()
				|| sconfig.getSmssenderid().contains(" ")) {
			addActionError("Please enter valid SMS senderid.");
			return "success";
		}
		if (sconfig.getSmspassword() == null || sconfig.getSmspassword().isEmpty()
				|| sconfig.getSmspassword().contains(" ")) {
			addActionError("Please enter valid SMS password.");
			return "success";
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		sconfig = service.saveSMSConfiguration(sconfig, ipAddress, userAgent);

		if (sconfig.getActionResult().equalsIgnoreCase("1000")) {
			addActionMessage("SMS configuration updated successfully.");
		} else {
			addActionError("An error has occurred.");
		}
		return "success";
	}

	public String saveWalletConfiguration() {
		setPtxnList(ptxnList);
		setEmailvalidList(emailvalidList);
		setKycvalidList(kycvalidList);
		setOtpsendtomailList(otpsendtomailList);
		setSmssendList(smssendList);
		setReqAgentApprovalList(reqAgentApprovalList);
		setCountryCode(service.getCountry());
		setPcountryCode(service.getCountryCode());
		Map<String, String> aggrigatorList = service.getAggreator();
		if (aggrigatorList != null && aggrigatorList.size() > 0) {
			setAggrigatorList(aggrigatorList);
		}

		Map<String, String> txnTypelist = service.getTxnTypeDtl();
		if (txnTypelist != null) {
			setTxnTypeList(txnTypelist);
		}

		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("LOGOANDBANNER");
		System.out.println("Image Location:" + filePath);
		File dir = new File(filePath, config.getAggreatorid());
		boolean mkdir = dir.mkdir();
		filePath = filePath.concat("/" + config.getAggreatorid());

		if (config.getLogo() != null || config.getLogoFileName() != null || config.getLogoContentType() != null) {
			try {

				if ((config.getLogoContentType().equalsIgnoreCase("image/png")
						|| config.getLogoContentType().equalsIgnoreCase("image/gif")
						|| config.getLogoContentType().equalsIgnoreCase("image/jpeg"))
						&& (config.getLogoFileName().toLowerCase().endsWith(".png")
								|| config.getLogoFileName().toLowerCase().endsWith(".gif")
								|| config.getLogoFileName().toLowerCase().endsWith(".jpeg"))
						&& config.getLogo().length() <= 1000000) {
					File fileToCreate1 = new File(filePath, config.getLogoFileName());
					FileUtils.copyFile(config.getLogo(), fileToCreate1);
					// config.setLogopic(encodeFileToBase64Binary(config.getLogo()));
					config.setLogopic("./LOGOANDBANNER/" + config.getAggreatorid() + "/" + config.getLogoFileName());

				} else {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}
				if (config.getLogo().length() > 1000000) {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					config.setLogopic("");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (config.getBanner() != null || config.getBannerFileName() != null || config.getBannerContentType() != null) {
			try {
				if ((config.getBannerContentType().equalsIgnoreCase("image/png")
						|| config.getBannerContentType().equalsIgnoreCase("image/gif")
						|| config.getBannerContentType().equalsIgnoreCase("image/jpeg"))
						&& config.getBanner().length() <= 1000000) {

					File fileToCreate2 = new File(filePath, config.getBannerFileName());
					FileUtils.copyFile(config.getBanner(), fileToCreate2);
					// config.setBannerpic(encodeFileToBase64Binary(config.getBanner()));
					config.setBannerpic(
							"./LOGOANDBANNER/" + config.getAggreatorid() + "/" + config.getBannerFileName());
				} else {
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					config.setLogopic("");
					return "success";
				}
				if (config.getBanner().length() > 1000000) {
					config.setBannerpic("");
					addActionError(
							"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
					return "success";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		if (config.getDomainName() == null || config.getDomainName().isEmpty()
				|| config.getDomainName().contains(" ")) {
			addActionError("Please enter valid Domain URL.");
			return "success";
		}
		request.setAttribute("themes", config.getThemes());
		if (config.getGcmAppKey() == null || config.getGcmAppKey().isEmpty() || config.getGcmAppKey().contains(" ")) {
			addActionError("Please enter valid GCM APP Key.");
			return "success";
		}

		if (config.getPgEncKey() == null || config.getPgEncKey().isEmpty() || config.getPgEncKey().contains(" ")) {
			addActionError("Please enter valid Payment Gateway Encription Key.");
			return "success";
		}
		if (config.getTxnType() == null || config.getTxnType().length == 0) {
			addActionError("Please select atleast one transaction type.");
			return "success";
		}
		if (config.getPgUrl() == null || config.getPgUrl().isEmpty() || config.getPgUrl().contains(" ")) {
			addActionError("Please enter valid Payment Gateway URL.");
			return "success";
		}

		if (config.getPgCallBackUrl() == null || config.getPgCallBackUrl().isEmpty()
				|| config.getPgCallBackUrl().contains(" ")) {
			addActionError("Please enter valid Payment Gateway Call Back URL.");
			return "success";
		}

		if (config.getPgAgId() == null || config.getPgAgId().isEmpty() || config.getPgAgId().contains(" ")) {
			addActionError("Please enter valid Payment Gateway Agent Id.");
			return "success";
		}

		if (config.getPgMid() == null || config.getPgMid().isEmpty() || config.getPgMid().contains(" ")) {
			addActionError("Please enter valid Payment Gateway MID.");
			return "success";
		}

		config = service.saveWalletConfiguration(config, ipAddress, userAgent);
		if (config.getActionResult().equalsIgnoreCase("1000")) {

			addActionMessage("Wallet configuration updated successfully.");
		} else {
			addActionError("An error has occurred.");
			WalletConfiguration conf = new WalletConfiguration();
			conf.setAggreatorid("0");
			config = service.getConfiguration(conf);

		}

		return "success";
	}

	public String newAgents() {
		User u = (User) session.get("User");

		List<WalletMastBean> agentList = service.getPendingAgentByAggId(u.getId());
		if (agentList == null) {
			agentList = new ArrayList<WalletMastBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String acceptAgentByAgg() {

		walletBean.setId(user.getUserId());
		service.acceptAgentByAgg(walletBean);

		User u = (User) session.get("User");

		List<WalletMastBean> agentList = service.getPendingAgentByAggId(u.getId());
		if (agentList == null) {
			agentList = new ArrayList<WalletMastBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String agentdetailsview() {
		// walletBean.aggreatorid
		AgentDetailsView av = service.agentdetailsview(walletBean);
		request.setAttribute("agentDetails", av);
		return "success";
	}

	public String rejectAgentByAgg() {

		System.out.println(
				"*****************getDeclinedComment***********************" + walletBean.getDeclinedComment());
		service.rejectAgentByAgg(user.getUserId(), walletBean.getDeclinedComment());
		User u = (User) session.get("User");

		List<WalletMastBean> agentList = service.getPendingAgentByAggId(u.getId());
		if (agentList == null) {
			agentList = new ArrayList<WalletMastBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response = arg0;
	}

	public String customerProfile() {
		User u = (User) session.get("User");
		List<CustomerProfileBean> customerList = service.getCustomerProfile(u.getAggreatorid());
		request.setAttribute("customerList", customerList);
		return "success";
	}

	public String changeMobileNo() {
		System.out.println("******************************************************************"
				+ request.getParameter("newMobileno"));
		System.out.println(
				"******************************************************************" + request.getParameter("otp"));
		String type = "Error";
		String msg = "";
		if (request.getParameter("otp") == null || request.getParameter("otp").isEmpty()) {
			try {
				JSONObject jObject = new JSONObject();

				jObject.put("type", "Error");
				jObject.put("msg", "OTP can not be empty.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
			return null;
		}

		User u = (User) session.get("User");
		String status = service.changeMobileNo(u.getId(), u.getAggreatorid(),
				request.getParameter("newMobileno").toString(), request.getParameter("otp"));
		if (status.equalsIgnoreCase("True")) {
			type = "Success";
			msg = "Mobile Number Updated Successfully.";
		} else {
			type = "Error";
			msg = "Please Provide valid OTP.";
		}

		try {
			JSONObject jObject = new JSONObject();

			jObject.put("type", type);
			jObject.put("msg", msg);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;
	}

	public String validateChangeMobile() {

		String type = "Error";
		String msg = "";
		// System.out.println("******************************************************************"+walletBean.getNewMobileno());
		// System.out.println("******************************************************************"+request.getParameter("newMobileno"));
		// System.out.println("******************************************************************"+request.getAttribute("newMobileno"));
		session.put("changeMobile", request.getParameter("newMobileno"));
		if (request.getParameter("newMobileno") == null || request.getParameter("newMobileno").isEmpty()) {
			try {
				JSONObject jObject = new JSONObject();

				jObject.put("type", "Error");
				jObject.put("msg", "Mobile Number can not be empty.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
			return null;
		}

		User u = (User) session.get("User");
		if (u.getSubAggregatorId() != null && !u.getSubAggregatorId().isEmpty()) {
			u.setId(u.getSubAggregatorId());
		}

		String status = service.validateChangeMobile(u.getId(), u.getAggreatorid(),
				request.getParameter("newMobileno"));

		if (status.equalsIgnoreCase("7000")) {
			msg = prop.getProperty("7000");
		}
		if (status.equalsIgnoreCase("1001")) {
			msg = prop.getProperty("100111");
		}
		if (status.equalsIgnoreCase("7040")) {
			msg = prop.getProperty("7040");
		}
		if (status.equalsIgnoreCase("7049")) {
			msg = prop.getProperty("7049");
		}
		if (status.equalsIgnoreCase("7002")) {
			msg = prop.getProperty("7002");
		}
		if (status.equalsIgnoreCase("7034")) {
			msg = prop.getProperty("7034");
		}
		if (status.equalsIgnoreCase("7051")) {
			msg = prop.getProperty("7051");
		}

		if (status.equalsIgnoreCase("1000")) {
			type = "Success";
			msg = "OTP has been sent on your new mobile number.";
		}

		try {
			JSONObject jObject = new JSONObject();

			jObject.put("type", type);
			jObject.put("msg", msg);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;

	}

	public String changeEmailId() {
		System.out.println("******************************************************************"
				+ request.getParameter("newEmailId"));
		System.out.println(
				"******************************************************************" + request.getParameter("otp"));
		String type = "Error";
		String msg = "";
		if (request.getParameter("otp") == null || request.getParameter("otp").isEmpty()) {
			try {
				JSONObject jObject = new JSONObject();

				jObject.put("type", "Error");
				jObject.put("msg", "One time verification can not be empty.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
			return null;
		}

		User u = (User) session.get("User");
		if (u.getSubAggregatorId() != null && !u.getSubAggregatorId().isEmpty()) {
			u.setId(u.getSubAggregatorId());
		}
		String status = service.changeEmailId(u.getId(), u.getAggreatorid(),
				request.getParameter("newEmailId").toString(), request.getParameter("otp"));
		if (status.equalsIgnoreCase("True")) {
			type = "Success";
			msg = "Email Id  Updated Successfully.";
		} else {
			type = "Error";
			msg = "Please Provide valid OTP.";
		}

		try {
			JSONObject jObject = new JSONObject();

			jObject.put("type", type);
			jObject.put("msg", msg);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;
	}

	public String validateChangeEmail() {

		String type = "Error";
		String msg = "";
		System.out.println(
				"******************************************************************" + walletBean.getNewMobileno());
		System.out.println("******************************************************************"
				+ request.getParameter("newEmailId"));
		System.out.println("******************************************************************"
				+ request.getAttribute("newEmailId"));
		session.put("changeEmail", request.getParameter("newEmailId"));
		if (request.getParameter("newEmailId") == null || request.getParameter("newEmailId").isEmpty()) {
			try {
				JSONObject jObject = new JSONObject();

				jObject.put("type", "Error");
				jObject.put("msg", "Email Id can not be empty.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
			return null;
		}

		User u = (User) session.get("User");
		if (u.getSubAggregatorId() != null && !u.getSubAggregatorId().isEmpty()) {
			u.setId(u.getSubAggregatorId());
		}
		String status = service.validateChangeEmail(u.getId(), u.getAggreatorid(), request.getParameter("newEmailId"));

		if (status.equalsIgnoreCase("7000")) {
			msg = prop.getProperty("7000");
		}
		if (status.equalsIgnoreCase("1001")) {
			msg = prop.getProperty("100101");
		}
		if (status.equalsIgnoreCase("7041")) {
			msg = prop.getProperty("7041");
		}
		if (status.equalsIgnoreCase("7049")) {
			msg = prop.getProperty("7049");
		}
		if (status.equalsIgnoreCase("7001")) {
			msg = prop.getProperty("7001");
		}
		if (status.equalsIgnoreCase("7034")) {
			msg = prop.getProperty("7034");
		}
		if (status.equalsIgnoreCase("7052")) {
			msg = prop.getProperty("7052");
		}

		if (status.equalsIgnoreCase("1000")) {
			type = "Success";
			msg = "OTP has been sent on your new Email Id.";
		}

		try {
			JSONObject jObject = new JSONObject();

			jObject.put("type", type);
			jObject.put("msg", msg);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;

	}

	public String changeOtpResend() {
		System.out.println("******************************************************************"
				+ request.getParameter("newEmailMobile"));
		String type = "Error";
		String msg = "";

		User u = (User) session.get("User");
		if (u.getSubAggregatorId() != null && !u.getSubAggregatorId().isEmpty()) {
			u.setId(u.getSubAggregatorId());
		}
		String status = service.changeOtpResend(u.getId(), u.getAggreatorid(),
				request.getParameter("newEmailMobile").toString());
		if (status.equalsIgnoreCase("True")) {
			type = "Success";
			msg = "OTP sent successfully.";
		} else {
			type = "Error";
			msg = "Please Try Again.";
		}

		try {
			JSONObject jObject = new JSONObject();

			jObject.put("type", type);
			jObject.put("msg", msg);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;
	}

	public String customerValidateOTP() {

		WalletBean walletbean = new WalletBean();
		String otp = new String();
		String aggId = (String) session.get("aggId");
		walletbean.setAggreatorid(aggId);
		if (session.get("custId") != null) {
			walletbean.setUserId((String) session.get("custId"));

		} else {
			addActionError("An error has occurred please try again.");
			return "fail";
		}
		if (request.getParameter("otp") != null) {
			otp = (String) request.getParameter("otp");
		}
		if (otp != null && !otp.isEmpty()) {
			walletbean.setOtp(otp);
			LoginResponse loginResp = service.customerValidateOTP(walletbean);
			if (loginResp.getStatusCode().equalsIgnoreCase("1000")) {
				UserSummary cuser = new UserSummary();
				cuser.setUserId((String) session.get("custId"));
				cuser.setPassword((String) session.get("custPwd"));
				cuser.setAggreatorid(aggId);
				;

				session.remove("custId");
				session.remove("custPwd");
				/*
				 * 
				 * 
				 * requestPayload.put("userId",user.getUserId());
				 * requestPayload.put("password",user.getPassword());
				 * requestPayload.put("imeiIP","null");
				 * requestPayload.put("aggreatorid",user.getAggreatorid());
				 * 
				 * Map<String, String> resultMap=service.ProfilebyloginId(user);
				 * 
				 * if(Double.parseDouble(String.valueOf(resultMap.get("usertype" )))==4){
				 * config.setAggreatorid(resultMap.get("id")); }else{
				 * config.setAggreatorid(resultMap.get("aggreatorid")); }
				 * 
				 * 
				 * Map<String,String> config1=service.getWalletConfig(config);
				 * 
				 * if(config1==null||config1.size()==0){ addActionError(
				 * "An error has occurred please try again."); return "fail"; } User user=new
				 * User();
				 * 
				 * String userImg=service.getProfilePic(resultMap.get("id"));
				 * user.setUserImg(userImg); user.setEmailid(resultMap.get("emailid"));
				 * user.setFinalBalance(Double.parseDouble(String.valueOf(
				 * resultMap.get("finalBalance")))); user.setId(resultMap.get("id"));
				 * user.setMobileno(resultMap.get("mobileno"));
				 * user.setName(resultMap.get("name"));
				 * user.setWalletid(resultMap.get("walletid"));
				 * user.setUsertype(Double.parseDouble(String.valueOf(resultMap.
				 * get("usertype")))); user.setAgentid(resultMap.get("agentid"));
				 * user.setSubAgentId(resultMap.get("subAgentId"));
				 * user.setDistributerid(resultMap.get("distributerid"));
				 * user.setAggreatorid(resultMap.get("aggreatorid"));
				 * 
				 * user.setCountry(config1.get("country"));
				 * user.setCountrycurrency(config1.get("countrycurrency"));
				 * user.setCountryid(Double.parseDouble(String.valueOf(config1.
				 * get("countryid"))));
				 * user.setEmailvalid(Double.parseDouble(String.valueOf(config1.
				 * get("emailvalid"))));
				 * user.setKycvalid(Double.parseDouble(String.valueOf(config1.
				 * get("kycvalid")))); user.setOtpsendtomail(Double.parseDouble(String.valueOf(
				 * config1.get("otpsendtomail"))));
				 * user.setSmssend(Double.parseDouble(String.valueOf(config1.get ("smssend"))));
				 * user.setStatus(Double.parseDouble(String.valueOf(config1.get( "status"))));
				 * user.setLogo(config1.get("logopic"));
				 * user.setBanner(config1.get("bannerpic"));
				 * session.put("logo",config1.get("logopic"));
				 * session.put("banner",config1.get("bannerpic"));
				 * 
				 * session.put("User",user); if(user.getUsertype()==1){
				 * session.put("registration", "true"); return "customer"; } return "success";
				 */

				String userAgent = request.getHeader("User-Agent");

				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}

				LoginResponse loginResponse = service.login(cuser, ipAddress, userAgent);
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("1010")) {
					// otp send to user
					if (session.get("custType") != null && ((String) session.get("custType")).equals("1")) {
						session.put("loginStatus", "first");
						request.getSession().putValue("custId", cuser.getUserId());
						return "custOtpVal";
					}
					return "otpvalidation";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("1011")) {

					/*
					 * if(!checkLoggedinUser(this.user)){ addActionError(
					 * "You have already logged into Bhartipay from another machine. Do you want to reset the session?  <a href='#' onClick='removeSession(\""
					 * +this.user.getUserId()+ "\")' title='Remove Duplication Session'>Yes</a>");
					 * return "fail"; }
					 */

					// user successfully login
					logger.debug("******************calling ProfilebyloginId(user) service ***********************");
					Map<String, String> resultMap = service.ProfilebyloginId(cuser);
					if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4) {
						config.setAggreatorid(resultMap.get("id"));
					} else {
						config.setAggreatorid(resultMap.get("aggreatorid"));
					}

					/*************************
					 * for menu options
					 ***************************/
					logger.debug("******************calling getPlanDtl() service ***********************");
					commBean.setAggreatorid(config.getAggreatorid());
					Map<String, HashMap<String, String>> res = commService.getPlanDtl(commBean);
					if (resultMap != null) {
						setPlans(res.get("PLAN"));
						setTxnType(res.get("TXNTYPE"));
					}

					session.put("menuMap", res.get("TXNTYPE"));
					/*************************
					 * end menu options
					 ***************************/

					logger.debug("******************calling getWalletConfig() service***********************");
					Map<String, String> config1 = service.getWalletConfig(config);
					if (config1 == null || config1.size() == 0) {
						addActionError("An error has occurred please try again.");

						session.put("loginStatus", "fail");

						return "fail";
					}
					ReportBean rBean = new ReportBean();
					rBean.setUserId(resultMap.get("id"));
					if (rBean.getUserId() != null && Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 1
							&& Double.parseDouble(String.valueOf(resultMap.get("usertype"))) != 99) {
						session.put("commission", new CommissionServiceImpl().getCommission(rBean));
					}
					User user = new User();
					logger.debug("******************calling getProfilePic() service***********************");
					String userImg = service.getProfilePic(resultMap.get("id"));

					user.setUserImg(userImg);
					user.setEmailid(resultMap.get("emailid"));
					user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
					user.setCashBackfinalBalance(
							Double.parseDouble(String.valueOf(resultMap.get("cashBackFinalBalance"))));
					user.setId(resultMap.get("id"));
					user.setMobileno(resultMap.get("mobileno"));
					user.setName(resultMap.get("name"));
					user.setWalletid(resultMap.get("walletid"));
					user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
					user.setAgentid(resultMap.get("agentid"));
					user.setSubAgentId(resultMap.get("subAgentId"));
					user.setDistributerid(resultMap.get("distributerid"));
					user.setSuperdistributerid(
							resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
					user.setAggreatorid(resultMap.get("aggreatorid"));
					user.setToken(loginResponse.getToken());
					user.setTokenSecurityKey(loginResponse.getTokenSecurityKey());
					user.setKycStatus(resultMap.get("kycStatus"));
					user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
					user.setWhiteLabel(
							Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));
					if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6
							|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 4
							|| Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 3) {
						menuMapping.setUserId(user.getId());
						menuMapping = service.getUserMenuMapping(menuMapping);
						if (menuMapping != null && menuMapping.getMenuType() != null) {
							List<String> aggMenus = Arrays.asList(menuMapping.getMenuType());
							session.put("aggMenus", aggMenus);
						}

						roleMapping.setUserId(user.getId());
						roleMapping = service.getUserRoleMapping(roleMapping);
						if (roleMapping != null && roleMapping.getRoleType() != null) {
							List<String> aggRoles = Arrays.asList(roleMapping.getRoleType());
							session.put("aggRoles", aggRoles);
						}

						if (Double.parseDouble(String.valueOf(resultMap.get("usertype"))) == 6) {
							user.setSubAggregatorId(user.getId());
							user.setId(user.getAggreatorid());
						}

					}
					user.setCountry(config1.get("country"));
					user.setCountrycurrency(config1.get("countrycurrency"));
					user.setCountryid(Double.parseDouble(String.valueOf(config1.get("countryid"))));
					user.setEmailvalid(Double.parseDouble(String.valueOf(config1.get("emailvalid"))));
					user.setKycvalid(Double.parseDouble(String.valueOf(config1.get("kycvalid"))));
					user.setOtpsendtomail(Double.parseDouble(String.valueOf(config1.get("otpsendtomail"))));
					user.setSmssend(Double.parseDouble(String.valueOf(config1.get("smssend"))));
					user.setStatus(Double.parseDouble(String.valueOf(config1.get("status"))));
					user.setLogo(config1.get("logopic"));
					user.setBanner(config1.get("bannerpic"));
					session.put("logo", config1.get("logopic"));
					user.setCustomerCare(
							config1.get("customerCare") != null ? config1.get("customerCare") : "0120-4000004");
					user.setSupportEmailId(config1.get("supportEmailId") != null ? config1.get("supportEmailId") : "");
					session.put("banner", config1.get("bannerpic"));
					session.put("User", user);
					if (user.getUsertype() == 1) {
						session.put("loginStatus", "success");
						TxnInputBean WToWMoneyBean = (TxnInputBean) session.get("WToWMoneyBean");
						if (WToWMoneyBean != null && WToWMoneyBean.getTrxAmount() != 0) {
							return "walletTowallet";
						}
						B2CMoneyTxnMast wToB = (B2CMoneyTxnMast) session.get("b2cMoneyTxnMast");
						if (wToB != null && wToB.getAmount() != 0) {
							return "walletToBank";
						}
						RechargeTxnBean rtb = (RechargeTxnBean) session.get("rechargeDetails");
						if (rtb != null && rtb.getRechargeNumber() != null && !rtb.getRechargeNumber().isEmpty()) {
							generateCsrfToken();
							return "recharge";
						}
						BillDetails billdetail = (BillDetails) session.get("BillDetails");
						if (billdetail != null && billdetail.getBillerId() != 0)
							return "customer";
						TxnInputBean inputBean = (TxnInputBean) session.get("inputBean");
						if (inputBean != null && inputBean.getTrxAmount() != 0) {
							return "addMoneyB2C";
						}
						return "input";
					}
					System.out.println("during login " + resultMap);

					if (user.getUsertype() == 99) {

						List<TxnReportByAdmin> list = rService.getTxnReportByAdmin();
						request.setAttribute("txnList", list);
					}
					return "success";

				}

				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7000")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7000"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7010")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7010"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7011")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7011"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7012")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7012"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7039")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7039"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				if (loginResponse.getStatusCode() != null && !loginResponse.getStatusCode().isEmpty()
						&& loginResponse.getStatusCode().equalsIgnoreCase("7050")) {
					request.setAttribute("login", "loginpopup");
					addActionError(prop.getProperty("7050"));

					session.put("loginStatus", "fail");

					return "fail";
				}
				request.setAttribute("login", "loginpopup");
				System.out.println("result is " + loginResponse.getStatusCode());
				return "input";

			} else {
				addActionError("Please enter valid OTP.");
				return "fail";
			}
		} else {
			addActionError("Please enter a valid OTP.");
			return "fail";
		}

	}

	public String getcurrencyByCountryId() {

		if (config == null || config.getCountryid() == 0) {

		}
		String result = service.getcurrencyByCountryId(config);
		if (result != null) {
			try {
				response.getWriter().print(result);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return null;
	}

	public String getcurrencyByCountryCode() {

		String result = service.getcurrencyByCountryCode(config);
		System.out.println(result);
		if (result != null) {
			try {
				response.getWriter().print(result);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return null;
	}

	public WalletMastBean createUserObject(Sheet s) {
		WalletMastBean bean = new WalletMastBean();

		return bean;
	}

	public String assignSubAggTask() {
		generateCsrfToken();
		User us = (User) session.get("User");
		setAggMenuList(service.getMenu());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));

		return "success";
	}

	public String saveUserMenuMapping() {
		User us = (User) session.get("User");
		setAggMenuList(service.getMenu());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));
		if (menuMapping.getUserId().equalsIgnoreCase("-1")) {
			addActionError("Please select Sub-aggregator.");
			return "success";
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		menuMapping = service.saveUserMenuMapping(menuMapping, ipAddress, userAgent);
		if (menuMapping.getActionResult().equalsIgnoreCase("1000")) {
			addActionMessage("Role asign successfully.");
		} else {
			addActionError("Role asign failed please try again later.");
		}
		return "success";
	}

	public String getUserMenuMapping() {
		User us = (User) session.get("User");
		setAggMenuList(service.getMenu());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));
		menuMapping = service.getUserMenuMapping(menuMapping);
		return "success";
	}

	public String paymnetCardRequest() {
		User user = (User) session.get("User");
		pcuser.setFirst_name(user.getName());
		pcuser.setEmail(user.getEmailid());
		pcuser.setMobile(user.getMobileno());
		return "success";
	}

	public String savePCUser() {
		System.out.println(
				"___________________________++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		pcuser = service.pcRequest(pcuser);
		return "success";
	}

	public String openSmartCard() {
		generateCsrfToken();
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		smartCardBean = service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardBean);
		return "success";
	}

	public String updatePrePaidCard() {
		generateCsrfToken();
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		smartCardBean = service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardBean);

		SmartCardBean smartcb = service.updatePrePaidCard(smartCardBean);
		if (smartcb == null) {
			request.setAttribute("flag", "false");
			addActionError("Oops something went wrong please try again.");
			return "fail";
		}
		if (smartcb.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Prepaid card details updated successfully.");
			request.setAttribute("smartCardList", smartcb);
			return "success";
		} else {
			addActionError(smartcb.getStatusDesc());
			request.setAttribute("flag", "false");
			return "fail";
		}

	}

	public String updateAddress() {
		generateCsrfToken();
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		// smartCardBean=service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardBean);
		request.setAttribute("updateAddress", "true");
		SmartCardBean smartcb = service.updateAddress(smartCardBean);
		if (smartcb == null) {
			request.setAttribute("flag", "false");
			request.setAttribute("smartCardList", service.getPrePaidCard(smartCardBean));
			addActionError("Oops something went wrong please try again.");
			return "fail";
		}
		if (smartcb.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Prepaid card address updated successfully.");
			smartCardBean = service.getPrePaidCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardBean);
			return "success";
		} else if (smartcb.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError(smartcb.getStatusDesc());
			request.setAttribute("smartCardList", service.getPrePaidCard(smartCardBean));
			request.setAttribute("flag", "false");
			return "fail";
		} else {
			request.setAttribute("flag", "false");
			request.setAttribute("smartCardList", service.getPrePaidCard(smartCardBean));
			addActionError("Oops something went wrong please try again.");
			return "fail";
		}
	}

	public String updatePassword() {
		generateCsrfToken();
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		// smartCardBean=service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardBean);
		request.setAttribute("updatePassword", "true");
		SmartCardBean smartcb = service.updatePassword(smartCardBean);
		if (smartcb == null || smartcb.getStatusCode().equalsIgnoreCase("7000")) {
			request.setAttribute("flag", "false");
			request.setAttribute("smartCardList", service.getPrePaidCard(smartCardBean));
			addActionError("Oops something went wrong please try again.");
			return "fail";
		}
		if (smartcb.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Prepaid card password updated successfully.");
			smartCardBean = service.getPrePaidCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardBean);
			return "success";
		} else {
			addActionError(smartcb.getStatusDesc());
			request.setAttribute("smartCardList", service.getPrePaidCard(smartCardBean));
			request.setAttribute("flag", "false");
			return "fail";
		}
	}

	public String linkCard() {
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		SmartCardBean smartCardList1 = service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList1);
		smartCardBean = service.linkCard(smartCardBean);

		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card linked successfully.");

			smartCardBean.setAggreatorId(user.getAggreatorid());
			smartCardList1 = service.getPrePaidCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardList1);
			return "success";
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
			// addActionError(prop.getProperty("100111"));
			addActionError(smartCardBean.getStatusDesc());
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("100111"));
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("9901")) {
			addActionError(prop.getProperty("9901"));
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("9902")) {
			addActionError(prop.getProperty("9902"));
		}

		return "success";
	}

	public String blockCard() {
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		SmartCardBean smb = smartCardBean;
		smartCardBean = service.linkCardBlocked(smartCardBean);
		SmartCardBean smartCardList1 = service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList1);

		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card blocked successfully.");
			smartCardBean.setAggreatorId(user.getAggreatorid());
			smartCardList1 = service.getPrePaidCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardList1);
			return "success";
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
			smartCardList1 = service.getPrePaidCard(smb);
			request.setAttribute("smartCardList", smartCardList1);
			addActionError("Card blocking fail. Please try again.");
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
			smartCardList1 = service.getPrePaidCard(smb);
			request.setAttribute("smartCardList", smartCardList1);
			addActionError("Card blocking fail. Please try again.");
		}
		return "success";
	}

	public String blockCardByAggregator() {
		User user = (User) session.get("User");
		smartCardBean = service.linkCardBlocked(smartCardBean);
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList1 = service.getdispatchCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList1);

		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card block successfully.");

			smartCardBean.setAggreatorId(user.getAggreatorid());
			smartCardList1 = service.getdispatchCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardList1);
			return "success";
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
			addActionError("Card blocking fail. Please try again.");
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError("Card blocking fail. Please try again.");
		}
		return "success";
	}

	public String cardBlockByAggregator() {
		smartCardBean = service.linkCardBlocked(smartCardBean);

		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getSmartCardList(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card block successfully.");
			smartCardBean.setAggreatorId(user.getAggreatorid());
			return "success";
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
			addActionError("Card blocking fail. Please try again.");
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError("Card blocking fail. Please try again.");
		}
		return "success";
	}

	public String generateSmartCard() {
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		smartCardBean.setWalletId(user.getWalletid());
		smartCardBean.setAggreatorId(user.getAggreatorid());
		smartCardBean.setMobileNo(user.getMobileno());
		smartCardBean.setEmailId(user.getEmailid());
		// smartCardBean.setName(user.getName());
		smartCardBean.setCountryCode(((Double) user.getCountryid()).intValue());
		if (!new PasswordValidator().validate(smartCardBean.getPassowrd())) {
			addActionError("Password should be alphanumeric and length 8-20.");
			return "success";
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		smartCardBean = service.generateSmartCard(smartCardBean, ipAddress, userAgent);
		if (smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {

			addActionMessage("Prepaid Card Request Generated with Ref. No.:" + smartCardBean.getReqId());
			smartCardBean = new SmartCardBean();
		} else if (smartCardBean.getStatusCode().equalsIgnoreCase("5001")) {
			addActionMessage("Your Prepaid Card Request is Pending.");
		} else if (smartCardBean.getStatusCode().equalsIgnoreCase("5002")) {
			addActionMessage("Your Prepaid Card Request is Accepted.");
		} else {
			addActionError("There is some problem please try again.");
		}

		return "success";
	}

	public String dispatchSmartCard() {
		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getdispatchCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String dispatchSmartCardRequest() {

		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList1 = service.getdispatchCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList1);

		smartCardBean.getPrePaidId();
		SmartCardBean smb = service.dispatchCard(smartCardBean);
		if (smb != null && smb.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card dispatched successfully.");

			smartCardBean.setAggreatorId(user.getAggreatorid());
			smartCardList1 = service.getdispatchCard(smartCardBean);
			request.setAttribute("smartCardList", smartCardList1);
			return "success";
		}
		if (smb != null && smb.getStatusCode().equalsIgnoreCase("1001")) {
			addActionError(prop.getProperty("100111"));
		}
		if (smb != null && smb.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("100111"));
		}

		return "success";
	}

	public String linkVirtualCard() {

		User user = (User) session.get("User");
		smartCardBean = service.linkVirtualCard(smartCardBean);
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Card linked successfully.");
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
			addActionError(smartCardBean.getStatusDesc());
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("100111"));
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("9901")) {
			addActionError(prop.getProperty("9901"));
		}
		if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("9902")) {
			addActionError(prop.getProperty("9902"));
		}
		SmartCardBean smart = new SmartCardBean();
		smart.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getdispatchCard(smart);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String getSmartCard() {
		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getSmartCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String smartCardList() {
		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getSmartCardList(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String rejectSmartCard() {
		System.out.println("*************************getReqId***************************" + smartCardBean.getReqId());
		if (smartCardBean.getReqId() == null || smartCardBean.getReqId() == null) {
			addActionError("There is some problem please try again.");
			return "fail";
		}
		String status = service.rejectSmartCard(smartCardBean);

		if (Boolean.parseBoolean(status)) {
			addActionMessage("Prepaid Card Rejected for Ref. Number  " + smartCardBean.getReqId());
		} else {
			addActionError("Prepaid Card Not Rejected, Please Try Again.");
		}

		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getSmartCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String acceptSmartCard() {
		System.out.println("*************************getReqId***************************" + smartCardBean.getReqId());
		if (smartCardBean.getReqId() == null || smartCardBean.getReqId() == null) {
			addActionError("There is some problem please try again.");
			return "fail";
		}
		SmartCardOutputBean smartCardBeanres = service.acceptSmartCard(smartCardBean);
		if (smartCardBeanres.getStatusCode() != null && smartCardBeanres.getStatusCode().equalsIgnoreCase("1001")) {
			addActionError(smartCardBeanres.getPrePaidError());
		} else if (smartCardBeanres.getStatusCode() != null
				&& smartCardBeanres.getStatusCode().equalsIgnoreCase("10000")) {
			addActionMessage(
					"Physical Prepaid Card accepted  and sending to generate card with prepaid card  Ref. Number  "
							+ smartCardBeanres.getPrePaidUserId());
		} else if (smartCardBeanres.getStatusCode() != null
				&& smartCardBeanres.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage(
					"Virtual Prepaid Card accepted  and sending to generate card with prepaid card  Ref. Number  "
							+ smartCardBeanres.getPrePaidId());

		} else if (smartCardBeanres.getStatusCode() != null
				&& smartCardBeanres.getStatusCode().equalsIgnoreCase("6789")) {
			addActionMessage("Wallet created but card not link due to " + smartCardBeanres.getPrePaidError());
		}
		User user = (User) session.get("User");
		smartCardBean.setAggreatorId(user.getAggreatorid());
		List<SmartCardBean> smartCardList = service.getSmartCard(smartCardBean);
		request.setAttribute("smartCardList", smartCardList);
		return "success";
	}

	public String myAccount() {
		return "success";
	}

	public String prePaidCard() {
		return "success";
	}

	public String walletBalance() {
		return "success";
	}

	public String insurance() {
		return "success";
	}

	public String healthAndWelness() {
		return "success";
	}

	public String ticketingAndBooking() {
		return "success";
	}

	public String offers() {
		try {
			List<Coupons> coupons = service.getCoupons("");
			request.setAttribute("coupons", coupons);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	public String getCouponsCategory() {
		List<CouponsBean> couponList = service.getCouponsByCategory(couponsBean);
		Gson gson = new Gson();
		try {
			response.getWriter().println(gson.toJson(couponList));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendCoupons() {
		String[] cpnids = couponsBean.getCpnids();
		String ids = "";
		if (cpnids != null) {
			for (String cp : cpnids) {
				ids = ids + "," + cp;
			}
		}
		ids = ids.substring(1, ids.length());
		couponsBean.setIds(ids);
		String result = service.mailCoupan(couponsBean);
		addActionMessage("Selected coupons have been sent to your registerd email id.");
		return "success";
	}

	public String authoriseAggregator() {
		List<WalletMastBean> aggList = service.getUnAuthorizedAggreator();
		request.setAttribute("aggList", aggList);
		return "success";
	}

	public String saveAuthoriseAggregator() {
		List<WalletMastBean> aggList = service.getUnAuthorizedAggreator();
		request.setAttribute("aggList", aggList);
		String result = service.authorizedAggreator(walletBean);
		if (result != null && result.equalsIgnoreCase("1000")) {
			aggList = service.getUnAuthorizedAggreator();
			request.setAttribute("aggList", aggList);
			addActionMessage("Authorisation success mail has been sent as confirmation.");
			return "success";
		} else if (result != null && result.equalsIgnoreCase("1001")) {
			addActionError(prop.getProperty("6000"));
			return "success";
		} else if (result != null && result.equalsIgnoreCase("7000")) {
			addActionError(prop.getProperty("6000"));
			return "success";
		} else if (result != null && result.equalsIgnoreCase("6000")) {
			addActionError(prop.getProperty("6000"));
			return "success";
		} else if (result != null && result.equalsIgnoreCase("6001")) {
			addActionError(prop.getProperty("6001"));
			return "success";
		} else if (result != null && result.equalsIgnoreCase("6002")) {
			addActionError(prop.getProperty("6002"));
			return "success";
		} else if (result != null && result.equalsIgnoreCase("6003")) {
			addActionError(prop.getProperty("6003"));
			return "success";
		}
		return "success";
	}

	public String fetchingCreatedUserdetail() {
		String reqId = request.getParameter("reqId");
		logger.debug("************************************fetchingCreatedUserdetail() starting");
		try {
			User us = (User) session.get("User");
			logger.debug("******************fetchingCreatedUserdetail() service result start***********************");
			String result = service.fetchingCreatedUserdetail(reqId);
			Gson gson = new Gson();
			DmtUserDetails dmtUser = gson.fromJson(result, DmtUserDetails.class);
			request.setAttribute("userDetail", dmtUser);
		} catch (Exception e) {
			logger.debug("******************fetchingCreatedUserdetail() problem***********************");
			return "success";
		}
		return "success";
	}

	public String fetchingWalletdetail() {
		String reqId = request.getParameter("reqId");
		logger.debug("************************************fetchingWalletdetail() starting");
		try {
			User us = (User) session.get("User");
			logger.debug("******************fetchingWalletdetail() service result start***********************");
			String result = service.fetchingWalletdetail(reqId);
			System.out.println(result + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			Gson gson = new Gson();
			DmtUserDetails dmtUser = gson.fromJson(result, DmtUserDetails.class);
			request.setAttribute("userDetail", dmtUser);
		} catch (Exception e) {
			logger.debug("******************fetchingWalletdetail() problem***********************");
			return "success";
		}
		return "success";
	}

	public String fetchingCardType() {
		String reqId = request.getParameter("reqId");
		logger.debug("************************************fetchingCardType() starting");
		try {
			User us = (User) session.get("User");
			logger.debug("******************fetchingCardType() service result start***********************");
			String result = service.fetchingCardType(reqId);
			System.out.println(result + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			Gson gson = new Gson();
			DmtUserDetails dmtUser = gson.fromJson(result, DmtUserDetails.class);
			request.setAttribute("userDetail", dmtUser);
		} catch (Exception e) {
			logger.debug("******************fetchingCardType() problem***********************");
			return "success";
		}
		return "success";
	}

	public String fetchingCardTypeCode() {
		String reqId = request.getParameter("reqId");
		logger.debug("************************************fetchingCardTypeCode() starting");
		try {
			User us = (User) session.get("User");
			logger.debug("******************fetchingCardTypeCode() service result start***********************");
			String result = service.fetchingCardTypeCode(reqId);
			System.out.println(result + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			Gson gson = new Gson();
			DmtUserDetails dmtUser = gson.fromJson(result, DmtUserDetails.class);
			request.setAttribute("userDetail", dmtUser);
		} catch (Exception e) {
			logger.debug("******************fetchingCardTypeCode() problem***********************");
			return "success";
		}
		return "success";
	}

	public String suspendedCard() {
		/*
		 * String userAgent=request.getHeader("User-Agent"); String ipAddress =
		 * request.getRemoteAddr(); if (ipAddress == null) { ipAddress =
		 * request.getHeader("X-FORWARDED-FOR"); }
		 */
		String reqId = request.getParameter("reqId");
		logger.debug("************************************suspendedCard() starting");
		try {
			smartCardBean = service.suspendedCard(reqId);
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
				response.getWriter().print("Card suspended successfully.");
				return null;
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
				response.getWriter().print("Card suspending fail. Please try again.");
				return null;
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
				response.getWriter().print("Card suspending fail. Please try again.");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String resumePrePaidCard() {
		String reqId = request.getParameter("reqId");
		smartCardBean = service.resumePrePaidCard(reqId);
		logger.debug("************************************resumePrePaidCard() starting");
		try {
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
				response.getWriter().print("Card resuming successfully.");
				return null;
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
				response.getWriter().print("Card resuming fail please try again.");
				return null;
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
				response.getWriter().print("Card resuming fail please try again.");
				return null;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String changePin() {
		// String reqId = request.getParameter("reqId");
		System.out.println("smartcardbean" + smartCardBean.getChangePinMode());
		smartCardBean = service.pinReset(smartCardBean);
		// smartCardBean=service.resumePrePaidCard(smartCardList());User
		// user=(User)session.get("User");
		User user = (User) session.get("User");
		smartCardBean.setUserId(user.getId());
		SmartCardBean smart = service.getPrePaidCard(smartCardBean);
		request.setAttribute("smartCardList", smart);

		logger.debug("************************************changePin() starting");
		try {
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")) {
				addActionMessage("PIN has been sent to your registered email.");
				return "success";
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1001")) {
				addActionMessage("Oops something went wrong.");
				return "fail";
			}
			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("7000")) {
				addActionMessage(smartCardBean.getStatusDesc());
				return "fail";

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";
	}

	public String generateCVV() {
		logger.debug("************************************generateCVV() starting" + smartCardBean.getReqId());
		try {
			smartCardBean = service.generateCVV(smartCardBean.getReqId());

			if (smartCardBean != null && smartCardBean.getStatusCode().equalsIgnoreCase("1000")
					&& smartCardBean.getCvv() != null && !smartCardBean.getCvv().isEmpty()) {
				addActionMessage("Your card CVV is : " + smartCardBean.getCvv());
				User user = (User) session.get("User");
				smartCardBean.setUserId(user.getId());
				smartCardBean = service.getPrePaidCard(smartCardBean);
				request.setAttribute("smartCardList", smartCardBean);
				return "success";
			}

			else {
				addActionError(smartCardBean.getStatusDesc());
				User user = (User) session.get("User");
				smartCardBean.setUserId(user.getId());
				smartCardBean = service.getPrePaidCard(smartCardBean);
				request.setAttribute("smartCardList", smartCardBean);
				return "fail";

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";
	}

	public boolean generateCsrfToken() {
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>) session
				.get("csrfPreventionSaltCache");

		if (csrfPreventionSaltCache == null) {
			csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000)
					.expireAfterWrite(180, TimeUnit.MINUTES).build();

			session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
		}

		// Generate the salt and store it in the users cache
		String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
		csrfPreventionSaltCache.put(salt, Boolean.TRUE);
		session.put("csrfPreventionSalt", salt);
		return true;
	}

	// **************************** code edited by amardeep : start
	// **********************

	public String getUserDetails() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getUserDetails() starting");
		try {
			Gson gs = new Gson();
			User us = (User) session.get("User");

			user.setUserId(us.getId());
			user.setAggreatorid(us.getAggreatorid());
			Map<String, String> resultMap = service.ProfilebyloginId(user);

			us.setEmailid(resultMap.get("emailid"));
			us.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
			us.setCashBackfinalBalance(Double.parseDouble(String.valueOf(resultMap.get("cashBackFinalBalance"))));
			us.setId(resultMap.get("id"));
			us.setMobileno(resultMap.get("mobileno"));
			us.setName(resultMap.get("name"));
			us.setWalletid(resultMap.get("walletid"));
			us.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
			us.setAgentid(resultMap.get("agentid"));
			us.setSubAgentId(resultMap.get("subAgentId"));
			us.setDistributerid(resultMap.get("distributerid"));
			us.setSuperdistributerid(
					resultMap.get("superdistributerid") != null ? resultMap.get("superdistributerid") : "-1");
			us.setAggreatorid(resultMap.get("aggreatorid"));
			us.setKycStatus(resultMap.get("kycStatus"));
			us.setIsimps(Integer.parseInt(resultMap.get("isimps")));
			us.setWhiteLabel(Integer.parseInt(resultMap.get("whiteLabel") != null ? resultMap.get("whiteLabel") : "0"));
			us.setWallet(Integer.parseInt(resultMap.get("wallet")));
			us.setBank(Integer.parseInt(resultMap.get("bank")));

			session.put("User", us);

			String jsonString = gs.toJson(us);
			logger.debug(serverName + "******************getUserDetails() service result start***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getUserDetails() problem***********************");
			return null;
		}
		return null;
	}

	public String validateSender() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************validateSender() action starting");
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		mastBean.setAgentId(us.getId());
		mastBean.setAggreatorId(us.getAggreatorid());
		logger.debug(serverName + "******************validateSender() service start***********************");
		mastBean = service.validateSender(mastBean, ipAddress, userAgent);
		if (mastBean != null && mastBean.getStatusCode().equalsIgnoreCase("1000")) {
			session.put("senderId", mastBean.getId());
		}
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mastBean);
			logger.debug(serverName + "******************validateSender() service result start***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************validateSender() problem***********************");

		}
		return null;
	}

	
	
	public String validateSenderMobile() {
		 
		logger.debug("************************************validateSenderMobile() starting");
	    String mob = request.getParameter("mobileNo");
	    String str1 = request.getParameter("mastBean.mobileNo");
		User us = (User) session.get("User");
		mastBean.setMobileNo(mob);
		mastBean.setAggreatorId(us.getAggreatorid());
		mastBean = service.validateSenderMobile(mastBean);
		if (mastBean != null && mastBean.getStatusCode().equalsIgnoreCase("1000")) {
			session.put("senderId", mastBean.getId());
		}
		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mastBean);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug("******************registerSenderobile() problem***********************");

		}
		return null;
	}
	
	
	
	public String registerSender() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************registerSender() starting");
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		mastBean.setAggreatorId(us.getAggreatorid());
		mastBean.setAgentId(us.getId());
		logger.debug(serverName + "******************registerSender() service start***********************");
		mastBean = service.registerSender(mastBean, ipAddress, userAgent);
		if (mastBean != null && mastBean.getStatusCode().equalsIgnoreCase("1000")) {
			session.put("senderId", mastBean.getId());
		}
		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mastBean);
			logger.debug(serverName + "******************registerSender() service result start***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************registerSender() problem***********************");

		}
		return null;
	}

	public String dmtOtpResend() {

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************dmtOtpResend() starting");
		generateCsrfToken();
		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		user.setMobileno(mastBean.getMobileNo());
		user.setUserId(mastBean.getMobileNo());
		JSONObject jObject = new JSONObject();
		logger.debug(serverName + "******************dmtOtpResend() service start***********************");
		String result = service.dmtOtpResend(user);
		logger.debug(
				serverName + "******************dmtOtpResend() service result start***********************" + result);
		if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {
			jObject.put("status", "true");
		} else {
			jObject.put("status", "false");
		}
		try {

			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************dmtOtpResend() problem***********************");

		}
		return null;

	}

	public String getBankDetails() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getBankDetails() starting");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mBean = gson.fromJson(data, MudraBeneficiaryMastBean.class);
		bankDetail.setBeneficiaryAccountNo(mBean.getAccountNo());
		bankDetail.setBankName(mBean.getBankName());
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getBankDetails() service start***********************");
		bankDetail = service.getBankDetails(bankDetail, ipAddress, userAgent);
		try {

			Gson gs = new Gson();

			String jsonString = gs.toJson(bankDetail);
			logger.debug(serverName + "******************getBankDetails() service result start***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getBankDetails() problem***********************");

		}
		return null;
	}

	public String getBankDetailsByIFSC() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getBankDetailsByIFSC() starting");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		bankDetail = gson.fromJson(data, BankDetailsBean.class);
		// bankDetail.setIfscCode(mBean.getIfscCode());

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getBankDetailsByIFSC() service start***********************");
		List<BankDetailsBean> bankDetailList = service.getBankDetailsByIFSC(bankDetail, ipAddress, userAgent);
		try {

			Gson gs = new Gson();

			String jsonString = gs.toJson(bankDetailList);
			logger.debug(serverName
					+ "******************getBankDetailsByIFSC() service result start***********************");
			JSONParser jsonParser = new JSONParser();
			JSONArray jObject = (JSONArray) jsonParser.parse(jsonString);

			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getBankDetailsByIFSC() problem***********************");

		}
		return null;
	}

	public String getBankDetailsByIFSCB2C() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getBankDetailsByIFSCB2C() starting");
		// "ifscCode="+ ifcs
		// +"&bankName="+bankName+"&branchName="+branchName+"&state="+state
		String bankName = request.getParameter("bankName");
		String branchName = request.getParameter("branchName");
		String state = request.getParameter("state");
		String ifsc = request.getParameter("ifscCode");
		bankDetail.setIfscCode(ifsc);
		bankDetail.setBankId(bankName);
		bankDetail.setBankName(bankName);
		bankDetail.setBranchName(branchName);
		bankDetail.setState(state);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getBankDetailsByIFSCB2C() service start***********************");
		List<BankDetailsBean> bankDetailList = service.getBankDetailsByIFSC(bankDetail, ipAddress, userAgent);
		try {

			Gson gs = new Gson();

			String jsonString = gs.toJson(bankDetailList);
			logger.debug(serverName
					+ "******************getBankDetailsByIFSCB2C() service result start***********************");
			JSONParser jsonParser = new JSONParser();
			JSONArray jObject = (JSONArray) jsonParser.parse(jsonString);

			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getBankDetailsByIFSCB2C() problem***********************");

		}
		return null;
	}

	public String registerBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************registerBeneficiary() starting");
		User u = (User) session.get("User");
		System.out.println("***************************************v" + request.getParameter("formData"));
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mBean = gson.fromJson(data, MudraBeneficiaryMastBean.class);
		mBean.setAggreatorId(u.getAggreatorid());
		mBean.setSenderId((String) session.get("senderId"));
		mBean.setTransferType("IMPS");
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************registerBeneficiary() service start***********************");
		mBean = service.registerBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mBean);
			logger.debug(
					serverName + "******************registerBeneficiary() service result start***********************"
							+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************registerBeneficiary() problem***********************");

		}
		return null;
	}

	public String registerBeneficiaryBK() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************registerBeneficiary() starting");
		User u = (User) session.get("User");
		System.out.println("***************************************v" + request.getParameter("formData"));
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mBean = gson.fromJson(data, MudraBeneficiaryMastBean.class);
		String verifyAcc = mBean.getVerifyBene();
		mBean.setAggreatorId(u.getAggreatorid());
		mBean.setSenderId((String) session.get("senderId"));
		mBean.setTransferType("IMPS");

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************registerBeneficiary() service start***********************");
		mBean = service.registerBeneficiary(mBean, ipAddress, userAgent);
		session.put("bene", mBean);
		try {
			if (mBean.getStatusCode().equals("1000") && verifyAcc.trim().equals("true")) {
				MudraMoneyTransactionBean mudraMoney = new MudraMoneyTransactionBean();
				mudraMoney.setAccountNo(mBean.getAccountNo());
				mudraMoney.setAgentid(u.getId());
				mudraMoney.setBankName(mBean.getBankName());
				mudraMoney.setBeneficiaryId(mBean.getId());
				mudraMoney.setIfscCode(mBean.getIfscCode());
				mudraMoney.setName(mBean.getName());
				mudraMoney.setNarrartion("IMPS");
				mudraMoney.setSenderId(mBean.getSenderId());
				mudraMoney.setTransType("IMPS");
				mudraMoney.setTxnAmount(0);
				mudraMoney.setUserId(u.getId());
				mudraMoney.setWalletId(u.getWalletid());
				session.put("mudraMoney", mudraMoney);
				return "verify";
			} else {

				Gson gs = new Gson();
				String jsonString = gs.toJson(mBean);
				logger.debug(serverName
						+ "******************registerBeneficiary() service result start***********************"
						+ jsonString);
				JSONParser jsonParser = new JSONParser();
				JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
				jObject.put("bene", jsonString);
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			}
		} catch (Exception e) {
			logger.debug(serverName + "******************registerBeneficiary() problem***********************");

		}
		return null;
	}

	public String deleteBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************deleteBeneficiary() starting");
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		// mBean=gson.fromJson(data,MudraBeneficiaryMastBean.class);
		mBean.setId(data);
		// mBean.setAggreatorId(u.getAggreatorid());
		mBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************deleteBeneficiary() service start***********************");
		mBean = service.deleteBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mBean);
			logger.debug("******************deleteBeneficiary() result service from*********************" + jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************deleteBeneficiary() problem***********************");

		}
		return null;
	}

	public String acceptBen() {
		logger.debug("******************acceptBen() method start***********************");
		MudraMoneyTransactionBean mudraMoney = (MudraMoneyTransactionBean) session.get("mudraMoney");
		System.out.println(mudraMoney.getBeneficiaryId());
		System.out.println(mudraMoney.getBeneficiaryId());
		String userAgent = request.getHeader("User-Agent");

		mBean.setId(mudraMoney.getBeneficiaryId());
		mBean.setName(mudraMoney.getAccHolderName());
		mBean.setSenderId(mudraMoney.getSenderId());

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		logger.debug("******************acceptBen() service start***********************");
		mBean = service.acceptBeneficiary(mBean, ipAddress, userAgent);
		logger.debug("******************acceptBen() service start***********************");
		try {

			if (mBean != null && mBean.getStatusCode().equalsIgnoreCase("1000")) {
				mBean.setStatusDesc("Beneficiary added and verified successfuly.");
			} else {
				mBean.setStatusDesc("Please try again.");
			}
			Gson gs = new Gson();
			String jsonString = gs.toJson(mBean);
			logger.debug("******************deleteBeneficiary() result service from*********************" + jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug("******************deleteBeneficiary() problem***********************");

		}
		return null;
	}

	public String rejectBen() {
		logger.debug("******************rejectBean() method start***********************");
		MudraMoneyTransactionBean mudraMoney = (MudraMoneyTransactionBean) session.get("mudraMoney");
		System.out.println(mudraMoney.getBeneficiaryId());

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		mBean.setSenderId(mudraMoney.getSenderId());
		mBean.setId(mudraMoney.getBeneficiaryId());
		logger.debug("******************rejectBean() service start***********************");
		// mBean = service.rejectBeneficiary(mBean, ipAddress, userAgent);
		logger.debug("******************rejectBean() service start***********************");
		mBean.setStatusCode("1000");
		if (mBean != null && mBean.getStatusCode().equalsIgnoreCase("1000")) {
			mBean.setStatusDesc("Beneficiary added.");
		} else {
			mBean.setStatusDesc("Please try again.");
		}
		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mBean);
			logger.debug("******************deleteBeneficiary() result service from*********************" + jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug("******************deleteBeneficiary() problem***********************");

		}
		return null;
	}

	public String setFavourite() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************setFavourite() starting");
		SenderFavouriteBean fBean = new SenderFavouriteBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		fBean = gson.fromJson(data, SenderFavouriteBean.class);
		fBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************setFavourite() calling service***********************");

		fBean = service.setFavourite(fBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(fBean);
			logger.debug(serverName + "******************setFavourite() result service from*********************"
					+ jsonString);

			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************setFavourite() problem start***********************");

		}
		return null;
	}

	public String deleteFavourite() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************deleteFavourite()  action starting");
		Gson gs = new Gson();
		SenderFavouriteBean fBean = new SenderFavouriteBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		fBean = gson.fromJson(data, SenderFavouriteBean.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************deleteFavourite() service start***********************");

		fBean = service.deleteFavourite(fBean, ipAddress, userAgent);
		try {

			String jsonString = gs.toJson(fBean);
			logger.debug(serverName + "******************deleteFavourite() service result start***********************"
					+ jsonString);

			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************deleteFavourite() problem start***********************");

			return null;
		}
		return null;
	}

	 
	
	public String getFavouriteList() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getFavouriteList() starting");
		SenderFavouriteBean fBean = new SenderFavouriteBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();

		fBean.setAgentId(u.getId());
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getFavouriteList() action start***********************");
		List<SenderFavouriteViewBean> fListBean = service.getFavouriteList(fBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(fListBean);
			logger.debug(serverName + "******************getFavouriteList() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonarrary = (JSONArray) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jsonarrary);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(serverName + "******************getFavouriteList() problem***********************");
		}
		return null;
	}

	public String forgotMPIN() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************forgotMPIN() starting");
		MudraSenderMastBean mSBean = new MudraSenderMastBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, MudraSenderMastBean.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************forgotMPIN() service start***********************");
		mSBean = service.forgotMPIN(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mSBean);
			logger.debug(
					serverName + "******************forgotMPIN() service result***********************" + jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************forgotMPIN() problem***********************");

		}
		return null;
	}

	public String deletedBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************deletedBeneficiary() action start***********************");

		User u = (User) session.get("User");
		String data = request.getParameter("benId");
		Gson gson = new Gson();
		mBean.setId(data);
		mBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************deletedBeneficiary() service ***********************");
		MudraSenderMastBean msBean = service.deletedBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(msBean);
			logger.debug(serverName + "******************deletedBeneficiary() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************deletedBeneficiary() problem***********************");
		}
		return null;
	}

	public String updateMPIN() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************updateMPIN() action start***********************");
		MudraSenderMastBean mSBean = new MudraSenderMastBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, MudraSenderMastBean.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************updateMPIN() action start***********************");
		mSBean = service.updateMPIN(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mSBean);
			logger.debug(
					serverName + "******************updateMPIN() service result***********************" + jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************updateMPIN() problem***********************");

		}
		return null;
	}

	
	public String getTransacationLedgerDtlForAgg(){

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start**********data*************"
				+ data);
		Gson gson = new Gson();
//		agentList
		if(u.getUsertype()==4) {
		Map<String, String> agentList = service.getActiveAgentByAgg(u.getId());
		setAgentList(agentList);
		} else {
		Map<String, String> agentList = service.getActiveAgentByAgg(u.getId());
		setAgentList(agentList);
		}
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		// mSBean = gson.fromJson(data, DMTReportInOut.class);
		mSBean.setStDate(inputBean.getStDate());
		mSBean.setEdDate(inputBean.getEndDate());
		mSBean.setAgentId(inputBean.getUserId());
		mSBean.setStatuscode(inputBean.getStatus());
		//mSBean.setAgentId(u.getId());
		mSBean.setUsertype(u.getUsertype());
		if(u.getUsertype()==4) {
		mSBean.setAggregatorId(u.getId());
		}else {
		mSBean.setAggregatorId(u.getAggreatorid());
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		// User us = (User) session.get("User");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
//		if (inputBean != null && inputBean.getStDate() != null && inputBean.getStDate().length() > 0
//				&& inputBean.getEndDate() != null) {
			mSBean = service.getTransacationLedgerDtlForAgg(mSBean, ipAddress, userAgent);
			request.setAttribute("transactionLedger", mSBean.getTransList());
//		}
//		try {
//			Gson gs = new Gson();
//
//			String jsonString = gs.toJson(mSBean);
//			logger.debug(
//					serverName + "******************getTransacationLedgerDtl() service result***********************"
//							+ jsonString);
//			JSONParser jsonParser = new JSONParser();
//			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
//			response.setContentType("application/json");
//			response.getWriter().println(jObject);
//		} catch (Exception e) {
//			logger.debug(serverName + "******************getTransacationLedgerDtl() problem***********************");
//
//		}
		return "success";
	
	}
	
	
	
	
	
	
	public String getTransacationLedgerPayoutForAgg(){

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start**********data*************"
				+ data);
		Gson gson = new Gson();
//		agentList
		if(u.getUsertype()==4) {
		Map<String, String> agentList = service.getActiveAgentByAgg(u.getId());
		setAgentList(agentList);
		} else {
		Map<String, String> agentList = service.getActiveAgentByAgg(u.getId());
		setAgentList(agentList);
		}
		if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		}

		// mSBean = gson.fromJson(data, DMTReportInOut.class);
		mSBean.setStDate(inputBean.getStDate());
		mSBean.setEdDate(inputBean.getEndDate());
		mSBean.setAgentId(inputBean.getUserId());
		mSBean.setStatuscode(inputBean.getStatus());
		//mSBean.setAgentId(u.getId());
		mSBean.setUsertype(u.getUsertype());
		if(u.getUsertype()==4) {
		mSBean.setAggregatorId(u.getId());
		}else {
		mSBean.setAggregatorId(u.getAggreatorid());
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		// User us = (User) session.get("User");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
//		if (inputBean != null && inputBean.getStDate() != null && inputBean.getStDate().length() > 0
//				&& inputBean.getEndDate() != null) {
			mSBean = service.getTransacationLedgerPayoutForAgg(mSBean, ipAddress, userAgent);
			request.setAttribute("transactionLedger", mSBean.getTransList());
//		}
//		try {
//			Gson gs = new Gson();
//
//			String jsonString = gs.toJson(mSBean);
//			logger.debug(
//					serverName + "******************getTransacationLedgerDtl() service result***********************"
//							+ jsonString);
//			JSONParser jsonParser = new JSONParser();
//			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
//			response.setContentType("application/json");
//			response.getWriter().println(jObject);
//		} catch (Exception e) {
//			logger.debug(serverName + "******************getTransacationLedgerDtl() problem***********************");
//
//		}
		return "success";
	
	}
	
	
	
	public String getTransacationLedgerDtl() {
		
		User u = (User) session.get("User");
		if (u.getUsertype() == 3) {
			Map<String, String> agentList = service.getAgentByDistId(u.getId());
			if (agentList != null && agentList.size() > 0) {
				setAgentList(agentList);
			}
	  	}
		
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		
		String data = request.getParameter("data");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start**********data*************"
				+ data);
		Gson gson = new Gson();
		// mSBean = gson.fromJson(data, DMTReportInOut.class);
		mSBean.setStDate(inputBean.getStDate());
		mSBean.setEdDate(inputBean.getEndDate());
		mSBean.setStatuscode(inputBean.getStatus());
		mSBean.setAgentId(u.getId());
		mSBean.setUsertype(u.getUsertype());
		
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		// User us = (User) session.get("User");
		logger.debug(serverName + "******************getTransacationLedgerDtl() action start***********************");
//		if (inputBean != null && inputBean.getStDate() != null && inputBean.getStDate().length() > 0
//				&& inputBean.getEndDate() != null) {
			mSBean = service.getTransacationLedgerDtl(mSBean, ipAddress, userAgent);
			request.setAttribute("transactionLedger", mSBean.getTransList());
//		}
//		try {
//			Gson gs = new Gson();
//
//			String jsonString = gs.toJson(mSBean);
//			logger.debug(
//					serverName + "******************getTransacationLedgerDtl() service result***********************"
//							+ jsonString);
//			JSONParser jsonParser = new JSONParser();
//			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
//			response.setContentType("application/json");
//			response.getWriter().println(jObject);
//		} catch (Exception e) {
//			logger.debug(serverName + "******************getTransacationLedgerDtl() problem***********************");
//
//		}
		return "success";
	}

	public String getAgentLedgerDtl() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************getAgentLedgerDtl() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		logger.debug(
				serverName + "******************getAgentLedgerDtl() action start**********data*************" + data);
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, DMTReportInOut.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getAgentLedgerDtl() action start***********************");
		mSBean = service.getAgentLedgerDtl(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mSBean);
			logger.debug(serverName + "******************getAgentLedgerDtl() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getAgentLedgerDtl() problem***********************");

		}
		return null;
	}

	public String getSenerLedgerDtl() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************getSenerLedgerDtl() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		logger.debug(
				serverName + "******************getSenerLedgerDtl() action start**********data*************" + data);
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, DMTReportInOut.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************getSenerLedgerDtl() action start***********************");
		mSBean = service.getSenerLedgerDtl(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mSBean);
			logger.debug(serverName + "******************getSenerLedgerDtl() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getSenerLedgerDtl() problem***********************");

		}
		return null;
	}

	public String initiateDMTRefundByAgent() {

		// String mobileNo=request.getParameter("mobileNo");
		// String accountNo=request.getParameter("accountNo");
		// String id=request.getParameter("id");
		// String mpin=request.getParameter("mpin");

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************initiateDMTRefundByAgent() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		logger.debug(serverName + "******************initiateDMTRefundByAgent() action start**********data*************"
				+ data);
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, DMTReportInOut.class);

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		mSBean.setAgentId(us.getId());
		mSBean = service.initiateDMTRefundByAgent(mSBean, ipAddress, userAgent);

		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mSBean);
			logger.debug("******************calling initiateDMTRefundByAgent() service result ***********************"
					+ jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jsonString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// request.setAttribute("list", mSBean);
		return null;

	}

	public String getBeneficiaryListForImport() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(
				serverName + "******************getBeneficiaryListForImport() action start***********************");
		MudraSenderMastBean mSBean = new MudraSenderMastBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mSBean = gson.fromJson(data, MudraSenderMastBean.class);
		mSBean.setAggreatorId(u.getAggreatorid());

		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(
				serverName + "******************getBeneficiaryListForImport() service start***********************");
		mSBean = service.getBeneficiaryListForImport(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mSBean);
			logger.debug(
					serverName + "******************getBeneficiaryListForImport() service result***********************"
							+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************getBeneficiaryListForImport() problem***********************");
		}
		return null;
	}

	public String calculateSurcharge() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************calculateSurcharge() action start***********************");
		SurchargeBean mSBean = new SurchargeBean();
		User u = (User) session.get("User");
		String data = request.getParameter("data");
		Gson gson = new Gson();
		mSBean.setAmount(Double.parseDouble(data));
		mSBean.setAgentId(u.getId());
		// mSBean=gson.fromJson(data,SurchargeBean.class);

		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		User us = (User) session.get("User");
		logger.debug(serverName + "******************calculateSurcharge() service start***********************");
		mSBean = service.calculateSurcharge(mSBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mSBean);
			logger.debug(serverName + "******************calculateSurcharge() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************calculateSurcharge() problem***********************");
		}
		return null;
	}

	public String activeBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************activeBeneficiary() action start***********************");
		User u = (User) session.get("User");
		String data = request.getParameter("benId");
		Gson gson = new Gson();
		mBean.setId(data);
		mBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************activeBeneficiary() service start***********************");
		MudraSenderMastBean msBean = service.activeBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(msBean);
			logger.debug(serverName + "******************activeBeneficiary() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************activeBeneficiary() problem***********************");
		}
		return null;
	}

	public String deActiveBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************deActiveBeneficiary() action start***********************");

		User u = (User) session.get("User");
		String data = request.getParameter("benId");
		Gson gson = new Gson();
		mBean.setId(data);
		mBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		logger.debug(serverName + "******************deActiveBeneficiary() service ***********************");
		MudraSenderMastBean msBean = service.deActiveBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(msBean);
			logger.debug(serverName + "******************deActiveBeneficiary() service result***********************"
					+ jsonString);
			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName + "******************deActiveBeneficiary() problem***********************");
		}
		return null;
	}

	public String copyBeneficiary() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "******************copybeneficiary() ***********************");
		User u = (User) session.get("User");
		String data = request.getParameter("benId");
		Gson gson = new Gson();
		mBean.setId(data);
		mBean.setSenderId((String) session.get("senderId"));
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		User us = (User) session.get("User");
		logger.debug(serverName + "******************calling copyBeneficiary service ***********************");
		mBean = service.copyBeneficiary(mBean, ipAddress, userAgent);
		try {
			Gson gs = new Gson();

			String jsonString = gs.toJson(mBean);
			logger.debug(serverName + "******************calling copyBeneficiary service result ***********************"
					+ jsonString);

			JSONParser jsonParser = new JSONParser();
			JSONObject jObject = (JSONObject) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName
					+ "******************some problem occour during calling copybeneficiary() action ***********************");

		}
		return null;
	}

	public String getBankDtl() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");

		logger.debug(serverName + "************************************getBankDtl() starting");
		User us = (User) session.get("User");
		logger.debug(serverName + "******************calling getBankDtl service ***********************");
		List<BankMastBean> mBean = service.getBankDtl();
		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mBean);
			logger.debug(serverName + "******************calling getBankDtl service result ***********************"
					+ jsonString);

			JSONParser jsonParser = new JSONParser();
			JSONArray jObject = (JSONArray) jsonParser.parse(jsonString);
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName
					+ "******************some problem occour during calling getBankDtl() action ***********************");

		}
		return null;
	}

	public String fundTransferRequest() {
		String status = "Y";
		User us = (User) session.get("User");
		Gson gson = new Gson();
		JSONObject jObject = new JSONObject();
		Properties prop = ReadPropertyFile
				.readProperties(request.getServletContext().getRealPath("/configuration/BhartiPayCofig.properties"));
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");
		logger.debug(serverName + "************************************fundTransferRequest() starting");
		String data = request.getParameter("data");
		MudraBeneficiaryMastBean mudraBen = gson.fromJson(data, MudraBeneficiaryMastBean.class);
		logger.debug(serverName
				+ "******************calling fundTransferRequest() service ************getTxnAmount***********"
				+ mudraBen.getTxnAmount());

		if (us.getUsertype() != 2) {
			try {
				jObject.put("Error", "Invalid User.");
				jObject.put("status", "F");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
				return null;
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(serverName
						+ "******************some problem occour during calling fundTransferRequest() action ***********************");

			}
		}

		if (mudraBen.getTxnAmount() < 0 || mudraBen.getTxnAmount() > 25000) {
			try {
				jObject.put("Error", "Txn Amount should be less then or equal to 25000");
				jObject.put("status", "F");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
				return null;
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(serverName
						+ "******************some problem occour during calling fundTransferRequest() action ***********************");

			}
		}

		MudraBeneficiaryMastBean mudraBenr = service.getBeneficiary(mudraBen.getId());
		if (!(mudraBen != null && mudraBen.getTransferType().equalsIgnoreCase("NEFT"))) {
			String insertFlag = new com.bhartipay.wallet.user.service.impl.UserServiceImpl().getTransDtlFlag(
					us.getId() + "|" + mudraBenr.getSenderId() + "|" + mudraBen.getId() + "|" + mudraBen.getTxnAmount(),
					"ALL");
			if (!insertFlag.equalsIgnoreCase("SUCCESS")) {
				try {
					// JSONObject jObject=new JSONObject();
					jObject.put("status", "F");
					if (!insertFlag.equalsIgnoreCase("FAIL"))
						jObject.put("Error", insertFlag);
					else
						jObject.put("Error",
								"You have already processed transaction with same details please wait for 10 minutes.");
					response.setContentType("application/json");
					response.getWriter().println(jObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}
		}

		mudraBen.setName(mudraBenr.getName());
		mudraBen.setBankName(mudraBenr.getBankName());
		mudraBen.setAccountNo(mudraBenr.getAccountNo());
		mudraBen.setIfscCode(mudraBenr.getIfscCode());

		MudraMoneyTransactionBean mudraMoney = new MudraMoneyTransactionBean();
		mudraMoney.setSenderId(mudraBen.getSenderId());
		mudraMoney.setBeneficiaryId(mudraBen.getId());

		mudraMoney.setName(mudraBenr.getName());
		mudraMoney.setBankName(mudraBenr.getBankName());
		mudraMoney.setAccountNo(mudraBenr.getAccountNo());
		mudraMoney.setIfscCode(mudraBenr.getIfscCode());
		mudraMoney.setSenderId(mudraBenr.getSenderId());

		mudraMoney.setTxnAmount(mudraBen.getTxnAmount());
		mudraMoney.setWalletId(us.getWalletid());
		mudraMoney.setUserId(us.getId());
		mudraMoney.setNarrartion(mudraBen.getTransferType());
		mudraMoney.setAgentid(us.getId());
		if (mudraBen != null && mudraBen.getTransferType().equalsIgnoreCase("NEFT"))
			mudraMoney.setTransType("NEFT");
		else
			mudraMoney.setTransType("IMPS");

		MudraMoneyTransactionBean mudraMoneyResult = service.calculateSurCharge(mudraMoney, ipAddress, userAgent);
		// System.out.println(mudraMoneyResult.getSurChargeAmount()+"____________________________________***********");
		mudraMoney.setSurChargeAmount(mudraMoneyResult.getSurChargeAmount());
		mudraMoney.setAgentRefund(mudraMoneyResult.getAgentRefund());
		jObject.put("surChargeAmount", mudraMoney.getSurChargeAmount());
		jObject.put("cashBackAmount", mudraMoney.getAgentRefund());

		jObject.put("paybleAmount", mudraMoney.getSurChargeAmount() - mudraMoney.getAgentRefund());

		jObject.put("txnAmount", mudraMoney.getTxnAmount());
		jObject.put("totalAmount", "" + (mudraMoney.getSurChargeAmount() + mudraMoney.getTxnAmount()));
		if (mudraBen != null && mudraBen.getTransferType().equalsIgnoreCase("NEFT")) {
			status = "N";
		} else {

			Map<String, String> mapConfig = (Map<String, String>) session.get("mapResult");

			String message = prop.getProperty("dmt.validation");
			message = message.replace("<<charge>>", prop.getProperty("dmt.validation.charge"));
			message = message.replace("<<amount>>", prop.getProperty("dmt.validation.amount"));
			
			jObject.put("verify",mudraBenr.getVerified());
			jObject.put("message", message);
			jObject.put("note",prop.getProperty("dmt.validation.note").replace("<<Bhartipay>>", mapConfig.get("appname")));
		
		}
		session.put("mudraMoney", mudraMoney);
		session.put("mudraBen", mudraBen);
		logger.debug(serverName + "******************calling fundTransferRequest() service ***********************");

		try {
			Gson gs = new Gson();
			String jsonString = gs.toJson(mBean);
			logger.debug(serverName
					+ "******************calling fundTransferRequest() service result ***********************"
					+ jsonString);

			jObject.put("beneName", mudraBen.getName());
			jObject.put("accountNo", mudraBen.getAccountNo());
			jObject.put("ifscCode", mudraBen.getIfscCode());
			jObject.put("bankName", mudraBen.getBankName());

			jObject.put("status", status);

			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName
					+ "******************some problem occour during calling fundTransferRequest() action ***********************");

		}
		return null;
	}

	public String verifyBeneficiary() {
		User us = (User) session.get("User");
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		Gson gson = new Gson();

		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");
		logger.debug(serverName + "************************************VerifyBeneficiary() starting");
		MudraMoneyTransactionBean mudraMoney = (MudraMoneyTransactionBean) session.get("mudraMoney");
		Properties prop2 = ReadPropertyFile
				.readProperties(request.getServletContext().getRealPath("/configuration/BhartiPayCofig.properties"));
		mudraMoney.setVerificationAmount(Double.parseDouble(prop2.getProperty("dmt.validation.amount")));
		mudraMoney.setSurChargeAmount(Double.parseDouble(prop2.getProperty("dmt.validation.charge")));
		logger.debug(serverName + "******************calling VerifyBeneficiary() service ***********************");
		// mudraMoney.setTxnAmount(mudraMoney.getVerificationAmount());
		mudraMoney.setAggreatorid(us.getAggreatorid());
		MudraMoneyTransactionBean moneyTransactionBean = service.verifyAccount(mudraMoney, ipAddress, userAgent);
		try {
			JSONObject jObject = new JSONObject();
			String result = "";
			if (moneyTransactionBean != null && moneyTransactionBean.getStatusCode() != null
					&& moneyTransactionBean.getStatusCode().equalsIgnoreCase("1000")) {

				jObject = new JSONObject();
				jObject.put("result", "Y");
				mudraMoney = (MudraMoneyTransactionBean) session.get("mudraMoney");
				if (mudraMoney.getTxnAmount() >= moneyTransactionBean.getVerificationAmount()) {
//					if ("V".equalsIgnoreCase(moneyTransactionBean.getVerifyStatus())) {
						mudraMoney
								.setTxnAmount(mudraMoney.getTxnAmount() - moneyTransactionBean.getVerificationAmount());
//						mudraMoney.setVerifyStatus("V");
//					} else {
//						mudraMoney.setTxnAmount(mudraMoney.getTxnAmount());
//						mudraMoney.setVerifyStatus("NV");
//					}
				}else {
					mudraMoney.setTxnAmount(moneyTransactionBean.getVerificationAmount());
				}
				mudraMoney.setTransType("IMPS");
				mudraMoney.setSurChargeAmount(
						(service.calculateSurCharge(mudraMoney, ipAddress, userAgent)).getSurChargeAmount());
				mudraMoney.setTxnId(moneyTransactionBean.getTxnId());
				String benName = moneyTransactionBean.getAccHolderName().replace("(NV)", "");
				benName = benName.replace("(V)", "");
				mudraMoney.setAccHolderName(benName);

				session.put("mudraMoney", mudraMoney);

				jObject.put("details", gson.toJson(moneyTransactionBean));
				result = moneyTransactionBean.getStatusCode();

				jObject.put("surChargeAmount", mudraMoney.getSurChargeAmount());
				jObject.put("cashBackAmount", mudraMoney.getAgentRefund());

				jObject.put("paybleAmount", mudraMoney.getSurChargeAmount() - mudraMoney.getAgentRefund());
				jObject.put("beneName",benName );
				jObject.put("accountNo", mudraMoney.getAccountNo());
				jObject.put("ifscCode", mudraMoney.getIfscCode());
				jObject.put("bankName", mudraMoney.getBankName());
			} else {
				jObject = new JSONObject();
				jObject.put("details", gson.toJson(moneyTransactionBean));
				jObject.put("result", "N");
				result = moneyTransactionBean.getStatusCode();
				jObject.put("message", "Verification failed.");

			}
			if (result.equalsIgnoreCase("1001")) {
				jObject.put("message", prop.getProperty("100111"));

			}
			if (result.equalsIgnoreCase("7023")) {
				jObject.put("message", prop.getProperty("17023"));

			}
			if (result.equalsIgnoreCase("7014")) {
				jObject.put("message", prop.getProperty("17014"));

			}
			if (result.equalsIgnoreCase("7017")) {
				jObject.put("message", prop.getProperty("17017"));
			}
			if (result.equalsIgnoreCase("7018")) {
				jObject.put("message", prop.getProperty("17018"));
			}
			if (result.equalsIgnoreCase("7019")) {
				jObject.put("message", prop.getProperty("17019"));
			}
			if (result.equalsIgnoreCase("7020")) {
				jObject.put("message", prop.getProperty("17020"));
			}
			if (result.equalsIgnoreCase("7021")) {
				jObject.put("message", prop.getProperty("17021"));
			}
			if (result.equalsIgnoreCase("7022")) {
				jObject.put("message", prop.getProperty("17022"));
			}
			if (result.equalsIgnoreCase("7045")) {
				jObject.put("message", prop.getProperty("17045"));
			}
			if (result.equalsIgnoreCase("7046")) {
				jObject.put("message", prop.getProperty("17046"));
			}
			if (result.equalsIgnoreCase("7047")) {
				jObject.put("message", prop.getProperty("17047"));
			}
			if (result.equalsIgnoreCase("9000")) {
				jObject.put("message", prop.getProperty("9000"));

			}
			if (result.equalsIgnoreCase("7048")) {
				jObject.put("message", prop.getProperty("17048"));
			}
			if (result.equalsIgnoreCase("7000")) {
				jObject.put("message", prop.getProperty("17000"));
			}
			if (result.equalsIgnoreCase("7023")) {
				jObject.put("message", prop.getProperty("17023"));
			}
			if (result.equalsIgnoreCase("7024")) {
				jObject.put("message", prop.getProperty("17024"));
			}
			if (result.equalsIgnoreCase("7042")) {
				jObject.put("message", prop.getProperty("17042"));
			}
			jObject.put("name", mudraMoney.getName());
			jObject.put("surChargeAmount", mudraMoney.getSurChargeAmount());
			jObject.put("txnAmount", mudraMoney.getTxnAmount());
			jObject.put("bene", gson.toJson((MudraBeneficiaryMastBean) session.get("bene")));
			jObject.put("totalAmount", "" + (mudraMoney.getSurChargeAmount() + mudraMoney.getTxnAmount()));
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName
					+ "******************some problem occour during calling fundTransferRequest() action ***********************");

		}
		return null;
	}

	public String initiateDMTRefund() {

		User us = (User) session.get("User");
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		if (dmtMast != null && dmtMast.getTxnid() != null)
			dmtMast.setTxnid(dmtMast.getTxnid().replaceAll("-", ","));

		treeMap.put("userId", us.getId());
		treeMap.put("txnid", dmtMast.getTxnid());
		treeMap.put("token", us.getToken());
		try {
			String checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(us.getTokenSecurityKey(),
					treeMap);
			dmtMast.setCHECKSUMHASH(checkSumString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<RefundTransactionBean> mudraMoney = service.initiateDMTRefund(dmtMast);
		/*
		 * try { Gson gs = new Gson(); String jsonString = gs.toJson(mudraMoney);
		 * logger.debug(
		 * "******************calling initiateDMTRefund() service result ***********************"
		 * +jsonString); response.setContentType("application/json");
		 * response.getWriter().println(jsonString); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		request.setAttribute("list", mudraMoney);
		return "success";
	}

	public String markFailedDmtTxn() {

		User us = (User) session.get("User");
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		if (dmtMast != null && dmtMast.getTxnid() != null)
			dmtMast.setTxnid(dmtMast.getTxnid().replaceAll("-", ","));
		treeMap.put("userId", us.getId());
		treeMap.put("txnid", dmtMast.getTxnid());
		treeMap.put("token", us.getToken());
		try {
			String checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(us.getTokenSecurityKey(),
					treeMap);
			dmtMast.setCHECKSUMHASH(checkSumString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String status = service.markFailedDmtTxn(dmtMast);
		if (status != null && status.equalsIgnoreCase("SUCCESS")) {
			request.setAttribute("status", "Mark status as failed successfully.");
		} else {
			request.setAttribute("status", "Transaction not updated successfully.");
		}
		return "success";
	}

	public String checkDMTStatus() {
		logger.debug("******************calling checkDMTStatus()  ***********************");
		DmtDetailsMastBean dmtDetailsMast = service.checkDMTStatus(dmtMast);
		/*
		 * try {
		 * 
		 * JSONObject jobj=new JSONObject(); jobj.put("status",
		 * dmtDetailsMast.getBankresponse()); logger.debug(
		 * "******************calling checkDMTStatus() service result ***********************"
		 * +jobj); response.setContentType("application/json");
		 * response.getWriter().println(jobj); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		request.setAttribute("status", dmtDetailsMast.getBankresponse());
		return "success";
	}

	public String fundTransfer() {
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		String status = "Y";
		User us = (User) session.get("User");
		Gson gson = new Gson();
		JSONObject jObject = new JSONObject();
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");
		logger.debug(serverName + "************************************fundTransferRequest() starting");
		String data = request.getParameter("data");
		System.out.println(data);
		logger.debug(serverName + "******************calling fundtransfer() service ***********************");
		MudraMoneyTransactionBean mudraMoney = (MudraMoneyTransactionBean) session.get("mudraMoney");
		mudraMoney.setAggreatorid(us.getAggreatorid());
		/*
		 * String insertFlag=new
		 * com.appnit.bhartipay.wallet.user.service.impl.UserServiceImpl().insertTransDtl
		 * (mudraMoney.getUserId()+"|"+mudraMoney.getSenderId()+"|"
		 * +mudraMoney.getBeneficiaryId()+"|"+mudraMoney.getTxnAmount());
		 * if(!insertFlag.equalsIgnoreCase("SUCCESS")) { try{ jObject.put("status","N");
		 * if(!insertFlag.equalsIgnoreCase("FAIL")) jObject.put("message", insertFlag);
		 * else jObject.put("message",
		 * "You have already processed transaction with same details please wait for 10 minutes."
		 * ); response.setContentType("application/json");
		 * response.getWriter().println(jObject); }catch(Exception e){
		 * e.printStackTrace(); } return null; }
		 */

		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("txnAmount", "" + mudraMoney.getTxnAmount());
		treeMap.put("userId", mudraMoney.getUserId());
		treeMap.put("beneficiaryId", mudraMoney.getBeneficiaryId());
		treeMap.put("token", us.getToken());
		try {
			String checkSumString = CheckSumHelper.getCheckSumHelper().genrateCheckSum(us.getTokenSecurityKey(),
					treeMap);
			mudraMoney.setCHECKSUMHASH(checkSumString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		FundTransactionSummaryBean moneyTransactionBean = service.fundTransfer(mudraMoney, ipAddress, userAgent);

		try {
			String result = "";
			Gson gs = new Gson();
			String jsonString = gs.toJson(mBean);
			logger.debug(serverName
					+ "******************calling fundTransferRequest() service result ***********************"
					+ jsonString);
			if (moneyTransactionBean != null && moneyTransactionBean.getStatusCode() != null
					&& moneyTransactionBean.getStatusCode().equalsIgnoreCase("1000")) {

				jObject.put("status", "Y");
				jObject.put("details", gson.toJson(moneyTransactionBean));

			} else {
				jObject.put("status", "N");

				result = moneyTransactionBean.getStatusCode();
				if (moneyTransactionBean.getStatusDesc() == null || moneyTransactionBean.getStatusDesc().isEmpty()) {
					jObject.put("message", "Something went wrong please try again.");
				} else {
					jObject.put("message", moneyTransactionBean.getStatusDesc());
				}
			}
			if (result.equalsIgnoreCase("1001")) {
				jObject.put("message", prop.getProperty("100111"));

			}
			if (result.equalsIgnoreCase("7023")) {
				jObject.put("message", prop.getProperty("17023"));

			}
			if (result.equalsIgnoreCase("7014")) {
				jObject.put("message", prop.getProperty("17014"));

			}
			if (result.equalsIgnoreCase("7017")) {
				jObject.put("message", prop.getProperty("17017"));
			}
			if (result.equalsIgnoreCase("7018")) {
				jObject.put("message", prop.getProperty("17018"));
			}
			if (result.equalsIgnoreCase("7019")) {
				jObject.put("message", prop.getProperty("17019"));
			}
			if (result.equalsIgnoreCase("7020")) {
				jObject.put("message", prop.getProperty("17020"));
			}
			if (result.equalsIgnoreCase("7021")) {
				jObject.put("message", prop.getProperty("17021"));
			}
			if (result.equalsIgnoreCase("7022")) {
				jObject.put("message", prop.getProperty("17022"));
			}
			if (result.equalsIgnoreCase("7045")) {
				jObject.put("message", prop.getProperty("17045"));
			}
			if (result.equalsIgnoreCase("7046")) {
				jObject.put("message", prop.getProperty("17046"));
			}
			if (result.equalsIgnoreCase("7047")) {
				jObject.put("message", prop.getProperty("17047"));
			}
			if (result.equalsIgnoreCase("7048")) {
				jObject.put("message", prop.getProperty("17048"));
			}
			if (result.equalsIgnoreCase("7000")) {
				jObject.put("message", prop.getProperty("17000"));
			}
			if (result.equalsIgnoreCase("7023")) {
				jObject.put("message", prop.getProperty("17023"));
			}
			if (result.equalsIgnoreCase("7024")) {
				jObject.put("message", prop.getProperty("17024"));
			}
			if (result.equalsIgnoreCase("7042")) {
				jObject.put("message", prop.getProperty("17042"));
			}
			if (result.equalsIgnoreCase("8001")) {
				jObject.put("message", prop.getProperty("8001"));
			}
			if (result.equalsIgnoreCase("8002")) {
				jObject.put("message", prop.getProperty("8002"));
			}

			response.setContentType("application/json");
			jObject.put("result", "ajet");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(serverName
					+ "******************some problem occour during calling fundTransferRequest() action ***********************");

		} finally {
			// String deleteBene = new
			// com.appnit.bhartipay.wallet.user.service.impl.UserServiceImpl().deleteTransDtl(mudraMoney.getUserId()+"|"+mudraMoney.getSenderId()+"|"+mudraMoney.getBeneficiaryId()+"|"+mudraMoney.getTxnAmount());
		}
		return null;
	}

	public String getTransactionDetailsByTxnId() {

		JSONObject jObject = new JSONObject();
		Gson gson = new Gson();
		FundTransactionSummaryBean moneyTransactionBean=new FundTransactionSummaryBean();
		try {
			MudraMoneyTransactionBean mudraMoney = new MudraMoneyTransactionBean();
			mudraMoney.setTxnId(request.getParameter("txnId"));
			moneyTransactionBean = service.getTransactionDetailsByTxnId(mudraMoney, "", "");
			if (moneyTransactionBean.getStatusCode().equalsIgnoreCase("1000")) {
				jObject.put("status", "Y");
				jObject.put("details", gson.toJson(moneyTransactionBean));
			} else {
				jObject.put("status", "N");

				if (moneyTransactionBean.getStatusDesc() == null || moneyTransactionBean.getStatusDesc().isEmpty()) {
					jObject.put("message", "Something went wrong please try again.");
				} else {
					jObject.put("message", moneyTransactionBean.getStatusDesc());
				}
			}
			response.setContentType("application/json");
			jObject.put("result", "ajet");
			//response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.debug(
					"******************some problem occour during calling fundTransferRequest() action ***********************");
		}
		request.setAttribute("result", moneyTransactionBean);
		return "success";
	}

	public String passwordOtpsend() {
		generateCsrfToken();
		User u = (User) session.get("User");
		String type = "Error";
		String msg = "";
		user.setAggreatorid(u.getAggreatorid());
		user.setUserId(u.getMobileno());

		if (user != null && user.getUserId() != null && !user.getUserId().isEmpty()) {
			String status = service.otpResend(user);

			if (status.equalsIgnoreCase("True")) {
				type = "Success";
				msg = "OTP sent successfully.";
			} else {
				type = "Error";
				msg = "Please Try Again.";
			}

			try {
				JSONObject jObject = new JSONObject();

				jObject.put("type", type);
				jObject.put("msg", msg);
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
		}
		return null;
	}

	public String getFaq() {
		User user = (User) session.get("User");
		System.out.println(request.getParameter("userId"));
		String userImg = service.getProfilePic(user.getId());
		System.out.println("=============================================================dfdfsf userImg" + userImg);
		return "success";
	}

	public String declinedAgent() {
		User user = (User) session.get("User");
		System.out.println("=============================================================declinedAgent"
				+ user.getSubAggregatorId());
		// List<WalletMastBean>
		// agentList=service.getPendingAgentByAggId(u.getId());
		List<DeclinedListBean> agentList = service.getRejectAgentDetail(user.getId(), user.getSubAggregatorId());
		// public List<DeclinedListBean> getRejectAgentDetail(String
		// aggregatorId,String createdBy)
		if (agentList == null) {
			agentList = new ArrayList<DeclinedListBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String editAgentOnBoard() {
		User user = (User) session.get("User");

		Map<String, String> distributorList = service.getActiveDistributerByAggId(user.getAggreatorid());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		if (reportBean.getUserId() == null || reportBean.getUserId() == null) {
			addActionError("An error has occurred.");
			return "fail";
		}

		WalletMastBean mastBean = service.showUserProfile(reportBean.getUserId());

		if (mastBean != null) {
			setWalletBean(mastBean);
		}

		request.setAttribute("statusflag", "true");
		return "success";

	}

	public String editAgent() {
		User user = (User) session.get("User");

		Map<String, String> distributorList = service.getActiveDistributerByAggId(user.getAggreatorid());
		if (distributorList != null && distributorList.size() > 0) {
			setDistributorList(distributorList);
		}

		WalletMastBean mastBean = service.editAgent(walletBean);
		if (mastBean != null) {
			setWalletBean(mastBean);
		}
		if (mastBean.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage("Profile updated successfully.");
			return "success";
		} else {
			addActionMessage("Some problem in Profile updated.please try again.");
			return "fail";
		}
	}

	public String agentList() {

		User user = (User) session.get("User");
		List<WalletMastBean> agentList = new ArrayList<>();
		if (walletBean.getId() == null) {
			if (user.getSubAggregatorId() == null) {
				agentList = service.getAgentList(user.getId(), walletBean.getStDate(), walletBean.getEndDate());
			} else {
				agentList = service.getAgentList(user.getSubAggregatorId(), walletBean.getStDate(),
						walletBean.getEndDate());
			}

		} else {
			agentList = service.getAgentList(walletBean.getId(), walletBean.getStDate(), walletBean.getEndDate());
		}

		request.setAttribute("agentList", agentList);

		return "success";
	}

	public String getApprovedAgentList() {

		User user = (User) session.get("User");
		List<WalletMastBean> agentList = new ArrayList<>();
		agentList = service.getApprovedAgentList(user.getAggreatorid(), walletBean.getStDate(),
				walletBean.getEndDate());
		request.setAttribute("agentList", agentList);

		return "success";
	}

	public String getRejectAgentList() {
		User user = (User) session.get("User");
		System.out.println("=============================================================declinedAgent"
				+ user.getSubAggregatorId());
		List<DeclinedListBean> agentList = service.getRejectAgentList(user.getAggreatorid(), walletBean.getStDate(),
				walletBean.getEndDate());
		if (agentList == null) {
			agentList = new ArrayList<DeclinedListBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String assignUserRole() {
		generateCsrfToken();
		User us = (User) session.get("User");
		setAggRoleList(service.getRole());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));

		return "success";
	}

	public String saveUserRoleMapping() {
		User us = (User) session.get("User");
		setAggRoleList(service.getRole());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));
		if (roleMapping.getUserId().equalsIgnoreCase("-1")) {
			addActionError("Please select Sub-aggregator.");
			return "success";
		}
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		roleMapping = service.saveUserRoleMapping(roleMapping, ipAddress, userAgent);
		if (roleMapping.getActionResult().equalsIgnoreCase("1000")) {
			addActionMessage("Role assigned successfully.");
		} else {
			addActionError("Role asign failed please try again later.");
		}
		return "success";
	}

	public String getRoleMenuMapping() {

		User us = (User) session.get("User");
		setAggRoleList(service.getRole());
		setSubAggList(service.getSubAggreator(us.getAggreatorid()));
		roleMapping = service.getUserRoleMapping(roleMapping);
		return "success";
	}

	private boolean checkLoggedinUser(UserSummary user) {
		if (logger.isInfoEnabled()) {
			logger.info(" -------   Before Login   -------  ");
			logger.info("loggedinUserMap : " + loggedinUserMap);
			logger.info("User Id         : " + user.getUserId());
			logger.info("IP Address      : " + request.getRemoteAddr());
		}
		// if(loggedinUserMap.containsKey(user.getUserName()) &&
		// !loggedinUserMap.get(user.getUserName()).equals(request.getRemoteAddr()))
		org.apache.struts2.dispatcher.SessionMap httpSession = (org.apache.struts2.dispatcher.SessionMap) request
				.getServletContext().getAttribute(user.getUserId());
		if (httpSession != null && httpSession.containsKey(user.getUserId()) && httpSession.get("User") != null) {
			return false;
		} else {
			ActionContext.getContext().getSession().put(user.getUserId(), user.getUserId());
			request.getServletContext().setAttribute(user.getUserId(), ActionContext.getContext().getSession());
		}
		if (logger.isInfoEnabled()) {
			logger.info(" -------   After Login   -------  ");
			logger.info("loggedinUserMap : " + loggedinUserMap);
		}
		return true;
	}

	public String removeSesion() {
		if (user != null) {
			// if(loggedinUserMap.containsKey(user.getUserName()) &&
			// !loggedinUserMap.get(user.getUserName()).equals(request.getRemoteAddr()))
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" + user.getUserId());
			org.apache.struts2.dispatcher.SessionMap httpSession = (org.apache.struts2.dispatcher.SessionMap) request
					.getServletContext().getAttribute(user.getUserId());
			if (httpSession.containsKey(user.getUserId())) {
				httpSession.remove(user.getUserId());
				httpSession.invalidate();
				// ActionContext.getContext().//removeAttribute(user.getUserId());
			}
		}
		user = null;
		return SUCCESS;
	}

	public String validatePan() {
		User u = (User) session.get("User");
		String type = "Error";

		String pan = request.getParameter("pan").toString();
		int userType = Integer.parseInt(request.getParameter("userType"));

		if (u != null && u.getAggreatorid() != null && !u.getAggreatorid().isEmpty()) {
			System.out.println("status****************************************************" + pan);
			String status = service.validatePan(pan, u.getAggreatorid(), userType);
			System.out.println("status****************************************************" + status);
			try {
				JSONObject jObject = new JSONObject();
				jObject.put("status", status);
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {

			}
		}
		return null;
	}

	public String getRevenue() {

		User user = (User) session.get("User");
		List<RevenueReportBean> revnList = new ArrayList<>();
		walletBean.setAggreatorid(user.getAggreatorid());
		revnList = service.getRevenueReportList(walletBean);
		request.setAttribute("agentList", revnList);

		return "success";
	}

	public String getDmtDetailsReport() {

		User user = (User) session.get("User");
		List<DmtDetailsMastBean> revnList = new ArrayList<>();
		walletBean.setAggreatorid(user.getAggreatorid());
		revnList = service.getDmtDetailsReportList(walletBean);
		request.setAttribute("agentList", revnList);

		return "success";
	}

	public String getAgentBalDetail() {
		User user = (User) session.get("User");
		reportBean.setAggId(user.getAggreatorid());
		List<AgentBalDetailsBean> revnList = new ArrayList<>();
		revnList = rService.getAgentBalDetail(reportBean);
		request.setAttribute("agentList", revnList);
		return "success";
	}

	public String getAgentDtlsByDistId() {
		User user = (User) session.get("User");

		System.out.println("*****************************" + user.getAggreatorid());
		System.out.println("getId*****************************" + user.getId());

		reportBean.setAggreatorid(user.getAggreatorid());
		reportBean.setDistributerId(user.getId());
		List<AgentDetailsBean> agentDtlsList = new ArrayList<>();
		agentDtlsList = rService.getAgentDtlsByDistId(reportBean);
		request.setAttribute("agentList", agentDtlsList);
		return "success";
	}

	public String agentdetails() {
		// walletBean.aggreatorid
		AgentDetailsView av = service.agentdetailsview(walletBean);
		request.setAttribute("agentDetails", av);
		return "success";
	}

	public String getAgentWalletBalance() {
		User us = (User) session.get("User");
		String agentId = walletBean.getId();

		String walletBal = new TransactionServiceImpl().getWalletBalance(agentId);
		System.out.println("************42*************" + walletBal);
		JSONObject jObject = new JSONObject();
		jObject.put("amount", walletBal);

		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;
	}

	public String getAgentWalletBalanceById() {
		User us = (User) session.get("User");
		String agentId = walletBean.getId();

		String walletBal = new TransactionServiceImpl().getWalletBalanceById(agentId);
		System.out.println("************42*************" + walletBal);
		JSONObject jObject = new JSONObject();
		jObject.put("amount", walletBal);

		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}

		return null;
	}

	public String blockUnblockUser() {
		User us = (User) session.get("User");

		System.out.println("*************************" + userBlockedBean.getAgentId());
		System.out.println("*************************" + userBlockedBean.getBlockBy());

		String status = service.blockUnblockUser(userBlockedBean);
		System.out.println("*************************" + status);
		JSONObject jObject = new JSONObject();
		jObject.put("status", status);
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}

	public String getAgentCurrentSummary() {
		User us = (User) session.get("User");
		System.out.println("*************************" + us.getId());
		AgentCurrentSummary agentCurrentSummary = new AgentCurrentSummary();
		agentCurrentSummary.setDistributerId(us.getId());
		List<AgentCurrentSummary> agentCurrentSummaryList = new ArrayList<>();
		agentCurrentSummaryList = rService.getAgentCurrentSummary(agentCurrentSummary);
		request.setAttribute("AgentCurrentSummary", agentCurrentSummaryList);
		return "success";

	}

	public String checkSession() {
		JSONObject jObject = new JSONObject();
		HttpSession session = request.getSession(false);
		if (session != null && ((String) session.getAttribute("domainName")) != null
				&& !((String) session.getAttribute("domainName")).isEmpty()) {
			jObject.put("status", "TRUE");
		} else {

			jObject.put("status", "FALSE");
		}

		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}

	public String checkLoginStatus() {
		JSONObject jObject = new JSONObject();
		HttpSession session = request.getSession(false);
		User user = (User) session.getAttribute("User");
		if (session != null && ((String) session.getAttribute("domainName")) != null
				&& !((String) session.getAttribute("domainName")).isEmpty() && user != null) {
			jObject.put("status", "TRUE");
		} else {

			jObject.put("status", "FALSE");
		}

		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {

		}
		return null;
	}

	public String distributorOnBoard() {
		generateCsrfToken();
		User user = (User) session.get("User");
		return "success";

	}

	public String saveSignUpDistributor() {
		request.setAttribute("userType", "" + walletBean.getUsertype());
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++savesignupuser");
		User user1 = (User) session.get("User");
		String aggId = (String) session.get("aggId");
		System.out.println(walletBean.getAgentid());

		System.out.println(
				"******************************************************************" + walletBean.getDistributerid());
		System.out.println(
				"******************************************************************" + walletBean.getAggreatorid());
		System.out.println(
				"******************************************************************" + walletBean.getAgentid());
		System.out.println(
				"******************************************************************" + walletBean.getSubAgentId());
		if (walletBean.getUsertype() == -1) {
			addActionError("Please select usertype.");
			return "success";
		}

		boolean b = walletBean.getName().contains(" +");
		if (b) {
			addActionError("More than one space not allowed in name.");
			return "success";
		}

		walletBean.setAggreatorid(aggId);
		String result = service.distributerOnBoad(walletBean, ipAddress, userAgent);
		System.out.println("result " + result);
		if (result.contains("1000|")) {

			addActionMessage(
					"Distributor created successfully.Distributor Id : " + result.substring(result.indexOf("|") + 1));
			walletBean = new WalletMastBean();
			request.setAttribute("userType", "-1");
			return "success";
		} else if (result.equalsIgnoreCase("7001")) {
			addActionError(prop.getProperty("7001"));
			return "success";
		} else if (result.equalsIgnoreCase("7002")) {
			addActionError(prop.getProperty("7002"));
			return "success";
		} else if (result.equalsIgnoreCase("7038")) {
			addActionError(prop.getProperty("7038"));
			return "success";
		} else if (result.equalsIgnoreCase("7041")) {
			addActionError(prop.getProperty("7041"));
			return "success";
		} else if (result.equalsIgnoreCase("7040")) {
			addActionError(prop.getProperty("7040"));
			return "success";
		} else {
			addActionError("There is some problem please try again.");
			return "success";
		}
	}

	public String uploadSenderPanF60() {

		User user = (User) session.get("User");
		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		if (mudraSenderPanDetails == null || mudraSenderPanDetails.getSenderId() == null) {
			try {
				JSONObject jObject = new JSONObject();
				jObject.put("type", "Error");
				jObject.put("msg", "Please enter valid Details.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {
			}

			return null;
		}

		if (mudraSenderPanDetails.getMyFile1() == null || mudraSenderPanDetails.getMyFile1FileName() == null
				|| mudraSenderPanDetails.getMyFile1ContentType() == null) {

			try {
				JSONObject jObject = new JSONObject();
				jObject.put("type", "Error");
				jObject.put("msg", "Please Upload valid file.");
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {
			}
			return null;

		} else {
			try {
				if ((mudraSenderPanDetails.getMyFile1ContentType().equalsIgnoreCase("image/png")
						|| mudraSenderPanDetails.getMyFile1ContentType().equalsIgnoreCase("image/gif")
						|| mudraSenderPanDetails.getMyFile1ContentType().equalsIgnoreCase("image/jpeg"))
						&& (mudraSenderPanDetails.getMyFile1FileName().toLowerCase().endsWith(".png")
								|| mudraSenderPanDetails.getMyFile1FileName().toLowerCase().endsWith(".gif")
								|| mudraSenderPanDetails.getMyFile1FileName().toLowerCase().endsWith(".jpeg"))
						&& mudraSenderPanDetails.getMyFile1().length() <= 1000000) {

					String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("SENDERPAN");
					File dir = new File(filePath, mudraSenderPanDetails.getSenderId());
					boolean mkdir = dir.mkdir();
					filePath = filePath.concat("/" + mudraSenderPanDetails.getSenderId());
					File fileToCreate1 = new File(filePath, mudraSenderPanDetails.getMyFile1FileName());
					FileUtils.copyFile(mudraSenderPanDetails.getMyFile1(), fileToCreate1);
					mudraSenderPanDetails.setProofTypePic("./SENDERPAN/" + mudraSenderPanDetails.getSenderId() + "/"
							+ mudraSenderPanDetails.getMyFile1FileName());
					// mudraSenderPanDetails.setProofTypePic(encodeFileToBase64Binary(mudraSenderPanDetails.getMyFile1()));

				} else {
					try {
						JSONObject jObject = new JSONObject();
						jObject.put("type", "Error");
						jObject.put("msg",
								"Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.");
						response.setContentType("application/json");
						response.getWriter().println(jObject);
					} catch (Exception e) {
					}
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mudraSenderPanDetails.setAggreatorId(user.getAggreatorid());
		MudraSenderPanDetails mudraSenderPanDetailsR = service.uploadSenderPanF60(mudraSenderPanDetails, ipAddress,
				userAgent);
		if (mudraSenderPanDetailsR.getStatusCode().equalsIgnoreCase("1000")) {
			try {
				JSONObject jObject = new JSONObject();
				jObject.put("type", "Success");
				jObject.put("msg", mudraSenderPanDetailsR.getStatusMessage());
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {
			}
			return null;

		} else {
			try {
				JSONObject jObject = new JSONObject();
				jObject.put("type", "Fail");
				jObject.put("msg", mudraSenderPanDetailsR.getStatusMessage());
				response.setContentType("application/json");
				response.getWriter().println(jObject);
			} catch (Exception e) {
			}
			return null;

		}

	}

	public String uploadSenderPanF60Panel() {
		mudraSenderPanDetails.setSenderId(request.getParameter("senderId"));
		return "success";

	}

	public String getSenderPanF60List() {
		User user = (User) session.get("User");
		System.out.println("*****************************" + user.getAggreatorid());
		mudraSenderPanDetails.setAggreatorId(user.getAggreatorid());
		reportBean.setAggreatorid(user.getAggreatorid());
		List<MudraSenderPanDetails> senderPanF60List = new ArrayList<>();
		senderPanF60List = service.getPendingPanReq(mudraSenderPanDetails);
		request.setAttribute("senderPanF60List", senderPanF60List);
		return "success";
	}

	public String acceptedRejectedSenderPanF60() {

		User user = (User) session.get("User");

		System.out.println(mudraSenderPanDetails.getId());
		System.out.println(mudraSenderPanDetails.getComment());
		System.out.println(mudraSenderPanDetails.getStatus());
		mudraSenderPanDetails.setAggreatorId(user.getAggreatorid());

		MudraSenderPanDetails mudraSenderPanDetailsR = service.rejectAcceptedSenderPan(mudraSenderPanDetails);
		if (mudraSenderPanDetailsR.getStatusCode().equalsIgnoreCase("1000")) {
			addActionMessage(mudraSenderPanDetailsR.getStatusMessage());
		} else {
			addActionError(mudraSenderPanDetailsR.getStatusMessage());

		}

		reportBean.setAggreatorid(user.getAggreatorid());
		List<MudraSenderPanDetails> senderPanF60List = new ArrayList<>();
		senderPanF60List = service.getPendingPanReq(mudraSenderPanDetails);
		request.setAttribute("senderPanF60List", senderPanF60List);
		return "success";

	}

	public String getAgentDetailForApprove() {
		User u = (User) session.get("User");

		List<WalletMastBean> agentList = service.getAgentDetailForApprove(u.getId());
		if (agentList == null) {
			agentList = new ArrayList<WalletMastBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String approvedAgentByAgg() {

		walletBean.setId(user.getUserId());
		service.approvedAgentByAgg(walletBean);

		User u = (User) session.get("User");

		List<WalletMastBean> agentList = service.getAgentDetailForApprove(u.getId());
		if (agentList == null) {
			agentList = new ArrayList<WalletMastBean>();
		}
		request.setAttribute("agentList", agentList);
		return "success";
	}

	public String refundAgent() {
		String resp = "fail";
		try {
			String senderMobile = request.getParameter("senderMobile").toString();
			String txnId = request.getParameter("txnId").toString();
			User user = (User) session.get("User");
			ResponseBean responseDtl = service.refundAgent(senderMobile, txnId, user.getId());
			if (responseDtl.getCode().equals("1000") && responseDtl.getRefundList() != null
					&& responseDtl.getRefundList().size() > 0) {
				Double txnAmount = 0d;
				Double refundAmount = 0d;
				for (RefundAgent refund : responseDtl.getRefundList()) {
					txnAmount = +Double.parseDouble(refund.getDramount());

					if (refund.getStatus().equalsIgnoreCase("FAILED")) {
						refundAmount = +Double.parseDouble(refund.getDramount());
					}
				}
				RefundAgent agentRefund = new RefundAgent();
				agentRefund.setDramount(Double.toString(refundAmount));
				agentRefund.setId(responseDtl.getRefundList().get(0).getId());
				agentRefund.setMobileno(senderMobile);
				agentRefund.setNarrartion(responseDtl.getRefundList().get(0).getNarrartion());
				agentRefund.setStatus(responseDtl.getRefundList().get(0).getStatus());
				agentRefund.setTxnamount(Double.toString(txnAmount));
				agentRefund.setOtp(responseDtl.getRefundList().get(0).getOtp());
				responseDtl.getRefundList().clear();

				List<RefundAgent> agentRefundList = new ArrayList<RefundAgent>();
				agentRefundList.add(agentRefund);
				responseDtl.setRefundList(agentRefundList);

				request.setAttribute("refundListResp", responseDtl);
				return "success";
			} else {
				addActionError(responseDtl.getMessage());
				return "success";
			}

		} catch (Exception e) {
			logger.info("************problem in refundAgent***e.message***" + e.getMessage());
			e.printStackTrace();
		}
		return resp;
	}

	public String refundAgentRequest() {
		String resp = "fail";

		com.bhartipay.wallet.user.service.UserService use = new com.bhartipay.wallet.user.service.impl.UserServiceImpl();

		String aggId = (String) session.get("aggId");
		user.setAggreatorid(aggId);
		String result = "false";
		if (user != null && user.getHceToken() != null && user.getHceToken().equalsIgnoreCase("1")) {
			result = use.verifyRefundOTP(user);
			System.out.println(result);
		} else {
			result = "true";
		}
		if (result != null && !result.isEmpty() && result.equalsIgnoreCase("true")) {
			try {
				String senderMobile;
				String mpin;
				senderMobile = user.getUserId();
				if (user != null && user.getHceToken() != null && user.getHceToken().equalsIgnoreCase("1")) {

					mpin = "-1";
				} else {
					mpin = user.getOtp();// request.getParameter("MPIN").toString();
				}
				String txnId = request.getParameter("transactionId").toString();
				User user = (User) session.get("User");
				ResponseBean responseDtl = service.refundAgentRequest(senderMobile, txnId, user.getId(), mpin);
				if (responseDtl.getCode().equals("3000")) {
					request.setAttribute("refundListResp", responseDtl);
					return "success";
				} else {
					addActionError(responseDtl.getMessage());
					return "success";
				}

			} catch (Exception e) {
				logger.info("************problem in refundAgent***e.message***" + e.getMessage());
				e.printStackTrace();
			}
			return resp;
		} else {
			addActionError("Invalid OTP.");
			return "success";
		}
	}

	public String getAgentSenderBalance() {
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");
		DecimalFormat df = new DecimalFormat("#");
		logger.info(serverName + "************************************getAgentSenderBalance() action starting");
		String userAgent = request.getHeader("User-Agent");

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		User us = (User) session.get("User");
		mastBean.setAgentId(us.getId());
		mastBean.setId(session.get("senderId").toString());
		mastBean.setAggreatorId(us.getAggreatorid());
		HashMap resp = service.getAgentSenderBalance(mastBean);
		JSONObject jObject = new JSONObject();
		if (resp.get("walletBalance") != null && !resp.get("walletBalance").toString().trim().equals("")) {
			jObject.put("SenderBalance", Double.parseDouble(resp.get("walletBalance").toString()));
		}
		if (resp.get("SenderTransferLimit") != null && !resp.get("SenderTransferLimit").toString().trim().equals("")) {
			jObject.put("SenderTransferLimit",
					Double.parseDouble(df.format(Double.parseDouble(resp.get("SenderTransferLimit").toString()))));
		}
		if (resp.get("AgentBalance") != null && !resp.get("AgentBalance").toString().trim().equals("")) {
			jObject.put("AgentBalance", Double.parseDouble(resp.get("AgentBalance").toString()));
		}
		logger.info(serverName + "---agentId:senderId:walletbal:translimit:agentbal+" + us.getId() + ":"
				+ session.get("senderId").toString() + ":" + resp.get("walletBalance") + ":"
				+ resp.get("SenderTransferLimit") + ":" + resp.get("AgentBalance"));
		try {
			response.setContentType("application/json");
			response.getWriter().println(jObject);
		} catch (Exception e) {
			logger.info(serverName + "******************validateSender() problem***********************");

		}
		return null;
	}

	
  public String getDmtReportBySuper(){
	
		User u = (User) session.get("User");
		int userType = (int)u.getUsertype();
	    UserService uService=new UserServiceImpl();
		 
	    Map<String, String> agentList = uService.getAgentBySuper(u.getId(),u.getAggreatorid(),userType);
	     if (agentList != null && agentList.size() > 0) {
			setAgentList(agentList);
		 }
	  	 
		
		ServletContext context = request.getServletContext();
		String serverName = context.getInitParameter("serverName");
	
		logger.debug(serverName + "******************getDmtReportBySuper() action start***********************");
		DMTReportInOut mSBean = new DMTReportInOut();
		
		String data = request.getParameter("data");
		logger.debug(serverName + "******************getDmtReportBySuper() action start**********data*************"
				+ data);
		Gson gson = new Gson();
		// mSBean = gson.fromJson(data, DMTReportInOut.class);
		mSBean.setStDate(inputBean.getStDate());
		mSBean.setEdDate(inputBean.getEndDate());
		mSBean.setStatuscode(inputBean.getStatus());
		mSBean.setAgentId(u.getId());
		mSBean.setUsertype(u.getUsertype());
		
		String userAgent = request.getHeader("User-Agent");
	
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
	  
	    logger.debug(serverName + "******************getDmtReportBySuper() action start***********************");
		//mSBean = service.getTransacationLedgerDtl(mSBean, ipAddress, userAgent);
		mSBean = service.getDmtReportBySuper(mSBean, ipAddress, userAgent);
		
		request.setAttribute("transactionLedger", mSBean.getTransList());

	  return "success";
	}
	
	
	
	
}
