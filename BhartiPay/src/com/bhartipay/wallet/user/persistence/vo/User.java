package com.bhartipay.wallet.user.persistence.vo;

public class User {

private String emailid;
private double finalBalance;//=1960.0,
private String id;//=AGGE001015,
private String mobileno;//=9999215888,
private String name;//=ambuj kumar,
private String walletid;//=999921588801092016130121
private double usertype;
private String userImg;
private String aggreatorid;
private String distributerid;	
private String superdistributerid;
private String agentid;
private String subAgentId;
private String subAggregatorId;
private String logo;
private String banner;
private String token;
private String tokenSecurityKey;
private String kycStatus;
private String country;//=INDIA,
private String countrycurrency;//=INR,
private double countryid;//=91.0, 
private double emailvalid;//=1.0, 
private double kycvalid;//=0.0, 
private double otpsendtomail;//=0.0, 
private double smssend;//=0.0, 
private double status;//=0.0
private double cashBackfinalBalance;//=1960.0,
private String shopName;
private int isimps;
private int wallet;
private int bank;
private int whiteLabel;
private String customerCare;
private String supportEmailId;
private String portalMessage;
private String agentCode;
private int otpVerificationRequired;
private String asAgentCode;
private String aepsChannel;
private String asCode;
private String bank1;
private String bank2;
private String bank3;
private String bank3_via;
private int onlineMoney;
private int recharge;
private int bbps;

private String managerId;
private String salesId;
private String managementId;




public int getOnlineMoney() {
	return onlineMoney;
}


public void setOnlineMoney(int onlineMoney) {
	this.onlineMoney = onlineMoney;
}


public int getRecharge() {
	return recharge;
}


public void setRecharge(int recharge) {
	this.recharge = recharge;
}


public int getBbps() {
	return bbps;
}


public void setBbps(int bbps) {
	this.bbps = bbps;
}


public String getBank1() {
	return bank1;
}


public void setBank1(String bank1) {
	this.bank1 = bank1;
}


public String getBank2() {
	return bank2;
}


public void setBank2(String bank2) {
	this.bank2 = bank2;
}


public String getBank3() {
	return bank3;
}


public void setBank3(String bank3) {
	this.bank3 = bank3;
}


public String getBank3_via() {
	return bank3_via;
}


public void setBank3_via(String bank3_via) {
	this.bank3_via = bank3_via;
}


public String getAsCode() {
	return asCode;
}


public void setAsCode(String asCode) {
	this.asCode = asCode;
}


public String getShopName() {
	return shopName;
}


public void setShopName(String shopName) {
	this.shopName = shopName;
}


public String getAsAgentCode() {
	return asAgentCode;
}


public void setAsAgentCode(String asAgentCode) {
	this.asAgentCode = asAgentCode;
}


public String getAepsChannel() {
	return aepsChannel;
}


public void setAepsChannel(String aepsChannel) {
	this.aepsChannel = aepsChannel;
}


public int getOtpVerificationRequired() {
	return otpVerificationRequired;
}


public void setOtpVerificationRequired(int otpVerificationRequired) {
	this.otpVerificationRequired = otpVerificationRequired;
}


public String getAgentCode() {
	return agentCode;
}


public void setAgentCode(String agentCode) {
	this.agentCode = agentCode;
}


public String getPortalMessage() {
	return portalMessage;
}


public void setPortalMessage(String portalMessage) {
	this.portalMessage = portalMessage;
}


public String getSupportEmailId() {
	return supportEmailId;
}


public void setSupportEmailId(String supportEmailId) {
	this.supportEmailId = supportEmailId;
}





public String getSuperdistributerid() {
	return superdistributerid;
}
public void setSuperdistributerid(String superdistributerid) {
	this.superdistributerid = superdistributerid;
}
public String getCustomerCare() {
	return customerCare;
}
public void setCustomerCare(String customerCare) {
	this.customerCare = customerCare;
}
public int getWhiteLabel() {
	return whiteLabel;
}
public void setWhiteLabel(int whiteLabel) {
	this.whiteLabel = whiteLabel;
}
public int getWallet() {
	return wallet;
}
public void setWallet(int wallet) {
	this.wallet = wallet;
}
public int getBank() {
	return bank;
}
public void setBank(int bank) {
	this.bank = bank;
}
public int getIsimps() {
	return isimps;
}
public void setIsimps(int isimps) {
	this.isimps = isimps;
}
public String getKycStatus() {
	return kycStatus;
}
public void setKycStatus(String kycStatus) {
	this.kycStatus = kycStatus;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getTokenSecurityKey() {
	return tokenSecurityKey;
}
public void setTokenSecurityKey(String tokenSecurityKey) {
	this.tokenSecurityKey = tokenSecurityKey;
}
public double getCashBackfinalBalance() {
	return cashBackfinalBalance;
}
public void setCashBackfinalBalance(double cashBackfinalBalance) {
	this.cashBackfinalBalance = cashBackfinalBalance;
}
public String getSubAggregatorId() {
	return subAggregatorId;
}
public void setSubAggregatorId(String subAggregatorId) {
	this.subAggregatorId = subAggregatorId;
}
public String getLogo() {
	return logo;
}
public void setLogo(String logo) {
	this.logo = logo;
}
public String getBanner() {
	return banner;
}
public void setBanner(String banner) {
	this.banner = banner;
}
public String getAggreatorid() {
	return aggreatorid;
}
public void setAggreatorid(String aggreatorid) {
	this.aggreatorid = aggreatorid;
}
public String getDistributerid() {
	return distributerid;
}
public void setDistributerid(String distributerid) {
	this.distributerid = distributerid;
}
public String getAgentid() {
	return agentid;
}
public void setAgentid(String agentid) {
	this.agentid = agentid;
}
public String getSubAgentId() {
	return subAgentId;
}
public void setSubAgentId(String subAgentId) {
	this.subAgentId = subAgentId;
}
public String getUserImg() {
	return userImg;
}
public void setUserImg(String userImg) {
	this.userImg = userImg;
}
public double getUsertype() {
	return usertype;
}
public void setUsertype(double usertype) {
	this.usertype = usertype;
}
public String getEmailid() {
	return emailid;
}
public void setEmailid(String emailid) {
	this.emailid = emailid;
}
public double getFinalBalance() {
	return finalBalance;
}
public void setFinalBalance(double finalBalance) {
	this.finalBalance = finalBalance;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getWalletid() {
	return walletid;
}
public void setWalletid(String walletid) {
	this.walletid = walletid;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getCountrycurrency() {
	return countrycurrency;
}
public void setCountrycurrency(String countrycurrency) {
	this.countrycurrency = countrycurrency;
}
public double getCountryid() {
	return countryid;
}
public void setCountryid(double countryid) {
	this.countryid = countryid;
}
public double getEmailvalid() {
	return emailvalid;
}
public void setEmailvalid(double emailvalid) {
	this.emailvalid = emailvalid;
}
public double getKycvalid() {
	return kycvalid;
}
public void setKycvalid(double kycvalid) {
	this.kycvalid = kycvalid;
}
public double getOtpsendtomail() {
	return otpsendtomail;
}
public void setOtpsendtomail(double otpsendtomail) {
	this.otpsendtomail = otpsendtomail;
}
public double getSmssend() {
	return smssend;
}
public void setSmssend(double smssend) {
	this.smssend = smssend;
}
public double getStatus() {
	return status;
}
public void setStatus(double status) {
	this.status = status;
}


public String getManagerId() {
	return managerId;
}


public void setManagerId(String managerId) {
	this.managerId = managerId;
}


public String getSalesId() {
	return salesId;
}


public void setSalesId(String salesId) {
	this.salesId = salesId;
}


public String getManagementId() {
	return managementId;
}


public void setManagementId(String managementId) {
	this.managementId = managementId;
}






}
