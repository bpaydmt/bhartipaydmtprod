package com.bhartipay.wallet.user.persistence.vo;

import java.math.BigDecimal;

public class TransacationLedgerBean {
	private String id;
	private String 	agentid;	
	private String 	senderid;
	private String ptytransdt;
	private String txnid;
	private BigDecimal dramount;
	private String narrartion;
	private String mobileno;
	private String firstname;
	private String lastname;
	private String accountno;
	private String name;
	private String ifsccode;
	private String bankname;
	private String bankrrn;
	private String status;
	private int isbank;
	private String merchanttransid;
	private double charges;
	private String acq;



	public String getAcq() {
		return acq;
	}
	public void setAcq(String acq) {
		this.acq = acq;
	}




	public double getCharges() {
		return charges;
	}
	public void setCharges(double charges) {
		this.charges = charges;
	}
	
	public String getMerchanttransid() {
		return merchanttransid;
	}
	public void setMerchanttransid(String merchanttransid) {
		this.merchanttransid = merchanttransid;
	}
	
	
	
	public int getIsbank() {
		return isbank;
	}
	public void setIsbank(int isbank) {
		this.isbank = isbank;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getPtytransdt() {
		return ptytransdt;
	}
	public void setPtytransdt(String ptytransdt) {
		this.ptytransdt = ptytransdt;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public BigDecimal getDramount() {
		return dramount;
	}
	public void setDramount(BigDecimal dramount) {
		this.dramount = dramount;
	}
	public String getNarrartion() {
		return narrartion;
	}
	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBankrrn() {
		return bankrrn;
	}
	public void setBankrrn(String bankrrn) {
		this.bankrrn = bankrrn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	

}
