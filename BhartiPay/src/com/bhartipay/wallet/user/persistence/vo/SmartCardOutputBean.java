package com.bhartipay.wallet.user.persistence.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class SmartCardOutputBean {

	@SerializedName("aggreatorId")
	@Expose
	private String aggreatorId;
	@SerializedName("countryCode")
	@Expose
	private Integer countryCode;
	@SerializedName("emailId")
	@Expose
	private String emailId;
	@SerializedName("lastName")
	@Expose
	private String lastName;
	@SerializedName("mobileNo")
	@Expose
	private String mobileNo;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("passowrd")
	@Expose
	private String passowrd;
	@SerializedName("prePaidError")
	@Expose
	private String prePaidError;
	@SerializedName("prePaidId")
	@Expose
	private String prePaidId;
	@SerializedName("prePaidStatus")
	@Expose
	private String prePaidStatus;
	@SerializedName("preferredName")
	@Expose
	private String preferredName;
	@SerializedName("reqDate")
	@Expose
	private String reqDate;
	@SerializedName("reqId")
	@Expose
	private String reqId;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("statusCode")
	@Expose
	private String statusCode;
	@SerializedName("userId")
	@Expose
	private String userId;
	@SerializedName("walletId")
	@Expose
	private String walletId;
	 private String prePaidUserId;
	 
	 
	 
	 
	public String getPrePaidUserId() {
		return prePaidUserId;
	}

	public void setPrePaidUserId(String prePaidUserId) {
		this.prePaidUserId = prePaidUserId;
	}

	/**
	* 
	* @return
	* The aggreatorId
	*/
	public String getAggreatorId() {
	return aggreatorId;
	}

	/**
	* 
	* @param aggreatorId
	* The aggreatorId
	*/
	public void setAggreatorId(String aggreatorId) {
	this.aggreatorId = aggreatorId;
	}

	/**
	* 
	* @return
	* The countryCode
	*/
	public Integer getCountryCode() {
	return countryCode;
	}

	/**
	* 
	* @param countryCode
	* The countryCode
	*/
	public void setCountryCode(Integer countryCode) {
	this.countryCode = countryCode;
	}

	/**
	* 
	* @return
	* The emailId
	*/
	public String getEmailId() {
	return emailId;
	}

	/**
	* 
	* @param emailId
	* The emailId
	*/
	public void setEmailId(String emailId) {
	this.emailId = emailId;
	}

	/**
	* 
	* @return
	* The lastName
	*/
	public String getLastName() {
	return lastName;
	}

	/**
	* 
	* @param lastName
	* The lastName
	*/
	public void setLastName(String lastName) {
	this.lastName = lastName;
	}

	/**
	* 
	* @return
	* The mobileNo
	*/
	public String getMobileNo() {
	return mobileNo;
	}

	/**
	* 
	* @param mobileNo
	* The mobileNo
	*/
	public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
	}

	/**
	* 
	* @return
	* The name
	*/
	public String getName() {
	return name;
	}

	/**
	* 
	* @param name
	* The name
	*/
	public void setName(String name) {
	this.name = name;
	}

	/**
	* 
	* @return
	* The passowrd
	*/
	public String getPassowrd() {
	return passowrd;
	}

	/**
	* 
	* @param passowrd
	* The passowrd
	*/
	public void setPassowrd(String passowrd) {
	this.passowrd = passowrd;
	}

	/**
	* 
	* @return
	* The prePaidError
	*/
	public String getPrePaidError() {
	return prePaidError;
	}

	/**
	* 
	* @param prePaidError
	* The prePaidError
	*/
	public void setPrePaidError(String prePaidError) {
	this.prePaidError = prePaidError;
	}

	/**
	* 
	* @return
	* The prePaidId
	*/
	public String getPrePaidId() {
	return prePaidId;
	}

	/**
	* 
	* @param prePaidId
	* The prePaidId
	*/
	public void setPrePaidId(String prePaidId) {
	this.prePaidId = prePaidId;
	}

	/**
	* 
	* @return
	* The prePaidStatus
	*/
	public String getPrePaidStatus() {
	return prePaidStatus;
	}

	/**
	* 
	* @param prePaidStatus
	* The prePaidStatus
	*/
	public void setPrePaidStatus(String prePaidStatus) {
	this.prePaidStatus = prePaidStatus;
	}

	/**
	* 
	* @return
	* The preferredName
	*/
	public String getPreferredName() {
	return preferredName;
	}

	/**
	* 
	* @param preferredName
	* The preferredName
	*/
	public void setPreferredName(String preferredName) {
	this.preferredName = preferredName;
	}

	/**
	* 
	* @return
	* The reqDate
	*/
	public String getReqDate() {
	return reqDate;
	}

	/**
	* 
	* @param reqDate
	* The reqDate
	*/
	public void setReqDate(String reqDate) {
	this.reqDate = reqDate;
	}

	/**
	* 
	* @return
	* The reqId
	*/
	public String getReqId() {
	return reqId;
	}

	/**
	* 
	* @param reqId
	* The reqId
	*/
	public void setReqId(String reqId) {
	this.reqId = reqId;
	}

	/**
	* 
	* @return
	* The status
	*/
	public String getStatus() {
	return status;
	}

	/**
	* 
	* @param status
	* The status
	*/
	public void setStatus(String status) {
	this.status = status;
	}

	/**
	* 
	* @return
	* The statusCode
	*/
	public String getStatusCode() {
	return statusCode;
	}

	/**
	* 
	* @param statusCode
	* The statusCode
	*/
	public void setStatusCode(String statusCode) {
	this.statusCode = statusCode;
	}

	/**
	* 
	* @return
	* The userId
	*/
	public String getUserId() {
	return userId;
	}

	/**
	* 
	* @param userId
	* The userId
	*/
	public void setUserId(String userId) {
	this.userId = userId;
	}

	/**
	* 
	* @return
	* The walletId
	*/
	public String getWalletId() {
	return walletId;
	}

	/**
	* 
	* @param walletId
	* The walletId
	*/
	public void setWalletId(String walletId) {
	this.walletId = walletId;
	}

	
}
