package com.bhartipay.wallet.user.persistence.vo;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Transient;

public class WalletMastBean implements Serializable {

	private String id;
	private String walletid;
	private String mobileno;
	private String emailid;
	// private String password;
	private String name;
	private int usertype;
	private String userstatus;
	private String creationDate;
	private String aggreatorid;
	private String distributerid;
	private String superdistributerid;
	private String agentid;
	private String subAgentId;
	private String createdby;
	private int loginStatus;
	private String lastlogintime;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String pin;
	// private String pan;
	// private String adhar;
	private String kycstatus;
	private String password;
	private String userId;
	private String oldpassword;
	private String imeiIP;
	private String confirmPassword;
	private String profilePic;
	private String otp;
	private String newMobileno;
	private String newEmailId;
	private String statusCode;
	private double applicationFee;
	private String formLoc;
	private String idLoc;
	private String addressLoc;
	private String dob;
	private String agentType;
	private String shopAddress1;
	private String shopAddress2;
	private String shopCity;
	private String shopState;
	private String shopPin;
	private String addressProofType;
	private String pan;
	private String adhar;
	private String shopName;
	private String bankName;
	private String paymentMode;
	private String paymentDate;
	private String asCode;
	private String territory;
	private String managerName;
	private String soName;
	private String distributorName;
	private String distributorMobileNo;
	private String accountNumber;
	private String ifscCode;
	
	
	private File file1;
	private String file1FileName;
	private String file1ContentType;

	private File file2;
	private String file2FileName;
	private String file2ContentType;

	private File file3;
	private String file3FileName;
	private String file3ContentType;
	
	private File file4;
	private String file4FileName;
	private String file4ContentType;
	
	
	private String utrNo;

	private String declinedComment;
	
	private String stDate;

	private String endDate;
	
	private String  approvalRequired;
	
	private String approveDate;
	private String remark;
	
	private String whiteLabel;
	private String agentCode;
	private String asAgentCode;
	private String aepsChannel;
	
	private String bankType;
	private String txnStatus;
	
	private String gender;
	private String planId;
	
	private String bank1;
	private String bank2;
	private String bank3;
	private String bank3_via;
	
	private int onlineMoney;
	private int recharge;
	private int bbps;
	
	private String passbookLoc;
	private String paymentLoc;
	
	private String managerId;
	private String salesId;
	private String subAggregatorId;
	
	
	
	public int getOnlineMoney() {
		return onlineMoney;
	}

	public void setOnlineMoney(int onlineMoney) {
		this.onlineMoney = onlineMoney;
	}

	public int getRecharge() {
		return recharge;
	}

	public void setRecharge(int recharge) {
		this.recharge = recharge;
	}

	public int getBbps() {
		return bbps;
	}

	public void setBbps(int bbps) {
		this.bbps = bbps;
	}

	public String getBank1() {
		return bank1;
	}

	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}

	public String getBank2() {
		return bank2;
	}

	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}

	public String getBank3() {
		return bank3;
	}

	public void setBank3(String bank3) {
		this.bank3 = bank3;
	}

	public String getBank3_via() {
		return bank3_via;
	}

	public void setBank3_via(String bank3_via) {
		this.bank3_via = bank3_via;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getSoName() {
		return soName;
	}

	public void setSoName(String soName) {
		this.soName = soName;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getDistributorMobileNo() {
		return distributorMobileNo;
	}

	public void setDistributorMobileNo(String distributorMobileNo) {
		this.distributorMobileNo = distributorMobileNo;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAsAgentCode() {
		return asAgentCode;
	}

	public void setAsAgentCode(String asAgentCode) {
		this.asAgentCode = asAgentCode;
	}

	public String getAepsChannel() {
		return aepsChannel;
	}

	public void setAepsChannel(String aepsChannel) {
		this.aepsChannel = aepsChannel;
	}
	
	
	
	public String getAsCode() {
		return asCode;
	}

	public void setAsCode(String asCode) {
		this.asCode = asCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	
	public String getSuperdistributerid() {
		return superdistributerid;
	}

	public void setSuperdistributerid(String superdistributerid) {
		this.superdistributerid = superdistributerid;
	}
	public String getWhiteLabel() {
		return whiteLabel;
	}

	public void setWhiteLabel(String whiteLabel) {
		this.whiteLabel = whiteLabel;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getApprovalRequired() {
		return approvalRequired;
	}

	public void setApprovalRequired(String approvalRequired) {
		this.approvalRequired = approvalRequired;
	}

	public String getStDate() {
		return stDate;
	}

	public void setStDate(String stDate) {
		this.stDate = stDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDeclinedComment() {
		return declinedComment;
	}

	public void setDeclinedComment(String declinedComment) {
		this.declinedComment = declinedComment;
	}

	public String getUtrNo() {
		return utrNo;
	}

	public void setUtrNo(String utrNo) {
		this.utrNo = utrNo;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAddressProofType() {
		return addressProofType;
	}

	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getShopAddress1() {
		return shopAddress1;
	}

	public void setShopAddress1(String shopAddress1) {
		this.shopAddress1 = shopAddress1;
	}

	public String getShopAddress2() {
		return shopAddress2;
	}

	public void setShopAddress2(String shopAddress2) {
		this.shopAddress2 = shopAddress2;
	}

	public String getShopCity() {
		return shopCity;
	}

	public void setShopCity(String shopCity) {
		this.shopCity = shopCity;
	}

	public String getShopState() {
		return shopState;
	}

	public void setShopState(String shopState) {
		this.shopState = shopState;
	}

	public String getShopPin() {
		return shopPin;
	}

	public void setShopPin(String shopPin) {
		this.shopPin = shopPin;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public String getFile1FileName() {
		return file1FileName;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public String getFile1ContentType() {
		return file1ContentType;
	}

	public void setFile1ContentType(String file1ContentType) {
		this.file1ContentType = file1ContentType;
	}

	public File getFile2() {
		return file2;
	}

	public void setFile2(File file2) {
		this.file2 = file2;
	}

	public String getFile2FileName() {
		return file2FileName;
	}

	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	public String getFile2ContentType() {
		return file2ContentType;
	}

	public void setFile2ContentType(String file2ContentType) {
		this.file2ContentType = file2ContentType;
	}

	public File getFile3() {
		return file3;
	}

	public void setFile3(File file3) {
		this.file3 = file3;
	}

	public String getFile3FileName() {
		return file3FileName;
	}

	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	public String getFile3ContentType() {
		return file3ContentType;
	}

	public void setFile3ContentType(String file3ContentType) {
		this.file3ContentType = file3ContentType;
	}

	public double getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(double applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getFormLoc() {
		return formLoc;
	}

	public void setFormLoc(String formLoc) {
		this.formLoc = formLoc;
	}

	public String getIdLoc() {
		return idLoc;
	}

	public void setIdLoc(String idLoc) {
		this.idLoc = idLoc;
	}

	public String getAddressLoc() {
		return addressLoc;
	}

	public void setAddressLoc(String addressLoc) {
		this.addressLoc = addressLoc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getNewMobileno() {
		return newMobileno;
	}

	public void setNewMobileno(String newMobileno) {
		this.newMobileno = newMobileno;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getSubAgentId() {
		return subAgentId;
	}

	public void setSubAgentId(String subAgentId) {
		this.subAgentId = subAgentId;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOldpassword() {
		return oldpassword;
	}

	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}

	public String getImeiIP() {
		return imeiIP;
	}

	public void setImeiIP(String imeiIP) {
		this.imeiIP = imeiIP;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUsertype() {
		return usertype;
	}

	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}

	public String getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getDistributerid() {
		return distributerid;
	}

	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getLastlogintime() {
		return lastlogintime;
	}

	public void setLastlogintime(String lastlogintime) {
		this.lastlogintime = lastlogintime;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAdhar() {
		return adhar;
	}

	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}

	public String getKycstatus() {
		return kycstatus;
	}

	public void setKycstatus(String kycstatus) {
		this.kycstatus = kycstatus;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getNewEmailId() {
		return newEmailId;
	}

	public void setNewEmailId(String newEmailId) {
		this.newEmailId = newEmailId;
	}

	public File getFile4() {
		return file4;
	}

	public void setFile4(File file4) {
		this.file4 = file4;
	}

	public String getFile4FileName() {
		return file4FileName;
	}

	public void setFile4FileName(String file4FileName) {
		this.file4FileName = file4FileName;
	}

	public String getFile4ContentType() {
		return file4ContentType;
	}

	public void setFile4ContentType(String file4ContentType) {
		this.file4ContentType = file4ContentType;
	}

	public String getPassbookLoc() {
		return passbookLoc;
	}

	public void setPassbookLoc(String passbookLoc) {
		this.passbookLoc = passbookLoc;
	}

	public String getPaymentLoc() {
		return paymentLoc;
	}

	public void setPaymentLoc(String paymentLoc) {
		this.paymentLoc = paymentLoc;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getSubAggregatorId() {
		return subAggregatorId;
	}

	public void setSubAggregatorId(String subAggregatorId) {
		this.subAggregatorId = subAggregatorId;
	}
	
	
	

}
