package com.bhartipay.wallet.user.persistence.vo;

public class MailConfigMast {
	
	private String aggreatorid;
	private String hosturl;
	private String from_mail;
	private String fromname;
	private String username;
	private String password;
	private String port;
	private String actionResult;
	
	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getHosturl() {
		return hosturl;
	}

	public void setHosturl(String hosturl) {
		this.hosturl = hosturl;
	}

	public String getFrom_mail() {
		return from_mail;
	}

	public void setFrom_mail(String from_mail) {
		this.from_mail = from_mail;
	}

	public String getFromname() {
		return fromname;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
	

	

}
