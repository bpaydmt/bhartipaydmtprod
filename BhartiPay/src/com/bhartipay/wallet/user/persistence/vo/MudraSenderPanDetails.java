package com.bhartipay.wallet.user.persistence.vo;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;



public class MudraSenderPanDetails implements Serializable {
	
	private int id;
	private String senderId;
	private String proofType;
	private String panNumber;
	private String proofTypePic;
	private String status;
	private Date loaddate;
	private String ipiemi;
	private String userAgent;
	private String comment;
	private Date approveDate;
	private String statusCode;
	private String statusMessage;
	private String aggreatorId;
	private String mobileNo;
	
	private File myFile1;
	private String myFile1FileName;
	private String myFile1ContentType;
	
	
	private String stDate;
	private String endDate;
	
	private String senderName;
	
	
	
	
	
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	public File getMyFile1() {
		return myFile1;
	}
	public void setMyFile1(File myFile1) {
		this.myFile1 = myFile1;
	}
	public String getMyFile1FileName() {
		return myFile1FileName;
	}
	public void setMyFile1FileName(String myFile1FileName) {
		this.myFile1FileName = myFile1FileName;
	}
	public String getMyFile1ContentType() {
		return myFile1ContentType;
	}
	public void setMyFile1ContentType(String myFile1ContentType) {
		this.myFile1ContentType = myFile1ContentType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getProofType() {
		return proofType;
	}
	public void setProofType(String proofType) {
		this.proofType = proofType;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	
	
	
	public String getProofTypePic() {
		return proofTypePic;
	}
	public void setProofTypePic(String proofTypePic) {
		this.proofTypePic = proofTypePic;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getLoaddate() {
		return loaddate;
	}
	public void setLoaddate(Date loaddate) {
		this.loaddate = loaddate;
	}
	public String getIpiemi() {
		return ipiemi;
	}
	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	
	
	
	

}
