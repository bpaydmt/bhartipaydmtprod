package com.bhartipay.wallet.user.persistence.vo;

import java.io.Serializable;
import java.util.List;

public class MudraSenderMastBean implements Serializable{
	private String id;
	private String aggreatorId;
	private String agentId;
	private String mobileNo;
	private String emailId;
	private String firstName;
	private String lastName;
	private String mpin;
	private String status;
	private String kycStatus;
	private double walletBalance;
	private double transferLimit;
	private String creationdate;
	private String ipiemi;
	private String agent;
	private String statusCode;
	private String statusDesc;
	private String otp;
	private List <MudraBeneficiaryMastBean> beneficiaryList;
	List<SenderFavouriteViewBean> senderFavouriteList;
	private String exportMobile;
	private String favourite;
	private double bcLimit;
	private int isPanValidate; 
	

	
	
	
	
	public int getIsPanValidate() {
		return isPanValidate;
	}

	public void setIsPanValidate(int isPanValidate) {
		this.isPanValidate = isPanValidate;
	}

	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}



	public double getBcLimit() {
		return bcLimit;
	}

	public void setBcLimit(double bcLimit) {
	
		this.bcLimit = bcLimit;
	}

	public String getFavourite() {
		return favourite;
	}

	public void setFavourite(String favourite) {
		this.favourite = favourite;
	}

	public String getExportMobile() {
		return exportMobile;
	}

	public void setExportMobile(String exportMobile) {
		this.exportMobile = exportMobile;
	}

	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public double getTransferLimit() {
		return transferLimit;
	}

	public void setTransferLimit(double transferLimit) {
		this.transferLimit = transferLimit;
	}

	public List<SenderFavouriteViewBean> getSenderFavouriteList() {
		return senderFavouriteList;
	}

	public void setSenderFavouriteList(
			List<SenderFavouriteViewBean> senderFavouriteList) {
		this.senderFavouriteList = senderFavouriteList;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public List<MudraBeneficiaryMastBean> getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List<MudraBeneficiaryMastBean> beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(String creationdate) {
		this.creationdate = creationdate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	

	

	
	
}
