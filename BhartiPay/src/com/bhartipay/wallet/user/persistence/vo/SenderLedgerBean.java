package com.bhartipay.wallet.user.persistence.vo;

import java.io.Serializable;

public class SenderLedgerBean implements Serializable{

	
	private String ptytransdt;
	private String senderid;
	private String id;
	private String narrartion;
	private String mobileno;
	private String accountno;
	private String ifsccode;
	private double cramount;
	private double dramount;
	private String bankrrn;
	private String remark;
	private int isbank;
	private String status;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getIsbank() {
		return isbank;
	}
	public void setIsbank(int isbank) {
		this.isbank = isbank;
	}
	public String getPtytransdt() {
		return ptytransdt;
	}
	public void setPtytransdt(String ptytransdt) {
		this.ptytransdt = ptytransdt;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNarrartion() {
		return narrartion;
	}
	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public double getCramount() {
		return cramount;
	}
	public void setCramount(double cramount) {
		this.cramount = cramount;
	}
	public double getDramount() {
		return dramount;
	}
	public void setDramount(double dramount) {
		this.dramount = dramount;
	}
	public String getBankrrn() {
		return bankrrn;
	}
	public void setBankrrn(String bankrrn) {
		this.bankrrn = bankrrn;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
}
