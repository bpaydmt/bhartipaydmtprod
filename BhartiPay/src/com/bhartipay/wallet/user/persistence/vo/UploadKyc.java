package com.bhartipay.wallet.user.persistence.vo;

import java.io.File;
import java.util.List;

import javax.persistence.Column;

public class UploadKyc {
	private String walletid;
	private String userId;
	
	private File myFile1;
	private String myFile1FileName;
	private String myFile1ContentType;
	
	
	
	
	private File myFile2;
	private String myFile2FileName;
	private String myFile2ContentType;
	
	
	
	
	 private int   addpkycid;
	 private String  addpdesc;
	 
	 private String  addpkycpic;
	 
	 
	 private int   idpkycid;
	 private String  idpdesc;
	 
	 private String  idpkycpic;
	 
	
	 private String userName ;
	private String mobileNo ;
	private String emailId ;
	 

	
	
	
	
	 
	 
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletId) {
		this.walletid = walletId;
	}

	public File getMyFile1() {
		return myFile1;
	}

	public void setMyFile1(File myFile1) {
		this.myFile1 = myFile1;
	}

	public File getMyFile2() {
		return myFile2;
	}

	public void setMyFile2(File myFile2) {
		this.myFile2 = myFile2;
	}

	
	public String getMyFile1FileName() {
		return myFile1FileName;
	}

	public void setMyFile1FileName(String myFile1FileName) {
		this.myFile1FileName = myFile1FileName;
	}

	public String getMyFile1ContentType() {
		return myFile1ContentType;
	}

	public void setMyFile1ContentType(String myFile1ContentType) {
		this.myFile1ContentType = myFile1ContentType;
	}

	public String getMyFile2FileName() {
		return myFile2FileName;
	}

	public void setMyFile2FileName(String myFile2FileName) {
		this.myFile2FileName = myFile2FileName;
	}

	public String getMyFile2ContentType() {
		return myFile2ContentType;
	}

	public void setMyFile2ContentType(String myFile2ContentType) {
		this.myFile2ContentType = myFile2ContentType;
	}

	public int getAddpkycid() {
		return addpkycid;
	}

	public void setAddpkycid(int addpkycid) {
		this.addpkycid = addpkycid;
	}

	public String getAddpdesc() {
		return addpdesc;
	}

	public void setAddpdesc(String addpdesc) {
		this.addpdesc = addpdesc;
	}

	public String getAddpkycpic() {
		return addpkycpic;
	}

	public void setAddpkycpic(String addpkycpic) {
		this.addpkycpic = addpkycpic;
	}

	public int getIdpkycid() {
		return idpkycid;
	}

	public void setIdpkycid(int idpkycid) {
		this.idpkycid = idpkycid;
	}

	public String getIdpdesc() {
		return idpdesc;
	}

	public void setIdpdesc(String idpdesc) {
		this.idpdesc = idpdesc;
	}

	public String getIdpkycpic() {
		return idpkycpic;
	}

	public void setIdpkycpic(String idpkycpic) {
		this.idpkycpic = idpkycpic;
	}
	
	
	
	
}
