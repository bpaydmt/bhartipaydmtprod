package com.bhartipay.wallet.user.persistence.vo;

import java.io.File;
import java.util.Date;


public class UploadSenderKyc {

	private String kycId;
	private String walletid;
	private String senderId;
	private String agentId;
	private String aggreagatorId;
	
	
	private String addressProofType;
	private String addressProof;
	private String addressProofUrl;
	private File myFile1;
	private String myFile1FileName;
	private String myFile1ContentType;
	
	
	
	private String idProofType;
	private String idProof;
	private String idProofUrl;
	private File myFile2;
	private String myFile2FileName;
	private String myFile2ContentType;
	
	
	 private String  addressDesc;
	 private String  idDesc;
	 
	 
	
	private String senderName ;
	private String senderAddress;
	private String senderMobileNo ;
	private String senderEmailId ;
	

	private Date requestDate;
	private Date approveDate;
	private String statusCode;
	private String statusMessage;
	private String stDate;
	private String endDate;
	
	private String finalStatus;
	private String remark;
	
	
	
	
	
	public String getSenderAddress() {
		return senderAddress;
	}
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFinalStatus() {
		return finalStatus;
	}
	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}
	public String getKycId() {
		return kycId;
	}
	public void setKycId(String kycId) {
		this.kycId = kycId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public String getStDate() {
		return stDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getAddressProof() {
		return addressProof;
	}
	public String getIdProof() {
		return idProof;
	}
	public void setAddressProof(String addressProof) {
		this.addressProof = addressProof;
	}
	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}
	public String getWalletid() {
		return walletid;
	}
	public String getSenderId() {
		return senderId;
	}
	public String getAgentId() {
		return agentId;
	}
	public String getAggreagatorId() {
		return aggreagatorId;
	}
	public String getAddressProofType() {
		return addressProofType;
	}
	public String getAddressProofUrl() {
		return addressProofUrl;
	}
	public File getMyFile1() {
		return myFile1;
	}
	public String getMyFile1FileName() {
		return myFile1FileName;
	}
	public String getMyFile1ContentType() {
		return myFile1ContentType;
	}
	public String getIdProofType() {
		return idProofType;
	}
	public String getIdProofUrl() {
		return idProofUrl;
	}
	public File getMyFile2() {
		return myFile2;
	}
	public String getMyFile2FileName() {
		return myFile2FileName;
	}
	public String getMyFile2ContentType() {
		return myFile2ContentType;
	}
	public String getAddressDesc() {
		return addressDesc;
	}
	public String getIdDesc() {
		return idDesc;
	}
	public String getSenderName() {
		return senderName;
	}
	
	public String getSenderEmailId() {
		return senderEmailId;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public void setAggreagatorId(String aggreagatorId) {
		this.aggreagatorId = aggreagatorId;
	}
	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}
	public void setAddressProofUrl(String addressProofUrl) {
		this.addressProofUrl = addressProofUrl;
	}
	public void setMyFile1(File myFile1) {
		this.myFile1 = myFile1;
	}
	public void setMyFile1FileName(String myFile1FileName) {
		this.myFile1FileName = myFile1FileName;
	}
	public void setMyFile1ContentType(String myFile1ContentType) {
		this.myFile1ContentType = myFile1ContentType;
	}
	public void setIdProofType(String idProofType) {
		this.idProofType = idProofType;
	}
	public void setIdProofUrl(String idProofUrl) {
		this.idProofUrl = idProofUrl;
	}
	public void setMyFile2(File myFile2) {
		this.myFile2 = myFile2;
	}
	public void setMyFile2FileName(String myFile2FileName) {
		this.myFile2FileName = myFile2FileName;
	}
	public void setMyFile2ContentType(String myFile2ContentType) {
		this.myFile2ContentType = myFile2ContentType;
	}
	public void setAddressDesc(String addressDesc) {
		this.addressDesc = addressDesc;
	}
	public void setIdDesc(String idDesc) {
		this.idDesc = idDesc;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}
	public String getSenderMobileNo() {
		return senderMobileNo;
	}
	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}
	 
	
	

}
