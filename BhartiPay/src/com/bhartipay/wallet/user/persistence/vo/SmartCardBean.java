package com.bhartipay.wallet.user.persistence.vo;

import java.sql.Date;

import javax.persistence.Column;



public class SmartCardBean {
	
	
private String reqId;
	
	private String cardType;
	private String userId;
	private String walletId;
	private String aggreatorId;	
	private String mobileNo;
	private String emailId;
	private String name;
	private String lastName;
	private String expDate;
	private String preferredName;
	private String passowrd;
	private int countryCode;
	
	private String status;	
	private String reqDate;
    private String statusCode;
    private String blockedType;
    
    private String idType;
    private String idNumber; 
    private String countryOfIssue;
    private String nationality; 
    private String gender;
    private String title;
    private String statusDesc;
    
    private String type;
    private String address_1;
    private String address_2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String changePinMode;
    private String cvv;
    
    
    
    
    
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getChangePinMode() {
		return changePinMode;
	}
	public void setChangePinMode(String changePinMode) {
		this.changePinMode = changePinMode;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAddress_1() {
		return address_1;
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getAddress_2() {
		return address_2;
	}
	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getCountryOfIssue() {
		return countryOfIssue;
	}
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBlockedType() {
		return blockedType;
	}
	public void setBlockedType(String blockedType) {
		this.blockedType = blockedType;
	}
	private String prePaidId;	
	private String prePaidStatus;
	private String prePaidError;

	private String prePaidCardNumber;
	private String prePaidCardPin;
    
	

	
	
    
	public String getPrePaidCardNumber() {
		return prePaidCardNumber;
	}
	public void setPrePaidCardNumber(String prePaidCardNumber) {
		this.prePaidCardNumber = prePaidCardNumber;
	}
	public String getPrePaidCardPin() {
		return prePaidCardPin;
	}
	public void setPrePaidCardPin(String prePaidCardPin) {
		this.prePaidCardPin = prePaidCardPin;
	}
	public String getPrePaidId() {
		return prePaidId;
	}
	public void setPrePaidId(String prePaidId) {
		this.prePaidId = prePaidId;
	}
	public String getPrePaidStatus() {
		return prePaidStatus;
	}
	public void setPrePaidStatus(String prePaidStatus) {
		this.prePaidStatus = prePaidStatus;
	}
	public String getPrePaidError() {
		return prePaidError;
	}
	public void setPrePaidError(String prePaidError) {
		this.prePaidError = prePaidError;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public String getPassowrd() {
		return passowrd;
	}
	public void setPassowrd(String passowrd) {
		this.passowrd = passowrd;
	}
	
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    
    
    
    
    

}
