package com.bhartipay.wallet.user.persistence.vo;


public class UserMenuMapping {
	private String userId;
	private String menuId;
	private String[] menuType;
	private String actionResult;
	
	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String[] getMenuType() {
		return menuType;
	}

	public void setMenuType(String[] menuType) {
		this.menuType = menuType;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	
	
	
	
	
	
	
	
	
	
}
