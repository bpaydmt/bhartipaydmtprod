package com.bhartipay.wallet.user.persistence.vo;


import java.io.Serializable;
import java.math.BigDecimal;

public class AgentDetailsView implements Serializable{
	
	private String id;
	private String   mobileno;
	private String   emailid;
	private String   name;
	private String   dob;
	private int   usertype;
	private String   aggreatorid;
	private String   distributerid;
	private String   address1;
	private String   address2;
	private String   city;
	private String   state;
	private String   pin;
	private String   pan;
	private String   addressprooftype;
	private String   adhar;
	private String   agenttype;
	private String   shopname;
	private String   shopaddress1;
	private String   shopaddress2;
	private String   shopcity;
	private String   shopstate;
	private String   shoppin;
	private BigDecimal   paymentamount;
	private String   paymentdate;
	private String  paymentmode;
	private String   bankname;
	private String   formlocation;
	 private String  idlocation;
	 private String   addresslocation;
	 
	//added for view details
	 private String distributerName;
	 private String territory;
	 private String managername;
	 private String soname;
	 private String accountnumber;
	 private String ifsccode;
	 private String distributorMobileNo;
	 
	 private String passbookLoc;
	 private String paymentLoc;
	 private String createdby;
	 
	 private String salesId;
	 private String managerId;
	
	public String getTerritory() {
		return territory;
	}
	public void setTerritory(String territory) {
		this.territory = territory;
	}
	public String getManagername() {
		return managername;
	}
	public void setManagername(String managername) {
		this.managername = managername;
	}
	public String getSoname() {
		return soname;
	}
	public void setSoname(String soname) {
		this.soname = soname;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public String getDistributerName() {
		return distributerName;
	}
	public void setDistributerName(String distributerName) {
		this.distributerName = distributerName;
	}
	public String getFormlocation() {
		return formlocation;
	}
	public void setFormlocation(String formlocation) {
		this.formlocation = formlocation;
	}
	public String getIdlocation() {
		return idlocation;
	}
	public void setIdlocation(String idlocation) {
		this.idlocation = idlocation;
	}
	public String getAddresslocation() {
		return addresslocation;
	}
	public void setAddresslocation(String addresslocation) {
		this.addresslocation = addresslocation;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getDistributerid() {
		return distributerid;
	}
	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getAddressprooftype() {
		return addressprooftype;
	}
	public void setAddressprooftype(String addressprooftype) {
		this.addressprooftype = addressprooftype;
	}
	public String getAdhar() {
		return adhar;
	}
	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}
	public String getAgenttype() {
		return agenttype;
	}
	public void setAgenttype(String agenttype) {
		this.agenttype = agenttype;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public String getShopaddress1() {
		return shopaddress1;
	}
	public void setShopaddress1(String shopaddress1) {
		this.shopaddress1 = shopaddress1;
	}
	public String getShopaddress2() {
		return shopaddress2;
	}
	public void setShopaddress2(String shopaddress2) {
		this.shopaddress2 = shopaddress2;
	}
	public String getShopcity() {
		return shopcity;
	}
	public void setShopcity(String shopcity) {
		this.shopcity = shopcity;
	}
	public String getShopstate() {
		return shopstate;
	}
	public void setShopstate(String shopstate) {
		this.shopstate = shopstate;
	}
	public String getShoppin() {
		return shoppin;
	}
	public void setShoppin(String shoppin) {
		this.shoppin = shoppin;
	}
	
	
	
	public BigDecimal getPaymentamount() {
		return paymentamount;
	}
	public void setPaymentamount(BigDecimal paymentamount) {
		this.paymentamount = paymentamount;
	}
	public String getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}
	public String getPaymentmode() {
		return paymentmode;
	}
	public void setPaymentmode(String paymentmode) {
		this.paymentmode = paymentmode;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getDistributorMobileNo() {
		return distributorMobileNo;
	}
	public void setDistributorMobileNo(String distributorMobileNo) {
		this.distributorMobileNo = distributorMobileNo;
	}
	public String getPassbookLoc() {
		return passbookLoc;
	}
	public void setPassbookLoc(String passbookLoc) {
		this.passbookLoc = passbookLoc;
	}
	public String getPaymentLoc() {
		return paymentLoc;
	}
	public void setPaymentLoc(String paymentLoc) {
		this.paymentLoc = paymentLoc;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getSalesId() {
		return salesId;
	}
	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
	
	
	
	
	
	
	

}
