package com.bhartipay.wallet.user.persistence.vo;

public class UserRoleMapping {
	
	
	
	private String userId;
	private String roleId;
	private String[] roleType;
	private String actionResult;
	private double amount;
	
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String[] getRoleType() {
		return roleType;
	}
	public void setRoleType(String[] roleType) {
		this.roleType = roleType;
	}
	public String getActionResult() {
		return actionResult;
	}
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	
	
	
	
	
	

}
