package com.bhartipay.wallet.user.persistence.vo;

import java.io.File;

import javax.persistence.Column;


	public class WalletConfiguration {
	private int countryid;//
	private String appname;//
	private String protocol;
	private String country;//
	private String countrycurrency ;//not update
	private int smssend;//1 active 0 inactive
	private int kycvalid;// same 
	private int emailvalid;// same
	private int otpsendtomail;// same
	private int reqAgentApproval;// same
	private int status;//
	private String actionResult;
	
	private String aggreatorid;
	private String logopic;
	private String bannerpic;
	private String favicon;
	
	
	private File logo;
	private String logoFileName;
	private String logoContentType;	
	
	
	private File banner;
	private String bannerFileName;
	private String bannerContentType;
	
	private String domainName ;
	private String[] txnType;
	
	private String themes ;
	private String gcmAppKey;
	private String caption;

	 private String pgAgId;
	 private String pgMid;
	 private String pgUrl;
	 private String pgEncKey;
	 private String pgTxnType="SALE";
	 private String pgCountry;
	 private String pgCurrency;
	 private String pgCallBackUrl;
	 private String  countryCode;
	
		private String retailerAppUrl;
		private String distributorAppUrl;
		
		public String getRetailerAppUrl() {
			return retailerAppUrl;
		}


		public void setRetailerAppUrl(String retailerAppUrl) {
			this.retailerAppUrl = retailerAppUrl;
		}


		public String getDistributorAppUrl() {
			return distributorAppUrl;
		}


		public void setDistributorAppUrl(String distributorAppUrl) {
			this.distributorAppUrl = distributorAppUrl;
		}


	
	public String getFavicon() {
		return favicon;
	}


	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}


	public String getProtocol() {
		return protocol;
	}


	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getPgCallBackUrl() {
		return pgCallBackUrl;
	}


	public void setPgCallBackUrl(String pgCallBackUrl) {
		this.pgCallBackUrl = pgCallBackUrl;
	}


	public String getPgAgId() {
		return pgAgId;
	}


	public void setPgAgId(String pgAgId) {
		this.pgAgId = pgAgId;
	}


	public String getPgMid() {
		return pgMid;
	}


	public void setPgMid(String pgMid) {
		this.pgMid = pgMid;
	}


	public String getPgUrl() {
		return pgUrl;
	}


	public void setPgUrl(String pgUrl) {
		this.pgUrl = pgUrl;
	}


	public String getPgEncKey() {
		return pgEncKey;
	}


	public void setPgEncKey(String pgEncKey) {
		this.pgEncKey = pgEncKey;
	}


	public String getPgTxnType() {
		return pgTxnType;
	}


	public void setPgTxnType(String pgTxnType) {
		this.pgTxnType = pgTxnType;
	}


	public String getPgCountry() {
		return pgCountry;
	}


	public void setPgCountry(String pgCountry) {
		this.pgCountry = pgCountry;
	}


	public String getPgCurrency() {
		return pgCurrency;
	}


	public void setPgCurrency(String pgCurrency) {
		this.pgCurrency = pgCurrency;
	}


	public String getCaption() {
		return caption;
	}


	public void setCaption(String caption) {
		this.caption = caption;
	}


	public String getGcmAppKey() {
		return gcmAppKey;
	}


	public void setGcmAppKey(String gcmAppKey) {
		this.gcmAppKey = gcmAppKey;
	}


	public String getThemes() {
		return themes;
	}


	public void setThemes(String themes) {
		this.themes = themes;
	}


	public String[] getTxnType() {
		return txnType;
	}


	public void setTxnType(String[] txnType) {
		this.txnType = txnType;
	}


	public String getDomainName() {
		return domainName;
	}


	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}


	public String getAggreatorid() {
		return aggreatorid;
	}


	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}


	public String getLogopic() {
		return logopic;
	}


	public void setLogopic(String logopic) {
		this.logopic = logopic;
	}


	public String getBannerpic() {
		return bannerpic;
	}


	public void setBannerpic(String bannerpic) {
		this.bannerpic = bannerpic;
	}


	public File getLogo() {
		return logo;
	}


	public void setLogo(File logo) {
		this.logo = logo;
	}


	public String getLogoFileName() {
		return logoFileName;
	}


	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}


	public String getLogoContentType() {
		return logoContentType;
	}


	public void setLogoContentType(String logoContentType) {
		this.logoContentType = logoContentType;
	}


	public File getBanner() {
		return banner;
	}


	public void setBanner(File banner) {
		this.banner = banner;
	}


	public String getBannerFileName() {
		return bannerFileName;
	}


	public void setBannerFileName(String bannerFileName) {
		this.bannerFileName = bannerFileName;
	}


	public String getBannerContentType() {
		return bannerContentType;
	}


	public void setBannerContentType(String bannerContentType) {
		this.bannerContentType = bannerContentType;
	}


	public String getActionResult() {
		return actionResult;
	}


	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}


	public int getReqAgentApproval() {
		return reqAgentApproval;
	}


	public void setReqAgentApproval(int reqAgentApproval) {
		this.reqAgentApproval = reqAgentApproval;
	}


	public String getAppname() {
		return appname;
	}


	public void setAppname(String appname) {
		this.appname = appname;
	}


	public int getSmssend() {
		return smssend;
	}


	public void setSmssend(int smssend) {
		this.smssend = smssend;
	}


	public int getCountryid() {
		return countryid;
	}


	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}





	public String getCountrycurrency() {
		return countrycurrency;
	}


	public void setCountrycurrency(String countrycurrency) {
		this.countrycurrency = countrycurrency;
	}





	public int getKycvalid() {
		return kycvalid;
	}


	public void setKycvalid(int kycvalid) {
		this.kycvalid = kycvalid;
	}


	public int getEmailvalid() {
		return emailvalid;
	}


	public void setEmailvalid(int emailvalid) {
		this.emailvalid = emailvalid;
	}


	public int getOtpsendtomail() {
		return otpsendtomail;
	}


	public void setOtpsendtomail(int otpsendtomail) {
		this.otpsendtomail = otpsendtomail;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	
	
	

}
