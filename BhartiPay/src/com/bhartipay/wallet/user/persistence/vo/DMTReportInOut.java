package com.bhartipay.wallet.user.persistence.vo;

import java.util.List;

public class DMTReportInOut {

	private String agentId;
	private String stDate; 
	private String edDate;
	private String mobileNo;
	private String accountno;
	private String senderid;
	private String txnid;
    private String mpin;
    private String statuscode;
    private String statusmsg;
    private double usertype;
    private String aggregatorId;
    
	private List <TransacationLedgerBean> transList;
	private List <AgentLedgerBean> agentLedger;
	private List <SenderLedgerBean> senderLedgerList;
	
	
	
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	public Double getUsertype() {
		return usertype;
	}
	public void setUsertype(double usertype) {
		this.usertype = usertype;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public String getStatusmsg() {
		return statusmsg;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public void setStatusmsg(String statusmsg) {
		this.statusmsg = statusmsg;
	}
	public String getSenderid() {
		return senderid;
	}
	public String getTxnid() {
		return txnid;
	}
	public String getMpin() {
		return mpin;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public void setMpin(String mpin) {
		this.mpin = mpin;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getEdDate() {
		return edDate;
	}
	public void setEdDate(String edDate) {
		this.edDate = edDate;
	}
	public List<TransacationLedgerBean> getTransList() {
		return transList;
	}
	public void setTransList(List<TransacationLedgerBean> transList) {
		this.transList = transList;
	}
	public List<AgentLedgerBean> getAgentLedger() {
		return agentLedger;
	}
	public void setAgentLedger(List<AgentLedgerBean> agentLedger) {
		this.agentLedger = agentLedger;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public List<SenderLedgerBean> getSenderLedgerList() {
		return senderLedgerList;
	}
	public void setSenderLedgerList(List<SenderLedgerBean> senderLedgerList) {
		this.senderLedgerList = senderLedgerList;
	}
	
	
	
	
}
