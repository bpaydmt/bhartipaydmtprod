package com.bhartipay.wallet.user.persistence.vo;


public class SMSConfigMast {
	
	private double usertype;
	private String aggreatorid;
	private String smsurl;
	private String smsfeedid;
	private String smsusername;
	private String smspassword;
	private String smssenderid;
	private String actionResult;
	
	private String agentid;
	
	
	
	
	
	
	
	
	

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public double getUsertype() {
		return usertype;
	}

	public void setUsertype(double usertype) {
		this.usertype = usertype;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public String getSmsurl() {
		return smsurl;
	}

	public void setSmsurl(String smsurl) {
		this.smsurl = smsurl;
	}

	public String getSmsfeedid() {
		return smsfeedid;
	}

	public void setSmsfeedid(String smsfeedid) {
		this.smsfeedid = smsfeedid;
	}

	public String getSmsusername() {
		return smsusername;
	}

	public void setSmsusername(String smsusername) {
		this.smsusername = smsusername;
	}

	public String getSmspassword() {
		return smspassword;
	}

	public void setSmspassword(String smspassword) {
		this.smspassword = smspassword;
	}

	public String getSmssenderid() {
		return smssenderid;
	}

	public void setSmssenderid(String smssenderid) {
		this.smssenderid = smssenderid;
	}
	
	
	
	
	
	
	
	
}
