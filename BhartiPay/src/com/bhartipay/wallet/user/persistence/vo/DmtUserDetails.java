package com.bhartipay.wallet.user.persistence.vo;

public class DmtUserDetails {

private String lastName;
private String preferredName;
private String status;
private String email;
private String activeDate;
private String mobileNumber;
private String firstName;
private String fundAvailable;
private String fundWithholding;
private String cardNumber;
private String holderName;

private String reqId;
private String cardCodeName;
private String cardName;
private String description;

private String holdaerName;
private String expiry;



public String getHoldaerName() {
	return holdaerName;
}
public void setHoldaerName(String holdaerName) {
	this.holdaerName = holdaerName;
}
public String getExpiry() {
	return expiry;
}
public void setExpiry(String expiry) {
	this.expiry = expiry;
}
public String getReqId() {
	return reqId;
}
public void setReqId(String reqId) {
	this.reqId = reqId;
}
public String getCardCodeName() {
	return cardCodeName;
}
public void setCardCodeName(String cardCodeName) {
	this.cardCodeName = cardCodeName;
}
public String getCardName() {
	return cardName;
}
public void setCardName(String cardName) {
	this.cardName = cardName;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}


public String getFundAvailable() {
	return fundAvailable;
}
public void setFundAvailable(String fundAvailable) {
	this.fundAvailable = fundAvailable;
}
public String getFundWithholding() {
	return fundWithholding;
}
public void setFundWithholding(String fundWithholding) {
	this.fundWithholding = fundWithholding;
}
public String getCardNumber() {
	return cardNumber;
}
public void setCardNumber(String cardNumber) {
	this.cardNumber = cardNumber;
}
public String getHolderName() {
	return holderName;
}
public void setHolderName(String holderName) {
	this.holderName = holderName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getPreferredName() {
	return preferredName;
}
public void setPreferredName(String preferredName) {
	this.preferredName = preferredName;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getActiveDate() {
	return activeDate;
}
public void setActiveDate(String activeDate) {
	this.activeDate = activeDate;
}
public String getMobileNumber() {
	return mobileNumber;
}
public void setMobileNumber(String mobileNumber) {
	this.mobileNumber = mobileNumber;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}

}
