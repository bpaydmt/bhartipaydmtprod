package com.bhartipay.wallet.user.persistence.vo;

import java.io.Serializable;

public class DmtDetailsMastBean implements Serializable{
	
	private String senderid ;
	private String agentid;
	private String txnid;
	private String sendermobileno;
	private String sendername;
	private String benemobileno;
	private String beneaccountno;
	private String beneifsccode;
	private double txnamount;
	private String remittancetype;
	private String remittancemode;
	private String remittancestatus;
	private String paymentinfo;
	private String txndatetime;
	private String banktranrefno;
	private String bankrrn;
	private String bankresponse;
	private String beneid;
	private String updatedatetime;
	private String kyctype;
	private String distributerid;
	private String aggreatorid;
	private String CHECKSUMHASH;
	private String name;
	private String bankname;
	
	
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}
	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	public String getDistributerid() {
		return distributerid;
	}
	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getSendermobileno() {
		return sendermobileno;
	}
	public void setSendermobileno(String sendermobileno) {
		this.sendermobileno = sendermobileno;
	}
	public String getSendername() {
		return sendername;
	}
	public void setSendername(String sendername) {
		this.sendername = sendername;
	}
	public String getBenemobileno() {
		return benemobileno;
	}
	public void setBenemobileno(String benemobileno) {
		this.benemobileno = benemobileno;
	}
	public String getBeneaccountno() {
		return beneaccountno;
	}
	public void setBeneaccountno(String beneaccountno) {
		this.beneaccountno = beneaccountno;
	}
	public String getBeneifsccode() {
		return beneifsccode;
	}
	public void setBeneifsccode(String beneifsccode) {
		this.beneifsccode = beneifsccode;
	}
	public double getTxnamount() {
		return txnamount;
	}
	public void setTxnamount(double txnamount) {
		this.txnamount = txnamount;
	}
	public String getRemittancetype() {
		return remittancetype;
	}
	public void setRemittancetype(String remittancetype) {
		this.remittancetype = remittancetype;
	}
	public String getRemittancemode() {
		return remittancemode;
	}
	public void setRemittancemode(String remittancemode) {
		this.remittancemode = remittancemode;
	}
	public String getRemittancestatus() {
		return remittancestatus;
	}
	public void setRemittancestatus(String remittancestatus) {
		this.remittancestatus = remittancestatus;
	}
	public String getPaymentinfo() {
		return paymentinfo;
	}
	public void setPaymentinfo(String paymentinfo) {
		this.paymentinfo = paymentinfo;
	}
	public String getTxndatetime() {
		return txndatetime;
	}
	public void setTxndatetime(String txndatetime) {
		this.txndatetime = txndatetime;
	}
	public String getBanktranrefno() {
		return banktranrefno;
	}
	public void setBanktranrefno(String banktranrefno) {
		this.banktranrefno = banktranrefno;
	}
	public String getBankrrn() {
		return bankrrn;
	}
	public void setBankrrn(String bankrrn) {
		this.bankrrn = bankrrn;
	}
	public String getBankresponse() {
		return bankresponse;
	}
	public void setBankresponse(String bankresponse) {
		this.bankresponse = bankresponse;
	}
	public String getBeneid() {
		return beneid;
	}
	public void setBeneid(String beneid) {
		this.beneid = beneid;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}
	public String getKyctype() {
		return kyctype;
	}
	public void setKyctype(String kyctype) {
		this.kyctype = kyctype;
	}
	
	
	

}
