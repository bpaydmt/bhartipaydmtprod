package com.bhartipay.wallet.user.persistence.vo;

public class SenderProfileBean {
	
	private String userid;
	private String payermobile;
	private String payername;
	private String payerEmail;
	private double amount;
	
	
	
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPayermobile() {
		return payermobile;
	}
	public void setPayermobile(String payermobile) {
		this.payermobile = payermobile;
	}
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public String getPayerEmail() {
		return payerEmail;
	}
	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	
	
	
	
	

}
