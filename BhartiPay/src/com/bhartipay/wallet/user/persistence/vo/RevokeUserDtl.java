package com.bhartipay.wallet.user.persistence.vo;

import java.util.Date;

import javax.persistence.Column;


public class RevokeUserDtl {

	private String id;
	
	private String aggreatorId;
	
	private String walletId;
	
	private String userId;
	
	private String requestedBy;
	
	private double amount;
	
	private String approverId;
	
	private String requesterRemark;
	
	private String approverRemark;
	
	private Date requestDate;
	
	private Date approveDate;
	
	private String status;

	public String getRequesterRemark() {
		return requesterRemark;
	}

	public void setRequesterRemark(String requesterRemark) {
		this.requesterRemark = requesterRemark;
	}

	public String getApproverRemark() {
		return approverRemark;
	}

	public void setApproverRemark(String approverRemark) {
		this.approverRemark = approverRemark;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getApproverId() {
		return approverId;
	}

	public void setApproverId(String approverId) {
		this.approverId = approverId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
