package com.bhartipay.wallet.user.persistence.vo;



import java.sql.Date;

import javax.persistence.Column;
public class WalletKYCBean {

	private String refid;
	private String UserId;
	private int addprofkycid;
	private String addprofdesc;
    private String addprofkycpic;
	private int idprofkycid;
	private String idprofdesc;
    private String idprofkycpic;
	private String status;
	private Date loaddate;
	
	private String userName ;
	private String mobileNo ;
	private String emailId ;
	


	
	
	
	
	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getRefid() {
		return refid;
	}


	public void setRefid(String refid) {
		this.refid = refid;
	}


	


	public String getUserId() {
		return UserId;
	}


	public void setUserId(String userId) {
		UserId = userId;
	}


	public int getAddprofkycid() {
		return addprofkycid;
	}


	public void setAddprofkycid(int addprofkycid) {
		this.addprofkycid = addprofkycid;
	}


	public String getAddprofdesc() {
		return addprofdesc;
	}


	public void setAddprofdesc(String addprofdesc) {
		this.addprofdesc = addprofdesc;
	}




	public int getIdprofkycid() {
		return idprofkycid;
	}


	public void setIdprofkycid(int idprofkycid) {
		this.idprofkycid = idprofkycid;
	}


	public String getIdprofdesc() {
		return idprofdesc;
	}


	public void setIdprofdesc(String idprofdesc) {
		this.idprofdesc = idprofdesc;
	}


	

	public String getAddprofkycpic() {
		return addprofkycpic;
	}


	public void setAddprofkycpic(String addprofkycpic) {
		this.addprofkycpic = addprofkycpic;
	}


	public String getIdprofkycpic() {
		return idprofkycpic;
	}


	public void setIdprofkycpic(String idprofkycpic) {
		this.idprofkycpic = idprofkycpic;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getLoaddate() {
		return loaddate;
	}


	public void setLoaddate(Date loaddate) {
		this.loaddate = loaddate;
	}

}
