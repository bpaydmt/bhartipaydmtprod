package com.bhartipay.wallet.user.persistence.vo;


import java.io.Serializable;
import java.util.List;



public class FundTransactionSummaryBean implements Serializable{
	
	private String senderMobile;
	private String beneficiaryName;
	private String beneficiaryIFSC;
	private String beneficiaryAccNo;
	private double amount;
	private String charges;
	private String mode;
	private List<MudraMoneyTransactionBean> mudraMoneyTransactionBean;
	private String statusCode;
	private String statusDesc;
	private double agentWalletAmount;
	private double senderLimit;
	private String bankName;
	private String agentName;
	private String agentMobileNo;
	private String shopName;
	
	private String ptDate;
	
	
	
	
	
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentMobileNo() {
		return agentMobileNo;
	}
	public void setAgentMobileNo(String agentMobileNo) {
		this.agentMobileNo = agentMobileNo;
	}
	public double getSenderLimit() {
		return senderLimit;
	}
	public void setSenderLimit(double senderLimit) {
		this.senderLimit = senderLimit;
	}
	public double getAgentWalletAmount() {
		return agentWalletAmount;
	}
	public void setAgentWalletAmount(double agentWalletAmount) {
		this.agentWalletAmount = agentWalletAmount;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryIFSC() {
		return beneficiaryIFSC;
	}
	public void setBeneficiaryIFSC(String beneficiaryIFSC) {
		this.beneficiaryIFSC = beneficiaryIFSC;
	}
	public String getBeneficiaryAccNo() {
		return beneficiaryAccNo;
	}
	public void setBeneficiaryAccNo(String beneficiaryAccNo) {
		this.beneficiaryAccNo = beneficiaryAccNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public List<MudraMoneyTransactionBean> getMudraMoneyTransactionBean() {
		return mudraMoneyTransactionBean;
	}
	public void setMudraMoneyTransactionBean(List<MudraMoneyTransactionBean> mudraMoneyTransactionBean) {
		this.mudraMoneyTransactionBean = mudraMoneyTransactionBean;
	}
	
	
	public String getPtDate() {
		return ptDate;
	}
	public void setPtDate(String ptDate) {
		this.ptDate = ptDate;
	}
	
	
	
	
	
	
	
	
	
	

}
