package com.bhartipay.wallet.user.persistence.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coupons {
	@SerializedName("featured")
	@Expose
	private String featured;
	@SerializedName("exclusive")
	@Expose
	private String exclusive;
	@SerializedName("promo_id")
	@Expose
	private String promoId;
	@SerializedName("offer_id")
	@Expose
	private String offerId;
	@SerializedName("offer_name")
	@Expose
	private String offerName;
	@SerializedName("coupon_title")
	@Expose
	private String couponTitle;
	@SerializedName("category")
	@Expose
	private String category;
	@SerializedName("coupon_description")
	@Expose
	private String couponDescription;
	@SerializedName("coupon_type")
	@Expose
	private String couponType;
	@SerializedName("coupon_code")
	@Expose
	private String couponCode;
	@SerializedName("ref_id")
	@Expose
	private String refId;
	@SerializedName("link")
	@Expose
	private String link;
	@SerializedName("coupon_expiry")
	@Expose
	private String couponExpiry;
	@SerializedName("added")
	@Expose
	private String added;
	@SerializedName("preview_url")
	@Expose
	private String previewUrl;
	@SerializedName("store_link")
	@Expose
	private String storeLink;
	@SerializedName("store_image")
	@Expose
	private String storeImage;

	/**
	* 
	* @return
	* The featured
	*/
	public String getFeatured() {
	return featured;
	}

	/**
	* 
	* @param featured
	* The featured
	*/
	public void setFeatured(String featured) {
	this.featured = featured;
	}

	/**
	* 
	* @return
	* The exclusive
	*/
	public String getExclusive() {
	return exclusive;
	}

	/**
	* 
	* @param exclusive
	* The exclusive
	*/
	public void setExclusive(String exclusive) {
	this.exclusive = exclusive;
	}

	/**
	* 
	* @return
	* The promoId
	*/
	public String getPromoId() {
	return promoId;
	}

	/**
	* 
	* @param promoId
	* The promo_id
	*/
	public void setPromoId(String promoId) {
	this.promoId = promoId;
	}

	/**
	* 
	* @return
	* The offerId
	*/
	public String getOfferId() {
	return offerId;
	}

	/**
	* 
	* @param offerId
	* The offer_id
	*/
	public void setOfferId(String offerId) {
	this.offerId = offerId;
	}

	/**
	* 
	* @return
	* The offerName
	*/
	public String getOfferName() {
	return offerName;
	}

	/**
	* 
	* @param offerName
	* The offer_name
	*/
	public void setOfferName(String offerName) {
	this.offerName = offerName;
	}

	/**
	* 
	* @return
	* The couponTitle
	*/
	public String getCouponTitle() {
	return couponTitle;
	}

	/**
	* 
	* @param couponTitle
	* The coupon_title
	*/
	public void setCouponTitle(String couponTitle) {
	this.couponTitle = couponTitle;
	}

	/**
	* 
	* @return
	* The category
	*/
	public String getCategory() {
	return category;
	}

	/**
	* 
	* @param category
	* The category
	*/
	public void setCategory(String category) {
	this.category = category;
	}

	/**
	* 
	* @return
	* The couponDescription
	*/
	public String getCouponDescription() {
	return couponDescription;
	}

	/**
	* 
	* @param couponDescription
	* The coupon_description
	*/
	public void setCouponDescription(String couponDescription) {
	this.couponDescription = couponDescription;
	}

	/**
	* 
	* @return
	* The couponType
	*/
	public String getCouponType() {
	return couponType;
	}

	/**
	* 
	* @param couponType
	* The coupon_type
	*/
	public void setCouponType(String couponType) {
	this.couponType = couponType;
	}

	/**
	* 
	* @return
	* The couponCode
	*/
	public String getCouponCode() {
	return couponCode;
	}

	/**
	* 
	* @param couponCode
	* The coupon_code
	*/
	public void setCouponCode(String couponCode) {
	this.couponCode = couponCode;
	}

	/**
	* 
	* @return
	* The refId
	*/
	public String getRefId() {
	return refId;
	}

	/**
	* 
	* @param refId
	* The ref_id
	*/
	public void setRefId(String refId) {
	this.refId = refId;
	}

	/**
	* 
	* @return
	* The link
	*/
	public String getLink() {
	return link;
	}

	/**
	* 
	* @param link
	* The link
	*/
	public void setLink(String link) {
	this.link = link;
	}

	/**
	* 
	* @return
	* The couponExpiry
	*/
	public String getCouponExpiry() {
	return couponExpiry;
	}

	/**
	* 
	* @param couponExpiry
	* The coupon_expiry
	*/
	public void setCouponExpiry(String couponExpiry) {
	this.couponExpiry = couponExpiry;
	}

	/**
	* 
	* @return
	* The added
	*/
	public String getAdded() {
	return added;
	}

	/**
	* 
	* @param added
	* The added
	*/
	public void setAdded(String added) {
	this.added = added;
	}

	/**
	* 
	* @return
	* The previewUrl
	*/
	public String getPreviewUrl() {
	return previewUrl;
	}

	/**
	* 
	* @param previewUrl
	* The preview_url
	*/
	public void setPreviewUrl(String previewUrl) {
	this.previewUrl = previewUrl;
	}

	/**
	* 
	* @return
	* The storeLink
	*/
	public String getStoreLink() {
	return storeLink;
	}

	/**
	* 
	* @param storeLink
	* The store_link
	*/
	public void setStoreLink(String storeLink) {
	this.storeLink = storeLink;
	}

	/**
	* 
	* @return
	* The storeImage
	*/
	public String getStoreImage() {
	return storeImage;
	}

	/**
	* 
	* @param storeImage
	* The store_image
	*/
	public void setStoreImage(String storeImage) {
	this.storeImage = storeImage;
	}

}
