package com.bhartipay.wallet.user.persistence.vo;

import java.io.Serializable;


public class UserBlockedBean implements Serializable{
	
	private String agentId;
	private String blockBy;
	private String declinedComment;
	private String status;
	
	
	
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getBlockBy() {
		return blockBy;
	}
	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}
	public String getDeclinedComment() {
		return declinedComment;
	}
	public void setDeclinedComment(String declinedComment) {
		this.declinedComment = declinedComment;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
