/**
 * 
 */
package com.bhartipay.wallet.user.persistence.vo;

import java.util.Date;
import java.util.Set;

import com.bhartipay.wallet.framework.persistence.vo.ValueObject;


/**
 * @author anshuman.nanda
 *
 */
public class UserSummary extends ValueObject {
	
	private static final long serialVersionUID = 109278426472223L;

	 private String walletid;	 
	 private String mobileno;	 
	 private String emailid;
	 private String oldpassword;
	 private String password;
	 private String confirmPassword;
	 private String name;	
	 private int documentType;
	 private String documentId;
	 private int usertype;	 
	 private String countrycode;	 
	 private String userstatus;
	 private Date creationDate;	 
	 private String aggreatorid; 
	 private String distributerid;	
	 private String agentid;	
	 private String subAgentId;
	
	 private String createdby;
	 private int loginStatus;	 
	 private Date lastlogintime;
	 private String userId;
	 private String otp;
	 private String blockStatus;
	 private String hceToken;
	 private String txnId;
	 
	 
		 
	
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getHceToken() {
		return hceToken;
	}
	public void setHceToken(String hceToken) {
		this.hceToken = hceToken;
	}
	public int getDocumentType() {
		return documentType;
	}
	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getBlockStatus() {
		return blockStatus;
	}
	public void setBlockStatus(String blockStatus) {
		this.blockStatus = blockStatus;
	}
	public String getOldpassword() {
		return oldpassword;
	}
	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}
	public String getSubAgentId() {
		return subAgentId;
	}
	public void setSubAgentId(String subAgentId) {
		this.subAgentId = subAgentId;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWalletid() {
		return walletid;
	}
	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emilid) {
		this.emailid = emilid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public String getUserstatus() {
		return userstatus;
	}
	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public String getDistributerid() {
		return distributerid;
	}
	public void setDistributerid(String distributerid) {
		this.distributerid = distributerid;
	}
	public String getAgentid() {
		return agentid;
	}
	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public int getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}
	public Date getLastlogintime() {
		return lastlogintime;
	}
	public void setLastlogintime(Date lastlogintime) {
		this.lastlogintime = lastlogintime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 
	
}
