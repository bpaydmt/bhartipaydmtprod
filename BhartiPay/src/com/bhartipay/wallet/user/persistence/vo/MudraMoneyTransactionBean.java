package com.bhartipay.wallet.user.persistence.vo;

import javax.persistence.Transient;

public class MudraMoneyTransactionBean {
	
	private String id;
	
	private String txnId;
	private String walletId;
	private String senderId;
	private String beneficiaryId;
	private double drAmount;
	private double crAmount;
	private String narrartion;
	private String status;
	private String bankRrn;
	private String ptytransdt;
	private double surChargeAmount;
	private String statusCode;
	private String statusDesc;
	private String ipiemi;
	private String agent;
	private String userId;
	private double txnAmount;
	private double verificationAmount;
	private String accHolderName;
	private String remark;
	private String verificationDesc;
	private String agentid;
	private String transType;
	private String CHECKSUMHASH;
	private double agentWalletAmount;
	private double senderLimit;
	
	private String name;
	private String bankName;
	private String accountNo;
	private String ifscCode;
	private String otp;
	private double agentRefund;
	private String aggreatorid;
	
	
	private String agentName;
	
	private String agentMobileNo;
	private String verifyStatus;
	
	public String getVerifyStatus() {
		return verifyStatus;
	}
	public void setVerifyStatus(String verifyStatus) {
		this.verifyStatus = verifyStatus;
	}
	public String getAggreatorid() {
		return aggreatorid;
	}
	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	public double getAgentRefund() {
		return agentRefund;
	}
	public void setAgentRefund(double agentRefund) {
		this.agentRefund = agentRefund;
	}
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentMobileNo() {
		return agentMobileNo;
	}

	public void setAgentMobileNo(String agentMobileNo) {
		this.agentMobileNo = agentMobileNo;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public double getAgentWalletAmount() {
		return agentWalletAmount;
	}

	public double getSenderLimit() {
		return senderLimit;
	}

	public void setAgentWalletAmount(double agentWalletAmount) {
		this.agentWalletAmount = agentWalletAmount;
	}

	public void setSenderLimit(double senderLimit) {
		this.senderLimit = senderLimit;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getVerificationDesc() {
		return verificationDesc;
	}

	public void setVerificationDesc(String verificationDesc) {
		this.verificationDesc = verificationDesc;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public double getVerificationAmount() {
		return verificationAmount;
	}

	public void setVerificationAmount(double verificationAmount) {
		this.verificationAmount = verificationAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public double getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(double txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBankRrn() {
		return bankRrn;
	}

	public void setBankRrn(String bankRrn) {
		this.bankRrn = bankRrn;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public double getDrAmount() {
		return drAmount;
	}

	public void setDrAmount(double drAmount) {
		this.drAmount = drAmount;
	}

	public double getCrAmount() {
		return crAmount;
	}

	public void setCrAmount(double crAmount) {
		this.crAmount = crAmount;
	}

	public String getNarrartion() {
		return narrartion;
	}

	public void setNarrartion(String narrartion) {
		this.narrartion = narrartion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public String getPtytransdt() {
		return ptytransdt;
	}

	public void setPtytransdt(String ptytransdt) {
		this.ptytransdt = ptytransdt;
	}

	public double getSurChargeAmount() {
		return surChargeAmount;
	}

	public void setSurChargeAmount(double surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getIpiemi() {
		return ipiemi;
	}

	public void setIpiemi(String ipiemi) {
		this.ipiemi = ipiemi;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	
	
	
	
	
	

}
