package com.bhartipay.wallet.user.persistence.vo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
public class MerchantOffersMast {
	private int id;
	private String aggreatorId;
	private String shopName;
	private String merchantId;
	private String terminalId;
	private int cashBack;
	
public MerchantOffersMast(){
	
}
	public MerchantOffersMast(String aggreatorId, String shopName, String merchantId, String terminalId, int cashBack) {
		super();
		this.aggreatorId = aggreatorId;
		this.shopName = shopName;
		this.merchantId = merchantId;
		this.terminalId = terminalId;
		this.cashBack = cashBack;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public int getCashBack() {
		return cashBack;
	}

	public void setCashBack(int cashBack) {
		this.cashBack = cashBack;
	}
	
	
	
}
