package com.bhartipay.wallet.user.service.bk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhartipay.biller.bean.WalletBean;
import com.bhartipay.wallet.recharge.bean.UserWishListBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.ResponseBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserWalletConfigBean;
import com.bhartipay.wallet.user.dao.PCUser;
import com.bhartipay.wallet.user.persistence.vo.AgentDetailsView;
import com.bhartipay.wallet.user.persistence.vo.BankDetailsBean;
import com.bhartipay.wallet.user.persistence.vo.BankMastBean;
import com.bhartipay.wallet.user.persistence.vo.Coupons;
import com.bhartipay.wallet.user.persistence.vo.CouponsBean;
import com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean;
import com.bhartipay.wallet.user.persistence.vo.DMTReportInOut;
import com.bhartipay.wallet.user.persistence.vo.DeclinedListBean;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.FundTransactionSummaryBean;
import com.bhartipay.wallet.user.persistence.vo.LoginResponse;
import com.bhartipay.wallet.user.persistence.vo.MailConfigMast;
import com.bhartipay.wallet.user.persistence.vo.MerchantOffersMast;
import com.bhartipay.wallet.user.persistence.vo.MudraBeneficiaryMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails;
import com.bhartipay.wallet.user.persistence.vo.RevenueReportBean;
import com.bhartipay.wallet.user.persistence.vo.SMSConfigMast;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.SenderProfileBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardOutputBean;
import com.bhartipay.wallet.user.persistence.vo.UploadKyc;
import com.bhartipay.wallet.user.persistence.vo.UserBlockedBean;
import com.bhartipay.wallet.user.persistence.vo.UserMenuMapping;
import com.bhartipay.wallet.user.persistence.vo.UserRoleMapping;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletKYCBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;


public interface UserService {

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getCountryCode();
	/**
	 * for getting country map.
	 * @return
	 */
	public Map<String, String> getCountry();
	
	/**
	 * 
	 * @param configuration
	 * @return
	 */
	public String getcurrencyByCountryId(WalletConfiguration configuration);
	
	/**
	 * 
	 * @param configuration
	 * @return
	 */
	public String getcurrencyByCountryCode(WalletConfiguration configuration);
	/**
	 * 
	 * @param value
	 * @return
	 */
	public String saveSignUpUser(UserSummary user,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param walletBean
	 * @return
	 */
	public String saveSignUpUserByAgent(WalletMastBean walletBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param walletBean
	 * @param ipimei
	 * @param userAgent
	 * @return
	 */
	
	public WalletMastBean saveAgentOnBoard(WalletMastBean walletBean,String ipimei,String userAgent);
	
	
	/**
	 * 
	 * @param walletBean
	 * @param ipimei
	 * @param userAgent
	 * @return
	 */
	
	public WalletMastBean updateAgentOnBoad(WalletMastBean walletBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param walletbeBean
	 * @return
	 */
	public String signUpUserByUpload(WalletMastBean walletbeBean,String ipimei,String userAgent);
	/**
	 * 
	 * @param user
	 * @return
	 */
	public LoginResponse login(UserSummary user,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param userId
	 * @param otp
	 * @return
	 */
	public String validateOTP(UserSummary user);
	
	/**
	 * 
	 * @param userId
	 * @param otp
	 * @return
	 */
	public String verifyOTP(UserSummary user);

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String otpResend(UserSummary user);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public Map<String,String> ProfilebyloginId(UserSummary user);
	
	/**
	 * 
	 * @param countyId
	 * @return
	 */
	public Map<String, String> getWalletConfig(WalletConfiguration config);
	
	
	
	/**
	 * 
	 * @param countyId
	 * @return
	 */
	public Map<String, String> getAepsUpStatus(String aggId);
	
	
	/**
	 * 
	 * @param config
	 * @return
	 */
	
	public String getAggIdByDomain(WalletConfiguration config);
	
	/**
	 * 
	 * @param configMast
	 * @return
	 */
	public MailConfigMast emailConfiguration(MailConfigMast configMast);
	
	/**
	 * 
	 * @param configMast
	 * @return
	 */
	public MailConfigMast saveMailConfiguration(MailConfigMast configMast,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param configMast
	 * @return
	 */
	public SMSConfigMast smsConfiguration(SMSConfigMast configMast);
	/**
	 * 
	 * @param configMast
	 * @return
	 */
	public SMSConfigMast saveSMSConfiguration(SMSConfigMast configMast,String ipimei,String userAgent);
	/**
	 * 
	 * @param txnId
	 * @return
	 */
	public Map<String, String> ProfilebytxnId(String txnId);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public WalletMastBean showUserProfile(String userId);
	
	/**
	 * 
	 * @param mastBean
	 * @return
	 */
	public WalletMastBean editProfile(WalletMastBean mastBean);
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getKyc();
	
	
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getKycId();
	
	/**
	 * 
	 * @param uploadKyc
	 * @return
	 */
	public String saveKycDocuments(UploadKyc uploadKyc,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param walletBean
	 * @return
	 */
	public String resetPassword(WalletMastBean walletBean);
	
	/**
	 * 
	 * @param agentId
	 * @return
	 */
	public Map<String, String> getCustomerByAgentId(double userType,String agentId);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public UserWalletConfigBean getWalletConfigByUserId(UserWalletConfigBean configBean);
	
	/**
	 * 
	 * @param config
	 * @return
	 */
	public UserWalletConfigBean saveLimit(UserWalletConfigBean config,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String getProfilePic(String userId);
	
	
	/**
	 * 
	 * @param txnId
	 * @return
	 */
	public PGPayeeBean profilebyPGtxnId(String txnId);
	
	/**
	 * 
	 * @return
	 */
	public Map<String ,String> getAggreator();
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getDistributerByAggId(String aggregatorId);
	
	/**
	 * 
	 * @param distributorId
	 * @return
	 */
	public Map<String,String> getAgentByDistId(String distributorId);
	
	/**
	 * 
	 * @param aggregatorId
	 * @return
	 */
	public Map<String,String> getActiveDistributerByAggId(String aggregatorId);
	
	/**
	 * 
	 * @param distributorId
	 * @return
	 */
	public Map<String,String> getActiveAgentByDistId(String distributorId);
	
	/**
	 * 
	 * @param aggId
	 * @return
	 */
	public Map<String, String> getActiveAgentByAgg(String aggId);
	/**
	 * 
	 */
	
	public Map<String,String> getSubAgentByAgentId(String agentId);
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<WalletMastBean> searchUser(ReportBean reportBean);
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<WalletKYCBean> getKycList(ReportBean reportBean);
	
	
	/**
	 * 
	 * @param reportBean
	 * @return
	 */
	public List<SenderProfileBean> getSenderProfile(ReportBean reportBean);
	
	/**
	 * 
	 * @param wishBean
	 * @return
	 */
	public UserWishListBean saveWishList(UserWishListBean wishBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param wishBean
	 * @return
	 */
	public List<UserWishListBean> getWishList(UserWishListBean wishBean);
	
	/**
	 * 
	 * @param wishBean
	 * @return
	 */
	public String deleteWishList(UserWishListBean wishBean);
	
	/**
	 * 
	 * @return
	 */
	public WalletConfiguration getConfiguration(WalletConfiguration configuration);
	
	/**
	 * 
	 * @return
	 */
	public WalletConfiguration saveWalletConfiguration(WalletConfiguration configuration,String ipimei,String userAgent);
	
	/**
	 * 
	 * @return
	 */
	public String forgotPassword(UserSummary bean);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public String setForgotPassword(UserSummary user);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public String setFirstTimePassword(UserSummary user);
	
	/**
	  * 
	  * @return
	  */
	 public List<CustomerProfileBean> getCustomerProfile(String aggreatorid);
	 
	 /**
	  * 
	  * @return
	  */
	 public List<WalletMastBean> getPendingAgentByAggId(String aggregatorId);
	 
	 /**
	  * 
	  * @param agentId
	  * @return
	  */
	 public String acceptAgentByAgg(WalletMastBean agentId);
	 
	 /**
	  * 
	  * @param agentId
	  * @return
	  */
	 public AgentDetailsView agentdetailsview(WalletMastBean walletMastBean);
	 
	 
	 /**
	  * 
	  * @param agentId
	  * @return
	  */
	 public String rejectAgentByAgg(String agentId,String declinedComment );
	 
	 /**
	  * 
	  * @param walletBean
	  * @return
	  */
	 public String saveProfilePic(WalletMastBean walletBean,String ipimei,String userAgent);
	 
	/**
	 *  
	 * @return
	 */
	public Map<String,String> getTxnTypeDtl();
	
	/**
	 * 
	 * @param refId
	 * @return
	 */
	public String acceptKyc(String refId);
	
	/**
	 * 
	 * @param refId
	 * @return
	 */
	public String rejectKyc(String refId);
	
	/**
	 * 
	 * @param userId
	 * @param aggreatorid
	 * @param mobileNo
	 * @return
	 */
	public String validateChangeMobile(String userId,String aggreatorid,String mobileNo) ;
	
	/**
	 * 
	 * @param userId
	 * @param aggreatorid
	 * @param mobileNo
	 * @param otp
	 * @return
	 */
	
	public String changeMobileNo(String userId,String aggreatorid,String mobileNo,String otp);
	
	
	/**
	 * 
	 * @param userId
	 * @param aggreatorid
	 * @param email
	 * @return
	 */
	
	public String validateChangeEmail(String userId,String aggreatorid,String email);
	
	/**
	 * 
	 * @param userId
	 * @param aggreatorid
	 * @param email
	 * @param otp
	 * @return
	 */
	
	public String changeEmailId(String userId,String aggreatorid,String email,String otp);
	
	/**
	 * 
	 * @param userId
	 * @param aggreatorid
	 * @param mobileNo
	 * @return
	 */
	
	public String changeOtpResend(String userId,String aggreatorid,String mobileNo) ;
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getMenu();
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getSubAggreator(String aggregatorId);
	
	/**
	 * 
	 * @param menuMapping
	 * @return
	 */
	public UserMenuMapping saveUserMenuMapping(UserMenuMapping menuMapping,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param menuMapping
	 * @return
	 */
	public UserMenuMapping getUserMenuMapping(UserMenuMapping menuMapping);
	
	//
	/**
	 * 
	 * @param user
	 * @return
	 */
	public PCUser pcRequest(PCUser user);
	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean generateSmartCard(SmartCardBean smartCardBean,String ipimei,String userAgent) ;
	
	/**
	 * 
	 * @return
	 */
	public SmartCardBean updatePrePaidCard(SmartCardBean bean);
	
	/**
	 * 
	 * @return
	 */
	public SmartCardBean updateAddress(SmartCardBean bean);
	
	/**
	 * 
	 * @return
	 */
	public SmartCardBean updatePassword(SmartCardBean bean);
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	public List<SmartCardBean> getSmartCard(SmartCardBean smartCardBean) ;
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public List<SmartCardBean> getSmartCardList(SmartCardBean smartCardBean) ;
	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public List<SmartCardBean> getdispatchCard(SmartCardBean smartCardBean) ;
	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean linkCard(SmartCardBean smartCardBean) ;
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean linkVirtualCard(SmartCardBean smartCardBean) ;
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean linkCardBlocked(SmartCardBean smartCardBean) ;
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean getPrePaidCard(SmartCardBean smartCardBean);
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean dispatchCard(SmartCardBean smartCardBean) ;
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	public String rejectSmartCard(SmartCardBean smartCardBean);
	

	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	public SmartCardOutputBean acceptSmartCard(SmartCardBean smartCardBean);
	
	/**
	 * 
	 * @param appkey
	 * @return
	 * @throws Exception
	 */
	public List<Coupons> getCoupons(String  appkey) throws Exception;
	
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getCouponsCategory();
	
	/**
	 * 
	 * @return
	 */
	public List<CouponsBean> getCouponsByCategory(CouponsBean couponsBean);
	
	/**
	 * 
	 * @param couponsBean
	 * @return
	 */
	public String mailCoupan(CouponsBean couponsBean);
	
	/**
	 * 
	 * @return
	 */
	public List<WalletMastBean> getUnAuthorizedAggreator();
	
	/**
	 * 
	 * @return
	 */
	public String authorizedAggreator(WalletMastBean walletBean);
	
	/**
	 * 
	 * @param aggregatorid
	 * @param offerList
	 * @return
	 */
	public String uploadMerchantOffer(String aggregatorid,List<MerchantOffersMast> offerList);
	
	/**
	 * 
	 * @param aggregatorid
	 * @return
	 */
	public List<MerchantOffersMast> getMerchantOffer(String aggregatorid);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public String fetchingCreatedUserdetail(String reqId);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public String fetchingWalletdetail(String reqId);	
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public String fetchingCardType(String reqId);

	/**
	 * 
	 * @param bean
	 * @return
	 */
	public String fetchingCardTypeCode(String reqId);	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean suspendedCard(String reqId) ;
	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean resumePrePaidCard(String reqId);
	
	
	/**
	 * 
	 * @param smartCardBean
	 * @return
	 */
	
	public SmartCardBean generateCVV(String reqId);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	public SmartCardBean pinReset(SmartCardBean bean);
	
	/**
	 * 
	 * @param bean
	 * @param ipimei
	 * @param userAgent
	 * @return
	 */
	public List<SenderFavouriteViewBean> getFavouriteList(SenderFavouriteBean bean,String ipimei,String userAgent);
	
	
	/**
	 * 
	 */
	public MudraSenderMastBean validateSender(MudraSenderMastBean mastBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param mastBean
	 * @return
	 */
	public MudraSenderMastBean registerSender(MudraSenderMastBean mastBean,String ipimei,String userAgent);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String dmtOtpResend(UserSummary user);
	
	/**
	 * 
	 * @param bean
	 * @return
	 */
	 public BankDetailsBean getBankDetails(BankDetailsBean bean,String ipimei,String userAgent);
	 
	 /**
	  * 
	  * @param bean
	  * @return
	  */
	 public List<BankDetailsBean> getBankDetailsByIFSC(BankDetailsBean bean,String ipimei,String userAgent);
	 
	 /**
	  * 
	  * @param bean
	  * @param ipimei
	  * @param userAgent
	  * @return
	  */
	 public MudraBeneficiaryMastBean registerBeneficiary(MudraBeneficiaryMastBean bean,String ipimei,String userAgent);
	 
	 /**
	  * 
	  * @param bean
	  * @param ipimei
	  * @param userAgent
	  * @return
	  */
	 public DMTReportInOut initiateDMTRefundByAgent( DMTReportInOut dmtreport,String ipimei,String userAgent);
	 
	 /**
	  * 
	  * @param bean
	  * @param ipimei
	  * @param userAgent
	  * @return
	  */
	 public String markFailedDmtTxn(DmtDetailsMastBean dmtMast);
	 
	 /**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraBeneficiaryMastBean deleteBeneficiary(MudraBeneficiaryMastBean bean,String ipimei,String userAgent);
		
		
		 /**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraBeneficiaryMastBean rejectBeneficiary(MudraBeneficiaryMastBean bean,String ipimei,String userAgent);
		
		 /**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraBeneficiaryMastBean acceptBeneficiary(MudraBeneficiaryMastBean bean,String ipimei,String userAgent);
	
		
	
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public SenderFavouriteBean setFavourite(SenderFavouriteBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public SenderFavouriteBean deleteFavourite(SenderFavouriteBean bean,String ipimei,String userAgent);
		
		 /**
		 * 
		 * @param mastBean
		 * @return
		 */
		public MudraSenderMastBean forgotMPIN(MudraSenderMastBean mastBean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraSenderMastBean updateMPIN(MudraSenderMastBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraSenderMastBean getBeneficiaryListForImport(MudraSenderMastBean bean, String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean bean,String ipimei,String userAgent);
		/**
		 * 
		 * @param dmtMast
		 * @return
		 */
		public List<RefundTransactionBean> initiateDMTRefund(DmtDetailsMastBean dmtMast);
		
		/**
		 * 
		 * @param dmtMast
		 * @return
		 */
		public DmtDetailsMastBean checkDMTStatus(DmtDetailsMastBean dmtMast);
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public SurchargeBean calculateSurcharge(SurchargeBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraSenderMastBean activeBeneficiary(MudraBeneficiaryMastBean bean, String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraSenderMastBean deActiveBeneficiary(MudraBeneficiaryMastBean bean, String ipimei,String userAgent);
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraBeneficiaryMastBean copyBeneficiary(MudraBeneficiaryMastBean bean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @return
		 */
		public List<BankMastBean> getBankDtl();
		
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		
		public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent);
		
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		
		public DMTReportInOut getTransacationLedgerDtlForAgg(DMTReportInOut bean, String ipimei, String userAgent);
		
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		
		public DMTReportInOut getTransacationLedgerPayoutForAgg(DMTReportInOut bean, String ipimei, String userAgent);
		
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		
		public DMTReportInOut getAgentLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent);
		
		
		/**
		 * 
		 * @param bean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		
		public DMTReportInOut getSenerLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent);
		
		
		/**
		 * 
		 * @param aggregatorId
		 * @param createdBy
		 * @return
		 */
		public List<DeclinedListBean> getPendingAgentByAggId(String aggregatorId,String createdBy);
		
		/**
		 * 
		 * @param aggregatorId
		 * @param createdBy
		 * @return
		 */
		public List<DeclinedListBean> getRejectAgentDetail(String aggregatorId,String createdBy);
		
		
		/**
		 * 
		 * @param mastBean
		 * @return
		 */
		public WalletMastBean editAgent(WalletMastBean mastBean);
		
		
		/**
		 * 
		 * @param id
		 * @param stDate
		 * @param endDate
		 * @return
		 */
		public List<WalletMastBean> getAgentList(String id,String stDate,String endDate);
		
		/**
		 * 
		 * @param aggreatorid
		 * @param stDate
		 * @param endDate
		 * @return
		 */
		public List<WalletMastBean> getApprovedAgentList(String aggreatorid,String stDate,String endDate);
		
		
		
		/**
		 * 
		 * @param aggreatorid
		 * @param stDate
		 * @param endDate
		 * @return
		 */
		public List<DeclinedListBean> getRejectAgentList(String aggreatorid,String stDate,String endDate) ;
		
		
		/**
		 * 
		 * @return
		 */
		
		public Map<String, String> getRole();
		
		
		/**
		 * 
		 * @param roleMapping
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public UserRoleMapping saveUserRoleMapping(UserRoleMapping roleMapping,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param roleMapping
		 * @return
		 */
		public UserRoleMapping getUserRoleMapping(UserRoleMapping roleMapping);
		
		
		/**
		 * 
		 * @param pan
		 * @param aggreatorid
		 * @return
		 */
		public String validatePan(String pan,String aggreatorid,int userType);
		
		
		/**
		 * 
		 * @return
		 */
		
		public List<RevenueReportBean> getRevenueReportList(WalletMastBean walletBean);
		
		/**
		 * 
		 * @param walletBean
		 * @return
		 */
		public List<DmtDetailsMastBean> getDmtDetailsReportList(WalletMastBean walletBean);
		
		/**
		 * 
		 * @param bean
		 * @return
		 */
		public String blockUnblockUser(UserBlockedBean userBlockedBean);
		/**
		 * 
		 * @param beneficiaryId
		 * @return
		 */
		public MudraBeneficiaryMastBean getBeneficiary(String beneficiaryId);
		
		/**
		 * 
		 * @param aggreatorId
		 * @return
		 */
		public Map<String, String> getAgentDister(String aggreatorId);
		
		/**
		 * 
		 * @param walletBean
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public String distributerOnBoad(WalletMastBean walletBean,String ipimei,String userAgent);
		
		/**
		 * 
		 * @param mudraSenderPanDetails
		 * @param ipimei
		 * @param userAgent
		 * @return
		 */
		public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,String ipimei,String userAgent);
		
		
		/**
		 * 
		 * @param mudraSenderPanDetails
		 * @return
		 */
		public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails);
		
		/**
		 * 
		 * @param mudraSenderPanDetails
		 * @return
		 */
		public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails) ;
		
		/**
		 * 
		 * @param aggregatorId
		 * @return
		 */
		public List<WalletMastBean> getAgentDetailForApprove(String aggregatorId) ;
		
		/**
		 * 
		 * @param mastBean
		 * @return
		 */
		public String approvedAgentByAgg(WalletMastBean mastBean);
		
		/**
		 * 
		 * @param configuration
		 * @return
		 */
		public Map<String, String> getWalletConfigPG(WalletConfiguration configuration);
		
		public WalletMastBean getUserByMobileNo(String mobileNo,String aggId);

		public LoginResponse customerValidateOTP(WalletBean walletbean);
		
		public MudraSenderMastBean deletedBeneficiary(MudraBeneficiaryMastBean bean, String ipimei,String userAgent);
		
		public ResponseBean refundAgentRequest(String senderMobile,String txnId,String userId,String mpin);
		
		public ResponseBean refundAgent(String senderMobile,String mpin,String userId);
		
		public HashMap getAgentSenderBalance(MudraSenderMastBean mastBean);

		public MudraSenderMastBean validateSenderMobile(MudraSenderMastBean mastBean);
		
		public DMTReportInOut getDmtReportBySuper(DMTReportInOut bean, String ipimei, String userAgent);

		public Map<String,String> getAgentBySuper(String id,String aggId,int type);
}
