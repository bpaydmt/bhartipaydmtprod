

package com.bhartipay.wallet.user.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.bhartipay.biller.bean.WalletBean;
import com.bhartipay.lean.AddBankAccount;
import com.bhartipay.lean.AssignedUnit;
import com.bhartipay.lean.CmsBean;
import com.bhartipay.lean.DistributorIdAllot;
import com.bhartipay.lean.DmtPricing;
import com.bhartipay.lean.MappingReport;
import com.bhartipay.lean.NewRegs;
import com.bhartipay.lean.OnboardStatusResponse;
import com.bhartipay.lean.ServiceConfig;
import com.bhartipay.wallet.aeps.AEPSAggregatorSettlement;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsSettlementBean;
import com.bhartipay.wallet.commission.persistence.vo.CommPercMaster;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.recharge.bean.UserWishListBean;
import com.bhartipay.wallet.report.bean.AgentDetailsBean;
import com.bhartipay.wallet.report.bean.ReportBean; 
import com.bhartipay.wallet.transaction.persistence.vo.MoneyRequestBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserWalletConfigBean;
import com.bhartipay.wallet.user.dao.PCUser;
import com.bhartipay.wallet.user.persistence.vo.AgentDetailsView;
import com.bhartipay.wallet.user.persistence.vo.BankDetailsBean;
import com.bhartipay.wallet.user.persistence.vo.BankMastBean;
import com.bhartipay.wallet.user.persistence.vo.Coupons;
import com.bhartipay.wallet.user.persistence.vo.CouponsBean;
import com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean;
import com.bhartipay.wallet.user.persistence.vo.DMTReportInOut;
import com.bhartipay.wallet.user.persistence.vo.DeclinedListBean;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.FundTransactionSummaryBean;
import com.bhartipay.wallet.user.persistence.vo.LoginResponse;
import com.bhartipay.wallet.user.persistence.vo.MailConfigMast;
import com.bhartipay.wallet.user.persistence.vo.MerchantOffersMast;
import com.bhartipay.wallet.user.persistence.vo.MudraBeneficiaryMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails;
import com.bhartipay.wallet.user.persistence.vo.RevenueReportBean;
import com.bhartipay.wallet.user.persistence.vo.RevokeUserDtl;
import com.bhartipay.wallet.user.persistence.vo.SMSConfigMast;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.SenderProfileBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardOutputBean;
import com.bhartipay.wallet.user.persistence.vo.UploadKyc;
import com.bhartipay.wallet.user.persistence.vo.UploadSenderKyc;
import com.bhartipay.wallet.user.persistence.vo.UserBlockedBean;
import com.bhartipay.wallet.user.persistence.vo.UserMenuMapping;
import com.bhartipay.wallet.user.persistence.vo.UserRoleMapping;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletKYCBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.OTOList10;
import com.bhartipay.wallet.user.service.impl.OTOList11;
import com.bhartipay.wallet.user.service.impl.OTOList9;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import appnit.com.crypto.RSAEncryptionWithAES;

/**
 * 
 * @author Ambuj Singh
 * UserService Impl
 *
 */
public class UserServiceImpl implements UserService{
	
	Logger logger=Logger.getLogger(UserServiceImpl.class);

	@Override
	public Map<String, String> getCountry() {

		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCountry");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	@Override
	public Map<String, String> getCountryCode() {

		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCountryCode");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	
	
public static void main(String[] args) throws Exception {
	UserServiceImpl u=new UserServiceImpl();
	CouponsBean c=new CouponsBean();
	c.setCategory("Mobile Recharge");
	List<CouponsBean> obj=u.getCouponsByCategory(c);
	System.out.println(obj);
	
}



@Override
public String saveSignUpUser(UserSummary user,String ipimei,String userAgent) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("countrycode",user.getCountrycode());   
	requestPayload.put("name",user.getName());
	requestPayload.put("mobileno",user.getMobileno());
	requestPayload.put("emailid",user.getEmailid());
	requestPayload.put("usertype",user.getUsertype());
	requestPayload.put("password",user.getPassword());
	requestPayload.put("createdby",user.getCreatedby());
	requestPayload.put("agentid",user.getAgentid());
	requestPayload.put("aggreatorid",user.getAggreatorid());
	requestPayload.put("distributerid",user.getDistributerid());
	requestPayload.put("subAgentId", user.getSubAgentId());
	requestPayload.put("documentType",user.getDocumentType());
	requestPayload.put("documentId", user.getDocumentId());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	String encText="";
	try {
		String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
		encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
		String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(user.getAggreatorid()));

		JSONObject  jsonObj=new JSONObject (); 
		jsonObj.put("aggreatorid",user.getAggreatorid());
		jsonObj.put("encText",encText);
		jsonObj.put("encKey", encAesKey);
		jsonText=JSONValue.toJSONString(jsonObj);
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signup");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}


@Override
public String saveSignUpUserByAgent(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	String encText="";
	try {
	String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
	encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
	String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(walletBean.getAggreatorid()));
	
	JSONObject  jsonObj=new JSONObject (); 
	jsonObj.put("aggreatorid",walletBean.getAggreatorid());
	jsonObj.put("encText",encText);
	jsonObj.put("encKey", encAesKey);
	jsonText=JSONValue.toJSONString(jsonObj);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signup");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);


	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


  @Override
  public String saveSignUpNewMember(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	String encText="";
	try {
	String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
	encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
	String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(walletBean.getAggreatorid()));
	
	JSONObject  jsonObj=new JSONObject (); 
	jsonObj.put("aggreatorid",walletBean.getAggreatorid());
	jsonObj.put("encText",encText);
	jsonObj.put("encKey", encAesKey);
	jsonText=JSONValue.toJSONString(jsonObj);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveSignUpNewMember");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);


	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public WalletMastBean saveAgentOnBoard(WalletMastBean walletBean,String ipimei,String userAgent) {

	logger.debug("******************calling agentOnBoard service response***********************"+walletBean.getApplicationFee());
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/agentOnBoard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}


@Override
public WalletMastBean updateAgentOnBoard(WalletMastBean walletBean,String ipimei,String userAgent) {

	logger.debug("******************calling agentOnBoard service response***********************"+walletBean.getApplicationFee());
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAgentOnBoard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}

@Override
public WalletMastBean saveSelfAgentOnBoard(WalletMastBean walletBean,String ipimei,String userAgent) {

	logger.debug("******************calling agentOnBoard service response***********************"+walletBean.getApplicationFee());
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveSelfAgentOnBoard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}

@Override
public WalletMastBean updateAgentOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAgentOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}


@Override
public LoginResponse login(UserSummary user,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",user.getUserId());   
	requestPayload.put("password",user.getPassword());
	requestPayload.put("imeiIP","null");
	requestPayload.put("aggreatorid",user.getAggreatorid());
	//requestPayload.put("hceToken",user.getHceToken());
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	String encText="";
	try {
		//encText=RSAEncryptionWithAES.encryptAESKey(jsonText,ServiceManager.getProperty(user.getAggreatorid()));
		String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
		encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
		String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(user.getAggreatorid()));
		
		JSONObject  jsonObj=new JSONObject (); 
		jsonObj.put("aggreatorid",user.getAggreatorid());
		jsonObj.put("encText",encText);
		jsonObj.put("encKey", encAesKey);
		jsonText=JSONValue.toJSONString(jsonObj);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/login");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	LoginResponse loginResponse=new LoginResponse();
	Gson gson=new Gson();
	loginResponse=gson.fromJson(output,LoginResponse.class);
	return loginResponse;
}



@Override
public String validateOTP(UserSummary user) {
JSONObject  requestPayload=new JSONObject (); 
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	
	
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}

@Override
public String validateOTPFirstTime(UserSummary user) {
JSONObject  requestPayload=new JSONObject (); 
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	
	
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateOTPFirstTime");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}

@Override
public String verifyOTP(UserSummary user) {
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);

	WebResource webResource=ServiceManager.getWebResource("WalletUser/verifyOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}





@Override
public String verifyRefundOTP(UserSummary user) {
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);

	WebResource webResource=ServiceManager.getWebResource("WalletUser/verifyRefundOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}



@Override
public String otpResend(UserSummary user) {
JSONObject  requestPayload=new JSONObject (); 
	

Gson gson = new Gson();
String jsonText=gson.toJson(user);

	
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public Map<String, String> ProfilebyloginId(UserSummary user) {
	
	JSONObject  json = new JSONObject (); 
	
	json.put("userId",user.getUserId());
	json.put("aggreatorid", user.getAggreatorid());
	
	String jsonText = JSONValue.toJSONString(json);
	
	System.out.println("______jsonText____________"+jsonText);
	
	WebResource webResource = ServiceManager.getWebResource("WalletUser/ProfilebyloginId");
	
	System.out.println("______________webResource________"+webResource);
	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getDashBoardDMTData(UserSummary user) {
	
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",user.getUserId());
	requestPayload.put("aggreatorid", user.getAggreatorid());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getDashBoardDMTData");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getWalletConfig(WalletConfiguration configuration) {
	
	System.out.println("___________Inside getWalletConfig()_____________");

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);

System.out.println("____________jsonText___________"+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

public WalletMastBean getUserDetailsById(WalletMastBean mastBean) {
	
	System.out.println("___________Inside getWalletConfig()_____________");

	
Gson gson = new Gson();
String jsonText=gson.toJson(mastBean);

System.out.println("____________jsonText___________"+jsonText);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getUserDetailsById");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	WalletMastBean bean = gson.fromJson(output, WalletMastBean.class);
	return bean;
}

public WalletConfiguration getConfiguration(WalletConfiguration configuration){

	Gson gson = new Gson();
	String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configuration = gson.fromJson(output, WalletConfiguration.class);
	return configuration;
	
}



@Override
public Map<String, String> ProfilebytxnId(String txnId) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("txnId",txnId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/profilebytxnId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public WalletMastBean showUserProfile(String userId) {
JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",userId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/showUserProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
///	return convertToMap(jo);
	
	
	
	Gson gson = new Gson();
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;

}



@Override
public WalletMastBean editProfile(WalletMastBean mastBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/editProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;
}



@Override
public Map<String, String> getKyc() {

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getKycType() {

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getKycType");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getKycId() {

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getIdProofKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public String resetPassword(WalletMastBean walletBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changePassword");	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI","").post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String saveKycDocuments(UploadKyc uploadKyc,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(uploadKyc);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String saveSenderKyc(UploadSenderKyc uploadKyc,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(uploadKyc);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveSenderKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public Map<String, String> getCustomerByAgentId(double userType,String agentId) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",agentId);
	if(userType==99)
	requestPayload.put("userType","99");
	if(userType==1)
		requestPayload.put("userType","1");
	if(userType==2)
		requestPayload.put("userType","2");
	if(userType==3)
		requestPayload.put("userType","3");
	if(userType==4)
		requestPayload.put("userType","4");
	if(userType==5)
		requestPayload.put("userType","5");
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getCustomerByAgentId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public UserWalletConfigBean getWalletConfigByUserId(UserWalletConfigBean configBean) {
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(configBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getWalletConfigByUserId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return walletBean;
}



@Override
public UserWalletConfigBean saveLimit(UserWalletConfigBean config,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(config);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveUserWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return walletBean;
}



@Override
public String getProfilePic(String userId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("userId",userId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getProfilePic");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
//	Gson gson = new Gson();
//	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return output;
}



@Override
public PGPayeeBean profilebyPGtxnId(String txnId) {
	
	Gson gson=new Gson();
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("txnId",txnId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/profilebyPGtxnId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	PGPayeeBean payeeBean = gson.fromJson(output, PGPayeeBean.class);
	return payeeBean;
}



@Override
public Map<String, String> getAggreator() {

//	JSONObject  requestPayload=new JSONObject (); 
//	requestPayload.put("userId",userId);
//	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getAggreatorList() {

//	JSONObject  requestPayload=new JSONObject (); 
//	requestPayload.put("userId",userId);
//	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAggreatorList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getWLAggreator() {
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getWLAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String,String> getSuperDistributerByAggId(String aggregatorId){
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSuperDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);

}



@Override
public Map<String, String> getDistributerBySuperDistributerForComm(String aggregatorId) {
	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributerBySuDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}
@Override
public Map<String, String> getDistributerByAggId(String aggregatorId) {
	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String,String> getDistributerBySuDistId(String suDistId){

	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("suDistId",suDistId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributerBySuDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getAgentByDistId(String distributorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentByDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getAllUser(String aggId) {
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAllUser");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getAgentByAggId(String aggid) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getAgentBySupDistId(String superdistid) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("suDistId",superdistid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentBySupDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String,String> getActiveSuperDistributerByAggId(String aggregatorId){

	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSuperActiveDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);

}


@Override
public Map<String, String> getActiveDistributerByAggId(String aggregatorId) {
	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getActiveDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getActiveAgentByDistId(String distributorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getActiveAgentByDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getManagerId(String distributorId,String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getManagerId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getDistId(String distributorId,String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getDistId(String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistIdd");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getSdId(String sdId,String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",sdId);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSdId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getSdId(String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSdIdd");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getSoId(String distributorId,String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSoId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

@Override
public Map<String, String> getSoId(String aggId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSoIdd");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getSubAgentByAgentId(String agentid){
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("agentId",agentid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSubAgentByAgentId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public List<WalletMastBean> searchUser(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/searchUser");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	return list;
}



@Override
public List<SenderProfileBean> getSenderProfile(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSenderProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SenderProfileBean> list = gson.fromJson(output,OTOList1.class);
	System.out.println(list);
	for(SenderProfileBean pb:list){
		System.out.println(pb.getPayerEmail());
		System.out.println(pb.getPayermobile());
	}
	return list;
}



@Override
public UserWishListBean saveWishList(UserWishListBean wishBean,String ipimei,String userAgent) {
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	 wishBean = gson.fromJson(output, UserWishListBean.class);
	return wishBean;
}



@Override
public List<UserWishListBean> getWishList(UserWishListBean wishBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	List<UserWishListBean> list = gson.fromJson(output,OTOList2.class);
	// wishBean = gson.fromJson(output, UserWishListBean.class);
	return list;
}



@Override
public String deleteWishList(UserWishListBean wishBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/deleteWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}



@Override
public WalletConfiguration saveWalletConfiguration(
		WalletConfiguration configuration,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveWalletConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	 configuration = gson.fromJson(output, WalletConfiguration.class);
	return configuration;
}



@Override
public String forgotPassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/forgotPassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}

@Override
public String setForgotPassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/setForgotPassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}

@Override
public String forgetPasswordConfirm(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/forgotPasswordConfirm");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}



@Override
public String setFirstTimePassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/firstTimePasswordChange");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}






@Override
public List<CustomerProfileBean> getCustomerProfile(String aggreatorid) {
 Gson gson = new Gson();
 JSONObject  requestPayload=new JSONObject (); 
requestPayload.put("aggId",aggreatorid);
String jsonText = JSONValue.toJSONString(requestPayload);
 
 
 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustomerDetail");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200){
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 List<CustomerProfileBean> list = gson.fromJson(output,OTOList3.class);
 System.out.println(list);
 for(CustomerProfileBean pb:list){
  System.out.println(pb.getEmailid());
  System.out.println(pb.getFinalBalance());
 }
 return list;
}



@Override
public List<WalletMastBean> getPendingAgentByAggId(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getPendingAgentByAggId");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}

@Override
public List<WalletMastBean> newBpayAgent(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/newBpayAgent");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}



@Override
public String acceptAgentByAgg(WalletMastBean mastBean) {

	 Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	String jsonText = gson.toJson(mastBean);
	
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/acceptAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;
	
}


@Override
public String acceptNewAgentByAgg(WalletMastBean mastBean) {

	 Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	String jsonText = gson.toJson(mastBean);
	
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/acceptNewAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;
	
}


@Override
public AgentDetailsView agentdetailsview(WalletMastBean walletMastBean) {
	JSONObject  requestPayload=new JSONObject (); 
	 Gson gson = new Gson();
	 String jsonText =  gson.toJson(walletMastBean);
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/agentdetailsview");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	    return null;
	 }
	 String output = response.getEntity(String.class);  
	AgentDetailsView av= gson.fromJson(output, AgentDetailsView.class);
	 return av;
	
}


@Override
public AgentDetailsView newBpayAgentdetailsview(WalletMastBean walletMastBean) {
	JSONObject  requestPayload=new JSONObject (); 
	 Gson gson = new Gson();
	 String jsonText =  gson.toJson(walletMastBean);
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/newBpayAgentdetailsview");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	    return null;
	 }
	 String output = response.getEntity(String.class);  
	AgentDetailsView av= gson.fromJson(output, AgentDetailsView.class);
	 return av;
	
}


@Override
public String rejectAgentByAgg(String createdBy,String agentId,String declinedComment) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("userId",agentId);
	requestPayload.put("declinedComment", declinedComment);
	requestPayload.put("createdBy", createdBy);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/rejectAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;

}





@Override
public List<WalletKYCBean> getKycList(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getKycList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<WalletKYCBean> list = gson.fromJson(output,OTOList4.class);
	return list;
}

public Map<String, String> convertToMap(JsonObject jsonObject) {
	Gson gson = new Gson();
	Map<String, String> myMap = gson.fromJson(jsonObject, Map.class);
	return myMap;
}





@Override
public MailConfigMast emailConfiguration(MailConfigMast configMast) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMailConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, MailConfigMast.class);
	return configMast;
}





@Override
public MailConfigMast saveMailConfiguration(MailConfigMast configMast,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveMailConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, MailConfigMast.class);
	return configMast;
}





@Override
public SMSConfigMast smsConfiguration(SMSConfigMast configMast) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getSMSConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, SMSConfigMast.class);
	return configMast;
}





@Override
public SMSConfigMast saveSMSConfiguration(SMSConfigMast configMast,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveSMSConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, SMSConfigMast.class);
	return configMast;
}





@Override
public String saveProfilePic(WalletMastBean walletBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveProfilePic");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public String getcurrencyByCountryId(WalletConfiguration configuration) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("countryId",configuration.getCountryid());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getcurrencyByCountryId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String getcurrencyByCountryCode(WalletConfiguration configuration) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("countryCode",configuration.getCountryCode());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getcurrencyByCountryCode");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());//
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public String signUpUserByUpload(WalletMastBean walletBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signUpUserByUpload");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





/*@Override
public String otpResend(String userId) {
JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",userId);	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public Map<String, String> getWalletConfig(WalletConfiguration configuration) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}
*/


@Override
public String getAggIdByDomain(WalletConfiguration config) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(config);
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAggIdByDomain");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	return output;
}





@Override
public Map<String, String> getTxnTypeDtl() {
	WebResource webResource=ServiceManager.getWebResource("CommissionManager/getTxnTypeDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public String acceptKyc(String refId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("refid",refId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/acceptKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}




@Override
public String rejectKyc(String refId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("refid",refId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/rejecttKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}


@Override
public String validateChangeMobile(String userId,String aggreatorid,String mobileNo) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateChangeMobile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String validateChangeEmail(String userId,String aggreatorid,String email) {
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("emailId",email);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateChangeEmail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String changeMobileNo(String userId,String aggreatorid,String mobileNo,String otp) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	requestPayload.put("otp",otp);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeMobileNo");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String changeEmailId(String userId,String aggreatorid,String email,String otp) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("emailId",email);
	requestPayload.put("otp",otp);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeEmailId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String changeOtpResend(String userId,String aggreatorid,String mobileNo) {
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeOtpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public Map<String, String> getMenu() {
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMenu");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public Map<String, String> getSubAggreator(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getSubAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public UserMenuMapping saveUserMenuMapping(UserMenuMapping menuMapping,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	

	Gson gson = new Gson();
	String jsonText=gson.toJson(menuMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveUserMenuMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		menuMapping=gson.fromJson(output, UserMenuMapping.class);
		return menuMapping;
}





@Override
public UserMenuMapping getUserMenuMapping(UserMenuMapping menuMapping) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(menuMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getUserMenuMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		menuMapping=gson.fromJson(output, UserMenuMapping.class);
		return menuMapping;
}





@Override
public PCUser pcRequest(PCUser user) {
	
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(user);		
		WebResource webResource=ServiceManager.getWebResource("paymentCard/pcRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		user=gson.fromJson(output, PCUser.class);
		return user;
}





@Override
public SmartCardBean generateSmartCard(SmartCardBean smartCardBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/generateSmartCardReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updatePrePaidCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updatePrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updateAddress(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAddress");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updatePassword(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updatePassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public List<SmartCardBean> getSmartCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getSmartCardReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}

@Override
public List<SmartCardBean> getSmartCardList(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getSmartCardList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}

@Override
public SmartCardBean linkCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}

@Override
public SmartCardBean linkCardBlocked(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkCardBlocked");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}


@Override
public List<SmartCardBean> getdispatchCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getdispatchCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}


@Override
public SmartCardBean dispatchCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/dispatchCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	 smartCardBean = gson.fromJson(output,SmartCardBean.class);
	return smartCardBean;
}


@Override
public SmartCardBean getPrePaidCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getPrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	 smartCardBean = gson.fromJson(output,SmartCardBean.class);
	return smartCardBean;
}


@Override
public String rejectSmartCard(SmartCardBean smartCardBean){
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/rejecttSmartCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	return output;
}



@Override
public SmartCardOutputBean acceptSmartCard(SmartCardBean smartCardBean){
 Gson gson = new Gson();
 String jsonText=gson.toJson(smartCardBean);
 
 WebResource webResource=ServiceManager.getWebResource("WalletUser/acceptSmartCard");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
 if (response.getStatus() != 200){
  System.out.println(response.getStatus());
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);
 SmartCardOutputBean smartCardOutputBean =gson.fromJson(output, SmartCardOutputBean.class);
 return smartCardOutputBean;
}


public List<Coupons> getCoupons(String  appkey) throws Exception {
	Gson gson = new Gson();
	String rest="";
	URL url = new URL("https://tools.vcommission.com/api/coupons.php?apikey=93f099e30a2d1dfe6ee5ab3517493855cf188557ffbf7524b0992f31b24f8df8");
    HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
    urlconnection.setRequestMethod("POST");
    urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
    urlconnection.setDoOutput(true);
    OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
   // outa.write(postData);
    outa.close();
    BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
    String decodedString;
    while ((decodedString = in.readLine()) != null) {
        System.out.println("decodedString~~~~~~~~```"+decodedString);
        rest=decodedString;
  }
    in.close();
    String output=rest;

	
	
	
	
	//String output = response.getEntity(String.class);		
	List<Coupons> list = gson.fromJson(output,OTOList6.class);
	return list;
}





@Override
public Map<String, Object> getCouponsCategory() {
Gson gson = new Gson();
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCouponsCategory");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Map<String,Object> list = gson.fromJson(output,OTOList7.class);
	
//	Map<String,String> catMap=(Map<String, String>)list.get("category");
//	List<CouponsBean> couponList=(List<CouponsBean>)list.get("couponList");

return list;
}





@Override
public List<CouponsBean> getCouponsByCategory(CouponsBean couponsBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCouponsByCategory");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	List<CouponsBean> list=gson.fromJson(output,OTOList8.class);
	return list;
}





@Override
public String mailCoupan(CouponsBean couponsBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/mailCoupan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	
	return output;
	
}

@Override
public List<WalletMastBean> getUnAuthorizedAggreator() {
	Gson gson = new Gson();
	//String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getUnAuthorizedAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	List<WalletMastBean> list=gson.fromJson(output,OTOList.class);
	return list;
}

@Override
public String authorizedAggreator(WalletMastBean walletBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/authorizedAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	
	return output;
}

@Override
public String uploadMerchantOffer(String aggregatorid,
		List<MerchantOffersMast> offerList) {
	
	Gson gson=new Gson();
	String arrayList=gson.toJson(offerList);
	JSONObject  requestPayload=new JSONObject ();    
	
	requestPayload.put("aggreatorid",aggregatorid);
	requestPayload.put("offerList",arrayList);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/uploadMerchantOffer");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public List<MerchantOffersMast> getMerchantOffer(String aggregatorid) {
	Gson gson=new Gson();
	JSONObject  requestPayload=new JSONObject ();    
	
	requestPayload.put("aggreatorid",aggregatorid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMerchantOffer");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return gson.fromJson(output, OTOList9.class);
}

@Override
public String fetchingCreatedUserdetail(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCreatedUserdetail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingWalletdetail(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingWalletdetail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingCardType(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCardType");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingCardTypeCode(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCardTypeCode");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public SmartCardBean suspendedCard(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/suspendedCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean resumePrePaidCard(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/resumePrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean generateCVV(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/generateCVV");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean pinReset(SmartCardBean bean) {
	Gson gson=new Gson();
	
	String jsonText = gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/pinReset");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	 bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean linkVirtualCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkVirtualCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}

@Override
public List<SenderFavouriteViewBean> getFavouriteList(SenderFavouriteBean bean, String ipimei, String userAgent) {
logger.debug("******************calling getFavouriteList service ***********************");
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getFavouriteList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getFavouriteList service response***********************"+output);
	List<SenderFavouriteViewBean> bList=gson.fromJson(output,OTOList10.class);
	return bList;
}

@Override
public MudraSenderMastBean validateSender(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling validateSender service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/validateSender");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling validateSender service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public HashMap getAgentSenderBalance(MudraSenderMastBean mastBean) {
	logger.debug("******************calling getAgentSenderBalance service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getAgentSenderBalance");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getAgentSenderBalance service response***********************"+output);
	HashMap mudra=gson.fromJson(output,HashMap.class);
	return mudra;
}

@Override
public MudraSenderMastBean registerSender(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling registerSender service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/registerSender");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling registerSender service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public String dmtOtpResend(UserSummary user) {
	logger.debug("******************calling dmtOtpResend service ***********************");
JSONObject  requestPayload=new JSONObject (); 
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	WebResource webResource=ServiceManager.getWebResource("MudraManager/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling dmtOtpResend service response***********************"+output);
	return output;
}


@Override
public String refundOtp(UserSummary user) {
	logger.debug("******************calling refundOtp service ***********************");
JSONObject  requestPayload=new JSONObject (); 
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	WebResource webResource=ServiceManager.getWebResource("MudraManager/refundOtp");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling refundOtp service response***********************"+output);
	return output;
}
@Override
public BankDetailsBean getBankDetails(BankDetailsBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDetails service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getBankDetails");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDetails service response***********************"+output);
	BankDetailsBean bankDetails=gson.fromJson(output,BankDetailsBean.class);
	return bankDetails;
}

@Override
public List<BankDetailsBean> getBankDetailsByIFSC(BankDetailsBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDetailsByIFSC service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getBankDetailsByIFSC");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDetailsByIFSC service response***********************"+output);
	List<BankDetailsBean> bankDetail=gson.fromJson(output,OTOList11.class);
	return bankDetail;
}

@Override
public MudraBeneficiaryMastBean registerBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling registerBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManager/registerBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling registerBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}

@Override
public MudraBeneficiaryMastBean deleteBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManager/deleteBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling deleteBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}

@Override
public SenderFavouriteBean setFavourite(SenderFavouriteBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling setFavourite service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/setFavourite");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling setFavourite service response***********************"+output);
	bean=gson.fromJson(output,SenderFavouriteBean.class);
	return bean;
}

@Override
public SenderFavouriteBean deleteFavourite(SenderFavouriteBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling deleteFavourite service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/deleteFavourite");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling deleteFavourite service response***********************"+output);
	bean=gson.fromJson(output,SenderFavouriteBean.class);
	return bean;
}

@Override
public MudraSenderMastBean forgotMPIN(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling forgotMPIN service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/forgotMPIN");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling forgotMPIN service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public MudraSenderMastBean updateMPIN(MudraSenderMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling updateMPIN service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/updateMPIN");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling updateMPIN service response***********************"+output);
	bean=gson.fromJson(output,MudraSenderMastBean.class);
	return bean;
}

@Override
public MudraSenderMastBean getBeneficiaryListForImport(MudraSenderMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBeneficiaryListForImport service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getBeneficiaryListForImport");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBeneficiaryListForImport service response***********************"+output);
	bean=gson.fromJson(output,MudraSenderMastBean.class);
	return bean;
}

@Override
public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/calculateSurCharge");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
    bean=gson.fromJson(output,MudraMoneyTransactionBean.class);
 return bean;
}



@Override
public MudraBeneficiaryMastBean getBeneficiary(String beneficiaryId) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 JSONObject  requestPayload=new JSONObject ();    
requestPayload.put("id",beneficiaryId);
 String jsonText=gson.toJson(requestPayload);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/getBeneficiary");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
 MudraBeneficiaryMastBean bean=gson.fromJson(output,MudraBeneficiaryMastBean.class);
 return bean;
}



@Override
public MudraSenderMastBean getSender(String id) {
logger.debug("******************calling getSender service ***********************");
 
 Gson gson = new Gson();
 JSONObject  requestPayload=new JSONObject ();    
requestPayload.put("id",id);
 String jsonText=gson.toJson(requestPayload);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/getSender");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling getSender service response***********************"+output);
 MudraSenderMastBean bean=gson.fromJson(output,MudraSenderMastBean.class);
 return bean;
}


@Override
public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/verifyAccount");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
    bean=gson.fromJson(output,MudraMoneyTransactionBean.class);
 return bean;
}


@Override
public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/fundTransfer");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
 FundTransactionSummaryBean resultBean=gson.fromJson(output,FundTransactionSummaryBean.class);
 return resultBean;
}


public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean bean,String ipimei,String userAgent){

logger.debug("******************calling getTransactionDetailsByTxnId service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManager/getTransactionDetailsByTxnId");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling getTransactionDetailsByTxnId service response***********************"+output);
 FundTransactionSummaryBean resultBean=gson.fromJson(output,FundTransactionSummaryBean.class);
 return resultBean;
	
	
}

@Override
public SurchargeBean calculateSurcharge(SurchargeBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling calculateSurcharge service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/calculateSurcharge");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling calculateSurcharge service response***********************"+output);
	bean=gson.fromJson(output,SurchargeBean.class);
	return bean;
}

@Override
public MudraSenderMastBean activeBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling activeBeneficiary service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/activeBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling activeBeneficiary service response***********************"+output);
MudraSenderMastBean	mBean=gson.fromJson(output,MudraSenderMastBean.class);
	return mBean;
}




@Override
public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getTransacationLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getTransacationLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getTransacationLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}



@Override
public DMTReportInOut getAgentLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getAgentLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getAgentLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getAgentLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}



@Override
public DMTReportInOut getSenerLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getSenerLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getSenderLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getSenerLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}






@Override
public MudraSenderMastBean deActiveBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/deActiveBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getBankDtl service response***********************"+output);
    MudraSenderMastBean	mBean=gson.fromJson(output,MudraSenderMastBean.class);
	return mBean;
}










@Override
public MudraBeneficiaryMastBean copyBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
logger.debug("******************calling copyBeneficiary service ***********************");
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/copyBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	logger.debug("******************calling copyBeneficiary service response***********************"+output);
    bean=gson.fromJson(output,MudraBeneficiaryMastBean.class);
	return bean;
}

@Override
public List<BankMastBean> getBankDtl() {
	logger.debug("******************calling getBankDtl service ***********************");
	Gson gson = new Gson();
	//String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getBankDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDtl service response***********************"+output);
    List<BankMastBean> bean=gson.fromJson(output,OTOList12.class);
	return bean;
}



@Override
public List<DeclinedListBean> getPendingAgentByAggId(String aggregatorId,String createdBy) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	requestPayload.put("createdBy",createdBy);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getPendingAgentByAggId");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<DeclinedListBean> getRejectAgentDetail(String aggregatorId,String createdBy) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	requestPayload.put("createdBy",createdBy);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getRejectAgentDetail");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}



@Override
public WalletMastBean editAgent(WalletMastBean mastBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/editAgent");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;
}


@Override
public WalletMastBean editAgentPending(WalletMastBean mastBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/editAgentPending");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;
}

@Override
public List<WalletMastBean> getAgentList(int usertype,String createdby,String id,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("id",id);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	requestPayload.put("usertype", usertype);
	requestPayload.put("createdby", createdby);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<WalletMastBean> getApprovedAgentList(String aggreatorid,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getApprovedAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<DeclinedListBean> getRejectAgentList(String aggreatorid,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getRejectAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}




@Override
public Map<String, String> getRole() {
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getRole");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}




@Override
public UserRoleMapping saveUserRoleMapping(UserRoleMapping roleMapping,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	

	Gson gson = new Gson();
	String jsonText=gson.toJson(roleMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveUserRoleMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		roleMapping=gson.fromJson(output, UserRoleMapping.class);
		return roleMapping;
}




@Override
public UserRoleMapping getUserRoleMapping(UserRoleMapping roleMapping) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(roleMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getUserRoleMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		roleMapping=gson.fromJson(output, UserRoleMapping.class);
		return roleMapping;
}




@Override
public String validatePan(String pan,String aggreatorid,int userType) {

	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("pan",pan);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("userType",userType);
	String jsonText = JSONValue.toJSONString(requestPayload);

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/validatePan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}




@Override
public List<RevenueReportBean> getRevenueReportList(WalletMastBean walletBean) {
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	 WebResource webResource=ServiceManager.getWebResource("ReportManager/getRevenueReportList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<RevenueReportBean> list = gson.fromJson(output,OTOList14.class);
	 System.out.println(list);
	 return list;
}



@Override
public List<RevenueReportBean> getCommissionSummary(WalletMastBean walletBean) {
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCommissionSummary");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<RevenueReportBean> list = gson.fromJson(output,OTOList14.class);
	 System.out.println(list);
	 return list;
}

@Override
public List<DmtDetailsMastBean> getDmtDetailsReportList(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getDmtDetailsReportList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DmtDetailsMastBean> list = gson.fromJson(output,OTOList15.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<AEPSLedger> getAepsReport(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsReport");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<AEPSLedger> list = gson.fromJson(output,OTOList20.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<AepsSettlementBean> getAepsSettlementPayoutReport(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsPayoutReport");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<AepsSettlementBean> list = gson.fromJson(output,OTOList23.class);
	 System.out.println(list);
	 return list;
}



@Override
public List<MATMLedger> getMatmReport(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getMatmReport");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<MATMLedger> list = gson.fromJson(output,OTOList22.class);
	 System.out.println(list);
	 return list;
}

@Override
public List<AEPSLedger> getAepsReportForAgent(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsReportForAgent");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<AEPSLedger> list = gson.fromJson(output,OTOList20.class);
	 System.out.println(list);
	 return list;
}

@Override
public List<AepsSettlementBean> getAepsPayoutReport(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsPayoutReportForAgent");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<AepsSettlementBean> list = gson.fromJson(output,OTOList23.class);
	 System.out.println(list);
	 return list;
}
@Override
public List<AEPSLedger> getAepsReportByDistAgg(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsReportByDistAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<AEPSLedger> list = gson.fromJson(output,OTOList20.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<MATMLedger> getMatmReportForAgent(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getMatmReportForAgent");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<MATMLedger> list = gson.fromJson(output,OTOList22.class);
	 System.out.println(list);
	 return list;
}

@Override
public List<DmtDetailsMastBean> GetSupDistDmtDetailsReport(WalletMastBean walletBean) {
	
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	 WebResource webResource=ServiceManager.getWebResource("ReportManager/GetSupDistDmtDetailsReport");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DmtDetailsMastBean> list = gson.fromJson(output,OTOList15.class);
	 System.out.println(list);
	 return list;
}

@Override
public String blockUnblockUser(UserBlockedBean userBlockedBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(userBlockedBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/blockUnblockUser");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}




@Override
public Map<String, String> getAgentDister(String aggreatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggreatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentDister");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public String distributerOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/distributerOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String userOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/userOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String allUserOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/allUserOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


public List<WalletMastBean> distributerOnBoardList(WalletMastBean walletBean,String ipimei,String userAgent){

Gson gson = new Gson();
String jsonText=gson.toJson(walletBean);

WebResource webResource=ServiceManager.getWebResource("WalletUser/distributerOnBoardList");
ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);


if (response.getStatus() != 200) {
	   throw new RuntimeException("Failed : HTTP error code : "
		+ response.getStatus());
}
String output = response.getEntity(String.class);	
List<WalletMastBean> wmBean=gson.fromJson(output,OTOList.class);
return wmBean;
}


@Override
public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/uploadSenderPanF60");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	mudraSenderPanDetails=gson.fromJson(output, MudraSenderPanDetails.class);
	return mudraSenderPanDetails;
}





@Override
public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails) {
	logger.debug("*****************calling method  getPendingPanReq ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	logger.debug("*****************json text as parameter to calling service getPendingPanReq***********************"+jsonText);
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getPendingPanReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("*****************response come from getPendingPanReq***********************");
	if (response.getStatus() != 200){
		logger.debug("*****************response come***********************"+response.getStatus());	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	logger.debug("*****************response come from service***********************"+response);
	String output = response.getEntity(String.class);		
	logger.debug("*****************response come from service string***********************"+output);
	List<MudraSenderPanDetails> list = gson.fromJson(output,OTOList16.class);
	System.out.println(list);
	
	return list;
}



@Override
public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/rejectAcceptedSenderPan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	mudraSenderPanDetails=gson.fromJson(output, MudraSenderPanDetails.class);
	return mudraSenderPanDetails;
}



@Override
public List<UploadSenderKyc> getPendingKycList(UploadSenderKyc senderKyc) {
	logger.debug("*****************calling method  getPendingKycList ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(senderKyc);
	logger.debug("*****************json text as parameter to calling service getPendingKycList***********************"+jsonText);
	WebResource webResource=ServiceManager.getWebResource("MudraManager/getPendingKycList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("*****************response come from getPendingPanReq***********************");
	if (response.getStatus() != 200){
		logger.debug("*****************response come***********************"+response.getStatus());	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	logger.debug("*****************response come from service***********************"+response);
	String output = response.getEntity(String.class);		
	logger.debug("*****************response come from service string***********************"+output);
	List<UploadSenderKyc> list = gson.fromJson(output,OTOList19.class);
	System.out.println(list);
	
	return list;
}




@Override
public UploadSenderKyc rejectAcceptedSenderKyc(UploadSenderKyc senderKyc) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(senderKyc);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManager/rejectAcceptedSenderKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	senderKyc=gson.fromJson(output, UploadSenderKyc.class);
	return senderKyc;
}





@Override
public List<WalletMastBean> getAgentDetailForApprove(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAgentDetailForApprove");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}



@Override
public List<MoneyRequestBean> getWLCashApprover(WalletMastBean walletBean) {
	Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	
	String jsonText = gson.toJson(walletBean);
	
	 
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/wLCashApprover");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<MoneyRequestBean> list = gson.fromJson(output,OTOList17.class);
	 System.out.println(list);
	 return list;
}




@Override
public String updateCashRequestStatus(WalletMastBean walletBean) {
	Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	
	String jsonText = gson.toJson(walletBean);
	
	 
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/updateCashRequestStatus");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
//	 List<MoneyRequestBean> list = gson.fromJson(output,OTOList17.class);
//	 System.out.println(list);
	 return output;
}
@Override
public String approvedAgentByAgg(WalletMastBean mastBean) {

	 Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	String jsonText = gson.toJson(mastBean);
	
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/approvedAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;
	
}



@Override
public Map<String, String> getWalletConfigPG(WalletConfiguration configuration) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfigPG");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

public WalletMastBean getUserByMobileNo(String mobileNo,String aggId)
{
	/*JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("mobileNo",mobileNo);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);*/
	WalletMastBean req = new WalletMastBean();
	Gson gson = new Gson();
	req.setMobileno(mobileNo);
	req.setAggreatorid(aggId);
	String jsonText = gson.toJson(req);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getUserByMobileNo");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	
	String output = response.getEntity(String.class);	
	WalletMastBean bean=gson.fromJson(output, WalletMastBean.class);
	return bean;

}

public MudraSenderMastBean deletedBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	 logger.debug("******************calling deletedBeneficiary service ***********************");
	 Gson gson = new Gson();
	 String jsonText=gson.toJson(bean);
	 
	 WebResource webResource=ServiceManager.getWebResource("MudraManager/deletedBeneficiary");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling deletedBeneficiary service response***********************"+output);
	    MudraSenderMastBean mBean=gson.fromJson(output,MudraSenderMastBean.class);
	 return mBean;
	}

@Override
public LoginResponse customerValidateOTP(WalletBean walletbean) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(walletbean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/customerValidateOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	LoginResponse loginResponse=new LoginResponse();
	/*Gson gson=new Gson();*/
	loginResponse=gson.fromJson(output,LoginResponse.class);
	return loginResponse;
	
}

@Override
public String insertTransDtl(String details) {
logger.debug("******************calling insertTransDtl service ***********************");


	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("details",details); 
	String jsonText = JSONValue.toJSONString(requestPayload);
	
 WebResource webResource=ServiceManager.getWebResource("TransactionManager/insertTransDtl");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);			
 logger.debug("******************calling insertTransDtl service response***********************"+output);
 return output;
}

@Override
public String deleteTransDtl(String details) {
logger.debug("******************calling deleteTransDtl service ***********************");
 
JSONObject  requestPayload=new JSONObject (); 
requestPayload.put("details",details); 
String jsonText = JSONValue.toJSONString(requestPayload);
 
 WebResource webResource=ServiceManager.getWebResource("TransactionManager/deleteTransDtl");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);			 
 logger.debug("******************calling deleteTransDtl service response***********************"+output);
 return output;
}

@Override
public String getTransDtlFlag(String details,String txnType) {
logger.debug("******************calling getTransDtlFlag service ***********************");
 
JSONObject  requestPayload=new JSONObject (); 

requestPayload.put("details",details);   
requestPayload.put("txnType",txnType);
String jsonText = JSONValue.toJSONString(requestPayload);

 WebResource webResource=ServiceManager.getWebResource("TransactionManager/getTransDtlFlag");
// ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);


 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);			
 logger.debug("******************calling getTransDtlFlag service response***********************"+output);
 return output;
}

public List<BankDetailsBean> graminBankIFSC(String bankName)
{
	 logger.debug("******************calling graminBankIFSC service ***********************");
	 Gson gson = new Gson();
	 
	 WebResource webResource=ServiceManager.getWebResource("MudraManager/graminBankIFSC");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,bankName);

	 if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling graminBankIFSC service response***********************"+output);
	 List<BankDetailsBean> mBean=gson.fromJson(output,OTOList11.class);
	 return mBean;
	}



public String getSessionId(String userId)
{
	logger.info("*****start execution of getSessionId**UserServiceImpl");
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getSessionId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,userId);
	if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling getSessionId service response***********************"+output);
	return output;	
}

public String requestRevoke(String userId,String revokeUserId,String remark,double amt,String ip,String agent)
{
	logger.info("*****start execution of requestRevoke**UserServiceImpl");
	JSONObject jsonObject = new JSONObject();
	jsonObject.put("userId", userId);
	jsonObject.put("revokeUserId", revokeUserId);
	jsonObject.put("remark", remark);
	jsonObject.put("amount", amt);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/requestRevoke");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI", ip).header("AGENT", agent).post(ClientResponse.class,JSONObject.toJSONString(jsonObject));
	if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling requestRevoke service response***********************"+output);
	return output;	
}
public List<RevokeUserDtl> getRevokeList(String id)
{
	logger.info("*****start execution of requestRevoke**getRevokeList");
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getRevokeList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,id);
	if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<RevokeUserDtl> list = new Gson().fromJson(output,OTOList18.class);
	 System.out.println(list);
	 return list;
}

public String rejectAcceptRevoke(String revokeType,String remark,String revokeId,String aggreatorId,String requesterId,String revokeUserId,double amount)
{
	logger.info("*****start execution of rejectRevoke**UserServiceImpl");
	JSONObject jsonObject = new JSONObject();
	jsonObject.put("revokeType", revokeType);
	jsonObject.put("remark", remark);
	jsonObject.put("revokeId", revokeId);
	jsonObject.put("aggregatorId", aggreatorId);
	jsonObject.put("revokeUserId", revokeUserId);
	jsonObject.put("requesterId", requesterId);
	jsonObject.put("amount", amount);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/rejectAcceptRevoke");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,JSONObject.toJSONString(jsonObject));
	if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;

}

@Override
public List<String> getCashDepositBank(String aggregatorId) {
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getCashDepositBank");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,aggregatorId);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	List<String> list = new Gson().fromJson(output,OTOList21.class);
	System.out.println(list);
	return list;
}

	@Override
	public Map<String, String> getSettledAggreator() {
		// TODO Auto-generated method stub
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSettledAggreator");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
		
	}
	
	
	
	@Override
	public Map<String, String> getAepsSettlementCharge(String agentId) {
		// TODO Auto-generated method stub
		System.out.println("-------------"+agentId);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getAepsSettlementCharge");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
		
	}
	
	
	
	

	@Override
	public String sendAggreatorSettlementRequest(String aggregatorid) {

		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAggreatorSettlement");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggregatorid);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}

	@Override
	public String saveAepsAggregatorSettlement(String aggSettlement) {

		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("ReportManager/saveAggreatorSettlement");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggSettlement);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}

	@Override
	public String sendSettlementRequest(String aeAgentId) {
		WebResource webResource = ServiceManager.getWebResource("ReportManager/saveAepsSettlement");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aeAgentId);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}

	
	public String sendSettlementRequestData(AddBankAccount account) {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("userId", account.getUserId());
		jsonObject.put("accountNumber",account.getAccountNumber());
		
		WebResource webResource = ServiceManager.getWebResource("ReportManager/saveAepsSettlementData");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,JSONObject.toJSONString(jsonObject));
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}
	

	@Override
	public String sendAmountSettlementRequestAgent(String aeAgentId, String aettlementAmount, String serviceCharges,String account,String mode) {
		// TODO Auto-generated method stub
		
		JSONObject jObject=new JSONObject();
		jObject.put("agentid",aeAgentId);
		jObject.put("debitAmount", aettlementAmount);
		jObject.put("serviceCharges", serviceCharges);
		jObject.put("accountNumber", account);
		jObject.put("txnMode", mode);
		
		WebResource webResource = ServiceManager.getWebResource("ReportManager/saveAmountAepsSettlementAgent");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jObject.toString());
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}

	
	
	@Override
	public String sendAmountSettlementRequest(String aeAgentId, String aettlementAmount, String serviceCharges) {
		// TODO Auto-generated method stub
		
		JSONObject jObject=new JSONObject();
		jObject.put("agentid",aeAgentId);
		jObject.put("debitAmount", aettlementAmount);
		jObject.put("serviceCharges", serviceCharges);
		
		
		WebResource webResource = ServiceManager.getWebResource("ReportManager/saveAmountAepsSettlement");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jObject.toString());
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}

	@Override
	public String changeServiceStatus(String statusService) {
		// TODO Auto-generated method stub
		
		WebResource webResource = ServiceManager.getWebResource("WalletUtil/changeServiceStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,statusService);

		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		return output;
	}

	@Override
	public String getServiceStatusByAggId(String serviceRequest) {
		// TODO Auto-generated method stub
		
		WebResource webResource = ServiceManager.getWebResource("WalletUtil/getServiceStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,serviceRequest);

		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}
	
	
	@Override
	public AEPSLedger getAepsDetailsView(String txnId) {
		JSONObject  requestPayload=new JSONObject (); 
		 Gson gson = new Gson();
		 String jsonText =  gson.toJson(txnId);
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAepsDetailsView");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		    return null;
		 }
		 String output = response.getEntity(String.class);  
		 AEPSLedger av= gson.fromJson(output, AEPSLedger.class);
		 return av;
		
	}
	
	
	@Override
	public OnboardStatusResponse onboardStatus(String agentId) {
		// TODO Auto-generated method stub
		System.out.println("-------------"+agentId);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/onboardStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Gson gson=new Gson();
		Map<String,String> pp=gson.fromJson(output, Map.class);
		OnboardStatusResponse onboardResponse=new OnboardStatusResponse();
		onboardResponse=gson.fromJson(output,OnboardStatusResponse.class);
		//String jsonStr = gson.toJson(pp);
		
		return onboardResponse;
	}

	
	@Override
	public String saveDmtPricing(DmtPricing pricing) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/saveDmtPricing");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	@Override
	public String deletePricing(DmtPricing pricing) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/deletePricing");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	@Override
	public WalletMastBean getPlan(String agentId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/getPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		Map<String,String> pp=gson.fromJson(output, Map.class);
		WalletMastBean walletMast=new WalletMastBean();
		walletMast=gson.fromJson(output,WalletMastBean.class);
		
		return walletMast;
	}

	@Override
	public String addNewPlan(String id) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		//String jsonText =  gson.toJson(mast);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/addNewPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,id);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
	/*	Map<String,String> pp=gson.fromJson(output, Map.class);
		WalletMastBean walletMast=new WalletMastBean();
		walletMast=gson.fromJson(output,WalletMastBean.class);
	*/	
		return output;
	}
	
	
	
	@Override
	public String updateAgentPlan(WalletMastBean mast) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(mast);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAgentPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
	/*	Map<String,String> pp=gson.fromJson(output, Map.class);
		WalletMastBean walletMast=new WalletMastBean();
		walletMast=gson.fromJson(output,WalletMastBean.class);
	*/	
		return output;
	}

	
	@Override
	public String updateNewPlan(CommPercMaster mast) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(mast);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/updateNewPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
	/*	Map<String,String> pp=gson.fromJson(output, Map.class);
		WalletMastBean walletMast=new WalletMastBean();
		walletMast=gson.fromJson(output,WalletMastBean.class);
	*/	
		return output;
	}
	
	
	@Override
	public String agentPlan(String agentId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/agentPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
	/*	Map<String,String> pp=gson.fromJson(output, Map.class);
		WalletMastBean walletMast=new WalletMastBean();
		walletMast=gson.fromJson(output,WalletMastBean.class);
	*/	
		return output;
	}
	
	@Override
	public Map<String, String> aggPlan(String aggId) {
		JSONObject  requestPayload=new JSONObject (); 
		//requestPayload.put("userId",aggId);
		//String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/aggPlan");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public String agentPricingDetail(String agentId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/agentPricingDetail");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		// List<DmtPricing> pp = gson.fromJson(output, OTOList24.class);
	
		
		return output;
	}
	
	@Override
	public String updateAgentAccount(AddBankAccount bank) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(bank);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAgentAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		// List<DmtPricing> pp = gson.fromJson(output, OTOList24.class);
	
		
		return output;
	}	
	
	@Override
	public String distributorDetails(String customerId) {
		logger.debug("**************************** calling leanAccountDetails ************************************");
		Gson gson = new Gson();
		//String jsonText=gson.toJson(aggId);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUser/distributorDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,customerId);
	
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
	    String jsonStr = gson.toJson(pp);
		
	    return jsonStr;
	}
	
	
	@Override
	public String validateEmail(String email) {
/*
		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("pan",pan);   
		requestPayload.put("aggreatorid",aggreatorid);
		requestPayload.put("userType",userType);
		String jsonText = JSONValue.toJSONString(requestPayload);
*/
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/validateEmail");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,email);
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		return output;
	}
	

	@Override
	public String validateMobile(String mobile) {

		WebResource webResource=ServiceManager.getWebResource("WalletUtil/validateMobile");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,mobile);
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		return output;
	}
	
	
	@Override
	public String validateAadhaar(String adhar,String aggreatorid,int userType) {

		JSONObject  requestPayload=new JSONObject ();    
		requestPayload.put("adhar",adhar);   
		requestPayload.put("aggreatorid",aggreatorid);
		requestPayload.put("usertype",userType);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/validateAadhaar");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		return output;
	}
	
	
	
	@Override
	public String saveAccount(AddBankAccount account) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/saveAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	@Override
	public String agentAccountDetails(String agentId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/agentAccountDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,agentId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		// List<DmtPricing> pp = gson.fromJson(output, OTOList24.class);
	
		
		return output;
	}
	
	
	@Override
	public String deleteUserAccount(AddBankAccount account) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/deleteUserAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	
	@Override
	public String updateUserAccount(AddBankAccount account) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(account);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/updateUserAccount");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		Map<String,String> pp=gson.fromJson(output, Map.class);
		String jsonStr = gson.toJson(pp);
		
		return jsonStr;
	}
	
	@Override
	public Map<String, String> agentAccountList(String userId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("userId",userId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/agentAccountList");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}

	
	@Override
	public String agentAccounts(String account,String id) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("accountNumber",account);
		requestPayload.put("id",id);
		String jsonText = JSONValue.toJSONString(requestPayload);
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/agentAccounts");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		// List<DmtPricing> pp = gson.fromJson(output, OTOList24.class);
	
		
		return output;
	}
	
	
	
	@Override
	public ServiceConfig  getFlag(String aggId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
	
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/getFlag");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		//List<ServiceConfig> pp = gson.fromJson(output, OTOList25.class);
		Map<String,String> pp=gson.fromJson(output, Map.class);
		ServiceConfig walletConfig = gson.fromJson(output,ServiceConfig.class);
	
		return walletConfig;
	}
	
	@Override
	public String getServicesStatus(String serviceRequest) {
		// TODO Auto-generated method stub
		
		WebResource webResource = ServiceManager.getWebResource("WalletUser/getServicesStatus");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,serviceRequest);

		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}
	
	@Override
	public String  updateFlag(String aggId) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
	
		//String jsonText =  gson.toJson(pricing);
		WebResource webResource=ServiceManager.getWebResource("WalletUser/updateFlag");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,aggId);
	
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		//List<ServiceConfig> pp = gson.fromJson(output, OTOList25.class);
		//Map<String,String> pp=gson.fromJson(output, Map.class);
		//ServiceConfig walletConfig = gson.fromJson(output,ServiceConfig.class);
	
		return output;
	}
	
	@Override
	public String saveNewRegs(NewRegs reg) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String jsonText =  gson.toJson(reg);
		WebResource webResource = ServiceManager.getWebResource("WalletUser/saveNewRegs");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(output);
		return jsonObj.toString();
	}
	
	
	@Override
	public Map<String,String> getSuperDist(String soId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggId",soId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSuperDist");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		
		//Map<String, Object> map = new HashMap<>();
		//String value = new Gson().toJson(jo);
		
		return convertToMap(jo);
	   //return value;
	}
	
	
	@Override
	public Map<String,String> getSoDist(String soId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggId",soId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getSoDist");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		
		//Map<String, Object> map = new HashMap<>();
		//String value = new Gson().toJson(jo);
		
		return convertToMap(jo);
	   //return value;
	}
	
	
	@Override
	public Map<String,String> getDistributorUnderSd(String sdId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggId",sdId);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributorUnderSd");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		 
		return convertToMap(jo); 
	}
	
	
	@Override
	public  DistributorIdAllot getDistributorAllotid(String id,String aggId) {
		logger.debug("*****************calling method  getDistributorAllotid ***********************"+id);
		Gson gson = new Gson();
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggreatorid",aggId);
		requestPayload.put("userId",id);
		String jsonText = JSONValue.toJSONString(requestPayload);
		//String jsonText=gson.toJson(id);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributorAllotid");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		DistributorIdAllot list = gson.fromJson(output,DistributorIdAllot.class);
		System.out.println(list);
		
		return list;
	}
	
	
	@Override
	public String rejectAgentByMuser(String aggId,String createdBy,String agentId,String declinedComment) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("userId",agentId);
		requestPayload.put("declinedComment", declinedComment);
		requestPayload.put("createdBy", createdBy);
		requestPayload.put("aggreatorid", aggId);
		
		
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		 Gson gson = new Gson();
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/rejectAgentByMuser");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 return output;
	}

	
	
	@Override
	public List<DeclinedListBean> declinedNewAgentList(String aggregatorId,String createdBy,int type) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggreatorid",aggregatorId);
		requestPayload.put("createdby",createdBy);
		requestPayload.put("usertype",type);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		 Gson gson = new Gson();
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/declinedNewAgentList");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public List<WalletMastBean> getAgentListNew(int usertype,String createdby,String id,String stDate,String endDate) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("id",id);
		requestPayload.put("stDate",stDate);
		requestPayload.put("endDate",endDate);
		requestPayload.put("usertype", usertype);
		requestPayload.put("createdby", createdby);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		 Gson gson = new Gson();
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAgentListNew");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public  DistributorIdAllot updateTokenId(WalletMastBean mast) {
		logger.debug("*****************calling method  updateTokenId ***********************"+mast.getSuperdistributerid());
		Gson gson = new Gson(); 
		String jsonText =  gson.toJson(mast);
		
		//JSONObject  requestPayload=new JSONObject (); 
		//String jsonText = JSONValue.toJSONString(mast);
		//String jsonText=gson.toJson(id);
		WebResource webResource=ServiceManager.getWebResource("ReportManager/updateTokenId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service updateTokenId***********************"+output);
		DistributorIdAllot list = gson.fromJson(output,DistributorIdAllot.class);
		System.out.println(list);
		
		return list;
	}

	
	
	@Override
	public List<AssignedUnit> getUserCount(int type,String aggregatorId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("aggreatorid",aggregatorId);
		requestPayload.put("usertype",type);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		 Gson gson = new Gson();
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/getUserCount");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<AssignedUnit> list = gson.fromJson(output,OTOList26.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public List<WalletMastBean> getUserManagement(String id,int type,String aggregatorId) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("id",id);
		requestPayload.put("aggreatorid",aggregatorId);
		requestPayload.put("usertype",type);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		 Gson gson = new Gson();
		 WebResource webResource=ServiceManager.getWebResource("WalletUser/getUserManagement");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public Map<String, String> getAgentBySuper(String id,String aggId,int type) {
		JSONObject  requestPayload=new JSONObject (); 
		requestPayload.put("distId",id);
		requestPayload.put("aggId",aggId);
		requestPayload.put("userType",type);
		String jsonText = JSONValue.toJSONString(requestPayload);
		
		WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentBySuper");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	
	@Override
	public List<AEPSLedger> getAepsReportSuper(WalletMastBean walletBean) {
		
		JSONObject  requestPayload=new JSONObject ();
		Gson gson = new Gson();
		String jsonText=gson.toJson(walletBean);
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getAepsReportSuper");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<AEPSLedger> list = gson.fromJson(output,OTOList20.class);
		 System.out.println(list);
		 return list;
	}
	
	@Override
	public List<WalletMastBean> getUserEnable() {
		
		JSONObject  requestPayload=new JSONObject ();
		Gson gson = new Gson();
		
		WebResource webResource = ServiceManager.getWebResource("ReportManager/getUserEnable");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
		 System.out.println(list);
		 return list;
	}
	
	
	@Override
	public List<CmsBean> getCmsReportForAgent(WalletMastBean walletBean) {
		
		JSONObject  requestPayload=new JSONObject ();
		 Gson gson = new Gson();
		 String jsonText=gson.toJson(walletBean);
		 WebResource webResource = ServiceManager.getWebResource("ReportManager/getCmsReportForAgent");
		 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

		 if (response.getStatus() != 200){
		     throw new RuntimeException("Failed : HTTP error code : "
		   + response.getStatus());
		 }
		 String output = response.getEntity(String.class);  
		 List<CmsBean> list = gson.fromJson(output,OTOList28.class);
		 System.out.println(list);
		 return list;
	}
	
	
	
	
}





class OTOList27 extends ArrayList<AgentDetailsView>{
	
}
class OTOList26 extends ArrayList<AssignedUnit>{
	
}
class OTOList extends ArrayList<WalletMastBean>{
	
}
class OTOList1 extends ArrayList<SenderProfileBean>{
	
}
class OTOList2 extends ArrayList<UserWishListBean>{
	
}
class OTOList3 extends ArrayList<CustomerProfileBean>{
 
}
class OTOList4 extends ArrayList<WalletKYCBean>{
	 
}

class OTOList5 extends ArrayList<SmartCardBean>{
	 
}
class OTOList6 extends ArrayList<Coupons>{
	 
}
class OTOList7 extends HashMap<String,Object>{
	 
}
class OTOList8 extends ArrayList<CouponsBean>{
	 
}
class OTOList9 extends ArrayList<MerchantOffersMast>{
	
}

class OTOList10 extends ArrayList<SenderFavouriteViewBean>{
	
}

class OTOList11 extends ArrayList<BankDetailsBean>{
	
}

class OTOList12 extends ArrayList<BankMastBean>{
	
}

class OTOList13 extends ArrayList<DeclinedListBean>{
	
}

class OTOList14 extends ArrayList<RevenueReportBean>{
	
}


class OTOList15 extends ArrayList<DmtDetailsMastBean>{
	
}

class OTOList16 extends ArrayList<MudraSenderPanDetails>{
	
}
class OTOList17 extends ArrayList<MoneyRequestBean>{
	
}
class OTOList18 extends ArrayList<RevokeUserDtl>{
	
}
class OTOList19 extends ArrayList<UploadSenderKyc>{
	
}
class OTOList20 extends ArrayList<AEPSLedger>{
	
}
class OTOList21 extends ArrayList<String>{
	
}
class OTOList22 extends ArrayList<MATMLedger>{
	
}

class OTOList23 extends ArrayList<AepsSettlementBean>{
	
}
class OTOList24 extends ArrayList<DmtPricing>{
	
}

class OTOList25 extends ArrayList<ServiceConfig>{
	
}
class OTOList28 extends ArrayList<CmsBean>{
	
}
