

package com.bhartipay.wallet.user.service.impl.bk2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.bhartipay.biller.bean.WalletBean;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.recharge.bean.UserWishListBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.ResponseBean;
import com.bhartipay.wallet.transaction.persistence.vo.PGPayeeBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserWalletConfigBean;
import com.bhartipay.wallet.user.dao.PCUser;
import com.bhartipay.wallet.user.persistence.vo.AgentDetailsView;
import com.bhartipay.wallet.user.persistence.vo.BankDetailsBean;
import com.bhartipay.wallet.user.persistence.vo.BankMastBean;
import com.bhartipay.wallet.user.persistence.vo.Coupons;
import com.bhartipay.wallet.user.persistence.vo.CouponsBean;
import com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean;
import com.bhartipay.wallet.user.persistence.vo.DMTReportInOut;
import com.bhartipay.wallet.user.persistence.vo.DeclinedListBean;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.FundTransactionSummaryBean;
import com.bhartipay.wallet.user.persistence.vo.LoginResponse;
import com.bhartipay.wallet.user.persistence.vo.MailConfigMast;
import com.bhartipay.wallet.user.persistence.vo.MerchantOffersMast;
import com.bhartipay.wallet.user.persistence.vo.MudraBeneficiaryMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderMastBean;
import com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails;
import com.bhartipay.wallet.user.persistence.vo.RevenueReportBean;
import com.bhartipay.wallet.user.persistence.vo.SMSConfigMast;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.SenderProfileBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardBean;
import com.bhartipay.wallet.user.persistence.vo.SmartCardOutputBean;
import com.bhartipay.wallet.user.persistence.vo.UploadKyc;
import com.bhartipay.wallet.user.persistence.vo.UserBlockedBean;
import com.bhartipay.wallet.user.persistence.vo.UserMenuMapping;
import com.bhartipay.wallet.user.persistence.vo.UserRoleMapping;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.persistence.vo.WalletKYCBean;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.bk2.UserService;
//import com.appnit.bhartipay.wallet.user.service.impl.UserServiceImpl;
//import com.appnit.bhartipay.wallet.user.service.impl.OTOList10;
//import com.appnit.bhartipay.wallet.user.service.impl.OTOList11;
//import com.appnit.bhartipay.wallet.user.service.impl.OTOList9;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import appnit.com.crypto.RSAEncryptionWithAES;

/**
 * 
 * @author Ambuj Singh
 * UserService Impl
 *
 */
public class UserServiceImpl implements UserService{
	
	Logger logger=Logger.getLogger(UserServiceImpl.class);

	@Override
	public Map<String, String> getCountry() {

		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCountry");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}
	
	@Override
	public Map<String, String> getCountryCode() {

		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCountryCode");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);			
		JsonParser parser = new JsonParser();
		JsonObject jo = (JsonObject) parser.parse(output);
		return convertToMap(jo);
	}

@Override
public String saveSignUpUser(UserSummary user,String ipimei,String userAgent) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("countrycode",user.getCountrycode());   
	requestPayload.put("name",user.getName());
	requestPayload.put("mobileno",user.getMobileno());
	requestPayload.put("emailid",user.getEmailid());
	requestPayload.put("usertype",user.getUsertype());
	requestPayload.put("password",user.getPassword());
	requestPayload.put("createdby",user.getCreatedby());
	requestPayload.put("agentid",user.getAgentid());
	requestPayload.put("aggreatorid",user.getAggreatorid());
	requestPayload.put("distributerid",user.getDistributerid());
	requestPayload.put("subAgentId", user.getSubAgentId());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	String encText="";
	try {
		String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
		encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
		String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(user.getAggreatorid()));

		JSONObject  jsonObj=new JSONObject (); 
		jsonObj.put("aggreatorid",user.getAggreatorid());
		jsonObj.put("encText",encText);
		jsonObj.put("encKey", encAesKey);
		jsonText=JSONValue.toJSONString(jsonObj);
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signup");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}


@Override
public String saveSignUpUserByAgent(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	String encText="";
	try {
	String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
	encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
	String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(walletBean.getAggreatorid()));
	
	JSONObject  jsonObj=new JSONObject (); 
	jsonObj.put("aggreatorid",walletBean.getAggreatorid());
	jsonObj.put("encText",encText);
	jsonObj.put("encKey", encAesKey);
	jsonText=JSONValue.toJSONString(jsonObj);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signup");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);


	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public WalletMastBean saveAgentOnBoard(WalletMastBean walletBean,String ipimei,String userAgent) {

	logger.debug("******************calling agentOnBoard service response***********************"+walletBean.getApplicationFee());
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/agentOnBoard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}


@Override
public WalletMastBean updateAgentOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAgentOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	
	logger.debug("******************calling agentOnBoard service response***********************"+output);
	WalletMastBean	mBean=gson.fromJson(output,WalletMastBean.class);
	return mBean;
}


@Override
public LoginResponse login(UserSummary user,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",user.getUserId());   
	requestPayload.put("password",user.getPassword());
	requestPayload.put("imeiIP","null");
	requestPayload.put("aggreatorid",user.getAggreatorid());
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	String encText="";
	try {
		//encText=RSAEncryptionWithAES.encryptAESKey(jsonText,ServiceManager.getProperty(user.getAggreatorid()));
		String aesKey =	RSAEncryptionWithAES.getSecretAESKeyAsString();
		encText = RSAEncryptionWithAES.encryptTextUsingAES(jsonText, aesKey);
		String	encAesKey =RSAEncryptionWithAES.encryptAESKey(aesKey,ServiceManager.getProperty(user.getAggreatorid()));
		
		JSONObject  jsonObj=new JSONObject (); 
		jsonObj.put("aggreatorid",user.getAggreatorid());
		jsonObj.put("encText",encText);
		jsonObj.put("encKey", encAesKey);
		jsonText=JSONValue.toJSONString(jsonObj);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/login");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	LoginResponse loginResponse=new LoginResponse();
	Gson gson=new Gson();
	loginResponse=gson.fromJson(output,LoginResponse.class);
	return loginResponse;
}



@Override
public String validateOTP(UserSummary user) {
JSONObject  requestPayload=new JSONObject (); 
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	
	
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}

@Override
public String verifyOTP(UserSummary user) {
	
Gson gson = new Gson();
String jsonText=gson.toJson(user);

	WebResource webResource=ServiceManager.getWebResource("WalletUser/verifyOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
	
	return output;
}

@Override
public String otpResend(UserSummary user) {
JSONObject  requestPayload=new JSONObject (); 
	

Gson gson = new Gson();
String jsonText=gson.toJson(user);

	
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public Map<String, String> ProfilebyloginId(UserSummary user) {
	
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",user.getUserId());
	requestPayload.put("aggreatorid", user.getAggreatorid());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/ProfilebyloginId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getWalletConfig(WalletConfiguration configuration) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


public WalletConfiguration getConfiguration(WalletConfiguration configuration){

	Gson gson = new Gson();
	String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configuration = gson.fromJson(output, WalletConfiguration.class);
	return configuration;
	
}



@Override
public Map<String, String> ProfilebytxnId(String txnId) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("txnId",txnId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/profilebytxnId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public WalletMastBean showUserProfile(String userId) {
JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",userId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/showUserProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
//	JsonParser parser = new JsonParser();
//	JsonObject jo = (JsonObject) parser.parse(output);
///	return convertToMap(jo);
	
	
	
	Gson gson = new Gson();
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;

}



@Override
public WalletMastBean editProfile(WalletMastBean mastBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/editProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;
}



@Override
public Map<String, String> getKyc() {

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getKycId() {

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getIdProofKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public String resetPassword(WalletMastBean walletBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changePassword");	
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI","").post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String saveKycDocuments(UploadKyc uploadKyc,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(uploadKyc);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public Map<String, String> getCustomerByAgentId(double userType,String agentId) {
	JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",agentId);
	if(userType==99)
	requestPayload.put("userType","99");
	if(userType==1)
		requestPayload.put("userType","1");
	if(userType==2)
		requestPayload.put("userType","2");
	if(userType==3)
		requestPayload.put("userType","3");
	if(userType==4)
		requestPayload.put("userType","4");
	if(userType==5)
		requestPayload.put("userType","5");
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getCustomerByAgentId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public UserWalletConfigBean getWalletConfigByUserId(UserWalletConfigBean configBean) {
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(configBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getWalletConfigByUserId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return walletBean;
}



@Override
public UserWalletConfigBean saveLimit(UserWalletConfigBean config,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(config);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveUserWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return walletBean;
}



@Override
public String getProfilePic(String userId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("userId",userId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getProfilePic");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
//	Gson gson = new Gson();
//	UserWalletConfigBean walletBean = gson.fromJson(output, UserWalletConfigBean.class);
	
	return output;
}



@Override
public PGPayeeBean profilebyPGtxnId(String txnId) {
	
	Gson gson=new Gson();
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("txnId",txnId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("TransactionManager/profilebyPGtxnId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	PGPayeeBean payeeBean = gson.fromJson(output, PGPayeeBean.class);
	return payeeBean;
}



@Override
public Map<String, String> getAggreator() {

//	JSONObject  requestPayload=new JSONObject (); 
//	requestPayload.put("userId",userId);
//	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getDistributerByAggId(String aggregatorId) {
	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getAgentByDistId(String distributorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentByDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public Map<String, String> getActiveDistributerByAggId(String aggregatorId) {
	
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getActiveDistributerByAggId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public Map<String, String> getActiveAgentByDistId(String distributorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("distId",distributorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getActiveAgentByDistId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public Map<String, String> getSubAgentByAgentId(String agentid){
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("agentId",agentid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSubAgentByAgentId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}


@Override
public List<WalletMastBean> searchUser(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/searchUser");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	return list;
}



@Override
public List<SenderProfileBean> getSenderProfile(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getSenderProfile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SenderProfileBean> list = gson.fromJson(output,OTOList1.class);
	System.out.println(list);
	for(SenderProfileBean pb:list){
		System.out.println(pb.getPayerEmail());
		System.out.println(pb.getPayermobile());
	}
	return list;
}



@Override
public UserWishListBean saveWishList(UserWishListBean wishBean,String ipimei,String userAgent) {
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	 wishBean = gson.fromJson(output, UserWishListBean.class);
	return wishBean;
}



@Override
public List<UserWishListBean> getWishList(UserWishListBean wishBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	List<UserWishListBean> list = gson.fromJson(output,OTOList2.class);
	// wishBean = gson.fromJson(output, UserWishListBean.class);
	return list;
}



@Override
public String deleteWishList(UserWishListBean wishBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(wishBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/deleteWishList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}



@Override
public WalletConfiguration saveWalletConfiguration(
		WalletConfiguration configuration,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveWalletConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	 configuration = gson.fromJson(output, WalletConfiguration.class);
	return configuration;
}



@Override
public String forgotPassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/forgotPassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}

@Override
public String setForgotPassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/setForgotPassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}


@Override
public String setFirstTimePassword(UserSummary bean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/firstTimePasswordChange");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}






@Override
public List<CustomerProfileBean> getCustomerProfile(String aggreatorid) {
 Gson gson = new Gson();
 JSONObject  requestPayload=new JSONObject (); 
requestPayload.put("aggId",aggreatorid);
String jsonText = JSONValue.toJSONString(requestPayload);
 
 
 WebResource webResource=ServiceManager.getWebResource("ReportManager/getCustomerDetail");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200){
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 List<CustomerProfileBean> list = gson.fromJson(output,OTOList3.class);
 System.out.println(list);
 for(CustomerProfileBean pb:list){
  System.out.println(pb.getEmailid());
  System.out.println(pb.getFinalBalance());
 }
 return list;
}



@Override
public List<WalletMastBean> getPendingAgentByAggId(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getPendingAgentByAggId");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}



@Override
public String acceptAgentByAgg(WalletMastBean mastBean) {

	 Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	String jsonText = gson.toJson(mastBean);
	
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/acceptAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;
	
}


@Override
public AgentDetailsView agentdetailsview(WalletMastBean walletMastBean) {
	JSONObject  requestPayload=new JSONObject (); 
	 Gson gson = new Gson();
	 String jsonText =  gson.toJson(walletMastBean);
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/agentdetailsview");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	AgentDetailsView av= gson.fromJson(output, AgentDetailsView.class);
	 return av;
	
}



@Override
public String rejectAgentByAgg(String agentId,String declinedComment) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("userId",agentId);
	requestPayload.put("declinedComment", declinedComment);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/rejectAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;

}



@Override
public List<WalletKYCBean> getKycList(ReportBean reportBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(reportBean);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getKycList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<WalletKYCBean> list = gson.fromJson(output,OTOList4.class);
	return list;
}

public Map<String, String> convertToMap(JsonObject jsonObject) {
	Gson gson = new Gson();
	Map<String, String> myMap = gson.fromJson(jsonObject, Map.class);
	return myMap;
}





@Override
public MailConfigMast emailConfiguration(MailConfigMast configMast) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMailConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, MailConfigMast.class);
	return configMast;
}





@Override
public MailConfigMast saveMailConfiguration(MailConfigMast configMast,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveMailConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, MailConfigMast.class);
	return configMast;
}





@Override
public SMSConfigMast smsConfiguration(SMSConfigMast configMast) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getSMSConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, SMSConfigMast.class);
	return configMast;
}





@Override
public SMSConfigMast saveSMSConfiguration(SMSConfigMast configMast,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(configMast);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveSMSConfiguration");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	configMast = gson.fromJson(output, SMSConfigMast.class);
	return configMast;
}





@Override
public String saveProfilePic(WalletMastBean walletBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/saveProfilePic");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public String getcurrencyByCountryId(WalletConfiguration configuration) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("countryId",configuration.getCountryid());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getcurrencyByCountryId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String getcurrencyByCountryCode(WalletConfiguration configuration) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("countryCode",configuration.getCountryCode());
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getcurrencyByCountryCode");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());//
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public String signUpUserByUpload(WalletMastBean walletBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/signUpUserByUpload");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





/*@Override
public String otpResend(String userId) {
JSONObject  requestPayload=new JSONObject (); 
	
	requestPayload.put("userId",userId);	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public Map<String, String> getWalletConfig(WalletConfiguration configuration) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfig");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}
*/


@Override
public String getAggIdByDomain(WalletConfiguration config) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(config);
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getAggIdByDomain");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	return output;
}





@Override
public Map<String, String> getTxnTypeDtl() {
	WebResource webResource=ServiceManager.getWebResource("CommissionManager/getTxnTypeDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public String acceptKyc(String refId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("refid",refId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/acceptKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}




@Override
public String rejectKyc(String refId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("refid",refId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/rejecttKyc");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}


@Override
public String validateChangeMobile(String userId,String aggreatorid,String mobileNo) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateChangeMobile");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String validateChangeEmail(String userId,String aggreatorid,String email) {
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("emailId",email);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/validateChangeEmail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String changeMobileNo(String userId,String aggreatorid,String mobileNo,String otp) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	requestPayload.put("otp",otp);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeMobileNo");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}


@Override
public String changeEmailId(String userId,String aggreatorid,String email,String otp) {
	
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("emailId",email);
	requestPayload.put("otp",otp);
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeEmailId");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}



@Override
public String changeOtpResend(String userId,String aggreatorid,String mobileNo) {
	
	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("userId",userId);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("mobileNo",mobileNo);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	WebResource webResource=ServiceManager.getWebResource("WalletUser/changeOtpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}





@Override
public Map<String, String> getMenu() {
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMenu");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public Map<String, String> getSubAggreator(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getSubAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}





@Override
public UserMenuMapping saveUserMenuMapping(UserMenuMapping menuMapping,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	

	Gson gson = new Gson();
	String jsonText=gson.toJson(menuMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveUserMenuMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		menuMapping=gson.fromJson(output, UserMenuMapping.class);
		return menuMapping;
}





@Override
public UserMenuMapping getUserMenuMapping(UserMenuMapping menuMapping) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(menuMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getUserMenuMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		menuMapping=gson.fromJson(output, UserMenuMapping.class);
		return menuMapping;
}





@Override
public PCUser pcRequest(PCUser user) {
	
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(user);		
		WebResource webResource=ServiceManager.getWebResource("paymentCard/pcRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		user=gson.fromJson(output, PCUser.class);
		return user;
}





@Override
public SmartCardBean generateSmartCard(SmartCardBean smartCardBean,String ipimei,String userAgent) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/generateSmartCardReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updatePrePaidCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updatePrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updateAddress(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updateAddress");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public SmartCardBean updatePassword(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/updatePassword");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		  // throw new RuntimeException("Failed : HTTP error code : "
		//	+ response.getStatus());
		   return null;
	}
	String output = response.getEntity(String.class);
	smartCardBean=gson.fromJson(output, SmartCardBean.class);
	return smartCardBean;
	
}

@Override
public List<SmartCardBean> getSmartCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getSmartCardReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}

@Override
public List<SmartCardBean> getSmartCardList(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getSmartCardList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}

@Override
public SmartCardBean linkCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}

@Override
public SmartCardBean linkCardBlocked(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkCardBlocked");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}


@Override
public List<SmartCardBean> getdispatchCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getdispatchCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	List<SmartCardBean> list = gson.fromJson(output,OTOList5.class);
	return list;
}


@Override
public SmartCardBean dispatchCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/dispatchCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	 smartCardBean = gson.fromJson(output,SmartCardBean.class);
	return smartCardBean;
}


@Override
public SmartCardBean getPrePaidCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getPrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	 smartCardBean = gson.fromJson(output,SmartCardBean.class);
	return smartCardBean;
}


@Override
public String rejectSmartCard(SmartCardBean smartCardBean){
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/rejecttSmartCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	return output;
}



@Override
public SmartCardOutputBean acceptSmartCard(SmartCardBean smartCardBean){
 Gson gson = new Gson();
 String jsonText=gson.toJson(smartCardBean);
 
 WebResource webResource=ServiceManager.getWebResource("WalletUser/acceptSmartCard");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
 if (response.getStatus() != 200){
  System.out.println(response.getStatus());
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);
 SmartCardOutputBean smartCardOutputBean =gson.fromJson(output, SmartCardOutputBean.class);
 return smartCardOutputBean;
}


public List<Coupons> getCoupons(String  appkey) throws Exception {
	Gson gson = new Gson();
	String rest="";
	URL url = new URL("https://tools.vcommission.com/api/coupons.php?apikey=93f099e30a2d1dfe6ee5ab3517493855cf188557ffbf7524b0992f31b24f8df8");
    HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
    urlconnection.setRequestMethod("POST");
    urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
    urlconnection.setDoOutput(true);
    OutputStreamWriter outa = new OutputStreamWriter(urlconnection.getOutputStream());
   // outa.write(postData);
    outa.close();
    BufferedReader in = new BufferedReader( new InputStreamReader(urlconnection.getInputStream()));
    String decodedString;
    while ((decodedString = in.readLine()) != null) {
        System.out.println("decodedString~~~~~~~~```"+decodedString);
        rest=decodedString;
  }
    in.close();
    String output=rest;

	
	
	
	
	//String output = response.getEntity(String.class);		
	List<Coupons> list = gson.fromJson(output,OTOList6.class);
	return list;
}





@Override
public Map<String, Object> getCouponsCategory() {
Gson gson = new Gson();
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCouponsCategory");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Map<String,Object> list = gson.fromJson(output,OTOList7.class);
	
//	Map<String,String> catMap=(Map<String, String>)list.get("category");
//	List<CouponsBean> couponList=(List<CouponsBean>)list.get("couponList");

return list;
}





@Override
public List<CouponsBean> getCouponsByCategory(CouponsBean couponsBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getCouponsByCategory");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	List<CouponsBean> list=gson.fromJson(output,OTOList8.class);
	return list;
}





@Override
public String mailCoupan(CouponsBean couponsBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/mailCoupan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	
	return output;
	
}

@Override
public List<WalletMastBean> getUnAuthorizedAggreator() {
	Gson gson = new Gson();
	//String jsonText=gson.toJson(couponsBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getUnAuthorizedAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	List<WalletMastBean> list=gson.fromJson(output,OTOList.class);
	return list;
}

@Override
public String authorizedAggreator(WalletMastBean walletBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/authorizedAggreator");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);				
	
	return output;
}

@Override
public String uploadMerchantOffer(String aggregatorid,
		List<MerchantOffersMast> offerList) {
	
	Gson gson=new Gson();
	String arrayList=gson.toJson(offerList);
	JSONObject  requestPayload=new JSONObject ();    
	
	requestPayload.put("aggreatorid",aggregatorid);
	requestPayload.put("offerList",arrayList);
	
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/uploadMerchantOffer");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public List<MerchantOffersMast> getMerchantOffer(String aggregatorid) {
	Gson gson=new Gson();
	JSONObject  requestPayload=new JSONObject ();    
	
	requestPayload.put("aggreatorid",aggregatorid);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getMerchantOffer");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return gson.fromJson(output, OTOList9.class);
}

@Override
public String fetchingCreatedUserdetail(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCreatedUserdetail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingWalletdetail(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingWalletdetail");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingCardType(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCardType");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public String fetchingCardTypeCode(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/fetchingCardTypeCode");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}

@Override
public SmartCardBean suspendedCard(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/suspendedCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean resumePrePaidCard(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/resumePrePaidCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean generateCVV(String reqId) {
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("reqId",reqId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/generateCVV");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	Gson gson=new Gson();
	SmartCardBean bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean pinReset(SmartCardBean bean) {
	Gson gson=new Gson();
	
	String jsonText = gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/pinReset");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	
	 bean= gson.fromJson(output,SmartCardBean.class);
	return bean;
}

@Override
public SmartCardBean linkVirtualCard(SmartCardBean smartCardBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(smartCardBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/linkVirtualCard");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200){
		System.out.println(response.getStatus());
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	SmartCardBean list = gson.fromJson(output,SmartCardBean.class);
	return list;
}

@Override
public List<SenderFavouriteViewBean> getFavouriteList(SenderFavouriteBean bean, String ipimei, String userAgent) {
logger.debug("******************calling getFavouriteList service ***********************");
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getFavouriteList");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getFavouriteList service response***********************"+output);
	List<SenderFavouriteViewBean> bList=gson.fromJson(output,OTOList10.class);
	return bList;
}

@Override
public MudraSenderMastBean validateSender(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling validateSender service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/validateSender");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling validateSender service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public MudraSenderMastBean registerSender(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling registerSender service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/registerSender");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling registerSender service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public String dmtOtpResend(UserSummary user) {
	logger.debug("******************calling dmtOtpResend service ***********************");
JSONObject  requestPayload=new JSONObject (); 
Gson gson = new Gson();
String jsonText=gson.toJson(user);
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/otpResend");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling dmtOtpResend service response***********************"+output);
	return output;
}

@Override
public BankDetailsBean getBankDetails(BankDetailsBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDetails service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getBankDetails");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDetails service response***********************"+output);
	BankDetailsBean bankDetails=gson.fromJson(output,BankDetailsBean.class);
	return bankDetails;
}

@Override
public List<BankDetailsBean> getBankDetailsByIFSC(BankDetailsBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDetailsByIFSC service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getBankDetailsByIFSC");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDetailsByIFSC service response***********************"+output);
	List<BankDetailsBean> bankDetail=gson.fromJson(output,OTOList11.class);
	return bankDetail;
}

@Override
public MudraBeneficiaryMastBean registerBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling registerBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/registerBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling registerBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}


@Override
public DMTReportInOut initiateDMTRefundByAgent(DMTReportInOut bean, String ipimei, String userAgent) {
		logger.debug("******************calling initiateDMTRefundByAgent service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/initiateDMTRefundByAgent");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling initiateDMTRefundByAgent service response***********************"+output);
		DMTReportInOut reportInOut=gson.fromJson(output,DMTReportInOut.class);
		return reportInOut;
}



@Override
public MudraBeneficiaryMastBean deleteBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/deleteBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling deleteBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}

@Override
public MudraBeneficiaryMastBean acceptBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/acceptBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling deleteBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}

@Override
public MudraBeneficiaryMastBean rejectBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
		logger.debug("******************calling deleteBeneficiary service ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(bean);
		
		WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/rejectBeneficiary");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		logger.debug("******************calling rejectBeneficiary service response***********************"+output);
		MudraBeneficiaryMastBean bankDetail=gson.fromJson(output,MudraBeneficiaryMastBean.class);
		return bankDetail;
}



@Override
public SenderFavouriteBean setFavourite(SenderFavouriteBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling setFavourite service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/setFavourite");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling setFavourite service response***********************"+output);
	bean=gson.fromJson(output,SenderFavouriteBean.class);
	return bean;
}

@Override
public SenderFavouriteBean deleteFavourite(SenderFavouriteBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling deleteFavourite service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/deleteFavourite");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling deleteFavourite service response***********************"+output);
	bean=gson.fromJson(output,SenderFavouriteBean.class);
	return bean;
}

@Override
public MudraSenderMastBean forgotMPIN(MudraSenderMastBean mastBean, String ipimei, String userAgent) {
	logger.debug("******************calling forgotMPIN service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/forgotMPIN");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling forgotMPIN service response***********************"+output);
	MudraSenderMastBean mudra=gson.fromJson(output,MudraSenderMastBean.class);
	return mudra;
}

@Override
public MudraSenderMastBean updateMPIN(MudraSenderMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling updateMPIN service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/updateMPIN");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling updateMPIN service response***********************"+output);
	bean=gson.fromJson(output,MudraSenderMastBean.class);
	return bean;
}

@Override
public MudraSenderMastBean getBeneficiaryListForImport(MudraSenderMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBeneficiaryListForImport service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getBeneficiaryListForImport");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBeneficiaryListForImport service response***********************"+output);
	bean=gson.fromJson(output,MudraSenderMastBean.class);
	return bean;
}

@Override
public MudraMoneyTransactionBean calculateSurCharge(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/calculateSurCharge");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
    bean=gson.fromJson(output,MudraMoneyTransactionBean.class);
 return bean;
}



@Override
public MudraBeneficiaryMastBean getBeneficiary(String beneficiaryId) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 JSONObject  requestPayload=new JSONObject ();    
requestPayload.put("id",beneficiaryId);
 String jsonText=gson.toJson(requestPayload);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getBeneficiary");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
 MudraBeneficiaryMastBean bean=gson.fromJson(output,MudraBeneficiaryMastBean.class);
 return bean;
}






@Override
public MudraMoneyTransactionBean verifyAccount(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/verifyAccount");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
    bean=gson.fromJson(output,MudraMoneyTransactionBean.class);
 return bean;
}


@Override
public FundTransactionSummaryBean fundTransfer(MudraMoneyTransactionBean bean, String ipimei, String userAgent) {
logger.debug("******************calling calculateSurCharge service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/fundTransfer");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling calculateSurCharge service response***********************"+output);
 FundTransactionSummaryBean resultBean=gson.fromJson(output,FundTransactionSummaryBean.class);
 return resultBean;
}
public FundTransactionSummaryBean getTransactionDetailsByTxnId(MudraMoneyTransactionBean bean,String ipimei,String userAgent){

logger.debug("******************calling getTransactionDetailsByTxnId service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(bean);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getTransactionDetailsByTxnId");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling getTransactionDetailsByTxnId service response***********************"+output);
 FundTransactionSummaryBean resultBean=gson.fromJson(output,FundTransactionSummaryBean.class);
 return resultBean;
	
	
}



@Override
public List<RefundTransactionBean> initiateDMTRefund(DmtDetailsMastBean dmtMast) {
logger.debug("******************calling initiateDMTRefund service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(dmtMast);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/initiateDMTRefund");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling initiateDMTRefund service response***********************"+output);
 List<RefundTransactionBean> resultBean=gson.fromJson(output,OTOList17.class);
 return resultBean;
}

@Override
public String markFailedDmtTxn(DmtDetailsMastBean dmtMast) {

logger.debug("******************calling initiateDMTRefund service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(dmtMast);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/markFailedDmtTxn");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling markFailedDmtTxn service response***********************"+output);
 //List<RefundTransactionBean> resultBean=gson.fromJson(output,OTOList17.class);
 return output;

}

@Override
public DmtDetailsMastBean checkDMTStatus(DmtDetailsMastBean dmtMast) {
logger.debug("******************calling checkDMTStatus service ***********************");
 
 Gson gson = new Gson();
 String jsonText=gson.toJson(dmtMast);
 
 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/checkDMTStatus");
 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

 if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
 }
 String output = response.getEntity(String.class);  
 logger.debug("******************calling checkDMTStatus service response***********************"+output);
 DmtDetailsMastBean resultBean=gson.fromJson(output,DmtDetailsMastBean.class);
 return resultBean;
}

@Override
public SurchargeBean calculateSurcharge(SurchargeBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling calculateSurcharge service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/calculateSurcharge");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling calculateSurcharge service response***********************"+output);
	bean=gson.fromJson(output,SurchargeBean.class);
	return bean;
}

@Override
public MudraSenderMastBean activeBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling activeBeneficiary service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/activeBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling activeBeneficiary service response***********************"+output);
MudraSenderMastBean	mBean=gson.fromJson(output,MudraSenderMastBean.class);
	return mBean;
}




@Override
public DMTReportInOut getTransacationLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getTransacationLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getTransacationLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getTransacationLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}



@Override
public DMTReportInOut getAgentLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getAgentLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getAgentLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getAgentLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}



@Override
public DMTReportInOut getSenerLedgerDtl(DMTReportInOut bean, String ipimei, String userAgent) {
	logger.debug("******************calling getSenerLedgerDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getSenderLedgerDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getSenerLedgerDtl service response***********************"+output);
	DMTReportInOut	mBean=gson.fromJson(output,DMTReportInOut.class);
	return mBean;
}






@Override
public MudraSenderMastBean deActiveBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	logger.debug("******************calling getBankDtl service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/deActiveBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	logger.debug("******************calling getBankDtl service response***********************"+output);
    MudraSenderMastBean	mBean=gson.fromJson(output,MudraSenderMastBean.class);
	return mBean;
}










@Override
public MudraBeneficiaryMastBean copyBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
logger.debug("******************calling copyBeneficiary service ***********************");
	
	Gson gson = new Gson();
	String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/copyBeneficiary");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);		
	logger.debug("******************calling copyBeneficiary service response***********************"+output);
    bean=gson.fromJson(output,MudraBeneficiaryMastBean.class);
	return bean;
}

@Override
public List<BankMastBean> getBankDtl() {
	logger.debug("******************calling getBankDtl service ***********************");
	Gson gson = new Gson();
	//String jsonText=gson.toJson(bean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getBankDtl");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getBankDtl service response***********************"+output);
    List<BankMastBean> bean=gson.fromJson(output,OTOList12.class);
	return bean;
}



@Override
public List<DeclinedListBean> getPendingAgentByAggId(String aggregatorId,String createdBy) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	requestPayload.put("createdBy",createdBy);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getPendingAgentByAggId");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<DeclinedListBean> getRejectAgentDetail(String aggregatorId,String createdBy) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	requestPayload.put("createdBy",createdBy);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getRejectAgentDetail");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}



@Override
public WalletMastBean editAgent(WalletMastBean mastBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/editAgent");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	WalletMastBean walletBean = gson.fromJson(output, WalletMastBean.class);
	
	return walletBean;
}

@Override
public List<WalletMastBean> getAgentList(String id,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("id",id);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<WalletMastBean> getApprovedAgentList(String aggreatorid,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getApprovedAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}


@Override
public List<DeclinedListBean> getRejectAgentList(String aggreatorid,String stDate,String endDate) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("stDate",stDate);
	requestPayload.put("endDate",endDate);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getRejectAgentList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DeclinedListBean> list = gson.fromJson(output,OTOList13.class);
	 System.out.println(list);
	 return list;
}




@Override
public Map<String, String> getRole() {
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getRole");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}




@Override
public UserRoleMapping saveUserRoleMapping(UserRoleMapping roleMapping,String ipimei,String userAgent) {
	JSONObject  requestPayload=new JSONObject (); 
	

	Gson gson = new Gson();
	String jsonText=gson.toJson(roleMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/saveUserRoleMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		roleMapping=gson.fromJson(output, UserRoleMapping.class);
		return roleMapping;
}




@Override
public UserRoleMapping getUserRoleMapping(UserRoleMapping roleMapping) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(roleMapping);	
		
		WebResource webResource=ServiceManager.getWebResource("WalletUtil/getUserRoleMapping");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		
		if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		String output = response.getEntity(String.class);	
		roleMapping=gson.fromJson(output, UserRoleMapping.class);
		return roleMapping;
}




@Override
public String validatePan(String pan,String aggreatorid,int userType) {

	JSONObject  requestPayload=new JSONObject ();    
	requestPayload.put("pan",pan);   
	requestPayload.put("aggreatorid",aggreatorid);
	requestPayload.put("userType",userType);
	String jsonText = JSONValue.toJSONString(requestPayload);

	WebResource webResource=ServiceManager.getWebResource("WalletUtil/validatePan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}




@Override
public List<RevenueReportBean> getRevenueReportList(WalletMastBean walletBean) {
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	 WebResource webResource=ServiceManager.getWebResource("ReportManager/getRevenueReportList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<RevenueReportBean> list = gson.fromJson(output,OTOList14.class);
	 System.out.println(list);
	 return list;
}



@Override
public List<DmtDetailsMastBean> getDmtDetailsReportList(WalletMastBean walletBean) {
	JSONObject  requestPayload=new JSONObject ();
	 Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	 WebResource webResource=ServiceManager.getWebResource("ReportManager/getDmtDetailsReportList");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<DmtDetailsMastBean> list = gson.fromJson(output,OTOList15.class);
	 System.out.println(list);
	 return list;
}



@Override
public String blockUnblockUser(UserBlockedBean userBlockedBean) {
	Gson gson = new Gson();
	String jsonText=gson.toJson(userBlockedBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/blockUnblockUser");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	return output;
}




@Override
public Map<String, String> getAgentDister(String aggreatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggreatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("ReportManager/getAgentDister");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}



@Override
public String distributerOnBoad(WalletMastBean walletBean,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(walletBean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/distributerOnBoad");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	return output;
}




@Override
public MudraSenderPanDetails uploadSenderPanF60(MudraSenderPanDetails mudraSenderPanDetails,String ipimei,String userAgent) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/uploadSenderPanF60");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	mudraSenderPanDetails=gson.fromJson(output, MudraSenderPanDetails.class);
	return mudraSenderPanDetails;
}





@Override
public List<MudraSenderPanDetails> getPendingPanReq(MudraSenderPanDetails mudraSenderPanDetails) {
	logger.debug("*****************calling method  getPendingPanReq ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	logger.debug("*****************json text as parameter to calling service getPendingPanReq***********************"+jsonText);
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getPendingPanReq");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	logger.debug("*****************response come from getPendingPanReq***********************");
	if (response.getStatus() != 200){
		logger.debug("*****************response come***********************"+response.getStatus());	
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	logger.debug("*****************response come from service***********************"+response);
	String output = response.getEntity(String.class);		
	logger.debug("*****************response come from service string***********************"+output);
	List<MudraSenderPanDetails> list = gson.fromJson(output,OTOList16.class);
	System.out.println(list);
	
	return list;
}




@Override
public MudraSenderPanDetails rejectAcceptedSenderPan(MudraSenderPanDetails mudraSenderPanDetails) {

	Gson gson = new Gson();
	String jsonText=gson.toJson(mudraSenderPanDetails);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/rejectAcceptedSenderPan");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
	
	
	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);	
	mudraSenderPanDetails=gson.fromJson(output, MudraSenderPanDetails.class);
	return mudraSenderPanDetails;
}





@Override
public List<WalletMastBean> getAgentDetailForApprove(String aggregatorId) {
	JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("aggId",aggregatorId);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	 Gson gson = new Gson();
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/getAgentDetailForApprove");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 List<WalletMastBean> list = gson.fromJson(output,OTOList.class);
	 System.out.println(list);
	 return list;
}



@Override
public String approvedAgentByAgg(WalletMastBean mastBean) {

	 Gson gson = new Gson();
	JSONObject  requestPayload=new JSONObject (); 
	String jsonText = gson.toJson(mastBean);
	
	 WebResource webResource=ServiceManager.getWebResource("WalletUser/approvedAgentByAgg");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200){
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class);  
	 return output;
	
}



@Override
public Map<String, String> getWalletConfigPG(WalletConfiguration configuration) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(configuration);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUtil/getWalletConfigPG");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);			
	JsonParser parser = new JsonParser();
	JsonObject jo = (JsonObject) parser.parse(output);
	return convertToMap(jo);
}

public WalletMastBean getUserByMobileNo(String mobileNo,String aggId)
{
	/*JSONObject  requestPayload=new JSONObject (); 
	requestPayload.put("mobileNo",mobileNo);
	requestPayload.put("aggId",aggId);
	String jsonText = JSONValue.toJSONString(requestPayload);*/
	WalletMastBean req = new WalletMastBean();
	Gson gson = new Gson();
	req.setMobileno(mobileNo);
	req.setAggreatorid(aggId);
	String jsonText = gson.toJson(req);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/getUserByMobileNo");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	
	String output = response.getEntity(String.class);	
	WalletMastBean bean=gson.fromJson(output, WalletMastBean.class);
	return bean;

}


@Override
public LoginResponse customerValidateOTP(WalletBean walletbean) {

	
Gson gson = new Gson();
String jsonText=gson.toJson(walletbean);
	
	WebResource webResource=ServiceManager.getWebResource("WalletUser/customerValidateOTP");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	LoginResponse loginResponse=new LoginResponse();
	/*Gson gson=new Gson();*/
	loginResponse=gson.fromJson(output,LoginResponse.class);
	return loginResponse;
	
}

public MudraSenderMastBean deletedBeneficiary(MudraBeneficiaryMastBean bean, String ipimei, String userAgent) {
	 logger.debug("******************calling getBankDtl service ***********************");
	 Gson gson = new Gson();
	 String jsonText=gson.toJson(bean);
	 
	 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/deletedBeneficiary");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT", userAgent).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling getBankDtl service response***********************"+output);
	    MudraSenderMastBean mBean=gson.fromJson(output,MudraSenderMastBean.class);
	 return mBean;
	}

public ResponseBean refundAgent(String senderMobile,String txnId,String userId) {

	 logger.debug("******************calling deletedBeneficiary service ***********************");
	 Gson gson = new Gson();
	 HashMap<String,String> bean = new HashMap<String,String>();
	 bean.put("senderMobile", senderMobile);
	 bean.put("txnId", txnId);
	 bean.put("userId", userId);
	 String jsonText=gson.toJson(bean);
	 
	 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/refundAgent");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);


	 if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling deletedBeneficiary service response***********************"+output);
	 ResponseBean mBean=gson.fromJson(output,ResponseBean.class);
	 return mBean;
	}


public ResponseBean refundAgentRequest(String senderMobile,String txnId,String userId,String mpin) {
	 logger.debug("******************calling deletedBeneficiary service ***********************");
	 Gson gson = new Gson();
	 HashMap<String,String> bean = new HashMap<String,String>();
	 bean.put("senderMobile", senderMobile);
	 bean.put("txnId", txnId);
	 bean.put("userId", userId);
	 bean.put("mpin", mpin);
	 String jsonText=gson.toJson(bean);
	 
	 WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/refundAgentRequest");
	 ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	 if (response.getStatus() != 200) {
	     throw new RuntimeException("Failed : HTTP error code : "
	   + response.getStatus());
	 }
	 String output = response.getEntity(String.class); 
	 logger.debug("******************calling deletedBeneficiary service response***********************"+output);
	 ResponseBean mBean=gson.fromJson(output,ResponseBean.class);
	 return mBean;
	}

@Override
public HashMap getAgentSenderBalance(MudraSenderMastBean mastBean) {
	logger.debug("******************calling getAgentSenderBalance service ***********************");
	Gson gson = new Gson();
	String jsonText=gson.toJson(mastBean);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/getAgentSenderBalance");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling getAgentSenderBalance service response***********************"+output);
	HashMap mudra=gson.fromJson(output,HashMap.class);
	return mudra;
}


@Override
public String checkLimit(String mob, String id) {
	logger.debug("******************calling checkLimit service ***********************");
	JSONObject  requestPayload=new JSONObject (); 	
	requestPayload.put("mobileno",mob);
	requestPayload.put("id",id);
	String jsonText = JSONValue.toJSONString(requestPayload);
	
	WebResource webResource=ServiceManager.getWebResource("MudraManagerBK2/checkLimit");
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);

	if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
	}
	String output = response.getEntity(String.class);
	logger.debug("******************calling checkLimit service response***********************"+output);
	
	return output;
}




}







class OTOList extends ArrayList<WalletMastBean>{
	
}
class OTOList1 extends ArrayList<SenderProfileBean>{
	
}
class OTOList2 extends ArrayList<UserWishListBean>{
	
}
class OTOList3 extends ArrayList<CustomerProfileBean>{
 
}
class OTOList4 extends ArrayList<WalletKYCBean>{
	 
}

class OTOList5 extends ArrayList<SmartCardBean>{
	 
}
class OTOList6 extends ArrayList<Coupons>{
	 
}
class OTOList7 extends HashMap<String,Object>{
	 
}
class OTOList8 extends ArrayList<CouponsBean>{
	 
}
class OTOList9 extends ArrayList<MerchantOffersMast>{
	
}

class OTOList10 extends ArrayList<SenderFavouriteViewBean>{
	
}

class OTOList11 extends ArrayList<BankDetailsBean>{
	
}

class OTOList12 extends ArrayList<BankMastBean>{
	
}

class OTOList13 extends ArrayList<DeclinedListBean>{
	
}

class OTOList14 extends ArrayList<RevenueReportBean>{
	
}


class OTOList15 extends ArrayList<DmtDetailsMastBean>{
	
}

class OTOList16 extends ArrayList<MudraSenderPanDetails>{
	
}

class OTOList17 extends ArrayList<RefundTransactionBean>{
	
}