package com.bhartipay.wallet.framework.db.util;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * HibernateDBCon class is use for getting the object of SessionFactory
 * @author Bhartipay
 *
 */
public class HibernateDBCon {
	public static Logger logger=Logger.getLogger(HibernateDBCon.class); 
	private static final SessionFactory sessionFactory = buildSessionFactory();
	/**
	 * buildSessionFactory method is use for building the SessionFactory object
	 * @return returning the SessionFactory object
	 */
	public static SessionFactory buildSessionFactory() {
		logger.debug("********************************************calling buildSessionFactory()********************************************");
		try {

			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			logger.debug("problem in buildingsessionfactory",ex);
			throw new RuntimeException();
		}
		
	}
	public static SessionFactory getSessionFactory() {
		return sessionFactory;

	}
	
	
}
