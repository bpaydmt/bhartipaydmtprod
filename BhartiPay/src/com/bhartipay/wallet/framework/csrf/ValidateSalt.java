package com.bhartipay.wallet.framework.csrf;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.google.common.cache.Cache;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class ValidateSalt implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {
		ActionContext context = arg0.getInvocationContext();
		Map mSession = context.getSession();
		Map mRequest = context.getParameters();

		String s[] = (String[]) mRequest.get("csrfPreventionSalt");
		String salt = s[0];
		// Validate that the salt is in the cache
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>) mSession.get("csrfPreventionSaltCache");

		if (csrfPreventionSaltCache != null && salt != null
				&& csrfPreventionSaltCache.getIfPresent(salt) != null) {
			// If the salt is in the cache, we move on
			csrfPreventionSaltCache.invalidate(salt);
			return arg0.invoke();
		} else {
			// Otherwise we throw an exception aborting the request flow
			throw new ServletException(
					"Potential CSRF detected!! Inform a scary sysadmin ASAP.");
		}
	}
}
