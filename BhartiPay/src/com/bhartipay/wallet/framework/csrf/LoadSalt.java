package com.bhartipay.wallet.framework.csrf;


import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.TimeUnit;



import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoadSalt implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {

		ActionContext context = arg0.getInvocationContext();
		Map mSession = context.getSession();
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>) mSession.get("csrfPreventionSaltCache");

		if (csrfPreventionSaltCache == null) {
			csrfPreventionSaltCache = CacheBuilder.newBuilder()	.maximumSize(5000).expireAfterWrite(5, TimeUnit.MINUTES).build();
			mSession.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
		}

		// Generate the salt and store it in the users cache
		String salt = RandomStringUtils.random(20, 0, 0, true, true, null,new SecureRandom());
		csrfPreventionSaltCache.put(salt, Boolean.TRUE);

		mSession.put("csrfPreventionSalt", salt);

		return arg0.invoke();

	}
}
