package com.bhartipay.wallet.framework.service.utility;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class ServiceManager {
	public static Logger logger=Logger.getLogger(ServiceManager.class);
	
	
public static WebResource getWebResource(String lastURL) {
	logger.debug("***************  calling method getWebResource for getting service URL  *********** ");
	
	System.out.println("______Inside Service manager______getWebResource(String URL)__________");
	
	WebResource webResource=null;
	try{
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);

			Properties prop = new Properties();
			InputStream in = ServiceManager.class.getResourceAsStream("Infrastructure.properties");
			
			try {
				prop.load(in);
				in.close();
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			webResource = client.resource(prop.getProperty("serviceurl")+lastURL);
			
			System.out.println("________WebResource_________"+webResource);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getWebResource for getting service URL  *********** "+e);
			e.printStackTrace();
	}
	return webResource;
}

public static WebResource getCouponUrl(String lastURL) {
	logger.debug("***************  calling method getWebResource for getting service URL  *********** ");
	WebResource webResource=null;
	try{
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);

			Properties prop = new Properties();
			InputStream in = ServiceManager.class
					.getResourceAsStream("Infrastructure.properties");
			try {
				prop.load(in);
				in.close();
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			webResource = client.resource(prop.getProperty("couponurl")+lastURL);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getWebResource for getting service URL  *********** "+e);
			e.printStackTrace();
	}
	return webResource;
}

public static String getProperty(String propertyName) {
	logger.debug("***************  calling method getWebResource for getting getProperty value  *********** ");
	try{
			

			Properties prop = new Properties();
			InputStream in = ServiceManager.class
					.getResourceAsStream("Infrastructure.properties");
			
				prop.load(in);
				in.close();
			
			return prop.getProperty(propertyName);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getWebResource for getting getProperty value  *********** "+e);
			e.printStackTrace();
	}
	return "";
}

public static WebResource getAsAepsTokenUrl() {
	logger.debug("***************  calling method getAsAepsTokenUrl for getting service URL  *********** ");
	
	System.out.println("______Inside Service getAsAepsTokenUrl(String URL)__________");
	
	WebResource webResource=null;
	try{
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);

			Properties prop = new Properties();
			InputStream in = ServiceManager.class.getResourceAsStream("Infrastructure.properties");
			
			try {
				prop.load(in);
				in.close();
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			webResource = client.resource(prop.getProperty("aadharshilatokenurl"));
			
			System.out.println("________getAsAepsTokenUrl_________"+webResource);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getAsAepsTokenUrl for getting service URL  *********** "+e);
			e.printStackTrace();
	}
	return webResource;
}

public static WebResource getFinoAepsUrl() {
	logger.debug("***************  calling method getFinoAepsUrl for getting service URL  *********** ");
	
	System.out.println("______Inside Service getFinoAepsUrl(String URL)__________");
	
	WebResource webResource=null;
	try{
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);

			Properties prop = new Properties();
			InputStream in = ServiceManager.class.getResourceAsStream("Infrastructure.properties");
			
			try {
				prop.load(in);
				in.close();
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			webResource = client.resource(prop.getProperty("finoaepsurl"));
			
			System.out.println("________getFinoAepsUrl_________"+webResource);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getFinoAepsUrl for getting service URL  *********** "+e);
			e.printStackTrace();
	}
	return webResource;
}

public static WebResource getCmsTokenUrl() {
	logger.debug("***************  calling method getAsAepsTokenUrl for getting service URL  *********** ");
	
	System.out.println("______Inside Service getAsAepsTokenUrl(String URL)__________");
	
	WebResource webResource=null;
	try{
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);

			Properties prop = new Properties();
			InputStream in = ServiceManager.class.getResourceAsStream("Infrastructure.properties");
			
			try {
				prop.load(in);
				in.close();
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			webResource = client.resource(prop.getProperty("cmstokenurl"));
			
			System.out.println("________getAsAepsTokenUrl_________"+webResource);
			

	}catch (Exception e) {
		logger.debug("***************  problem in getAsAepsTokenUrl for getting service URL  *********** "+e);
			e.printStackTrace();
	}
	return webResource;
}



}
