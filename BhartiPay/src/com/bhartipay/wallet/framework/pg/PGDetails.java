package com.bhartipay.wallet.framework.pg;

import org.apache.log4j.Logger;

/**
 * PGDetails is setting the parameter values as for requirement of 
 * payment gateway api
 * @author WebInitiate
 *
 */
public class PGDetails {

	public static Logger logger=Logger.getLogger(PGDetails.class); 
	String custName = "Appnit Technologies";
	String custAddress = "403, Tower A, 4th Floor, Logix Technova, sector 132";
	String custCity = "Noida";
	String custState = "Uttar Pradesh";
	String custPinCode = "201301";
	String custCountry = "IN";
	String custPhoneNo1 = "01206900881";
	String custPhoneNo2 = "01206900882";
	String custPhoneNo3 = "01206900881";
	String custMobileNo = "9335235731";
	String custEmailId = "sandeep.prajapati@appnittech.com";
	String otherNotes = "appnit";
/**
 * getBillingDtls is use fro getting the billing details for 
 * hitting the PG api
 * @return return billing details as string
 */
	public String getBillingDtls() {
		logger.debug("*********************************calling getBillingDtls()****************************************");

		String billingDtls = custName + "|" + custAddress + "|" + custCity
				+ "|" + custState + "|" + custPinCode + "|" + custCountry + "|"
				+ custPhoneNo1 + "|" + custPhoneNo2 + "|" + custPhoneNo3 + "|"
				+ custMobileNo + "|" + custEmailId + "|" + otherNotes;
		logger.debug("*********************************return value on calling getBillingDtls()****************************************");
		logger.debug("*********************************"+billingDtls+"****************************************");

		return billingDtls;
	}

	String deliveryName = "Appnit";
	String deliveryAddress = "403, Tower A, 4th Floor, Logix Technova, sector 132";
	String deliveryCity = "Noida";
	String deliveryState = "Uttar Pradesh";
	String deliveryPinCode = "201301";
	String deliveryCountry = "IN";
	String deliveryPhNo1 = "01206900881";
	String deliveryPhNo2 = "01206900881";
	String deliveryPhNo3 = "01206900882";
	String deliveryMobileNo = "9335235731";
/**
 * getShippingDtls is use for getting the shipping details for hitting the
 * PG api
 * @return return shipping details as string
 */
	public String getShippingDtls() {
		logger.debug("*********************************calling getShippingDtls()****************************************");
		String shippingDtls = deliveryName + "|" + deliveryAddress + "|"
				+ deliveryCity + "|" + deliveryState + "|" + deliveryPinCode
				+ "|" + deliveryCountry + "|" + deliveryPhNo1 + "|"
				+ deliveryPhNo2 + "|" + deliveryPhNo3 + "|" + deliveryMobileNo;
		logger.debug("*********************************return value on calling getshippingDtls()****************************************");
		logger.debug("*********************************"+shippingDtls+"****************************************");

		return shippingDtls;
	}

}
