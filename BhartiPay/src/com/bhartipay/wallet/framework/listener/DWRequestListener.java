package com.bhartipay.wallet.framework.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DWRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestInitialized(ServletRequestEvent arg0) {
		System.out.println("-----------------------------------------------------------------------------request listener start --------------------------------------------------------------------------------");
		HttpServletRequest request=(HttpServletRequest)arg0.getServletRequest();
		HttpSession session=request.getSession();
		String domainName1=(String)session.getAttribute("domainName");
		String domainName2=request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).substring(0, request.getRequestURL().substring(request.getRequestURL().indexOf("//")+2).indexOf("/"));
		System.out.println(domainName1);
		System.out.println(domainName2);
		if(domainName1!=null&&domainName2!=null&&domainName1.equalsIgnoreCase(domainName2)){
			System.out.println("-----------------------------------------------------------------------------request listener if --------------------------------------------------------------------------------");

		}else{
			System.out.println("-----------------------------------------------------------------------------request listener else --------------------------------------------------------------------------------");
	
			request.getSession().removeAttribute("User");
			session.invalidate();
		}
		System.out.println("-----------------------------------------------------------------------------request listener end --------------------------------------------------------------------------------");

	}

}
