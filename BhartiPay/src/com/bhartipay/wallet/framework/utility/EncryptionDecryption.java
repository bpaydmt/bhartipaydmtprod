package com.bhartipay.wallet.framework.utility;

import java.io.IOException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptionDecryption {

	
	
	public static String getKey(){
		
		String key = null;
        try {
        	KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    		//keyGenerator.init(128);
        	keyGenerator.init(256);
    		SecretKey secretKey = keyGenerator.generateKey();
    		byte[] raw = secretKey.getEncoded();
            key = new BASE64Encoder().encode((byte[])raw);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return key;
		
	}

	
	
	private static SecretKey genKeyFromString(String key) throws IOException  {
		byte[] decodedKey =  new BASE64Decoder().decodeBuffer(key);
		SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		return secretKey;
	}
	
	
	private static String encrypt(String plainText, SecretKey secretKey)	throws Exception {
		
		Cipher cipher=Cipher.getInstance("AES");
		byte[] plainTextByte = plainText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		BASE64Encoder encoder = new BASE64Encoder();
		String encryptedText = encoder.encode(encryptedByte);
		return encryptedText;
	}

	
	private static String decrypt(String encryptedText, SecretKey secretKey)	throws Exception {
		Cipher cipher=Cipher.getInstance("AES");
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] encryptedTextByte = decoder.decodeBuffer(encryptedText);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}
	
	
	public static String getEncrypted(String value,String key) throws IOException, Exception{		
		return encrypt(value,genKeyFromString(key));
	}
	
	public static String getdecrypted(String value,String key) throws IOException, Exception{		
		return decrypt(value,genKeyFromString(key));
	}
	
	
	
	public static void main(String[] args) throws IOException, Exception {
		
		String val="koi mara kaya";
		
		String key=getKey();
		System.out.println("key~~~~~~~~~~~~~"+key);
		System.out.println("getEncrypted~~~~~~~~~~~~~"+getEncrypted(val,key));
		
		System.out.println("getdecrypted~~~~~~~~~~~~~"+getdecrypted(getEncrypted(val,key),key));
		
		
	}
	

}
