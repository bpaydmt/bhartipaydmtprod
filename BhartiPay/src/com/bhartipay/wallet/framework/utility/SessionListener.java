package com.bhartipay.wallet.framework.utility;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		HttpSession session=event.getSession();
		
		// TODO Auto-generated method stub
		 Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.getAttribute("csrfPreventionSaltCache");

		        if (csrfPreventionSaltCache == null){
		            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(180, TimeUnit.MINUTES).build();

		            session.setAttribute("csrfPreventionSaltCache", csrfPreventionSaltCache);
		        }

		        // Generate the salt and store it in the users cache
		        String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
		        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

		        session.setAttribute("csrfPreventionSalt", salt);
		       // session.setMaxInactiveInterval(300);

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}