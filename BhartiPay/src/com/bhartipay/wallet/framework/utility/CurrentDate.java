package com.bhartipay.wallet.framework.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
/**
 * CurrentDate class is use for getting current date and time
 * @author Bhartipay
 *
 */
public class CurrentDate {
	public static Logger logger=Logger.getLogger(CurrentDate.class);
	/**
	 * getCurrentDate is use for getting current date and time into string
	 * @return date and time into dd/MM/yyyy HH:mm:ss format
	 */
	public static String getCurrentDate() {
		logger.debug("***************  calling method getCurrentDate() for getting current date  *********** ");
		// getting current date and time using Date class
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateobj = new Date();
		System.out.println(df.format(dateobj));

		/*
		 * getting current date time using calendar class An Alternative of
		 * above
		 */
		Calendar calendar = Calendar.getInstance();
		String cdate = df.format(calendar.getTime());
		System.out.println(cdate);
		logger.debug("***************  end calling method getCurrentDate() for getting current date *********** ");
		return cdate;
	}
}
