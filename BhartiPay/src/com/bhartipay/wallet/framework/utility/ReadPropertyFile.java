package com.bhartipay.wallet.framework.utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadPropertyFile {
public static Properties readProperties(String fileName){
	Properties props=new Properties();
	try{
		props.load(new FileInputStream(new File(fileName)));
	}catch(Exception e){
		e.printStackTrace();
	}
	return props;
}
}
