package com.bhartipay.wallet.framework.utility;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.wallet.framework.db.util.HibernateDBCon;
import com.bhartipay.wallet.recharge.bean.RechargeOperatorCode;
/**
 * Trx is use for getting uniq transaction id
 * @author Bhartipay
 *
 */
public class Trx {
	/**
	 * getTrxId is use  for getting the uniq transaction id on the basis of service
	 * @param name service provider name like cyrus or jolo
	 * @return uniq transaction id
	 */
public static String getTrxId(String name) {
	SessionFactory factory=HibernateDBCon.getSessionFactory();
	Session session=factory.openSession();
	Transaction transaction=session.beginTransaction();
	String id="";
	try{
	Query query=session.createSQLQuery("select nextval('"+name+"')");
	List list=query.list();
	id=(String)list.get(0);
	transaction.commit();
	}catch(Exception e){
		transaction.rollback();
		System.out.println(e);
	}
	finally{
		session.close();
	}
	return id;
}
/**
 * getTrxId is use for getting the unique transaction id on the basis of operator name and service type
 * @param operatorName service operator name
 * @param serviceType service type like parpaid/postpaid
 * @return unique transaction id
 */
public static String getTrxId(String operatorName,String serviceType) {
	SessionFactory factory=HibernateDBCon.getSessionFactory();
	Session session=factory.openSession();
	Transaction transaction=session.beginTransaction();
	String id="";
	String serviceClient="";
	try{
		
		
		Query query=session.createQuery("from RechargeOperatorCode where operatorName= :oname and serviceType= :stype and flag= :flag");
		query.setString("oname",operatorName);
		query.setString("stype",serviceType);
		query.setInteger("flag",1);
		List<RechargeOperatorCode> list=query.list();
		serviceClient=list.get(0).getServiceClient();
		
		
		
		
	Query query1=session.createSQLQuery("select nextval('"+serviceClient+"')");
	List list1=query.list();
	id=(String)list1.get(0);
	transaction.commit();
	}catch(Exception e){
		transaction.rollback();
		System.out.println(e);
	}
	finally{
		session.close();
	}
	return id;
}



}
