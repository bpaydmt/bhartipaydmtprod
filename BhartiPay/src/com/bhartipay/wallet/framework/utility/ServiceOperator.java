package com.bhartipay.wallet.framework.utility;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bhartipay.wallet.framework.db.util.HibernateDBCon;

/**
 * 
 * @author Bhartipay
 * ServiceOperator class is use for getting some details like prepaid operator names,
 * post paid operators name, dth operators name and datacard  operators name
 * 
 */
public class ServiceOperator {
public static final Logger LOGGER=Logger.getLogger(ServiceOperator.class.getName());
	SessionFactory factory=HibernateDBCon.getSessionFactory();
	Session session;
	static Integer PrepaidOperatorint = 0;
	static Integer getPostpaidOperator = 0;               
	static Integer getDthOperator = 0;
	static Integer getDatacardOperator= 0;
	static Integer getCircle = 0;
	
	
	
	
	/**
	 * getPrepaidOperator is use for getting all operators  name supported by this application
	 * @return it return sortedset of prepaid operators
	 */
	public TreeSet<String> getPrepaidOperator(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("PrepaidOperatorint>>>>>>>"+ ++PrepaidOperatorint);
			session=factory.openSession();
			Query query=session.createSQLQuery("select name from rechargeoperatormaster where servicetype='PREPAID'and flag=1");
			List<String> list=query.list();
			treeSet.addAll(list);
			
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	
	/**
	 * getPostpaidOperator is use for getting the postpaid operators names
	 * @return return sorted set of postpaid operators
	 */ 
	public TreeSet<String> getPostpaidOperator(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("getPostpaidOperator>>>>>>>"+ ++getPostpaidOperator);
			session=factory.openSession();
			Query query=session.createSQLQuery("select name from rechargeoperatormaster where servicetype='POSTPAID'and flag=1");
			List<String> list=query.list();
			treeSet.addAll(list);
			
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	
	/**
	 * getDthOperator is use for getting the dth operators name
	 * @return sorted set of all dth operators
	 */
	public TreeSet<String> getDthOperator(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("getDthOperator>>>>>>>"+ ++getDthOperator);
			session=factory.openSession();
			Query query=session.createSQLQuery("select name from rechargeoperatormaster where servicetype='DTH'and flag=1");
			List<String> list=query.list();
			treeSet.addAll(list);
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	
	
	/**
	 * getDatacardOperator is use for getting the all datacard operators  name
	 * @return sorted set of all datacard operators name
	 */
	public TreeSet<String> getDatacardOperator(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("getDatacardOperator>>>>>>>"+ ++getDatacardOperator);
			session=factory.openSession();
			Query query=session.createSQLQuery("select name from rechargeoperatormaster where servicetype='DATACARD'and flag=1");
			List<String> list=query.list();
			treeSet.addAll(list);
			
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	
	
	public TreeSet<String> getLandLine(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("getDatacardOperator>>>>>>>"+ ++getDatacardOperator);
			session=factory.openSession();
			Query query=session.createSQLQuery("select name from rechargeoperatormaster where servicetype='LANDLINE'and flag=1");
			List<String> list=query.list();
			treeSet.addAll(list);
			
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	/**
	 * getCircle is use for getting the all circle names
	 * @return sorted set of all circle names
	 */
	public TreeSet<String> getCircle(){
		TreeSet<String> treeSet=new TreeSet<String>(new Comp());
		try{
			LOGGER.debug("getCircle>>>>>>>"+ ++getCircle);
			session=factory.openSession();
			Query query=session.createSQLQuery("select circle_name from recharge_circle_code");
			List<String> list=query.list();
			treeSet.addAll(list);
			
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return treeSet;
	}
	
	/**
	 * getServiceOperatorIcon is use for getting the serviceOperator icon url
	 * @param serviceType prepaid, postpaid, dth or datacard
	 * @param operatorName like airtel, vodafone etc
	 * @return url of icon
	 */
	public String getServiceIcon(String serviceType,String operatorName){
		String url="";
		try{
			session=factory.openSession();
			Query query=session.createSQLQuery("select icon_url from recharge_operator_master where service_type='"+serviceType+"' and operator_name='"+operatorName+"'");
			List<String> list=query.list();
			url=list.get(0);
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return url;
	}
	
	/**
	 * getTransactionCount is use for getting the number of transacion by a particular account id
	 * @param walletId account id 
	 * @return return number of transaction of a particular account id
	 */
	public  int getTransactionCount(String walletId){
		int number=0;
		try{
			session=factory.openSession();
			Query query=session.createSQLQuery("select count(wallet_id) from wallet_transaction where wallet_id='"+walletId+"'");
			List list=query.list();
			//System.out.println(((BigInteger)list.get(0)).intValue());
		number=((BigInteger)list.get(0)).intValue();
		}
		catch(Exception e){
			//transaction.rollback();
			LOGGER.debug("porblem in fetching PrepaidOperators "+e.getMessage(),e);
		}
		finally{
			session.close();
		}
		return number;
	}
	
	
	
	
	public static void main(String[] args) {
		ServiceOperator operator= new ServiceOperator();
	System.out.println(operator.getPrepaidOperator());
	System.out.println(operator.getPostpaidOperator());
	System.out.println(operator.getDatacardOperator());
	System.out.println(operator.getDthOperator());
	}
	
	
	
	
	class Comp implements Comparator<String>{

		@Override
		public int compare(String o1, String o2) {
			String str1=o1.trim();
			String str2=o2.trim();
			return str1.compareToIgnoreCase(str2);
		}
		
	}
	
}
