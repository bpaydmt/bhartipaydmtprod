package com.bhartipay.wallet.framework.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author ambuj singh The base class which will be extended by all action classes
 */
public class BaseAction extends ActionSupport implements SessionAware,ServletRequestAware{

	private static final long serialVersionUID = 1L;

	protected static final Logger logger = Logger.getLogger(BaseAction.class.getName());
	@SuppressWarnings("unchecked")
	protected Map session = null;
	protected HttpServletRequest request = null;


	
	/**
	 * Sets the session
	 */
	@SuppressWarnings("unchecked")
	public void setSession(Map session) {
		this.session = session;
	}

	/**
	 * 
	 * @return session
	 */
	@SuppressWarnings("unchecked")
	public Map getSession() {
		return session;
	}

	/**
	 * Sets the request
	 */
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * 
	 * @return request
	 */
	public HttpServletRequest getServletRequest() {
		return request;
	}

}
