package com.bhartipay.wallet.framework.security;

//import net.pg.appnit.utility.AppnitBase64Coder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;

import org.apache.log4j.Logger;
public class AES128Bit {
	public static Logger logger=Logger.getLogger(AES128Bit.class);
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static String iv              = "fedcba9876543210";
    public static String encrypt(String valueToEnc, String secretKey) {
    	logger.debug("**************************start encryption by AES128 bit*************************************");
        String encryptedValue = null;
        try {
        	
        	IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            Key key = AES128Bit.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(1, key,ivspec);
            byte[] encValue = c.doFinal(valueToEnc.getBytes("UTF-8"));
            encryptedValue = new String(AppnitBase64Coder.encode((byte[])encValue));
        	logger.debug("************************** encryption done by AES128 bit*************************************");

        }
        catch (Exception ex) {
        	logger.debug("**************************problem in encryption by AES128 bit*************************************");
        	logger.debug("problem in encryption by AES128 bit"+ex);

        	ex.printStackTrace();
        }
        return encryptedValue;
    }

    public static String decrypt(String encryptedValue, String secretKey) {
    	logger.debug("**************************start decryption by AES128 bit*************************************");
        String decryptedValue = null;
        try {
        	IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            Key key = AES128Bit.generateKeyFromString(secretKey);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(2, key,ivspec);
            byte[] decordedValue = AppnitBase64Coder.decode((String)encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);
            decryptedValue = new String(decValue);
            logger.debug("************************** decryption done by AES128 bit*************************************");
        }
        catch (Exception ex) {
        	logger.debug("**************************problem in decryption by AES128 bit*************************************");
        	logger.debug("problem in  decryption by AES128 bit"+ex);
            ex.printStackTrace();
        }
        return decryptedValue;
    }

    private static Key generateKeyFromString(String secretKey) throws Exception {
    	logger.debug("**************************start generating key from secretkey*************************************");
        byte[] keyValue=Arrays.copyOf(AppnitBase64Coder.decode((String)secretKey), 32);
        SecretKeySpec key = new SecretKeySpec(keyValue, "AES");
        return key;
    }

    public static String generateNewKey() {
    	logger.debug("**************************start generating new key*************************************");
        String newKey = null;
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128);
            SecretKey skey = kgen.generateKey();
            byte[] raw = skey.getEncoded();
            newKey = new String(AppnitBase64Coder.encode((byte[])raw));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return newKey;
    }

    public static void main(String[] args) {
    /*  String val=AES128Bit.encrypt("sandeep", "3flprMuiK7Dtg9GdQlIMJYVlCSrOq+nZ5tdQoPUBMMoo");
      System.out.println(val);
             val=AES128Bit.decrypt("9KUgC3D9jayK9nu+brG56w66", "3flprMuiK7Dtg9GdQlIMJYVlCSrOq+nZ5tdQoPUBMMoo");
      System.out.println(val);*/
    	//System.out.println("Key:"+new AES128Bit().generateNewKey());
    	try
    	{
    	System.out.println("Again Key:"+new AES128Bit().generateKeyFromString("Aldz62H0n1+2pcvkqC/Z1g=="));
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
}
