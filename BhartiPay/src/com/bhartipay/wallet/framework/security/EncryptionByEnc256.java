package com.bhartipay.wallet.framework.security;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class EncryptionByEnc256
{

 public EncryptionByEnc256()
 {
 }

 public static String encrypt(String textToEncrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(1, makeKey(key), makeIv());
         return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 public static String decrypt(String textToDecrypt, String key)
 {
     try
     {
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         cipher.init(2, makeKey(key), makeIv());
         return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt.getBytes())));
     }
     catch(Exception e)
     {
         throw new RuntimeException(e);
     }
 }

 private static AlgorithmParameterSpec makeIv()
 {
     try
     {
         return new IvParameterSpec("0123456789abcdef".getBytes("UTF-8"));
     }
     catch(UnsupportedEncodingException e)
     {
         e.printStackTrace();
     }
     return null;
 }

 private static Key makeKey(String encryptionKey)
 {
     try
     {
         byte key[] = Base64.decodeBase64(encryptionKey.getBytes());
         return new SecretKeySpec(key, "AES");
     }
     catch(Exception e)
     {
         e.printStackTrace();
     }
     return null;
 }

 public static String generateMerchantKey()
 {
     String newKey = null;
     try
     {
         KeyGenerator kgen = KeyGenerator.getInstance("AES");
         kgen.init(256);
         SecretKey skey = kgen.generateKey();
         byte raw[] = skey.getEncoded();
         newKey = new String(Base64.encodeBase64(raw));
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
     return newKey;
 }

 private static final String ENCRYPTION_IV = "0123456789abcdef";
 private static final String PADDING = "AES/CBC/PKCS5Padding";
 private static final String ALGORITHM = "AES";
 private static final String CHARTSET = "UTF-8";

 static 
 {
     try
     {
         Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
         field.setAccessible(true);
         field.set(null, Boolean.FALSE);
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
 }
}


