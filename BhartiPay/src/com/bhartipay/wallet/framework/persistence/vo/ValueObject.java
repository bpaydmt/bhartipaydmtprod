package com.bhartipay.wallet.framework.persistence.vo;

import java.io.Serializable;
/**
 * Base Class for all the persistable objects. 
 * @author anshuman
 *
 */
public class ValueObject implements Serializable {
	
	private static final long serialVersionUID = 109278426472222L;
	
	/*  Unique Identifier */
	private Long id;
	private int version;	
	
	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}


	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	} 

}
