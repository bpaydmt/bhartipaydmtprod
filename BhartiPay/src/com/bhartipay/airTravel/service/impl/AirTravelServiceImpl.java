package com.bhartipay.airTravel.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.airTravel.bean.RequestBean;
import com.bhartipay.airTravel.bean.Response;
import com.bhartipay.airTravel.bean.ResponseBean;
import com.bhartipay.airTravel.bean.TravelDtlBean;
import com.bhartipay.airTravel.service.AirTravelService;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class AirTravelServiceImpl  implements AirTravelService
{

	Logger logger = Logger.getLogger(AirTravelServiceImpl.class.getName());
	
	public ResponseBean getInitialSearch(RequestBean requestBean)
	{
		logger.info("******start execution of getInitialSearch****");
		ResponseBean response = new ResponseBean();
		try
		{
			String jsonStr = new Gson().toJson(requestBean);
			WebResource webResp = ServiceManager.getWebResource("AirTravel/getInitialSearch");
			ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
			int responseCode = clientResp.getStatus();
			if(responseCode != 200)
			{
				logger.debug("*****************response come***********************"+responseCode);	
				response.setResponse("fail");
				throw new RuntimeException("Failed : HTTP error code :"+responseCode);
			}
			response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
		}
		catch(Exception e)
		{
			logger.info("**********problem in getInitialSearch ***e.getMessage*"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	public ResponseBean getSearchFlight(RequestBean requestBean)
	{
		logger.info("******start execution of getInitialSearch****");
		ResponseBean response = new ResponseBean();
		try
		{
			String jsonStr = new Gson().toJson(requestBean);
			WebResource webResp = ServiceManager.getWebResource("AirTravel/flightSearch");
			ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
			int responseCode = clientResp.getStatus();
			if(responseCode != 200)
			{
				logger.debug("*****************response come***********************"+responseCode);	
				response.setResponse("fail");
				throw new RuntimeException("Failed : HTTP error code :"+responseCode);
			}
			response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
		}
		catch(Exception e)
		{
			logger.info("**********problem in getInitialSearch ***e.getMessage*"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public ResponseBean fareQuote(RequestBean requestBean)
	{
		logger.info("******start execution of fareQuote****");
		ResponseBean response = new ResponseBean();
		try
		{
			String jsonStr = new Gson().toJson(requestBean);
			WebResource webResp = ServiceManager.getWebResource("AirTravel/fareQuote");
			ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
			int responseCode = clientResp.getStatus();
			if(responseCode != 200)
			{
				logger.debug("*****************response come***********************"+responseCode);	
				response.setResponse("fail");
				throw new RuntimeException("Failed : HTTP error code :"+responseCode);
			}
			response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
		}
		catch(Exception e)
		{
			logger.info("**********problem in fareQuote ***e.getMessage*"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	


public ResponseBean flightBooking(RequestBean requestBean)
 {
  logger.info("******start execution of flightBooking****");
  ResponseBean response = new ResponseBean();
  try
  {
   String jsonStr = new Gson().toJson(requestBean);
   WebResource webResp = ServiceManager.getWebResource("AirTravel/flightBooking");
   ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
   int responseCode = clientResp.getStatus();
   if(responseCode != 200)
   {
    logger.debug("*****************response come***********************"+responseCode); 
    response.setResponse("fail");
    throw new RuntimeException("Failed : HTTP error code :"+responseCode);
   }
   response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
  }
  catch(Exception e)
  {
   logger.info("**********problem in flightBooking ***e.getMessage*"+e.getMessage());
   e.printStackTrace();
  }
  return response;
 }




public ResponseBean getTicketDetails(RequestBean requestBean)
 {
  logger.info("******start execution of flightBooking****");
  ResponseBean response = new ResponseBean();
  try
  {
	  String jsonStr = new Gson().toJson(requestBean);
   WebResource webResp = ServiceManager.getWebResource("AirTravel/getTicketDetails");
   ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
   int responseCode = clientResp.getStatus();
   if(responseCode != 200)
   {
    logger.debug("*****************response come***********************"+responseCode); 
    response.setResponse("fail");
    throw new RuntimeException("Failed : HTTP error code :"+responseCode);
   }
   response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
  }
  catch(Exception e)
  {
   logger.info("**********problem in flightBooking ***e.getMessage*"+e.getMessage());
   e.printStackTrace();
  }
  return response;
 }


public ResponseBean cancelTicket(RequestBean requestBean)
{
 logger.info("******start execution of flightBooking****");
 ResponseBean response = new ResponseBean();
 try
 {
	  String jsonStr = new Gson().toJson(requestBean);
  WebResource webResp = ServiceManager.getWebResource("AirTravel/CancelTicket");
  ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
  int responseCode = clientResp.getStatus();
  if(responseCode != 200)
  {
   logger.debug("*****************response come***********************"+responseCode); 
   response.setResponse("fail");
   throw new RuntimeException("Failed : HTTP error code :"+responseCode);
  }
  response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
 }
 catch(Exception e)
 {
  logger.info("**********problem in flightBooking ***e.getMessage*"+e.getMessage());
  e.printStackTrace();
 }
 return response;
}

public ResponseBean releasePNR(RequestBean requestBean)
{
 logger.info("******start execution of releasePNR****");
 ResponseBean response = new ResponseBean();
 try
 {
	  String jsonStr = new Gson().toJson(requestBean);
  WebResource webResp = ServiceManager.getWebResource("AirTravel/releasePNR");
  ClientResponse clientResp = webResp.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonStr);
  int responseCode = clientResp.getStatus();
  if(responseCode != 200)
  {
   logger.debug("*****************response come***********************"+responseCode); 
   response.setResponse("fail");
   throw new RuntimeException("Failed : HTTP error code :"+responseCode);
  }
  response = new Gson().fromJson(clientResp.getEntity(String.class), ResponseBean.class);
 }
 catch(Exception e)
 {
  logger.info("**********problem in releasePNR ***e.getMessage*"+e.getMessage());
  e.printStackTrace();
 }
 return response;
}




	public static void main(String s[])
	{

		try
		{
		 DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = f2.parse("2017-10-06 04:20:00");
		Date date2= f2.parse("2017-10-07 04:40:00");
		long time = (date2.getTime() - date1.getTime())/(1000*60);
		String timeS="";
		if(time >= 60)
		{
			timeS=time / 60 +"Hr "+time % 60 +"Min";
		}
		else
		{
			timeS = time +"Min";
		}
		System.out.println("-----"+timeS);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
