package com.bhartipay.airTravel.service;

import org.apache.log4j.Logger;

import com.bhartipay.airTravel.bean.RequestBean;
import com.bhartipay.airTravel.bean.Response;
import com.bhartipay.airTravel.bean.ResponseBean;
import com.bhartipay.airTravel.bean.TravelDtlBean;

public interface AirTravelService
{
	public ResponseBean getInitialSearch(RequestBean requestBean);
	public ResponseBean getSearchFlight(RequestBean requestBean);
	public ResponseBean fareQuote(RequestBean requestBean);
	public ResponseBean flightBooking(RequestBean requestBean);
	public ResponseBean getTicketDetails(RequestBean requestBean);
	public ResponseBean cancelTicket(RequestBean requestBean);
	public ResponseBean releasePNR(RequestBean requestBean);
}
