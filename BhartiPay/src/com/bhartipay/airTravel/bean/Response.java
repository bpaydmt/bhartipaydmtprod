package com.bhartipay.airTravel.bean;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {
    private String statusCode;
    private String status;
private String statusMsg;
private PayLoadBean resPayload;
private List<PassangerDetails> passengerList;
private CancellationCharges cancel;
@SerializedName("SegmentList")
@Expose
private List<TicketSegment> segmentList;

@SerializedName("PassengerList")
@Expose
private List<PassangerDetails> passengerListcancel;

@SerializedName("PNR")
@Expose
private String pnr;

@SerializedName("BookingId")
@Expose
private String bookingId;

@SerializedName("Error")
@Expose
private Error error;
@SerializedName("Response")
@Expose
private Response_ response;
@SerializedName("ResponseStatus")
@Expose
private Double responseStatus;
@SerializedName("TraceId")
@Expose
private String traceId;


public Error getError() {
	return error;
}
public void setError(Error error) {
	this.error = error;
}
public Response_ getResponse() {
	return response;
}
public void setResponse(Response_ response) {
	this.response = response;
}
public Double getResponseStatus() {
	return responseStatus;
}
public void setResponseStatus(Double responseStatus) {
	this.responseStatus = responseStatus;
}
public String getTraceId() {
	return traceId;
}
public void setTraceId(String traceId) {
	this.traceId = traceId;
}
public List<TicketSegment> getSegmentList() {
	return segmentList;
}
public void setSegmentList(List<TicketSegment> segmentList) {
	this.segmentList = segmentList;
}
public List<PassangerDetails> getPassengerListcancel() {
	return passengerListcancel;
}
public void setPassengerListcancel(List<PassangerDetails> passengerListcancel) {
	this.passengerListcancel = passengerListcancel;
}
public String getPnr() {
	return pnr;
}
public void setPnr(String pnr) {
	this.pnr = pnr;
}
public String getBookingId() {
	return bookingId;
}
public void setBookingId(String bookingId) {
	this.bookingId = bookingId;
}
public List<PassangerDetails> getPassengerList() {
	return passengerList;
}
public void setPassengerList(List<PassangerDetails> passengerList) {
	this.passengerList = passengerList;
}

public CancellationCharges getCancel() {
	return cancel;
}
public void setCancel(CancellationCharges cancel) {
	this.cancel = cancel;
}
public String getStatusCode() {
	return statusCode;
}
public void setStatusCode(String statusCode) {
	this.statusCode = statusCode;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getStatusMsg() {
	return statusMsg;
}
public void setStatusMsg(String statusMsg) {
	this.statusMsg = statusMsg;
}
public PayLoadBean getResPayload() {
	return resPayload;
}
public void setResPayload(PayLoadBean resPayload) {
	this.resPayload = resPayload;
}

}
