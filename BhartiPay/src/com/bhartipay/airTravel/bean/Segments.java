package com.bhartipay.airTravel.bean;

import com.google.gson.annotations.SerializedName;

public class Segments 
{
	@SerializedName("Origin")
	private String origin;
	
	@SerializedName("Destination")
	private String destination;
	
	@SerializedName("FlightCabinClass")
	private String flightCabinClass;
	
	@SerializedName("PreferredDepartureTime")
	private String preferredDepartureTime;
	
	@SerializedName("PreferredArrivalTime")
	private String preferredArrivalTime;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFlightCabinClass() {
		return flightCabinClass;
	}

	public void setFlightCabinClass(String flightCabinClass) {
		this.flightCabinClass = flightCabinClass;
	}

	public String getPreferredDepartureTime() {
		return preferredDepartureTime;
	}

	public void setPreferredDepartureTime(String preferredDepartureTime) {
		this.preferredDepartureTime = preferredDepartureTime;
	}

	public String getPreferredArrivalTime() {
		return preferredArrivalTime;
	}

	public void setPreferredArrivalTime(String preferredArrivalTime) {
		this.preferredArrivalTime = preferredArrivalTime;
	}
}
