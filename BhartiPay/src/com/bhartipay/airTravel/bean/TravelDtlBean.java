package com.bhartipay.airTravel.bean;

public class TravelDtlBean 
{
	private String aggregatorId;
	
	private String travelId;
	
	private String isWalletUse;
	
	private String walletId;
	

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getIsWalletUse() {
		return isWalletUse;
	}

	public void setIsWalletUse(String isWalletUse) {
		this.isWalletUse = isWalletUse;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}
	
	
}
