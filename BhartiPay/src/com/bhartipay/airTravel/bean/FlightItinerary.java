package com.bhartipay.airTravel.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightItinerary {

    @SerializedName("BookingId")
    @Expose
    private String bookingId;
    @SerializedName("PNR")
    @Expose
    private String pNR;
    @SerializedName("IsManual")
    @Expose
    private Boolean isManual;
    @SerializedName("IsDomestic")
    @Expose
    private Boolean isDomestic;
    @SerializedName("Source")
    @Expose
    private Double source;
    @SerializedName("Origin")
    @Expose
    private String origin;
    @SerializedName("Destination")
    @Expose
    private String destination;
    @SerializedName("AirlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("ValidatingAirlineCode")
    @Expose
    private String validatingAirlineCode;
    @SerializedName("AirlineRemark")
    @Expose
    private String airlineRemark;
    @SerializedName("IsLCC")
    @Expose
    private Boolean isLCC;
    @SerializedName("NonRefundable")
    @Expose
    private Boolean nonRefundable;
    @SerializedName("FareType")
    @Expose
    private String fareType;
    @SerializedName("LastTicketDate")
    @Expose
    private String lastTicketDate;
    @SerializedName("Fare")
    @Expose
    private Fare fare;
    @SerializedName("Passenger")
    @Expose
    private List<Passenger> passenger = null;
    @SerializedName("Segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("FareRules")
    @Expose
    private List<FareRule> fareRules = null;
    @SerializedName("Status")
    @Expose
    private Double status;
    @SerializedName("InvoiceNo")
    @Expose
    private String invoiceNo;
    @SerializedName("InvoiceCreatedOn")
    @Expose
    private String invoiceCreatedOn;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getPNR() {
        return pNR;
    }

    public void setPNR(String pNR) {
        this.pNR = pNR;
    }

    public Boolean getIsManual() {
        return isManual;
    }

    public void setIsManual(Boolean isManual) {
        this.isManual = isManual;
    }

    public Boolean getIsDomestic() {
        return isDomestic;
    }

    public void setIsDomestic(Boolean isDomestic) {
        this.isDomestic = isDomestic;
    }

    public Double getSource() {
        return source;
    }

    public void setSource(Double source) {
        this.source = source;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getValidatingAirlineCode() {
        return validatingAirlineCode;
    }

    public void setValidatingAirlineCode(String validatingAirlineCode) {
        this.validatingAirlineCode = validatingAirlineCode;
    }

    public String getAirlineRemark() {
        return airlineRemark;
    }

    public void setAirlineRemark(String airlineRemark) {
        this.airlineRemark = airlineRemark;
    }

    public Boolean getIsLCC() {
        return isLCC;
    }

    public void setIsLCC(Boolean isLCC) {
        this.isLCC = isLCC;
    }

    public Boolean getNonRefundable() {
        return nonRefundable;
    }

    public void setNonRefundable(Boolean nonRefundable) {
        this.nonRefundable = nonRefundable;
    }

    public String getFareType() {
        return fareType;
    }

    public void setFareType(String fareType) {
        this.fareType = fareType;
    }

    public String getLastTicketDate() {
        return lastTicketDate;
    }

    public void setLastTicketDate(String lastTicketDate) {
        this.lastTicketDate = lastTicketDate;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public List<Passenger> getPassenger() {
        return passenger;
    }

    public void setPassenger(List<Passenger> passenger) {
        this.passenger = passenger;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public List<FareRule> getFareRules() {
        return fareRules;
    }

    public void setFareRules(List<FareRule> fareRules) {
        this.fareRules = fareRules;
    }

    public Double getStatus() {
        return status;
    }

    public void setStatus(Double status) {
        this.status = status;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceCreatedOn() {
        return invoiceCreatedOn;
    }

    public void setInvoiceCreatedOn(String invoiceCreatedOn) {
        this.invoiceCreatedOn = invoiceCreatedOn;
    }

}
