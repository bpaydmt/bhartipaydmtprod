package com.bhartipay.airTravel.bean;

public class CancellationCharges {
	private double totalFare;
	
	private double airlinePenalty;
	
	private double tbuServiceFee;
	
	private double convenienceFee;
	
	private String isRefundable;
	
	private double refundableAmount;
	
	

	public double getRefundableAmount() {
		return refundableAmount;
	}

	public void setRefundableAmount(double refundableAmount) {
		this.refundableAmount = refundableAmount;
	}

	public String getIsRefundable() {
		return isRefundable;
	}

	public void setIsRefundable(String isRefundable) {
		this.isRefundable = isRefundable;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public double getAirlinePenalty() {
		return airlinePenalty;
	}

	public void setAirlinePenalty(double airlinePenalty) {
		this.airlinePenalty = airlinePenalty;
	}

	public double getTbuServiceFee() {
		return tbuServiceFee;
	}

	public void setTbuServiceFee(double tbuServiceFee) {
		this.tbuServiceFee = tbuServiceFee;
	}

	public double getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(double convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

}
