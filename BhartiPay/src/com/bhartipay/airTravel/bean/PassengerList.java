package com.bhartipay.airTravel.bean;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerList {

	private Integer paxId;
 
    private List<String> firstName;
 
    private List<String> lastName;

    private List<String> dateOfBirth;
 
    private List<Integer> gender;
    
    private int adultCount;
    
    private int childCount;
    
    private int infantCount;
    
    private String fareBreakDown;
    
    private String fare;
    
    private String fareBreakDownReturn;
    
    private String fareReturn;
    
    private String travelId;
    
    private String traceId;
    
    private String  resultIndex;
    
    

	public String getFareBreakDownReturn() {
		return fareBreakDownReturn;
	}

	public void setFareBreakDownReturn(String fareBreakDownReturn) {
		this.fareBreakDownReturn = fareBreakDownReturn;
	}

	public String getFareReturn() {
		return fareReturn;
	}

	public void setFareReturn(String fareReturn) {
		this.fareReturn = fareReturn;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getResultIndex() {
		return resultIndex;
	}

	public void setResultIndex(String resultIndex) {
		this.resultIndex = resultIndex;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public String getFareBreakDown() {
		return fareBreakDown;
	}

	public void setFareBreakDown(String fareBreakDown) {
		this.fareBreakDown = fareBreakDown;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public Integer getPaxId() {
		return paxId;
	}

	public void setPaxId(Integer paxId) {
		this.paxId = paxId;
	}

	public List<String> getFirstName() {
		return firstName;
	}

	public void setFirstName(List<String> firstName) {
		this.firstName = firstName;
	}

	public List<String> getLastName() {
		return lastName;
	}

	public void setLastName(List<String> lastName) {
		this.lastName = lastName;
	}

	public List<String> getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(List<String> dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public List<Integer> getGender() {
		return gender;
	}

	public void setGender(List<Integer> gender) {
		this.gender = gender;
	}

    
}
