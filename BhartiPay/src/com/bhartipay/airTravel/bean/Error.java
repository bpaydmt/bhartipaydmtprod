package com.bhartipay.airTravel.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("ErrorCode")
    @Expose
    private Double errorCode;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;

    public Double getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Double errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
