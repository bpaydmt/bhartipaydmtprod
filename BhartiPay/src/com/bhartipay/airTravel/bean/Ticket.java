package com.bhartipay.airTravel.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ticket {

    @SerializedName("TicketId")
    @Expose
    private Double ticketId;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("IssueDate")
    @Expose
    private String issueDate;
    @SerializedName("ValidatingAirline")
    @Expose
    private String validatingAirline;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("ServiceFeeDisplayType")
    @Expose
    private String serviceFeeDisplayType;
    @SerializedName("Status")
    @Expose
    private String status;

    public Double getTicketId() {
        return ticketId;
    }

    public void setTicketId(Double ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getValidatingAirline() {
        return validatingAirline;
    }

    public void setValidatingAirline(String validatingAirline) {
        this.validatingAirline = validatingAirline;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getServiceFeeDisplayType() {
        return serviceFeeDisplayType;
    }

    public void setServiceFeeDisplayType(String serviceFeeDisplayType) {
        this.serviceFeeDisplayType = serviceFeeDisplayType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
