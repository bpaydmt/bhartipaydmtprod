package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

public class PassangerDetails 
{
	private long passangerId;
	
	private String airFareId;
	
	private String tripId;
	
	private int tripIndicator;
	
	private String passangername;
	
	private String airlineCode;
	
	private String status;
	
	private String lifeInsurance;
	
	private String checkinBag;
	
	private String checkinCabin;
	
	private String mealType;

	private String sourceDestinationcode;
	
	private String ticketNumber;
	
	private String ticketId;
	
	private String pnr;

	private String isPnrCancelled;
	
	private int paxType;
	
	private double publishedFare;
	
	private  double cancellationCharge;

	private  double cancellationFees;
	
	private  double refundedAmount;
	
	private  String cancelledMode;
	
	
	
	public String getCancelledMode() {
		return cancelledMode;
	}

	public void setCancelledMode(String cancelledMode) {
		this.cancelledMode = cancelledMode;
	}

	public double getCancellationCharge() {
		return cancellationCharge;
	}

	public void setCancellationCharge(double cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	public double getCancellationFees() {
		return cancellationFees;
	}

	public void setCancellationFees(double cancellationFees) {
		this.cancellationFees = cancellationFees;
	}

	public double getRefundedAmount() {
		return refundedAmount;
	}

	public void setRefundedAmount(double refundedAmount) {
		this.refundedAmount = refundedAmount;
	}

	public double getPublishedFare() {
		return publishedFare;
	}

	public void setPublishedFare(double publishedFare) {
		this.publishedFare = publishedFare;
	}

	public int getPaxType() {
		return paxType;
	}

	public void setPaxType(int paxType) {
		this.paxType = paxType;
	}

	public String getIsPnrCancelled() {
		return isPnrCancelled;
	}

	public void setIsPnrCancelled(String isPnrCancelled) {
		this.isPnrCancelled = isPnrCancelled;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getLifeInsurance() {
		return lifeInsurance;
	}

	public void setLifeInsurance(String lifeInsurance) {
		this.lifeInsurance = lifeInsurance;
	}

	public String getCheckinBag() {
		return checkinBag;
	}

	public void setCheckinBag(String checkinBag) {
		this.checkinBag = checkinBag;
	}

	public String getCheckinCabin() {
		return checkinCabin;
	}

	public void setCheckinCabin(String checkinCabin) {
		this.checkinCabin = checkinCabin;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public long getPassangerId() {
		return passangerId;
	}

	public void setPassangerId(long passangerId) {
		this.passangerId = passangerId;
	}

	public int getTripIndicator() {
		return tripIndicator;
	}

	public void setTripIndicator(int tripIndicator) {
		this.tripIndicator = tripIndicator;
	}

	public String getPassangername() {
		return passangername;
	}

	public void setPassangername(String passangername) {
		this.passangername = passangername;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSourceDestinationcode() {
		return sourceDestinationcode;
	}

	public void setSourceDestinationcode(String sourceDestinationcode) {
		this.sourceDestinationcode = sourceDestinationcode;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	

}
