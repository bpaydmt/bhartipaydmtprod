package com.bhartipay.airTravel.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response_ {

    @SerializedName("PNR")
    @Expose
    private String pNR;
    @SerializedName("BookingId")
    @Expose
    private Double bookingId;
    @SerializedName("SSRDenied")
    @Expose
    private Boolean sSRDenied;
    @SerializedName("Status")
    @Expose
    private Double status;
    @SerializedName("IsPriceChanged")
    @Expose
    private Boolean isPriceChanged;
    @SerializedName("IsTimeChanged")
    @Expose
    private Boolean isTimeChanged;
    @SerializedName("FlightItinerary")
    @Expose
    private FlightItinerary flightItinerary;
    @SerializedName("TicketStatus")
    @Expose
    private Double ticketStatus;

    public String getPNR() {
        return pNR;
    }

    public void setPNR(String pNR) {
        this.pNR = pNR;
    }

    public Double getBookingId() {
        return bookingId;
    }

    public void setBookingId(Double bookingId) {
        this.bookingId = bookingId;
    }

    public Boolean getSSRDenied() {
        return sSRDenied;
    }

    public void setSSRDenied(Boolean sSRDenied) {
        this.sSRDenied = sSRDenied;
    }

    public Double getStatus() {
        return status;
    }

    public void setStatus(Double status) {
        this.status = status;
    }

    public Boolean getIsPriceChanged() {
        return isPriceChanged;
    }

    public void setIsPriceChanged(Boolean isPriceChanged) {
        this.isPriceChanged = isPriceChanged;
    }

    public Boolean getIsTimeChanged() {
        return isTimeChanged;
    }

    public void setIsTimeChanged(Boolean isTimeChanged) {
        this.isTimeChanged = isTimeChanged;
    }

    public FlightItinerary getFlightItinerary() {
        return flightItinerary;
    }

    public void setFlightItinerary(FlightItinerary flightItinerary) {
        this.flightItinerary = flightItinerary;
    }

    public Double getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(Double ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

}
