package com.bhartipay.airTravel.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareBreakdown {
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("PassengerType")
    @Expose
    private String passengerType;
    @SerializedName("PassengerCount")
    @Expose
    private Integer passengerCount;
    @SerializedName("BaseFare")
    @Expose
    private String baseFare;
    @SerializedName("Tax")
    @Expose
    private String tax;
    @SerializedName("YQTax")
    @Expose
    private String yQTax;
    @SerializedName("AdditionalTxnFeeOfrd")
    @Expose
    private String additionalTxnFeeOfrd;
    @SerializedName("AdditionalTxnFeePub")
    @Expose
    private String additionalTxnFeePub;
    @SerializedName("PGCharge")
    @Expose
    private String pGCharge;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getYQTax() {
        return yQTax;
    }

    public void setYQTax(String yQTax) {
        this.yQTax = yQTax;
    }

    public String getAdditionalTxnFeeOfrd() {
        return additionalTxnFeeOfrd;
    }

    public void setAdditionalTxnFeeOfrd(String additionalTxnFeeOfrd) {
        this.additionalTxnFeeOfrd = additionalTxnFeeOfrd;
    }

    public String getAdditionalTxnFeePub() {
        return additionalTxnFeePub;
    }

    public void setAdditionalTxnFeePub(String additionalTxnFeePub) {
        this.additionalTxnFeePub = additionalTxnFeePub;
    }

    public String getPGCharge() {
        return pGCharge;
    }

    public void setPGCharge(String pGCharge) {
        this.pGCharge = pGCharge;
    }


}
