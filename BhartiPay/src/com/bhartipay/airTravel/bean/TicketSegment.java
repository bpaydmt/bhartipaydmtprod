package com.bhartipay.airTravel.bean;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketSegment {
	
	private long segmentId;
	
	private String airFareId;
	
	private String tripId;
	
	private String source;
	
	private String destination;
	
	private String airlineName;
	
	private String airlineCode;
	
	private String deptdate;
	
	private String arrdate;
	
	private String sourceAirportName;
	
	private String destAirportName;

	private String regulations;
	
	private String airlineNumber;
	
	private String sourceAirportCode;
	
	private String sourceCityCode;
	
	private String destAirportCode;
	
	private String destCityCode;

	@SerializedName("Destination")
	@Expose
	private  Destination dest;
	
	private  Origin origin;
	
	public String getSourceAirportCode() {
		return sourceAirportCode;
	}

	public void setSourceAirportCode(String sourceAirportCode) {
		this.sourceAirportCode = sourceAirportCode;
	}

	public String getSourceCityCode() {
		return sourceCityCode;
	}

	public void setSourceCityCode(String sourceCityCode) {
		this.sourceCityCode = sourceCityCode;
	}

	public String getDestAirportCode() {
		return destAirportCode;
	}

	public void setDestAirportCode(String destAirportCode) {
		this.destAirportCode = destAirportCode;
	}

	public String getDestCityCode() {
		return destCityCode;
	}

	public void setDestCityCode(String destCityCode) {
		this.destCityCode = destCityCode;
	}

	public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin;
	}

	public Destination getDest() {
		return dest;
	}

	public void setDest(Destination dest) {
		this.dest = dest;
	}

	public String getAirlineNumber() {
		return airlineNumber;
	}

	public void setAirlineNumber(String airlineNumber) {
		this.airlineNumber = airlineNumber;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getAirFareId() {
		return airFareId;
	}

	public void setAirFareId(String airFareId) {
		this.airFareId = airFareId;
	}

	public String getRegulations() {
		return regulations;
	}

	public void setRegulations(String regulations) {
		this.regulations = regulations;
	}

	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getDeptdate() {
		return deptdate;
	}

	public void setDeptdate(String deptdate) {
		this.deptdate = deptdate;
	}

	public String getArrdate() {
		return arrdate;
	}

	public void setArrdate(String arrdate) {
		this.arrdate = arrdate;
	}

	public String getSourceAirportName() {
		return sourceAirportName;
	}

	public void setSourceAirportName(String sourceAirportName) {
		this.sourceAirportName = sourceAirportName;
	}

	public String getDestAirportName() {
		return destAirportName;
	}

	public void setDestAirportName(String destAirportName) {
		this.destAirportName = destAirportName;
	}
}
