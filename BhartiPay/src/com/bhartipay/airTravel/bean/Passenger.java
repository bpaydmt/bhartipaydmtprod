package com.bhartipay.airTravel.bean;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger implements Cloneable {
    @SerializedName("PaxId")
    @Expose
    private Double paxId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("PaxType")
    @Expose
    private Integer paxType;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("PassportNo")
    @Expose
    private String passportNo;
    @SerializedName("AddressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("AddressLine2")
    @Expose
    private String addressLine2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("CountryName")
    @Expose
    private String countryName;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("IsLeadPax")
    @Expose
    private Boolean isLeadPax;
    @SerializedName("FFNumber")
    @Expose
    private String fFNumber;
    @SerializedName("Fare")
    @Expose
    private Fare fare;
    @SerializedName("Ticket")
    @Expose
    private Ticket ticket;
    @SerializedName("SegmentAdditionalInfo")
    @Expose
    private List<SegmentAdditionalInfo> segmentAdditionalInfo = null;
    @SerializedName("Nationality")
    @Expose
    private String nationality;
	/*private Integer PaxId;
   
    private String Title;
   
    private String FirstName;

    private String LastName;
 
    private Integer PaxType;
  
  
    
    private Integer Gender;
    
    private String PassportNo;*/
 
    private String DateOfBirth;
    
    private String PassportExpiry;
    
    @SerializedName("FFAirline")
    @Expose
    private String FFAirline;
   
    @SerializedName("FareList")
    @Expose
    private List<Fare> FareList;

    @SerializedName("FFAirlineCode")
    @Expose
    private Object FFAirlineCode;
    

	public Double getPaxId() {
		return paxId;
	}


	public void setPaxId(Double paxId) {
		this.paxId = paxId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Integer getPaxType() {
		return paxType;
	}


	public void setPaxType(Integer paxType) {
		this.paxType = paxType;
	}


	public Integer getGender() {
		return gender;
	}


	public void setGender(Integer gender) {
		this.gender = gender;
	}


	public String getPassportNo() {
		return passportNo;
	}


	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}


	public String getAddressLine1() {
		return addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getCountryName() {
		return countryName;
	}


	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	public String getContactNo() {
		return contactNo;
	}


	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Boolean getIsLeadPax() {
		return isLeadPax;
	}


	public void setIsLeadPax(Boolean isLeadPax) {
		this.isLeadPax = isLeadPax;
	}


	public Fare getFare() {
		return fare;
	}


	public void setFare(Fare fare) {
		this.fare = fare;
	}


	public Ticket getTicket() {
		return ticket;
	}


	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}


	public List<SegmentAdditionalInfo> getSegmentAdditionalInfo() {
		return segmentAdditionalInfo;
	}


	public void setSegmentAdditionalInfo(List<SegmentAdditionalInfo> segmentAdditionalInfo) {
		this.segmentAdditionalInfo = segmentAdditionalInfo;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public String getDateOfBirth() {
		return DateOfBirth;
	}


	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}


	public String getPassportExpiry() {
		return PassportExpiry;
	}


	public void setPassportExpiry(String passportExpiry) {
		PassportExpiry = passportExpiry;
	}


	public String getFFAirline() {
		return FFAirline;
	}


	public void setFFAirline(String fFAirline) {
		FFAirline = fFAirline;
	}


	public List<Fare> getFareList() {
		return FareList;
	}


	public void setFareList(List<Fare> fareList) {
		FareList = fareList;
	}


	public Object getFFAirlineCode() {
		return FFAirlineCode;
	}


	public void setFFAirlineCode(Object fFAirlineCode) {
		FFAirlineCode = fFAirlineCode;
	}


	public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }


	public String getFFNumber() {
		return fFNumber;
	}


	public void setFFNumber(String fFNumber) {
		this.fFNumber = fFNumber;
	}	
	
}

