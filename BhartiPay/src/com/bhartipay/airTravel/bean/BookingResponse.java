
package com.bhartipay.airTravel.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingResponse {

    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("Response")
	@Expose
	private Response response;
 
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flightBookingList")
    @Expose
    private List<FlightBookingList> flightBookingList = null;
    @SerializedName("travelId")
    @Expose
    private String travelId;
    @SerializedName("statusMsg")
    @Expose
    private String statusMsg;
	private double flight1TotalFare;
	
	private double flight1ConvienceFee;
	
	private double flight2TotalFare;
	
	private double flight2ConvienceFee;
	
	 private double cancelRefundAmount;
	 
	 

    public double getCancelRefundAmount() {
		return cancelRefundAmount;
	}

	public void setCancelRefundAmount(double cancelRefundAmount) {
		this.cancelRefundAmount = cancelRefundAmount;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public double getFlight1TotalFare() {
		return flight1TotalFare;
	}

	public void setFlight1TotalFare(double flight1TotalFare) {
		this.flight1TotalFare = flight1TotalFare;
	}

	public double getFlight1ConvienceFee() {
		return flight1ConvienceFee;
	}

	public void setFlight1ConvienceFee(double flight1ConvienceFee) {
		this.flight1ConvienceFee = flight1ConvienceFee;
	}

	public double getFlight2TotalFare() {
		return flight2TotalFare;
	}

	public void setFlight2TotalFare(double flight2TotalFare) {
		this.flight2TotalFare = flight2TotalFare;
	}

	public double getFlight2ConvienceFee() {
		return flight2ConvienceFee;
	}

	public void setFlight2ConvienceFee(double flight2ConvienceFee) {
		this.flight2ConvienceFee = flight2ConvienceFee;
	}

	public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FlightBookingList> getFlightBookingList() {
        return flightBookingList;
    }

    public void setFlightBookingList(List<FlightBookingList> flightBookingList) {
        this.flightBookingList = flightBookingList;
    }

    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

}
