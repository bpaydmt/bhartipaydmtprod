package com.bhartipay.airTravel.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelRequest {

    @SerializedName("BookingId")
    @Expose
    private String bookingId;
    @SerializedName("travelId")
    @Expose
    private String travelId;
    @SerializedName("RequestType")
    @Expose
    private Integer requestType;
    @SerializedName("CancellationType")
    @Expose
    private Integer cancellationType;
    @SerializedName("Sectors")
    @Expose
    private List<Sector> sectors = null;
    @SerializedName("TicketId")
    @Expose
    private List<Integer> ticketId = null;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("EndUserIp")
    @Expose
    private String endUserIp;
    @SerializedName("TokenId")
    @Expose
    private String tokenId;
    @SerializedName("Source")
    @Expose
    private Integer source;
    @SerializedName("ChangeRequestId")
    @Expose
    private Integer changeRequestId;
    
    private String origin;
    
    private String destination;
    
    private List<String> ticketIdList = null;
    
    private String pnr;
    
  	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<String> getTicketIdList() {
  		return ticketIdList;
  	}

  	public void setTicketIdList(List<String> ticketIdList) {
  		this.ticketIdList = ticketIdList;
  	}

  
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getChangeRequestId() {
		return changeRequestId;
	}

	public void setChangeRequestId(Integer changeRequestId) {
		this.changeRequestId = changeRequestId;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}


	public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getRequestType() {
        return requestType;
    }

    public void setRequestType(Integer requestType) {
        this.requestType = requestType;
    }

    public Integer getCancellationType() {
        return cancellationType;
    }

    public void setCancellationType(Integer cancellationType) {
        this.cancellationType = cancellationType;
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }

    public List<Integer> getTicketId() {
        return ticketId;
    }

    public void setTicketId(List<Integer> ticketId) {
        this.ticketId = ticketId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getEndUserIp() {
        return endUserIp;
    }

    public void setEndUserIp(String endUserIp) {
        this.endUserIp = endUserIp;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

}

