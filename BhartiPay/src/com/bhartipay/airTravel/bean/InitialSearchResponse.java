package com.bhartipay.airTravel.bean;

import java.util.List;

import com.bhartipay.biller.vo.PayLoadBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitialSearchResponse {
	
	private String statusCode;
	private String status;
	private String statusMsg;
	private PayLoadBean resPayload=new PayLoadBean();

	@SerializedName("Response")
	@Expose
	private Response response;
 
	@SerializedName("amount")
    @Expose
    private double amount;
 
	@SerializedName("travelId")
    @Expose
    private String travelId;
	
	@SerializedName("flightBooking")
    @Expose
    private FlightBookingResp flightBooking;
	
	@SerializedName("bookingDetails")
    @Expose
    private FlightBookingResp bookingDetails;

	@SerializedName("getBookingDetails")
	@Expose
	private FlightBookingResp getBookingDetails;
	
	@SerializedName("flightBookingList")
    @Expose
    private List<FlightBookingResp> flightBookingList;
	
	@SerializedName("cancellationResp")
	@Expose
	private FlightBookingResp cancellationResp;
	
	@SerializedName("version")
    @Expose
    private String version;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public PayLoadBean getResPayload() {
		return resPayload;
	}

	public void setResPayload(PayLoadBean resPayload) {
		this.resPayload = resPayload;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTravelId() {
		return travelId;
	}

	public void setTravelId(String travelId) {
		this.travelId = travelId;
	}

	public FlightBookingResp getFlightBooking() {
		return flightBooking;
	}

	public void setFlightBooking(FlightBookingResp flightBooking) {
		this.flightBooking = flightBooking;
	}

	public FlightBookingResp getBookingDetails() {
		return bookingDetails;
	}

	public void setBookingDetails(FlightBookingResp bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

	public FlightBookingResp getGetBookingDetails() {
		return getBookingDetails;
	}

	public void setGetBookingDetails(FlightBookingResp getBookingDetails) {
		this.getBookingDetails = getBookingDetails;
	}

	public List<FlightBookingResp> getFlightBookingList() {
		return flightBookingList;
	}

	public void setFlightBookingList(List<FlightBookingResp> flightBookingList) {
		this.flightBookingList = flightBookingList;
	}

	public FlightBookingResp getCancellationResp() {
		return cancellationResp;
	}

	public void setCancellationResp(FlightBookingResp cancellationResp) {
		this.cancellationResp = cancellationResp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	
	
}
