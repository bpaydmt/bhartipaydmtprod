package com.bhartipay.airTravel.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SegmentAdditionalInfo {

    @SerializedName("FareBasis")
    @Expose
    private String fareBasis;
    @SerializedName("NVA")
    @Expose
    private String nVA;
    @SerializedName("NVB")
    @Expose
    private String nVB;
    @SerializedName("Baggage")
    @Expose
    private String baggage;
    @SerializedName("Meal")
    @Expose
    private String meal;

    public String getFareBasis() {
        return fareBasis;
    }

    public void setFareBasis(String fareBasis) {
        this.fareBasis = fareBasis;
    }

    public String getNVA() {
        return nVA;
    }

    public void setNVA(String nVA) {
        this.nVA = nVA;
    }

    public String getNVB() {
        return nVB;
    }

    public void setNVB(String nVB) {
        this.nVB = nVB;
    }

    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

}
