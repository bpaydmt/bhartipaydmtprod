package com.bhartipay.airTravel.bean;

import java.util.List;

import com.bhartipay.airTravel.bean.AirportDetails;

public class PayLoadBean {	
	private List<AirportDetails> airportList;
	private List<AirportDetails> popularAirportList;
	public List<AirportDetails> getAirportList() {
		return airportList;
	}
	public void setAirportList(List<AirportDetails> airportList) {
		this.airportList = airportList;
	}
	public List<AirportDetails> getPopularAirportList() {
		return popularAirportList;
	}
	public void setPopularAirportList(List<AirportDetails> popularAirportList) {
		this.popularAirportList = popularAirportList;
	}
	
}
