package com.bhartipay.bbps.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.bhartipay.bbps.service.BBPService;
import com.bhartipay.bbps.service.impl.BBPServiceImpl;
import com.bhartipay.bbps.vo.BillerDetailsResponseBean;
import com.bhartipay.lean.BbpsComplaint;
import com.bhartipay.lean.BbpsRequestData;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class BBPSTransaction extends BaseAction implements ServletResponseAware, ServletRequestAware{
	
	public static Logger logger=Logger.getLogger(BBPSTransaction.class);
	
	//protected HttpServletRequest request = null;
	BBPService bbpsServic;
	BillerDetailsResponseBean billerDetailsResponseBean = new BillerDetailsResponseBean();
	
	HttpServletResponse response=null;
	String billerCategory = null;
	String billerId = null;
	String billFetchRequest = null;
	String billRequest = null;
	String txtStatusRequest = null;
	String complaintStatus = null;
	String complaintRaiseReq = null;
	String aggreatorid = null;
	String depositEnqReq = null;
	
	
	
	public String getComplaintRaiseReq() {
		return complaintRaiseReq;
	}

	public void setComplaintRaiseReq(String complaintRaiseReq) {
		this.complaintRaiseReq = complaintRaiseReq;
	}
	
	public String getDepositEnqReq() {
		return depositEnqReq;
	}

	public void setDepositEnqReq(String depositEnqReq) {
		this.depositEnqReq = depositEnqReq;
	}

	public String getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	public String getTxtStatusRequest() {
		return txtStatusRequest;
	}

	public void setTxtStatusRequest(String txtStatusRequest) {
		this.txtStatusRequest = txtStatusRequest;
	}

	public String getBillRequest() {
		return billRequest;
	}

	public void setBillRequest(String billRequest) {
		this.billRequest = billRequest;
	}

	public BillerDetailsResponseBean getBillerDetailsResponseBean() {
		return billerDetailsResponseBean;
	}

	public void setBillerDetailsResponseBean(BillerDetailsResponseBean billerDetailsResponseBean) {
		this.billerDetailsResponseBean = billerDetailsResponseBean;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	/*
	 * public void setRequest(HttpServletRequest request) { this.request = request;
	 * }
	 */
	public String getBillerCategory() {
		return billerCategory;
	}

	public void setBillerCategory(String billerCategory) {
		this.billerCategory = billerCategory;
	}
	
	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}
	
	public String getBillFetchRequest() {
		return billFetchRequest;
	}

	public void setBillFetchRequest(String billFetchRequest) {
		this.billFetchRequest = billFetchRequest;
	}
	
	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		response=arg0;
	}
	
	public String getbillerInfo() {
		System.out.println("Wellcome to BBPS Service!!!");
		logger.debug("*********** Wellcome to BBPS Service!!! *****************");
		
		User user=(User)session.get("User");
		if(user.getAsAgentCode()==null && !user.getAsAgentCode().toUpperCase().contains("CC01"))
			return "error";
		
//		List<Map<String, String>> objList = (List)session.get("serviceMaster");
//		
//		for(Map<String, String> strMap : objList)
//			if(strMap.get("servicesname").equalsIgnoreCase("BBPS"))
//				if(strMap.get("status").equalsIgnoreCase("0"))
//					return "error";

		return "success";
	}
	
	public String loadBillerList() {
		logger.debug("*********** Wellcome to BBPS Load Billers List!!! *****************");
		try{
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.loadAllBillerCategory();
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getBillerCategories() {
		System.out.println("Wellcome to BBPS Get Biller Categories Service!!!");
		logger.debug("*********** Wellcome to BBPS Get Biller Categories Service!!! *****************");
		try{
		
			User user=(User)session.get("User");
			if(user.getAsAgentCode()!=null && user.getAsAgentCode().toUpperCase().contains("CC01") ) {	
				bbpsServic = new BBPServiceImpl();
				billerDetailsResponseBean = bbpsServic.getAllBillerCategory();
				
				logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
				logger.debug(billerDetailsResponseBean.getResponse());
				
				response.getWriter().flush();
				response.getWriter().write(billerDetailsResponseBean.getResponse());
				response.getWriter().close();
				
			}
			else {
				JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Please contact our sales executive/Distributor for BBPS."); 
			      
			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
				  response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(new Gson().toJson(jObject));
			      response.getWriter().close();
			      
			}
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 
			      
			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
				  response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
			      
		      }catch(Exception eIn){
		      }
		}
		
		return "success";
	}
	
	
	@SuppressWarnings("unchecked")
	public String fetchBillerList() {

		logger.debug("*********** Wellcome to fetchBillerList()!!! *****************");
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.getBillerByCategory(billerCategory);
			
			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 
			      

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
			      }catch(Exception eIn){
			      }
		}
		
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getCustomParam() {

		logger.debug("*********** Wellcome to getCustomParam()!!! *****************");
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.getCustomParam(billerId);
			
			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.setContentType("application/json");
	              response.getWriter().println(jObject);
	              response.getWriter().close();
			      }catch(Exception eIn){
			      }
		}
		
		
		return "success";
	}
	
@SuppressWarnings("unchecked")
public String fetchBillDetails() {

		logger.debug("*********** Wellcome to fetchBillDetails()!!! *****************");
	
		try{
			
			User user=(User)session.get("User");
			
			String userAgent=request.getHeader("User-Agent");
			   
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(billFetchRequest);
			
			json.put("agentId", user.getAsAgentCode());
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.fetchBillDetails(json.toString(), userAgent, ipAddress);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
			      }catch(Exception eIn){
			      }
		}
		
		
		return "success";
	}

	
	@SuppressWarnings("unchecked")
	public String billPaymentRequest() {

		logger.debug("*********** Wellcome to billPaymentRequest()!!! *****************");
		
		User user=(User)session.get("User");
		
		String userAgent=request.getHeader("User-Agent");
			   
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		try{
			
			bbpsServic = new BBPServiceImpl();
			
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(billRequest);
			
			json.put("agentId", user.getAsAgentCode());
			json.put("aggreatorId", user.getAggreatorid());
			json.put("welletId", user.getWalletid());
			json.put("agentidDmt", user.getId());
			
			
			billerDetailsResponseBean = bbpsServic.billPaymentRequest(json.toString(), userAgent, ipAddress);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			JSONParser parserResponse = new JSONParser();
			JSONObject jsonResponse = (JSONObject) parserResponse.parse(billerDetailsResponseBean.getResponse());
			
			jsonResponse.put("agentName", user.getName());
			jsonResponse.put("agentMobile", user.getMobileno());
			jsonResponse.put("shopName", user.getShopName());
			response.getWriter().flush();
			response.getWriter().write(jsonResponse.toString());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}
	
	public String getTxtStatusPage() {
		System.out.println("Wellcome to BBPS Txn Status Service!!!");
		logger.debug("*********** Wellcome to BBPS Txn Status Service!!! *****************");
		User user=(User)session.get("User");
		if(user.getAsAgentCode()!=null && user.getAsAgentCode().toUpperCase().contains("CC01") ) {
			return "success";
		}else
		{
			return "error";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String txnStatusRequest() {

		logger.debug("*********** Wellcome to txnStatusRequest()!!! *****************");
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.checkTxtStatusBytxtID(txtStatusRequest);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}
	
	public String checkComplaintStatus() {
		System.out.println("Wellcome to BBPS Complaint Service!!!");
		logger.debug("*********** Wellcome to BBPS Complaint Service!!! *****************");
		User user=(User)session.get("User");
		if(user.getAsAgentCode()!=null && user.getAsAgentCode().toUpperCase().contains("CC01") ) {
			return "success";
		}else
		{
			return "error";
		}
	}
	
	
	public String getComplaintDetails() {
		System.out.println("Wellcome to BBPS Complaint Service!!!");
		logger.debug("*********** Wellcome to BBPS Complaint Service!!! *****************");
		User user=(User)session.get("User");
		
		bbpsServic = new BBPServiceImpl();
		List<BbpsComplaint> list = bbpsServic.getComplaintDetails(user.getId());
		request.setAttribute("listResult",list);
		
		return "success";
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String checkComplaintStatusRequest() {

		logger.debug("*********** Wellcome to checkComplaintStatusRequest()!!! *****************");
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.requestForComplaintStatus(complaintStatus);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}
	
	public String complaintRaisePage() {
		System.out.println("Wellcome to registration of new  Complaint Service!!!");
		logger.debug("*********** Wellcome to registration of new  Complaint Service!!! *****************");
		User user=(User)session.get("User");
		if(user.getAsAgentCode()!=null && user.getAsAgentCode().toUpperCase().contains("CC01") ) {
			return "success";
		}else
		{
			return "error";
		}
	}
	
	public String getUserId()
	{
		User user=(User)session.get("User");
		String id = user.getId();
		//String str = bbpsServic.getUserId(id);
		BbpsRequestData data=new BbpsRequestData();
		data.setRequest(complaintRaiseReq);
		data.setUser(id);
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			BillerDetailsResponseBean request = bbpsServic.getUserId(data);


			response.getWriter().flush();
			response.getWriter().write(request.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String generateComplaintRegistrationRequest() {

		logger.debug("*********** Wellcome to generateComplaintRegistrationRequest()!!! *****************");	
		User user=(User)session.get("User");
		String id = user.getId();
		try{
		
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.requestForComplaintRegistration(complaintRaiseReq);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String enquireDeposit() {

		logger.debug("*********** Wellcome to generateComplaintRegistrationRequest()!!! *****************");
		
		try{
			
			bbpsServic = new BBPServiceImpl();
			billerDetailsResponseBean = bbpsServic.fetchDepositDetails(depositEnqReq);

			logger.debug("*********** billerDetailsResponseBean.getResponse() *****************");
			logger.debug(billerDetailsResponseBean.getResponse());
			
			response.getWriter().flush();
			response.getWriter().write(billerDetailsResponseBean.getResponse());
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			try{
			      JSONObject jObject=new JSONObject();
			      
			      jObject.put("statusCode","1");
			      jObject.put("status", "ERROR"); 
			      jObject.put("statusMsg", "Request not completed ,please try again."); 

			      logger.debug("*********** jObject *****************");
				  logger.debug(jObject);
				  
			      response.getWriter().flush();
			      response.setContentType("application/json");
			      response.getWriter().println(jObject);
			      response.getWriter().close();
		      }catch(Exception eIn){
		      }
		}
		return "success";
	}

}
