package com.bhartipay.bbps.service;

import java.util.List;
import java.util.Set;

import com.bhartipay.bbps.vo.BillerDetailsResponseBean;
import com.bhartipay.lean.BbpsComplaint;
import com.bhartipay.lean.BbpsRequestData;

//import com.bhartipay.bbps.bean.Biller;

public interface BBPService {
	
	public BillerDetailsResponseBean loadAllBillerCategory();
	
	public BillerDetailsResponseBean getAllBillerCategory();
	
	public BillerDetailsResponseBean getBillerByCategory(String billerID);

	public BillerDetailsResponseBean getCustomParam(String billerId);
	
	public BillerDetailsResponseBean fetchBillDetails(String billFetchRequest, String userAgent, String ipAddress);
	
	public BillerDetailsResponseBean billPaymentRequest(String billRequest, String userAgent, String ipAddress);
	
	public BillerDetailsResponseBean checkTxtStatusBytxtID(String bbpsTxtID);
	
	public BillerDetailsResponseBean requestForComplaintStatus(String str);

	public BillerDetailsResponseBean requestForComplaintRegistration(String complaintRequest);

	public BillerDetailsResponseBean fetchDepositDetails(String depositEnqReq);
	
	public BillerDetailsResponseBean getUserId(BbpsRequestData id);

	public List<BbpsComplaint> getComplaintDetails(String userId);
	
	
}
