package com.bhartipay.bbps.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import com.bhartipay.bbps.bean.Biller;
import com.bhartipay.bbps.service.BBPService;
import com.bhartipay.bbps.vo.BillerDetailsResponseBean;
import com.bhartipay.biller.vo.BillerResponseBean;
import com.bhartipay.lean.BbpsComplaint;
import com.bhartipay.lean.BbpsRequestData;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.report.bean.TravelTxn;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.media.jfxmedia.logging.Logger;


public class BBPServiceImpl implements BBPService {

	@Override
	public BillerDetailsResponseBean loadAllBillerCategory() {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/loadBillerCategory");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		return billerDetailsResponseBean;
	}

	@Override
	public BillerDetailsResponseBean getAllBillerCategory() {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/getBillerCategory");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BillerDetailsResponseBean getBillerByCategory(String category) {
		// TODO Auto-generated method stub
		
		JSONObject jObject=new JSONObject();
		jObject.put("billerCategory",category);
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/getBillerByCategory");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jObject.toString());
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BillerDetailsResponseBean getCustomParam(String billerId) {
		// TODO Auto-generated method stub
		
		JSONObject jObject=new JSONObject();
		jObject.put("billerId",billerId);
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/getCustomParam");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jObject.toString());
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;

	}
	
	@Override
	public BillerDetailsResponseBean fetchBillDetails(String billFetchRequest, String userAgent, String ipimei) {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/fetchBillDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,billFetchRequest);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;

	}

	@Override
	public BillerDetailsResponseBean billPaymentRequest(String billRequest, String userAgent, String ipimei) {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/billPaymentRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,billRequest);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;
	}

	@Override
	public BillerDetailsResponseBean checkTxtStatusBytxtID(String bbpsTxtID) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/bbpsTxtIDRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,bbpsTxtID);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		
		return billerDetailsResponseBean;
	}

	@Override
	public BillerDetailsResponseBean requestForComplaintStatus(String complaintID) {
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/complaintStatusRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,complaintID);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		return billerDetailsResponseBean;
	}
	
	
	@Override
	public BillerDetailsResponseBean requestForComplaintRegistration(String complaintRequest) {
		
		Gson gson = new Gson();
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/complaintRegistrationRequest");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,complaintRequest);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		return billerDetailsResponseBean;
	}

	@Override
	public BillerDetailsResponseBean fetchDepositDetails(String depositEnqReq) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public BillerDetailsResponseBean getUserId(BbpsRequestData data) {
		
		Gson gson = new Gson();
		String jsonText=gson.toJson(data);
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/getUserId");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		
		if (response.getStatus() != 200) {
			System.out.println("Failed : HTTP error code : " + response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		BillerDetailsResponseBean billerDetailsResponseBean = gson.fromJson(output, BillerDetailsResponseBean.class);
		
		return billerDetailsResponseBean;
	}

	
	@Override
	public List<BbpsComplaint> getComplaintDetails(String userId) {

		Gson gson = new Gson();
		//String jsonText=gson.toJson(userId);
		WebResource webResource = ServiceManager.getWebResource("BbpsManager/getComplaintDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,userId);
		
		if (response.getStatus() != 200){
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		
		String output = response.getEntity(String.class);		
		List<BbpsComplaint> list = gson.fromJson(output,OTOList30.class);
		System.out.println(list);
		
		return list;
	}
	
	
	
}

class OTOList30 extends ArrayList<BbpsComplaint>{
	
}