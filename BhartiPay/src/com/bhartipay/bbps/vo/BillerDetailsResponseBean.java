package com.bhartipay.bbps.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BillerDetailsResponseBean  implements Serializable{
	
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
