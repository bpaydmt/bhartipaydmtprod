package com.bhartipay.bbps.vo;

import java.sql.Date;


public class BbpsPayment{

	private int id;
	
	private String txnid;
	
	private String resptxnid;

	private String walletid;

	private String agentid;

	private String aggreatorid;

	private String txndate;

	private double txnamount;

	private String requestid;

	private String request;

	private Date requestdate;

	private String response;

	private Date responsedate;

	private String status;

	private String billertype;

	private String billername;

	private String billerid;

	private String quickpaytype;
	
	private String consumerNumber;

	private String remark;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getResptxnid() {
		return resptxnid;
	}

	public void setResptxnid(String resptxnid) {
		this.resptxnid = resptxnid;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}
	/*
	 * public Date getTxndate() { return txndate; }
	 * 
	 * public void setTxndate(Date txndate) { this.txndate = txndate; }
	 */
	
	
	
	
	public double getTxnamount() {
		return txnamount;
	}

	public String getTxndate() {
		return txndate;
	}

	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}

	public void setTxnamount(double txnamount) {
		this.txnamount = txnamount;
	}

	public String getRequestid() {
		return requestid;
	}

	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Date getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(Date requestdate) {
		this.requestdate = requestdate;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getResponsedate() {
		return responsedate;
	}

	public void setResponsedate(Date responsedate) {
		this.responsedate = responsedate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBillertype() {
		return billertype;
	}

	public void setBillertype(String billertype) {
		this.billertype = billertype;
	}

	public String getBillername() {
		return billername;
	}

	public void setBillername(String billername) {
		this.billername = billername;
	}

	public String getBillerid() {
		return billerid;
	}

	public void setBillerid(String billerid) {
		this.billerid = billerid;
	}

	public String getQuickpaytype() {
		return quickpaytype;
	}

	public void setQuickpaytype(String quickpaytype) {
		this.quickpaytype = quickpaytype;
	}

	public String getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(String consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
