package com.bhartipay.recharge.response;

public class BillPaymentResponse {
	private ExtBillPayResponse ExtBillPayResponse;

    public ExtBillPayResponse getExtBillPayResponse ()
    {
        return ExtBillPayResponse;
    }

    public void setExtBillPayResponse (ExtBillPayResponse ExtBillPayResponse)
    {
        this.ExtBillPayResponse = ExtBillPayResponse;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ExtBillPayResponse = "+ExtBillPayResponse+"]";
    }
}
