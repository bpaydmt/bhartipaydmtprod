package com.bhartipay.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD)
public class BillerInfoResponse {
	private Biller biller;

    private String responseCode;

    private ErrorInfo errorInfo;


    public ErrorInfo getErrorInfo ()
    {
        return errorInfo;
    }

    public void setErrorInfo (ErrorInfo errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    
    public Biller getBiller ()
    {
        return biller;
    }

    public void setBiller (Biller biller)
    {
        this.biller = biller;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [biller = "+biller+", responseCode = "+responseCode+"]";
    }
}
