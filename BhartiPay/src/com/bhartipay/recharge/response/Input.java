package com.bhartipay.recharge.response;

public class Input
{
    private String paramName;

    private String paramValue;

    public String getParamName ()
    {
        return paramName;
    }

    public void setParamName (String paramName)
    {
        this.paramName = paramName;
    }

    public String getParamValue ()
    {
        return paramValue;
    }

    public void setParamValue (String paramValue)
    {
        this.paramValue = paramValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [paramName = "+paramName+", paramValue = "+paramValue+"]";
    }
}
			
		