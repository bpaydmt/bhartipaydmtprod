package com.bhartipay.recharge.request;

public class BillRequest {
	 private BillPaymentRequest billPaymentRequest;

	    public BillPaymentRequest getBillPaymentRequest ()
	    {
	        return billPaymentRequest;
	    }

	    public void setBillPaymentRequest (BillPaymentRequest billPaymentRequest)
	    {
	        this.billPaymentRequest = billPaymentRequest;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [billPaymentRequest = "+billPaymentRequest+"]";
	    }
}
