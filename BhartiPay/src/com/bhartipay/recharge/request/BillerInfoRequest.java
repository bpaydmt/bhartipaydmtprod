package com.bhartipay.recharge.request;

public class BillerInfoRequest
{
    private String billerId;

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [billerId = "+billerId+"]";
    }
}
			
		