package com.bhartipay.recharge.request;

public class CustomerInfo
{
    private String customerAdhaar;

    private String customerEmail;

    private String customerMobile;

    private String customerPan;

    public String getCustomerAdhaar ()
    {
        return customerAdhaar;
    }

    public void setCustomerAdhaar (String customerAdhaar)
    {
        this.customerAdhaar = customerAdhaar;
    }

    public String getCustomerEmail ()
    {
        return customerEmail;
    }

    public void setCustomerEmail (String customerEmail)
    {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMobile ()
    {
        return customerMobile;
    }

    public void setCustomerMobile (String customerMobile)
    {
        this.customerMobile = customerMobile;
    }

    public String getCustomerPan ()
    {
        return customerPan;
    }

    public void setCustomerPan (String customerPan)
    {
        this.customerPan = customerPan;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customerAdhaar = "+customerAdhaar+", customerEmail = "+customerEmail+", customerMobile = "+customerMobile+", customerPan = "+customerPan+"]";
    }
}
			
		