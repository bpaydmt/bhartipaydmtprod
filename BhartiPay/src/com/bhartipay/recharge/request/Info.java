package com.bhartipay.recharge.request;
public class Info
{
    private String infoName;

    private String infoValue;

    public String getInfoName ()
    {
        return infoName;
    }

    public void setInfoName (String infoName)
    {
        this.infoName = infoName;
    }

    public String getInfoValue ()
    {
        return infoValue;
    }

    public void setInfoValue (String infoValue)
    {
        this.infoValue = infoValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [infoName = "+infoName+", infoValue = "+infoValue+"]";
    }
}
			
		