package com.bhartipay.recharge.request;

public class AgentDeviceInfo
{
    private String ip;

    private String initChannel;

    private String mac;

    public String getIp ()
    {
        return ip;
    }

    public void setIp (String ip)
    {
        this.ip = ip;
    }

    public String getInitChannel ()
    {
        return initChannel;
    }

    public void setInitChannel (String initChannel)
    {
        this.initChannel = initChannel;
    }

    public String getMac ()
    {
        return mac;
    }

    public void setMac (String mac)
    {
        this.mac = mac;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ip = "+ip+", initChannel = "+initChannel+", mac = "+mac+"]";
    }
}