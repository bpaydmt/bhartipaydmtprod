package com.bhartipay.recharge.request;

public class BillPaymentRequest
{
    private String billerId;

    private String agentId;

    private InputParams inputParams;

    private CustomerInfo customerInfo;

    private PaymentMethod paymentMethod;

    private AgentDeviceInfo agentDeviceInfo;

    private AmountInfo amountInfo;

    private PaymentInfo paymentInfo;

    private String billerAdhoc;

    public String getBillerId ()
    {
        return billerId;
    }

    public void setBillerId (String billerId)
    {
        this.billerId = billerId;
    }

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }

    public InputParams getInputParams ()
    {
        return inputParams;
    }

    public void setInputParams (InputParams inputParams)
    {
        this.inputParams = inputParams;
    }

    public CustomerInfo getCustomerInfo ()
    {
        return customerInfo;
    }

    public void setCustomerInfo (CustomerInfo customerInfo)
    {
        this.customerInfo = customerInfo;
    }

    public PaymentMethod getPaymentMethod ()
    {
        return paymentMethod;
    }

    public void setPaymentMethod (PaymentMethod paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public AgentDeviceInfo getAgentDeviceInfo ()
    {
        return agentDeviceInfo;
    }

    public void setAgentDeviceInfo (AgentDeviceInfo agentDeviceInfo)
    {
        this.agentDeviceInfo = agentDeviceInfo;
    }

    public AmountInfo getAmountInfo ()
    {
        return amountInfo;
    }

    public void setAmountInfo (AmountInfo amountInfo)
    {
        this.amountInfo = amountInfo;
    }

    public PaymentInfo getPaymentInfo ()
    {
        return paymentInfo;
    }

    public void setPaymentInfo (PaymentInfo paymentInfo)
    {
        this.paymentInfo = paymentInfo;
    }

    public String getBillerAdhoc ()
    {
        return billerAdhoc;
    }

    public void setBillerAdhoc (String billerAdhoc)
    {
        this.billerAdhoc = billerAdhoc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [billerId = "+billerId+", agentId = "+agentId+", inputParams = "+inputParams+", customerInfo = "+customerInfo+", paymentMethod = "+paymentMethod+", agentDeviceInfo = "+agentDeviceInfo+", amountInfo = "+amountInfo+", paymentInfo = "+paymentInfo+", billerAdhoc = "+billerAdhoc+"]";
    }
}