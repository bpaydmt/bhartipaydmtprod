package com.bhartipay.ledgerReports.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.util.ServerInfo;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.aeps.AepsResponseData;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.pg.PGDetails;
import com.bhartipay.wallet.framework.security.AES128Bit;
import com.bhartipay.wallet.framework.security.EncryptionByEnc256;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.Utils.AEPSUtil;
import com.bhartipay.wallet.kyc.Utils.ObjectFactory;
import com.bhartipay.wallet.mechant.service.MerchantService;
import com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter;
import com.bhartipay.wallet.merchant.service.impl.MerchantServiceImpl;
import com.bhartipay.wallet.payment.vo.CustomerDtls;
import com.bhartipay.wallet.payment.vo.PayGateHandler;
import com.bhartipay.wallet.payment.vo.ShippingDtls;
import com.bhartipay.wallet.payment.vo.TDSecurePayGateObject;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.recharge.service.impl.RechargeServiceImpl;
import com.bhartipay.wallet.report.bean.AgentClosingBalBean;
import com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.CommSummaryBean;
import com.bhartipay.wallet.report.bean.PartnerLedgerBean;
import com.bhartipay.wallet.report.bean.RefundTransactionBean;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.bean.SMSSendDetails;
import com.bhartipay.wallet.report.bean.SenderClosingBalBean;
import com.bhartipay.wallet.report.bean.SupportTicketBean;
import com.bhartipay.wallet.report.bean.TravelTxn;
import com.bhartipay.wallet.report.bean.TxnReportByAdmin;
import com.bhartipay.wallet.report.bean.WalletTxnDetailsRpt;
import com.bhartipay.wallet.report.service.ReportService;
import com.bhartipay.wallet.report.service.impl.ReportServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.AskMoneyBean;
import com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast;
import com.bhartipay.wallet.transaction.persistence.vo.DMTBean;
import com.bhartipay.wallet.transaction.persistence.vo.DMTInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.EscrowBean;
import com.bhartipay.wallet.transaction.persistence.vo.PartnerPrefundBean;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport;
import com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean;
import com.bhartipay.wallet.transaction.persistence.vo.SurchargeBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.persistence.vo.UserDetails;
import com.bhartipay.wallet.transaction.persistence.vo.UserWalletConfigBean;
import com.bhartipay.wallet.transaction.persistence.vo.WalletExist;
import com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast;
import com.bhartipay.wallet.transaction.servicebk.impl.TransactionServiceImpl;
import com.bhartipay.wallet.transactionbk.action.ManageTransaction;
import com.bhartipay.wallet.transactionbk.service.TransactionService;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteBean;
import com.bhartipay.wallet.user.persistence.vo.SenderFavouriteViewBean;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.service.bk.UserService;
import com.bhartipay.wallet.user.service.impl.bk.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;

import appnit.com.crypto.CheckSumHelper;

public class LedgerReports extends BaseAction implements ServletResponseAware{	
	
	HttpServletResponse response=null;
	public static Logger logger=Logger.getLogger(ManageTransaction.class);	
	TransactionService tService=new TransactionServiceImpl();
	UserService uService=new UserServiceImpl();
	TxnInputBean inputBean=new TxnInputBean();
	DMTInputBean dmtInputBean=new DMTInputBean();
	WalletToBankTxnMast wtob=new WalletToBankTxnMast();
	Map<String,String> customerList=new TreeMap<String, String>();
	UserWalletConfigBean config=new UserWalletConfigBean();
	MerchantService mService=new MerchantServiceImpl();
	ReportService rService=new ReportServiceImpl();
	Map<String, String> plans=new HashMap<String, String>(); 
	Map<String, String> txnType=new HashMap<String, String>();
	CommissionService comService=new CommissionServiceImpl();	
	CommPlanMaster commBean=new CommPlanMaster();
	SMSSendDetails smsBean=new SMSSendDetails();
	SupportTicketBean supBean=new SupportTicketBean();
	CommissionService commService=new CommissionServiceImpl();
	UserDetails userDetails=new UserDetails();
	Map<String,String> accountTypeList=new LinkedHashMap<String, String>();
	CashDepositMast depositMast=new CashDepositMast();
	Map<String,String> txnTypeList=new HashMap<String,String>();
	RefundMastBean refundBean=new RefundMastBean();
	AskMoneyBean amb=new AskMoneyBean();
	EscrowBean bean=new EscrowBean();
	UserSummary user=new UserSummary();
	SurchargeBean suBean=new SurchargeBean();
	public String statusMessage;
	Properties prop =null;
	Map<String,String> bankNameList=new HashMap<String,String>();
	Map<String, String> agentList=new LinkedHashMap<String, String>();
	private UserService service=new UserServiceImpl();
	
	PartnerPrefundBean partnerPrefundBean=new PartnerPrefundBean();
	
	public Map<String,String> txnSourceList=new LinkedHashMap<String, String>();
	public Map<String,String> partnerList=new LinkedHashMap<String, String>();
	public Map<String,String> tranType=new LinkedHashMap<String, String>();
	B2CMoneyTxnMast b2cMoneyTxnMast = new B2CMoneyTxnMast();
	 ObjectFactory oFactory = new ObjectFactory();
	 RechargeService reService=new RechargeServiceImpl();
		HashMap<String, String> prePaid = new HashMap<String, String>();
		HashMap<String, String> postPaid = new HashMap<String, String>();
		HashMap<String, String> dth = new HashMap<String, String>();
		HashMap<String, String> landLine = new HashMap<String, String>();
		HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
		HashMap<String, String> postPaidDataCard = new HashMap<String, String>();
		HashMap<String, String> circle = new HashMap<String, String>();
	 
	 
	 
	 
	 
	 
	 
	 
	 public HashMap<String, String> getPrePaid() {
			return prePaid;
		}

		public void setPrePaid(HashMap<String, String> prePaid) {
			this.prePaid = prePaid;
		}

		public HashMap<String, String> getPostPaid() {
			return postPaid;
		}

		public void setPostPaid(HashMap<String, String> postPaid) {
			this.postPaid = postPaid;
		}

		public HashMap<String, String> getDth() {
			return dth;
		}

		public void setDth(HashMap<String, String> dth) {
			this.dth = dth;
		}

		public HashMap<String, String> getLandLine() {
			return landLine;
		}

		public void setLandLine(HashMap<String, String> landLine) {
			this.landLine = landLine;
		}

		public HashMap<String, String> getPrePaidDataCard() {
			return prePaidDataCard;
		}

		public void setPrePaidDataCard(HashMap<String, String> prePaidDataCard) {
			this.prePaidDataCard = prePaidDataCard;
		}

		public HashMap<String, String> getPostPaidDataCard() {
			return postPaidDataCard;
		}

		public void setPostPaidDataCard(HashMap<String, String> postPaidDataCard) {
			this.postPaidDataCard = postPaidDataCard;
		}

		public HashMap<String, String> getCircle() {
			return circle;
		}

		public void setCircle(HashMap<String, String> circle) {
			this.circle = circle;
		}

	public B2CMoneyTxnMast getB2cMoneyTxnMast() {
	  return b2cMoneyTxnMast;
	 }

	 public void setB2cMoneyTxnMast(B2CMoneyTxnMast b2cMoneyTxnMast) {
	  this.b2cMoneyTxnMast = b2cMoneyTxnMast;
	 }
	
	
	
	
	
	
	
	
	public Map<String, String> getTranType() {
		tranType.put("DEBIT", "DEBIT");
		tranType.put("CREDIT", "CREDIT");
		return tranType;
	}

	public void setTranType(Map<String, String> tranType) {
		this.tranType = tranType;
	}


	
	
	public Map<String, String> getTxnSourceList() {
		return txnSourceList;
	}
	public void setTxnSourceList(Map<String, String> txnSourceList) {
		this.txnSourceList = txnSourceList;
	}
	public Map<String, String> getPartnerList() {
		return partnerList;
	}
	public void setPartnerList(Map<String, String> partnerList) {
		this.partnerList = partnerList;
	}
	
	
	
	public PartnerPrefundBean getPartnerPrefundBean() {
		return partnerPrefundBean;
	}
	public void setPartnerPrefundBean(PartnerPrefundBean partnerPrefundBean) {
		this.partnerPrefundBean = partnerPrefundBean;
	}
	public Map<String, String> getAgentList() {
		return agentList;
	}
	public void setAgentList(Map<String, String> agentList) {
		this.agentList = agentList;
	}
	
	
	
	
	public LedgerReports(){
		/* added by neeraj call error code using properties file. */
		prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("errors.properties");
		try {
			prop.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}}
		/*end by neeraj*/
	public SurchargeBean getSuBean() {
		return suBean;
	}

	public String getStatusMessage() {
		return statusMessage;
	}
	
	public void setBankNameList(Map<String, String> bankNameList) {
		this.bankNameList = bankNameList;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public void setSuBean(SurchargeBean suBean) {
		this.suBean = suBean;
	}

	public UserSummary getUser() {
		return user;
	}

	public void setUser(UserSummary user) {
		this.user = user;
	}

	public AskMoneyBean getAmb() {
		return amb;
	}

	public void setAmb(AskMoneyBean amb) {
		this.amb = amb;
	}

	public RefundMastBean getRefundBean() {
		return refundBean;
	}

	public void setRefundBean(RefundMastBean refundBean) {
		this.refundBean = refundBean;
	}

	public Map<String, String> getTxnTypeList() {
		
		txnTypeList.put("NEFT","NEFT");
		txnTypeList.put("RTGS","RTGS");
		txnTypeList.put("IMPS","IMPS");
		txnTypeList.put("CASH", "CASH");
		txnTypeList.put("CREDIT", "CREDIT");
		return txnTypeList;
	}

	public void setTxnTypeList(Map<String, String> txnTypeList) {
		this.txnTypeList = txnTypeList;
	}

	public CashDepositMast getDepositMast() {
		return depositMast;
	}

	public void setDepositMast(CashDepositMast depositMast) {
		this.depositMast = depositMast;
	}

	public EscrowBean getBean() {
		return bean;
	}

	public void setBean(EscrowBean bean) {
		this.bean = bean;
	}

	public Map<String, String> getAccountTypeList() {
		return accountTypeList;
	}

	public void setAccountTypeList(Map<String, String> accountTypeList) {
		accountTypeList.put("Savings","Saving");
		accountTypeList.put("Current","Current");
		this.accountTypeList = accountTypeList;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public SupportTicketBean getSupBean() {
		return supBean;
	}

	public void setSupBean(SupportTicketBean supBean) {
		this.supBean = supBean;
	}

	public SMSSendDetails getSmsBean() {
		return smsBean;
	}

	public void setSmsBean(SMSSendDetails smsBean) {
		this.smsBean = smsBean;
	}

	public CommPlanMaster getCommBean() {
		return commBean;
	}

	public void setCommBean(CommPlanMaster commBean) {
		this.commBean = commBean;
	}

	public Map<String, String> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, String> plans) {
		this.plans = plans;
	}

	public Map<String, String> getTxnType() {
		return txnType;
	}

	public void setTxnType(Map<String, String> txnType) {
		this.txnType = txnType;
	}

	public DMTInputBean getDmtInputBean() {
		return dmtInputBean;
	}

	public void setDmtInputBean(DMTInputBean dmtInputBean) {
		this.dmtInputBean = dmtInputBean;
	}

	public UserWalletConfigBean getConfig() {
		return config;
	}

	public void setConfig(UserWalletConfigBean config) {
		this.config = config;
	}

	public Map<String, String> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(Map<String, String> customerList) {
		this.customerList = customerList;
	}

	public WalletToBankTxnMast getWtob() {
		return wtob;
	}

	public void setWtob(WalletToBankTxnMast wtob) {
		this.wtob = wtob;
	}

	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}


	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		// TODO Auto-generated method stub
		
	}
	
	

	public String lReport(){
		logger.debug("*********************************calling LReport************************************");
		
		return "success";
	}

	
	public String lReport1(){
		logger.debug("*********************************calling LReport************************************");
		
		return "success";
	}
	
	public String txnStatus(){
		logger.debug("*********************************calling txn Status Report************************************");
		
		return "success";
	}

	
	
	
	
	
	
	
	
	
		
		

		
	 
}
