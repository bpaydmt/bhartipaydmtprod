package com.bhartipay.lean;

public class ServiceConfig 
{

	private String fImps;
	private String fNeft;
	private String asImps;
	private String asNeft;
	private String pImps;
	private String pNeft;
	private String pMode;
	private String finoAeps;
    private String yesAeps;
	private String iciciAeps;
	private String matm;
	
	public String getfImps() {
		return fImps;
	}
	public void setfImps(String fImps) {
		this.fImps = fImps;
	}
	public String getfNeft() {
		return fNeft;
	}
	public void setfNeft(String fNeft) {
		this.fNeft = fNeft;
	}
	public String getAsImps() {
		return asImps;
	}
	public void setAsImps(String asImps) {
		this.asImps = asImps;
	}
	public String getAsNeft() {
		return asNeft;
	}
	public void setAsNeft(String asNeft) {
		this.asNeft = asNeft;
	}
	public String getpImps() {
		return pImps;
	}
	public void setpImps(String pImps) {
		this.pImps = pImps;
	}
	public String getpNeft() {
		return pNeft;
	}
	public void setpNeft(String pNeft) {
		this.pNeft = pNeft;
	}
	public String getpMode() {
		return pMode;
	}
	public void setpMode(String pMode) {
		this.pMode = pMode;
	}
	public String getFinoAeps() {
		return finoAeps;
	}
	public void setFinoAeps(String finoAeps) {
		this.finoAeps = finoAeps;
	}
	public String getYesAeps() {
		return yesAeps;
	}
	public void setYesAeps(String yesAeps) {
		this.yesAeps = yesAeps;
	}
	public String getIciciAeps() {
		return iciciAeps;
	}
	public void setIciciAeps(String iciciAeps) {
		this.iciciAeps = iciciAeps;
	}
	public String getMatm() {
		return matm;
	}
	public void setMatm(String matm) {
		this.matm = matm;
	}
	
	
	
}
