package com.bhartipay.lean;

public class CmsEnquiryResponse {

	public String rrn;
	public String stanNo;
	public String txnId;
	public String aepstxnId;
	public String action;
	public String device;
	public String status;
	public String txnStatus;
	public String bankName;
	public String mobileNo;
	public String uId;
	public String dateOfTransaction;
	public String authCode;
	public String Agent_Id;
	public String Service;
	public double Amount;
	
	public String clientTransactionId;

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getStanNo() {
		return stanNo;
	}

	public void setStanNo(String stanNo) {
		this.stanNo = stanNo;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getAepstxnId() {
		return aepstxnId;
	}

	public void setAepstxnId(String aepstxnId) {
		this.aepstxnId = aepstxnId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(String dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAgent_Id() {
		return Agent_Id;
	}

	public void setAgent_Id(String agent_Id) {
		Agent_Id = agent_Id;
	}

	public String getService() {
		return Service;
	}

	public void setService(String service) {
		Service = service;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public String getClientTransactionId() {
		return clientTransactionId;
	}

	public void setClientTransactionId(String clientTransactionId) {
		this.clientTransactionId = clientTransactionId;
	}
	
	
	
	
	
	
}
