package com.bhartipay.lean;

public class PgReturnResponse {


 private String pgTxnDate;
 private String pgTxnResCode;
 private String custPhone;
 private String mopType;
 private String cardmask;
 private String currencyCode;
 private String pgTxnRrn;
 private String pgStatus;
 private String pgAmount;
 private String pgResMsg;
 private String paymentId;
 private String custEmail;
 private String pgGateway;
 private String pgTxnId;
 private String acqId;
 private String pgTxnType;
 private String dupYn;
 private String pgHash;
 private String pgPaymentType;
 private String pgReturnUrl;
 private String pgPayId;
 private String pgOrderId;
 private String custName;
 private String agentId; 
 
 public PgReturnResponse() {
	super();
}

public PgReturnResponse(String pgTxnDate, String pgTxnResCode, String custPhone, String mopType, String cardmask,
		String currencyCode, String pgTxnRrn, String pgStatus, String pgAmount, String pgResMsg, String paymentId,
		String custEmail, String pgGateway, String pgTxnId, String acqId, String pgTxnType, String dupYn, String pgHash,
		String pgPaymentType, String pgReturnUrl, String pgPayId, String pgOrderId, String custName, String agentId)
    {
	this.pgTxnDate = pgTxnDate;
	this.pgTxnResCode = pgTxnResCode;
	this.custPhone = custPhone;
	this.mopType = mopType;
	this.cardmask = cardmask;
	this.currencyCode = currencyCode;
	this.pgTxnRrn = pgTxnRrn;
	this.pgStatus = pgStatus;
	this.pgAmount = pgAmount;
	this.pgResMsg = pgResMsg;
	this.paymentId = paymentId;
	this.custEmail = custEmail;
	this.pgGateway = pgGateway;
	this.pgTxnId = pgTxnId;
	this.acqId = acqId;
	this.pgTxnType = pgTxnType;
	this.dupYn = dupYn;
	this.pgHash = pgHash;
	this.pgPaymentType = pgPaymentType;
	this.pgReturnUrl = pgReturnUrl;
	this.pgPayId = pgPayId;
	this.pgOrderId = pgOrderId;
	this.custName = custName;
	this.agentId = agentId; 
}

public String getPgTxnDate() {
	return pgTxnDate;
}

public void setPgTxnDate(String pgTxnDate) {
	this.pgTxnDate = pgTxnDate;
}

public String getPgTxnResCode() {
	return pgTxnResCode;
}

public void setPgTxnResCode(String pgTxnResCode) {
	this.pgTxnResCode = pgTxnResCode;
}

public String getCustPhone() {
	return custPhone;
}

public void setCustPhone(String custPhone) {
	this.custPhone = custPhone;
}

public String getMopType() {
	return mopType;
}

public void setMopType(String mopType) {
	this.mopType = mopType;
}

public String getCardmask() {
	return cardmask;
}

public void setCardmask(String cardmask) {
	this.cardmask = cardmask;
}

public String getCurrencyCode() {
	return currencyCode;
}

public void setCurrencyCode(String currencyCode) {
	this.currencyCode = currencyCode;
}

public String getPgTxnRrn() {
	return pgTxnRrn;
}

public void setPgTxnRrn(String pgTxnRrn) {
	this.pgTxnRrn = pgTxnRrn;
}

public String getPgStatus() {
	return pgStatus;
}

public void setPgStatus(String pgStatus) {
	this.pgStatus = pgStatus;
}

public String getPgAmount() {
	return pgAmount;
}

public void setPgAmount(String pgAmount) {
	this.pgAmount = pgAmount;
}

public String getPgResMsg() {
	return pgResMsg;
}

public void setPgResMsg(String pgResMsg) {
	this.pgResMsg = pgResMsg;
}

public String getPaymentId() {
	return paymentId;
}

public void setPaymentId(String paymentId) {
	this.paymentId = paymentId;
}

public String getCustEmail() {
	return custEmail;
}

public void setCustEmail(String custEmail) {
	this.custEmail = custEmail;
}

public String getPgGateway() {
	return pgGateway;
}

public void setPgGateway(String pgGateway) {
	this.pgGateway = pgGateway;
}

public String getPgTxnId() {
	return pgTxnId;
}

public void setPgTxnId(String pgTxnId) {
	this.pgTxnId = pgTxnId;
}

public String getAcqId() {
	return acqId;
}

public void setAcqId(String acqId) {
	this.acqId = acqId;
}

public String getPgTxnType() {
	return pgTxnType;
}

public void setPgTxnType(String pgTxnType) {
	this.pgTxnType = pgTxnType;
}

public String getDupYn() {
	return dupYn;
}

public void setDupYn(String dupYn) {
	this.dupYn = dupYn;
}

public String getPgHash() {
	return pgHash;
}

public void setPgHash(String pgHash) {
	this.pgHash = pgHash;
}

public String getPgPaymentType() {
	return pgPaymentType;
}

public void setPgPaymentType(String pgPaymentType) {
	this.pgPaymentType = pgPaymentType;
}

public String getPgReturnUrl() {
	return pgReturnUrl;
}

public void setPgReturnUrl(String pgReturnUrl) {
	this.pgReturnUrl = pgReturnUrl;
}

public String getPgPayId() {
	return pgPayId;
}

public void setPgPayId(String pgPayId) {
	this.pgPayId = pgPayId;
}

public String getPgOrderId() {
	return pgOrderId;
}

public void setPgOrderId(String pgOrderId) {
	this.pgOrderId = pgOrderId;
}

public String getCustName() {
	return custName;
}

public void setCustName(String custName) {
	this.custName = custName;
}

public String getAgentId() {
	return agentId;
}

public void setAgentId(String agentId) {
	this.agentId = agentId;
}
 

 
 
 
}
