package com.bhartipay.lean;

public class DistributorIdAllot {
 
	private String userId; 
	private int allToken; 
	private int consumeToken; 
	private int remainingToken;
	private String aggregatorId;
	
	 
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getAllToken() {
		return allToken;
	}
	public void setAllToken(int allToken) {
		this.allToken = allToken;
	}
	public int getConsumeToken() {
		return consumeToken;
	}
	public void setConsumeToken(int consumeToken) {
		this.consumeToken = consumeToken;
	}
	public int getRemainingToken() {
		return remainingToken;
	}
	public void setRemainingToken(int remainingToken) {
		this.remainingToken = remainingToken;
	}
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	
	
	
}
