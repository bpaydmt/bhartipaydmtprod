package com.bhartipay.lean;


public class BbpsComplaint 
{

	private String userId;
	
	private String txnRefId;
	
	private String requestId;
	
	private String complaintDisposition;
	
	private String complaintDesc;
	
	private String complaintId;
	
	private String complaintAssigned;
	
	private String responseCode;
	
	private String responseReason;



	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTxnRefId() {
		return txnRefId;
	}

	public void setTxnRefId(String txnRefId) {
		this.txnRefId = txnRefId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getComplaintDisposition() {
		return complaintDisposition;
	}

	public void setComplaintDisposition(String complaintDisposition) {
		this.complaintDisposition = complaintDisposition;
	}

	public String getComplaintDesc() {
		return complaintDesc;
	}

	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}

	public String getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(String complaintId) {
		this.complaintId = complaintId;
	}

	public String getComplaintAssigned() {
		return complaintAssigned;
	}

	public void setComplaintAssigned(String complaintAssigned) {
		this.complaintAssigned = complaintAssigned;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseReason() {
		return responseReason;
	}

	public void setResponseReason(String responseReason) {
		this.responseReason = responseReason;
	}
	

	
	
}
