package com.bhartipay.lean;



public class DmtPricing
{
  
	private int id;
    
    private String userId;
	
	private String aggregatorId;
	
	//private String distributorId;
	
	//private String name;
	
	//private String walletId;
	
	private boolean status;
	
	private Integer rangeFrom;
	private Integer rangeTo;
	private double distributorPercentage;
	private double superDistributorPercentage;
	//private double agentPercentage;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	
	public Integer getRangeFrom() {
		return rangeFrom;
	}
	public void setRangeFrom(Integer rangeFrom) {
		this.rangeFrom = rangeFrom;
	}
	public Integer getRangeTo() {
		return rangeTo;
	}
	public void setRangeTo(Integer rangeTo) {
		this.rangeTo = rangeTo;
	}
	public double getDistributorPercentage() {
		return distributorPercentage;
	}
	public void setDistributorPercentage(double distributorPercentage) {
		this.distributorPercentage = distributorPercentage;
	}
	public double getSuperDistributorPercentage() {
		return superDistributorPercentage;
	}
	public void setSuperDistributorPercentage(double superDistributorPercentage) {
		this.superDistributorPercentage = superDistributorPercentage;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
	
	
	
}
