package com.bhartipay.biller.service.impl;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.bhartipay.biller.service.BillerService;
import com.bhartipay.biller.vo.BillerRequest;
import com.bhartipay.biller.vo.BillerResponseBean;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class BillerServiceImpl implements BillerService{
	public static Logger logger = Logger.getLogger(BillerServiceImpl.class);
	
	
	/**
	 * 
	 */
	public BillerResponseBean getBillerDetails(BillerRequest billerRequest){
		logger.debug("*************************calling method getBillerDetails*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(billerRequest);
		WebResource webResource = ServiceManager.getWebResource("BillerManager/getBillerDetails");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);
		String output = response.getEntity(String.class);
		BillerResponseBean billerResponseBean = gson.fromJson(output, BillerResponseBean.class);
		logger.debug("output result is**********" + billerResponseBean);
		return billerResponseBean;
		
	}
	
	public BillerResponseBean getBillOperatorDetails(BillerRequest billerRequest,String ipimei, String userAgent){
		logger.debug("*************************calling method getBillerDetails*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(billerRequest);
		WebResource webResource = ServiceManager.getWebResource("BillerManager/getBillerverification");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);
		String output = response.getEntity(String.class);
		BillerResponseBean billerResponseBean = gson.fromJson(output, BillerResponseBean.class);
		logger.debug("output result is**********" + billerResponseBean);
		return billerResponseBean;
		
	}
	
	public BillerResponseBean payOperatorBill(BillerRequest billerRequest,String ipimei, String userAgent)
	{

		logger.debug("*************************calling method getBillerDetails*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(billerRequest);
		WebResource webResource = ServiceManager.getWebResource("BillerManager/getBillerPayment");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).header("IPIMEI",ipimei).header("AGENT",userAgent).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);
		String output = response.getEntity(String.class);
		BillerResponseBean billerResponseBean = gson.fromJson(output, BillerResponseBean.class);
		logger.debug("output result is**********" + billerResponseBean);
		return billerResponseBean;
		
	
	}
	
	public BillerResponseBean getBillerDetailsB2C(BillerRequest billerRequest){
		logger.debug("*************************calling method getBillerDetails*********************************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(billerRequest);
		WebResource webResource = ServiceManager.getWebResource("BillerManager/getBillerDetailsB2C");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*************************getting response from service*********************************");
		if (response.getStatus() != 200) {
			logger.debug("*************************getting response from service::" + response.getStatus()
					+ "*********************************");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		logger.debug("getting response from service*********************************" + response);
		String output = response.getEntity(String.class);
		BillerResponseBean billerResponseBean = gson.fromJson(output, BillerResponseBean.class);
		logger.debug("output result is**********" + billerResponseBean);
		return billerResponseBean;
		
	}
}
