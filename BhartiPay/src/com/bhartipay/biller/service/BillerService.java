package com.bhartipay.biller.service;

import com.bhartipay.biller.vo.BillerRequest;
import com.bhartipay.biller.vo.BillerResponseBean;

public interface BillerService {
	
	/**
	 * 
	 * @param billerRequest
	 * @return
	 */
	
	public BillerResponseBean getBillerDetails(BillerRequest billerRequest);
	
	public BillerResponseBean getBillOperatorDetails(BillerRequest billerRequest,String ipimei, String userAgent);

	public BillerResponseBean payOperatorBill(BillerRequest billerRequest,String ipimei, String userAgent);
	
	public BillerResponseBean getBillerDetailsB2C(BillerRequest billerRequest);
}
