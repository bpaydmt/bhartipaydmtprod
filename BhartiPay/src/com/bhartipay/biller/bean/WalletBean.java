package com.bhartipay.biller.bean;

public class WalletBean {

	private String userId;
	private String oldpassword;
	private String password;
	private String imeiIP;
	private String otp;
	private int countryId;
	private String encText;
	private String walletid;
	private int addpkycid;
	private String addpdesc;
	private String addpkycpic;
	private int idpkycid;
	private String idpdesc;
	private String idpkycpic;
	private String profilePic;
	private int userType;
	private String aggreatorid;
	private int txnType;
	private String id;
	private String domainName;
	private String themes;
	private String mobileNo;
	private String emailId;
	private String userName;
	private String countryCode;
	String offerList;
	private String mobileno;
	private String utrNo;
	private String declinedComment;
	private String encKey;
	
	private String pan;
	

	private String numberSeries;
	private String distributerId;
	
	private String blockStatus;
	
	private String operatorName;
	private String circleName;
	

	public String getEncKey() {
		return encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	public String getEncText() {
		return encText;
	}

	public void setEncText(String encText) {
		this.encText = encText;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getBlockStatus() {
		return blockStatus;
	}

	public void setBlockStatus(String blockStatus) {
		this.blockStatus = blockStatus;
	}

	public String getDistributerId() {
		return distributerId;
	}

	public void setDistributerId(String distributerId) {
		this.distributerId = distributerId;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getNumberSeries() {
		return numberSeries;
	}

	public void setNumberSeries(String numberSeries) {
		this.numberSeries = numberSeries;
	}

	public String getDeclinedComment() {
		return declinedComment;
	}

	public void setDeclinedComment(String declinedComment) {
		this.declinedComment = declinedComment;
	}

	public String getUtrNo() {
		return utrNo;
	}

	public void setUtrNo(String utrNo) {
		this.utrNo = utrNo;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getOfferList() {
		return offerList;
	}

	public void setOfferList(String offerList) {
		this.offerList = offerList;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getThemes() {
		return themes;
	}

	public void setThemes(String themes) {
		this.themes = themes;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getTxnType() {
		return txnType;
	}

	public void setTxnType(int txnType) {
		this.txnType = txnType;
	}

	public String getAggreatorid() {
		return aggreatorid;
	}

	public void setAggreatorid(String aggreatorid) {
		this.aggreatorid = aggreatorid;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getOldpassword() {
		return oldpassword;
	}

	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImeiIP() {
		return imeiIP;
	}

	public void setImeiIP(String imeiIP) {
		this.imeiIP = imeiIP;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getWalletid() {
		return walletid;
	}

	public void setWalletid(String walletid) {
		this.walletid = walletid;
	}

	public int getAddpkycid() {
		return addpkycid;
	}

	public void setAddpkycid(int addpkycid) {
		this.addpkycid = addpkycid;
	}

	public String getAddpdesc() {
		return addpdesc;
	}

	public void setAddpdesc(String addpdesc) {
		this.addpdesc = addpdesc;
	}

	public String getAddpkycpic() {
		return addpkycpic;
	}

	public void setAddpkycpic(String addpkycpic) {
		this.addpkycpic = addpkycpic;
	}

	public int getIdpkycid() {
		return idpkycid;
	}

	public void setIdpkycid(int idpkycid) {
		this.idpkycid = idpkycid;
	}

	public String getIdpdesc() {
		return idpdesc;
	}

	public void setIdpdesc(String idpdesc) {
		this.idpdesc = idpdesc;
	}

	public String getIdpkycpic() {
		return idpkycpic;
	}

	public void setIdpkycpic(String idpkycpic) {
		this.idpkycpic = idpkycpic;
	}

}
