package com.bhartipay.biller.bean;

public class BillDetails 
{
	private String aggreatorId;
	
	private String number;
	
	private String  account;
	
	private String additionalInfo;
	
	private String billerType;
	
	private String billGrpNumber;
	
	private String walletUserId;
	
	private int billerId;
	
	private String billerAmount;
	
	private String billerConsumerId;
	
	private String phoneNumber;
	
	private String txn;

	private String amount;
	

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTxn() {
		return txn;
	}

	public void setTxn(String txn) {
		this.txn = txn;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getBillerType() {
		return billerType;
	}

	public void setBillerType(String billerType) {
		this.billerType = billerType;
	}

	public String getBillGrpNumber() {
		return billGrpNumber;
	}

	public void setBillGrpNumber(String billGrpNumber) {
		this.billGrpNumber = billGrpNumber;
	}

	public String getWalletUserId() {
		return walletUserId;
	}

	public void setWalletUserId(String walletUserId) {
		this.walletUserId = walletUserId;
	}

	public int getBillerId() {
		return billerId;
	}

	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}

	public String getBillerAmount() {
		return billerAmount;
	}

	public void setBillerAmount(String billerAmount) {
		this.billerAmount = billerAmount;
	}

	public String getBillerConsumerId() {
		return billerConsumerId;
	}

	public void setBillerConsumerId(String billerConsumerId) {
		this.billerConsumerId = billerConsumerId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
}
