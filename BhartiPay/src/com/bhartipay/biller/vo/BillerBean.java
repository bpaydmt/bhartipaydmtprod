package com.bhartipay.biller.vo;

import java.io.Serializable;

public class BillerBean implements Serializable{
	
	private String billerType;

	private String walletUserId;
	private String billerId;
	private String billerAccount;
	private String billerAmount;
	private String billerConsumerId;
	private String billGrpNumber;
	private String phoneNumber;
	private String additionalInfo;
	private String billerNumber;
	
	
	
	public String getWalletUserId() {
		return walletUserId;
	}

	public void setWalletUserId(String walletUserId) {
		this.walletUserId = walletUserId;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getBillerAccount() {
		return billerAccount;
	}

	public void setBillerAccount(String billerAccount) {
		this.billerAccount = billerAccount;
	}

	public String getBillerAmount() {
		return billerAmount;
	}

	public void setBillerAmount(String billerAmount) {
		this.billerAmount = billerAmount;
	}

	public String getBillerConsumerId() {
		return billerConsumerId;
	}

	public void setBillerConsumerId(String billerConsumerId) {
		this.billerConsumerId = billerConsumerId;
	}

	public String getBillGrpNumber() {
		return billGrpNumber;
	}

	public void setBillGrpNumber(String billGrpNumber) {
		this.billGrpNumber = billGrpNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getBillerNumber() {
		return billerNumber;
	}

	public void setBillerNumber(String billerNumber) {
		this.billerNumber = billerNumber;
	}

	public String getBillerType() {
		return billerType;
	}

	public void setBillerType(String billerType) {
		this.billerType = billerType;
	}
	
	
	
	
	
	
	
	
	

}
