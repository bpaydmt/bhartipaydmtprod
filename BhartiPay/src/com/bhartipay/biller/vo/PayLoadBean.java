package com.bhartipay.biller.vo;

import java.util.ArrayList;

public class PayLoadBean {

	private ArrayList<BillerDetailsBean> billerList;
	private String message;

	private String billerType;
	private String walletUserId;
	private int billerId;
	private String billerAccount;

	private double billerAmount;
	private String txnId;
	private String aggreatorId;
	
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAggreatorId() {
		return aggreatorId;
	}

	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}

	public String getBillerType() {
		return billerType;
	}

	public void setBillerType(String billerType) {
		this.billerType = billerType;
	}

	public String getWalletUserId() {
		return walletUserId;
	}

	public void setWalletUserId(String walletUserId) {
		this.walletUserId = walletUserId;
	}



	public int getBillerId() {
		return billerId;
	}

	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}

	public String getBillerAccount() {
		return billerAccount;
	}

	public void setBillerAccount(String billerAccount) {
		this.billerAccount = billerAccount;
	}

	public double getBillerAmount() {
		return billerAmount;
	}

	public void setBillerAmount(double billerAmount) {
		this.billerAmount = billerAmount;
	}

	

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public ArrayList<BillerDetailsBean> getBillerList() {
		return billerList;
	}

	public void setBillerList(ArrayList<BillerDetailsBean> billerList) {
		this.billerList = billerList;
	}
}
