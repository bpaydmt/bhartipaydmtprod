package com.bhartipay.biller.vo;

import java.io.Serializable;

public class BillerRequest implements Serializable {

	
	private String aggreatorId;
    private String request;
    
    
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
    

}
