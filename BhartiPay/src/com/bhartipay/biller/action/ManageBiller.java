package com.bhartipay.biller.action;

import java.io.InputStream;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;

import com.bhartipay.biller.bean.BillDetails;
import com.bhartipay.biller.bean.BillDetailsResponse;
import com.bhartipay.biller.service.BillerService;
import com.bhartipay.biller.service.impl.BillerServiceImpl;
import com.bhartipay.biller.vo.BillerBean;
import com.bhartipay.biller.vo.BillerDetailsBean;
import com.bhartipay.biller.vo.BillerRequest;
import com.bhartipay.biller.vo.BillerResponse;
import com.bhartipay.biller.vo.BillerResponseBean;
import com.bhartipay.biller.vo.PayLoadBean;
import com.bhartipay.util.OxyJWTSignUtil;
import com.bhartipay.wallet.cme.vo.ResPayload;
import com.bhartipay.wallet.commission.persistence.vo.CommPlanMaster;
import com.bhartipay.wallet.commission.service.CommissionService;
import com.bhartipay.wallet.commission.service.impl.CommissionServiceImpl;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.framework.pg.PGDetails;
import com.bhartipay.wallet.framework.security.AES128Bit;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;
import com.bhartipay.wallet.kyc.Utils.ObjectFactory;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.recharge.service.RechargeService;
import com.bhartipay.wallet.recharge.service.impl.RechargeServiceImpl;
import com.bhartipay.wallet.transaction.action.ManageTransaction;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.UserSummary;
import com.bhartipay.wallet.user.persistence.vo.WalletConfiguration;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;

import appnit.com.crypto.CheckSumHelper;
import io.jsonwebtoken.Claims;

public class ManageBiller extends BaseAction implements ServletResponseAware{
		HttpServletResponse response;
		BillerBean billerBean=new BillerBean();
		String mKey="e391ab57590132714ad32da9acf3013eb88c";
		BillerRequest billerRequest;
		OxyJWTSignUtil oxyJWTSignUtil=new OxyJWTSignUtil();
		BillerService billerService=new BillerServiceImpl();
		public BillDetails billDetails=new BillDetails();
		BillerResponseBean billerResponseBean=new BillerResponseBean();
		BillDetailsResponse billDetailsResp= new BillDetailsResponse();
		UserService service=new UserServiceImpl();
		WalletConfiguration config=new WalletConfiguration();
		ArrayList<BillerDetailsBean> resp;
		HashMap<String, String> prePaid = new HashMap<String, String>();
		HashMap<String, String> postPaid = new HashMap<String, String>();
		HashMap<String, String> dth = new HashMap<String, String>();
		HashMap<String, String> landLine = new HashMap<String, String>();
		HashMap<String, String> prePaidDataCard = new HashMap<String, String>();
		HashMap<String, String> postPaidDataCard = new  HashMap<String, String>();
		HashMap<String, String> circle = new HashMap<String, String>();
		TransactionService tService=new TransactionServiceImpl();
		UserService uService=new UserServiceImpl();
		CommPlanMaster commBean=new CommPlanMaster();
		CommissionService commService=new CommissionServiceImpl();
		Map<String, String> plans=new HashMap<String, String>();
		Map<String, String> txnType=new HashMap<String, String>();
		ActionContext actionContext ;
		ObjectFactory oFactory = new ObjectFactory();
		
		
		
		
	
	public ActionContext getActionContext() {
			return actionContext;
		}
		public void setActionContext(ActionContext actionContext) {
			this.actionContext = actionContext;
		}
	public Map<String, String> getPlans() {
			return plans;
		}
		public void setPlans(Map<String, String> plans) {
			this.plans = plans;
		}
		public Map<String, String> getTxnType() {
			return txnType;
		}
		public void setTxnType(Map<String, String> txnType) {
			this.txnType = txnType;
		}
	public CommPlanMaster getCommBean() {
			return commBean;
		}
		public void setCommBean(CommPlanMaster commBean) {
			this.commBean = commBean;
		}
	public HashMap<String, String> getPrePaid() {
			return prePaid;
		}
		public void setPrePaid(HashMap<String, String> prePaid) {
			this.prePaid = prePaid;
		}
		public HashMap<String, String> getPostPaid() {
			return postPaid;
		}
		public void setPostPaid(HashMap<String, String> postPaid) {
			this.postPaid = postPaid;
		}
		public HashMap<String, String> getDth() {
			return dth;
		}
		public void setDth(HashMap<String, String> dth) {
			this.dth = dth;
		}
		public HashMap<String, String> getLandLine() {
			return landLine;
		}
		public void setLandLine(HashMap<String, String> landLine) {
			this.landLine = landLine;
		}
		public HashMap<String, String> getPrePaidDataCard() {
			return prePaidDataCard;
		}
		public void setPrePaidDataCard(HashMap<String, String> prePaidDataCard) {
			this.prePaidDataCard = prePaidDataCard;
		}
		public HashMap<String, String> getPostPaidDataCard() {
			return postPaidDataCard;
		}
		public void setPostPaidDataCard(HashMap<String, String> postPaidDataCard) {
			this.postPaidDataCard = postPaidDataCard;
		}
		public HashMap<String, String> getCircle() {
			return circle;
		}
		public void setCircle(HashMap<String, String> circle) {
			this.circle = circle;
		}
	public ArrayList<BillerDetailsBean> getResp() {
			return resp;
		}
		public void setResp(ArrayList< BillerDetailsBean> resp) {
			this.resp = resp;
		}
	public UserService getService() {
			return service;
		}
		public void setService(UserService service) {
			this.service = service;
		}
		public WalletConfiguration getConfig() {
			return config;
		}
		public void setConfig(WalletConfiguration config) {
			this.config = config;
		}
	public BillDetails getBillDetails() {
			return billDetails;
		}
		public void setBillDetails(BillDetails billDetails) {
			this.billDetails = billDetails;
		}
	public BillerBean getBillerBean() {
			return billerBean;
		}
	public void setBillerBean(BillerBean billerBean) {
			this.billerBean = billerBean;
		}



	public String getBillerDetails() {
		  User user=(User)session.get("User");
		   System.out.println("232134232"+request.getParameter("billetType"));
		   String billetType=request.getParameter("billetType");
		  
		  
		  if(billetType==null ||billetType.isEmpty()){
		   try{
		    JSONObject jObject=new JSONObject();
		    
		    jObject.put("statusCode","1");
		    jObject.put("status", "ERROR"); 
		    jObject.put("statusMsg", "Please select the biller Type ."); 
		    
		    response.setContentType("application/json");
		          response.getWriter().println(jObject);
		    }catch(Exception e){
		    }
		   return null;
		  }
		  
		  HashMap<String, Object> reqParam=new HashMap<String, Object>();
		  reqParam.put("billerType", billetType);
		  billerRequest=new BillerRequest();
		  billerRequest.setAggreatorId(user.getAggreatorid());
		  billerRequest.setRequest(oxyJWTSignUtil.generateToken(reqParam, mKey));
		  billerResponseBean=billerService.getBillerDetails(billerRequest);
		  
		  if(billerResponseBean.getRequestId()==null ||billerResponseBean.getRequestId().isEmpty()||billerResponseBean.getResponse()==null ||billerResponseBean.getResponse().isEmpty()){
		   try{
		    JSONObject jObject=new JSONObject();
		    
		    jObject.put("statusCode","1");
		    jObject.put("status", "ERROR"); 
		    jObject.put("statusMsg", "Request not completed ,please try again."); 
		   
		    response.setContentType("application/json");
		          response.getWriter().println(jObject);
		    }catch(Exception e){
		    }
		   return null;
		  }
		   try{
		    Claims claim=oxyJWTSignUtil.parseToken(billerResponseBean.getResponse(),mKey); 
		    if(claim==null){
		     try{
		      JSONObject jObject=new JSONObject();
		      
		      jObject.put("statusCode","1");
		      jObject.put("status", "ERROR"); 
		      jObject.put("statusMsg", "Request not completed ,please try again."); 
		     
		      response.setContentType("application/json");
		            response.getWriter().println(jObject);
		      }catch(Exception e){
		      }
		     return null;
		     }
		    
		    JSONObject jObjectres=new JSONObject(claim);
		    BillerResponse billerResponse= new Gson().fromJson(jObjectres.toJSONString(), BillerResponse.class);
		    try{
		     response.setContentType("application/json");
		           response.getWriter().println(jObjectres);
		     }catch(Exception e){
		      e.printStackTrace(); 
		     }
		    return null;
		    }catch (Exception e) {
		     e.printStackTrace();
		    
		     try{
		     JSONObject jObject=new JSONObject();
		     jObject.put("statusCode","1");
		     jObject.put("status", "ERROR"); 
		     jObject.put("statusMsg", "Request not completed ,please try again."); 
		     response.setContentType("application/json");
		           response.getWriter().println(jObject);
		     }catch(Exception ex){
		     }
		    return null;
		   }
		 }

	public String getBillOperatorDetails(){
		User user = (User)session.get("User");
		try
			{
			String billerType = billDetails.getBillerType();
			int billerId =billDetails.getBillerId();
			String account=billDetails.getAccount();
			String number=billDetails.getNumber();
			String billGrpNumber="";
			String amt = billDetails.getAmount();
			String additionalInfo=billDetails.getAdditionalInfo();
			billerRequest=new BillerRequest();
			String userAgent=request.getHeader("User-Agent");
			
			
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			String respString="";
			HashMap<String,Object> reqParam =  new HashMap<String,Object>();
			billerRequest.setAggreatorId(user.getAggreatorid());
	
			
		    // billDetails-billerType,billerId,walletUserId,billerAccount,additionalInfo
			if(billerType.equalsIgnoreCase("Electricity")){
				//// inputs:---walletUserId,billerRequest.getAggreatorId(),billerId,billerNumber,serverName,requestId,ipiemi,agent,billerAccount,additionalInfo
				////walletUserId,billerRequest.getAggreatorId(),billerId,billerConsumerID,billerAccount,serverName,requestId,ipiemi,agent
				////walletUserId,billerRequest.getAggreatorId(),billerId,billerAccount,billGrpNumber, serverName, requestId,ipiemi,agent
				////walletUserId,billerRequest.getAggreatorId(),billerId,phoneNumber,billerAccount,authenticator3,termId, serverName, requestId,ipiemi,agent,amount
				switch(billerId)
				{
				case 235:
			 
					 
					 if(Pattern.compile("^[\\d]{9}$").matcher(billDetails.getNumber()).matches() && Pattern.compile("^[\\d]{2}$").matcher(billDetails.getAccount()).matches()){
						 additionalInfo="";
						 
					 }else{
						 
						 	if(!Pattern.compile("^[\\d]{9}$").matcher(billDetails.getNumber()).matches()){
						 		billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
					 			billDetailsResp.setErrorCode(1);
						 	} 
						 	
						 	if(!Pattern.compile("^[\\d]{2}$").matcher(billDetails.getAccount()).matches()){
						 	
						 		billDetailsResp.setErrorMessage("Please Fill The Valid Cycle Number.");
					 			billDetailsResp.setErrorCode(1);
						 	}
					 }
					 
					 break;
				 case 236:
				 case 237:
					 if( Pattern.compile("^[\\d]{9}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 238:
					 if( Pattern.compile("^[\\d]{11,12}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 340:
					 if( Pattern.compile("^[\\d]{9,10}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Service Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 342:
					 if(Pattern.compile("^[\\d]{12}$").matcher(billDetails.getNumber()).matches()  && Pattern.compile("^[\\d]{4}$").matcher(billDetails.getAccount()).matches()  && !billDetails.getAdditionalInfo().equals("-1")){
						 		
						 
					 }else{
						 		if(!Pattern.compile("^[\\d]{12}$").matcher(billDetails.getNumber()).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		if(!Pattern.compile("^[\\d]{4}$").matcher(billDetails.getAccount()).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill the Billing Unit.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		if(billDetails.getAdditionalInfo().equals("-1")){
						 			billDetailsResp.setErrorMessage("Please Select Processing Cycle.");
						 			billDetailsResp.setErrorCode(1);
						 		}
					 }
					 break;	 
				 case 330:
					 if(Pattern.compile("^[23][\\d]{11}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid K Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break; 
				 case 332 :
								
					 if(Pattern.compile("^[\\d]{1,15}$").matcher(billDetails.getNumber()).matches() &&  !billDetails.getAccount().equals("-1")){
						
						 additionalInfo="";
						 
					 }else{
						 		if(!Pattern.compile("^[\\d]{1,15}$").matcher(billDetails.getNumber()).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		
						 		if(billDetails.getAccount().equals("-1")){
						 			billDetailsResp.setErrorMessage("Please Select City Name.");
						 			billDetailsResp.setErrorCode(1);
						 			}
					 }
					 
					 break;
				 case 315:
				 case 345:
				 case 335:
				 case 326:
					 if(Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 317:
					 if(Pattern.compile("^[\\d]{11}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer ID.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 318:
					 if(Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Business Partner Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 324:
					 if(Pattern.compile("^[\\d\\w]{5,13}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 325:
					 if(Pattern.compile("^[\\d]{6,10}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Business Partner Number.");
				 			billDetailsResp.setErrorCode(1);
					 } 
					 break;
				 case 333:
					 if(Pattern.compile("^[\\d]{1,12}$").matcher(billDetails.getNumber()).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Consumer ID.");
				 			billDetailsResp.setErrorCode(1);
					 } 
					 break;
					 default:
						 billDetailsResp.setErrorMessage("No Data Found.");
				 		 billDetailsResp.setErrorCode(1);
				 		 break;
				}
			}else if(billerType.equalsIgnoreCase("Gas")){
				////input -------walletUserId,billerRequest.getAggreatorId(),billerId,billerConsumerID,billerAccount,serverName,requestId,ipiemi,agent
				 switch(billerId)
				 {
				  case 241:
			 			if(Pattern.compile("^[2]{1}[1]{1}[0-9]{10}$").matcher(billDetails.getNumber()).matches()  && Pattern.compile("^[\\d\\w]{1,8}$").matcher(billDetails.getAccount()).matches() ){
			 				
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("/[2]{1}[1]{1}[0-9]{10}$").matcher(billDetails.getNumber()).matches() ){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Customer Account Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 				if(!Pattern.compile("^[\\d\\w]{1,8}$").matcher(billDetails.getAccount()).matches() ){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Group Number");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
			 			break;
			 		case 321:
			 			if(Pattern.compile("^[\\d]{1,15}$").matcher(billDetails.getNumber()).matches()){
			 				account="";
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Sevice Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
				 		break;
			 		case 310:
			 			if(Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
			 				account="";
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Consumer Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
				 		break;
			 		case 338:
			 		if(Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
			 			account="";
						 additionalInfo="";
		 			}else{
		 				
		 				if(!Pattern.compile("^[\\d]{10}$").matcher(billDetails.getNumber()).matches()){
		 					billDetailsResp.setErrorMessage("Please Fill The Valid Customer ID.");
					 		 billDetailsResp.setErrorCode(1);
		 				}
		 			}
			 		break;
			 		default:
			 			billDetailsResp.setErrorMessage("No Data Found.");
				 		 billDetailsResp.setErrorCode(1);
				 		 break;
			 	
			 	
				 }
				 
			 }else if(billerType.equalsIgnoreCase("Insurance")){
				 account=billDetails.getNumber();
								 
				 Date dob = new Date();
				 SimpleDateFormat dformat = new SimpleDateFormat("dd-MM-yyyy");
				 SimpleDateFormat dparse = new SimpleDateFormat("yyyy-MMM-dd");
				 try
				 {
					 //Date db=dparse.parse(billDetails.getAccount());
					dob= dformat.parse(dformat.format(dparse.parse(billDetails.getAccount())));
					 billGrpNumber=dformat.format(dparse.parse(billDetails.getAccount()));
				 }
				 catch(ParseException e)
				 {
					 logger.info("Insurance DOB cannot parse to a date format.");
					 e.printStackTrace();
					 billDetailsResp.setErrorMessage("Invalid DOB.");
			 		 billDetailsResp.setErrorCode(1);
				 } 
				 if(billerId == 242 && billDetails.getNumber().length() < 10){
					 billDetailsResp.setErrorMessage("Invalid Policy No.");
			 		 billDetailsResp.setErrorCode(1);
				 		
				 		
				 	}else if(billerId == 243 && billDetails.getNumber().length() < 8){
				 		billDetailsResp.setErrorMessage("Invalid Policy No.");
				 		 billDetailsResp.setErrorCode(1);
					 		
				 	}else{
				 		account="";
						 additionalInfo="";
				 	}
				 
			  }else if(billerType.equalsIgnoreCase("Landline")){
				  if(amt == null || amt.isEmpty())
				  {
					  reqParam.put("amount","" );
				  }
				  else
				  {
					reqParam.put("amount",amt );
				  }
				  switch(billerId)
				  {
					 case 239:
						 if(Pattern.compile("^[\\d]{11}$").matcher(billDetails.getNumber()).matches() && Pattern.compile("^[\\d]{1,5}$").matcher(billDetails.getAmount()).matches()){
							 account="";
							 additionalInfo="";
							 
						 }else{
							 billDetailsResp.setErrorMessage("Please Provide Valid Phone Number Along with STD Code(11 Digits).");
					 		 billDetailsResp.setErrorCode(1);
						 }
						break;
					 case 240:
						 
						 	if(Pattern.compile("^[\\d]{8}$").matcher(billDetails.getNumber()).matches() && Pattern.compile("^[\\d]{10}$").matcher(billDetails.getAccount()).matches()){
						 		account="";
								 additionalInfo="";
						 		
						 	}else{
						 		if(!Pattern.compile("^[\\d]{8}$").matcher(billDetails.getNumber()).matches()){
						 			billDetailsResp.setErrorMessage("Please Provide Valid Phone Number (8 Digits).");
							 		 billDetailsResp.setErrorCode(1);          
						 		}
						 		if(!Pattern.compile("^[\\d]{10}$").matcher(billDetails.getAccount()).matches()){
						 			billDetailsResp.setErrorMessage("Please Provide Valid Customer Account Number(10 Digits).");
							 		 billDetailsResp.setErrorCode(1);
						 		}
						 		
						 	}
					 break;
					 case 344:
						 if(Pattern.compile("^[\\d]{1,10}$").matcher(billDetails.getNumber()).matches() && Pattern.compile("^[\\d]{10}$").matcher(billDetails.getAccount()).matches() && !billDetails.getAdditionalInfo().equals("-1")){
							 account="";
							 additionalInfo="";
						 }
						 else{
							
							 if(billDetails.getAdditionalInfo().equals("-1")){
								 billDetailsResp.setErrorMessage("Please select the Authenticator type.");
						 		 billDetailsResp.setErrorCode(1);
							 }
							 if(!Pattern.compile("^[\\d]{1,10}$").matcher(billDetails.getNumber()).matches()){
								 billDetailsResp.setErrorMessage("This Field Is Mandatsadasdroy. Please Fill This Feild.");
						 		 billDetailsResp.setErrorCode(1);
							 }
							 if(!Pattern.compile("^[\\d]{10}$").matcher(billDetails.getAccount()).matches()){
								 billDetailsResp.setErrorMessage("Please Provide Valid Account Number(10 Digits).");
						 		 billDetailsResp.setErrorCode(1);
							 }
							
						 }
						 break;
						 default:
							 billDetailsResp.setErrorMessage("No Data Found.");
					 		 billDetailsResp.setErrorCode(1);
				  }
				  
			  }
			if(billDetailsResp.getErrorCode() == 1)
			{
			    JSONObject jObject=new JSONObject();
			    
			    jObject.put("statusCode","1");
			    jObject.put("status", "ERROR"); 
			    jObject.put("statusMsg",billDetailsResp.getErrorMessage() ); 
			    
			    response.setContentType("application/json");
			    response.getWriter().println(jObject);	    
			}
			reqParam.put("billerConsumerId", number);
			reqParam.put("billerNumber", number);
			reqParam.put("billerType", billerType);
			reqParam.put("walletUserId", user.getId());
			reqParam.put("billerId", billerId);
			reqParam.put("billerAccount",account );
			reqParam.put("additionalInfo",additionalInfo );
			reqParam.put("billGrpNumber",billGrpNumber);
			reqParam.put("phoneNumber", number);
			billerRequest.setRequest(oxyJWTSignUtil.generateToken(reqParam, mKey));
			billerResponseBean = billerService.getBillOperatorDetails(billerRequest,ipAddress,userAgent);

			
		    if(billerResponseBean.getRequestId()==null ||billerResponseBean.getRequestId().isEmpty()||billerResponseBean.getResponse()==null ||billerResponseBean.getResponse().isEmpty()){
				   try{
				    JSONObject jObject=new JSONObject();
				    
				    jObject.put("statusCode","1");
				    jObject.put("status", "ERROR"); 
				    jObject.put("statusMsg", "Request not completed ,please try again."); 
				   
				    response.setContentType("application/json");
				          response.getWriter().println(jObject);
				         // return jObject.toString();
				    }catch(Exception e){
				    }
				   return null;
				  }
				   try{
				    Claims claim=oxyJWTSignUtil.parseToken(billerResponseBean.getResponse(),mKey); 
				    if(claim==null){
				     try{
				      JSONObject jObject=new JSONObject();
				      
				      jObject.put("statusCode","1");
				      jObject.put("status", "ERROR"); 
				      jObject.put("statusMsg", "Request not completed ,please try again."); 
				     
				      response.setContentType("application/json");
				            response.getWriter().println(jObject);
				           // return jObject.toString();
				      }catch(Exception e){
				      }
				     return null;
				     }
				    
				    JSONObject jObjectres=new JSONObject(claim);
				    BillerResponse billerResponse= new Gson().fromJson(jObjectres.toJSONString(), BillerResponse.class);
				    try{
				     response.setContentType("application/json");
				           response.getWriter().println(jObjectres);
				          // return jObjectres.toString();
				     }catch(Exception e){
				      e.printStackTrace(); 
				     }
				    return null;
				    }catch (Exception e) {
				     e.printStackTrace();
				    
				     try{
				     JSONObject jObject=new JSONObject();
				     jObject.put("statusCode","1");
				     jObject.put("status", "ERROR"); 
				     jObject.put("statusMsg", "Request not completed ,please try again."); 
				     response.setContentType("application/json");
				           response.getWriter().println(jObject);
				          // return jObject.toString();
				     }catch(Exception ex){
				     }
				    return null;
				 
				    }
			}
		catch(RuntimeException e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		catch(Exception e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		
			return null;
		}
	
	
	public String checkForRechargeAmount()
	{
		User user = (User)session.get("User");
		double walletBalance=0.0;
		double amount=0.0;
		String flag="fail";
		try
		{
			amount=Double.parseDouble(request.getParameter("billerAmount"));
			walletBalance=Double.parseDouble(new TransactionServiceImpl().getWalletBalance(user.getWalletid()));
			TxnInputBean inputBean=new TxnInputBean();
			String aggId = (String)session.get("aggId");
			if(walletBalance < amount)
			{
				//for b2c customer low amount will redirect to PG
				double pgAmt = amount-walletBalance;
				if(user.getUsertype() == 1)
				{
					String userAgent=request.getHeader("User-Agent");
					
					String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
					if (ipAddress == null) {
						   ipAddress = request.getRemoteAddr();
					}
					//new ManageTransaction().paymentGatewayB2C(amount,request.getHeader("User-Agent"),request.getHeader("X-FORWARDED-FOR"));
					flag = paymentGatewayB2C(user,aggId,pgAmt,userAgent,ipAddress);
					//inputBean.setTxnId(Double.toString(amount-walletBalance));
					if(flag.equalsIgnoreCase("addMoney"))
					{
						return "addMoney";
					}
					
				}
				else
				{
					 try{
						    JSONObject jObject=new JSONObject();
						    
						    jObject.put("statusCode","1");
						    jObject.put("status", "ERROR"); 
						    jObject.put("statusMsg", "Your wallet Balance is low, Please recharge your wallet Account."); 
						   
						    response.setContentType("application/json");
						          response.getWriter().println(jObject);
						        //  return jObject.toString();
						    }catch(Exception e){
						    }
						   return null;
				}
			}
			else
			{
				
			}
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return flag;
	}
	
	public String payOperatorBill()
	{
		try
		{
			session = ActionContext.getContext().getSession();
			request=ServletActionContext.getRequest();
			response=ServletActionContext.getResponse();
		User user = (User)session.get("User");
		HashMap<String,String> map = new HashMap<String,String>();
		
			generateCsrfToken();
			String transId = request.getParameter("txnId");
			String billerType = request.getParameter("billerType");
			 request.getSession().putValue("billerType", billerType);
		        session.put("billerType", billerType);
			double amount=Double.parseDouble(request.getParameter("billerAmount"));
			double walletBalance=0.0;
			billerRequest=new BillerRequest();
			String userAgent=request.getHeader("User-Agent");
		
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			String respString="";
			HashMap<String,Object> reqParam =  new HashMap<String,Object>();
			billerRequest.setAggreatorId(user.getAggreatorid());		
			
			if(billDetailsResp.getErrorCode() == 1)
			{
			    JSONObject jObject=new JSONObject();
			    
			    jObject.put("statusCode","1");
			    jObject.put("status", "ERROR"); 
			    jObject.put("statusMsg",billDetailsResp.getErrorMessage() ); 
			    
			    response.setContentType("application/json");
			    response.getWriter().println(jObject);	    
			}
			reqParam.put("billerType", billerType);
			reqParam.put("walletUserId", user.getId());
			reqParam.put("billerAmount",amount);
			reqParam.put("txnId",transId);
			

			TreeMap<String,String> treeMap=new TreeMap<String,String>();
			
			treeMap.put("billerType", billerType);
			treeMap.put("walletUserId", user.getId());
			treeMap.put("billerAmount",""+amount);
			treeMap.put("txnId",transId);
			treeMap.put("token",user.getToken());
			
			  try {
					String checkSumString=CheckSumHelper.getCheckSumHelper().genrateCheckSum(user.getTokenSecurityKey(),treeMap);
					reqParam.put("CHECKSUMHASH",checkSumString);
			       } catch (Exception e) {
					e.printStackTrace();
			       }
			
			
			
			
			billerRequest.setRequest(oxyJWTSignUtil.generateToken(reqParam, mKey));
			
			billerResponseBean = billerService.payOperatorBill(billerRequest,ipAddress,userAgent);
			////walletUserId,billerRequest.getAggreatorId(),txnId,billerAmount,serverName,requestId,ipiemi,agent,authenticator3,termId
			
		    if(billerResponseBean.getRequestId()==null ||billerResponseBean.getRequestId().isEmpty()||billerResponseBean.getResponse()==null ||billerResponseBean.getResponse().isEmpty()){
				   try{
				    JSONObject jObject=new JSONObject();
				    
				    jObject.put("statusCode","1");
				    jObject.put("status", "ERROR"); 
				    jObject.put("statusMsg", "Request not completed ,please try again."); 
				   
				    response.setContentType("application/json");
				          response.getWriter().println(jObject);
				        //  return jObject.toString();
				    }catch(Exception e){
				    }
				   return null;
				  }
				   try{
				    Claims claim=oxyJWTSignUtil.parseToken(billerResponseBean.getResponse(),mKey); 
				    if(claim==null){
				     try{
				      JSONObject jObject=new JSONObject();
				      
				      jObject.put("statusCode","1");
				      jObject.put("status", "ERROR"); 
				      jObject.put("statusMsg", "Request not completed ,please try again."); 
				     
				      response.setContentType("application/json");
				            response.getWriter().println(jObject);
				          //  return jObject.toString();
				      }catch(Exception e){
				      }
				     return null;
				     }
				    
				    JSONObject jObjectres=new JSONObject(claim);
				    BillerResponse billerResponse= new Gson().fromJson(jObjectres.toJSONString(), BillerResponse.class);
				    try{
				     response.setContentType("application/json");
				           response.getWriter().println(jObjectres);
				          // return jObjectres.toString();
				     }catch(Exception e){
				      e.printStackTrace(); 
				     }
				    return null;
				    }catch (Exception e) {
				     e.printStackTrace();
				    
				     try{
				     JSONObject jObject=new JSONObject();
				     jObject.put("statusCode","1");
				     jObject.put("status", "ERROR"); 
				     jObject.put("statusMsg", "Request not completed ,please try again."); 
				     response.setContentType("application/json");
				           response.getWriter().println(jObject);
				          // return jObject.toString();
				     }catch(Exception ex){
				     }
				    return null;
				 
				    }
			}
		catch(RuntimeException e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		catch(Exception e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		
			return null;
		
	}

	public String getBillerDetailsB2C()
	{
		try
		{
			generateCsrfToken();
			HashMap<String,Object> reqParam = new HashMap<String,Object>();
			resp = new ArrayList<BillerDetailsBean>();//This parameter is response parameter.
			String billerType = request.getParameter("billerType");
			request.getSession().putValue("billerType", billerType);
	        session.put("billerType", billerType);
			String aggrid=(String)session.get("aggId");//service.getAggIdByDomain(config);
			if(billerType == null || billerType.equals(""))
			{
				addActionError("Oops! something went wronge. Please try after sometime.");
				return "fail";
			}
			reqParam.put("billerType",billerType);
			billerRequest=new BillerRequest();
			billerRequest.setAggreatorId(aggrid);
			billerRequest.setRequest(oxyJWTSignUtil.generateToken(reqParam, mKey));
			billerResponseBean = billerService.getBillerDetails(billerRequest);
			if(billerResponseBean.getRequestId() == null || billerResponseBean.getRequestId().equals("") || billerResponseBean.getResponse() == null || billerResponseBean.getResponse().equals(""))
			{
				addActionError("Oops! something went wronge. Please try after sometime.");
				return "fail";
			}
			try
			{
				Claims claim = oxyJWTSignUtil.parseToken(billerResponseBean.getResponse(), mKey);
				if(claim == null)
				{
					addActionError("Oops! something went wronge. Please try after sometime.");
					return "fail";
				}
				else
				{
					//session.put("resp", claim.get("resPayload"));
					HashMap payLoad  = (HashMap)claim.get("resPayload");
					setResp((ArrayList<BillerDetailsBean>)payLoad.get("billerList"));
					//session.put("resp",(ArrayList<BillerDetailsBean>)payLoad.get("billerList"));
					return "success";
				}			
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return "fail";
	}

	public String getBillOperatorDetailsB2C(){
		session = ActionContext.getContext().getSession();
		request=ServletActionContext.getRequest();
		generateCsrfToken();
		//User user = (User)request.getSession().getAttribute("User");
		
		User user = (User)session.get("User");
		try
		{
			session.remove("electFlow");
			session.put("loginStatus", "");
			if(billDetails != null)
			{
				String objGson = new Gson().toJson(billDetails);
				boolean flag = oFactory.checkScript(objGson);
				if(!flag)
				{
					addActionError("Oops  something went wronge. Please try after sometime !!");
					return "fail";
				}
			}
			if(user == null || user.getId() == null)
			{
				session.put("loginStatus", "fail");

				session.put("BillDetails", billDetails);
				
				RechargeService rs=new RechargeServiceImpl();
				Map<String, HashMap<String,String>> mapResult2= rs.getRechargeOperator();
				request.setAttribute("mapResult",mapResult2);
				

				if(mapResult2!=null){
					try{
					setPrePaid(mapResult2.get("PREPAID"));
					setPostPaid(mapResult2.get("POSTPAID"));
					setDth(mapResult2.get("DTH"));
					setLandLine(mapResult2.get("LANDLINE"));
					setPrePaidDataCard(mapResult2.get("PREPAIDDATACARD"));
					setPostPaidDataCard(mapResult2.get("POSTPAIDDATACARD"));
					setCircle(mapResult2.get("CIRCLE"));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				return "login";
			}
			BillDetails billDetailBean = (BillDetails)session.get("BillDetails");
			String billerType ;
			int billerId ;
			String account;
			String number;
			String billGrpNumber="";
			String amt ;
			String additionalInfo;
			if(billDetailBean != null && billDetailBean.getBillerType() != null)
			{
				billDetails = billDetailBean;
				if(billDetails != null)
				{
					String objGson = new Gson().toJson(billDetails);
					boolean flag = oFactory.checkScript(objGson);
					if(!flag)
					{
						addActionError("Oops  something went wronge. Please try after sometime !!");
						return "fail";
					}
				}
				billerType = billDetails.getBillerType();
				billerId =billDetails.getBillerId();
				account=billDetails.getAccount();
				number=billDetails.getNumber();
				amt = billDetails.getAmount();
				additionalInfo=billDetails.getAdditionalInfo();
				session.remove("BillDetails");
			}
			else
			{
				billerType = billDetails.getBillerType();
				billerId =billDetails.getBillerId();
				account=billDetails.getAccount();
				number=billDetails.getNumber();
				billGrpNumber="";
				amt = billDetails.getAmount();
				additionalInfo=billDetails.getAdditionalInfo();
			}
			request.getSession().putValue("billerType", billerType);
	        session.put("billerType", billerType);
			billerRequest=new BillerRequest();
			String userAgent=request.getHeader("User-Agent");
			
			
			String ipAddress = request.getHeader("X-FORWARDED-FOR") ;
			if (ipAddress == null) {
				   ipAddress = request.getRemoteAddr();
			}
			String respString="";
			HashMap<String,Object> reqParam =  new HashMap<String,Object>();
			billerRequest.setAggreatorId(user.getAggreatorid());
	
			if(billerType.equalsIgnoreCase("Electricity")){
				switch(billerId)
				{
				case 235:
			 
					 
					 if(Pattern.compile("^[\\d]{9}$").matcher(number).matches() && Pattern.compile("^[\\d]{2}$").matcher(account).matches()){
						 additionalInfo="";
						 
					 }else{
						 
						 	if(!Pattern.compile("^[\\d]{9}$").matcher(number).matches()){
						 		billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
					 			billDetailsResp.setErrorCode(1);
						 	} 
						 	
						 	if(!Pattern.compile("^[\\d]{2}$").matcher(account).matches()){
						 	
						 		billDetailsResp.setErrorMessage("Please Fill The Valid Cycle Number.");
					 			billDetailsResp.setErrorCode(1);
						 	}
					 }
					 
					 break;
				 case 236:
				 case 237:
					 if( Pattern.compile("^[\\d]{9}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 238:
					 if( Pattern.compile("^[\\d]{11,12}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 340:
					 if( Pattern.compile("^[\\d]{9,10}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 	billDetailsResp.setErrorMessage("Please Fill The Valid Service Number.");
				 			billDetailsResp.setErrorCode(1);		
						 
					 }
					
					 break;
				 case 342:
					 if(Pattern.compile("^[\\d]{12}$").matcher(number).matches()  && Pattern.compile("^[\\d]{4}$").matcher(account).matches()  && !additionalInfo.equals("-1")){
						 		
						 
					 }else{
						 		if(!Pattern.compile("^[\\d]{12}$").matcher(number).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		if(!Pattern.compile("^[\\d]{4}$").matcher(account).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill the Billing Unit.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		if(additionalInfo.equals("-1")){
						 			billDetailsResp.setErrorMessage("Please Select Processing Cycle.");
						 			billDetailsResp.setErrorCode(1);
						 		}
					 }
					 break;	 
				 case 330:
					 if(Pattern.compile("^[23][\\d]{11}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
						 
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid K Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break; 
				 case 332 :
								
					 if(Pattern.compile("^[\\d]{1,15}$").matcher(number).matches() &&  !account.equals("-1")){
						
						 additionalInfo="";
						 
					 }else{
						 		if(!Pattern.compile("^[\\d]{1,15}$").matcher(number).matches()){
						 			billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
						 			billDetailsResp.setErrorCode(1);
						 		}
						 		
						 		if(account.equals("-1")){
						 			billDetailsResp.setErrorMessage("Please Select City Name.");
						 			billDetailsResp.setErrorCode(1);
						 			}
					 }
					 
					 break;
				 case 315:
				 case 345:
				 case 335:
				 case 326:
					 if(Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 317:
					 if(Pattern.compile("^[\\d]{11}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer ID.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 318:
					 if(Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Business Partner Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 324:
					 if(Pattern.compile("^[\\d\\w]{5,13}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Customer Number.");
				 			billDetailsResp.setErrorCode(1);
					 }
					 break;
				 case 325:
					 if(Pattern.compile("^[\\d]{6,10}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Business Partner Number.");
				 			billDetailsResp.setErrorCode(1);
					 } 
					 break;
				 case 333:
					 if(Pattern.compile("^[\\d]{1,12}$").matcher(number).matches()){
						 account="";
						 additionalInfo="";
					 }else{
						 billDetailsResp.setErrorMessage("Please Fill The Valid Consumer ID.");
				 			billDetailsResp.setErrorCode(1);
					 } 
					 break;
					 default:
						 billDetailsResp.setErrorMessage("No Data Found.");
				 		 billDetailsResp.setErrorCode(1);
				 		 break;
				}
			}else if(billerType.equalsIgnoreCase("Gas")){
				////input -------walletUserId,billerRequest.getAggreatorId(),billerId,billerConsumerID,billerAccount,serverName,requestId,ipiemi,agent
				 switch(billerId)
				 {
				  case 241:
			 			if(Pattern.compile("^[2]{1}[1]{1}[0-9]{10}$").matcher(number).matches()  && Pattern.compile("^[\\d\\w]{1,8}$").matcher(account).matches() ){
			 				
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("/[2]{1}[1]{1}[0-9]{10}$").matcher(number).matches() ){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Customer Account Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 				if(!Pattern.compile("^[\\d\\w]{1,8}$").matcher(account).matches() ){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Group Number");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
			 			break;
			 		case 321:
			 			if(Pattern.compile("^[\\d]{1,15}$").matcher(number).matches()){
			 				account="";
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Sevice Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
				 		break;
			 		case 310:
			 			if(Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
			 				account="";
							 additionalInfo="";
			 			}else{
			 				
			 				if(!Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
			 					billDetailsResp.setErrorMessage("Please Fill The Valid Consumer Number.");
						 		 billDetailsResp.setErrorCode(1);
			 				}
			 			}
				 		break;
			 		case 338:
			 		if(Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
			 			account="";
						 additionalInfo="";
		 			}else{
		 				
		 				if(!Pattern.compile("^[\\d]{10}$").matcher(number).matches()){
		 					billDetailsResp.setErrorMessage("Please Fill The Valid Customer ID.");
					 		 billDetailsResp.setErrorCode(1);
		 				}
		 			}
			 		break;
			 		default:
			 			billDetailsResp.setErrorMessage("No Data Found.");
				 		 billDetailsResp.setErrorCode(1);
				 		 break;
			 	
			 	
				 }
				 
			 }else if(billerType.equalsIgnoreCase("Insurance")){
				// account=number;
								 
				 Date dob = new Date();
				 SimpleDateFormat dformat = new SimpleDateFormat("dd-MMM-yyyy");
				 SimpleDateFormat dparse = new SimpleDateFormat("yyyy-MMM-dd");
				 try
				 {
					 //Date db=dparse.parse(billDetails.getAccount());
					dob= dformat.parse(dformat.format(dparse.parse(account)));
					 billGrpNumber=dformat.format(dparse.parse(account));
				 }
				 catch(ParseException e)
				 {
					 logger.info("Insurance DOB cannot parse to a date format.");
					 e.printStackTrace();
					 billDetailsResp.setErrorMessage("Invalid DOB.");
			 		 billDetailsResp.setErrorCode(1);
				 } 
				 if(billerId == 242 && number.length() < 10){
					 billDetailsResp.setErrorMessage("Invalid Policy No.");
			 		 billDetailsResp.setErrorCode(1);
				 		
				 		
				 	}else if(billerId == 243 && number.length() < 8){
				 		billDetailsResp.setErrorMessage("Invalid Policy No.");
				 		 billDetailsResp.setErrorCode(1);
					 		
				 	}else{
				 		account="";
						 additionalInfo="";
				 	}
				 
			  }else if(billerType.equalsIgnoreCase("Landline")){
				  if(amt == null || amt.isEmpty())
				  {
					  reqParam.put("amount","" );
				  }
				  else
				  {
					reqParam.put("amount",amt );
				  }
				  switch(billerId)
				  {
					 case 239:
						 if(Pattern.compile("^[\\d]{11}$").matcher(number).matches() && Pattern.compile("^[\\d]{1,5}$").matcher(amt).matches()){
							 account="";
							 additionalInfo="";
							 
						 }else{
							 billDetailsResp.setErrorMessage("Please Provide Valid Phone Number Along with STD Code(11 Digits).");
					 		 billDetailsResp.setErrorCode(1);
						 }
						break;
					 case 240:
						 
						 	if(Pattern.compile("^[\\d]{8}$").matcher(number).matches() && Pattern.compile("^[\\d]{10}$").matcher(account).matches()){
						 		account="";
								 additionalInfo="";
						 		
						 	}else{
						 		if(!Pattern.compile("^[\\d]{8}$").matcher(number).matches()){
						 			billDetailsResp.setErrorMessage("Please Provide Valid Phone Number (8 Digits).");
							 		 billDetailsResp.setErrorCode(1);          
						 		}
						 		if(!Pattern.compile("^[\\d]{10}$").matcher(account).matches()){
						 			billDetailsResp.setErrorMessage("Please Provide Valid Customer Account Number(10 Digits).");
							 		 billDetailsResp.setErrorCode(1);
						 		}
						 		
						 	}
					 break;
					 case 344:
						 if(Pattern.compile("^[\\d]{1,10}$").matcher(number).matches() && Pattern.compile("^[\\d]{10}$").matcher(account).matches() && !additionalInfo.equals("-1")){
							 account="";
							 additionalInfo="";
						 }
						 else{
							
							 if(additionalInfo.equals("-1")){
								 billDetailsResp.setErrorMessage("Please select the Authenticator type.");
						 		 billDetailsResp.setErrorCode(1);
							 }
							 if(!Pattern.compile("^[\\d]{1,10}$").matcher(number).matches()){
								 billDetailsResp.setErrorMessage("This Field Is Mandatsadasdroy. Please Fill This Feild.");
						 		 billDetailsResp.setErrorCode(1);
							 }
							 if(!Pattern.compile("^[\\d]{10}$").matcher(account).matches()){
								 billDetailsResp.setErrorMessage("Please Provide Valid Account Number(10 Digits).");
						 		 billDetailsResp.setErrorCode(1);
							 }
							
						 }
						 break;
						 default:
							 billDetailsResp.setErrorMessage("No Data Found.");
					 		 billDetailsResp.setErrorCode(1);
				  }
				  
			  }
			if(billDetailsResp.getErrorCode() == 1)
			{
			    JSONObject jObject=new JSONObject();
			    
			    jObject.put("statusCode","1");
			    jObject.put("status", "ERROR"); 
			    jObject.put("statusMsg",billDetailsResp.getErrorMessage() ); 
			    
			    response.setContentType("application/json");
			    response.getWriter().println(jObject);	    
			}
			reqParam.put("billerConsumerId", number);
			reqParam.put("billerNumber", number);
			reqParam.put("billerType", billerType);
			reqParam.put("walletUserId", user.getId());
			reqParam.put("billerId", billerId);
			reqParam.put("billerAccount",account );
			reqParam.put("additionalInfo",additionalInfo );
			reqParam.put("billGrpNumber",billGrpNumber);
			reqParam.put("phoneNumber", number);
			billerRequest.setRequest(oxyJWTSignUtil.generateToken(reqParam, mKey));
			billerResponseBean = billerService.getBillOperatorDetails(billerRequest,ipAddress,userAgent);

			 session.put("electFlow","TRUE");
			 PayLoadBean respay = new PayLoadBean();
			 respay.setMessage("301");
			 session.put("resPayLoad",respay);
		    if(billerResponseBean.getRequestId()==null ||billerResponseBean.getRequestId().isEmpty()||billerResponseBean.getResponse()==null ||billerResponseBean.getResponse().isEmpty()){
				   try{

				   }catch(Exception e){
				    }
				   return "fail";
				  }
		   
				   try{
				    Claims claim=oxyJWTSignUtil.parseToken(billerResponseBean.getResponse(),mKey); 
				    if(claim==null){
				     try{
				    	 
				     }catch(Exception e){
				      }
				     return "fail";
				     }
				    
				    JSONObject jObjectres=new JSONObject(claim);
				    BillerResponse billerResponse= new Gson().fromJson(jObjectres.toJSONString(), BillerResponse.class);
				    try{
				    	session.remove("resPayLoad");
				    	respay = billerResponse.getResPayload();
				    	respay.setMessage("300");
				    	session.put("resPayLoad",respay);
						String flag=checkForRechargeAmount();
				    	/*jObjectres.put("BillerType", billerType);
				     response.setContentType("application/json");
				           response.getWriter().println(jObjectres);*/
				           session.put("electFlow","TRUE");
				           if(flag.equalsIgnoreCase("addMoney"))
				           {
				        	   return "addMoney";
				           }
				           return "success";
				     }catch(Exception e){
				      e.printStackTrace(); 
				     }
				    
				    }catch (Exception e) {
				     e.printStackTrace();
				    
				     try{
				    
				     }catch(Exception ex){
				     }
				    return "fail";
				 
				    }
			}
		catch(RuntimeException e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		catch(Exception e)
		{
			logger.info("************problem in *********e.message"+e.getMessage());
			e.printStackTrace();
		}
		
			return null;
		}
	
	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response = arg0;
	}

	public String paymentGatewayB2C(User user,String aggId,double amount,String userAgent,String ipAddress){		
		logger.debug("**********************  calling paymentGateway() **************************");
		
		if(user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty()){
			logger.debug("********************** user==null||user.getWalletid()==null||user.getMobileno()==null||user.getWalletid().isEmpty()||user.getMobileno().isEmpty() **************************");
			addActionError("Please try again..");
			return "fail";
		}		
		
		if(amount <=0 ){
			logger.debug("********************** inputBean.getTrxAmount() **************************"+amount);
			addActionError("Please Enter Valid Amount.");
			return "fail";
		}
		
		
		logger.debug("**********************  calling service savePGTrx **************************");
		
		/*String userAgent=request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR") ;*/
		if (ipAddress == null) {
			   ipAddress = "0.0.0.0";
		}
		
		String result=tService.savePGTrx(amount, user.getWalletid(), user.getMobileno(), "b2c customer","",aggId,ipAddress,userAgent);
		logger.debug("**********************  getting response from paymentGateway()="+result+" **************************");

		if(result.equalsIgnoreCase("1001")){
			addActionError("There is some problem please try again.");
			return "fail";
		}
		
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid mobile no.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7014")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your per transaction limit.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7017")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7018")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7019")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7020")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per day.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7021")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per week.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7022")){
			addActionError("Transaction failed, we regret to inform you that you are crossing the limit of max number of transaction per month.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7045")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per quarter.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7046")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per halt yearly.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7047")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your funds transfer limit per year.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7048")){
			addActionError("Transaction failed, Wallet balance violated.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7000")){
			addActionError("Transaction fail.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7023")){
			addActionError("Invalid reciver mobile number.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7024")){
			addActionError("Transaction failed due to insufficient balance.");
			return "fail";
		}
		if(result.equalsIgnoreCase("7042")){
			addActionError("Transaction failed, we regret to inform you that you are crossing your minimum balance limit.");
			return "fail";
		}
		
		
		String txnId=result;
		Properties prop = new Properties();
		InputStream in = ServiceManager.class.getResourceAsStream("PaymentGateway.properties");
		try{
		prop.load(in);
		in.close();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		WalletConfiguration config1=new WalletConfiguration();
		config1.setAggreatorid(aggId);
		
		
		Map<String,String> mapResult=service.getWalletConfigPG(config1);
		
		
		//Map<String,String> mapResult=(Map<String, String>)session.get("mapResult");
		String appName=mapResult.get("pgAgId");
		String userName=mapResult.get("pgAgId");
		String MID=mapResult.get("pgMid");
		
		String successURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/CRSecurePay.action";
		String	failURL=mapResult.get("pgCallBackUrl")+"/BhartiPay/CRSecurePay.action";
		
		logger.debug("**********************  CallBackFailURL**************************"+successURL);
		logger.debug("**********************  failURL**************************"+failURL);
		
		String submitTo=mapResult.get("pgUrl");
		String securitykey=mapResult.get("pgEncKey");
//		String TxnType=mapResult.get("pgTxnType");
//		String country=mapResult.get("pgCountry");
//		String currency=mapResult.get("pgCurrency");
		//String Channel="WEB";
		
//		String operatingMode=prop.getProperty("operatingMode");
//		String otherDetails=prop.getProperty("otherDetails");
//		String collaborator=prop.getProperty("collaborator");
				
				
				String requestParameter="Appname="+appName+"|TransID="+txnId+"|Amount="+amount+"|Appuser="+userName+"|CallBackURL="+successURL+"|MID="+MID;
				PGDetails pgDetails=new PGDetails();
				String billingDtls = pgDetails.getBillingDtls();
logger.debug("***********************Before encryption parameters **********************************");				
logger.debug("request parameter for paymentgateway  "+requestParameter);
logger.debug("pgdetails    "+pgDetails);
logger.debug("billing details    "+billingDtls);

			    if(user.getEmailid()!=null&&user.getEmailid().length()>0){
			    	billingDtls=billingDtls.replace("sandeep.prajapati@appnittech.com",user.getEmailid());
			    }
			   /* if(user.getName()!=null&&user.getName().length()>0){
			    	billingDtls=billingDtls.replace("Appnit Technologies",user.getName());
			    }*/
			    if(user.getMobileno()!=null&&user.getMobileno().length()>0){
			    	billingDtls=billingDtls.replace("9335235731",user.getMobileno());
			    }
			    
				String shippingDtls = pgDetails.getShippingDtls();
				//String key = Encryption.getEncryptionKey("rechappPG");
				
				System.out.println(pgDetails.getBillingDtls());
				System.out.println(pgDetails.getShippingDtls());
				System.out.println(requestParameter);
				requestParameter = AES128Bit.encrypt(requestParameter, securitykey);
				billingDtls = AES128Bit.encrypt(billingDtls, securitykey);
				shippingDtls = AES128Bit.encrypt(shippingDtls, securitykey);
				requestParameter = requestParameter.replaceAll("\n", "");
				billingDtls = billingDtls.replaceAll("\n", "");
				shippingDtls = shippingDtls.replaceAll("\n", "");
				logger.debug("***********************after encryption parameters **********************************");				
				logger.debug("request parameter for paymentgateway  "+requestParameter);
				logger.debug("pgdetails    "+pgDetails);
				logger.debug("billing details    "+billingDtls);
				logger.debug("MID is  "+MID);
				request.setAttribute("requestparameter",requestParameter);
				request.setAttribute("billingDtls",billingDtls);
				request.setAttribute("shippingDtls",shippingDtls);
				request.setAttribute("MID",MID);
				request.setAttribute("submitTo",submitTo);
				
				
				
		return "addMoney";
		
	}
	
	public String pgResponseB2C() {
		logger.debug("********************************************response come from payment gateway********************************************");
		User user=null;
		StringBuffer mobileAppResponse = new StringBuffer();
		String responseparams = request.getParameter("responseparams");
		String hash=request.getParameter("hash");
		Properties paymentProp = null;

		String result = "fail";
		String msg="";

		Logger logger = Logger.getLogger(this.getClass().getName());
		logger.debug("********************************************** response come from Payment gateway = "
				+ responseparams);
		if (responseparams == null) {
			logger.debug("********************************************** getting resp Parameter as null so recharge could not be done   ****************************************");
			msg="Payment fialed.";
			return result;
		} else {

			paymentProp = new Properties();
			StringTokenizer stokz = new StringTokenizer(responseparams, "|");
			while (stokz.hasMoreTokens()) {
				String tok = (String) stokz.nextToken();
				paymentProp.put(tok.substring(0, tok.indexOf('=')),
						tok.substring(tok.indexOf('=') + 1));
			}
			String id=null;
			String trxId=null;
			String amount="0.0";
			String method = request.getMethod();
			if (method.equalsIgnoreCase("GET")) {
				logger.debug("********************************************** request coming from get method    ****************************************");
				msg="Payment fialed.";
				result = "fail";
				
			} else {
				logger.debug("********************************************response come from post method ********************************************");
				id = paymentProp.getProperty("plutusTxnId");
				String status = paymentProp.getProperty("status");
				String code = paymentProp.getProperty("responseCode");
				String otherDetails = paymentProp.getProperty("appName");
				trxId = paymentProp.getProperty("appTransId");
				amount = paymentProp.getProperty("amount");  
				
				if (status != null && status.contains("success")|| code.equals("01")) {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");
					msg="Amount successfully added to your wallet.";
					result="success";
					//return "success";
				} else {
					logger.debug("********************************************response come from payment gateway is "+status+" ********************************************");

					msg="Payment failed.";
					result = "fail";
					//return "fail";
				}
				logger.debug("*******************************calling service profileBytxnid and getwalletconfig*************************************");

				Map<String, String> resultMap=uService.ProfilebytxnId(trxId);
				WalletConfiguration conf=new WalletConfiguration();
				if(Double.parseDouble(String.valueOf(resultMap.get("usertype")))==4){
				conf.setAggreatorid(resultMap.get("id"));
				}
				else{
					conf.setAggreatorid(resultMap.get("aggreatorid"));
				}
				Map<String,String> config=uService.getWalletConfig(conf);
				if(config==null||config.size()==0){
					logger.debug("*******************************getting wallet config as null******************************");
					msg="An error has occurred please try again.";
					return "fail";
				}
				logger.debug("**********************************creating user session*************************************");
				
				createUserSession(resultMap, config,conf.getAggreatorid());
			    user=(User)session.get("User");
			    logger.debug("**********************************calling service updatePGTrx*************************************");
				String updateResult=tService.updatePGTrx(hash,responseparams,user.getId(),trxId,id,Double.parseDouble(amount), result, "");
				logger.debug("********************************getting response from service ************************************"+updateResult);
				if(updateResult.equalsIgnoreCase("1001")){
				msg="Payment failed.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7029")){
				msg="Payment failed from payment gateway.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("1000")){
				msg="Amount successfully added";
				result="success";
				}
				if(updateResult.equalsIgnoreCase("7000")){
				msg="An error has occurred.";
				result="fail";
				}
				if(updateResult.equalsIgnoreCase("7015")){
				msg="Payment failed.";
				result="fail";
				}
			}
			if(result.equalsIgnoreCase("success")){
				if(user!=null&&user.getEmailid()!=null){
					UserSummary usummery=new UserSummary();
					usummery.setUserId(user.getEmailid());
					usummery.setAggreatorid(user.getAggreatorid());
				Map<String, String> resultMap=uService.ProfilebyloginId(usummery);
				updateWalletDetails(resultMap);
				}
				addActionMessage(msg);
			}
			else{
				addActionError(msg);
			}
			logger.debug("**********************************returning result *************************************"+result);
			return result;
		}
	

	}
	
	public void updateWalletDetails(Map<String,String> resultMap){
		logger.debug("*********************************updateing walletDetails************************************");
		User user=(User)session.get("User");
		
		
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setAgentid(resultMap.get("agentid"));
		user.setSubAgentId(resultMap.get("subAgentId"));
		user.setDistributerid(resultMap.get("distributerid"));
		user.setSuperdistributerid(resultMap.get("superdistributerid")!=null?resultMap.get("superdistributerid"):"-1");
		user.setAggreatorid(resultMap.get("aggreatorid"));
		user.setKycStatus(resultMap.get("kycStatus"));
		user.setIsimps(Integer.parseInt(resultMap.get("isimps")));
		user.setWhiteLabel(Integer.parseInt(resultMap.get("whiteLabel")!=null?resultMap.get("whiteLabel"):"0"));
		user.setWallet(Integer.parseInt(resultMap.get("wallet")));
		user.setBank(Integer.parseInt(resultMap.get("bank")));
		user.setPortalMessage(resultMap.get("portalMessage")!=null?resultMap.get("portalMessage"):"");
		user.setAgentCode(resultMap.get("agentCode")!=null?resultMap.get("agentCode"):"");
		user.setAsCode(resultMap.get("asCode")!=null?resultMap.get("asCode"):"");
		user.setAsAgentCode(resultMap.get("asAgentCode")!=null?resultMap.get("asAgentCode"):"");
		user.setShopName(resultMap.get("shopName")!=null?resultMap.get("shopName"):"");
		user.setAepsChannel(resultMap.get("aepsChannel")!=null?resultMap.get("aepsChannel"):"");
		user.setBank1(resultMap.get("bank1")!=null?resultMap.get("bank1"):"1");
		user.setBank2(resultMap.get("bank2")!=null?resultMap.get("bank2"):"1");
		user.setBank3(resultMap.get("bank3")!=null?resultMap.get("bank3"):"1");
		user.setBank3_via(resultMap.get("bank3_via")!=null?resultMap.get("bank3_via"):"1");
		user.setOnlineMoney(resultMap.get("onlineMoney")!= null ? Integer.parseInt(String.valueOf(resultMap.get("onlineMoney")).substring(0, 1)) : Integer.parseInt("0"));
		user.setRecharge(resultMap.get("recharge")!= null ? Integer.parseInt(String.valueOf(resultMap.get("recharge")).substring(0, 1)) : Integer.parseInt("0") );
		user.setBbps(resultMap.get("bbps")!= null ? Integer.parseInt(String.valueOf(resultMap.get("bbps")).substring(0, 1)) : Integer.parseInt("0") );

		session.put("User",user);
		logger.debug("*********************************successfully updated walletdetails************************************");
	}
	
	public void createUserSession(Map<String,String> resultMap,Map<String,String> config,String aggId){
		logger.debug("*********************************user session creating************************************");
		
		
		/*************************for menu options***************************/
		commBean.setAggreatorid(aggId);
		Map<String, HashMap<String,String>> res=commService.getPlanDtl(commBean);
		if(resultMap!=null){
			setPlans(res.get("PLAN"));
			setTxnType(res.get("TXNTYPE"));
		}
		
		session.put("menuMap", res.get("TXNTYPE"));
		session.put("aggId",aggId);
		/*************************end menu options***************************/
		
		
		User user=new User();
		
		String userImg=uService.getProfilePic(resultMap.get("id"));
		user.setUserImg(userImg);
		user.setEmailid(resultMap.get("emailid"));
		user.setFinalBalance(Double.parseDouble(String.valueOf(resultMap.get("finalBalance"))));
		user.setId(resultMap.get("id"));
		user.setMobileno(resultMap.get("mobileno"));
		user.setName(resultMap.get("name"));
		user.setWalletid(resultMap.get("walletid"));
		user.setUsertype(Double.parseDouble(String.valueOf(resultMap.get("usertype"))));
		user.setAgentid(resultMap.get("agentid"));
		user.setSubAgentId(resultMap.get("subAgentId"));
		user.setDistributerid(resultMap.get("distributerid"));
		user.setSuperdistributerid(resultMap.get("superdistributerid")!=null?resultMap.get("superdistributerid"):"-1");
		user.setAggreatorid(resultMap.get("aggreatorid"));
		
		user.setCountry(config.get("country"));
		user.setCountrycurrency(config.get("countrycurrency"));
		user.setCountryid(Double.parseDouble(String.valueOf(config.get("countryid"))));
		user.setEmailvalid(Double.parseDouble(String.valueOf(config.get("emailvalid"))));
		user.setKycvalid(Double.parseDouble(String.valueOf(config.get("kycvalid"))));
		user.setOtpsendtomail(Double.parseDouble(String.valueOf(config.get("otpsendtomail"))));
		user.setSmssend(Double.parseDouble(String.valueOf(config.get("smssend"))));
		user.setStatus(Double.parseDouble(String.valueOf(config.get("status"))));
		
		user.setLogo(config.get("logopic"));
		user.setCustomerCare(config.get("customerCare")!=null?config.get("customerCare"):"0120-4000004");
		user.setSupportEmailId(config.get("supportEmailId")!=null?config.get("supportEmailId"):"");
		user.setBanner(config.get("bannerpic"));
		session.put("logo",config.get("logopic"));
		session.put("banner",config.get("bannerpic"));
		
		session.put("User",user);
//		session.put("serviceMaster",config.get("serviceMaster"));
		session.put("mapResult",config);
		logger.debug("*********************************user session created successfully************************************");
	}
	
	
	public boolean generateCsrfToken(){
		logger.info("*****generateCsrfToken****");
		try
		{
		Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)session.get("csrfPreventionSaltCache");

	    if (csrfPreventionSaltCache == null){
	        csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.MINUTES).build();

	        session.put("csrfPreventionSaltCache", csrfPreventionSaltCache);
	    }

	    // Generate the salt and store it in the users cache
	    String salt = RandomStringUtils.random(20, 0,0, true, true,null, new SecureRandom());
	    csrfPreventionSaltCache.put(salt, Boolean.TRUE);

	    session.put("csrfPreventionSalt", salt);
	    return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	
	public static void main(String[] args) {
		 Date dob = new Date();
		 SimpleDateFormat dformat = new SimpleDateFormat("dd-MM-yyyy");
		 SimpleDateFormat dparse = new SimpleDateFormat("yyyy-MMM-dd");
		 try
		 {
			dob= dformat.parse(dformat.format(dparse.parse("2017-nov-12")));
			System.out.println(dformat.format(dob)); 
		 }
		 catch(ParseException e)
		 {
			 logger.info("Insurance DOB cannot parse to a date format.");
			 e.printStackTrace();
		 } 
	//new ManageBiller().	getBillerDetails();
	}

}
