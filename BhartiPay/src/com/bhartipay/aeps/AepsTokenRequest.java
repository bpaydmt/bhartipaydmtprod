package com.bhartipay.aeps;

public class AepsTokenRequest {
	
    public String agentAuthId;
    public String agentAuthPassword;
    public String retailerId; 
    public String pipe;
    
	public AepsTokenRequest() {
	}

	public String getAgentAuthId() {
		return agentAuthId;
	}

	public void setAgentAuthId(String agentAuthId) {
		this.agentAuthId = agentAuthId;
	}

	public String getAgentAuthPassword() {
		return agentAuthPassword;
	}

	public void setAgentAuthPassword(String agentAuthPassword) {
		this.agentAuthPassword = agentAuthPassword;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
 
	public String getPipe() {
		return pipe;
	}

	public void setPipe(String pipe) {
		this.pipe = pipe;
	}
    
    
	
}
