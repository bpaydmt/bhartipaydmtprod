package com.bhartipay.customerCare.vo;

import java.io.Serializable;



public class CustomerCareResponse implements Serializable {
	
	private String statusCode;
	private String statusMsg;
	
	private double walletBalance;
	private double oxyCashBalance;
	
	private AgentCustDetails agentCustDetails;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public double getOxyCashBalance() {
		return oxyCashBalance;
	}

	public void setOxyCashBalance(double oxyCashBalance) {
		this.oxyCashBalance = oxyCashBalance;
	}

	public AgentCustDetails getAgentCustDetails() {
		return agentCustDetails;
	}

	public void setAgentCustDetails(AgentCustDetails agentCustDetails) {
		this.agentCustDetails = agentCustDetails;
	}
	
	
	
	
	
	
	
	
	

}
