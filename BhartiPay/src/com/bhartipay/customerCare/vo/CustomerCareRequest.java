package com.bhartipay.customerCare.vo;

import java.io.Serializable;

public class CustomerCareRequest implements Serializable {
	
	private String userId;
	private String aggreatorId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAggreatorId() {
		return aggreatorId;
	}
	public void setAggreatorId(String aggreatorId) {
		this.aggreatorId = aggreatorId;
	}
	
	
	

}
