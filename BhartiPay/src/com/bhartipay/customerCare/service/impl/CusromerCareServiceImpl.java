package com.bhartipay.customerCare.service.impl;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.bhartipay.customerCare.service.CusromerCareService;
import com.bhartipay.customerCare.vo.CustomerCareRequest;
import com.bhartipay.customerCare.vo.CustomerCareResponse;
import com.bhartipay.wallet.framework.service.utility.ServiceManager;



public class CusromerCareServiceImpl implements CusromerCareService{
	
	private static final Logger logger = Logger.getLogger(CusromerCareServiceImpl.class.getName());

	@Override
	public CustomerCareResponse getUserDtl(CustomerCareRequest customerCareRequest) {
		
		logger.debug("*****************calling method  getPendingPanReq ***********************");
		Gson gson = new Gson();
		String jsonText=gson.toJson(customerCareRequest);
		logger.debug("*****************json text as parameter to calling service getUserDtl***********************"+jsonText);
		WebResource webResource=ServiceManager.getWebResource("CustomerCareManager/CustomerCareResponse");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,jsonText);
		logger.debug("*****************response come from getPendingPanReq***********************");
		if (response.getStatus() != 200){
			logger.debug("*****************response come***********************"+response.getStatus());	
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
		}
		logger.debug("*****************response come from service***********************"+response);
		String output = response.getEntity(String.class);		
		logger.debug("*****************response come from service string***********************"+output);
		CustomerCareResponse customerCareResponse= gson.fromJson(output,CustomerCareResponse.class);
		return customerCareResponse;
	}
	
	
	

}
