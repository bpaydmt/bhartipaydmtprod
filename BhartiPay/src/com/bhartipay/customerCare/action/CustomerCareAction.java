package com.bhartipay.customerCare.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.bhartipay.bbps.vo.BbpsPayment;
import com.bhartipay.customerCare.service.CusromerCareService;
import com.bhartipay.customerCare.service.impl.CusromerCareServiceImpl;
import com.bhartipay.customerCare.vo.CustomerCareRequest;
import com.bhartipay.customerCare.vo.CustomerCareResponse;
import com.bhartipay.lean.OnboardStatusResponse;
import com.bhartipay.wallet.aeps.AEPSLedger;
import com.bhartipay.wallet.framework.action.BaseAction;
import com.bhartipay.wallet.matm.MATMLedger;
import com.bhartipay.wallet.recharge.bean.RechargeTxnBean;
import com.bhartipay.wallet.report.bean.B2CMoneyTxnMast;
import com.bhartipay.wallet.report.bean.ReportBean;
import com.bhartipay.wallet.report.service.ReportService;
import com.bhartipay.wallet.report.service.impl.ReportServiceImpl;
import com.bhartipay.wallet.transaction.persistence.vo.PassbookBean;
import com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean;
import com.bhartipay.wallet.transaction.service.TransactionService;
import com.bhartipay.wallet.transaction.service.impl.TransactionServiceImpl;
import com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean;
import com.bhartipay.wallet.user.persistence.vo.User;
import com.bhartipay.wallet.user.persistence.vo.WalletMastBean;
import com.bhartipay.wallet.user.service.UserService;
import com.bhartipay.wallet.user.service.impl.UserServiceImpl;


public class CustomerCareAction extends BaseAction implements ServletResponseAware{
	
	HttpServletResponse response=null;
	CustomerCareRequest customerCareRequest;
	CustomerCareResponse customerCareResponse;
	CusromerCareService cusromerCareServicel=new CusromerCareServiceImpl ();
	TxnInputBean inputBean=new TxnInputBean();
	TransactionService tService=new TransactionServiceImpl();
	ReportService rService=new ReportServiceImpl();
	private UserService service=new UserServiceImpl();
	
	
	

	public TxnInputBean getInputBean() {
		return inputBean;
	}

	public void setInputBean(TxnInputBean inputBean) {
		this.inputBean = inputBean;
	}

	public CustomerCareRequest getCustomerCareRequest() {
		return customerCareRequest;
	}

	public void setCustomerCareRequest(CustomerCareRequest customerCareRequest) {
		this.customerCareRequest = customerCareRequest;
	}



	public String startCustomerCare(){
		return SUCCESS;
	}


	public String getUserDtl(){
		customerCareResponse=new CustomerCareResponse();
		User user=(User)session.get("User");
		if(user==null){
			return "error";
		}
		
		if(customerCareRequest.getUserId()==null ||customerCareRequest.getUserId().isEmpty()){
			addActionError("Please provide user ID/ Mobile Number/Emailid .");
			return SUCCESS;
		}
		
		customerCareRequest.setAggreatorId(user.getAggreatorid());
		customerCareResponse=cusromerCareServicel.getUserDtl(customerCareRequest);
		if(customerCareResponse.getAgentCustDetails()==null )
		{
			addActionError("No data found.");
		 return "error";	
		}
		WalletMastBean data=service.getPlan(customerCareRequest.getUserId());
		request.setAttribute("plan",data);	
		
		OnboardStatusResponse onboardStatus = service.onboardStatus(customerCareRequest.getUserId());
		request.setAttribute("onboardStatus",onboardStatus);
		
		request.setAttribute("GETUSERDTL",customerCareResponse);
		return SUCCESS;
		
	}


	
	public String customerPassbookById(){
		  User us=(User)session.get("User");
		  String userId=request.getParameter("userId");
		 if(userId!=null)
		  inputBean.setUserId(userId);
		  
		  logger.debug("*********************************calling show customerPassbookById************************************");
		  List<PassbookBean> listResult=tService.showPassbookById(inputBean);
		  request.setAttribute("listResult",listResult);
		  return "success";
		 }
	
	
	public String customerCashBackPassbookById(){
		 
		 User us=(User)session.get("User");
		  String userId=request.getParameter("userId");
		 
		  if(userId!=null)
			 inputBean.setUserId(userId);
		  logger.debug("*********************************calling show customerCashBackPassbookById************************************");
		  //tService.showPassbook(inputBean);
		  List<PassbookBean> listResult=tService.showCashBackPassbookById(inputBean);
		  request.setAttribute("listResult",listResult);
		  return "success";
		 }
	
	
	
	public String customerRechargeReport(){
		logger.debug("*********************************calling rechargeReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
		logger.debug("*********************************calling service rechargeReport************************************");
		List<RechargeTxnBean>	list=rService.rechargeReport(reportBean);
		request.setAttribute("resultList",list);
		return "success";
	}
	
	
	
	
	public String getDmtDetailsReport(){
		
		logger.debug("*********************************calling getDmtDetailsReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  	  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
			
		List <DmtDetailsMastBean> revnList=new ArrayList<>();
		revnList =rService.getDmtDetailsReportList(reportBean);
		request.setAttribute("agentList", revnList);
		
		return "success";	
	}
	
	
	
	
	public String getCustAepsReport(){
		
		logger.debug("*********************************calling getCustAepsReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  	  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
			
		List <AEPSLedger> aepsList=new ArrayList<>();
		aepsList =rService.getCustAepsReport(reportBean);
		request.setAttribute("aepsList", aepsList);
		
		return "success";	
	}
	
	public String getCustBBPSReport(){
		
		logger.debug("*********************************calling getCustBBPSReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  	  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
			
		List <BbpsPayment> bbpsList=new ArrayList<>();
		bbpsList =rService.getCustBBPSReport(reportBean);
		request.setAttribute("bbpsList", bbpsList);
		
		return "success";	
	}
	
	
	public String getCustMATMReport(){
		
		logger.debug("*********************************calling getCustMATMReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  	  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
			
		List <MATMLedger> matmList=new ArrayList<>();
		matmList =rService.getCustMATMReport(reportBean);
		request.setAttribute("matmList", matmList);
		
		return "success";	
	}
	
	
	
	
	
	public String getCustDmtDetailsReport(){
		
		logger.debug("*********************************calling getDmtDetailsReport************************************");
		User user=(User)session.get("User");
		ReportBean reportBean=new ReportBean();
		String userId=request.getParameter("userId");
	  if(userId!=null){
		  	  inputBean.setUserId(userId);
			  reportBean.setUserId(userId);
	  }else{
		  reportBean.setUserId(inputBean.getUserId());  
	  }
	  	
		reportBean.setStDate(inputBean.getStDate());
		reportBean.setEndDate(inputBean.getEndDate());
			
		List <B2CMoneyTxnMast> revnList=new ArrayList<B2CMoneyTxnMast>();
		revnList =rService.getCustDmtDetailsReport(reportBean);
		request.setAttribute("agentList", revnList);
		
		return "success";	
	}
	


	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		response=arg0;
	}

}
