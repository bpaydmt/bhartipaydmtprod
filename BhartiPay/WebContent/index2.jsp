<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>



<%
	User userSession = (User) session.getAttribute("User");
	if (userSession != null && userSession.getAggreatorid() != null
			&& !userSession.getAggreatorid().isEmpty()) {
		response.sendRedirect("UserHome");
	} else {
%>

<%
	    response.setHeader("Strict-Transport-Security", "max-age=7776000; includeSubdomains");
		response.addHeader("X-FRAME-OPTIONS", "DENY");
		response.setHeader("X-Frame-Options", "SAMEORIGIN");
		response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
		response.setDateHeader("Expires", -1); //prevents caching at the proxy server
		response.flushBuffer();
		// String pgToken=  net.pg.utility.Helper.getEncodedValue();
%>

<%@taglib uri="/struts-tags" prefix="s"%>

<%
		Map<String, String> mapResult = (Map<String, String>) session.getAttribute("mapResult");
		String theams = mapResult.get("themes");
		String favicon = "";
		if (mapResult != null)
			favicon = mapResult.get("favicon");
%>

<%

	String merchantName = mapResult.get("appname");
		String domainName = mapResult.get("domainName");
%>
<!DOCTYPE html>
<html class="<%=theams%>">

<%if(domainName.indexOf("atsv") < 0){%>
<head>
<title><%=mapResult.get("caption")%></title>
<link href="./css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="./css/new_signup.css" rel='stylesheet' type='text/css' /> 
<link href="./css/normalize.min.css" rel='stylesheet' type='text/css' /> 
<link rel="shortcut icon" href="./images/<%=favicon%>">


<%-- <script src="./js/sql.injection.js"></script> --%>



<script type="text/javascript">

function removeSession(userName)
{
	/* $.ajax({
		method:"Post",
		url:
	}) */
   document.login.userId.value=userName;
   document.login.action= "/RemoveSesion";
   document.login.submit();
}

</script>


<%
	String redirect = request.getParameter("redirect");

		if (redirect == null) {
			redirect = "false";
		}
%>

<style>
.appLinkWrapper{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column;
}
.appLinkWrapper .appLinkInnerWrapper{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: row;
    width: fit-content;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
}
.appLinkWrapper p {
}
.appLinkWrapper a{
}
</style>

</head>






<body onload="backButtonOverride();">

<% if(domainName.indexOf("partner.bhartipay.com")>-1){ %>
<jsp:include page="independencePopup.jsp"></jsp:include>
<%} %>
    <div class="container-fluid"> 
			<%
				String logo = (String) mapResult.get("logopic");
				String banner = (String) mapResult.get("bannerpic");
			%> 
        <div class="row no-gutters min-h-fullscreen bg-white">				
			
			<div class="col-md-7 col-sm-7 col-xs-12 bg-img" data-overlay="5" style="background-color: #bee4c7;">
			
				<div class="row h-100 pl-50">
					<div class="col-md-10 col-lg-8 align-self-end" style="padding-left: 0px;">
					<%
					if(domainName.indexOf("saksham") > 0){
					%>
					<img src="<%=logo%>"  style="width: 60%;" class="login-logo"/>
					<%
					}
					else{
						%>
						<img src="<%=logo%>"  style="width: 25%;" class="login-logo"/>
						<%
					}
					%>
					   <%--  <img src="<%=logo%>"  style="width: 25%;" class="login-logo"/>  --%>
					    <!-- <img src="./image/happy%20diwali%20(BHARTIPAY).gif"  style="" class="diwali-image"/> -->
					    <img src="./images/giphy.gif"  style="" class="diwali-image"/>
					</div>
					<!-- <div class="col-md-12 col-lg-12 align-self-end">
					     
					</div> -->
				</div>
			</div>				
				
			<div class="col-md-5 col-sm-5 col-xs-12 align-self-center" style="position:relative">
				<div class="login-box" id="login">
					<h4>Login</h4>

					<p style="margin-bottom: 0px;">
						<small>Sign into your account</small>
					</p>

					<s:form name=" " action="UserLogin" method="POST"
						onselectstart="return false" oncontextmenu="return false;">
						<s:token />
						
						<div>
							<font color="red"><s:actionerror escape="false" /></font> 
							<font color="blue"><s:actionmessage /></font>
						</div>
						
						<s:hidden name="totalAttempt" />
						<s:hidden name="oldUser" />

						<table style="width: 100%;">
							<tr>
								<td>  
									<s:textfield name="user.userId" type="text" id="userId" style="width: 100%;padding: 12px 0px;margin-bottom: 0px;"  placeholder="User Id" autocomplete="off" required="required"  />  								
								</td>
							</tr>
							<tr>
								<td> 
									<s:textfield name="user.password" type="password" id="Login_user_password" placeholder="Password" autocomplete="off" style="width: 100%;padding: 12px 0px;margin-bottom: 0px;" required="required" onkeypress="passCheck()" maxlength="32" /> 
								</td>
							</tr>
							
						</table>

						
						 
						<br />

						<div class="rederror" id="error3"></div>
						<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 

						<tr>
							<td align="left" height="60" valign="middle" style="width:100%;">
								<s:fielderror class="redvalid">
									<s:param>userId</s:param>
									<s:param>Login_user_password</s:param> 
									<span class="margin2"> 
										<input type="checkbox" checked="checked" class="term-conditions termAndCondition">I agree to usage 
										<a href="#" data-toggle="modal" data-target="#termCondition">Terms and Conditions</a>
							        </span>
								    <span class="errorMsg termAndConditionErr"></span> 
								</s:fielderror> 
								<s:submit class="btn btn-bold btn-block btn-primary" key="submit" value="Login" onclick="submitVaForm('#login-form')"></s:submit>
							</td>
						</tr>

						<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							<tr> 
						 <%-- 	<td align="center" valign="middle">
									New user? <s:a class="text-primary fw-500" action="RetailerDistributorOnboarding"> Sign up</s:a>
								</td>
						 --%>		
								<td align="right" valign="middle">
									<s:a class="text-primary fw-500" action="ForgotPassword">Forgot Password?</s:a>
								</td>
							</tr>
						</table>

					</s:form>
					 
					<%--  
					<div classs="appLinkWrapper"  style="display: flex; justify-content: center; align-items: center; flex-flow: column;">
			      	 <div class="appLinkInnerWrapper" style="display: flex;justify-content: center;align-items: center;flex-flow: row;width: fit-content;padding: 10px;border: 1px solid #ccc;border-radius: 5px;    position: absolute; bottom: -110px;">
		             <p style=" margin: 0px; margin-right: 20px; font-size: 14px;font-weight: 600; ">Download app</p>
		             <a href="<%=mapResult.get("retailerAppUrl") %>" style="margin-right: 10px;">Retailer</a> 
		             <a href="<%=mapResult.get("distributorAppUrl") %>"  style="margin-right: 10px;">Distributor </a>
		             </div>
		           </div>
		          --%>
		         
		         
				</div>
				 
			</div>

		 
		  
		</div> 
    </div>
	<!-- Javascript -->
	<script src="./js/jquery.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/jquery.backstretch.min.js"></script>
	<script src="./js/scripts.js"></script>
	

<jsp:include page="policy.jsp"></jsp:include>


</body>

<%}else{ %>



<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<meta name="description" content="Welcome:: ATSV E-SOLUTION INDIA PVT LTD">
	<meta name="CreativeLayers" content="ATFN">
	<!-- css file -->
	<link rel="stylesheet" href="atsv/css/bootstrap.min.css">
	<link rel="stylesheet" href="atsv/css/style.css">
	<!-- Responsive stylesheet -->
	<link rel="stylesheet" href="atsv/css/responsive.css">

	<!-- Slick -->
	<link type="text/css" rel="stylesheet" href="atsv/css/slick.css" />
	<link type="text/css" rel="stylesheet" href="atsv/css/slick-theme.css" />
	<!-- Title -->
	<title>Welcome:: ATSV E-SOLUTION INDIA PVT LTD</title>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style>
  	.sign_up_form .heading {    margin-bottom: 0px;}
  	.why-txt {border-width: 0 1px 1px 0;color: #fff;font-size: 25px;min-height: 120px;padding: 34px 9px 14px 0;border-style: solid;}
  	.about_box_home6 .details h3 {color: #ff1053;}
  	.top_courses .thumb .overlay .tag{ width: 100px;}
  	.txt-f25{ font-size: 20px; text-transform: uppercase; font-weight: 600;}
  	.bor-right{ border-right: 0;}
  	.bor-bottom{ border-bottom: 0;}
  	.img_hvr_box{ min-height: 180px;}
  	.icon {font-size: 50px !important;text-align: center; margin-left: 38px;top: 0;}
  	.top_courses .details .tc_content{ padding: 10px; background: #192675;}
  	.top_courses .details .tc_content p{ color: #fff; text-align: center;}
  	.home1-divider2{padding: 50px 0 40px; }      
  	button.slick-prev.slick-arrow {BACKGROUND: #fcfaff;width: 50px;height: 50px; left: 5px; z-index: 1;font-weight: 700;border-radius: 50%;}
  	button.slick-next.slick-arrow{BACKGROUND: #fcfaff;width: 50px;height: 50px;right: 5px;font-weight: 700;border-radius: 50%;}
  	.slick-prev:before{ content: url(atsv/images/left-icon.png);    } 
  	.slick-next:before{ content: url(atsv/images/right-icon.png);   }  
  	.sign_up_form.inner_page, .login_form.inner_page{padding: 10px 20px ;}
  	.btn-thm2{background-color:#be1210}
  	.btn-thm2:hover, .btn-thm2:active, .btn-thm2:focus{ border-color:#be1210;color:#000;}
  	.sign_up_form .form-control{ margin-bottom: 10px;}
  	.form-control {padding: 5px 15px;}
  	.about_box_home6 .details p {line-height: 25px;margin-bottom: 10px;max-width: 680px;text-align: justify;}
  	.navigation { height: 100px;background: #fff;}
  	.brand {position: absolute;padding-left: 20px;float: left;line-height: 93px;}
  	.brand a, .brand a:visited {color: #000;text-decoration: none;}
  	.nav-container {max-width: 1320px;margin: 0 auto;}
  	#Registration_Div input{
  		text-transform: uppercase;
  	}
  	nav {float: right;}
  	.footer_one { padding: 30px 0;}
  	.b2v-box{width:400px; min-height:460px; position:absolute; top:140px; right:60px;border-radius:0px;  background: rgba(255,255,255,1);}


  	@media only screen and (max-width: 1023px) {
  		.b2v-box {width:80%;position:relative;box-shadow:none; margin:0 auto;top:20px;right:none;}
  	}
  	@media only screen and (max-width: 1023px) {
  		.why-txt{ text-align:center; border-width:1px 1px 1px 1px; }
  		.bor-right {border-right: 1px solid #fff;}
  		.bor-bottom {border-bottom: 1px solid #fff;}
  		.Forgot{cursor: pointer;}
  	}
  </style>

  <!-- ==================================================== -->
  <!-- dmt script start  -->
  <!-- ====================================================== -->
  <script type="text/javascript">

  	function removeSession(userName){
  		document.login.userId.value=userName;
  		document.login.action= "/RemoveSesion";
  		document.login.submit();
  	}

  	$(function(){
  		$("#forgetPwd input[type='password']").val('')
  	})

  	function sendOTP(aggid){
  		var inp = $(mobileNo).val();
  		if(inp.match(/^[6789][0-9]{9}$/)){
  			$.ajax({
  				method:'Post',
  				url:'SendAgentOTP',
  				data:"walletBean.mobileno="+inp+"&walletBean.aggreatorid="+aggid,
  				success:function(result){
  					console.log(result.status)
				//if(result.status == "true"){
					$("#error").text("");
					$("#msg").text("OTP sent on you mobile number "+inp);
				/* }else{
					$("#error").text("Someting went wrong.");
				} */
			}
		})
  		}else{
  			$("#msg").text("");
  			$("#error").text("Please enter a valid mobile number.");

  		}

  	}
  	function validatePan(){
  		var inp = $(pan).val().toUpperCase();
  		if(inp.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){
  			$.ajax({
  				method:'Post',
  				url:'ValidatePan',
  				data:"pan="+inp+"&userType=2",
  				success:function(result){
  					console.log(result.status)

  					if(result.status == "true"){
  						$("#error").text("This PAN is already register with us.");
  					}else{
  						$("#error").text("");
  					}
  				}
  			})
  		}else{
  			$("#error").text("Please enter a valid PAN.");
  		}
  	}


  	function checkMobileNumber()
  	{
  		var pass1 = document.getElementById('mobileNo');

  		console.log(mobileNo.value.length);
  		if(mobileNo.value.length!=10){
  			pass1.style.borderColor = "#f05050";
  		}
  		else{
  			pass1.style.borderColor = "#dfdfdf";
  		}

  	}

  	function emailIsValid (email) {

  		var email = document.getElementById('emailId').value;
  		let checkEmail =  /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)

  		if(checkEmail){
  			document.getElementById('emailId').style.borderColor = "#dfdfdf";
  		}
  		else{
  			document.getElementById('emailId').style.borderColor = "#f05050";
  			$("#error").text("Please enter a valid email.");
  		}

  	}


  	function checkOtp(){
  		var otp = document.getElementById('otp').value;
  		console.log(otp);
  		if(otp){
  			document.getElementById('submitButton').disabled = false;

  		}
  		else{
  			document.getElementById('submitButton').disabled = true;

  		}

  	}

  </script>

  <%
  String redirect = request.getParameter("redirect");

  if (redirect == null) {
  redirect = "false";
}
%>

<!-- ==================================================== -->
<!-- dmt script end  -->
<!-- ====================================================== -->




</head>

<body onload="backButtonOverride();">

	
	<%
	String logo = (String) mapResult.get("logopic");
	String banner = (String) mapResult.get("bannerpic");
	String aggId = "OAGG001058";
	%> 

	<div class="wrapper">
		<div class="preloader11"></div>
		<!-- nav bar section start -->
		<div class="navigation">
			<div class="nav-container">
				<div class="brand">
					<a href=""> <img src="atsv/images/logo.png" alt="Logo"> </a>
				</div>

			</div>
		</div>
		<!-- End -->


		<!-- Slick slider start -->
		<div id="slider">
			<div class="home-wrap">
				<!-- home slick -->
				<div id="home-slick">
					<div class="banner banner-1"> <img src="./atsv//images/banner/slider1.jpg" alt=""></div>

					<div class="banner banner-1"> <img src="./atsv/images/banner/slider2.jpg" alt=""></div>

					<div class="banner banner-1"> <img src="./atsv/images/banner/slider3.jpg" alt=""></div>

				</div>
				<!-- /home slick -->
			</div>
		</div>
		<!-- End -->


		<!-- loging section start !-->
		<div class="b2v-box">
			<!-- login section start !-->
			<div  id="Login_Div" class="login_form inner_page">
				<div class="heading">
					<h3 class="text-center">Login to your account</h3>

				</div>
				<!-- login form atsv  -->



				<s:form name=" " action="UserLogin" method="POST"
				onselectstart="return false" oncontextmenu="return false;">
				<s:token />

				<div>
					<font color="red"><s:actionerror escape="false" /></font> 
					<font color="blue"><s:actionmessage /></font>
				</div>

				<s:hidden name="totalAttempt" />
				<s:hidden name="oldUser" />

				<table style="width: 100%;">
					<tr>
						<td>  
							<s:textfield name="user.userId" type="text" id="userId"  placeholder="User Id" autocomplete="off" class="form-control" required="required"  />  								
						</td>
					</tr>
					<tr>
						<td> 
							<s:textfield name="user.password" type="password" id="Login_user_password" placeholder="Password" autocomplete="off" class="form-control" required="required" onkeypress="passCheck()" maxlength="32" /> 
						</td>
					</tr>

				</table>



				<br />

				<div class="rederror" id="error3"></div>
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 

				<tr>
					<td align="left" height="60" valign="middle" style="width:100%;">
						<s:fielderror class="redvalid">
						<s:param>userId</s:param>
						<s:param>Login_user_password</s:param> 
							<!-- <span class="margin2"> 
								<input type="checkbox" checked="checked" class="term-conditions termAndCondition">I agree to usage 
								<a href="#" data-toggle="modal" data-target="#termCondition">Terms and Conditions</a>
							</span> -->

							<div class="form-group custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="exampleCheck3">
								<label class="custom-control-label" for="exampleCheck3">Remember me</label>
								<a class="tdu btn-fpswd float-right Forgot" onclick="Forgot()" style="cursor: pointer;">Forgot Password?</a>
							</div>

							<span class="errorMsg termAndConditionErr"></span> 
						</s:fielderror> 
						<s:submit class="btn btn-log btn-block btn-thm2" key="submit" value="Login" onclick="submitVaForm('#login-form')"></s:submit>
					</td>
				</tr>

				<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
					<tr> 
						<td align="center" valign="middle">
							<p class="text-center">Don't have an account? 
								<a class="text-thm" onclick="Singup()">Sign Up!</a>
							</p>
						</td>
					</tr>
				</table>

			</s:form>





			<!-- login form atsv end  -->

		</div>
		<!-- login section end !-->   

		<!-- Register section start !-->
		<div id="Registration_Div" class="sign_up_form inner_page"  style="display:none;" >
			<div class="heading">
				<h3 class="text-center">Sign Up <small>Enter your details</small> </h3>
				<p class="text-center">You have an account? <a class="text-thm" onclick="Login()" >Login here!</a></p>
			</div>
			<div class="details">
				
				<s:form name=" " action="ValidateAgentOTP" method="POST"
				onselectstart="return false" oncontextmenu="return false;">
				<s:token />

				<div>
					<font color="red"><span id="error"></span><s:actionerror escape="false" /></font> 
					<font color="blue"><span id="msg"></span><s:actionmessage /></font>
				</div>
				<input	 name="walletBean.aggreatorid"  id="aggid" type="hidden" value="<%=aggId %>" />
				<table  width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
					<tr>
						<td colspan="2">  
							<input name="walletBean.name" class="form-control" value="<s:property value='%{walletBean.name}'/>" type="text" id="fullName"   placeholder="Full Name" autocomplete="off"  />  								
						</td>
					</tr>
					<tr>
						<td colspan="2"> 
							<input name="walletBean.emailid" value="<s:property value='%{walletBean.emailid}'/>" type="email" id="emailId" placeholder="Email Id" autocomplete="off" class="form-control"   maxlength="32" /> 
						</td>
					</tr>
					<tr>
						<td>  
							<input name="walletBean.pan"  value="<s:property value='%{walletBean.pan}'/>" type="text" id="pan"  placeholder="Pan No" autocomplete="off"   class="form-control" onblur="validatePan()"/>  								
						</td>
						<td> 
							<input name="walletBean.mobileno" value="<s:property value='%{walletBean.mobileno}'/>" type="number" id="mobileNo" placeholder="Mobile No" autocomplete="off" class="form-control" required="required" title="Mobile number must have 10 digits and should start with 7,8 or 9."  maxlength="10"  pattern="[789][0-9]{9}" onblur="sendOTP('<%=aggId %>')"   required/> 
						</td>
					</tr>

					<tr>
						<td> 
							<input name="walletBean.otp" type="text" id="otp" placeholder="OTP" autocomplete="off" class="form-control" maxlength="32"  onkeyup="checkOtp()" /> 
						</td>
						<td style="padding-left: 30px; padding-top: 8px; font-weight: bold;"> 
							<a href="#" onclick="sendOTP('<%=aggId %>')">Resend OTP</a>
						</td>
					</tr>

				</table>

				<br />

				<div class="rederror" id="error3"></div>
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 

				<tr>
					<td align="left" height="60" valign="middle" style="width:100%;">
						<s:fielderror class="redvalid">
						<s:param>userId</s:param>
						<s:param>Login_user_password</s:param> 
		<!-- 				<span class="margin2"> 
							<input type="checkbox" checked="checked" class="term-conditions termAndCondition">I agree to usage 
							<a href="#" data-toggle="modal" data-target="#termConditionAob">Terms and Conditions</a>
						</span> -->
						<span class="errorMsg termAndConditionErr"></span> 
					</s:fielderror> 
					<s:submit class="btn btn-log btn-block btn-thm2" key="submit" value="Sign Up" onclick="submitVaForm('#login-form')" id="submitButton" disabled="true"></s:submit>
				</td>
			</tr>

			<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
				<tr> 
					<td align="center" valign="middle">
						<s:a class="text-primary fw-500" action="UserHome">Already User? Login</s:a>
					</td>
				</tr>
			</table>

		</s:form>
	</div>
</div>
<!-- Register section end !-->

<!-- Forget password section start !-->
<div id="Forgetpassword_Div" class="login_form inner_page" style="display:none;" >
	<div class="heading">
		<h3 class="text-center">Recover Your Password Here</h3>								
	</div>
	<font color="red">
		<s:actionerror></s:actionerror>
	</font>
	<form action="ForgotPasswordOTP" method="POST" id="forget-pwd1" class="login-form" name="fpo"  autocomplete="off">

		<label class="sr-only" for="form-username">Username</label>
		<input type="text" name="user.userId" placeholder="User Id" class="form-control" required="required" id="u">
		<%-- <s:textfield name="user.userName" cssClass="form-control" id="form-username"  maxlength="20"/> --%>

	</form>

	<button  onclick="submitVaForm('#forget-pwd1',this)"  name="FrgtPwd" class="btn btn-log btn-block btn-thm2"> Submit</button>
	<p class="text-center">Don't have an account? 
		<a class="text-thm" onclick="Singup()">Sign Up!</a>
	</p>
</div>
<!-- Forget password section end !-->  


<!-- otp section start !-->
<div id="Otp_Div" class="login_form inner_page" style="display:none;" >

	<s:form action="forgetPasswordConfirm" method="POST" id="forgetPwd" class="login-form" autocomplete="off">
	<input type="passoword" style="display:none">

	<!-- <form role="form" action="" method="post" class="login-form"> -->
		<div class="form-group text-left">

			<font color="red"><s:actionerror></s:actionerror></font>
			<font color="blue"><s:actionmessage/></font>

		</div>
		<div class="form-group">
			<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
			<input type="hidden" name="user.usertype" class="form-control" value=" <s:property value='%{user.usertype}'/>"/>
			<input type="hidden" name="user.userId"  autocomplete="new-password" autocomplete="off" placeholder="Username" value="<s:property value='%{user.userId}'/>" class="form-control" id="userid" >
			<input type="password" name="user.password"  autocomplete="new-password" autocomplete="off" placeholder="Enter New Password" value="<s:property value='%{user.otp}'/>" class="form-control" required="required" >
			<input type="password" name="user.confirmPassword" autocomplete="off" placeholder="Enter Confirm New Password" value="<s:property value='%{user.otp}'/>" class="form-control" required="required"  >
			<input type="text" autocomplete="new-password"  autocomplete="off" maxlength="6" name="user.otp" placeholder="Enter OTP" value="<s:property value='%{user.otp}'/>" class="form-control" required="required" id="otp" >
			
			<div class="text-right"><a href="javascript:void(0)" onclick="document.getElementById('resendOtp').submit()" class="resendOtp">Resend OTP</a></div>
		</div>





	</s:form>

	<button  style="width:100%" name="login" class="submit" value="Login" onclick="submitVaForm('#forgetPwd',this)" class="btn btn-log btn-block btn-thm2">Submit</button>

	<p class="text-center">Don't have an account? 
		<a class="text-thm" onclick="Singup()">Sign Up!</a>
	</p>

	<div style="display:none">
		<s:form action="OtpResend" method="POST" cssClass="login-form" id="resendOtp">
		<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
		<input type="hidden" name="user.userstatus" value="fpo"/>
		<input type="hidden" name="user.userId" value="<s:property value='%{user.userId}'/>"/>
		<input type="submit" class="login-form" style="background: #ff6e26;border: none;padding: 4px 7px;color: #FFF;font-size: 12px;" value="Resend OTP"/>
	</s:form>
</div>
</div>
<!-- Forget password section end !-->  

</div>	
<!-- End !-->

<section class="home6_about pt35 bgc-f9">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="about_box_home6">
					<div class="details">

						<p> 
							ATSV is an initiative from ATSV E-SOLUTION INDIA PVT LTD company. It started its operations in the year 2017 and has made its presence known in India with over 8,000 active retail touch-points across 630 districts and 27 states.
						</p>	

						<p> 
							ATSV was started with the idea of bridging the gap between the untouched market segments and the service providers. Using ATSV's intelligent electronic transaction processing platform, the consumer can do transactions by visiting retail touch points and can pay digitally as well as through cash.
						</p>

						<p> 
							We at ATSV believe in the business philosophy of "Making Life Simple" and provide every possible opportunity to our retailers and distributors to earn with minimum investment and maximum return.
						</p>

						<p> 
							Under this platform, ATSV offers a plethora of utility services to its consumers in terms of AEPS, Domestic Money Remittance, Mobile and DTH recharges, Rail, Air and Bus reservation and Utility Bill collections, Digital Wallets, CMS (Cash Management Service) collection, Cash withdrawal points and Assisted e-commerce. ATSV also facilitate SME's loans. Approx. over 50 million transactions are done using this platform every year through our 8,000 active retail touch points spread across India.
						</p>

					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="about_box_home6">
					<div class="thumb"><img class="img-fluid img-rounded" src="atsv/images/1.jpg" alt="1.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="divider_home1 bg-img2 parallax" data-stellar-background-ratio="0.3">
	<div class="container" style="width: 80%;">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="divider-one">
					<h1 class="color-white text-uppercase">Why ATSV E-SOLUTION INDIA PVT LTD </h1>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 why-txt">
				<div class="row">
					<div class="col-md-4 col-xs-12"> <i class="fa fa-university icon"></i> </div>
					<div class="col-md-8 col-xs-12 noPadding ">
						<p class="txt-f25">Expertise team</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 why-txt">
				<div class="row">
					<div class="col-md-4 col-xs-12"> <i class="fa fa-clock-o icon"></i> </div>
					<div class="col-md-8 col-xs-12 noPadding ">
						<p class="txt-f25">24/7 CUSTOMER SUPPORT</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 why-txt bor-right">
				<div class="row">
					<div class="col-md-4 col-xs-12"><i class="fa fa-user icon"></i></div>
					<div class="col-md-8 col-xs-12 noPadding">
						<p class="txt-f25">EASY BOOKING </p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 why-txt bor-bottom">
				<div class="row">
					<div class="col-md-4 col-xs-12"><i class="fa fa-check-circle icon"></i></div>
					<div class="col-md-8 col-xs-12 noPadding">
						<p class="txt-f25">BEST PRICE</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 why-txt bor-bottom">
				<div class="row">
					<div class="col-md-4 col-xs-12"><i class="fa fa-bar-chart icon"></i></div>
					<div class="col-md-8 col-xs-12 noPadding">
						<p class="txt-f25">Quality
						</div>
					</div>
				</div>
				<div class="col-md-4 col-xs-4 why-txt bor-right bor-bottom">
					<div class="row">
						<div class="col-md-4 col-xs-12"><i class="fa fa-calendar-check-o icon"></i></div>
						<div class="col-md-8 col-xs-12 noPadding">
							<p class="txt-f25">Proceed</p>
						</div>
					</div>
				</div>


			</div>
		</div>
	</section>

	<section id="our-courses" class="our-courses pt50 pt650-992">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Our Services</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/1.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>AEPS (Aadhar Enable Payment Services) </h5>
								<p>Aadhaar Enabled Payment System Cash Withdrawal Cash Deposit, Balance Enquiry, Aadhaar to Aadhaar Fund Transfer Mini Statement Best Finger Detection</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/2.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Domestic Money Transfer</h5>
								<p>Anytime, anywhere,Send and receive money, shop, trade and play Get easy access to your winnings,Low, transparent fees Instant money transfers and withdrawals.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/3.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Bill Payments</h5>
								<p>Mobile, DTH and Data Card Recharge Pay Electricity Bill, Gas Bill & Water Bill instantly in just a few clicks Enjoy uncomplicated utility bill online payment service </p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/4.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Tickets (Rail/Flight/Bus/Hotels)</h5>
								<p>Start your own business with AePS, DMT, PAN card, Insurance, Flight,Hotel,Train,Bus , Prepaid Mobile ,DTH , Data card , Postpaid Bill payment, Loan services, and BBPS...</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/5.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Fast Tag/Pan Card</h5>
								<p>Avoid long queues at the toll lanes and move quickly through them with the help of FASTag.<br> <br><br> </p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="img_hvr_box" style="background-image: url(atsv/images/courses/6.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>White Label ATM'S</h5>
								<p>Get your agency online with Flight Booking/Hotel Booking/Money Transfer/IRCTC Booking..<br> <br><br></p>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</section>


	<!-- footer section start -->
	<section class="footer_one" style="display:none">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-3">
					<div class="footer_contact_widget"> 
						<p>C-Shanti Shoping Center Mira Road Mumbai</p>
						<p>Maharashtra, 400068</p>
						<p><b>Call Us</b> - 0000 00 0000 / 0000 00 0000</p>
						<p> <b>Mail Id:</b> sunilsir171272@gmail.com</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
					<div class="footer_company_widget">
						<ul class="list-unstyled">
							<li><a href="#">Book & Notes</a></li>
							<li><a href="#">Director Speech</a></li>
							<li><a href="#">Interview</a></li>
							<li><a href="#">New Batch Schedule </a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
					<div class="footer_program_widget">
						<ul class="list-unstyled">
							<li><a href="#">Syllabus</a></li>
							<li><a href="#">Fee Structure & Mode</a></li>
							<li><a href="#">Gallery</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
					<div class="footer_support_widget">
						<ul class="list-unstyled">
							<li><a href="#">Online Registration</a></li>
							<li><a href="#">Online Classes</a></li>
							<li><a href="#">Test Series</a></li>
							<li><a href="#">Online Result</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-md-3 col-lg-3">
					<div class="footer_apps_widget">
						<div class="app_grid">
							<div class="footer_social_widget home6 mt15" style="text-align: left;">
								<ul>
									<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>

									<li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a>
									</li>

									<li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End -->

	<section class="footer_bottom_area pt10 pb10">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="copyright-widget text-center">
						<p>Copyright ATSV � 2020. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<a class="scrollToHome" href="#"><i class="flaticon-up-arrow-1"></i></a>
</div>
<!-- Wrapper End -->

<script type="text/javascript" src="atsv/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="atsv/js/bootstrap.min.js"></script>
<script type="text/javascript" src="atsv/js/bootstrap-select.min.js"></script>

<script src="atsv/js/theme.js"></script>
<script src="atsv/js/slick.min.js"></script>

<script>
	$(document).ready(function () {
		$('#home-slick').slick({
			autoplay: true,
			infinite: true,
			speed: 700,
			arrows: true
		});


	})
	function Singup(){
		$('#Registration_Div').show();
		$('#Login_Div').hide();
		$('#Forgetpassword_Div').hide();
		$('#Otp_Div').hide();
	}	
	function Forgot(){
		$('#Registration_Div').hide();
		$('#Login_Div').hide();
		$('#Forgetpassword_Div').show();
		$('#Otp_Div').hide();
	}	

	function Login(){
		$('#Registration_Div').hide();
		$('#Login_Div').show();
		$('#Forgetpassword_Div').hide();
		$('#Otp_Div').hide();
	}	
	function otp(){
		$('#Registration_Div').hide();
		$('#Login_Div').hide();
		$('#Forgetpassword_Div').hide();
		$('#Otp_Div').show();
	}	


</script>

    <!-- <script src="./js/jquery.min.js"></script>
    	<script src="./js/jquery.backstretch.min.js"></script> -->
    	<script src="./js/scripts.js"></script>
    	<script src="./js/validateWallet.js"></script>


    </body>


<%} %>
</html>
<%} %>
