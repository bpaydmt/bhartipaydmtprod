/*dmt.controller("importSenderCtrl",function($http,$rootScope){
	is = this;
	is.info = {};
	is.searchImportSender = function() {
		//alert($rootScope.LoginSender.mobileNo)
		is.info['mobileNo'] = $rootScope.LoginSender.mobileNo;
		console.log(JSON.stringify(is.info))
		$http({	
			 method:"POST",
			 url:"GetBeneficiaryListForImport",
			  data    :'data='+ JSON.stringify(is.info), //forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		 }).then(function(res){
			 console.log(res)
		 },function(){
			 console.log(res)
		 })
		
	}
});*/

dmt.controller("importSenderCtrl",function($http,$rootScope,$location,$timeout){ debugger
	var is = this;
	is.info = {};
	is.listData = false;
	is.error = false;
	is.importList = [];
	is.searchImportSender = function() {
		//alert($rootScope.LoginSender.mobileNo)
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		is.info['mobileNo'] = $rootScope.LoginSender.mobileNo;
		console.log(JSON.stringify(is.info))
		$http({	
			 method:"POST",
			 url:"GetBeneficiaryListForImportBK2",
			  data    :'data='+ JSON.stringify(is.info), //forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		 }).then(function(res){
			 console.log(res)
			if(res.data.statusCode == "1000"){
			is.importList =  res.data;
			 is.listData = true;
			 is.error = false;
			
			}else{
				 is.errorMsg = res.data.statusDesc;
				 is.error = true;
			}
		 },function(){
			 console.log(res)
		 });
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });

		
	}
	is.importBene = function(benId){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		$http({
			method:"POST",
			url:"CopyBeneficiaryBK",
			 data    :'benId='+ benId, //forms user object
	          headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res){ 
			console.log(res)
			if(res.data.statusCode == "1000"){
				 $rootScope.modelBoxContent = "jspbk2/app/components/importSender/beneImportSuccess.html" + $rootScope.version;
				 $('#myModal').modal('show');					
					 $location.path('/regUser');
					 console.log($rootScope.LoginSender.id)
					 res.data.senderId = $rootScope.LoginSender.id
					 $rootScope.LoginSender.beneficiaryList.push(res.data)
					 
					
			}else{
				$rootScope.rootErrorMsg  = res.data.statusDesc
				$rootScope.modelBoxContent = "jspbk2/app/components/error/errorMsg.html" + $rootScope.version;
			
				 $('#myModal').modal('show');
			}
			
		});
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });

	}
});