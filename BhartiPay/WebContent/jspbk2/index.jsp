<%@page import="java.util.Map"%>

<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String  theams=mapResult.get("themes");

String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");

String version = "?i=1000tuesdrrra69";
String sessionid=(String)session.getAttribute("sessionid");

%>

<!DOCTYPE html>
<html>
<head>
<title><%=mapResult.get("caption") %></title>
 <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<%=mapResult.get("caption") %>">
    <meta name="author" content="<%=mapResult.get("caption") %>">   
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
<!--     <link rel="shortcut icon" href="./images/favIcon/faviconbhartipay.png">
 -->
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="shortcut icon" href="./images/<%=favicon%>">
 
<link rel="stylesheet" type="text/css" href="jspbk2/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">
<!-- <link rel="stylesheet" type="text/css" href="jspbk2/mainStyle.css">
 --><link rel="stylesheet" type="text/css" href="../../css/charisma-app.css">

<link href="./css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="jspbk2/assets/cssLibs/chosen.css" rel="stylesheet" type="text/css">
							<script type="text/javascript" src="jspbk2/assets/jsLibs/angular.min.js"></script>
							<script type="text/javascript" src="jspbk2/assets/jsLibs/angular-route.js"></script>

<script type="text/javascript" src="jspbk2/assets/jsLibs/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="jspbk2/assets/jsLibs/angu-fixed-header-table.js"></script>
<script type="text/javascript"  src="jspbk2/assets/jsLibs/dirPagination.js"></script>
<script type="text/javascript" src="jspbk2/assets/jsLibs/pdfmake.min.js"></script>
<script type="text/javascript" src="jspbk2/assets/jsLibs/vfs_fonts.js"></script>
<script type="text/javascript" src="jspbk2/assets/cssLibs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="jspbk2/app/dmt.module.js<%=version %>"></script>
							<script type="text/javascript" src="jspbk2/app/dmt.routes.js<%=version %>"></script>
<script type="text/javascript" src="jspbk2/app/services/appServices.js<%=version %>"></script>  
<script type="text/javascript" src="jspbk2/app/components/newUser/senderFormCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk2/app/components/error/errorMsgCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk2/app/components/addBeneficiary/addBeneficiaryCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk2/app/components/regUser/regUserCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk2/app/components/importSender/importSenderCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk2/app/components/favGrid/favListCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk2/app/components/reports/reportCtrl.js<%=version %>"></script> 


<script  type="text/javascript" src="./js/datepicker.js"></script>
<script  type="text/javascript"  src="./js/datepicker.en.js"></script>
<script  type="text/javascript"  src="jspbk2/assets/jsLibs/Blob.min.js"></script>
<script  type="text/javascript" src="jspbk2/assets/jsLibs/FileSaver.min.js"></script>
<script  type="text/javascript"  src="jspbk2/assets/jsLibs/tableexport.min.js"></script>
<script  type="text/javascript"  src="jspbk2/assets/jsLibs/alasql.min.js"></script>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.7/xlsx.core.min.js"></script>
<script  type="text/javascript" src="jspbk2/assets/jsLibs/chosen.jquery.js"></script> 

<script src="./js/Newtheme-column-visibility.js" type="text/javascript"></script>

 <style> 
 #bankNames_chosen{
 width:100%!important
 }
.modal-lg {
    width: 53% !important;
}
 @media (max-width:768px){
	 .modal-lg {
         width: 95% !important;
		}
 }
.modal-body {
    position: relative;
    padding: 10px 20px 10px 20px !important;
}

.modal-footer {
    padding: 10px 10px 10px 10px !important;
}
 </style>

</head>

<body data-ng-app="oxyModule" id="bank-mod" >

   <!-- topbar starts -->
<jsp:include page="/jsp/header.jsp"></jsp:include>
    <!-- topbar ends -->        
        <!-- left menu starts -->
<jsp:include page="/jsp/mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

	<div id="main" role="main"> 
	    <div id="content">  
			<div class=" row">

				<div class="box" data-ng-controller="mainCtrl as main" data-ng-init="main.getTheUserData()"  data-ng-click="main.getAgentSenderBalance()">
					<!--1-->
					<div class="col-md-12 midSection" > 
						<!--2-->

						
						<div class=""  data-ng-view>
							
<div class="col-md-12">

							<div class="loader"></div>
						</div> 
						</div>

						<!--3-->
									<div class="modal fade print-enable" id="myModal" role="dialog" style="display: none;">
										    <div class="modal-dialog  modal-lg">
										    
										      <!-- Modal content-->
										      <div class="modal-content" >
										        <div class="modal-header">
										       <h4 class="modal-title">{{$root.popupHeading}}</h4> 
										          <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
										            </div>
										        <div class="modal-body" id="modelboxContent">
										      
												<div data-ng-include="$root.modelBoxContent"></div>
										        </div>
										     
										      </div>
										      
										    </div>
										  </div>
						
						
					</div> 

				</div>

			</div>  
        </div>
    </div> 

<script   type="text/javascript" type="text/javascript">
    $(function(){
	SESSIONID = '<%=sessionid %>'
		$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			
			if(data.sessionId != SESSIONID){

				window.close();
				window.location.replace("/UserHome");
			}
		 })
		 })
		 
    })
    window.onpopstate = function (e) { window.history.forward(1); } 
</script>

</div> 
<jsp:include page="/jsp/footer.jsp"></jsp:include></body>
</html> 