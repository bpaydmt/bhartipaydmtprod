<%@page import="java.util.Map"%>
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String  theams=mapResult.get("themes");
String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");

String version = "?i=1001fri34refdddsd03";

String sessionid=(String)session.getAttribute("sessionid");

int kycpopup=0;
String message="Something went wrong please try again later.";
String statusCode=(String) request.getAttribute("statuscode");
if(statusCode!=null&&statusCode.equalsIgnoreCase("success")){
	kycpopup=1;
	message="Sender KYC uploaded.";
}
if(statusCode!=null&&statusCode.equalsIgnoreCase("fail")){
	kycpopup=2;
	message="Something went wrong please try again later.";
}

%>

 <!DOCTYPE html>
<html>
<head >
 
 <title><%=mapResult.get("caption") %></title>
 <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=<%=mapResult.get("caption") %>>
    <meta name="author" content="<%=mapResult.get("caption") %>">   
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
    <link rel="shortcut icon" href="jspas/assets/img/<%=favicon%>">
 
 <link rel="stylesheet" type="text/css" href="jspas/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">

 <link rel="stylesheet" type="text/css" href="jspas/mainStyle.css">
 	<link href="./css/datepicker.min.css" rel="stylesheet" type="text/css">
 	<link href="jspas/assets/cssLibs/chosen.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="jspas/assets/jsLibs/angular.min.js"></script>  

<script type="text/javascript" src="jspas/assets/jsLibs/angular-route.js"></script>
<script type="text/javascript" src="jspas/assets/jsLibs/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="jspas/assets/jsLibs/angu-fixed-header-table.js"></script>
<script  type="text/javascript"  src="jspas/assets/jsLibs/dirPagination.js"></script>
<script type="text/javascript" src="jspas/assets/jsLibs/pdfmake.min.js"></script>
<script type="text/javascript" src="jspas/assets/jsLibs/vfs_fonts.js"></script>
<script type="text/javascript" src="jspas/assets/cssLibs/bootstrap-3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="jspas/app/dmt.module.js<%=version %>"></script>
<script type="text/javascript" src="jspas/app/dmt.routes.js<%=version %>"></script>
<script type="text/javascript" src="jspas/app/services/appServices.js<%=version %>"></script>  
<script type="text/javascript" src="jspas/app/components/newUser/senderFormCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspas/app/components/error/errorMsgCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspas/app/components/addBeneficiary/addBeneficiaryCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspas/app/components/regUser/regUserCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspas/app/components/importSender/importSenderCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspas/app/components/favGrid/favListCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspas/app/components/reports/reportCtrl.js<%=version %>"></script> 
 <script  type="text/javascript" src="./js/datepicker.js"></script>
<script  type="text/javascript"  src="./js/datepicker.en.js"></script>
<script  type="text/javascript"  src="jspas/assets/jsLibs/Blob.min.js"></script>
<script  type="text/javascript" src="jspas/assets/jsLibs/FileSaver.min.js"></script>
<script  type="text/javascript"  src="jspas/assets/jsLibs/tableexport.min.js"></script>
<script  type="text/javascript"  src="jspas/assets/jsLibs/alasql.min.js"></script>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.7/xlsx.core.min.js"></script>
<script type="text/javascript" src="jspas/assets/jsLibs/chosen.jquery.js"></script>


 
 <style> #bankNames_chosen{width:100%!important}</style> 
</head>
<body data-ng-app="oxyModule" >
<div class="" data-ng-controller="mainCtrl as main" data-ng-init="main.getTheUserData()" data-ng-click="main.getAgentSenderBalance()">
<header >
 <div class="innerHeader">
  <div class="container">


   <div class="logo pull-left" style="background:url({{$root.loginUserData.logo}}) no-repeat center center/100%"></div>
 
   <div class="header-right pull-right">
   <div class="user-info ">
   <!-- Begin TranslateThis Button -->
    
	
     
     <span class="balance user-name" >{{$root.loginUserData.finalBalance | currency: $root.loginUserData.countrycurrency + "  " }}</span>
     
     <span class="user-pic"><img data-ng-src="data:image/gif;base64,{{$root.loginUserData.userImg}}" alt="{{$root.loginUserData.name}}"></span><span class="glyphicon glyphicon-triangle-bottom"></span>
    <span class="user-name" >{{$root.loginUserData.name}}</span>
     <!-- <span class="user-name" ><a href="Home.action">Home</a></span> -->
    </div>
    
    
   </div>
  </div>
 </div>
 
</header>
<nav class="navbar navbar-inverse">
  <div class="container">
    
  
    <ul class="nav navbar-nav navbar-right">

      <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" >Reports<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#/reports">Ledger</a></li>
         
        </ul>
      </li>
     
    </ul>
  
  </div>
</nav>

<!--     <div class="modal " id="transactionProcsses"  style="z-index:1111111;display:none;background:rgba(255, 255, 255, 0.99)!important">
 	<h2>Your transaction is under process. Please don't close or don't refresh the window. </h2>
 
  </div> -->
<div class="container midSection" >

 <div class="row mainRow"  data-ng-view><div class="loader"></div></div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog  modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
       <h4 class="modal-title">{{$root.popupHeading}}</h4> 
          <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        <div class="modal-body" id="modelboxContent">
      
		<div data-ng-include="$root.modelBoxContent"></div>
        </div>
     
      </div>
      
    </div>
  </div>

	

 
 
    <%if(request.getParameter("kycForm")!=null&&request.getParameter("kycForm").equals("true") || kycpopup != 0 ){ %>
   <div class="modal fade in" id="myModalTerms" role="dialog" style="display:none;background:rgba(255, 255, 255, 0.99)!important">
   <%  if(kycpopup==1){


	%>
	<script>
	$(function(){
		
	alert("Sender KYC uploaded.")
	})
	
	</script>
<%
	}else if(kycpopup==2) {
		%>
	<script>
	$(function(){
		
	alert("Something went wrong please try again later.")
	});
	
	</script>
		<%
	}%>
   <%}else if(request.getParameter("kycForm")!=null&&request.getParameter("kycForm").equals("fail")) {%>
   <div class="modal fade in" id="myModalTerms" role="dialog" style="display:none;background:rgba(255, 255, 255, 0.99)!important">
   <%}else{%>
   <div class="modal fade in" id="myModalTerms" role="dialog" style="display:block;background:rgba(255, 255, 255, 0.99)!important">
   <%} %>  
    
    <div class="modal-dialog  modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
       <h1 class="modal-title">Term & Condition</h1> 
       
            </div>
        <div class="modal-body" id="modelboxContent">
  
		<div data-ng-include="'jspas/app/components/termAndConditions/termAndCondition.html'+$root.version"></div>
        </div>
         <div class="modal-footer">
         <a  class="btn btn-default" href="#" onclick="window.close();">Disagree</a>
        <button type="button" class="btn btn-primary" onclick="closePopUp3()" data-dismiss="modal">I Agree</button>
      </div>
     	
      </div>
      
    </div>
  </div>
  
  
</div>

<!-- <script src="jquery.js"></script> -->

<script   type="text/javascript" type="text/javascript">

$(function(){
	SESSIONID = '<%=sessionid %>'

	
		$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			
			if(data.sessionId != SESSIONID){

				window.close();
				

			     window.location.replace("/UserHome");

			}
		 })
		 })
		 
})

window.onpopstate = function (e) { window.history.forward(1); }

function closePopUp3(){

	$('#myModalTerms').removeClass('in')
	
	setTimeout(function(){
		$('#myModalTerms').hide();
	},300)
}
  </script>

</div>
<!-- <div class="splash-screen " data-ng-hide="$root.intialDataStatus" >
 <div class="logo"></div>
</div> -->
<footer class="text-center">&copy; 2018 <%=mapResult.get("appname")%> All rights reserved. Powered by Appnit Technologies. </footer>


</body>
</html> 