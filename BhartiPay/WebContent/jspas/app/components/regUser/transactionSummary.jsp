<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.Map"%>
<%
Date date=new Date();
SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
String currentDate=sdf.format(date);

Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");

String merchantName = mapResult.get("appname");
	String domainName = mapResult.get("domainName");
	%>
<div  >
<div id="transLogo" style="display:none;margin:20px 0 20px">
<img ng-src="{{$root.loginUserData.logo}}" style="width:150px" id="" />

<span class="dateStapm"><strong>RECEIPT DATE:- </strong><%=currentDate %></span>
</div>



<div id="transSummary">

      <h2>Transaction Summary</h2>
  <table class="table table-bordered table-striped">
   
    <tbody>
      <tr>
        <td><strong>Sender Mobile</strong></td>
        <td>{{$root.transSummary.senderMobile}}</td>
          <td><strong>Beneficiary Name</strong></td>
        <td>{{$root.transSummary.beneficiaryName}}</td>
      </tr>
   
     
    
       <tr>
          <td><strong>Beneficiary IFSC</strong></td>
        <td>{{$root.transSummary.beneficiaryIFSC}}</td>
          <td><strong>Beneficiary Account No.</strong></td>
        <td>{{$root.transSummary.beneficiaryAccNo}}</td>
     </tr>
        <tr>
         <td><strong>Amount</strong></td>
        <td>{{$root.transSummary.amount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
          <td><strong>Charges</strong></td>
        <td>{{$root.transSummary.charges}}</td>
   
      </tr>
        <tr>
      <tr>
         <td><strong>Transaction Mode</strong></td>
        <td>{{$root.transSummary.mode}}</td>
          <td>&nbsp;</td>
        <td>&nbsp;</td>
     </tr>
       
  </table>
    <table class="table table-bordered table-striped">
   <thead>
   		<tr>
   			<!--   <th><strong>Beneficiary Account No.</strong></th> -->
        <th>Transaction Number</th>
   			<th>Transaction Type</th>
   			<th>Credit Amount</th>
   			<th>Debit Amount</th>
   			<th>Status</th>
   			<th>Remark</th>
   		</tr>
   
   </thead>
    <tbody>
    $scope.myFilter = function (item) { 
    return item === 'red' || item === 'blue'; 
	};
	
      <tr data-ng-repeat="mudraTrans in $root.transSummary.mudraMoneyTransactionBean  | orderBy:'-'" >
      
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'">{{mudraTrans.id}}</td>
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'">{{mudraTrans.narrartion}}</td>
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'">{{mudraTrans.crAmount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'">{{mudraTrans.drAmount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'"><strong>{{mudraTrans.status}}</strong></td>
        <td ng-if="mudraTrans.narrartion != 'WALLET LOADING'">{{mudraTrans.remark}}</td>
      </tr>  
    
     
       
  </table>
  
<div class="alert alert-success">
  Your transaction has been completed. Reference Number is <strong> {{$root.transSummary.mudraMoneyTransactionBean[0].txnId}}.</strong> Thanks for using <%=merchantName %>.  
</div>
</div>
 <div class="modal-footer">
 
          <button class="btn-primary pull-right btn" onclick="printContent('#transSummary')">Print</button>
 </div>
 </div>
 <style>
 
 .dateStapm{display:none}

 
 
 @media print {
    .btn-primary.pull-right.btn, .mainRow, header, footer, .modal-header,h2 {
       display:none!important
    }
    

#transSummary .table-bordered>tbody>tr>td, #transSummary .table-bordered>tbody>tr>th, #transSummary .table-bordered>tfoot>tr>td, #transSummary .table-bordered>tfoot>tr>th, #transSummary .table-bordered>thead>tr>td, #transSummary .table-bordered>thead>tr>th {
    border: 1px solid #999!important;
    font-size:13px!important;
    padding:10px!important
}
    #transLogo, .dateStapm{display:block!important}
      #transLogo img{  margin: 10px 0 22px auto;}
   #transLogo{    display:none;margin: 20px 0 11px;text-align: center;position: relative;}
    .modal-content{
    box-shadow: none!important;
    border: none;
    }
    .table > thead > tr > th {
    background: #f36d25;
    color: #000 !important;
    font-size:20px;
}
.alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 19px!important;}
  .dateStapm{  font-size: 20px;
    float: right;
    position: absolute;
    right: 0px;
    top: 71px;
    }
    table-bordered>tbody>tr>td{
    	font-size:29px!important
    }
     table th{
    	font-size:18px!important;
    	
    }
}
 
 @page {
   size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 0mm 5mm 5mm 5mm;  
    font-size:29px!important;
   
}
 
 
 </style>