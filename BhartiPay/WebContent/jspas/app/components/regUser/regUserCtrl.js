dmt.controller('regUserCtrl',function($http,$rootScope, $location,$timeout,$interval) {
	var rg =  this;
	rg.senderDetails = 	 	   $rootScope.LoginSender;
	rg.mpinFormSubmit = {};
	rg.counter =  90;
	rg.otpResendLink = false;
	rg.favlink = "Add to Priority";
	rg.isLoading = false;
	rg.updateSenderKYC = function(mobile){ 	
		vm.isProccessing = true;
		$http.get('CheckSession')
	       .then(function(res){ 
	    	   //alert(mobile)
	    	   console.log(res)
	    if(res.data.status == "TRUE"){
	    	 $rootScope.modelBoxContent = "jspas/app/components/favGrid/kycUpdateOption.html"+  $rootScope.version;
	    	 $('#myModal').modal('show');
	    	 
	    	 $timeout(function () {
	    		 $("#senderId").val(mobile)
	    	    }, 500);
	    	
		   
	    }
	       else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });
		};
	rg.uploadPancard = function(){
		
		$http.get('CheckSession')
	       .then(function(res){ 
	    	
	    	   if(res.data.status == "TRUE"){
	    			$rootScope.popupHeadingPanCard = "Upload Pan Card";	
	    			$rootScope.proofType = "panCard";
	    			$rootScope.panCard =true;
	    			$rootScope.fileLabelHeading = "Upload PAN Card:"
	    				$("#panErrMsg").text('')
	            		$("#panCardInp, #panfile").val('');
	    			$("#panCardInp").val('')
	    			$("#panField").show()
	    			$("#heading").text("Upload Pan Card	")
	    			$("#proofType").val("PAN")
	    			$("#panfile").val('');
	    			$("#from60").hide();
	    			$("#panMsg").show();
	    		 
	    		   $rootScope.modelBoxContent =  "jspas/app/components/regUser/uploadPancard.jsp" + $rootScope.version; 
	    		   
		    		
		    		
	    		   $('#myModal').modal('show');		
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	    	   
	       });   
	}
	
	rg.addCounter = function(){
		if(rg.counter != 0){
			rg.counter--
		}else{
			rg.otpResendLink = true;
		}
	}
	rg.regInit =  function(){
		localStorage.removeItem('senderData');
		$('[data-toggle="tooltip"]').tooltip();
		
		console.log($location.path())
	
		if(($rootScope.LoginSender  == '' || $rootScope.LoginSender  ==  'Not Define Yet' || $rootScope.LoginSender  == 'undefined') && $location.path() == "/regUser"){
			  
			$location.path("/")
			$rootScope.modelBoxContent =  "jspas/app/components/error/dmtError.html"; 		 
		   $('#myModal').modal('show');			
	
			
		}else{
		var senderFav = $rootScope.LoginSender.favourite;
		var loggedInAggentId = $rootScope.loginUserData.id
		var  sederFavArr = senderFav.split(",");
		if(sederFavArr.indexOf(loggedInAggentId) !== -1) {
			rg.isFav = true;
			}else{
				rg.isFav = false;
			}
		}
	};

	rg.resendMpin = function(){	
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		   $http({
		     method: 'POST',
		     url: 'DmtOtpResend',
		     data    :'mastBean.mobileNo='+ SENDERMOBILENO ,//forms user object
		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		   })
		    .then(function(res){ 	   
		    	console.log(res)
		    	   if(res.data.status == "true"){
					   $("#newMpinForm .alert").text("OTP has been sent to sender mobile number");
					   $("#newMpinForm .alert").addClass("alert-success");
					   $("#newMpinForm .alert").removeClass("alert-danger");
					   
				   }else{
					   
					   $("#newMpinForm .alert").text(res.data.statusDesc);
					   $("#newMpinForm .alert").addClass("alert-danger");
					   $("#newMpinForm .alert").removeClass("alert-success");
				   }			  	      
		    	rg.otpResendLink = false;
	    		rg.counter = 90;
		  },function(res){
		   console.log(res.statusText)
		  });
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });


		   
		  };
	
		  rg.showMpinPop =  function(){
			  
				$http.get('CheckSession')
			       .then(function(res){ 
			    	
			    	  // alert(2)
			    	   console.log(res)
			    	   if(res.data.status == "TRUE"){
			 rg.mpinForm = {};
			rg.mpinForm.aggreatorId = $rootScope.LoginSender.aggreatorId;
			rg.mpinForm.mobileNo = 	 $rootScope.LoginSender.mobileNo;
			rg.mpinForm.id = 		 $rootScope.LoginSender.id;
			rg.mpinFormSubmit.mpin = ""; 
			rg.mpinFormSubmit.otp = "";
			
	    if($("#mpinPopup").is(":hidden")){
	    	
			
			 $http({
			     method: 'POST',
			     url: "ForgotMPIN",
			     data    :'data='+ JSON.stringify(rg.mpinForm), //forms user object
			           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				 console.log(res)
				 $("#mpinPopup").show();
				   if(res.data.statusCode == 1000){
					   $("#newMpinForm .alert").text("OTP has been sent to sender mobile number");
					   $("#newMpinForm .alert").addClass("alert-success");
					   $("#newMpinForm .alert").removeClass("alert-danger");
					 
					   
				   }else{
					   
					   $("#newMpinForm .alert").text(res.data.statusDesc);
					   $("#newMpinForm .alert").addClass("alert-danger");
					   $("#newMpinForm .alert").removeClass("alert-success");
				   }
				  rg.countDown = $interval(rg.addCounter,1000)
			   },function(res){
				   console.log(res.statusText)
				   
			   });		
		
		}else{
			
			$("#mpinPopup").hide();
		}
	    
			    	   } else{
			    		   alert("Your session has expired. Please login again.")
			    		   window.close();
			    	   }
			       },function(res){
			  		   console.log(res.statusText)
			  		  });


	    
	};
	rg.closePop =  function(){
		$("#mpinPopup").hide();
		rg.otpResendLink = false;
		$interval.cancel(rg.countDown);
		rg.counter = 90;
	}
	$rootScope.$on('$routeChangeStart', function (next, last) {
		
		$interval.cancel(rg.countDown);  
		});
	rg.addFav = function(){
		rg.favlink = "Loading...."
			rg.isLoading = true;
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		    	   if(res.data.status == "TRUE"){
			var addFavObj  = {};
			addFavObj.agentId = 	 $rootScope.loginUserData.id;
			addFavObj.senderId = 	 $rootScope.LoginSender.id;
			addFavObj.mobileNo = 	 $rootScope.LoginSender.mobileNo;
			console.log(addFavObj)
		     $http({
				     method: 'POST',
				     url: "SetFavourite",
				     data    :'data='+ JSON.stringify(addFavObj),  //forms user object
				           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				   }).then(function(res){
					   if(res.data.statusCode == "1000"){
						   $rootScope.favList = res.data.senderFavouriteList;
						   console.log(res)
						   rg.favlink = "Add to Priority"
						   rg.isLoading = false;
						   $rootScope.modelBoxContent =  "jspas/app/components/favGrid/addFavSuccessMsg.html" + $rootScope.version;
						   rg.isFav = true;
						  
					   }else{
						   $rootScope.modelBoxContent =  "jspas/app/components/error/errorMsg.html" + $rootScope.version; 
					   }
					 
					   $('#myModal').modal('show');
					   console.log(res)
				   },function(res){
					   console.log(res.statusText)
					   
				});		
		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });

		

	};
	
	rg.summitNewMpin =  function(){

		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){

			rg.mpinFormSubmit.aggreatorId = $rootScope.LoginSender.aggreatorId;
			rg.mpinFormSubmit.mobileNo = 	 $rootScope.LoginSender.mobileNo;
			rg.mpinFormSubmit.id = 		 $rootScope.LoginSender.id;
			
			
			console.log( rg.mpinFormSubmit)
	     $http({
		     method: 'POST',
		     url: "UpdateMPIN",
		     data    :'data='+ JSON.stringify(rg.mpinFormSubmit), //forms user object
		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		   }).then(function(res){
			   console.log(res.data)
			   if(res.data.statusCode == 1000){
				   $("#newMpinForm .alert").text("New mpin has been set!!");
				   $("#newMpinForm .alert").addClass("alert-success");
				   $("#newMpinForm .alert").removeClass("alert-danger");
				   $timeout(function () {
					   rg.closePop()
					   rg.mpinFormSubmit = {}
					  }, 2000);
				  
				   
				   
			   }else{
				   
				   $("#newMpinForm .alert").text(res.data.statusDesc);
				   $("#newMpinForm .alert").addClass("alert-danger");
				   $("#newMpinForm .alert").removeClass("alert-success");
			   }
			 rg.mpinFormSubmit.mpin = ""; 
		   rg.mpinFormSubmit.otp = "";
		   },function(res){
			   console.log(res.statusText)
			   
		});
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });

	}
	
});
dmt.controller('fundTransCtrl',function($http,$rootScope,validateService, $sce,$scope){
	var ft = this;
	ft.deleteConfirmation = "Are you sure? Do you Want to delete this beneficiary";
	ft.amountRegex = '\\d+';
	ft.isprocessing = false;
	
	

	
	ft.transAmount = function(id,beneId, transType){
		$rootScope.currentTransBene = beneId;
		$rootScope.transactionMode = false;
	
	ft.isprocessing = true;
	$http.get('CheckSession')
       .then(function(res){ 
    	  // alert(2)
    	   console.log(res)
    	   if(res.data.status == "TRUE"){
		if(  $rootScope.LoginSender.transferLimit >= parseFloat($(id).val())){
			$rootScope.trasactionAmount =  $(id).val()
			$rootScope.beneDataAmountObj = {}
 			$rootScope.beneDataAmountObj.id = beneId;
			$rootScope.beneDataAmountObj.txnAmount = $(id).val();
			$rootScope.beneDataAmountObj.transferType = $(transType + " .active").text();
			
			
			
		
	
	
	 $http({
		 method:'POST',
		 url:'FundTransferRequest',
		 data:"data="+ JSON.stringify($rootScope.beneDataAmountObj),
		 headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		 
	 }).then(function(res){
		console.log( res)
		//$(".grid-search-sm").val('')
		 $rootScope.validateBeneoObj = res.data
		 
		 $rootScope.validateBeneoObj.message =  $sce.trustAsHtml(res.data.message)
		 console.log($rootScope.validateBeneoObj)
		 if($rootScope.validateBeneoObj.status == "F"){
			
			
			 $rootScope.rootErrorMsg  = $rootScope.validateBeneoObj.Error;
			
			 $rootScope.modelBoxContent = "jspas/app/components/loading/loading.html" + $rootScope.version;
			 $rootScope.modelBoxContent =  "jspas/app/components/error/errorMsg.html" + $rootScope.version; 
			 $('#myModal').modal({
				   backdrop: 'static',
				    keyboard: false
				 
			 });
		 }else{
		 if($rootScope.validateBeneoObj.status == "Y"){	
			 $rootScope.modelBoxContent = "jspas/app/components/loading/loading.html" + $rootScope.version;
			 $rootScope.modelBoxContent = "jspas/app/components/regUser/validatateBene.html" + $rootScope.version;
			 $('#myModal').modal({
				   backdrop: 'static',
				    keyboard: false
				 
			 });
			
			 setTimeout(function(){
				
				 $(".curr-inr").html("INR");
			 },100)
			 
		 	}else if($rootScope.validateBeneoObj.status == "N"){
		 		$rootScope.modelBoxContent = "jspas/app/components/loading/loading.html" + $rootScope.version;
			 $rootScope.modelBoxContent = "jspas/app/components/regUser/transactionConfirmation.html" + $rootScope.version;
			 $('#myModal').modal('show');
		 	}	
		 }
		
		ft.isprocessing = false;
		
	 },function(res){
		 
	 })
		
		}
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });
		
	};
	 ft.onlyNumber = function($event){
		
		  validateService.onlyNumber($event); 
		  
	  };

	ft.changeBeneStatus = function(beneId,status){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		console.log(status)
		if(status == "Active"){
		 $http({
			 method:'POST',
			 url:'DeActiveBeneficiary',
			 data:'benId='+ beneId,
			 headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			 
		 }).then(function(res){
			 console.log(res)
			 if(res.data.statusCode == "1000"){
				 $rootScope.modelBoxContent =  "jspas/app/components/regUser/deactiveSuccessMsg.html" + $rootScope.version; 
				 $rootScope.LoginSender.beneficiaryList =  res.data.beneficiaryList;
			 }else{
				 $rootScope.rootErrorMsg  = res.data.statusDesc;
				 $rootScope.modelBoxContent =  "jspas/app/components/error/errorMsg.html" + $rootScope.version; 
			 }  
			
			 $('#myModal').modal('show');
		 },function(res){
			 
		 })
		}else if(status == "Deactive"){
			
			$rootScope.selectedSender = beneId
			 $rootScope.modelBoxContent =  "jspas/app/components/regUser/deleteOrActive.html" + $rootScope.version; 
			
			 $('#myModal').modal('show');

		}
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });


	};
	ft.showDeleteBene = function(id){
		$("#delete-bene" + id).next(".popover").show();		
	}
	ft.deleteBene = function(id){
		 $http({
			     method: 'POST',
			     url: 'DeleteBeneficiary',
			     data    :'data='+ id, //forms user object
			           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				   console.log(res);
				   if(res.data.statusCode == 1000){
					   $("#row-bene" + id).remove();				
				   }
			   },function(res){				   
				   console.log(res);
			   })
		
	};
	ft.ChangeTrans = function(el){
		
		ChangeTransMode(el);
		
		
		}
	ft.sortBy = function(){
		
	};
	
});


dmt.controller("fundTransferPopupCtrl",function($http, $rootScope){
	var ftp = this;
	ftp.btnText = "Yes";
	ftp.loading = false;
	ftp.AmountConfirmation = function(UserResponse){
		ftp.btnText = "Loading...";
		varificationBene($http,$rootScope, UserResponse);
		ftp.btnText = "Yes";
		
	
	}
});
dmt.controller("transferConfirmCtrl",function($rootScope,$http,$templateCache){
	var traCon = this;
	traCon.btnText = "Confirm";
	traCon.transactionOtp = null;
	traCon.isOtpVarified = $rootScope.loginUserData.otpVerificationRequired == 1 ? true : false;
	traCon.success = false;
	traCon.error = false;
	traCon.successMsg = '';
	traCon.errorMsg = false;
	
	traCon.initTrans = function(){
		if(traCon.isOtpVarified){			
			traCon.success = true;
			traCon.successMsg = 'OTP has been sent to your registered mobile number.';
			
		}else{
			traCon.success = false;
			traCon.successMsg = '';
			
		}
		
		traCon.error = false;
		traCon.errorMsg = '';

		
	}
	
	traCon.transferConfirmed = function(elm){
		$rootScope.transactionMode = true;
		//$("#transactionProcsses").show()
		
		$('.grid-search-sm').val('').keypress()
			
		$templateCache.removeAll();
		$(elm).prop('disabled',true)
		traCon.btnText = "Loading...";
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
	      $http({
			method:'POST',
			url:'FundTransfer',
			  data    :'otp='+ 	traCon.transactionOtp,
			 headers : {'Content-Type': 'application/x-www-form-urlencoded'}		
			
						}).then(function(res){
							console.log(res)
							
							traCon.btnText = "Confirm";
							$(elm).prop('disabled',false)
							
							$rootScope.transactionMode = false;
							$("#transactionProcsses").hide()
							if(res.data.status == "Y"){	
								$rootScope.transSummary = angular.fromJson(res.data.details);
								$rootScope.loginUserData.finalBalance = $rootScope.transSummary.agentWalletAmount;
								$rootScope.LoginSender.transferLimit  =  $rootScope.transSummary.senderLimit;
								
								$rootScope.modelBoxContent =  "jspas/app/components/regUser/transactionSummary.jsp";
								 $('#myModal').modal('show');
							  		 $http({
							  		     method: 'POST',
							  		     url: 'ValidateSender',
							  		     data    :'mastBean.mobileNo='+ $rootScope.LoginSender.mobileNo, //forms user object
							  		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
							  		 })
							  		    .then(function(res){ 
							  		    	console.log(res)
							  		       if(res.data.statusCode == "1000"){
							  		        	   $rootScope.LoginSender  = res.data;
							  		        	   console.log($rootScope.LoginSender )
							  		        	
							  		       }
							  		    
							  		  },function(res){
							  			
										alert("Please check your internet connection.")
							  		   console.log(res.statusText)
							  		  });
							  		 
						
				
							}else{
								
								$rootScope.modelBoxContent = "jspas/app/components/error/errorMsg.html"
									$rootScope.rootErrorMsg = res.data.message;
								 $('#myModal').modal('show');
									//$rootScope.popupHeading  = "Trasaction Summary";
							}
					 
						},function(err){
								alert("Please check your internet connection.")
						});
		
	    	   }else{
 		   alert("Your session has expired. Please login again.")
 		   window.close();
 	   }
    },function(res){
    	alert("Please check your internet connection.")
    	location.reload();
		   console.log(res.statusText)
		  });

		
	
	}
})

dmt.controller('uploadPanCtrl',function($http,$rootScope,multipartForm) {
	
	var pc =  this;	
	$rootScope.popupHeadingPanCard = "Upload Pan Card";	
	
	$rootScope.proofType = "panCard"
	$rootScope.fileLabelHeading = "Upload PAN Card:"
		

	pc.uploadFrom60 = function(){
		
		$rootScope.panCard =  false;	
		$rootScope.popupHeadingPanCard = "Upload FORM 60";	
		$rootScope.fileLabelHeading = 'Upload duly filled & sender copy of "Form 60".';
		$rootScope.proofType = "form60"
	},
	pc.panCardForm = function(){
		$rootScope.panCard =  true;	
		$rootScope.popupHeadingPanCard = "Upload Pan Card";	
		$rootScope.fileLabelHeading = "Upload PAN Card:"
		$rootScope.proofType = "panCard"
	},
	pc.submitPanForm = function(){
		
		var proofType = $("#proofType").val()
		var errSpan  = $("#panErrMsg")
		var el = $("#panCardInp").val()
		
	if($("#panfile").val().length != 0){
		
		if(!el.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}/)  && proofType == 'Pan' ){
			errSpan.text("Please provide the valid PAN number.").css("color",'red')
			
		}else{
			
			
			$http.get('CheckSession')
		       .then(function(res){ 
		    	   if(res.data.status == "TRUE"){
		    	   	 		     
		    		        var form = $("#panFrom60Form")[0]
			    			 var panData = new FormData(form)	

		    		        $.ajax({
		    		            type: 'POST',
		    		            enctype: 'multipart/form-data',
		    		            processData: false,  // Important!
		    		            contentType: false,
		    		            cache: false,
		    		            url: 'UploadSenderPanF60',
		    		            data:panData,
		    		            success: function (data) {
		    		            	if(data.type == "Success"){
		    		            		
		    		            		$("#panErrMsg").text(data.msg).css("color",'green');
		    		            		
		    		            		$("#panCardInp, #panfile").val('')
		    		            	}else {
		    		            		$("#panErrMsg").text(data.msg).css("color",'red');
		    		            	}
		    		               
		    		                console.log(data);
		    		                
		    		            },
		    		            error: function (data) {
		    		                console.log('An error occurred.');
		    		                console.log(data);
		    		            },
		    		        });
		    		      
		    	   

		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		    	   
		       }); 
			
		}
	}else{
		errSpan.text("Please upload the file").css("color",'red')
		
	}
		
	
	  
		
	}
	
})


dmt.controller('senderDeleteAndActiveCtrl',function($http,$rootScope) {
var sad = this;
sad.SenderAction = function(senderId,status){

	var url = status == "Delete" ? "deletedBeneficiary" : "ActiveBeneficiary"

	 $http({
		 method:'POST',
		 url:url,
		 data:'benId='+ senderId,
		 headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		 
	 }).then(function(res){
		console.log( res )
		 if(res.data.statusCode == "1000" ){
			 if(status == "Active"){
				 $rootScope.modelBoxContent =  "jspas/app/components/regUser/activatedSuccessMsg.html" + $rootScope.version; 
			 }else{
				 $rootScope.modelBoxContent =  "jspas/app/components/regUser/deleteSuccessMsg.html" + $rootScope.version;  
			 }
		
			 $rootScope.LoginSender.beneficiaryList =  res.data.beneficiaryList;
		 }else{
			 $rootScope.rootErrorMsg  = res.data.statusDesc;
			 $rootScope.modelBoxContent =  "jspas/app/components/error/errorMsg.html" + $rootScope.version; 
		 }  

		
	 },function(res){
		 
	 })
} 
	
});



function varificationBene($http, $rootScope,UserResponse){
	
	
	
	if(UserResponse == "Y"){
		
	$http({
		 method:'GET',
		 url:'VerifyBeneficiary',
		 headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		 
	 }).then(function(res){
		 console.log(res.data)
		 if(res.data.result == "Y" ){
			 $rootScope.validateBeneoObj  = res.data;
			 $rootScope.modelBoxContent = "jspas/app/components/regUser/transactionConfirmation.html" + $rootScope.version;
			 
		 }else{
		
			 
			 $rootScope.modelBoxContent = "jspas/app/components/error/errorMsg.html" + $rootScope.version;
					$rootScope.rootErrorMsg = res.data.message;
			
		 }
		;
		// $('#myModal').modal('show');
	 })
	}else{
		 $rootScope.modelBoxContent = "jspas/app/components/regUser/transactionConfirmation.html" + $rootScope.version;
	}
}

function printContent(DivID){
	window.print()
}

function switchPanForm(formType){
	
	if(formType == "Pan"){
		$("#panCardInp").val('')
		$("#panField").show()
		$("#heading").text("Upload Pan Card	")
		$("#fileLabelHeading").text("Upload Pan Card	")
		$("#proofType").val("Pan")
		$("#panfile").val('');
		$("#from60").hide();
		$("#panMsg").show();
		
	}else{
		$("#panMsg").hide();
		$("#heading").text("Upload form 60	")
		$("#fileLabelHeading").text("Upload duly filled & sender copy of 'Form 60'.	")
		$("#panField").hide();
		$("#from60").show();
		$("#panfile").val('')
		$("#proofType").val("form60")
	}
	$("#panErrMsg").text('')
}

