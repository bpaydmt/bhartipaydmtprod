dmt.controller("addBeneficiaryCtrl",function($http,$rootScope,$timeout){
		var ab = this;
		ab.bene = ab.beneForm;
		ab.error = false;
		ab.loading = false;
		ab.dropOption;
		$rootScope.beneIfscInvalid = false;
		$rootScope.beneSelectedBank;
		ab.isOtpSent = false;
		ab.success = false;
		
		
		ab.isOtpVarified = $rootScope.loginUserData.otpVerificationRequired == 1 ? true : false;
		
		
		
		ab.btnText = "Submit";
		$rootScope.Benefields = {};
		
		
		//vm.bankDetails = "";
		ab.refreshTab = function(){	
			ab.isOtpVarified = $rootScope.loginUserData.otpVerificationRequired == 1 ? true : false;
			ab.isOtpSent = false;
			ab.success = false;
			ab.successMsg = '';
			//alert(1)
			$rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
			$('[data-toggle="tooltip"]').tooltip();
			
			$("#bank-prefilled").hide()
	};
		ab.initBeneForm = function(){		
			//$rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
		
			$('[data-toggle="tooltip"]').tooltip();
			
		
					$http.get('GetBankDtl').then(function(res){
						console.log(res)
						//var defaultOption = {bankName:"Select Your Bank",id:-1}
						ab.dropOption = res.data;
						
						$rootScope.beneSelectedBank = 	ab.dropOption[0].id;
						setTimeout(function(){
						$('#bankNames').chosen({search_contains: true})
						
						$('#bankNames').on('change',function(e, params){
							console.log(params.selected)
								ab.searchIfcsbyBankName()						 // params.selected and params.deselected will now contain the values of the
							 // or deselected elements.
							});
						},1500)
						//ab.dropOption.unshift(defaultOption)
						/*setTimeout(function(){
						/*	$("#bankNames").searchit({
							      textFieldClass: 'searchbox form-control',
							      selected: true
							  });*/
						/*	$("#bankNames").searchable( {  maxListSize: 100,      // if list size are less than maxListSize, show them all
									   maxMultiMatch: 50,      // how many matching entries should be displayed
									   exactMatch: false,      // Exact matching on search
									   wildcards: true,      // Support for wildcard characters (*, ?)
									   ignoreCase: true,      // Ignore case sensitivity
									   latency: 200,       // how many millis to wait until starting search
									   warnMultiMatch: 'top {0} matches ...', // string to append to a list of entries cut short by maxMultiMatch 
									   warnNoMatch: 'no matches ...',   // string to show in the list when no entries match
									   zIndex: 'auto'       // zIndex for elements generated by this plugin
									     });
							
						},1000)*/
						
					})
				
			
			//$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
	};
	
	ab.searchIfcsbyBankName = function(){
		//alert(2)
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	   if(res.data.status == "TRUE"){
		   
		   var bankName = $("#bankNames").val().toUpperCase();
		   var acc = $("#accountNoBene").val();



			   
			   if(bankName =="STATE BANK OF INDIA"){
				   $rootScope.Benefields.ifscCode = "SBIN0001537";	
				   ab.searchIfscCode("autoFill");	 
				   
			   }
			   else if(bankName ==  "CITI BANK"){
				   $rootScope.Benefields.ifscCode = "CITI0000004";	
				   ab.searchIfscCode("autoFill");	 
				   
			   }
			
	
		  else if(acc.length > 2 && bankName == "KARNATAKA BANK LIMITED"){
			 var accSub = acc.substr(0, 3);
			  $rootScope.Benefields.ifscCode = "KARB0000" + accSub ;
			  ab.searchIfscCode("autoFill");
		 }else if(acc.length > 5 && bankName == "PUNJAB NATIONAL BANK"){
			 var accSub = acc.substr(0, 6);
			  $rootScope.Benefields.ifscCode = "PUNB0" + accSub ;
			  ab.searchIfscCode("autoFill");
		 }else if( bankName == "MADHYA BIHAR GRAMIN BANK"){
			
			    $rootScope.Benefields.ifscCode = "PUNB0MBGB06";
			     ab.searchIfscCode("autoFill");
			   //  var gramin = bankName.toLowerCase().search("gramin");
			   }else if( bankName == "UTTAR BIHAR GRAMIN BANK MUZAFFARPUR"){
			    $rootScope.Benefields.ifscCode = "CBIN0R10001";
			     ab.searchIfscCode("autoFill");
			   }else if(bankName == "KASHI GOMTI SAMYUT GRAMIN BANK"){
			    $rootScope.Benefields.ifscCode = "UBIN0RRBKGS";
			     ab.searchIfscCode("autoFill");
			   }
				else if(acc.length > 3 && bankName != "KARNATAKA BANK LIMITED"  && bankName != "PUNJAB NATIONAL BANK"){
				   
					 
				   var accSub = acc.substr(0, 4);
			  
			   
			   switch(bankName){
			   case "ANDHRA BANK":
				   $rootScope.Benefields.ifscCode = "ANDB000" + accSub;
				  
				   break;
			   case "BANK OF INDIA":
				   $rootScope.Benefields.ifscCode = "BKID000" + accSub;			  
				   break;
			   case "CANARA BANK":
				   $rootScope.Benefields.ifscCode = "CNRB000" + accSub;			 
				   break;
			   case "CATHOLIC SYRIAN BANK LIMITED":
				   $rootScope.Benefields.ifscCode = "CSBK000" + accSub;			 
				   break;
			  
			   case "CORPORATION BANK":
				   $rootScope.Benefields.ifscCode = "CORP000" + accSub;			 
				   break;
			   case "DENA BANK":
				   $rootScope.Benefields.ifscCode = "BKDN000" + accSub;			 
				   break;
			   case "FEDERAL BANK":
				   $rootScope.Benefields.ifscCode = "FDRL000" + accSub;			 
				   break;		  
			   case "ICICI BANK LIMITED":
				   $rootScope.Benefields.ifscCode = "ICIC000" + accSub;			 
				   break;			   
			   case "INDIAN OVERSEAS BANK":
				   $rootScope.Benefields.ifscCode = "IOBA000" + accSub;			 
				   break;
			   case "KARUR VYSYA BANK":
				   $rootScope.Benefields.ifscCode = "KVBL000" + accSub;			 
				   break;
			   case "KOTAK MAHINDRA BANK LIMITED":
				   $rootScope.Benefields.ifscCode = "KKVK000" + accSub;	
			   case "LAXMI VILAS BANK":
				   $rootScope.Benefields.ifscCode = "LAVB000" + accSub;	
				   break;
			   case "ORIENTAL BANK OF COMMERCE":
				   $rootScope.Benefields.ifscCode = "ORBC010" + accSub;	
				   break;
			   case "SOUTH INDIAN BANK":
				   $rootScope.Benefields.ifscCode = "SIBL000" + accSub;	
				   break;
			   case "SYNDICATE BANK":
				   $rootScope.Benefields.ifscCode = "SYNB000" + accSub ;	
				   break;
			   case "UCO BANK":
				   $rootScope.Benefields.ifscCode = "UCBA000" + accSub ;	
				   break;
			   case "VIJAYA BANK":
				   $rootScope.Benefields.ifscCode = "VIJB000" + accSub ;	
			   case "YES BANK":
				   $rootScope.Benefields.ifscCode = "YESB000" + accSub ;
				   break;
				 //New bank Added 5/2/2019
			   case "AHEMDABAD MERCANTILE CO OP BANK LTD":
			   $rootScope.Benefields.ifscCode = "AMCB0660016";
			   break;
			   case "ALLAHABAD BANK":
			   $rootScope.Benefields.ifscCode = "ALLA0211975";
			   break;
			   case "AMBERNATH JAIHIND CO OP BANK LTD":
			   $rootScope.Benefields.ifscCode = "ICIC00AJHCB";
			   break;
			   case "AXIS BANK":
			   $rootScope.Benefields.ifscCode = "UTIB0000248";
			   break;
			   case "BANK OF BARODA UP GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "BARB0BUPGBX";
			   break;
			   case "BARODA RAJASTHAN GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "BARB0BRGBXX";
			   break;
			   case "CENTRAL BANK OF INDIA":
			   $rootScope.Benefields.ifscCode = "CBIN0282643";
			   break;
			   case "ELLAQUAI DEHATI BANK RRB":
			   $rootScope.Benefields.ifscCode = "SBIN0RRELGB";
			   break;
			   case "IDBI BANK":
			   $rootScope.Benefields.ifscCode = "IBKL0000039";	
			   break;	
			   case "INDIAN BANK":
			   $rootScope.Benefields.ifscCode = "IDIB000C128";	
			   break;
			   case "KASI GOMTI BANK":
			   $rootScope.Benefields.ifscCode = "UBIN0RRBKGS";	
			   break;	
			   case "KOTTAYAM CO-OPRATIVE URBAN BANK LTD":
			   $rootScope.Benefields.ifscCode = "lBKL0027K01";	
			   break;	
			   case "MADHYA BIHAR GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "PUNB0MBGB06";	
			   break;	
			   case "MAHARASHTRA GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "MAHB0RRBMGB";	
			   break;	
			   case "NAGALAND RURAL BANK RRB":
			   $rootScope.Benefields.ifscCode = "SBIN0RRNLGB";	
			   break;	
			   case "PURVANCHAL GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "SBIN0RRPUGB";	
			   break;	
			   case "SHAHADA PEOPLE CO OP BANK SHIRPUR":
			   $rootScope.Benefields.ifscCode = "KKBK0SPCB01";	
			   break;	
			   case "THE BANKI CENTRAL CO OP BANK LTD":
			   $rootScope.Benefields.ifscCode = "YESB0BNKCCB";	
			   break;	
 case "THE BERHAMPORE CO OPRATIVE CENTRAL BANK":
 $rootScope.Benefields.ifscCode = "IBKL0216BCB";
 break;

			   case "THE BHAWANI PATNA CENTRAL CO OPRATIVE BANK":
			   $rootScope.Benefields.ifscCode = "ICIC00BHCCB";	
			   break;
			   case "THE COSMOS CO OPRATIVE BANK LTD":
			   $rootScope.Benefields.ifscCode = "COSB0000019";	
			   break;	
			   case "THE VARACHHA COOPERATIVE BANK LIMITED":
			   $rootScope.Benefields.ifscCode = "VARA0000001";	
			   break;	
			   case "UDAYPUR MAHILA URBAN CO OPRATIVE BANK LTD":
			   $rootScope.Benefields.ifscCode = "YESB0MSB004";	
			   break;	
			   case "UNION BANK OF INDIA":
			   $rootScope.Benefields.ifscCode = "UBIN0551945";	
			   break;
				   
				   default:
					   return false;
				   
			   
			   }
			   ab.searchIfscCode("autoFill");
			 }
			   
			   
			   else{
				  
				   $rootScope.Benefields.branchName = null;
					$rootScope.Benefields.ifscCode = null;
					$rootScope.Benefields.city = null;
					$rootScope.Benefields.state = null;
					$rootScope.Benefields.address = null;
				
					$("#bank-prefilled").hide(); 
					
			   }
		   
		   	
		   	
		   	
		   
		   
		   
	   }
	       });	
	}
		ab.searchIfcs= false;
		ab.addBenficiary = function(){
			
				  var beneForm = $("#add-beneficiary-form")
				  
				 if(beneForm.is(':hidden')){
					 
					 beneForm.slideDown()
				 }else{
					 beneForm.slideUp()
				 }
				$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
				  ab.initBeneForm();  
		   }
		ab.searchIfscCode = function(medium){	
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		   if(res.data.status == "TRUE"){
			if(medium == "button"){
				
			
			$rootScope.beneSelectedBank =  $("#bankNames").val();
			console.log($rootScope.beneSelectedBank)
			$rootScope.modelBoxContent = "jspas/app/components/loading/loading.html";
			$('#myModal').modal('show');
			$timeout(ab.openIfscPopup, 100);
			
			}else{
				if(ab.beneForm.beneIfsc.$valid){
					$rootScope.beneIfscInvalid  = false;
					$http({
					     method: 'POST',
					     url: 'GetBankDetailsByIFSC',
					     data :'data='+ JSON.stringify({"ifscCode":$rootScope.Benefields.ifscCode}), //forms user object
					     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
					   }).then(function(res){
						   
						   if(res.data.length > 0){
								var bankD = res.data[0]
								
								console.log(bankD)
									
								$rootScope.Benefields.bankName = bankD.bankName;
								$rootScope.Benefields.branchName = bankD.branchName;
								$rootScope.Benefields.ifscCode = bankD.ifscCode;
								$rootScope.Benefields.city = bankD.city;
								$rootScope.Benefields.state = bankD.state;
								$rootScope.Benefields.address = bankD.address;
								$("#bankNames").val($rootScope.Benefields.bankName)
								$("#bank-prefilled").show();
								 $('#bankNames').trigger("chosen:updated");
						   }else{
								$rootScope.beneIfscInvalid = true;
							
								$rootScope.Benefields.branchName = "";
							
								$rootScope.Benefields.city = "";
								$rootScope.Benefields.state ="";
								$rootScope.Benefields.address = "";
								//$("#bankNames").val($rootScope.Benefields.bankName)
								$("#bank-prefilled").hide(); 
							}
						   
						
										
					
					
					//ifsc.bankDetailGrid = res.data;
					
					
					
				},function(res){
					console.log(res);
				
					})
					
				}else{
					$rootScope.beneIfscInvalid = true;
				}
		
			}
		
			
		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });
	
		
    		
		}
	
		ab.openIfscPopup = function(){
			 $rootScope.modelBoxContent = "jspas/app/components/addBeneficiary/seachIfscCode.html" + $rootScope.version;
		}
	/*	ab.validateAddBene = function(){
			if(ab.isOtpVarifie){
				
				 $rootScope.modelBoxContent = "jspas/app/components/addBeneficiary/otp.html" + $
				 $('#myModal').modal('show');
			}else{
				ab.addBeneficiary();
			}
		}*/
		
		ab.sendOTP = function(){
			
			ab.isOtpSent = true;
			$http({
				
			     method: 'POST',
			     url: 'senderOtpSend',
			     data :'data='+ $rootScope.LoginSender.mobileNo, //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   })
			.then(function(res){
				
				console.log(res)
				
				if(res.data.status == "true"){
					ab.isOtpVarified =  false;
				
					ab.success = true;
					ab.successMsg = 'OTP has been sent to your registered mobile number.';
				}
				
			})
		}
		ab.addBeneficiary = function(){
			ab.loading = true;
			ab.btnText = "Loading..";
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		    	   if(res.data.status == "TRUE"){
			$rootScope.Benefields['transferType'] = $("#addBeneFormMode .active").text();			
			$rootScope.Benefields['branchName'] = $("#senderBranch").text();
			$rootScope.Benefields['city'] = $("#senderCity").text();
			$rootScope.Benefields['state']= $("#senderState").text();
			$rootScope.Benefields['address'] = $("#senderAddress").text();
			$http({
				
			     method: 'POST',
			     url: 'RegisterBeneficiary',
			     data :'data='+ JSON.stringify($rootScope.Benefields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				console.log(res)
				ab.loading = false;
				ab.btnText = "Submit";
				if(res.data.statusCode == 1000){
					$('#myModal').modal('hide');
					$("#add-beneficiary-form").collapse('hide');
					$rootScope.LoginSender.beneficiaryList.unshift(res.data)
					console.log($rootScope.LoginSender.beneficiaryList)
					 $rootScope.modelBoxContent = "jspas/app/components/addBeneficiary/beneficiaryAddedSuccessMsg.html"+ $rootScope.version;
					 $('#myModal').modal('show');
					 $rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
					//$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
					$("#bank-prefilled").hide();
					ab.error = false;
				}else{
					ab.errorMsg = res.data.statusDesc;
					console.log(ab.error)
					console.log(ab.errorMsg)
					ab.error = true;
					ab.success = false;
					ab.successMsg = '';
				}
				
			},function(res){
				   console.log(res.statusText)
			  });
		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });

		};
		ab.getBankDetails = function(){		
			
			$http({
			     method: 'POST',
			     url: 'GetBankDetails',
			     data :'data='+ JSON.stringify($rootScope.Benefields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				   console.log(res)
				   var resData = res.data
				   	$rootScope.Benefields.branchName = resData.branchName;
					$rootScope.Benefields.ifscCode = resData.ifscCode;
					$rootScope.Benefields.city = resData.city;
					$rootScope.Benefields.state = resData.state;
					$rootScope.Benefields.address = resData.address;
				  $("#bank-prefilled").show();
				 console.log( res.data) ;
				
			},function(res){
				   console.log(res.statusText)
			  });
			
		};
	
		

		ab.ChangeTrans = function(el){
			ChangeTransMode(el);
			}
			
});
dmt.controller("getBankDetailsCtrl",function($http,$rootScope){
	var ifsc = this;
	ifsc.dropOption;

	ifsc.btnText = "Submit";
	ifsc.loading = false;
	ifsc.fields = {};
	ifsc.fields.ifscCode  = '';
	ifsc.fields.state = "-1";
	ifsc.fields.bankId = "";
	ifsc.BtnMsg = (ifsc.fields.state == "-1" &&  ifsc.fields.ifscCode =='') ? "Please select the state" : '';
	ifsc.gridData = false;
	ifsc.getDropVal = function(){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		$http.get('GetBankDtl',{cache: false,}).then(function(res){
			
			//var defaultOption = {bankName:"Select Your Bank",id:-1}
			ifsc.dropOption = res.data;
			//ifsc.dropOption.unshift(defaultOption)
			ifsc.dropOption = res.data;
		
			   
		})
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });
	}
	ifsc.getBankList = function(){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		ifsc.loading = true;
		ifsc.btnText = "Loading...";
		ifsc.fields.bankId = $("#bankNameIfsc").val();
		console.log($("#bankNameIfsc").val())
		console.log(ifsc.fields);
		$http({
			     method: 'POST',
			     url: 'GetBankDetailsByIFSC',
			     data :'data='+ JSON.stringify(ifsc.fields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
			console.log(res)
			ifsc.loading = false;
			ifsc.btnText = "Submit"
			ifsc.gridData = true;
			ifsc.bankDetailGrid = res.data;
			
			
			
		},function(res){
			console.log(res);
		})
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });
	}
	
	ifsc.returnBankDetails = function(ifscCode){
		for(var i= 0; i < ifsc.bankDetailGrid.length; i++){
			
			if(ifsc.bankDetailGrid[i].ifscCode == ifscCode){
				
				return  ifsc.bankDetailGrid[i]
			}
		}
		
		
	}
	ifsc.getSelectedBank = function(ifscCode){
		console.log(ifscCode)
		console.log(ifsc.returnBankDetails(ifscCode))
		
		var bankD = ifsc.returnBankDetails(ifscCode)
	
		
			
		$rootScope.Benefields.bankName = bankD.bankName;
		$rootScope.Benefields.branchName = bankD.branchName;
		$rootScope.Benefields.ifscCode = bankD.ifscCode;
		$rootScope.Benefields.city = bankD.city;
		$rootScope.Benefields.state = bankD.state;
		$rootScope.Benefields.address = bankD.address;
		$("#bankNames").val($rootScope.Benefields.bankName)
		$rootScope.beneIfscInvalid = false;
		$("#bank-prefilled").show();
		$('#myModal').modal('hide');
	}
	
});
function ChangeTransMode(el){
	if(!$(el).hasClass("active") && !$(el).hasClass("isImpsFalse")){
	
		$(el).addClass("active")
		$(el).siblings().removeClass("active");
	}
	
}

