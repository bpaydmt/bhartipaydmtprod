dmt.controller('favlistCtrl',function($http,$location,$rootScope,$timeout){
	vm = this;
	//var favGrid = "";
	vm.isProccessing = false;
	
	vm.popGrid = function(){ 	
	
		   $http.get('GetFavouriteList')
		    	.then(function(res){ 		               		 
		        $rootScope.favList = res.data;	
		       
		  },function(res){
		   console.log(res.statusText)
		  });
	 };
	vm.loginSender = function(mobile){ 	
		vm.isProccessing = true;
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    if(res.data.status == "TRUE"){
		   $http({
		     method: 'POST',
		     url: 'ValidateSender',
		     data    :'mastBean.mobileNo='+mobile, //forms user object
		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		   })
		    .then(function(res){ 
		    	console.log(res)
		       if(res.data.statusCode == "1000"){		        
		        	$rootScope.LoginSender = res.data;
		        	localStorage.setItem("senderData", mobile);		        	        		
		        	$location.path('/regUser');
		        	//$rootScope.modelBoxContent = "jspas/app/components/favGrid/senderLoginMsg.html";
		    		//$('#myModal').modal('show');
		        	    
		      }		
		    	vm.isProccessing = false;
		  },function(res){
		   console.log(res.statusText)
		  });
		   
	    }
	       else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });
		};
		  
		vm.updateSenderKYC = function(mobile,senderId){
			
			vm.isProccessing = true;
			$http.get('CheckSession')
		       .then(function(res){ 
		    	   //alert(mobile)
		    	   console.log(res)
		    if(res.data.status == "TRUE"){
		    	 $rootScope.modelBoxContent = "jspas/app/components/favGrid/kycUpdateOption.html"+  $rootScope.version;
		    	 $('#myModal').modal('show');
		    	 $("#senderMobileTop").text('')
		    	 $timeout(function () {
		    		 $("#mobileNo").val(mobile)
		    		  $("#senderId").val(senderId)
		    		 $("#senderMobileTop").text(senderId)
		    		 
		    	    }, 500);
		    	
			   
		    }
		       else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });
			vm.isProccessing = false;
			};
		
	vm.showDeleteFav = function(id){		
		$("#delete" + id).next(".popover").show();
		
	}
	vm.hideDeletePopup = function(id){
		$("#delete" + id).next(".popover").hide();
	}
	vm.deleteFav = function(senderId, id){
		
		 var deletFavObj = {senderId: senderId, id: id}
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		    	   if(res.data.status == "TRUE"){
			  $http({
			     method: 'POST',
			     url: 'DeleteFavourite',
			     data    :'data='+ JSON.stringify(deletFavObj), //forms user object
			           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				  console.log(res)
				   if(res.data.statusCode == 1000){
					   $("#row-" + id).remove();					
				   }else{
						$rootScope.modelBoxContent = "jspas/app/components/error/errorMsg.html"+ $rootScope.version;
			    		$('#myModal').modal('show');
				   }
				  vm.hideDeletePopup(id)
			   },function(res){
				   
				   console.log(res);
			   })
			   
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });
		
	};	

	vm.orderByField = true;
	vm.deleteConfirmation = "Are you sure? Do you Want to delete this beneficiary"
	vm.sortBy = function(){
		
	};
	
	
});

dmt.controller('kycCtrl',function($rootScope, $http){
	
	vm = this;
	
	vm.updateDocAddressType = function(){
		
	}

		vm.updateDocIdType = function(){
		}
	vm.initKyc = function(){
		
		vm.getIdProofDoc();
		
		vm.getAddressProofDoc()
		
	}
	vm.bioMet =['Please Select a Device','Mantra','Startek','Morpho','Secugen','Precision','IriShield','Cogent'];
	vm.bio = vm.bioMet[0];
	vm.getIdProofDoc = function(){
		$http.get('GetKycId').then(function(res){
			console.log(res)
			
			vm.idDoc = res.data;

			vm.idProofDoc = vm.idDoc[0]
			
		})
	}
	vm.getAddressProofDoc = function(){
		$http.get('GetKycAddress').then(function(res){
			console.log(res)
			
			
			vm.addDoc=res.data;
			vm.addressDoc = vm.addDoc[0];
		})
	}
	
	
	vm.submitKYC = function(e){
		
				/*if(type == "otp"){
			$("#serviceType").val("05")
		}else{
			$("#serviceType").val("04")
		}*/
		
		}
	

});
