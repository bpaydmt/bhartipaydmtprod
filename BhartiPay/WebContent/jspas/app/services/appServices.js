
dmt.factory("senderService",function ($rootScope){
	vm = this;
/*	if (typeof(Storage) !== "undefined") {
	    // Store
	    localStorage.setItem("lastname", "Smith");
	    // Retrieve
	    document.getElementById("result").innerHTML = localStorage.getItem("lastname");
	} */
});
dmt.factory("validateService",function ($rootScope){
	validateObj = {};
	validateObj.onlyNumber =function($event){
		var code = $event.which ? $event.which : $event.keyCode;
		console.log(String.fromCharCode($event))
        if(isNaN(String.fromCharCode($event.code))){
            $event.preventDefault();
        }
	};
			
	
	return validateObj
});
dmt.directive("mwInputRestrict", [
                               function () {
                                   return {
                                       restrict: "A",
                                       link: function (scope, element, attrs) {
                                    	   
                                           element.on("keypress", function (event) {
                                        	   var key = event.keyCode || event.which;
                                        	   
                                        	   var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                                        	   var exclusions=[8,9,37,38,39,40,46,13];
                                        	   var regDot = new RegExp("^[.']+$");
                                        	   
                                        	   console.log(key)
                                        	   if(exclusions.indexOf(key)>-1  && event.shiftKey == false && !regDot.test(str)){return;}
                                               if (attrs.mwInputRestrict === "onlynumbers") {
                                            	   
                                            	   if(element.hasClass('noZero') &&  key == 48 && element.val().length == 0){
                                            		   event.preventDefault();
                                            		   console.log(element.val().length)
                                            		   
                                            	   }
                                            		   
                                            	   console.log(key)
                                            	   var numRegex = new RegExp("^[0-9]+$"); 
                                            	
                                                   // allow only digits to be entered, or backspace and delete keys to be pressed
                                                   if (!numRegex.test(str)){
                                                	   event.preventDefault()
                                                   }
                                                   
                                               }else if(attrs.mwInputRestrict === "onlyAlpha"){
                                            	   var alphRegex = new RegExp("^[a-zA-Z ]+$"); 
                                               	
                                          	    
                                                 // allow only digits to be entered, or backspace and delete keys to be pressed
                                                 if (!alphRegex.test(str)){
                                              	   event.preventDefault()
                                                 }
                                                 
                                             }else if(attrs.mwInputRestrict === "onlyAlphaNum"){
                                            	 var alphRegex = new RegExp("^[a-zA-Z0-9]+$"); 
                                                	
                                           	    
                                                 // allow only digits to be entered, or backspace and delete keys to be pressed
                                                 if (!alphRegex.test(str)){
                                              	   event.preventDefault()
                                                 }
                                             }
                                             
                                           });
                                       }
                                   }
                               }
                           ]);

dmt.directive("otpResendLink", function($scope, $timeout) {
	   $scope.counter = 0;
	    $scope.onTimeout = function(){
	        $scope.counter++;
	        mytimeout = $timeout($scope.onTimeout,1000);
	    }
	    var mytimeout = $timeout($scope.onTimeout,1000);
	    
	    $scope.stop = function(){
	        $timeout.cancel(mytimeout);
	    }
    return {
        template : "<h1>Made by a directive!{{counter}}</h1>"
    };
});
dmt.directive('capitalize', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
      }
    };
  });

/*$(function(){
	$(document).on('cut copy paste','body', function (e) {
        e.preventDefault();
    });
})*/

dmt.service('multipartForm', ['$http', function($http){
	this.post = function(uploadUrl, data){
		
		//console.log(data)
		
		var fd = new FormData();
		for(var key in data){
			fd.append(key, data[key]);
		}
		console.log(1)
		
		 $http({
		     method: 'POST',
		     url: uploadUrl,
		     data    :'data='+ "2", //forms user object
		     transformRequest: angular.indentity,
				headers: { 'Content-Type': undefined }
		   }).then(function(res){
			   console.log(res);
			  
		   },function(res){				   
			   console.log(res);
		   })
/*		$http.post(uploadUrl, "data="+JSON.stringify(fd), {
			transformRequest: angular.indentity,
			headers: { 'Content-Type': undefined }
		}).success(function(result){
				callback(result);
		}).error(function (data, status, headers, config) {
			console.log(status);
        });*/
	}
}])
dmt.directive('fileModel', ['$parse', function($parse){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				console.log(element[0].files[0])
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				})
			})
		}
	}
}])
dmt.factory('Excel',function($window){
		var uri='data:application/vnd.ms-excel;base64,',
			template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
			base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
			format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
		return {
			tableToExcel:function(tableId,worksheetName){
				var table=$(tableId),
					ctx={worksheet:worksheetName,table:table.html()},
					href=uri+base64(format(template,ctx));
				return href;
			}
		};
	})