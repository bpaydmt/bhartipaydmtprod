 <%@page import="java.util.Map"%>
<%@taglib prefix="s" uri="/struts-tags"%>
 <%Map<String,String> mapResult=(Map<String,String>)request.getSession().getAttribute("mapResult");
 String errMsg="";
 if(session.getAttribute("loginerr")!= null)
 {
	 errMsg =(String)session.getAttribute("loginerr");
	 session.removeAttribute("loginerr");
 }
 
 %>

      <div class="modal-body">
        <ul class="list-inline">
   	<li class="active"><a data-toggle="pill" href="#loginForm">Log In</a></li>
   	<li id="signUp-link"><a data-toggle="pill"  href="#registerForm">Sign Up</a></li>
   		<!-- <li><a data-toggle="pill"  href="#forgetPwd">Forgot Password</a></li> -->
  </ul>
      <div class="tab-content">
 
      <div id="loginForm" class="tab-pane fade in active">
      <form method="post" name="login" id="login_form" action="Login.action">
      
      <font color="red"><%=errMsg %></font>
      <%if(session.getAttribute("msg") != null) { %>
  <font color="green"><%=(String)session.getAttribute("msg") %></font><% }else if(session.getAttribute("errmsg") != null){%> <font color="red"><%=(String)session.getAttribute("errmsg") %></font><%} %>
<%session.removeAttribute("msg");
session.removeAttribute("errormsg");
%>
			<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
											
	<%session.putValue("custType", "1");
	if(session.getAttribute("loginStatus") != null && ((String)session.getAttribute("loginStatus")).equalsIgnoreCase("logoff"))
	{%>
	
	<span style="color:red">Your session has been expired. Please login again.</span>
	<%
	session.removeAttribute("loginStatus");
	} %>
			<input type="text" name="user.userId" id="username"
				class="payplutus_textbox_login mandatory email-mobile no-space" autocomplete="off" required
				placeholder="Email/Mobile number">
<div class="errorMsg"></div>
			<input type="password"   name="user.password" id="password"
				class="payplutus_textbox_login mandatory" autocomplete="off" required
				placeholder="Password">
				<div class="errorMsg" ></div>
		
		
		
						
		
	</form>
	<button class="btn-block primary-btn submit-form" onclick="submitVaForm('#login_form',this)">Log In </button>
	</div>
	
	<div id="registerForm"  class="tab-pane fade clearfix">
			<form method="post" name="register" id="register1"
		action="SaveSignUpUser1.action">			                        
		<input type="hidden" value="1" name="custType">

					
		<%
		String signUpErr="";
		if(session.getAttribute("errorsignup") != null && ((String)session.getAttribute("errorsignup")).equalsIgnoreCase("errorsignup") && session.getAttribute("errmsg") != null)
		{
			signUpErr=(String)session.getAttribute("errmsg");
			session.removeAttribute("errorsignup");
			session.removeAttribute("errmsg");
		}
		
		%>
		<font color="red"><%=signUpErr %></font>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
					<input type="hidden" name="user.usertype" value="1">
				<input	class="form-control" name="user.aggreatorid" id="wwdw" type="hidden" value="-1" />															
				<input	class="form-control" name="user.distributerid" type="hidden" value="-1" />
				<input	class="form-control" name="user.agentid" type="hidden" value="-1" />
				<input	class="form-control" name="user.subAgentId" type="hidden" value="-1" />
			<input type="text" name="user.name" id="name"
				class="payplutus_textbox_login userName mandatory" autocomplete="off" 
				placeholder="Your Name" value="<s:property value='%{user.name}'/>">
				<div class="errorMsg" ></div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="email" name="user.emailid" id="email"
				class="payplutus_textbox_login emailid mandatory" autocomplete="off" 
				placeholder="Your Email" value="<s:property value='%{user.emailid}'/>">
				<div class="errorMsg" ></div>
		</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<select class="mandatory" name="user.documentType" id="documentDropDown" onchange="changeTheDocType(this)">
				<option value="-1">Please Select Document Type</option>
			<option value="1">Passport</option>
				<option value="2">Voter ID</option>
				<option value="3">Aadhaar Card</option>
				<option value="4">Pan Card</option>
				<option value="5">Driving Licence </option>
			
			</select>
			
				<div class="errorMsg" ></div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="text" name="user.documentId" id="documentInp"
				class="payplutus_textbox_login mandatory" autocomplete="off" 
				placeholder="Id Number"  value="<s:property value='%{user.mobileno}'/>">
				<div class="errorMsg" ></div>
		</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="text" name="user.mobileno" id="mobile"
				class="payplutus_textbox_login mobile mandatory" autocomplete="off" 
				placeholder="Your Mobile No."  value="<s:property value='%{user.mobileno}'/>">
				<div class="errorMsg" ></div>
		</div>
<%

if(mapResult!=null&&mapResult.get("emailvalid")!=null&&Double.parseDouble(String.valueOf(mapResult.get("emailvalid")))==0)
{
%>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="password"   name="user.password" id="passw"
				class="payplutus_textbox_login password-vl mandatory" autocomplete="off" 
				placeholder="Password">
				<div class="errorMsg" ></div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="password"   name="user.confirmPassword" id="passwConfirm"
				class="payplutus_textbox_login confirm-vl mandatory" autocomplete="off" 
				placeholder="Confirm Password">
				<div class="errorMsg" ></div>
		</div>
<%
}
%>
	
	</form>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="button" onclick="submitVaForm('#register1',this)" id="signup" name="signup" value="Sign Up"
				class="btn-block primary-btn submit-form">
		</div>
	</div>
	
	<div id="forgetPwd" class="tab-pane fade clearfix">
		<form action="CForgotPwd.action" id="cforgetPwdForm" method="post">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="text"   name="user.userId" id="passwConfirm"
				class="mandatory email-mobile no-space" autocomplete="off" 
				placeholder="Email/Mobile number">
				<div class="errorMsg" ></div>
		</div>
		
		</form>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="button" onclick="submitVaForm('#cforgetPwdForm',this)" value="Submit"
				class="btn-block primary-btn submit-form">
		</div>
	
	</div>
      </div>
      </div>
      <script>
      function changeTheDocType(elm){
    	  var v =  $(elm).val();
    	  var pl = $("#documentInp")
    	 
    	  switch(v){
    	  case "1":
    		  pl.attr("placeholder","Your Passport No.");
    		  break;
    	  case "2":
    		  pl.attr("placeholder","Voter ID No.");
    		  break;
    	  case "3":
    		  pl.attr("placeholder","Your Adhaar No.");
    		  break;
    	  case "4":
    		  pl.attr("placeholder","Your Pan No.");
    		  break;
    	  case "5":
    		  pl.attr("placeholder","Your Driving Licence  No.");
    		  break;
    	 default:
    		 pl.attr("placeholder","Your ID Number");
    		  
    		  
    		  
    	  
    	  
    	  }
      }
      
      
      
      </script>
     