<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
//User user=(User)session.getAttribute("User");
User user=new User();


%>

<script>
$(function(){
	 $("#datacard_operator, #datacard_circle").msDropDown();
	// appendErrorDiv("#eleOpretor")
})
</script>
<h4 class="form-heading">Recharge your Data card easy &amp; secure</h4>
	
				<form method="post" class="oxyForm" id="datacard" action="RechargeService.action">
				<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
					
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					
					<div class="form-group clearfix payplutus_mt10">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input type="radio" name="rechargeBean.rechargeType"   class="rechargetypeDataCard" value="Datacard" checked ><label class="tab">Prepaid</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	                            <input type="radio" name="rechargeBean.rechargeType" class="rechargetypeDataCard"  value="Postpaid-Datacard" ><label class="tab">Postpaid</label>
	                        </div>
                    </div>
					<div class="form-group payplutus_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="datacard_mobile"
							class="payplutus_textbox onlyNum mandatory" required
							placeholder="Enter your datacard number" onkeyup="getOpretorDetails(this,'datacard')" maxlength="10" />
					</div>
					<div class="form-group payplutus_mt10">
						<s:select list="%{prePaidDataCard}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory" id="datacard_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);validateSelectOnChange(this,'#datacardErr')"/>

						
						</select>
						<div class="errorMsg customSelectErr" id="datacardErr" style="display: none;">Please select the operator</div>
						
					</div>
					<div class="form-group payplutus_mt10">
						
						<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown mandatory" id="datacard_circle" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);;validateSelectOnChange(this,'#datacardCireclrErr')"/>
						
						<div class="errorMsg customSelectErr" style="display: none;" id="datacardCireclrErr">Please select the circle</div>		
					</div>
					<div class="twoFields">
						
							<div class="half-width pull-left">
							
						<input type="text" name="rechargeBean.rechargeAmt" id="datacard_amount"
							class="payplutus_textbox amount-vl mandatory" required placeholder="Enter amount"
							maxlength="4" />
					</div>
					<div class="half-width pull-right">
				<input type="button" name="proceed"
							value="Proceed to Pay"  class="oxySubmitBtn"  data-open-popup="datacard" onclick="submitVaForm('#datacard',this)">
							</div>
				
				
				</div>
				</form>