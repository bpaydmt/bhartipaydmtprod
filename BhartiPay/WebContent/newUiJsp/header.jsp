<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.UserSummary"%>
<%@page import="java.text.DecimalFormat"%>
<%
User user = (User) session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
DecimalFormat decimalFormat = new DecimalFormat("0.00");
%>

<header>
<div class="topheader">

<ul class=" pull-right list-inline">
<li><a href="#">Accept Payment</a> </li>
<li><a href="#">Contact Us</a> </li>
<%
	if(user != null && user.getId() != null){%>

<li><a href="javascript:void(0)">Wallet Balance <%=user.getFinalBalance() %> </a> </li>

<li class="dropdown oxyHeaderDropdown">
  <a href="javascriot:void(0)" class=" dropdown-toggle" type="button" data-toggle="dropdown" ><%=user.getName() %>
  <span class="caret"></span></a>
  <ul class="dropdown-menu">
      <li><a href="#"><%=user.getMobileno() %> <br><%=user.getEmailid() %>  </a></li>
    <li><a href="#" onclick="getTransactionHistory()">Transaction History</a></li>
  <!--   <li><a href="#">Passbook</a></li> -->
     <li><a href="javascript:void(0)" onclick="getUserInfo()">My Account</a></li>
    <li><a href="Logoff.action">Log out</a></li>
  </ul>
 
</li>


		
		
		
		
		
	<%}else{
		%>

		<li><a href="javascript:void(0)" onclick="openLoginPopup('Login');">Log In/Sign Up</a> </li>
	<%}

%>

</ul>
</div>
<div class="innderHeader">

		<%if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>" alt="BhartiPay" class="oxyLogo" />
					<%
						}else{
					%>
					 <img alt="BhartiPay"  class="oxyLogo" src="./images/wallet_logo.png" />
						<%} %>
<%String errmsg="";
String msg="";
if(session.getAttribute("cAddMnyErr") != null)
{
	errmsg=(String)session.getAttribute("cAddMnyErr");
	session.removeAttribute("cAddMnyErr");
}
else if(session.getAttribute("cAddMnyMsg") != null)
{
	msg=(String)session.getAttribute("cAddMnyMsg");
	session.removeAttribute("cAddMnyMsg");
}

%>
						
						<div class="pull-right width5">
						
							<form action="custAddMoney" id="addMoney" class="oxyForm" method="post" name="PG">
							
																	<div class="form-group col-md-7 " style="position:relative">
																	<span style="    position: absolute;
    right: 5px;
    top: 40px;
    width: 797px;
    text-align: right;">			<font color="red"><%=errmsg %> </font>
	
									<font color="blue"><%=msg %></font>
																</span>
									 <input class="  mandatory amount-vl" name="inputBean.trxAmount" type="text"
																id="add-money-amount" maxlength="6" placeholder="Amount" />
													
																
									</div>

<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									<div class="col-md-5">
									<button type="button" style="margin: 0px 0 0 0;" onclick="submitVaForm('#addMoney',this)" class="oxySubmitBtn"
											 >Add Money
										</button>
									</div>
										
</form>
						
						</div>
</div>

</header>