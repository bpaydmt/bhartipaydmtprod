
  <h2>
Customer Grievances Policy
</h2>
<p>
In line with the efforts to deliver the best, use the ISO 9001:2015 standards to govern our day to day activities for a smooth and hassle free service experience to all our customers.</p>
<p>This policy document lists various means available for our customers to reach out to us, our service guarantee and timelines by which we will try and ensure resolution to our customer concerns.</p>
<h4>Key Points:</h4>
<ul>
<li>Customers remain the Key focus for all initiatives and strategies developed at BhartiPay.</li>
<li>Customers and their Feedback is treated as the most valuable asset for the organization. </li>
<li>We endeavor to simplify our customers life through our innovations and product offerings.</li>
<li>Constantly evolve and invest in our grievance redressing systems for a seamless service delivery.</li>

</ul>
<h4>Our Commitment:</h4>
<ul>
	<li>Grievances will be dealt with utmost importance.</li>
	<li>We will try best to resolve any or all issues faced by our customers within the communicated time frame.</li>
	
</ul>
<h4>We Value your Feedback:</h4>
<p>All customers have the right to share their feedback or complaint in case they find our services are not meeting their expectations or are dissatisfied.

</p>
<p>The Customers can send in their Queries, Requests or Complaints in the following ways:

</p>
<ul>
	<li>Over Phone - Customer Engagement Cell:-Customers can call us on 0120-4000-004 (Call Charges as applicable apply), between 08:00 AM to 8:00 PM, 7 Days working (National Holidays excluded).
	</li>
	<li>Over Email:- Sending us an email on <a href="mailto:care@BhartiPay.com"> care@BhartiPay.com</a>  </li>
	<li>Contact Us section:- Customers can choose the contact us section on our website <a href="https://b2b.BhartiPay.com/Home"> https://b2b.BhartiPay.com/Home</a> or using the contact us option in the APP.</li>

</ul>
<p>

On receiving customer feedback, our executives would reach out to the customers and ensure that all grievances are redressed within a predefined Service Level Agreement as communicated below.


</p>

<p><strong>Note - </strong>escalations without a complaint reference number will not be treated as complaints
Customer Resolution Timelines

</p>
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Serial No.</th>
        <th>Complaint Type</th>
        <th>Estimated Timelines (SLA)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><strong>1</strong></td>
        <td>Money Transfer - Load or Send Money</td>
        <td> 3 Working Days for reversal or credits (third party transaction timelines followed by leading banks)</td>
      </tr>
        <tr>
        <td><strong>2</strong></td>
        <td>Successful transaction but beneficiary account not credited</td>
        <td>3 - 10 Working Days for reversal or credits (third party transaction timelines followed by leading banks)</td>
      </tr>
       <tr>
        <td><strong>3</strong></td>
        <td>Recharge Transaction failure</td>
        <td>3 Working Days</td>
      </tr>
      <tr>
        <td><strong>4</strong></td>
        <td>Travel & Ticketing Related Concerns</td>
        <td>3 Working Days for booking related concerns 7 Days for refunds as some airlines and third-party portals may have a higher turnaround time</td>
      </tr>
      <tr>
        <td><strong>5</strong></td>
        <td>Bill payment Related Issues</td>
        <td>3 Working Days</td>
      </tr>
      <tr>
        <td><strong>6</strong></td>
        <td>Online Shopping Related Issue</td>
         <td>3 Working Days</td>
      </tr>
        <tr>
        <td><strong>7</strong></td>
        <td>Wallet related issues</td>
        <td>2 - 5 Working Days</td>
      </tr>
     
    </tbody>
  </table>
  
  <h4>
Acknowledgement of Grievances:
  </h4>
  
  <ul>
  	<li>Customer Engagement Cell will acknowledge the grievance immediately on the receipt of complaint in the form of : -
  		<ul>
  			<li>Auto response in case of Emails or Contact us section, or</li>
  		</ul>
  	
  	</li>
  
  
  </ul>
  <h4>In all the scenarios a unique reference number would be shared for all future communicatio</h4>
  <ul>
  	<li>The customer will also be kept informed on the progress towards the final resolution, or communicate any delays in redressing the concern</li>
  </ul>
  <h4>
  Customer Grievance Redress Escalation:
  
  </h4>
  <p>As "Customer Delight" our priority, we are committed to provide Best Payment Solution Experience to all our customers. We extend a level 2 escalation matrix to all our customers.
  </p>
  
  
  <h4>Escalation: Level 2</h4>
  
  <p>In case the customer is not satisfied with the</p>
  <ul>
  <li>Resolution provided by Level 1 executives</li>
  
<li>Breach in the above mentioned Service Level Agreements or timelines</li>
 
  	
  </ul>
  <p>The customer may choose to escalate the concern using the below mentioned methods</p>
  <ol>
  	<li>Write to us at:,</br>
BhartiPay</br>
C/O Bhartipay Services Pvt. Ltd,</br>
A100 Wing A, 1st Floor,Sector 4,</br>
Noida, Uttar Pradesh 201301
  	</li>
  <li>
  	Email ID: <a href="mailto:nodal@BhartiPay.com">nodal@BhartiPay.com</a>
  </li>
  
  </ol>
<p>All escalations received with the required details such as reference number provided at Level 1, contact details (both phone & email) would be addressed within forty eight (48) working hours up to a max of seven (7) days in special cases pertaining to third party transactions.</p>
 <h4>Escalation: Level 3</h4>
 
 <p>In case the customer is still not satisfied with the resolution provided or delay in response beyond the timelines communicated even after following the escalation steps at Levels 1 & 2 respectively, the customer can escalate the concern to the highest level by:</p>
  <ol>
  	<li>Write to us at:,</br>
BhartiPay</br>
C/O Bhartipay Services Pvt. Ltd,</br>
A100 Wing A, 1st Floor,Sector 4,</br>
Noida, Uttar Pradesh 201301
  	</li>
  <li>
  	Email ID: <a href="nitin@BhartiPay.com">: nitin@BhartiPay.com</a>
  </li>
  
  </ol>
  <p>On receiving the escalation the complaint will get acknowledged within twenty four (24) working hours post acknowledgement, up to a maximum of seven (7) working days in special cases pertaining to third party transactions</p>