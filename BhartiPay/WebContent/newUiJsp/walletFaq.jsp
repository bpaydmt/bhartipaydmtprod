<h2>FAQ's</h2>
<ol class="cfaqList">
	<li>
	<strong>Question:- </strong>Is registration for BhartiPay  wallet free?<br>
	<strong>Answer:- </strong>Yes! It's absolutely free, you do not need to spend any money  for registration.
	
	
	</li>
	<li>
	<strong>Question:- </strong>Is it secure to use BhartiPay  wallet?<br>
	<strong>Answer:- </strong>BhartiPay  wallet is BANK approved mobile wallet and every Transaction is secure using highest level of encryption.
	
	
	</li>
	<li>
	<strong>Question:- </strong>Do I get any alert for credit and debit transaction?<br>
	<strong>Answer:- </strong>BhartiPay  will keep you updated about Transaction or activity via SMS and email  notification.
	
	
	</li>
	<li>
	<strong>Question:- </strong>How do I add money to my BhartiPay  wallet?<br>
	<strong>Answer:- </strong>Money can be added through Credit/debit card, internet banking, UPI.
	
	
	</li>
	<li>
	<strong>Question:- </strong>Is there any minimum balance requirement?<br>
	<strong>Answer:- </strong>No! there is no such requirement.
	
	
	</li>
	<li>
	<strong>Question:- </strong>Is there any limit on maximum funds to be added?<br>
	<strong>Answer:- </strong>The Maximum limit is INR 10,000/- for Non- KYC customers and if you update to full KYC using aadhar then limit will be increased to INR 1,00,000/-.
	
	
<jsp:include page="walletLimits.jsp"></jsp:include>
	
	</li>
		<li>
	<strong>Question:- </strong>How can I check my BhartiPay  wallet balance?<br>
	<strong>Answer:- </strong>In case of mobile application, you need to login your wallet account,it will display on top of the mobile screen.
	
	</li>
	<li>
	<strong>Question:- </strong>What is the validity of my wallet?<br>
	<strong>Answer:- </strong>BhartiPay  wallet provides many services including Recharge/Bill payments/ DTH/ Flights/Hotels booking/Bus ticketing/Electricity bill/Cabs/Fund Transactions. you can choose any of the services for the wallet balance.
	
	</li>
	<li>
	<strong>Question:- </strong>Wrong mobile number recharged, can I get my money back?<br>
	<strong>Answer:- </strong>if once recharge successfully done, then it cannot reverse into BhartiPay  wallet.
	</li>
	
	<li>
	<strong>Question:- </strong>Amount deducted but recharge not done.<br>
	<strong>Answer:- </strong>This is the very rare case, if it happens, you need to wait for a couple of hours, BhartiPay  team will update you, or you can get in touch with us on 120-4000004.
	</li>
	
	<li>
	<strong>Question:- </strong>I want my money in my bank account via wallet?<br>

	<strong>Answer:- </strong>As per standard bank procedure, it will take 24 working hours to reflect the amount in your respective bank account/card statement, also you can use our “wallet to bank” feature to avail the same.	</li>
	
	<li>
	<strong>Question:- </strong>Money added but not reflecting in the wallet.<br>

	<strong>Answer:- </strong>please wait for 30 minutes after that please provide us the details as mentioned 
		<ol>
			<li>Transaction id</li>
			<li>Transaction amount</li>
			<li>Registered email address</li>
			<li>Registered contact number</li>
		</ol>
	</li>
	<li>
	<strong>Question:- </strong>Wants to close account.<br>


	<strong>Answer:- </strong>if you want to close your account, you need to write us on care@BhartiPay.com, we'll check the unused amount and refund the amount to your bank account.</li>
	<li>

	<strong>Question:- </strong>Can I send money to a mobile number outside India using BhartiPay  wallet?<br>


	<strong>Answer:- </strong> No!! we are providing services in all over India only.	</li>
	<li>
	<strong>Question:- </strong>What are the charges will deduct to transfer the amount through wallet to a bank account?<br>


	<strong>Answer:- </strong>if you will transfer the amount through wallet to bank account then it will deduct 3% (minimum 5 rupees) + GST.	</li>
	<li>

	<strong>Question:- </strong>When I'll get refund to my bank account.<br>


	<strong>Answer:- </strong>As per standard bank procedure, it will take 5 to 20 working days to reflect the refund in your respective bank account/card statement.	</li>
	
	

</ol>