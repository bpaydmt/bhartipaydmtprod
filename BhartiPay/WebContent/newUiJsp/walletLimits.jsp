<table class="table table-bordered">
    <thead>
      <tr>
        <th>Serial No.</th>
        <th>Particulars</th>
         <th>Limits Minimum KYC Wallet</th>
          <th>Limits Full KYC Wallet</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td>Non-KYC Wallet</td>
         <td>Rs. 10,000**/- Monthly Wallet Limit</td>
        <td></td>
      </tr>
         <tr>
        <td>1</td>
        <td>Maximum Balance limit</td>
         <td>Rs. 10,000**/-</td>
        <td>Rs. 1,00,000/-</td>
      </tr>
      <tr>
        <td>2</td>
        <td>Money Transfer limits</td>
         <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td>Single Transaction Limit</td>
         <td>NIL</td>
        <td>Rs. 25,000/-</td>
      </tr>
       <tr>
        <td></td>
        <td>Daily Transaction Limit</td>
         <td>NIL</td>
        <td>Rs. 49,999/-</td>
      </tr>
       <tr>
        <td></td>
        <td>Monthly limit</td>
         <td>NIL</td>
        <td>Rs. 1,00,000/-</td>
      </tr>
          <tr>
        <td>3</td>
        <td>Transaction limits</td>
         <td></td>
        <td></td>
      </tr>
        <tr>
        <td></td>
        <td>Single Transaction Limit</td>
         <td></td>
        <td></td>
      </tr>
        <tr>
        <td></td>
        <td>Recharge : Mobile /Data Card</td>
         <td>Rs. 2500/-</td>
        <td>Rs. 2500/-</td>
      </tr>
       <tr>
        <td></td>
        <td>DTH Recharge</td>
         <td>Rs. 10,000/-</td>
        <td>Rs. 10,000/-</td>
      </tr>
       <tr>
        <td></td>
        <td>Bill payment: Mobile/DTH/Data card</td>
         <td>Rs. 5000/-</td>
        <td>Rs. 5000/-</td>
      </tr>
      <tr>
        <td></td>
        <td>Electricity Bill payment</td>
         <td>Rs. 10,000/-</td>
        <td>Rs. 10,000/-</td>
      </tr>
      <tr>
        <td></td>
        <td>Daily Transaction Limit</td>
         <td>Rs. 10,000**/-</td>
        <td>Rs. 1,00,000/-</td>
      </tr>
      <tr>
        <td></td>
        <td>Monthly limit</td>
         <td>Rs 10,000**/-</td>
        <td>Rs. 1,00,000/-</td>
      </tr>
    </tbody>
  </table>
  
  
 <em>**Rs. 10000 is the limit as per the BANK guidelines, unless amended.</em>