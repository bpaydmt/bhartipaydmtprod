<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
//User user=(User)session.getAttribute("User");
User user=new User();

%>
<script>
$(function(){
	 $("#eleOpretor").msDropDown();
	 appendErrorDiv("#eleOpretor")
})
</script>
<h4 class="form-heading">Pay your Electricity bill easily</h4>
			<form method="post" class="oxyForm" id="electricityForm" data-btc-form="true" action="CRBillerDetails.action" >
			<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					
							<input type="hidden" name="billDetails.billerType" value="Electricity" > 
							
										
											 <%-- 
											        <div>
											            <span>Employee ID: <c:out value="${resList.billerId}"/></span>
											            <span>Employee Pass: <c:out value="${resList.billerName}"/></span>  
											        </div> --%>
											
							
					<div class="form-group payplutus_mt10">						
					<select class="payplutus_dropdown mandatory" id="eleOpretor" name="billDetails.billerId" onchange="electricityProviderChange()">
						<option value="-1">Select Operator</option>
						   <c:forEach items="${resp}" var="resList">
						<option value="${resList.billerId}" data-image=".${resList.billerIcon }">${resList.billerName }</option>
						    </c:forEach>
								
						</select>
						<input type="hidden" name="rechargeBean.rechargeType" value="dsd">
					
					
					
						<span class="errorMsg customSelectErr" id="oprErr" style="display: none;"></span>
					</div>
					
				<div class="form-group payplutus_mt10">
						<input type="text"  id="electricity_consumer"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Consumer Number"  name="billDetails.number" />
					</div>
					<div class="form-group payplutus_mt10" id="eleAccount" style="display:none;">
						<input type="text"  id="electricity_account"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Consumer Number" name="billDetails.account"  />
					</div>
						<div class="form-group payplutus_mt10" id="elethirdOption" style="display:none;">
						<select id="thirdEleOption" class="payplutus_dropdown mandatory" name="billDetails.additionalInfo"></select>
							
					</div>
					
					<div class="form-group payplutus_mt10" >
						<input type="Submit" name="proceed" 
							value="Proceed to Pay" class="oxySubmitBtn"  onclick="electricityFormSubmit(event)"  >
					</div>
				
				</form>