<%@page import="com.bhartipay.biller.vo.PayLoadBean" %>

<%
PayLoadBean resPayload=(PayLoadBean)session.getAttribute("resPayLoad");
String msg = resPayload.getMessage();
if(msg.equals("300")){
	%>
	<h5 class="responseMsg"></h5>

	<table class="table table-bordered" id="billerTable">

		<tbody>
			<tr>
				<td><strong>Biller Type</strong></td>
				<td><%=resPayload.getBillerType()%></td>
			</tr>
			<tr>
				<td><strong>Wallet ID</strong></td>
				<td><%=resPayload.getWalletUserId()%></td>
			</tr>
			<tr>
				<td><strong>Biller Account</strong></td>
				<td><%=resPayload.getBillerAccount()%></td>
			</tr>
			<tr>
				<td><strong>TXN Id</strong></td>
				<td><%=resPayload.getTxnId()%></td>
			</tr>
			<tr>
				<td><strong>Amount</strong></td>
				<td><%=resPayload.getBillerAmount()%></td>
			</tr>
				</tbody>
	</table>
<%

	
}else{%>

<h3 style="color:red">
	Oops something went wrong. Please try after sometime.
</h3>
<%
	
}
%>

