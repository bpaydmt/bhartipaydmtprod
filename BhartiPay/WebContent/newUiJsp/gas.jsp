<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<script>
$(function(){
	 $("#gas-opretor").msDropDown();
	 appendErrorDiv("#gas-opretor")
})
</script>
<h4 class="form-heading">Pay your gas bill easily</h4>






				<form method="post" id="gasRecharge" action="GetBillOperatorDetailsB2C.action" data-btc-form="true" class="oxyForm" >
				<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				<input type="hidden" name="billDetails.billerType" value="Gas" > 
				<div class="form-group payplutus_mt10">
				
				<select class="payplutus_dropdown mandatory" id="gas-opretor"  onchange="gasOpratorChange(this)" name="billDetails.billerId">
						<option value="-1">Select Operator</option>
						   <c:forEach items="${resp}" var="resList">
						<option value="${resList.billerId}" data-image=".${resList.billerIcon }">${resList.billerName }</option>
						    </c:forEach>
								
						</select>
						
						<span id="gas-provider" class="errorMsg customSelectErr" style="display: none;">Please Select The Service Provider.</span>
					</div>
					
				<div class="form-group payplutus_mt10">
						<input type="text"  id="gas-consumer"  name="billDetails.number"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Operator Number"   />
							<span  class="errorMsg customSelectErr" style="display: none;"></span>
					</div>
						<div class="form-group payplutus_mt10"  id="accoundBill" style="display:none">
						<input type="text"  id="accoundBillInp"  name="billDetails.account"
							class="payplutus_textbox alphaNum-vl " 
							placeholder="Enter Operator Number" maxlength="8"  />
							<span  class="errorMsg" style="display: none;"></span>
					</div>
				<div class="form-group payplutus_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="oxySubmitBtn" onclick="validateGasForm(event)"  >
					</div>
				</form>


