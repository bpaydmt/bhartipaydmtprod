<%----%>
<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>
<%@page import="java.util.ArrayList"%>

<%
//User user=(User)session.getAttribute("User");
User user=new User();
%>
<script>

$(function(){
	 $("#circlei, #rechargeOperator").msDropDown();
	 //appendErrorDiv("#eleOpretor")
})
</script>
<h4 class="form-heading">Recharge your Mobile easy & secure</h4>
			<form method="post" id="mobile_form" class="oxyForm" action="RechargeService.action">
			<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					
					<div class="form-group clearfix payplutus_mt10">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" class="rechargeTypeRadio" name="rechargeBean.rechargeType"
								value="PREPAID" checked  ><label class="tab">Prepaid</label>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" class="rechargeTypeRadio" name="rechargeBean.rechargeType" id="connection_type"
								value="POSTPAID"><label class="tab">Postpaid</label>
						</div>
					</div>
				<div class="form-group payplutus_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" 
							class="mandatory mobile" 
							placeholder="Enter your mobile number" 
							autocomplete="off"  id="rechargeMobile" maxlength="10" onkeyup="getOpretorDetails(this,'mobile')"  />
				
						
						
					
					<div class="errorMsg" style="display: none;"></div>
					</div>
					<div class="form-group payplutus_mt10">
					
			
	
	
						<s:select list="%{prePaid}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown  " id="rechargeOperator" headerKey="-1" headerValue="Please select operator" onChange="validateSelectOnChange(this,'#opratorErr')"/>
	
	<div class="errorMsg customSelectErr " id="opratorErr" style="display: none;">Please select the operator</div>
	</div>
	
	<div class="form-group payplutus_mt10">

<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass=" " id="circlei" headerKey="-1" headerValue="Please select a circle" onChange="validateSelectOnChange(this,'#opratoCireclrErr')"/>

					<div class="errorMsg customSelectErr" id="opratoCireclrErr" style="display: none;">Please select the circle</div>	
					</div>
					<div class="twoFields">
						<div class="half-width pull-left">
						<input type="text" name="rechargeBean.rechargeAmt" id="amount"
							class="   amount-vl mandatory" 
							placeholder="Enter amount" maxlength="4" />
					<div class="errorMsg" style="display: none;"></div>	
						</div>
						<div class="half-width pull-right">
							<a href="javascript:void(0)" data-open-popup="rechage" data-b2c="true" onclick="submitVaForm('#mobile_form',this)" class="oxySubmitBtn"> Proceed</a>
							
							</div>
					
					</div>
						
						
				
				
				</form>