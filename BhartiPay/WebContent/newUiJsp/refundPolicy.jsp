<h2>Refund Policy</h2>

<p><a href="https://www.bhartipay.com/">bhartipay.com</a> processes all refunds within 48hrs.</p>
<p>Please note:  banks may take 5-20 working days to process the refunds.</p>
<p><strong>Who can get a refund?</strong></p>
<p>BhartiPay provides refund in case of successful payment, and payment is debited from BhartiPay balance but user is not able to get service.</p>
<p><strong>How to request a refund?</strong></p>
<p>Raise a ticket / call or mail to customer support : <a href="mailto:care@BhartiPay.com">care@BhartiPay.com</a></p>
