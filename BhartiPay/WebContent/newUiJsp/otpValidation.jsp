<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%
	String mobileNo = (String)session.getAttribute("mobileNo");
	session.removeAttribute("mobileNo");
%>
<div class="container" style="width:400px;margin-top:100px" >
  
  <form action="WalletPaymentConfirm.action" method="post" >
      <div class="form-group">
      <label for="usr">Mobile No: <%=mobileNo %></label>
      
    </div>
    <div class="form-group">
      <label for="usr">Enter OTP:</label>
      <input type="text" class="form-control" name="otp" id="usr">
    </div>
   <input type="submit" class="btn btn-primary btn-block" />
  </form>
</div>

</body>
</html>