<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<script>
$(function(){
	 $("#landline_operator").msDropDown();
	 appendErrorDiv("#landlineOption")
})
</script>
<h4 class="form-heading">Pay your landline bill  easily &amp; securely</h4>
	
	
	
	
	<form method="post" id="landline_bill"  class="oxyForm" data-btc-form="true" action="CRBillerDetails.action"  >
	<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
					<input type="hidden" name="billDetails.billerType" value="Landline" >
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				
			
               
                    	<div class="form-group payplutus_mt10">
                    		
                    	
                	<select class="payplutus_dropdown mandatory" data-btc-form="true" onChange="landlinServiceProviderOnChange(this)"  name="billDetails.billerId" id="landline_operator" >
						<option value="-1">Select Operator</option>
						   <c:forEach items="${resp}" var="resList">
						<option value="${resList.billerId}" data-image=".${resList.billerIcon }">${resList.billerName }</option>
						    </c:forEach>
								
						</select>
						<span class="errorMsg" id="oprErr" style="display: none;"></span>
							
						<input type="hidden" name="rechargeBean.rechargeType" value="dsd">
					
					
					
						
						<%-- <s:select list="%{landLine}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory" onChange="landlinServiceProviderOnChange(this)" id="landline_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
 --%>
					<div class="errorMsg" id="landlineError" style="display: none;"></div>
					</div>
                    <div class="form-group payplutus_mt10">
						<input type="text" id="landlineNum"
							class="payplutus_textbox onlyNum mandatory "     name="billDetails.number" placeholder="Enter Phone Number"
							maxlength="11" />
							<div class="errorMsg" style="display: none;"></div>
					</div>
			
			  <div class="form-group payplutus_mt10" id="landlineAmountWrapper">
						<input type="text"  id="landlineAmount"
							class="payplutus_textbox onlyNum mandatory "   maxlength="5"  name="billDetails.amount" placeholder="Enter amount" />
					</div>
				
					<div class="form-group payplutus_mt10" id="landineAddInfoWrapper" style="display:none">
						<input type="text"  id="landline_add_info"
							class="payplutus_textbox onlyNum mandatory" name="billDetails.account"  placeholder="Customer Account Number"
							maxlength="10" />
					</div>
						<div class="form-group payplutus_mt10" id="Authenticator" style="display:none" >
                    	
                    	<select class="payplutus_dropdown mandatory"   name="billDetails.additionalInfo">
                    		<option value="-1">Please Select Authenticator</option>
                    		<option value="LLI">LLI</option>
                    		<option value="LLC">LLC</option>
                    		
                    	
                    	</select>
					
					<div class="errorMsg" id="AuthenticatorError" style="display: none;"></div>
					</div>
						<div class="form-group payplutus_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" onclick="validateLandlineForm(event)" class="oxySubmitBtn"  />
					</div>
				</form>
	
	
	
	
	
	
	
	
	