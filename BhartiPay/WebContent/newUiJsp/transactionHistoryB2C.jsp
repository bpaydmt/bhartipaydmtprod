
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.List" %>
<%@page import="com.bhartipay.wallet.report.bean.TravelTxn" %>
<script>
$(document).ready(function() {

	     
	    
    $('#transactionHistory').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Transaction History',
              
            },  {
             
                extend: 'csv',
                text: 'CSV',
              
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Transaction History',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Transaction History',
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Transaction History',
              
            }
        ]
    } );
} );
   </script>
   <div class="row">
   
   
   
 
   
   <h2 class="oxyHeading col-md-12">
   	<span>
  
   Transaction History
    	</span>
   </h2>

 <div class="table-responsive col-md-12">          
  <table class="table" id="transactionHistory">
    <thead>
      <tr>
   <!--       <th>Transaction Details</th> -->
    
      <th>Date</th>
        <th> Description</th>
       
      
        <th>Amount</th>
        <th>Status</th>
        
      </tr>
    </thead>
    <tbody>
				<%
							List<TravelTxn> txn=(List<TravelTxn>)request.getAttribute("listResult");

				if(txn!=null){
				
				
				for(int i=0; i<txn.size(); i++) {
					TravelTxn tdo=txn.get(i);%>
		          		  <tr>
		          		 <%--  	<td><%=tdo.getDetails() %></td> --%>
		          		<%--  <td><%=tdo.getTransid() %></td> --%>
		          	<td  onclick="openTransactionDetails('<%=tdo.getDetails() %>','<%=tdo.getType() %>','<%=tdo.getTransid()%>','<%=tdo.getTxndate()%>','<%=tdo.getAmount()%>')">
		          	<%=tdo.getTxndate() %>	          			     

		          		
		          			</td>
		         		
		         		
		         		<td><%if(tdo.getType().equalsIgnoreCase("Recharge")) {
		         		StringTokenizer str = new StringTokenizer(tdo.getDetails(),"|");
		         		String opr =str.nextToken();
		         		if(opr.equalsIgnoreCase("PREPAID") || opr.equalsIgnoreCase("POSTPAID")){%><%="Recharge (Mobile)" %><%}else{%>
		         		<%=opr%><%}}else{%>
		         		<%=tdo.getType()%> 
		         		<%} %> </td>
		         		
		         		
		         	
		         		<td>Rs. <%=tdo.getAmount() %></td>
		         		<td  ><span class="<% if(tdo.getStatus().equalsIgnoreCase("Pending")){%> txtPending <% }else if(tdo.getStatus().equalsIgnoreCase("Success")){%> txtSuccess <%}else{ %> txtFailed <%} %>"> <%=tdo.getStatus()%></span></td>
		         		
		    
		          
                  		  </tr>
			      <%}}%>	
			        </tbody>
  </table>
  </div>
  
    </div>
   <jsp:include page="../newUiJsp/cfooter.jsp"></jsp:include>