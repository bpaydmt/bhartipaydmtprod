<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TxnInputBean" %>

<%TxnInputBean inputBean=new TxnInputBean();
String errorMsg="";
String successMsg="";
if(session.getAttribute("errorMsg") != null)
	{ errorMsg = (String)session.getAttribute("errorMsg");
		session.removeAttribute("errorMsg");
	}
else if(session.getAttribute("successMsg") != null)
{ successMsg = (String)session.getAttribute("successMsg");
session.removeAttribute("successMsg");
}

%>
<form  method="post" action="CWT" id="walletToWalletForm">
	<%-- <font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font> --%>
		<font color="red"><%=errorMsg %></font>
		<font color="blue"><%=successMsg %></font>
	<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
	<div class="form-group">
	<input type="text" placeholder="Enter your mobile number"  maxlength="10" class="mobile mandatory" name="inputBean.mobileNo" />
<span class="errorMsg "  style="display: none;"></span>
	</div>
<div class="form-group">
<input type="text" placeholder="Enter Amount" class="onlyNum mandatory" maxlength="4" name="inputBean.trxAmount" />
<span class="errorMsg "  style="display: none;"></span>
</div>
	<div class="form-group">
<input type="text" name="inputBean.txnReasion"  class="form-contol "  placeholder="Remark (Optional)"  />
<span class="errorMsg "  style="display: none;"></span>

</div>


</form> 
<button class="oxySubmitBtn" onclick="submitVaForm('#walletToWalletForm',this)">Proceed</button>