<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div class="leftCol">
<ul class="inline">
	<li><a href="javascript:void(0)" id="rechargeLink" onclick="getPage('Recharge','#form-section',this,'CRBiller')">Recharge <span class="oxy-icon-iphone"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)" id="DthLink" onclick="getPage('DTH','#form-section',this,'CRDTH')">DTH <span class="oxy-icon-satellite-dish"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)" id="dataCardLink"  onclick="getPage('DataCard','#form-section',this,'CRDataCard')">Data Card <span class="oxy-icon-datacard" ></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)" id="electricityLink"  onclick="getPage('Electricity','#form-section',this,'CRElectricity')" id="electricityLink">Electricity <span class="oxy-icon-electricity"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)"  id="landlineLink"  onclick="getPage('Landline','#form-section',this,'CRLandline')" id="landlineLink">Landline <span class="oxy-icon-landline"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)" id="gasLink"  onclick="getPage('Gas','#form-section',this,'CRGas')" id="gasLink">Gas <span class="oxy-icon-gas"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)"  id="insuranceLink" onclick="getPage('Insurance','#form-section',this,'CRInsurance')" id="insurancLink">Insurance <span class="oxy-icon-insurance2"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)"  id="offerLink" onclick="getPage('Offers','#form-section',this)">Offers <span class="oxy-icon-percentage"></span><span class="rightBorder"></span></a></li>
	<li><a href="javascript:void(0)"  id="acceptPaymentLink" onclick="getPage('AcceptPayment','#form-section',this)">Accept Payment <span class="oxy-icon-shop"></span><span class="rightBorder"></span></a></li>


</ul>



</div>