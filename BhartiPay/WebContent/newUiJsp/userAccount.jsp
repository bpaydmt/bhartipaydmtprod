<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean" %>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.UserSummary"%>

<% User user = (User)session.getAttribute("User"); 
WalletMastBean walletmast = (WalletMastBean)session.getAttribute("custProfile");
%>
<%
String msg="";
if(session.getAttribute("editMsg") != null)
{
msg=(String)session.getAttribute("editMsg") ;
session.removeAttribute("editMsg");
}
%>


      <form id="saveEditChanges" action="CEditProfile.action" method="post" class="oxyForm">

<div class="userInfoWrapper">

<div class="userImg row">
<div class="col-md-12"><font class="clearfix" style="font-size: 17px;
    margin: 16px 0 6px 16px;
    color: green;"><%=msg %></font></div>


<img src="data:image/png;base64, <%=user.getUserImg() %>" />
<div class="userChangeBtnWrapper" style="width: 200px;
    margin: auto;">
<a href="javascript:void(0)" class="oxySubmitBtn" id="EditProfile" onclick="userProfileEdit()" >Edit Profile</a>
<input type="submit" style="display:none;" class="oxySubmitBtn" id="savEditedChanges" value="Save Change(s)" />
</div>
<input type="hidden" name="walletBean.id" value=<%=user.getId() %> />
<input type="hidden" name="walletBean.pan" value=<%=walletmast.getPan() %> />
<input type="hidden" name="walletBean.adhar" value=<%=walletmast.getAdhar() %> />
<input type="hidden" name="walletBean.userstatus" value="A" />

</div>

<div class="row marginTopBottom5">
<div class="col-md-6">
<div class="form-wrapper userAccountTile ">
<div  class="col-md-12">
<h4 class="form-heading">Personal Information</h4>
</div>
<div class="fieldRow">

<div  class="col-md-6">
<strong>
User Name
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=user.getName() %>
</span>
<input type="text" name="walletBean.name" value="<%=user.getName() %>" class="editInp" />


</div>
</div>

<div class="fieldRow">
<div  class="col-md-6">
<strong>
Mobile
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=user.getMobileno() %>
</span>
<input type="text"  name="" value="<%=user.getMobileno() %>" class="editInp" disabled="disabled" />


</div>

</div>

<div class="fieldRow">
<div  class="col-md-6">
<strong>
Email Id
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=user.getEmailid() %>
</span>
<input type="text"  name="" value="<%=user.getEmailid() %>" class="editInp" disabled="disabled" />

</div>
</div>



</div>


</div>
<div class="col-md-6">
<div class="form-wrapper userAccountTile ">
<div  class="col-md-12">
<h4 class="form-heading">Address Details</h4>
</div>
<div class="fieldRow">

<div  class="col-md-6">
<strong>
Address 
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=walletmast.getAddress1() %>
</span><input type="text"  name="walletBean.address1" value="<%=walletmast.getAddress1() %>" class="editInp" />
</div>
</div>
<div class="fieldRow">
<div  class="col-md-6">
<strong>
Area 
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=walletmast.getAddress2()%>
</span><input type="text"  name="walletBean.address2" value="<%=walletmast.getAddress2() %>"   class="editInp" />
</div>
</div>
<div class="fieldRow">
<div  class="col-md-6">
<strong>
City 
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=walletmast.getCity()%>
</span><input type="text"  name="walletBean.city" value="<%=walletmast.getCity()%>"   class="editInp" />
</div>
</div>


<div class="fieldRow">
<div  class="col-md-6">
<strong>
State 
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=walletmast.getState()%> 
</span><input type="text"  name="walletBean.state" value="<%= walletmast.getState()%>" class="editInp" />
</div>
</div>
<div class="fieldRow">
<div  class="col-md-6">
<strong>
Pincode 
</strong>
</div>
<div  class="col-md-6">
<span class="staticField">
<%=walletmast.getPin()%>
</span><input type="text"  name="walletBean.pin"  value="<%=walletmast.getPin()%>" class="editInp" />
</div>
</div>
<div class="fieldRow">
<div  class="col-md-6">
<strong>
Created On
</strong>
</div>
<div  class="col-md-6">
<span class="static">
<%=walletmast.getCreationDate() %>

</span>

</div>
</div>





</div>


</div>
</div>




<div class="row marginTopBottom5">









</div>










<div class="row marginTopBottom5">

</div>

<div class="row marginTopBottom5">

</div>


<div class="row marginTopBottom5">

</div>

<div class="row marginTopBottom5">

</div>


</div>
</form>