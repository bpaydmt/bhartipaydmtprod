<h2>
Terms and Conditions
</h2>


<p>Welcome to BhartiPay Wallet. </p>
<p>
Your use of BhartiPay  Wallet on our Website or Mobile App is governed by the following terms and conditions (Terms) which are incorporated herein by way of reference. The Terms are effective upon acceptance of usage of our platform, and governs the relationship between the users of this platform ("You", "Your", "Yours" or the "User" as the context may necessitate) and 
<strong>Appnit Technologies Pvt Ltd.</strong>("BhartiPay Wallet", "Us", "Our" or "We" as the context may necessitate.
</p>
<p>
By accepting the Terms, You understand and agree that BhartiPay 
 Wallet will treat Your use of the Services such as your use of our prepaid mobile/DTH recharge purchasing services or Services regarding bill payment or money transfer or services regarding Our online marketplace or such other services which may be added from time to time (all such services are individually or collectively are referred as Service or Services) on the Site <a href="https://www.bhartipay.com/" target="_blank">(BhartiPay.com)</a>, Our mobile Platform/Network and Our mobile application (Related Applications).
</p>
<p>You specifically agree that in order to facilitate the provision of BhartiPay Wallet, it may be required to disclose any information or particulars pertaining to You to any authority, statutory or otherwise. Privacy of communication is subject to the terms of the certificate of authorization granted by the Reserve Bank of India and Reserve 
Bank of India notifications/directives etc.</p>
<p>We, or Our licensors, own all right, title and interest, including, but not limited to all copyright, trademark, patent, trade secret or other proprietary rights ("IP rights"), in and to the site, the platform, the usage data. You shall not reproduce, distribute, transmit, modify, create derivative works, display, perform or otherwise use the Site, the platform or any of the IP rights, or attempt to reverse engineer, decompile, disassemble, or derive the source code for the platform or use the platform or Site to create a competing product.
 BhartiPay Wallet is owned by<strong> Bhartipay Services Pvt. Ltd.</strong> unauthorized use is strictly prohibited.
  All rights are expressly reserved to BhartiPay Wallet.</p>
  
<p>
We make no warranty, express or implied, with respect to the Site and Related Applications and Services offered. We expressly disclaim the implied warranties of non-infringement, merchantability, and fitness for any particular purpose. We provide the Site and Related Applications and any other technology and Services on the Site on an "as is", "where is", "with all faults" basis. We do not warrant that the Site or Related Applications and any products or Services shown or described on the Site or Related Applications, or other technology and services will be uninterrupted, error-free, available or operational at any particular time, or that any known defects will be corrected. We disclaim any liability for any information that may have become outdated since the last time the particular piece of information was updated. We reserve the right to make changes and corrections to any part of the content of this
 website at any time without prior notice.

</p>
<p>
You expressly understand and agree that Your use of the services is at Your sole risk and that the Services are provided "as is" and "as available". In particular, BhartiPay"  Wallet, its subsidiaries and affiliates, and its licensors do not represent or warrant to You that:

</p>
<p>

You acknowledge that BhartiPay"  Wallet is a facilitator for Mobile recharges and other Services and is not liable for any 3rd party products and services available on the Site and other Related Applications with respect to rates, quality, and all other instances. You expressly agree that use of the Services and the Site and Related Applications is at your sole risk and you shall not hold BhartiPay"  Wallet liable for any discrepancy in the third party products. It is your responsibility to evaluate the accuracy, 
completeness and usefulness of all opinions, advice, services, merchandise and other information provided through the site or on the internet generally.
</p>

<p>Any material downloaded or otherwise obtained through the use of the Services is done at Your own discretion and risk and that You will be solely responsible for any damage to Your computer system or other device or loss of data that results from the download of any such material. No advice or information, whether oral or written, obtained by You from BhartiPay"  Wallet or through or from the Services shall create any warranty not expressly stated in the Terms. BhartiPay"  Wallet further expressly disclaims all warranties and conditions of any kind, whether express or implied, including, but not limited to the implied warranties and conditions of merchantability, fitness for a particular purpose and non-infringement
</p>
<p>


You agree to indemnify, defend and hold BhartiPay"  Wallet and/or related parties harmless from any and all claims, losses, damages, and liabilities, costs and expenses, including and without limitation legal fees and expenses, arising out of or related to your use or misuse of the BhartiPay"  Wallet, any violation of these Terms, or any breach of the representations, warranties, and covenants made by you
NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, NEITHER BhartiPay"  WALLET, NOR ITS AFFILIATED COMPANIES, SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES OR ANY RELATED PARTY SHALL HAVE ANY LIABILITY TO USERS OR TO ANY THIRD PARTY FOR ANY INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR ANY LOSS OF REVENUE OR PROFITS ARISING UNDER OR RELATING TO THESE TERMS, THE SITE OR THE OFFERINGS, EVEN IF ANY OF SAID PARTIES HAD BEEN ADVISED OF, KNEW OF, OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES. TO THE MAXIMUM EXTENT PERMITTED BY LAW, OUR MAXIMUM AGGREGATE LIABILITY TO YOU FOR ANY CAUSES WHATSOEVER, AND REGARDLESS OF THE FORM OF ACTION (WHETHER LIABILITY ARISES DUE TO NEGLIGENCE OR OTHER TORT, BREACH OF CONTRACT, VIOLATION OF STATUTE, MISREPRESENTATION OR FOR ANY OTHER REASON), WILL AT ALL TIMES BE LIMITED TO THE TRANSACTION VALUE
By accepting the Terms, You authorize us to hold, receive, disburse and settle funds on Your behalf. Your authorization permits us to generate an electronic funds transfer between the payment system providers and our nodal account to process each payment transaction that you authorize. Thereafter You authorise us to transfer the payments received from Your buyers to the bank account designated by You for this purpose at the time of registration. Your authorization will remain in full force and effect until Your BhartiPay"  Wallet account is closed or terminated.


</p>

<p>All sales are final with no refund or exchange permitted. You are responsible for the information you
 provide for purchase and all charges that result from those purchases. BhartiPay"  Wallet is not responsible
  for any purchase of incorrect information provided. If in a transaction performed by you on the Site,
   money has been charged to your BhartiPay"  wallet, credit/debit card or bank account and a recharge is not delivered within 
   two hours of your completion of the transaction then you may inform us by sending an email to our customer services email
    address as mentioned. BhartiPay"  Wallet shall investigate the incident and if it is found that money was
     charged to your BhartiPay"  wallet, credit/debit card or bank account without delivery of the recharge 
     then you will be refunded the money within 7 working days from the date of receipt of your email.
      All refunds will be credited to your BhartiPay"  Wallet account.</p>
      
   <p>
   You agree you will not transact any payments in connection with the following businesses, business activities or business practices pertaining to the following items:
In case of any technical issues, please raise a support ticket by emailing <a href="mailto:care@BhartiPay.com">care@BhartiPay.com </a>from Your service dashboard to let us know of the same. You can also contact us on 0120 6900881 in case of urgent matters.
   
   </p>
   
   <p>
   The Terms will be governed by and construed in accordance with the laws of India. You agree that any legal action or proceedings arising out of these Terms may be brought exclusively in the competent courts or tribunals having jurisdiction in Delhi, India and irrevocably submit themselves to the jurisdiction of such courts or tribunals. Bhartipay"  Wallet may elect to resolve any controversy or claim arising out of or relating to these Terms or Site and related application by binding arbitration in accordance with the provisions of the Indian Arbitration & Conciliation Act, 1996. Any such controversy or claim shall be arbitrated on an individual basis and shall not be consolidated in any arbitration with any claim or controversy of any other party. The arbitration shall be conducted in New Delhi, India and judgment on the arbitration award may be entered in any court having jurisdiction thereof. The language of Arbitration will be English.
   
    </p>
    
    <p>
    Customer also agrees to receive information regarding what BhartiPay"  Wallet perceives to be of their interest based on usage history via SMS, email, Push notification & phone call. This includes offers, discounts and general information. In case the customer does not want such information, they have to explicitly ask to be excluded by emailing us at <a href="mailto:care@BhartiPay.com">care@BhartiPay.com</a> or responding to opt-out SMSes sent from time to time to the customers.
    </p>
    
    
    <ol>
    <li>ACCEPTANCE OF TERMS</li>
    <li>ELIGIBILITY
    
    	<ul>
    		<li>You must be 12 years of age to use BhartiPay Wallet.</li>
    		<li>Individuals below 12 years can use BhartiPay Wallet under the guidance of an adult.</li>
    		<li>We reserve the right to terminate any account or Your use of BhartiPay Wallet if any information provided by You is false, fictitious, inaccurate, not current or incomplete, with or without notice to You.</li>
    	
    	</ul>
    </li>
    <li>
    	REGISTRATION TO USE THE SERVICES
    	<ul>
    		<li>You will be issued a user ID and password to access Your account.</li>
<li>You must keep the password confidential and not share it with any other person as You are responsible for all activity on Your account, whether or not You authorized it.</li>
<li>In case of any unauthorized use of Your account or Your user id and password, please contact us at care@BhartiPay.com</li>
    		
    	
    	
    	</ul>
    </li>
    <li>
    GENERAL CONDITIONS OF BhartiPay WALLET
    	<ul>
    		<li>After successful registration, BhartiPay Wallet will issue a Prime Account upon submission of your KYC documents at the time of registration or on a later date.</li>
    	
    	
 	<li>You will be able to use the BhartiPay Wallet Account after 3 (three) days from loading the Wallet from Banking Channel for the Send Money option. Rest of the services like recharges and bill payments can be used immediately and uninterruptedly.</li>
    	<li>The maximum monetary value that can be stored at any point of time, transferred to another account, utilized in a month in BhartiPay Wallet Account shall be in accordance with BANK (Reserve Bank of India) and BhartiPay Wallet guidelines as applicable from time to time.</li>
    	<li>Subject to guidelines/notifications issued by BANK from time to time these limitations may be reviewed and modified at the discretion of BhartiPay Wallet without prior intimation to the Customer.</li>
    	<li>BhartiPay Wallet reserves the right to suspend/discontinue BhartiPay Wallet Services to You at any time, for any cause, including, but not limited, to the following:</li>
    	<li>For any suspected violation of the rules, regulations, orders, directions, notifications issued by BANK from time to time or for any violation of the terms and conditions mentioned in this document</li>
    	<li>For any suspected discrepancy in the particular(s), documentation or KYC (know your customer) enrolment form provided by you</li>
    	<li>To combat potential fraud, use of abusive language, sabotage, wilful destruction, threat to national security or for any other force majeure reasons</li>
    	<li>If the same is due to technical failure, modification, upgrade, variation, relocation, repair, and/or maintenance due to any emergency or for any technical reasons</li>
    	<li>If the same is due to any transmission deficiencies caused by topographical and geographical constraints/limitations</li>
    	<li>If the mobile connection with which your BhartiPay Wallet is related ceases to be operational or in not in your possession and direct control.</li>
    	<li>If BhartiPay  Wallet believes, in its reasonable opinion, that cessation/ suspension is necessary.</li>
    	<li>BhartiPay Wallet reserves the right to amend, modify, change, and/or terminate (collectively "Changes") the Terms, the Site (https://www.bhartipay.com
			) and Related Applications, at any time in its sole discretion. Your continued use of the Services shall be deemed as Your acceptance of any such Changes.</li>
    	
    	</ul>
    
    </li>
    
    <li>YOUR CONDUCT ON THE MOBILE APPLICATION
    <ul>
    	<li>If BhartiPay Wallet requests registration information from You, You will provide with true, accurate, current, and complete information
    
    	</li>
    	<li>
    	You agree to log out from your accounts at the end of each session for Your own security
    	
    	</li>
    	<li>
    	You agree not to copy, modify, rent, lease, loan, sell, assign, distribute, reverse engineer, grant a security interest in, or otherwise transfer any right to the technology or software underlying its web-sites/app or the Services
    	
    	</li>
    	<li>
    		Without limiting the foregoing, You agree to not use the Services offered through Our Site or Related Applications to take any of the following actions
    	</li>
    	<li>
    	Engage in any act which is obscene, offensive, indecent, racial, anti religious, anti-national, objectionable, defamatory, abusive, harass, stalk, threaten, or otherwise violate the legal right of others
    	
    	</li>
    	<li>Publish, post, upload, e-mail, distribute, or disseminate (collectively, "Transmit") any inappropriate, profane, defamatory, infringing, obscene, indecent, or unlawful content</li>
    	<li>Transmit files that contain viruses, corrupted files, or any other similar software or programs that may damage or adversely affect the operation of another person's computer, its web-sites/app, any software or hardware, or telecommunications equipment</li>
    	<li>Advertise or offer to sell any goods or services for any commercial purpose unless you have its written consent to do so</li>
   		<li>Transmit web-sites/app, services, products, surveys, contests, pyramid schemes, spam, unsolicited advertising or promotional materials, or chain letters</li>
   <li>Download any file, recompile or disassemble or otherwise affect its products that you know or reasonably should know cannot be legally obtained in such manner</li> 
    <li>Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or the source of software or other material</li>
    <li>Collect or store personal information about other end users</li>
    <li>Impersonate any person or entity, including, but not limited to, a representative of BhartiPay Wallet or falsely state or otherwise misrepresent your affiliation with a person or entity</li>
    <li>Forge headers or manipulate identifiers or other data in order to disguise the origin of any content transmitted through Our Site or related application or to manipulate your presence on the Site or related application available through authorised sources</li>
    <li>Take any action that imposes an unreasonably or disproportionately large load on Our infrastructure/ network</li>
   <li>Engage in any illegal activities</li>
   <li>Unauthorized access to Our Site or related application is a breach of these Terms and a violation of the law</li>
   	<li>You agree not to access Our Site or related application by any means other than through the interface that is provided by BhartiPay Wallet for use in accessing Our Site or related application</li>
   <li>You agree not to use any automated means, including, without limitation, agents, robots, scripts, or spiders, to access, monitor, or copy any part of Our Site or related application, except those automated means that BhartiPay Wallet have approved in advance and in writing.</li>

    </ul>    
    </li>
    <li>
   KYC COMPLIANCE
   
   		<ul>
   			<li>"KYC" stands for Know Your Customer and refers to the various norms, rules, laws and statutes issued by Reserve Bank of India from time to time.</li>
   		<li>BhartiPay"  Wallet is required to procure personal identification details from You before any Services can be delivered and at the time of Registration and/ or on a later date, for availing and / or continuation of the BhartiPay"  Wallet.</li>
   		<li>The collection, verification, audit and maintenance of correct and updated customer information is a continuous process and BhartiPay"  Wallet reserves the right, at any time, to take steps necessary to ensure compliance with all relevant and applicable KYC requirements.</li>
   		<li>We reserve the right to discontinue Services/ reject applications for BhartiPay Wallet at anytime if there are discrepancies in information and/or documentation provided by you.</li>
   		<li>Any information provided to Us with the intention of securing BhartiPay Wallet shall vest with Us, and may be used, for any purpose consistent with any applicable law or regulation, at its discretion.</li>
   		<li>If the particulars provided by you in the KYC documents do not match with the details mentioned in the KYC enrolment form, then BhartiPay Wallet has the right to forfeit the balance amount in your wallet.</li>
   		<li>Subject to guidelines/notifications issued by BANK from time to time these limitations may be reviewed and modified at the discretion of BhartiPay Wallet without prior intimation you.</li>
   	
   		</ul>
    </li>
    <li>WALLET CHARGES & VALIDITY
    
    	<ul>
    		<li>You shall pay the service charges prescribed by BhartiPay Wallet in the form and manner prescribed for such payment.</li>
    		<li>BhartiPay Wallet may at its discretion, change, amend, increase, or reduce the Service Charges without prior intimation to the customer.</li>
    		<li>Any value in your BhartiPay Wallet that is utilized towards making payments for any transaction shall be automatically debited from your BhartiPay Wallet.</li>
    		<li>Our responsibility is limited to the debiting of your BhartiPay"  Wallet and the subsequent payment to any merchant establishment that you might transact with.</li>
    		<li>We do not endorse, promote, or warrant any goods and/or services that might be availed or proposed to be availed using BhartiPay Wallet.</li>
    		<li>We reserve the right to levy charges upon any amount loaded upon your BhartiPay Wallet or any amounts spent by You using BhartiPay Wallet.</li>
    		<li>BhartiPay Wallet reserves the right to set off any balance in your wallet in order to recover funds for transactions processed as per your request.</li>
    		<li>Wallet expiry and balance forfeiture will depend upon the inactive use of Your wallet for a long period of time wherein BhartiPay"  Wallet reserves the rights to forfeit the amount in accordance with prevalent regulatory guidelines.</li>
    		<li>Charges pertaining to transactions shall be in accordance with BANK guidelines as applicable from time to time.</li>
    	
    	
    	</ul>
    </li>
    <li>CONFIDENTIALITY </li>
     <li>INTELLECTUAL PROPERTY RIGHTS </li>
     <li>DISCLAIMER</li>
     <li>EXCLUSION OF WARRANTIES
     	<ul>
     		<li>Your use of the Services will meet Your requirements</li>
     		<li>Your use of the Services will be uninterrupted, timely, secure or free from error</li>
     			<li>Any information obtained by You as a result of Your use of the services will be accurate or reliable, and</li>
     			<li>That defects in the operation or functionality of any software provided to You as part of the Services will be corrected.</li>
     			
     	</ul>
     
     
     </li>
     <li>
  INDEMNITY
     </li>
     <li>LIMITATION OF LIABILITY</li>
     <li>AUTHORIZATION</li>
     <li>REFUND POLICY</li>
     <li>PROHIBITED SERVICES
     	<ul>
     		<li>adult goods and services which includes pornography and other sexually suggestive materials (including literature, imagery and other media); escort or prostitution services</li>
     		<li>alcohol which includes alcohol or alcoholic beverages such as beer, liquor, wine, or champagne</li>
     		<li>body parts which includes organs or other body parts</li>
     		<li>bulk marketing tools which includes email lists, software, or other products enabling unsolicited email messages (spam)</li>
     		<li>cable descramblers and black boxes which includes devices intended to obtain cable and satellite signals for free</li>
     		<li>child pornography which includes pornographic materials involving minors</li>
     		<li>copyright unlocking devices which includes mod chips or other devices designed to circumvent copyright protection</li>
     		<li>copyrighted media, which includes unauthorized copies of books, music, movies, and other licensed or protected materials</li>
     		<li>copyrighted software, which includes unauthorized copies of software, video games and other licensed or protected materials, including bundled software</li>
     	  		<li>counterfeit and unauthorized goods which includes replicas or imitations of designer goods; items without a celebrity endorsement that would normally require such an association; fake autographs, counterfeit stamps, and other potentially unauthorized goods</li>
     		<li>drugs and drug paraphernalia which includes illegal drugs and drug accessories, including herbal drugs like salvia and magic mushrooms</li>
     		<li>drug test circumvention aids which includes drug cleansing shakes, urine test additives, and related items</li>
     		<li>endangered species, which includes plants, animals or other organisms (including product derivatives) in danger of extinction</li>
     		<li>gaming/gambling which includes lottery tickets, sports bets, memberships/ enrolment in online gambling sites, and related content</li>
     		<li>government ids or documents which includes fake ids, passports, diplomas, and noble titles</li>
     		<li>hacking and cracking materials which includes manuals, how-to guides, information, or equipment enabling illegal access to software, servers, websites, or other protected property</li>
     		<li>illegal goods, which includes materials, products, or information promoting illegal goods or enabling illegal acts</li>
     		<li>miracle cures which includes unsubstantiated cures, remedies or other items marketed as quick health fixes</li>
     		<li>offensive goods, which includes literature, products or other materials that: a) defame or slander any person or groups of people based on race, ethnicity, national origin, religion, sex, or other factors b) encourage or incite violent acts c) promote intolerance or hatred</li>
     		<li>offensive goods, crime that includes crime scene photos or items, such as personal belongings, associated with criminals</li>
     		<li>prescription drugs or herbal drugs or any kind of online pharmacies which includes drugs or other products requiring a prescription by a licensed medical practitioner</li>
     		<li>pyrotechnic devices and hazardous materials which includes fireworks and related goods; toxic, flammable, and radioactive materials and substances</li>
     		<li>regulated goods which includes air bags; batteries containing mercury; or similar substances/refrigerants; chemical/industrial solvents; government uniforms; car titles; license plates; police badges and law enforcement equipment; lock-picking devices; pesticides; postage meters; recalled items; slot machines; surveillance equipment; goods regulated by government or other agency specifications</li>
     		<li>securities, which includes stocks, bonds, or related financial products</li>
     		<li>tobacco and cigarettes which includes cigarettes, cigars, chewing tobacco, and related products</li>
     		<li>traffic devices, which includes radar detectors/hammers, license plate covers, traffic signal changers, and related products</li>
     		<li>weapons which includes firearms, ammunition, knives, brass knuckles, gun parts, and other armaments</li>
     		<li>wholesale currency, which includes discounted currencies or currency, exchanges</li>
     		<li>live animals</li>
     		<li>multi level marketing collection fees</li>
     		<li>matrix sites or sites using a matrix scheme approach</li>
     		<li>work-at-home information</li>
     		<li>drop-shipped merchandise</li>
     		<li>collecting and effecting / remitting payments directly /indirectly outside India in any form towards overseas foreign exchange trading through electronic/internet trading portal</li>
     	<li>any product or service, which is not in compliance with all applicable laws and regulations whether federal, state, local or international including the all laws of India.</li>
     	</ul>
     
     
     </li>
    
    </ol>