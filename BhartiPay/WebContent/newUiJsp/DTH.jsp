<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>


<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
//User user=(User)session.getAttribute("User");
User user=new User();
%>
<script>
$(function(){
	 $("#dth_operator").msDropDown();
	// appendErrorDiv("#eleOpretor")
})
</script>

<h4 class="form-heading">Recharge your DTH easy &amp; secure</h4>
				<form method="post" id="d2h" class="oxyForm" action="RechargeService.action">
				
<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					<input type="hidden" name="rechargeBean.aggreatorid" value="<%=user.getAggreatorid()%>">
					
					<input type="hidden" name="rechargeBean.rechargeType" value="DTH">
					<div class="form-group payplutus_mt10">
		<s:select list="%{dth}" name="rechargeBean.rechargeOperator" onchange="changeDTHPlaceholderAndLen(this,'#dth_mobile_no')" cssClass="payplutus_dropdown mandatory" id="dth_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
					<div class="errorMsg customSelectErr" style="display: none;" id="dthOpratorErr">Please select operator</div>	
					</div>
					<div class="form-group payplutus_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="dth_mobile_no"
							class="payplutus_textbox mandatory onlyNum" required
							placeholder="Enter your DTH number" maxlength="12" />
					</div>
					<div class="form-group payplutus_mt10">
								
						<input type="hidden" name="rechargeBean.rechargeCircle" value="Delhi">			
 					</div>
						<div class="twoFields">
							<div class="half-width pull-left">
						<input type="text" name="rechargeBean.rechargeAmt" id="dth_amount"
							class="payplutus_textbox  amount-vl mandatory"  placeholder="Enter amount"
							maxlength="4" />
							<div class="errorMsg" style="display: none;"></div>	
						</div>
						<div class="half-width pull-right">
							<a href="javascript:void(0)" data-b2c="true" data-open-popup="DTH" onclick="submitVaForm('#d2h',this)" class="oxySubmitBtn"> Proceed</a>
							
							</div>
					</div>
					
				</form>
			