<h2>
Privacy Policy
</h2>
<p>

We at <strong>BhartiPay ( Legal Name as Appnit Technologies Private Limited),</strong> respect your privacy and the confidentiality of your information submitted on our portal. Also, we are committed to protecting the privacy of the user's personal information collected. We do not sell, trade, or rent your personal information to others. Information collected on our website is kept strictly confidential. We use the information we collect on the website to enhance the satisfaction by designing services especially for you. By using our website, you consent to the collection and use of this information by our website. This policy applies solely to information collected by Appnit and it is a part of Terms and Conditions of Use. Personal Information may be used that is stored in user profile after user has requested such features, for example to send invitation e-mails to people that you specified as being interested in joining the community.

</p>

<p>Although <strong>BhartiPay</strong> allows setting privacy options that limit access to User pages, please be aware that no security measures are perfect or impenetrable.</p>
<h4>Confidentiality</h4>
<p>BhartiPay only shares user information with others when it has user's permission.</p>

<p>Legal Matters: Protection of Rights, Property and Interests, Safety: BhartiPay may, without user consent, access and disclose his/her information, any communications sent or received by them, and any other information that it may have about user or user account: as may be permitted or required by law, regulation, rule or court order, pursuant to requests from governmental, regulatory or administrative agencies or law enforcement authorities, or to prevent, investigate, identify persons or organizations potentially involved in, or take any action regarding suspected fraud, violations of the Terms of usage, or activity that appear to be illegal or may expose Appnit to legal liability.</p>
<p>BhartiPay only shares User Information with others when it has user's permission or under the types of circumstances described in this Privacy Policy. In addition, the we may limit access to User Information to only those employees who we believe reasonably need to come into contact with that information to provide products or services to you.</p>
<h4>Scope of services</h4>
<p>This Privacy Policy covers BhartiPay.com treatment of personally identifiable information collected from customer/ agents / merchants / Sub-Merchants who use the BhartiPay.com services, as well as consumer information that we acquire in the course of our business. This Policy also covers the treatment of personally identifiable information that BhartiPay back end technology providers / partners may share with it.</p>
<h4>Why we require your Information</h4>
<ul>
	<li>To identify right and appropriate user: World of internet contains severe risk of fake users, we gather users' information so as to establish their true identity.</li>
<li>To provide customized as well as personalized service to individual user: Your identity helps us to design the appropriate and personalize service for you to enhance the experience.</li>
<li>To help our advertisers/affiliate: Affiliates / advertiser want to know the visitor details on our portal to justify their spending on their advertisements. But we maintain the confidentiality by not sharing your data/information with them. We only provide them with the number of users visited/landed.</li>
<li>To help our system to maintain the quality of services we offer to you: It helps us to improve the service quality/ feature time to time, so as to provide you with the updated technology and usage experience.</li>



</ul>
<h4>For Merchants / Vendors / Customers / Sub-Merchants / Agents / Business Partners </h4>
<ul>
	<li>We collect your personal data only to process payments and comply with laws.</li>
		<li>We do not share your data with third parties without your consent.</li>
			<li>We do share your data with our back-end service providers for making the e-commerce happen.</li>
				<li>You have access to your data on your merchant / customer / agents account.</li>
				<li>We do not control the data collection on your website, you do.</li>
				<li>We intimate you while making changes to our privacy policy.</li>


</ul>
<h4>Security Measures</h4>
<p>Information security is critical to our business. We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input. We store information gathered on secure computers. The number of employees involved in the management of the data center that have physical access to these computers is limited. We use advanced security technology to prevent our computers from being accessed by unauthorized persons. It is important for you (Merchant) to protect against unauthorized access to your Login ID/password and to your computer. Be sure to sign off when finished using a shared computer and otherwise protect the password used to access the BhartiPay services.</p>
<p>We also track information like the domain name from which you contact us, the pages you request, the products you buy, the referring site, and the time that you spend on the site. For everyone who visits an BhartiPay Merchant purchase page, we log IP addresses, the type of operating system that your computer uses, and the type of browser software used by you. From the IP address, we can determine the Internet Service Provider and the geographic location of your point of connectivity</p>
<p>Currently, we collect personal information of our Sub-Merchants / merchants / agents / customers, through the registration process, the service agreement that we execute and various submitted documents of the Sub-Merchants / merchants / agents / customers creditability as required by us from time to time. If you are our Merchant then the personal information you provide - name, address, etc.-allows us to inform you about updates, or changes to the service and to notify you of products and services of BhartiPay, its backend service providers and affiliates that we believe may be of interest to you. We use this data to protect the, vendors, sub merchant and ourselves against risk. Some of our web pages use "cookies" so that we can better serve you with customized information and in some cases protect ourselves and the sub-merchant from the risk of fraud.</p>
<p>Personally identifiable consumer information is also shared with third parties (such as our Acquiring banks and credit card processors) to the extent necessary for BhartiPay to deliver payment processing services. We are not responsible for their privacy policies or how they treat information about their users.</p>
<p>If there is a change in the privacy policy, it will thereafter be posted as a link to those changes on homepage so that the users are always aware of what information is being collected, how is it being used it, and under what circumstances, if any, it is disclosed.</p>
<p>BhartiPay encourages you to periodically review this page for the latest information on website's privacy practices.</p>
<h4>Uses of your Information</h4>
<p>We may also send you other information about us, our other websites, our products, sales promotions, our newsletters, SMS updates, anything relating to other companies in our group or our business partners. If you would prefer not to receive any of this additional information as detailed in this paragraph (or any part of it) please click the unsubscribe link in any email that we send to you or follow the unsubscription process as detailed in the SMS. Kindly note that unsubscribing from one medium does not automatically lead to unsubscription from the other. Within 7 working days (days which are neither (i) a Saturday or Sunday, nor (ii) a public holiday anywhere in India) of receipt of your instruction we will cease to send you information as requested. If your instruction is unclear we will contact you for clarification.</p>
<h4>Account Protection</h4>
<p>Your password is the key to your account. You shall be solely responsible for all the activities taking place under your username and you shall be solely responsible for keeping your password secure. Keep your credentials safe. If you share your credentials with others, you shall be solely responsible for all actions taken under your username and you may lose substantial control over your personal information and may be subject to legally binding actions taken on your behalf. In the case of password sharing to any other person(s) from your side, we would not be liable for any financial loss to you.</p>
<h4>Refund Policy</h4>
<p>Attached separately for wallet.</p>
<p><strong>For Payment Gateway :</strong>  Refund will be only processed if BhartiPay rejects an application, if it does not meet the qualification criteria. BhartiPay reserves the right to reject the application without providing any explanation.</p>


<h4>Cancellation Policy</h4>
<p>In case of cancellation of an order placed with BhartiPay the full amount is forfeited and no money is refunded back.</p>
<h4>Governing Law and Jurisdiction</h4>
<p>The relationship between you and BhartiPay shall be governed by the laws of India. You and BhartiPay agree to submit to the personal and exclusive jurisdiction of the courts located at Noida, UP, India. If you face any privacy issue, if you find any misappropriation / incompleteness / contravention in our privacy policy, BhartiPay welcomes your suggestions at <a href="mailto:care@BhartiPay.com"> care@BhartiPay.com</a></p>









