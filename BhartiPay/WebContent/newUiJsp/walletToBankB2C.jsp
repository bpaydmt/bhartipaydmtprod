<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.bhartipay.wallet.report.bean.B2CMoneyTxnMast" %>

<%@page import="com.bhartipay.wallet.user.persistence.vo.BankDetailsBean" %>



<%B2CMoneyTxnMast b2cMoneyTxnMast=new B2CMoneyTxnMast(); 
BankDetailsBean bankDetail=new BankDetailsBean();
String errorMsg="";
String successMsg="";
if(session.getAttribute("errorMsg") != null)
	{ errorMsg = (String)session.getAttribute("errorMsg");
		session.removeAttribute("errorMsg");
	}
else if(session.getAttribute("successMsg") != null)
{ successMsg = (String)session.getAttribute("successMsg");
session.removeAttribute("successMsg");
}
%>
<form  id="walletToBankForm" method="post" action="moneyTxn.action" >
	<font color="red"><%=errorMsg %></font>
		<font color="blue"><%=successMsg %></font>
	<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
<div class="form-group">
<input type="text" placeholder="Enter Amount"  class="onlyNum mandatory" maxlength="4" name="b2cMoneyTxnMast.amount" />
<span class="errorMsg "  style="display: none;"></span>
</div>
<div class="form-group">
<input type="text" placeholder="Account Holder's Name" class=" mandatory" name="b2cMoneyTxnMast.accHolderName" />
<span class="errorMsg "  style="display: none;"></span>
</div>
<div class="form-group">
<input type="text" placeholder="Account Number" class=" onlyNum account-vl mandatory" name="b2cMoneyTxnMast.accountNo" />
<span class="errorMsg "  style="display: none;"></span>
</div>

<div class="" style="position:relative">
<input type="text" placeholder="IFSC" class="transformToUpperCase mandatory ifscCode" id="bankIfsc" name="b2cMoneyTxnMast.ifscCode" />
<span class="errorMsg "  style="display: none;"></span>
<a href="javascript:void(0)" style="position: absolute;
    top: 7px;
    right: 0px;" onclick="searchIFSCode(false)">Search IFSC</a>
</div>




<input type="text" name="b2cMoneyTxnMast.remarks"  class="" placeholder="Remark (Optional)" />




</form> 
<button class="oxySubmitBtn" onclick="submitVaForm('#walletToBankForm',this)">Proceed</button>