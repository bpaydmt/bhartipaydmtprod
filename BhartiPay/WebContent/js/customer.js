//$function

function getPage(billerType,tar,el,urlName){
	console.log($("#rechargeWrapper").length)
	if($("#rechargeWrapper").length == 0 ){
		$.get('newUiJsp/rechargePage.jsp',function(res){
			$("#rightCol").html(res)
		
			
				
			//alert(url)
			getPartialForm(urlName, billerType, tar)

		})
		
	}else{
		getPartialForm(urlName, billerType, tar)
		
	}
	
	window.location.hash = "/" + billerType
	$(el).parent().addClass('active').siblings().removeClass('active');
	

}


//Get The Partial Form Custormer

function getPartialForm(urlName, billerType, tar){
	$(tar).html("loading...")
$.ajax({
	method:'POST',
	url:urlName + ".action",		
	data:'billerType='+ billerType,
	success: function(data){

		$(tar).html(data)
		
		try{
    		 $('#slider').nivoSlider();
    	}catch(err){
    		console.log(err)
    	}
    	appendErrorDiv("input")
		
		
	},
	error:function(err){
		console.log(err)
	}
	
	
	
});


}

//Open Login Popup

function openLoginPopup(page){

	//$("#popup").model('show')
	$('#popupLogin .modal-content').html("Loading...");
	
	if(page == "Login"){
	$.get('./newUiJsp/login.jsp',function(data){
		 $("#login_form").attr('action','Login.action');
		$('#popupLogin').modal('show');
		$('#popupLogin .modal-content').html(data);


	});
}
	else{
		$.get('./newUiJsp/otp.jsp',function(data){
			 $("#login_form").attr('action','Login.action');
			$('#popupLogin').modal('show');
			$('#popupLogin .modal-content').html(data);


		});
	}
	
	

	
/*	$.get( "./newUiJsp/login.jsp", function( data ) {
		
	  $( "#popup .modal-content" ).html( data );
	 
	});*/
	
	
}





function formSubmit(formId,target,e) {
	e.preventDefault()
	 $("#login_form").attr('action','IsCustomer.action');
	//alert($(formId).attr('action'))
	$.get('CheckLoginStatus.action',function(res){
		
		if(res.status == "TRUE"){
			
			 $.ajax({
	    	        type: 'POST',
	    	        url: $(formId).attr('action'),
	    	        data:  $(formId).serialize(),
	    	        success: function (data) {
	    	        	 $("#popup").modal('hide')
	    	            console.log('Submission was successful.');
	    	            console.log(data);
	    	            $(target + " .modal-body").html(data)
	    	            $(target).modal('show')
	    	        },
	    	        error: function (data) {
	    	            console.log('An error occurred.');
	    	            console.log(data);
	    	        },
	    	    });
		}else{
			$(formId).submit();	
		}
		
	})
 
    	
   
 
}


function checkCustomer(e){
	e.preventDefault();
	 $.ajax({
	        type: 'POST',
	        url: $("#login_form").attr('action'),
	        data:  $("#login_form").serialize(),
	        success: function (data) {
	        	
	         console.log(data)
	         	 $("#login_form").attr('action','Login.action');
	         if(data.isCustomer == 'TRUE' ){
	        	 
	        	 $.ajax({
		    	        type: 'POST',
		    	        url: $("#login_form").attr('action'),
		    	        data: $("#login_form").serialize(),
		    	        success: function (res) {
		    	        	console.log(res)
		    	        	  $("#InvoicePop .modal-body").html(res)
		    	   	            $("#InvoicePop").modal('show')
		    	        },
		    	        error: function (data) {
		    	            console.log('An error occurred.');
		    	            console.log(data);
		    	        },
		    	    });
	        	 
	         }else{
	        
	        	 $("#login_form").submit();
	         }
	        },
	        error: function (data) {
	            console.log('An error occurred.');
	            console.log(data);
	        },
		})
}

function searchIFSCode(isPopupOpen){
	
	$("#IfscTable tbody").html('')
	if(isPopupOpen == false){
		$("#ifsCode, #ifscBankName, #ifscbranchName, #ifscState").val('')
		$("#ifscCodePopup").modal('show')
		var ifcs = $("#bankIfsc").val();
		
		$.get('GetBankDtl.action',function(data){
			console.log(data)
			 $.each(data,function(i, item) {
			$("<option>"+item.bankName+"</option>").appendTo("#ifscBankName");
			 })
			 $("#ifscBankName").msDropDown();
		})
	}else{
		var ifcs = $("#ifsCode").val();
		
	}
	var bankName = $("#ifscBankName").val();
	var branchName = $("#ifscbranchName").val();
	var state = $("#ifscState").val();
		
		$("#IfscTable tbody").html("<tr><td colspan='6'>Loading....</td></tr>")
	if(ifcs.length > 3 || isPopupOpen == true){
		$.ajax({
			method:'POST',
			url:'CBIFSC',
			data:"ifscCode="+ ifcs +"&bankName="+bankName+"&branchName="+branchName+"&state="+state,
			success:function(data){
				console.log(data)
				
				if(data.length == 0 ){
					$("#IfscTable tbody").html("<tr><td colspan='6'>No Data Found</td></tr>")
				}else{
					$("#IfscTable tbody").html("")
					 $.each(data,function(i, item) {
						
						$("#ifscCodePopup").modal('show')
						$("<tr><td>"+ item.bankName+"</td><td>"+ item.ifscCode +"</td><td>"+ item.branchName +"</td><td>"+ item.city+ "</td><td>"+item.state+"</td><td><button class='green-btn oxySubmitBtn' onclick='selectIfscCode("+'"'+ item.ifscCode +'"'+")'>Select</button></td></tr>").appendTo("#IfscTable tbody")
					
					 });
					
				}
				
					
				
				
				}
			
			})
	}
	
}
$(function(){
	var currentLoc = window.location.href;
    str = currentLoc.split("#")[1];
    switch(str){

    case "/DTH":
    	
    	getPage('DTH','#form-section','#DthLink','CRDTH');
    	break;
    case "/DataCard":
    	getPage('DataCard','#form-section','#dataCardLink','CRDataCard');
    	break;
    case "/electricityLink":
    	getPage('Electricity','#form-section','#electricityLink','CRElectricity');
    	
    	break;
    case "/Landline":
    	getPage('Landline','#form-section','#landlineLink','CRLandline')
    	break;
    case "/Gas":
    	getPage('Gas','#form-section','#gasLink','CRGas')
    	break;
 
    case "/Insurance":
    	getPage('Insurance','#form-section','#insuranceLink','CRInsurance')
    	break;
    case "/TransactionHistory":
    	
    	getTransactionHistory()
    	break;
    case "/privacyPolicy":
    	
    	getThePolicyPages('./newUiJsp/privacyPolicy.jsp',false,'privacyPolicy','#cprivacyPolicy')
    	break;
    case "/termAndConditionss":    	
    	getThePolicyPages('./newUiJsp/walletTermAndCondi.jsp',false,'termAndConditionss','#termCondition')
    	break;
    case "/refundPolicy":    	
    	getThePolicyPages('./newUiJsp/refundPolicy.jsp',false,'refundPolicy','#crefundPolicy')
    	break;
    case "/grievancePolicy":    	
    	getThePolicyPages('./newUiJsp/customerGrievancesPolicy.jsp',false,'grievancePolicy','#cgrievancPolicy')
    	break;
    case "/walletLimits":    	
    	getThePolicyPages('./newUiJsp/walletLimits.jsp',false,'walletLimits','#cWalletLimits')
    	break;
    case "/faq":    	
    	getThePolicyPages('./newUiJsp/walletFaq.jsp',false,'faq','#cFaq');
    	break;
    case "/userAccount":    	
    	getUserInfo()
    	break;
    	default:
        	getPage('Recharge','#form-section',"#rechargeLink",'CRBiller');

    	
    }
})
function selectIfscCode(ifsc){
	$("#bankIfsc").val( ifsc )
	$("#ifscCodePopup").modal('hide')
}
function getTransactionHistory(){

	$.ajax({
		method:'POST',
		url:'CTxnDtls.action',
		success:function(data){

			window.location.hash = "/TransactionHistory"
			$("#rightCol").html(data)
			console.log(data)
		}
		
	})
}

function openTransactionDetails(details,billerType,txtId,txtDate,amount){
	console.log(details)
	console.log(billerType)
	console.log(txtId)
	console.log(txtDate)
	var detailArr = details.split("|")
	$("#TransactionPop tbody").html("")
	console.log(detailArr)
	if(billerType == "Recharge"){
		$("<tr><td><strong>Recharge Type</strong></td><td>"+billerType+"</td></tr>").appendTo("#TransactionPop tbody")
		$("<tr><td><strong>Operator</strong></td><td>"+detailArr[2] +"("+ detailArr[0]+")"+"</td></tr>").appendTo("#TransactionPop tbody")
		$("<tr><td><strong>Mobile</strong></td><td>"+detailArr[3]+"</td></tr>").appendTo("#TransactionPop tbody")
		$("<tr><td><strong>Amount</strong></td><td>"+amount+"</td></tr>").appendTo("#TransactionPop tbody")
		$("<tr><td><strong>Transaction Date</strong></td><td>"+txtDate+"</td></tr>").appendTo("#TransactionPop tbody")
		$("<tr><td><strong>Transaction ID</strong></td><td>"+txtId+"</td></tr>").appendTo("#TransactionPop tbody")
	}
	$("#TransactionPop").modal('show')
	
}



function getUserInfo(){
	/*$("#profileInfo .modal-body").html("loading...");
	$("#profileInfo").modal("show");*/
	window.location.hash = "/" + "userAccount"
	
	$.get('CProfile.action',function(html){
		console.log(html)
		$("#rightCol").html(html)
	})
}

function userProfileEdit(){
$(".editInp, #savEditedChanges").show();


$(".staticField, #EditProfile").hide()
}
/*function customerForgetPwd(){
	
	
}*/

function getThePolicyPages(page,policyPage,urlName,activeLink){
	if(policyPage ==  false){
	
	$.get('./newUiJsp/policyPage.jsp',function(data){
		$("#rightCol").html(data)
		$.get(page,function(res){
		
				$("#policyContent").html(res);
				
			
			
			
			$(activeLink).addClass('active').siblings().removeClass('active');
			window.location.hash = "/" + urlName
			$(window).scrollTop( 0 );
			
		});
	
		
	})
	}else{
		$.get(page,function(res){
			
			$("#policyContent").html(res)
		
		
		window.location.hash = "/" + urlName
		$(activeLink).addClass('active').siblings().removeClass('active');
	});
	
		
	}
	
	
}