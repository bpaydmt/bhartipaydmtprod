var billInfoJson = "";
var billerType = "";
var participationtype = "";
var cust_param_delimiter="~#CUST_PARAM_DELIMITER#~";
var message = [
                {
                        msgId : 1,
                        msgErr : "Provided mobile number is not valid! Please enter valid 10 digit mobile number start with '5/6/7/8/9'."
                },
                {
                        msgId : 2,
                        msgErr : "Provided PAN number is not valid! PAN Card should be entered in [AAAAA9999A]"
                },
                {
                        msgId : 3,
                        msgErr : "Provided Email address is not valid!"
                },
                {
                        msgId : 4,
                        msgErr : "Please provide alpha-numeric value!"
                },
                {
                        msgId : 5,
                        msgErr : "Please provide numeric value!"
                },
                {
                        msgId : 6,
                        msgErr : "Please provide data in mandatory field!"
                },
                {
                        msgId : 7,
                        msgErr : "Please provide valid amount"
                },
                {
                        msgId : 8,
                        msgErr : "Please select a Payment Mode"
                },
                {
                        msgId : 9,
                        msgErr : "Provided Aadhar is not valid! Aadhar should not start with 0 or 1 and should be 12 digits"
                },
                {
                        msgId : 10,
                        msgErr : "Amount should be numeric or decimal."
                },
                {
                        msgId : 11,
                        msgErr : "Amount should be greater than 0."
                }
                ,
                {
                        msgId : 12,
                        msgErr : "QuickPay Amount can't be blank."
                }
                ,
                {
                        msgId : 13,
                        msgErr : "Payment Amount can't be blank."
                }
                ];

function hideAll() {
        $("#biller").hide();
        $("#subBiller").hide();
        $("#customerInfoArea").hide();
}


function showbiller(billerCat) {
	billerType = billerCat;
        hideAll();

        if (billerCat != "") {
        
        	
                $.ajax({
                        url : "fetchBillerList",
                        method : 'Post',
                        cache:0,
                        async : false,
                        data : "billerCategory="+billerCat,
                        beforeSend: function(){
                        	$('#loaderDiv').show();
                         },
                         complete: function(){
                            $('#loaderDiv').hide();
                         },
                        success : function(data) {
								$('#BillerLists').empty().append('<option value="">Please Select</option>');
                                $.each($.parseJSON(data) , function(key, value) { 
									 $('#BillerLists').append('<option value="'+key+'">'+value+'</option>');
								});
                                $("#biller").show();
                        }
                });	
        }

}


function getBillerCategory(){
		$.ajax({
			 method:'Post',
     		 cache:0,
     		 url:'bbpsAllBillerCategoriesServices',
     		 beforeSend: function(){
     			$("#loaderDiv").css("display", "block");
     		 },
     		 complete: function(){
     			$("#loaderDiv").css("display", "none");
     		 },
			 success:function(response){
			 var resJson = $.parseJSON(response);
			 if(resJson.status == "ERROR"){
//				 alert(resJson.statusMsg);
				 showNotify(true, resJson.statusMsg);
			 }
			 var data = resJson.billerCategories;
			 
			 var result = '';
			 result = result + '<ul class="nav nav-tabs col-lg-2 col-md-2 col-sm-2 col-xs-12">';
			
			 $.each(data , function(key, value) { 
				 var cat = value.category;
				 result = result + '<li class="tabthenopen col-7-width payplutus_tab">';
				 result = result + '<a onclick="showbiller(\''+cat+'\');" data-toggle="tab">'+cat+'</a>';
				 result = result + '</li>';
			 });
			 result = result + '</ul>';
			 $('#divbillerCat').empty().append(result);
			 $('#divbillerCat').show();
			}
		 });
	}


function clearalldivInput(classval) {

        $('.' + classval).val('');
}

function showCustomerInfoArea(biller) {
        if (biller.value != "") {
                clearalldivInput('billinfobox');
                $("#customerInfoArea").hide();
                $("#billInfoArea").hide();
                $("#subBiller").hide();
                $("#btnFetchBill").removeAttr("disabled");
                
                $("#customerInfoArea").show();
                $('#customerInfoArea').addClass('animated');
                fetchBillerInfo(biller);
        } else {
                $("#customerInfoArea").hide();
                $("#billInfoArea").hide();
                $("#subBiller").hide();
        }
}


function fetchBillerInfo(biller) {
        $(".errorbox").hide();
        var billerId = biller.value;

        hideshowQuickBox(0);
        
        $("#btn_quick_pay").hide();
        $("#btnFetchBill").hide();
        $.ajax({
	                url : "getCustomParam",
	                cache:0,
	                type : 'Post',
	                data : "billerId="+billerId,
	                beforeSend: function(){
	                	$('#loaderDiv').show();
	                 },
	                 complete: function(){
	                    $('#loaderDiv').hide();
	                 },
	                success : function(data) {
		                var json = $.parseJSON(data);
		                //console.clear();
//		                console.log(json);
		                custinfoparam(json);
	                }
	        });
	}


function hideshowQuickBox(val) {
	        if (val == 1) {
		$("#quickPayCCF").val("");
		$("#quickPayTotal").val("");
		$("#quickPayAmount").val("");
		$('.quickpayboxes').show();
		$("#btn_quick_pay").show();
	} else {
		$('.quickpayboxes').hide();
		$("#btn_quick_pay").hide();
	}
}



function showSubBiller(billerId) {
        $(".errorbox").hide();
        if (billerId.value != "") {
                $.ajax({
                        url : "getSubBillerList",
                        type : 'POST',
                        beforeSend: function(){
                        	$('#loaderDiv').show();
                         },
                         complete: function(){
                            $('#loaderDiv').hide();
                         },
                        success : function(results) {
//                                console.log(results);

                                $("#SubBillerLists").html(results);
                                $("#subBiller").show();
                        }
                });
        } else {
                $("#subBiller").hide();
        }

}




function custinfoparam(response){
	var paymentmodes = response.billerPaymentModes; 
	var custParamArray = response.billerInputParams;
	var custinfo = "";
	
	if (response.statusCode.toUpperCase() == "OK".toUpperCase()) {

        $(".custparam").remove();
        var billerDesc=response.billerDescription;
        if(billerDesc!=null&&billerDesc!='')
        {
        	$("#billerDescInCustInfoSection").css('display','')
        	$("#billerDescInCustInfo").text(billerDesc);
        }
        else
        {
        	$("#billerDescInCustInfoSection").css('display','none')
        	$("#billerDescInCustInfo").text('');
        }
        
	    	//console.log(item);
        
	        /*
	        	for (var i = 0; i < 0; i++) {	//obj.length
	        */
	        
        
        $.each(custParamArray , function(key, value) {    
        var custparam = $.parseJSON(value);
        var pName = custparam.paramName;
        custinfo += '<div class="col-sm-3 custparam"><div class="form-group form-group-md label-static"> <label class="control-label">'
                        + pName;

        var custParamIsOptional = custparam.isOptional;
        if (custParamIsOptional == "true") {
                custinfo += '<span style="color:red">*</span>';
        }
        custinfo += '</label>';
	        custinfo += ' <input type="text" class="custinfo" data-dataType="'
	                        + custparam.dataType
	                        + '" data-name="'
	                        + pName
	                        + '"  data-req="'
	                        + custparam.isOptional;
	        
	        if(custparam.maxLength>0){
	        	custinfo += ' "maxlength="'
	                        + custparam.maxLength;
	        }
	        
	        if(custparam.minLength>0){
	        	custinfo += ' "minlength="'
	                        + custparam.minLength;
	        }
	        custinfo += '"/>';
         custinfo += ' </div> </div>';
         
         });
        
        //$("#billerParam").html(custinfo);
        $('#billerexactness').val(response.billerPaymentExactness);
        var quickpaymentModes = document.getElementById('quickPaymentmethod');// PMPC IMplemention
        addPaymentModes(quickpaymentModes,paymentmodes);// PMPC IMplemention
         
        $("#inputMobile").attr("maxlength", "10");
        $("#inputMobile").attr("placeholder", "+91");
        $(custinfo).insertBefore($("#quickPayChannel"));
         
        var fetch_Req = response.billerFetchRequiremet;
        if (fetch_Req != "NOT_SUPPORTED") {
                $("#btnFetchBill").show();
        }

        if (fetch_Req == 'MANDATORY') {

            hideshowQuickBox(0);

	     } else {
	        hideshowQuickBox(1);
        }
    if ($("#btn_quick_pay").length){
    	//$("#btn_quick_pay").attr("disabled","disabled");
    }
    $("#customerinfobox").show();
        // $("#inputPAN").attr("maxlength", "10");
        // $("#inputAadhaarNum").attr("maxlength", "12");

        // $("#inputMobile").focusout(function(){
        // validateMobileNumber();
        // });

	} else {
	       // alert(obj.statusCode);
	        alert(obj.statusCode);
	}
}


//-------------------------- PMPC ------------------
function addPaymentModes(selectElement,data)
{
	var optionsElement='<option value="" selected>-- Select Payment Mode --</option>';
	
	var pMode = data.split(",");
	
	for(var i=0;i <pMode.length;i++)
		{
		optionsElement+='<option value="'+pMode[i]+'" >'+pMode[i]+'</option>';
		}
	selectElement.innerHTML =optionsElement
	
}


function confirmFetchBill(fetchbt){

	
	if($('#billInfoArea:visible').length > 0)
	{
		
		BootstrapDialog.show({
           message: 'You will lose any changes that you have made in the current Bill Information form. Do you want to continue?',
            closable:false,
            buttons: [{
                label: 'Cancel',
                action: function(dialog){
                dialog.close()
                }
            },{
                label: 'Confirm',
                action: function(dialog){
                	fetchBill(fetchbt);                	
                	dialog.close();
                }
            }]
        });
	}else{
		fetchBill(fetchbt);
	}
}

function prepareFetchBillData(fetchbt){

	$(".errorbox").hide();
    if (!validateCustomerInfo()) {
            return;
    }
    //fetchbt.disabled = true;
    
    var billers = document.getElementById('BillerLists');
    var billerId = billers.options[billers.selectedIndex].value;
    var billerName = billers.options[billers.selectedIndex].innerHTML;
    
    /*var isParent = billers.options[billers.selectedIndex]
                    .getAttribute('data-isparent')
    if (parseInt(isParent) == 0) {
            billerId = billers.options[billers.selectedIndex].value;
            billerName = billers.options[billers.selectedIndex].innerHTML;
    } else {
            var subbiller = document.getElementById('SubBillerLists');
            billerId = subbiller.options[subbiller.selectedIndex].value;
            billerName = subbiller.options[subbiller.selectedIndex].innerHTML;
    }*/
    var customparams = document.getElementsByClassName('custinfo');
    
    var inputParams={};
	var inputlist=[];
    var customparam = "";
    for (var i = 0; i < customparams.length; i++) {
            var param = customparams[i];
            if (param.value != "") {
            
            	var input = {
            		paramName : param.getAttribute('data-name'),
            		paramValue : param.value
            	}
            	inputlist.push(input);
            	
                customparam += param.getAttribute('data-name') + ":" + param.value;
                if (i < customparams.length - 1) {
                        //customparam += "@";
                		customparam += cust_param_delimiter;
            	}
            }
    }
    inputParams.input=inputlist;
    
    var ipAddress = "192.168.2.73";
    var agentDeviceInfo={
		ip : ipAddress,
		initChannel : "AGT",
		mac : "01-23-45-67-89-ab"
	};
	
	var customerInfo={
		customerMobile : document.getElementById('inputMobile').value,
		customerEmail : document.getElementById('inputEmail').value,
		customerAdhaar :"",
		customerPan :""
	};
	
	
	var sendInfo = {
                /*agentId : document.getElementById('agentId').value,*/
                agentDeviceInfo : agentDeviceInfo,
                customerInfo : customerInfo,
                billerId : billerId,
                inputParams : inputParams
        };
       
       return sendInfo;
}

function fetchBill(fetchbt){
	
		var billInfoDataJson = prepareFetchBillData(fetchbt);
		
		if(billInfoDataJson != '' && billInfoDataJson != null){
			
			var billInfoDataStr = JSON.stringify(billInfoDataJson);
	      	$.ajax({
		                url : "fetchBillDetails",
		                cache:0,
		                type : 'Post',
		                data : "billFetchRequest="+billInfoDataStr,
		                beforeSend: function(){
		                	$('#loaderDiv').show();
		                 },
		                 complete: function(){
		                    $('#loaderDiv').hide();
		                 },
	                    success : function(response) {
	                    	//alert(response);
	                    	var json = $.parseJSON(response);
	                    	
	                    	if(json.responseCode == '000'){	//successful response code 000
//	                    		console.log(json);
	                    		billFetchInformation(json);
	                    	}
	                    	else/* if(json.responseCode == '001')	*/	//INVALID RESPONE CODE 001
	                    	{
//	                    		$("#billFetchErrorDecs").text('('+json.errorCode+') '+json.errorMessage);
//	                    		$(".errorbox").show();
	                    		var errorTxt = '('+json.errorCode+') '+json.errorMessage;
	                    		showNotify(true, errorTxt);
	                		}
	                    }
	        });
	        $("#btnFetchBill").removeAttr("disabled");
		}
}

function validateCustomerInfo() {
        if ($("#inputMobile").val() == '') {
                getErrMessage(6);
                return false;
        }
        var customparams = document.getElementsByClassName('custinfo');
        for (var i = 0; i < customparams.length; i++) {
                var param = customparams[i];
                // trim values before processing
                param.value = param.value.trim();
                var isValEnteredFlg = param.value != "";
                var isMandatoryCheckReq = param.getAttribute('data-req').trim() == 'true';
                var isMinLengthCheckReq = param.hasAttribute("minlength");
                var isNumericCheckReq = param.getAttribute('data-datatype').trim().toUpperCase()== "NUMERIC";
                var isAlphaNumericCheckReq = param.getAttribute('data-datatype').trim().toUpperCase() == "ALPHANUMERIC";
                
                // if mandatory
                if(isMandatoryCheckReq ){
                	// check if value present
                	if (!isValEnteredFlg){
                		 getErrMessage(6);
                         return false;
                	}
                }
                
                // if value has been entered in the field then apply common vals
                if(isValEnteredFlg){
                
                	// if min length spec
                	// validate min length
			        if (isMinLengthCheckReq) {
			        	var minLength = param.getAttribute("minlength");
			        	// param.value cannot be undefined or blank here
			        	if (param.value.length < minLength) {
			        		alert('Minimum length of '
							+ param.getAttribute("data-name") + ' should be '
							+ minLength, function(result) {
								param.focus();
							});
			        		// paraminfo.value='';
			        		return false;
			        	}
			        }
			        if(isNumericCheckReq){
			        	if (!NumericValidation(param)) {
	                         //alert('Please provide numeric value for '
	                                       //  + param.getAttribute('data-name') + ' !');
	                         alert('Please provide numeric value for '
	                                 + param.getAttribute('data-name') + ' !');
	                         return false;
			        	}
			        }
			        if(isAlphaNumericCheckReq){
			        	if (!alfanumericValidationWithSpecialChar(param)) {
	                         //alert('Please provide numeric value for '
	                                       //  + param.getAttribute('data-name') + ' !');
			        		  alert('Please provide alpha-numeric and special character for '
	                                  + param.getAttribute('data-name') + ' !');
	                         return false;
			        	}
		        }
        }
    }
    return true;
}


	function validateMobileNumber() {
	
        var Number = $("#inputMobile").val();
        var IndNum = /^[0]?[56789]\d{9}$/;
		if(Number != ''){
	        if (IndNum.test(Number)) {
	                return;
	        }else {
	                // $("#inputMobile").focus();
	                getErrMessage(1);
	                /*$("#inputMobile").val('');*/
	        }
		}
	}
	
	function validateEmail() {

        var value = $("#inputEmail").val();
        //var IndNum = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        //var EmailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        var EmailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (EmailReg.test(value) || value == "") {
                return;
        }

        else {
                getErrMessage(3);
                // $("#inputEmail").focus();
                $("#inputEmail").val('');
        }
	}



	function getErrMessage(msgId) {
	        for (msg in message) {
	
	                if (message[msg].msgId == msgId) {
	                       // alert(message[msg].msgErr);
	                        alert(message[msg].msgErr);
	                }
	        }
	}
	
	function NumericValidation(item) {
        var Number = item.value
        if (isNaN(Number) || Number == "") {
                //getErrMessage(5);
                // document.getElementById(item).focus();
                return false;
        }
        return true;
	}
	
	function billFetchInformation(billInfo){
//		var billerJson = $.parseJSON(billInfo.billerResponse);
		var billerJson = billInfo.billerResponse;
		$('#inputCustomerName').val(billerJson.customerName);
		
		var billers = document.getElementById('BillerLists');
	    var billerName = billers.options[billers.selectedIndex].innerHTML;
	    var billerID = billers.options[billers.selectedIndex].value;
	    
		$('#inputBillerName').val(billerName);
		$('#inputBillerNameID').val(billerID);
		$('#inputCustomerMobile').val($("#inputMobile").val());
		$('#inputBillDate').val(billerJson.billDate);
		$('#inputBillNumber').val(billerJson.billNumber);
		$('#inputBillPeriod').val(billerJson.billPeriod);
		
		$('#inputDueDate').val(billerJson.dueDate);
		//billerJson.billAmount = 1000;
		$('#inputBillAmount').val(billerJson.billAmount);
		
		$('#billerAmountOptions').empty().append('<option value="AllAmountOptions" selected>Select All</option>');
		
		var totalAdditionAmout = 0.00;
        $.each(billerJson.amountOptions , function(key, value) { 
			 $('#billerAmountOptions').append('<option value="'+value.amountValue+'|'+value.amountName+'" disabled>'+value.amountName+' (&#x20B9;'+value.amountValue+')</option>');
			 totalAdditionAmout = parseFloat(totalAdditionAmout) + parseFloat(value.amountValue);
		});
        
        var exactnessAmnt = parseFloat(billerJson.billAmount)+parseFloat(totalAdditionAmout);
        var ccf_texes = parseFloat(exactnessAmnt) * 0.1;
        var netAmout = parseFloat(exactnessAmnt) + parseFloat(ccf_texes);
        
        
        $('#billerexactness_amnt').val(billerJson.billAmount);	//exactnessAmnt
        $('#inputCCFTax').val('0');	//ccf_texes
        $('#inputTotalAmtToPaid').val(billerJson.billAmount);	//netAmout
		
		$("#billInfoArea").show();
		billInfoJson = billInfo;
		isShowBillerArea(false);
	}
	
	function checkVal(){
		return $("#billerexactness").val().toUpperCase() ==='EXACT';
	}
	
	function isShowBillerArea(status){
		if(status == true){
			$("#biller").show();
			$("#customerInfoArea").show();
			$("#divbillerCat").show();
		}
		if(status == false){
			$("#biller").hide();
			$("#customerInfoArea").hide();
			$("#divbillerCat").hide();
		}
	}
	
	function otherPayment(enable)
	{
		//$("#billerAmountOptions").unbind("change");
		val=$("#billerAmountOptions").val();
		 $("#inputCCFTax").val("");
	     $("#inputTotalAmtToPaid").val("");
	     $('#billerexactness_amnt').prop('readonly', false);
	     $('#billerexactness_amnt').val('');
//	     $("#billerexactness_amnt").bind("change", function(e) {
//	         return calculateCCF();});
		
		if(val=='')
			{
			  $('#billerexactness_amnt').prop('readonly', enable);
			}
		else if( val == 'other_payment')
			{
//			console.log("other payment selected");
			$("#billerAmountOptions").unbind("change");
			 /*$("#billerexactness_amnt").bind("change", function(e) {
	             return calculateCCF();}
			 );*/
			}
		else
			{
			var splitRowObject = $("#billerAmountOptions").val().split('|');
	         amnt=splitRowObject[0];
	         $('#billerexactness_amnt').val(amnt);
	         
			 /*$('#billerexactness_amnt').prop('readonly', enable);
			  calculateCCF();*/
			}
	}
	
	
	function calculateCCF() {
        var paymentmethod = $("#paymentmethod").val();
        var billerId;
        
        amnt=$("#billerexactness_amnt").val();
        if(!isNumeric(amnt) || !greaterThanZero(amnt))
                {
                $("#inputCCFTax").val("");
                $("#inputTotalAmtToPaid").val("");
                $("#billerexactness_amnt").val(0);
                return;
                }
        updateMinMaxValue(document.getElementById('billerexactness_amnt'));
        amnt=$("#billerexactness_amnt").val();
        
//        if($('#billerexactness').val().toUpperCase()!='EXACT')
//        {
//        amnt=$("#billerexactness_amnt").val();
//        if(!isNumeric(amnt) || !greaterThanZero(amnt))
//                {
//                $("#inputCCFTax").val("");
//                $("#inputTotalAmtToPaid").val("");
//                $("#billerexactness_amnt").val(0);
//                return;
//                }
//        }
//        else
//                {
//                if($("#billerAmountOptions").val()=='')
//                        {
//                        $("#inputCCFTax").val("");
//                        $("#inputTotalAmtToPaid").val("");
//                        return;
//                        }
//                var splitRowObject = $("#billerAmountOptions").val().split('|');
//                amnt=splitRowObject[0];
//                }
        
        if (paymentmethod == "") {
                document.getElementById('billerAmountOptions').selectedIndex = 0;

               // alert("Please select Payment Method .")
                alert("Please select Payment Mode.");

                return false;
        } else if (amnt != "") {

                
//              var refId = $("#refId").val();

                var isParent = BillerLists.options[BillerLists.selectedIndex]
                                .getAttribute('data-isparent');

                if (parseInt(isParent) == 1) {
                        billerId = $("#SubBillerLists").val();
                } else {
                        billerId = $("#BillerLists").val();
                }
        }

}
	
function isNumeric(val){
    //var IndNum = /^\d*\.?\d*$/;
    
    if(val == ""){
                return false;
        }
        return !isNaN(val);
}

function updateMinMaxValue(obj){
    var minAllowed = parseFloat(obj.min);
    var maxAllowed = parseFloat(obj.max);
    var maxlength  = obj.maxLength;
   
    if(obj.value == ""){
           return;
    }
    if(!isNaN(obj.value)){
           var tempVal=obj.value;
           if (tempVal < minAllowed){
                  obj.value = parseFloat(minAllowed).toFixed(2);
                  return;
           }
           if (tempVal > maxAllowed){
                  if(tempVal.length == maxlength){
                        tempVal = tempVal.substring(0,tempVal.length-1);
                        obj.value = parseFloat(tempVal/100).toFixed(2);
                  }
                       
                  if(tempVal.length == maxlength-1){
                        obj.value = parseFloat(tempVal/100).toFixed(2);
                  }
                       
                  if(tempVal.length == maxlength-2){
                        obj.value = parseFloat(tempVal/10).toFixed(2);
                  }
                       
                  return;
           }
           tempVal = tempVal.substr(0, maxAllowed);
           obj.value = parseFloat(tempVal).toFixed(2);
    }
    else{
           obj.value = parseFloat(minAllowed).toFixed(2);
    }
}

function greaterThanZero(input){
    if(isNaN(input)){
            //getErrMessage(11);
            //input.value='';
            return false;
    }
    else{
            if(parseFloat(input)<=0){
                    //getErrMessage(11);
                    //input.value='';
                    return false;
            } else {
                    return true;
            }
    }
}

function validateAmount(amountInput ,isquickpay) {
    var amount = amountInput.value;
    
    //var IndNum = /^\d*\.?\d*$/;
    //if (IndNum.test(amount)) {
    if(amount != "" && !isNaN(amount)){
            if(!greaterThanZero(amount)){
                    getErrMessage(11);
                    amountInput.value='';
                    return;
            }
            else{
//                    console.log("existing value was"+amountInput.value);
                    //amountInput.value=parseFloat(amountInput.value).toFixed(2);
                    updateMinMaxValue(amountInput);
//                    console.log("setting value to"+amountInput.value);
            }
    } 
    else if(amount != "") {
    	
            getErrMessage(10);
            amountInput.value='';
    }
}

function greaterThanZero(input){
    if(isNaN(input)){
            //getErrMessage(11);
            //input.value='';
            return false;
    }
    else{
            if(parseFloat(input)<=0){
                    //getErrMessage(11);
                    //input.value='';
                    return false;
            } else {
                    return true;
            }
    }
}

function updateMinMaxValue(obj){
    var minAllowed = parseFloat(obj.min);
    var maxAllowed = parseFloat(obj.max);
    var maxlength  = obj.maxLength;
   
    if(obj.value == ""){
           return;
    }
    if(!isNaN(obj.value)){
           var tempVal=obj.value;
           if (tempVal < minAllowed){
                  obj.value = parseFloat(minAllowed).toFixed(2);
                  return;
           }
           if (tempVal > maxAllowed){
                  if(tempVal.length == maxlength){
                        tempVal = tempVal.substring(0,tempVal.length-1);
                        obj.value = parseFloat(tempVal/100).toFixed(2);
                  }
                       
                  if(tempVal.length == maxlength-1){
                        obj.value = parseFloat(tempVal/100).toFixed(2);
                  }
                       
                  if(tempVal.length == maxlength-2){
                        obj.value = parseFloat(tempVal/10).toFixed(2);
                  }
                       
                  return;
           }
           tempVal = tempVal.substr(0, maxAllowed);
           obj.value = parseFloat(tempVal).toFixed(2);
    }
    else{
           obj.value = parseFloat(minAllowed).toFixed(2);
    }
}

function convertToDecimalAndUpdateMinMaxVal(obj){
    var minAllowed = obj.min;
    var maxAllowed = obj.max;
    var maxlength = obj.maxlength;
    obj.value = obj.value.trim();
    if(obj.value == ""){
            return;
    }
    if(!isNaN(obj.value)){
            var tempVal=parseFloat(obj.value).toFixed(2);
            if (tempVal < minAllowed){
                    obj.value = parseFloat(minAllowed).toFixed(2);
                    return;
            }
            if (tempVal > maxAllowed){
                    obj.value = parseFloat(maxAllowed).toFixed(2);
                    return;
            }
            if (tempVal.length > maxlength){
                    tempVal = tempVal.substr(0, maxlength);
                    obj.value = parseFloat(tempVal).toFixed(2);
            }
    }
    else{
    		// trim to maxlength
    		var currVal =  obj.value;
    		if (currVal.length > maxlength){
    			currVal = currVal.substr(0, maxlength);
    		}
    		
    		//remove all non numeric chars
    		currVal = removeNonNumericCharsExceptDots(currVal);
    		//remove all dots except first
    		currVal = removeAllDotsExceptFirst(currVal);
    		
    		if(currVal =="" || currVal == "."){
    			 obj.value =currVal;
    		}
    		else{
    			obj.value = parseFloat(currVal).toFixed(2);
    		}
    }
}

function removeNonNumericCharsExceptDots( str ) {
	return str.replace(/[^\d.-]/g, '');
}
function removeAllDotsExceptFirst( str ) {
	var output = str.split('.');
	return output.shift() + (output.length ? '.' + output.join('') : '');
}


function appendPayForm() {
    
    
    if($("#billerAmountOptions").val() == "")
            {
            //alert('Please select amount option');
            alert('Please select amount option');
            return false;
            }
    
    if($('#paymentmethod').val() == "")
    {

    alert('Please select Payment Mode.');

    return false;
    }
    
    var pgMin = $('#inputTotalAmtToPaid').attr("min");
    var totAmt = $('#inputTotalAmtToPaid').val();
    if(totAmt < pgMin)
    {
    	alert('Minimum Net Payable amount supported by Payment Gateway is '+pgMin);
    	return false;
    }
    
    
    var form = document.getElementById("paybillform");
    
    var agentId = $("#agentId").val();
    var customparams = document.getElementsByClassName('custinfo');
    var billerId = '';
    var billers = document.getElementById('BillerLists');
    var isParent = billers.options[billers.selectedIndex]
                    .getAttribute('data-isparent')
    if (parseInt(isParent) == 0) {
            billerId = billers.options[billers.selectedIndex].value;

    } else {
            var subbiller = document.getElementById('SubBillerLists');
            //billerId = subbiller.options[subbiller.selectedIndex].value;
    }

    var customparam = "";
    for (var i = 0; i < customparams.length; i++) {
            var param = customparams[i];
            if (param.value != "") {
                    customparam += param.getAttribute('data-name') + ":" + param.value;
                    if (i < customparams.length - 1) {
                            //customparam += "@";cust_param_delimiter
                    	customparam += cust_param_delimiter;
                    }
            }
    }
    /*var splitRowObject = $("#billerAmountOptions").val().split('|');
    var amountOpt='';
    var amnt='';
    if(splitRowObject == 'other_payment')
    	{
    	amnt=$("#billerexactness_amnt").val();
    	}
    else
    	{
    	amnt=splitRowObject[0];
    	}*/
  
   
    var basebillamount='';
    if($('#billerexactness').val().toUpperCase()!='EXACT')
            {
            basebillamount=$("#inputBillAmount").val();
            amnt=$("#billerexactness_amnt").val();
            if(amnt=='')
                    {
                  //  alert("Payment Amount can't be blank");
                    alert("Payment Amount can't be blank");
                    return false;
                    }
            if(parseFloat(amnt)<=0)
            {
           // alert("Payment Amount must be greater than 0");
            alert("Payment Amount must be greater than 0");
            return false;
            }
            amountOpt=$("#billerAmountOptions").val()
            }
    /*var isAmountValidated = validateAmountOption($('#inputPaymentChannel').val(), $('#paymentmethod').val(),amnt,billerId,false,amountOpt,basebillamount,agentId);*/
    
    var isAmountValidated = true;
    
    if (isAmountValidated) {
    $("#btn_bill_pay").attr("disabled","disabled");	
    var element2 = document.createElement("input");
    element2.setAttribute("type", "hidden");
    element2.value = customparam;
    element2.name = "customparam";
    var element3 = document.createElement("input");
    element3.setAttribute("type", "hidden");
    element3.value = billerId;
    element3.name = "billerId";
    /*var element4 = document.createElement("input");
    element4.setAttribute("type", "hidden");	
    element4.value = document.getElementById('agentId').value;
    element4.name = "agentId";*/
    var element5 = document.createElement("input");
    element5.setAttribute("type", "hidden");
    element5.value = document.getElementById('inputEmail').value;
    element5.name = "inputEmail";
    var element6 = document.createElement("input");
    element6.setAttribute("type", "hidden");
    element6.value = document.getElementById('inputMobile').value;
    element6.name = "inputMobile";

    // form.appendChild(element1);
    
    //form.appendChild(element2);
    //form.appendChild(element3);
    //form.appendChild(element4);
    //form.appendChild(element5);
    //form.appendChild(element6);
    
    return true;
    
    }
    return false;
}

function bindAmountOption(obj){
	var select = "<option value=\"\">"
				+ "Please Select"
				+ "</option>"
				
	$.each(obj, function(index, value) {
					var name = value.name;
					var value = value.value;
					select += "<option value='"+name+"'>"+value+"</option>"
	});
	return select;
}

	
function payNow(){
	
	if(validateCustomerInfo()){
		if(!appendPayForm()){
			var abc = '';
		}
		else{
			
			var inputCustomerName = document.getElementById("inputCustomerName").value;
			var inputBillerName = document.getElementById("inputBillerName").value;
			var inputBillDate = document.getElementById("inputBillDate").value;
			var inputBillNumber = document.getElementById("inputBillNumber").value;
			var inputBillPeriod = document.getElementById("inputBillPeriod").value;
			var inputDueDate = document.getElementById("inputDueDate").value;
			var inputBillAmount = document.getElementById("inputBillAmount").value;
			var inputPaymentChannelAlias = document.getElementById("inputPaymentChannelAlias").value;
			/*var paymentmethod = $('#paymentmethod').val();*/
			var billerexactness = document.getElementById("billerexactness").value;
			
			var billerAmountOptions = $('#billerAmountOptions').val();
			var billerexactness_amnt = document.getElementById("billerexactness_amnt").value;
			var inputCCFTax = document.getElementById("inputCCFTax").value;
			var inputTotalAmtToPaid = document.getElementById("inputTotalAmtToPaid").value;
			var billerId = $('#inputBillerNameID').val();
			var custMobile = $('#inputMobile').val();
			
			var tAmt = parseFloat(inputTotalAmtToPaid);
		    var gb = $('#totalBalance').val();
		    var tBalance = parseFloat(gb);
		    if(tAmt > tBalance){
		    	alert('***** Your Wellet Balance is less. *****');
				return false;
			}
			
			var printJsonObj = {
					billerId : billerId,
					billerName : inputBillerName,
					paymentChannel : inputPaymentChannelAlias,
					paymentmethod : "Cash",
					custMobile : custMobile,
					quickPay : "N"
			};
			
			var amountInfoJson = {
					amount : inputBillAmount,
					currency : "356",
					custConvFee : "0",
					amountTags : ""
			};
			
			var paymentMethodJson = {
				paymentMode : "Cash",
//				paymentMode : "Internet Banking",
				quickPay : "N",
				splitPay : "N"
			};
			
			var infoJson = {
					infoName : "Remarks",
					infoValue : "Received"
			};
			
			var paymentInfoJson = {
					info : infoJson
			};
			
			var amount_payment_method_info = {
					billerAdhoc : "true",
					amountInfo : amountInfoJson,
					paymentMethod : paymentMethodJson,
					paymentInfo : paymentInfoJson
			};
			
			var amountOptionsJsonObj = {
					option : billInfoJson.billerResponse.amountOptions
			}		
			
			var additionalInfoJsonObj = {
					info : billInfoJson.additionalInfo
			}
			
			var billerResponseObj = {
					amountOptions : amountOptionsJsonObj,
					billAmount : billInfoJson.billerResponse.billAmount,
					billDate : billInfoJson.billerResponse.billDate,
					billNumber : billInfoJson.billerResponse.billNumber,
					billPeriod : billInfoJson.billerResponse.billPeriod,
					customerName : billInfoJson.billerResponse.customerName,
					dueDate : billInfoJson.billerResponse.dueDate
			};
			
			var billers = document.getElementById('BillerLists');
		    var billerName = billers.options[billers.selectedIndex].innerHTML;
			
			var sendInfo  = prepareFetchBillData('');
			sendInfo.billerResponse = billerResponseObj;
			sendInfo.billerAdhoc = "true";
			sendInfo.amountInfo = amountInfoJson;
			sendInfo.paymentMethod = paymentMethodJson;
			sendInfo.paymentInfo = paymentInfoJson;
			sendInfo.additionalInfo = additionalInfoJsonObj;
			sendInfo.billerName = billerName;
			sendInfo.billerType = billerType;

			var billPaymentRequest = JSON.stringify(sendInfo);
			gotoPayNow(billPaymentRequest, printJsonObj);
		}
	}
}

function gotoPayNow(billPaymentRequest, printJsonObj){
	
	payPopUpNo();
	$.ajax({
        url : "billPaymentRequest",
        method : 'Post',
        cache:0,
        data : "billRequest="+billPaymentRequest,
        beforeSend: function(){
        	$('#loaderDiv').show();
         },
         complete: function(){
            $('#loaderDiv').hide();
         },
        success : function(response) {
        	//alert(response);
        	var json = $.parseJSON(response);
        	
        	if(json.responseCode == '000'){	//successful response code 000
        		isShowBillerArea(false);
        		displayBillPaymentReceipt(json, printJsonObj);
        	}
        	else //if(json.responseCode == '001')		//INVALID RESPONSE CODE 001
        	{
        		var errorTxt = '('+json.errorCode+') '+json.errorMessage;
        		showNotify(true, errorTxt);
        		
    		}
        }
});
}


function processPayment(a,fetchData){
	$.ajax({
        url : "fetchBillerList",
        method : 'Post',
        cache:0,
        async : false,
        data : "billerCategory="+billerCat,
        beforeSend: function(){
        	$('#loaderDiv').show();
         },
         complete: function(){
            $('#loaderDiv').hide();
         },
        success : function(data) {
				$('#BillerLists').empty().append('<option value="">Please Select</option>');
                $.each($.parseJSON(data) , function(key, value) { 
					 $('#BillerLists').append('<option value="'+key+'">'+value+'</option>');
				});
                $("#biller").show();
        }
});
}

////////////////////////////////////////////////// complaint status start ////////////////////////////////////////////////////////////////////////

function statusCheckRequest() {
	
	$("#complaintErrorDiv").hide();
	
	//var complaintType = document.getElementById("complainttype_status").value; //Transaction Type
	//var complaintId = document.getElementById("complaintid_status").value;
	
	var complaintId = document.getElementById("cStatus").value;
	var complaintType ="Transaction Type";
	//var complaintId = userId;
	
	if (complaintType == "" || complaintId == "") {
		//alert("Please fill all the fields");
		alert('Please fill all the fields.');
	} else {
		
			// If Captcha Enabled - Start
		
		if(complaintId.trim() === ''){
			getErrMessage(13);
			return false;
		}
		if(complaintId.length<15){
			getErrMessage(11);
			return false;
		}
		if(!alfanumericValidation(complaintId)){
	        getErrMessage(19);
	        return false;
	    }
		$("#timeoutmsgdiv").hide();
		$("#msg").hide();
		
		
		var complaintObj = {
				complaintType : complaintType,
				complaintId : complaintId
		}
		
		var complaintRequest = JSON.stringify(complaintObj);
		
		$.ajax({
	        url : "complaintRequest",
	        method : 'Post',
	        cache:0,
	        data : "complaintStatus="+complaintRequest,
	        beforeSend: function(){
	        	$('#loaderDiv').show();
	         },
	         complete: function(){
	            $('#loaderDiv').hide();
	         },
			success : function(response) {
					if (response != "")
					{
						var results = JSON.parse(response);
						if(results.responseCode == '000'){	
							complaintSuccessStatusResp(results);
						}
						else //if(json.responseCode == '001')		//INVALID RESPONSE CODE 001
        	        	{
//        	        		$("#complaintError").text('('+results.errorCode+') '+results.errorMessage);
//        	        		$("#complaintErrorDiv").show();
							var errorTxt = '('+results.errorCode+') '+results.errorMessage;
			        		showNotify(true, errorTxt);
        	    		}
					}
				},
				error : function(textStatus, errorThrown) {
						$("#captcha_modal_div").modal('hide');
					
						$("#statusInfoArea .erormsg").html("Complaint not found");
						$("#statusInfoArea").show();
						$("#statusInfoArea .errorbox").show();
					}
				});
			} // If Captcha Enabled - End
			
			// If captcha Disabled - Start
			/*else{
					$
					.ajax({
						url : "/ComplaintService/"
								+ document.getElementById("tenantId").value
								+ "/complaintstatus/" + complaintType + "/" + complaintId,
						type : 'POST',
						dataType : 'json',
						contentType : 'application/json',
						success : function(results) {
							if (results != "") {
									$("#statusInfoArea").hide();
									$("#statusInfoArea .errorbox").hide();
									$("#statusInfoArea .successbox").hide();
	                                if (results.errorMessages.length == 0) {
	                                	var assignedTo = results.assigned==null?"":results.assigned;
										if(results.responseCode ==="000"){
											
											var successMessage = "<table class='table table-bordered paymentRecept'>"
												+"<tr>"
												+  "<th>" 
												+   "Complaint Id" 
												+  "</th>"
											    + "<td>"
												+ results.complaintId
												+ "</td>"
												+ "</tr>"
											if($('#complainttype_status :selected').val() ==='TRANSACTION'){
												successMessage += "<tr>"
													+ "<th>"
													+ "Current Status: "
													+ "</th>"
													+ "<td>"
													+ results.complaintStatus
													+ "</td>"
													+ "</tr>"
											}
											successMessage += "<tr>"
												+ "<th>"
												+ "Assigned To: "
												+ "</th>"
												+ "<td>"
												+ assignedTo;
											+ "</td>"
											+ "</tr>"
											+ "</table>"

											$("#statusInfoArea .successmsg").html(successMessage);
											$("#statusInfoArea").show();
											$("#statusInfoArea .successbox").show();
										} else if(results.responseCode ==="001"){
											
											$("#statusInfoArea .erormsg").html(results.responseReason);
											$("#statusInfoArea").show();
											$("#statusInfoArea .errorbox").show();
										}
									}else{
										var errormsg = ""
	                                        for (var i = 0; i < results.errorMessages.length; i++) {
	                                                errormsg += results.errorMessages[i].errorCd
	                                                                + " "
	                                                                + results.errorMessages[i].errorDtl;
	                                        }
										
										$("#statusInfoArea .erormsg").html(errormsg);
										$("#statusInfoArea").show();
										$("#statusInfoArea .errorbox").show();
										
									}
							} else {
								
								$("#statusInfoArea .erormsg").html("Complaint not found");
								$("#statusInfoArea").show();
								$("#statusInfoArea .errorbox").show();
							}
						},
						error : function(textStatus, errorThrown) {
						
							$("#statusInfoArea .erormsg").html("Complaint not found");
							$("#statusInfoArea").show();
							$("#statusInfoArea .errorbox").show();
						}
					});
				}*/ // If captcha Disabled - End
		//} // else
}

function alfanumericValidation(item) {
	   // var IndNum = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+$/;
	    var IndNum =/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
	    if (!(IndNum.test(item))) {
	            return false;
	    }
	    return true;
	}




function complaintSuccessStatusResp(response){
	
	//var response = json;
	var result = '';
	
	//result = result + '';
	
	result = result + '<div>';
    	result = result + '<table style=\" border-radius: 40px; border: 2px solid #AED6F1; padding: 20px; \" >';
    	
    	result = result + '<thead style=\"font-weight:bold;\" >';
    	result = result + '<tr>';
        		result = result + '<td>';
        			result = result + 'complaint Assigned';
        		result = result + '</td>';
    			
        		result = result + '<td>';
        			result = result + 'complaintId';
        		result = result + '</td>';
        		
        		result = result + '<td>';
    				result = result + 'Status';
    			result = result + '</td>';
    		result = result + '</tr>';
    		result = result + '</thead>';
    		
    		result = result + '<tbody>';
    		result = result + '<tr>';
        		result = result + '<td>';
        			result = result + response.complaintAssigned;
        		result = result + '</td>';
    			
        		result = result + '<td>';
        			result = result + response.complaintId;
        		result = result + '</td>';
        		
        		result = result + '<td style=\"color:green;\">';
    				result = result + response.responseReason;
    			result = result + '</td>';
   			result = result + '</tr>';
   			result = result + '</tbody>';
			
    	result = result + '</table>';
	result = result + '</div>';
	
	$("#complaintSuccessRespDiv").append(result);
}


function resetComplaint(){
	$("#statusInfoArea").hide();
	$("#statusInfoArea .errorbox").hide();
	$("#statusInfoArea .successbox").hide();
	$("#complaintid_status").val('');
}

function validateAlphaNumeric(obj){ 
	// call this method on onkeyup event. It will remove all special character except 0-9 and a-z/A-Z
	obj.value=obj.value.replace(/[\W_]/g, '');
}

//////////////////////////////////////////////// complaint status start ////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////// complaint raise start //////////////////////////////////////////////////////////////////////////////////////////////

function selectComplaintType(selctedValue) {
	if (selctedValue.value == 'transaction') {
		hideAllServiceComplaintDiv();
		$("#transactionType").show();
		complainttype = 'Transaction';
	} else if (selctedValue.value == 'service') {
		hideAllTransactionComplaintDiv();
		$("#serviceType").show();
		complainttype = 'Service';
		
	} else {
		hideAllTransactionComplaintDiv();
		hideAllServiceComplaintDiv();
	}
}

function hideAllServiceComplaintDiv() {
	$("#msg").hide();
	$("#serviceType").hide();
	$("#registerservicecomplaint").hide();
	$("#timeoutmsgdiv").hide();
	$("#errormsg").hide();
//	$("#srvc_otpBox").hide();
//	$("#srvc_otp_input").hide();
}
function hideAllTransactionComplaintDiv() {
	$("#msg").hide();
	$("#txnlist").hide();
	$("#registertxncomplaint").hide();
	$("#transactionType").hide();
	$("#timeoutmsgdiv").hide();
	$("#errormsg").hide();
}

function selectTxnSearch(txnSearchType){
	
	$('#txnmobile').val('');
	$("#txnrefid").val('');
	//$('#'+txnSearchType.value+'').show();
	if(txnSearchType.value=='_transMobile'){
		$("#_transMobile").show();
		$("#_transRefID").hide();
		//$("#btn_reset").show();
		removeSearchRecord();
	}else{
		$("#_transRefID").show();
		$("#_transMobile").hide();
		removeSearchRecord();
		//$("#btn_reset").hide();
		//document.getElementById('btn_srch').removeAttribute("disabled");
	}
	initializeFromDate();
	initializeToDate();
	$('#todt').datepicker('setDate', 'today');
	$('#fromdt').datepicker('setDate', 'today');
	$("#msg").hide();
	$("#txnlist").hide();
	$("#registertxncomplaint").hide();
	//$("#transactionType").hide();
	$("#timeoutmsgdiv").hide();
	$("#errormsg").hide();
}

function convertToNumberAndUpdateMinMaxVal(obj) {
	var minAllowed = obj.min;
	var maxAllowed = obj.max;
	var maxlength = obj.maxlength;

	if (obj.value == "") {
		return;
	}
	if (!isNaN(obj.value)) {
		var tempVal = parseInt(obj.value);
		if (tempVal < minAllowed) {
			obj.value = parseInt(minAllowed);
		}
		if (tempVal > maxAllowed) {
			obj.value = parseInt(maxAllowed);
		}
	} else {
		var currVal = obj.value;
		if (currVal.length > maxlength) {
			currVal = currVal.substr(0, maxlength);
		}

		//remove all non numeric chars
		currVal = removeNonNumericChars(currVal);
		if (currVal == "") {
			obj.value = currVal;
		} else {
			obj.value = parseInt(currVal);
		}
	}
}


function validateOTPLength(obj) {
	var minLength = obj.minLength;

	if (obj.value.length < minLength) {
		//alert('OTP should be '+minLength+ ' digits in length');
		BootstrapDialog.alert('OTP should be ' + minLength
				+ ' digits in length.');
	}
}

function selectServiceCompType(serviceType) {
	document.getElementById('svc_resend_otp').setAttribute('disabled',
			'disabled');
	/*document.getElementById('servicecomplaintbtn').setAttribute('disabled',
			'disabled');*/
	document.getElementById('svc_gen_otp').setAttribute('disabled', 'disabled');
	//document.getElementById('otp_svc_mobile').removeAttribute("disabled");
	$('#complaintServdesc').val('');
	$('#complaintagentid').val('');
	$('#svc_otp').val('');
	$("#svcOtp_errormsg").hide();
	document.getElementById('svcOtp_errormsgcont').innerHTML = "";
	//$("#svcOtp_errormsgcont").html("");
	//$('#svcOtp_errormsgcont').val('');
	//document.getElementById("otp_svc_mobile").value("");
	//$("#otp_svc_mobile").val('');
	if (serviceType.value != "") {
		$("#registerservicecomplaint").show();
		$('#registerservicecomplaint').addClass('animated');
		if (serviceType.value == 'agent') {
			// alert($(this).val());
			participationtype = 'agent';
			$("#divbillerid").hide();
			$("#divagentid").show();
			//$("#otpBoxService").show();
			$("#timeoutmsgdiv").hide();
			$("#errormsg").hide();
			$("#msg").hide();
		} else {
			// alert($(this).val());
			$("#divagentid").hide();
			$("#divbillerid").show();
			if (serviceType.value == 'biller') {
				participationtype = 'biller';
				//$("#otpBoxService").show();
				$("#timeoutmsgdiv").hide();
				$("#errormsg").hide();
				$("#msg").hide();
			} else {
				//  alert($(this).val());
				participationtype = 'system';
				$("#timeoutmsgdiv").hide();
				$("#errormsg").hide();
				$("#msg").hide();
			}
		}
		raiseServiceComplaint(participationtype);
	} else {
		$("#registerservicecomplaint").hide();
	}
}

function raiseServiceComplaint(pt) {
	$("#complaintServtype").val('Service');

	if (pt == 'agent') {
		$("#participatetype").val("AGENT");
		showServiceReasonList(pt);
		showAgentList();
	} else if (pt == 'biller') {
		$("#participatetype").val("BILLER");
		showServiceReasonList(pt);
		showBillerList();
	} else if (pt == 'system') {
		$("#participatetype").val("SYSTEM");
		showServiceReasonList(pt);
		showBillerList();
	}
}

function showServiceReasonList(pt) {
	/*$.ajax({
		url : "/ComplaintService/" + document.getElementById("tenantId").value
				+ "/servicereason/" + pt,
		type : 'POST',
		beforeSend: function(){
			$('#loaderDiv').show();
		 },
		 complete: function(){
		    $('#loaderDiv').hide();
		 },
		success : function(results) {
			$("#servicereason").html(results);
		}
	});*/
	
	var result = '';
	if (pt == 'agent') {
		result = result + '<option value="0">Select reason</option>';
		result = result + '<option value="1">Agent not willing to print receipt</option>';
		result = result + '<option value="2">Agent misbehaved</option>';
		result = result + '<option value="3">Agent outlet closed</option>';
		result = result + '<option value="4">Agent denying registration of complaint</option>';
		result = result + '<option value="5">Agent not accepting certain bills</option>';
		result = result + '<option value="6">Agent overcharging</option>';
	} else if (pt == 'biller') {
		result = result + '<option value="0">Select reason</option>';
		result = result + '<option value="1">Biller available. Unable to transact</option>';
		result = result + '<option value="2">Multiple failure for same biller</option>';
		result = result + '<option value="3">Denomination not available</option>';
		result = result + '<option value="4">Incorrect bill details displayed</option>';
		result = result + '<option value="5">Incomplete / no details reflecting</option>';
	}
	$("#servicereason").empty().append(result);
	
}

function showAgentList() {
	//	$.ajax({
	//
	//		url : "/ComplaintService/"+document.getElementById("tenantId").value+"/agentslist",
	//		type: 'GET',
	//		async :false,
	//		header:{   "Access-Control-Allow-Origin" : "*",
	//				   "Access-Control-Allow-Methods": "GET"},
	//		success: function (results) {
	//			$("#complaintagentid").html(results);
	//		}	
	//	});
}

function showBillerList() {
	/*$.ajax({
		url : "/ComplaintService/" + document.getElementById("tenantId").value
				+ "/billerslist",
		type : 'POST',
		beforeSend: function(){
			$('#loaderDiv').show();
		 },
		 complete: function(){
		    $('#loaderDiv').hide();
		 },
		success : function(results) {
			//			alert(results);
			$("#complaintbillerid").html(results);
		}
	});*/
	
	/*var result = '';
	
	result = result + '<option value="0">Select biller</option>';
	result = result + '<option value="OTME00005XXZ43">OTME</option>';
	result = result + '<option value="OTOE00005XXZ43">OTOE</option>';
	result = result + '<option value="OTNS00005XXZ43">OTNS</option>';
	result = result + '<option value="OFME00005DNZ43">OFMEDN</option>';
	
	$("#complaintbillerid").empty().append(result);*/
	
}

function removeNonNumericChars(str) {
	return str.replace(/\D/g, '');
}

function initializeFromDate() {
	$('#fromdt').datepicker({
		autoclose : true,
		format : 'dd-mm-yyyy',
		endDate : "current",
		defaultDate : new Date(),
		orientation : "bottom left",
		todayHighlight : true
	});
}

function initializeToDate() {
	$('#todt').datepicker({
		autoclose : true,
		format : 'dd-mm-yyyy',
		endDate : "current",
		defaultDate : new Date(),
		orientation : "bottom left",
		todayHighlight : true
	});
}

function removeSearchRecord() {
	//window.location.href='/ComplaintService/raiseComplaintreq';
	$('#txnmobile').val('');
	$("#txnrefid").val('');
	initializeFromDate();
	initializeToDate();
	$('#todt').datepicker('setDate', 'today');
	$('#fromdt').datepicker('setDate', 'today');
	$("#msg").hide();
	$("#txnlist").hide();
	$("#registertxncomplaint").hide();
	$("#timeoutmsgdiv").hide();
	$("#errormsg").hide();
	$("#tranOtp_errormsg_txn_mob").hide();
	$('#otp_txn_mob').val('');
	$('#otp_tran_type_comp').val('');
	$('#tranOtp_errormsg_txn_mob_txn_type_comp').hide();
	$('#tranOtp_errormsgcont_txn_mob_txn_type_comp').val('');
	$("#otpBoxService").hide();
	
	document.getElementById('tran_gen_otp_txn_mob').removeAttribute('disabled');
	document.getElementById('tran_gen_otp_txn_type_comp').removeAttribute('disabled');
	document.getElementById('tran_resend_otp_txn_type_comp').setAttribute('disabled','disabled');
	document.getElementById('otp_tran_type_comp').removeAttribute('disabled');
	document.getElementById('btn_srch').setAttribute('disabled','disabled');
	document.getElementById('tran_resend_otp_txn_mob').setAttribute('disabled','disabled');
	//document.getElementById('btn_srch').removeAttribute('disabled');
	document.getElementById('txnmobile').removeAttribute('disabled');
	document.getElementById('txnrefid').removeAttribute('disabled');
	document.getElementById('otp_txn_mob').removeAttribute('disabled');
    document.getElementById('todt').removeAttribute('disabled');
	document.getElementById('fromdt').removeAttribute('disabled');
	//document.getElementById('radioMobile').removeAttribute('disabled');
	document.getElementById('radioRefId').removeAttribute('disabled');
}

function setMinDate(fromDate) {
	if (fromDate.value == '') {
		document.getElementById('todt').removeAttribute('minDate');
	} else {
		$('#todt').datepicker('setStartDate', fromDate.value);
		var todt = document.getElementById('todt').value;
		if (todt != "") {
			var x = new Date(fromDate.value);
			var y = new Date(todt);
			if (x > y) {
				$('#todt').datepicker('setDate', fromDate.value);
			}
		}
		//	 $( "#todt" ).datepicker( "option", "minDate", fromDate.value );
		//	document.getElementById('todt').setAttribute('min',fromDate.value);
	}
}

///////////////////////////////////////////////         ///////////////////////////////////////////////////////////////////////////////////////
	


////////////////////////////////////////////////// Transaction Status ///////////////////////////////////////////////////////////////////////

function txnSuccessStatusResp(response){
	
	var result = '';
	
	result = result + '<div>';
    	result = result + '<table style=\" border-radius: 40px; border: 2px solid #AED6F1; padding: 20px; \" >';
    	
    	result = result + '<thead style=\"font-weight:bold;\" >';
    	result = result + '<tr>';
        		result = result + '<td>';
        			result = result + 'Reference Id';
        		result = result + '</td>';
    			
        		result = result + '<td>';
        			result = result + 'Biller Id';
        		result = result + '</td>';
        		
        		result = result + '<td>';
    				result = result + 'Txn Date';
    			result = result + '</td>';
    			
    			result = result + '<td>';
    				result = result + 'Amount';
        		result = result + '</td>';
        		
        		result = result + '<td>';
    				result = result + 'Status';
    			result = result + '</td>';
    		result = result + '</tr>';
    		result = result + '</thead>';
    		
    		result = result + '<tbody>';
    		result = result + '<tr>';
        		result = result + '<td>';
        			result = result + response.txnList.txnReferenceId;
        		result = result + '</td>';
    			
        		result = result + '<td>';
        			result = result + response.txnList.billerId;
        		result = result + '</td>';
        		
        		result = result + '<td>';
    				result = result + response.txnList.txnDate;
    			result = result + '</td>';
    			
    			result = result + '<td>';
    				result = result + response.txnList.amount;
        		result = result + '</td>';
        		
        		result = result + '<td style=\"color:green;\">';
    				result = result + response.txnList.txnStatus;
    			result = result + '</td>';
   			result = result + '</tr>';
   			result = result + '</tbody>';
			
    	result = result + '</table>';
	result = result + '</div>';
	
	$("#txnFormDiv").hide();
	$("#txnSuccessStatusRespDiv").append(result);
}

function txtStatus(){
	if(Validate()){
		
		var txtnumber = document.getElementById("txttxnrefid").value;
		
		var txtType = document.getElementById('rdbtn1').value;
		
		var txtStatusRequestObj = {
    		"trackType" : txtType,
    		"trackValue" : txtnumber,
		};
		
		var txtStatusRequest = JSON.stringify(txtStatusRequestObj);
		$.ajax({
	        url : "getTxnStatusInfo",
	        method : 'Post',
	        cache:0,
	        data : "txtStatusRequest="+txtStatusRequest,
	        beforeSend: function(){
	        	$('#loaderDiv').show();
             },
             complete: function(){
                $('#loaderDiv').hide();
             },
             success : function(response) {
 	        	var json = $.parseJSON(response);
 	        	
 	        	if(json.responseCode == '000'){	//successful response code 000
// 	        		console.log(json);
 	        		txnSuccessStatusResp(json);
 	        	}
 	        	else //if(json.responseCode == '001')		//INVALID RESPONSE CODE 001
 	        	{
// 	        		$("#lblerror").text('('+json.errorCode+') '+json.errorMessage);
 	        		//$("#paymentErrorDecsDiv").show();
 	        		var errorTxt = '('+json.errorCode+') '+json.errorMessage;
 	        		showNotify(true, errorTxt);
 	    		}
 	        }
		});
	}
}


function Validate() {
    
    var checkedrdbtn1 = document.getElementById('rdbtn1').checked;
    if ((checkedrdbtn1 == true) && (document.getElementById('txttxnrefid').value == "")) {
        $('#lblerror').text("Please enter transaction reference id.");
        return false;
    }
    /*else if ((document.getElementById('txttxnrefid').value != "") && (document.getElementById('txttxnrefid').value.length != 12)) {
        $('#lblerror').text("Transaction reference id is not valid.");
        checkedrdbtn1 = false;
        return false;
    }
     else if ((checkedrdbtn1 == false) && (document.getElementById('txt_MobNumber').value == "")) {
        $('#lblerror').text("Please enter mobile Number.");
        checkedrdbtn2 = false;
        return false;
    }
    else if ((document.getElementById('txt_MobNumber').value != "") && (document.getElementById('txt_MobNumber').value.length != 10)) {
        $('#lblerror').text("mobile Number is not valid.");
        return false;
    }
    else if (((document.getElementById('txt_MobNumber').value != "") && (document.getElementById('txttxnrefid').value == "")) && ((document.getElementById('txt_FrmDt').value == "") || (document.getElementById('txt_ToDt').value == ""))) {
        $('#lblerror').text("Please Enter Date.");
        return false;
    } */
    else {
        $('#lblerror').text("");
        return true;
    }
}


function quickPay() {
	
    $(".errorbox").hide();
    if (!validateCustomerInfo()) {
            return false;
    }
    
    var amnt=$("#quickPayAmount").val();
    if(amnt==''){
            alert("Quick Pay Amount  can't be blank");
//            BootstrapDialog.alert("Quick Pay Amount  can't be blank");
            return false;
    }
    if(parseFloat(amnt)<=0)
            {
            alert("Quick Pay Amount must be greater than 0 ");
//            BootstrapDialog.alert("Quick Pay Amount must be greater than 0 ");
            }
    if($("#quickPaymentmethod1").val()==''){
            alert("Please select Payment Mode.");
//            BootstrapDialog.alert("Please select Payment Mode.");
            return false;
    }
    var pgMin = $('#quickPayTotal').attr("min");
    var totAmt = $('#quickPayTotal').val();
    /*if(totAmt < pgMin)
    {
//    	BootstrapDialog.alert('The Minimum Net payable amount supported by Payment Gateway is '+pgMin);
    	alert('The minimum payable amount supported by the Payment Gateway is '+pgMin);
    	return false;
    }*/
    
    var tAmt = parseFloat(totAmt);
    var gb = $('#totalBalance').val();
    var tBalance = parseFloat(gb);
    if(tAmt > tBalance){
    	alert('***** Your Wellet Balance is less. *****');
		return false;
	}
    
    var agentId = $("#agentId").val();
    var billers = document.getElementById('BillerLists');
    var isParent = billers.options[billers.selectedIndex].getAttribute('data-isparent')
    var billerName = $('#BillerLists option:selected').text();
    var billerId = $('#BillerLists').val();
    var customparams = document.getElementsByClassName('custinfo');
    var customparam = "";
    for (var i = 0; i < customparams.length; i++) {
            var param = customparams[i];
            if (param.value != "") {
                    customparam += param.getAttribute('data-name') + ":" + param.value;
                    if (i < customparams.length - 1) {
                            //customparam += "@";//
                            customparam += cust_param_delimiter;
                    }
            }
    }
    //var isAmountValidated = validateAmountOption($('#quickPaymentChannel').val(), $('#quickPaymentmethod').val(), $('#quickPayAmount').val(),billerId,true,"","",agentId);
    var isAmountValidated = true;
    
    if (isAmountValidated) {
	    //$('#btn_quick_pay').attr("disabled","disabled");
	    
		var inputPaymentChannelAlias = document.getElementById("quickPaymentChannelAlias").value;
		var inputBillAmount = document.getElementById("quickPayAmount").value;
		/*var paymentmethod = $('#quickPaymentmethod').val();*/
		var billerexactness = document.getElementById("quickPayCCF").value;
		var billerAmountOptions = $('#quickPayAmount').val();
		var custMobile = $('#inputMobile').val();
		
		var printJsonObj = {
				billerId : billerId,
				billerName : billerName,
				paymentChannel : inputPaymentChannelAlias,
				paymentmethod : "Cash",
				custMobile : custMobile,
				quickPay : "Y",
		};
		
	    var amountInfoJson = {
				amount : billerAmountOptions,
				currency : "356",
				custConvFee : "0",
				amountTags : ""
		};
		
		var paymentMethodJson = {
			paymentMode : "Cash",
//			paymentMode : "Internet Banking",
			quickPay : "Y",
			splitPay : "N"
		};
		
		var infoJson = {
				infoName : "Remarks",
				infoValue : "Received"
		};
		
		var paymentInfoJson = {
				info : infoJson
		};
		
		var billers = document.getElementById('BillerLists');
	    var billerName = billers.options[billers.selectedIndex].innerHTML;
		
		var sendInfo  = prepareFetchBillData('');
		sendInfo.billerAdhoc = "true";
		sendInfo.amountInfo = amountInfoJson;
		sendInfo.paymentMethod = paymentMethodJson;
		sendInfo.paymentInfo = paymentInfoJson;
		sendInfo.billerName = billerName;
		sendInfo.billerType = billerType;
		

		var billPaymentRequest = JSON.stringify(sendInfo);
		gotoPayNow(billPaymentRequest, printJsonObj);
	    
    }

}

/*function gotoQuickPayNow(request){
	alert(request);
}*/

function calculateCCFQuickPay() {
    var paymentmethod = $("#quickPaymentmethod").val();
    var billerLists = document.getElementById('BillerLists');
    var quickPayAmt=$("#quickPayAmount").val();
    var billerId;
    if (paymentmethod == "") {
            document.getElementById('billerAmountOptions').selectedIndex = 0;

            BootstrapDialog.alert("Please select Payment Mode.");

            return false;
    } else if (quickPayAmt != "") {
            
            if(!isNumeric(quickPayAmt) || !greaterThanZero(quickPayAmt))
            {
            $("#inputCCFTax").val("");
            $("#inputTotalAmtToPaid").val("");
            $("#quickPayAmount").val(1.00);
            return;
            }
            updateMinMaxValue(document.getElementById('quickPayAmount'));
            var isParent = BillerLists.options[BillerLists.selectedIndex]
                            .getAttribute('data-isparent');

            if (parseInt(isParent) == 1) {
                    billerId = $("#SubBillerLists").val();
            } else {
                    billerId = $("#BillerLists").val();
            }

            // var splitRowObject = $("#billerAmountOptions").val().split('|')

            // alert(splitRowObject[0]);

            /*
             * url : "/AgentService/calculateCCF/" + $("#BillerLists").val() + "/" +
             * $("#quickPayAmount").val() + "/" + paymentmethod + "/" +
             * document.getElementById('agentId').value + "/" +
             * document.getElementById('quickPaymentChannel').value,
             */

            $.ajax({
                    //url : "",
                    type : 'POST',
                    async : false,
                    data : JSON.stringify({
                                            billerId : billerId,
                                            billAmount : $("#quickPayAmount").val(),
                                           /* agentId : document.getElementById('agentId').value,
                                            payMentChannel : document
                                                            .getElementById('quickPaymentChannel').value,*/
                                            paymentMode : paymentmethod
                                    }),
                    datatype : "json",
                    contentType : "application/json",
                    header : {
                            "Access-Control-Allow-Origin" : "*",
                            "Access-Control-Allow-Methods" : "POST"
                    },
                    success : function(resp) {
                            obj = JSON.parse(resp);
                            if (obj.statusCode == 'OK') {
                            		
                            		/* DISPLAY TAX BREAKUP START */
                            	
                            		var ccf=parseFloat(obj.body.ccf);
                            		var couccf=parseFloat(obj.body.couccf);
                            		var display=String("Tax Breakup") + "\n" +String("CCF : "+ccf) + "\n" +String("COUCCF : "+couccf);
                            		$("#taxBreakupQP").css("display", "inline-block");
                            		$("#taxBreakupQP").attr("title",display);
                            		
                            		/* DISPLAY TAX BREAKUP END */
                            		
                            		var tottax =ccf+couccf ;
                                    var tot = tottax + parseFloat($("#quickPayAmount").val());
                                   
                                    if ($("#quickPayAmount").val() != "") {
                                            $("#quickPayCCF").val(tottax.toFixed(2));
                                            $("#quickPayCcf").val(ccf);
                                            $("#quickPayCouccf").val(couccf);
                                            $("#quickPayTotal").val(tot.toFixed(2));
                                            $("#btn_quick_pay").removeAttr("disabled");
                                    } else {
                                            $("#quickPayCCF").val("");
                                            $("#quickPayTotal").val("");
                                            //$("#btn_quick_pay").attr("disabled", "disabled");
                                    }
                            } else {
                                    $("#quickPayCCF").val("");
                                    $("#quickPayTotal").val("");
                                    //$("#btn_quick_pay").attr("disabled", "disabled");
                                   // alert('Internal Server Error !');
                                    BootstrapDialog.alert('Unexpected Error occurred. Kindly contact helpdesk!');
                            }
                            /*
                             * $("#CCF_^_+_^_Tax(s)").attr('disabled', true);
                             * $("#Total_^_Amount_^_to_^_be_^_paid").attr('disabled', true);
                             */
                    }
            });
    } else {
            $("#quickPayCCF").val("");
            $("#quickPayTotal").val("");
            //$("#btn_quick_pay").attr("disabled", "disabled");
    }

}



function showNotifyNewCommented(status, text){
	
	$(".popup-overlay-other1, .popup-content-other1").addClass("active");
	  
	     var result = '';
		  
		  result = result + '<table style="font-size:150%">';
		  
		  result = result + '<tr ><td style="color:red;">'+text+'</td></tr>';
		  
		  // result = result + '<tr ><td style="text-align:left;">Mobile No.</td><td style="text-align:left;"><span style="color:blue;">'+mob+'</span></td></tr>';
		  
		  result = result + '</table><br>';
		  
		  $("#popupConfirmation").empty().append(result);
}

function popupOk(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
	//myfunction();
	document.getElementById("inputMobile").value=""; 
	document.getElementById("inputEmail").value="";

	//document.getElementsByClassName("custinfo").value = "";
	
}


function popupOkl(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
	
	//document.getElementById("billInfoArea").reset();
	
	$(".reset").click(function() {
	    $(this).closest('quickpayform').find("input[type=text], textarea").val("");
	});
	
	// $("#billInfoArea").empty();
	
	$('#quickpayform').find('input:text, input:password, select')
    .each(function () {
        $(this).val('');
    });
	
}
 
function showNotify(status, text){
	if(status){
		
		var result = '';
		result = result + '<span id="spanNoti">';
		result = result + text;
		result = result + '</span>';
		
		$("#bbpsnoti").append(result);
		$("#bbpsnoti").show().animate({width: '400px', opacity: '0.8'}, 1000*1);
		text = '';
		$(this).text('loading...').delay(1000*5).queue(function() {
            showNotify(false, '');
            $(this).dequeue();
        }); 
	}
	else{
		$('#spanNoti').text('');
		$('#spanNoti').hide();
		$("#bbpsnoti").find("#spanNoti").remove();
		$("#bbpsnoti").animate({width: '0px', opacity: '0.8'}, 1000*1);
	}
}


function showNotifyPrevious(status, text){
	if(status){
		
		var result = '';
		result = result + '<span id="spanNoti">';
		result = result + text;
		result = result + '</span>';
		
		$("#bbpsnoti").append(result);
		$("#bbpsnoti").show().animate({width: '400px', opacity: '0.8'}, 1000*1);
		text = '';
		$(this).text('loading...').delay(1000*5).queue(function() {
            showNotify(false, '');
            $(this).dequeue();
        }); 
	}
	else{
		$('#spanNoti').text('');
		$('#spanNoti').hide();
		$("#bbpsnoti").find("#spanNoti").remove();
		$("#bbpsnoti").animate({width: '0px', opacity: '0.8'}, 1000*1);
	}
}


function alfanumericValidationWithSpecialChar(item) {
    // var target = $( event.target );

    var Number = item.value
    var IndNum = /^[a-zA-Z0-9 \\/!#$%&(),-\\.\\*_|?{};:<>^`\\~@/.=+\\"\\'\\[\\]*]*$/;
    //var IndNum = /^[0-9a-zA-Z\\-]+$/;
    if (!(IndNum.test(Number))) {
            //getErrMessage(4);
            // document.getElementById(item).focus();
            return false;
    }
    return true;
}


function displayBillPaymentReceipt(data, request){
	
	$("#btn_bill_pay").removeAttr("disabled");
	
	$('#printTransactionID').text(data.txnRefId);
	$('#printCustomerName').text(data.RespCustomerName);
	$('#printBillDate').text(data.RespBillDate);
	$('#printBillPeriod').text(data.RespBillPeriod);
	$('#printBillNumber').text(data.RespBillNumber);
	$('#printDueDate').text(data.RespDueDate);
	
	$('#printConsumerNumber').text(data.RespConsumerNumber);
	
	$('#printBillAmount').text(data.RespAmount);
	$('#printCCFTax').text(data.CustConvFee);
	$('#printTxnStatus').text(data.responseReason);
	$('#printOrderID').text(data.orderID);
	$('#printAgentName').text(data.agentName);
	$('#printShopName').text(data.shopName);
	$('#printAgentMobile').text(data.agentMobile);
	
	var customerNumber = request.custMobile;
	
	$.each(data.inputParams , function(key, value) { 
		if(value.paramName.includes('hone') || value.paramName.includes('obile') || value.paramName.includes('umber')){
			customerNumber = '';
			customerNumber = value.paramValue;
		}
	});
	
	$('#printBillerID').text(request.billerId);
	$('#printBillerName').text(request.billerName);
	$('#printCustomerNumber').text(customerNumber);
	$('#printTotalAmount').text(parseFloat(data.RespAmount) + parseFloat(data.CustConvFee));
	$('#printTxnDateTime').text(data.txnDateTime);
	$('#printPaymentChannel').text(request.paymentChannel);
	$('#printPaymentMode').text(request.paymentmethod);
	$('#printApprovalNumber').text(data.approvalRefNumber);
	
	$.get('GetUserConfigByDomain',function(data){
		var img = $.parseJSON(data.result);
		$('#merchantLogo').attr('src',img.logopic);
		
	});
	
	if(request.quickPay == 'N'){
		$('#billInfoArea').hide();
	}
	$('#billPrintArea').show();
}

function cancelNow(){
	$('#billInfoArea').hide();
	$('#billPrintArea').hide();
	hideAll();
	getBillerCategory();
}

function submitTransactionComplaint() {
	var txnId = document.getElementById("txnrefid").value;
	var txnRsn = document.getElementById("transactionReason").value;
	var txnReason = $('#transactionReason option:selected').text();
	var txnDesc = document.getElementById("transactionComplaintServDesc").value;

	if (isEmpty(txnId)) {
		alert("Transaction Reference Id can't be blank.");
	} else if (isEmpty(txnReason)) {
		alert("Please select Transaction Complaint Reason");
	} else if (isEmpty(txnDesc)) {
		alert("Complaint Description can't be blank.");
	} else if (!txnDesc.match(/^[0-9a-zA-Z|.| ]+$/)) {
		getErrMessage(4);
	}
	else{
		var requestJsonObj = {
				complaintType : "Transaction",
				txnRefId : txnId,
				complaintDesc : txnDesc,
				complaintDisposition : txnReason
		};
		
		var requestJsonStr = JSON.stringify(requestJsonObj);
		generateComplaint(requestJsonStr, 'Transaction');
	}
}

function isEmpty(str) {
	return str === null || str.match(/^ *$/) !== null;
}

function generateComplaint(request,complaintType){
	if(request != null){
		$.ajax({
			url : "complaintRaiseRequest",
			method : 'POST',
			cache : 0,
			data : "complaintRaiseReq="+request,
			beforeSend : function(){
				$('loaderDiv').show();
			},
			complete : function(){
				$('loaderDiv').hide();
			},
			success : function(response){
				var json = $.parseJSON(response);
 	        	
 	        	if(json.responseCode == '000'){	//successful response code 000
 	        		txnRaiseCompaintResp(json, complaintType);
 	        	}
 	        	else		//INVALID RESPONSE CODE 001
 	        	{
 	        		var errorTxt = '('+json.errorCode+') '+json.errorMessage;
 	        		showNotify(true, errorTxt);
 	    		}
			}
		});
	}
}

function validateDescAlphNumeric(obj) {
	var str = obj.value;
	if (!str.match(/^[0-9a-zA-Z|.| ]+$/)) {
		getErrMessage(4);
		return false;
	}
	return true;
}

function txnRaiseCompaintResp(response, complaintType){
	
	var result = '';
	
	result = result + '<div>';
    	result = result + '<table style=\" border-radius: 40px; border: 2px solid #AED6F1; padding: 20px; \" >';
    	
    	result = result + '<thead style=\"font-weight:bold;\" >';
    	result = result + '<tr>';
        		result = result + '<td>';
        			result = result + 'Complaint Id';
        		result = result + '</td>';
    			
        		result = result + '<td>';
    				result = result + 'Complaint Assigned';
    			result = result + '</td>';
    			
        		result = result + '<td>';
    				result = result + 'Status';
    			result = result + '</td>';
    		result = result + '</tr>';
    		result = result + '</thead>';
    		
    		result = result + '<tbody>';
    		result = result + '<tr>';
        		result = result + '<td>';
        			result = result + response.complaintId;
        		result = result + '</td>';
    			
        		result = result + '<td>';
        			result = result + response.complaintAssigned;
        		result = result + '</td>';
        		
        		result = result + '<td style=\"color:green;\">';
    				result = result + response.responseReason;
    			result = result + '</td>';
   			result = result + '</tr>';
   			result = result + '</tbody>';
			
    	result = result + '</table>';
	result = result + '</div>';
	
	$("#boxBodyDiv").hide();
	if(complaintType =='Service')
		$("#registerservicecomplaint").hide();
	if(complaintType =='Transaction')
		$("#registertxncomplaint").hide();
	$("#txnRaiseCompaintRespDiv").append(result);
}


function submitServiceComplaint(){
	
	var requestJsonObj = {};
	var participationType = document.getElementById("participatetype").value;
	
	if (participationType == 'AGENT') {
		var participationId = document.getElementById("complaintagentid").value;
		if (isEmpty(participationId)) {
			alert("AgentId can't be balnk.");
			return false;
		}
		requestJsonObj.agentId = participationId;
	} else {
		var participationId = document.getElementById("complaintbillerid").value;
		if (isEmpty(participationId)) {
			alert("Please select biller name.");
			return false;
		}
		requestJsonObj.billerId = participationId;
	}
	
	var servReasonValue = document.getElementById("servicereason").value;
	var servReasonText = $('#servicereason option:selected').text();
	var desc = document.getElementById("complaintServdesc").value;

	if (isEmpty(servReasonValue)) {
		alert("Please select service reason. ");
	}

	else if (isEmpty(desc)) {
		alert("Complaint description can't be blank. ");
	} else if (!desc.match(/^[0-9a-zA-Z|.| ]+$/)) {
		getErrMessage(4);
	}

	else {
		
		requestJsonObj.complaintType = "Service";
		requestJsonObj.participationType = participationType;
		requestJsonObj.complaintDesc = desc;
		requestJsonObj.servReason = servReasonText;
		
		var requestJsonStr = JSON.stringify(requestJsonObj);
		generateComplaint(requestJsonStr, 'Service');
	}
}

function selectTxnStatus(txnSearchType){
	
	if(txnSearchType.value=='TRANS_REF_ID'){
		$("#div_RefiD").show();
		$("#div_MobileD").hide();
	}else{
		$("#div_RefiD").hide();
		$("#div_MobileD").show();
	}
}

function getTotalBalance(){
  	$.get("GetWalletBalance",function(result){
  		$("#totalBalance").val(result.amount);
  	});
  }


function printNow()
{
	var originalContents = document.body.innerHTML;
	var divToPrint=document.getElementById("billPrintArea");
	
	$('#bbpsLogo').attr('src','./image/BBPSLOGO.png');
	
	$("#merchantLogo").show();
	$("#bbpsLogo").show();
	$("#printAgentTR").show();
	
	$("#billPrintArea").find('#removePrint').remove();
	newWin= window.open("");
	newWin.document.write(divToPrint.outerHTML);
	newWin.print();
	newWin.close();
	document.body.innerHTML = originalContents;
}

function qPayPopUp(element, isQuickPay){

	  $(".popup-overlay-other1, .popup-content-other1").addClass("active");
	  
	  
		  
		  var customparams = document.getElementsByClassName('custinfo');
		  var param = customparams[0];
		  var paramName = '';
		  var paramValue = '';
		  
		  if(param.value !=""){
		  	paramName = param.getAttribute('data-name');
		      paramValue = param.value;
		  }
		  
		  var payableAmount = '';
			  
		  if(isQuickPay){
			  payableAmount = $("#quickPayAmount").val();
			  $("#payPopUpYes").attr('onclick','return quickPay();');
		  }
		  else{
			  payableAmount = $("#inputTotalAmtToPaid").val();
			  $("#payPopUpYes").attr('onclick','payNow();');
		  }
			  
		  var billers = document.getElementById('BillerLists');
		  var billerName = billers.options[billers.selectedIndex].innerHTML;
		  
		  var mob = $("#inputMobile").val();
		  
		  var result = '';
		  
		  result = result + '<table style="font-size:150%">';
		  
		  result = result + '<tr ><td style="text-align:left;">Payment For <br>&nbsp;<span style="font-weight:bold;">'+paramValue+'</span></td><td style="text-align:left;"><span style="color:blue;font-size:40px;">&#8377;'+payableAmount+'</span></td></tr>';
		  result = result + '<tr ><td style="text-align:left;">Category</td><td style="text-align:left;"><span style="color:blue;">'+billerType+'</span></td></tr>';
		  result = result + '<tr ><td style="text-align:left;">Biller Name</td><td style="text-align:left;"><span style="color:blue;">'+billerName+'</span></td></tr>';
		  result = result + '<tr ><td style="text-align:left;">Mobile No.</td><td style="text-align:left;"><span style="color:blue;">'+mob+'</span></td></tr>';
		  
		  result = result + '</table><br>';
		  
		  $("#popupInfoDetails").empty().append(result);
  }

function payPopUpNo(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
}

//Rohit