//  Add .mobile class to validate mobile Mobile Field field
//  Add .passward-vl class to validate Passward field
//  Add .confirm-vl class to validate Confirm password field
//  Add .pincode class to validate Pincode field
//  Add .emailid class to validate Email ID 
//  Add .userName Class to validate User Name
//  Add .amount-vl class for rount up the amount


   
function appendErrorDiv(elm){ 
 $(elm).each(function(index){
       var err =  $(this).next().hasClass('errorMsg')
      if(!err){
     
         $("<div class='errorMsg'></div>").insertAfter(this); 
      } 
 })
 
   
}
function triggerSubmit(e,formID) {
    var code = e.which ? e.which : e.keyCode;
   // e.preventDefault()

    if(code == 13 && $("#"+formID).hasClass("entered-esc") == false) {
    
        submitVaForm("#"+formID);
    }
    	e.preventDefault();
        return false;
   

}


// fade out error msg if it's not mandatory
function fadeOutNonmandatory(el){
    var el = $(el)
      if(!el.hasClass("mandatory") && el.val().length == 0 ){
        
            setTimeout(function(){
            insertErrMsg(el,"",false);
             makeitValid(el)
            },100)
          
        }

}
function validateEmialNPhone(el){
 
 var inp = $(el).val();
  
    if(inp.match(/^[6789]\d{9}$/) || inp.match(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i)){        
        makeitValid(el);
        insertErrMsg(el,"",false);
        
    }else{
            makeitInvalid(el)
          insertErrMsg(el,"Please enter valid Email/Mobile number.",true);
   }
         
}
function validateEmialNPhoneSubmit(formId){
	
	$(formId + " .email-mobile").each(function(){
	try{
	
	 var inp = $(this).val();
	  
	    if(inp.match(/^[6789]\d{9}$/) || inp.match(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i)){ 
	       
	        makeitValid(this);
	        insertErrMsg(this,"",false);
	        
	        
	    }else{
	    	 
	            makeitInvalid(this);
	          insertErrMsg(this,"Please enter valid Email/Mobile number.",true);
	   }
	}catch(err){
	
	}  
	 });
}
//Insert error massge
function insertErrMsg(elm,msg,bol){
    var pos = $(elm).next(".errorMsg").offset();
    
    if(bol == true){
         $(elm).next(".errorMsg").text(msg).fadeIn();
        /*   $(".errorMsg").each(function(){
              var pos = $(this).prev().position();  
              var w = $(this).prev().outerWidth();  
              var h = $(this).prev().innerHeight();
              var t = pos.top + h;
             
             $(this).css({"top":t + 14,"width": w})
        });*/
    }else if(bol == false){
    	if($(elm).next().is(".errorMsg")){
    		 $(elm).next(".errorMsg").text(msg).hide();
    	}else{
    		 $(elm).next().next(".errorMsg").text(msg).hide();
    	}
       
    }
    
   
}
function makeitValid(elm){
    
         $(elm).addClass("valid");
        $(elm).removeClass("invalid");
}
function makeitInvalid(elm){
      $(elm).addClass("invalid");
      $(elm).removeClass("valid");
}
//Only alphabets
function onlyAtoZ(elm, msg, event){
		
     var el = $(elm)
    var elv = el.val();   
 	var chop = elv.trim();  
    var choppedVal = el.val(chop);
        elv = el.val();
     
    	
   
    if(elv.length > 0 ){
         if(!elv.match(/^[A-Za-z ]+$/) || elv.match(/\w[ ]{2,}/)){         
       
           makeitInvalid(el)
          insertErrMsg(elm,msg,true);
          fadeOutNonmandatory(el);
    
    }else{
        insertErrMsg(elm,"",false);
        makeitValid(elm)
    }
  }

}
function userNameId(elm, msg, event){
	
    var el = $(elm)
   var elv = el.val();   
	var chop = elv.trim();  
   var choppedVal = el.val(chop);
       elv = el.val();
    
   	
  
   if(elv.length > 0 ){
        if(!elv.match(/^[A-Za-z0-9]+$/)){         
      
          makeitInvalid(el)
         insertErrMsg(elm,msg,true);
         fadeOutNonmandatory(el);
   
   }else{
       insertErrMsg(elm,"",false);
       makeitValid(elm)
   }
 }

}
function customerName(elm, event){
   
    var code = event.which ? event.which : event.keyCode;

   
    if(code == 32 && code != 50 && code != 9 && code != 8 && code != 37 && code != 39 && code != 46 && code != 118){
    	event.preventDefault(); 
    	
    }     
     
}
function validateAddress(elm, msg, event){
	
    var el = $(elm)
    var elv = el.val();   
 	var chop = elv.trim();  
    var choppedVal = el.val(chop);
        elv = el.val();
  
      if(elv.match(/[a-zA-Z0-9@\-=_:\/.?][ ]{2,}/) || !elv.match(/^[a-zA-Z0-9\-\/ ]+$/) || !elv.match(/.*[a-zA-Z]+.*/)) {  
       
          insertErrMsg(elm,msg,true)
           makeitInvalid(el)
         fadeOutNonmandatory(el);
    
    }else{
      
        makeitValid(elm)
        insertErrMsg(elm,"",false);
        fadeOutNonmandatory(el);
        
    } 

}
function validateDiscription(elm, msg, event){
	
    var el = $(elm)
    var elv = el.val();   
 	var chop = elv.trim();  
    var choppedVal = el.val(chop);
        elv = el.val();
  
      if(elv.match(/[a-zA-Z0-9@\-=_:\/.?][ ]{2,}/) || !elv.match(/^[a-zA-Z0-9\-\/.?,@ ]+$/) || !elv.match(/.*[a-zA-Z]+.*/)) {  
       
          insertErrMsg(elm,msg,true)
           makeitInvalid(el)
         fadeOutNonmandatory(el);
    
    }else{
      
        makeitValid(elm)
        insertErrMsg(elm,"",false);
        fadeOutNonmandatory(el);
        
    }
}
function singleName(elm, event){
    var el = $(elm)
    var elv = el.val(); 
    var code = event.which ? event.which : event.keyCode;
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    var regex = new RegExp("^[a-zA-Z]+$");
    if(elv.length > 0 ){
    if(!regex.test(str) && (code != 50 && code != 9 && code != 8 && code != 37 && code != 39 && code != 46)){
    	event.preventDefault();
    	 

    	
    }
    if(code == 32){
    	event.preventDefault(); 
    	
    }
    }
     
}
function alphaNumerical(elm, event){
    var el = $(elm)
    var elv = el.val(); 
    var code = event.which ? event.which : event.keyCode;
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    var regex = new RegExp("^[a-zA-Z0-9]+$");

    if(!regex.test(str) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46 &&  code != 118)){
    	event.preventDefault();
    }
    if(code == 32){
    	event.preventDefault(); 
    	
    }     
     
}

function alpha(elm, event){
    var el = $(elm)
    var elv = el.val(); 
    var code = event.which ? event.which : event.keyCode;
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    var regex = new RegExp("^[a-zA-Z]+$");
console.log(code)
    if(!regex.test(str) && ( code != 9  && code != 8 && code != 37 && code != 39 && code != 46 &&  code != 118)){
    	event.preventDefault();
    }
    if(code == 32){
    	event.preventDefault(); 
    	
    }     
     
}

function softUrlValidetor(elm, msg, event) {
    var inp = $(elm).val();
    
    var code = event.which ? event.which : event.keyCode;
 
    if (code == 32) {
        event.preventDefault();
    }
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!inp.match(/[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}/i) || inp.indexOf('?') != -1 || inp.indexOf('=') != -1 ||  inp.charAt(inp.length -1) ==  "." ||  inp.indexOf(".") ==  0 || inp.match(/[.]{2,}/)  ){

        insertErrMsg(elm, msg, true);
        makeitInvalid(elm);
        fadeOutNonmandatory(elm);
    } else {
        insertErrMsg(elm, "", false);
        makeitValid(elm);
    }

}

//Only numerical
function onlyNumerical(elm, msg){
    var el = $(elm)
    var elv = el.val(); 
     if(elv.length > 0 ){
    if(!elv.match(/^[0-9]+$/)){
    	
        var chopped = elv.replace(/[^0-9]+$/g,"")
        el.val(chopped)
       
        insertErrMsg(elm,msg,true)
        makeitInvalid(elm)
        fadeOutNonmandatory(el);
    }else{
        insertErrMsg(elm,"",false)
       makeitValid(elm)
    }
 }
}
function mobileValidation(elm, msg){
    var el = $(elm)
    var elv = el.val(); 
    
 if(elv.length > 0 ){
    if(!elv.match(/^[6789]\d{9}$/)){
    //  var chopped = elv.replace(/[^789]\d{9}$/,"")
    // el.val(chopped)
        insertErrMsg(elm,msg,true)
        makeitInvalid(elm)
        fadeOutNonmandatory(el);
        
        
        
    }else{
        insertErrMsg(elm,"",false)
        makeitValid(elm)
    }
}

}
function validatePincode(elm, msg){
    var el = $(elm)
    var elv = el.val(); 
     if(elv.length > 0 ){
    if(!elv.match(/^[0-9]{6}$/)){
       // var chopped = elv.replace(/[^0-9]+$/,"")
      //  el.val(chopped)
        makeitInvalid(elm)
        insertErrMsg(elm,msg,true)
        fadeOutNonmandatory(el);
    }else{
        insertErrMsg(elm,"",false)
        makeitValid(elm) 
    }
    }
}
function emaild(elm, msg){
    var el = $(elm)
    var emailRegEx = /^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;
    var elv = el.val(); 
    
    if(elv.length > 0 ){
    if(!elv.match(emailRegEx)){       
      
        insertErrMsg(elm,msg,true);
        makeitInvalid(elm)
        fadeOutNonmandatory(el);
    }else{
        insertErrMsg(elm,"",false);
         makeitValid(elm);
    }
    }
}


function validateCity(elm){
    var el = $(el)

}
function submitFrom(formId){
   var allowSubmit = true;


   $.each($(formId + " .mandatory"), function(index, formField) {
  
   if($(formField).val().trim().length == 0 && $(formId + " .invalid").length != 0) {
	   if( $(this).next().is('.errorMsg')){
		   $(this).next().html("This field is required.").show();
		   
	   }else{
		   $(this).next().next().html("This field is required.").show();
		   
	   }
              
           allowSubmit = false;            
                
    }else{
    	
    	  $(formId).submit();
    } 
    });      
 
}
function roundNum2(elm,event){
    var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    var regex = new RegExp("^[0-9.]+$");
    var curPos = inp.slice(0, elm.selectionStart).length - inp.length;
 
     var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if(inp.length == 0 && (code == 48 || code == 46) &&  $(elm).hasClass('zero-allowed') == false){
        event.preventDefault(); 
      }else{
          if (!regex.test(str) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46)){
          event.preventDefault();  
          }if(inp.indexOf(".") != -1 ){
              $(elm).attr("maxlength","10")
           if(code == 46 || ((inp.length - inp.lastIndexOf(".") > 2) && (curPos == 0 || curPos == -1) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46))){
              event.preventDefault(); 
          
         }
      
    
    }
    else{
               
        
        if(code == 46){
            $(elm).attr("maxlength","10");
         }else{
            $(elm).attr("maxlength","7");
         }
    }
}
}

function roundNum(elm,event){
    var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    var regex = new RegExp("^[0-9.]+$");
    var curPos = inp.slice(0, elm.selectionStart).length - inp.length;
 
     var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if(inp.length == 0 && (code == 48 || code == 46) &&  $(elm).hasClass('zero-allowed') == false){
        event.preventDefault(); 
      }else{
          if (!regex.test(str) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46)){
          event.preventDefault();  
          }if(inp.indexOf(".") != -1 ){
              $(elm).attr("maxlength","8")
           if(code == 46 || ((inp.length - inp.lastIndexOf(".") > 2) && (curPos == 0 || curPos == -1) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46))){
              event.preventDefault(); 
          
         }
      
    
    }
    else{
               
        
        if(code == 46){
            $(elm).attr("maxlength","8");
         }else{
            $(elm).attr("maxlength","5");
         }
    }
}
}

function percentageNum(elm,event){
    var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    var regex = new RegExp("^[0-9.]+$");
    var curPos = inp.slice(0, elm.selectionStart).length - inp.length;
 
     var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      
          if (!regex.test(str) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46)){
          event.preventDefault();  
          }if(inp.indexOf(".") != -1 ){
              $(elm).attr("maxlength","4")
           if(code == 46 || ((inp.length - inp.lastIndexOf(".") > 2) && (curPos == 0 || curPos == -1) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46))){
              event.preventDefault(); 
          
         }
      
    
   
  
    
}else{
	 $(elm).attr("maxlength","3");
}
}

function removeDigitAfterDot(elm,event){
        var inp = $(elm).val();
        var code = event.which ? event.which : event.keyCode;
        var le = inp.length;    
        var dot = inp.lastIndexOf(".")
       if(inp.lastIndexOf(".") == -1 && inp.length > 5){
         var res = inp.slice(0,5);
            $(elm).val(res);
         
               
        }
}
function removeDigitAfterDot2(elm,event){
    var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    var le = inp.length;    
    var dot = inp.lastIndexOf(".")
   if(inp.lastIndexOf(".") == -1 && inp.length > 7){
     var res = inp.slice(0,7);
        $(elm).val(res);
     
           
    }
}
  


function passwordVal(elm){
    var el = $(elm).val();
    if(el.length > 0){ 
    if(!el.match(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[!@#$^*]){1,})(?!.*\s).{8,15}$/)){
        insertErrMsg(elm,"Password must have at least 1 lower case character, 1 upper case character, 1 digit, 1 special character(!, @, #, $, ^, *) and length 8-15.",true);
        makeitInvalid(elm)    
    }else{
        insertErrMsg(elm,"",false);
        makeitValid(elm);
    }
    }
}

function matchCofirmPwd(pwd,con){
 var p =$(pwd).val();
 var c =$(con).val();
 if(c != p){     
     insertErrMsg(con,"New Password and Confirm New Password don't match.",true);            
    makeitInvalid(con)    
 }else if(c.length != 0){    
    insertErrMsg(con,"",false);
    makeitValid(con);
 }
}
function validateAdhar(elm){
        
        var elv = $(elm).val();
         
     if(elv.length > 0 ){
        
        if(!elv.match(/^[0-9]+$/)){
           
            var chopped = elv.replace(/\s/g,"")
            $(elm).val(chopped);
            
        }
     }
  
}

//changed should be include
function validateAdharBlur(elm,e){
    var el = $(elm).val(); 
      if(el.length != 0){
     if(!el.match(/^[0-9]{12}$/)){
        
       insertErrMsg(elm,"Please enter a valid 12 digits aadhaar card number.",true);
       makeitInvalid(elm);

        }else{
              // $(elm).val( $(elm).val().replace(/[^\d]/g, '').replace(/(.{4})/g, '$1 ').trim());
                insertErrMsg(elm,"",false);
                makeitValid(elm)
        }
}

  if(el.length == 0){
  fadeOutNonmandatory(elm)
  }
}



function validaeUrl(elm, msg, event) {
    var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    if (code == 32) {
        event.preventDefault();
    }
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!inp.match(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i) || inp.match(/[.]{2,}/) || !inp.match(/^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*([a-zA-Z0-9\-\.\,\'\/\\\+&amp;%\$#_]*)?$/i) || inp.charAt(inp.length -1) ==  "." ) {

        insertErrMsg(elm, msg, true);
        makeitInvalid(elm);
        fadeOutNonmandatory(elm);
    } else {
        insertErrMsg(elm, "", false);
        makeitValid(elm);
    }

}



function pancardValidate(elm,event){
     var inp = $(elm).val();
    var code = event.which ? event.which : event.keyCode;
    var regex = new RegExp("^[0-9A-Za-z]+$");    
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
     
        if(code == 32){
        event.preventDefault(); 
        }
        if(!regex.test(str) && (code != 50 && code != 9 && code != 8 && code != 37 && code != 39 && code != 46)){
            event.preventDefault();  
        }
    
 }

function pancardValidateBlur(elm){
      var el = $(elm).val();
      if(el.length != 0){
         
     if(!el.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){

       insertErrMsg(elm,"Please enter valid PAN Card.",true);
       makeitInvalid(elm)

   }else{
        insertErrMsg(elm,"",false);
       makeitValid(elm)

   }
    
    }
     if(el.length == 0){
  insertErrMsg(elm,"Please enter valid PAN Card.",true);
  fadeOutNonmandatory(elm)
}
}
function checkAmountOnSubmit(){
    $(".amount-vl").each(function(){
        try{
            var amle = $(this).val().length;
            var doLastIn = $(this).val().lastIndexOf(".");
            var countDots = $(this).val().split(".").length - 1;; 
            var checkDots = amle - doLastIn;
            if(doLastIn != -1 ){
                    if(checkDots > 3){
                           insertErrMsg(this,"Only 2 decimal places are allowed in Amount.",true);
                
                    makeitInvalid(this);
                   
                    }if( countDots > 1){
                            insertErrMsg(this,"Please enter a valid amount.",true);
                             makeitInvalid(this);                        
                    }                    
                    
                } 
            if((parseFloat($(this).val()) < 1 || parseFloat($(this).val()) > 99999.99) && $(this).hasClass("zero-allowed") == false){
                    insertErrMsg(this,"The amount should not be less than 1 and not more than 99999.99",true);                
                    makeitInvalid(this);

            }
            if((parseFloat($(this).val()) < 0 || parseFloat($(this).val()) > 99999.99) && $(this).hasClass("zero-allowed")){
                insertErrMsg(this,"The amount should not be less than 0 and not more than 99999.99",true);                
                makeitInvalid(this);

        }
        }catch(err){
           console.log(err)

        }
  });
}


function checkAmountOnSubmit2(){
    $(".amount-vl2").each(function(){
        try{
            
            if(isNaN($(this).val()) == true){
                            insertErrMsg(this,"Please enter a valid value.",true);
                             makeitInvalid(this);                        
            }else{
                	
                       
                         makeitValid(this);                        
                     
                }
    

        
        }catch(err){
           console.log(err)

        }
  });
}

function checkUserNameOnSubmit(){

    $(".userName").each(function(){
        try{

           
         var user = $(this).val();   
          if(user.length > 0){    
       if(user.match(/\w[ ]{2,}/) || !user.match(/^[a-zA-Z ]+$/)){
          
        insertErrMsg(this,"Only characters with no consecutive spaces are allowed.",true)
        makeitInvalid(this);
        
          
          
       }else{
            makeitValid(this);  
       }
       } 
        }catch(err){
           

        }
    });
}
function checkUserNameIdOnSubmit(){

    $(".userNameId").each(function(){
        try{

           
         var user = $(this).val();   
          if(user.length > 0){    
       if(!user.match(/^[a-zA-Z0-9]+$/)){
          
        insertErrMsg(this,"No special characters and spaces are allowed.",true)
        makeitInvalid(this);
        
          
          
       }else{
            makeitValid(this);

       }
       } 
        }catch(err){
           

        }
    });
}
function checkConfirlOnSubmit(){


        try{
        	  matchCofirmPwd(".password-vl",".confirm-vl");
        }catch(err){
           

        }
    
}

function validateUrlSubmit(){
	try{
		$(".url-vl").each(function(event){
			 validaeUrl(this,"Please enter valid URL.", event)
		});
	
	}catch(err){
		console.log(err)
	}
}
//address validation on validation on submit
function checkUAddressOnSubmit(){

    $(".address-vl").each(function(){
        try{
         
       if( $(this).val().match(/[a-zA-Z0-9@\-=_:\/.?][ ]{2,}/) || !elv.match(/^[a-zA-Z0-9\-\/ ]+$/) || !elv.match(/.*[a-zA-Z]+.*/)){
          
        insertErrMsg(this,"Only allowed special characters(- , /) with no consecutive spaces.",true)
        makeitInvalid(this);
          
          
       }else{
            makeitValid(this);

       } 
        }catch(err){
           

        }
    });
}
function setMinMax(elm, min, max,msg){
   var el = $(elm).val();
	
	 if(el.length < min || el.length > max){
		 
		    insertErrMsg(elm,msg,true)
		    makeitInvalid(elm);
		 
	 }else{
		 
		 makeitValid(this);
	 }
		  
	
}
function checkDiscOnSubmit(){
$(".disc-vl").each(function(){
    try{
     
   if( $(this).val().match(/[a-zA-Z0-9@\-=_:\/.?][ ]{2,}/) || !elv.match(/^[a-zA-Z0-9\-\/.?,@ ]+$ ]/) || !elv.match(/.*[a-zA-Z]+.*/)){
      
    insertErrMsg(this,"Please provide valid description with no consecutive spaces.",true)
    makeitInvalid(this);
      
      
   }else{
        makeitValid(this);

   } 
    }catch(err){
       

    }
});
}
function makeUpperCase(elm){
    var el = $(elm).val().toUpperCase();
    $(elm).val(el)


}

function testValidationOnBlur(elm,msg,regex){
        $(elm).each(function( index ) {
           var el = $(this).val();
            try{
                if(el.match(regex) || (el.length == 0 && $(this).hasClass("mandatory") == false)){
                	 makeitValid(this);
                }else{ 
                	
                       
                   
                        insertErrMsg(this,msg,true);              
                        makeitInvalid(this)
                } 
            }catch(err){
                console.log(err)
           }
    });
     
 
}
function resetForm(formid){
	 $(formid)[0].reset();
	$(".errorMsg").hide();
	$("form input[type='text'], form input[type='email'], form textarea, form input[type='password'], form select").removeClass("invalid")
  $("form input[type='text'], form input[type='email'], form textarea, form input[type='password'], form select").removeClass("valid")
}
function intialErrSpan(){
    appendErrorDiv("form input[type='text'], form input[type='email'], form textarea, form input[type='password'], form input[type='file'], form select");
    $(".userName, emaild").attr("maxlength","50");    
    $(".aadhar-vl").attr("maxlength","12");
    $(".pancard-vl").attr("maxlength","10");
    $(".amount-vl").attr("maxlength","5");
    $(".amount-vl2").attr("maxlength","7");
    $(".password-vl").attr("maxlength","15");
    $(".mobile").attr("maxlength","10");
    $(".otp-vl").attr("maxlength","6");
    $(".mpin-vl").attr("maxlength","4");
    $(".address-vl").attr("maxlength","100");
    $(".email-mobile").attr("maxlength","100");
}
$(function(){
	
	$(document).on("keyup",".transformToUpperCase",function(){
	
    var val = $(this).val()
    $(this).val(val.toUpperCase())
   

	})
	intialErrSpan()

    //  $(document).on("focus",".aadhar-vl",function(event){
        
    // validateAdhar(this,event);
     
    //  });
       $(document).on("blur",".otp-vl",function(){
     	
         validatePincode(this,"Please enter valid OTP.");
         fadeOutNonmandatory(this)
       });
	
		
	
	   $(document).on("blur",".userNameId",function(event){   	   
		    
		   userNameId(this,"No special characters and spaces are allowed.",event);
	      
	    });
  $(document).on("keyup",".amount-vl",function(event){
        removeDigitAfterDot(this,event);
     
    });
  $(document).on("keyup",".amount-vl2",function(event){
      removeDigitAfterDot2(this,event);
   
  });
	  $(document).on("blur",".email-mobile",function(event){
		  validateEmialNPhone(this);
	     
	    });
	
    $(document).on("blur",".address-vl",function(event){
    validateAddress(this,"Only allowed special characters(- , /) with no consecutive spaces.",event);
    
    });
    $(document).on("blur",".disc-vl",function(event){
    	validateDiscription(this,"Please provide valid description with no consecutive spaces.",event);
        
      });
    
   
    $(document).on("input keypress",".alphaNum-vl",function(event){  	
    	
    	alphaNumerical(this,event);
      
     });
  $(document).on("input keypress",".alph-vl",function(event){  	
    	
    	alpha(this,event);
      
     });
  
    $(document).on("input keypress",".singleName",function(event){
    	singleName(this,event);
    });
    $(document).on("focus",".mandatory",function(event){
    	$(this).next().hide()
    })
    
     $(document).on("blur",".aadhar-vl",function(event){
        validateAdharBlur(this,event);
         
    });        
     
  $(document).on("input keypress",".onlyNum",function(event){
	 	  	
	  onlyNumerical(this,"Please enter valid value");
  });
    $(document).on("input keypress",".otp-vl",function(event){
	 	  	
	  onlyNumerical(this,"Please enter valid value");
  });
    $(document).on("input keypress",".mpin-vl",function(event){
 	  	
  	  onlyNumerical(this,"Please enter valid value");
    });
  $(document).on("blur",".soft-url",function(event){
      
	    softUrlValidetor(this,"Please type valid SMTP URL",event)
	  });
     $(".pancard-vl").on("input keypress",function(event){
    	
    	 var start = this.selectionStart;
         var  end = this.selectionEnd;
        makeUpperCase(this);
        pancardValidate(this,event)
       
        this.setSelectionRange(start, end);
    })
     $(document).on("blur",".pancard-vl",function(){
        pancardValidateBlur(this);
    });
      
    $(document).on("keypress",".amount-vl",function(event){
   	 var start = this.selectionStart;
     var  end = this.selectionEnd;
        roundNum(this,event);
        this.setSelectionRange(start, end);
      });
    
    $(document).on("keypress",".percent-vl",function(event){
      	 var start = this.selectionStart;
        var  end = this.selectionEnd;
        percentageNum(this,event);
           this.setSelectionRange(start, end);
         });
    $(document).on("keypress",".amount-vl2",function(event){
      	 var start = this.selectionStart;
        var  end = this.selectionEnd;
           roundNum2(this,event);
           this.setSelectionRange(start, end);
         });

    $(document).on("blur",".landline-vl",function(){
    	var vl = $(this).val();
    	  if(!vl.match(/^[0-9]{6,8}$/)){
   		   makeitInvalid(this)
   	       insertErrMsg(this,"Please enter valid landline number",true);
         }else{
       	  insertErrMsg(this,"",false);
             makeitValid(this)
         }
    });
  $(document).on("blur",".std-vl",function(){ 
	var vl = $(this).val();
	  if(!vl.match(/^[0][0-9]{2,6}$/)){
		 
		   makeitInvalid(this)
	       insertErrMsg(this,"Please enter valid STD code.",true);
      }else{
    	  insertErrMsg(this,"",false);
          makeitValid(this)
      }
   });
      $(document).on("blur",".password-vl",function(){
        passwordVal(this);
    })
    $(document).on("blur",".confirm-vl",function(){     
        matchCofirmPwd(".password-vl",".confirm-vl")
    });
    $(document).on("blur",".userName",function(event){   
   
    
        onlyAtoZ(this,"Only characters with no consecutive spaces are allowed.",event);
      
    });
   
     $(document).on("blur",".pincode",function(){
    	
        validatePincode(this,"Please enter a valid 6 digits PIN code.");
        fadeOutNonmandatory(this)
      });
    $(document).on("keyup",".pincode",function(){       
            var el = $(this)
            var elv = el.val();
            el.attr("maxlength","6")
             if(elv.length > 0 ){
        
        if(!elv.match(/^[0-9]+$/)){
            
            var chopped = elv.replace(/[^0-9]$/,"")
             el.val(chopped)
            insertErrMsg(this,"Please enter a valid 6 digits PIN code.",true)
        }else{
            insertErrMsg(this,"",false)
        
         }
             }
            onlyNumerical(this,"Please enter a valid 6 digits PIN code.")
    });
   
 
       $(document).on("blur",".emailid",function(){     
        emaild(this, "Please enter a valid email address.");
    });
       $(document).on("input keypress",".url-vl, .mobile, .soft-url, .pincode, .emailid, .password-vl, .no-space, .confirm-vl",function(event){
    	   
    	   customerName(this,event);  
       });

       $(document).on("keyup", "form input[type='text'], form textarea, form input[type='password'], form input[type='email']", function (event) {
    	   var formId = $(this).parent("form").attr("id");
    	
    	   triggerSubmit(event,formId);
  
          });
     
       
       $(document).on("blur",".url-vl",function(event){        
    
       validaeUrl(this, "Please enter a valid URL.",event);
       })
    
    $(".mobile").blur(function(){     
        mobileValidation(this,"Please enter a valid 10 digits mobile number starting with 7, 8 or 9.")
    });
       
       $(document).on("keypress",".customer-id",function(){  
    	   customerName(this,event);
     });


    $(document).on("keypress",".mobile, .pincode",function(event){
    	var inp = $(this).val();
    	var code = event.which ? event.which : event.keyCode;
        var regex = new RegExp("^[0-9]+$");       
        var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if(inp.length == 0 && code == 48){
            event.preventDefault(); 
          }else{
              if (!regex.test(str) && (code != 50 && code != 9  && code != 8 && code != 37 && code != 39 && code != 46 &&  code != 118)){
              event.preventDefault();  
              }         
          }
    });
    $(document).on("input keypress",".url-vl",function(event){        
        
        customerName(this, event)
       });
    
    //datepicker

});
function calcPer(){
	if($("#aggregatorperc").length != 0){
	var a = parseFloat($("#aggregatorperc").val());
	var b = parseFloat($("#distributorperc").val());
	var c = parseFloat($("#agentperc").val());
	var d = parseFloat($("#subagentperc").val());
	var t = a + b + c + d;
	if(t !== 100){
		makeitInvalid("#aggregatorperc, #distributorperc, #agentperc, #subagentperc")
		alert("Please check user percentage bifurcation.");
	}
	else{
		makeitValid("#aggregatorperc, #distributorperc, #agentperc, #subagentperc")
	}
	}
}

function submitRechargeForm(formId,elm){
	  $(formId).submit();
}
function submitVaForm(formId,elm){
debugger
	try{   
    $("input[type='text'], textarea, input[type='password'], form input[type='email']").val().trim();
	}catch(err){
		console.log(err)
	}	
		 var allowSubmit = true;
		
		testValidationOnBlur(formId + " .pancard-vl","Please enter valid PAN Card.",/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/);
		testValidationOnBlur(formId + " .std-vl","Please enter valid STD code.",/^[0][0-9]{2,6}$/);
		testValidationOnBlur(formId + " .landline-vl","Please enter valid Landline Number.",/^[0-9]{6,8}$/);
		testValidationOnBlur(formId + " .password-vl"," Password must have at least 1 lower case character, 1 upper case character, 1 digit, 1 special character(!, @, #, $, ^, *) and length 8-15.",/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[!@#$^*]){1,})(?!.*\s).{8,15}$/);                 
        testValidationOnBlur(formId + " .otp-vl","Please enter valid OTP.",/^[0-9]{6}$/);   
        testValidationOnBlur(formId + " .alphaNum-vl","This field only accepts  alphabets and numbers.",/^[A-Za-z0-9]+$/);
        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i);
        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/[^.]{2,}/);
        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$/i);
        testValidationOnBlur(formId + " .mobile","Please enter a valid 10 digits mobile number starting with 6, 7, 8 or 9.",/^[6789]\d{9}$/);
        testValidationOnBlur(formId + " .pincode","Please enter a valid 6 digits PIN code.",/^[0-9]{6}$/);
        testValidationOnBlur(formId + " .aadhar-vl","Please enter a valid 12 digits aadhaar card number.",/^[0-9]{12}$/);
        testValidationOnBlur(formId + " .amount-vl","Please enter valid amount.",/^[0-9.]+$/);
        testValidationOnBlur(formId + " .onlyNum","Please enter only numerical value.",/^[0-9]+$/);
        testValidationOnBlur(formId + " .userName","Only characters with no consecutive spaces are allowed.",/^[a-zA-Z ]+$/);
        testValidationOnBlur(formId + " .no-space, .customer-id","This field can't have any space(s)",/^[a-zA-Z0-9@\-_:\/.=?+]+$/i);
        testValidationOnBlur(formId + " .emailid","Please enter a valid email address.",/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i);
        testValidationOnBlur(formId + " .ifscCode","Please enter a valid IFSC.",/^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$/i);
    	testValidationOnBlur(formId + " .account-vl","Please enter account number..",/\d{9,}/i);
        
        checkAmountOnSubmit();
        checkAmountOnSubmit2();
        checkUserNameOnSubmit();
        checkUserNameIdOnSubmit();
        checkUAddressOnSubmit();
        checkDiscOnSubmit();
        checkConfirlOnSubmit();
        validateEmialNPhoneSubmit(formId);
        validateUrlSubmit();
        calcPer();
        testValidationOnBlur(formId + " .mpin-vl","Please enter valid MPIN.",/^[0-9]{4,4}$/);
 
           $.each($(formId + " .mandatory"), function(index, formField) {
        	   if($(formField).val()!=null){

		   if($(formField).val().length == 0 || $(formId + " .invalid").length != 0 ||  $(formField).val() == "-1" ) {
		
		               if($(this).val().length == 0 || $(this).val() == "-1"){
		            	/*   $(".errorMsg").each(function(){
		                       var pos = $(this).prev().position();  
		                       var w = $(this).prev().outerWidth();  
		                       var h = $(this).prev().innerHeight();
		                       var t = pos.top + h;
		                       
		                      $(this).css({"top":t + 14,"width": w})
		                 });*/
		            	   if( $(this).next().is('.errorMsg')){
		            		   $(this).next().html("This field is required.").show();
		            		   
		            	   }else{
		            		   $(this).next().next().html("This field is required.").show();
		            		   
		            	   }
		               }
		           allowSubmit = false;            
		             
		    }
        	   }
		    });
       if( $(formId + " .invalid").length != 0){ 
    	    allowSubmit = false;   
       }
       if( $(formId + " .registerPanCard").length != 0){ 
   	    allowSubmit = false;   
      }
       
       if( $(formId + " .termAndCondition").length != 0 && !$(formId + " .termAndCondition").prop("checked")){ 
    	   $(".termAndConditionErr").show().text("Please accept the Terms and Conditions.")
   	    allowSubmit = false;   
   	    
      }
       
       if ( $(elm).attr("data-open-popup") == "rechage"){
    	 
    	/*   if($("#amount").val() <10){
    		   alert("inside great 10 "+$("#amount").val());
    		   $("#amountGreater10").show();
    		   allowSubmit = false; 
    	   }*/
    	   
    	   if($("#amount").val() < 10 && $("#amount").val()!=null && $("#amount").val()!=""){
   	    	
     		  
   			$("#amountGreater10").show();
   			allowSubmit = false; 
   		}
   	   
   	    if($("#amount").val() >=10){
   	    	$("#amountGreater10").hide();
   	    }
       }
       
      
       if ( $(elm).attr("data-open-popup") == "rechage" && ($("#circlei").val() == "-1" ||$("#circlei").val() == null|| $("#rechargeOperator").val() == "-1"||$("#rechargeOperator").val() == null) ){
    		
    	   //alert("inside all");
    		if($("#circlei").val() == "-1" ||$("#circlei").val() ==null){
    		//	alert("inside circle");
    			$("#opratoCireclrErr").show()
    			console.log(2)
    		}
    		if($("#rechargeOperator").val() == "-1" ||$("#rechargeOperator").val() == null){
    			//alert("inside opeartor");
    			$("#opratorErr").show()
    			console.log(3)
    		}
    	   
    	   allowSubmit = false;   
    	   
       }
       
       if ( $(elm).attr("data-open-popup") == "datacard" && ($("#datacard_circle").val() == "-1" || $("#datacard_operator").val() == "-1" ) ){
   			
   		if($("#datacard_circle").val() == "-1"){
   			$("#datacardCireclrErr").show()
   			
   		}
   		if($("#datacard_operator").val() == "-1"){
   			$("#datacardErr").show()
   			
   		}
   	   
   	   allowSubmit = false;   
   	   
      }
       
     //  alert("before");
     //  alert($(elm).attr("data-open-popup"));
      // alert($("#dth_operator").val());
       
       if ( $(elm).attr("data-open-popup") == "DTH"){
    	   
    	   
    	
    	  
    	    if($("#dth_amount").val() <20 && $("#dth_amount").val()!=null && $("#dth_amount").val()!=""){
    	    	
    		  
    			$("#dth_amountrErrGreat10").show();
    			allowSubmit = false; 
    		}
    	   
    	    if($("#dth_amount").val() >=20){
    	    	$("#dth_amountrErrGreat10").hide();
    	    }
    	   
    	   
       }
       
       if ( $(elm).attr("data-open-popup") == "DTH" && ($("#dth_operator").val() == "-1" ||$("#dth_operator").val() ==null) ){
    	  // alert("after");

   			$("#dthOpratorErr").show()
   	
   	   
   	   allowSubmit = false;   
   	   
      }
      
       if ((allowSubmit != false) && ( $(elm).attr("data-open-popup") == "rechage" ||  $(elm).attr("data-open-popup") == "electricity" || $(elm).attr("data-open-popup") == "DTH" || $(elm).attr("data-open-popup") == "datacard")){
    	   $(".responseMsg").text('')
    	   if($(elm).attr("data-open-popup") == "rechage"){
    		  
    		   mobileRechargePopUp()
    		  
    	   }else if($(elm).attr("data-open-popup") == "DTH"){
    		  
  		     $("#servicePoviderDTH").text($("#dth_operator").val())
  		      $("#dthNo").text($("#dth_mobile_no").val())
  		       $("#rechargeAmountDTH").text($("#dth_amount").val())
  		   
    		   $("#DTPpopup").modal();
    		   
    	   }else if($(elm).attr("data-open-popup") == "datacard"){
    		   $("#rechargetypeDataCard").text($(".rechargetypeDataCard:checked").val())
   		    $("#mobileNoDataCard").text($("#datacard_mobile").val())
   		     $("#servicePoviderDataCard").text($("#datacard_operator").val())
   		      $("#rechargecircleDataCard").text($("#datacard_circle").val())
   		       $("#rechargeAmountDataCard").text($("#datacard_amount").val())
   		   
    		   $("#dataCardPopup").modal();
    		   
    	   }else if($(elm).attr("data-open-popup") == "electricity"){
    	
   		    $("#electicityServicePovider").text($("#eleOpretor :selected").val())
   		    $("#consumerNumber").text($("#electricity_consumer").val())
   		    $("#electicityrechargeAmount").text(2323)
   		  
   		   $("#electicityPop").modal();
   		   
    	   }
    	   else if($(elm).attr("data-open-popup") == "gas"){
       		
      		    $("#gasServicePovider").text($("#gas-opretor :selected").val())
      		    $("#gasConsumerNumber").text($("#gas-consumer").val())
      		    $("#gasAmount").text(2323)
      		  
      		   $("#gasPopup").modal();
      		   
       	   }
    	   else if($(elm).attr("data-open-popup") == "insurar"){
          		
     		    $("#gasServicePovider").text($("#gas-opretor :selected").val())
     		    $("#gasConsumerNumber").text($("#gas-consumer").val())
     		    $("#gasAmount").text(2323)
     		  
     		   $("#insurarPopUp").modal();
     		   
      	   }

    	 
    	   
    	   	allowSubmit = false;  
       }
		
       
		   if(allowSubmit){
			   	$(elm).prop('disabled',true).text("loading...")
			  console.log(formId)
			  if ( $(elm).attr("data-oxy-popup") == "balanceReq"){
				  
				
				  
		   			
				  $.post( 'MoneyRequest', $(formId).serialize(), function(data) {
						$(elm).prop('disabled',false).val("Submit")
				        console.log(data.status)
				        
				        if(data.status == "1000" ){
				        	$("#addMoneyRequstMsg").text("Your request has been sent successfully").css("color","green")
				        }else{
				        	$("#addMoneyRequstMsg").text("Something went wrong please try after sometime.").css("color","red")
				        }
				       }
				      // I expect a JSON response
				    );
		   	   
		   	   
		   	   
		      }else{
		    	  
		    	  $(formId).submit();
		      }
		    	
			 
		    	
			} 
		// alert( allowSubmit)
		  console.log(allowSubmit);
		  return allowSubmit;
	}

function checkSelectVal(elm,target){
 var el = $(elm).val();
 $(target).val(el);
}
/*$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input:not([readonly]):not([type=radio]):not([type=checkbox]), textarea, [contentEditable], [contentEditable=true]")) {
        e.preventDefault();
    }
});*/


// Datepicker code

$(document).on("keypress input",".datepicker-here, .datepicker-here1, .datepicker-here2 ",function(event){
		event.preventDefault();
});

$(document).on("keypress", "form", function(event) { 
    return event.keyCode != 13;
});



function mobileRechargePopUp(){

	 $("#opratoCireclrErr, #opratorErr").hide()
	$("#opratoCireclrErr, #rechargeOperator").hide()
	 $("#rechargetype").text($(".rechargeTypeRadio:checked").val())
	    $("#mobileNo").text($("#rechargeMobile").val())
	     $("#servicePovider").text($("#rechargeOperator").val())
	      $("#rechargecircle").text($("#circlei").val())
	      console.log($("#amount").css("color","red"));
	       $("#rechargeAmount").text($("#amount").val())
	   
	   $("#rechagePop").modal();

}
function closeDmitWindow(){
	
	try{
		//MYWalletWINDOW.close();
	
	}catch(e){
		
	}
	try{
	
		//MYBKWINDOW.close();
	}catch(e){
		
	}
}

function validateSelectOnChange(el,err){
	//alert("call Inside validateWallet.js____validateSelectOnChange()")
	
	if($(el).val() != "-1"){
		//alert($(el).val());
		//alert(err);
		$(err).hide()
	}
}
isSESSIONCHECKED = false;
function CheckSession(){
	
	if(isSESSIONCHECKED ==  false){
	$.get('CheckSession',function(result){
		if(result.status == "FALSE"){
			alert("Your session has expired. Please login again.")
			  window.location.replace("/UserHome");
			closeDmitWindow();
			
			isSESSIONCHECKED = true;
		}
		
	})
	}
}

$(function(){

$('select').on('change',function(){

	if($(this).val() != "-1"){
				if($(this).next().is(".errorMsg")){
		   		 $(this).next(".errorMsg").hide();
		   	}else{
		   		 $(this).next().next(".errorMsg").hide();
		   	}
      
	}
})
    $('form').attr('autocomplete','off')
            if( $('select').prop('multiple') ==  false &&  $('.no-chosen-drop').length == 0){

            	try{
            	  $('select').chosen({search_contains: true})
            	}catch(err){
            		console.log(err)
            	}

            }
     
    $(document).ajaxComplete(function(){
        setTimeout(function(){
            $('form').attr('autocomplete','off')

  if( $('select').prop('multiple') ==  false && $('.no-chosen-drop').length == 0){

            try{
            $('select').chosen({search_contains: true})
            }catch(err){
            	console.log(err)
            }
  }

        },200)

    });
	$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			try{
			if(data.sessionId != SESSIONID){

				window.location = "/UserHome";
			}
			}catch(err){
				console.log(err)
			}
		 })
	
		CheckSession();
		refreshBalance()
	
		
	})
})
  

// Onboard submit form


function agentOnboardFormSubmit(formId)
{
debugger
		try{   
	    $("input[type='text'], textarea, input[type='password'], form input[type='email']").val().trim();
		}catch(err){
			console.log(err)
		}	
			 var allowSubmit = true;
			
			testValidationOnBlur(formId + " .pancard-vl","Please enter valid PAN Card.",/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/);
			testValidationOnBlur(formId + " .std-vl","Please enter valid STD code.",/^[0][0-9]{2,6}$/);
			testValidationOnBlur(formId + " .landline-vl","Please enter valid Landline Number.",/^[0-9]{6,8}$/);
			testValidationOnBlur(formId + " .password-vl"," Password must have at least 1 lower case character, 1 upper case character, 1 digit, 1 special character(!, @, #, $, ^, *) and length 8-15.",/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[!@#$^*]){1,})(?!.*\s).{8,15}$/);                 
	        testValidationOnBlur(formId + " .otp-vl","Please enter valid OTP.",/^[0-9]{6}$/);   
	        testValidationOnBlur(formId + " .alphaNum-vl","This field only accepts  alphabets and numbers.",/^[A-Za-z0-9]+$/);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/[^.]{2,}/);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$/i);
	        testValidationOnBlur(formId + " .mobile","Please enter a valid 10 digits mobile number starting with 6, 7, 8 or 9.",/^[6789]\d{9}$/);
	        testValidationOnBlur(formId + " .pincode","Please enter a valid 6 digits PIN code.",/^[0-9]{6}$/);
	        testValidationOnBlur(formId + " .aadhar-vl","Please enter a valid 12 digits aadhaar card number.",/^[0-9]{12}$/);
	        testValidationOnBlur(formId + " .amount-vl","Please enter valid amount.",/^[0-9.]+$/);
	        testValidationOnBlur(formId + " .onlyNum","Please enter only numerical value.",/^[0-9]+$/);
	        testValidationOnBlur(formId + " .userName","Only characters with no consecutive spaces are allowed.",/^[a-zA-Z ]+$/);
	        testValidationOnBlur(formId + " .no-space, .customer-id","This field can't have any space(s)",/^[a-zA-Z0-9@\-_:\/.=?+]+$/i);
	        testValidationOnBlur(formId + " .emailid","Please enter a valid email address.",/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i);
	        testValidationOnBlur(formId + " .ifscCode","Please enter a valid IFSC.",/^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$/i);
	    	testValidationOnBlur(formId + " .account-vl","Please enter account number..",/\d{9,}/i);
	        
	        checkAmountOnSubmit();
	        checkAmountOnSubmit2();
	        checkUserNameOnSubmit();
	        checkUserNameIdOnSubmit();
	        checkUAddressOnSubmit();
	        checkDiscOnSubmit();
	        checkConfirlOnSubmit();
	        validateEmialNPhoneSubmit(formId);
	        validateUrlSubmit();
	        calcPer();
	        testValidationOnBlur(formId + " .mpin-vl","Please enter valid MPIN.",/^[0-9]{4,4}$/);
	 
	           $.each($(formId + " .mandatory"), function(index, formField) {
	        	   if($(formField).val()!=null){

			   if($(formField).val().length == 0 || $(formId + " .invalid").length != 0 ||  $(formField).val() == "-1" ) {
			
			               if($(this).val().length == 0 || $(this).val() == "-1"){
			            	/*   $(".errorMsg").each(function(){
			                       var pos = $(this).prev().position();  
			                       var w = $(this).prev().outerWidth();  
			                       var h = $(this).prev().innerHeight();
			                       var t = pos.top + h;
			                       
			                      $(this).css({"top":t + 14,"width": w})
			                 });*/
			            	   if( $(this).next().is('.errorMsg')){
			            		   $(this).next().html("This field is required.").show();
			            		   
			            	   }else{
			            		   $(this).next().next().html("This field is required.").show();
			            		   
			            	   }
			               }
			           allowSubmit = false;            
			      // alert( $(formField).val());      
			    }
	        	   }
			    });
	       if( $(formId + " .invalid").length != 0){ 
	    	    allowSubmit = false;   
	       }
	      /* if( $(formId + " .registerPanCard").length != 0){ 
	   	    allowSubmit = false;   
	      }*/
	       
	       if( $(formId + " .termAndCondition").length != 0 && !$(formId + " .termAndCondition").prop("checked")){ 
	    	   $(".termAndConditionErr").show().text("Please accept the Terms and Conditions.")
	   	    allowSubmit = false;   
	   	    
	      }
	       
	      if(allowSubmit){
	    	$(formId).submit() 
	       } 
		
	      // alert( allowSubmit)
			  console.log(allowSubmit);
			  return allowSubmit;
		


}

//  Update 

function updateOnboardFormSubmit(formId)
{
debugger



		try{   
	    $("input[type='text'], textarea, input[type='password'], form input[type='email']").val().trim();
		}catch(err){
			console.log(err)
		}	
			 var allowSubmit = true;
			
			testValidationOnBlur(formId + " .pancard-vl","Please enter valid PAN Card.",/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/);
			testValidationOnBlur(formId + " .std-vl","Please enter valid STD code.",/^[0][0-9]{2,6}$/);
			testValidationOnBlur(formId + " .landline-vl","Please enter valid Landline Number.",/^[0-9]{6,8}$/);
			testValidationOnBlur(formId + " .password-vl"," Password must have at least 1 lower case character, 1 upper case character, 1 digit, 1 special character(!, @, #, $, ^, *) and length 8-15.",/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[!@#$^*]){1,})(?!.*\s).{8,15}$/);                 
	        testValidationOnBlur(formId + " .otp-vl","Please enter valid OTP.",/^[0-9]{6}$/);   
	        testValidationOnBlur(formId + " .alphaNum-vl","This field only accepts  alphabets and numbers.",/^[A-Za-z0-9]+$/);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/[^.]{2,}/);
	        testValidationOnBlur(formId + " .url-vl","Please enter valid URL.",/^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$/i);
	        testValidationOnBlur(formId + " .mobile","Please enter a valid 10 digits mobile number starting with 6, 7, 8 or 9.",/^[6789]\d{9}$/);
	        testValidationOnBlur(formId + " .pincode","Please enter a valid 6 digits PIN code.",/^[0-9]{6}$/);
	        testValidationOnBlur(formId + " .aadhar-vl","Please enter a valid 12 digits aadhaar card number.",/^[0-9]{12}$/);
	        testValidationOnBlur(formId + " .amount-vl","Please enter valid amount.",/^[0-9.]+$/);
	        testValidationOnBlur(formId + " .onlyNum","Please enter only numerical value.",/^[0-9]+$/);
	        testValidationOnBlur(formId + " .userName","Only characters with no consecutive spaces are allowed.",/^[a-zA-Z ]+$/);
	        testValidationOnBlur(formId + " .no-space, .customer-id","This field can't have any space(s)",/^[a-zA-Z0-9@\-_:\/.=?+]+$/i);
	        testValidationOnBlur(formId + " .emailid","Please enter a valid email address.",/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i);
	        testValidationOnBlur(formId + " .ifscCode","Please enter a valid IFSC.",/^[A-Za-z]{4}[0]{1}[A-Za-z0-9]{6}$/i);
	    	testValidationOnBlur(formId + " .account-vl","Please enter account number..",/\d{9,}/i);
	        
	        checkAmountOnSubmit();
	        checkAmountOnSubmit2();
	        checkUserNameOnSubmit();
	        checkUserNameIdOnSubmit();
	        checkUAddressOnSubmit();
	        checkDiscOnSubmit();
	        checkConfirlOnSubmit();
	        validateEmialNPhoneSubmit(formId);
	        validateUrlSubmit();
	        calcPer();
	        testValidationOnBlur(formId + " .mpin-vl","Please enter valid MPIN.",/^[0-9]{4,4}$/);
	 
	           $.each($(formId + " .mandatory"), function(index, formField) {
	        	   if($(formField).val()!=null){

			   if($(formField).val().length == 0 || $(formId + " .invalid").length != 0 ||  $(formField).val() == "-1" ) {
			
			               if($(this).val().length == 0 || $(this).val() == "-1"){
			            	/*   $(".errorMsg").each(function(){
			                       var pos = $(this).prev().position();  
			                       var w = $(this).prev().outerWidth();  
			                       var h = $(this).prev().innerHeight();
			                       var t = pos.top + h;
			                       
			                      $(this).css({"top":t + 14,"width": w})
			                 });*/
			            	   if( $(this).next().is('.errorMsg')){
			            		   $(this).next().html("This field is required.").show();
			            		   
			            	   }else{
			            		   $(this).next().next().html("This field is required.").show();
			            		   
			            	   }
			               }
			           allowSubmit = false;            
			      // alert( $(formField).val());      
			    }
	        	   }
			    });
	       if( $(formId + " .invalid").length != 0){ 
	    	    allowSubmit = false;   
	       }
	      /* if( $(formId + " .registerPanCard").length != 0){ 
	   	    allowSubmit = false;   
	      }*/
	       
	       if( $(formId + " .termAndCondition").length != 0 && !$(formId + " .termAndCondition").prop("checked")){ 
	    	   $(".termAndConditionErr").show().text("Please accept the Terms and Conditions.")
	   	    allowSubmit = false;   
	   	    
	      }
	       
	      if(allowSubmit){
	    	$(formId).submit() 
	       } 
	      document.getElementById("updateOnboardForm").disabled=true; 	
	      // alert( allowSubmit)
			  console.log(allowSubmit);
			  return allowSubmit;
		


}



