function selectTab(id) // it sets the Background color and border.
{
  try{	
    document.getElementById(id).style.backgroundColor='#E9E9F3';
    document.getElementById(id).style.border='1px solid #4F88EC';
  }catch(e){}
}



function trim(stringToTrim) // it clear white spaces trailing and following the text.
{
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function isAlpha(thisValue) // it checks whether passed value contains Alphabets only or not.
{
	var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
	var strChar;
	var blnResult = true;
	var t1 =thisValue;	
	for (i = 0; i < t1.length && blnResult == true; i++)
	{
		strChar = t1.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)
		{
			blnResult = false;			
			return false;
		}
	}
	return true;
}

function isNumeric(thisValue) // it checks whether passed value is Numeric only or not.
{
	var strValidChars = "0123456789.,";
	var strChar;
	var blnResult = true;
	var t1 =thisValue;	
	for (i = 0; i < t1.length && blnResult == true; i++)
	{
		strChar = t1.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)
		{
			blnResult = false;
			return false;			
		}
	}
	return true;
}

/*
 * isAlphaNumeric(thisValue)
 * 	it checks whether passed value is AlphaNumeric or not.
 * 	AlphaNumeric values contain Alphabets and Numeric only with dashes and underscores in between
 * 	but they must not start or end with a dash or an underscore.
 *  
 */
function isAlphaNumeric(thisValue) 
{
	var s=thisValue;
	if (s.match(/^[A-Za-z][A-Za-z0-9-_]*[A-Za-z0-9]$/)){
		return true;
	}else{
		return false;
	}
}

function containNumeric(thisValue) // it checks whether given value contains Numeric(with or without other characters) or not.
{
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = false;
	var t1 =thisValue;	
	for (i = 0; i < t1.length; i++)
	{
		strChar = t1.charAt(i);
		if (strValidChars.indexOf(strChar) != -1)
		{
			blnResult = true;
			return true;			
		}
	}
	return false;
}

function containSpecialChar(thisValue) // it checks whether given value contains Special Character(with or without other characters) or not.

{
	var strValidChars = "@#!|?$";
	var strChar;
	var blnResult = false;
	var t1 =thisValue;	
	for (i = 0; i < t1.length; i++)
	{
		strChar = t1.charAt(i);
		if (strValidChars.indexOf(strChar) != -1)
		{
			blnResult = true;
			return true;			
		}
	}
	return false;
}

function echeck(str) // it checks whether the passed string is a valid e-mail id or not.
{

        var at="@"
        var dot="."
        var lat=str.indexOf(at)
        var lstr=str.length
        var ldot=str.indexOf(dot)
        if (str.indexOf(at)==-1){
           alert("Please enter valid email-Id.");
           return false
        }

        if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
           alert("Please enter valid email-Id.");
           return false
        }

        if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
            alert("Please enter valid email-Id");
            return false
        }

         if (str.indexOf(at,(lat+1))!=-1){
            alert("Please enter valid email-Id");
            return false
         }

         if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
            alert("Please enter valid email-Id");
            return false
         }

         if (str.indexOf(dot,(lat+2))==-1){
            alert("Please enter valid email-Id");
            return false
         }

         if (str.indexOf(" ")!=-1){
            alert("Please enter valid email-Id");
            return false
         }

         return true					
}
function checkTransactionAmount()
{
	var frmObj=document.frmNewApp;
    	
	
    var amountTx=trim(frmObj.supportAmountTx.value);
    var amountTxD=trim(frmObj.supportAmountD.value);
    var amountTxW=trim(frmObj.supportAmountW.value);
    var amountTxM=trim(frmObj.supportAmountM.value);
    var TxD=trim(frmObj.supportTxD.value);
    var TxW=trim(frmObj.supportTxW.value);
    var TxM=trim(frmObj.supportTxM.value);
	
    if(amountTx=="") amountTx=0;
    if(amountTxD=="") amountTx=0;
    if(amountTxW=="") amountTxW=0;
    if(amountTxM=="") amountTxM=0;
    if(TxD=="") TxD=0;
    if(TxW=="") TxW=0;
    if(TxM=="") TxM=0;
    
    // on amount volume     
     if ((eval(amountTx)!=0 && eval(amountTxD)!=0) && (eval(amountTx)>eval(amountTxD)) ){
		 alert("Amount per transaction must be less than amount per day");
		 frmObj.supportAmountTx.select();
		 return false;
	 }
	 if ((eval(amountTx)!=0 && eval(amountTxD)==0 && eval(amountTxW)!=0) && (eval(amountTx)>eval(amountTxW)) ){
		 alert("Amount per transaction must be less than amount per week");
		 frmObj.supportAmountTx.select();
		 return false;
	 }
	 if(eval(amountTx)!=0 && eval(amountTxD)==0 && eval(amountTxW)==0 && eval(amountTxM)!=0 && eval(amountTx)>eval(amountTxM)){
		 alert("Amount per transaction must be less than amount per month");
		 frmObj.supportAmountTx.select();
		 return false;
 	 }
	 if ((eval(amountTxD)!=0 && eval(amountTxW)!=0) && (eval(amountTxD)>eval(amountTxW)) ){
		 alert("Amount per day must be less than amount per Week");
		 frmObj.supportAmountTxD.select();
		 return false;
	 }
	 if ((eval(amountTxD)!=0 && eval(amountTxW)==0 && eval(amountTxM)!=0) && (eval(amountTxD)>eval(amountTxM)) ){
		 alert("Amount per day must be less than amount per month");
		 frmObj.supportAmountTxD.select();
		 return false;
	 }
	 if ((eval(amountTxW)!=0)&& eval(amountTxM)!=0 && (eval(amountTxW)>eval(amountTxM))){
		 alert("Amount per week must be less than amount per month");
		 frmObj.supportAmountTxW.select();
		 return false;
	 }
     
	 // on number of transactions
	 
	 if (eval(TxD) != 0 && eval(TxW) != 0 && eval(TxD) > eval(TxW)){
		 alert("Transactions per day must be less than transactions per week");
		 frmObj.supportTxD.select();
		 return false;
	 }
	 if (eval(TxD) != 0 && eval(TxW) == 0 && eval(TxM) != 0 && eval(TxD) > eval(TxM)){
		 alert("Transactions per day must be less than transactions per month");
		 frmObj.supportTxD.select();
		 return false;
	 }
	 if (eval(TxW) != 0 && eval(TxM) != 0 && eval(TxW) > eval(TxM)){
		 alert("Transactions per week must be less than transactions per month");
		 frmObj.supportTxW.select();
		 return false;
	 }
	 
return true;
}

