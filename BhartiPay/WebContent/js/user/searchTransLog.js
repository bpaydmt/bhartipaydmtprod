function checkValidation()
{
    if(isAlphaNumeric(document.frmSearchLog.appTransId.value)==false)
    {
            alert("Invalid AppTransID.");
            return false;
    }
    if(isAlphaNumeric(document.frmSearchLog.systemTransId.value)==false)
    {
            alert("Invalid SystemTransID.");
            return false;
    }
    if(isAlphaNumeric(document.frmSearchLog.paymentGatewayTransId.value)==false)
    {
            alert("Invalid PaymentGatewayTransID.");
            return false;
    }
    if(isNumeric(document.frmSearchLog.msisdn.value)==false)
    {
            alert("Invalid MSISDN.");
            return false;
    }
    if(isNumeric(document.frmSearchLog.transAmount.value)==false)
    {
            alert("Invalid Amount.");
            return false;
    }
    
    
    return true;
}

