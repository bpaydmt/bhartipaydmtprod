/*
 * checkUserValidation()
 * 		It gets called when a user is created or edited or password is changed.
 * 		It checks for all the fields whether they are valid or not using the functions of ValidationFunctions.js.
 * 
 */

function checkUserValidation()
{
	// It performs following Validations.
	//alert('here');
	// on User Name
    if(trim(document.frmNewUser.userId.value)=="")  // not blank.
    {
        alert("Please enter User Name.");
        document.frmNewUser.userId.focus();
        return false;
    }
    else if(!isAlphaNumeric(document.frmNewUser.userId.value)) // Alphanumeric.
    {
        alert("Please enter alphanumeric User Name - it must start with an alphabet and must not end with an underscore or dash.");
        document.frmNewUser.userId.focus();
        return false;
    }
    else if(document.frmNewUser.userId.value.length<5) // at least 5 characters long.
    {
        alert("User Name must be atleast 5 characters in length.");
        document.frmNewUser.userId.focus();
        return false;
    }
    
    // on Password
    if(document.frmNewUser.password)
    {
        if(trim(document.frmNewUser.password.value)=="") // not blank.
        {
            alert("Please enter Password.");
            document.frmNewUser.password.focus();
            return false;
        }
        else if(document.frmNewUser.password.value.length<8) // at least 8 characters long.
        {
            alert("Password must be atleast 8 char long.");
            document.frmNewUser.password.focus();
            return false;
        }
        else if(!containNumeric(document.frmNewUser.password.value) || !containSpecialChar(document.frmNewUser.password.value)) // at least contains one numeric and one special character.
        {
            alert("Password must be alphanumeric and include atleast 1 special character(special chars @, #, !, |, ?, $).");
            document.frmNewUser.password.focus();
            return false;
        }
      
        //on Confirm Password
        if(trim(document.frmNewUser.cpassword.value)=="") // not blank.
        {
            alert("Please enter Confirm Password.");
            document.frmNewUser.cpassword.focus();
            return false;
        }
 
        if(trim(document.frmNewUser.password.value)!=trim(document.frmNewUser.cpassword.value)) // matches with the Password field.
        {
            alert("Password and Confirm Password do not match.");
            document.frmNewUser.password.value="";
            document.frmNewUser.cpassword.value="";
            document.frmNewUser.password.focus();
            return false;
        }        
    }    

    // on Allowed Applications  
    if(trim(document.frmNewUser.userType.value)=="MIS" && trim(document.frmNewUser.appIdsAllowed.value)=="") // user selects at least one application.
    {
        alert("Please select atleast one application.");
        document.frmNewUser.appIdsSelect.focus();
        return false;
    }

    // on Full Name
    
    if(trim(document.frmNewUser.userName.value)=="") // not blank.
    {
        alert("Please enter Full Name.");
        document.frmNewUser.userName.focus();
        return false;
    }
    else if(!isAlpha(document.frmNewUser.userName.value)) // only alphabets.
    {
        alert("Please enter correct Full Name (Only Alphabets allowed).");
        document.frmNewUser.userName.focus();
        return false;
    }
    else if(document.frmNewUser.userName.value.length<5) // at least 2 characters long.
    {
        alert("Full Name must be atleast 5 characters in length.");
        document.frmNewUser.userName.focus();
        return false;
    }

    // on E-mail
    
    if(trim(document.frmNewUser.userEmail.value)=="") // valid e-mail id.
    {
        alert("Please enter email-Id.");
        document.frmNewUser.userEmail.focus();
        return false;
    }
    else if(!echeck(document.frmNewUser.userEmail.value))
    {
        return false;
    }
	
    // on Mobile Number
    
    if(trim(document.frmNewUser.userMobile.value)!="") // not blank.
    {
 	    if(document.frmNewUser.userMobile.value.length!=10) // 10 digits.
	    {
	        alert("Please enter a 10 digit Mobile Number.");
	        document.frmNewUser.userMobile.focus();
	        return false;
	    }else if(isNaN(document.frmNewUser.userMobile.value)) // contains Numeric only.
	    {
	        alert("Please enter a 10 digit Mobile Number.");
	        document.frmNewUser.userMobile.focus();
	        return false;
	    }
	    if((document.frmNewUser.userMobile.value)<0) // not negative.
		{
			alert("Negative numbers not allowed");
			document.frmNewUser.userMobile.focus();
			return false;
		}
    }
    if(trim(document.frmNewUser.userType.value)=="")
    {
        alert("Please select User Type.");
        document.frmNewUser.userType.focus();
        return false;
    }
         
   
    //alert('return');     
    return true;
}

/*
 * CheckAppValidation()
 * 		It gets called when an application is created or edited.
 * 		It checks for all the fields whether they are valid or not using the functions of ValidationFunctions.js.
 * 
 */
function checkAppValidation()
{  
	 
		// It performs following Validations.
	
		// on Application ID
		if(trim(document.frmNewApp.appId.value)=="") // not blank.
	    {
	        alert("Please enter Application Name");
	        document.frmNewApp.appId.focus();
	        return false;
	    }
	    else if(!isAlphaNumeric(document.frmNewApp.appId.value)) // Alphanumeric.
	    {
	        alert("Please enter alphanumeric Application Name - it must start with an alphabet and must not end with an underscore or dash.");
	        return false;
	    }
	   
		// on Valid From
	    if(trim(document.frmNewApp.validFrom.value)=="") // must be selected.
	    {
	        alert("Please enter Validity Period of Application.");
	        document.frmNewApp.validFrom.focus();
	        return false;
	    }
	
	    // on Valid To
	    if(trim(document.frmNewApp.validTo.value)=="") // must be selected.
	    {
	        alert("Please enter Validity Period of Application.");
	        document.frmNewApp.validTo.focus();
	        return false;
	    }
	    if(!isDates(document.frmNewApp.validFrom.value,document.frmNewApp.validTo.value,"Valid To date should be greater than Valid From date.")) return false;;
	    
	    // on App Type
	    if(trim(document.frmNewApp.appType.value)=="") // must be selected.
	    {
	        alert("Please select App Type.");
	        document.frmNewApp.appType.focus();
	        return false;
	    }
	    
	    // on Allowed Payment Gateway
	    if(trim(document.frmNewApp.allowedPaymentGateway.value)=="") // must be selected.
	    {
	        alert("Please select Allowed Payment Gateway.");
	        document.frmNewApp.allowedPaymentGateway.focus();
	        return false;
	    }
	    
	    //On Primary Email
	    /*if(trim(document.frmNewApp.primaryEmail.value)=="") // valid e-mail id.
	    {
	        alert("Please enter primary email-Id.");
	        document.frmNewUser.primaryEmail.focus();
	        return false;
	    }
	    else if(!echeck(document.frmNewApp.primaryEmail.value))
	    {
	        return false;
	    }*/
	    
	    if(document.frmNewApp.restrictionFlag.checked==true) // checks whether restriction flag is set or not.
	    {

	    		// on Maximum Amount per Transaction.
	    		if(isNaN(document.frmNewApp.supportAmountTx.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Amount");
	    			document.frmNewApp.supportAmountTx.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportAmountTx.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportAmountTx.focus();
	    			return false;
	    		}
	    	
	    		// on Maximum Amount per Day
	    		if(isNaN(document.frmNewApp.supportAmountD.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Amount");
	    			document.frmNewApp.supportAmountD.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportAmountD.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportAmountD.focus();
	    			return false;
	    		}
	    		
	    		// on Maximum Amount per Week
	    		if(isNaN(document.frmNewApp.supportAmountW.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Amount");
	    			document.frmNewApp.supportAmountW.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportAmountW.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportAmountW.focus();
	    			return false;
	    		}
	    	
	    		// on Maximum Amount per Month
	    		if(isNaN(document.frmNewApp.supportAmountM.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Amount");
	    			document.frmNewApp.supportAmountM.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportAmountM.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportAmountM.focus();
	    			return false;
	    		}
	    		
	    		// on Maximum Transaction per Day
	    		if(isNaN(document.frmNewApp.supportTxD.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Number");
	    			document.frmNewApp.supportTxD.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportTxD.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportTxD.focus();
	    			return false;
	    		}
	    		
	    		// on Maximum Transaction per Week
	    		if(isNaN(document.frmNewApp.supportTxW.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Number");
	    			document.frmNewApp.supportTxW.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportTxW.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportTxW.focus();
	    			return false;
	    		}
	    		
	    		// on Maximum Transaction per Month
	    		if(isNaN(document.frmNewApp.supportTxM.value)) // contains Numeric only.
	    		{
	    			alert("Please Enter Valid Number");
	    			document.frmNewApp.supportTxM.focus();
	    			return false;
	    		}
	    		if((document.frmNewApp.supportTxM.value)<0) // not negative.
	    		{
	    			alert("Negative numbers not allowed");
	    			document.frmNewApp.supportTxM.focus();
	    			return false;
	    		}
	    		
	    }
	    
	    // on Full Name
	    if(trim(document.frmNewApp.userName.value)=="") // not blank.
	    {
	        alert("Please enter Full Name.");
	        document.frmNewApp.userName.focus();
	        return false;
	    }
	    else if(!isAlpha(document.frmNewApp.userName.value)) // only alphabets.
	    {
	        alert("Please enter correct Full Name (Only Alphabets allowed).");
	        document.frmNewApp.userName.focus();
	        return false;
	    }
	    else if(document.frmNewApp.userName.value.length<5) // at least 2 characters long.
	    {
	        alert("Full Name must be atleast 5 characters in length.");
	        document.frmNewApp.userName.focus();
	        return false;
	    }
	    
	    // on E-mail
	    if(trim(document.frmNewApp.supportEmail.value)=="") // not blank.
	    {
	        alert("Please enter Support email-Id.");
	        document.frmNewApp.supportEmail.focus();
	        return false;
	    }else if(!echeck(document.frmNewApp.supportEmail.value))    { // valid e-mail id.       
	        return false;
	    }
	    
	    // on Mobile Number
	    if(trim(document.frmNewApp.supportMobile.value)!="") // not blank.
	    {
		    if(document.frmNewApp.supportMobile.value.length!=10) // 10 digits.
		    {
		        alert("Please enter a 10 digit Support Mobile Number.");
		        document.frmNewApp.supportMobile.focus();
		        return false;
		    }else  if(isNaN(document.frmNewApp.supportMobile.value) ) // contains Numeric only.
		    {
		        alert("Please enter a 10 digit Support Mobile Number.");
		        document.frmNewApp.supportMobile.focus();
		        return false;
		    } 
		    if((document.frmNewApp.supportMobile.value)<0) // not negative.
			{
				alert("Negative numbers not allowed");
				document.frmNewApp.supportMobile.focus();
				return false;
			}
	    }
	    
	    // on User Name
	    if(trim(document.frmNewApp.userId.value)=="") // not blank.
	    {
	        alert("Please enter User Name.");
	        document.frmNewApp.userId.focus();
	        return false;
	    }
	    else if(!isAlphaNumeric(document.frmNewApp.userId.value)) // Alphanumeric.
	    {
	        alert("Please enter alphanumeric User Name - it must start with an alphabet and must not end with an underscore or dash.");
	        document.frmNewApp.userId.focus();
	        return false;
	    }
	    else if(document.frmNewApp.userId.value.length<5) // at least 5 characters long.
	    {
	        alert("User Name must be atleast 5 characters in length.");
	        document.frmNewApp.userId.focus();
	        return false;
	    }
	  
	    // on Password
	    if(document.frmNewApp.password){
		    if(trim(document.frmNewApp.password.value)=="") // not blank.
		    {
		        alert("Please enter Password.");
		        document.frmNewApp.password.focus();
		        return false;
		    }
		    else if(document.frmNewApp.password.value.length<8) // at least 8 characters long.
		    {
		        alert("Password must be atleast 8 char long.");
		        document.frmNewApp.password.focus();
		        return false;
		    }
		    else if(!containNumeric(document.frmNewApp.password.value)|| !containSpecialChar(document.frmNewApp.password.value)) // at least contains one numeric and one special character.
		    {
		        alert("Please include atleast 1 numeric and 1 special character in Password (special chars @, #, !, |, ?, $).");
		        document.frmNewApp.password.focus();
		        return false;
		    }
		    
		    // on Confirm Password
		    if(trim(document.frmNewApp.cpassword.value)=="") // not blank.
		    {
		        alert("Please enter Confirm Password.");
		        document.frmNewApp.cpassword.focus();
		        return false;
		    }
		    if(trim(document.frmNewApp.password.value)!=trim(document.frmNewApp.cpassword.value)) // matches with the Password field.
		    {
		        alert("Password and Confirm Password do not match.");
		        document.frmNewApp.password.value="";
		        document.frmNewApp.cpassword.value="";
		        document.frmNewApp.password.focus();
		        return false;
		    }
	    }
	    
	   
	    
    return true;
}


/*
 * CheckPwdUpdateValidation()
 * 		It gets called when Password is changed.
 *  
 */

function checkPwdUpdateValidation(){
try{
	// It performs following Validations.
	
	// on Current Password
	if(trim(document.UpdatePasswordSave.currpassword.value)=="") // not blank.
    {
        alert("Please enter Current Password.");
        document.UpdatePasswordSave.currpassword.focus();
        return false;
    }
	
	// on New Password
    if(trim(document.UpdatePasswordSave.newPassword.value)=="") // not blank.
    {
        alert("Please enter New Password.");
        document.UpdatePasswordSave.newPassword.focus();
        return false;
    }
	
    if(document.UpdatePasswordSave.newPassword.value.length<8) // at least 8 characters long.
        {
            alert("New Password must be atleast 8 char long.");
            document.UpdatePasswordSave.newPassword.focus();
            return false;
        }
  
    if(!containNumeric(document.UpdatePasswordSave.newPassword.value)|| !containSpecialChar(document.UpdatePasswordSave.newPassword.value)) // at least contains one numeric and one special character.
        {
            alert("Please include atleast 1 numeric and 1 special character in Password (special chars @, #, !, |, ?, $).");
            document.UpdatePasswordSave.newPassword.focus();
            return false;
        }
      
    // on Confirm Password
    if(trim(document.UpdatePasswordSave.confPassword.value)=="") // not blank.
        {
            alert("Please enter Confirm New Password.");
            document.UpdatePasswordSave.confPassword.focus();
            return false;
        }

    if(trim(document.UpdatePasswordSave.newPassword.value)!=trim(document.UpdatePasswordSave.confPassword.value)) // matches with Password field.
        {
            alert("New Password and Confirm New Password do not match.");
            document.UpdatePasswordSave.newPassword.value="";
            document.UpdatePasswordSave.confPassword.value="";
            document.UpdatePasswordSave.newPassword.focus();
            return false;
        }        
    plsWaitText();
}catch(e){}
	return true;
}
 function plsWaitText(){
		document.getElementById('plswaitId').innerHTML = '<font color="blue">Please Wait .......</font>';
	}

function resetSearchPage(action) // it resets the View MIS Page. 
{

    var obj=eval("document."+action);;
  try{
    if(obj.appTransId!=null)  	  
    	obj.appTransId.value="";
    if(obj.paymentGatewayTransId!=null)  	  
    	obj.paymentGatewayTransId.value="";
    if(obj.systemTransId!=null)  	 
    	obj.systemTransId.value="";
    if(obj.cliTransId!=null)  	 
    	obj.cliTransId.value="";
    if(obj.fromDateId!=null)  	 
    	obj.fromDateId.value="";
    if(obj.toDateId!=null)  	 
    	obj.toDateId.value="";
    
    if(obj.appName!=null)  	 
    	obj.appName.value="";
    if(obj.pgId!=null)  	 
    	obj.pgId.value="0";
  }catch(e){}
}
function resetSuspectPage(action)
{
	 var obj=eval("document."+action);;
	 obj.fromDate.value="";
	 obj.toDate.value="";
	 obj.appName.value="";
}

function resetUserSearchPage() // it resets the Search User Page.
{

    var obj=document.form_name;
    obj.userId.value="";
    obj.userName.value="";
    obj.userEmail.value="";
    obj.userMobile.value="";
    obj.form_name_uso_userType.value="MIS";
    obj.form_name_uso_userStatus.value="1";
    obj.form_name_uso_availableAppsForm.value="";
}

function resetAppCreatePage() // it resets the Add Application Page.
{

    var obj=document.frmNewApp;
    obj.appId.value="";
    obj.appDispName.value="";
    obj.validFrom.value="";
    obj.validTo.value="";
    obj.appType.value="";
    obj.allowedPaymentGateway.value="0";
    obj.appStatus.value="0";
    obj.callbackURL.value="";
    obj.supportAmountTx.value="";
    obj.supportAmountW.value="";
    obj.supportAmountM.value="";
    
    obj.currency.value="INR";
    obj.dispCurrency.value="";
    obj.maxCardPerUser.value="0";
    obj.mulUserPerCard.value="0";
    obj.maxValidAttempt.value="0";
	  
    obj.userId.value="";
    obj.userName.value="";
    obj.supportEmail.value="";
    obj.supportMobile.value="";
    
    obj.appStatus.value="1";
    obj.password.value="";
    obj.cpassword.value="";
    
    obj.restrictionFlag.checked=false;
    showRes(obj.restrictionFlag);
    
    showOTL(obj.appType.value);
    
}

function resetGatewaySearch() // it resets the Search Gateway Page.
{
try{
    var obj=document.GatewaySearchResult;
    obj.validFrom.value="";
    obj.validTo.value="";
    obj.GatewaySearchResult_gso_name.value="All";
    obj.GatewaySearchResult_gso_type.value="All";
}catch(e){}

}

function resetApplicationSearch() // it resets the Search Application Page.
{
	
try{
    var obj=document.form_name;
    obj.form_name_searchObject_appName.value="";
    obj.form_name_searchObject_pgId.value="0";
    obj.form_name_searchObject_appType.value="";
    obj.form_name_searchObject_status.value="1";
    
    obj.fromDate.value="";
    obj.toDate.value="";

}catch(e){
	
	
}

}

function resetaddtransactionpage()
{
	var obj=document.SaveTransaction;
	
	obj.applicationUserId.value="";
	obj.amount.value="";
	obj.appTxnId.value="";
	
}

/*
 * To compare to dates( date1 and date2, msg will be the error message
 * */

function isDates(date1,date2,msg)
{
	//alert("calling isDates()");
	var from=date1.split('-');
    var dateFromYear = from[2];
    var dateFromMon = from[1];
    var dateFromDay = from[0];
    var dateFrom = new Date(dateFromMon+" "+dateFromDay+","+dateFromYear);
    
    var to=date2.split('-');
    var dateToYear = to[2];
    var dateToMon = to[1];
    var dateToDay = to[0];
    var dateTo = new Date(dateToMon+" "+dateToDay+","+dateToYear);

    if(dateFrom>dateTo)
    {
    	alert(msg);
    	return false;
    }
    return true;
}
// Block page Scripts
var showRow = (navigator.appName.indexOf("Internet Explorer") != -1) ? "block" : "table-row";

function block() {
	try{
	var frmobj=	document.forms["frm"];
	if(frmobj.actionType[0].checked)
		 document.frm.action="BlockedUserSearch";
	else if(frmobj.actionType[1].checked) 
		 document.forms["frm"].action="BlockUnblockIdentifier";
	else
	{
		alert("Please select atleast one action type");
		return false;
	}	
	}catch(e){document.frm.action="BlockedUserSearch";}
	document.forms["frm"].submit();
	return true;
}

function resetManageBlocking()
{
	var frmobj=	document.forms["frm"];
   if(frmobj.type!=null)
	frmobj.type.value='CC';
   if(frmobj.identifier!=null)	
	frmobj.identifier.value='';
   if(frmobj.appId!=null)	
	frmobj.appId.value='';
   if(frmobj.startdate!=null)	
	frmobj.startdate.value='';
   if(frmobj.enddate!=null)	{
	frmobj.enddate.value='';
	if(frmobj.actionType[1].checked){
	  frmobj.enddate.value=frmobj.futureDate.value;
	}
   }
   if(frmobj.blockedby!=null)	
	frmobj.blockedby.value='';
   if(frmobj.remarks!=null)		
	frmobj.remarks.value='';
   showApp();
}

function show(objValue){

	var frmobj=	document.forms["frm"];
	if(objValue=='B'){	
		  document.getElementById("remId").style.display=showRow;

		  document.getElementById("blbyId").style.display="none"; 
		  frmobj.blockedby.value="";
		  document.getElementById("stDateId").style.display="none"; 
		  frmobj.startdate.value="";
		  frmobj.enddate.value="";
		
	  }else
	  {
		  document.getElementById("remId").style.display="none"; 
		  if(frmobj.remarks!=null)		
		  	frmobj.remarks.value="";
		  document.getElementById("blbyId").style.display=showRow;
		  document.getElementById("stDateId").style.display=showRow;
	   }	
	  showApp();
}
function showApp(){
	//var frmobj=	document.forms["frm"];
	//var bType=frmobj.type.value;
	//if(bType=='USER'){	
	//	frmobj.appId.disabled=false;
		 // frmobj.appId.value="";
	//  }else
	//  {
	//	  frmobj.appId.disabled=true;
	//	  frmobj.appId.value="";
	//   }	
}
function fnChngeAction(ob)
{
	try{	
		var obj=document.getElementById("userAction"+ob);
		obj.value="edit";
	}catch(e){}
}
function fnClrAction(ob)
{
  try{	
		
		var obj=document.getElementById("userAction"+ob);
		obj.value="";
	}catch(e){}
}
function changePageNo(no)
{
	document.frm.action="BlockedUserSearch";
	document.frm.pageno.value=no;
	document.frm.submit();
 }
function disabledApp()
{
	var frmobj=	document.forms["frm"];
  if(frmobj.identifier.value!=''){	
	if(frmobj.type.value=='CC')
	{
		  frmobj.appId.disabled=true;
		  frmobj.appId.value="";
	}else
	{
		frmobj.appId.disabled=false;
		 // frmobj.appId.value="";
	}
  }	
}
function isInt(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}


function validateCCLimitPage()
{
	  // perTxnMaxAmt
	  if(isNaN(document.form_name.perTxnMaxAmt.value)) // contains Numeric only.
	    {
	    	alert("Please Enter Valid Amount");
	    	document.form_name.perTxnMaxAmt.select();
	    	return false;
	    }
	    if((document.form_name.perTxnMaxAmt.value)<0) // not negative.
	    	{
	    		alert("Negative numbers not allowed");
	    		document.form_name.perTxnMaxAmt.select();
	    		return false;
	    	}
	    if((document.form_name.perTxnMaxAmt.value)=='') // not negative.
   	     {
	    	document.form_name.perTxnMaxAmt.value=0;
	      }
	    
	 // perDayTxnMaxAmt
		  if(isNaN(document.form_name.perDayTxnMaxAmt.value)) // contains Numeric only.
		    {
		    	alert("Please Enter Valid Amount");
		    	document.form_name.perDayTxnMaxAmt.select();
		    	return false;
		    }
		    if((document.form_name.perDayTxnMaxAmt.value)<0) // not negative.
		    	{
		    		alert("Negative numbers not allowed");
		    		document.form_name.perDayTxnMaxAmt.select();
		    		return false;
		    	}
		    if((document.form_name.perDayTxnMaxAmt.value)=='') // not negative.
	   	     {
		    	document.form_name.perDayTxnMaxAmt.value=0;
		      }
		 // perWeekTxnMaxAmt
			  if(isNaN(document.form_name.perWeekTxnMaxAmt.value)) // contains Numeric only.
			    {
			    	alert("Please Enter Valid Amount");
			    	document.form_name.perWeekTxnMaxAmt.select();
			    	return false;
			    }
			    if((document.form_name.perWeekTxnMaxAmt.value)<0) // not negative.
			    	{
			    		alert("Negative numbers not allowed");
			    		document.form_name.perWeekTxnMaxAmt.select();
			    		return false;
			    	}
			    if((document.form_name.perWeekTxnMaxAmt.value)=='') // not negative.
		   	     {
			    	document.form_name.perWeekTxnMaxAmt.value=0;
			      }
	    
			 // perMonthTxnMaxAmt
				  if(isNaN(document.form_name.perMonthTxnMaxAmt.value)) // contains Numeric only.
				    {
				    	alert("Please Enter Valid Amount");
				    	document.form_name.perMonthTxnMaxAmt.select();
				    	return false;
				    }
				    if((document.form_name.perMonthTxnMaxAmt.value)<0) // not negative.
				    	{
				    		alert("Negative numbers not allowed");
				    		document.form_name.perMonthTxnMaxAmt.select();
				    		return false;
				    	}
				    if((document.form_name.perMonthTxnMaxAmt.value)=='') // not negative.
			   	     {
				    	document.form_name.perMonthTxnMaxAmt.value=0;
				      }
	    
				 // noOfTxnPerDay
					if(isNaN(document.form_name.noOfTxnPerDay.value) || (document.form_name.noOfTxnPerDay.value).indexOf('.')>0) // contains Numeric only.
					    {
					    	alert("Please Enter a Valid Number");
					    	document.form_name.noOfTxnPerDay.select();
					    	return false;
					    }
					if((document.form_name.noOfTxnPerDay.value)<0) // not negative.
					    	{
					    		alert("Negative numbers not allowed");
					    		document.form_name.noOfTxnPerDay.select();
					    		return false;
					    	}
					if((document.form_name.noOfTxnPerDay.value)=='') // not negative.
				   	     {
					    	document.form_name.noOfTxnPerDay.value=0;
					      }
					// noOfTxnPerWeek
					if(isNaN(document.form_name.noOfTxnPerWeek.value)|| (document.form_name.noOfTxnPerWeek.value).indexOf('.')>0) // contains Numeric only.
						    {
						    	alert("Please Enter a Valid Number");
						    	document.form_name.noOfTxnPerWeek.select();
						    	return false;
						    }
					 if((document.form_name.noOfTxnPerWeek.value)<0) // not negative.
				    	{
				    		alert("Negative numbers not allowed");
				    		document.form_name.noOfTxnPerWeek.select();
				    		return false;
				    	}
				    if((document.form_name.noOfTxnPerWeek.value)=='') // not negative.
			   	     {
				    	document.form_name.noOfTxnPerWeek.value=0;
				      }
				    
				 // noOfTxnPerMonth
					  if(isNaN(document.form_name.noOfTxnPerMonth.value)|| (document.form_name.noOfTxnPerMonth.value).indexOf('.')>0) // contains Numeric only.
					    {
					    	alert("Please Enter a Valid Number");
					    	document.form_name.noOfTxnPerMonth.select();
					    	return false;
					    }
					    if((document.form_name.noOfTxnPerMonth.value)<0) // not negative.
					    	{
					    		alert("Negative numbers not allowed");
					    		document.form_name.noOfTxnPerMonth.select();
					    		return false;
					    	}
					    if((document.form_name.noOfTxnPerMonth.value)=='') // not negative.
				   	     {
					    	document.form_name.noOfTxnPerMonth.value=0;
					      } 
							    
}    



