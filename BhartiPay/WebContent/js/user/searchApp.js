function checkValidation()
{
    if(isAlphaNumeric(document.frmSearch.appId.value)==false)
    {
            alert("Invalid AppID.");
            return false;
    }
    if(isAlphaNumeric(document.frmSearch.description.value)==false)
    {
            alert("Invalid characters in Description.");
            return false;
    }
    if(isNumeric(document.frmSearch.ipsAllowed.value)==false)
    {
            alert("Invalid IP.");
            return false;
    }
    if(isAlphaNumeric(document.frmSearch.supportEmail.value)==false)
    {
            alert("Invalid entry for Support Email.");
            return false;
    }
    if(isNumeric(document.frmSearch.supportMobile.value)==false)
    {
            alert("Invalid Support Mobile.");
            return false;
    }
    return true;
}

