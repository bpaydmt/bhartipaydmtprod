function flightSearchResponse(resultFlight, formData, leaving , returnLeaving){

		  
			// Loop through the each array
		   resultFlight.Response.OriginCity = resultFlight.Response.Results["0"]["0"].Segments["0"]["0"].Origin.Airport.CityName
		   resultFlight.Response.DestiCity = resultFlight.Response.Results["0"]["0"].Segments["0"][resultFlight.Response.Results["0"]["0"].Segments["0"].length -1].Destination.Airport.CityName
 		$("#TraceId").val(resultFlight.Response.TraceId)
 		
 			flightResult(resultFlight.Response.Results[0])
 		
 		if(formData.JourneyType == 2){
 			 resultFlight.Response.OriginCity = resultFlight.Response.Results["0"]["1"].Segments["0"]["0"].Origin.Airport.CityName
     		   resultFlight.Response.DestiCity = resultFlight.Response.Results["0"]["1"].Segments["0"][resultFlight.Response.Results["0"]["0"].Segments["0"].length -1].Destination.Airport.CityName
 			$("#singleDetails, #returnDetails").show();
 			flightResult(resultFlight.Response.Results[1])
 			$("#fliight-wrapper").addClass("returnFlightTrue")
 			$(".f-header h2 .travel-info, .f-header h4").show();
 		}else{
 			$(".f-header h2 .travel-info, .f-header h4").show();
 			
 		}
 
		   resultFlight.Response.leavingtDate = customDateFormat(new Date(leaving));
		   resultFlight.Response.leavingtDateReturn = customDateFormat(new Date(returnLeaving));
 	    console.log(resultFlight)
 	 genrateTemplate('flight-template',resultFlight)
 	
 	    FLIGHTSEARCH = resultFlight;
 	 $('.available-seats').each(function(index,item){
 	
 			if(parseInt($(this).find('span').text()) < 6){
 			
 				$(this).css('color','red')
 			}
 		});
 	 
}

function bookFlightAction(curr,amount,elm){
	
	console.log(elm)
	if(elm == undefined){
	
		 $(".flight-loading-div").show()
	}
	$("#booking-btn").text('Loading..').prop('disbaled',true);
	
 	var obj = {};
	obj.AdultCount = $("#AdultCount").val();
	obj.ChildCount = $("#ChildCount").val();
	obj.InfantCount = $("#InfantCount").val();
	obj.TraceId = $("#TraceId").val();
	obj.JourneyType = parseInt($("#JourneyType").val());
	
	if(obj.JourneyType ==  2){
		obj.IsLCC = $("#IsLCC").val() + "," + $("#IsLCC").val();
		obj.AirlineCode =$("#AirlineCode").val()  + "," + $("#AirlineCodeR").val();
		obj.ResultIndex = $("#ResultIndex").val()  + "," + $("#ResultIndexR").val();
	}else{
		
		$(".flight-loading-div").show()
		obj.IsLCC = $("#IsLCC").val();
		obj.AirlineCode =$("#AirlineCode").val();
		obj.ResultIndex = $("#ResultIndex").val();
	}
	
	   $.ajax({
           type: "POST",
           url: 'fareQuote',
           data: 'fareQuote='+JSON.stringify(obj), // serializes the form's elements.
           success: function(data)
           {
        	   $(".flight-loading-div").hide()
        	   var dateInf = new Date();
        	   $('.flight-btn').attr('disabled',false).css('opacity',"1")
        	   $(elm).text(curr +" " + amount);
        	  console.log(data)
        	 if(data.statusCode == "300" && data.fareQuote.Response.Error.ErrorCode == 0){
        		 
        	$(".bookingFooter").hide();
        
        	$.each(data.fareQuote.Response.Results.Segments,function(index,item){
        		
        		$.each(item,function(inx,it){
        			//getTravelDuration(d1, d2)
        			data.fareQuote.Response.Results.Segments[index][inx].Origin.DepTime = new Date(data.fareQuote.Response.Results.Segments[index][inx].Origin.DepTime).toUTCString().slice(0, 22)
        			data.fareQuote.Response.Results.Segments[index][inx].Destination.ArrTime = new Date(data.fareQuote.Response.Results.Segments[index][inx].Destination.ArrTime).toUTCString().slice(0, 22)
        		})

        	})
        	
        	
        	
        	
        			
        		 if(data.fareQuote.Response.Results.Segments.length > 1){
        			 $.each(data.fareQuote.Response.Results.Segments["1"],function(index, item){
        			 data.fareQuote.Response.Results.Segments["1"][index].OringinReturn =  data.fareQuote.Response.Results.Segments["1"]["0"].Origin.Airport.CityName
        			 
        			 data.fareQuote.Response.Results.Segments["1"][index].DestinationReturn = data.fareQuote.Response.Results.Segments["1"][data.fareQuote.Response.Results.Segments["1"].length -1].Destination.Airport.CityName
        			  })
        		        
            		 
        		 
        		 }
      console.log('----')
      console.log(data)
        		 data.fareQuote.Response.surchargesTaxes =  addZeroes(data.fareQuote.Response.Results.Fare.ChargedFare - (data.fareQuote.Response.Results.Fare.BaseFare - data.fareQuote.Response.Results.Fare.ConvenienceFee)) ;
      data.fareQuote.Response.Results.Fare.BaseFare = addZeroes( data.fareQuote.Response.Results.Fare.BaseFare)
      data.fareQuote.Response.Results.Fare.ConvenienceFee = addZeroes( data.fareQuote.Response.Results.Fare.ConvenienceFee)
       data.fareQuote.Response.Results.Fare.ChargedFare = addZeroes( data.fareQuote.Response.Results.Fare.ChargedFare)
         
         
        		 data.fareQuote.Response.AdultCount = $("#AdultCount").val()
        	  data.fareQuote.Response.ChildCount = $("#ChildCount").val()
        	  data.fareQuote.Response.InfantCount = $("#InfantCount").val()
        	 
        	  	 genrateTemplate('booking-details',data)
        	  	  $("#origin0").text(data.fareQuote.Response.Results.Segments["0"]["0"].Origin.Airport.CityName)
        	  	 $("#destina0").text(data.fareQuote.Response.Results.Segments["0"][data.fareQuote.Response.Results.Segments["0"].length -1].Destination.Airport.CityName)
        	  	   if(data.fareQuote.Response.Results.Segments.length > 1){
        	  		  $("#origin1").text(data.fareQuote.Response.Results.Segments["0"][data.fareQuote.Response.Results.Segments["0"].length -1].Destination.Airport.CityName)
             	  	 $("#destina1").text(data.fareQuote.Response.Results.Segments["0"]["0"].Origin.Airport.CityName)
        	  		   
        	  	   }
        	  	  $('<h3>Adult(s)</h3>').appendTo("#ticketingForm");
        	  	for(var i = 0; i < parseInt(data.fareQuote.Response.AdultCount); i++){
        	  		 var source   = document.getElementById('adult-field').innerHTML;
 					var template = Handlebars.compile(source);
 					var context = data;
 					var html    = template(context);
 					
 					$(html).appendTo("#ticketingForm");
 					
 					
        	  	}

       		
        		 if(parseInt(data.fareQuote.Response.ChildCount) > 0){
        			 $('<h3>Child(ren)</h3>').appendTo("#ticketingForm");
        			 for(var j = 0; j < parseInt(data.fareQuote.Response.ChildCount); j++){
               	  		 var source   = document.getElementById('children-field').innerHTML;
        					var template = Handlebars.compile(source);
        					var context = data;
        					var html    = template(context);
        					$(html).appendTo("#ticketingForm");
        					$(".child-inp.dob").datetimepicker({
        						 
        						
        						 i18n:{
        							  de:{
        							   months:[
        							    'Januar','Februar','März','April',
        							    'Mai','Juni','Juli','August',
        							    'September','Oktober','November','Dezember',
        							   ],
        							   dayOfWeek:[
        							    "So.", "Mo", "Di", "Mi", 
        							    "Do", "Fr", "Sa.",
        							   ]
        							  }
        							 },
        							 timepicker:false,
        							 format:'Y/m/d',
        							 theme:'dark',
             						
             						/*maxDate:new Date(dateInf.setFullYear(dateInf.getFullYear() - 2)),
             						minDate: new Date(dateInf.setFullYear(dateInf.getFullYear() - 10)),*/
        						     						
        						
        					});
        					
        				
               	  	}
        		 }else{
        			 $("#child-pax").hide();
        		 }
        		 
        		 if(parseInt(data.fareQuote.Response.InfantCount) > 0){
        			 $('<h3>Infant(s)</h3>').appendTo("#ticketingForm");
        			 
        		 	for(var k = 0; k < parseInt(data.fareQuote.Response.InfantCount); k++){
           	  		 var source   = document.getElementById('infant-field').innerHTML;
    					var template = Handlebars.compile(source);
    					var context = data;
    					var html    = template(context);
    					$(html).appendTo("#ticketingForm");
    					
    					$(".infant-inp.dob").datetimepicker({
    						i18n:{
  							  de:{
  							   months:[
  							    'Januar','Februar','März','April',
  							    'Mai','Juni','Juli','August',
  							    'September','Oktober','November','Dezember',
  							   ],
  							   dayOfWeek:[
  							    "So.", "Mo", "Di", "Mi", 
  							    "Do", "Fr", "Sa.",
  							   ]
  							  }
  							 },
  							 timepicker:false,
  							 format:'Y/m/d',
  							 theme:'dark',
       						/*
       						maxDate:new Date(),
       						minDate: new Date(dateInf.setFullYear(dateInf.getFullYear() - 2)),
    						     						*/
    						
    					});
           	  	}
        		 }else{
        			 $("#infant-pax").hide();
        		 }
        		 
        		$(".gender-select").attr('name','pass.gender')
        	
        		$(".first-Name").attr('name','pass.firstName')
        	
        		$(".last-Name").attr('name','pass.lastName')
        
        		$(".dob").attr('name','pass.dateOfBirth')
      
          
			$("#bookingFareBreakDown").val(JSON.stringify(data.fareQuote.Response.ResultList["0"].FareBreakdown));
			$("#bookingFare").val(JSON.stringify(data.fareQuote.Response.ResultList["0"].Fare));
			
			if(data.fareQuote.Response.ResultList.length > 1){
				$("#bookingFareBreakDownReturn").val(JSON.stringify(data.fareQuote.Response.ResultList["1"].FareBreakdown));
				$("#bookingFareReturn").val(JSON.stringify(data.fareQuote.Response.ResultList["1"].Fare));
			}
           	
        	$("#BookingResultIndex").val(data.fareQuote.Response.Results.ResultIndex);
        	$("#bookingTravelId").val(data.travelId);
        	$("#BookingTraceId").val(data.fareQuote.Response.TraceId)
        	$("#bookingadultCount").val( $("#AdultCount").val())
        	$("#bookingchildCount").val( $("#ChildCount").val())
        	$("#bookingInfantCount").val( $("#InfantCount").val())	
        
      		
        	 }else{
        		if (data.statusCode == "300" && data.fareQuote.Response.Error.ErrorCode != 0){
        			
        			
        			$("#errModel").modal('show');
        			$("#errMsgFli").text(data.fareQuote.Response.Error.ErrorMessage)
	   			}else{
	   				$("#errModel").modal('show');
        			$("#errMsgFli").text(data.statusMsg)
	   			
	   			}
        		 $("#booking-btn").text('BOOK').prop('disbaled',false);
        	 }
		        	 
		     
           }
           
	   });
	   
	   
	console.log(obj)	
		
		
}
function getTravelDuration(d1, d2){
    
	  var date1 = new Date(d1)
	  var  date2= new Date(d2)

	  var time = (date2.getTime() - date1.getTime())/(1000*60);
	  
	  if(time >= 60)
	  {
		  
	  return ((Math.trunc(time / 60)<10?'0':'') + Math.trunc(time / 60)) +" Hr "+ ((Math.trunc(time % 60)<10?'0':'') + Math.trunc(time % 60)) +" Min";
	  }
	  else
	  {
	   return ((Math.trunc(time)<10?'0':'') + Math.trunc(time)) +"Min";
	  }
}

function flightResult(flightResponse){
	   $.each(flightResponse,function(index,item){
		  
		   var result = flightResponse[index]
		  
	
		   // Adult Price Fare
			var price =  parseFloat(result.FareBreakdown[0].BaseFare) + parseFloat(result.FareBreakdown[0].Tax) + parseFloat(result.FareBreakdown[0].PGCharge) + ( parseFloat(result.Fare.OtherCharges) / (parseFloat(result.FareBreakdown[0].PassengerCount) + (result.FareBreakdown.length > 1  && result.FareBreakdown[1].PassengerType == 2 ? parseFloat(result.FareBreakdown[1].PassengerCount) : 0) ))
		 
		  
		   //  departure Time
		 var d =  new Date(result.Segments["0"]["0"].Origin.DepTime) 
		
			// Arrival Time
		 var a =  new Date( result.Segments["0"][result.Segments[0].length - 1 ].Destination.ArrTime) // set Arrival Time
		 
		  
		  //Cal Traval Duratioin
			result.travalDuration =  getTravelDuration(d, a)
			
			//Set the Departure Time
		   result.departureTime =  ((d.getHours()<10?'0':'') + d.getHours()) + ":" + ((d.getMinutes()<10?'0':'') + d.getMinutes())
		   //Pass the Flight price
		   result.flightPrice = addZeroes(price);
			 //Set the Arrival Time
		   result.arrivalTime = ((a.getHours()<10?'0':'') + a.getHours()) + ":" + ((a.getMinutes()<10?'0':'') + a.getMinutes())
		  //isRefundable 
		   result.refundalbe  = result.IsRefundable == true ? "Refundable" : "Non-Refundable" ;
    	 
		    
			   
		   if(result.Segments["0"].length > 1 ){
			   result.stops = result.Segments["0"].length - 1 + " Stops" ;
			   var via = "Via ";
			   $.each(result.Segments["0"],function(index,item){
				
				   if(index > 0 ){
					 
					  if(index == 1){
						  result.via =  via  + result.Segments["0"][index].Origin.Airport.AirportName;
					  
				   }else{
					   result.via =  via + ", " + result.Segments["0"][index].Origin.Airport.AirportName; 
				   }
				  }
			   })
			  
		   }else{
			   result.stops = "Non-Stop"
		   }
		   
	   })
	   console.log("--------")
	     console.log(flightResponse)
	       console.log("--------")
	   return flightResponse;
}


function selectFlight(div,elm,amount,curr,AirlineCode,isLCC,ResultIndex){
	
	var JourneyType = $("#JourneyType").val();
	//alert(elm)
	
	
	if(JourneyType == 2){
	$(".bookingFooter").show();
	var clone = $(elm).parent().parent().clone();

	$("."+div +" .flight-btn i" ).hide();
	$(elm).find('.fa').show()
 
		
	
	if(div == 'single-flight'){
		
			$("#booksingle-flight").html(clone)
			$("#singleFlightPrice").val(amount)
		
		
		
		
				
		 $("#IsLCC").val(isLCC);
		$("#AirlineCode").val(AirlineCode);
		 $("#ResultIndex").val(ResultIndex);
	}else{
		//$(".flight-loading-div").show()
		$("#IsLCCR").val(isLCC);
		$("#AirlineCodeR").val(AirlineCode);
		$("#ResultIndexR").val(ResultIndex);
		$("#returnFlightPrice").val(amount)
		$("#bookreturn-flight").html(clone)
		
		
			
		
		
	}
	var sp = $("#singleFlightPrice").val();
	var rp =  $("#returnFlightPrice").val();
	
	if(rp.length != 0 && sp.length != 0 ){
	

	$(".final-booking").show();
		$("#total-amount #cost").text(addZeroes(parseInt(sp) + parseInt(rp)))
	}else{
		$(".final-booking").hide();
	}
	
	$(".bookingFooter .flight-btn").removeAttr('onclick')
	
	
	$("#curr").text(curr)
	}else{
		 $('.flight-btn').attr('disabled',true).css('opacity',".3");
			$(elm).text("Loading...");
		 $("#IsLCC").val(isLCC);
			$("#AirlineCode").val(AirlineCode);
			 $("#ResultIndex").val(ResultIndex);
			if( div == 'single-flight'){
				
				
			}
			 
			 bookFlightAction(curr,amount,elm)
			 	
			
	}
	
	 //$(elm).text(curr+" "+amount).attr('disabled',false)
}
function genrateTemplate(id, data){
var source   = document.getElementById(id).innerHTML;
var template = Handlebars.compile(source);
var context = data;
var html    = template(context);
$("#fliight-wrapper").html(html)

}

$(function(){
	$(".booking-type").click(function(){
		$(".booking-type").removeClass('fbtn-active')
		$(this).addClass('fbtn-active')
		$(this).find('input[type="radio"]').prop('checked',true)
		if($("#roundTripRadio").prop('checked')){
			$("#returnFlight").show()
			
			$("#JourneyType").val(2)
		}else{
			$("#returnFlight").hide()
			$("#JourneyType").val(1)
		}
	})
	
/*	$("#directFlight").change(function(){
		if($(this).find('input').prop('checked')){
			$(this).addClass("fbtn-active");
		}else{
			$(this).removeClass("fbtn-active");
		}
	});*/
	$.get('InitialSearch',function(data){
		console.log(data)
		$.each(data.resPayload.airportList,function(index,item){
			
			$('<option value="'+item.airportCode+'">' + item.cityName + ' ( '+item.airportName + ' - ' +    item.airportCode +' )'+ '</option>').appendTo(".airport-select")
			/* $('.airport-select').selectpicker({
			      liveSearch: true,
			      maxOptions: 1
			    }); */
		
		})
		 $('select').trigger("chosen:updated");
		 //  $('select').chosen({search_contains: true})
/*	var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-no-single' : { disable_search_threshold: 10 },
  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}*/
		
	})
	

	
	
/*	$('.passanger').on("change",function(e){
		 console.log($(this).val().length)
		if($(this).val().length > 0){
			alert(1)
			e.preventDefault();
		} 
		var a = parseInt($("#adult").val());
		var c = parseInt($("#children").val());
		var i = parseInt($("#infant").val());
		$("#children").attr('max', 9 - a )
	
			if((a + c) > 9){
				
				$("#children").val(c - 1)
			}
			$("#infant").attr('max', a)
		
		$("#children").prop('disabled',false )
		
		console.log(c)
		console.log(i)
	})
	*/
//Flight Search	
	
$("#flightForm").submit(function(e) {
	var forigin = $("#flight-origin").val();
	var fdestination= $("#flight-destination").val();
	var jType = $(".radioBtn:checked").val()
	
	if(jType == "2"){
		$('#loading-f-animation').addClass('flight-animation-return')
		$('#loading-f-animation').removeClass('flight-animation')
	}else{
		$('#loading-f-animation').addClass('flight-animation')
		$('#loading-f-animation').removeClass('flight-animation-return')
		
	}
	$("#load-origin").text(forigin)
	$("#load-destination").text(fdestination)
if(forigin == "-1" || fdestination == "-1" || forigin == fdestination){
	
	if(forigin == fdestination){
		$("#errSpanFlight").text("Please enter different departure and destination city.")
	}
	if(forigin == "-1" || fdestination == "-1"){
		$("#errSpanFlight").text("Please fill the departure and destination city fields both are mandatory fields.")
	}
	
}else{
	/*var directFlight = $("#directFlight").prop("checekd")*/
	$(".flight-loading-div").show()
	var formData = {  
   "EndUserIp":"",
   "TokenId":"",
   "AdultCount":"1",
   "ChildCount":"",
   "InfantCount":"",
   "DirectFlight": $("#directFlight input").prop('checked') == true ? "true" : "false",
   "OneStopFlight":"false",
   "JourneyType":"1",
   "Segments":[  
      {  
         "Origin":"BLR",
         "Destination":"DEL",
         "FlightCabinClass":"2",
         "PreferredDepartureTime":"2017-10-09T00:00:00",
         "PreferredArrivalTime":"2017-10-09T00:00:00"
      }
   ]
}
	

var depart =  $("#departureDate").val();

$("#adult").val($("#adult-t").text());
$("#children").val($("#children-t").text());
 $("#infant").val($("#infant-t").text());
	var adultC = $("#adult").val();
	var childC = $("#children").val()
	var infantC = $("#infant").val();
	formData.AdultCount = adultC;
	formData.ChildCount = childC;
	formData.InfantCount = infantC;
	formData.JourneyType = jType;
	formData.Segments[0].FlightCabinClass = $("#travelClass").val();	
	formData.Segments[0].Origin = forigin;
	formData.Segments[0].PreferredDepartureTime =depart.replace(/\s+/g,"T").replace(/\//g,"-")+":00";
	formData.Segments[0].PreferredArrivalTime = depart.replace(/\s+/g,"T").replace(/\//g,"-")+":00";
	formData.Segments[0].Destination = fdestination;

	if(formData.JourneyType == 2){
		var arrival = $("#departureDate").val();
		formData.Segments[1]= {};  
		formData.Segments[1].FlightCabinClass = $("#travelClass").val();     
		formData.Segments[1].Origin = fdestination;
		formData.Segments[1].PreferredDepartureTime = arrival.replace(/\s+/g,"T").replace(/\//g,"-")+":00";
		formData.Segments[1].PreferredArrivalTime =arrival.replace(/\s+/g,"T").replace(/\//g,"-")+":00";
		formData.Segments[1].Destination =  forigin;
		
	}
	/* console.log($("#departureDate").val());
	console.log($("#returnDate").val()); */
	
	$("#AdultCount").val($("#adult").val())
	if(childC.length != 0){
		$("#ChildCount").val($("#children").val())
	}
	if(infantC.length != 0){
		$("#InfantCount").val($("#infant").val())
	}
		
		
	var leaving = $("#departureDate").val();
	
	var returnLeaving = $("#returnDate").val();
	console.log(formData)
	
	    $.ajax({
	           type: 'POST',
	           url: 'flightSearch',
	           data: 'searchFlight='+JSON.stringify(formData), // serializes the form's elements.
	           success: function(data)
	           
	           {
	        		$(".flight-loading-div").hide()
	        		   console.log(data);
	        		 
	        		   resultFlight = data.flightSearch;
	        		   if(data.statusCode == "300" && resultFlight.Response.Error.ErrorCode == 0){
	        	   flightSearchResponse(resultFlight, formData, leaving , returnLeaving);
		        	  
	        	   }else{
	        		   $(".flight-loading-div").hide()
	        		  if( data.statusCode == "300" &&  resultFlight.Response.Error.ErrorCode != 0){
	        			  
	        			   $("#errSpanFlight").text(resultFlight.Response.Error.ErrorMessage);
	        		  }else{
	        			  $("#errSpanFlight").text(data.statusMsg);
	        		  }
	        		  
	        		 
	        		
		              
	        	   }
	        	 
	        	   console.log(data);
	        	   $('[data-toggle="tooltip"]').tooltip(); 
	               
	           }
	  
	         });

	  
	
}	
e.preventDefault();

});	
})


function ticketBooking(){
	
	
	$('.f-required').each(function(index,val){
		let cVal= $(this).val()
		if(cVal.length < 1 || cVal == '-1' ){
			alert("Please fill all fields. All fields are mandatory. ");
			return false;
		}
	})
	$(".flight-loading-div").show()
	$("#confirmBtn").val("Loading...")
	var ticketing = {  
		   "TravelId":"TPID001159",
		  "TraceId":"824f3e1f-8ff3-4d6a-bb10-05e60bec5cad",
		   "ResultIndex":"OB8",
		   "Passengers":[  
		      {  
		         "Title":"Mr",
		         "FirstName":"AJEET",
		         "LastName":"MAURYA",
		         "PaxType":1,
		         "Gender":1,
		         "IsLeadPax":true,
		         "Fare":{  
		            "TransactionFee":0.0,
		            "AirTransFee":0.0,
		            "AdditionalTxnFee":0.0,
		            "BaseFare":5200.0,
		            "Tax":1219.0,
		            "YQTax":0.0,
		            "AdditionalTxnFeeOfrd":0.0,
		            "AdditionalTxnFeePub":0.0
		         }
		      }
		   ]
	
	
	}
	
	$('.gender-select').each(function(index){
		
		
	})
	flightMakeItInvalid('.last-Name')
	flightMakeItInvalid('.first-Name')
	flightMakeItInvalid('.child-inp.dob')
	flightMakeItInvalid('.infant-inp.dob')
	flightMakeItInvalid('.gender-select')
	
	console.log($(".first-Name").val())
	

	
	if($('.invalid').length != 0){
		$('#flight-err').text('Red fields are mandatory')
	}else{
		$('#flight-err').text('')
		var findex = 0;
	
		$(".adult-inp.first-Name").each(function(index){
			findex++
			ticketing.Passengers.push({  
				         "Title":"Mr",
				         "FirstName":"AJEET",
				         "LastName":"MAURYA",
				         "PaxType":1,
				         "Gender":1,
				         "IsLeadPax":true,
				         "DateOfBirth":"",
				         "Fare":{  
				            "TransactionFee":0.0,
				            "AirTransFee":0.0,
				            "AdditionalTxnFee":0.0,
				            "BaseFare":5200.0,
				            "Tax":1219.0,
				            "YQTax":0.0,
				            "AdditionalTxnFeeOfrd":0.0,
				            "AdditionalTxnFeePub":0.0
				         }
			}
			
			
			);
			
			 if(index == 0){
				 ticketing.Passengers[findex].IsLeadPax  = true
			 }else{
				 ticketing.Passengers[findex].IsLeadPax  = false;
			 } 
			 ticketing.Passengers[findex].FirstName 
			ticketing.Passengers[findex].FirstName = $(this).val();
			ticketing.Passengers[findex].LastName = $(this).parent().next().find('.last-Name').val();
			ticketing.Passengers[findex].Gender = $(this).parent().prev().find('.gender-select').val();
			ticketing.Passengers[findex].Title = $(this).parent().prev().find('.gender-select').val() == "1" ? "Mr" : "Ms" ;
			
			});
		ticketing.Passengers.pop()
		if($(".child-inp").length != 0){
$(".child-inp.first-Name").each(function(index){
			
			ticketing.Passengers.push({  
				         "Title":"Mr",
				         "FirstName":"AJEET",
				         "LastName":"MAURYA",
				         "PaxType":2,
				         "Gender":1,
				         "IsLeadPax":false,
				         "DateOfBirth":"",
				         "Fare":{  
				            "TransactionFee":0.0,
				            "AirTransFee":0.0,
				            "AdditionalTxnFee":0.0,
				            "BaseFare":5200.0,
				            "Tax":1219.0,
				            "YQTax":0.0,
				            "AdditionalTxnFeeOfrd":0.0,
				            "AdditionalTxnFeePub":0.0
				         }
			}
			
			
			);
			
			ticketing.Passengers[findex].FirstName = $(this).val();
			ticketing.Passengers[findex].LastName = $(this).parent().next().find('.last-Name').val();
			ticketing.Passengers[findex].Gender = $(this).parent().prev().find('.gender-select').val();
			ticketing.Passengers[findex].Title = $(this).parent().prev().find('.gender-select').val() == "1" ? "Mstr" : "Ms" ;
			ticketing.Passengers[findex].DateOfBirth = $(this).parent().next().next().find('.dob').val();
			});
		}
		if($(".child-inp").length != 0){
$(".infant-inp.first-Name").each(function(index){
	
	ticketing.Passengers.push({  
		         "Title":"Mr",
		         "FirstName":"AJEET",
		         "LastName":"MAURYA",
		         "PaxType":3,
		         "Gender":1,
		         "IsLeadPax":false,
		         "DateOfBirth":"",
		         "Fare":{  
		            "TransactionFee":0.0,
		            "AirTransFee":0.0,
		            "AdditionalTxnFee":0.0,
		            "BaseFare":5200.0,
		            "Tax":1219.0,
		            "YQTax":0.0,
		            "AdditionalTxnFeeOfrd":0.0,
		            "AdditionalTxnFeePub":0.0
		         }
	}
	
	
	);
	
	ticketing.Passengers[findex].FirstName = $(this).val();
	ticketing.Passengers[findex].LastName = $(this).parent().next().find('.last-Name').val();
	ticketing.Passengers[findex].Gender = $(this).parent().prev().find('.gender-select').val();
	ticketing.Passengers[findex].Title = $(this).parent().prev().find('.gender-select').val() == "1" ? "Mstr" : "Ms" ;
	ticketing.Passengers[findex].DateOfBirth = $(this).parent().next().next().find('.dob').val();
	})
ticketing.Passengers.pop()


		}
		$("#ticketingForm").submit()
		console.log(ticketing)
		
	}
	
}


function flightMakeItInvalid(className){
	
	$(className).each(function(index){
		console.log(index)
		console.log($(this).val().length )
		console.log($(this).val())
		if($(this).val().length < 1 || $(this).val() == "-1"){
			$(this).addClass('invalid');
		}else{
			$(this).removeClass('invalid');
		}
		//ticketing.Passengers.push({})
		
	})
}

function travallerCount(select,action){
	var adult = parseInt($("#adult-t").text());
	var child = parseInt($("#children-t").text());
	var infant = parseInt($("#infant-t").text());
	var count=  parseInt($(select).text());
	
	if(count < 9 &&  action == "Plus" ){
		console.log(1)
		if(select == "#children-t" && (adult + child + infant) < 9){
			console.log(adult + child)
			count++
			$(select).text(count)
		}else if(select == "#adult-t" ){
			count++
			$(select).text(count)
		}else if( select == "#infant-t" &&   infant < adult && (adult + child + infant) < 9){
			count++
			$(select).text(count)
		}
		
		
		if((adult + child + infant) >= 9  && select == "#adult-t"){
			if(infant == 0){
			child-- 
			$("#children-t").text(child-- )
			}else{
				infant-- 
				$("#infant-t").text(infant-- )
			}
		}
		
	}else if(count > 1 &&  action == "Minus" && select == "#adult-t" ){
	
		count--
		$(select).text(count)
		if(infant > count){
			
			$("#infant-t").text(count )
		}
		if(child > count){
			
			$("#children-t").text(count)
		}
	}else if(count > 0 &&  action == "Minus" && select != "#adult-t" ){
		count--
		$(select).text(count)
	}
	
	
	
	
}


/*function travallerCount(select,action){
	var adult = parseInt($("#adult-t").text());
	var child = parseInt($("#children-t").text());
	var infant = parseInt($("#infant-t").text());
	var count=  parseInt($(select).text());
	
	if(count < 9 &&  action == "Plus" ){
		console.log(1)
		if(select == "#children-t" && adult > count && (adult + child) < 9){
			console.log(adult + child)
			count++
			$(select).text(count)
		}else if(select == "#adult-t" ){
			count++
			$(select).text(count)
		}else if( select == "#infant-t" &&   infant < adult){
			count++
			$(select).text(count)
		}
		
		
		if((adult + child) >= 9  && select == "#adult-t"){
			
			child-- 
			$("#children-t").text(child-- )
		}
		
	}else if(count > 1 &&  action == "Minus" && select == "#adult-t" ){
	
		count--
		$(select).text(count)
		if(infant > count){
			
			$("#infant-t").text(count )
		}
		if(child > count){
			
			$("#children-t").text(count)
		}
	}else if(count > 0 &&  action == "Minus" && select != "#adult-t" ){
		count--
		$(select).text(count)
	}
	
	
	
	
}
*/
function sortDuration(flightType,elm){
	
	console.log(FLIGHTSEARCH)
	var arrNum = flightType == 'single' ? 0 : 1 
	
	var arr = FLIGHTSEARCH.Response.Results[arrNum];
	

	if($(elm).hasClass('sortedPrice')){
	 
		var sort = arr.sort(function(a,b){
			return parseInt(a.travalDuration.replace(/[^0-9]+/g, "")) - parseInt(b.travalDuration.replace(/[^0-9]+/g, "")) 
			
		})	
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	
			$(elm).addClass('reverseSortedPrice')
			$(elm).removeClass('sortedPrice')
			var text = $(elm).text()
				$(elm).html('<i class="fa fa-arrow-down" aria-hidden="true"></i>' + text)
	}else{
	
		
		var sort = arr.reverse(function(a,b){
			return parseFloat(a.travalDuration.replace(/[^0-9]+/g, "")) - parseFloat(b.travalDuration.replace(/[^0-9]+/g, "") )
			
		})
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	$(elm).addClass('sortedPrice')
	var text = $(elm).text()
	$(elm).html('<i class="fa fa-arrow-up" aria-hidden="true"></i>' + text)

	}


 	
 	
 	 $('.available-seats').each(function(index,item){
 	
 			if(parseInt($(this).find('span').text()) < 6){
 			
 				$(this).css('color','red')
 			}
 		});
 	 
 	 
 	  $('[data-toggle="tooltip"]').tooltip(); 
      
 	//$(elm).attr('onclick',reverseSort(flightType,elm))
	
}
function compareStrings(a, b) {
	  // Assuming you want case-insensitive comparison
	  a = a.toLowerCase();
	  b = b.toLowerCase();

	  return (a < b) ? -1 : (a > b) ? 1 : 0;
	}


function sortByAirline(flightType,elm){
	
	console.log(FLIGHTSEARCH)
	var arrNum = flightType == 'single' ? 0 : 1 
	
	var arr = FLIGHTSEARCH.Response.Results[arrNum];
	

	if($(elm).hasClass('sortedPrice')){
	 
		var sort = arr.sort(function(a,b){
		console.log(a.Segments["0"]["0"].Airline.AirlineName)
		
			return compareStrings( a.Segments["0"]["0"].Airline.AirlineName, b.Segments["0"]["0"].Airline.AirlineName)
			
		})	
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	
			$(elm).addClass('reverseSortedPrice')
			$(elm).removeClass('sortedPrice')
			var text = $(elm).text()
				$(elm).html('<i class="fa fa-arrow-down" aria-hidden="true"></i>' + text)
	}else{
	
		
		var sort = arr.reverse(function(a,b){
			return compareStrings( a.Segments["0"]["0"].Airline.AirlineName, b.Segments["0"]["0"].Airline.AirlineName)
			
		})
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	$(elm).addClass('sortedPrice')
	var text = $(elm).text()
	$(elm).html('<i class="fa fa-arrow-up" aria-hidden="true"></i>' + text)

	}


 	
 
 	 $('.available-seats').each(function(index,item){
 	
 			if(parseInt($(this).find('span').text()) < 6){
 			
 				$(this).css('color','red')
 			}
 		});
 	 
 	  $('[data-toggle="tooltip"]').tooltip(); 
      
 	//$(elm).attr('onclick',reverseSort(flightType,elm))
	
}


function sortPrice(flightType,elm){
	
	console.log(FLIGHTSEARCH)
	var arrNum = flightType == 'single' ? 0 : 1 
	
	var arr = FLIGHTSEARCH.Response.Results[arrNum];
	
	console.log(arr)
	if($(elm).hasClass('sortedPrice')){
	 
		var sort = arr.sort(function(a,b){
			return parseFloat(a.flightPrice) - parseFloat(b.flightPrice) 
			
		})	
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	
			$(elm).addClass('reverseSortedPrice')
			$(elm).removeClass('sortedPrice')
			var text = $(elm).text()
				$(elm).html('<i class="fa fa-arrow-down" aria-hidden="true"></i>' + text)
	}else{
	
		
		var sort = arr.reverse(function(a,b){
			return parseFloat(a.flightPrice) - parseFloat(b.flightPrice) 
			
		})
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	$(elm).addClass('sortedPrice')
	var text = $(elm).text()
	$(elm).html('<i class="fa fa-arrow-up" aria-hidden="true"></i>' + text)

	}


 	

 	 $('.available-seats').each(function(index,item){
 	
 			if(parseInt($(this).find('span').text()) < 6){
 			
 				$(this).css('color','red')
 			}
 		});
 	 
 	  $('[data-toggle="tooltip"]').tooltip(); 
      
 	//$(elm).attr('onclick',reverseSort(flightType,elm))
	
}

function sortTime(flightType,elm,j){

	var arrNum = flightType == 'single' ? 0 : 1 
	
	var arr = FLIGHTSEARCH.Response.Results[arrNum];
	
	console.log(arr)
	if($(elm).hasClass('sortedPrice')){
		if ( j == 'arrival' ){
			var sort = arr.sort(function(a,b){				
				return parseInt(a.arrivalTime.replace(':','')) - parseInt(b.arrivalTime.replace(':','')) 
				
			})	
			
		}else{ 
		var sort = arr.sort(function(a,b){				
			return parseInt(a.departureTime.replace(':','')) - parseInt(b.departureTime.replace(':','')) 
			
		})	
		
		}
	FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	$(elm).addClass('reverseSortedPrice')
			$(elm).removeClass('sortedPrice')
			var text = $(elm).text()
				$(elm).html('<i class="fa fa-arrow-down" aria-hidden="true"></i>' + text)
	}else{
	
		if ( j == 'arrival' ){
			var sort = arr.reverse(function(a,b){				
				return parseInt(a.arrivalTime.replace(':','')) - parseInt(b.arrivalTime.replace(':','')) 
				
			})	
			
		}else{ 
		var sort = arr.reverse(function(a,b){				
			return parseInt(a.departureTime.replace(':','')) - parseInt(b.departureTime.replace(':','')) 
			
		})	
		
		}
			FLIGHTSEARCH.Response.Results[arrNum] = sort;
	genrateTemplate('flight-template',FLIGHTSEARCH)
	$(elm).addClass('sortedPrice')
	var text = $(elm).text()
	$(elm).html('<i class="fa fa-arrow-up" aria-hidden="true"></i>' + text)

	}


 
 	 $('.available-seats').each(function(index,item){
 	
 			if(parseInt($(this).find('span').text()) < 6){
 			
 				$(this).css('color','red')
 			}
 		});
 	//$(elm).attr('onclick',reverseSort(flightType,elm))
 	  $('[data-toggle="tooltip"]').tooltip(); 
      
}

function customDateFormat(date) {
    var d = date;
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";


var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

   return weekday[d.getDay()] + ", " + monthNames[d.getMonth()] + " " + (parseInt(d.getUTCDate()) + 1) + " "+ d.getFullYear();

}
function addZeroes( num ) {
    var value = Number(num);
   
  
        value = value.toFixed(2);
  
    return value
}

