function hasC(e,c) {
	return (e.className && e.className.indexOf(c)>-1);
};

function addC(e,c) {
	if (!hasC(e,c)){
		if (e.className) e.className += " " + c;
		else e.className = c;
	}
};

function remC(e,c) {
	if (hasC(e,c)) {
		var r = new RegExp("(^|\\s)" + c + "(\\s|$)");
		e.className = e.className.replace(r, "$2");
	}
};

function tm(el) 
{
    var p=el.parentNode;
    var d=document;
    function cl() {remC(p,'selected');d.onclick=null;}
    function sh() {d.onclick=cl;}
    if(hasC(p,'selected')) cl();
    else{
        addC(p,'selected');
        setTimeout(sh, 0);
        try {
            var u=(p.getElementsByTagName("UL")[1]), c=u.childNodes, w=u.clientWidth, e=u, x=0;  
        }
        catch (ig){}
    }
    return false;
}

module.exports = function(options) {
	  if (options && options.setOnOldIE) {
	    return function(req, res, next) {
	      res.setHeader('X-XSS-Protection', '1; mode=block');
	      next();
	    };
	  } else {
	    return function(req, res, next) {
	      var matches = /msie\s*(\d+)/i.exec(req.headers['user-agent']);
	      var value;
	     if (!matches || (parseFloat(matches[1]) >= 9)) {
	        value = '1; mode=block';
	      } else {
	        value = '0';
	      }
	      res.setHeader('X-XSS-Protection', value);
	      next();
	    };
	  }
	};