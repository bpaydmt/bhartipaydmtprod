// JavaScript Document

function unloadJS(index) {
  var head = document.getElementsByTagName('head').item(0);
  var js = document.getElementById(scriptName);
  js.parentNode.removeChild(js);
}
 
function unloadAllJS() {
  var jsArray = new Array();
  jsArray = document.getElementsByTagName('script');
  for (i = 0; i < jsArray.length; i++){
    if (jsArray[i].id){
      unloadJS(jsArray[i].id)
    }else{
      jsArray[i].parentNode.removeChild(jsArray[i]);
    }
  }       
}
jQuery.fn.ForceNumericOnly = function() {
	return this.each(function() {
		$(this).keydown(function(e) {
			var key = e.charCode || e.keyCode || 0 || e.KeyCode == 16;
			// allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
			// home, end, period, and numpad decimal
			return (
				key == 8 ||
				key == 9 ||	
				key == 13 ||
				key == 16 ||
				key == 46 ||
				key == 110 ||
				key == 190 ||
				(key >= 35 && key <= 40) ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
		});
	});
};


jQuery(document).ready(function (){
	$("#mobile_no").ForceNumericOnly(); 
	$("#amount").ForceNumericOnly(); 
	$("#dth_mobile_no").ForceNumericOnly(); 
	$("#dth_amount").ForceNumericOnly();
	$("#datacard_mobile").ForceNumericOnly();
	$("#datacard_amount").ForceNumericOnly();
	
	
	$("#open_plans").click(function(){
		$("#payplutus_popup").show();
	});
	$("#close_popup").click(function(){
		$("#payplutus_popup").hide();
	});
	
	$("#login_popup").click(function(){
		$("#payplutus_login").show();
	});
	$("#login_popup1").click(function(){
		$("#payplutus_login").show();
	});
	$("#login_popup2").click(function(){
		$("#payplutus_login").show();
	});
	$("#close_login_popup").click(function(){
		$("#payplutus_login").hide();
	});
	$("#register_user").click(function(){
		$("#registration_box").show(300);
		$("#login_box").hide(300);
		$("#forgot_box").hide(300);
	});
	$("#login_user").click(function(){
		$("#login_box").show(300);
		$("#registration_box").hide(300);
		$("#forgot_box").hide(300);
	});
	$("#forgot_link").click(function(){
		$("#forgot_box").show(300);
		$("#login_box").hide(300);
		$("#registration_box").hide(300);
	});
	$("#login_user_2").click(function(){
		$("#login_box").show(300);
		$("#registration_box").hide(300);
		$("#forgot_box").hide(300);
	});
	
	var currentLoc = window.location.href;
    str = currentLoc.split("#")[1];
	
	// RECHARGE OPTION 
    if (str == "mobileRecharge") { mobile_Recharge(); }
    else if (str == "dthRecharge") { dth_Recharge(); }
    else if (str == "electricityRecharge") { electricity_bill(); }
    else if (str == "datacardRecharge") { datacard_Recharge(); }
    else if (str == "gasRecharge") { gas_bill(); }
    else if (str == "insuranceRecharge") { insurance_selector(); }
    else if (str == "landlinebill") { landline_Recharge(); }
	else if (str == "talktime") { talktime1(); }
	else if (str == "sms") { sms1(); }
	else if (str == "twog") { twog1(); }
	else if (str == "threeg") { threeg1(); }
	else if (str == "local") { local1(); }
	else if (str == "std") { std1(); }
	else if (str == "isd") { isd1(); }
	else if (str == "other") { other1(); }
	$("#mobile_recharge").click(function(){ mobile_Recharge(); });
	$("#dth_recharge").click(function(){ dth_Recharge(); });
	$("#datacard_recharge").click(function(){ datacard_Recharge(); });
	$("#landline_Recharge").click(function(){ landline_Recharge(); });
	$("#electricity_recharge").click(function(){ electricity_bill(); })
	$("#gas_recharge").click(function(){gas_bill(); })
	$("#insurance_recharge").click(function(){insurance_selector(); })
	
	function insurance_selector() {
		getBillerDetails('Insurance',"#insurance")
		$("#insurance_form").show();
		$("#mobile_recharge_form, #gasRecharge_form, #d2h_recharge_form, #datacard_recharge_form, #electricityRecharge_form, #landline_recharge_form  ").hide();
		$( "#ir").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh, #dc, #el, #gr, #mb, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
	}
	
	function mobile_Recharge() {
		$("#mobile_recharge_form").show();
		$("#insurance_form, #gasRecharge_form, #d2h_recharge_form, #datacard_recharge_form, #electricityRecharge_form, #landline_recharge_form  ").hide();
		$( "#mb").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh, #dc, #el, #gr, #ir, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
	}
	
	function gas_bill() {
		getBillerDetails('Gas',"#gas-opretor")
		$("#gasRecharge_form").show();
		$("#insurance_form, #mobile_recharge_form, #d2h_recharge_form, #datacard_recharge_form, #electricityRecharge_form, #landline_recharge_form ").hide();
		$( "#gr").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh, #dc, #el, #mb, #ir, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
	}
	function electricity_bill() {
		getBillerDetails('Electricity',"#eleOpretor")
		$("#electricityRecharge_form").show();
		$("#insurance_form, #gasRecharge_form, #d2h_recharge_form, #datacard_recharge_form, #mobile_recharge_form, #landline_recharge_form").hide();
		$( "#el").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh, #dc, #mb, #gr, #ir, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");		
		
	}
	function dth_Recharge() {
		$("#d2h_recharge_form").show();
		$("#insurance_form, #gasRecharge_form, #electricityRecharge_form, #datacard_recharge_form, #mobile_recharge_form, #landline_recharge_form").hide();
		$( "#dh").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#el, #dc, #mb, #gr, #ir, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
	
	}
	function datacard_Recharge() {
		$("#datacard_recharge_form").show();
		$("#insurance_form, #gasRecharge_form, #d2h_recharge_form, #electricityRecharge_form, #mobile_recharge_form, #landline_recharge_form").hide();
		$( "#dc").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh, #el, #mb, #gr, #ir, #lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");		
	}
	function landline_Recharge() {
		getBillerDetails('Landline',"#landline_operator")
		$("#landline_recharge_form").show();
		$("#insurance_form, #gasRecharge_form, #d2h_recharge_form, #electricityRecharge_form, #mobile_recharge_form, #datacard_recharge_form").hide();
		$( "#lb").removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#el, #dc, #mb, #gr, #ir, #dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
	
	
	}
	
	
	// RECHARGE PLAN POPUP BOX OPTIONS
	
	$("#m_talktime").click(function(){ talktime1(); });
	$("#m_sms").click(function(){ sms1(); });
	$("#m_twog").click(function(){ twog1(); });
	$("#m_threeg").click(function(){ threeg1(); });
	$("#m_local").click(function(){ local1(); });
	$("#m_std").click(function(){ std1(); });
	$("#m_isd").click(function(){ isd1(); });
	$("#m_other").click(function(){ other1(); });
	// RECHARGE PLAN POPUP BOX OPTIONS FUNCTION
	
	function talktime1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$("#landline_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").show();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_talktime" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	function sms1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").show();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_sms" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function twog1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").show();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_twog" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function threeg1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").show();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_threeg" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function local1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").show();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_local" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function std1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").show();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_std" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function isd1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").show();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_isd" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function other1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").show();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_other" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
	}
	
});



// FUNCTION FOR CHECK OPERATOR

//function check_operator(){
//	var mobile_no = $("#mobile_no").val();
//	var mobile = $("#mobile_no").val().length;
//	if(mobile == 5){
//		$.ajax({
//			url: "digit.jsp",
//			type: "GET",
//			dataType: 'json',
//			data: {mobile_no: mobile_no},
//			success: function(response){
//				$("#operator")
//					.prepend($("<option selected></option>")
//         			.attr("value",response.val1)
//         			.text(response.val1));
//				$("#circle").prepend($("<option selected></option>")
//         			.attr("value",response.val2)
//         			.text(response.val2));
//				
//				$("#payplutus_popup").show(500);				
//				$("#pop_operator").attr("value",response.val1);
//				$("#pop_circle").attr("value",response.val2);
//				$("#oeprator_bold").html(response.val1);
//			    $("#circle_bold").html(response.val2);
//				show_popup_data();
//			}			
//		});
//	}
//}

function check_operator(){
	 var mobile_no = $("#mobile_no").val();
	 var mobile = $("#mobile_no").val().length;
	 var recharge_type = $('input[type="radio"]:checked').val(); 
	 if(mobile == 5){

	  if(recharge_type === "Prepaid-Mobile"){
	 
		  $.ajax({
				url: "digit.jsp",
				type: "GET",
				dataType: 'json',
				data: {mobile_no: mobile_no},
				success: function(response){
					$("#operator")
						.prepend($("<option selected></option>")
	         			.attr("value",response.val1)
	         			.text(response.val1));
					$("#circle").prepend($("<option selected></option>")
	         			.attr("value",response.val2)
	         			.text(response.val2));
					
					$("#payplutus_popup").show(500);				
					$("#pop_operator").attr("value",response.val1);
					$("#pop_circle").attr("value",response.val2);
					$("#oeprator_bold").html(response.val1);
				    $("#circle_bold").html(response.val2);
					show_popup_data();
				}			
			});
	  }
	  else{
		  $.ajax({
				url: "digit.jsp",
				type: "GET",
				dataType: 'json',
				data: {mobile_no: mobile_no},
				success: function(response){
					$("#operator")
						.prepend($("<option selected></option>")
	         			.attr("value",response.val1)
	         			.text(response.val1));
					$("#circle").prepend($("<option selected></option>")
	         			.attr("value",response.val2)
	         			.text(response.val2));
					
									
					$("#pop_operator").attr("value",response.val1);
					$("#pop_circle").attr("value",response.val2);
					$("#oeprator_bold").html(response.val1);
				    $("#circle_bold").html(response.val2);
				    $("#open_plans").show();
				    
				    $("#open_blank").hide();
					show_popup_data();
				}			
			});
	   $("#payplutus_popup").hide(500);
	   $("#open_blank").show();
	  }
	 } else {
	 }
	}


function show_popup_data(){
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	$.ajax({						
		url: "popup.jsp",
		type: "GET",
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);		
			$("#oeprator_bold").html(operator);
			$("#circle_bold").html(circle);
			
		}
	});
}
// Function for change operator

function change_operator(){
	debugger
	//alert("calling change_operator");
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	//alert(operator);
	//alert(circle);
	$("#pop_operator").attr("value",operator);
	$.ajax({						
		url: "popup.jsp",
		type: "GET",	
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);	
			$("#oeprator_bold").html(operator);
			  
		}
	});
}
function change_circle(){
	debugger
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	$("#pop_circle").attr("value",circle);
	$("#pop_operator").attr("value",operator);
	$.ajax({						
		url: "popup.jsp",
		type: "GET",
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);	
			 $("#circle_bold").html(circle);
		}
	});
}


// Ajax Function for check email id is exist or not

function check_mail(){
	var email = $("#user-email").val();
	$.ajax({
		url : "checkMail.php",
		type : "GET",
		data : {email: email},
		success : function(response){
			$("#error").html(response);
		}
	});
}

function Check_pwd(){
	var password = $("#password").val();
	var cp_password = $("#cpassword").val();
	if(password == cp_password){
		$("#recover").submit();
	}else{
		alert("Password Does not match.");	
	}
}

function show_value(a){
	
	var va = $("#reach").val();
	$("#amount").val(a);
}

//function confirm_guest(){
//	 var email = $("#email").val();
//	 var name = $("#username").val();
//	 var trxid = $("#trxid").val();
//	 
//	 $.ajax({
//	  url: "guestUser.jsp",
//	  type: "GET",
//	  data:{email: email, name: name, trxid: trxid},
//	  success: function(data){
//		  //alert(data);
//		  
//		  $("#myForm").submit();
//	  }
//	 }); 
//	}

function confirm_guest(){
	 
	 var email = $("#email").val();
	 var name = $("#username").val();
	 var trxid = $("#trxid").val();
	 
	 if(email && name !=''){
	  $.ajax({  
	   url: "guestUser.jsp",
	   type: "GET",
	   data:{email: email, name: name, trxid: trxid},
	   success: function(data){

	    $("#myForm").submit();
	   }
	  }); 
	 }
	 else{
	  $("#error").show();
	  $("#email").focus();
	 }  
	}

function hideFieldData()
{
	 debugger
	// document.getElementById('rcpost').style.visibility = 'hidden';
	 if (document.getElementById('connection_type_prepaid').checked) {
	 document.getElementById("proceeds").style.display = 'none';
	 document.getElementById("rcpost").style.display = 'none';
	 }else
	 {
	  document.getElementById("proceeds").style.display = 'block';
	  document.getElementById("rcpost").style.display = 'block'; 
	 
	 }
	 
}

function hide_plan(){
	debugger
	 var recharge_type = $("input[type='radio']:checked").val();
	 if(recharge_type == "Prepaid-Mobile"){
	  $("#payplutus_popup").show(500);
	 }else{
	  $("#payplutus_popup").hide(500);
	 }
	}

function hide_plan(){
	debugger
	 var recharge_type = $('input[type="radio"]:checked').val();
	
	 var operator = $('#rechargeOperator').children("option:selected").val();
	 //alert("Operator "+operator);
		 
	   if (document.getElementById('connection_type_prepaid').checked) {
		   document.getElementById("rc").style.display = 'block';
		   document.getElementById("rcpost").style.display = 'none';
		   document.getElementById("proceeds").style.display = 'none';
	       // document.getElementById('rc').style.visibility = 'visible';
	       // document.getElementById('rcpost').style.visibility = 'hidden';
	    } else if (document.getElementById('connection_type_postpaid').checked)
	    {
	    	 document.getElementById("proceeds").style.display = 'block';
	    	 document.getElementById("rcpost").style.display = 'block';
	    	 document.getElementById("rc").style.display = 'none';
	       // document.getElementById('rcpost').style.visibility = 'visible';
	       // document.getElementById('rc').style.visibility = 'hidden';
	    }
	
/*	 if(recharge_type === "Prepaid-Mobile"){
	  $("#open_plans").show();
	  $("#open_blank").hide();  
	 } else {
	  $("#open_plans").hide();
	  $("#open_blank").show();
	  $("#payplutus_popup").hide(500);
	 }
	 */
	}


(function() {

    var quotes = $(".payplutus_para");
    var quoteIndex = -1;
    
    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(2000)
            .fadeOut(2000, showNextQuote);
    }
    
    showNextQuote();
    
})();



function validateAllInp(elm){
    var inp =  $(elm).val();
 // remove string if it has onload word
    var reg1 = /onload(.*?)=/i;
 //remove string if it has <script></scirpt> tags
 var reg2 = /<script>(.*?)<\/script>/i;
 //remove string if it has onload </scirpt> tag
 var reg3 =/<\/script>/i;
 // remove string if it has src= attribute
 var reg4 = /src=(.*?)/i;
 //remove string if it has <script> tag
 var reg5 = /<script(.*?)>/i;
 //remove string if it has eval() mathod
 var reg6 = /eval(\(.*?)\)/i;
 //remove string if has expression() mathod
 var reg7 = /expression(\(.*?)\)/i;
 //remove string if has javasript word
 var reg8 = /javascript:(.*?)/i;
 //remove string if has javasript word
 var reg9 = /javascript:(.*?)/i;
 //remove string if has vbscript word
 var reg10 = /vbscript:(.*?)/i;

 var reg11 = /^[a-zA-Z0-9 //.@:_-]+$/g;
	var reg12 = / (:)/i;

 if(inp.match(reg12)){
	   var chopped = inp.replace(/ (:)/g, "");   
	   $(elm).val(chopped);

	}

if(!inp.match(reg11)){
var chopped = inp.replace(/[^a-zA-Z0-9 //.@:_-]/g, "");   
$(elm).val(chopped);


}
 

    if(inp.match(reg1) || inp.match(reg2) || inp.match(reg3) 
 || inp.match(reg4) || inp.match(reg5) || inp.match(reg6) 
 || inp.match(reg7) || inp.match(reg8) || inp.match(reg9)
 || inp.match(reg10))
 {
        $(elm).val("");
    } 

 }
 $(function(){
     $("input[type='text']").keyup(function(){
          validateAllInp(this)   
     });

 })
 
 function getBillerDetails(billetType,select){
	 	var host = window.location.hostname;
	 	$(".newRechargeUi   .r-tab-content").css("opacity","0.2")
	 		$(".loader").show();
	 $.ajax({
		 method:'POST',
		 url:'GetBillerDetails.action',
		 data:"billetType="+billetType,
		 success:function(data){
			 console.log("------")
			 console.log(data)
			 console.log("------")
			// console.log( data.resPayload.billerList)
			 if(data.statusCode == "300"){
				for(var i = 0; i < data.resPayload.billerList.length; i++){
					
					$("<option value="+data.resPayload.billerList[i].billerId+" data-image="+"."+data.resPayload.billerList[i].billerIcon+">"+ data.resPayload.billerList[i].billerName +"</option>").appendTo(select)
				}
			 }
		
				$(".newRechargeUi   .r-tab-content").css("opacity","1")
				$(".loader").hide();
					 try{
					 $(select).msDropDown();
					 appendErrorDiv(select)
					 } catch(e) {
						 alert(e.message);
					 }
				
				
				
		 }
	 })

 }
 
 function validtateInpLen(min,max,elm){
	 $(elm).attr("data-minLe",min)
	 $(elm).attr("maxlength",max)
	
 }
 function insurerChange(elm){
		// console.log(elm.val())
		 var el = $(elm).val()
		 var cun = $("#gas-consumer")
		 cun.val('')
		 	if(el == "241" ){
			 		$("#accoundBill").show()
			 		cun.attr("maxlength","12")
		 	}else{
		 		$("#accoundBill").hide();
		 			if(el == "310" || el == "338"){
		 				cun.attr("maxlength","10")
		 			}else if(el == "321"){
		 				cun.attr("maxlength","15")
		 			}
		 	
		 	}
		 	
	 }
 
 // Gas validation starts from Here
 
 function gasOpratorChange(elm){
	// console.log(elm.val())
	 var el = $(elm).val()
	 var cun = $("#gas-consumer")
	 cun.val('')
	 	if(el == "241" ){
		 		$("#accoundBill").show()
		 		cun.attr("maxlength","12").attr("placeholder","Customer Account Number ")
		 		$("#accoundBillInp").attr("placeholder","Bill Group Number")
	 	}else{
	 		$("#accoundBill").hide();
	 			if(el == "310" || el == "338"){
	 				if(el == "310"){
	 					cun.attr("placeholder","Consumer Number")
	 				}else{
	 					cun.attr("placeholder","Service Number")
	 				}
	 				cun.attr("maxlength","10")
	 			}else if(el == "321"){
	 				cun.attr("maxlength","15").attr("placeholder","Customer ID")
	 			}
	 	
	 	}
	 	
 }
 
 
 //On change Landline option
 function landlinServiceProviderOnChange(elm){
	 var el = $(elm)
	 var elVal = el.val();
	 var leNo = $("#landlineNum");
	 var leAmount = $("#landline_add_info")
	 var leAddInfo = $("#landineAddInfoWrapper");
	 var leAddInfoInp = leAddInfo.next('#landline_add_info').val();
	 
	 console.log(elVal)
	 if(elVal != "-1"){
		 $("#landline_bill .errorMsg").hide();
	 }
	
	 switch(elVal){
	 case "240":	
		
		 leAddInfo.show()
		 $("#Authenticator, #landlineAmountWrapper").hide().find('input').val('')
		  $('#landline_add_info').attr("placeholder","Customer Account Number")
		 leNo.attr("maxlength","8").attr("placeholder","Enter Phone Number Without STD Code");
		 break;
	 case "239": 	 	
		 leAddInfo.hide()
		 $("#landlineAmountWrapper").show().find('input').val('')
		 $("#Authenticator").hide()
		 leNo.attr("maxlength","11").attr("placeholder","Enter Phone Number Along With STD Code");
		 break;
		 
	 case "344":
		 leAddInfo.show()
		 $("#Authenticator").show();
		$("#landlineAmountWrapper").hide().find('input').val('');
		 $('#landline_add_info').attr("placeholder","Account Number")
		 leNo.attr("maxlength","10").attr("placeholder","Phone Number (with STD code but without starting 0 (1-10 digits)),");
		 break;
	  default:
		  console.log("default!");
	 }
 }
 
 // Validate Landline  form
 function validateLandlineForm(e){
	 e.preventDefault(e);
	 var landProv = $("#landline_operator").val();
	 var landlineNum = $("#landlineNum").val();
	 var landlineAmount = $("#landline_add_info").val();
	 var Authenticator = $("#Authenticator")

	 if(landProv != "-1"){
		 $("#landlineError",  $("#landlineNum").next(".errorMsg"),  $("#landlineNum").next(".errorMsg")).hide();
		 if( landlineNum.length == 0){
				
			 $("#landlineNum").next(".errorMsg").show().text("This Field Is Mandatroy. Please Fill This Feild.") 
		 }
		 switch(landProv){
		 case "239":
			  console.log(2)
			 if(landlineNum.length == 11){
				 console.log("Submitted!!")
				 submitBillerForm('#landline_bill',e)
			 }else{
			
				 $("#landlineNum").next(".errorMsg").show().text("Please Provide Valid Phone Number Along with STD Code(11 Digits).");         
			 }
			break;
		 case "240":
			 
			 	if(landlineNum.length == 8 && landlineAmount.length == 10){
			 		 submitBillerForm('#landline_bill',e)
			 		
			 	}else{
			 		if(landlineNum.length != 8){
			 			$("#landlineNum").next(".errorMsg").show().text("Please Provide Valid Phone Number (8 Digits).");           
			 		}
			 		if(landlineAmount.length != 10){
			 			$("#landline_add_info").next(".errorMsg").show().text("Please Provide Valid Customer Account Number(10 Digits)."); 
			 		}
			 		
			 	}
		 break;
		 case "344":
		
			 if(landlineNum.length > 0 && landlineAmount.length == 10 && Authenticator.find('select') != "-1"){
				 submitBillerForm('#landline_bill',e)
			 }
			 else{
				
				 if(Authenticator.find('select') != "-1"){
					 $("#AuthenticatorError").show().text("Please select the Authenticator type.");
				 }
				 if(landlineAmount.length == 0){
				
					 $("#landlineNum").next(".errorMsg").show().text("This Field Is Mandatsadasdroy. Please Fill This Feild.")
				 }
				 if($("#landline_add_info").length != 10){
					 $("#landline_add_info").next(".errorMsg").show().text("Please Provide Valid Account Number(10 Digits).")
				 }
				
			 }
			 break;
			 default:
				 console.log(3)
		 
		 }
	 }else{
		 
		if(landProv == "-1") {
			$("#landlineError").show().text("This Field Is Mandatroy. Please Fill This Feild.") 
		}
		 if( landlineNum.length == 0){
		
			 $("#landlineNum").next(".errorMsg").show().text("This Field Is Mandatroy. Please Fill This Feild.") 
		 }
		
	 }

	 
	 
 }
 
 //Validate Gas Form

 function validateGasForm(e){	 
	
	 e.preventDefault(e);
	
	 var el = $("#gas-opretor").val();
	 var oprError = $("#gas-provider");
	 var  cusInp = $("#gas-consumer")
	 var cun = cusInp.val();
	 if(el == "-1" || cun.length == 0){
		 
		 if(el == "-1"){
			 oprError.show().text("Please Select The Service Provider")
		 }else{
			 oprError.hide()
		 }
		 if( cun.length == 0){
			 cusInp.next(".errorMsg").show().text("This Field Is Mandatroy. Please Fill This Feild.")
		 }else{
			 cusInp.next(".errorMsg").hide()
		 }
		
		 
	 }else{
		 	switch(el){
	 		case "241":
	 			
	 			if(cun.match(/[2]{1}[1]{1}[0-9]{10}$/) && $("#accoundBillInp").val().match(/^[\d\w]{1,8}$/)){
	 					console.log("submited")
	 					submitBillerForm("#gasRecharge",e)
	 				 $("#gas-consumer, #accoundBillInp").next(".errorMsg").hide()
	 			}else{
	 				
	 				if(!cun.match(/[2]{1}[1]{1}[0-9]{10}$/) ){
	 				
	 					$("#gas-consumer").next(".errorMsg").show().text("Please Fill The Valid Customer Account Number.")
	 			 	}
	 				if(!$("#accoundBillInp").val().match(/^[\d\w]{1,8}$/)){
	 				
	 					$("#accoundBillInp").next(".errorMsg").show().text("Please Fill The Valid Group Number")
	 				}
	 				
	 			}
	 			break;
	 		case "321":
	 		
	 			
	 			if(!cun.match(/^[\d]{1,15}$/)){
	 				$("#gas-consumer").next(".errorMsg").show().text("Please Fill The Valid Sevice Number.")
	 				
	 			}else{
	 				submitBillerForm("#gasRecharge",e)
	 			}
	 		break;
	 		case "310":
	 	

	 			if(!cun.match(/^[\d]{10}$/)){
	 				$("#gas-consumer").next(".errorMsg").show().text("Please Fill The Valid Consumer Number.")
	 				
	 			}else{
	 				submitBillerForm("#gasRecharge",e)
	 			}
	 		break;
	 		case "338":
	 	
	 			
	 			if(!cun.match(/^[\d]{10}$/)){
	 			
	 				$("#gas-consumer").next(".errorMsg").show().text("Please Fill The Valid Customer ID.")
	 				
	 			}else{
	 				submitBillerForm("#gasRecharge",e)
	 			}
	 		break;
	 		default:
	 	       console.log("Default")
	 	
	 	
	 	}
		 
	 }

 }
 
 
 function validateInsuranceForm(e){
	 e.preventDefault(e);
	 var insProvider = $("#insurance").val();
	 var polNum   = $("#policy_number").val()
	 var dob    = $("#insurer_dob").val()


	 if(insProvider != "-1" && polNum.length != 0 && dob.length != 0){
		 $("#insurance-error").hide()
		 	validatePolicyNo(insProvider, polNum,e)
		 
	 }else{
		
		 if(insProvider == "-1"){
			$("#insurance-error").show().text("Please select the insurer.")
		 }else{
			 $("#insurance-error").next('.errorMsg').hide()
		 }
		
		 if(polNum.length == 0){
			
			 $("#policy_number").next(".errorMsg").show().text("Please Provide Policy No.")
		 		
		 }else{
			 $("#policy_number").next(".errorMsg").hide()
		 }
		 
		 
		 
	 }
	 validateInsDob(dob)
	 
 }
 
 function validateInsDob(dob){
	
	 if(dob.length == 0){
		// alert(dob)
		 $("#insurer_dob").next(".errorMsg").show().text("Please Select DOB.")
		 
	 }else{
		 $("#insurer_dob").next(".errorMsg").hide()
	 }
 }
 
 function validatePolicyNo(insProvider, polNum,e){
	 if(insProvider == "242" && polNum.length < 10){
		 	
	 		
	 		$("#policy_number").next(".errorMsg").show().text("Invalid Policy No.")
	 		
	 		
	 	}else if(insProvider == "243" && polNum.length < 8){
	 		
	 		$("#policy_number").next(".errorMsg").show().text("Invalid Policy No.")
		 		
	 	}else{
	 		submitBillerForm("#insuranceRecharge",e)
	 		$("#policy_number").next(".errorMsg").hide()
	 	}
 }
 function changeInsurar(){
	var insProvider = $("#insurance").val();
	var polNum   = $("#policy_number")
	 polNum.val('')
	 
	if(insProvider == "242"){
		polNum.attr("maxlength","10")
		polNum.addClass("alphaNum-vl")
		polNum.removeClass("onlyNum");
		 $("#insurance-error").hide()
	}else if(insProvider == "243"){
		polNum.attr("maxlength","8")
		polNum.addClass("onlyNum")
		polNum.removeClass("alphaNum-vl");
		 $("#insurance-error").hide()
	}else{
		polNum.attr("maxlength","10")
	}
 }
 function electricityProviderChange(){
	 var opr = $("#eleOpretor").val()

	 $("#electricity_consumer, #electricity_account").val('')
	 if(opr != "-1"){
		 $("#oprErr").hide();
	 }
	 switch(opr){
	 case "332":
		 var cityName = ["AGRA","AHMEDABAD","SURAT","BIWANDI"]
		 $("#eleAccount").hide();
		 $("#elethirdOption").show();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		  $("#elethirdOption select").html("")
		  $("<option value='-1'>Please Select City</option>").appendTo("#elethirdOption select")
		 for(var i = 0; i <  cityName.length ;i++){
			 $("<option value="+ cityName[i] +">"+ cityName[i] + "</option>").appendTo("#elethirdOption select");
		 }
		 $("#electricity_account").attr("Placeholder","Billing Unit").attr("maxlength","4")
		 
		 break;
	 case "315":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "345":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "335":
		 
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "326":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","9").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "317":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer ID").attr("maxlength","11").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "318":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Business Partener Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
	 case "324":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","13").addClass(" alphaNum-vl").removeClass(" onlyNum")
		 break;
	 case "325":
		 
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Business Partener Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl");
		 
		 break;
	 case "333":
		 
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer ID").attr("maxlength","12").addClass("onlyNum").removeClass("alphaNum-vl");
		 break;
	 case "235":
 
		 
		 $("#eleAccount").show();
		 $("#elethirdOption").hide()
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","9").addClass("onlyNum").removeClass("alphaNum-vl")
		 
		 $("#electricity_account").attr("Placeholder","Cycle Number").attr("maxlength","2")
		 
		 break;
	 case "236":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","9").addClass("onlyNum").removeClass("alphaNum-vl")
		 
		
		 break;
	 case "237":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","9").addClass("onlyNum").removeClass("alphaNum-vl")
		 
		 break;
	 case "238":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","12").addClass("onlyNum").removeClass("alphaNum-vl")
		 
		 break;
	 case "340":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","10").addClass("onlyNum").removeClass("alphaNum-vl")
		 
		 break;
	 case "342":
		 $("#eleAccount, #elethirdOption").show();
		 $("#electricity_consumer").attr("Placeholder","Customer Number").attr("maxlength","12").addClass("onlyNum").removeClass("alphaNum-vl")
		 $("#elethirdOption select").html("")
		  $("<option value='-1'>Please Select Processing Cycle</option>").appendTo("#elethirdOption select")
		 for(var i = 0; i <  10 ;i++){
			 $("<option value="+ '0' +  i +">"+ '0' + i + "</option>").appendTo("#elethirdOption select");
		 }
		 $("#electricity_account").attr("Placeholder","Billing Unit").attr("maxlength","4")
		 break;
		 
	 case "330":
		 $("#eleAccount, #elethirdOption").hide();
		 $("#electricity_consumer").attr("Placeholder","K Number").attr("maxlength","12").addClass("onlyNum").removeClass("alphaNum-vl")
		 break;
		 default:
			 console.log("Default.")
		 
		 
	 
	 }
 }
 
  function electricityFormSubmit(e){
	  e.preventDefault(e);
	 var opr = $("#eleOpretor").val()
	 var cusInp = $("#electricity_consumer")
	 var cusNum = cusInp.val();
	 var oprError = $("#oprErr")
	 var accInp = $("#electricity_account")
	 var accVal = accInp.val()
	 

	 if(opr == "-1" || cusNum.length == 0){
		 
		 if(opr == "-1"){
			 oprError.show().text("Please Select The Service Provider")
		 }else{
			 oprError.hide()
		 }
		 if( cusNum.length == 0){
			 cusInp.next(".errorMsg").show().text("This Field Is Mandatroy. Please Fill This Feild.")
		 }else{
			 cusInp.next(".errorMsg").hide()
		 }
		
		 
	 }/*else{
		 oprError.hide();
		 switch(opr){
		 case "332":
			
			
			 if(cusNum.match(/^[\d]{1,15}$/) &&  $("#thirdEleOption").val() != "-1"){
				 accInp.val($("#elethirdOption select").val());
				 console.log("submitted!!!")	
			
				 
				 submitBillerForm("#electricityForm",e)
				 
			 }else{
				 		if(!cusNum.match(/^[\d]{1,15}$/)){
				 			cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 		}
				 		
				 		if($("#thirdEleOption").val() == "-1"){
				 			$("#thirdEleOption").next(".errorMsg").show().text("Please Select Processing Cycle.")
				 		}
			 }
			 
			 break;
		 case "315":
			 if(cusNum.match(/^[\d]{10}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			
			 break;
		 case "345":
			 if(cusNum.match(/^[\d]{10}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			
			 break;
		 case "335":	 
			 if(cusNum.match(/^[\d]{10}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			
			 break;
		 case "326":
			 if(cusNum.match(/^[\d]{10}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			
			 break;
		 case "317":
			 if(cusNum.match(/^[\d]{11}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer ID.")
				 
			 }
			
			 break;
		 case "318":
			 if(cusNum.match(/^[\d]{10}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Business Partner Number.")
				 
			 }
			
			 break;
		 case "324":
			 if(cusNum.match(/^[\d\w]{5,13}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Consumer Number.")
				 
			 }
			 break;
		 case "325":
			 
			 if(cusNum.match(/^[\d]{6,10}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Business Partner Number.")
				 
			 }

			 
			 break;
		 case "333":
			 
			 if(cusNum.match(/^[\d]{1,12}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 alert(111)		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Consumer ID.")
				 
			 }

			 break;
		 case "235":
	 
			 
			 if(cusNum.match(/^[\d]{9}$/) && accVal.match(/^[\d]{2}$/)){
				 submitBillerForm("#electricityForm",e)
			
				 
			 }else{
				 
				 	if(!cusNum.match(/^[\d]{9}$/)){
				 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 	} 
				 	
				 	if(!accVal.match(/^[\d]{2}$/)){
				 	
				 		accInp.next(".errorMsg").show().text("Please Fill The Valid Cycle Number.")
				 	}
			 }
			 
			 break;
		 case "236":
			 if(cusNum.match(/^[\d]{9}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			
			 break;
		 case "237":
			 if(cusNum.match(/^[\d]{9}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			 
			 break;
		 case "238":
			 if(cusNum.match(/^[\d]{11,12}$/)){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			 
			 break;
		 case "340":
			 if(cusNum.match(/^[\d]{9,10}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 
			 }
			 
			 break;
		 case "342":
			 if(cusNum.match(/^[\d]{12}$/) &&  accVal.match(/^[\d]{4}$/) && $("#thirdEleOption").val() != "-1"){
				 submitBillerForm("#electricityForm",e)		
				 
			 }else{
				 		if(!cusNum.match(/^[\d]{12}$/)){
				 			cusInp.next(".errorMsg").show().text("Please Fill The Valid Customer Number.")
				 		}
				 		if(!accVal.match(/^[\d]{4}$/)){
				 			accInp.next(".errorMsg").show().text("Please Fill the Billing Unit.")
				 		}
				 		if($("#thirdEleOption").val() == "-1"){
				 			$("#thirdEleOption").next(".errorMsg").show().text("Please Select Processing Cycle.")
				 		}
			 }
			 break;
			 
		 case "330":
			 
			 if(cusNum.match(/^[23][\d]{11}$/)){
				 submitBillerForm("#electricityForm",e)	
				 
			 }else{
				 		 		
				 		cusInp.next(".errorMsg").show().text("Please Fill The Valid K Number.")
				 
			 }
			 break; 
			
			 default:
				 console.log("Default.")
			 
			 
		 
		 }
	 }
	*/
 }
 
function getBillDetails(){
	
	 $.ajax({
         type: 'POST',
         url: frm.attr('action'),
         data: frm.serialize(),
         success: function (data) {
             console.log('Submission was successful.');
             console.log(data);
         },
         error: function (data) {
             console.log('An error occurred.');
             console.log(data);
         },
     });
 
} 

 
 $(function(){
	 

	 
	 
	 
	
		
	 var insProvider = $("#insurance").val();
	 var polNum   = $("#policy_number").val()
	  var dob    = $("#insurer_dob").val()
	 	$("#policy_number").blur(function(){
	 		validatePolicyNo(insProvider, polNum)
	 	});
	
	 	$("#insurer_dob").blur(function(){
	 		setTimeout(function(){
	 			validateInsDob(dob)
	 		},300)
	 		
	 	
	 	});
 })
 
 function submitBillerForm(formId,e){
	
 e.preventDefault(); 
	
//r eleServ = $("#eleOpretor option:selected").text();

var btcFrom = $(formId).attr('data-btc-form');
	if(btcFrom == "true"){
		 $(formId).submit();
	}else{
		$(formId + " input[type='submit']").prop("disabled",true).val("Loading...")
		
		$(".responseMsg").text('')
	    $.ajax({
	           type: "POST",
	           url: $(formId).attr("action"),
	           data: $(formId).serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	        	   console.log(data)
	        	   $("#proceedBiller").show()
	        	  
	        	   $("#txnId").val(data.resPayload.txnId);
	        	   $("#billerAmount").val(data.resPayload.billerAmount)
	        	    $("#billerType").val(data.resPayload.billerType)
	               	
	        	   console.log(data)
	        	//ta.resPayload.billerId = eleServ;
	        	    	$(formId + " input[type='submit']").prop("disabled",false).val("Proceed to Pay")
		
	        	
	        		  if(data.statusCode ==  "300"){
	                  	
	                	
	                	  $("#billerTable").html('');
	             		 // console.log(val)
	                	  $.each(data.resPayload,function(key,val){
	                		  console.log(key)
	                		  
	                		  
	                		  if(key != "billerList"){
	                		  $('<tr><td><strong>'+ key +'</strong></td><td>'+ val +'</td></tr>').appendTo("#billerTable");
	                		 // console.log(val)
	                		  }
	                	  });
	                	  $("#proceedBiller").show();
	                  }else{
	                	  $("#proceedBiller").hide();
	                	  $("#billerTable").html("<div style='color:red;font-size:20px'>"+data.statusMsg+"</div>");
	                	
	                  }
	        	  
	        	   $("#billerPopup").modal();
	             
	           },
	            error: function (data) {
	                console.log('An error occurred.');
	                console.log(data);
	            },
	         });
	}


    // avoid to execute the actual submit of the form.


}
 function changeDTHPlaceholderAndLen(el,targetInp){
	var elv = $(el).val()
	var tar = $(targetInp)
	var le = 12
	var ph = 'Enter your DTH number'
		
		if(elv != "-1"){
			$("#dthOpratorErr").hide()
		}
	switch(elv){
	case 'AIRTEL DIGITAL TV':
		le = 10
		ph = 'Customer ID'
		tar.attr('Placeholder','Customer ID','maxlength','10');
		break;
	case 'DISH TV':
		le = 11
		ph = 'Viewing Card Number'
	
		break;
	case 'RELIANCE DIGITAL TV':	
		le = 12
		ph = 'Smart Card Number'
		
		break;
	case 'VIDEOCON D2H':
		le = 10
		ph = 'Customer ID'
	
		break;
	case 'TATA SKY':	
		le = 10
		ph = 'Subscriber ID'
		
		break;
	case 'SUN DIRECT':	
		le = 11
		ph = 'Smart Card Number'
		break;
		
	default:
		le = 12
		
		
		
	
	}
	tar.attr('Placeholder',ph)
	tar.attr('maxlength',le)
}

 
 function billerConfirm(amount,txnId,billerType,csrf){
	 
	if($("#billerTypePlatForm").val() == "B2B"){
		var txnId = $("#txnId").val();
		 var amount = $("#billerAmount").val()
		 var billerType = $("#billerType").val()
	}
	
	
	 $.ajax({
		 method:'POST',
		 url:'payOperatorBill',
		 data:"billerAmount="+amount+"&txnId="+txnId+"&billerType="+billerType+"&csrfPreventionSalt="+csrf,
		 success:function(res){
		console.log(res)
						 if(res.statusCode == "300"){
				 $(".responseMsg").text(res.statusMsg).css("color","green")
			
			 }else{
				 $(".responseMsg").text(res.statusMsg).css("color","red")
				
			 }
			$("#doneBTN").text("Done");
			$("#proceedBiller").hide();
			 
		 }
	 })
 }
 // Get opretor 
 
 function getOpretorDetails(id,device){
	 debugger
	 //alert("function getOpretorDetails---");
	 //alert("-id--"+id);
	 //alert("-device--"+device);
		var el = $(id).val();
	 //alert(el);
	if(el.length > 4 ){
		
		el = el.slice(0,5)
		console.log(el)
		$.ajax({
			  type: "POST",
			  url: 'GetOperatorCircle.action',
			  data: "rechargeBean.numberSeries="+el,
			  
			  success: function(data){
				 // alert($("#rechargeOperator").val());
				 // alert($("#rechargeOperatorPost").val());
					console.log(data);
					if(device == "mobile"){
					$("#rechargeOperator").val(data.operator.trim())
					$("#circlei").val(data.circle.trim())
					$("#rechargeOperator").trigger('change')
					$("#circlei").trigger('change')
					}else{
						$("#datacard_operator").val(data.operator.trim())
						$("#datacard_circle").val(data.circle.trim())
					}$("#datacard_operator").trigger('change')
					$("#datacard_circle").trigger('change')
			  }
			  
			});
		//alert("-data--"+data);
			}
		;
	}