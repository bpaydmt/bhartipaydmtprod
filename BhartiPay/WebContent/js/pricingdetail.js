

//  Pricing

(function () {
	  document.getElementById("pricingTable").style.display = "none";
	  $("#pricingTable").hide();
})();

 
function save_row(no)
{
	debugger

 var userId=document.getElementById("agentId").value;
 var rangeFrom=document.getElementById("fromInput"+no).value;
 var rangeTo=document.getElementById("toInput"+no).value;
 var distributor=document.getElementById("distInput"+no).value;
 var superDistributor=document.getElementById("supDistInput"+no).value;

 
 if(distributor<0 || superDistributor<0)
  {
	 $('#msg').html("Values can not be negative");
 	 $("#msg").show();
  	 setTimeout(function() { $("#msg").hide(); }, 3000); 
	 
  return false;
  }
	// if(superDistributor<0) return false;
 
 if(distributor=="") distributor=0;
 if(superDistributor=="") superDistributor=0;
 
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="none"; 
 document.getElementById("delete_button"+no).style.display="block";
     $.ajax({
			method:'Post',
			cache:0,  		
			url:'savePricing',
			data:'userId='+userId+'&distributorPercentage='+distributor+'&superDistributorPercentage='+superDistributor+'&rangeFrom='+rangeFrom+'&rangeTo='+rangeTo,
			
			success:function(response){
			    var json = JSON.parse(response);
			    if(json.status==true)
                {
			     agentPricingDetails();
			     $('#msg').html(json.aggregatorId);
			     //$('#msg').html("Pricing details saved");
            	 $("#msg").show();
             	 setTimeout(function() { $("#msg").hide(); }, 3000);
	            }else
	            {
	            	 document.getElementById("save_button"+no).style.display="block"; 
	            	 document.getElementById("delete_button"+no).style.display="none";
	            	
	                $('#msg').html(json.aggregatorId);
                 // $('#msg').html("Pricing details  not saved");
	           	  $("#msg").show();
	           	  setTimeout(function() { $("#msg").hide(); }, 3000);   
	            }
		        
			}
		})
 
}



function addNewPlan()
{
	var userId=document.getElementById("agentId").value;
	if(document.getElementById('leanAccountTable')){
		deleteData();
		}
	
	  $.ajax({
			method:'Post',
			cache:0,  		
			url:'agentPricingDetail',
			data:'userId='+userId,
			success:function(response){
	   		var count=0;
	   		$.each(JSON.parse(response), function(idx, obj) {
	   		   count++;
	   		  $('#pricingTable').find('tbody')
	            .append('<tr><td width="15%">'+count+'</td> <td width="15%" >'+obj.rangeFrom+'</td> <td width="15%">'+obj.rangeTo+'</td><td width="20%">'+obj.distributorPercentage+'</td><td width="20%">'+obj.superDistributorPercentage+'</td> <td align="center">  <input type="button" value="Delete" onclick="deleteRecord(\'' +obj.userId+ '\', \'' +obj.rangeFrom+ '\', \'' +obj.rangeTo+ '\')" id="deleteButtons'+obj.userId+'" /></td> <tr>');
	   
	   		});
	   		 
	   		}
	  })

}


 
