

//  Pricing

(function () {
	 document.getElementById("data_table").style.display = "none";
	 document.getElementById("leanAccountTable").style.display = "none";
	 document.getElementById("scheme").style.display = "none";
	 document.getElementById("markedAslean").style.display = "none";
})();


function edit_row(no)
{debugger
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
	
 var from=document.getElementById("from"+no);
 var to=document.getElementById("to"+no);
 var dist=document.getElementById("dist"+no);
 var supDist=document.getElementById("supDist"+no);

 var fromData=from.innerHTML;
 var toData=to.innerHTML;
 var distData=dist.innerHTML;
 var supData=supDist.innerHTML;
	
 from.innerHTML="<input type='text' id='fromInput"+no+"' value='"+fromData+"'  readonly>";
 to.innerHTML="<input type='text' id='toInput"+no+"' value='"+toData+"'>";
 dist.innerHTML="<input type='text' id='distInput"+no+"' value='"+distData+"'>";
 supDist.innerHTML="<input type='text' id='supDistInput"+no+"' value='"+supData+"'>";
}

function save_row(no)
{
	debugger

 var userId=document.getElementById("agentId").value;
 var rangeFrom=document.getElementById("fromInput"+no).value;
 var rangeTo=document.getElementById("toInput"+no).value;
 var distributor=document.getElementById("distInput"+no).value;
 var superDistributor=document.getElementById("supDistInput"+no).value;

 
 if(distributor<0 || superDistributor<0)
  {
	 $('#msg').html("Values can not be negative");
 	 $("#msg").show();
  	 setTimeout(function() { $("#msg").hide(); }, 3000); 
	 
  return false;
  }
	// if(superDistributor<0) return false;
 
 if(distributor=="") distributor=0;
 if(superDistributor=="") superDistributor=0;
 
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="none"; 
 document.getElementById("delete_button"+no).style.display="block";
     $.ajax({
			method:'Post',
			cache:0,  		
			url:'savePricing',
			data:'userId='+userId+'&distributorPercentage='+distributor+'&superDistributorPercentage='+superDistributor+'&rangeFrom='+rangeFrom+'&rangeTo='+rangeTo,
			
			success:function(response){
			    var json = JSON.parse(response);
			    if(json.status==true)
                {
			     agentPricingDetails();
			     $('#msg').html(json.aggregatorId);
			     //$('#msg').html("Pricing details saved");
            	 $("#msg").show();
             	 setTimeout(function() { $("#msg").hide(); }, 3000);
	            }else
	            {
	            	 document.getElementById("save_button"+no).style.display="block"; 
	            	 document.getElementById("delete_button"+no).style.display="none";
	            	
	                $('#msg').html(json.aggregatorId);
                 // $('#msg').html("Pricing details  not saved");
	           	  $("#msg").show();
	           	  setTimeout(function() { $("#msg").hide(); }, 3000);   
	            }
		        
			}
		})
 
}




function delete_row(no)
{
debugger

     if(document.getElementById('leanAccountTable')){
	  deleteData();
	 }
	 var userId=document.getElementById("agentId").value;
	 var rangeFrom=document.getElementById("fromInput"+no).value;
	 var rangeTo=document.getElementById("toInput"+no).value;
	 
    $.ajax({
		method:'Post',
		cache:0,  		
		url:'deletePricing',
		data:'userId='+userId+'&rangeFrom='+rangeFrom+'&rangeTo='+rangeTo,
		
		success:function(response){
		    var json = JSON.parse(response);
		    if(json.status==true)
	         {
		     document.getElementById("row"+no+"").outerHTML="";
		     agentPricingDetails();
			 $('#msg').html("Pricing details deleted");
	     	 $("#msg").show();
	      	 setTimeout(function() { $("#msg").hide(); }, 3000);
	         }else{
	        	 document.getElementById("row"+no+"").outerHTML="";
	        	 agentPricingDetails();
		         }
	        
		}
	})
 

 
}


function add_row()
{debugger
 var newFrom=document.getElementById("newFrom").value;
 var newTo=document.getElementById("newTo").value;
 var newDist=document.getElementById("newDist").value;
 var newSupDist=document.getElementById("newSupDist").value;

 
 var fixValue="5000";
 if(parseInt(newTo)>parseInt(newFrom) && parseInt(fixValue)>=parseInt(newTo) && parseInt(newTo.trim())!=""){  // new_name1.trim()!="" &&
 var table=document.getElementById("data_table");
 var table_len=(table.rows.length)-1;
 
 var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='from"+table_len+"'>"+newFrom+"</td>    <td id='to"+table_len+"'>"+newTo+"</td>     <td id='dist"+table_len+"'>"+newDist+"</td>      <td id='supDist"+table_len+"'>"+newSupDist+"</td><td><input type='button' id='edit_button"+table_len+"' value='Edit' class='edit' onclick='edit_row("+table_len+")'> <input type='button' id='save_button"+table_len+"' value='Save' class='save' onclick='save_row("+table_len+")'> <input type='button' id='delete_button"+table_len+"'  value='Delete' class='delete' onclick='delete_row("+table_len+")'></td>   </tr>";

 document.getElementById("edit_button"+table_len).style.display="none";
 document.getElementById("delete_button"+table_len).style.display="none";
 
 var from=document.getElementById("from"+table_len);
 var to=document.getElementById("to"+table_len);
 var dist=document.getElementById("dist"+table_len);
 var supDist=document.getElementById("supDist"+table_len);
	
 var fromData=from.innerHTML;
 var toData=to.innerHTML;
 var distData=dist.innerHTML;
 var supDistData=supDist.innerHTML;
	
 from.innerHTML="<input type='number' id='fromInput"+table_len+"' value='"+fromData+"'  readonly>";
 to.innerHTML="<input type='number' id='toInput"+table_len+"' value='"+toData+"'>";
 dist.innerHTML="<input type='number' id='distInput"+table_len+"' value='"+distData+"'>";
 supDist.innerHTML="<input type='number' id='supDistInput"+table_len+"' value='"+supDistData+"'>";
 
 document.getElementById("newFrom").value=newTo;
 document.getElementById("newTo").value="";
 document.getElementById("newDist").value="";
 document.getElementById("newSupDist").value="";
 }else
	 {

	 $('#msg').html("Please enter valid range value");
 	 $("#msg").show();
  	 setTimeout(function() { $("#msg").hide(); }, 3000);
  	 
	 //var msg=document.getElementById("msg");
	 //msg.append("Please enter valid range value");
	
	//setTimeout(function() { $("#msg").remove(); }, 3000);  
	 }
 
}



function getLeanAccount(){
debugger


    var list=document.getElementById("selectAggregator");
    var aggr=list.options[list.selectedIndex].value;
    document.getElementById("agentId").value=aggr;
    $("#markedAslean").show();
   // document.getElementById("markedAslean"+no).style.display="block";
   
    
    //document.getElementById("data_table"+no).style.display="none";
    //document.getElementById("data_table"+no).style.display="block";
 }

function agentPricingDetails()
{
	var userId=document.getElementById("agentId").value;
	if(document.getElementById('leanAccountTable')){
		deleteData();
		}
	
	  $.ajax({
			method:'Post',
			cache:0,  		
			url:'agentPricingDetail',
			data:'userId='+userId,
			success:function(response){
	   		var count=0;
	   		$.each(JSON.parse(response), function(idx, obj) {
	   		   count++;
	   		  $('#leanAccountTable').find('tbody')
	            .append('<tr><td width="4%">'+count+'</td> <td width="10%" >'+obj.rangeFrom+'</td> <td width="10%">'+obj.rangeTo+'</td><td width="10%">'+obj.distributorPercentage+'</td><td width="10%">'+obj.superDistributorPercentage+'</td> <td align="center">  <input type="button" value="Delete" onclick="deleteRecord(\'' +obj.userId+ '\', \'' +obj.rangeFrom+ '\', \'' +obj.rangeTo+ '\')" id="deleteButtons'+obj.userId+'" /></td> <tr>');
	   
	   		});
	   		 
	   		}
	  })

}

function deleteData()
{
    var myTable = document.getElementById("leanAccountTable");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
}


function deleteDataRow()
{
    var myTable = document.getElementById("data_table");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
}


function deleteRecord(userId,rangeFrom,rangeTo)
{
	if(document.getElementById('leanAccountTable')){
		deleteData();
		}
	
	$.ajax({
		method:'Post',
		cache:0,  		
		url:'deletePricing',
		data:'userId='+userId+'&rangeFrom='+rangeFrom+'&rangeTo='+rangeTo,
		
		success:function(response){
		    var json = JSON.parse(response);
		    if(json.status==true)
	         {
		      $('#msg').html("Pricing detail deleted");
		      agentPricingDetails();
	     	 $("#msg").show();
	      	 setTimeout(function() { $("#msg").hide(); }, 3000);
	         }
	        
		}
	})

}

function markAsLean()
{
	  
	    $("#data_table").show(); 
		document.getElementById("data_table").style.display = "block";
		 $("#leanAccountTable").show();
		document.getElementById("leanAccountTable").style.display = "block";
		 $("#scheme").show(); 
		document.getElementById("scheme").style.display = "block";

		//$("#loadDiv").load(location.href + " #loadDiv");
		//$("#tableDiv").load(location.href + " #tableDiv");
		$('#loadDiv').load(document.URL +  '  #loadDiv');
		//$('#tableDiv').load(document.URL +  '  #tableDiv');
		if(document.getElementById('data_table')){
			//deleteDataRow();
			//location.reload()
			}
		agentPricingDetails();

		
}

