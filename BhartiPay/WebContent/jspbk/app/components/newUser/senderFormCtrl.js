dmt.controller('senderFromCtrl',function($http, $location,$rootScope,$timeout,$interval){
	
	var newUser =  this;
	newUser.senderMobileNo = SENDERMOBILENO;
	newUser.error= false;
	newUser.otpResendLink = false;
	newUser.btnText = "Continue";
	newUser.loading = false;
	newUser.counter = 10;
	newUser.addCounter = function(){
		if(newUser.counter != 0){
			newUser.counter--
		}else{
			newUser.otpResendLink = true;
		}
		console.log(newUser.counter)
	}
	newUser.stop =  function(){
		$interval.cancel(newUser.runCoundown);
	}

	newUser.showModelBox = function(){	
		console.log(newUser.senderMobileNo)
		if(newUser.senderMobileNo  == '' || newUser.senderMobileNo  ==  'Not Defined Yet' || newUser.senderMobileNo  == 'undefine'){
			  
			$location.path("/")
			$rootScope.modelBoxContent =  "jspbk/app/components/error/dmtError.html" +  $rootScope.version;		 
		   $('#myModal').modal('show');			
	
			
		}else{
			newUser.stop();               
			newUser.runCoundown = $interval(newUser.addCounter,1000)
			$timeout(function(){
		   $rootScope.modelBoxContent =  "jspbk/app/components/newUser/newUserMsg.html" +  $rootScope.version;
		   $('#myModal').modal('show');
			}
		   , 500);
		}
		 
		
}
   newUser.postSenderData = function(){ 
	   $http.get('CheckSession')
       .then(function(res){ 
    	  // alert(2)
    	   console.log(res)
    	   if(res.data.status == "TRUE"){
	   newUser.btnText = "Loading";
		newUser.loading = true;
		   $http({
		     method: 'POST',
		     url: 'RegisterSenderBK',
		     data    :'mastBean.firstName='+ newUser.firstName +'&mastBean.mpin='+ newUser.mpin+'&mastBean.otp='+ newUser.otp +'&mastBean.mobileNo='+ SENDERMOBILENO ,//forms user object
		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		   })
		    .then(function(res){ 	
		    	newUser.senderData = res.data;
		        console.log(res)
		    	 if(newUser.senderData.statusCode == "1000"){
		    		 $rootScope.LoginSender = res.data;
		              $location.path('/regUser'); 
		              newUser.error = false;
		              
		              
		           }else{
		        	   
		        	   newUser.errorMsg = res.data.statusDesc 
		        	   newUser.error= true;
		          
		           }
		        newUser.btnText = "Continue";
		    	newUser.loading = false;
		       
		  },function(res){
		   console.log(res.statusText)
		  });
    	   } else{
    		   alert("Your session has expired. Please login again.")
    		   window.close();
    	   }
       },function(res){
  		   console.log(res.statusText)
  		  });


		  },
		
		  newUser.DmtOtpResend = function(){ 
				$http.get('CheckSession')
			       .then(function(res){ 
			    	  // alert(2)
			    	   console.log(res)
			    	   if(res.data.status == "TRUE"){
			   $http({
			     method: 'POST',
			     url: 'DmtOtpResendBK',
			     data:'mastBean.mobileNo='+ SENDERMOBILENO ,//forms user object
			           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   })
			    .then(function(res){ 
			    	if(res.data.status == "true"){
			    		$rootScope.modelBoxContent = "jspbk/app/components/newUser/resendOTpSuccess.html" +  $rootScope.version;
			    		newUser.otpResendLink = false;
			    		newUser.counter = 90;
			    	
			    	}else{
			    		$rootScope.modelBoxContent = "jspbk/app/components/error/errorMsg.html" +  $rootScope.version;
			    		
			    	}
			        
			    	$('#myModal').modal('show');  	      
			       console.log(res)
			  },function(res){
			   console.log(res.statusText)
			  });
		  } else{
   		   alert("Your session has expired. Please login again.")
   		   window.close();
   	   }
      },function(res){
 		   console.log(res.statusText)
 		  });


			  }
		  $rootScope.$on('$routeChangeStart', function (next, last) {
		
			   newUser.stop();  
			});
});

