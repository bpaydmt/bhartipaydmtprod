<div  >
<img src="./jspbk/assets/img/logo2.png"  id="transLogo" style="display:none"/>
<div id="transSummary">
  <h2>Transaction Summary</h2>
      
  <table class="table table-bordered table-striped">
   
    <tbody>
      <tr>
        <td><strong>Sender Mobile</strong></td>
        <td>{{$root.transSummary.senderMobile}}</td>
          <td><strong>Beneficiary Name</strong></td>
        <td>{{$root.transSummary.beneficiaryName}}</td>
      </tr>
   
     
    
       <tr>
          <td><strong>Beneficiary IFSC</strong></td>
        <td>{{$root.transSummary.beneficiaryIFSC}}</td>
          <td><strong>Beneficiary Account No.</strong></td>
        <td>{{$root.transSummary.beneficiaryAccNo}}</td>
     </tr>
        <tr>
         <td><strong>Amount</strong></td>
        <td>{{$root.transSummary.amount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
          <td><strong>Charges</strong></td>
        <td>{{$root.transSummary.charges}}</td>
   
      </tr>
        <tr>
      <tr>
         <td><strong>Transaction Mode</strong></td>
        <td>{{$root.transSummary.mode}}</td>
          <td>&nbsp;</td>
        <td>&nbsp;</td>
     </tr>
       
  </table>
    <table class="table table-bordered table-striped">
   <thead>
   		<tr>
   			<!--   <th><strong>Beneficiary Account No.</strong></th> -->
        <th>Transaction Number</th>
   			<th>Transaction Type</th>
   			<th>Credit Amount</th>
   			<th>Debit Amount</th>
   			<th>Status</th>
   			<th>Remark</th>
   		</tr>
   
   </thead>
    <tbody>
      <tr data-ng-repeat="mudraTrans in $root.transSummary.mudraMoneyTransactionBean | orderBy:'-'">
      <!--   <td>{{transSummary.beneficiaryAccNo}}</td> -->
        <td>{{mudraTrans.id}}</td>
        <td>{{mudraTrans.narrartion}}</td>
         <td>{{mudraTrans.crAmount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
         <td>{{mudraTrans.drAmount | currency: $root.loginUserData.countrycurrency + "  "}}</td>
        <td><strong>{{mudraTrans.status}}</strong></td>
          <td>{{mudraTrans.remark}}</td>
      </tr>  
    
     
       
  </table>

<div class="alert alert-success">
  Your transaction has been completed. Reference Number is <strong> {{$root.transSummary.mudraMoneyTransactionBean[0].txnId}}.</strong> Thanks for using !  
</div>
</div>
 <div class="modal-footer">
 
          <button class="btn-primary pull-right btn" onclick="printContent('#transSummary')">Print</button>
 </div>
 </div>
 <style>


 

 
 
 @media print {
    .btn-primary.pull-right.btn, .mainRow, header, footer, .modal-header {
       display:none!important
    }
    #transLogo{display:block!important}
    .modal-content{
    box-shadow: none!important;
    border: none;
    }
    .table > thead > tr > th {
    background: #f36d25;
    color: #000 !important;
    font-size:24px;
}
    table-bordered>tbody>tr>td{
    	font-size:29px!important
    }
     table th{
    	font-size:16px!important;
    	color:#000;
    }
}
 
 @page {
    size: auto;   /* auto is the initial value */
    font-size:29px!important;
    margin: 0;  /* this affects the margin in the printer settings */
}
 
 
 </style>
 
 