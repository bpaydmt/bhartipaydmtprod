<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<jsp:include page="/jsp/reqFiles.jsp"></jsp:include>
<jsp:include page="/jsp/gridJs.jsp"></jsp:include>
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String  theams=mapResult.get("themes");

String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");

String version = "?i=1000tuesday20191";
String sessionid=(String)session.getAttribute("sessionid");

%>

<!DOCTYPE html>
<html>
<head>
<title><%=mapResult.get("caption") %></title>
 <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<%=mapResult.get("caption") %>">
    <meta name="author" content="<%=mapResult.get("caption") %>">   
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />

<link rel="shortcut icon" href="jspbk/assets/img/<%=favicon%>"> 
<!-- <link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css"> --> 
<!-- <link rel="stylesheet" type="text/css" href="jspbk/mainStyle.css"> -->
 
<!-- <link href="./css/datepicker.min.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<link href="jspbk/assets/cssLibs/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jspbk/assets/jsLibs/angular.min.js"></script>
<script type="text/javascript"  src="jspbk/assets/jsLibs/angular-route.js"></script>
<!-- <script type="text/javascript"  src="jspbk/assets/jsLibs/jquery-3.1.1.min.js"></script> -->
<script type="text/javascript"  src="jspbk/assets/jsLibs/angu-fixed-header-table.js"></script>
<script  type="text/javascript" src="jspbk/assets/jsLibs/dirPagination.js"></script>

<!-- <script type="text/javascript" src="jspbk/assets/jsLibs/pdfmake.min.js"></script>
<script type="text/javascript" src="jspbk/assets/jsLibs/vfs_fonts.js"></script> -->
<script type="text/javascript" src="jspbk/assets/cssLibs/bootstrap-3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="jspbk/app/dmt.module.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/dmt.routes.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/services/appServices.js<%=version %>"></script>  
<script type="text/javascript" src="jspbk/app/components/newUser/senderFormCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/error/errorMsgCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/addBeneficiary/addBeneficiaryCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/regUser/regUserCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/components/importSender/importSenderCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/components/favGrid/favListCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/reports/reportCtrl.js<%=version %>"></script> 

<!-- <script  type="text/javascript" src="./js/datepicker.js"></script>
<script  type="text/javascript"  src="./js/datepicker.en.js"></script> -->

<script  type="text/javascript"  src="jspbk/assets/jsLibs/Blob.min.js"></script>
<script  type="text/javascript" src="jspbk/assets/jsLibs/FileSaver.min.js"></script>
<script  type="text/javascript"  src="jspbk/assets/jsLibs/tableexport.min.js"></script>
<script  type="text/javascript"  src="jspbk/assets/jsLibs/alasql.min.js"></script>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.7/xlsx.core.min.js"></script>
<script type="text/javascript" src="jspbk/assets/jsLibs/chosen.jquery.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >
<script src="/js/Newtheme-column-visibility.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/newtheme.css" /> 

<style>
#reportrange { 
    width: 30% !important; ;
}
</style>

<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>

<script type="text/javascript"> 
    $(document).ready(function() {
    	
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 360 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#dpStart').val(start.format('DD-MMM-YYYY'));
         $('#dpEnd').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
   }); 
</script>


<script type="text/javascript"> 
    $(document).ready(function() { 
        
    $('#ta-data').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	
</script>


 <style> #bankNames_chosen{width:100%!important}</style>

<style type="text/css">
	
	.smart-style-3 .nav>li>a:focus, .smart-style-3 .nav>li>a { 
	    border-color: rgba(255, 255, 225, .15);
	    color: #666!important;
	}
	
	.form-control[readonly] {
       background-color: transparent!important;
	}
</style>

</head>
<body data-ng-app="oxyModule" >
	
	 <!-- topbar starts -->
<jsp:include page="/jsp/header.jsp"></jsp:include>
    <!-- topbar ends -->        
        <!-- left menu starts -->
<jsp:include page="/jsp/mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->


<div id="main" role="main"> 
    <div id="content">  
		<div class=" row"> 
			<div class="" data-ng-controller="mainCtrl as main" data-ng-init="main.getTheUserData()"  data-ng-click="main.getAgentSenderBalance()"> 
				<div class="container midSection" >  
				    <div  class="container-fluid" >
					    <div class="grid-box row"> 
							<div class="box-inner col-md-12" >

							    <div class="box-header ng-binding"> 
							        Reports 
							        <a class="pull-right" href="#/">Sender Portal</a>
							    </div> 

								<div class="row">
									<div class="col-md-12"> 
										<div class="panel with-nav-tabs panel-default">

											<div class="panel-heading">
											
												
											
											    <ul id="" class="ledger-Buttons nav nav-pills">
												    <li class="active" style="background-color: #337ab7; border-top: none!important; border-left: none!important; border-right: none!important; border-bottom: 1px solid #ccc; background-color: transparent!important; color: #666!important; border-radius: 25px;">
												    	<a data-toggle="pill" href="javascript:void(0)" data-target="#transactionReport" style="background-color: transparent!important;">Transaction Ledger</a>
												    </li>
												   <!--  <li style="background-color: #337ab7; border-top: none!important; border-left: none!important; border-right: none!important; border-bottom: 1px solid #ccc; background-color: transparent!important; color: #666!important; border-radius: 25px;">
												    	<a data-toggle="pill"   href="javascript:void(0)" data-target="#agentLedger" style="background-color: transparent!important;">Agent Ledger</a>
												    </li>
												    <li style="background-color: #337ab7; border-top: none!important; border-left: none!important; border-right: none!important; border-bottom: 1px solid #ccc; background-color: transparent!important; color: #666!important; border-radius: 25px;">
												     	<a data-toggle="pill"   href="javascript:void(0)" data-target="#senderLedger"  style="background-color: transparent!important;">Sender Ledger</a>
												    </li> --> 
										        </ul>
										    </div>
											<div class="panel-body"> 
											    <div class="tab-content"  >
											    
											    	<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
														    <i class="fa fa-calendar"></i>&nbsp;
														    <span></span> <i class="fa fa-caret-down"></i>
													</div>
											    
											        <div data-ng-include="'jspbk/app/components/reports/transactionLedger.html'+$root.version "  id="transactionReport" class="tab-pane fade in active"></div> 
											        <!-- <div data-ng-include="'jspbk/app/components/reports/agentLedger.html'+$root.version" id="agentLedger" class="tab-pane fade"></div> 
											        <div data-ng-include="'jspbk/app/components/reports/senderLedger.html'+$root.version" id="senderLedger" class="tab-pane fade"></div> --> 
											    </div> 
											</div> 
									    </div>
								    </div>
								</div> 

							</div>
					    </div> 
				    </div>
				 </div> 
			</div> 
		</div>  
    </div>
</div>

<script type="text/javascript" type="text/javascript">
    $(function(){
	SESSIONID = '<%=sessionid %>' 
		$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			
			if(data.sessionId != SESSIONID){

				window.close();
				window.location.replace("/BhartiPay/UserHome");
			}
		 })
		 })
		 
		})
		window.onpopstate = function (e) { window.history.forward(1); } 
</script>

<jsp:include page="/jsp/footer.jsp"></jsp:include>
</body>
</html> 