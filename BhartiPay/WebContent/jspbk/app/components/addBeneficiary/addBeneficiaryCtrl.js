dmt.controller("addBeneficiaryCtrl",function($http,$rootScope,$timeout){
		var ab = this;
		ab.bene = ab.beneForm;
		ab.error = false;
		ab.loading = false;
		ab.dropOption;
	
		$rootScope.beneIfscInvalid = false;
		$rootScope.beneSelectedBank;
		
		ab.btnText = "Submit";
		$rootScope.Benefields = {};
		$rootScope.beneSelectedBank;
		
		
		
		//vm.bankDetails = "";
		ab.refreshTab = function(){		
			//alert(1)
			$rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
			//$('[data-toggle="tooltip"]').tooltip();
			
			$("#bank-prefilled").hide()
	};
	
	ab.searchIfcsbyBankName = function(){
		//alert(2)
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	   if(res.data.status == "TRUE"){
		   
		   var bankName = $("#bankNames").val().toUpperCase();
		   var acc = $("#accountNoBene").val();



switch(bankName){
	   case "STATE BANK OF INDIA":
		   $rootScope.Benefields.ifscCode = "SBIN0001537";	
		   ab.searchIfscCode("autoFill");				   
		   break;	
	   case "CITI BANK":
		   $rootScope.Benefields.ifscCode = "CITI0000004";	
		   ab.searchIfscCode("autoFill");				   
		   break;
	   case "AHEMDABAD MERCANTILE CO OP BANK LTD":
		   $rootScope.Benefields.ifscCode = "AMCB0660016";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "ALLAHABAD BANK":
		   $rootScope.Benefields.ifscCode = "ALLA0211975";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "AMBERNATH JAIHIND CO OP BANK LTD":
		   $rootScope.Benefields.ifscCode = "ICIC00AJHCB";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "AXIS BANK":
		   $rootScope.Benefields.ifscCode = "UTIB0000248";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "BANK OF BARODA UP GRAMIN BANK":
		   $rootScope.Benefields.ifscCode = "BARB0BUPGBX";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "BARODA RAJASTHAN GRAMIN BANK":
		   $rootScope.Benefields.ifscCode = "BARB0BRGBXX";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "CENTRAL BANK OF INDIA":
		   $rootScope.Benefields.ifscCode = "CBIN0282643";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "ELLAQUAI DEHATI BANK RRB":
		   $rootScope.Benefields.ifscCode = "SBIN0RRELGB";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "IDBI BANK":
		   $rootScope.Benefields.ifscCode = "IBKL0000039";
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "INDIAN BANK":
		   $rootScope.Benefields.ifscCode = "IDIB000C128";
		   ab.searchIfscCode("autoFill");
		   break;
		   case "KASI GOMTI BANK":
		   $rootScope.Benefields.ifscCode = "UBIN0RRBKGS";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "KOTTAYAM CO-OPRATIVE URBAN BANK LTD":
		   $rootScope.Benefields.ifscCode = "lBKL0027K01";
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "MADHYA BIHAR GRAMIN BANK":
		   $rootScope.Benefields.ifscCode = "PUNB0MBGB06";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "MAHARASHTRA GRAMIN BANK":
		   $rootScope.Benefields.ifscCode = "MAHB0RRBMGB";
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "NAGALAND RURAL BANK RRB":
		   $rootScope.Benefields.ifscCode = "SBIN0RRNLGB";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "PURVANCHAL GRAMIN BANK":
		   $rootScope.Benefields.ifscCode = "SBIN0RRPUGB";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "SHAHADA PEOPLE CO OP BANK SHIRPUR":
		   $rootScope.Benefields.ifscCode = "KKBK0SPCB01";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "THE BANKI CENTRAL CO OP BANK LTD":
		   $rootScope.Benefields.ifscCode = "YESB0BNKCCB";	
		   ab.searchIfscCode("autoFill");
		   break;
		   case "THE BERHAMPORE CO OPRATIVE CENTRAL BANK":
		   $rootScope.Benefields.ifscCode = "IBKL0216BCB";
		   ab.searchIfscCode("autoFill");
		   break;

		   case "THE BHAWANI PATNA CENTRAL CO OPRATIVE BANK":
		   $rootScope.Benefields.ifscCode = "ICIC00BHCCB";	
		   ab.searchIfscCode("autoFill");
		   break;
		   case "THE COSMOS CO OPRATIVE BANK LTD":
		   $rootScope.Benefields.ifscCode = "COSB0000019";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "THE VARACHHA COOPERATIVE BANK LIMITED":
		   $rootScope.Benefields.ifscCode = "VARA0000001";
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "UDAYPUR MAHILA URBAN CO OPRATIVE BANK LTD":
		   $rootScope.Benefields.ifscCode = "YESB0MSB004";	
		   ab.searchIfscCode("autoFill");
		   break;	
		   case "UNION BANK OF INDIA":
		   $rootScope.Benefields.ifscCode = "UBIN0551945";	
		   ab.searchIfscCode("autoFill");
		   break;
		   case "RATNAKAR BANK":
			   $rootScope.Benefields.ifscCode = "RATN0000001";	
			   ab.searchIfscCode("autoFill");
			   break;			   

	 case "PURVANCHAL BANK":
			   $rootScope.Benefields.ifscCode = "SBIN0RRPUGB";	
			   ab.searchIfscCode("autoFill");
			   break;
	 case "SARASWAT COOPERATIVE BANK LIMITED":
			   $rootScope.Benefields.ifscCode = "SRCB0000001";	
			   ab.searchIfscCode("autoFill");
			   break;
	 case "BARODA UTTAR PRADESH GRAMIN BANK":
			   $rootScope.Benefields.ifscCode = "BARB0BUPGBX";	
			   ab.searchIfscCode("autoFill");
			   break;	

	 case "BANK OF BARODA":
			   $rootScope.Benefields.ifscCode = "BARB0COLABA";	
			   ab.searchIfscCode("autoFill");
			   break;			   

	 case "HDFC BANK":
			   $rootScope.Benefields.ifscCode = "HDFC0000240";	
			   ab.searchIfscCode("autoFill");
			   break;
	 case "IDBI BANK":
			   $rootScope.Benefields.ifscCode = "IBKL0000039";	
			   ab.searchIfscCode("autoFill");
			   break;
		   default:

			   if(acc.length > 2 && bankName == "KARNATAKA BANK LIMITED"){
				 var accSub = acc.substr(0, 3);
				  $rootScope.Benefields.ifscCode = "KARB0000" + accSub ;
				  ab.searchIfscCode("autoFill");
			   }else if(acc.length > 3 && bankName == "THE COSMOS CO OPERATIVE BANK LIMITED"){
					 var accSub = acc.substr(0, 3);
					  $rootScope.Benefields.ifscCode = "COSB0000" + accSub ;
					  ab.searchIfscCode("autoFill");
				 
	}else if(acc.length > 3 && bankName == "ORIENTAL BANK OF COMMERCE"){
					 var accSub = acc.substr(0, 3);
					  $rootScope.Benefields.ifscCode = "ORBC0100" + accSub ;
					  ab.searchIfscCode("autoFill");
	}else if(acc.length > 4 && bankName == "CORPORATION BANK"){
					 var accSub = acc.substr(0, 4);
					  $rootScope.Benefields.ifscCode = "CORP000" + accSub ;
					  ab.searchIfscCode("autoFill");
	}else if(acc.length > 4 && bankName == "VIJAYA BANK"){
					 var accSub = acc.substr(0, 4);
					  $rootScope.Benefields.ifscCode = "VIJB000" + accSub ;
					  ab.searchIfscCode("autoFill");
	}else if(acc.length > 4 && bankName == "STANDARD CHARTERED BANK"){
					 var accSub = acc.substr(0, 4);
					  $rootScope.Benefields.ifscCode = "SCBL000" + accSub ;
					  ab.searchIfscCode("autoFill");
			   }else if(acc.length > 5 && bankName == "PUNJAB NATIONAL BANK"){
				 var accSub = acc.substr(0, 6);
				  $rootScope.Benefields.ifscCode = "PUNB0" + accSub ;
				  ab.searchIfscCode("autoFill");
			 }else if( bankName == "MADHYA BIHAR GRAMIN BANK"){
				
				    $rootScope.Benefields.ifscCode = "PUNB0MBGB06";
				     ab.searchIfscCode("autoFill");
				   //  var gramin = bankName.toLowerCase().search("gramin");
				   }else if( bankName == "UTTAR BIHAR GRAMIN BANK MUZAFFARPUR"){
				    $rootScope.Benefields.ifscCode = "CBIN0R10001";
				     ab.searchIfscCode("autoFill");
				   }
				   else if(acc.length > 3 && bankName != "KARNATAKA BANK LIMITED"  && bankName != "PUNJAB NATIONAL BANK"){				   
						 
					   var accSub = acc.substr(0, 4);
				  
				   
				   switch(bankName){
					
				   case "ANDHRA BANK":
					   $rootScope.Benefields.ifscCode = "ANDB000" + accSub;
					  
					   break;
				   case "BANK OF INDIA":
					   $rootScope.Benefields.ifscCode = "BKID000" + accSub;			  
					   break;
				   case "CANARA BANK":
					   $rootScope.Benefields.ifscCode = "CNRB000" + accSub;			 
					   break;
				   case "CATHOLIC SYRIAN BANK LIMITED":
					   $rootScope.Benefields.ifscCode = "CSBK000" + accSub;			 
					   break;
				  
				   case "CORPORATION BANK":
					   $rootScope.Benefields.ifscCode = "CORP000" + accSub;			 
					   break;
				   case "DENA BANK":
					   $rootScope.Benefields.ifscCode = "BKDN000" + accSub;			 
					   break;
				   case "FEDERAL BANK":
					   $rootScope.Benefields.ifscCode = "FDRL000" + accSub;			 
					   break;		  
				   case "ICICI BANK LIMITED":
					   $rootScope.Benefields.ifscCode = "ICIC000" + accSub;			 
					   break;			   
				   case "INDIAN OVERSEAS BANK":
					   $rootScope.Benefields.ifscCode = "IOBA000" + accSub;			 
					   break;
				   case "KARUR VYSYA BANK":
					   $rootScope.Benefields.ifscCode = "KVBL000" + accSub;			 
					   break;
				   case "KOTAK MAHINDRA BANK LIMITED":
					   $rootScope.Benefields.ifscCode = "KKVK000" + accSub;	
				   case "LAXMI VILAS BANK":
					   $rootScope.Benefields.ifscCode = "LAVB000" + accSub;	
					   break;
				   case "ORIENTAL BANK OF COMMERCE":
					   $rootScope.Benefields.ifscCode = "ORBC010" + accSub;	
					   break;
				   case "SOUTH INDIAN BANK":
					   $rootScope.Benefields.ifscCode = "SIBL000" + accSub;	
					   break;
				   case "SYNDICATE BANK":
					   $rootScope.Benefields.ifscCode = "SYNB000" + accSub ;	
					   break;
				   case "UCO BANK":
					   $rootScope.Benefields.ifscCode = "UCBA000" + accSub ;	
					   break;
				   case "VIJAYA BANK":
					   $rootScope.Benefields.ifscCode = "VIJB000" + accSub ;	
				   case "YES BANK":
					   $rootScope.Benefields.ifscCode = "YESB000" + accSub ;
					   break;
			
				   break;				   
					   default:
						   return false;				   
				   
				   }
				   ab.searchIfscCode("autoFill");
				 }
				   
				   
				   else{
					  
					   $rootScope.Benefields.branchName = null;
						$rootScope.Benefields.ifscCode = null;
						$rootScope.Benefields.city = null;
						$rootScope.Benefields.state = null;
						$rootScope.Benefields.address = null;
					
						$("#bank-prefilled").hide(); 
						
				   }				   
	   
	   }
			
	
		   
		   	
		   	
		   	
		   
		   
		   
	   }
	       });	
	}
		ab.initBeneForm = function(){		
			//$rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
		//	$('[data-toggle="tooltip"]').tooltip();
			
		
					$http.get('GetBankDtlBK').then(function(res){
						//var defaultOption = {bankName:"Select Your Bank",id:-1}
						ab.dropOption = res.data;
						
						$rootScope.beneSelectedBank = 	ab.dropOption[0].id;
						//ab.dropOption.unshift(defaultOption)
						setTimeout(function(){
							$('#bankNames').chosen({search_contains: true,})
							
							$('#bankNames').on('change',function(e, params){
								console.log(params.selected)
									ab.searchIfcsbyBankName()						 // params.selected and params.deselected will now contain the values of the
								 // or deselected elements.
								});
							},1500);
						
					})
				
			
			//$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
	};
		ab.searchIfcs= false;
		ab.addBenficiary = function(){
			
				  var beneForm = $("#add-beneficiary-form")
				  
				 if(beneForm.is(':hidden')){
					 
					 beneForm.slideDown()
				 }else{
					 beneForm.slideUp()
				 }
				$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
				  ab.initBeneForm();  
		   }
		ab.searchIfscCode = function(medium){	
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		   if(res.data.status == "TRUE"){
			if(medium == "button"){
				
			
			$rootScope.beneSelectedBank =  $("#bankNames").val();
			console.log($rootScope.beneSelectedBank)
			$rootScope.modelBoxContent = "jspbk/app/components/loading/loading.html";
			$('#myModal').modal('show');
			$timeout(ab.openIfscPopup, 100);
			
			}else{
				if(ab.beneForm.beneIfsc.$valid){
					$rootScope.beneIfscInvalid  = false;
					$http({
					     method: 'POST',
					     url: 'GetBankDetailsByIFSCBK',
					     data :'data='+ JSON.stringify({"ifscCode":$rootScope.Benefields.ifscCode}), //forms user object
					     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
					   }).then(function(res){
						   
						   if(res.data.length > 0){
								var bankD = res.data[0]
								
								console.log(bankD)
									
								$rootScope.Benefields.bankName = bankD.bankName;
								$rootScope.Benefields.branchName = bankD.branchName;
								$rootScope.Benefields.ifscCode = bankD.ifscCode;
								$rootScope.Benefields.city = bankD.city;
								$rootScope.Benefields.state = bankD.state;
								$rootScope.Benefields.address = bankD.address;
								$("#bankNames").val($rootScope.Benefields.bankName)
								$("#bank-prefilled").show(); 
								 $('#bankNames').trigger("chosen:updated");
						   }else{
							   $rootScope.Benefields.branchName = "";
								
								$rootScope.Benefields.city = "";
								$rootScope.Benefields.state ="";
								$rootScope.Benefields.address = "";
								//$("#bankNames").val($rootScope.Benefields.bankName)
								$("#bank-prefilled").hide(); 
								$rootScope.beneIfscInvalid = true;
							}
						   
						
										
					
					
					//ifsc.bankDetailGrid = res.data;
					
					
					
				},function(res){
					console.log(res);
				
					})
					
				}else{
					$rootScope.beneIfscInvalid = true;
				}
		
			}
		
			
		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });
	
		
    		
		}
	
		ab.openIfscPopup = function(){
			 $rootScope.modelBoxContent = "jspbk/app/components/addBeneficiary/seachIfscCode.html" + $rootScope.version;

			 setTimeout(function(){
				 $('#bankNamePopUp').focus();
			 },200)
		}
		
		ab.addBeneficiary = function(){
			ab.loading = true;
			ab.btnText = "Loading..";
			$http.get('CheckSession')
		       .then(function(res){ 
		    	  // alert(2)
		    	   console.log(res)
		    	   if(res.data.status == "TRUE"){
			$rootScope.Benefields['transferType'] = $("#addBeneFormMode .active").text();			
			$rootScope.Benefields['branchName'] = $("#senderBranch").text();
			$rootScope.Benefields['city'] = $("#senderCity").text();
			$rootScope.Benefields['state']= $("#senderState").text();
			$rootScope.Benefields['address'] = $("#senderAddress").text();
			$rootScope.Benefields['verifyBene'] = $rootScope.Benefields['verifyBeneCheckBox'] == true ? "true" : "false";
			console.log($rootScope.Benefields['verifyBene'])
			
			$http({
				
			     method: 'POST',
			     url: 'RegisterBeneficiaryBK',
			     data :'data='+ JSON.stringify($rootScope.Benefields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				console.log(res)
				ab.loading = false;
				ab.btnText = "Submit";
				if(res.data.details != undefined){
				
				var vdetails = angular.fromJson(res.data.details);
				console.log(vdetails)
				$rootScope.loginUserData.finalBalance = vdetails.agentWalletAmount;
				$rootScope.LoginSender.transferLimit  =  vdetails.senderLimit;
				}
				if(res.data.statusCode == 1000){
					$('#myModal').modal('hide');
					$("#add-beneficiary-form").collapse('hide');
					
					$rootScope.LoginSender.beneficiaryList.unshift( angular.fromJson(res.data.bene))
					
				
				
					 $rootScope.modelBoxContent = "jspbk/app/components/addBeneficiary/beneficiaryAddedSuccessMsg.html"+ $rootScope.version;
					 $('#myModal').modal('show');
					 $rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
					 
					//$("#senderBranch, #senderCity, #senderState, #senderAddress").text('');
					$("#bank-prefilled").hide();
					ab.error = false;
				}else if(res.data.result == "Y"){
					$('#myModal').modal('hide');
					$("#add-beneficiary-form").collapse('hide');
					$rootScope.LoginSender.beneficiaryList.unshift( angular.fromJson(res.data.bene))
						$rootScope.verifiedBeneDetails =  angular.fromJson(res.data);
						$rootScope.MsgShowVbPup =  false;
						 $rootScope.modelBoxContent = "jspbk/app/components/addBeneficiary/beneficiaryAddedAndVerified.html"+ $rootScope.version;
						 $('#myModal').modal('show');
						
						 $rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
						 $("#bank-prefilled").hide();
						 
					
				}else if(res.data.result == "N"){
					$('#myModal').modal('hide');
					$("#add-beneficiary-form").collapse('hide');
					$rootScope.LoginSender.beneficiaryList.unshift( angular.fromJson(res.data.bene))
					 $rootScope.modelBoxContent = "jspbk/app/components/addBeneficiary/beneficiaryAddedVarficationFailed.html"+ $rootScope.version;
					 $('#myModal').modal('show');
					 $rootScope.Benefields = {name:'',bankName:'',accountNo:'',mpin:''};
					 $("#bank-prefilled").hide();
				}else{
					ab.errorMsg = res.data.statusDesc;
					console.log(ab.error)
					console.log(ab.errorMsg)
					ab.error = true;
				}
				
			},function(res){
				   console.log(res.statusText)
			  });
		    	   } else{
		    		   alert("Your session has expired. Please login again.")
		    		   window.close();
		    	   }
		       },function(res){
		  		   console.log(res.statusText)
		  		  });

		};
		ab.getBankDetails = function(){		
			
			$http({
			     method: 'POST',
			     url: 'GetBankDetailsBK',
			     data :'data='+ JSON.stringify($rootScope.Benefields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
				   console.log(res)
				   var resData = res.data
				   	$rootScope.Benefields.branchName = resData.branchName;
					$rootScope.Benefields.ifscCode = resData.ifscCode;
					$rootScope.Benefields.city = resData.city;
					$rootScope.Benefields.state = resData.state;
					$rootScope.Benefields.address = resData.address;
				  $("#bank-prefilled").show();
				 console.log( res.data) ;
				
			},function(res){
				   console.log(res.statusText)
			  });
			
		};
	
		

		ab.ChangeTrans = function(el){
			ChangeTransMode(el);
			}
			
});
dmt.controller("getBankDetailsCtrl",function($http,$rootScope){
	var ifsc = this;
	ifsc.dropOption;

	ifsc.btnText = "Submit";
	ifsc.loading = false;
	ifsc.fields = {};
	ifsc.fields.ifscCode  = '';
	ifsc.fields.state = "-1";
	ifsc.fields.bankId = "";
	ifsc.BtnMsg = (ifsc.fields.state == "-1" &&  ifsc.fields.ifscCode =='') ? "Please select the state" : '';
	ifsc.gridData = false;
	ifsc.getDropVal = function(){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		$http.get('GetBankDtlBK',{cache: false,}).then(function(res){
		
			//var defaultOption = {bankName:"Select Your Bank",id:-1}
			ifsc.dropOption = res.data;
			//ifsc.dropOption.unshift(defaultOption)
			ifsc.dropOption = res.data;
		})
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });
	}
	ifsc.getBankList = function(){
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		ifsc.loading = true;
		ifsc.btnText = "Loading...";
		ifsc.fields.bankId = $("#bankNameIfsc").val();
		console.log($("#bankNameIfsc").val())
		console.log(ifsc.fields);
		$http({
			     method: 'POST',
			     url: 'GetBankDetailsByIFSCBK',
			     data :'data='+ JSON.stringify(ifsc.fields), //forms user object
			     headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			   }).then(function(res){
			console.log(res)
			ifsc.loading = false;
			ifsc.btnText = "Submit"
			ifsc.gridData = true;
			ifsc.bankDetailGrid = res.data;
			
			
			
		},function(res){
			console.log(res);
		})
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });
	}
	
	ifsc.returnBankDetails = function(ifscCode){
		for(var i= 0; i < ifsc.bankDetailGrid.length; i++){
			
			if(ifsc.bankDetailGrid[i].ifscCode == ifscCode){
				
				return  ifsc.bankDetailGrid[i]
			}
		}
		
		
	}
	ifsc.getSelectedBank = function(ifscCode){
		console.log(ifscCode)
		console.log(ifsc.returnBankDetails(ifscCode))
		
		var bankD = ifsc.returnBankDetails(ifscCode)
	
		
			
		$rootScope.Benefields.bankName = bankD.bankName;
		$rootScope.Benefields.branchName = bankD.branchName;
		$rootScope.Benefields.ifscCode = bankD.ifscCode;
		$rootScope.Benefields.city = bankD.city;
		$rootScope.Benefields.state = bankD.state;
		$rootScope.Benefields.address = bankD.address;
		$("#bankNames").val($rootScope.Benefields.bankName)
		$rootScope.beneIfscInvalid = false;
		$("#bank-prefilled").show();
		$('#myModal').modal('hide');
	}
	
});
function ChangeTransMode(el){
	if(!$(el).hasClass("active") && !$(el).hasClass("isImpsFalse")){
	
		$(el).addClass("active")
		$(el).siblings().removeClass("active");
	}
	
}

dmt.controller("variBene",function($http,$rootScope){
	
	var vb  = this;

	$rootScope.MsgShowVbPup =  false;
	vb.msgClass = null;
	vb.acceptVari =  function(){
		$http.get('AcceptBen').then(function(res){
			console.log(res)
			if(res.data.statusCode == "1000"){
			
				vb.msgClass = "alert-success";
				$rootScope.LoginSender.beneficiaryList = res.data.list
			}else{
				vb.msgClass = "alert-danger";
			}
			vb.Msg = res.data.statusDesc;
			$rootScope.MsgShowVbPup =  true;
		})
	}
	vb.reject =  function(){
		$http.get('RejectBen').then(function(res){
			console.log(res)
			if(res.data.statusCode == "1000"){
				
				vb.msgClass = "alert-success";
				//$rootScope.LoginSender.beneficiaryList = res.data.list
			}else{
				vb.msgClass = "alert-danger";
			}
			vb.Msg = res.data.statusDesc;
			$rootScope.MsgShowVbPup =  true;
		})
		
	}
})
