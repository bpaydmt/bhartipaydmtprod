// JavaScript Document
// JavaScript Document
function unloadJS(index) {
  var head = document.getElementsByTagName('head').item(0);
  var js = document.getElementById(scriptName);
  js.parentNode.removeChild(js);
}
 
function unloadAllJS() {
  var jsArray = new Array();
  jsArray = document.getElementsByTagName('script');
  for (i = 0; i < jsArray.length; i++){
    if (jsArray[i].id){
      unloadJS(jsArray[i].id)
    }else{
      jsArray[i].parentNode.removeChild(jsArray[i]);
    }
  }       
}
jQuery.fn.ForceNumericOnly = function() {
	return this.each(function() {
		$(this).keydown(function(e) {
			var key = e.charCode || e.keyCode || 0 || e.KeyCode == 16;
			// allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
			// home, end, period, and numpad decimal
			return (
				key == 8 ||
				key == 9 ||	
				key == 13 ||
				key == 16 ||
				key == 46 ||
				key == 110 ||
				key == 190 ||
				(key >= 35 && key <= 40) ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
		});
	});
};


jQuery(document).ready(function (){
	$("#mobile_no").ForceNumericOnly(); 
	$("#amount").ForceNumericOnly(); 
	$("#dth_mobile_no").ForceNumericOnly(); 
	$("#dth_amount").ForceNumericOnly();
	$("#datacard_mobile").ForceNumericOnly();
	$("#datacard_amount").ForceNumericOnly();
	
	
	$("#open_plans").click(function(){
		$("#payplutus_popup").show();
	});
	$("#close_popup").click(function(){
		$("#payplutus_popup").hide();
	});
	
	$("#login_popup").click(function(){
		$("#payplutus_login").show();
	});
	$("#login_popup1").click(function(){
		$("#payplutus_login").show();
	});
	$("#login_popup2").click(function(){
		$("#payplutus_login").show();
	});
	$("#close_login_popup").click(function(){
		$("#payplutus_login").hide();
	});
	$("#register_user").click(function(){
		$("#registration_box").show(300);
		$("#login_box").hide(300);
		$("#forgot_box").hide(300);
	});
	$("#login_user").click(function(){
		$("#login_box").show(300);
		$("#registration_box").hide(300);
		$("#forgot_box").hide(300);
	});
	$("#forgot_link").click(function(){
		$("#forgot_box").show(300);
		$("#login_box").hide(300);
		$("#registration_box").hide(300);
	});
	$("#login_user_2").click(function(){
		$("#login_box").show(300);
		$("#registration_box").hide(300);
		$("#forgot_box").hide(300);
	});
	
	var currentLoc = window.location.href;
    str = currentLoc.split("#")[1];
	
	// RECHARGE OPTION 
    if (str == "mobileRecharge") { mobile_Recharge(); }
    else if (str == "dthRecharge") { dth_Recharge(); }
    else if (str == "datacardRecharge") { datacard_Recharge(); }
    else if (str == "landlinebill") { datacard_Recharge(); }
	else if (str == "talktime") { talktime1(); }
	else if (str == "sms") { sms1(); }
	else if (str == "twog") { twog1(); }
	else if (str == "threeg") { threeg1(); }
	else if (str == "local") { local1(); }
	else if (str == "std") { std1(); }
	else if (str == "isd") { isd1(); }
	else if (str == "other") { other1(); }
	$("#mobile_recharge").click(function(){ mobile_Recharge(); });
	$("#dth_recharge").click(function(){ dth_Recharge(); });
	$("#datacard_recharge").click(function(){ datacard_Recharge(); });
	$("#landline_Recharge").click(function(){ landline_Recharge(); });
	
	function mobile_Recharge() {
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$("#landline_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
	}
	function dth_Recharge() {
		console.log("cll fun");
		$("#mobile_recharge_form").hide();
		$("#d2h_recharge_form").show();
		$("#datacard_recharge_form").hide();
		$("#landline_recharge_form").hide();
		$( "#dh" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#mb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dh" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
	}
	function datacard_Recharge() {
		$("#mobile_recharge_form").hide();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").show();
		$("#landline_recharge_form").hide();
		$( "#dc" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
	}
	function landline_Recharge() {
		$("#mobile_recharge_form").hide();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$("#landline_recharge_form").show();
		$( "#lb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
	}
	
	
	// RECHARGE PLAN POPUP BOX OPTIONS
	
	$("#m_talktime").click(function(){ talktime1(); });
	$("#m_sms").click(function(){ sms1(); });
	$("#m_twog").click(function(){ twog1(); });
	$("#m_threeg").click(function(){ threeg1(); });
	$("#m_local").click(function(){ local1(); });
	$("#m_std").click(function(){ std1(); });
	$("#m_isd").click(function(){ isd1(); });
	$("#m_other").click(function(){ other1(); });
	// RECHARGE PLAN POPUP BOX OPTIONS FUNCTION
	
	function talktime1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$("#landline_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").show();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_talktime" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	function sms1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#lb" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").show();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_sms" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function twog1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").show();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_twog" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function threeg1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").show();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_threeg" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function local1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").show();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_local" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function std1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").show();
		$("#isd_plan").hide();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_std" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function isd1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").show();
		$("#other_plan").hide();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_isd" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
		
		$( "#other_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_other" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
	}
	
	function other1() {
		// ALWAYS SHOW MOBILE RECHARGE BOX
		$("#mobile_recharge_form").show();
		$("#d2h_recharge_form").hide();
		$("#datacard_recharge_form").hide();
		$( "#mb" ).removeClass( "payplutus_tab" ).addClass( "payplutus_tab_active" );
		$( "#dh" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#dc" ).addClass( "payplutus_tab" ).removeClass("payplutus_tab_active");
		$( "#mb" ).addClass( "payplutus_tab_active" ).removeClass("payplutus_tab");
		// POPUP OPTION SHOW
		$("#topup_plan").hide();
		$("#sms_plan").hide();
		$("#twog_plan").hide();
		$("#threeg_plan").hide();
		$("#local_plan").hide();
		$("#std_plan").hide();
		$("#isd_plan").hide();
		$("#other_plan").show();
		// ADD REMOVE CLASS ON PLAN OPTIONS
		$( "#talktime_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_talktime" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#sms_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_sms" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#twog_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_twog" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#threeg_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_threeg" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#local_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_local" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#std_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_std" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#isd_btn" ).removeClass( "payplutus_plan_tab_active" ).addClass("payplutus_plan_tab");
		$( "#m_isd" ).removeClass( "plan_tab_active" ).addClass("plan_tab");
		
		$( "#other_btn" ).addClass( "payplutus_plan_tab_active" ).removeClass("payplutus_plan_tab");
		$( "#m_other" ).addClass( "plan_tab_active" ).removeClass("plan_tab");
	}
	
});



// FUNCTION FOR CHECK OPERATOR

//function check_operator(){
//	var mobile_no = $("#mobile_no").val();
//	var mobile = $("#mobile_no").val().length;
//	if(mobile == 5){
//		$.ajax({
//			url: "digit.jsp",
//			type: "GET",
//			dataType: 'json',
//			data: {mobile_no: mobile_no},
//			success: function(response){
//				$("#operator")
//					.prepend($("<option selected></option>")
//         			.attr("value",response.val1)
//         			.text(response.val1));
//				$("#circle").prepend($("<option selected></option>")
//         			.attr("value",response.val2)
//         			.text(response.val2));
//				
//				$("#payplutus_popup").show(500);				
//				$("#pop_operator").attr("value",response.val1);
//				$("#pop_circle").attr("value",response.val2);
//				$("#oeprator_bold").html(response.val1);
//			    $("#circle_bold").html(response.val2);
//				show_popup_data();
//			}			
//		});
//	}
//}

function check_operator(){
	 var mobile_no = $("#mobile_no").val();
	 var mobile = $("#mobile_no").val().length;
	 var recharge_type = $('input[type="radio"]:checked').val(); 
	 if(mobile == 5){
	  //alert(recharge_type);
	  if(recharge_type === "Prepaid-Mobile"){
	   //alert(recharge_type);
		  $.ajax({
				url: "digit.jsp",
				type: "GET",
				dataType: 'json',
				data: {mobile_no: mobile_no},
				success: function(response){
					$("#operator")
						.prepend($("<option selected></option>")
	         			.attr("value",response.val1)
	         			.text(response.val1));
					$("#circle").prepend($("<option selected></option>")
	         			.attr("value",response.val2)
	         			.text(response.val2));
					
					$("#payplutus_popup").show(500);				
					$("#pop_operator").attr("value",response.val1);
					$("#pop_circle").attr("value",response.val2);
					$("#oeprator_bold").html(response.val1);
				    $("#circle_bold").html(response.val2);
					show_popup_data();
				}			
			});
	  }
	  else{
		  $.ajax({
				url: "digit.jsp",
				type: "GET",
				dataType: 'json',
				data: {mobile_no: mobile_no},
				success: function(response){
					$("#operator")
						.prepend($("<option selected></option>")
	         			.attr("value",response.val1)
	         			.text(response.val1));
					$("#circle").prepend($("<option selected></option>")
	         			.attr("value",response.val2)
	         			.text(response.val2));
					
									
					$("#pop_operator").attr("value",response.val1);
					$("#pop_circle").attr("value",response.val2);
					$("#oeprator_bold").html(response.val1);
				    $("#circle_bold").html(response.val2);
				    $("#open_plans").show();
				    
				    $("#open_blank").hide();
					show_popup_data();
				}			
			});
	   $("#payplutus_popup").hide(500);
	   $("#open_blank").show();
	  }
	 } else {
	 }
	}


function show_popup_data(){
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	$.ajax({						
		url: "popup.jsp",
		type: "GET",
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);		
			$("#oeprator_bold").html(operator);
			$("#circle_bold").html(circle);
			
		}
	});
}
// Function for change operator

function change_operator(){
	
	alert("____Inside payplutus.js----change_Operator()_________")
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	alert("___Operator____"+operator);
	alert("___circle____"+circle)
	$("#pop_operator").attr("value",operator);
	$.ajax({						
		url: "popup.jsp",
		type: "GET",	
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);	
			$("#oeprator_bold").html(operator);
			  
		}
	});
}
function change_circle(){
	var operator = $("#operator").val();
	var circle = $("#circle").val();
	$("#pop_circle").attr("value",circle);
	$("#pop_operator").attr("value",operator);
	$.ajax({						
		url: "popup.jsp",
		type: "GET",
		data: {operator: operator, circle: circle},
		success: function(data){	
			// Open Popup Plan with operator and circle hidden value
			$("#payplutus_popup_box").html(data);	
			 $("#circle_bold").html(circle);
		}
	});
}


// Ajax Function for check email id is exist or not

function check_mail(){
	var email = $("#user-email").val();
	$.ajax({
		url : "checkMail.php",
		type : "GET",
		data : {email: email},
		success : function(response){
			$("#error").html(response);
		}
	});
}

function Check_pwd(){
	var password = $("#password").val();
	var cp_password = $("#cpassword").val();
	if(password == cp_password){
		$("#recover").submit();
	}else{
		alert("Password Does not match.");	
	}
}

function show_value(a){
	
	var va = $("#reach").val();
	$("#amount").val(a);
}

//function confirm_guest(){
//	 var email = $("#email").val();
//	 var name = $("#username").val();
//	 var trxid = $("#trxid").val();
//	 
//	 $.ajax({
//	  url: "guestUser.jsp",
//	  type: "GET",
//	  data:{email: email, name: name, trxid: trxid},
//	  success: function(data){
//		  //alert(data);
//		  
//		  $("#myForm").submit();
//	  }
//	 }); 
//	}

function confirm_guest(){
	 
	 var email = $("#email").val();
	 var name = $("#username").val();
	 var trxid = $("#trxid").val();
	 
	 if(email && name !=''){
	  $.ajax({  
	   url: "guestUser.jsp",
	   type: "GET",
	   data:{email: email, name: name, trxid: trxid},
	   success: function(data){

	    $("#myForm").submit();
	   }
	  }); 
	 }
	 else{
	  $("#error").show();
	  $("#email").focus();
	 }  
	}


function hide_plan(){
	 var recharge_type = $("input[type='radio']:checked").val();
	 if(recharge_type == "Prepaid-Mobile"){
	  $("#payplutus_popup").show(500);
	 }else{
	  $("#payplutus_popup").hide(500);
	 }
	}

function hide_plan(){
	debugger
	 var recharge_type = $('input[type="radio"]:checked').val();
	 if(recharge_type === "Prepaid-Mobile"){
	  $("#open_plans").show();
	  $("#open_blank").hide();  
	 } else {
	  $("#open_plans").hide();
	  $("#open_blank").show();
	  $("#payplutus_popup").hide(500);
	 }
	}


(function() {

    var quotes = $(".payplutus_para");
    var quoteIndex = -1;
    
    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(2000)
            .fadeOut(2000, showNextQuote);
    }
    
    showNextQuote();
    
})();



function validateAllInp(elm){
    var inp =  $(elm).val();
 // remove string if it has onload word
    var reg1 = /onload(.*?)=/i;
 //remove string if it has <script></scirpt> tags
 var reg2 = /<script>(.*?)<\/script>/i;
 //remove string if it has onload </scirpt> tag
 var reg3 =/<\/script>/i;
 // remove string if it has src= attribute
 var reg4 = /src=(.*?)/i;
 //remove string if it has <script> tag
 var reg5 = /<script(.*?)>/i;
 //remove string if it has eval() mathod
 var reg6 = /eval(\(.*?)\)/i;
 //remove string if has expression() mathod
 var reg7 = /expression(\(.*?)\)/i;
 //remove string if has javasript word
 var reg8 = /javascript:(.*?)/i;
 //remove string if has javasript word
 var reg9 = /javascript:(.*?)/i;
 //remove string if has vbscript word
 var reg10 = /vbscript:(.*?)/i;

 var reg11 = /^[a-zA-Z0-9 //.@:_-]+$/g;
	var reg12 = / (:)/i;

 if(inp.match(reg12)){
	   var chopped = inp.replace(/ (:)/g, "");   
	   $(elm).val(chopped);

	}

if(!inp.match(reg11)){
var chopped = inp.replace(/[^a-zA-Z0-9 //.@:_-]/g, "");   
$(elm).val(chopped);


}
 

    if(inp.match(reg1) || inp.match(reg2) || inp.match(reg3) 
 || inp.match(reg4) || inp.match(reg5) || inp.match(reg6) 
 || inp.match(reg7) || inp.match(reg8) || inp.match(reg9)
 || inp.match(reg10))
 {
        $(elm).val("");
    } 

 }
 $(function(){
     $("input[type='text']").keyup(function(){
          validateAllInp(this)   
     });

 })
 