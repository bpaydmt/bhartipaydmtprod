<!DOCTYPE html>
<html lang="en">
<head>
<title>BhartiPay | FAQ</title>

<jsp:include page="jsp/cssFiles.jsp"></jsp:include>

 
 
</head>
<body>
 <jsp:include page="jsp/mainHeader.jsp"></jsp:include>
	<jsp:include page="jsp/mainMenuOption.jsp"></jsp:include>
	<div class="section-wrap">
		<div class="section">
			<!-- CONTENT -->
			<div class="content center faqPage">
				<!-- POST -->
				<article class="post blog-post">
					<!-- POST IMAGE 
					<div class="post-image">
						<figure class="product-preview-image large liquid">
							<img src="images/blog/01b.jpg" alt="">
						</figure>
					</div>
					<!-- /POST IMAGE -->

					<!-- POST CONTENT -->
					<div class="post-content with-title">
						<p class="text-header big">New to BhartiPay?</p>
						<div class="meta-line">
							<a href="#">
								<p class="category primary">Home</p>
							</a>
							<!-- METADATA -->
							<div class="metadata">
								<!-- META ITEM -->
								<div class="meta-item">
									<span class="icon-bubble"></span>
									<p>05</p>
								</div>
								<!-- /META ITEM -->

								<!-- META ITEM -->
								<div class="meta-item">
									<span class="icon-eye"></span>
									<p>68</p>
								</div>
								<!-- /META ITEM -->
							</div>
							<!-- /METADATA -->
							<p>Jan 15th, 2017</p>
						</div>
						

					
				
					<!-- /POST CONTENT -->
					<div class="container-fluid">


  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      What are mobile wallets?
        </h3>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
        <p>It is your purse or wallet on your phone in the form on an app, just like WhatsApp. But here, you store money to buy things or send money to others.</p>

<p>Your smartphone is effectively a computer in your hands. You use it for social interaction, sharing information and even to conclude business deals. Why would you not use it for your financial transactions with similar ease and better security.
</p></div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse2">
      
          Why should I get a mobile wallet?
        </h3>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body"><p>
        	<h4>Eight reasons you should get a mobile wallet:</h4>
<ol>
	<li>You can recharge your phone, make purchases online or in your neighborhood store and pay bills for your electricity, gas or school directly from your account without withdrawing cash.</li>
	<li>You can send money to your dear ones using your phone. If you are short of cash, you can even request your friend to BhartiPay�� it to you.</li>
	<li>You will never need to worry about change.</li>
	<li>By using your ATM/Debit card or Credit card for small transactions, you are exposing your details to all merchant sites, whereas the wallet lets you set aside small amounts for such use, reducing your risk.</li>
	<li>If you are a small business or shop, you reduce your time spent on handling money and if you are a customer, you save time standing in various lines to first get cash and then to use that cash for making payments.</li>
	<li>It is your money in your hands 24 hours and you can check balance anytime you like.</li>
	<li>It is easier to resolve disputes about transactions because you have full details with time, date and names. You know what you did with your money.</li>
	<li>Of course, you get attractive cashback and no need to carry coupons to claim the same!</li>


</ol>









        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse3">
             
          How can I get a mobile wallet?
        </h3>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body"><p>You will need a smartphone and data or wi-fi connection. If you expect to load up to 20,000 rupees in a month, you will only need your mobile number. If you expect to load up to 1,00,000 rupees, you will need KYC documents like Aadhaar card and PAN card, but you may first want to download the wallet and use it for up to 20,000 rupees.</p></div>
      </div>
    </div>
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse5">
             
         What will I have to do?
        </h3>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body"><p>You have to download the mobile wallet app from your play/app store on your smartphone. Register it with your mobile number and password. Be careful to create your password yourself and not share your password with others.</p></div>
      </div>
    </div>
     
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse10">
             
         Is there a registration fee?
        </h3>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
        <div class="panel-body"><p>No, there is no registration fee.</p></div>
      </div>
    </div>
         <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse6">
             
         How do I load the wallet?
        </h3>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body"><p>It's Simple. Click on the Add Money tab, enter the amount, select payment mode you wish to use and enter required information. You will receive an SMS on the registered mobile number and the wallet will show the balance in passbook.</p></div>
      </div>
    </div>
          <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse7">
             
         How do I use it?
        </h3>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="panel-body"><p>Mostly, you will use your wallet to pay for something or send money to someone. You should tap ‘pay or send’ on the app. If paying to someone who has a QR code, scan the code (camera should be enabled), enter the amount, and you are done. If paying to someone using mobile number, simply enter mobile number and amount. You will receive an SMS and your passbook will show details as well as updated balance every time.</p></div>
      </div>
    </div>
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse8">
             
       Is my wallet a prepaid wallet or postpaid wallet?
        </h3>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
        <div class="panel-body"><p>Mobile wallets are prepaid wallets. You can use only what you transfer to it.</p></div>
      </div>
    </div>
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapse9">
             
       Is my money safe?
        </h3>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body"><p>Make sure you are putting your money in a wallet licensed by the reserve bank of India. The money that such companies receive from you is required under BANK regulation to be kept in a separate bank account of a scheduled commercial bank. This money cannot be used by the wallet company, but can only be released by the bank to complete your transaction following your instructions on the wallet app.</p>
        <p>Mobile wallet cannot be physically stolen or lost, unlike your purse or wallet or currency notes. A wallet provider like BhartiPay would typically spend a lot of time and money on security of your money and information. In addition, there are rules of the Reserve Bank of India and other regulators on security of transactions and data.</p>
        <p>However, you need to be as careful about your mobile wallet as you are with your purse or currency note. Here are a few helpful tips:</p>
        <ol>
        	<li>Always use a password for your phone and do not share this with anyone you do not want to handle your money. This will ensure that even if someone takes your phone, they cannot use your wallet.</li>
        	<li>Just like your money is important, your personal information is also very important. Do not leave photocopies of your important documents like Aadhaar Card or PAN Card with untrusted people. Share any personal information only if needed and with trusted people.</li>
        	<li>If your mobile is lost, call the helpline urgently, just as you would do on losing your Debit/Credit card.</li>
        	<li>Just like you would not share your ATM/Debit PIN with anyone, do not share your wallet password with anyone. If you think someone has come to know, change it.</li>
        	<li>Make a habit to check your passbook, so that you track activities in your wallet.</li>
        	<li>Do not believe in rumors. Do not panic if you suspect any unwanted activity. Call the helpline. Keep helpline number handy.</li>
        </ol>
        </div>
      </div>
    </div>
  </div> 
</div>
	</div>
					<hr class="line-separator">
				</article>
				<!-- /POST -->

				
			</div>
			<!-- CONTENT -->

		
		</div>
	</div>

<jsp:include page="jsp/subscribeNewsLetter.jsp"></jsp:include>
 <jsp:include page="jsp/footer.jsp"></jsp:include>   
</body>
</html>
