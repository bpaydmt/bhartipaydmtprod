<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%
User user = (User) session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
DecimalFormat d = new DecimalFormat("0.00");
%>

<div class="author-profile-meta-wrap">
		<div class="author-profile-meta">
			<!-- AUTHOR PROFILE INFO -->
			<div class="author-profile-info">
				<!-- AUTHOR PROFILE INFO ITEM -->
				
				<!-- /AUTHOR PROFILE INFO ITEM -->

				<!-- AUTHOR PROFILE INFO ITEM -->
				<div class="author-profile-info-item">
					<p class="text-header">Balance:</p>
					<p><%=user.getCountrycurrency()+"  "+d.format(user.getFinalBalance()) %></p>
				</div>
				<!-- /AUTHOR PROFILE INFO ITEM -->

				<!-- AUTHOR PROFILE INFO ITEM -->
				<div class="author-profile-info-item">
					<p class="text-header">Phone No.</p>
					<p><%=user.getMobileno()%></p>
				</div>
				<!-- /AUTHOR PROFILE INFO ITEM -->

				<!-- AUTHOR PROFILE INFO ITEM -->
				
				<!-- /AUTHOR PROFILE INFO ITEM -->
			</div>
			<!-- /AUTHOR PROFILE INFO -->
		</div>
	</div>