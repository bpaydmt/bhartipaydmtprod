

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.merchant.persistence.vo.PaymentParameter"%>
<%
  response.setHeader("Strict-Transport-Security", "max-age=7776000; includeSubdomains");
  response.addHeader( "X-FRAME-OPTIONS", "DENY" );
  response.setHeader("X-Frame-Options", "SAMEORIGIN");
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer(); 
 // String pgToken=  net.pg.utility.Helper.getEncodedValue(); 
  %> 
<%@taglib uri="/struts-tags" prefix="s" %>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script>

function backButtonOverride()
{
  setTimeout("backButtonOverrideBody()", 1);
}

function backButtonOverrideBody()
{
  try {
    history.forward();
  } catch (e) {
    // OK to ignore
  }
  setTimeout("backButtonOverrideBody()", 500);
}
function onLoadLoginPage(redirect){

if(redirect == false)
{        
       if(window.opener!=null){
            alert("Session Expired.  Please Login again to continue.");
            window.opener.location="Logoff";
             this.close();
           }
       }
}      
function removeSession(userName)
{
   document.Login.userId.value=userName;
   document.Login.action="RemoveSesion";
   document.Login.submit();
}


/* To prevent right click of mouse */
function right(e) {
       if (navigator.appName == 'Netscape' &&(e.which == 3 || e.which == 2))
              return false;
       else if (navigator.appName == 'Microsoft Internet Explorer' &&    (event.button == 2 || event.button == 3)) {
              alert("Mouse Right Click Disabled.");
              return false;
       }
       return true;
}

/***Disable right click ***/

 var message="Function Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")


document.onmousedown=right;
document.onmouseup=right;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
if (document.layers) window.captureEvents(Event.MOUSEUP);
window.onmousedown=right;
window.onmouseup=right;




       if(top != self)
       {
              top.location=self.location;
       }
function disableCtrlKeyCombination(e)
       {
               //list all CTRL + key combinations you want to disable
               var forbiddenKeys = new Array("a", "n", "c", "x", "v", "j");
               var key;
               var isCtrl;

               if(window.event)
               {
                       key = window.event.keyCode;     //IE
                       if(window.event.ctrlKey)
                               isCtrl = true;
                       else
                               isCtrl = false;
               }
              else
               {
                       key = e.which;     //firefox
                       if(e.ctrlKey)
                               isCtrl = true;
                       else
                               isCtrl = false;
               }

               //if ctrl is pressed check if other key is in forbidenKeys array
               if(isCtrl)
               {
                       for(i=0; i<forbiddenKeys.length; i++)
                       {
                               //case-insensitive comparation
                               if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                               {
                                       alert('Key combination CTRL + '+String.fromCharCode(key)+ 'has been disabled.');
                                       return false;
                               }
                      }
              }
               return true;
       }

function backButtonOverride()
       {
         setTimeout("backButtonOverrideBody()", 1);
       }

function backButtonOverrideBody()
       {
         try {
           history.forward();
         } catch (e) {
          // OK to ignore
         }
         setTimeout("backButtonOverrideBody()", 500);
       }      
history.forward(0);
/* To prevent right click of mouse */
function right(e) {
       if (navigator.appName == 'Netscape' &&(e.which == 3 || e.which == 2))
              return false;
       else if (navigator.appName == 'Microsoft Internet Explorer' &&    (event.button == 2 || event.button == 3)) {
              alert("Mouse Right Click Disabled.");
              return false;
       }
       return true;
}

document.onmousedown=right;
document.onmouseup=right;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
if (document.layers) window.captureEvents(Event.MOUSEUP);
window.onmousedown=right;
window.onmouseup=right;

</script>
</head>


<%
String redirect=request.getParameter("redirect");

if(redirect == null)
{
       redirect="false";
}
%>

<body onload="backButtonOverride();onLoadLoginPage(<%=redirect%>);">
        <div class="container-fluid">
        <div class="navbar-header">
            <img src="assets/img/wallet_logo.png">
        </div>
       <!--  <div class="form-top-rightl">
            <img src="assets/img/logo1.png" border="0" >
        </div> -->

    </div>
<%
    if(request.getParameter("msg")!= null)
        out.println("<center>"+request.getParameter("msg")+"</center><br><br>");
%>
        <!-- Top content -->
        <div class="top-content">
             
            <div class="inner-bg">
                <div class="container">


                    
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                          <div class="form-top">
                                 <div class="form-top-left">
                                        
                                 </div>
                                 <div class="form-top-right">
                                        <i class="fa fa-lock"></i>
                                 </div>
                            </div>
                            <div class="form-bottom">
                                     <%
                                     PaymentParameter paymentParameter=(PaymentParameter)session.getAttribute("PaymentParameter");                                    
									User user=(User)session.getAttribute("User");
			
	%>
                                               
                <table>
                 <tr>
                <td>
                <label style="color: black;">Merchant Name : </label>
               </td>
                 <td>
                   <label style="color: black;"><%=paymentParameter.getMerchantName()%> </label>
                 </td>
                </tr>
                <tr>
                <td>
                <label style="color: black;">Merchant ID : </label>
               </td>
                 <td>
                   <label style="color: black;"><%=paymentParameter.getMid()%> </label>
                 </td>
                </tr>
                   <tr>
                <td>
                <label style="color: black;">Transaction ID :</label>
               </td>
                 <td>
                   <label style="color: black;"><%=paymentParameter.getTxnId()%> </label>
                 </td>
                </tr>
                   <tr>
                <td>
                <label style="color: black;">Transaction Amount :</label>                                           
               </td>
                 <td>
                      <label style="color: black;"><%=paymentParameter.getAmount()%> </label>
                 </td>
                </tr>
                <tr>
                <td>
                <%if(user.getFinalBalance()-paymentParameter.getAmount()>=0){ %>
                <s:form action="WalletPaymentConfirm" method="POST" cssClass="login-form">
                    <button type="submit" name="Confirm" value="Login" style="color: black;">Confirm</button>
                   </s:form>
                   <%}else{ %>
                      <%-- <s:form action="WalletPaymentGateway" method="POST" cssClass="login-form"> --%>
                     <s:form action="OpenPaymentGateway" method="POST" cssClass="login-form">
                      <input type="hidden" value="<%=paymentParameter.getAmount()-user.getFinalBalance() %>" name="inputBean.trxAmount"/>
                    <button type="submit" name="Confirm" value="Login" style="color: black;">Add Money</button>
                   </s:form>
                   <%} %>
                   </td>
                 <td>
                   <s:form action="WalletPaymentCancel" method="POST" cssClass="login-form">
                    <button type="submit" name="Cancel" value="Login" style="color: black;">Cancel</button>
                   </s:form>
                 </td>
                </tr>
                </table>
                   
                   
                                       
                                  </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            
        </div>

       <jsp:include page="footer1.jsp"/>
        <!-- Javascript -->
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.backstretch.min.js"></script>
        <script src="../js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>


<head>
<meta http-equiv='Pragma' content='no-cache'/>
<meta http-equiv='Cache-Control' content='no-cache'/>
<meta http-equiv="Expires" content="-1"/>
</head>
</html>
