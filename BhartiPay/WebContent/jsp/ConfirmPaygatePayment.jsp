
<%response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %>
<%@taglib uri="/struts-tags" prefix="s" %> 
<head>

<script type = "text/javascript" >
function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
document.onkeydown = function() {    
    switch (event.keyCode) { 
        case 116 : //F5 button
            event.returnValue = false;
            event.keyCode = 0;
            return false; 
        case 82 : //R button
            if (event.ctrlKey) { 
                event.returnValue = false; 
                event.keyCode = 0;  
                return false; 
            } 
    }
}

</script>
</head>
<body onload="fnSubmit()">
      
 
 	 <form name="Ecom" method="post" action="<s:property value="#session.secureObject.targetUrl"/>"> 
	<table align="center">
		<tr><td><STRONG>Transaction is being processed,</STRONG></td></tr>
		<tr><td><font color='blue'>Please wait ...</font></td></tr>
		<tr><td>(Please do not press "Refresh" or "Back" button</td></tr>
	</table>		
<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
<input type="hidden" name="me_id" value="<s:property value="#session.secureObject.me_id"/>"/>
<input type="hidden" name="txn_details" value="<s:property value="#session.secureObject.txn_details"/>"/>
<input type="hidden" name="pg_details" value="<s:property value="#session.secureObject.pg_details"/>"/>
<input type="hidden" name="card_details" value="<s:property value="#session.secureObject.card_details"/>"/>
<input type="hidden" name="cust_details" value="<s:property value="#session.secureObject.cust_details"/>"/>
<input type="hidden" name="bill_details" value="<s:property value="#session.secureObject.bill_details"/>"/>
<input type="hidden" name="ship_details" value="<s:property value="#session.secureObject.ship_details"/>"/>
<input type="hidden" name="item_details" value="<s:property value="#session.secureObject.item_details"/>"/>
<input type="hidden" name="other_details" value="<s:property value="#session.secureObject.other_details"/>"/>
		
	</form>
</body>
<script >

function fnSubmit(){
	document.Ecom.submit();
}



/* To prevent right click of mouse */
function right(e) {
	if (navigator.appName == 'Netscape' &&(e.which == 3 || e.which == 2))
		return false;
	else if (navigator.appName == 'Microsoft Internet Explorer' &&	(event.button == 2 || event.button == 3)) {
		alert("Mouse Right Click Disabled.");
		return false;
	}
	return true;
}

/***Disable right click ***/
 
 var message="Function Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false");


document.onmousedown=right;
document.onmouseup=right;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
if (document.layers) window.captureEvents(Event.MOUSEUP);
window.onmousedown=right;
window.onmouseup=right;

var pk=1;


	if(top != self)
	{
		top.location=self.location;
	}
function disableCtrlKeyCombination(e,inf)
	{
	        //list all CTRL + key combinations you want to disable
	        var forbiddenKeys = new Array("a", "n", "c", "x", "v", "j");
	        var key;
	        var isCtrl;

	        if(window.event)
	        {
	                key = window.event.keyCode;     //IE
	                if(window.event.ctrlKey)
	                        isCtrl = true;
	                else
	                        isCtrl = false;
	        }
	        else
	        {
	                key = e.which;     //firefox
	                if(e.ctrlKey)
	                        isCtrl = true;
	                else
	                        isCtrl = false;
	        }

	        //if ctrl is pressed check if other key is in forbidenKeys array
	        if(isCtrl)
	        {
	                for(i=0; i<forbiddenKeys.length; i++)
	                {
	                        //case-insensitive comparation
	                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
	                        {
	                                alert('Key combination CTRL + '+String.fromCharCode(key)+ 'has been disabled.');
	                                return false;
	                        }
	                }
	        }
	        //alert(forbiddenKeys.length);
	       if(inf.value.length == 0){
	    	   
	        return (
					key == 8 ||
					key == 9 ||	
					key == 13 ||
					key == 16 ||
					key == 46 ||
					key == 110 ||
					key == 190 ||
					(key >= 35 && key <= 40) ||
					(key >= 51 && key <= 53) ||
					(key >= 96 && key <= 105));
	        
	       }else{
	    	   return (
						key == 8 ||
						key == 9 ||	
						key == 13 ||
						key == 16 ||
						key == 46 ||
						key == 110 ||
						key == 190 ||
						(key >= 35 && key <= 40) ||
						(key >= 48 && key <= 57) ||
						(key >= 96 && key <= 105));
	       }
	}

function backButtonOverride()
	{
	  setTimeout("backButtonOverrideBody()", 1);
	}

function backButtonOverrideBody()
	{
	  try {
	    history.forward();
	  } catch (e) {
	    // OK to ignore
	  }
	  setTimeout("backButtonOverrideBody()", 500);
	}	
history.forward(0);
/* To prevent right click of mouse */
function right(e) {
	if (navigator.appName == 'Netscape' &&(e.which == 3 || e.which == 2))
		return false;
	else if (navigator.appName == 'Microsoft Internet Explorer' &&	(event.button == 2 || event.button == 3)) {
		alert("Mouse Right Click Disabled.");
		return false;
	}
	return true;
}

document.onmousedown=right;
document.onmouseup=right;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
if (document.layers) window.captureEvents(Event.MOUSEUP);
window.onmousedown=right;
window.onmouseup=right;



function radio_check()
{
var x=document.getElementById('nbs').value;
if(x==0){
	document.getElementById("nbs").disabled=true;
}
}

</script>


