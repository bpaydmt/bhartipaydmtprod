<%@page import="java.util.Map"%>
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");

%>
<% 
String merchantName = mapResult.get("appname");
String domainName = mapResult.get("domainName");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>MT | Customer Grievances Policy</title>

<jsp:include page="reqFiles.jsp"></jsp:include>


 <style>
 
 .btn-group.pull-right{display:none}

 </style>
 
</head>
<body>
 <jsp:include page="header.jsp"></jsp:include>
	<%-- <jsp:include page="jsp/mainMenuOption.jsp"></jsp:include> --%>
	<div class="section-wrap container">
		<div class="section">
			<!-- CONTENT -->
			<div class="content center faqPage">
				<!-- POST -->
				<article class="post blog-post">
					<!-- POST IMAGE 
					<div class="post-image">
						<figure class="product-preview-image large liquid">
							<img src="images/blog/01b.jpg" alt="">
						</figure>
					</div>
					<!-- /POST IMAGE -->

					<!-- POST CONTENT -->
					<div class="post-content with-title">
						
						

					
				
					<!-- /POST CONTENT -->
					<div class="container-fluid" style="    background: #fff;
    text-align: left;
    padding: 20px 56px;">

<jsp:include page="../newUiJsp/customerGrievancesPolicy.jsp"></jsp:include>
</div>
	</div>
					<hr class="line-separator">
				</article>
				<!-- /POST -->

				
			</div>
			<!-- CONTENT -->

		
		</div>
	</div>

<%-- <jsp:include page="jsp/subscribeNewsLetter.jsp"></jsp:include> --%>
<jsp:include page="footer.jsp"></jsp:include> 
</body>
</html>
