<jsp:include page="reqFiles.jsp"></jsp:include>

<script type="text/javascript">
	$(function(){
		$.get('GetUserConfigByDomain',function(data){
			var img = $.parseJSON(data.result);
			console.log(img);
			$('#logoPics').attr('src',img.logopic);
			
		})
	})
</script>

<style type="text/css">


@import url('https://fonts.googleapis.com/css?family=Raleway');
@import url('https://fonts.googleapis.com/css?family=Lato');

.error-section {
    text-align: center;
    padding: 4em 0 0;
}
.error-section h4 {
    font-weight: bold;
    font-size: 1.8em;
    margin-top: 5rem;
}

.error-section img {
}

.error-section h5 {
    font-size: 1.3em;
    margin: 1rem 0;
    font-family: 'Raleway',sans-serif;
    font-weight: normal;
}

.error-section a {
    background: #37474f;
    padding: 1rem 2rem;
    box-shadow: 0 4px #7e939e;
    color: white;
    border-radius: 2px;
    font-weight: bold;
    line-height: 4.5;
    text-decoration: none;
    transition: all 0.5s ease;
    font-family: 'Raleway',sans-serif;
}

.error-section a:hover {
    background: white;
    color: #37474f;
}

body {
    background: #f2f2f2;
}

.error-section h1 {
    font-size: 3em;
    font-family: 'Lato',sans-serif;
    font-weight: bold;
    color: #37474f;
    text-transform: uppercase;
    margin: 1rem 0;
}


	
</style>

</head>

<body class="oopsbg">
<div class="error-section">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12">
					<img src="./image/404-img.png">
					<h1>Page Not Found</h1>
					<h5>The Page you are looking for was moved, removed, renamed or might never existed.</h5>
					<a href="UserHome">Go Home</a><br>
			
					<br>
					<img id="logoPics" class="error-logo">
				</div>
				
			</div>
		</div>
	</div>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

</body>
</html>


