<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.lean.ServiceConfig"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script> 

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<style>

  
  .popup-overlay-other1{ 
    visibility:hidden;
    position:fixed; 
    width:50%;
    height:50%;
    left:25%;
    top: 38%; 

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content-other1 { 
   	visibility:hidden;
  }

  .popup-content-other1.active { 
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
	  width: 50%;
	  height: auto;
	  left: 25%;  
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px; 
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  
  
.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important; 
        margin-bottom: 15px;
        color: #111 !important;
}

.bbpsloader{
	position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    display: none;
    background: url('images/bbps/ring_loader.gif') 50% 50% no-repeat rgba(255, 255, 255, 0.8);
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


.switch-field {
	display: flex;
	overflow: hidden;
	    margin-top: 5px;
}

.switch-field input {
	position: absolute !important;
	clip: rect(0, 0, 0, 0);
	height: 1px;
	width: 1px;
	border: 0;
	overflow: hidden;
}

.switch-field label {
	background-color: #e4e4e4;
	color: rgba(0, 0, 0, 0.6);
	font-size: 14px;
	line-height: 1;
	text-align: center;
	padding: 8px 16px;
	margin-right: -1px;
	border: 1px solid rgba(0, 0, 0, 0.2);
	box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
	transition: all 0.1s ease-in-out;
}

.switch-field label:hover {
	cursor: pointer;
}

.switch-field input:checked + label {
	background-color: green;
	box-shadow: none;
}

.switch-field label:first-of-type {
	border-radius: 4px 0 0 4px;
}

.switch-field label:last-of-type {
	border-radius: 0 4px 4px 0;
}

/* This is just for CodePen. */

.form {
	max-width: 600px;
    font-family: "Lucida Grande", Tahoma, Verdana, sans-serif;
    font-weight: normal;
    line-height: 1.625;
    display: flex;
    justify-content: center;
    align-items: center;
}


</style>
</head>
<body onload="divHide()">
<% 
User user = (User) session.getAttribute("User");
List<ServiceConfig> list=(List<ServiceConfig>)request.getAttribute("service");

 
	//out.print("----------------------"+list.size());

%>
    <!-- topbar starts -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends --> 

    <!-- left menu starts -->
    <jsp:include page="mainMenu.jsp"></jsp:include>
    <!-- left menu ends -->
    
    <div id="main" role="main">
	<div id="content">
	<div class="row">
    <div class="box2">
	<div class="box-inner">
	
		<div class="box-header">
			<h2>Service Control</h2>
		</div>
		
			<div class="box-content">
		
			 <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<div class="row">
					<%
					if(user.getWhiteLabel()==0){
					%>
						<div class="col-md-12">
											
				<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
				<!-- <label for="dateTo">Date To:</label>  -->
				
				<s:select list="%{aggrigatorList}" headerKey=""  id="aggregatorid" 
				name="walletBean.aggreatorid" cssClass="form-username" requiredLabel="true"/>
			    </div>
												
				<div class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
					
					<div  id="wwctrl_submit">
					<a href="GetFlag">	<input class="btn btn-success" id="submit" type="submit" value="Submit" ></a>
						<!-- <input class="btn btn-info" id="reset" type="reset" value="Reset">    onclick="getAgg()"-->
					</div>
				</div>

				</div>
				
		     <%} %>		
								
		  </div> 
		
	  </div>
    </div></div>
    
    
    <div class="container p-3 my-3 border" style="margin-top: 40px;">
    
    	<!--Creates the popup body-->
	      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
	        <div class="popup-content-other1"> 
	         <div class='box-login-title-other1'> 
	           <div class="MessageBoxMiddle-other1">  
	            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Do you want to proceed ...</span>
		            <div style="text-align:center;margin-bottom: 10px;">
		            	<div id="popupInfoDetails"> </div>
			           <button type="button" class="btn btn-success" id="popUpYes" style="transition: none;">Yes</button> 
			           <button type="button" class="close-other1 btn btn-primary" onclick="popUpNo();" style="padding: 6px 12px;transition: none;">No</button>
		            </div> 
	          </div>
	         </div>   
	        </div>
	      </div> 
	   <!-- Popup End -->
 
	    
	
<div id="test"></div>	

<span id="demo"> </span>    
	    
<div id="tableDt">	    
	    
     
	    
  </div>
  
  </div></div></div>
  
    <script type="text/javascript">
		function bank2ImpsServiceStatus(reqType){

			var bank2ImpsAggregatorId = $("#bank2ImpsAggregatorId").val();
			var bank2Imps = $("#Bank2imps").val();
			var bank2ImpsStatus = $('input:radio[name="switch-bank2-imps-one"]:checked').val();

			var serviceJson = {
                aggregatorId : bank2ImpsAggregatorId,
                serviceType : bank2Imps,
                status : bank2ImpsStatus
            }

			if(reqType=='GET'){
				if(bank2ImpsAggregatorId == 'All'){
					$("#radio-bank2-imps-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(serviceJson,bank2Imps);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(serviceJson);
				/* callAjaxServiceMaster(serviceJson); */
			}
		}
		function callAjaxServiceMaster(serviceJson){
			popUpNo();
			$.ajax({
                url : "servicesStatusAction",
                method : 'Post',
                cache:0,
                async : false,
                data : "serviceRequest="+JSON.stringify(serviceJson),
                success : function(data) {
                    var dataJson = JSON.parse(data);
						alert(dataJson.statusMsg);
                }
	        });	
		}

		/* Bank2-NEFT */
		function bank2NeftServiceStatus(reqType){

			var bank2NeftAggregatorId = $("#bank2NeftAggregatorId").val();
			var Bank2neft = $("#Bank2neft").val();
			var bank2neftStatus = $('input:radio[name="switch-bank2-neft-one"]:checked').val();

			var serviceJson = {
                aggregatorId : bank2NeftAggregatorId,
                serviceType : Bank2neft,
                status : bank2neftStatus
            }

			if(reqType=='GET'){
				if(bank2NeftAggregatorId == 'All'){
					$("#radio-bank2-neft-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(serviceJson,Bank2neft);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(serviceJson);
				/* callAjaxServiceMaster(serviceJson); */
			}
		}

		function finoImpsServiceStatus(reqType){
			var finoImpsAggregatorId = $("#finoImpsAggregatorId").val();
			var finoImps = $("#finoImps").val();
			var finoImpsStatus = $('input:radio[name="switch-fino-imps-one"]:checked').val();

			var finoImpsJson = {
                aggregatorId : finoImpsAggregatorId,
                serviceType : finoImps,
                status : finoImpsStatus
            }

			if(reqType=='GET'){
				if(finoImpsAggregatorId == 'All'){
					$("#radio-fino-imps-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(finoImpsJson,finoImps);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(finoImpsJson);
				/* callAjaxServiceMaster(finoImpsJson); */
			}
		}

		function finoNeftServiceStatus(reqType){
			var finoNeftAggregatorId = $("#finoNeftAggregatorId").val();
			var finoNeft = $("#finoNeft").val();
			var finoNeftStatus = $('input:radio[name="switch-fino-neft-one"]:checked').val();

			var finoNeftJson = {
                aggregatorId : finoNeftAggregatorId,
                serviceType : finoNeft,
                status : finoNeftStatus
            }
            
			if(reqType=='GET'){
				if(finoNeftAggregatorId == 'All'){
					$("#radio-fino-neft-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(finoNeftJson,finoNeft);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(finoNeftJson);
				/* callAjaxServiceMaster(finoNeftJson); */
			}
		}
		 
		function bbpsServiceStatus(reqType){
			var bbpsAggregatorId = $("#bbpsAggregatorId").val();
			var bbpsServ = $("#bbpsServ").val();
			var bbpsStatus = $('input:radio[name="switch-bbps-one"]:checked').val();

			var bbpsJson = {
                aggregatorId : bbpsAggregatorId,
                serviceType : bbpsServ,
                status : bbpsStatus
            }

			if(reqType=='GET'){
				if(bbpsAggregatorId == 'All'){
					$("#radio-bbps-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(bbpsJson,bbpsServ);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(bbpsJson);
				/* callAjaxServiceMaster(bbpsJson); */
			}
		}

		function aepsServiceStatus(reqType){
			var aepsAggregatorId = $("#aepsAggregatorId").val();
			var aepsServ = $("#aepsServ").val();
			var aepsStatus = $('input:radio[name="switch-aeps-one"]:checked').val();

			var aepsJson = {
                aggregatorId : aepsAggregatorId,
                serviceType : aepsServ,
                status : aepsStatus
            }
			if(reqType=='GET'){
				if(aepsAggregatorId == 'All'){
					$("#radio-aeps-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(aepsJson,aepsServ);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(aepsJson);
				/* callAjaxServiceMaster(aepsJson); */
			}
		}

		function mAtmServiceStatus(reqType){
			var mAtmAggregatorId = $("#mAtmAggregatorId").val();
			var mAtmServ = $("#mAtmServ").val();
			var mAtmStatus = $('input:radio[name="switch-mATM-one"]:checked').val();

			var mAtmJson = {
                aggregatorId : mAtmAggregatorId,
                serviceType : mAtmServ,
                status : mAtmStatus
            }

			if(reqType=='GET'){
				if(mAtmAggregatorId == 'All'){
					$("#radio-mATM-one").prop("checked", true);
				}
				else{
					callAjaxGetServiceMaster(mAtmJson,mAtmServ);
				}
			}
			if(reqType=='POST'){
				serviceMasterPopUp(mAtmJson);
				/* callAjaxServiceMaster(mAtmJson); */
			}
		}

	function callAjaxGetServiceMaster(serviceJson,serviceType){
		$.ajax({
            url : "getServicesStatusInfo",
            method : 'Post',
            cache:0,
            async : false,
            data : "serviceRequest="+JSON.stringify(serviceJson),
            success : function(data) {
				var dataJson = JSON.parse(data);
				var statusJson = JSON.parse(dataJson.statusMsg);

				//mATM
				if(statusJson.status =='UP' && serviceType == 'mATM'){
					$("#radio-mATM-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'mATM'){
					$("#radio-mATM-two").prop("checked", true);
				}

				//Aeps
				if(statusJson.status =='UP' && serviceType == 'Aeps'){
					$("#radio-aeps-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'Aeps'){
					$("#radio-aeps-two").prop("checked", true);
				}

				//BBPS
				if(statusJson.status =='UP' && serviceType == 'BBPS'){
					$("#radio-bbps-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'BBPS'){
					$("#radio-bbps-two").prop("checked", true);
				}

				//Fino-NEFT
				if(statusJson.status =='UP' && serviceType == 'Fino-NEFT'){
					$("#radio-fino-neft-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'Fino-NEFT'){
					$("#radio-fino-neft-two").prop("checked", true);
				}

				//Fino-IMPS
				if(statusJson.status =='UP' && serviceType == 'Fino-IMPS'){
					$("#radio-fino-imps-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'Fino-IMPS'){
					$("#radio-fino-imps-two").prop("checked", true);
				}

				//Bank2-NEFT
				if(statusJson.status =='UP' && serviceType == 'Bank2-NEFT'){
					$("#radio-bank2-neft-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'Bank2-NEFT'){
					$("#radio-bank2-neft-two").prop("checked", true);
				}

				//Bank2-IMPS
				if(statusJson.status =='UP' && serviceType == 'Bank2-IMPS'){
					$("#radio-bank2-imps-one").prop("checked", true);
				}
				if(statusJson.status =='DOWN' && serviceType == 'Bank2-IMPS'){
					$("#radio-bank2-imps-two").prop("checked", true);
				}
            }
        });	
	}

	function serviceMasterPopUp(baseData){
		  $(".popup-overlay-other1, .popup-content-other1").addClass("active");
		  
			  var result = '';
			  result = result + '<br>';
				  result = result + '<p style="color:green;font-size:15px;">';
					  result = result + ''+baseData.serviceType+' ';
					  result = result + 'service '+baseData.status+' ';
					  result = result + 'for '+baseData.aggregatorId+' ?';
				  result = result + '</p>';
			  result = result + '<br>';
		 	  $("#popupInfoDetails").empty().append(result);

		 	  var basicData = JSON.stringify(baseData);
		 	  $("#popUpYes").attr('onclick','callAjaxServiceMaster('+basicData+');');
	  }

	function popUpNo(){
		$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
	}


	function getAgg()
	   {
        // $('#tableDt').show();   
		  debugger
		  var aggr = $('#aggregatorid').children("option:selected").val();
		 
		  if(aggr=='-1')
	      {
	  		alert("Please select Aggregator.");
	  		return false;
	  	  }
			  $.ajax({
	    		method:'Post',
	    		cache:0,  		
	    		url:'GetFlag',
	    		data:"id="+aggr,
	    		success:function(data){
	    		var json = JSON.parse(data);

	    	window.location.href = "http://www.w3schools.com";      
	    		
//	var result = '';
/*
result = result + ' <div class="row"><div class="col-md-3 text-center" style="height:40px;border-bottom-color: lavenderblush;border-bottom-style: solid;">';
result = result + '<label style="padding-top: 15px;">M-ATM</label>';
	 
result = result + '</div> <div class="col-md-3 text-center" style="height:40px;border-bottom-color: lavender;border-bottom-style: solid;">';
result = result + '	<form class="form"> ';
result = result + '	<div class="switch-field">';
result = result + '<input type="checkbox" id="payMethod" name="payMethod" data-size="small" value="Credit" data-on-text="Credit" data-on-color="success" data-off-text="Cash" data-off-color="warning" tabindex="13">';

if(json.yesAeps=="0" || json.yesAeps==null ){
result = result + '	<input type="radio" id="radio-mATM-one" name="switch-mATM-one" value="up" onchange="updateService()" />';
result = result + '	<label for="radio-mATM-one">UP</label>';
result = result + '	<input type="radio" id="radio-mATM-two" name="switch-mATM-one" value="down" checked onchange="updateService()" />';
result = result + '	<label for="radio-mATM-two">DOWN</label>';
}else{
result = result + '	<input type="radio" id="radio-mATM-one" name="switch-mATM-one" value="up" checked onchange="updateService()" />';
result = result + '	<label for="radio-mATM-one">UP</label>';
result = result + '	<input type="radio" id="radio-mATM-two" name="switch-mATM-one" value="down" onchange="updateService()" />';
result = result + '	<label for="radio-mATM-two">DOWN</label>';
}
result = result + '  </div> </form> </div> </div>';
 
*/

	    		
//$("#test").empty().append(result);

//"{"yesAeps":"0","asImps":"null","iciciAeps":"null","asNeft":"null","fNeft":"0","pImps":"1","pNeft":"1","fImps":"0","pMode":"PAYTM","finoAeps":"null","matm":"null"}"    		
		         /*
				    var userName=json.userName;
	      			var agentId=json.agentId;
	      			var walletId=json.walletId;
	      			var aggregatorId=json.aggregatorId;	
	      			document.getElementById("userName").value=userName;
	      			document.getElementById("agentId").value=agentId;
	      			document.getElementById("walletId").value=walletId;    		 
	      			document.getElementById("aggregatorId").value=aggregatorId; 
	      			*/
	    		}
	    	})

	   }

	function divHide()	
	{
		$('#tableDt').hide();
		 
	}

function changeService()
{


}

function updateService()
{ 
	debugger
	alert("UPDATE SERVICE");
	
}

function handleChange(service)
{
debugger
alert(service);


}
	
    </script>
</body>
</html>
