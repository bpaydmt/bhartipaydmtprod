<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
<% 
User user = (User) session.getAttribute("User");

DecimalFormat d=new DecimalFormat("0.00");

%>
      
       


            


<div class="box2 col-md-12">
	<div class="box-inner">
	
		
		
		<div class="box-content row">
		
			<form id="dmtForm" >


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<input	type="hidden" name="inputBean.userId" id="userid" value="<s:property value='%{inputBean.userId}'/>"/>	
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dmtdpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									 <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="dmtdpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left ">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="dmtTable" class="display">
				<thead>
				
				
					<tr>
						<th width="5%" style="word-wrap: break-word;"><u>Txndatetime</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>AgentId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>TxnId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderMobileNo</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderName</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneaccountno</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneifsccode</u></th>	
						<th width="5%" style="word-wrap: break-word;"><u>txnamount</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneid</u></th>
						<th width="5%"><u>Remittance status</u></th>
						
						<th width="5%" style="word-wrap: break-word;display:none;" ><u>remittancetype</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancemode</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancestatus</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>paymentinfo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>BeneMobileNo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>banktranrefno</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>bankrrn</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>bankresponse</th>
						
						<th width="5%" style="word-wrap: break-word;display:none;"><u>updatedatetime</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>kyctype</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Distributorid</th>
						
							
					</tr>
				</thead>
				<tbody>
				<%
				List<DmtDetailsMastBean>list=(List<DmtDetailsMastBean>)request.getAttribute("agentList");

				if(list!=null){
				
					int j=1;
				for(int i=0; i<list.size(); i++) {
					DmtDetailsMastBean tdo=list.get(i);
					
					%>
		          	 <tr>
		          	 <td ><%=tdo.getTxndatetime() %></td>
		             <td><%=tdo.getSenderid()%></td>
		             <td><%=tdo.getAgentid() %></td>
		             <td><%=tdo.getTxnid()%></td>
		             <td><%=tdo.getSendermobileno()%></td>
		             <td><%=tdo.getSendername()%></td>
		              <td><%=tdo.getBeneaccountno()%></td>
		             <td><%=tdo.getBeneifsccode() %></td>
		             <td><%=tdo.getTxnamount()%></td>
		              <td><%=tdo.getBeneid()%></td>
		                <td><%=tdo.getRemittancestatus()%></td>
		              
		              
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancetype()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancemode()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancestatus()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getPaymentinfo()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBenemobileno() %></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBanktranrefno()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankrrn()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankresponse()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getUpdatedatetime()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getKyctype()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getDistributerid()%></td>
		            
                  	</tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

<script>

$(function(){
	$("#dmtForm").submit(function(e) {


	    $.ajax({
	           type: "POST",
	           url: "GetCDmtDetailsReport.action",
	           data: $("#dmtForm").serialize(), 
	           success: function(data)
	           {
	        	   $("#dmt").html(data);
	   		    
	   		    $('#dmtTable').DataTable( {
	   		        dom: 'Bfrtip',
	   		        autoWidth: false,
	   		        order: [[ 0, "desc" ]],
	   		        buttons: [
	   		             {
	   		             
	   		                extend: 'copy',
	   		                text: 'COPY',
	   		                title:'DMT Details - ' + '<%= currentDate %>',
	   		                message:'<%= currentDate %>',
	   		            },  {
	   		             
	   		                extend: 'csv',
	   		                text: 'CSV',
	   		                title:'DMT Details - ' + '<%= currentDate %>',
	   		              
	   		            },{
	   		             
	   		                extend: 'excel',
	   		                text: 'EXCEL',
	   		                title:'DMT Details - ' + '<%= currentDate %>',
	   		            
	   		            }, {
	   		             
	   		                extend: 'pdf',
	   		                text: 'PDF',
	   		                title:'DMT Details - ' + '<%= currentDate %>',
	   		                message:" "+ "<%= currentDate %>" + "",
	   		               
	   		            },  {
	   		             
	   		                extend: 'print',
	   		                text: 'PRINT',
	   		                title:'DMT Details - ' + '<%= currentDate %>',
	   		              
	   		            }
	   		        ]
	   		    } );
	   		    
	   		    
	   		    $('#rechargedpStart').datepicker({
	   		     language: 'en',
	   		     autoClose:true,
	   		     maxDate: new Date(),
	   		     

	   		    });
	   		 
	   		     

	   		     
	   		 
	   		    $("#rechargedpStart").blur(function(){
	   		       $('#rechargedpEnd').val("")
	   		     $('#rechargedpEnd').datepicker({
	   		          language: 'en',
	   		         autoClose:true,
	   		         minDate: new Date(converDateToJsFormat($('#rechargedpStart').val())),           
	   		         maxDate: new Date(),
	   		         
	   		        }); 
	   		    })
	         	}
	   		
	   		
	    })
	           
	    

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});	
})



</script>






