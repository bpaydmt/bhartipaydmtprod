<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.lean.LeanAccount"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 

<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             
        ]
    } );
        
} );


function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}


</script>


<script>

</script>

</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
      
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  

<%
    User user = (User) session.getAttribute("User");

%>             
 
<div class="box2">
	<div class="box-inner">
	<div class="box-header">
		<h2>Set Pricing</h2>
	 </div>
	<div class="box-content">
	<div class="row">
	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
	<div class="row">
	<div class="container">
	<div class="col-sm-4"> 
	<s:select list="%{agentList}" headerKey="All"
				headerValue="Select Agent" onchange="getLeanAccount()" id="selectAggregator"
				 cssClass="form-username" requiredLabel="true"/>
				   <input type="hidden"    id="agentId"  name="agentId" /> 
	
	</div></div></div>	</div>
																			
	  <!-- <div class="form-group col-md-3 txtnew col-sm-4 col-xs-6 text-left "> -->
		<div class="col-md-2">
		<input type="button" id="markedAslean" class="btn btn-success btn-fill" style="margin-top: 22px;"
					onclick="markAsLean()" value="Submit"  />
		</div>
		 <div class="col-md-2" id="msg" style="color: red ;" ></div>
        </div>
		</div>
	</div>
	</div>	
<div id="loadDiv">
<table align='center' cellspacing=2 cellpadding=5 id="data_table" border=1 >
<tr>
<td>Range From</td>
<td>Range To</td>
<td>Distributor %</td>
<td>SuperDistributor %</td>
<td>Action</td>
</tr>

<tr>
<td><input type="number" id="newFrom" value="0"  readonly ></td>
<td><input type="number" id="newTo" required></td>
<td><input type="number" id="newDist" ></td>
<td><input type="number" id="newSupDist" ></td>

<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
<div id="msg" style="color: red ;" ></div>
</tr>

</table></div>
<br><br>
<div class="box-header" id="scheme">
	<h2> Pricing Scheme Details</h2>
</div>
<div id="tableDiv">	
 <table align="center" border="5" cellpadding="5" cellspacing="5"  id="leanAccountTable" class="leanAccountTable" style="display: block;float: left;width: 100%;">
	
 
  <thead>  
      <tr>
		<td align="center" style="width: 3%;"><u><b>Sr.No<b></u></td>
		<td align="center" style="width: 10%;"><u><b>From<b></u></td>
		<td align="center" style="width: 15%;"><u><b>To<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Distributor %<b></u></td>
		<td align="center" style="width: 10%;"><u><b>SuperDistributor %<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Action<b></u></td> 
		
	  </tr>
  </thead>				
 
 <tbody>
  
</tbody>
</table>	
        <!-- contents ends -->
  	 </div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<%-- <jsp:include page="footer.jsp"></jsp:include> --%>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<script src="./js/Price.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->



</body>
</html>


