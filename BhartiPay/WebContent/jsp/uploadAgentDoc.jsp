<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

	
</head>
<%
String userType=(String)request.getAttribute("userType");
if(userType!=null&&!userType.equalsIgnoreCase("-1")&&!userType.isEmpty())
%>

	<body>



    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<%
User user=(User)session.getAttribute("User");
%>
    
<div class=" row">
<div class="row"  id="hidethis2" style="margin-top:130px;">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Agent Onboard</h2>
            
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="UpdateAgentOnBoad" id="form_name" method="post" enctype="multipart/form-data" name="PG" >
	 
		
									<div class="row">
									<font id="err" color="red"><s:actionerror/> </font>
									<font id="suc" color="blue"><s:actionmessage/> </font>
									<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									
							
<!-- ****************************************************************************************************************************-->
								<input type="hidden" name="walletBean.id" value="<s:property value='%{walletBean.id}'/>">	

<div class="content-block"> 
                								      
                <h3 class="title">Upload files</h3>
                  
												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select ID proof<font color='red'> *</font></label>
															<input type="file" name="walletBean.file1" required="required">
															
														</div>
													</div>
													
												</div>
												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Address Proof<font color='red'> *</font></label>
															<input type="file" name="walletBean.file2" required="required">
														</div>
													</div>
													
													
												</div>
												
													<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Agent Form<font color='red'> *</font></label>
															<input type="file" name="walletBean.file3" required="required">
														</div>
													</div>
													
													
												</div>
	
	<div class="col-md-12">	<div class="alert alert-warning">
   
    <strong>Note:</strong>  Only png, jpeg ,gif and pfd file format are allowed and file size must not exceed 1 MB.
</div></div>

                </div>




												
												

												<div class="col-md-12">

													



													

													 <div class="col-md-4">
														<div class="form-group">
														
														<input type="submit" class="btn btn-info btn-fill submit-form"
																style="margin-top: 20px;"  value="Submit"  /> 

														<!-- 	<input type="button" class="btn btn-info btn-fill submit-form"
																style="margin-top: 20px;"  value="Submit" onclick="submitVaForm('#form_name')" /> -->
																


															<button type="reset" onclick="resetForm('#form_name')"  class="btn reset-form btn-info btn-fill"
																style="margin-top: 20px;">Reset</button>


														</div>
													</div> 

												</div>



												<div class="col-md-12">


									</div>
									
									</div>
									
									</form>
								
									

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


