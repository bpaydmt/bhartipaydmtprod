<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 </style>
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Customer Details - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Customer Details - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Customer Details - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Customer Details - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Customer Details - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
List<CustomerProfileBean> list=(List<CustomerProfileBean>)request.getAttribute("customerList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Customer Details</h2>
		</div>
	</div>
	</div>	

		
		<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
																					</div>

									</div>
								


								</div>
							
		<div id="xyz">
			<table id="example" class="display" style="table-layout: fixed; overflow:scroll;word-break: break-word;">
				<thead>
				
				
					<tr>
						<th><u>User Id</u></th>
						<th><u>Name</u></th>
						<th><u>Mobile</u></th>
						<th><u>Email</u></th>
						<th><u>Status</u></th>
						<th><u>Wallet Balance</u></th>
						<th><u>KYC Status</u></th>
						<th><u>Last Login</u></th>
						<th><u></u></th>
						
					</tr>
				</thead>
				<tbody>
				<%
				for(CustomerProfileBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getId()%></td>
		             <td><%=wtb.getName()%></td>
		             <td><%=wtb.getMobileno()%></td>
		             <td><%=wtb.getEmailid()%></td>
		             <td><%if(wtb.getUserstatus().equalsIgnoreCase("A")){out.print("ACTIVE");}else{out.print("INACTIVE");} %></td>
		              <td><%=user.getCountrycurrency()%>:<%=wtb.getFinalBalance()%></td>
		              <td><%if(wtb.getKycstatus()==null){out.print("NON KYC");}else{out.print("Approved");}%></td>
		              <td><%=wtb.getLastlogintime() %></td>
		               <td><form action="SetLimit.action" method="post">
<input type="hidden" name="config.id" value="<%=wtb.getId()%>">
<input type="hidden" name="config.walletid" value="<%=wtb.getWalletid()%>">
<input type="submit" value="Set limit" class="btn btn-sm btn-block btn-success">
</form> </td>
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


