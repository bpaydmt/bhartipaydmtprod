<%@page import="java.text.DecimalFormat"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>


<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>

<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
<%!
private static String input;
private static int num;
private static String[] units=
{"",
 " One",
 " Two",
 " Three",
 " Four",
 " Five",
 " Six",
 " Seven",
 " Eight",
 " Nine"
};
private static String[] teen=
{" Ten",
" Eleven",
" Twelve",
" Thirteen",
" Fourteen",
" Fifteen",
" Sixteen",
" Seventeen",
" Eighteen",
" Nineteen"
};
private static String[] tens=
{ " Twenty",
" Thirty",
" Forty",
" Fifty",
" Sixty",
" Seventy",
" Eighty",
" Ninety"
};
private static String[] maxs=
{"",
"",
" Hundred",
" Thousand",
" Lakh",
" Crore"
}; 
public String convertNumberToWords(double nn)
{    
	int n=(int)nn;
    input=numToString(n);
    String converted=""; 
    int pos=1; 
    boolean hun=false;
    while(input.length()> 0)
    {
        if(pos==1) // TENS AND UNIT POSITION
        {   if(input.length()>= 2) // TWO DIGIT NUMBERS
            {   
             String temp=input.substring(input.length()-2,input.length());
             input=input.substring(0,input.length()-2);
             converted+=digits(temp);
            }
            else if(input.length()==1) // 1 DIGIT NUMBER
            {
             converted+=digits(input); 
             input="";
            }
            pos++;
        }
        else if(pos==2) // HUNDRED POSITION
        { 
            String temp=input.substring(input.length()-1,input.length());
            input=input.substring(0,input.length()-1);
            if(converted.length()> 0&&digits(temp)!="")
            {
                converted=(digits(temp)+maxs[pos]+" and")+converted;
                hun=true;
            }
            else
            {
                if
                (digits(temp)=="");
                else
                converted=(digits(temp)+maxs[pos])+converted;hun=true;
            }
            pos++;
        }
        else if(pos > 2) // REMAINING NUMBERS PAIRED BY TWO
        {
            if(input.length()>= 2) // EXTRACT 2 DIGITS
            {  
             String temp=input.substring(input.length()-2,input.length());
             input=input.substring(0,input.length()-2);
               if(!hun&&converted.length()> 0)
                    converted=digits(temp)+maxs[pos]+" and"+converted;
                else
                {
                    if(digits(temp)=="")  ;
                    else
                    converted=digits(temp)+maxs[pos]+converted;
                }
             }
             else if(input.length()==1) // EXTRACT 1 DIGIT
             {
               if(!hun&&converted.length()> 0)
                converted=digits(input)+maxs[pos]+" and"+converted;
                else
                {
                    if(digits(input)=="")  ;
                    else
                    converted=digits(input)+maxs[pos]+converted;
                    input="";
                }
             }
             pos++; 
         }
    }
    return converted;
}
private String digits(String temp) // TO RETURN SELECTED NUMBERS IN WORDS
{
    String converted="";
    for(int i=temp.length()-1;i >= 0;i--)
    {   int ch=temp.charAt(i)-48;
        if(i==0&&ch>1 && temp.length()> 1)
        converted=tens[ch-2]+converted; // IF TENS DIGIT STARTS WITH 2 OR MORE IT FALLS UNDER TENS
        else if(i==0&&ch==1&&temp.length()==2) // IF TENS DIGIT STARTS WITH 1 IT FALLS UNDER TEENS
        {
            int sum=0;
            for(int j=0;j < 2;j++)
            sum=(sum*10)+(temp.charAt(j)-48);
            return teen[sum-10];
        }
        else
        {
            if(ch > 0)
            converted=units[ch]+converted;
        } // IF SINGLE DIGIT PROVIDED
    }
    return converted;
}
private String numToString(int x) // CONVERT THE NUMBER TO STRING
{
    String num="";
    while(x!=0)
    {
        num=((char)((x%10)+48))+num;
        x/=10;
    }
    return num;
}private void inputNumber()
{
    Scanner in=new Scanner(System.in);
    try
    {
      System.out.print("Please enter number to Convert into Words : ");
      num=in.nextInt();
    }
    catch(Exception e)
    {
     System.out.println("Number should be Less than 1 Arab ");
     System.exit(1);
    }
} %>

<script type="text/javascript">
   $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Cash Deposit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 

function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}
function cashDepositAction(id,type){
	$("#hidden-cashval").val(id)
	$('#cashAction').modal('show');
	$('#remoark-inp').val('')
	
	console.log(type)
	console.log(id)
	if(type=="Accept"){
		$("#cash-action").attr("action","ApproveCashDeposit")
		
	}else{
		
		$("#cash-action").attr("action","RejectApproverCashDeposit")
	}
	//var con = confirm("Are you sure you want to accept the request?")
	
	/* if(prom != ""){
		
		$(form).submit();
		
	}else{
		return false;
	} */
}

function cashDepositReject(form){
	
	var con = confirm("Are you sure you want to reject the request?")
	if(con){		
		$(form).submit();
		
	}else{
		return false;
	}
}
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>

<script>
       
       function acceptMoney(){
    	   
       }
       
       
       </script>

<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->

	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<div id="main" role="main">
		<div id="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">





					<div class="box2 ">
						<div class="box-inner">



							<div class="box-header  ">
								<h2>Approve Cash Deposit</h2>

							</div>


							<div class="box-content  ">
								<font style="color: red;"> <s:actionerror />
								</font> <font style="color: blue;"> <s:actionmessage />
								</font>



							</div>
						</div>
					</div>




					<div id="xyz">
						<table id="example"
							class="scrollD cell-border dataTable no-footer">
							<thead>


								<tr>
									<th><u>User Type</u></th>
									<th><u>Agent Name</u></th>
									<th><u>Id</u></th>
									<th><u>Agent Id</u></th>
									<th><u>Type</u></th>
									<th><u>Amount</u></th>
									<th><u>Reference No.</u></th>
									<th><u>Bank Name</u></th>
									<th><u>Branch Name</u></th>
									<th><u>Deposit Date</u></th>
									<th><u>Checker Remark</u></th>
									<th style="width: 30px"><u>Receipt</u></th>

									<th style="width: 50px"><u></u></th>

								</tr>
							</thead>
							<tbody>

								<%
		
			List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					CashDepositMast tdo=eList.get(i);%>
								<tr>

								<td><%=tdo.getUserId().contains("BPA")==true?"Agent":tdo.getUserId().contains("PROA")==true?"Agent":tdo.getUserId().contains("TURA")==true?"Agent":tdo.getUserId().contains("FTOA")==true?"Agent":tdo.getUserId().contains("BPD")==true?"Distributor":tdo.getUserId().contains("PROD")==true?"Distributor":tdo.getUserId().contains("TURD")==true?"Distributor":tdo.getUserId().contains("FTOD")==true?"Distributor":tdo.getUserId().contains("BPSD")==true?"Super Distributor":tdo.getUserId().contains("PROSD")==true?"Super Distributor":tdo.getUserId().contains("TURSD")==true?"Super Distributor":tdo.getUserId().contains("FTOSD")==true?"Super Distributor":tdo.getUserId().contains("BPMU")==true?"Management User":tdo.getUserId().contains("PROM")==true?"Management User":tdo.getUserId().contains("TURM")==true?"Management User":tdo.getUserId().contains("FTOM")==true?"Management User":"-"%></td>				
								
								
								<%-- 	<td><%=tdo.getUserId().contains("OAGG")==true?"Aggregator":tdo.getUserId().contains("OXMA")==true?"Agent":tdo.getUserId().contains("OXMC")==true?"Customer":tdo.getUserId().contains("OXMD")==true?"Distributor":"-"%></td>
				                 --%>
									<%-- <td>
										<%if(user.getUsertype()==2){out.print("Agent");}else if(user.getUsertype()==3){out.print("Distributor"); }else if(user.getUsertype()==7){out.print("Super Distributor");}else if(user.getUsertype()==6){out.print("Management User");}else{out.print("-"); }%>
									</td>
									 --%>
									<td><%=tdo.getAgentname()%></td>
									<td><%=tdo.getDepositId()%></td>
									<td><%=tdo.getUserId()%></td>
									<td><%=tdo.getType()%></td>
									<td><p><abbr title="<%=convertNumberToWords(tdo.getAmount())%>"><%=d.format(tdo.getAmount())%></abbr></p></td>
									<td><%=tdo.getNeftRefNo()%></td>
									<td>
										<%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%>
										<%}%>
									</td>
									<td>
										<%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%>
										<%}%>
									</td>
									<td>
										<%if(tdo.getCashDepositDate()==null){}else{%><%=tdo.getCashDepositDate()%>
										<%}%>
									</td>
                                    <td><%=tdo.getRemarkChecker()%></td>

									<td>
										<%if(tdo.getReciptPic()!=null&&!tdo.getReciptPic().isEmpty()){%>

										<img alt="userImg"
										src="<%if(tdo.getReciptPic()!=null&&tdo.getReciptPic().length()>200){%>data:image/gif;base64,<%} %><%=tdo.getReciptPic()%>"
										data-toggle="modal"
										data-target="#myModal<%=tdo.getDepositId()%>" width="40px"
										height="40px" />										
										<div id="myModal<%=tdo.getDepositId()%>" class="modal fade"
											role="dialog">
											<div class="modal-dialog">

												<!-- Modal content-->
												<div class="modal-content">
													<button type="button" class="close" data-dismiss="modal">&times;</button>


													<img alt="userImg"
														src="data:image/gif;base64,<%=tdo.getReciptPic()%>" />


												</div>

											</div>
										</div> <%} %>

									</td>
									<td><input type="button"
										onclick="cashDepositAction('<%=tdo.getDepositId()%>','Accept')"
										class="btn btn-sm btn-block btn-success grid-small-btn"
										value="Accept"> <input type="button"
										onclick="cashDepositAction('<%=tdo.getDepositId()%>','Reject')"
										class="btn btn-sm btn-block btn-success grid-small-btn"
										value="Reject"></td>


								</tr>
								<%} }%>
							</tbody>
						</table>
					</div>







				</div>
			</div>
		</div>
	</div>
	<!--/.fluid-container-->


	<div id="cashAction" class="modal fade" role="dialog">
		<div class="modal-dialog" style="width: 416px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Approver Remark</h4>
				</div>
				<div class="modal-body">
					<form id="cash-action" method="post">
						<div class="form-group">
							<input type="hidden" value="${csrfPreventionSalt}"
								name="csrfPreventionSalt"> <input type="hidden"
								name="depositMast.depositId" id="hidden-cashval" /> <input
								type="text" placeholder="Remark"
								name="depositMast.remarkApprover" id="remoark-inp"
								class="form-control" required />


						</div>
						<input type="submit" value="submit"
							class="btn btn-info btn-fill btn-block" />
					</form>
				</div>

			</div>

		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

<script>
	function OpenInNewWindow(data){
		var image = new Image();
        image.src = "data:image/jpg;base64," + data;

        var w = window.open("");
        w.document.write(image.outerHTML);
	}
	</script>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



