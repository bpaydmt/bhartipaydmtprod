<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <link rel="stylesheet" type="text/css" href="../jspas/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../jspas/mainStyle.css">
  
  
	 <script src="../js/jquery.min.js"></script>  
	   
	 <script src="../js/validateWallet.js"></script>  
 <style>
 
 
 body{    background: none;}
 </style>

</head>
<body>
<div >
<div class="container-fluid">
<form action="UploadSenderPanF60.action" method="post" id="panFrom60Form" enctype="multipart/form-data">

<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
<s:fielderror></s:fielderror>
									

<div class="row">
<div class="col-md-12">
<h2 style="margin-top:0px" id="heading">
		Upload Pan Card	</h2>

</div>
<input type="hidden" value="<s:property value='%{mudraSenderPanDetails.senderId}'/>" name="mudraSenderPanDetails.senderId" id="desc1" >
<input type="hidden" value="PAN" name="mudraSenderPanDetails.proofType" id="proofType" >

</div>
	<div class="row" id="pancard" >

		<div class="col-md-12 form-group">
		
		<label>Enter Your PAN Card Number:</label>
				<input type="text"  value="<s:property value='%{mudraSenderPanDetails.panNumber}'/>" name="mudraSenderPanDetails.panNumber" id="panCardInp" 
																		class="form-control pancard-vl mandatory"
																		placeholder="Enter PAN" >
				
		</div>
	
		<div class="col-md-12 form-group">
	
		<label>Upload PAN Card:</label>
				<input type="file" value="<s:property value='%{mudraSenderPanDetails.myFile1}'/>" name="mudraSenderPanDetails.myFile1" id="panfile"  class="mandatory" placeholder="PAN CARD" required readonly="readonly" required> 
				
		</div>
		
	<div class="col-md-12" style="margin-top:15px"><strong>Note:</strong> If you don't have PAN card. You can submit FORM 60 instead of PAN Card. To submit FORM 60 <a href="javascript:void(0)" id="form60LInk" >Click here</a></div>
		
	</div>
	<div class="row" id="from60"  style="display:none" >
	
	
		<div class="col-md-12 form-group">
		<label><a href="../jspas/assets/downloads/Form60.pdf"  download>Download "Form 60" </a></label>
		<label class="pull-right"><a href="javascript:void(0)" id="uploadPanCard" >Upload PAN Card</a></label>
			
		</div>
	
		<div class="col-md-12 form=group">
			<label>Upload duly filled & sender copy of "Form 60".</label>
			
				<input type="file" value="<s:property value='%{mudraSenderPanDetails.myFile1}'/>" name="mudraSenderPanDetails.myFile1" id="form60file"  placeholder="FORM 60" required readonly="readonly" required> 
				
		</div>
	
	</div>



</form>
	<div class="row">
	<div class="col-md-12">
	<input class="btn btn-primary btn-info btn-block" type="button" onclick="submitVaForm('#panFrom60Form',this)" value="Submit" style="
    padding: 11px;
    margin: 20px 0 10px;
">
</div>
</div>
	</div>
</div>
 <div class="modal-footer " style="text-align:left!important">
     <p><strong>
     
     "As Mandated by the IT Dept., Ministry of Finance (Govt. of India), sender will only be
     allowed to transfer upto Rs 50,000 per year without submission of PAN card. It is 
     strongly advised that you upload sender's PAN Card document along with KYC or
     fill form 60, to allow sender more than Rs 50,000 in a year.
     "
     </strong></p>
      </div>
      
    <style>
    .errorMsg{color:red}
    </style>  
      
   <script>
   
   $(function(){
	   $("#form60LInk").click(function(){
		   $("#from60").show();
		   $("#heading").text("Upload From 60");
		   $("#pancard, .errorMsg").hide();
		   $("#proofType").val('form60')
		   $("#panCardInp, #panfile").removeClass("mandatory");
		   $("#form60file").addClass("mandatory").val('')
	   }) 
	   $("#uploadPanCard").click(function(){
		   $("#from60, .errorMsg").hide();
		   $("#heading").text("Upload PAN Card");
		   $("#pancard").show();
		   $("#panCardInp").val('')
		   $("#proofType").val('PAN')
		   $("#form60file").removeClass("mandatory");
		   $("#panCardInp, #panfile").addClass("mandatory").val('');
		  
	   }) 
	   
   }) 
   
   </script>
</body>
</html>