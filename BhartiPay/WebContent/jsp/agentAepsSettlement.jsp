<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script> 

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 

<style>

  
  .popup-overlay-other1{ 
    visibility:hidden;
    position:fixed; 
    width:50%;
    height:50%;
    left:25%;
    top: 38%; 

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content-other1 { 
   	visibility:hidden;
  }

  .popup-content-other1.active { 
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
	  width: 50%;
	  height: auto;
	  left: 25%;  
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px; 
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  
  
.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important; 
        margin-bottom: 15px;
        color: #111 !important;
}

.bbpsloader{
	position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    display: none;
    background: url('images/bbps/ring_loader.gif') 50% 50% no-repeat rgba(255, 255, 255, 0.8);
}

</style>


<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>
 
<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
   }); 
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>

<script type="text/javascript">

function getAccountDetail()
{
debugger
	var list=document.getElementById("account");
    var account=list.options[list.selectedIndex].value;
 

	$.ajax({
  		method:'Post',
  		cache:0,  		
  		url:'agentAccounts',
  		data:"account="+account,
  		success:function(data){
  		var json = JSON.parse(data);

  		var name=json.name;
  		var bankName=json.bankName;
  		var accountNumber=json.accountNumber;
  		var ifscCode=json.ifscCode;
  		var mobileNo=json.mobileNo;
  		var userId=json.userId;

  		document.getElementById("name").value=name;
		document.getElementById("bankName").value=bankName;		
		document.getElementById("accountNumber").value=accountNumber;
		document.getElementById("ifscCode").value=ifscCode;
		document.getElementById("mobileNo").value=mobileNo;
		document.getElementById("userId").value=userId;


		var currText = '';

		currText +='<strong>Agent Info!</strong><br>';
		
		currText +='<span style="color:#A569BD;" >Agent ID/Mobile No. : </span>';
		currText +='<span style="color:#A569BD;" id="currAgent">'+userId+'</span><br>';
		currText +='<span style="color:#A569BD;" >Name : '+name+'</span><br>';
		currText +='<span style="color:#A569BD;" >Bank Name : '+bankName+'</span><br>';
		currText +='<span style="color:#A569BD;" >Account No : '+accountNumber+'</span><br>';
		currText +='<span style="color:#A569BD;" >Ifsc : '+ifscCode+'</span><br>';
		//currText +='<span style="color:#A569BD;" id="finalBalance">Current balance : '+resJson.finalBalance+'</span><br>';
		
		$("#currBalance").empty().append(currText);
		$("#currBalance").show();

		   		 
  	    }
  	})
}


</script>



</head>

<% 
	User user = (User) session.getAttribute("User");
	Date myDate = new Date(); 
	SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
	String toDate=format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from= cal.getTime();    
	String fromDate = format.format(from);
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

	//List<String>list=(List<String>)request.getAttribute("resultList");
	DecimalFormat d=new DecimalFormat("0.00"); 
%> 
<body onload="getAgentDetails();">

    <!-- topbar starts -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends --> 

    <!-- left menu starts -->
    <jsp:include page="mainMenu.jsp"></jsp:include>
    <!-- left menu ends -->

    
          
	<div id="main" role="main"> 
	    <div id="content"> 

			<div class=" row"> 

				<div class="box col-md-12">
				<div class="bbpsloader" id="loaderDiv"></div>
					<div class="box-inner">
					
						<!--Creates the popup body-->
						      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
						        <div class="popup-content-other1"> 
						         <div class='box-login-title-other1'> 
						           <div class="MessageBoxMiddle-other1">  
						            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Do you want to proceed?</span>
							            <div style="text-align:center;margin-bottom: 10px;">
							            	<div id="popupInfoDetails"> </div>
								           <button type="button" class="btn btn-success" id="payPopUpYes">Yes</button> 
								           <button type="button" class="close-other1 btn btn-primary" onclick="payPopUpNo();" style="padding: 6px 12px;">No</button>
							            </div> 
						          </div>
						         </div>   
						        </div>
						      </div> 
						   <!-- Popup End -->
					    
						<div class="box-header">
							<h2>Bank Settlement</h2>
						</div> 
						

  <input type="hidden"    id="name"  name="name" /> 
  <input type="hidden"    id="bankName"  name="bankName" /> 
  <input type="hidden"    id="accountNumber"  name="accountNumber" /> 
  <input type="hidden"    id="ifscCode"  name="ifscCode" /> 
  <input type="hidden"    id="mobileNo"  name="mobileNo" /> 
  <input type="hidden"    id="userId"  name="userId" />  

						<div class="container col-md-8" style=" margin-left: 50px;">
							<div id="currBalance" style="display:none; margin-top: 10px;" class="alert alert-info ">
							</div>
							<div id="currBalanceFinal" style="display:none; margin-top: 10px;" >
							</div>
						
						</div>
						
						<div class="container">  
							<!-- <form action="AepsSettlementRequest" method="post"> -->
								<div class="row">
							
									 <div class="col-md-12" style="height:50px;margin-top: 25px;" id="settmntTypeDiv" > 
									  	   <label for="apptxt">Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>  
										     <s:select list="%{agentAccountList}" headerKey="-1"  
											 id="account" onchange="getAccountDetail()"
											 name="account" cssClass=""   style="background: #fff; cursor: pointer; padding: 1px 30px;  border: 5px solid #ccc; "
											 requiredLabel="true" />
										
									 </div>		      
		         
									<div id="wwctrl_div" style="margin-top: 25px;display:none;">
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">  
										    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										    <input type="hidden" value="<%=user.getId() %>" name="aeAgentId" id="aeAgentId" placeholder="Enter agent id/mobile no">
										    <%-- <s:select list="%{aggrigatorWLList}" headerKey="All"
												headerValue="All" id="aggregatorid"
												name="walletBean.aggreatorid" cssClass="form-username"
												requiredLabel="true" /> --%>
										 </div>
										<!-- <div class="col-md-4 col-sm-4 col-xs-12"> 
											<div  id="wwctrl_submit">
												<input class="btn btn-success submit-form" id="submit" type="submit" onclick="getAgentDetails();" value="Submit" >
												<input class="btn btn-primary btn-fill" id="reset" type="reset" value="Reset"  >
											</div>
										</div> -->
									</div>
									<div id="aggregatorAreaDiv" style="margin-top:25px;display:none;"> 
										<div class="col-md-4 form-group">
											<label for="apptxt">Select Aggregator  </label>  
										    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
										    <s:select list="%{aggrigatorSettledMap}" headerKey="SelectAnyOne" headerValue="Please select" id="aggregatorid" onchange="prepareAggrigatorList();" name="aggregatorid" cssClass="form-username" requiredLabel="true" />
										 </div>
										<div class="col-md-4"> 
											<div  id="aggregator_wwctrl_submit">
												<button class="btn btn-success submit-form" id="aggregator_submit" onclick="prepareAepsSettelment();">Submit</button>
												<button class="btn btn-primary btn-fill" id="aggregator_reset" onclick="resetAll();">Reset</button>
											</div>
										</div>
									</div>
								</div>
								<div class="container">
									<div class="row" id="aepsSettlementArea" style="display:none;">
										<div class="col-md-8">
										
											<table class="table table-hover">
										      <tr>
										        <td style="font-weight:bold;">Category</td>
										        <td class="text-right" style="font-weight:bold;">Amount</td>
										      </tr>
										     <tbody id="amountTbody"></tbody>
										  </table>
										  <!-- <div id="currBalance" style="margin-bottom: 30px;"></div> -->
										</div>
									</div>
									
									<div  class="box-content" id="settlementDiv" style="display: none">
									
								    <div>
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Total Settlement Amount </label>
													<input id="totalSettlementAmountID"  name="totalSettlementAmount" readonly="readonly" type="text" />
													<div class="errorMsg" style="display: none;"></div>
										
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Total Aeps Transaction Amount </label>
													<input id="aepsSettlementAmountID" name="aepsSettlementAmount" readonly="readonly" type="text" />
													<div class="errorMsg" style="display: none;"></div>
												</div>
											</div>
										
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Amount to be Settled </label>
														<input id="settledAmountID" name="settledAmount" readonly="readonly"  type="text" />
													<div class="errorMsg"></div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Available Amount for Settlement</label>
														<input id="availableSettlementAmountID" name="availableSettlementAmount" onkeyup="return NumericValidation(this);" onblur="validateAmount(this);" type="text" />
													<div class="errorMsg"></div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<button class="btn btn-success submit-form" style="margin-top: 20px;" onclick="submitAvailableSettlement()">Submit</button>
													<button onclick="resetAll();" class="btn reset-form btn-info btn-fill" style="margin-top: 20px;">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									
									
								</div>
						    <!-- </form>  -->
						</div>
					</div>
				</div>	
			</div> 
        </div>
    </div>
        <!-- contents ends -->
</div>
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%-- <script src='js/bootstrap.min.js'></script> --%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->



<script type="text/javascript">

	function getAgentDetails(){
  debugger
    getAccountDetail();
		var aeAgentId = $("#aeAgentId").val();
		var list=document.getElementById("account");
	    var account=list.options[list.selectedIndex].value;
	    
		
		if(aeAgentId != null && aeAgentId != "undefined"){
		
			$.ajax({
				 method:'Post',
				 url:'AepsSettlementRequestAgent',
				 cache:0,
				 async : false,
		         data : "aeAgentId="+aeAgentId+"&account="+account,
		         beforeSend: function(){
		        	$('#loaderDiv').show();
	             },
	             complete: function(){
	                $('#loaderDiv').hide();
	             },
				 success:function(response){
					 var resJson = $.parseJSON(response);
					 if(resJson.status == "ERROR"){
						 alert(resJson.statusMsg);
					 }
					 else
						// getAccountDetail();
						 calsettlement(resJson);
				}
			 });
		}
	}

	function calsettlement(resJson){
     debugger
		var agtId = $("#aeAgentId").val();
		$("#wwctrl_div").hide();

		var aepsAmount = 0.0;
		var preAmount = 0.0;
		var canAmount = 0.0;
		var aepsSetAmt = 0.0;
		var matmSetAmt = 0.0;
        var settlementCharge=0.0;
        var failedSetAmount =0.0;
		
		var result = '';

		$(resJson).each(function(i,val)
		 {
		    $.each(val,function(key,val)
		  {

/*		 	if(val.txncode === '80' || val.txncode === '81' || val.txncode === '88' || val.txncode === '89' || val.txncode === '133'){
				aepsAmount += parseFloat(val.sumCredit);
	  		}
			if(val.txncode === '88' || val.txncode === '89'){
				matmSetAmt += parseFloat(val.sumCredit);
	  		}
			if(val.txncode === '80' || val.txncode === '81' ){
				aepsSetAmt += parseFloat(val.sumCredit);
	  		}
*/
	    	if(val.txncode === '80'  || val.txncode === '88' || val.txncode === '133'){
				aepsAmount += parseFloat(val.sumCredit);
	  		}
			if(val.txncode === '88'  ){
				matmSetAmt += parseFloat(val.sumCredit);
	  		}
			if(val.txncode === '80'  ){
				aepsSetAmt += parseFloat(val.sumCredit);
	  		}
	  		
			if(val.txncode === '124'){
				preAmount = parseFloat(val.sumDebit);
	  		}
			if(val.txncode === '133'){
				failedSetAmount = parseFloat(val.sumCredit);
	  		}
			
	  		
		  });
		});

		canAmount = aepsAmount-preAmount;

		preAmount=preAmount-failedSetAmount;
		//canAmount = aepsAmount-preAmount + failedSetAmount;

		var agentId = $("#aeAgentId").val();
		
		$.ajax({
			 method:'Post',
			 url:'GetAepsSettlementCharge',
			 cache:0,
	         data : "{\"agentId\":\"+agentId+\"}",
             success:function(response){

            	 settlementCharge=JSON.parse(response);
             // $('#charge').append(settlementCharge);
              $('#setCharge').append('<div class="col-md-6"> <input type="hidden" class="form-control" id="srvCharge" value="'+settlementCharge+'" placeholder="Service charge">');
              
               }

             

		      });

		 



		
		result = result + '<tr>';
        result = result + '<td>AEPS</td>';
        result = result + '<td class="text-right">'+aepsSetAmt+'</td>';
        result = result + '</tr>';
        
        result = result + '<tr>';
        result = result + '<td>m-ATM</td>';
        result = result + '<td class="text-right">'+matmSetAmt+'</td>';
        result = result + '</tr>';
        
        result = result + '<tr>';
        result = result + '<td>Pre Settled</td>';
        result = result + '<td class="text-right">- '+preAmount+'</td>';
        result = result + '</tr>';  
		 
		result = result + '<tr>';
		result = result + '<td>Can be Settled</td>';
        result = result + '<td class="text-right" id="canSettledAmt">'+canAmount+'</td>';
		result = result + '</tr>';

		result = result + '<tr>';
		result = result + '<td>To be Settled</td>';
		result = result + '<td><div class="row">';
		result = result + '<div class="col-md-4"> <input type="text" class="form-control" style="margin-top:0px;" id="toSettledAmt" placeholder="Settlement amonut"> </div>';

		result = result + '<div class="col-md-3"> <select class="form-control"  id="txMode" ><option value="IMPS">IMPS</option><option value="NEFT">NEFT</option></select> </div>';
		
		/* result = result + '<div class="col-md-4"><span>INR 11.80 settlement charge will be apply.</spna><input type="hidden" class="form-control" id="srvCharge" value="11.80" placeholder="Service charge"> </div>'; */
		result = result + '<span id="setCharge"></span> <input type="submit" class="btn btn-success" onclick="settlementRequest();"> </div>';
		result = result + '</div>';
		result = result + '</td>';
		result = result + '</tr>';
		result = result + '<tr><td colspan="2">';
		result = result + '<div  class="row">';
		result = result + '<span > <b>Note:</b></span><br> ';
		//result = result + '<ul><li><span> INR <span id="charge"></span> settlement charge will be applied.</span><br></li> ';
		result = result + '<li><span >No settlement will be done on bank holidays or holiday declared by RBI.</span><br></li> ';
		result = result + '<li><span >Before confirmation please check your account details carefully.</span></li> ';
		result = result + '</ul><br><br><br><br><br></div>';
		
		result = result + '</td>';
		result = result + '</tr>';

		
		$("#amountTbody").empty().append(result);

		var currText = '';
/* 
		currText +='<strong>Agent Info!</strong><br>';
		
		currText +='<span style="color:#A569BD;" >Agent ID/Mobile No. : </span>';
		currText +='<span style="color:#A569BD;" id="currAgent">'+agtId+'</span><br>';
		currText +='<span style="color:#A569BD;" >Name : '+resJson.name+'</span><br>';
		currText +='<span style="color:#A569BD;" >Bank Name : '+resJson.bankName+'</span><br>';
		currText +='<span style="color:#A569BD;" >Account No : '+resJson.accountNumber+'</span><br>';
		currText +='<span style="color:#A569BD;" >Ifsc : '+resJson.ifscCode+'</span><br>';
		currText +='<span style="color:#A569BD;" id="finalBalance">Current balance : '+resJson.finalBalance+'</span><br>';
		
		$("#currBalance").empty().append(currText);
		$("#currBalance").show();
		 */
		 
var finalBals="";
//finalBals +='<span style="color:#A569BD;" id="finalBalance">Current balance : '+resJson.finalBalance+'</span><br>';
finalBals +='<span style="color:#A569BD;" id="finalBalance">Settlement Amount : '+canAmount+'</span>';

$("#currBalanceFinal").empty().append(finalBals);
$("#currBalanceFinal").show();					 
		 
		$("#aepsSettlementArea").show();
	}

	$("#settmntType").change(function(){
		$("#settmntTypeDiv").hide();

		var settmntType = $("#settmntType option:selected").val();
		if(settmntType == '1'){
			$("#wwctrl_div").show();
		}
		if(settmntType == '2'){
			$("#aggregatorAreaDiv").show();
		}
	});

	function isAmountAvl(){
debugger
		var toSettledAmt = parseFloat($("#toSettledAmt").val());
		var srvCharge = parseFloat($("#srvCharge").val());

		if(!isNaN(toSettledAmt) && !isNaN(srvCharge) && toSettledAmt>=0  && srvCharge>=0 ) {
		//if(greaterThanZero(toSettledAmt) && greaterThanZero(srvCharge)){

			var finalBalance = parseFloat($("#finalBalance").text());
			var canSettledAmt = parseFloat($("#canSettledAmt").text());
			var totalSettle = 0.0;

 			if(toSettledAmt <= srvCharge) {
				alert('Service charge must be less than settlement amount.');
				return false;
			}
 			else
 				totalSettle = toSettledAmt + srvCharge;

			if(totalSettle>finalBalance){
				alert('You want to settle '+toSettledAmt+' + '+serviceAmt+' = '+totalSettle+' amount, But Agent\'s current balance is less, which is '+finalBalance+'');
				return false;
			}
			if(toSettledAmt>canSettledAmt){
				alert('You can not settle '+toSettledAmt+' amount, Because you entered more than '+canSettledAmt+'');
				return false;
			}
			return true;
		}
		else{
			alert('Please enter valid amounts.');
			//$("#toSettledAmt").val('');
			return false;
		}
	}

	function settlementRequest(){
		if(isAmountAvl()){
			$(".popup-overlay-other1, .popup-content-other1").addClass("active");
			$("#payPopUpYes").attr('onclick','return callAjaxAepsSettlement();');

		}
	}

</script>



<script>
function prepareAepsSettelment(){
debugger
	var aggregatorid = document.getElementById('aggregatorid').value;
	
	
	if(aggregatorid == 'SelectAnyOne'){
		alert('Please select aggregetor.');
	}else{
	
	$.ajax({
		 method:'Post',
		 url:'AepsAggrigatorSettledRequest',
		 cache:0,
		 async : false,
         data : "aggregatorid="+aggregatorid,
         beforeSend: function(){
	        	$('#loaderDiv').show();
          },
          complete: function(){
             $('#loaderDiv').hide();
          },
		 success:function(response){

			 /* response = '{"total":{"totalSettlementAmount":"3118000.0"},"aeps":{"totalAepsTransactionAmount":"2400.8999999999996"}}'; */
			 var resJson = $.parseJSON(response);
			 
			 if(resJson.status == "ERROR"){
				 alert(resJson.statusMsg);
			 }
			 else{
				var total = resJson.total.totalSettlementAmount;
				var aeps =	resJson.aeps.totalAepsTransactionAmount;
				
				
				if(total != null && total != "undefined" && aeps != null && aeps != "undefined"){
					if(parseFloat(total) > parseFloat(aeps))
					{
						var settledAmount = total - aeps;
						$("#totalSettlementAmountID").val(total);
						$("#aepsSettlementAmountID").val(aeps);
						$("#settledAmountID").val(settledAmount);
						
						$("#settlementDiv").show();
					}
					else
					{
						alert('\'Total Settlement Amount\' should be greater than \'Total Aeps Transaction Amount\'.');
					}
				}else
					alert('Amount does not available for settlement.');
			 }
		}
	 });
	}
}


function prepareAggrigatorList(){
	$("#settlementDiv").hide();
}

function greaterThanZero(input){
    if(isNaN(input)){
            return false;
    }
    else{
         if(parseFloat(input)<=0){
                 return false;
         } else {
                 return true;
         }
    }
}

function validateAmount(amountInput) {
    var amount = amountInput.value;
    
    if(amount != "" && !isNaN(amount)){
            if(!greaterThanZero(amount)){
            	$("#availableSettlementAmountID").val('');
                    return false;
            }
    }
    
    var  settledAmount = $("#settledAmountID").val();
    
    if(parseFloat(amount) > parseFloat(settledAmount)){
    	$("#availableSettlementAmountID").val('');
    	alert('\'Available Amount for Settlement\' should not be greater than \'Amount to be Settled\'.');
    	document.getElementById("focus").focus();
    }
}

function NumericValidation(item) {
    var Number = item.value
    if (isNaN(Number) || Number == "") {
    	$("#availableSettlementAmountID").val('');
            return false;
    }
    return true;
}


function submitAvailableSettlement(){
debugger	
	var totalSettlementAmount = $("#totalSettlementAmountID").val();
	var aepsSettlementAmount = $("#aepsSettlementAmountID").val();
	var settledAmount = $("#settledAmountID").val();
	var availableSettlementAmount = $("#availableSettlementAmountID").val();
	var aggregatorid = $('#aggregatorid').val();
	//var aggregatorid = 'OAGG001057';
	 
	if(	aggregatorid != null && aggregatorid != "undefined" && aggregatorid != '' && 
		totalSettlementAmount != null && totalSettlementAmount != "undefined" && totalSettlementAmount != '' &&
		aepsSettlementAmount != null && aepsSettlementAmount != "undefined" && aepsSettlementAmount != '' &&
		settledAmount != null && settledAmount != "undefined" && settledAmount != '' &&
		availableSettlementAmount != null && availableSettlementAmount != "undefined" && availableSettlementAmount != ''
	  ){
		
		var aggSettlementObj = {
			aggregatorid : aggregatorid,
			totalSettlementAmount : totalSettlementAmount,
			aepsSettlementAmount : aepsSettlementAmount,
			settledAmount : settledAmount,
			availableSettlementAmount : availableSettlementAmount
		};

		$.ajax({
			 method:'Post',
			 url:'submitAepsAggregatorSettlement',
			 cache:0,
			 async : false,
	         data : "aggSettlement="+JSON.stringify(aggSettlementObj),
	         beforeSend: function(){
		        	$('#loaderDiv').show();
	             },
	             complete: function(){
	                $('#loaderDiv').hide();
             },
			 success:function(response){
				 var resJson = $.parseJSON(response);
				 if(resJson.status == "ERROR"){
					 alert(resJson.statusMsg);
				 }
				 else
				 	aggSettlemtResponse(resJson);
			}
		 });
	}
}

function aggSettlemtResponse(json){
	alert(json.status.creditwallet);
	resetAll();
}	

function resetAll(){
debugger
	$('#aggregatorid').val('SelectAnyOne');
	$('#totalSettlementAmountID').val('');
	$('#aepsSettlementAmountID').val('');
	$('#settledAmountID').val('');
	$('#availableSettlementAmountID').val('');
	$("#settlementDiv").hide();
}

  $('#le-alert').addClass('in'); // shows alert with Bootstrap CSS3 implem
  $('.close').click(function () {
    $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
  });

</script>


<script type="text/javascript">
 
	function callAjaxAepsSettlement(){ 
debugger
            var account=document.getElementById("accountNumber").value;
			var toSettledAmt = parseFloat($("#toSettledAmt").val());

			var tMode = $("#txMode").val();
			  
			var srvCharge = parseFloat($("#srvCharge").val());
			var currAgent = $.trim($("#currAgent").text());

			var data = {"aeAgentId":currAgent,
		        	"aettlementAmount":toSettledAmt,
		        	"serviceCharges":srvCharge,
		        	"account":account,
		        	"txnMode":tMode};
			payPopUpNo();
			$.ajax({
				 method:'Post',
				 url:'AepsAmountSettlementRequestAgent',
				 cache:0,
				 async : false,
		         data : data,
		         beforeSend: function(){
		        	$('#loaderDiv').show();
	             },
	             complete: function(){
	                $('#loaderDiv').hide();
	             },
				 success:function(response){
					 var resJson = $.parseJSON(response);
					 if(resJson.status == "ERROR"){
						 alert(resJson.statusMsg);
					 }
					 else if(resJson.status == "Success"){
						 alert(resJson.statusMsg);
						 $("#aepsSettlementArea").hide();
						 $("#currBalance").hide();
						 $("#wwctrl_div").show();
					 }
				}
			 });
			getAgentDetails();
	}

function payPopUpNo(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
}

</script>

</body>
</html>



