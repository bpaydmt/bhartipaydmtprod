<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.cme.vo.CMERequestBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>

<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'CME Deposit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'CME Deposit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'CME Deposit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'CME Deposit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'CME Deposit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 

function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		<div class="box-header well">
			<h2>CME Request Checker</h2>
		
		</div>
		
	
		<div class="box-content row">
		<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color: blue;">
		<s:actionmessage/>
		</font>	
		
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Id</u></th>
						<th><u>User Id</u></th>
						<th><u>Type</u></th>
						<th><u>Amount</u></th>
						<th><u>Bank Name</u></th>
						<th><u>Transaction Id</u></th>
						<th><u>Request Date</u></th>
					 <th><u>Receipt</u></th>
						<th><u></u></th>
									
					</tr>
				</thead>
				<tbody>
				
				<%
		
			List<CMERequestBean> eList=(List<CMERequestBean>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					CMERequestBean tdo=eList.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getCmeRequestid()%></td>
		          	   <td><%=tdo.getUserId()%></td>
		             <td><%=tdo.getDocType()%></td>
		              <td><%=d.format(tdo.getAmount())%></td>
		              <td><%=tdo.getBankName()%></td>
		           <td><%=tdo.getTxnRefNo()%></td> 
		           <td><%=tdo.getReqestdate()%></td> 
		            <%--  <td>
		             
		             
		             <%if(tdo.getReciptDoc()!=null&&!tdo.getReciptDoc().isEmpty()){%>
		             <img alt="userImg" src="data:image/gif;base64,<%=tdo.getReciptDoc()%>" data-toggle="modal" data-target="#myModal<%=tdo.getCmeRequestid()%>" width="40px" height="40px"/>
		                        <div id="myModal<%=tdo.getCmeRequestid()%>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	          
	
	    <!-- Modal content-->
	    <div class="modal-content">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	   
	  
	       <img alt="userImg" src="data:image/gif;base64,<%=tdo.getReciptDoc()%>"  />
	     
	      
	    </div>
	
	  </div>
	</div>
		            <%} %>
		
			             </td>  --%>
			             <td>
			            
			             <input type="button" onclick="cmeRequest('<%=tdo.getCmeRequestid()%>','Accept')" class="btn btn-sm btn-block btn-success grid-small-btn" value="Accept">
			            
			             
			             <input type="button" onclick="cmeRequest('<%=tdo.getCmeRequestid()%>','Reject')" class="btn btn-sm btn-block btn-success grid-small-btn" value="Reject">
			         

			             </td>
		            
		          
                  		  </tr>
          <div id="cmeAction" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
        <form id="cmeForm" method="post">
        <div class="form-group">
        	  <input type="hidden" name="cmeBean.cmeRequestid"  id="hidden-cme" >
        	<input type="text" placeholder="Remark" name="cmeBean.remarksReason" id="remoark-inp" class="form-control" required/>
        	
        	
        </div>
        <input type="submit" value="submit" class="btn btn-info btn-fill btn-block"/>
        </form>
      </div>
     
    </div>

  </div>
</div>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
<!-- contents ends -->


</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->



<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->
<script>
function cmeRequest(id,type){
	$("#hidden-cme").val(id)
	$('#cmeAction').modal('show');
	$('#remoark-inp').val('')
	
	console.log(type)
	console.log(id)
	if(type=="Accept"){
		$("#cmeForm").attr("action","AcceptCMERequestChecker")
		
	}else{
		
		$("#cmeForm").attr("action","RejectCMERequestChecker")
	}
}
</script>

</body>
</html>



