<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript">
			 			
			function showDiv(usertype){  
				if(usertype==6){
					document.getElementById("subone").style.display ="none";
					document.getElementById("ageone").style.display ="none";
					document.getElementById("disone").style.display ="none";
					document.getElementById("aggoneone").style.display ="none";
						
					}
			else if(usertype==-1){
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="none";
						document.getElementById("aggoneone").style.display ="none";
							
						}
					else if(usertype==1){
						document.getElementById("subone").style.display ="block";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";				
						
						
					} else if (usertype == 2) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 3) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="none";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 4) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 5) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";			
						
					}

				}

		function hideShowDiv(usertype){  
			if(usertype==6 ){
				document.getElementById("manager").style.display ="none";
				//document.getElementById("sales").style.display ="none";
			}else if(usertype==9 )
			{
		    document.getElementById("manager").style.display ="none";
			//document.getElementById("sales").style.display ="none";
			
			} else if(usertype==8)
			{
			document.getElementById("manager").style.display ="block";
			//document.getElementById("sales").style.display ="none";	
			}
	     }

		function onLoadData()
		{
			var list=document.getElementById("usertype");
		    var usertype=list.options[list.selectedIndex].value;
		//alert(usertype);
			hideShowDiv(usertype);
		}      
  
			
		 	  
</script>
</head>
<%-- 
<%
String userType=(String)request.getAttribute("userType");
if(userType!=null&&!userType.equalsIgnoreCase("-1")&&!userType.isEmpty()){
%>
<body onload="showDiv(<%=userType%>)">
	<%	
}else{
	%>

<body>
	<% 
}
%>

 --%>

<body onload="onLoadData()">

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->


	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<div id="main" role="main">
		<div id="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
				<%
				User user=(User)session.getAttribute("User");
				%>
                 <div class="box ">
						<div class="box-inner">
							 <div class="box-header  ">
								<h2>
									<i class="glyphicon "></i>Create User
								</h2>

							</div>
							 
                             <form action="SaveSignUpNewMember" id="form_name" method="post" name="PG">
               
                                 	<div class="row">
											<font id="err" color="red"><s:actionerror /> </font> <font
												id="suc" color="blue"><s:actionmessage /> </font> <input
												type="hidden" value="${csrfPreventionSalt}"
												name="csrfPreventionSalt">
                                         </div>
									
									<%if(user.getUsertype()==4){%> 
									<div class="col-md-12">
                                         <div class="col-md-4">
											<div class="form-group">
											<label for="apptxt">Select User Type <font color='red'> *</font></label>
											    <s:select
												list="#{'6':'Management User','9':'Sales Manager','8':'Sales Officer'}"
												name="walletBean.usertype" id="usertype" onchange="onLoadData(document.getElementById('usertype').value);" >
                                                </s:select>
											  </div>
											</div>
									
									      <div class="col-md-4" id="manager">
											<div class="form-group">
											<label for="apptxt">Select Sales Manager <font color='red'> *</font></label>
											 <s:select list="%{managerList}" headerKey="-1"
												headerValue="Select User Type" id="managerId"
												name="walletBean.managerId" cssClass=""
												requiredLabel="true"  />
											
											  </div>
											</div>
									
									<%-- 
									       <div class="col-md-4" id="sales">
											<div class="form-group">
											<label for="apptxt">Select Sales Officer <font color='red'> *</font></label>
											 <s:select list="%{soList}" headerKey="-1"
												headerValue="Select User Type" id="salesId"
												name="walletBean.salesId" cssClass=""
												requiredLabel="true"  />
											
											  </div>
											</div>
									 --%>  
											
									 </div>
											
									<% }%>

											


											<!-- ****************************************************************************************************************************-->
											
											<%if(user.getUsertype()==4){ %>
											<input class="form-control" name="walletBean.aggreatorid"
												id="wwdw" type="hidden" value="<%=user.getId()%>" />
											<%}%>
 
											<input class="form-control" name="walletBean.createdby"
												id="createdby" type="hidden" value="<%=user.getId() %>" />


											<div class="col-md-12">
<br>
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Name <font color='red'>
																*</font></label> <input type="text" name="walletBean.name"
															placeholder="Name"
															class="form-control userName mandatory" id="name"
															value="<s:property value='%{walletBean.name}'/>">

													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Email <font color='red'>
																*</font></label> <input type="email" name="walletBean.emailid"
															placeholder="Email"
															class=" form-control emailid mandatory" id="email"
															value="<s:property value='%{walletBean.emailid}'/>">

													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Mobile Number <font
															color='red'> *</font></label> <input type="text"
															name="walletBean.mobileno" placeholder="Mobile Number"
															class=" form-control mobile mandatory" id="mobile"
															value="<s:property value='%{walletBean.mobileno}'/>"
															title="Mobile number must have 10 digits and should start with 7,8 or 9."
															maxlength="10" pattern="[6789][0-9]{9}"
															requiredmessage="invalid mobile number">

													</div>
												</div>
											</div>

											<div class="col-md-12">

												<% 
									/*  Map<String,String> mapResult=(Map<String,String>)request.getSession().getAttribute("mapResult");
			                        if(mapResult!=null&&mapResult.get("emailvalid")!=null&&Double.parseDouble(String.valueOf(mapResult.get("emailvalid")))==0){ */
			                        	  if(user.getEmailvalid()==0){
			                        %>


												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Password <font color="red">
																*</font></label> <input type="password" name="walletBean.password"
															placeholder="Password "
															class=" form-control mandatory password-vl" id="password"
															required>

													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Confirm Password <font
															color="red"> *</font></label> <input type="password"
															name="walletBean.confirmPassword"
															placeholder="Confirm Password "
															class=" form-control confirm-vl mandatory" id="password"
															required>

													</div>
												</div>
												 

												<%}if(user.getUsertype()==99){ %>

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">White Label</label>
														<!--  <input type="radio"   name="walletBean.whiteLabel"  value="1" class=" form-control confirm-vl mandatory" id="whitelabel" > -->
														<select name="walletBean.whiteLabel" id="slectboxid">
															<option value="0">Normal</option>
															<option value="1">White Label</option>
														</select>
													</div>
												</div>

												<%} else{%>

												<input type="hidden" name="walletBean.whiteLabel"
													value="<%=user.getWhiteLabel()%>">
												<%}
									
									%>
											</div>







											<div class="col-md-12">

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Address 1</label> <input type="text"
															name="walletBean.address1" placeholder="Address 1"
															value="<s:property value='%{walletBean.address1}'/>"
															class=" form-control address-vl" maxlength="100"
															id="address1" />

													</div>
												</div>



												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Address 2</label> <input type="text"
															name="walletBean.address2" placeholder="Address 2"
															maxlength="100" class=" form-control address-vl"
															id="address2"
															value="<s:property value='%{walletBean.address2}'/>" />

													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">City</label> <input type="text"
															name="walletBean.city" placeholder="City"
															class=" form-control userName" id="city"
															value="<s:property value='%{walletBean.city}'/>" />

													</div>
												</div>


											</div>


											<div class="col-md-12">

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">State</label> <input type="text"
															name="walletBean.state" placeholder="State"
															class=" form-control userName" id="state"
															value="<s:property value='%{walletBean.state}'/>" />

													</div>
												</div>



												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">PIN Code</label> <input type="text"
															name="walletBean.pin" placeholder="PIN Code"
															maxlength='6' min='100000' class=" form-control pincode"
															id="pin" value="<s:property value='%{walletBean.pin}'/>" />

													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">

														<input type="button" class="btn btn-success submit-form"
															style="margin-top: 20px;" value="Submit"
															onclick="submitVaForm('#form_name')" />



														<button type="reset" onclick="resetForm('#form_name')"
															class="btn reset-form btn-info btn-fill"
															style="margin-top: 20px;">Reset</button>


													</div>
												</div>

											</div>
 
								</form>







							</div>
						</div>

					</div>






				</div>
			</div>
		</div>
	</div>
	<!--/.fluid-container-->


	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


