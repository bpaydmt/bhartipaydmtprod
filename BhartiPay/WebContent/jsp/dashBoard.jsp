<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>


<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/whitelabel.css" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400" rel="stylesheet">



<script type="text/javascript">

</script>

<style>
/* 	.chosen-container { 
	    width: 10% ;
	    top: 3px; 
	    float: right; 
	    position: absolute;
	    right: 26px;
	} */
	
	.chosen-container-single .chosen-single {
    border-top: none!important;
    border-left: none!important;
    border-right: none!important;
    border-bottom: 2px solid #ccc;
    background-color: #f0eded!important;
    color: #111!important;
    border-radius: 25px !important;
    font-size: 12px !important;
    font-weight: bold;
	}




.container-dashboard-head {
    text-align: center;
    margin-top: 30px;
}

.card {
    display: inline-block;
    height: 230px;
    width: 12%;
    border-radius: 10px;
    transition: 0.4s ease-out;
    position: relative;
    left: 0px;
    margin: 5px;
}

.cards {
    display: inline-block;
    height: 230px;
    width: 12%;
    border-radius: 10px;
    transition: 0.4s ease-out;
    position: relative;
    left: 0px;
    margin: 5px;
}

.card h3 {
    color: #fff;
    font-size: 14px;
    font-weight: bold;
}

@media (max-width:1024px){
	.card h3 {
	    color: #fff;
	    font-size: 12px;
	    font-weight: bold;
	}
}

.dashboard-head{
  /* background-color: #c1d7ff; */
  background-color: #f2f2f2;
  width: 77%;
  display: block; 
  position: relative;
  left: 11.5%; 
  border-radius: 25px;
  box-shadow: 4px -1px 6px #888b90;
  margin-bottom: 10px;
}

.dashboard-head p {
    padding: 5px;
    color: #ee8422;
    font-size: 20px;
    font-weight: bold;
    margin: 0px;
    display: inline-block;
}

.dashboard-head select { 
    display: inline-block; 
    position: absolute;
    right: 30px;
    top: 10px;
}

.dash-arrow img {
    width: 77%;
    position: absolute;
    left: 157px; 
    bottom: 10px;
}

.card-img1 {
    width: 33%;
    position: absolute;
    top: 5%;
    left: 36%;
}
.card-img2 {
    width: 60%;
    position: absolute;
    top: 0%;
    left: 21%;
}

.card-img3 {
    width: 49%;
    position: absolute;
    top: 3%;
    left: 25%;
}
.card-img4 {
    width: 31%;
    position: absolute;
    top: 6%;
    left: 36%;
}
.card-img5 {
    width: 31%;
    position: absolute;
    top: 6%;
    left: 38%;
}
.card-img6 {
    width: 23%;
    position: absolute;
    top: 9%;
    left: 39%;
}	
</style>

</head>
<% 
  DecimalFormat d=new DecimalFormat("0.00");
  User user = (User) session.getAttribute("User");
  Date myDate = new Date();
  System.out.println(myDate);
  SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
  String toDate=format.format(myDate);
  Calendar cal = Calendar.getInstance();
  cal.add(Calendar.DATE, -0);
  Date from= cal.getTime();    
  String fromDate = format.format(from);
  HashMap<String,String> commDetails=(HashMap<String,String>)session.getAttribute("commission");
  HashMap<String,String> dashBoard=(HashMap<String,String>)session.getAttribute("dashBoard");
  if(commDetails==null){
  	commDetails=new HashMap();
  }
  Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
%>
 
<body>
  <div id="main" role="main"> 
    <div id="content">       
        <div class="row">  
        <div class="col-md-12">
        
           <%
            if(user.getUsertype()!=99&&user.getUsertype()!=1&&user.getUsertype()!=2){
            %>    
            <%
            }
  if(user.getUsertype()==3||user.getUsertype()==7){
          %>       
<div class="container-dashboard-head">

  <div class="dashboard-head">
      <p>Dashboard</p>

      <!-- <select>
        <option value="volvo">Today</option>
        <option value="saab">Month</option>
        <option value="opel">Year</option> 
      </select> --> 

  </div>

  <div class="card" style="background-color: #054252;">
    <div style="text-align: center;">
      <img src="./image/add-money.png" class="card-img1">
      <h3 style="margin-top: 62%;">Wallet Loading</h3>
      <h3><%if(dashBoard!=null&&dashBoard.get("Deposit")!=null){
     out.print( d.format(Double.parseDouble(dashBoard.get("Deposit").toString())) );
      }%></h3>
    </div> 
  </div>


  <div class="card" style="background-color: #17540b">
    <div style="text-align: center;">
      <img src="./image/money-transfer1.png" class="card-img2">
      <h3 style="margin-top: 62%;">Money Transfer</h3>
      <h3><%if(dashBoard!=null&&dashBoard.get("MoneyTrans")!=null){
      out.print(d.format(Double.parseDouble(dashBoard.get("MoneyTrans").toString())) );
      }%></h3>
    </div> 
  </div>

  <div class="card" style="background-color: #4e5a06">
    <div style="text-align: center;">
      <img src="./image/aeps-icon11.png" class="card-img3">
      <h3 style="margin-top: 62%;">AEPS</h3>
      <h3><%if(dashBoard!=null&&dashBoard.get("AEPS")!=null){
      out.print(d.format(Double.parseDouble(dashBoard.get("AEPS").toString())));
      }
      %></h3>
    </div> 
  </div>

  <div class="card" style="background-color: #4c2709">
    <div style="text-align: center;">
      <img src="./image/clipboard-image11.png" class="card-img4">
      <h3 style="margin-top: 62%;">m-ATM</h3>
      <h3><% 
      if(dashBoard!=null&&dashBoard.get("MATM")!=null){
    	out.print(  d.format(Double.parseDouble(dashBoard.get("MATM").toString())) );
    	  }%></h3>
    </div> 
  </div>

  <div class="card" style="background-color: #0c3a23">
    <div style="text-align: center;">
      <img src="./image/recharges-card.png" class="card-img5">
      <h3 style="margin-top: 62%;">Recharges</h3>
      <h3><%if(dashBoard!=null&&dashBoard.get("Recharge")!=null){
     out.print( d.format(Double.parseDouble(dashBoard.get("Recharge").toString()))); 
      }
      %></h3>
    </div> 
  </div>
  
 
<%--   <div class="cards" style="background-color: ##a88787">
    <div style="text-align: center;">
      <img src="./image/recharges-card.png" class="card-img6">
      <h3 style="margin-top: 62%;">BBPS</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("BBPS")!=null){
    out.print(d.format(Double.parseDouble(dashBoard.get("BBPS").toString())));
  
       }
      %></h3>
    </div> 
  </div>
 --%>
  

  <div class="card" style="background-color: #4a4508">
    <div style="text-align: center;">
      <img src="./image/working1.png" class="card-img6">
      <h3 style="margin-top: 62%;">Working Agent</h3>
      <h3>
      <%if(dashBoard!=null&&dashBoard.get("AGENT")!=null){
         out.print(dashBoard.get("AGENT").toString()); 
      }%></h3>
    </div> 
  </div>
  



	<!-- <div class="dash-arrow">
	    <img src="./image/ddd22222.png">
	</div>  -->  


</div>
  

   
            
          </div> 


 			</div>
        </div>	 
      	
        <%	
  }if(user.getUsertype()==2 && !user.getId().contains("MERCSPAY")){
  %>
<div class="container-dashboard-head">

  <div class="dashboard-head">
 <% long mm = System.currentTimeMillis();
	String endDate = "2021/01/10 17:30:10";
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = sdf.parse(endDate);
	long millis = date.getTime();
  if(mm<=millis)
  { %>
  <marquee><b style="color:#ee8422; font-size: 18px;   ">

  
   Dear Partner, Due to scheduled maintenance activity, Services will not be available from 11:00 PM 20th Aug to 05:00 AM 21st Aug. Sorry for the inconvenience.
  
     </b></marquee>
  <% }else{ %>
      <p>Dashboard</p>
      <%} %>
</div>
 	
<div class="row">

	<div class="container">
	

  <div class="card" style="background-color: #054252;">
    <div style="text-align: center;">
      <img src="./image/add-money.png" class="card-img1">
      <h3 style="margin-top: 62%;">Wallet Loading</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("Deposit")!=null){
     out.print( d.format(Double.parseDouble(dashBoard.get("Deposit").toString()))); 
      }
      %></h3>
    </div> 

  </div>



  <div class="card" style="background-color: #17540b">
    <div style="text-align: center;">
      <img src="./image/money-transfer1.png" class="card-img2">
      <h3 style="margin-top: 62%;">Money Transfer</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("MoneyTrans")!=null){
     out.print( d.format(Double.parseDouble(dashBoard.get("MoneyTrans").toString())) );
      }
      %></h3>
    </div> 

</div>




  <div class="card" style="background-color: #4e5a06">
    <div style="text-align: center;">
      <img src="./image/aeps-icon11.png" class="card-img3">
      <h3 style="margin-top: 62%;">AEPS</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("AEPS")!=null){
    out.print(  d.format(Double.parseDouble(dashBoard.get("AEPS").toString())));
      }
      %></h3>
    </div> 

</div>

 
  <div class="card" style="background-color: #4c2709">
    <div style="text-align: center;">
      <img src="./image/clipboard-image11.png" class="card-img4">
      <h3 style="margin-top: 62%;">m-ATM</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("MATM")!=null){
     out.print( d.format(Double.parseDouble(dashBoard.get("MATM").toString())) );
      }
      %></h3>
    </div> 

</div>

 
  <div class="card" style="background-color: #0c3a23">
    <div style="text-align: center;">
      <img src="./image/recharges-card.png" class="card-img5">
      <h3 style="margin-top: 62%;">Recharges</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("Recharge")!=null){
    out.print(d.format(Double.parseDouble(dashBoard.get("Recharge").toString())));
      }
      %></h3>
    </div> 

  </div>
  
<%--      <div class="cards" style="background-color: #32ca7e">
    <div style="text-align: center;">
      <img src="./image/recharges-card.png" class="card-img6">
      <h3 style="margin-top:62%;">BBPS</h3>
      <h3><%
      if(dashBoard!=null&&dashBoard.get("BBPS")!=null){
    out.print(d.format(Double.parseDouble(dashBoard.get("BBPS").toString())));
      }
      %></h3>
    </div> 

  </div> --%>
  
  
   </div>
  </div>

   </div>
  

   
            
          </div> 


 			</div>
        </div>	 

	  
 <% }
  if(user.getUsertype()==100){}
        %> 


    </div> 
  </div> 
 
</body>