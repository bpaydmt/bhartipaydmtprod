
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.UserSummary"%>
<%@page import="java.text.DecimalFormat"%>
<%
User user = (User) session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
DecimalFormat decimalFormat = new DecimalFormat("0.00");
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String sessionid=(String)session.getAttribute("sessionid");

%>

<script src="js/sql.injection.js"></script>
<script>
$(function(){
	SESSIONID = '<%=sessionid %>'
	console.log(SESSIONID)
})

function refreshBalance(){
	$("#walletRefresh").addClass("spinning")
	$.get("GetWalletBalance",function(result){
		console.log(result)
	
		$("#wallet-balance").text(result.amount)
		$("#walletRefresh").removeClass("spinning")
	})
	
}
</script>
<div class="navbar navbar-default" role="navigation" style="margin-bottom: 4px;"> 

<%if(banner!=null&&!banner.isEmpty()){ %>

	<div class="navbar-inner" style="border-bottom: 3px solid #87c440;background-image: url('<%=banner%>');">
  <%}else{ %>
       <div class="navbar-inner" style="border-bottom: 3px solid #87c440;">
  <%} %>

  <button type="button" class="navbar-toggle pull-left animated flip">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
  </button>

  <a class="navbar-brand" href="Home" style="cursor:default;margin-left: 35px;">     
    <%
			if(logo != null && !logo.isEmpty()) {
			%>
	 <img  src="<%=logo%>" width="250" height="50"/>
		<%
			} else {
		%>
	  <img alt="Bhartipay" src="./newmis/img/wallet_logo.png" width="250" height="50" />
		<%} %>
			
	</a>
            <div class="btn-group pull-right" style="margin-right: 50px;">
                <ul class="list-inline">
                <li>
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Download Drivers
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <ul>
                  			<li>
                          <a href="./drivers/mantra.zip" download>Mantra</a>
                        </li>
              	  	  	<li>
                          <a href="./drivers/STARTEK-SDK.zip" download>Startek</a>
                        </li>
              	  	  	<li>
                          <a href="./drivers/MORPHO-SDK.zip" download>Morpho</a>
                        </li> 
                      </ul> 
                    </div>
                  </div>
                </li>

                <%if(user!=null&&user.getUsertype()==1){ %>
                <li><a href="Home">Recharge</a></li>
                <%}
                if(user!=null){
                %>
                   
                <li>
                  <img alt="userImg" src="data:image/gif;base64,<%=user.getUserImg()%>" width="40px" height="40px"/>
                  <span> <%=user.getName() %>     
                  <%}if(user!=null&&user.getUsertype()==1){
                    out.print("(Customer)");
                    }else if(user!=null&&user.getUsertype()==2){ 
                    }else if(user!=null&&user.getUsertype()==3){
                    	out.print("(Distributor)");
                    }else if(user!=null&&user.getUsertype()==7){
                    	out.print("(Super-Distributor)");
                    }else if(user!=null&&user.getUsertype()==4){
                    	out.print("(Aggregator)");
                    }else if(user!=null&&user.getUsertype()==5){
                    	out.print("(Sub-agent)");
                    }else if(user!=null&&user.getUsertype()==99){
                    	out.print("(Admin)");
                    }else if(user!=null&&user.getUsertype()==6){
                    	out.print("(Sub-aggregator)");
                    }%> </span></li>
                   <%
                  if(user!=null&&user.getUsertype()!=99){
                  %>
                
                 <li>
                  <i class="glyphicon glyphicons-folder-open"></i>            
                  <span > 
                    <%=user.getCountrycurrency() %>&nbsp;
                    <% if(user.getUsertype() == 4||user.getUsertype() == 7){
                    %>
                          
                    <span id="wallet-balance"  style="cursor:pointer" onclick="openAddMoneyRequest()"><%=decimalFormat.format(user.getFinalBalance()) %></span> 
                    <% }else{
                    %>
                    <span id="wallet-balance" ><%=decimalFormat.format(user.getFinalBalance()) %></span> <%}%> <span class="glyphicon glyphicon-repeat" id="walletRefresh" onclick="refreshBalance()"></span>&nbsp;</span> 
                  </li>
                          
                  <li title="Oxy Cash" style="padding: 4px 15px 4px 9px;"><i class="fa fa-money" aria-hidden="true" style="padding: 0px 10px 0 0;color:#ff6e26"></i><strong><%=user.getCountrycurrency() %>&nbsp;<span ><%=decimalFormat.format(user.getCashBackfinalBalance()) %></span> </strong></li> 
                <%} %>
                
                    
                <% if (user!=null&&user.getUsertype() != 4 && user.getUsertype() != 99 && user.getUsertype() != 6) { %>
                    
              			<li >
                      <a href="CustomerSupport">
                        <i class="glyphicon glyphicon-phone-alt" style="font-size: 18px; margin: 0 9px; position: relative; top: 2px;"></i><%=user.getCustomerCare() %>
                      </a>
                    </li>
               <%} %> 
                <li><a href="Logoff" onclick="closeDmitWindow()"><i class="glyphicon glyphicon-off" style="margin: 7px 7px 0 0;position:relative;top:2px;font-size:18px"></i>Log Out</a></li>
                </ul>
            </div>
        </div>
    </div>
    
 
<%if(user!=null&&user.getUsertype()==4||user!=null&&user.getUsertype()==7){
%>
<script>
function openAddMoneyRequest(){
	$("#bankNameAggre").html('')
	$("#bankNameAggre").append('<option value="-1">Please select your bank</option>');
	$.get('getCashDepositBank',function(data){
	
	$.each(data,function(value,key){
	 	 $("#bankNameAggre").append($("<option></option>")
	                   .attr("value", value)
	                   .text(key)); ;
	})
    
	
	    $("#bankNameAggre").trigger('chosen:updated');

		$("#balancepop select").val("-1")
	
	})

	$("#balancepop input[type='text']").val("");
	$("#balancepop").modal('show')
}

</script>
<!-- Modal -->
<div id="balancepop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Fund Request</h4>
      </div>
      <div class="modal-body">
        <div id="addMoneyRequstMsg" style="color: red; margin: 2px 8px 5px;"></div>
        <form id="addMoneyOption"> 
          <div class="row">
           	<div class="col-md-6 form-group">
             	<label>Bank</label>
             	<select class="mandatory from-control" id="bankNameAggre" name="moneyRequest.bank"> 
              </select> 
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Transaction Type</label>
           		<select class="form-control mandatory" name="moneyRequest.txnType">
           		  <option  value="-1">Please select Transaction Type</option>
           			<option value="NEFT">NEFT</option>
           			<option value="IMPS">IMPS</option>
           			<option value="RTGS">RTGS</option>
           			<option value="CREDIT">CREDIT</option>
           			<option value="CASH">CASH</option> 
           		</select>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Amount</label>
           		<input type="text" maxlength="7" class="form-control onlyNum mandatory" name="moneyRequest.amount"/>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>UTR No</label>
           		<input type="text" class="form-control mandatory" name="moneyRequest.utrNo"/>
           	</div> 
          </div> 
        </form> 
      </div>
      <div class="modal-footer">
      	<input type="button" value="Submit" data-oxy-popup="balanceReq" onclick="submitVaForm('#addMoneyOption',this)" class="btn btn-info" /> 
      </div>
    </div>
  </div>
</div>
  <%
  } 
  if(user!=null&&user.getUsertype()==2){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(mapResult!=null&&mapResult.get("flaxMessage")!=null){out.print(mapResult.get("flaxMessage"));} %></marquee>
  <%
  }else if(user!=null&&user.getUsertype()==6){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></marquee>
  <%
  }
  %>
<div id="maintenancePop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-body">
        <h2>Sorry for Inconvenience.</h2>
        <p><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div> 
  </div> 
</div>

