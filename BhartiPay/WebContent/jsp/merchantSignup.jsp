<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script type="text/javascript">
function phonenumber(inputtxt)
{
	//alert(inputtxt.value);
  var phoneno = /^\d{10}$/;
  if(inputtxt.value.match(phoneno))
  {
      return true;
  }
  else
  {
     alert("Not a valid Mobile Number");
     return false;
  }
  }
</script>

<jsp:include page="head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="header2.jsp"></jsp:include>



<div class="container" style="margin-top:0%; background-color:#FFF; height:auto; padding:10px 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<h1 class="payplutus_h1">Merchant SignUp</h1>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
    	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">&nbsp;</div>
    	<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <!--
        <p class="payplutus_para">Payplutus has a team of professionals which is focused towards customer support and satisfaction.If you have any queries/issue related to service and support, please fill out the form below.</p>-->
        
        <!--****************************Signup Form here*******************************-->
        
        	<form name="mform" class="login-form" action="SetMerchantSignup.action" method="post">

		<div style="color:green;font-size:16px; margin-left:55px;"><s:property value="msg" /><p>&nbsp;</p></div>
	
		<!--CONTENT-->
    	<div class="form-group">
			
			<div class="form-group payplutus_mt5">
			 	<label class="sr-only" for="form-username">Username</label>
			   	<input type="text" name="username" placeholder="Full Name" class="payplutus_textbox_login" id="username" required/>
				 
			</div> <!--End The Usename div-->
			
			<div class="form-group payplutus_mt5">
			 	<label class="sr-only" for="form-email">Email</label>
			     <input type="email" name="email" placeholder="Email" class="payplutus_textbox_login" id="email" required />
				
			</div> <!--End The Email div-->
			
			<div class="form-group payplutus_mt5">
			 	<label class="sr-only" for="form-mobile">MobileNo.</label>
			     <input type="text" name="mobile" placeholder="Mobile Number " class="payplutus_textbox_login" maxlength="10"  id="mobile" required  onblur="return phonenumber(document.mform.mobile)"/>
				 
			</div> <!--End The mobile div-->
			
			<div class="form-group payplutus_mt5">
			 	<label class="sr-only" for="form-web">Website</label>
			     <input type="url" name="web" placeholder="Website "  class="payplutus_textbox_login" id="web" required value='http://www.' />
				  
			</div> <!--End The Web div-->
			<div class="form-group payplutus_mt5">
			<button type="submit" class="payplutus_button" onclick="return phonenumber(document.mform.mobile)">SignUp</button>						
    		</div>
    	</div>
     	</form>  <!--END LOGIN FORM-->
	
        
        <!--*********************end the signup form here*******************************-->
        
            
        	
        	
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">&nbsp;</div>
    	
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>