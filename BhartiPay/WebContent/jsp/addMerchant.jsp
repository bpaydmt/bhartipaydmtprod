<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Add Merchant</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="SaveAddMerchant" id="form_name" method="post" name="PG">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">
													
					<!-- NAME, EMAIL, M.NO   FIELDS ARE NOT REQUIRED. -->
													<div class="col-md-4">
														<div class="form-group ">
															<label for="apptxt">Username<font color="red"> *</font></label>
															 <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
															 <input	class="form-control userNameId no-space mandatory" name="merchantBean.merchantUser" type="text"
																id="merchantUser" maxlength="20" placeholder="Username" value="<s:property value='%{merchantBean.merchantUser}'/>" />

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Merchant Name <font color="red"> *</font></label>
															 <input	class="form-control mandatory userName" name="merchantBean.merchantName" type="text"
																id="merchantName" maxlength="20"  placeholder="Merchant Name" value="<s:property value='%{merchantBean.merchantName}'/>" />

														</div>
													</div>
													
													
												<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Merchant Email <font color="red"> *</font></label>
															 <input	class="form-control emailid mandatory" name="merchantBean.merchantEmail" type="text"
																id="merchantEmail" maxlength="40" placeholder="Merchant Email" value="<s:property value='%{merchantBean.merchantEmail}'/>" />

														</div>
													</div>

									
									</div>
									
									
									
									
									 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Merchant Mobile <font color="red"> *</font></label>
															 <input	class="form-control mobile mandatory" name="merchantBean.merchantMobile" type="text"
																id="merchantMobile" maxlength="10" placeholder="Merchant Mobile" value="<s:property value='%{merchantBean.merchantMobile}'/>" />

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Business Web URL <font color="red"> *</font></label>
															 <input	class="form-control url-vl mandatory" name="merchantBean.merchantSite" type="text"
																id="merchantSite" placeholder="Business Web URL" value="<s:property value='%{merchantBean.merchantSite}'/>" />

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Status</label>
													<%-- 		 <s:select name="merchantBean.merchantStatus" list="" >
																<s:property value='%{merchantBean.address2}'/>
																<option>
																</s:select> --%>
																<s:select list="%{statusList}" id="status"
																		name="merchantBean.merchantStatus" cssClass="form-username"
																/>
																

														</div>
													</div>
									</div>
									
									
									
									
									
									 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Password <font color="red"> *</font></label>
															 <input	class="form-control password-vl mandatory" name="merchantBean.merchantPassword" type="password"
																id="merchantPassword" maxlength="20" placeholder="Password" value=""/>

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Confirm Password <font color="red"> *</font></label>
															 <input	class="form-control confirm-vl mandatory" name="merchantBean.merchantConfirmPassword" type="password"
																id="merchantConfirmPassword" maxlength="20" placeholder="Confirm Password" value="" />


														</div>
													</div>
													
								<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Merchant Type</label>
															<s:select list="%{merchantTypeList}" id="merchantType"
																		name="merchantBean.merchantType" cssClass="form-username"
																/>
																

														</div>
													</div>
												


									</div>
									
									
									
									
									
												 <div class="col-md-12">
		
													
									<div class="col-md-4">
										<div class="form-group">
										
										<button type="button" onclick="submitVaForm('#form_name',this)" class="btn btn-info submit-form btn-fill"
											 style="margin-top: 20px;">Submit
										</button>
											
										<button type="button" onclick="resetForm('#form_name')" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Reset
										</button>

									
									</div>
									</div>
												


									</div>
									
									
									
			<%-- 	<div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Pan</label>
															 <input	class="form-control" name="merchantBean.pan" type="text"
																id="pan" maxlength="20" placeholder="Pan" value="<s:property value='%{merchantBean.pan}'/>" />

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">AAdhar</label>
															 <input	class="form-control" name="merchantBean.adhar" type="text"
																id="aadhar" maxlength="20" placeholder="AAdhar" value="<s:property value='%{merchantBean.adhar}'/>" />

														</div>
													</div>
													
													
												
													
								


									</div> --%>
									
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


