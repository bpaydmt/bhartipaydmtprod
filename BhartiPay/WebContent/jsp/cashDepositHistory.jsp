<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<jsp:include page="theams.jsp"></jsp:include>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >
 
 
<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>
 
<script type="text/javascript">
  function showSearchDetails(){

  	 var div = $("#searchDetails")
  	 
  	 if(div.is(":hidden")){
  	  div.show()
  	  $("#showDetailsLink span").text("Hide details")
  	  $("#showDetailsLink .fa").addClass("fa-minus-square")
  	  $("#showDetailsLink .fa").removeClass("fa-plus-square")
  	 }else{
  	  div.hide()

  	 
  	  $("#showDetailsLink span").text("Show details")
  	  $("#showDetailsLink .fa").removeClass("fa-minus-square")
  	  $("#showDetailsLink .fa").addClass("fa-plus-square")
  	 }
  	}

</script>


<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
	
   }); 
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Wallet Update History - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Wallet Update History - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Wallet Update History - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Wallet Update History Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Wallet Update History - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>
<style>
  .row-m-15{ 
    /*margin: 5px 8px 10px;*/
    border-bottom: 1px solid #ccc;
    padding: 1px 0 8px;
    float: left;
    width: 100%;
  }
  .box-content {
    padding: 10px; 
    background-color: #fff;
/*     margin-top: 15px;
    margin-bottom: 15px; */
}




.custom-file-input {
  color: transparent;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'Upload Payment Slip';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 6px 12px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  /* font-weight: 700;
  font-size: 10pt; */
  border-radius: 15px;
  position: absolute;
  top: 22px;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active {
  outline: 0;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9); 
}


input[type=file] {
    display: inline-block !important;
}
</style>
</head>

<% 
  User user = (User) session.getAttribute("User");
  Date myDate = new Date();
  System.out.println(myDate);
  SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
  String toDate=format.format(myDate);
  Calendar cal = Calendar.getInstance();
  cal.add(Calendar.DATE, -0);
  Date from= cal.getTime();    
  String fromDate = format.format(from);
  Object[] commDetails=(Object[])session.getAttribute("commDetails");
  Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

  List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");  
%>
      
       

<body>

    <!-- topbar starts -->
  <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    

        
        <!-- left menu starts -->
  <jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->

  <div id="main" role="main"> 
    <div id="content">       
        <div class="row">   

          <div class="col-md-12 box" id="hidethis2">  
          	<div class="box-inner"> 
          		<div class="box-header">
          			<h2>Wallet Update History</h2> 
          		</div> 
          		<!-- <div class="box-content">
            		
            	</div>  --> 
          </div>
<div class="box-inner"> 
            	<div class="box-content"> 
            	
            	<font style="color:red;">
            		  <s:actionerror/>
            		</font>
            		<font style="color: blue;">
            		  <s:actionmessage/>
            		</font>
            		
            		<form action="CashDeposit" method="post"> 
                  <div class="row">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group" style="margin-bottom: 5px;">  
									    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">  

									    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div>	 
										<input type="hidden" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="from" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>

										<input type="hidden" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="to" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
									</div>

              			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"> 
              				<div  id="wwctrl_submit">
              					<input class="btn btn-success submit-form" id="submit" type="submit" value="Submit">
              					<input class="btn btn-primary btn-fill" id="reset" type="reset" value="Reset">
              				</div>
              			</div>

                  </div>
            		</form>
            		
            	</div>  
          </div>


          	<div id="xyz" class="box">
          		<table id="example" class="scrollD cell-border dataTable no-footer" width="100%">
          			<thead> 
          				<tr>
          				<!-- Sr.No	Agent ID	Agent Name	Reuest Date & Time	Type	Bank	Amount	Transaction ID	Branch	Status	Approve Date & Time	Status	Reciept -->
          					<th><u>Id</u></th>
          					<th><u>Reuest Date & Time</u></th>
          					<th><u>Type</u></th>
          					<th><u>Bank</u></th>
          					<th><u>Amount</u></th>
          					<th><u>Transaction id</u></th>
          					<th><u>Branch</u></th>
          					<!-- <th><u>Approve Date & Time</u></th> -->
          					<th><u>Status</u></th>
          					<th><u>Remark</u></th>
          					<th><u>Receipt</u></th> 		
          				</tr>
          			</thead>
          			<tbody>
                  <% 
          		      List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
          			    if(eList!=null){ 
          			      for(int i=0; i<eList.size(); i++) {
          				    CashDepositMast tdo=eList.get(i);%>
          	          <tr>
          	          <td><%=tdo.getDepositId()%></td>
          	          <td><%=tdo.getRequestdate()%></td> 
          	          <td><%=tdo.getType()%></td>
          	          <td>
                        <%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%><%}%> 
                      </td>
          	          <td><%=tdo.getAmount()%></td>  
          	           <td><%=tdo.getNeftRefNo()%></td>
          	          <td>

                        <%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%><%}%>
                          
                      </td>     
          	          <%-- <td><%=tdo.getApproveDate()%></td> --%>
          	          
          	          <td><%=tdo.getStatus()%></td>
          	          <td><%
          	          if("Rejected".equalsIgnoreCase(tdo.getCheckerStatus())){
          	        	out.print(tdo.getRemarkChecker());
          	          }else if("Rejected".equalsIgnoreCase(tdo.getApproverStatus())){
						out.print(tdo.getRemarkApprover());
          	          }else{
						out.print(tdo.getStatus());
          	          }
          	          %></td> 
          	          <td> 
          	            <%if(tdo.getReciptPic()!=null&&!tdo.getReciptPic().isEmpty()){%>
          	            <img alt="userImg" src="data:image/gif;base64,<%=tdo.getReciptPic()%>" data-toggle="modal" data-target="#myModal<%=tdo.getDepositId()%>" width="40px" height="40px"/>
          	            <div id="myModal<%=tdo.getDepositId()%>" class="modal fade" role="dialog">
                          <div class="modal-dialog">  
                      	    <div class="modal-content">
                	            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            	                <img alt="userImg" src="data:image/ gif;base64,<%=tdo.getReciptPic()%>" /> 
          	                </div> 
          	              </div>
                        </div>
          	            <%} %> 

          		          </td>  
                          </tr>
              			      <%}
              				  }%>	
          		  </tbody>		
              </table>
          	</div>

          </div>

        </div>     
    </div>
  </div>


  <jsp:include page="footer.jsp"></jsp:include>

  <!-- external javascript -->

  <%-- <script src="js/bootstrap.min.js"></script> --%>

  <!-- library for cookie management -->
  <script src="./newmis/js/jquery.cookie.js"></script>
  <script src="./newmis/js/jquery.noty.js"></script>
  <script src="./newmis/js/jquery.history.js"></script>
  <!-- application script for Charisma demo -->
  <%-- <script src="./newmis/js/charisma.js"></script> --%>

</body>
</html>




