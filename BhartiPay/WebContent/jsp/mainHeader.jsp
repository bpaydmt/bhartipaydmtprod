


<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<script src="js/validateWallet.js"></script>
<%
User user=(User)session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
DecimalFormat d = new DecimalFormat("0.00");
if(user!=null&&user.getEmailid()!=null&&user.getEmailid().length()>0){

%>
<!-- Call after login success -->


<div class="container"  id="after_login">
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: -3px; margin-left:-14px;background-image:url(<%=banner%>)" >    
	

    	<div class="container">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                	<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
             	</button>
                <a class="navbar-brand" href="Home" style="color:#FFFFFF;font-family:'Bauhaus 93';font-size:28px;font-style:normal;font-weight:100;text-transform:uppercase;">
              <!--   <img src="./images/logo1.png" style="width:100%; height:auto; margin-left:7%;"> -->
                
              
               
               
               <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>"  style="width:100%; height:auto; margin-left:7%;"/>
					<%
						}else{
					%>
					 <img alt="Bhartipay" src="./images/wallet_logo.png" style="width:100%; height:auto; margin-left:7%;"/>
						<%} %>
               
                </a>
               
         	</div>
            <div id="navbar" class="navbar-collapse collapse pull-right" style="line-height:40px;">
            	<ul class="nav navbar-nav">                    
                    <li><a href="javascript:;" style="cursor:text;">Welcome &nbsp;<%=user.getName() %></a></li>
                    <li><a href="javascript:;" style="cursor:text;"> &nbsp;&nbsp;<%=user.getCountrycurrency()+"  "+d.format(user.getFinalBalance()) %>&nbsp</a></li>
                    <li><a href="javascript:;" style="cursor:text;">&nbsp;|&nbsp;</a></li>
                    
                    <!--  <li><a href="Pricing.action">Pricing</a></li> -->
            	         <li><a href="AddMoney">Add Money</a></li>
            	       		<li><a href="Dmt">Money Transfer</a></li>
                   <!--  <li><a href="Support.action">Support</a></li> -->
                    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account</a>
                    	<ul class="dropdown-menu">
                        	<li style="background-color:#ccc; padding:0px 20px;">
                        	               
                        	
                        	<img src="data:image/gif;base64,<%=user.getUserImg()%>" style="border:1px solid #ccc;" height="30px" width="30px">&nbsp;<%=user.getName() %></li>
                            <li style="background-color:#ccc; padding:0px 20px;"><%=user.getEmailid() %> </li>
                            <li><a href="AddMoney">Add Money</a></li>
            	       		<li><a href="Dmt">Money Transfer</a></li>
                        	<li><a href="ShowProfile">User Profile</a></li>
                        	<li><a href="ShowPassbook">Transaction History</a></li>
                            <li><a href="Logout">Logout</a></li>                            
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<%
}
else{
%>
<div class="container" id="before_login" >
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: -3px; margin-left:-14px;">    	
    	<div class="container">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                	<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
             	</button>
                <a class="navbar-brand" href="Home" style="color:#FFFFFF;font-family:'Bauhaus 93';font-size:28px;font-style:normal;font-weight:100;text-transform:uppercase;">
                <!-- <img src="./images/logo1.png" style="width:100%; height:auto; margin-left:7%;"> -->
                 <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>"style="width:100%; height:auto; margin-left:7%;"/>
					<%
						} else {
					%>
					 <img alt="Bhartipay" src="./images/wallet_logo.png" style="width:100%; height:auto; margin-left:7%;" />
						<%} %>
               
                
                </a>
         	</div>
            <div id="navbar" class="navbar-collapse collapse pull-right" style="line-height:40px;">
            	<ul class="nav navbar-nav">
                       <li><a href="javascript:;" id="login_popup1">Add Money</a></li>
                    <!--  <li><a href="Pricing.action">Pricing</a></li> -->
            	      <li><a href="javascript:;" id="login_popup2">Money Transfer</a></li>
                  <!--   <li><a href="Support">Support</a></li> -->
                    <li><a href="javascript:;" id="login_popup">Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<%
}
%>




<!-- Use a container to wrap the slider, the purpose is to enable slider to always fit width of the wrapper while window resize -->
<div class="container">
	<img u="image" src="./images/banner-1.png" width="100%" height="auto" style="margin-top:-3px;"/>
</div>
<!-- Banner Form -->
<div class="container">
	<div class="mobileRechargeForm">

  <div
			class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_formshadow">
			<!-- ======================================== Form Tab Button ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div
					class="col-lg-3 col-md-3 col-sm-3 col-xs-3 payplutus_tab_active"
					id="mb">
					<a href="#mobileRecharge" class="tab" id="mobile_recharge"><img
						src="./images/mobile.png">&nbsp;Mobile</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 payplutus_tab"
					id="dh">
					<a href="#dthRecharge" class="tab" id="dth_recharge"><img
						src="./images/dth.png">&nbsp;DTH</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 payplutus_tab"
					id="dc">
					<a href="#datacardRecharge" class="tab" id="datacard_recharge"><img
						src="./images/datacard.png">&nbsp;Datacard</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 payplutus_tab"
					id="lb">
					<a href="#landlineRecharge" class="tab" id="landline_Recharge"><img
						src="./images/datacard.png">&nbsp;Landline bill</a>
				</div>
			</div>
			<!-- ========================================== Form Fields =========================================== -->
			<!-- ======================================== MOBILE RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
				id="mobile_recharge_form">

				<!--================================== INCLUDE mobile-recharge.HTML  =====================================-->



				<form method="post" id="mobile-recharge-form" action="RechargeService">
					<%-- <input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>"> --%>
					<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					
					<div class="form-group clearfix  Bhartipay_mt10">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" name="rechargeBean.rechargeType" id="connection_type[]"
								value="PREPAID" checked  onClick="hide_plan();"><label class="tab">Prepaid</label>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" name="rechargeBean.rechargeType" id="connection_type[]"
								value="POSTPAID" onClick="hide_plan();"><label class="tab">Postpaid</label>
						</div>
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="mobile_no"
							class="payplutus_textbox mandatory mobile" required
							placeholder="Enter your mobile number" maxlength="10"
							autocomplete="off"    />
					</div>
					<div class="form-group Bhartipay_mt10">
						
						
						<s:select list="%{prePaid}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory"  required="required" id="operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
					</div>
					<div class="form-group Bhartipay_mt10">
<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown mandatory"   id="circlei" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);"/>

						
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="amount"
							class="payplutus_textbox mandatory amount-vl" required
							placeholder="Enter amount" maxlength="4" />
							
						
					</div>
				
				</form>
					<div class="form-group Bhartipay_mt10" style="margin-top: 15%;">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Recharge" class="payplutus_button submit-form" onclick="submitVaForm('#mobile-recharge-form')">
					</div>
			</div>
			
			<!-- ======================================== D2H RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
				id="d2h_recharge_form" style="display: none;">
				<!--================================== INCLUDE d2h-recharge.HTML  =====================================-->



				<form method="post" id="d2h" action="RechargeService" >
				
<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				
				<%-- 	<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>"> --%>
					
					<input type="hidden" name="rechargeBean.rechargeType" value="DTH">
					<div class="form-group Bhartipay_mt10">
		<s:select list="%{dth}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory "  id="dth_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
					
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="dth_mobile_no"
							class="payplutus_textbox mandatory onlyNum" required
							placeholder="Enter your d2h number" maxlength="10" />
					</div>
					<div class="form-group Bhartipay_mt10">
								
						<input type="hidden" name="rechargeBean.rechargeCircle" value="Delhi">			
 					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="dth_amount"
							class="payplutus_textbox  mandatory amount-vl" required placeholder="Enter amount"
							maxlength="4" />
					</div>
					
				</form>
				<div class="form-group Bhartipay_mt10">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Recharge" class="payplutus_button submit-form" onclick="submitVaForm('#d2h')">
					</div>
			</div>
			 
			<!-- ======================================== DATACARD RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
				id="datacard_recharge_form" style="display: none;">
				<!--================================== INCLUDE datacard-recharge.HTML  =====================================-->




				<form method="post" id="datacard" action="RechargeService">
					
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<%-- <input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>"> --%>
					
					<div class="form-group Bhartipay_mt10 clearfix  ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input type="radio" name="rechargeBean.rechargeType" id="connection_type[]" value="Datacard" checked ><label class="tab">Prepaid</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input type="radio" name="rechargeBean.rechargeType" id="connection_type[]" value="Postpaid-Datacard" ><label class="tab">Postpaid</label>
                        </div>
                    </div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="datacard_mobile"
							class="payplutus_textbox mandatory mobile" required
							placeholder="Enter your datacard number" maxlength="10" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<s:select list="%{prePaidDataCard}" name="rechargeBean.rechargeOperator" required="required" cssClass="payplutus_dropdown mandatory" id="datacard_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>

						
						</select>
					</div>
					<div class="form-group Bhartipay_mt10">
						
						<s:select list="%{circle}" name="rechargeBean.rechargeCircle" required="required" cssClass="payplutus_dropdown mandatory" id="datacard_circle" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);"/>
						
						
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="datacard_amount"
							class="payplutus_textbox mandatory amount-vl" required placeholder="Enter amount"
							maxlength="4" />
					</div>
					
				</form>
				<div class="form-group Bhartipay_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Recharge" class="payplutus_button submit-form" onclick="submitVaForm('#datacard')">
					</div>
			</div>
			
						<!-- ======================================== Landline RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
				id="landline_recharge_form" style="display: none;">
				<!--================================== INCLUDE datacard-recharge.HTML  =====================================-->




				<form method="post" id="landline" action="RechargeService">
					<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				
					<%-- <input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>"> --%>
					
					<div class="form-group Bhartipay_mt10">
                        <input type="hidden" name="rechargeBean.rechargeType" value="landline" checked >
                      
                    </div>
                    	<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.stdCode" id="landline_amount"
							class="payplutus_textbox onlyNum std-vl mandatory"  placeholder="Enter std code start with Zero(0123)"
							maxlength="6" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="landline_mobile"
							class="payplutus_textbox onlyNum mandatory landline-vl" 
							placeholder="Enter your Landline Number" maxlength="8" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<s:select list="%{landLine}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory"  id="landline_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>

					
					</div>
					<div class="form-group Bhartipay_mt10">
	<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown mandatory" id="landline_circle " headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);"/>
					
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="landline_amount"
							class="payplutus_textbox amount-vl mandatory"  placeholder="Enter amount"
							maxlength="4" />
					</div>
				
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.accountNumber" id="landline_amount"
							class="payplutus_textbox onlyNum mandatory"  placeholder="Enter account number"
							maxlength="4" />
					</div>
					
				</form>
				<div class="form-group Bhartipay_mt10">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Recharge" class="payplutus_button submit-form" onclick="submitVaForm('#landline')">
					</div>
			</div>
		</div>
	</div>
</div>
