<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script type="text/javascript">
$(document).ready(function() {
	
	setCurrentDate()
    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	     useCurrent:true
	    });
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
	 
	 
	 function setCurrentDate(){
		 var d = new Date();
		 var month =  ["Jan","Feb","March","April","May", "Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
		 $("#dpStart").val(d.getDate() + "-" + month[d.getMonth()] + "-" + d.getFullYear())
		}
 </script>


 
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

	<div id="main" role="main"> 
	    <div id="content">       
	        <div class="row"> 

				<div class="col-md-12" id="hidethis2">
				    <div class="box">
				        <div class="box-inner">
				            <div class="box-header">
				                <h2><i class="glyphicon "></i>Partner Prefund</h2> 
				            </div>
				            <div class="box-content"> 
				               
								<form action="SavePrefund" id="prefund" method="post">
							
									<div class="row">
										<div class="col-md-12">
											
											<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
											<font color="red"><s:actionerror/> </font>
											<font color="blue"><s:actionmessage/> </font> 

											<div class="row"> 
												<div class="col-md-4 form-group">
													<label for="apptxt">Select Partner Name</label>
												</div>
												<div class="col-md-4 form-group">
													<s:select list="%{partnerList}" headerKey="-1" headerValue="Please select Partner Name" id="partnerid" name="partnerPrefundBean.partnerName" cssClass="form-username" requiredLabel="true" />
												</div>
											</div>
			
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Select Transaction Type</label>
												</div>
												<div class="col-md-4 form-group">
													<s:select list="%{tranType}" headerKey="-1" headerValue="Please select Transaction Type" id="txnType" name="partnerPrefundBean.txnType" cssClass="form-username" requiredLabel="true"  required="required"/>
												</div>
											</div>
																
												
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Transaction Amount</label>
												</div>
												<div class="col-md-4 form-group">
													<input class="form-control mandatory amount-vl2" name="partnerPrefundBean.amount" type="text" id="amount" maxlength="8" placeholder="Enter Amount" required="required"/>
												</div>
											</div>
														
														
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Select Transaction Source</label>
												</div>
												<div class="col-md-4 form-group">
													<s:select list="%{txnSourceList}" headerKey="-1" headerValue="Please select Transaction Source" id="txnSource" name="partnerPrefundBean.txnSource" cssClass="form-username" requiredLabel="true"  required="required"/>
												</div>
											</div>
												
																		
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Transaction Ref. Number</label>
												</div>
												<div class="col-md-4 form-group">
													<input class="form-control mandatory" name="partnerPrefundBean.txnRefNo" type="text" id="txnRefNo" maxlength="20" placeholder="Enter Transaction Ref. Number" required="required"/>
												</div>
											</div>
														
															
														
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Transaction Date</label>
												</div>
												<div class="col-md-4 form-group">
													<input type="text" name="partnerPrefundBean.stDate" id="dpStart" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/> 
												</div>
											</div>
								
																		
											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Partner's Ref. Number</label>
												</div>
												<div class="col-md-4 form-group">
													<input class="form-control  " name="partnerPrefundBean.partnerRefNo" type="text" id="partnerRefNo" maxlength="20" placeholder="Enter Partner's Ref. Number" />
												</div>
											</div>
														
														

											<div class="row">
												<div class="col-md-4 form-group">
													<label for="apptxt">Description</label>
												</div>
												<div class="col-md-4 form-group">
													<input class="form-control  " name="partnerPrefundBean.description" type="text" id="description" maxlength="200" placeholder="Enter Description" />
												</div>
											</div>	
														
											    <div class="col-md-4">
												    <div class="form-group"> 
														<input type="button" class="btn btn-success btn-fill submit-form" style="margin-top: 20px;"  value="Submit" onclick="submitVaForm('#prefund')" /> 

														<button type="reset" onclick="resetForm('#prefund')"  class="btn reset-form btn-info btn-fill" style="margin-top: 20px;">Reset</button> 
													</div>
												</div> 

										</div>
                                    </div>
								</form> 

						    </div>
				        </div> 
				    </div> 
				</div>

			</div> 
	    </div> 
	</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->




</body>
</html>


