
<%@page import="com.bhartipay.wallet.report.bean.AgentCurrentSummary"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wallet History - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            },{
			    extend: 'colvis',
			    columnText: function ( dt, idx, title ) 
			    {
			        return (idx+1)+': '+title;
			    }
			}
        ]
    } );
   

	} );

   
 </script>
 

</head>

<% 
	User user = (User) session.getAttribute("User");
	Date myDate = new Date(); 
	SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
	String toDate=format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from= cal.getTime();    
	String fromDate = format.format(from);
	Object[] commDetails=(Object[])session.getAttribute("commDetails");
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
	DecimalFormat d=new DecimalFormat("0.00"); 
%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
	<div id="main" role="main"> 
	    <div id="content">  
			<div class=" row">    
				<div class="col-md-12"> 
					<div class="box2">
						<div class="box-inner"> 
							<div class="box-header">
								<h2>Agents Current Summary</h2>
							</div>
						</div>
					</div>	 
											
					<div id="xyz">
						<table id="example" class="scrollD cell-border dataTable no-footer">
							<thead> 
								<tr>
									<th><u>Agent ID</u></th>
									<th><u>Name</u></th>
									<th><u>Type</u></th>
									<th><u>Debit Amount</u></th>
									<th><u>Credit Amount</u></th>
									
									<th><u>Balance</u></th>  
								</tr>
							</thead>
							<tbody>
							<%
							
							List<AgentCurrentSummary>list=(List<AgentCurrentSummary>)request.getAttribute("AgentCurrentSummary");

							if(list!=null){
							
							
							for(int i=0; i<list.size(); i++) {
								AgentCurrentSummary tdo=list.get(i);%>
					          		  <tr>
					         
					       	    <td><%=tdo.getAgentId()%></td>
					       	    <td><%=tdo.getName()%></td>
					       	    <td><%=tdo.getType()%></td>
					       	     <td><%=d.format(tdo.getTxnDebit())%></td>
					       	    <td><%=d.format(tdo.getTxnCredit())%></td>
					       	     <td><%=d.format(tdo.getFinalBalance())%></td>
					       	     
					       	   
					       	   
						      <%} }%>	
						        </tbody>		</table>
					</div> 

				</div>   

			</div>  
        </div>
    </div>
 
<jsp:include page="footer.jsp"></jsp:include>

	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



