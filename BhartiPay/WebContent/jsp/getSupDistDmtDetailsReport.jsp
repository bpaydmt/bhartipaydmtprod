<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 1, "asc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'DMT Details Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
  

    $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),


    });
    if($('#dpStart').val().length != 0){
    	var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
    	$('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
    	
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   
    
} );



	function converDateToJsFormat(date) {
	   
		var sDay = date.slice(0,2);
		var sMonth = date.slice(3,6);
		var yYear = date.slice(7,date.length)
	
		return sDay + " " +sMonth+ " " + yYear;
	}

 </script>
 <style>
#agentFooter, .paymentref{display:none}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Transaction Summary</h2>
		</div>
		
		<div class="box-content row">
		
			<form action="GetSupDistDmtDetailsReport" method="post" autocomplete="off">

<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> 
									<s:select list="%{distributorList}" headerKey="all"
												headerValue="All" id="distri"
												name="walletBean.distributerid" cssClass="form-username"
												requiredLabel="true"
												/>
											</div>		
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="walletBean.id" id="id"
										value="<%=user.getSubAggregatorId()%>"> 
										
										<input type="text"
										value="<s:property value='%{walletBean.stDate}'/>"
										name="walletBean.stDate" id="dpStart"
										class="form-control" value=""  placeholder="Start Date"
										data-language="en"/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="dpEnd"
													class="form-control " placeholder="End Date"
													data-language="en" >
											</div>
											
												
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th width="5%" style="word-wrap: break-word;"><u>Txndatetime</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>AgentId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Distributorid</th>
						<th width="5%" style="word-wrap: break-word;"><u>TxnId</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderMobileNo</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>SenderName</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneaccountno</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneifsccode</u></th>	
						<th width="5%" style="word-wrap: break-word;"><u>txnamount</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>beneid</u></th>
						<th width="5%"><u>Remittance status</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;" ><u>Via</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;" ><u>remittancetype</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancemode</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancestatus</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>paymentinfo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>BeneMobileNo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>banktranrefno</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>bankrrn</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>bankresponse</th>
						
						<th width="5%" style="word-wrap: break-word;display:none;"><u>updatedatetime</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>kyctype</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Aggregator Id</th>
						
							
					</tr>
				</thead>
				<tbody>
				<%
				List<DmtDetailsMastBean>list=(List<DmtDetailsMastBean>)request.getAttribute("agentList");

				if(list!=null){
				
					int j=1;
				for(int i=0; i<list.size(); i++) {
					DmtDetailsMastBean tdo=list.get(i);
					
					%>
		          	 <tr>
		          	 <td ><%=tdo.getTxndatetime() %></td>
		             <td><%=tdo.getSenderid()%></td>
		             <td><%=tdo.getAgentid() %></td>
		             <td><%=tdo.getDistributerid()%></td>
		             <td><%=tdo.getTxnid()%></td>
		             <td><%=tdo.getSendermobileno()%></td>
		             <td><%=tdo.getSendername()%></td>
		              <td><%=tdo.getBeneaccountno()%></td>
		             <td><%=tdo.getBeneifsccode() %></td>
		             <td><%=tdo.getTxnamount()%></td>
		             <td><%=tdo.getBeneid()!=null?tdo.getBeneid():"-"%></td>
		             <td><%=tdo.getRemittancestatus()%></td>
		              
		             <td style="word-wrap: break-word;display:none;"><%if(tdo.getTxnid().indexOf("FBTX")>=0){out.print("BANK");}else{out.print("WALLET");}%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancetype()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancemode()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancestatus()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getPaymentinfo()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBenemobileno() %></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBanktranrefno()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankrrn()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankresponse()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getUpdatedatetime()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getKyctype()%></td>
		            <td style="word-wrap: break-word;display:none;"><%=tdo.getAggreatorid()%></td>
                  	</tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>


</body>
</html>



