<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>



<style type="text/css">
 	.nav>li>a {
		padding: 10px;
		/* background-color: #87c440; */
	} 
	ul.main-menu li.active {
		margin-left: 0px;
		/*background-color: #87c440;*/
	} 
	nav ul ul {
    margin: 0;
    display: none;
    background: rgba(69, 69, 69, .6);
    /* padding: 7px 0; */
    padding: 3px 0px 0px 0px;
	}

</style>


</head>



	<!-- desktop-detected menu-on-top pace-done smart-style-3 fixed-header fixed-navigation fixed-page-footer -->
	<%
		User user = (User) session.getAttribute("User");
		Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
		System.out.println(mapResult);
		String domainName = mapResult.get("domainName");
		Map<String,String> menuMap=(Map<String,String>)session.getAttribute("menuMap");
		List<String> aggMenus=(List<String>)session.getAttribute("aggMenus");
		List<String> aggRoles=(List<String>)session.getAttribute("aggRoles");
		if(aggMenus==null){
			aggMenus=new ArrayList();
		}
		if(aggRoles==null){
			aggRoles=new ArrayList();
		}
		String userId=user.getId();
		boolean whiteLevel=false;
		if(user.getWhiteLabel()==1){
		whiteLevel=true;
		
		} 
%>

<body class="smart-style-3 menu-style<%=mapResult.get("themes")%> fixed-header menu-on-top fixed-navigation fixed-page-footer">

	<script>
	    function openDmt(isMaintenanceGoingOn){ 
		 closeDmitWindow() 
		 
		 if(isMaintenanceGoingOn == false){
		 $.get('CheckSession',function(result){
		  console.log(result)
		  if(result.status == "TRUE"){
		   MYWalletWINDOW = window.open('MoneyTransfer', "", "width=1366,height=800");
		  }else{
		     alert("Your session has expired. Please login again.")
		     window.location.replace("/BhartiPay/UserHome");
		  }
		  
		 })
		 }else{
		  $("#maintenancePop").modal('show')
		 }
		 
		} 

		function openDmtFromBak2(isMaintenanceGoingOn){ 
		 alert("isMaintenanceGoingOn"+isMaintenanceGoingOn);
		 if(isMaintenanceGoingOn == false){
		 $.get('CheckSession',function(result){
		  console.log(result)
		  if(result.status == "TRUE"){
			  return true;
	 
		  }else{
		     alert("Your session has expired. Please login again.")
		     window.location.replace("/BhartiPay/UserHome");
		  }
		  
		 })
		 }else{
		  $("#maintenancePop").modal('show')
		 } 
		} 
	</script>



	<aside id="left-panel"> 

		<!-- User info -->
		<div class="login-info"> 
			<!-- <span style="padding-left: 15px;">  
				<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut" style="margin-top: 8px;"> 
					<i class="fa fa-user user-icon-colo" aria-hidden="true"></i>
					<span style="margin-left: 3px;margin-right: 3px;color: #fff!important;">
						User Name 
					</span>
					<i class="fa fa-angle-down user-icon-colo"></i>
				</a> 
				
			</span> --> 
		</div>
		<!-- end user info -->

		<nav class="left-menu">   
            <ul class="nav nav-pills nav-stacked main-menu">
					
					<li class="nav-header"></li>
                    <li id="s">
                    	<a class="ajax-link" href="Home" style="box-shadow: inset 0px 0px 0px 0px #434756;">
                    		<i class="glyphicon glyphicon-home"></i>
                    		<%-- <span class="menuIconcolomn"></span> --%>
							<span class="menu-item-parent">Home</span>
						</a>
					</li>
						  		
					<%
						if (user.getUsertype() == 99) {	
					%>
	
					<li id="s">
						<a class="ajax-link" href="CreateUser">
							<i class="glyphicon glyphicon-user"></i>
							<span class="menuIconcolomn"></span>
					        <span>Create User</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="WalletConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<span class="menuIconcolomn"></span>
					        <span>Wallet Configuration</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="SMSConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<!--  -->
					        <span >SMS Configuration</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="EmailConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<!-- <span  class="menuIconcolomn"></span> -->
					        <span >Email Configuration</span>
					    </a>
					</li>
							
				    <li id="s">
				    	<a class="ajax-link" href="AuthoriseAggregator">
				    		<i class="glyphicon glyphicon-edit"></i>
				    		<!-- <span  class="menuIconcolomn"></span> -->
					        <span >Authorise Aggregator</span>
					    </a>
					</li>
							

					<%-- <li id="ea">
						<a class="ajax-link" href="ShowProfile">
							<i class="glyphicon glyphicon-edit" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
					        <span style="padding: 8px;"> Edit Profile</span>
					    </a>
					</li> --%>

					<%-- <li id="rp">
						<a class="ajax-link" href="ResetPassword">
							<i class="glyphicon glyphicon-lock" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
							<span style="padding: 8px;"> Change Password</span>
						</a>
					</li> --%>
							
					<%-- <li id="rp">
						<a class="ajax-link" href="GetFaq"  >
							<i class="glyphicon glyphicon-lock" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
							<span style="padding: 8px;"> FAQ</span>
						</a>
					</li> --%>
							
					<!-- <li id="rp">
						<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
							<i class="glyphicon glyphicon-lock" ></i>
							<span  class="menuIconcolomn"></span>
                            <span style="padding: 8px;"> Grievance Policy </span>
                        </a>
                    </li> --> 

					<%}else if (user.getUsertype() == 1) { %>	

					<li id="aggFT"> 
						<a href="#"> 
							 <!-- <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  --> 
							<i class="glyphicon glyphicon-folder-close" ></i> 
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>  
						</ul>
					</li>
					
					<li id="s">
					    <a class="ajax-link" href="Recharge"> 
							  <!-- <i>
							    <img src="./image/recharges-menu.png" style="width: 30%;">
							  </i> -->
							  <i class="glyphicon glyphicon-phone" ></i>
							  <span style="">Recharges</span>
						</a>
					</li>
							


							
							
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style="">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							<%if (user.getUsertype() == 3){%>
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							<%} %>
						
						</ul>
					</li>
					
					<li id="mb2">
						<a class="ajax-link" href="OrdersView">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Orders View</span>
					    </a>
					</li>
					
					<li id="ctl">
						<a class="ajax-link" href="WalletToWallet">
							<i class="glyphicon glyphicon-cloud-upload" ></i>
							
					        <span >Wallet to Wallet</span>
					    </a>
					</li>
					
					<li id="ctl">
						<a class="ajax-link" href="KycUpload">
							<i class="glyphicon glyphicon-cloud-upload" ></i>
							
					        <span >Upload KYC</span>
					    </a>
					</li>
					
					<%-- <li id="ea">
						<a class="ajax-link" href="CustomerSupport">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					</li> --%>
					
					
					<%-- <li id="ea">
						<a class="ajax-link" href="ShowProfile">
							<i class="glyphicon glyphicon-edit" ></i>
							
					        <span style="padding: 8px;"> Edit Profile</span>
					    </a>
					</li> --%>

					<%-- <li id="rp">
						<a class="ajax-link" href="ResetPassword">
							<i class="glyphicon glyphicon-lock" ></i>
							
							<span style="padding: 8px;"> Change Password</span>
						</a>
					</li> --%>
							
					<%-- <li id="rp">
						<a class="ajax-link" href="GetFaq"  >
							<i class="glyphicon glyphicon-lock" ></i>
							
							<span style="padding: 8px;"> FAQ</span>
						</a>
					</li> --%>
							
					<!-- <li id="rp">
						<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
							<i class="glyphicon glyphicon-lock" ></i>
							
                            <span style="padding: 8px;"> Grievance Policy </span>
                        </a>
                    </li> -->


					<%}else if (user.getUsertype() == 2) { %>


					<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>

					 <%-- <li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i>
							
					        <span style="padding: 8px;">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Retailer</a>
							</li> 
						</ul>
					</li>  --%>
					
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					
					<li id="s">
					    <a class="ajax-link" href="Recharge"> 
							  <!-- <i>
							    <img src="./image/recharges-menu.png" style="width: 30%;">
							  </i> -->
							   <i class="glyphicon glyphicon-phone" ></i>
							  <span style="">Recharges</span>
						</a>
					</li>
							

							


					<%
					if(user.getBank()==0 && user.getWallet() == 1){
						%>
								 
					<li id="rp"> 	
							<a href="#"> 
								<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
								<i class="glyphicon glyphicon-transfer" ></i>
								<span style=" ">Money Transfer</span>
							</a>
							<ul class="nav nav-pills nav-stacked"> 
								<li id="mtBank">
								    <a onclick="openDmtFromBak2(true)" href="javascript:void(0)">Bank</a>
								</li>
								<li id="refundAgentLink">
									<a  href="agentRefund">Refund</a>
								</li> 
								<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 	
							</ul> 
						</a>
					</li>
								
								
					<%}else if(user.getWallet() == 0 && user.getBank()== 1 && !user.getId().equalsIgnoreCase("BPA001056")){%>
									 
					<li id="rp"> 	
						<a href="#"> 
							<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
							<i class="glyphicon glyphicon-transfer" ></i>
							<span style=" ">Money Transfer</span>
						</a>
						<ul class="nav nav-pills nav-stacked"> 
							<li id="mtBank">
								<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK">FINO</a>
							</li> 
							
							<li id="mtBank">
								<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK2">Bank</a>
							</li> 
							
							<li id="refundAgentLink">
								<a  href="agentRefund">Refund</a>
							</li>
							<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 
						</ul> 
						</a>
					</li> 

					<%}else if(user.getWallet() == 1 && user.getBank()== 1){%> 

					<li id="rp"> 
						<a href="#"> 
							<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
							<i class="glyphicon glyphicon-transfer" ></i>
							<span style=" ">Money Transfer</span></a>
							<ul class="nav nav-pills nav-stacked"> 
								<li id="mtBank">
									<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK">FINO</a>
								</li> 
								
								<li id="mtBank">
									<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK2">Bank</a>
								</li> 
								
								<li id="refundAgentLink">
									<a  href="agentRefund">Refund</a>
								</li>
								<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 
							</ul> 
							</a>
						</li>
								
						<%}
					        if(domainName.toLowerCase().indexOf("transpaytech.co.in")>-1||domainName.toLowerCase().indexOf("b2b.ezeejunction.com")>-1
							 ||domainName.toLowerCase().indexOf("partner.bhartipay.com")>-1 || domainName.toLowerCase().indexOf("localhost")>-1){
						%>

						<!-- <li id="rp"> 
						    <a href="#"> 
						    	<i class="glyphicon glyphicon glyphicon-plane" ></i>
						    	
							    <span style="padding: 8px;">Travel </span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<li id="mtWallet">
									<a  href="flightBooking">Flight Booking</a>
								</li>
								<li id="mtBank">
									<a href="getFlightDetail">Flight Details</a>
								</li> 
							</ul> 
							</a>
						</li> -->


						<%} 
					        
					        if("1".equalsIgnoreCase(user.getAepsChannel())){%> 

							<li id="ea">
								<a class="ajax-link" href="ASAEPSPayments">
									<i>
									   <img src="./image/aeps-icon.png">
									</i> 
									<!-- <i class="glyphicon glyphicon-hand-down" ></i> -->
							        <span style=" ">AEPS</span>
							    </a>
							</li> 
							 <%}
					        else if("2".equalsIgnoreCase(user.getAepsChannel())){ %> 	
					        <%-- <%}else if (user.getUsertype() == 2) { %> --%>
							<li id="ea">
								<a class="ajax-link" href="AepsCall">
									<i>
									   <img src="./image/aeps-icon.png">
									</i> 
									<!-- <i class="glyphicon glyphicon-hand-down" ></i> -->
							        <span style=" ">AEPS</span>
							    </a>
							</li>
							
								<%}   
						
						
						%> 
						
						<li id="ea">
								<a class="ajax-link" href="MatmCall">
									<i>
									   <img src="./image/m-ATM.png">
									</i> 
									<!-- <i class="glyphicon glyphicon-print" ></i> -->
							        <span style="">m-ATM</span>
							    </a>
						</li>
						
							

						<%-- <li id="ea">
							<a class="ajax-link" href="AEPSPayments">
								<i class="glyphicon glyphicon-transfer" ></i>
								
						        <span style="padding: 8px;">AEPS</span>
						    </a>
						</li> --%>
							
							
					 	<li id="u">
					 		<a href="#"> 
					 			<i class="glyphicon glyphicon-list-alt" ></i>
					 			
							    <span style=" ">Report</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<%if(user.getAgentCode() != null && !user.getAgentCode().isEmpty())
								{%>
								    <li>
								    	<a href="GetAepsReport">AEPS</a>
								    </li>
								    <li>
								    	<a href="GetMatmReport">m-ATM</a>
								    </li>
								<%} %>
								<li>
									<a href="GetTransacationLedgerDtlBK">Money Transfer</a>
								</li>
							
								<li>
									<a href="RechargeReport">Recharge</a>
								</li>
								<!-- <li >
									<a href="ShowCashBackPassbook">Cash Back Wallet History</a>
								</li>
								<li>
									<a href="GetSenderKycList">Sender KYC List</a>
								</li> -->
								
							</ul> 
						</li> 
							
						<li id="#">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-download-alt" ></i>
								
						        <span style="">Download</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
								
								<li >
									<a href="https://partner.finopaymentbank.in/Testcamerabio.html" target="_blank">Driver</a>
								</li>
								<li>
									<a href="#">Certificate</a>
								</li> 
							</ul> 
						</li>
						
						<!--  <li id="#">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-bold" style="font-size: 18px;"></i>
								
						        <span style="padding: 7px;">BBPS</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
								
								<li >
									<a href="bbpsServices">Pay Bill</a>
								</li>
								<li>
									<a href="#">Register Complaint</a>
								</li> 
								<li>
									<a href="#">Check Complaint Status</a>
								</li>
							</ul>
 						</li>-->
						<%-- <li id="ea">
							<a class="ajax-link" href="CustomerSupport">
								<i class="glyphicon glyphicon-headphones" ></i>
								
						        <span style="padding: 7px;">Customer Support</span>
						    </a>
						</li> --%>
						
						
						
						<%-- <li id="ea">
							<a class="ajax-link" href="ShowProfile">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span style="padding: 8px;"> Profile</span>
						    </a>
						</li> --%>

					    <%-- <li id="rp">
					    	<a class="ajax-link" href="ResetPassword">
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> Change Password</span>
							</a>
						</li> --%>
							
					    <%-- <li id="rp">
					    	<a class="ajax-link" href="GetFaq"  >
					    		<i class="glyphicon glyphicon-question-sign" ></i>
					    		
							    <span style="padding: 8px;"> FAQ</span>
							</a>
						</li> --%>
							
						<!-- <li id="rp">
							<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
								<i class="glyphicon glyphicon-asterisk" ></i>
								
                                <span style="padding: 8px;"> Grievance Policy </span>
                            </a>
                        </li> -->

						<%}else if (user.getUsertype() == 3) { %>

						<li id="s">
							<a class="ajax-link" href="GetAgentDtlsByDistId">
								<i class="glyphicon glyphicon-edit" ></i> 
						        <span>Agents Dashboard</span>
						    </a>
						</li>
													
						<li id="ft"> 
							<a href="#"> 
								<!-- <i>
								   <img src="./image/Wallet-Update.png" style="width:30%;">
								</i> -->  
								<i class="glyphicon glyphicon-folder-close" ></i>
								<span style="">Wallet Update</span>
							</a>
							<ul class="nav nav-pills nav-stacked">  
								<li id="cashDeposit">
									<!-- <a class="ajax-link" href="CashDeposit">Cash Deposit
									</a> -->
									
									<a class="ajax-link" href="CashDepositRequest">Cash Deposit</a>
								</li>
								<li id="addMoney">
									<a class="ajax-link" href="AddMoney">Online</a>
								</li>
								<li id="#">
									<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
								</li>
							</ul>
						</li>
						<%if (user.getUsertype() == 3){%>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%}else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>
							
						<li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	<li id="ags">
									<a href="MovementReportByDist">Cash Movement Report</a>
								</li>
								<li id="ags">
									<a href="CommissionSummary">Commission  Summary</a>
								</li>
								<li id="ags">
									<a href="GetTransacationLedgerDtlBK">Money Transfer</a>
								</li>
								<li id="ags">
									<a href="GetAepsReportByDistAgg">AEPS</a>
								</li>
								<li id="ags">
									<a href="GetMatmReport">m-ATM</a>
								</li> 
								<li id="ags">
									<a href="RechargeReport">Recharge</a>
								</li>  
							</ul>
						</li> 

						<%}else if(user.getUsertype() == 7){
						%> 

						<li id="s">
							<a class="ajax-link" href="DistributorOnBoard">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >Distributor On Board</span>
						    </a>
						</li>
							
							 
							
						<%if (user.getUsertype() == 3){%>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%}else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>
							
						<li id="mb1">
							<a class="ajax-link " href="GetAgentDtlsBySupDistId">
								<i class="glyphicon glyphicon-book" ></i>
								
						        <span >Dashboard</span>
						    </a>
						</li>
						
						<li id="mb1">
							<a class="ajax-link " href="CashDepositsByDist">
								<i class="glyphicon glyphicon-book"></i>
								
						        <span >Cash Deposit Request</span>
						    </a>
						</li>
						 
						
						<li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	<li id="ags">
									<a href="MovementReportByDist">Cash Movement Report</a>
								</li>
								<li id="ags">
									<a href="CommissionSummary">Commission  Summary</a>
								</li>
								<li id="ags">
									<a href="#">DMT</a>
								</li>
								<li id="ags">
									<a href="#">AEPS</a>
								</li>
								<li id="ags">
									<a href="#">m-ATM</a>
								</li> 
								<li id="ags">
									<a href="#">Recharge</a>
								</li>  
							</ul>
						</li>

						<%-- <li id="ea">
							<a class="ajax-link" href="ShowProfile">
								<i class="glyphicon glyphicon-edit" ></i>
								
							    <span style="padding: 8px;"> Profile</span>
							</a>
						</li> --%>
							
				 

					    <%-- <li id="rp">
					    	<a class="ajax-link" href="ResetPassword">
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> Change Password</span>
							</a>
						</li> --%>
							
						<%-- <li id="rp">
							<a class="ajax-link" href="GetFaq"  >
								<i class="glyphicon glyphicon-lock" ></i>
								
								<span style="padding: 8px;"> FAQ</span>
							</a>
						</li> --%>
							
						<!-- <li id="rp">
							<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
								<i class="glyphicon glyphicon-lock" ></i>
								
                                <span style="padding: 8px;"> Grievance Policy </span>
                            </a>
                        </li> -->
						

						<%
						}else if (user.getUsertype() == 4) { %>

						<%if(domainName.equalsIgnoreCase("pookcity.partner.bhartipay.com")){ %>
											
						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
					            <span style="">Reports</span>
					        </a>
							<ul class="nav nav-pills nav-stacked">		
								<li id="recha">
									<a href="RechargeReportAggreator">Recharges and bill payments</a>
								</li>
								<li id="rech">
								<li id="rech">
									<a href="CashDepositsRpt">Cash Deposits</a>
								</li>
							</ul>
					    </li> 	
							
						<%}else{ %>
							
						<li id="s">
							<a class="ajax-link" href="AssignUserRole">
								<i class="glyphicon glyphicon-edit" ></i>
								
						        <span >User Role Mapping</span>
						    </a>
						</li> 

						<%if(whiteLevel != true) {%>
						
						
							
							
						<!-- <li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
							    <span >Merchant</span>
							</a>
						    <ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="AddMerchant">Add Merchant</a>
								</li>
								 <li id="cu">
								 	<a href="TPMerchantList">Merchant List</a>
								 </li> 
							</ul>
						</li>-->
						
						<%}%>
						
						
						<%if (user.getUsertype() == 3){%>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%} else if((user.getUsertype() == 4 || user.getUsertype() == 6 ) && whiteLevel){%>
						
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Revenue Wallet
								</a>
							</li>
							
							<li id="rech">
									<a href="showCashDeposite">Deposit Wallet</a>
							</li>
							
						</ul>
					</li>
						
						
						<% }else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>

						<%}}%> 

						<%

                        if (user.getUsertype() == 6 ||user.getUsertype() == 4 ||user.getUsertype() == 3) { 

						if((user.getUsertype() == 4) && (domainName.toLowerCase().indexOf("partner.bhartipay.com")>-1 ||domainName.toLowerCase().indexOf("transpaytech.co.in")>-1 ||domainName.toLowerCase().indexOf("b2b.ezeejunction.com")>-1)) {
						%>
						<!-- <li id="ac">
							<a class="ajax-link " href="AssignCommission">
								<i class="glyphicon glyphicon-book" ></i>
								
								<span >Assign
								Commission</span>
							</a>
						</li>--> 

						<%
						}
		                if((aggRoles.contains("9013") || aggRoles.contains("9003")) && user.getUsertype() != 3){  
						if(aggRoles.contains("9003")){%>
						<li id="u">
						<a class="ajax-link" href="StartCustomerCare">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					   </li>
					   <%}%>

						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<%if(aggRoles.contains("9010")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
								<li id="rech">
									<a href="GetAgentBalDetail">Agent Wallet Balance</a>
								</li>
								<li id="rech">
									<a href="ShowPassbookById">Agent Wallet Summary</a>
								</li>
									
								<%}if(aggRoles.contains("9003")){ %>
									
								<li id="rech">
									<a href="GetRevenue">Revenue Report</a>
								</li>
								<li id="rech">
									<a href="GetDmtDetailsReport">Money Transfer</a>
								</li>
								<li id="rech">
									<a href="GetAepsReportByDistAgg">AEPS Report</a>
								</li>
								<li id="recha">
									<a href="RechargeReportAggreator">Recharges and bill payments</a>
								</li>
								
								
								
							
								
									
							
								<li id="rech">
									<a href="CashDepositsRpt">Cash Deposits Report</a>
								</li>
								<%} %>
							</ul>
						</li> 	
							
						 
							
								
							

						<%} if(aggRoles.contains("9004")){  %>

						 <li >
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span style="padding: 8px;">CME</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						        <li id="cu">
						        	<a href="CMERequest">CME Request</a>
						        </li>
						 
								<li id="cu">
									<a href="CMETransactionHistory">View CME Requests</a>
								</li>
								<li id="cu">
									<a href="CMEAcountStatement">CME Account Statement</a>
								</li>
								<li id="cu">
									<a href="CMETOAgentTransfer">CME TO Agent Transfer</a>
								</li>
								<li id="cu">
									<a href="DMTBalance">DMT Balance</a>
								</li>

							</ul>
						</li> 

						<%} if(aggRoles.contains("9005")&& user.getUsertype() != 7){  %> 

				        <li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">AOB Maker</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<li id="aob">
									<a href="AgentOnBoard">Agent On Board</a>
								</li>
								<li id="da">
									<a href="DeclinedAgent">Declined Agent</a>
								</li>
								<li id="alist">
									<a href="AgentList">Agent List</a>
								</li>
							</ul>
						</li>  

						<%}if(aggRoles.contains("9007") && user.getUsertype() != 3&& user.getUsertype() != 7){  %> 

						<li >
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >Support Tickets</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="SupportTickets">Open Tickets</a>
								</li>
								<li id="cu">
									<a href="SupportTicketList">Ticket List</a>
								</li> 
							</ul>
						</li> 

						
						<%}if(aggRoles.contains("9008")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
 
							
							
						<li id="checker">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >Cash Deposits</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<li id="cashD">
									<a href="CashDeposits">Checker</a>
								</li> 
								<%if(aggRoles.contains("9014")) {%>
								<li id="cashA">
									<a href="CashDepositApproveList">Approver</a>
								</li> 
								<%}%>
								<!-- <li id="sepan">
									<a href="GetSenderPanF60List">Sender PAN List</a>
								</li>-->
								
								<!-- <li id="sepan">
									<a href="GetSenderKycList">Sender KYC List</a>
								</li>-->
							</ul>
						</li> 


						<%}if(aggRoles.contains("9009")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>

						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >AOB Checker</span>
						    </a>
					        <ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="NewAgents">Pending</a>
								</li>
								 <li id="cu">
								 	<a href="GetRejectAgentList">Reject</a>
								 </li>
								 <li id="cu">
								 	<a href="GetApprovedAgentList">Approved</a>
								 </li>
							</ul>
						</li>


						<%}if(aggRoles.contains("9010")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>

						<li id="s" >
							<a class="ajax-link" href="CreateUser">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >Create User</span>
						    </a>
						</li>
						
						<!-- <li id="s">
							<a class="ajax-link" href="SearchUser">
								<i class="glyphicon glyphicon-search" ></i>
							
						        <span >Search User</span>
						    </a>
						</li>-->
						
						<li id="distri">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-edit" ></i>
								
						        <span >Distributor</span>
						    </a>
						
						    <ul class="nav nav-pills nav-stacked">
							    <li id="s">
							    	<a class="ajax-link" href=" DistributorOnBoardList">
							    		<i class="glyphicon glyphicon-edit" ></i>
							    		
										<span >Distributor On Board List</span>
									</a>
								</li>
								<li id="s">
									<a class="ajax-link" href="DistributorOnBoard">
										<i class="glyphicon glyphicon-edit" ></i>
										
								        <span >Distributor On Board</span>
								    </a>
								</li>
							</ul> 
						</li>
							
					    
					    <li id="#">
								<a href="#"> 
									<i class="glyphicon glyphicon-stats"></i>
									
							        <span >Utilities</span>
							    </a>
								<ul class="nav nav-pills nav-stacked">
									<li id="#">
										<a href="#">User Details</a>
									</li> 
									<li id="#">
										<a href="UtilProfileChange">Contact Info</a>
									</li>
									<li id="#">
										<a href="SubAggregatorUtility">Change Mapping</a>
									</li>
									<li id="#">
										<a href="#">Aeps</a>
									</li>
								</ul>
						</li>
							


						<%} if(aggRoles.contains("9011")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
				 
						
							<%if(whiteLevel != true){ %>
								
							<li id="u">
							
						    <a href="AgentDetailForApprove"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >AOB Approver</span>
						    </a>
						    </li>
						<%} %>  
						 

						<%
						}if(aggRoles.contains("9013")&& user.getUsertype() != 3){
							if(whiteLevel != true){
						%>
	
						<li id="s">
							<a class="ajax-link" href="RefundDMTTxn">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >DMT Refund</span>
						    </a>
						</li>
		
						<%
							}
							}if(aggRoles.contains("9013") && user.getUsertype() != 3&& user.getUsertype() != 7){  %>
	
	                    <li id="u">
	                    	<a href="#"> 
	                    		<i class="glyphicon glyphicon-list-alt" ></i>
	                    		
							    <span style="">Reports</span>
							</a>
							<ul class="nav nav-pills nav-stacked"> 
								<li id="rech">
									<a href="GetDmtDetailsReport">Money Transfer</a>
								</li>
								<li id="rech">
									<a href="showCashDeposite">Wallet Transactions</a>
								</li>
								<li id="rech">
									<a href="CashDepositsRpt">Cash Deposits</a>
								</li>
							    <li id="rech">
									<a href="GetRefundTransactionReport">Refund</a>
								</li>
							    <li id="sepan">
									<a href="getRevokeList">Revoked List</a>
								</li>
							    <li id="recha">
									<a href="GetAgentClosingBalReport">Agent Balance</a>
								</li>	
									<li id="cu">
									<a href="GetWalletTxnDetailsReport">Wallet Transaction Summary</a>
								</li>
								<!-- <li id="rech">
									<a href="showCashDeposite">Wallet Update History</a>
								</li> -->
							</ul>
						</li> 

						<%}%>


                        <%if(!domainName.equalsIgnoreCase("pookcity.partner.bhartipay.com")){  %>
						<%-- <li id="ea">
							<a class="ajax-link" href="ShowProfile">
								<i class="glyphicon glyphicon-edit" ></i>
								
							    <span style="padding: 8px;">Profile</span>
							</a>
						</li> --%>

						<%} %>

					    <%-- <li id="rp">
					    	<a class="ajax-link" href="ResetPassword">
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> Change Password</span>
							</a>
						</li> --%>

						<%if(user.getUsertype()==2){ %>	

					   <%--  <li id="rp">
					    	<a class="ajax-link" href="GetFaq"  >
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> FAQ</span>
							</a>
						</li> --%>
					
						<!-- <li id="rp">
							<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
								<i class="glyphicon glyphicon-lock"  ></i>
								<span   class="menuIconcolomn"></span>
	                            <span style="padding: 8px;"> Grievance Policy </span>
	                        </a>
	                    </li> -->

						       <%} %> 
						<%} %> 
					</ul>  
		</nav> 

		<span class="minifyme" data-action="minifyMenu"> 
			<i class="fa fa-arrow-circle-left hit"></i> 
		</span>
    </aside>




	<div class="col-sm-3 col-lg-3 leftcol">
		<div class="sidebar-nav">
			<div class="nav-canvas">
				<div class="nav-sm nav nav-stacked"></div>

			</div>
		</div>
	</div>

	<!-- <script src="./js/Newtheme-bootstrap.min.js"></script>  -->
	<%-- <script src="../js/Newtheme-Mainbhartipay.js"></script> --%>
	<script src="./js/Newtheme-app.config.js"></script>
	<script src="./js/Newtheme-jquery.ui.touch-punch.min.js"></script>
	<!-- <script src="./js/Newtheme-bootstrap.min.js"></script> -->
	<script src="./js/Newtheme-jarvis.widget.min.js"></script>
	<script src="./js/Newtheme-app.min.js"></script>
	<%-- <script src="./js/Newtheme-fontowesome.js"></script> --%>
		
	<!-- <script src="./js/Newtheme-bhartipay-for-toggle.js"></script> -->

</html>


