<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>  
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 
 
 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             
        ]
    } );
    

    
} );



function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}


 

 </script>
 

</head>

<body>
<%
    User user = (User) session.getAttribute("User");   
%>             
    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
  
     <div class="box2">
	  <div class="box-inner">
	    <div class="box-header">
		   <!-- 	<h2>Member List</h2>  -->
		 <!--   <div> 
		      
		   <td><a href=""  > Manager </a></td> &nbsp;&nbsp;&nbsp; <td><a href=""  >  Super Distributor  </a></td> &nbsp;&nbsp;&nbsp; <td><a href=""  >Distributor</a></td> 
		   <td><button type="button" name="Aggregator">Aggregator</button></td>
		   <td><button type="submit" form="form1" value="Submit">Submit</button></td>
		   
		   </div>
		 -->
		</div>
		
		<div class="box-content">
		
<!-- 		 							
  						
		<div id="xyz">
			<table id="example" class="display" width="100%">
		  	<thead>
			  <tr>
				<th width="5%"><u>Sr.No</u></th>
				<th><u>User Id</u></th>
				<th><u>User Name</u></th>
				<th><u>Wallet Id</u></th>
				<th><u>Amount</u></th>
				<th><u>Action</u></th>
				</tr>
			  </thead>
			<tbody>
				 
			</tbody>
			</table>
		</div>
 
 -->
  		</div>
    </div>
	</div> 
</div>
</div>

<!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


