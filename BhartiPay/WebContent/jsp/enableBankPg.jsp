<%@page import="com.bhartipay.wallet.report.bean.RefundTransactionBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean"%>
<%@page import="com.bhartipay.customerCare.vo.AgentCustDetails"%>
<%@page import="com.bhartipay.customerCare.vo.CustomerCareResponse"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 

<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
  
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Activate Serives</h2>
		</div>
		 
		 <form action="PendingRechargeTxn" id="dmtForm" method="post">
          <div class="row"> 
			<font color="red"><s:actionerror/> </font>
		
		
					<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  -->
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
								 <input type="text"
										name="rechargeBean.rechargeId" id="rechargeId"
										class="form-control" placeholder="Txnid"
										 required/>
										<%--  value="<s:property value='%{rechargeBean.rechargeId}'/>" --%>
								<input type="hidden" name="rechargeBean.status" id="status" />		 
								</div>
								<div class="form-group  col-md-5 col-sm-3 txtnew col-xs-6">
								 
								 </div>
								<div
									class="form-group col-md-12 txtnew col-sm-12 col-xs-12 text-left 	">
									<!--  -->
									<div  id="wwctrl_submit">
										<input class="btn btn-info" onclick="refundStatus('refund',event,'pg')"
											type="submit" value="PG Enable">
										<input class="btn btn-info" onclick="enablebank3('refund',event,'bank3')"
											type="submit" value="Bank3 Enable">	 
									</div>
								</div>
		
		
		
		
							 
			</div>
			</form> 
		</div>
        </div>


  <div class="col-md-12">

	<table id="example" class="scrollD cell-border dataTable" width="100%">
    <thead>
      <tr>
        <th>Sno.</th>
        <th>UserId</th>
        <th>SO Id</th>
        <th>SM Id</th>
        <th>PG</th>
        <th>Bank3</th> 
         
        
     
      </tr>
    </thead>
    <tbody>
     <% 
 List<WalletMastBean> list=(List<WalletMastBean>)request.getAttribute("markPending");  
 if(list.size()>0)
 {
	 int count=0;
   for(WalletMastBean data:list){	 
    count++;
 %>
    
     <tr>
       <td><%=  count %></td>
       <td><%=  data.getId()  %></td>
       <td><%=  data.getSalesId() %></td>
       <td><%=  data.getManagerId() %></td>
       <td><%=  data.getOnlineMoney() %></td>
       <td><%=  data.getBank3() %></td> 
           
    </tr>
    
    <%}} %>
    
 
      
    </tbody>
  </table>
					
   </div>




	 </div>
	</div>	
    </div>
					
      </div>
        </div>
	 
</div><!--/.fluid-container-->


<script>
 
function refundStatus(flag,e,pg){

	debugger
	e.preventDefault();
	var f = $("#dmtForm")
    $("#status").val(pg);
	
	var r = confirm("Do you want to enable PG service for this Agent.");
	if (r == true) {
		f.attr("action", "EnablePg")
	   f.submit()
	} 
	 
}

function enablebank3(flag,e,bank)
{
	debugger
	e.preventDefault();
	var f = $("#dmtForm")
	$("#status").val(bank);
	var r = confirm("Do you want to mark enable bank3 for this Agent.");
	if (r == true) {
		f.attr("action", "EnablePg")
	   f.submit()
	}
}

</script>

<jsp:include page="footer.jsp"></jsp:include>

<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>


</body>
</html>


