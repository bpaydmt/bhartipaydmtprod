<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%
WalletMastBean mastBean =(WalletMastBean) request.getAttribute("agentDetails");
String logo=(String)session.getAttribute("logo");
User user =(User) session.getAttribute("User");

%>

	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
		<style type="text/css">
			*{
				margin: 0px;
				padding: 0px;
				box-sizing: border-box;
				font-family: 'Roboto Slab', serif;
			}
			.wrapper{
				float: left;
				width: 600px;
				padding: 0px 0px;
			}
			.innerWrapper{
				float: left;
				width: 100%;
				border-top: 2px solid #ccc;
			}
			.logo{
				float: left;
				padding: 10px 10px;
				width: 175px;
				height: 75px;
				justify-content: left;
				align-items: center;
				display: flex;
			}
			.logo img{
				width: 85%;
			}
			.heading{
				float: left;
				margin: 0px;
				text-align: right;
				width: 100%;
				padding: 0px 10px 0px 10px;
				font-size: 22px;
				color: #e4322e;

			}
			.detaileWrapper{
				float: left;
				width: 100%;
				display: flex;
				justify-content: center;
				align-items: end;
				flex-flow: row;
				padding: 10px;
				padding-bottom: 10px;
			}
			.userimage{
				width: 210px;
				height: 210px;
				border: 1px solid #000;
				padding: 10px;
				box-sizing: border-box;
			}
			.userimage img{
				width: 100%;
				height: 100%;
			}
			.userdDetaiWraper{
				float: left;
				width: 100%;
				display: flex;
				flex-flow: column;
				position: relative;
			}
			.userdDetaiWraper h3{
				text-align: center;
				font-size: 18px;
				color: #13096b;
			}
			.userdDetaiWraper h2{
				background-color: #e4322e;
				color: #fff;
				font-size: 15px;
				text-align: center;
				padding: 5px;
				width: 95%;
				margin: auto;
			}
			.userdDetaiWraper .inputField{
				margin: 5px 0px 0px 15px;
			}
			.userdDetaiWraper .inputField p { 
				float: left;
				font-size: 18px;
				border-bottom: 1px dashed #000;
			}
			.userdDetaiWraper .inputField  span{ 
				float: left;
				font-size: 18px;

				font-weight: 600;
				margin-right: 10px;
			}
			.signatureWrapper{
				width: 147px;
				position: absolute;
				right: -2px;
				bottom: -30px;
				text-align: center;
				line-height: 22px;
				font-size: 13px;
				font-weight: 600;
			}

			.footer{
				float: left;
				width: 100%;
				background-color: #483d8c;
				color: #fff;
				text-align: center;
				font-size: 16px;
				padding: 6px;
				line-height: 28px;
			}
			.footer p{

			}
			.instructionWrapper{
				display: flex;
				justify-content: center;
				align-items: center;
				flex-flow: column;
				padding: 14px 0px 0px 0px;
			}
			.intructionHeading{
				font-size: 24px;
				background-color: #e4322e;
				color: #fff;
				font-weight: 600;
				padding: 4px 20px;
			}
			.instructionWrapper ul{
				float: left;
				width: 100%;
				padding: 15px 30px;
			}
			.instructionWrapper ul li{
				font-size: 16px;
				font-weight: 600;
				line-height: 30px;
			}
			.instructionWrapper .subheading{
				font-size: 22px;
				color: #140090;
				font-weight: 600;
				margin-bottom: 10px;
			}
			.addressWrapper{
				float: left;
				width: 100%;
				padding: 0px 45px;
			}
			.addressWrapper h2{
				text-align: center;
			}
			.addressWrapper p{
				text-align: center;
				font-size: 16px;
				line-height: 25px;
				margin: 10px 0px;
				font-weight: 600;
			}
			.topheader{

			}
			.topheader h3{
				width: 235px;
				text-align: right;
				line-height: 80px;
				float: left;
				font-size: 20px;
			}
		</style>
	</head>
	<body>
	
	<%
	if("OAGG001057".equalsIgnoreCase(mastBean.getAggreatorid())){
	%>
		<div class="wrapper">
			<div class="innerWrapper">
				<div class="topheader">
					<div class="logo">
						<img src="<%=logo%>" style=" width: 102px;">
					</div>
					<h3>Banking Correspondent of </h3>
					<div class="logo" style="float:right;padding-left: 0px;">
						<img src="./images/finoLogo.png">
					</div>
				</div>

				<h3 class="heading" style="color:#d91b5c">Essential Banking Services Provider to the country</h3>
				<div class="detaileWrapper">
					<div class="userimage">
						<img alt="Fix your photo here" src="data:image/gif;base64,<%=user.getUserImg()%>"/>
					</div>
					<div class="userdDetaiWraper">
						<h3>Banking & ATM Services</h3>
						<h2 style="background-color: #d91b5c">IDENTITY CARD - ProMoney</h2>
						<label class="inputField"><span>Name:</span> <p style="width: 321px;"><%=mastBean.getName()!=null ? mastBean.getName() :"N/A" %></p></label>
						<label class="inputField"><span>Merchant Id:</span><p style="width: 270px;"><%=mastBean.getId()!=null ? mastBean.getId() :"N/A"  %></p></label>
						<label class="inputField"><span>State:</span><p style="width: 327px;"> <%=mastBean.getTerritory()!=null ? mastBean.getTerritory() :"N/A"  %></p></label>
						<label class="inputField"><span>Department:</span><p style="width: 150px;">Business Associate</p></label>
						<label class="inputField"><span>DOJ:</span><p style="width: 207px;"><%=mastBean.getCreationDate()!=null ? mastBean.getCreationDate() :"N/A" %></p></label>
						<label class="inputField"><span>Valid Till:</span><p style="    width: 154px;">2021-12-31</p></label>
						<div class="signatureWrapper">
							<p style="position: relative;">
								(ISSUING AUTHORITY)<br>
								SACHIN VERMA</br>
								ProMoney
								<img src="./images/promoneySignature.png" style="position: absolute;right: 22px;width: 98px;top: -28px;">
							<br><br>
							</p>
						</div>
					</div>

				</div>
				<div class="footer" style="background-color: #193562;">
					<p>The holder of this card has the documentary evidence to prove that he is involved in providing essential services to the citizens of the country</p>
				</div>
			</div>
		</div>

		<div class="wrapper">
			<div class="instructionWrapper">
				<P class="intructionHeading" style="background-color: #d91b5c;">INSTRUCTIONS</P>
				<ul>
					<li>This card must be produced as per demand</li>
					<li>Holders of the card will accountable for any misuse, loss or damage caused</li>
					<li>Loss must be reported to the registered office on an immediate basis</li>
				</ul>
				<p class="subheading" style="color: #13096b;">If found, please return to Register office</p>

				<div class="addressWrapper">
					<h2>ProMoney</h2>
					<p>GF 5 Sona Akruti <br> Opp. Pramukh Park, Geratpur, Ahmedabad - 382435<br>Board Line Number : +91 8866447788 </p>
				</div>
			</div>

			<div class="footer" style="background-color: #193562; padding: 20px 5px; margin-left: 2px; margin-top: 11px;">
				<p>Email:support@promoney.in | Website: www.promoney.in</p>
			</div>
		</div>

<%
	}else if("OAGG001050".equalsIgnoreCase(mastBean.getAggreatorid())){
%>
				<div class="wrapper">
			<div class="innerWrapper">
				<div class="topheader">
					<div class="logo">
						<img src="<%=logo%>" style=" width: 102px;">
					</div>
					<h3>Banking Correspondent of </h3>
					<div class="logo" style="float:right;padding-left: 0px;">
						<img src="./images/finoLogo.png">
					</div>
				</div>

				<h3 class="heading" style="color:#f88923">Essential Banking Services Provider to the country</h3>
				<div class="detaileWrapper">
					<div class="userimage">
						<img alt="Fix your photo here" src="data:image/gif;base64,<%=user.getUserImg()%>"/>
					</div>
					<div class="userdDetaiWraper">
						<h3 style="color: #00994b">Banking & ATM Services</h3>
						<h2 style="background-color:#f88923">IDENTITY CARD - Bhartipay Services Pvt Ltd</h2>
						<label class="inputField"><span>Name:</span> <p style="width: 321px;"><%=mastBean.getName()!=null ? mastBean.getName() :"N/A"  %></p></label>
						<label class="inputField"><span>Merchant Id:</span><p style="width: 270px;"><%=mastBean.getId()!=null ? mastBean.getId() :"N/A"  %></p></label>
						<label class="inputField"><span>State:</span><p style="width: 327px;"> <%=mastBean.getTerritory()!=null ? mastBean.getTerritory() :"N/A"  %></p></label>
						<label class="inputField"><span>Department:</span><p style="width: 150px;">Business Associate</p></label>
						<label class="inputField"><span>DOJ:</span><p style="width: 207px;"><%=mastBean.getCreationDate() !=null ? mastBean.getCreationDate() :"N/A" %></p></label>
						<label class="inputField"><span>Valid Till:</span><p style="    width: 154px;">2021-12-31</p></label>
						<div class="signatureWrapper">
							<p style="position: relative;">
								(ISSUING AUTHORITY)<br>
								SAKSHI CHAWLA<br>
								BHARTIPAY SERVICES PVT. LTD
								<img src="./images/signature.png" style="position: absolute;left: 58px;right: 0px;transform: rotate(-90deg);width: 50px;top: -70px;">
							<br><br>
							</p>
						</div>
					</div>

				</div>
				<div class="footer" style="background-color:#00994b;">
					<p>The holder of this card has the documentary evidence to prove that he is involved in providing essential services to the citizens of the country</p>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<div class="instructionWrapper">
				<P class="intructionHeading" style="background-color: #f88923;">INSTRUCTIONS</P>
				<ul>
					<li>This card must be produced as per demand</li>
					<li>Holders of the card will accountable for any misuse, loss or damage caused</li>
					<li>Loss must be reported to the registered office on an immediate basis</li>
				</ul>
				<p class="subheading" style="color: #00994b;">If found, please return to Register office</p>

				<div class="addressWrapper">
					<h2>Bhartipay Services Private Limited</h2>
					<p>A-100, Sector 4 <br> Noida, Uttar Pradesh - 201 301<br>Board Line Number : +91 (120) 429-2590 </p>
				</div>
			</div>

			<div class="footer" style="background-color:#00994b; padding: 20px 5px; margin-left: 2px; margin-top: 11px;">
				<p>Email:partnersupport@bhartipay.com | Website: www.bhartipay.com</p>
			</div>
		</div>
		<%} %>
	</body>
	</html>