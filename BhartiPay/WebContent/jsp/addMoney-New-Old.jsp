<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard" class="wll">Home</a>
        </li>
        <li>
            <a href="Search" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->


<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Add Money</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<!-- <form action="PaymentGateway" id="form_name" method="post" name="PG"> -->
		<form action="PaymentGateway" id="addMoneyForm" method="post" name="PG">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Enter Amount <font color="red"> *</font></label> <input
																class="form-control mandatory amount-vl" name="inputBean.trxAmount" type="text"
																id="amount" maxlength="6" placeholder="Enter Amount" />

														</div>
													</div>




													<div class="col-md-4">
										<div class="form-group">
										<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										<!-- <button type="submit" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button> -->
										<button type="submit"  onclick="submitVaForm('#addMoneyForm',this)" class="btn submit-form btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button>
										 <button type="reset" onclick="resetForm('#addMoney')"  style="margin: 16px 19px 0 13px;
    										position: relative;
   										 top: 1px;" class="btn btn-info btn-fill" value="Reset">Reset</button>

									
									</div>
									</div>
									</div>
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


