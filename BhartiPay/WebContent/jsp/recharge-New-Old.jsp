<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
User user=(User)session.getAttribute("User");

%>


<jsp:include page="theams.jsp"></jsp:include>
<%@include file="reqFiles.jsp" %>
 
<link href='./css/rechargeUi.css' rel='stylesheet'>
<link href='./css/dd.css' rel='stylesheet'>
<jsp:include page="head.jsp"></jsp:include>
<script>

/* function getOpretorDetails(id){

	
	var el = $(id).val();
	if(el.length > 4 ){
		var data = getServiceProvider(el)
		//console.log(data)

		$("#operator").val(data.operator)
		$("#circlei").val(data.circle)
	}
	
} */




function changeDTHPlaceholderAndLen(el,targetInp){
	var elv = $(el).val()
	var tar = $(targetInp)
	var le = 12
	var ph = 'Enter your DTH number'
	switch(elv){
	case 'AIRTEL DIGITAL TV':
		le = 10
		ph = 'Customer ID'
		tar.attr('Placeholder','Customer ID','maxlength','10');
		break;
	case 'DISH TV':
		le = 11
		ph = 'Viewing Card Number'
	
		break;
	case 'RELIANCE DIGITAL TV':	
		le = 12
		ph = 'Smart Card Number'
		
		break;
	case 'VIDEOCON D2H':
		le = 10
		ph = 'Customer ID'
	
		break;
	case 'TATA SKY':	
		le = 10
		ph = 'Subscriber ID'
		
		break;
	case 'SUN DIRECT':	
		le = 11
		ph = 'Smart Card Number'
		break;
		
	default:
		le = 12
		
		
		
	
	}
	tar.attr('Placeholder',ph)
	tar.attr('maxlength',le)
}
function resetTab(e){
	 e.preventDefault();
	  
	$("input[type='text']").val('')
	$('select').val('')
	$(".errorMsg").hide()
	

    // Failing the above, you could use this, however the above is recommended
    return false;
}
$(function(){
	 $("#rechargeOperator, #circlei, #dth_operator, #datacard_operator, #datacard_circle").msDropDown();
})
</script>
<style>
.errorMsg{    right: 12%;
    top: 57px;}
    .chosen-container {
    width: 90%!important;
}
.chosen-container-single .chosen-single{
padding: 9px 0 8px 8px!important;}
.chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
}
</style>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="" style="height: 571px;">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            



<div class=" row">
<!-- Banner Form -->


<div class="container">
		<div class="newRechargeUi no-chosen-drop">
		
		  <h3 style="font-size:18px">
		  
		 
	<font style="color:red;">
		<%if(session.getAttribute("rechargeError") != null){%><%=session.getAttribute("rechargeError")%><%session.removeAttribute("rechargeError"); } %>
		</font>
		<font style="color:green;">
		</font>
		</h3>
	
		 
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_formshadow" style=" margin-top: 24px;">
 	<div class="loader"></div>
			<!-- ======================================== Form Tab Button ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div
					class="col-7-width payplutus_tab_active"
					id="mb">
					<a href="#mobileRecharge" class="tab" onclick="resetTab(event)" id="mobile_recharge">
					<span class="r-icon mobile"></span>Mobile</a>
				</div>
				<div class="col-7-width payplutus_tab"
					id="dh">
					<a href="#dthRecharge" onclick="resetTab(event)" class="tab" id="dth_recharge"><span class="r-icon dth"></span>DTH</a>
				</div>
				<div class="col-7-width  payplutus_tab"
					id="dc">
					<a href="#datacardRecharge" onclick="resetTab(event)" class="tab" id="datacard_recharge"><span class="r-icon datacard"></span>Data card</a>
				</div>
					<div class="col-7-width payplutus_tab"
					id="el">
					<a href="#electricityRecharge" onclick="resetTab(event)" class="tab" id="electricity_recharge"><span class="r-icon datacard"></span>Electricity</a>
				</div>
					<div class="col-7-width  payplutus_tab"
					id="gr">
					<a href="#gasRecharge" onclick="resetTab(event)" class="tab" id="gas_recharge"><span class="r-icon datacard"></span>Gas</a>
				</div>
					<div class="col-7-width payplutus_tab"
					id="lb">
					<a href="#landlinebill" onclick="resetTab(event)" class="tab" id="landline_Recharge"><span class="r-icon datacard"></span>Landline</a>
				</div>
					<div class="col-7-width payplutus_tab"
					id="ir">
					<a href="#insuranceRecharge" onclick="resetTab(event)" class="tab" id="insurance_recharge"><span class="r-icon datacard"></span>Insurance</a>
				</div>
				
			</div>
			<!-- ========================================== Form Fields =========================================== -->
			<!-- ======================================== MOBILE RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 r-tab-content col-sm-12 col-xs-12"
				id="mobile_recharge_form">

				<!--================================== INCLUDE mobile-recharge.HTML  =====================================-->



				<form method="post" id="mobile_form" action="RechargeService.action">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					
					<div class="form-group clearfix Bhartipay_mt10">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" class="rechargeTypeRadio" name="rechargeBean.rechargeType" id="connection_type"
								value="PREPAID" checked  onClick="hide_plan();"><label class="tab">Prepaid</label>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="radio" class="rechargeTypeRadio" name="rechargeBean.rechargeType" id="connection_type"
								value="POSTPAID" onClick="hide_plan();"><label class="tab">Postpaid</label>
						</div>
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" 
							class="payplutus_textbox mobile form-control mandatory" 
							placeholder="Enter your mobile number" 
							autocomplete="off"  id="rechargeMobile" onkeyup="getOpretorDetails(this,'mobile')"  />
					</div>
					<div class="form-group Bhartipay_mt10">
						
						
						<s:select list="%{prePaid}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory form-control" id="rechargeOperator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);validateSelectOnChange(this,'#opratorErr');"/>
					
					<div class="errorMsg customSelectErr " id="opratorErr" style="display: none;">Please select the operator</div>
					</div>
					<div class="form-group Bhartipay_mt10">
<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown form-control mandatory" id="circlei" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);validateSelectOnChange(this,'#opratoCireclrErr');"/>

				<div class="errorMsg customSelectErr" id="opratoCireclrErr" style="display: none;">Please select the circle</div>	
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="amount"
							class="payplutus_textbox  form-control amount-vl mandatory" required
							placeholder="Enter amount" maxlength="4" />
						
					</div>
				
				</form>
					<div class="form-group Bhartipay_mt10" style="margin-top: 10PX;">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="rechage" onclick="submitVaForm('#mobile_form',this)">
					</div>
			</div>
			<!-- ======================================== D2H RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 col-sm-12  r-tab-content col-xs-12"
				id="d2h_recharge_form" style="display: none;">
				<!--================================== INCLUDE d2h-recharge.HTML  =====================================-->



				<form method="post" id="d2h" action="RechargeService.action">
				
				

				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					
					<input type="hidden" name="rechargeBean.rechargeType" value="DTH">
					<div class="form-group Bhartipay_mt10">
		<s:select list="%{dth}" name="rechargeBean.rechargeOperator" onchange="changeDTHPlaceholderAndLen(this,'#dth_mobile_no')" cssClass="payplutus_dropdown mandatory" id="dth_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
					<div class="errorMsg customSelectErr" style="display: none;" id="dthOpratorErr">Please select operator</div>	
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="dth_mobile_no"
							class="payplutus_textbox mandatory onlyNum" required
							placeholder="Enter your DTH number" maxlength="12" />
					</div>
					<div class="form-group Bhartipay_mt10">
								
						<input type="hidden" name="rechargeBean.rechargeCircle" value="Delhi">			
 					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="dth_amount"
							class="payplutus_textbox  amount-vl mandatory"  placeholder="Enter amount"
							maxlength="4" />
					</div>
					
				</form>
				<div class="form-group Bhartipay_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="DTH" onclick="submitVaForm('#d2h',this)">
					</div>
				
			</div>
				<!-- ======================================== Landline RECHARGE ========================================= -->
		 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r-tab-content"
				id="landline_recharge_form" style="display: none;">
				<!--================================== INCLUDE datacard-recharge.HTML  =====================================-->



				<form method="post" id="landline_bill" action="GetBillDetails.action"  onsubmit="validateLandlineForm(event)">
					<input type="hidden" name="billDetails.billerType" value="Landline" >
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					
			<!-- 		<div class="form-group Bhartipay_mt10">
                        <input type="hidden" name="rechargeBean.rechargeType" value="landline" checked >
                      
                    </div> -->
                    	<div class="form-group Bhartipay_mt10">
                    	
                    	<select class="payplutus_dropdown mandatory" onChange="landlinServiceProviderOnChange(this)"  name="billDetails.billerId" id="landline_operator">
                    		<option value="-1">Select Operator</option>
                    	
                    	</select>
						<%-- <s:select list="%{landLine}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory" onChange="landlinServiceProviderOnChange(this)" id="landline_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>
 --%>
					<div class="errorMsg" id="landlineError" style="display: none;"></div>
					</div>
                    <div class="form-group Bhartipay_mt10">
						<input type="text" id="landlineNum"
							class="payplutus_textbox onlyNum mandatory "     name="billDetails.number" placeholder="Enter Phone Number"
							maxlength="11" />
					</div>
			
			  <div class="form-group Bhartipay_mt10" id="landlineAmountWrapper">
						<input type="text"  id="landlineAmount"
							class="payplutus_textbox onlyNum mandatory "   maxlength="5"  name="billDetails.amount" placeholder="Enter amount" />
					</div>
				
					<div class="form-group Bhartipay_mt10" id="landineAddInfoWrapper" style="display:none">
						<input type="text" name="rechargeBean.accountNumber" id="landline_add_info"
							class="payplutus_textbox onlyNum mandatory" name="billDetails.account"  placeholder="Customer Account Number"
							maxlength="10" />
					</div>
						<div class="form-group Bhartipay_mt10" id="Authenticator" style="display:none" >
                    	
                    	<select class="payplutus_dropdown mandatory"   name="billDetails.additionalInfo">
                    		<option value="-1">Please Select Authenticator</option>
                    		<option value="LLI">LLI</option>
                    		<option value="LLC">LLC</option>
                    		
                    	
                    	</select>
					
					<div class="errorMsg" id="AuthenticatorError" style="display: none;"></div>
					</div>
						<div class="form-group Bhartipay_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form"  />
					</div>
				</form>
			
			</div> 
			<!-- ======================================== DATACARD RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 r-tab-content col-sm-12 col-xs-12"
				id="datacard_recharge_form" style="display: none;">
				<!--================================== INCLUDE datacard-recharge.HTML  =====================================-->




				<form method="post" id="datacard" action="RechargeService.action">
					
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					
					<div class="form-group clearfix Bhartipay_mt10">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input type="radio" name="rechargeBean.rechargeType" id="connection_type"  class="rechargetypeDataCard" value="Datacard" checked ><label class="tab">Prepaid</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	                            <input type="radio" name="rechargeBean.rechargeType" class="rechargetypeDataCard" id="connection_type" value="Postpaid-Datacard" ><label class="tab">Postpaid</label>
	                        </div>
                    </div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="datacard_mobile"
							class="payplutus_textbox onlyNum mandatory" required
							placeholder="Enter your datacard number" onkeyup="getOpretorDetails(this,'datacard')" maxlength="10" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<s:select list="%{prePaidDataCard}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory" id="datacard_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>

						
						</select>
					</div>
					<div class="form-group Bhartipay_mt10">
						
						<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown mandatory" id="datacard_circle" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);"/>
						
						
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="datacard_amount"
							class="payplutus_textbox amount-vl mandatory" required placeholder="Enter amount"
							maxlength="4" />
					</div>
				
				</form>
						<div class="form-group Bhartipay_mt10">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="datacard" onclick="submitVaForm('#datacard',this)">
					</div>
			</div>
			
			
			
			<!-- ======================================== Electricity RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 r-tab-content col-sm-12 col-xs-12"
				id="electricityRecharge_form" style="display: none;">
				<!--================================== INCLUDE Electricity-recharge.HTML  =====================================-->




				<form method="post" id="electricityForm"  action="GetBillDetails.action" onsubmit="electricityFormSubmit(event)">
				
					
							<input type="hidden" name="billDetails.billerType" value="Electricity" > 
							<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							
							
					<div class="form-group Bhartipay_mt10">						
					<select class="payplutus_dropdown mandatory" id="eleOpretor" name="billDetails.billerId" onchange="electricityProviderChange()">
						
							<option value="-1">Select Operator</option>
							
						</select>
						<span class="errorMsg" id="oprErr" style="display: none;"></span>
					</div>
					
				<div class="form-group Bhartipay_mt10">
						<input type="text"  id="electricity_consumer"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Consumer Number"  name="billDetails.number" />
					</div>
					<div class="form-group Bhartipay_mt10" id="eleAccount" style="display:none;">
						<input type="text"  id="electricity_account"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Consumer Number" name="billDetails.account"  />
					</div>
						<div class="form-group Bhartipay_mt10" id="elethirdOption" style="display:none;">
						<select id="thirdEleOption" class="payplutus_dropdown mandatory" name="billDetails.additionalInfo"></select>
							
					</div>
					
					<div class="form-group Bhartipay_mt10" >
						<input type="Submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="electricity" >
					</div>
				
				</form>
						
			</div>
			
			<!-- ======================================== Gas RECHARGE ========================================= -->
			<div class="col-lg-12 col-md-12 r-tab-content col-sm-12 col-xs-12"
				id="gasRecharge_form" style="display: none;">
				<!--================================== INCLUDE Gas-recharge.HTML  =====================================-->




				<form method="post" id="gasRecharge" action="GetBillDetails.action" onsubmit="validateGasForm(event)">
				<input type="hidden" name="billDetails.billerType" value="Gas" > 
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				<div class="form-group Bhartipay_mt10">
						<select class="payplutus_dropdown" id="gas-opretor"  name="billDetails.billerId" onchange="gasOpratorChange(this)">
							<option value="-1">Select Operator</option>
							
						</select>
						<span id="gas-provider" class="errorMsg" style="display: none;">Please Select The Service Provider.</span>
					</div>
					
				<div class="form-group Bhartipay_mt10">
						<input type="text"  id="gas-consumer"  name="billDetails.number"
							class="payplutus_textbox onlyNum " 
							placeholder="Enter Operator Number"   />
					</div>
						<div class="form-group Bhartipay_mt10"  id="accoundBill" style="display:none">
						<input type="text"  id="accoundBillInp"  name="billDetails.account"
							class="payplutus_textbox alphaNum-vl " 
							placeholder="Enter Operator Number" maxlength="8"  />
					</div>
				<div class="form-group Bhartipay_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="gas" >
					</div>
				</form>
						
			</div>
				<!-- ======================================== Insurance  ========================================= -->
			<div class="col-lg-12 col-md-12 r-tab-content col-sm-12 col-xs-12"
				id="insurance_form" style="display: none;">
				<!--================================== INCLUDE Insurance  =====================================-->




				<form method="post" id="insuranceRecharge" action="GetBillDetails.action" onsubmit="validateInsuranceForm(event)">
					<input type="hidden" name="billDetails.billerType" value="Insurance" > 
					<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<div class="form-group Bhartipay_mt10">
						<select class="payplutus_dropdown mandatory"  name="billDetails.billerId" id="insurance" onchange="changeInsurar()">
							<option value="-1">Please Select Operator</option>
						</select>
						<span id="insurance-error" class="errorMsg"></span>
					</div>
					
				<div class="form-group Bhartipay_mt10">
						<input type="text"  id="policy_number"
							class="payplutus_textbox alphaNum-vl " 
							placeholder="Enter Policy Number"    name="billDetails.number" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text"  id="insurer_dob"
							class="payplutus_textbox datepicker-here" 
							placeholder="DOB"  data-language='en'  name="billDetails.account" readonly/>
					</div>
				<div class="form-group Bhartipay_mt10">
						<input type="submit" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" data-open-popup="insurar" />
					</div>
				</form>
						
			</div>
			
						<!-- ======================================== Landline RECHARGE ========================================= -->
			<%-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
				id="landline_recharge_form" style="display: none;">
				<!--================================== INCLUDE datacard-recharge.HTML  =====================================-->




				<form method="post" id="landline_bill" action="RechargeService.action">
					
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
					<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
					<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
					
					<div class="form-group Bhartipay_mt10">
                        <input type="hidden" name="rechargeBean.rechargeType" value="landline" checked >
                      
                    </div>
                    	<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.stdCode" id="landline_amount"
							class="payplutus_textbox onlyNum mandatory std-vl"   placeholder="Enter std code start with Zero(0123)"
							maxlength="6" />
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeNumber" id="landline_mobile"
							class="payplutus_textbox mandatory landline onlyNum landline-vl" 
							placeholder="Enter your Landline Number"  maxlength="8"/>
					</div>
					<div class="form-group Bhartipay_mt10">
						<s:select list="%{landLine}" name="rechargeBean.rechargeOperator" cssClass="payplutus_dropdown mandatory" id="landline_operator" headerKey="-1" headerValue="Please select operator" onChange="change_operator(this);"/>

					
					</div>
					<div class="form-group Bhartipay_mt10">
	<s:select list="%{circle}" name="rechargeBean.rechargeCircle" cssClass="payplutus_dropdown mandatory" id="landline_circle" headerKey="-1" headerValue="Please select a circle" onChange="change_circle(this);"/>
					
					</div>
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.rechargeAmt" id="landline_amount"
							class="payplutus_textbox amount-vl mandatory"  placeholder="Enter amount"
							maxlength="4" />
					</div>
				
					<div class="form-group Bhartipay_mt10">
						<input type="text" name="rechargeBean.accountNumber" id="landline_amount"
							class="payplutus_textbox onlyNum mandatory"  placeholder="Enter account number"
							maxlength="11" />
					</div>
					
				</form>
				<div class="form-group Bhartipay_mt10">
						<input type="button" name="proceed" id="proceed"
							value="Proceed to Pay" class="payplutus_button submit-form" onclick="submitVaForm('#landline_bill')" />
					</div>
			</div> --%>
		</div>
		</div>
		
	</div>

</div>

</div>
        
        

      <div class="modal fade" id="rechagePop" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Recharge Type </th>
        <th id="rechargetype">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mobile Number</td>
        <td id="mobileNo"></td>
        
      </tr>
      <tr>
        <td >Service Provider</td>
        <td id="servicePovider"></td>
        
      </tr>
      <tr>
        <td>Circle </td>
        <td id="rechargecircle"></td>
        
      </tr>
      <tr>
        <td>Amount </td>
        <td id="rechargeAmount"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('mobile_form').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!--   Electricity Bill -->
    <div class="modal fade" id="electicityPop" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Electricity Bill Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">

    <tbody>
   
      <tr>
        <td >Service Provider</td>
        <td id="electicityServicePovider"></td>
        
      </tr>
      <tr>
        <td>Consumer Number</td>
        <td id="consumerNumber"></td>
        
      </tr>
      <tr>
        <td>Bill Amount </td>
        <td id="electicityrechargeAmount"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('electricityForm').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!--   Gas -->
<div class="modal fade" id="gasPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Gas Bill Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">

    <tbody>
   
      <tr>
        <td >Service Provider</td>
        <td id="gasServicePovider"></td>
        
      </tr>
      <tr>
        <td>Consumer Number</td>
        <td id="gasConsumerNumber"></td>
        
      </tr>
      <tr>
        <td>Bill Amount </td>
        <td id="gasAmount"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('gasRecharge').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
<!--  Insurance -->
  <div class="modal fade" id="insurarPopUp" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Insurance Renew Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">

    <tbody>
   
      <tr>
        <td >Insurer</td>
        <td id="insuranceServicePovider"></td>
        
      </tr>
      <tr>
        <td>Insurance Policy Number</td>
        <td id="insuranceConsumerNumber"></td>
        
      </tr>
      <tr>
        <td>Policy Renew Amount </td>
        <td id="insuranceAmount"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('electricityForm').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
  
      <div class="modal fade" id="DTPpopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Service Provider </th>
        <th id="servicePoviderDTH">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>DTH Number</td>
        <td id="dthNo"></td>
        
      </tr>
     
      <tr>
        <td>Amount </td>
        <td id="rechargeAmountDTH"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('d2h').submit();">Confirm</button>
        </div>
      </div>
      
      
    </div>
  </div>
       
           <div class="modal fade" id="dataCardPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Recharge Type </th>
        <th id="rechargetypeDataCard">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mobile Number</td>
        <td id="mobileNoDataCard"></td>
        
      </tr>
      <tr>
        <td >Service Provider</td>
        <td id="servicePoviderDataCard"></td>
        
      </tr>
      <tr>
        <td>Circle </td>
        <td id="rechargecircleDataCard"></td>
        
      </tr>
      <tr>
        <td>Amount </td>
        <td id="rechargeAmountDataCard"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('datacard').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>

</div>
</div><!--/.fluid-container-->

<!-- Biller Popup -->
  <div class="modal fade" id="billerPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Biller Information</h4>
        </div>
        <div class="modal-body">
        <div class="responseMsg"></div>
        <table class="table table-bordered" id="billerTable">
    <tbody>
  
    </tbody>
  </table>

        </div>
        <div class="modal-footer">
     
        	<input type="hidden" id="txnId" value="" />
        	<input type="hidden" id="billerAmount" value="" />
        	  	<input type="hidden" id="billerType" value="" />
        	  		  	<input type="hidden" id="billerTypePlatForm" value="B2B" />
        	
     
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary btn-info" id="proceedBiller" style="display:none" onclick="billerConfirm('null','null','null','${csrfPreventionSalt}')">Proceed</button>
        </div>
      </div>
      
    </div>
  </div>

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->
<!-- ********************************************************************************************* -->
			<!-- Placed at the end of the document so the pages load faster -->
	
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./js/ie10-viewport-bug-workaround.js"></script>
		<script src="./js/payplutus.js<%=version %>" type="text/javascript"></script>
		<!-- jssor slider scripts-->
		<!-- use jssor.js + jssor.slider.js instead for development -->
		<!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
		<%-- <script type="text/javascript" src="./js/jssor.slider.mini.js"></script>
		<script>
    jQuery(document).ready(function ($) {
        var options = {
            $AutoPlay: false,            //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
            $AutoPlaySteps: 1,           //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
            $AutoPlayInterval: 2000,     //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 1,            //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

            $ArrowKeyNavigation: false,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
            $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
            $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
            $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
            //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
            //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
            $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
            $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
            $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
            $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
            $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
            $DragOrientation: 0,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

            $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 1 Always
                $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                $Scale: false                                   //Scales bullets navigator or not while slider scale
            },

            $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                $SpacingY: 0,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                $Scale: false                                   //Scales bullets navigator or not while slider scale
            }
        };

        //Make the element 'slider1_container' visible before initialize jssor slider.
        $("#slider1_container").css("display", "block");
        var jssor_slider1 = new $JssorSlider$("slider1_container", options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
            if (parentWidth) {
                jssor_slider1.$ScaleWidth(parentWidth - 30);
            }
            else
                window.setTimeout(ScaleSlider, 30);
        }
        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
     --%>
    



<script src="./js/jquery.dd.min.js"></script>
</body>
</html>



