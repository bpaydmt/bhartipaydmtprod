<%@page import="com.bhartipay.wallet.user.persistence.vo.UploadSenderKyc"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<%-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> > --%>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" / >  
 
  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 
$(document).ready(function() {
    
        
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#to').val(start.format('DD-MMM-YYYY'));
         $('#from').val(end.format('DD-MMM-YYYY'));

        }
     );
  //Set the initial state of the picker label
    $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
 });


</script>  
 
<script type="text/javascript">
   $(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Sender PAN List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Sender PAN List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Sender PAN List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Sender PAN List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Sender PAN List - ' + '<%= currentDate %>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
        ]
    } );
   
  

	   /*  $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    }) */
	   
	    
	} );


	 /* function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 } */
   
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
<div id="main" role="main"> 
    <div id="content">  

		<div class=" row">  
			<div class="box2 col-md-12">
				<div class="box-inner">
					
						<div class="box-header">
							<h2>Sender KYC Request</h2>
						</div>
						
				    <div class="box-content"> 
						<form action="GetSenderKycList" method="post">
						<div class="row">  
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">  
							    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
							    

							    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
								    <i class="fa fa-calendar"></i>&nbsp;
								    <span></span> <i class="fa fa-caret-down"></i>
								</div>	
							    
								<input type="hidden" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="to" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>

								<input type="hidden" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="from" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
							</div>

							<div class="col-md-4 col-xs-12"> 
								<div  id="wwctrl_submit">
									<input class="btn btn-success" id="submit" type="submit" value="Submit">
										
									<input class="btn btn-info" id="reset" type="reset" value="Reset">
								</div>
							</div>
						</div>
						</form>
						
					</div> 
				</div>
			</div>	
			<div class="col-md-12">	 							
				<div id="xyz">
					<font color="red"><s:actionerror/> </font>
					<font color="blue"><s:actionmessage/> </font>
					<s:fielderror></s:fielderror>
						<table id="example" class="scrollD cell-border dataTable">
							<thead>
							
							
								<tr>
									<th><u>Sender Mobile</u></th>
									<th><u>Name</u></th>
									<%if(user.getUsertype() != 2){ %>
									<th><u>Agent ID</u></th>
									<%} %>
									<th><u>Request Date</u></th>
									<th><u>Add Proof Type</u></th>
									<th><u>Id Proof Type</u></th>
									<th><u>Address Proof</u></th>
									<th><u>Id Proof</u></th>
									<th><u>Status</u></th>
									<%if(user.getUsertype() != 2){ %>
									<th><u>Action</u></th>
									<%} %>
									<th style="display: none;"><u>Address</u></th>
								</tr>
							</thead>
							<tbody>
							<%
							List <UploadSenderKyc> list=(List<UploadSenderKyc>)request.getAttribute("senderKycList");

							if(list!=null){
							
							
							for(int i=0; i<list.size(); i++) {
								UploadSenderKyc usk=list.get(i);%>
					          		  <tr>
					         
					       	  
					       	   <td><%=usk.getSenderMobileNo()%></td>
					       	   <td><%=usk.getSenderName()%></td>
					       	   <%if(user.getUsertype() != 2){ %>
					       	   <td><%=usk.getAgentId()%></td>
					       	   <%} %>
					       	   <td><%=usk.getRequestDate()%></td>
					       	   <td><%=usk.getAddressProofType()%>-<%=usk.getAddressProof()%></td>
					       	 
					       	   <td><%=usk.getIdProofType()%>-<%=usk.getIdProof()%></td>
					       	  <td class="image-td">
					       	   <img style="cursor: pointer;" class="grid-img" alt="userImg" src="<%=usk.getAddressProofUrl()%>" width="40px" height="40px" data-toggle="modal" />
					       	   
					       	   <a class="grid-link" onclick="viewImgPopup('<%=usk.getAddressProofUrl()%>','ID Proof')" data-toggle="modal" href="#imagePopUp">View</a>
					       	  
					       	  </td>
					       	  <td class="image-td"> 
					       	  <img class="grid-img" style="cursor: pointer;" alt="userImg" src="<%=usk.getIdProofUrl()%>" width="40px" height="40px" data-toggle="modal" />
					       	   
					       	   <a  class="grid-link" onclick="viewImgPopup('<%=usk.getIdProofUrl()%>','Address Proof')" data-toggle="modal" href="#imagePopUp">View</a>
					       	  
					       	  
					       	  </td>
					       	   
					       	    <td>
					       	    
					       	    <%if(usk.getFinalStatus()!=null&&usk.getFinalStatus().equalsIgnoreCase("ACCEPT")){
					       	    out.print("ACCEPTED");
					       	    }else if(usk.getFinalStatus()!=null&&usk.getFinalStatus().equalsIgnoreCase("REJECT")){
					       	    	out.print("REJECTED");
					       	    }else if(usk.getFinalStatus()!=null&&usk.getFinalStatus().equalsIgnoreCase("Pending")){
					       	    	out.print("PENDING");
					       	    }%>
					       	    </td>
					       	    <%if(user.getUsertype() != 2){ %>
					       	    <td>
					       	    <%if(usk.getFinalStatus()!=null&&usk.getFinalStatus().equalsIgnoreCase("Pending")){ %>
					       	    <button id="<%=usk.getSenderId()%>acceptDoc" onclick="approveAction('<%=usk.getSenderId()%>','ACCEPT','<%=usk.getKycId()%>')" data-toggle="modal" data-target="#approveActionPop" class="grid-small-btn">Accept</button>
					       	    
					       	    	<button id="<%=usk.getSenderId()%>rejectDoc" onclick="approveAction('<%=usk.getSenderId()%>','REJECT','<%=usk.getKycId()%>')" data-toggle="modal" data-target="#approveActionPop" class="grid-small-btn">Reject</button>
					       	   <%} %>
					       	    </td>
					       	    <%} %>
					       	    <td style="display: none;"><%=usk.getSenderAddress()%></td>
					    	  </tr>
						      <%} }%>	
						        </tbody>		</table>
				</div> 
            </div> 
		</div> 
		        
		        

		        <!-- contents ends -->

		       

    </div>
</div>

<div id="approveActionPop" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
        <form id="pan-action" method="post" action="AcceptedRejectedSenderKyc" >
        <div class="form-group">
            <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
        	<input name="senderKyc.senderId" id="hidden-id" value="" type="hidden">
        	<input name="senderKyc.kycId" id="hidden-kycid" value="" type="hidden">
        	<input name="senderKyc.finalStatus" id="hidden-status" value="" type="hidden">
        	<input placeholder="Remark" name="senderKyc.remark" id="remoark-inp" class="form-control mandatory"  type="text"><span class="errorMsg"></span>
         	
        </div>
        
        <div class="form-group">
        	 <input placeholder="Sender Name" name="senderKyc.senderName" id="sender-inp"  class="form-control userName mandatory"  type="text" ><span class="errorMsg"></span>
        </div>
        
        <div class="form-group">
        	 <input placeholder="Sender Address" name="senderKyc.senderAddress" id="sender-inp-add"  class="form-control mandatory"  type="text" ><span class="errorMsg"></span>
        </div>
        
        </form>
        <input value="submit" class="btn btn-info btn-fill btn-block" type="Button"  onclick="submitVaForm('#pan-action',this)">
      </div>
   
    </div>

  </div>
</div>

<div id="imagePopUp" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="imgHead">Document</h4>
      </div>
      <div class="modal-body">
    	<img src="" id="imgHolder" style="width:100%; height:200px" />
      </div>
   
    </div>

  </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>

<script>

function viewImgPopup(url,title){
	$("#imgHead").text(title)
	$("#imgHolder").attr('src',url);
	
}

function approveAction(reqId,action,kycId){
	if(action=="ACCEPT"){ 
		$("#sender-inp").show().addClass('mandatory');
		$("#sender-inp-add").show().addClass('mandatory');
		
}else{ 
		$("#sender-inp").hide().removeClass('mandatory');
		$("#sender-inp-add").hide().removeClass('mandatory');
		}
	
	$("#remoark-inp").val("")
	//$("#approveActionPop").model('show')
	$("#hidden-id").val(reqId)
	$("#hidden-kycid").val(kycId)
	$("#hidden-status").val(action)
}
</script>

