<%@page import="com.bhartipay.wallet.user.persistence.vo.TransacationLedgerBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script> 

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ > 

<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>
 
<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#to').val(start.format('DD-MMM-YYYY'));
         $('#from').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
   }); 
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Payout Transfer - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Payout Transfer - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Payout Transfer - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Payout Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Payout Transfer - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>
<style>
.selectBox{
    border: 2px solid #ccc !important;
    padding: 5px;
}
</style>
</head>

<% 
	User user = (User) session.getAttribute("User");
	Date myDate = new Date(); 
	SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
	String toDate=format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from= cal.getTime();    
	String fromDate = format.format(from);
	Object[] commDetails=(Object[])session.getAttribute("commDetails");
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

	List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");
	DecimalFormat d=new DecimalFormat("0.00"); 
%> 

<%
	List<TransacationLedgerBean> ledger=(List<TransacationLedgerBean>) request.getAttribute("transactionLedger");
%>
<body>

    <!-- topbar starts -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends --> 

    <!-- left menu starts -->
    <jsp:include page="mainMenu.jsp"></jsp:include>
    <!-- left menu ends -->

 
          
	<div id="main" role="main"> 
	    <div id="content"> 

			<div class=" row"> 

				<div class="box col-md-12">
					<div class="box-inner"> 
						<div class="box-header">
						<% if(user.getId().contains("MERCSPAY")) { %>
						    <h2>Payout Report</h2>
						<% }else{ %>
							<h2>Money Transfer</h2>
						 <% } %> 
						</div> 
						<div class="box-content">  
							<form action="GetTransacationLedgerPayout" method="post">
								<div class="row"> 

									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">  
									    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
									    

									    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div>	
									    
										<input type="hidden" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="to" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>

										<input type="hidden" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="from" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
									</div>
									<% if( user.getUsertype() ==3){%>	
							<div class="col-md-4 col-sm-6 col-xs-12 form-group"> 
								<s:select list="%{agentList}" headerKey="-1" headerValue="Select Agent" id="agentsub" name="inputBean.userId" cssClass="form-username selectBox" requiredLabel="true" />				
						    </div>
							<%} %>

<div class="col-md-2 col-sm-2 col-xs-12 form-group"> 
								<s:select list="%{statusList}" headerKey="-1" headerValue="Select Status" id="agentsub" name="inputBean.status" cssClass="form-username selectBox" requiredLabel="true" />				
						    </div>
									<div class="col-md-2 col-sm-4 col-xs-12"> 
										<div  id="wwctrl_submit">
											<input class="btn btn-success submit-form" id="submit" type="submit" value="Submit" >
											<input class="btn btn-primary btn-fill" id="reset" type="reset" value="Reset"  >
										</div>
									</div>
									
									<!-- <div class="col-md-2 col-sm-4 col-xs-12"> 
										<div  id="wwctrl_submit">
											Total
											<input class="btn btn-primary btn-fill" id="reset" type="reset" value="Reset"  >
										</div>
									</div> -->
									
									<% 
									int successCount=0;
									double successToatalAmount=0;
								    if(ledger!=null){ 
								    for(int i=0; i<ledger.size(); i++) {
								    	 TransacationLedgerBean tdo=ledger.get(i);
								    	if(tdo.getStatus()!=null && "SUCCESS".equalsIgnoreCase(tdo.getStatus())){
								    		successCount=successCount+1;
								    		successToatalAmount=successToatalAmount+(tdo.getDramount().doubleValue());
								    	}
								    }
								    }
								    %>
									
							
							
								</div>
						    </form> 
						</div>
					</div>
				</div>	
					
			
				
				
				<div class="col-md-12">			
					<div id="xyz">
						<table id="example" class="scrollD cell-border dataTable no-footer">
							<thead> 
								<tr>
									
<!-- 	</td>agentid</u></th> 

Sr.No	Agent ID	Agent Name		Via				Sender Name	Bank Name	IFSC code	Transction Amount	Transaction Status	Bank RRN	Bank Response
-->	
	
	<th><u>Date & Time</u></th>
	<td><u>TxnID</u></td>
	<td><u>Merch TxnID</u></td>
	<th><u>Amount</u></th>
	<th><u>Mode</u></th>
	<th><u>Account No</u></th>
	<th><u>Beneficiary Name</u></th>
	<th><u>IFSC Code</u></th>
	<th><u>Bank Name</u></th>
	<th><u>Bank RRN</u></th>
	<th><u>Status</u></th>
								</tr>
							</thead>
							<tbody>
							    <% 
							    if(ledger!=null){ 
							    for(int i=0; i<ledger.size(); i++) {
							    TransacationLedgerBean tdo=ledger.get(i);%>
					          	<tr> 
					          	   
	
	<td><%=tdo.getPtytransdt()%></td>
	<td><%=tdo.getId()%></td>
	<td><%=tdo.getMerchanttransid()!= null ? tdo.getMerchanttransid():"-" %></td>
	<td><%=tdo.getDramount()%></td>
	<td><%=tdo.getNarrartion()%></td>
	<td><%=tdo.getAccountno()%></td>
	<td><%=tdo.getName()%></td>
	<td><%=tdo.getIfsccode()%></td>
	<td><%=tdo.getBankname()%></td>
	<td><%=tdo.getBankrrn()!=null?tdo.getBankrrn():""%></td>
	<td><%=tdo.getStatus()%></td>
			                  	</tr>
						        <%} }%>	
						    </tbody>		
						</table>
					</div> 
                </div>
                
			</div> 

        </div>
    </div>
        

        <!-- contents ends -->


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%-- <script src='js/bootstrap.min.js'></script> --%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="transactionDtl" class="modal fade" role="dialog" style="width: auto;">
  <div class="modal-dialog model-lg" style="width:900px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>

<script>
function getTxnDetails(txnId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "TransactionDetailsByTxnIdBK",
		  data: "txnId="+txnId,
		  success: function(result){
			 
			  $("#transactionDtl .modal-body").html(result)
		  },
		  
		});
	
}

</script>

</body>
</html>



