<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.util.Map"%>
<html lang="en">
<head>
<%	Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult"); %> 
<title>Bhartipay | Load Money</title>
<jsp:include page="cssFiles.jsp"></jsp:include>


</head>

	<!-- topbar starts -->
	<jsp:include page="mainHeader.jsp"></jsp:include>
	<jsp:include page="mainMenuOption.jsp"></jsp:include>
	
	<div class="section-headline-wrap">
		<div class="section-headline">
			<p>Home<span class="separator">/</span><span class="current-section">Load Money</span></p>
		</div>
	</div>
	
	<jsp:include page="userDetailsRow.jsp"></jsp:include>
	
	<div class="section-wrap">
		<div class="section overflowable">
			<!-- SIDEBAR -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- /SIDEBAR -->

			<!-- CONTENT -->
		
                <!-- FORM POPUP -->
			<div class="form-popup">

				<!-- FORM POPUP HEADLINE -->
				<div class="form-popup-headline secondary">
					<h2>Load Money</h2>
				</div>
				<!-- /FORM POPUP HEADLINE -->

				<!-- FORM POPUP CONTENT -->
				<div class="form-popup-content">
					<form action="SetLoadMoney.action" id="loadMoney" method="post" name="loadMoney">
					
						<font color="red"><s:actionerror/> </font>
						<font color="blue"><s:actionmessage/> </font>
						
						<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
						<label for="username5" class="rl-label">Enter Amount*</label>
						
						<input name="inputBean.trxAmount" type="text" id="amount"  class="mandatory amount-vl-cr" placeholder="Enter Amount">
						<!-- LINE SEPARATOR -->
					<hr class="line-separator double">
					<!-- /LINE SEPARATOR -->
					<button type="button"  onclick="submitVaForm('#loadMoney')" class="button mid submit half">Submit</button>
					<button type="reset" onclick="resetForm('#loadMoney')" class="button mid reset half" >Reset</button>
					
					</form>
				

					
					<!-- <a href="login-register.html#" class="button mid submit half">Submit</a> -->
					<!-- <a href="login-register.html#" class="button mid reset half">Reset</a> -->
					
				</div>
				<!-- /FORM POPUP CONTENT -->
			</div>
			<!-- /FORM POPUP -->

			<!-- /CONTENT -->
			<div class="clearfix"></div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</body>
		</html>