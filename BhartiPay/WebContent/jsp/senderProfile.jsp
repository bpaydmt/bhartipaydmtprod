<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.SenderProfileBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Sender Details - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Sender Details - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Sender Details - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Sender Details - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Sender Details - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
List<SenderProfileBean> list=(List<SenderProfileBean>)request.getAttribute("senderList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Sender Details</h2>
		</div>
	</div>
	</div>	
		
		<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
																					</div>

									</div>
								

						<%-- 		<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
									<label for="email">User Type:</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
<div class="wwctrl" id="wwctrl_paymentMethods">
    
<select name="paymentMethods" class="form-control" id="paymentMethods" autocomplete="off">
    <option value="0">ALL</option>
    <option value="4">Aggregator</option>
    <option value="3">Distributor</option>
    <option value="2">Agent</option>
    <option value="5">Sub-Agent</option>
    <option value="1">Customer</option>
</select>

</div> </div>
								</div> 
								

								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateFrom">Date From:</label> <br>
								
    
<input type="text" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="dp" class="form-control datepicker-here"  placeholder="Start Date" data-language="en" required readonly="readonly" >
								</div>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateTo">Date To:</label> <br>
							
    
<input type="text" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="dp1" class="form-control datepicker-here"  placeholder="End Date" data-language="en" required readonly="readonly">
								</div>
								<div class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit"><input class="btn btn-sm btn-block btn-success" id="submit" type="submit" value="Submit">
</div> --%>
								</div>
							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>User Id</u></th>
						<th><u>Name</u></th>
						<th><u>Mobile</u></th>
						<th><u>Email</u></th>
						<th><u>Pay Amount</u></th>
						
					</tr>
				</thead>
				<tbody>
				<%
				for(SenderProfileBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getUserid()%></td>
		             <td><%=wtb.getPayername()%></td>
		             <td><%=wtb.getPayermobile()%></td>
		              <td><%=wtb.getPayerEmail() %></td>
		              <td><%=user.getCountrycurrency()%>:<%=wtb.getAmount()%></td>
		               
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


