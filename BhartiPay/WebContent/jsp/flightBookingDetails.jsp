<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.List" %>
<%@page import="com.bhartipay.wallet.report.bean.TravelTxn" %>
<%@page import="com.bhartipay.airTravel.bean.FlightDetails" %>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  

 


</head>


      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Flight Details</h2>
		</div>
		
		<div class="box-content row">
		
			<div class="details-block">
		
<%List<FlightDetails> flightList = (List<FlightDetails>)request.getAttribute("FlightDtl");
for(int i = 0;i<flightList.size();i++)
{
	FlightDetails flight = flightList.get(i);
%>

<h2>
<table class="table table-bordered" style=" font-size: 13px; width: 50%;">


    	<tr>
    	<td><strong>PNR</strong></td>
    		<td> <%= flight.getPnr() %></td>
    		<td><strong>Booking ID</strong></td>
    		<td> <%= flight.getTxnId() %></td>
   </tr>

</table>
<%
String fName="Bhartipay_Ticket_"+flight.getTxnId()+".pdf";
%>
 <a class="pull-right" href="./flightPDF/<%=flight.getFileName() %>" download> 
Download Ticket
 
</a>
<span id=""><%=flight.getSourceName()+" ("+flight.getSourceCode()+")" %></span> 
<i class="fa fa-plane" style="margin: 0 10px;"></i> 
 <span id=""><%=flight.getDestName() +" (" +flight.getDestCode()+")"  %>  </span> 

</h2>



		<div class="ticket-block">

				<div class="ticket-single">
					<div class="col-md-3">
						<img  src="./images/flights/<%=flight.getFlightCode()%>.png" class="f-img" alt="{{Airline.AirlineName}}" />
					<div class="airline-name"><%=flight.getFlightName() %></div>	
<div class="airline-code"><%=flight.getFlightCode()+" - "+flight.getFlightNumber() %></div>
					</div>
				<div class="col-md-3">
					<div class="time"><%=flight.getDeptDate()%></div>
					<div class="airport-code"><%=flight.getSourceCode()%></div>
				<div class="airport-name"><%=flight.getSourceName() %></div>
				</div>
	<div class="col-md-3">
					---
				</div>
	<div class="col-md-3">
					<div class="time"><%=flight.getArrDate()%></div>
					<div class="airport-code"><%=flight.getDestCode()%></div>
				<div class="airport-name"><%=flight.getDestName()%></div>
				</div>
				
				<button class="btn-info btn"  class="btn-info btn" style="float: right;
    margin: 21px 14px 0 0;" onclick="getPassengerDetails('<%=flight.getPnr()%>','<%=flight.getTxnId() %>')">Passenger List</button></div>

		</div> 

		
		<div class="passangerList" id="passenger<%= flight.getPnr() %>">
		
		
		</div>
	
	
	<%} %>

</div>
		
		</div>
	</div>
	</div>	
	
		
					


 
	</div>

</div>
</div>

</div>
        
        <script>
        function getPassengerDetails(pnr,bookingId){
        	$("#passenger"+pnr).html("<h2 style='text-align: center;margin: 20px;'>Your request is under process....</h2><div class='loader'></div>").show();
        	$(".loader").show();
        	$.ajax({
        		method:'Post',
        		url:'getTicketDetails',
        		data:"pnr="+pnr+"&bookingId="+bookingId,
        		success:function(data){
        			
        			$("#passenger"+pnr).html(data)
        		        		
        		/* $("#passenger"+pnr).find('.cnPnr').val(pnr)
        			$("#passenger"+pnr).find('.cnId').val(bookingId) */
        		}
        	})
        	
        }
function  cancelTicket(id,e){
	console.log(id)
	e.preventDefault();
	var formData =  $("#passengerDetails"+id).serialize()
	$("#passenger"+id).html("<h2 style='text-align: center;margin: 20px;'>Ticket cancellation is under process..</h2><div class='loader'></div>").show();
	$(".loader").show()

	   console.log($("#passengerDetails"+id).serialize())
$.ajax({
 type: 'POST',
 url: 'cancelTicket',
 data: formData , // serializes the form's elements.
 success: function(data)
 {
	   
	   console.log(data)
	   $("#passenger"+id).html(data)
 }
});
}

		    	   


</script> 
        </script>

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->


	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



