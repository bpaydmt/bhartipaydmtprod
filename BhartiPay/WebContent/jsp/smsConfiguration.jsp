<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 
	function getSMSConfiguration(aggId)
	{  
		
		$.ajax({
	        url:"GetSMSConfiguration",
	        cache:0,
	        data:"sconfig.aggreatorid="+aggId,
	        success:function(result){
	        	
	               document.getElementById('sconfig').innerHTML=result;
	               intialErrSpan();
	               
	         }
	  });  
		return false;
	}
	
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>SMS Configuration</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">   
                <font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
                <div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Aggregator</label>
															
													<s:select list="%{aggrigatorList}" headerKey="admin"
												headerValue="Common" id="aggregatorid"
												name="sconfig.aggreatorid" cssClass="form-username"
												requiredLabel="true"
												onchange="getSMSConfiguration(document.getElementById('aggregatorid').value);checkSelectVal(this,'#hiddenConfig')"
												 />
															
															
														</div>
													</div>
												</div>                 
		<form action="SaveSMSConfiguration" id="smsConf" method="post" name="PG">
		<input type="hidden" name="sconfig.aggreatorid" value="<s:property value="%{sconfig.aggreatorid}"/>" id="hiddenConfig" />


            
		
									<div class="row">
									
												

<div id="sconfig">

													<div class="col-md-12">
														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">SMS URL<font color="red"> *</font></label> <input
																	class="form-control  mandatory url-vl" name="sconfig.smsurl" type="text"
																	id="smsurl" placeholder="SMS URL"
																	value="<s:property value='%{sconfig.smsurl}'/>" />
															</div>
														</div>


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">SMS Feedid<font color="red"> *</font></label> 
																<input
																	class="form-control mandatory alphaNum-vl" name="sconfig.smsfeedid"
																	type="text" id="smsfeedid" placeholder="SMS Feedid"
																	value="<s:property value='%{sconfig.smsfeedid}'/>" />

															</div>
														</div>


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">SMS Username<font color="red"> *</font></label> <input
																	class="form-control mandatory alphaNum-vl" name="sconfig.smsusername"
																	type="text" id="smsusername" placeholder="SMS Username"
																	value="<s:property value='%{sconfig.smsusername}'/>" />

															</div>
														</div>



													</div>




													<div class="col-md-12">


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">SMS Senderid<font color="red"> *</font></label> <input
																	class="form-control mandatory alphaNum-vl" name="sconfig.smssenderid"
																	type="text"  placeholder="SMS Senderid"
																	value="<s:property value='%{sconfig.smssenderid}'/>" />

															</div>
														</div>

														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">SMS Password<font color="red"> *</font></label> <input
																	class="form-control mandatory" name="sconfig.smspassword"
																	type="password" id="smsurl" placeholder="SMS Password"
																	value="<s:property value='%{sconfig.smspassword}'/>" />
															</div>
														</div>



													</div>


												</div>



												<div class="col-md-12">

			
											
													
													<div class="col-md-4">
										<div class="form-group">
										
										<button type="button" class="btn btn-info btn-fill submit-form"	 	onClick="submitVaForm('#smsConf')" style="margin-top: 20px;">Submit
										</button>
										<button type="reset" onclick="resetForm('#smsConf')"  style="margin: 16px 19px 0 0;
    position: relative;
    top: 3px;" class="btn btn-info btn-fill" value="Reset">Reset</button>

									
									</div>
									</div>


									</div>
									
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->
<%-- 
<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>--> --%>


</body>
</html>


