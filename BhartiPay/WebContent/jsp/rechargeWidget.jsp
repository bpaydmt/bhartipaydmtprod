<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- SEARCH WIDGET -->
<div class="search-widget">
	<div class="post-tab">
		<!-- TAB HEADER -->
		<div class="tab-header tertiary">
			<!-- TAB ITEM -->
			<div class="tab-item selected">
				<p class="text-header">Mobile</p>
			</div>
			<!-- /TAB ITEM -->

			<!-- TAB ITEM -->
			<div class="tab-item">
				<p class="text-header">DTH</p>
			</div>
			<!-- /TAB ITEM -->

			<!-- TAB ITEM -->
			<div class="tab-item">
				<p class="text-header">DataCard</p>
			</div>
			<!-- /TAB ITEM -->
			<div class="tab-item">
				<p class="text-header">Landline Bill</p>
			</div>
		</div>
		<!-- /TAB HEADER -->

		<!-- TAB CONTENT -->
		<div class="tab-content void">
			<!-- COMMENTS -->
			<div class="comment-list">

				<font color="red"> <s:actionerror />
				</font> <font color="blue"> <s:actionmessage />
				</font>

				<form method="post" action="RechargeService"
					id="mobile-recharge-form" class="login-form">
					<input type="hidden" value="${csrfPreventionSalt}"
						name="csrfPreventionSalt">
					<ul>
						<li><input type="radio" name="rechargeBean.rechargeType"
							id="connection_type[]" value="PREPAID" checked
							onClick="hide_plan();"><label class="tab">Prepaid</label></li>
						<li><input type="radio" name="rechargeBean.rechargeType"
							id="connection_type[]" value="POSTPAID" onClick="hide_plan();"><label
							class="tab">Postpaid</label></li>
					</ul>

					<label class="select-block"> 
					<input type="text" 	name="rechargeBean.rechargeNumber" id="mobile_no" class="mandatory mobile" 
						placeholder="Enter your mobile number" maxlength="10"
						autocomplete="off" />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{prePaid}" name="rechargeBean.rechargeOperator"
							cssClass="payplutus_dropdown mandatory" 
							id="operator" headerKey="-1" headerValue="Please select operator"
							onChange="change_operator(this);" />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{circle}" name="rechargeBean.rechargeCircle"
							cssClass="payplutus_dropdown mandatory" id="circlei"
							headerKey="-1" headerValue="Please select a circle"
							onChange="change_circle(this);" />
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.rechargeAmt" id="amount"
						class="payplutus_textbox mandatory amount-vl" 
						placeholder="Enter amount" maxlength="4" />
					</label> <input type="button" class="button mid dark"
						value="Proceed To Recharge"
						onclick="submitVaForm('#mobile-recharge-form')" />
				</form>
			</div>
			<!-- /COMMENTS -->
		</div>
		<!-- /TAB CONTENT -->

		<!-- TAB CONTENT -->
		<div class="tab-content void">
			<!-- COMMENTS -->
			<div class="comment-list">
				<form method="post" id="d2h" action="RechargeService" class="login-form">
					<input type="hidden" value="${csrfPreventionSalt}"
						name="csrfPreventionSalt"> <input type="hidden"
						name="rechargeBean.rechargeType" value="DTH"> <label
						for="categories" class="select-block"> <s:select
							list="%{dth}" name="rechargeBean.rechargeOperator"
							cssClass=" mandatory "
							headerKey="-1" headerValue="Please select operator"/>
							
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.rechargeNumber" id="dth_mobile_no"
						class="mandatory onlyNum"
						placeholder="Enter your d2h number" maxlength="10" />
					</label> <label class="select-block"> <input type="hidden"
						name="rechargeBean.rechargeCircle" value="Delhi"> <input
						type="text" name="rechargeBean.rechargeAmt" id="dth_amount"
						class="mandatory amount-vl"
						placeholder="Enter amount" maxlength="4" />
					</label> <input type="button" class="button mid dark"
						value="Proceed To Recharge" onclick="submitVaForm('#d2h')" />
				</form>
			</div>
			<!-- /COMMENTS -->
		</div>
		<!-- /TAB CONTENT -->

		<!-- TAB CONTENT -->
		<div class="tab-content void">
			<!-- COMMENTS -->
			<div class="comment-list">
				<form method="post" id="datacard" action="RechargeService" class="login-form">
					<input type="hidden" value="${csrfPreventionSalt}"
						name="csrfPreventionSalt">
					<ul>
						<li><input type="radio" name="rechargeBean.rechargeType"
							id="connection_type[]" value="Datacard" checked><label
							class="tab">Prepaid</label></li>
						<li><input type="radio" name="rechargeBean.rechargeType"
							id="connection_type[]" value="Postpaid-Datacard"><label
							class="tab">Postpaid</label></li>
					</ul>
					<label class="select-block"> <input type="text"
						name="rechargeBean.rechargeNumber" id="datacard_mobile"
						class="mandatory mobile"
						placeholder="Enter your datacard number" maxlength="10" />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{prePaidDataCard}" name="rechargeBean.rechargeOperator"
						    cssClass=" mandatory"
							headerKey="-1"
							headerValue="Please select operator"
							 />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{circle}" name="rechargeBean.rechargeCircle"
							cssClass=" mandatory"
							headerKey="-1"
							headerValue="Please select a circle"
							 />
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.rechargeAmt" id="datacard_amount"
						class=" mandatory amount-vl"
						placeholder="Enter amount" maxlength="4" />
					</label> <input type="button" class="button mid dark"
						value="Proceed To Recharge" onclick="submitVaForm('#datacard')"/>
				</form>
			</div>
			<!-- /COMMENTS -->
		</div>
		<!-- /TAB CONTENT -->

		<!-- TAB CONTENT -->
		<div class="tab-content">
			<!-- ITEM-FAQ -->
			<div class="comment-list">
				<form method="post" id="landline" action="RechargeService" class="login-form">
					<input type="hidden" value="${csrfPreventionSalt}"
						name="csrfPreventionSalt"> <input type="hidden"
						name="rechargeBean.rechargeType" value="landline" checked>
					<label class="select-block"> <input type="text"
						name="rechargeBean.stdCode" id="landline_amount"
						class="std-vl mandatory"
						placeholder="Enter std code start with Zero(0123)" maxlength="6" />
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.rechargeNumber" id="landline_mobile"
						class="mandatory landline-vl"
						placeholder="Enter your Landline Number" maxlength="8" />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{landLine}" name="rechargeBean.rechargeOperator"
							cssClass="payplutus_dropdown mandatory" id="landline_operator"
							headerKey="-1" headerValue="Please select operator"
							 />
					</label> <label for="categories" class="select-block"> <s:select
							list="%{circle}" name="rechargeBean.rechargeCircle"
							cssClass="payplutus_dropdown mandatory" id="landline_circle "
							headerKey="-1" headerValue="Please select a circle"
							 />
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.rechargeAmt" id="landline_amount"
						class="amount-vl mandatory"
						placeholder="Enter amount" maxlength="4" />
					</label> <label class="select-block"> <input type="text"
						name="rechargeBean.accountNumber" id="landline_amount"
						class=" onlyNum mandatory"
						placeholder="Enter account number" maxlength="4" />
					</label> <input type="button" class="button mid dark"
						value="Proceed To Recharge"
						onclick="submitVaForm('#landline')" />
				</form>
			</div>
			<!-- /ITEM-FAQ -->
		</div>
		<!-- /TAB CONTENT -->
	</div>
	<!-- /POST TAB -->
</div>
<!-- /SEARCH WIDGET -->