<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommissionBean"%>
<%@page import="java.util.List"%>

<%

List<CommissionBean> list=(List<CommissionBean>)request.getAttribute("commList");


	
		//cBean.
		
%>	

<div class="row">
<div class="col-md-12">
<table class="table table table-bordered " style="font-size: 11px;">
    <thead>
      <tr>
        <th title="Txt Type" style="color:#000">Txn Mode</th>
      <th title="Txt Type" style="color:#000">Txn Type</th>
    <th title="Commission Type" style="width:120px;color:#000">Commission Type </th>
    <th title="Commission Percentage"  style="width: 80px;color:#000">Commission </th>

        <th title="Super Distributor Commission" style="width: 80px;color:#000">Super Distributor</th>
        <th title="Distributor Commission" style="color:#000">Distributor</th>
       <!--  <th title="Agent" style="color:#000">Agent</th> -->
          <th title="Range From" style="color:#000">Range From</th>
            <th title="Range To" style="color:#000">Range To</th>
            <th title="Fixed Charges" style="color:#000">Fixed Charges</th>
             <!-- <th title="Min charges" style="color:#000">Min Charges</th> -->
              <th title="Max Charges" style="color:#000">Max Charges</th>
              
              
             <th title="Action" style="color:#000">Action</th>
              
      </tr>
    </thead>
    <tbody id="commissionTempTable">
   
  <%
  if(list!=null){
  if(list.size()!=0 ){
   int count=0;
    for(CommissionBean commBean:list){ 
    	if(commBean.getTxntype() != null  && !commBean.getTxntype().isEmpty()){
    	 %>
    	
      <tr class="commissionRow txnType<%=commBean.getTxntype()%>">
      <td>
       <input type="hidden" class="txnModeInp" value="<%=commBean.getBankrefid() %>"    name="cBean[<%=count %>]bankrefid"/>
           <select class="txnModeSelect" disabled="disabled">
      
      
       <option value="1"  <%if( commBean.getBankrefid()== 1){ %>Selected <%  }%>>Bank</option>
       <option value="0"  <%if( commBean.getBankrefid() == 0){ %>Selected <%  }%>>Wallet</option>
        
       
       
       </select>
      </td>
       <td> 
       <input type="hidden" class="planTxtIdInp" name="cBean[<%=count %>].txntype" value="<%=commBean.getTxntype()%>" />
       <select class="planTxtIdSelect" disabled="disabled">
      
      
       <option value="NEFT"  <%if( commBean.getTxntype()!=null&&commBean.getTxntype().equalsIgnoreCase("NEFT")){ %>Selected <%  }%>>NEFT</option>
       <option value="IMPS"  <%if( commBean.getTxntype()!=null&&commBean.getTxntype().equalsIgnoreCase("IMPS")){ %>Selected <%  }%>>IMPS</option>
          <option value="VERIFY"  <%if(  commBean.getTxntype()!=null&&commBean.getTxntype().equalsIgnoreCase("Verify")){ %>Selected <%  }%>>VERIFY</option>
       
       
       </select></td>
        <td>
        <input type="hidden" class="CommissionTypeInp" name="cBean[<%=count%>].fixedorperc" >
        <select class="CommissionTypeSelect" disabled="disabled">
        
         <option value="P" <%if(commBean.getFixedorperc()!=null&& commBean.getFixedorperc().equalsIgnoreCase("P")){ %>Selected <%  }%>>Percentage</option>
          <option value="F" <%if( commBean.getFixedorperc()!=null&&commBean.getFixedorperc().equalsIgnoreCase("F")){ %>Selected <%  }%>>Fixed</option>
                          
        </select>
     
        </td>
      
         <td>
        <input type="text"  readonly="readonly" class="form-control commisionPercentage mustBeFilled  <%if( commBean.getFixedorperc()!=null&&commBean.getFixedorperc().equalsIgnoreCase("F")){ %>amount-vl <%  }else{%> percent-vl<%} %>" value="<%= commBean.getCommperc()%>" name="cBean[<%=count %>].commperc"/></td>
           <td>
        <input type="text"  readonly="readonly" class="form-control SuperDistributoInp mustBeFilled <%if( commBean.getFixedorperc()!=null&&commBean.getFixedorperc().equalsIgnoreCase("F")){ %>amount-vl <%  }else{%> percent-vl<%} %>l " value="<%= commBean.getSuperDisPer()%>" name="cBean[<%=count %>].superDisPer"/></td>
           <td><input type="text" readonly="readonly" class="form-control DistributorInp mustBeFilled <%if( commBean.getFixedorperc()!=null&&commBean.getFixedorperc().equalsIgnoreCase("F")){ %>amount-vl <%  }else{%> percent-vl<%} %>" value="<%=commBean.getDisPer()%>" name="cBean[<%=count %>].disPer"/></td>
<%--           <td><input type="text"  readonly="readonly" class="form-control AgentInp" value="<%= commBean.getAgen() %>" name="cBean[<%=count %>].agentPer"/></td> --%>
           <td><input type="text"  readonly="readonly" class="form-control mustBeFilled RangeFrom amount-vl zero-allowed rangeVal<%=commBean.getTxntype()%><%=commBean.getBankrefid() %>" value="<%= commBean.getRangeFrom()%>" name="cBean[<%=count %>].rangeFrom" /></td>
            <td><input type="text"  readonly="readonly"class="form-control mustBeFilled RangeTo amount-vl zero-allowed rangeVal<%=commBean.getTxntype()%><%=commBean.getBankrefid() %>" value="<%= commBean.getRangeTo()%>" name="cBean[<%=count %>].rangeTo" /></td>
            <td><input type="text"  readonly="readonly" class="form-control fixedChargesInp amount-vl" value="<%= commBean.getFixedCharges()%>" name="cBean[<%=count %>].maxCharges"/></td>
              <td>
                <input type="hidden"  class="maxChargesCheckBox"  value="<%=commBean.getMaxChargesCheck()%>" name="cBean[<%=count %>].maxChargesCheck"/>
                
              
              <input type="checkbox" style="float:left;" disabled="disabled" class="maxChargesCheckBoxCheck" <%= commBean.getMaxChargesCheck() == 1 ? "Checked" :  "" %> > <input type="text"  readonly="readonly"  class="form-control mustBeFilled MaxCharges" value="<%= commBean.getMaxCharges()%>" name="cBean[<%=count %>].maxCharges"/></td>
           <%--    <td><%= commBean.get%></td> --%>
           
              <td><input type="button" value="Edit" class="btn-grid EditRow"></td>
               
      </tr>
    

      
      
      
      
      
    <% 
    	}
    count++;
    }
    %>
    <script>
    
    	
    
    </script>
   <% 
    } 
} %>
 
 

    </tbody>
  </table>

</div>

</div>


<div class="row">
	<div class="col-md-12" style="    margin: 0 0 0 9px;">
	<a href="javascript:void(0)" class="btn" onclick="addCommissionRow()"> Add Commission Row</a> <input type="submit" value="Confirm And Save" style="margin: 0 0 0 10px" class="btn"  />
	</div>
	
</div>
