<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 



<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{walletBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{walletBean.stDate}"/>' + ' - ' + '<s:property value="%{walletBean.endDate}"/>');	
	  }
   }); 
</script> 
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 1, "asc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'DMT Details Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
  

    $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),


    });
    if($('#dpStart').val().length != 0){
    	var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
    	$('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
    	
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   
    
} );



	function converDateToJsFormat(date) {
	   
		var sDay = date.slice(0,2);
		var sMonth = date.slice(3,6);
		var yYear = date.slice(7,date.length)
	
		return sDay + " " +sMonth+ " " + yYear;
	} 

 </script>
 <style>
#agentFooter, .paymentref{display:none}

.setborder{
    padding: 7px 17px;
    width: 100%;
    border: 2px solid #ccc !important;}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12"> 
        
        
 
            

 
 
 
<div class="box2">
	<div class="box-inner">
	
		<div class="box-header">
			<h2>Money Transfer</h2>
		</div>
		
		<div class="box-content">
		
			<form action="GetDmtDetailsReport" method="post" autocomplete="off">
 <div class="row"> 

								<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="walletBean.id" id="id"
										value="<%=user.getSubAggregatorId()%>"> 
										<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
											    <i class="fa fa-calendar"></i>&nbsp;
											    <span></span> <i class="fa fa-caret-down"></i>
										</div>
										<input type="hidden"
										value="<s:property value='%{walletBean.stDate}'/>"
										name="walletBean.stDate" id="from"
										class="form-control" value=""  placeholder="Start Date"
										data-language="en"/>
										<input
													type="hidden"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="to"
													class="form-control " placeholder="End Date"
													data-language="en" >
								</div>
								
								
											<%
											if(user.getWhiteLabel()==0){
											%>
												<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-3" >
									<!-- <label for="dateTo">Date To:</label>  -->
									<br>
									<s:select list="%{aggrigatorList}" headerKey="All" class="setborder"
												headerValue="All" id="aggregatorid"
												name="walletBean.aggreatorid" cssClass="form-username setborder"
											    requiredLabel="true"/>
											</div>
										<%} %>
										

										<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-3">
											<br>
											<s:select list="%{bankTypeList}" headerKey="All"  class="setborder"
												headerValue="All" id="bankType" name="walletBean.bankType"
												cssClass="form-username setborder" requiredLabel="true" />
										</div>

										<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-3">
											<br>
											<s:select list="%{txnStatusList}" headerKey="All"  
												headerValue="All" id="txnStatus" name="walletBean.txnStatus"
												cssClass="form-username setborder" requiredLabel="true" 
												multiple = "true"
												/>
										</div>
										<div
											class="form-group col-md-3 txtnew col-sm-3 col-xs-6 text-left margin-top17">

											<div id="wwctrl_submit">
												<input class="btn btn-success" id="submit" type="submit"
													value="Submit"> <input class="btn btn-info"
													id="reset" type="reset" value="Reset">
											</div>
										</div>



									</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
			 
					<tr>
						<th width="5%" style="word-wrap: break-word;"><u>Date & Time</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Agent ID</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Txn ID</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Amount</u></th>
						<th width="5%" style="word-wrap: break-word;" ><u>Via</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Mode</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Mobile No</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Account No</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Beneficiary Name</u></th>	
						<th width="5%" style="word-wrap: break-word;"><u>IFSC Code</u></th>	
						<th width="5%" style="word-wrap: break-word;"><u>Bank Name</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Bank RRN</u></th>
						<th width="5%"><u>Status</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Distributor ID</th>
						<th width="5%" style="word-wrap: break-word;"><u>Aggregator ID</th>
						
						<th width="5%" style="word-wrap: break-word;display:none;"><u>SenderId</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>SenderMobileNo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>SenderName</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancetype</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>remittancestatus</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>paymentinfo</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>banktranrefno</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>bankresponse</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>updatedatetime</u></th>
					
						
							
					</tr>
				</thead>
				<tbody>
				<%
				List<DmtDetailsMastBean>list=(List<DmtDetailsMastBean>)request.getAttribute("agentList");

				if(list!=null){
				
					int j=1;
				for(int i=0; i<list.size(); i++) {
					DmtDetailsMastBean tdo=list.get(i);
					
					%>
		          	 <tr>
		          	 <td ><%=tdo.getTxndatetime() %></td>
		             <td><%=tdo.getAgentid() %></td>
		             <td><%=tdo.getTxnid()%></td>
		             <td><%=tdo.getTxnamount()%></td>
		             <td><%if(tdo.getTxnid().indexOf("FB")>-1)
		             {
		            	 out.print("Bank2");
		             }else if(tdo.getTxnid().indexOf("AS")>-1)
		             {
		            	 out.print("Bank1");
		              }
		              else if(tdo.getTxnid().indexOf("PT")>-1)
		              {
		            	 out.print("Bank3");
		               }
		              else if(tdo.getTxnid().indexOf("PU")>-1)
		              {
		            	 out.print("Bank3");
		               }
		              %></td>
		            
		            
		             <td><%=tdo.getRemittancemode()%></td>
		             <td><%=tdo.getSendermobileno()%></td>
		             <td><%=tdo.getBeneaccountno()%></td>
		             <td><%=tdo.getName()%></td>
		             <td><%=tdo.getBeneifsccode() %></td>
		             <td><%=tdo.getBankname()%></td>
		             <td><%=tdo.getBankrrn()!=null?tdo.getBankrrn():"-"%></td>
		             <td><%=tdo.getRemittancestatus()%></td>
		             <td ><%=tdo.getDistributerid()%></td>
		             <td><%=tdo.getAggreatorid()%></td>
		            
	
		             
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getSenderid()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getSendermobileno()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getSendername()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancetype()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getRemittancestatus()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getPaymentinfo()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBanktranrefno()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankresponse()%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getUpdatedatetime()%></td>
		             
                  	</tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
 

   
        
        

        <!-- contents ends -->

       

  		</div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>


</body>
</html>



