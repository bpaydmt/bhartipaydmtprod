
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 <%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'User List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'User List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'User List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'User List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'User List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function getSubagentByAgentId(agentId)
{  
	
	$.ajax({
        url:"GetSubagent",
        cache:0,
        data:"reportBean.agentId="+agentId,
        success:function(result){
        	
               document.getElementById('subagent').innerHTML=result;
               
         }
  });  
	return false;
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12"> 
        
        
 
            

 
 
<div class="box2">
	<div class="box-inner">
	
		<div class="box-header ">
			<h2>Search Users</h2>
		</div>
		<div class="box-content ">
	<form action="SearchUser" method="post">
		   <div class="row"> 
							<div class="form-group  col-md-12  txtnew  col-xs-6">
							<%if(user.getUsertype()==99||(user.getWhiteLabel()==0&&user.getUsertype()==4)){ %>
								<div class="col-sm-3">
									<label for="email">Select Aggregator</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{aggrigatorList}" headerKey="-1"
												headerValue="Select Aggregator" id="aggregator"
												name="reportBean.aggId" cssClass="form-username"
												requiredLabel="true"
												onchange="getDistributerByAggId(document.getElementById('aggregator').value);" />


	<!--
	private String aggId;
	private String distId;
	private String agentId; -->
										</div>
									</div>
								</div>

<%}if(user.getUsertype()==99||user.getUsertype()==4){ %>
								<div class="col-sm-3">
									<label for="email">Select Distributor</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="distributor">

											<s:select list="%{distributorList}" headerKey="-1"
												headerValue="Select Distributor" id="distri"
												name="reportBean.distId" cssClass="form-username"
												requiredLabel="true"
												onchange="getAgentByDistId(document.getElementById('distri').value);" />

										</div>
									</div>

								</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3){ %>
								<div class="col-sm-3">
									<label for="email">Select Agent</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="agent">

											<s:select list="%{agentList}" headerKey="-1"
												headerValue="Select Agent" id="agentsub"
												name="reportBean.agentId" cssClass="form-username"
												requiredLabel="true"
												onchange="getSubAgentByAgentId(document.getElementById('agentsub').value);"
												/>

										</div>
									</div>

								</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3||user.getUsertype()==2){ %>
								<div class="col-sm-3">
								<label for="email">Select Sub-agent</label> <br>
								<div class="wwgrp" id="wwgrp_paymentMethods">
									<div class="wwctrl" id="subagent">

										<s:select list="%{subagentList}" headerKey="-1"
											headerValue="Select Sub-agent" id="subagentsel"
											name="reportBean.subagentId" cssClass="form-username"
											requiredLabel="true"											
											/>

									</div>
								</div>

							</div>
<%} %>

								<div
									class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit">
										<input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Search">
									</div>
								</div>
							</div>

</div>
</form>
	</div>
	</div>


</div>
			
		
		
							
							
			<%
		List<WalletMastBean>userList=(List<WalletMastBean>)request.getAttribute("userL");
	
			%>				
							
			 <div id="xyz">
			<table id="example" class="scrollD cell-border dataTable no-footer">
				<thead>
				
				
					<tr>
						<th><u>User Id</u></th>
						
						<th><u>Name</u></th>
						<th><u>Mobile</u></th>
						<th><u>Email</u></th>
						<th><u>Address 1</u></th>
						<th><u>Address 2</u></th>
						<th><u>City</u></th>
						<th><u>State</u></th>
						<th><u>PIN Code</u></th>
						<th><u>Status</u></th>
						<th width="12%"><u></u></th>
						
						
					</tr>
				</thead>
				<tbody>
				<%
				if(userList!=null&&userList.size()>0){	
				for(WalletMastBean wmb:userList){
				%>
				  	<tr>
		             <td><%=wmb.getId() %></td>
		             
		             <td><%=wmb.getName()%></td>
		             <td><%=wmb.getMobileno() %></td>
		              <td><%=wmb.getEmailid()%></td>
		              <td><%if(wmb.getAddress1()==null){out.print("-");}else{out.print(wmb.getAddress1());}%></td>
		               <td><%if(wmb.getAddress2()==null){out.print("-");}else{out.print(wmb.getAddress2());}%></td>
		                <td><%if(wmb.getCity()==null){out.print("-");}else{out.print(wmb.getCity());}%></td>
		                <td><%if(wmb.getState()==null){out.print("-");}else{out.print(wmb.getState());}%></td>
		                 <td><%if(wmb.getPin()==null){out.print("-");}else{out.print(wmb.getPin());}%></td>
		                  <td><%if(wmb.getUserstatus().equalsIgnoreCase("A")){out.print("Active");}else if(wmb.getUserstatus().equalsIgnoreCase("D")){out.print("Inactive");}else{out.print("Suspended");}%></td>
<td><form action="UpdateUser" method="post">
<input type="hidden" name="reportBean.userId" value="<%=wmb.getId()%>">
<input type="submit" value="Edit Profile" class="btn btn-sm btn-block btn-success">
</form> <br><form action="SetLimit" method="post">
<input type="hidden" name="config.id" value="<%=wmb.getId()%>">
<input type="hidden" name="config.walletid" value="<%=wmb.getWalletid()%>">
<input type="submit" value="Set limit" class="btn btn-sm btn-block btn-success">
</form> </td>                  		
 </tr>
                  <%
                  }}
				%>
			        </tbody>		</table>
		</div> 
		
		
 








 
 
 
  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


