<%@page import="com.bhartipay.wallet.report.bean.AgentConsolidatedReportBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Agent Transaction Consolidated Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Agent Transaction Consolidated Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Agent Transaction Consolidated Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Agent Transaction Consolidated Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Agent Transaction Consolidated Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
   
  
	    
	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	     

	    });
	    if($('#dpStart').val().length != 0){
	     

	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	       $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	
	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");
DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Agent Transaction Consolidated Report</h2>
		</div>
		
		<div class="box-content row">
		
			<form action="GetAgentConsolidatedReport" method="post">
								
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> 
									
									
								<%-- 	<input
													type="text"
													value="<s:property value='%{inputBean.userId}'/>"
													name="inputBean.userId" id="UserId"
													class="form-control" placeholder=" Agent ID"
													data-language="en"  required> --%>
													
									<s:select list="%{agentList}" headerKey="-1"
												headerValue="Select Agent" id="agentsub"
												name="inputBean.userId" cssClass="form-username"
												requiredLabel="true"
												/>				
													
													
													
													
											</div>
											
											
											

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
																		
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="dpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Txn Date</u></th>
						<th><u>Agent Id</u></th>
						<th><u>Name</u></th>
						<th><u>type</u></th>
						<th><u>Debit Amount</u></th>
						<th><u>Credit Amount</u></th>
						
						
									 			
						
						
					</tr>
				</thead>
				<tbody>
			 <%
			 List<AgentConsolidatedReportBean> pb=(List<AgentConsolidatedReportBean>)request.getAttribute("listResult");

			if(pb!=null){
				for(int i=0; i<pb.size(); i++) {
					AgentConsolidatedReportBean tdo=pb.get(i);%>
		   <tr>
		          	   <td><%=tdo.getTxnDate()%></td>
		          	    <td><%=tdo.getAgentId()%></td>
		          	     <td><%=tdo.getName()%></td>
		          	      <td><%=tdo.getType()%></td>
		          	      <td><%=d.format(tdo.getTxnDebit())%></td>
		          	       <td><%=d.format(tdo.getTxnCredit())%></td>
		          	       
	
		          		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



