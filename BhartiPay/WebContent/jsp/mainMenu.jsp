<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>

<style type="text/css">
 	.nav>li>a {
		padding: 10px;
		/* background-color: #87c440; */
	} 
	ul.main-menu li.active {
		margin-left: 0px;
		/*background-color: #87c440;*/
	} 
	nav ul ul {
    margin: 0;
    display: none;
    background: rgba(69, 69, 69, .6);
    /* padding: 7px 0; */
    padding: 3px 0px 0px 0px;
	}

	/* fallback */
	@font-face {
	  font-family: 'Material Icons';
	  font-style: normal;
	  font-weight: 400;
	  src: url(https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
	}
	
	.material-icons {
	  font-family: 'Material Icons';
	  font-weight: normal;
	  font-style: normal;
	  font-size: 24px;
	  line-height: 1;
	  letter-spacing: normal;
	  text-transform: none;
	  display: inline-block;
	  white-space: nowrap;
	  word-wrap: normal;
	  direction: ltr;
	  -webkit-font-feature-settings: 'liga';
	  -webkit-font-smoothing: antialiased;
	}

	.material-icons.md-25 { font-size: 25px; }
	
	.smart-style-3.menu-on-top aside#left-panel nav>ul>li>a {
	 font-weight:700;
	}

</style>


</head>



	<!-- desktop-detected menu-on-top pace-done smart-style-3 fixed-header fixed-navigation fixed-page-footer -->
	<%
		User user = (User) session.getAttribute("User");
		Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
		System.out.println(mapResult);
		String domainName = mapResult.get("domainName");
		Map<String,String> menuMap=(Map<String,String>)session.getAttribute("menuMap");
		List<String> aggMenus=(List<String>)session.getAttribute("aggMenus");
		List<String> aggRoles=(List<String>)session.getAttribute("aggRoles");
		if(aggMenus==null){
			aggMenus=new ArrayList();
		}
		if(aggRoles==null){
			aggRoles=new ArrayList();
		}
		String userId=user.getId();
		boolean whiteLevel=false;
		if(user.getWhiteLabel()==1){
		whiteLevel=true;
		
		} 
%>

<body class="smart-style-3 menu-style<%=mapResult.get("themes")%> fixed-header menu-on-top fixed-navigation fixed-page-footer">

	<script>
	    function openDmt(isMaintenanceGoingOn){ 
		 closeDmitWindow() 
		 
		 if(isMaintenanceGoingOn == false){
		 $.get('CheckSession',function(result){
		  console.log(result)
		  if(result.status == "TRUE"){
		     MYWalletWINDOW = window.open('MoneyTransfer', "", "width=1366,height=800");
		  }else{
		     alert("Your session has expired. Please login again.")
		     window.location.replace("UserHome");
		  }
		  
		 })
		 }else{
		  $("#maintenancePop").modal('show')
		 }
		 
		} 

		function openDmtFromBak(isMaintenanceGoingOn){ 
		  //alert("isMaintenanceGoingOn"+isMaintenanceGoingOn);
		 if(isMaintenanceGoingOn == false){
	     console.log("if");
		 $.get('CheckSession',function(result){
		  //console.log(result)
		  if(result.status == "TRUE"){
			  //return true;
			  window.location.replace("MoneyTransferBK");
		  }else{
		     alert("Your session has expired. Please login again.")
		     window.location.replace("UserHome");
		  }
		  
		 })
		 }else{
		   console.log("else");
		  $("#maintenancePop").modal('show')
		 }
		 //alert("here");
		} 
		
		
		function openDmtFromBak2(isMaintenanceGoingOn){ 
			  //alert("isMaintenanceGoingOn"+isMaintenanceGoingOn);
			 if(isMaintenanceGoingOn == false){
			 $.get('CheckSession',function(result){
			  //console.log(result)
			  if(result.status == "TRUE"){
				  window.location.replace("MoneyTransferBK2");  
				  //return true;
		 
			  }else{
			     alert("Your session has expired. Please login again.")
			     window.location.replace("UserHome");
			  }
			  
			 })
			 }else{
			  $("#maintenancePop").modal('show')
			 } 
			} 
		
		function openDmtFromBak3(isMaintenanceGoingOn){ 
			  //alert("isMaintenanceGoingOn"+isMaintenanceGoingOn);
			 if(isMaintenanceGoingOn == false){
			 $.get('CheckSession',function(result){
			  //console.log(result)
			  if(result.status == "TRUE"){
				  window.location.replace("MoneyTransferBK3");  
				  //return true;
		 
			  }else{
			     alert("Your session has expired. Please login again.")
			     window.location.replace("UserHome");
			  }
			  
			 })
			 }else{
			  $("#maintenancePop").modal('show')
			 } 
			}
	</script>



	<aside id="left-panel" style="z-index: 9;"> 

		<!-- User info -->
		<div class="login-info"> 
			<!-- <span style="padding-left: 15px;">  
				<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut" style="margin-top: 8px;"> 
					<i class="fa fa-user user-icon-colo" aria-hidden="true"></i>
					<span style="margin-left: 3px;margin-right: 3px;color: #fff!important;">
						User Name 
					</span>
					<i class="fa fa-angle-down user-icon-colo"></i>
				</a> 
				
			</span> --> 
		</div>
		<!-- end user info -->

		<nav class="left-menu" style="position: relative;z-index: 99999;">   
            <ul class="nav nav-pills nav-stacked main-menu">
					
					<li class="nav-header"></li>
                    <li id="s">
                    	<a class="ajax-link" href="Home" style="box-shadow: inset 0px 0px 0px 0px #434756;">
                    		<i class="glyphicon glyphicon-home"></i>
                    		<%-- <span class="menuIconcolomn"></span> --%>
							<span class="menu-item-parent">Home</span>
						</a>
					</li>
						  		
					<%
						if (user.getUsertype() == 99) {	
					%>
	
					<li id="s">
						<a class="ajax-link" href="CreateUser">
							<i class="glyphicon glyphicon-user"></i>
							<span class="menuIconcolomn"></span>
					        <span>Create User</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="WalletConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<span class="menuIconcolomn"></span>
					        <span>Wallet Configuration</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="SMSConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<!--  -->
					        <span >SMS Configuration</span>
					    </a>
					</li>
							
					<li id="s">
						<a class="ajax-link" href="EmailConfiguration">
							<i class="glyphicon glyphicon-edit"></i>
							<!-- <span  class="menuIconcolomn"></span> -->
					        <span >Email Configuration</span>
					    </a>
					</li>
							
				    <li id="s">
				    	<a class="ajax-link" href="AuthoriseAggregator">
				    		<i class="glyphicon glyphicon-edit"></i>
				    		<!-- <span  class="menuIconcolomn"></span> -->
					        <span >Authorise Aggregator</span>
					    </a>
					</li>
							

					<%-- <li id="ea">
						<a class="ajax-link" href="ShowProfile">
							<i class="glyphicon glyphicon-edit" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
					        <span style="padding: 8px;"> Edit Profile</span>
					    </a>
					</li> --%>

					<%-- <li id="rp">
						<a class="ajax-link" href="ResetPassword">
							<i class="glyphicon glyphicon-lock" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
							<span style="padding: 8px;"> Change Password</span>
						</a>
					</li> --%>
							
					<%-- <li id="rp">
						<a class="ajax-link" href="GetFaq"  >
							<i class="glyphicon glyphicon-lock" ></i>
							<!-- <span  class="menuIconcolomn"></span> -->
							<span style="padding: 8px;"> FAQ</span>
						</a>
					</li> --%>
							
					<!-- <li id="rp">
						<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
							<i class="glyphicon glyphicon-lock" ></i>
							<span  class="menuIconcolomn"></span>
                            <span style="padding: 8px;"> Grievance Policy </span>
                        </a>
                    </li> --> 

					<%}else if (user.getUsertype() == 1) { %>	

					<li id="aggFT"> 
						<a href="#"> 
							 <!-- <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  --> 
							<i class="glyphicon glyphicon-folder-close" ></i> 
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">
						<% if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>  
							<%} %>
						</ul>
					</li>
					
					<li id="s">
					    <a class="ajax-link" href="Recharge"> 
							  <!-- <i>
							    <img src="./image/recharges-menu.png" style="width: 30%;">
							  </i> -->
							  <i class="glyphicon glyphicon-phone" ></i>
							  <span style="">Recharges</span>
						</a>
					</li>
							


							
							
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style="">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							<%if (user.getUsertype() == 3){%>
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							<%} %>
						
						</ul>
					</li>
					
					<li id="mb2">
						<a class="ajax-link" href="OrdersView">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Orders View</span>
					    </a>
					</li>
					
					<li id="ctl">
						<a class="ajax-link" href="WalletToWallet">
							<i class="glyphicon glyphicon-cloud-upload" ></i>
							
					        <span >Wallet to Wallet</span>
					    </a>
					</li>
					
					<li id="ctl">
						<a class="ajax-link" href="KycUpload">
							<i class="glyphicon glyphicon-cloud-upload" ></i>
							
					        <span >Upload KYC</span>
					    </a>
					</li>
					
					<%-- <li id="ea">
						<a class="ajax-link" href="CustomerSupport">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					</li> --%>
					
					
					<%-- <li id="ea">
						<a class="ajax-link" href="ShowProfile">
							<i class="glyphicon glyphicon-edit" ></i>
							
					        <span style="padding: 8px;"> Edit Profile</span>
					    </a>
					</li> --%>
					
					
					

					<%-- <li id="rp">
						<a class="ajax-link" href="ResetPassword">
							<i class="glyphicon glyphicon-lock" ></i>
							
							<span style="padding: 8px;"> Change Password</span>
						</a>
					</li> --%>
							
					<%-- <li id="rp">
						<a class="ajax-link" href="GetFaq"  >
							<i class="glyphicon glyphicon-lock" ></i>
							
							<span style="padding: 8px;"> FAQ</span>
						</a>
					</li> --%>
							
					<!-- <li id="rp">
						<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
							<i class="glyphicon glyphicon-lock" ></i>
							
                            <span style="padding: 8px;"> Grievance Policy </span>
                        </a>
                    </li> -->

        <%}else if (user.getUsertype() == 8){ %>

                      

					<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%} %>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>
 
					
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					    
					 <li id="s">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-edit" ></i> 
						        <span>User Dashboard</span>
						    </a>
						    
						     <ul class="nav nav-pills nav-stacked">
								<li id="da">
									<a href="GetAgentDtlsBySdList">SD DashBoard</a>
								</li>
								<li id="alist">
									<a href="GetAgentDtlsByDList">Distributer DashBoard</a>
								</li>
								<li id="alist">
									<a href="GetAgentDtlsByRList">Retailer DashBoard</a>
								</li>
							 	
							</ul>
						    
					  </li>
					
					 <li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								 <li id="alist">
									<a href="OnboardBySales">On Board</a>
								</li>
								
								<li id="da">
									<a href="DeclinedNewAgentList">Declined List</a>
								</li>
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
								 
							</ul>
						</li> 
					
				 
					
					<li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	
						    	<li id="ags">
									<a href="GetAepsReportBySuper">AEPS</a>
								</li> 
								<li id="ags">
									<a href="GetDmtReportBySuper">Money Transfer</a>
								</li>
								 
								 <li>
								 <a class="ajax-link" href="Recharge">  Recharge&Bill</a>
								</li>
								
								<!--  <li>
								 <a class="ajax-link" href="RechargeMaintainance">  Recharge&Bill</a>
								</li>
								 -->
								
								<li id="rech">
									<a href="GetMatmReportByDistAgg">m-ATM Report</a>
								</li>
								 
								
								<li id="ags">
									<a href="BBPSReportDistAggregator">BBPS</a>
								</li>  
								<li id="ags">
									<a href="GetAepsPayoutReportByDistAgg">Bank Settlement</a>
								</li>
								
								<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
								
							</ul>
						</li> 
					
					
					<li id="u">
						<a class="ajax-link" href="StartCustomerCare">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					   </li>
					
					
					  <li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Support Tickets</span>
						    </a>
					  </li>  

<%}else if (user.getUsertype() == 9){ %>

                     

					<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%} %>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>

					
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 


					  <li id="s">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-edit" ></i> 
						        <span>User Dashboard</span>
						    </a>
						    
						    <ul class="nav nav-pills nav-stacked">
								 <li id="alist">
									<a href="GetAgentDtlsBySoList">Sales Dashboard</a>
								</li>
								
								<li id="da">
									<a href="GetAgentDtlsBySdList">SD DashBoard</a>
								</li>
								<li id="alist">
									<a href="GetAgentDtlsByDList">Distributer DashBoard</a>
								</li>
								<li id="alist">
									<a href="GetAgentDtlsByRList">Retailer DashBoard</a>
								</li>
							 	
							</ul>
						    
						</li>
						
                       <li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								 <li id="alist">
									<a href="OnboardByManager">On Board</a>
								</li>
								
								<li id="da">
									<a href="DeclinedNewAgentList">Declined List</a>
								</li>
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
							 	
							</ul>
						</li>  



                  <li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	
						    	<li id="ags">
									<a href="GetAepsReportBySuper">AEPS</a>
								</li> 
								<li id="ags">
									<a href="GetDmtReportBySuper">Money Transfer</a>
								</li>
								 
								 <li>
								 <a class="ajax-link" href="Recharge">  Recharge&Bill</a>
								</li>
								
								<li id="rech">
									<a href="GetMatmReportByDistAgg">m-ATM Report</a>
								</li>
								  
								<li id="ags">
									<a href="BBPSReportDistAggregator">BBPS</a>
								</li>  
								<li id="ags">
									<a href="GetAepsPayoutReportByDistAgg">Bank Settlement</a>
								</li>
								
								<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
								
							</ul>
						</li>


                   <li id="u">
						<a class="ajax-link" href="StartCustomerCare">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					   </li>
					   
					   

                      <li id="agsr">
							<a href="#"> <i class="fa fa-ticket" aria-hidden="true"></i>
								<!-- <i class="glyphicon glyphicon-list-alt" ></i> -->
								
						        <span style="">Support Tickets</span>
						    </a>
					  </li>

					<%}else if (user.getUsertype() == 2 && !user.getId().startsWith("MERCSPAY")) { %>


					<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%} %>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>

					 <%-- <li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i>
							
					        <span style="padding: 8px;">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Retailer</a>
							</li> 
						</ul>
					</li>  --%>
					
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					
					<li id="s">
					 <a class="ajax-link" href="Recharge"> 
					<!--  <a class="ajax-link" href="RechargeMaintainance"> -->
					 
					    <!--  <a class="ajax-link" href="PrepaidRecharge">  -->
							  <!-- <i>
							    <img src="./image/recharges-menu.png" style="width: 30%;">
							  </i> -->
							   <i class="glyphicon glyphicon-phone" ></i>
							  <span style="">Recharge&Bill</span>
						</a>
					</li>
							

							


					<%
					if(user.getBank()==0 && user.getWallet() == 1){
						%>
								 
					<li id="rpq"> 	
							<a href="#"> 
								<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
								<i class="glyphicon glyphicon-transfer" ></i>
								<span style=" ">Money Transfer</span>
							</a>
							<ul class="nav nav-pills nav-stacked"> 
								<li id="mtBank">
								    <a onclick="openDmtFromBak(false)" href="MoneyTransferBK">Bank1</a>
								</li>
								<li id="refundAgentLink">
									<a  href="agentRefund">Refund</a>
								</li> 
								<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 	
							</ul> 
					</li>
								
								
					<%}else if(user.getWallet() == 0 && user.getBank()== 1){ %>
									 
					<li id="rp"> 	
						<a href="#"> 
							<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
							<i class="glyphicon glyphicon-transfer" ></i>
							<span style=" ">Money Transfer</span>
						</a>
						<ul class="nav nav-pills nav-stacked"> 
							<li id="mtBank">
							<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK2">Bank1</a>
							</li> 
							
							<li id="mtBank">
								<a onclick="openDmtFromBak(false)" href="MoneyTransferBK">Bank2</a>
							</li> 
							
							<li id="refundAgentLink">
								<a  href="agentRefund">Refund</a>
							</li>
							<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 
						</ul> 
						</a>
					</li> 

					<%}else if(user.getWallet() == 1 && user.getBank()== 1){ %> 

					<li id="rp"> 
						<a href="#"> 
							<!-- <i>
								   <img src="./image/money-transfer.png" style="">
								</i> -->
							<i class="glyphicon glyphicon-transfer" ></i>
							<span style=" ">Money Transfer</span></a>
							<ul class="nav nav-pills nav-stacked"> 
								<%if(!"0".equalsIgnoreCase(user.getBank1())){ %>
								<li id="mtBank">
								<a onclick="openDmtFromBak2(false)" href="MoneyTransferBK2">Bank1</a>
								</li> 
								<%} if(!"0".equalsIgnoreCase(user.getBank2())){%>
								<li id="mtBank">
									<a onclick="openDmtFromBak(false)" href="MoneyTransferBK">Bank2</a>
								</li> 
								<%} if(!"0".equalsIgnoreCase(user.getBank3())){ %>
								<li id="mtBank">
									<a onclick="openDmtFromBak3(false)" href="MoneyTransferBK3">Bank3</a>
								</li> 
								<%} %>
								<li id="refundAgentLink">
									<a  href="agentRefund">Refund</a>
								</li>
								<li id="#">
									<a  href="TransactionStatus">Txn Status</a>
								</li> 
							</ul> 
							</a>
						</li>
								
						<%}
					        if(domainName.toLowerCase().indexOf("transpaytech.co.in")>-1||domainName.toLowerCase().indexOf("b2b.ezeejunction.com")>-1
							 ||domainName.toLowerCase().indexOf("partner.bhartipay.com")>-1 || domainName.toLowerCase().indexOf("localhost")>-1){
						%>

						<!-- <li id="rp"> 
						    <a href="#"> 
						    	<i class="glyphicon glyphicon glyphicon-plane" ></i>
						    	
							    <span style="padding: 8px;">Travel </span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<li id="mtWallet">
									<a  href="flightBooking">Flight Booking</a>
								</li>
								<li id="mtBank">
									<a href="getFlightDetail">Flight Details</a>
								</li> 
							</ul> 
							</a>
						</li> -->


						<%} %>
					        
					        <%-- <%  if("1".equalsIgnoreCase(user.getAepsChannel())){%> 

							<li id="ea">
								<a class="ajax-link" href="ASAEPSPayments">
									<i>
									   <img src="./image/aeps-icon.png">
									</i> 
									<!-- <i class="glyphicon glyphicon-hand-down" ></i> -->
							        <span style=" ">AEPS</span>
							    </a>
							</li> 
							 <%}
					        else if("2".equalsIgnoreCase(user.getAepsChannel())){ %> 	
					        <%}else if (user.getUsertype() == 2) { %>
							<li id="ea">
								<a class="ajax-link" href="AepsCall">
									<i class="material-icons md-48">fingerprint</i>
							        <span style=" ">AEPS</span>
							    </a>
							</li>
							
								<%}   
						
						
						%> --%> 
						
						
						<li id="rp"> 	
						<a href="#"> 
							<i>
							<img src="./image/aeps-icon.png">
							</i> 
							<span style=" ">AEPS</span>
						</a>
						<ul class="nav nav-pills nav-stacked"> 
							<li id="mtBank">
								<a href="ASAEPSPayments" target="_blanck">Yes Bank</a>
							</li> 
							
							<li id="mtBank">
								<a href="ASICICIAEPSPayments" target="_blanck">ICICI Bank</a>
							</li> 
							
							<li id="mtBank">
								<a  href="AepsCall">Fino Bank</a>
							</li> 
							
							
							<%-- 
							<% if("BPA001000".equalsIgnoreCase(user.getId())){ %>
							
							<li id="mtBank">
								<a  href="AepsTransactionCallApi">Fino Transaction</a>
							</li>
							 
							<li id="mtBank">
									<a  href="AepsBalanceEnquiryApi">Fino Enquiry</a>
							</li>
							
							<%} %>
							 --%>
							
						<!--	
							<li id="mtBank">
								<a  href="finoAEPSStatusResponseApi">Fino Response</a>
							</li> 
							
							<li id="mtBank">
									<a  href="ASAEPSPaymentsApi">Aadharshila Bank</a>
							</li>
						 -->
						 
						 <!-- 
							<li id="mtBank">
									<a  href="MakeAepsTxn">AEPS CashWithdraw</a>
							</li>
							
							<li id="mtBank">
									<a  href="MakeAepsBalEnquiry">AEPS BalanceEnquiry</a>
							</li>
							
							<li id="mtBank">
									<a  href="MakeAepsTxnStatus">AEPS Status Enquiry</a>
							</li>
						 -->	
							
							
						</ul> 
						
					</li> 
						
						
						
						
						<li id="ea">
								<a class="ajax-link" href="MatmCall">
									<i>
									   <img src="./image/m-ATM.png">
									</i> 
									<!-- <i class="glyphicon glyphicon-print" ></i> -->
							        <span style="">m-ATM</span>
							    </a>
						</li>
						
						<li id="#">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-bold" style="font-size: 18px;"></i>
						        <span style="padding: 7px;">BBPS</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						       
								<li >
									<a href="bbpsServices">Pay Bill</a>
								</li>
							   
								 <li>
									<a href="complaintRaise">Register Complaint</a>
								</li> 
								<li>
									<a href="#">Check Status</a>
									<ul>
						              <li><a href="complaintStatus">Complaint Status</a></li>
						              <li><a href="getTxnStatus">Transaction Status</a></li>
						            </ul>
								</li>
							</ul>
 						</li>
						
						
				    <li>
                       <a href="#"> <i class="glyphicon glyphicon-stats"></i> CMS  </a>
							<ul>
								<li><a href="GetCmsPayment">CMS Payment</a></li>
                                <li><a href="GetCmsResponse">CMS Txn Status</a></li>
					      </ul>
					</li>
					 	
							
						 <%if(user != null && !"OAGG001058".equalsIgnoreCase(user.getAggreatorid())) {%>
						
						<%--<li id="#">
								<a href="#"> 
									<i class="glyphicon glyphicon-stats"></i>
							        <span >Utilities</span>
							    </a>
								<ul class="nav nav-pills nav-stacked">
									<li id="#">
										<a href="AgentAepsMatmSettlement">AEPS Settlement</a>
									</li>
								</ul>
						</li>--%>
						
						<li id="ea">
								<a class="ajax-link" href="AgentAepsMatmSettlement">
									<i class="fa fa-bank"></i>
							        <span style=" ">Move To Bank</span>
							    </a>
						</li>
						 <%}	%>
							
						<li id="u">
					 		<a href="#"> 
					 			<i class="glyphicon glyphicon-list-alt" ></i>
					 			
							    <span style=" ">Report</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
							
							    <li>
									<a href="RechargeReport">Recharge&Bill</a>
								</li>
								<li>
									<a href="GetTransacationLedgerDtlBK">Money Transfer</a>
								</li>
							
								<%if(user.getAgentCode() != null && !user.getAgentCode().isEmpty())
								{%>
								    <li>
								    	<a href="GetAepsReport">AEPS</a>
								    </li>
								     <li>
								    	<a href="GetMatmReport">m-ATM</a>
								    </li>
								    
								    <li>
									<a href="BBPSReport">BBPS</a>
								   </li>
								    
								    <li>
								    	<a href="GetAepsPayoutReport">Bank Settlement</a>
								    </li>
								   
								    <li>
								    	<a href="AddAccountSelf">Add Account</a>
								    </li>
								    
								    <li><a href="GetCmsReport">CMS Report</a>
								    </li>
								    
								<%} %>
								 
							
								
								
								<!-- <li >
									<a href="ShowCashBackPassbook">Cash Back Wallet History</a>
								</li>
								<li>
									<a href="GetSenderKycList">Sender KYC List</a>
								</li> -->
								
							</ul> 
						</li> 	
							
							
						<li id="#">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-download-alt" ></i>
								
						        <span style="">Download</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
								
								<li >
									<a href="https://partner.finopaymentbank.in/Testcamerabio.html" target="_blank">Driver</a>
								</li>
								<li>
									<a href="GetUserCertificate" target="_blank">Certificate</a>
								</li> 
								<li>
									<a href="GetUserIdCard" target="_blank">ID CARD</a>
								</li> 
							</ul> 
						</li>
						
						
 						
 						<%-- 
 						<li>
 							<a class="ajax-link" href="#">
								<i class="fa fa-bank"></i>
						        <span style="padding: 7px;">Bank List</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked" id="ulBankList"></ul>
 						</li>
 						 --%>
						<%-- <li id="ea">
							<a class="ajax-link" href="CustomerSupport">
								<i class="glyphicon glyphicon-headphones" ></i>
								
						        <span style="padding: 7px;">Customer Support</span>
						    </a>
						</li> --%>
						
						
						
						<%-- <li id="ea">
							<a class="ajax-link" href="ShowProfile">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span style="padding: 8px;"> Profile</span>
						    </a>
						</li> --%>

					    <%-- <li id="rp">
					    	<a class="ajax-link" href="ResetPassword">
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> Change Password</span>
							</a>
						</li> --%>
							
					    <%-- <li id="rp">
					    	<a class="ajax-link" href="GetFaq"  >
					    		<i class="glyphicon glyphicon-question-sign" ></i>
					    		
							    <span style="padding: 8px;"> FAQ</span>
							</a>
						</li> --%>
							
						<!-- <li id="rp">
							<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
								<i class="glyphicon glyphicon-asterisk" ></i>
								
                                <span style="padding: 8px;"> Grievance Policy </span>
                            </a>
                        </li> -->

						<%
						}else if(user.getUsertype() == 2 && user.getId().startsWith("MERCSPAY")){
						%>
					<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Load Wallet
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%}%>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>

					 <%-- <li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i>
							
					        <span style="padding: 8px;">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Retailer</a>
							</li> 
						</ul>
					</li>  --%>
					
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					
					<li id="mb1">
						<a class="ajax-link " href="GetTransacationLedgerPayout">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Payout Ledger</span>
					    </a>
					   
					</li> 
					   
					</li> 
						<% 
						}else if (user.getUsertype() == 3) { 
						%>

													
						<li id="ft"> 
							<a href="#"> 
								<!-- <i>
								   <img src="./image/Wallet-Update.png" style="width:30%;">
								</i> -->  
								<i class="glyphicon glyphicon-folder-close" ></i>
								<span style="">Wallet Update</span>
							</a>
							<ul class="nav nav-pills nav-stacked">  
								<li id="cashDeposit">
									<!-- <a class="ajax-link" href="CashDeposit">Cash Deposit
									</a> -->
									
									<a class="ajax-link" href="CashDepositRequest">Cash Deposit</a>
								</li>
								<%if(user.getOnlineMoney()==1){ %>
								<li id="addMoney">
									<a class="ajax-link" href="AddMoney">Online</a>
								</li>
								<%} %>
								<li id="#">
									<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
								</li>
							</ul>
						</li>
						
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>

						<li id="s">
							<a class="ajax-link" href="GetAgentDtlsByDistId">
								<i class="glyphicon glyphicon-edit" ></i> 
						        <span>Users Dashboard</span>
						    </a>
						</li>


				<li id="agenOnBoard">
				   <a href="#"> <i
						class="glyphicon glyphicon-stats"></i> <span
						style="padding: 8px;">On Boarding</span>
				   </a>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="OnboardByDist">On Board</a></li>


						<li id="da"><a href="DeclinedNewAgentList">Declined List</a>
						</li>


						<li id="alist"><a href="AgentListNew">User List</a></li>

					</ul>
				</li>



				<%-- 
						<%if (user.getUsertype() == 3){ %>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%}else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>
						 --%>	
						 
						<li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	<li id="ags">
									<a href="MovementReportByDist">Cash Movement Report</a>
								</li>
								<li id="ags">
									<a href="CommissionSummary">Commission  Summary</a>
								</li>
								<li id="ags">
									<a href="GetTransacationLedgerDtlBK">Money Transfer</a>
								</li>
								<li id="ags">
									<a href="GetAepsReportByDistAgg">AEPS</a>
								</li>
								
								<li id="ags">
									<a href="GetAepsPayoutReportByDistAgg">Bank Settlement</a>
								</li>
								
								<li id="rech">
									<a href="GetMatmReportByDistAgg">m-ATM Report</a>
								</li>
								
							<!-- 	
								<li id="ags">
									<a href="RechargeReport">Recharge</a>
								</li>
								
								<li id="ags">
									<a href="BBPSReport">BBPS</a>
								</li>  
							-->	
							    <li id="ags">
									<a href="RechargeReportDist">Recharge</a>
								</li>
								
								<li id="ags">
									<a href="BBPSReportDistAggregator">BBPS</a>
								</li>  
								
							</ul>
						</li> 

						<%}else if(user.getUsertype() == 7){
						%> 
						
						<li id="ft"> 
						<a href="#"> 
							<!--  <i>
							   <img src="./image/Wallet-Update.png" style="width:30%;">
							</i>  -->
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%} %>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
							 
							
						</ul>
						</li>
						
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					    </li> 
						
						<li id="mb1">
							<a class="ajax-link " href="GetAgentDtlsBySupDistId">
								<i class="glyphicon glyphicon-book" ></i>
								
						        <span >User Dashboard</span>
						    </a>
						    <!-- 
						    <ul class="nav nav-pills nav-stacked">
								<li id="alist">
									<a href="GetAgentDtlsByDList">Distributer DashBoard</a>
								</li>
								<li id="alist">
									<a href="GetAgentDtlsByRList">Retailer DashBoard</a>
								</li>
							 	
							</ul>
						     -->
						    
						</li>
						
						
						<li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								 
								<li id="alist">
									<a href="OnboardBySuperDist">On Board</a>
							    </li>
								
								<li id="da">
									<a href="DeclinedNewAgentList">Declined List</a>
								</li>
								
								
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
						     </ul>
						 </li>
						 
						
						
						
<%-- 						<%if(aggRoles.contains("9005")&& user.getUsertype() == 7){  %> 

				        <li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<!-- 
								<li id="aob">
									<a href="AgentOnBoard">Agent On Board</a>
								</li>
								 -->
								<li id="alist">
									<a href="OnboardBySuperDist">On Board</a>
							    </li>
								
								<li id="da">
									<a href="DeclinedNewAgentList">Declined List</a>
								</li>
								
								
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
						     </ul>
						 </li>
                         <%} %>     
 --%>

						<li id="s">
						<%-- 
							<%if("OAGG001058".equalsIgnoreCase(user.getAggreatorid())){%>
							<a class="ajax-link" href="DistributorOnBoard">
							<%}else{ %>
							<a class="ajax-link" href="DistributorOnBoard">
							<%} %>
						 --%>	
							
							<%-- 
								<i class="glyphicon glyphicon-user" ></i>
						        <span >Distributor On Board</span>
						     --%>
						    </a>
						</li>
							
							 
						
					<%-- 		
						<%if (user.getUsertype() == 3){%>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%}else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>
					 --%>
					
							
						
						<li id="mb1">
							<a class="ajax-link " href="CashDepositsByDist">
								<i class="glyphicon glyphicon-book"></i>
								
						        <span >Cash Deposit Request</span>
						    </a>
						</li>
						 
						
						<li id="agsr">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						    	<li id="ags">
									<a href="MovementReportByDist">Cash Movement Report</a>
								</li>
								<li id="ags">
									<a href="CommissionSummary">Commission  Summary</a>
								</li>
								<li id="ags">
									<a href="GetDmtReportBySuper">DMT</a>
								</li>
								<li id="ags">
									<a href="GetAepsReportBySuper">AEPS</a>
								</li>
								<li id="ags">
									<a href="#">m-ATM</a>
								</li> 
								<li id="ags">
									<a href="#">Recharge</a>
								</li>  
							</ul>
						</li>

						
						<%
						}else if (user.getUsertype() == 4 && !"OAGG001061".equalsIgnoreCase(user.getId())) { %>

						<%if(domainName.equalsIgnoreCase("pookcity.partner.bhartipay.com")){ %>
											
						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
					            <span style="">Reports</span>
					        </a>
							<ul class="nav nav-pills nav-stacked">		
								<li id="recha">
									<a href="RechargeReportAggreator">Recharges and bill payments</a>
								</li>
								<li id="re">
									<a href="BBPSReportAggreator">BBPS</a>
								</li>
								<li id="rech">
								<li id="rech">
									<a href="CashDepositsRpt">Cash Deposits</a>
								</li>
							</ul>
					    </li> 	
							
						<%}else{ %>
							
						<li id="s">
							<a class="ajax-link" href="AssignUserRole">
								<i class="glyphicon glyphicon-edit" ></i>
								
						        <span >User Role Mapping</span>
						    </a>
						</li> 

						<%if(whiteLevel != true) {%>
						
						
							
							
						<!-- <li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
							    <span >Merchant</span>
							</a>
						    <ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="AddMerchant">Add Merchant</a>
								</li>
								 <li id="cu">
								 	<a href="TPMerchantList">Merchant List</a>
								 </li> 
							</ul>
						</li>-->
						
						<%}%>
						
						
						<%if (user.getUsertype() == 3){%>
						<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Self
								</a>
							</li>
							
							<li id="addMoney">
								<a class="ajax-link" href="ShowPassbookForDist">Agent</a>
							</li> 
							
						</ul>
					</li>
					<%} else if((user.getUsertype() == 4 || user.getUsertype() == 6 ) && whiteLevel){%>
						
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					    <ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="ShowPassbook">Revenue Wallet
								</a>
							</li>
							
							<li id="rech">
									<a href="showCashDeposite">Deposit Wallet</a>
							</li>
							
						</ul>
					</li>
						
						
						<% }else{ %>
					<li id="mb1">
						<a class="ajax-link " href="ShowPassbook">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Passbook</span>
					    </a>
					   
					</li> 
					<%} %>

						<%}
						
						
						}
					if (user.getUsertype() == 4 && "OAGG001061".equalsIgnoreCase(user.getId())) {
							%>
						
						<li id="ft"> 
						<a href="#"> 
							<i class="glyphicon glyphicon-folder-close" ></i>
							<span style="">Wallet Update</span>
						</a>
						<ul class="nav nav-pills nav-stacked">  
							<li id="cashDeposit">
								<a class="ajax-link" href="CashDepositRequest">Cash Deposit
								</a>
							</li>
							<%if(user.getOnlineMoney()==1){ %>
							<li id="addMoney">
								<a class="ajax-link" href="AddMoney">Online</a>
							</li>
							<%} %>
							<li id="#">
								<a class="ajax-link" href="CashDeposit">Wallet Update History</a>
							</li>
						</ul>
					</li>

						<li id="mb1">
							<a class="ajax-link " href="ShowPassbook">
								<i class="glyphicon glyphicon-book" ></i> 
						        <span style=" ">Passbook</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">  
								<li id="cashDeposit">
									<a class="ajax-link" href="ShowPassbook">Revenue Wallet
									</a>
								</li>
								
								<li id="rech">
										<a href="showCashDeposite">Deposit Wallet</a>
								</li>
								
							</ul>
						</li>

				<li id="mb1"><a class="ajax-link "
					href="GetTransacationLedgerPayoutForAgg"> <i
						class="glyphicon glyphicon-book"></i> <span style="">Payout
							Report</span>
				</a></li>
				
				
				<li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<li id="aob">
									<a href="AgentOnBoard">On Board</a>
								</li>
								<li id="da">
									<a href="DeclinedAgent">Declined List</a>
								</li>
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
							</ul>
						</li> 
						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >AOB Checker</span>
						    </a>
					        <ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="NewAgents">Pending</a>
								</li>
								 <li id="cu">
								 	<a href="GetRejectAgentList">Reject</a>
								 </li>
								 <li id="cu">
								 	<a href="GetApprovedAgentList">Approved</a>
								 </li>
								 
								
								
							</ul>
						</li>	
							
						<% 
						}
						%> 

						<%

                        if ((user.getUsertype() == 6 ||user.getUsertype() == 4 ||user.getUsertype() == 3) &&  !"OAGG001061".equalsIgnoreCase(user.getId())) { 

						if((user.getUsertype() == 4) && (domainName.toLowerCase().indexOf("partner.bhartipay.com")>-1 ||domainName.toLowerCase().indexOf("transpaytech.co.in")>-1 ||domainName.toLowerCase().indexOf("localhost:8080")>-1)) {
						%>
						<%-- <li id="ac">
							<a class="ajax-link " href="AssignCommission">
								<i class="glyphicon glyphicon-book" ></i>
								
								<span >Assign
								Commission</span>
							</a>
						</li>  --%>

						<%
						}
		                if((aggRoles.contains("9013") || aggRoles.contains("9003")) && user.getUsertype() != 3){  
						if(aggRoles.contains("9003")){%>
						<li id="u">
						<a class="ajax-link" href="StartCustomerCare">
							<i class="glyphicon glyphicon-edit"></i>
							
					        <span style="padding: 7px;">Customer Support</span>
					    </a>
					   </li>
					   <%}%>

						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-list-alt" ></i>
								
						        <span style="">Report</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<%if(aggRoles.contains("9010")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
								<li id="rech">
									<a href="GetAgentBalDetail">Agent Wallet Balance</a>
								</li>
								<li id="rech">
									<a href="ShowPassbookById">Agent Wallet Summary</a>
								</li>
									
								<%}if(aggRoles.contains("9003")){ %>
									
								<li id="rech">
									<a href="GetRevenue">Revenue Report</a>
								</li>
								<li id="rech">
									<a href="GetDmtDetailsReport">Money Transfer</a>
								</li>
								<li id="rech">
									<a href="GetAepsReportByDistAgg">AEPS Report</a>
								</li>
								
								<li id="rech">
									<a href="GetMatmReportByDistAgg">m-ATM Report</a>
								</li>
								<li id="recha">
									<a href="RechargeReportAggreator">Recharges and bill payments</a>
								</li>
								
								<li id="re">
									<a href="BBPSReportAggreator">BBPS</a>
								</li>
								
								<li id="recha">
									<a href="PGReportAggreator">PG Transactions Report</a>
								</li>
								
								
							
								
									
							
								<!-- <li id="rech">
									<a href="CashDepositsRpt">Cash Deposits Report</a>
								</li> -->
								<%} %>
							</ul>
						</li> 	
							
						 <%}if(aggRoles.contains("9019")){ %>
						
					<%-- 		
					<li id="mb1">
						<a class="ajax-link " href="GetTransacationLedgerPayoutForAgg">
							<i class="glyphicon glyphicon-book" ></i> 
					        <span style=" ">Payout Report</span>
					    </a>
					   
					</li> 
						 --%>		
							

						<%} if(aggRoles.contains("9004")){  %>

<!--  CME Option Remove -->

<%-- 	
						 <li >
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span style="padding: 8px;">CME</span>
						    </a>
						    <ul class="nav nav-pills nav-stacked">
						        <li id="cu">
						        	<a href="CMERequest">CME Request</a>
						        </li>
						 
								<li id="cu">
									<a href="CMETransactionHistory">View CME Requests</a>
								</li>
								<li id="cu">
									<a href="CMEAcountStatement">CME Account Statement</a>
								</li>
								<li id="cu">
									<a href="CMETOAgentTransfer">CME TO Agent Transfer</a>
								</li>
								<li id="cu">
									<a href="DMTBalance">DMT Balance</a>
								</li>

							</ul>
						</li> 
						
 --%>
					<%-- 	<%} if(aggRoles.contains("9005")&& user.getUsertype() != 7){  %>  --%>
                    <%} if(aggRoles.contains("9005")&& (user.getUsertype() ==6 || user.getUsertype() ==4) ){  %> 

				        <li id="agenOnBoard">
				        	<a href="#"> 
				        		<i class="glyphicon glyphicon-stats" ></i>
				        		
							    <span style="padding: 8px;">On Boarding</span>
							</a>
							<ul class="nav nav-pills nav-stacked">
								<!-- 
								<li id="aob">
									<a href="AgentOnBoard">Agent On Board</a>
								</li>
								 -->
								
								
								<%if(user.getUsertype()==6) {%>
								<li id="alist">
									<a href="OnboardByBpmu">On Board</a>  
								</li>
								<%} else if(user.getUsertype()==4) {%>
								<li id="alist">
									<a href="OnboardByBpmu">On Board</a>  
								</li>
								<%}else { %>
								<li>
								<a href="OnboardByDist">On Board</a>
								</li>
								<%} %>
								
								<li id="da">
									<a href="DeclinedNewAgentList">Declined List</a>
								</li>
								
								
								<li id="alist">
									<a href="AgentListNew">User List</a>
								</li>
								
							</ul>
						</li>  

						<%}if(aggRoles.contains("9007") && user.getUsertype() != 3&& user.getUsertype() != 7){  %> 

						<li >
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >Support Tickets</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<li id="cu">
									<a href="SupportTickets">Open Tickets</a>
								</li>
								<li id="cu">
									<a href="SupportTicketList">Ticket List</a>
								</li> 
							</ul>
						</li> 

						
						<%}if(aggRoles.contains("9008")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
 
							
							
						<li id="checker">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >Cash Deposits</span>
						    </a>
							<ul class="nav nav-pills nav-stacked">
								<li id="cashD">
									<a href="CashDeposits">Checker</a>
								</li> 
								<%if(aggRoles.contains("9014")) {%>
								<li id="cashA">
									<a href="CashDepositApproveList">Approver</a>
								</li> 
								<%}%>
								<!-- <li id="sepan">
									<a href="GetSenderPanF60List">Sender PAN List</a>
								</li>-->
								
								<!-- <li id="sepan">
									<a href="GetSenderKycList">Sender KYC List</a>
								</li>-->
							</ul>
						</li> 


						<%}if(aggRoles.contains("9009")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>

						<li id="u">
							<a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >AOB Checker</span>
						    </a>
					        <ul class="nav nav-pills nav-stacked">
								<!-- <li id="cu">
									<a href="NewAgents">Pending</a>
								</li> -->
								 <li id="cu">
								 	<a href="GetRejectAgentList">Reject</a>
								 </li>
								 <li id="cu">
								 	<a href="GetApprovedAgentList">Approved</a>
								 </li>
								 
								  <li id="cu">
									<a href="NewBpayAgentPending">Pending</a>
								</li>
								 
							</ul>
						</li>


						<%}if(aggRoles.contains("9010")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>

                        <%  if(user.getUsertype() == 4){%>
				<%-- 		<li id="s" >
							<a class="ajax-link" href="CreateUser">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >Create User</span>
						    </a>
						</li>
				 --%>		
						<li id="s" >
							<a class="ajax-link" href="CreateNewMember">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >Create User</span>
						    </a>
						</li>
						
						<%} %>
						<!-- <li id="s">
							<a class="ajax-link" href="SearchUser">
								<i class="glyphicon glyphicon-search" ></i>
							
						        <span >Search User</span>
						    </a>
						</li>-->
						
					    <li id="#">
								<a href="#"> 
									<i class="glyphicon glyphicon-stats"></i>
									
							        <span >Utilities</span>
							    </a>
								<ul class="nav nav-pills nav-stacked">
									<!-- <li id="#">
										<a href="utilUserDetails">User Details</a>
									</li>  -->
									<li id="#">
										<a href="UtilProfileChange">Contact Info</a>
									</li>
									<li id="#">
										<a href="SubAggregatorUtility">Change Dist Mapping</a>
									</li>
									
									<li id="#">
										<a href="ChangeSuperDistMapping">Change Sup Dist Mapping</a>
									</li>
								<!-- 	<li id="#">
										<a href="onboardStatus">Onboard Status</a>
									</li> -->
									<!-- 
									<li id="#">
										<a href="LeanAccountUtility">Lean Account</a>
									</li>
									 -->
									 <li id="#">
										<a href="Lean">Lean</a>
									</li>
								<!-- 	 
									<li id="#">
										<a href="pc">Test</a>
									</li>
									 -->
									<%-- 
									<%if(user.getWhiteLabel()==0) {%>
									<li id="#">
										<a href="Pricing">Pricing</a>
									</li>
									<%} %>
									 --%>
									<!-- 
									<li id="#">
										<a href="DmtPricing">DMT Pricing</a>
									</li>
									 -->
								 
									<%--  
									<%if("BPMU001000".equalsIgnoreCase(user.getSubAggregatorId())) {%>
									<li id="#">
										<a href="MemberList">Member List</a>
									</li>
									<%} %>
									  --%>
									  
									<%--  <%if(user.getWhiteLabel()==0) {%> --%>
									<li id="#">
										<a href="PricingDetails">Pricing Details</a>
									</li>
									<%-- <%} %> --%>
									 
									 <li id="#">
										<a href="AddAccount">Add Account</a>
									</li>
									
									 <li id="#">
										<a href="UpdateAccount">Update Account</a>
									 </li>
									 
									<%if(user.getWhiteLabel()==0) {%>
									<li id="#">
										<a href="checkLeanReport">Lean Report</a>
									</li>
									<%} %>
									<li id="#">
										<a href="PGTransaction">PG Txn</a>
									</li>
									<%if(whiteLevel != true) {%>
									<li id="#">
										<a href="AepsSettlement">AEPS Settlement</a>
									</li>
									<!-- <li id="#">
										<a href="allServicesStatus">Service Master</a>
									</li> -->
									
									<li id="#">
										<a href="ServiceControl">Service</a>
									</li>
									
									<% if("BPMU001000".equalsIgnoreCase(user.getSubAggregatorId())) { %>
									 
									<li id="#">
										<a href="PendingRechargeTxn">Recharge Pending</a>
									</li>
									
									<li id="#">
										<a href="PendingDmtTxn">DMT Pending</a>
									</li>
									
									<li id="#">
										<a href="PGTransaction">PG Agg. Settlement</a>
									</li>
									
									<li id="#">
										<a href="GetUserCount">Get User Count</a>
									</li>
									
									<li id="#">
										<a href="GetUserManagement">User Mapping</a>
									</li>
									<!-- 
									<li id="#">
										<a href="ApproveAccountSelf">Approve Account</a>
									</li>
									 -->
									<li id="#">
										<a href="EnablePg">Enable PG&Bank3</a>
									</li>
									<li id="#">
										<a href="GetUserEnable">PG&Bank3 Service</a>
									</li>
									<!-- <li id="#">
										<a href="OnboardByBpmu">New Onboarding</a>
									</li> -->
									<!-- 
									<li id="#">
										<a href="paymentGatewayResponseOkk">PG Response</a>
									</li>
									 -->
									<%} %>
									
								<%}%>
									
									<!-- <li id="#">
										<a href="ReconcileAepsTxn">Reconcile AEPS Txn</a>
									</li> -->
									
									<!-- <li id="#">
										<a href="BpayForm">Bpay Form</a>
									</li> -->
								</ul>
						</li>
						
							


						<%}
						
						if(aggRoles.contains("9018")&& user.getUsertype() != 3&& user.getUsertype() != 7){
							%>
							
						<%-- 	
						<li id="distri">
							<a class="ajax-link" href="#">
								<i class="glyphicon glyphicon-edit" ></i>
								
						        <span >Distributor</span>
						    </a>
						
						    <ul class="nav nav-pills nav-stacked">
							    <li id="s">
							    	<a class="ajax-link" href=" DistributorOnBoardList">
							    		<i class="glyphicon glyphicon-edit" ></i>
							    		
										<span >Distributor On Board List</span>
									</a>
								</li>
								<li id="s">
									<a class="ajax-link" href="DistributorOnBoard">
										<i class="glyphicon glyphicon-edit" ></i>
										
								        <span >Distributor On Board</span>
								    </a>
								</li>
							</ul> 
						</li>
				 --%>		
						
                       <%}if(aggRoles.contains("9011")&& user.getUsertype() != 3&& user.getUsertype() != 7){  %>
				 
						
							<%if(whiteLevel != true){ %>
								
							<li id="u">
							
						    <a href="#"> 
								<i class="glyphicon glyphicon-stats" ></i>
								
						        <span >AOB Approver</span>
						    </a>
						    
						      <ul class="nav nav-pills nav-stacked"> 
								<li id="rech">
									<a href="AgentDetailForApproveNew">AOB Approver</a>
								</li>
								<li id="rech">
									<a href="ApproveAccountSelf">Approve Account</a>
								</li>
						      </ul>
						    </li>
						<%} %>  
						 

						<%
						}if(aggRoles.contains("9013")&& user.getUsertype() != 3){
							if(whiteLevel != true){
						%>
	
						<li id="s">
							<a class="ajax-link" href="RefundDMTTxn">
								<i class="glyphicon glyphicon-user" ></i>
								
						        <span >DMT Refund</span>
						    </a>
						</li>
		
						<%
							}
							}if(aggRoles.contains("9013") && user.getUsertype() != 3&& user.getUsertype() != 7){  %>
	
	                    <li id="u">
	                    	<a href="#"> 
	                    		<i class="glyphicon glyphicon-list-alt" ></i>
	                    		
							    <span style="">Reports</span>
							</a>
							<ul class="nav nav-pills nav-stacked"> 
								<li id="rech">
									<a href="GetDmtDetailsReport">Money Transfer</a>
								</li>
								<li id="rech">
									<a href="showCashDeposite">Wallet Transactions</a>
								</li>
								<li id="rech">
									<a href="CashDepositsRpt">Cash Deposits</a>
								</li>
							    <li id="rech">
									<a href="GetRefundTransactionReport">Refund</a>
								</li>
							    <li id="sepan">
									<a href="getRevokeList">Revoked List</a>
								</li>
							    <li id="recha">
									<a href="GetAgentClosingBalReport">Agent Balance</a>
								</li>	
									<li id="cu">
									<a href="GetWalletTxnDetailsReport">Wallet Transaction Summary</a>
								</li>
								<li id="ags">
									<a href="GetAepsPayoutReportByDistAgg">Bank Settlement</a>
								</li>
								<!-- <li id="rech">
									<a href="showCashDeposite">Wallet Update History</a>
								</li> -->
							</ul>
						</li> 

						<%}%>


                        <%if(!domainName.equalsIgnoreCase("pookcity.partner.bhartipay.com")){  %>
						<%-- <li id="ea">
							<a class="ajax-link" href="ShowProfile">
								<i class="glyphicon glyphicon-edit" ></i>
								
							    <span style="padding: 8px;">Profile</span>
							</a>
						</li> --%>

						<%} %>

					    <%-- <li id="rp">
					    	<a class="ajax-link" href="ResetPassword">
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> Change Password</span>
							</a>
						</li> --%>

						<%if(user.getUsertype()==2){ %>	

					   <%--  <li id="rp">
					    	<a class="ajax-link" href="GetFaq"  >
					    		<i class="glyphicon glyphicon-lock" ></i>
					    		
							    <span style="padding: 8px;"> FAQ</span>
							</a>
						</li> --%>
					
						<!-- <li id="rp">
							<a class="ajax-link" href="GetGrievancePolicy" target="_blank" >
								<i class="glyphicon glyphicon-lock"  ></i>
								<span   class="menuIconcolomn"></span>
	                            <span style="padding: 8px;"> Grievance Policy </span>
	                        </a>
	                    </li> -->

						       <%} %> 
						<%} %> 
					</ul>  
		</nav> 

		<span class="minifyme" data-action="minifyMenu"> 
			<i class="fa fa-arrow-circle-left hit"></i> 
		</span>
    </aside>




	<div class="col-sm-3 col-lg-3 leftcol">
		<div class="sidebar-nav">
			<div class="nav-canvas">
				<div class="nav-sm nav nav-stacked"></div>

			</div>
		</div>
	</div>

	<!-- <script src="./js/Newtheme-bootstrap.min.js"></script>  -->
	<%-- <script src="../js/Newtheme-Mainbhartipay.js"></script> --%>
	<script src="./js/Newtheme-app.config.js"></script>
	<script src="./js/Newtheme-jquery.ui.touch-punch.min.js"></script>
	<!-- <script src="./js/Newtheme-bootstrap.min.js"></script> -->
	<script src="./js/Newtheme-jarvis.widget.min.js"></script>
	<script src="./js/Newtheme-app.min.js"></script>
	<%-- <script src="./js/Newtheme-fontowesome.js"></script> --%>
		
	<!-- <script src="./js/Newtheme-bhartipay-for-toggle.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
	var result = '';
	$.ajax({
		 method:'Post',
		 cache:0,
		 url:'menuBankList',
		 success:function(response){
			var jResponse = JSON.parse(response);
			$.each(jResponse, function(index, value) {
				if(index.toUpperCase() == "STATUSMSG"){
					$.each(value, function(key, obj) {
						result += '<li><a href="CashDepositRequest?depositBank='+key+'">'+obj+'</a></li>';
					});
				}
			});
			$("#ulBankList").empty().append(result);
		}
	 });
});

</script>

</html>
