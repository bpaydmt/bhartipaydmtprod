<%@page import="com.bhartipay.wallet.report.bean.TxnReportByAdmin"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.HashMap" %>
    <%@ page import="java.util.Map" %>
    
    <%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src='./js/highcharts.js'></script>


<style>

</style>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
        
      function createGraph(id,date,credit,debit){
    	
        var cr = parseInt(credit)
        var db = parseInt(debit)
    Highcharts.setOptions({ colors: ['#19aede', '#87c440']});
    $("#" + id).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Transaction Report'
        },
         subtitle: {
            text: id
        },       
        xAxis: {
            categories: [
                date,
                //obj.txndate,
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },

        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Credit Amount',
            data: [cr]

        }, {
            name: 'Debit Amount',
            data: [db]

        }]
    });
        }
    </script>
    
  
</head>
<% 
User user = (User) session.getAttribute("User");
List<TxnReportByAdmin> list=(List<TxnReportByAdmin>)request.getAttribute("txnList");

%>

<body>


			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<%
       // User user = (User) session.getAttribute("User");
        if(user.getUsertype()==99){
        %>
			<script src='./js/highcharts.js'></script>
			

	<div id="main" role="main" style="margin-top: 121px !important;"> 
	<div id="content">  
	        <div class="box col-md-12"> 

				

							<div class="box-header">
								<h2>
									<i class="glyphicon "></i>Dashboard
								</h2> 
							</div>
							<div class="box-content row">
								<div class="col-lg-12 col-md-12">
									<div class="col-lg-12 col-md-12"></div>
									<!--  <div id="container" style="min-width: 310px; height: 400px; padding: 0 0 45px 0;margin: 0 auto"></div> -->

								</div>



							</div>




						<form>


							<%
	for(TxnReportByAdmin wtb:list){
	%>
							<div id="<%=wtb.getAggreatorid()%>"
								style="min-width: 310px; min-height: 400px; padding: 0 0 45px 0; margin: 0 auto"></div>
							<script>
  $(document).ready(function(){
	createGraph("<%=wtb.getAggreatorid()%>","<%=wtb.getTxndate()%>","<%=wtb.getTxncredit()%>","<%=wtb.getTxndebit()%>"); 
  });
  	</script>
							<%
                  }
				%>





						</form>

				






			<%
        }else{
        %>
			<jsp:include page="dashBoard.jsp"></jsp:include>
			<%} %>
			<!-- dashboard ends -->



		</div>
		
	 
		     <div class="col-md-4" style="margin-top: 40px;">
                  <s:select list="subAggregatorList" listKey="key" onchange="changeCust()" id="selectAggregator" listValue="value"  headerValue="Select Agent" headerKey="-1" />
                  
                                <span id="distChanged" style="display:none">Distributor Changed Successfully.		</span>     
		     </div>
		    
	         <div class="col-md-12" style="margin-top: 20px;">
	             <div class="col-md-4">
	              <h4 style="font-weight: bold;">Agent Details</h4>
	                  <div class="twodibba">
	                      <p>Id :- <span id="customerId"></span></p>
	                      <p>Name :- <span id="customername"> </span></p>
	                      <p>Mobile Number :- <span id="customermobileNo"></span></p> 
	                  </div>
	                  
	                  
	             </div>
	             <div class="col-md-2">
	                <div style="text-align:center;padding-top: 34px;">
	                  <img src="./image/download1.png">
	                </div>
	             </div>
	             <div class="col-md-4">
	             <h4 style="font-weight: bold;">Distributor Details</h4>
	                  <div class="twodibba">
	                      <p>Id :- <span id="distributerId"></span></p>
	                      <p>Name :- <span id="distributername"> </span></p>
	                      <p>Mobile Number :- <span id="distributermobileNo"></span></p> 
	                  </div>
	             </div>
	         </div>
		
		
	       <!-- <div class="col-md-4"></div>
	       <div class="col-md-2"></div> -->
	       <div class="row">
	       <div class="col-md-6"></div>
		   <div class="col-md-6" style="margin-top: -20px ;display:none;" id="divDist" >
		      <s:select list="distributorList" style="width:100% !important;" listKey="key" id="selectDistributor" listValue="value"  headerValue="Select Distributor" headerKey="-1" />
		   	<input type="button" onclick="changeDist()" class="btn btn-info" style="margin-left: 5px;margin-top: 15px;"  value="change" />
		   </div> 
		   </div> 
        

  
	   
       
		
          
        </div>
    </div>
</div><!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<script src="./js/subAggUtil.js"></script>
	<!-- application script for Charisma demo -->
	


</body>
</html>
