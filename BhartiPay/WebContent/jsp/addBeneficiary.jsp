              
                <%@page import="java.util.Iterator"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TAG0"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Beneficiary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CardDetail"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.UserDetails"%>
<%@taglib prefix="s" uri="/struts-tags"%>

   
 <jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard" class="wll">Home</a>
        </li>
        <li>
            <a href="Search" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->
<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Add Beneficiary</h2>

                
            </div>
								<div class="box-content row">
								<font color="red"><s:actionerror/> </font>
								<font color="blue"><s:actionmessage/> </font>
								
								
								 <form action="AddBeneficiaryRequest" id="addBenficiaryRequest" method="post" name="PG">
							
									<div class="row">
									<font color="red"> </font>
									<font color="blue"> </font>
          					 <div class="col-md-12">
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Mobile</label>
															 <input readonly="readonly" class="form-control mandatory" name="dmtInputBean.mobile" type="text" id="addBenMobile" maxlength="10" placeholder="Please type mobile number here" value="<s:property value='%{dmtInputBean.mobile}'/>" >
															 <span class="errorMsg" id="addBenMobileErr"></span>

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label >Beneficiary Name <font color="red"> *</font></label>
															 <input class="form-control mandatory" name="dmtInputBean.benName" type="text" id="BeneFicName" maxlength="50" placeholder="Beneficiary Name" value="<s:property value='%{dmtInputBean.benName}'/>">
															 <span class="errorMsg " id="BeneFicNameErr"></span>

														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Account Number<font color="red"> *</font></label>
															 <input class="form-control mandatory" name="dmtInputBean.benAccount" type="text" id="benAccountNum" maxlength="20" placeholder="Account Number" value="<s:property value='%{dmtInputBean.benAccount}'/>" >
															 <span class="errorMsg" id="benAccountNumErr"></span>

														</div>
													</div>
											
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Account Type</label>
															 <s:select name="dmtInputBean.accountType" list="%{accountTypeList}">
												
															 </s:select>
															 <span class="errorMsg" id=""></span>

														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">IFSC<font color="red"> *</font></label>
															 <input class="form-control mandatory" name="dmtInputBean.benIFSC" type="text" id="ifscInp" maxlength="11" placeholder="Please type IFSC code here" value="<s:property value='%{dmtInputBean.benIFSC}'/>" >
															 <span class="errorMsg" id="ifscInpErr"></span>

														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
													<button type="button" id="submitBeneficiary" onclick="submitVaForm('#addBenficiaryRequest')" class="btn form-btn btn-info btn-fill" >Submit
										</button>
													</div>
												



	


									
									</div>
									
									
									

									
									</div>
									
									</form>

              
                
                
                
                
                
                
  
								
								
								</div>



							</div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


                