<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtUserDetails"%>
<%

DmtUserDetails userDetails=(DmtUserDetails)request.getAttribute("userDetail");
if(userDetails!=null&&userDetails.getCardName()!=null&&userDetails.getCardCodeName()!=null){
%>
<table class="table table-bordered table-striped">
<tr>
<td><strong>Request Id</strong></td><td><%=userDetails.getReqId()%></td>
</tr>
<tr>
<td><strong>Card Name</strong></td><td><%=userDetails.getCardName()%></td>
</tr>
<tr>
<td><strong>Card Code Name</strong></td><td><%=userDetails.getCardCodeName()%></td>
</tr>
<tr>
<td><strong>Card Holder Name</strong></td><td><%=userDetails.getHoldaerName()%></td>
</tr>
<tr>
<td><strong>Card Number</strong></td><td><%=userDetails.getCardNumber()%></td>
</tr>
<tr>
<td><strong>Expiry</strong></td><td><%=userDetails.getExpiry()%></td>
</tr>
<tr>
<td><strong>Description</strong></td><td><%=userDetails.getDescription()%></td>
</tr>
</table>
<%
}else{
%>
Detail not found.
<%}%>