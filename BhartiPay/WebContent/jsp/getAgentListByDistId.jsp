<%@page import="com.bhartipay.wallet.report.bean.AgentDetailsBean"%> 
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.lean.DistributorIdAllot"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
    $(document).ready(function() { 
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
	            extend: 'copy',
	            text: 'COPY',
	            title:'Agent Details - ' + '<%= currentDate %>',
	            message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Agent Details - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Agent Details - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Agent Details - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Agent Details - ' + '<%= currentDate %>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
        ]
    } );
   

	} );


	function setMonth(month){
	 var m = parseInt(month)
	 if(month == 11){
	  return 0;
	 }else{
	  console.log(month)
	  return month + 2;
	 }

	 }
	 function setYear(month,year){

	 if(month == 11){
	  return year;
	 }else{
	  console.log(month)
	  return year + 1;
	 }

	 }
	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   
	function revokeAgentAmount(){
		
		
		
		
		 
	 }
	
	function openRevokePopup(id){
		
		$('#revoke-agentId').val(id)
		
	}
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
DecimalFormat d=new DecimalFormat("0.00");



%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
    <div id="main" role="main"> 
	    <div id="content">       
	        <div class="row">     

			    <div class="box2 col-md-12">
				   <div class="box-inner"> 
						<div class="box-header">
							<h2>Manage Users</h2>
						</div> 
						
					<%
					DistributorIdAllot allotId=(DistributorIdAllot)request.getAttribute("allotId");
					 
					
					if(allotId!=null){	 %>
					<div class="col-md-2 col-xs-12" style="padding:10px 0px;"> 
						<strong>Total Count -  <span><%=allotId.getAllToken() %></span></strong>
					</div>
					
					<div class="col-md-4 col-xs-12" style="padding:10px 0px;"> 
						<strong>Used Count -  <span><%=allotId.getConsumeToken() %></span></strong>
					</div>	
						
					<div class="col-md-4 col-xs-12" style="padding:10px 0px;"> 
					<strong>Remaining Count -  <span><%=allotId.getRemainingToken() %></span></strong>
					</div>
					<%} %>
<!-- <div class="box-content">  
	<form action="PassbookSummary" method="post" autocomplete="off">
		<div class="row"> 

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">   
					<label for="apptxt">Search By Mobile No. / Id</label> 
					<input class="form-control" name="#" type="text" id="#"  placeholder="Mobile No. / Id"> 
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12"> 
				<div id="wwctrl_submit">
					<input class="btn btn-success" id="#" type="submit" value="Submit" style="margin-top: 20px;"> 
				</div>
			</div>
		</div>
    </form> 
</div> -->







				    </div>
				</div>	 
                
                <div class="col-md-12">
					<div id="xyz">
						<font style="color:red;">
						    <s:actionerror/>
						</font>
						<font style="color:green;">
						    <s:actionmessage/>
						</font>	
						<table id="example" class="scrollD cell-border dataTable no-footer">
							<thead> 
								<tr>
								    <th><u>User Type</u></th>
									<th><u>User ID</u></th>
									<th><u>Name</u></th>
									<th><u>Shop Name</u></th> 
									<th><u>Mobile Number</u></th> 
									<th><u>Revoke</u></th>
									<th><u>Status</u></th>
									<!-- <th><u>Block / Unblock</u></th> -->
									<th><u>Balance</u></th>
									<th><u>Credit Transfer</u></th>  
								</tr>
							</thead>
							<tbody>
								<%
								List<AgentDetailsBean>list=(List<AgentDetailsBean>)request.getAttribute("agentList");

								if(list!=null){
								
								
								for(int i=0; i<list.size(); i++) {
									AgentDetailsBean tdo=list.get(i);%>
						          		  <tr>
						        <td><% if(tdo.getUsertype()==2){out.print("Retailer");}else if(tdo.getUsertype()==3){out.print("Distributor");}else if(tdo.getUsertype()==7){out.print("SuperDistributor");}else{out.print("");}  %></td>
		          	
						         
					       	   <td><a href="javascript:void(0)" data-toggle="modal" data-target="#newAgentBox" onclick=" getAgentDetails('<%=tdo.getId()%>','<%=user.getAggreatorid()%>')"><%=tdo.getId()%></a></td>
					       	   <td><%=tdo.getName()%></td>
					       	   <td><%=tdo.getShopname()%></td>
					       	
					 		   <td><%=tdo.getMobileno()%></td>
					 		
					 		     <td><button data-toggle="modal" data-target="#removeModal" onclick="openRevokePopup('<%=tdo.getId()%>')">Revoke</button></td>
					 		    <td id="agentLink<%=tdo.getId()%>" ><%if(tdo.getUserstatus().equalsIgnoreCase("A")){ %>
					 		  		 		    <span style="color:green" >Active</span>
					 		    <%}else if(tdo.getUserstatus().equalsIgnoreCase("D")){ %>
					 		  				    <span style="color:blue" >De-Active</span>
					 		    <%}else if(tdo.getUserstatus().equalsIgnoreCase("B")){ %>
					 		     				<span style="color:red" >Blocked</span>
					 		    
					 		   <%} %></td>
					 		    
					 		   <%-- <%if(tdo.getUserstatus().equalsIgnoreCase("B")){ %> 
					 		    <td><button data-toggle="modal" data-target="#agentBlockUnblock" id="agentBtn<%=tdo.getId()%>" onclick="blockUnblockAgent('<%=tdo.getId()%>','<%=user.getId()%>',this)">Un-Block</button></td>
					 		   <%}	   else if(tdo.getUserstatus().equalsIgnoreCase("A")){%>
					 		   <td><button  data-toggle="modal" data-target="#agentBlockUnblock" id="agentBtn<%=tdo.getId()%>"  onclick="blockUnblockAgent('<%=tdo.getId()%>','<%=user.getId()%>',this)">Block</button></td>
					 		   <%}else{ %>
					 		   <td></td>
					 		   <%} %> --%>
					 		   
					 		   
					 		    <td><button onclick=" refreshBalance('<%=tdo.getId()%>','<%=user.getCountrycurrency() %>')"> Balance
					 		    <span style="color:green" id="agent<%=tdo.getId()%>"></span></button></td>
					 		   
					 		   <%if(tdo.getUserstatus().equalsIgnoreCase("A")){ %>
					 		   		 		    <td><button data-toggle="modal" data-target="#agendDetailsPopUp"  onclick="getAgentDetailsCreditTransfer('<%=tdo.getMobileno()%>')">Fund Transfer</button></td>
					            
					            <% }else{%> 
					            <td></td>
					            <%} %>
					          
			                  		  </tr>
						        <%} }%>	
						    </tbody>		
						</table>
					</div>  
                </div>
	        </div> 
		</div>
    </div>

<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg" style="width:900px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>

<div id="agendDetailsPopUp" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Transfer Amount</h4>
      </div>
      <div class="modal-body">
       <div class="row">
       <div class="col-md-4"></div> <div class="col-md-6"><span style="padding: 0 0 10px 0;
float: left;" id="fundTransMsg"></span></div>
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Agent ID</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       			
       				<input type="text" id="agentIdPop" readonly class="form-control" name="cmeBean.agentId" />
       			</div>
       		
       		</div>
      </div>
      
      
       <div class="row">
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Mobile No.</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       				<input type="text" id="agentMobileNoPop" readonly class="form-control" name="cmeBean.mobileNo" />
       			</div>
       		
       		</div>
      </div>
        
    
          <div class="row">
      
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Agent name</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       				<input type="text" id="agentNamePop" readonly class="form-control"  />
       			</div>
       		
       		</div>
      </div>
          <div class="row">
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Agent Balance</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       				<input type="text"  id="agentBalancePop" readonly class="form-control"  />
       			</div>
       		
       		</div>
      </div>
          <div class="row">
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Transfer Amount</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       				<input type="text"  id="transferAmountPop" class="form-control onlyNum" name="cmeBean.amount" maxlength="6" />
       				<span id="transferAmountPopErr" style="color:red"></span>
       			</div>
       		
       		</div>
      </div>
          <div class="row">
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       					<button onclick="submitAgentDetails()" id="fundTransBtn" class="btn btn-infobtn btn-info btn-fill submit-form btn-block" >Submit</button>
       
       			</div>
       		
       		</div>
      </div>
      <!--     <div class="row">
       <div class="col-md-4 text-right" style="line-height: 37px;">
       			<label>Reason</label>
       	
       
       </div>
       		
       		<div class="col-md-6">
       			<div class="form-group">
       				<textarea  class="form-control"  ></textarea>
       			</div>
       		
       		</div>
      </div> 
      
   
      -->
    
    
    </div>

  </div>
</div>
</div>
       <div id="agentBlockUnblock" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
       <span style="color:red" id="remark-err"></span>
        <div class="form-group">
        	  <input type="hidden" id="hidden-agentId" >
        	    <input type="hidden" id="hidden-aggrId" >
        	    <input type="hidden" id="hidden-status" >
        	<input type="text" placeholder="Remark" name="cmeBean.remarksReason" id="remoark-inp" class="form-control" />
        	
        	
        </div>
        <input type="button" value="Submit" onclick="changeAgentStatus()" class="btn btn-info btn-fill btn-block"/>
     
      </div>
     
    </div>

  </div>
</div>

 <div id="removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Revoke</h4>
      </div>
         <form action="requestRevoke" method="post" onsubmit="revokeAgentAmount()">
        <s:hidden name="csrfPreventionSalt"
					value="%{#session.csrfPreventionSalt}"></s:hidden>
      <div class="modal-body">
   
       <span style="color:red" id="revoke-err"></span>
       
         <div class="form-group">
              	    
        	    
        	<input type="text" placeholder="Revoke Amount" name="amt" id="remoark-inp" class="form-control onlyNum" required />
        	
        	
        </div>
        <div class="form-group">
        
        	  <input type="hidden" name="revokeUserId" id="revoke-agentId" >
        	
        	    
        	<input type="text" placeholder="Remark" name="remark" id="remoark-inp" class="form-control" required />
        	
        	
        </div>
       
        <input type="submit" value="Submit" class="btn btn-info btn-fill btn-block"/>
     
      </div>
      </form>
    
     
    </div>

  </div>
</div>

<script>
function submitAgentDetails(){

	if(parseInt($("#transferAmountPop").val()) > 200000){
		
		$("#transferAmountPopErr").text("Amount can't be more the 2,00,000")
	}else{
		 $("#fundTransBtn").prop('disabled',true);
		$("#transferAmountPopErr").text("")
	$.ajax({
		  type: "POST",
		  url: "FundTransferByDist",
		  data: "cmeBean.agentId="+$("#agentIdPop").val()+"&cmeBean.mobileNo="+$("#agentMobileNoPop").val()+"&cmeBean.amount="+$("#transferAmountPop").val(),
		  success: function(result){
			  console.log(result)
			  if(result.status == "FAILED"){
				 $("#fundTransMsg").css("color","red") 
				  
			  }else{
				  $("#fundTransMsg").css("color","green")
			  }
			  
			  $("#transferAmountPop").val('')
			  $("#fundTransMsg").text(result.statusMsg);
			  $("#fundTransBtn").prop('disabled',false);
		  },
		  
		});
	}
	}
	
	
function getAgentDetails(agentId,aggrId){
	
	$.ajax({
		  type: "POST",
		  url: "NewUserdetails",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			 
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}
function changeAgentStatus(){
	
	if($("#remoark-inp").val().length != 0 && parseInt($("#transferAmountPop").val()) > 300000){
	$.ajax({
		type:'POST',
		url:'BlockUnblockUser',
		data:"userBlockedBean.agentId="+$('#hidden-agentId').val()+"&userBlockedBean.blockBy="+$('#hidden-aggrId').val()+"&userBlockedBean.declinedComment="+$("#remoark-inp").val()+"&userBlockedBean.status="+$("#hidden-status").val(),
		success:function(result){
			console.log(result)
			$(".close").click()
			if(result.status != 7000 &&  result.status != 1001){
				
				$("#agentBtn"+ $('#hidden-agentId').val()).text(result.status)
				if(result.status == "Block"){
					$("#agentLink"+ $('#hidden-agentId').val()).text("Active").css('color','Green')
				}else if(result.status == "Un-Block"){
					$("#agentLink"+ $('#hidden-agentId').val()).text("Blocked").css('color','red')
				}
				
			}
			$("#remark-err").text("");
			$("#remoark-inp").val('')
		}
		

			
	})
	
	}else{
		if(parseInt($("#transferAmountPop").val()) < 300000){
			$("#remark-err").text("Amount can't be more than 3,00,000");
		}
		if($("#remoark-inp").val().length == 0){
			$("#remark-err").text("Please write your remark");
		}
		
	}
}

function refreshBalance(agentId,currency){
	$.ajax({
		  type: "POST",
		  url: "GetAgentWalletBalanceById",
		  data: "&walletBean.id="+agentId,
		  success: function(result){
			
			  $("#agent"+agentId).html(currency+" "+ result.amount)
		  },
		  
		});
	
}

function blockUnblockAgent(agentId,aggrID,el){

	$("#hidden-status").val($(el).text());
	$('#hidden-agentId').val(agentId)
	$('#hidden-aggrId').val(aggrID)
	
}
function getAgentDetailsCreditTransfer(agentMobile){
	$("#transferAmountPopErr").text("")
	 $("#transferAmountPop, #agentIdPop, #agentMobileNoPop, #agentNamePop, #agentBalancePop").val('')
	  $("#fundTransMsg").text('')
	
	
		$.ajax({
			method:'post',
			url:'GetAgnDtlByUserId',
			data:'cmeBean.mobileNo='+agentMobile,
			success:function(data){
				//console.log(data)
			$("#agentIdPop").val(data.agentDetail.id)
			$("#agentMobileNoPop").val(agentMobile)
			$("#agentNamePop").val(data.agentDetail.name);
			$("#agentBalancePop").val(data.agentDetail.closingBal)
			 
				
			}
		})
	
}

</script>
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



