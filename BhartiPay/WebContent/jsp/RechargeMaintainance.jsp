<%@taglib prefix="s" uri="/struts-tags"%>


<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>  

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="mainMenu.jsp"></jsp:include>    
    
 
        <!-- left menu ends -->
<div id="main" role="main"> 
    <div id="content">       
        <div class="row">    
            <div class="col-lg-12 col-md-12 col-sm-12">   

                    <div class="box"> 
                        <div class="box-inner">
                            <div class="box-header">
                                <h2><i class="glyphicon "></i>Recharge&Bill</h2> 
                            </div>
                            <div class="box-content">      
    							<div class="row"> 
                 					<div class="col-md-12"> 
										<div class="col-md-8" style="padding-bottom: 8px;">
											<font color="red">
											 Maintenance is in progress. Please try later
											</font>
										</div> 
    								</div>
    							</div>  
                            </div> 
                        </div> 
                    </div>  
  
            </div>
        </div>
    </div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include> 

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


