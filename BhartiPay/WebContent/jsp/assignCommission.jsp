<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>
<script>

$(function(){

			$(document).on('change','.planTxtIdSelect',function(){
				$(this).parent().find('.planTxtIdInp').val($(this).val());
				var txnMode = $(this).parent().prev().find('.txnModeInp').val();
				
				
				var txnModeRevert =  txnMode == "1" ? 0 : 1;
			
				 if($(this).val() == 'NEFT'){
					$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValNEFT'+txnMode).removeClass('rangeValIMPS1 rangeValVERIFY1 rangeValIMPS0 rangeValVERIFY0 rangeValIMPS1 rangeValNEFT'+txnModeRevert)
				 }
				 else if($(this).val() == 'IMPS'){
						$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValIMPS'+txnMode).removeClass('rangeValNEFT1  rangeValVERIFY1 rangeValNEFT0  rangeValVERIFY0 rangeValIMPS'+txnModeRevert )
					 }
					 
				 else{
						$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValVERIFY'+txnMode).removeClass('rangeValIMPS0 rangeValNEFT0 rangeValIMPS1 rangeValNEFT1 rangeValVERIFY'+txnModeRevert)
					 }
					 
				
			});
			
			
			$(document).on('change','.txnModeSelect',function(){
			
				var txnMode = $(this).val();
				var txnModeRevert =  txnMode == "1" ? 0 : 1;
				var txnType = $(this).parent().siblings().find('.planTxtIdSelect').val()
		
				 if(txnType == 'NEFT'){
					$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValNEFT'+txnMode).removeClass('rangeValIMPS1 rangeValVERIFY1 rangeValIMPS0 rangeValVERIFY0 rangeValIMPS1 rangeValNEFT'+txnModeRevert)
				 }
				 else if(txnType == 'IMPS'){
						$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValIMPS'+txnMode).removeClass('rangeValNEFT1  rangeValVERIFY1 rangeValNEFT0  rangeValVERIFY0 rangeValIMPS'+txnModeRevert )
					 }
					 
				 else{
						$(this).parent().siblings().find('.RangeTo, .RangeFrom').addClass('rangeValVERIFY'+txnMode).removeClass('rangeValIMPS0 rangeValNEFT0 rangeValIMPS1 rangeValNEFT1 rangeValVERIFY'+txnModeRevert)
					 }
					 
				
			});
			
			
			$(document).on('change','.CommissionTypeSelect',function(){
				
				$(this).parent().find('.CommissionTypeInp').val($(this).val());
				$(this).parent().siblings().find('input[type="text"]').val('')
				if($(this).val() == "P"  ){
					$('.commisionPercentage, .SuperDistributoInp, .DistributorInp').addClass('percent-vl').removeClass('amount-vl')
				}else{
					$('.commisionPercentage, .SuperDistributoInp, .DistributorInp').addClass('amount-vl').removeClass('percent-vl')
				}
				
			});
			
			$(document).on('change','.txnModeSelect',function(){
				$(this).prev().val($(this).val());
				
			});
			
			
			$(document).on('change','.maxChargesCheckBoxCheck',function(){
				console.log($(this).is("checked"))
				if($(this).is(":checked")){
					
					$(this).parent().find('.maxChargesCheckBox').val("1");
				}else{
					$(this).parent().find('.maxChargesCheckBox').val("0");
				}
				
			});
	       
	$(document).on('click','.EditRow',function(){
		
		var elm =  $(this).parent().siblings();
		
		elm.find('input').prop('readonly',false)
		elm.find('select').prop('disabled',false)
		elm.find('input').prop('disabled',false)
		//$('select').chosen({search_contains: true})
		$('select').trigger("chosen:updated");
		elm.find('span').hide();
		elm.find('.chosen-container').show();
		
	 
	});
	 $(document).on('click','.remove-row',function(){
			//  alert(1)
			  $(this).parent().parent().remove()
			  
			  
			 }); 

	$("#assignCommisionUser").change(function(){
		

		
		if($(this).val() == "SuperDistributor" ){
			
		
			$.get('GetSuperDistributerByAggIdForComm',function(res){
				$("#SuperDistributorList select").html("")
				 $("#SuperDistributorList select").append('<option value="-1">Please Select Super Distributor</option>')
				$.each(res,function(index, item){
					console.log(item)
					console.log(index)
					
					var str = item;
				    var resString = str.lastIndexOf(":");
				   
				    var val = str.slice(resString + 1,str.length);
				    $("#SuperDistributorList select").append("<option value="+val+">"+ val +"</option>")
				})
				
					$('#SuperDistributorList select').trigger("chosen:updated");
				
				$("#SuperDistributorList").show();
				$("#DistributorList, #AgentList").hide();
			});
				

		}else if($(this).val() == "Distributor" ){
			getDistritbutorList('GetDistributerByAggIdForComm')
		}else if($(this).val() == "Agent" ){

			
			$.get('GetAgentByAggIdForComm',function(res){
				console.log(res)
				$("#AgentList select").html("")
				$("#AgentList select").append('<option value="-1">Please Select Agent</option>')
				$.each(res,function(index, item){
					console.log(item)
					console.log(index)
					
					var str = item;
				    var resString = str.lastIndexOf(":");
				   
				    var val = str.slice(resString + 1,str.length);
				    $("#AgentList select").append("<option value="+val+">"+ val +"</option>")
				})
				
					$('#AgentList select').trigger("chosen:updated");
				
				$("#AgentList").show();
				$("#DistributorList, #SuperDistributorList").hide()
				
			})

			
		}else{
			// do Nothing
		}
	})

})
function getDistritbutorList(url){
	$.get(url,function(res){
	
		console.log(res)
		$("#DistributorList select").html("")
		 $("#DistributorList select").append('<option value="-1">Please Select Distributor</option>')
		$.each(res,function(index, item){
			console.log(item)
			console.log(index)
			
			var str = item;
		    var resString = str.lastIndexOf(":");
		   
		    var val = str.slice(resString + 1,str.length);
		    $("#DistributorList select").append("<option value="+val+">"+ val +"</option>")
		})
		
			$('#DistributorList select').trigger("chosen:updated");
		
		$("#DistributorList").show();
		$("#SuperDistributorList, #AgentList").hide()
	})
	
	
	}
function getAgentByDistId()
{  

	$.ajax({
        url:"GetActiveAgentByDistId",
        cache:0,
        data:"reportBean.distId="+$("#distributer").val(),
        success:function(result){
        	console.log(result)
               document.getElementById('agent').innerHTML=result;
        	$("#ageone").show();
               
         }
  });  
	return false;
}
function getSubagentByAgentId(agentId)
{  
	var str = agentId;
	console.log(str)
    var resString = str.lastIndexOf("-");
   console.log(resString)
    var val = str.slice(0,resString);
	console.log(val)
	$.ajax({
        url:"GetCommissionByAgentId",
        cache:0,
        data:"assignCommission.agentId="+val,
        success:function(result){
        	console.log(result)
              document.getElementById('commissionDiv').innerHTML=result;
         }
  });  
	return false;
}
function addCommissionRow(){

	var source   = document.getElementById("commissionTemp").innerHTML;
	var template = Handlebars.compile(source);
	console.log(template)
	$("#commissionTempTable").append(template)
	 $('select').chosen({search_contains: true})
}

function assignCommissionForm(e){


	$('.planTxtIdInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].txntype')
		
	})

	$('.CommissionTypeInp').each(function(index,item){
		$(item).attr('name','cBean['+index+'].fixedorperc')
		
	})
		
	$('.SuperDistributoInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].superDisPer')
		
	})
		
	$('.DistributorInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].disPer')
		
	})
	
		$('.commisionPercentage').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].commperc')
		
	})
		

	
	$('.AgentInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].agentPer')
		
	})
		
	$('.RangeFrom').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].rangeFrom')
		
	})
	
		$('.txnModeInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].bankrefid')
		
	})
		$('.RangeTo').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].rangeTo')
		
	})
		$('.MaxCharges').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].maxCharges')
		
	})
		$('.maxChargesCheckBox').each(function(index,item){
		
		
		$(item).attr('name','cBean['+index+'].maxChargesCheck')
		
	})
		$('.fixedChargesInp').each(function(index,item){
		
		$(item).attr('name','cBean['+index+'].fixedCharges')
		
	})
		

	$("#commissionDiv .mustBeFilled").each(function(index, item){
		
		if($(this).val().length == 0)
		{
		    $(this).attr("style","border:1px solid red")
	        $(this).addClass('inValid')
	        }else{
	         $(this).attr("style","")
	         $(this).removeClass('inValid')
	         	comparePrevVal('.rangeValIMPS1')
				comparePrevVal('.rangeValNEFT1')
				comparePrevVal('.rangeValVERIFY1')
		}	comparePrevVal('.rangeValIMPS0')
			comparePrevVal('.rangeValNEFT0')
			comparePrevVal('.rangeValVERIFY0')
	});

	
	if($(".inValid").length != 0  ){
		
		e.preventDefault();
	}
	
	var confirmMsg =  confirm("Are you sure you want assign these commision plans?");
	if(!confirm){
		
		e.preventDefault();
	}
	
	
	
	
	
}

function comparePrevVal(compareVal){
	var prevVal = parseFloat(-2.0);

	$(compareVal).each(function(index,item){
		
	     var val = parseFloat($(this).val())
	    
	     
	        if(val < prevVal){
	          
	        $(this).attr("style","border:1px solid red")
	        $(this).addClass('inValid')
	        }else{
	         $(this).attr("style","")
	         $(this).removeClass('inValid')
	        }
	     prevVal =  val
	    });
	

	
}

function changeMoneyPrefix(val){

	if(val == "Fixed"){
		$('.typePrefix').text("Rs.").show();
		
	}else{
		$('.typePrefix').text("").hide();
	}
}
</script>

<body>
<script id="commissionTemp" type="text/x-handlebars-template">


<tr>
    <td>
 <input type="hidden" class="txnModeInp" value="1"  />

       <select class="txnModeSelect">
       <option value="1">Bank</option>
       <option value="0">Wallet</option>
         
       
       </select>
       
       </td>
       <td>
 <input type="hidden" class="planTxtIdInp" value="NEFT"  />

       <select class="planTxtIdSelect">
       <option value="NEFT">NEFT</option>
       <option value="IMPS">IMPS</option>
          <option value="VERIFY">VERIFY</option>
       
       
       </select>
       
       </td>
        <td>
<input type="hidden" class="CommissionTypeInp" value="P">
        <select class="CommissionTypeSelect">
         <option value="P">Percentage</option>
          <option value="F">Fixed</option>
                          
        </select>
        </td>
   <td>
        <input type="text"  class="form-control commisionPercentage mustBeFilled percent-vl "  value="" name="" />
        </td>
        <td><input type="text" class="form-control SuperDistributoInp mustBeFilled percent-vl" name="cBean.superDisPer"></td>
               <td><input type="text" class="form-control DistributorInp mustBeFilled percent-vl" name="cBean.disPer"></td>
           
               <td><input type="text" class="form-control amount-vl RangeFrom zero-allowed rangeVal rangeValNEFT1 mustBeFilled" name="cBean.rangeFrom"></td>
                 <td><input type="text" class="form-control amount-vl RangeTo zero-allowed rangeVal rangeValNEFT1 mustBeFilled" name="cBean.rangeTo"></td>
 <td><input type="text" class="form-control amount-vl fixedChargesInp mustBeFilled" value="" /></td>
                 <td>
<input type="hidden"  class="maxChargesCheckBox"  value="0"/>
                <input type="checkbox" style="margin: 7px 15px;" class="maxChargesCheckBoxCheck " value="0" /> <input type="text" class="form-control MaxCharges mustBeFilled" name="cBean.maxCharges"></td>
 <td><input type="button" value="Remove" class="btn-grid remove-row"></td>

               
      </tr>  


</script>
    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-12 col-sm-12 "  style="margin-top: 70px;">
            <!-- content hellostarts -->
            



<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" style="padding: 4px 10px 5px 10px;">
                <h2><i class="glyphicon "></i>Assign Commission</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<!-- <form action="PaymentGateway" id="form_name" method="post" name="PG"> -->

	<form action="SaveDefineCommission" method="post" onsubmit="assignCommissionForm(event)">

<%
String status=(String)request.getAttribute("status");
if(status !=  null &&  status.equalsIgnoreCase("Success")){
	%>
		<div class="alert alert-success" style="display:block">
 <s:actionmessage/>
</div>
	<%
	
}

if(status !=  null &&  status.equalsIgnoreCase("fail")){
	%>
<div class="alert alert-danger" style="display:block">
  <s:actionerror/>
</div>

	<%
	
}

%>

			<%
User user = (User) session.getAttribute("User");
%>
	<div class="col-md-4">
	<label>Assign Commission To  </label>
	<select id="assignCommisionUser"> 
		<option value="-1">Please Select User type </option>
		<% if(user!=null&&user.getUsertype()==4){%>
		<option value="SuperDistributor">Super Distributor </option>
		<%}if(user!=null&&user.getUsertype()==7||user.getUsertype()==4){%>
		<option value="Distributor">Distributor </option>
		<%}if(user!=null&&user.getUsertype()==3||user.getUsertype()==4||user.getUsertype()==7){  %>
			<option value="Agent">Agent </option>
		<%} %>
	</select>
	</div>
	<div style=" width: 100%;
    float: left;
    margin: 20px 0;">
    	<div class="col-md-4" style="display:none" id="SuperDistributorList">
		<label>Super Distributor </label>
	<select onchange="getSubagentByAgentId(this.value)">
	</select>
	</div>
	<div class="col-md-4" style="display:none" id="DistributorList">
		<label>Distributor </label>
<select onchange="getSubagentByAgentId(this.value)" name="agentId"></select>
	</div>
	
		<div class="col-sm-4"  id="AgentList" style="display: none;">
									<label for="email">Agent </label> 
											<select name="agentId" onchange="getSubagentByAgentId(this.value)"></select>
	</div>
	<%-- <script>
	$(function(){
		getDistritbutorList('GetDistributerBySuperDistributerForComm')	
	})
	</script> --%>
	
<%-- 	<div class="col-md-4" style="display:none" id="DistributorList">
		<label>Distributor </label>
<select onchange="getSubagentByAgentId(this.value)" ></select>
	</div> --%>
	
								</div>
	
	

	<script>
	$(function(){
		
		$(function(){
			
			$.get('GetAgentByDistIdForComm',function(res){
				
				console.log(res)
				$("#ageone select").html("")
				 $("#ageone select").append('<option value="-1">Please Select Agent</option>')
				$.each(res,function(index, item){
					console.log(item)
					console.log(index)
					
					var str = item;
				    var resString = str.lastIndexOf(":");
				   
				    var val = str.slice(resString + 1,str.length);
				    $("#ageone select").append("<option value="+val+">"+ val +"</option>")
				})
				
					$('#ageone select').trigger("chosen:updated");
				
				//$("#DistributorList").show();
				//$("#SuperDistributorList").hide()
			})
		
		//	getDistritbutorList('GetDistributerBySuperDistributerForComm')	
		})
	
		
	})
	
	</script>
<div id="commissionDiv" style="margin: 0px;width: 100% !important; ">
</div>		
</form>
                </div>
            </div>
        </div>
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


