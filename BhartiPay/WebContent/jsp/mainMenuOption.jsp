<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%
User user = (User) session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
DecimalFormat d = new DecimalFormat("0.00");
%>

<!-- MAIN MENU -->
<div class="main-menu-wrap">
	<div class="menu-bar">
		<nav>
			<ul class="main-menu">
			
				<!-- MENU ITEM -->
				<li class="menu-item"><a href="Home">Home</a></li>
				<!-- /MENU ITEM -->

				<!-- MENU ITEM -->
				<li class="menu-item"><a href="AboutUs">About Us</a></li>
				<!-- /MENU ITEM -->

				<li class="menu-item"><a href="ContectUs">Contact Us</a>
				</li>
				
				<%if(user!=null&&user.getEmailid()!=null&&user.getEmailid().length()>0){
					if (user.getUsertype() != 99 && user.getUsertype() != 4 &&user.getUsertype()!=6){
					%>
				<li class="menu-item"><a href="AddMoney">Add Money</a>
				</li>
				<%}%>

				<%}%>
				
				<!-- /MENU ITEM -->
			</ul>
		</nav>
		<!-- <form class="search-form">
			<input type="text" class="rounded" name="search" id="search_products"
				placeholder="Search products here..."> <input type="image"
				src="images/search-icon.png" alt="search-icon">
		</form> -->
	</div>
</div>
<!-- /MAIN MENU -->