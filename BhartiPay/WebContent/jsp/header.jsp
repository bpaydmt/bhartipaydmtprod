
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.UserSummary"%>
<%@page import="java.text.DecimalFormat"%>
<%
  User user = (User) session.getAttribute("User");
  String logo=(String)session.getAttribute("logo");
  String banner=(String)session.getAttribute("banner");
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
  String sessionid=(String)session.getAttribute("sessionid"); 
%>

<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-skins.min.css">
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/whitelabel.css" />
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/ownstyle.css"/>


<script src="js/sql.injection.js"></script>
<script>
	
  var timer =  setInterval(refreshBalance, 1000*30);
	
  $(function(){
  	SESSIONID = '<%=sessionid %>'
  	console.log(SESSIONID)
  })

  function refreshBalance(){
  	$("#walletRefresh").addClass("spinning")
  	$.get("GetWalletBalance",function(result){
  		$("#wallet-balance").text(result.amount)
  		$("#walletRefresh").removeClass("spinning")
  	})
  	
  }
</script>



<style> 


	.chosen-container { 
	    width: 100% !important;
	    top: 3px; 
	    float: right; 
	    position: relative !important;
	    right: 0px;
	}
	
	.dropdown-menu {
	    border-radius: 0;
	    border-bottom: 2px solid #ADADAD;
	    background: #f0eded !important;
	}
   .header-dropdown-list>li>.dropdown-toggle { 
	    background: rgba(181, 164, 164, 0.2) !important;
    }
	.header-dropdown-list>li {
	    display: inline-block;
	    padding-right: 0px !important;
	}
	#mobile-profile-img { 
	     padding-right:0px!important; 
	     padding-left: 0px!important; 
	}
	#mobile-profile-img a.userdropdown img {
	    width: 31px !important;
	    margin-top: 0px !important;
	    margin-left: 0px !important;
	    border-radius: 3px !important;
	    border: 0px solid #797979 !important;
	}

  .Login-alignment{
    margin-top: 9px;
  } 
  @media (max-width: 414px){
    .Login-alignment{
      margin-top: 0px;
    } 
  }  
  .popup-overlay{ 
    visibility:hidden;
    position:fixed; 
    width:50%;
    height:50%;
    left:25%;
    top: 38%; 

  }
  .overlay {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      z-index: 100000;
     }

  .popup-overlay.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content { 
   visibility:hidden;
  }

  .popup-content.active { 
    visibility:visible;
  }

  .box-login-title{
      top: 38%;
      color: #fff;
      position: absolute;
      width: 100%;
      background-color: #232323;
      background-color: rgba(0,0,0,.8);
      padding-top: 30px;
      padding-bottom: 30px;
  }
  .MessageBoxMiddle {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle .MsgTitle {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark {
      color: #a57225!important;
  }
  .MessageBoxMiddle .pText {
      font-size: 13px;
  }

  .MessageBoxButtonSection span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  .logoWraper{
        display: flex !important;
	    justify-content: center;
	    align-items: center;
	    height: 100% !important;
  }
  
  
  
  
  
  .chosen-container-single .chosen-single{
      border: 2px solid #ccc !important;
      margin-top:8px;
  }
  .box-header{
  padding: 5px 10px;
  }
  
  .smart-style-3 input[type=text]{
      border: 1px solid #ccc;
      margin-top:10px;
  }
  .modal-dialog.modal-lg{
   background-color:#fff;
  }
</style>



<!-- HEADER -->
<header id="header" style="    z-index: 10;">

    <!--  Logo part Start -->
    <div class="logoWraper">
      <span id="logo"> 
          <a href="Home" style="cursor:default;">     
            <%
              if(logo != null && !logo.isEmpty()) {
            	  if("OAGG001070".equalsIgnoreCase(user.getAggreatorid())){
            		  %>
            		  <img  src="<%=logo%>"  style="width:100%;margin-left:30px"/>
            		  <%
            	  }else{
            		 
              %>
              
           <img  src="<%=logo%>" />
            <%
            	  }
            	  } else {
            %>
            <img alt="" src="./newmis/img/wallet_logo.png" />
            <%} %>  
          </a> 
        </span> 
    </div>
    <!--  Logo part End -->
    
    <!-- Logo Right side Start -->
    <div class="project-context hidden-xs">   
  
    </div>
     <!--  Logo Right side End -->

    <!--  Right Part of Header Start -->
    <div id="rightAlign" class="pull-right"> 

	    <div id="hide-menu" class="btn-header pull-right">
	        <span> 
	            <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> 
	        </span>
	    </div> 



    <!-- New Header Design Start -->
    <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs">
    
    
    
         <li>
		  <!--   <a href="#">
		       <img src="./image/notification-bell.png" style="width: 54%;padding: 4px 0px 0px 5px;">
		    </a>
		    -->
		</li>
		
		<li class="" style="">
			<a href="javascript:void(0)" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown" aria-expanded="false">  
				<img alt="userImg" src="data:image/gif;base64,<%=user.getUserImg()!=null ? user.getUserImg() : ""%>" class="online"/>
				<span style="position: relative; color: #111; top: 3px;"><%=user.getName() %></span>
			</a>
			
			<ul class="dropdown-menu pull-right">
			
				<!-- <li>
					<a href="profile.php" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
				</li> -->	
				<li id="ea">
					<a class="ajax-link padding-10 padding-top-0 padding-bottom-0" href="ShowProfile">
						<i class="fa fa-user"></i> 
					    Profile
					</a>
				</li>	
				<li class="divider"></li>
				<li id="rp">
			    	<a class="ajax-link padding-10 padding-top-0 padding-bottom-0" href="ResetPassword">
			    		<i class="fa fa-unlock-alt"></i>
			    		Change Password
					</a>
				</li>	
				<li class="divider"></li>
				
				<li id="ea">
					<a class="ajax-link padding-10 padding-top-0 padding-bottom-0" href="CustomerSupport">
						<i class="fa fa-life-ring"></i>
						 Customer Support 
					</a>
				</li>
				<li class="divider"></li>
				<!-- 
				<li id="rp">
					<a class="ajax-link padding-10 padding-top-0 padding-bottom-0" href="GetFaq"  >
						<i class="fa fa-question-circle"></i>
						FAQ
					</a>
				</li>
				 -->	
				<li class="divider"></li>
				
				
				
					    
				<li>
					<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
				</li>
				<%-- <li class="divider"></li>
				<li>
					<a  href="CustomerSupport" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-phone"></i> <%=user.getCustomerCare() %></a>
				</li> --%>
				<li class="divider"></li>
				<li>
					<a  class="open" action="Logoff" title="Log Out" style="padding: 1px 8px 0px 8px;cursor: pointer;"> <i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
				</li> 
				
			</ul>
		</li>
		
		
	</ul>
	<!-- New Header Design End -->


	    <!--Creates the popup body-->
	    <div class="popup-overlay overlay" style="z-index: 1050;display: block;">  
	      <div class="popup-content"> 
	       <div class='box-login-title'> 
	            <div class="MessageBoxMiddle">  
	          <span class="MsgTitle">
	            <i class="fa fa-sign-out txt-color-orangeDark"></i> Logout 
	              <span class="txt-color-orangeDark">
	                <strong style="color: #fff;"> <s:property value="%{#session.USER.businessName}" /> </strong>
	              </span> ?
	          </span>
	          <p class="pText">You can improve your security further after logging out by closing this opened browser</p> 

	          <div class="MessageBoxButtonSection">
	            <span class="close">CLOSE</span>
	            <a href="Logoff" onclick="closeDmitWindow()" title="Log Out">
	              <span class="popup-btn">YES</span> 
	            </a> 
	          </div>
	           
	        </div>
	       </div>   
	      </div>
	    </div> 
	    <!-- Popup End --> 



	 
	    <div class="pull-right hidden-xs" style="padding-top: 12px;padding-right:8px;">
	        <ul class="list-inline" style="margin-bottom: 0px;"> 
	          <%if(user!=null&&user.getUsertype()==1){ %>
	          <li><a href="Home">Recharge</a></li>
	          <%}
	          if(user!=null){
	          %>
	             
	          <li>
	            
	            <strong>     
	            <%}if(user!=null&&user.getUsertype()==1){
	              out.print("(Customer)");
	              }else if(user!=null&&user.getUsertype()==2){ 
	              }else if(user!=null&&user.getUsertype()==3){
	                out.print("(Distributor)");
	              }else if(user!=null&&user.getUsertype()==7){
	                out.print("(Super-Distributor)");
	              }else if(user!=null&&user.getUsertype()==4){
	                out.print("Revenue Wallet");
	              }else if(user!=null&&user.getUsertype()==5){
	                out.print("(Sub-agent)");
	              }else if(user!=null&&user.getUsertype()==99){
	                out.print("(Admin)");
	              }else if(user!=null&&user.getUsertype()==6){
	                out.print("(Sub-aggregator)");
	              }%> </strong></li>
	             <%
	            if(user!=null&&user.getUsertype()!=99){
	            %>
	          
	           <li>
	            <i class="glyphicon glyphicons-folder-open"></i>            
	            <strong > 
	              <%=user.getCountrycurrency() %>&nbsp;
	              <% if(user.getUsertype() == 4||user.getUsertype() == 7){
	         
	              %>
	                    
	              <span id="wallet-balance"  style="cursor:pointer" onclick="openAddMoneyRequest()"><%=decimalFormat.format(user.getFinalBalance()) %></span> 
	             <%if((user.getUsertype()==4 || user.getUsertype() == 6) && user.getWhiteLabel()==1){ %>
	             &nbsp;&nbsp;Deposit Wallet: <%=user.getCountrycurrency() %>&nbsp; <span id="wallet-balance"  style="cursor:pointer" onclick="openAddMoneyRequest()"><%=decimalFormat.format(user.getCashBackfinalBalance()) %></span> 
	              <% }
	             }else{
	              %>
	              <span id="wallet-balance" ><%=decimalFormat.format(user.getFinalBalance()) %></span> <%}%> <span class="glyphicon glyphicon-repeat" style="color: #de7b1f;font-size: 15px;" id="walletRefresh" onclick="refreshBalance()"></span>&nbsp;</strong> 
	            </li>
	            <li>
					<a  href="CustomerSupport" style="padding: 4px 15px 4px 9px;">
					<i class="fa fa-phone" style="color:#e47e20;font-size: 17px;"></i> 
					<strong><%=user.getCustomerCare() %></strong>
					</a>
					
				</li>
				<li>
					<span>
					<i class="fa fa-envelope" style="color:#e47e20;font-size: 17px;"></i> 
					<strong><%=user.getSupportEmailId()%></strong>
					</span>
					
				</li>   
	            <%-- <li title="Bhartipay" style="padding: 4px 15px 4px 9px;"><i class="fa fa-money" aria-hidden="true" style="padding: 0px 10px 0 0;color:#e47e20;"></i><strong><%=user.getCountrycurrency() %>&nbsp;<span ><%=decimalFormat.format(user.getCashBackfinalBalance()) %></span> </strong></li>  --%>
	          <%} %>
	          
	              
	          <% if (user!=null&&user.getUsertype() != 4 && user.getUsertype() != 99 && user.getUsertype() != 6) { %>
	              
	            <!--   <li >
	                <a href="CustomerSupport">
	                  <i class="" style="font-size: 13px;color: #e37e20; margin: 0 9px; position: relative; top: 2px;"></i><strong></strong>
	                </a>
	              </li> -->
	         <%} %> 
	          
	          </ul>
	    </div> 



    </div>



    <!--  Right Part of Header End -->
					
</header>
<!-- END HEADER -->




    

<script> 
  $(".open").on("click", function(){
    $(".popup-overlay, .popup-content").addClass("active");
  }); 
  $(".close").on("click", function(){
    $(".popup-overlay, .popup-content").removeClass("active");
  });
</script>    
 
<!-- <%if(user!=null&&user.getUsertype()==4||user!=null&&user.getUsertype()==7){
%> -->
<script>
function openAddMoneyRequest(){
	$("#bankNameAggre").html('')
	$("#bankNameAggre").append('<option value="-1">Please select your bank</option>');
	$.get('getCashDepositBank',function(data){
	
	$.each(data,function(value,key){
	 	 $("#bankNameAggre").append($("<option></option>")
	                   .attr("value", value)
	                   .text(key)); ;
	})
    
	
	    $("#bankNameAggre").trigger('chosen:updated');

		$("#balancepop select").val("-1")
	
	})

	$("#balancepop input[type='text']").val("");
	$("#balancepop").modal('show')
}

</script>
<!-- Modal -->
<div id="balancepop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Fund Request</h4>
      </div>
      <div class="modal-body">
        <div id="addMoneyRequstMsg" style="color: red; margin: 2px 8px 5px;"></div>
        <form id="addMoneyOption"> 
          <div class="row">
           	<div class="col-md-6 form-group">
             	<label>Bank</label>
             	<select class="mandatory from-control" id="bankNameAggre" name="moneyRequest.bank"> 
              </select> 
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Transaction Type</label>
           		<select class="form-control mandatory" name="moneyRequest.txnType">
           		  <option  value="-1">Please select Transaction Type</option>
           			<option value="NEFT">NEFT</option>
           			<option value="IMPS">IMPS</option>
           			<option value="RTGS">RTGS</option>
           			<option value="CREDIT">CREDIT</option>
           			<option value="CASH">CASH</option> 
           		</select>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Amount</label>
           		<input type="text" maxlength="7" class="form-control onlyNum mandatory" name="moneyRequest.amount"/>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>UTR No</label>
           		<input type="text" class="form-control mandatory" name="moneyRequest.utrNo"/>
           	</div> 
          </div> 
        </form> 
      </div>
      <div class="modal-footer">
      	<input type="button" value="Submit" data-oxy-popup="balanceReq" onclick="submitVaForm('#addMoneyOption',this)" class="btn btn-info" /> 
      </div>
    </div>
  </div>
</div>
  <%
  } 
  if(user!=null&&user.getUsertype()==2){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(mapResult!=null&&mapResult.get("flaxMessage")!=null){out.print(mapResult.get("flaxMessage"));} %></marquee>
  <%
  }else if(user!=null&&user.getUsertype()==6){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></marquee>
  <%
  }
  %>
<div id="maintenancePop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-body">
        <h2>Sorry for Inconvenience.</h2>
        <p><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div> 
  </div> 
</div>

