<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 function closePopup(){
	 $("#confirmation").fadeOut();

	 }
function submitCashInForm(){
	//var mobile = $("#mobileno").val();
	var amount = $("#trxAmount").val();
	

	$.ajax({
		  type: "POST",
		  url: 'CalculateCashinSurcharge',
		  data:{"suBean.amount":amount},
		  success: function( data ) {
			  console.log(data)
			  if(data.status == 1000){
				 var surcharge =  data.samount;
				 var amount =  data.amount;
			   $("#confirmation").fadeIn(); 
			   $("#surcharge").text(surcharge)
			   $("#cash-in-amount").text(amount)
			   $("#total-cash-in").text(surcharge + amount );
			   $("#surChargeAmount").val(surcharge)
			  }else{
				  alert("Oops!!! something went wrong. Please try after sometime.")
			  }
		  }, 
		
		});
	
	
}
</script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Cash In</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">
													
 <form action="CashInRequest" method="post" id='wtow' class="entered-esc">
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <div class=" form-group">
    											<label for="apptxt">Payee Mobile Number<font color="red"> *</font></label>
 													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
 													
 													 <input type="hidden" name="inputBean.surChargeAmount" id="surChargeAmount" value="">
                                                   <input type="hidden" name="inputBean.userId" id="userid" value="<%=user.getId()%>">
                                                    <input type="hidden" name="inputBean.walletId" id="userid" value="<%=user.getWalletid()%>">
                                                   
                                                <input type="text" value="<s:property value='%{inputBean.mobileNo}'/>" name="inputBean.mobileNo" id="mobileno" class="form-control mandatory mobile"  placeholder="Payee Mobile Number" title="Mobile number must have 10 digits and should start with 7,8 or 9."  maxlength="10"  pattern="[6789][0-9]{9}" requiredmessage="invalid mobile number"  required>
                                                
                                        
                                            </div>       
                                        </div>

                                        
                                        	<div class="col-md-4" >
                                            	<div class="form-group">
           							<label for="apptxt">Amount <font color="red"> *</font> </label>
                                                <input type="text" <%-- value="<s:property value='%{inputBean.trxAmount}'/>" --%> name="inputBean.trxAmount" id="trxAmount" class="form-control mandatory amount-vl"  placeholder="Amount"  onkeypress="roundNum(this,event)">
                                               
                                            </div>
                                          </div>
                                      
                                         <div class="col-md-4">
                                        	<div class="form-group"><button type="button"  onclick="submitCashInForm()" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button>
										 <button type="reset" onclick="resetForm('#wtow')" style="margin-top: 20px;" class="btn btn-info btn-fill" value="Reset">Reset</button>
                             			</div>
                             	<div class="popover confirmation left top in" id="confirmation">
              
               <h3 class="popover-title">Are you sure?</h3><div class="popover-content">
               <p class="confirmation-content" style="display: none;"></p><div class="confirmation-buttons text-center">
              You will be charge Rs. <span id="surcharge"> </span> as surcharge fee on your Rs. <span id="cash-in-amount"></span> amount and total
              total amount is Rs. <span id="total-cash-in"></span>.
              <br/>
               <div class="btn-group">
               <a href="#" class="btn btn-xs btn-primary" onclick="submitVaForm('#wtow')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
               <a href="#" class="btn btn-xs btn-default" onclick="closePopup('#confirmation')"><i class="glyphicon glyphicon-remove"></i> No</a></div></div></div></div>
                                         </div>
                                        	<%-- <div class="col-md-3" style="margin-top:30px;">
                                            	<div class="form-group">
                                                 <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div><div class="selectwrap">
                                                 <s:select list="%{availableApps}" id="imid" name="tso.appId" cssStyle="width:200px;"/>
                                                </div>
                                                
                                               </div>
                                            </div>
                                          </div> --%>
                                          
                                          
                                          
                                   
                                      <div class="col-md-12" style="margin-top:30px;">
                                      <div class="form-gr">
                                      
                                      <!-- 	<input type="submit" class="btn btn-info btn-fill pull-left" value="Submit"> -->
                                      
                                    
                                    </div>
                                    
                                    
                                    
                                    
                                    </div>
                                           </div>
                                           
                                        <div class="row">
                                      
                                    
                                  
                                    </div>
                                    <div class="clearfix"></div>
                                </form>

									
									</div>
									
									</div>
									
								

                </div>
                

    
            </div>
            
              
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


