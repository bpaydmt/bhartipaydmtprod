
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.UserSummary"%>
<%@page import="java.text.DecimalFormat"%>
<%
  User user = (User) session.getAttribute("User");
  String logo=(String)session.getAttribute("logo");
  String banner=(String)session.getAttribute("banner");
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
  String sessionid=(String)session.getAttribute("sessionid"); 
%>

<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/smartadmin-skins.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="./css/newthemecss/css/ownstyle.css"/>


<script src="js/sql.injection.js"></script>
<script>
  $(function(){
  	SESSIONID = '<%=sessionid %>'
  	console.log(SESSIONID)
  })

  function refreshBalance(){
  	$("#walletRefresh").addClass("spinning")
  	$.get("GetWalletBalance",function(result){
  		console.log(result)
  	
  		$("#wallet-balance").text(result.amount)
  		$("#walletRefresh").removeClass("spinning")
  	})
  	
  }
</script>

  <!-- HEADER -->
  <header id="header">

    <!--  Logo part Start -->
    <div>
      <span id="logo"> 
          <a href="Home" style="cursor:default;">     
            <%
              if(logo != null && !logo.isEmpty()) {
              %>
           <img  src="<%=logo%>" />
            <%
              } else {
            %>
            <img alt="Bhartipay" src="./newmis/img/wallet_logo.png" />
            <%} %>  
          </a> 
        </span> 
    </div>
    <!--  Logo part End -->
    
    <!-- Logo Right side Start -->
    <div class="project-context hidden-xs">   
  
    </div>
     <!--  Logo Right side End -->

    <!--  Right Part of Header Start -->
    <div id="rightAlign" class="pull-right"> 

      <div id="hide-menu" class="btn-header pull-right">
          <span> 
            <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> 
          </span>
      </div> 

      <div class="btn-header transparent pull-right">
        <span>
          <a  class="open ttuserheading" href="Logoff" onclick="closeDmitWindow()" title="Log Out" style="padding: 1px 8px 0px 8px;margin-right: 8px;">
          <i class="fa fa-sign-out"></i> </a>
        </span>
      </div> 

      <div class="pull-right hidden-xs" style="padding-top: 5px;">
        <ul class="list-inline" style="margin-bottom: 0px;"> 
          <%if(user!=null&&user.getUsertype()==1){ %>
          <li><a href="Home">Recharge</a></li>
          <%}
          if(user!=null){
          %>
             
          <li>
            <img alt="userImg" src="data:image/gif;base64,<%=user.getUserImg()%>" width="40px" height="40px"/>
            <strong> <%=user.getName() %>     
            <%}if(user!=null&&user.getUsertype()==1){
              out.print("(Customer)");
              }else if(user!=null&&user.getUsertype()==2){ 
              }else if(user!=null&&user.getUsertype()==3){
                out.print("(Distributor)");
              }else if(user!=null&&user.getUsertype()==7){
                out.print("(Super-Distributor)");
              }else if(user!=null&&user.getUsertype()==4){
                out.print("(Aggregator)");
              }else if(user!=null&&user.getUsertype()==5){
                out.print("(Sub-agent)");
              }else if(user!=null&&user.getUsertype()==99){
                out.print("(Admin)");
              }else if(user!=null&&user.getUsertype()==6){
                out.print("(Sub-aggregator)");
              }%> </strong></li>
             <%
            if(user!=null&&user.getUsertype()!=99){
            %>
          
           <li>
            <i class="glyphicon glyphicons-folder-open"></i>            
            <strong > 
              <%=user.getCountrycurrency() %>&nbsp;
              <% if(user.getUsertype() == 4||user.getUsertype() == 7){
              %>
                    
              <span id="wallet-balance"  style="cursor:pointer" onclick="openAddMoneyRequest()"><%=decimalFormat.format(user.getFinalBalance()) %></span> 
              <% }else{
              %>
              <span id="wallet-balance" ><%=decimalFormat.format(user.getFinalBalance()) %></span> <%}%> <span class="glyphicon glyphicon-repeat" style="color: #de7b1f;" id="walletRefresh" onclick="refreshBalance()"></span>&nbsp;</strong> 
            </li>
                    
            <li title="Oxy Cash" style="padding: 4px 15px 4px 9px;"><i class="fa fa-money" aria-hidden="true" style="padding: 0px 10px 0 0;color:#e47e20;"></i><strong><%=user.getCountrycurrency() %>&nbsp;<span ><%=decimalFormat.format(user.getCashBackfinalBalance()) %></span> </strong></li> 
          <%} %>
          
              
          <% if (user!=null&&user.getUsertype() != 4 && user.getUsertype() != 99 && user.getUsertype() != 6) { %>
              
              <li >
                <a href="CustomerSupport">
                  <i class="glyphicon glyphicon-phone-alt" style="font-size: 13px;color: #e37e20; margin: 0 9px; position: relative; top: 2px;"></i><strong><%=user.getCustomerCare() %></strong>
                </a>
              </li>
         <%} %> 
          
          </ul>
      </div> 

      <div class="pull-right hidden-xs" style="padding-top: 9px; padding-right: 20px;">
<!--         <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" style="background: #de7b1f;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Download Drivers
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <ul style="margin-bottom: 0px;">
              <li>
                <a style="font-size: 10px;" href="./drivers/mantra.zip" download>Mantra</a>
              </li>
              <li>
                <a style="font-size: 10px;" href="./drivers/STARTEK-SDK.zip" download>Startek</a>
              </li>
              <li>
                <a style="font-size: 10px;" href="./drivers/MORPHO-SDK.zip" download>Morpho</a>
              </li> 
            </ul> 
          </div>
        </div> -->
      </div>

    </div>






   
    <!--  Right Part of Header End -->
    
  </header>
    <!-- END HEADER -->




    





    
 
<!-- <%if(user!=null&&user.getUsertype()==4||user!=null&&user.getUsertype()==7){
%> -->
<script>
function openAddMoneyRequest(){
	$("#bankNameAggre").html('')
	$("#bankNameAggre").append('<option value="-1">Please select your bank</option>');
	$.get('getCashDepositBank',function(data){
	
	$.each(data,function(value,key){
	 	 $("#bankNameAggre").append($("<option></option>")
	                   .attr("value", value)
	                   .text(key)); ;
	})
    
	
	    $("#bankNameAggre").trigger('chosen:updated');

		$("#balancepop select").val("-1")
	
	})

	$("#balancepop input[type='text']").val("");
	$("#balancepop").modal('show')
}

</script>
<!-- Modal -->
<div id="balancepop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Fund Request</h4>
      </div>
      <div class="modal-body">
        <div id="addMoneyRequstMsg" style="color: red; margin: 2px 8px 5px;"></div>
        <form id="addMoneyOption"> 
          <div class="row">
           	<div class="col-md-6 form-group">
             	<label>Bank</label>
             	<select class="mandatory from-control" id="bankNameAggre" name="moneyRequest.bank"> 
              </select> 
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Transaction Type</label>
           		<select class="form-control mandatory" name="moneyRequest.txnType">
           		  <option  value="-1">Please select Transaction Type</option>
           			<option value="NEFT">NEFT</option>
           			<option value="IMPS">IMPS</option>
           			<option value="RTGS">RTGS</option>
           			<option value="CREDIT">CREDIT</option>
           			<option value="CASH">CASH</option> 
           		</select>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>Amount</label>
           		<input type="text" maxlength="7" class="form-control onlyNum mandatory" name="moneyRequest.amount"/>
           	</div>
           	<div class="col-md-6 form-group">
           	  <label>UTR No</label>
           		<input type="text" class="form-control mandatory" name="moneyRequest.utrNo"/>
           	</div> 
          </div> 
        </form> 
      </div>
      <div class="modal-footer">
      	<input type="button" value="Submit" data-oxy-popup="balanceReq" onclick="submitVaForm('#addMoneyOption',this)" class="btn btn-info" /> 
      </div>
    </div>
  </div>
</div>
  <%
  } 
  if(user!=null&&user.getUsertype()==2){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(mapResult!=null&&mapResult.get("flaxMessage")!=null){out.print(mapResult.get("flaxMessage"));} %></marquee>
  <%
  }else if(user!=null&&user.getUsertype()==6){
  %>
  <marquee style="margin-top: 1px;margin-bottom: 0px;font-size: 12px;"" scrollamount="4"><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></marquee>
  <%
  }
  %>
<div id="maintenancePop" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-body">
        <h2>Sorry for Inconvenience.</h2>
        <p><% if(user!=null&&user.getPortalMessage()!=null){out.print(user.getPortalMessage());} %></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div> 
  </div> 
</div>

