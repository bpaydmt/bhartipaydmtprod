<%@page import="java.util.Map"%>
<%!
String version = "?i=10017fd9f";

%>   
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");
%>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bhartipay">
    <meta name="author" content="Bhartipay">   
   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
    <link rel="shortcut icon" href="./images/<%=favicon%>">
    <link href="./css/bootstrap-simplex.min.css" rel="stylesheet">
	<link href="./css/font-awesome.css" rel="stylesheet">
    <link href='./css/animate.min.css' rel='stylesheet'>  
    <link href='./css/chosen.css' rel='stylesheet'>
    
    
	<link href="./css/datepicker.min.css" rel="stylesheet" type="text/css">
	<link href="./css/charisma-app.css" rel="stylesheet">
	<link href='./css/dataTables.min.css' rel='stylesheet'>
	<link href='./css/Tables1.min.css' rel='stylesheet'>	
	<link href='./css/demo_table_jui.css' rel='stylesheet'> 
	<link href='./css/demo_table.css' rel='stylesheet'>   
	<link href='./css/customStyle.css<%=version %>' rel='stylesheet'>
	<link href='./css/font-awesome.min.css<%=version %>' rel='stylesheet'>
	<link href='./css/chosen.css' rel='stylesheet'>
	<link href='./css/jquery-customselect.css<%=version %>' rel='stylesheet'>
	<link href='./css/multiple-select.css<%=version %>' rel='stylesheet'>
	
	 <script src="./js/jquery.min.js"></script>    
	 
	  <script src="./js/chosen.jquery.js"></script>    

  
	<script src="./js/datepicker.js"></script>

	
	<script src="./js/datepicker.en.js"></script>
	<script src="./js/handlebars-v4.0.5.js"></script>
	<script src="./js/multiple-select.js"></script>
	<%-- <script type="text/javascript"> <%@ include  file="../js/common.js"%></script> --%>
	<script type="text/javascript"> <%@ include  file="../js/user/validationFunctions.js"%></script>
	<script type="text/javascript"> <%@ include  file="../js/user/create.js"%></script>
	<script src="./js/validateWallet.js<%=version %>"></script>
	<script src="./js/bootstrap.min.js"></script>
	  <script src="./js/chosen.jquery.js"></script> 
	  	<script src="./js/sql.injection.js"></script>   
	<script src="./js/charisma.js"></script>
	 	
	