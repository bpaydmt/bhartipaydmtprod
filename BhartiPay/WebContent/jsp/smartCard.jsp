<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.SmartCardBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletKYCBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Prepaid Card - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Prepaid Card - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Prepaid Card - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Prepaid Card - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Prepaid Card - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

 
 
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
 <div class="box2 col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Prepaid Card</h2>
            </div>

        </div>
  
</div>
<%if(request.getAttribute("flag")==null||!((String)request.getAttribute("flag")).equalsIgnoreCase("false")){ %>
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
<%
}

SmartCardBean smartCardList=(SmartCardBean)request.getAttribute("smartCardList");
if(smartCardList==null||smartCardList.getReqId()==null||smartCardList.getReqId().isEmpty()){
%>
<form action="GenerateSmartCard" id="smartCard" method="post" name="PG">




							<div class="col-md-4">
								<div class="form-group">
									<label for="apptxt">First Name</label> 
									<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									<input
										class="form-control userName  mandatory" name="smartCardBean.name" type="text"
										id="preferredName" placeholder="First Name"
										value="<s:property value='%{smartCardBean.name}'/>"  />
								</div>
							</div>


							<div class="col-md-4">
								<div class="form-group">
									<label for="apptxt">Last Name</label> <input
										class="form-control userName mandatory" name="smartCardBean.lastName" type="text"
										placeholder="Last Name"
										value="<s:property value='%{smartCardBean.lastName}'/>" />
								</div>
							</div>



							<div class="col-md-4">
								<div class="form-group">
									<label for="apptxt">Preferred Name</label> <input
										class="form-control userName onlyAlph mandatory" name="smartCardBean.preferredName" type="text"
										id="preferredName" placeholder="Preferred Name"
										value="<s:property value='%{smartCardBean.preferredName}'/>" />
								</div>
							</div>


							<div class="col-md-4">
								<div class="form-group">
									<label for="apptxt">Password</label> <input
										class="form-control password-vl mandatory " name="smartCardBean.passowrd" type="password"
										id="smsurl" placeholder="Password"
										value="<s:property value='%{smartCardBean.passowrd'/>"/>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="apptxt">Card Type</label>
									<select
										class="form-control mandatory " name="smartCardBean.cardType">
									<option value="PC">Physical Card</option>
									<option value="VC">Virtual Card</option>
									</select>
								</div>
							</div>



							<button type="button" class="btn btn-info btn-fill" onclick="submitVaForm('#smartCard')" style="margin-top: 20px;">Generate  Prepaid Card</button>
</form>					
			<%
									}else{	
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Req ID</u></th>
						<th><u>Card Type</u></th>
						<th><u>First Name</u></th>
						<th><u>Last Name</u></th>	
						<th><u>Preferred  Name</u></th>	
						<th><u>Email</u></th>						
						<th><u>Mobile</u></th>
						<!-- <th><u>Card Number</u></th>
						<th><u>Expiry Date</u></th> -->
						<th><u>Card Status</u></th>	
						
						<!-- <th style="width:130px"><u></u></th> -->
						
					</tr>
				</thead>
				<tbody>
				<%
				if(smartCardList!=null){	
				
				%>
				  <tr>				  	
		             <td><%=smartCardList.getReqId() %></td>
		            <td><%if(smartCardList.getCardType()!=null&&smartCardList.getCardType().equalsIgnoreCase("PC")){out.print("Physical Card");}else if(smartCardList.getCardType()!=null&&smartCardList.getCardType().equalsIgnoreCase("VC")){out.print("Virtual Card");}; %></td>
		              <td><%=smartCardList.getName()%></td>
		               <td><%=smartCardList.getLastName() %></td>
		               <td><%=smartCardList.getPreferredName() %></td>
		              <td><%=smartCardList.getEmailId() %></td>	          	             
		             <td><%=smartCardList.getMobileNo() %></td>
		           <%-- <td><%=smartCardList.getPrePaidCardNumber()%></td>
		            <td><%if(smartCardList.getExpDate()==null){}else{out.print(smartCardList.getExpDate());}%></td> --%>
		              <td><%=smartCardList.getPrePaidStatus()%></td>
		            <!--  <td>
	
					</td>  -->
		                
                  </tr>
                  
			        </tbody>		</table>
			        
			        
			        
			  	     <%if(smartCardList.getStatus()!=null&&smartCardList.getStatus().equalsIgnoreCase("Card Dispatch")){%>
		             <input type="submit" value="Link Card" data-toggle="modal" data-target="#myModal<%=smartCardList.getReqId()%>" class="btn btn-info">
					<%} if(smartCardList.getStatus()!=null&&smartCardList.getStatus().equalsIgnoreCase("Card Linked")){
					%>
					<input type="submit" value="Update Cardholder Profile" data-toggle="modal" data-target="#myModal3<%=smartCardList.getReqId()%>" class="btn btn-info">	
					<input type="submit" value="Update Address" data-toggle="modal" data-target="#myModal4" class="btn btn-info">
					<%
					if(smartCardList.getCardType()!=null&&smartCardList.getCardType().equalsIgnoreCase("PC")){
					%>
			 		<input type="submit" value="Get PIN" data-toggle="modal" data-target="#changePin" class="btn btn-info">
					<%}else{						
						%>
						
				 		<input type="submit" value="Get CVV"  class="btn btn-info" onclick="submitVaForm('#getCVV')">
				 		
					<% }%>
					<input type="submit" value="Update Password" data-toggle="modal" data-target="#myModal5" class="btn btn-info">
		             <input type="submit" value="Block Card" data-toggle="modal" data-target="#myModal2<%=smartCardList.getReqId()%>" class="btn btn-info">
		             
					<%} %>
					 <input type="submit" value="Card Details" onclick="getSmartCard()" data-toggle="modal" data-target="#myModal10" class="btn btn-info">
					
					
							
					<form action="GenerateCVV" method="post" class="pull-left" style="margin: 0 4px 0 0;" id="getCVV">
						<input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
				 		</form>
							<div id="myModal2<%=smartCardList.getReqId()%>" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Block prepaid Card</h4>
							      </div>
							      <div class="modal-body">
							      
							      <form action="BlockCard" method="post">
							      <div class="form-group">
							      <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							      <input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
							      <label>
							      Select reason for blocking card
							      </label>
							      <select name="smartCardBean.blockedType">
							      <option value="lost">Lost</option>
							       <option value="stolen">Stolen</option>
							        <option value="damaged">Damaged</option>
							        
							      </select>
							      </div>
							     
							      <div class="text-right">
								          <input type="submit" class="btn btn-info" value="Block prepaid card">
								  </div>
							     
							      </form> 
							      
							      </div>
							      
							     
							    </div>
							
							  </div>
							</div>
								<div id="myModal10" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Card Details</h4>
							      </div>
							      <div class="modal-body" id="smardCardDetails">
							      
							
							      
							      </div>
							      
							     <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
							    </div>
							
							  </div>
							</div>
					
					<%
					String flag=(String)request.getAttribute("flag");
	if(!(request.getAttribute("updatePassword")!=null&&((String)request.getAttribute("updatePassword")).equalsIgnoreCase("true"))&&!(request.getAttribute("updateAddress")!=null&&((String)request.getAttribute("updateAddress")).equalsIgnoreCase("true"))&&flag!=null&&flag.equalsIgnoreCase("false")){
						%>
						<script>
						
						$(function(){
							$("#myModal3<%=smartCardList.getReqId()%>").modal("show");
						
							})
						                     
						
						</script>
						
						<%
					}else
						if((request.getAttribute("updateAddress")!=null&&((String)request.getAttribute("updateAddress")).equalsIgnoreCase("true"))&&flag!=null&&flag.equalsIgnoreCase("false")){
						%>
						<script>
						
						$(function(){
							$("#myModal4").modal("show");
						
							})
						                     
						
						</script>
						
						<%
					}else if((request.getAttribute("updatePassword")!=null&&((String)request.getAttribute("updatePassword")).equalsIgnoreCase("true"))&&flag!=null&&flag.equalsIgnoreCase("false")){
						%>
						<script>
						
						$(function(){
							$("#myModal5").modal("show");
						
							})
						                     
						
						</script>
						
						<%
					}
					%>
					
					<div id="myModal5" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Upadate Password</h4>
							      </div>
							       <%if((request.getAttribute("updatePassword")!=null&&((String)request.getAttribute("updatePassword")).equalsIgnoreCase("true"))&&request.getAttribute("flag")!=null&&((String)request.getAttribute("flag")).equalsIgnoreCase("false")){ %>
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
<%
}%>
							      <div class="modal-body">
							      
							      <form action="UpdatePassword" method="post" id="updatePwd" class="form-horizontal small-input-form">
								<div class="form-group">
								<label class="control-label col-md-4">Password</label>
								<div class="col-md-7">
								 <input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
								  <input type="hidden" name="smartCardBean.emailId" value="<%=smartCardList.getEmailId()%>">
								<input class="form-control password-vl mandatory" name="smartCardBean.passowrd" type="password">
								</div>								
								
								
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Confirm Password</label>
								<div class="col-md-7">
								<input class="form-control confirm-vl mandatory"  type="password">
								</div>								
								
								
								</div>
								
							      </form> 
							      <div class="text-right">
							     <input type="button" onclick="submitVaForm('#updatePwd')" class="btn btn-info" value="Submit" />
							      </div>
							      </div>
							      
							     
							    </div>
							
							  </div>
							</div>
						<div id="myModal3<%=smartCardList.getReqId()%>" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Prepaid User Details</h4>
							      </div>
							      <%if(!(request.getAttribute("updatePassword")!=null&&((String)request.getAttribute("updatePassword")).equalsIgnoreCase("true"))&&!(request.getAttribute("updateAddress")!=null&&((String)request.getAttribute("updateAddress")).equalsIgnoreCase("true"))&&request.getAttribute("flag")!=null&&((String)request.getAttribute("flag")).equalsIgnoreCase("false")){ %>
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
<%
}%>
							      
							      <div class="modal-body">
							      <form action="UpdatePrePaidCard" method="post" class="form-horizontal small-input-form">
							      	<div class="form-group">
								<label class="control-label col-md-4">Title</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.reqId" value="<%=smartCardList.getReqId() %>" type="hidden">
								<input class="form-control"  name="smartCardBean.title" value="<%if(smartCardList.getTitle()!=null){out.print(smartCardList.getTitle());} %>" type="text">
								</div>								
								</div>
								
								<div class="form-group">
								<label class="control-label col-md-4">First Name</label>
								<div class="col-md-7">
								<input disabled="disabled" class="form-control" name="smartCardBean.name" value="<%if(smartCardList.getName()!=null){out.print(smartCardList.getName());} %>" type="text">
								</div>
								
								
								
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Last Name</label>
								<div class="col-md-7">
								<input disabled="disabled" class="form-control" name="smartCardBean.lastName" value="<%if(smartCardList.getLastName()!=null){out.print(smartCardList.getLastName());}%>" type="text">	
								</div>							
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Preffered Name</label>
									<div class="col-md-7">
									<input disabled="disabled" class="form-control" name="smartCardBean.preferredName" value="<%if(smartCardList.getPreferredName()!=null){out.print(smartCardList.getPreferredName());}%>" type="text">
									</div>							
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Gender</label>
									<div class="col-md-7">
									<input class="form-control" name="smartCardBean.gender" value="<%if(smartCardList.getGender()!=null){out.print(smartCardList.getGender());}%>" type="text">	
									</div>							
								</div>
								
								<div class="form-group">
								<label class="control-label col-md-4">Email</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.emailId" value="<%if(smartCardList.getEmailId()!=null){out.print(smartCardList.getEmailId());}%>" type="text">
								</div>								
								</div>
								<div class="form-group">
								
								<label class="control-label col-md-4">Mobile Country Code</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.countryCode" value="<%if(smartCardList.getCountryCode()>=0){out.print(smartCardList.getCountryCode());}%>" type="text">
								</div>								
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Mobile</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.mobileNo" value="<%if(smartCardList.getMobileNo()!=null){out.print(smartCardList.getMobileNo());}%>" type="text">	
								</div>							
								</div>
							
								<div class="form-group">
								<label class="control-label col-md-4">Nationality</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.nationality" value="<%if(smartCardList.getNationality()!=null){out.print(smartCardList.getNationality());}%>" type="text">	
								</div>							
								</div>
								
									<div class="form-group">
								<label class="control-label col-md-4">Country of Issue</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.countryOfIssue" value="<%if(smartCardList.getCountryOfIssue()!=null){out.print(smartCardList.getCountryOfIssue());}%>" type="text">	
								</div>							
								</div>
								
									<div class="form-group">
								<label class="control-label col-md-4">Id Type</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.idType" value="<%if(smartCardList.getIdType()!=null){out.print(smartCardList.getIdType());}%>" type="text">	
								</div>							
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Id Number</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.idNumber" value="<%if(smartCardList.getIdNumber()!=null){out.print(smartCardList.getIdNumber());}%>" type="text">	
								</div>							
								</div>
								<div class="text-right">
						<input type="submit" class="btn btn-info " />
						</div>
							      </form> 
							      
							      </div>
							      
							     
							    </div>
							
							  </div>
							</div>
									</script>
						
				
						<div id="myModal4" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Prepaid User Address</h4>
							      </div>
							      <%if((request.getAttribute("updateAddress")!=null&&((String)request.getAttribute("updateAddress")).equalsIgnoreCase("true"))&&request.getAttribute("updateAddress")!=null&&((String)request.getAttribute("updateAddress")).equalsIgnoreCase("true")&&request.getAttribute("flag")!=null&&((String)request.getAttribute("flag")).equalsIgnoreCase("false")){ %>
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
<%
}%>
							      
							      <div class="modal-body">
							    <form action="UpdateAddress" method="post" class="form-horizontal small-input-form">
								<div class="form-group">
								<label class="control-label col-md-4">Address Type</label>
								<div class="col-md-7">
								<select class="form-control" name="smartCardBean.type" style="    padding: 1px 5px;
    line-height: 17px;">
								<option value="residential">Residential</option>
								<option value="billing">Billing</option>
								</select>
								</div>
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">Address 1</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.address_1" value="<%if(smartCardList.getAddress_1()!=null){out.print(smartCardList.getAddress_1());}%>" type="text">
								</div>
								</div>
								
								<div class="form-group">
								<label class="control-label col-md-4">Address 2</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.reqId" value="<%=smartCardList.getReqId() %>" type="hidden">
								<input class="form-control" name="smartCardBean.address_2" value="<%if(smartCardList.getAddress_2()!=null){out.print(smartCardList.getAddress_2());}%>" type="text">	
								</div>							
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">City</label>
									<div class="col-md-7">
									<input class="form-control" name="smartCardBean.city" value="<%if(smartCardList.getCity()!=null){out.print(smartCardList.getCity());}%>" type="text">
									</div>							
								</div>
								<div class="form-group">
								<label class="control-label col-md-4">State</label>
									<div class="col-md-7">
									<input class="form-control" name="smartCardBean.state" value="<%if(smartCardList.getState()!=null){out.print(smartCardList.getState());}%>" type="text">	
									</div>							
								</div>
								
								<div class="form-group">
								<label class="control-label col-md-4">Country</label>
								<div class="col-md-7">
								<%-- <input class="form-control" name="smartCardBean.country" value="<%if(smartCardList.getCountry()!=null){out.print(smartCardList.getCountry());}%>" type="text"> --%>
						<select name="smartCardBean.country" class="form-control" style="padding: 1px 5px;
line-height: 17px;">
							<option value="india">India</option>
							<option value="china">China</option>
						
						</select>		


								</div>								
								</div>
								<div class="form-group">
								
								<label class="control-label col-md-4">zipcode</label>
								<div class="col-md-7">
								<input class="form-control" name="smartCardBean.zipcode" value="<%if(smartCardList.getZipcode()!=null){out.print(smartCardList.getZipcode());}%>" type="text">
								</div>								
								</div>
												
						
								<div class="text-right">
						<input type="submit" class="btn btn-info " />
						</div>
							      </form> 
							      
							      </div>
							      
							     
							    </div>
							
							  </div>
							</div>
					<div id="myModal<%=smartCardList.getReqId()%>" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Link Prepaid Card</h4>
							      </div>
							      <div class="modal-body">
							      
   							      <form action="LinkCard" method="post" id="dispatchSmart">
							      <div class="form-group">
							      <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							      <input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
							      <label>
							      Card Proxy Number
			  			      </label> <input type="text" name="smartCardBean.prePaidCardPin" id="card<%=smartCardList.getReqId()%>" class="form-control onlyNum mandatory"  maxlength="12"/>
							      
							      </div>
							      <%--  <div class="form-group">
							      <label>
							      Card Number
							      </label> <input type="text" name="smartCardBean.prePaidCardNumber" id="cardNum<%=smartCardList.getReqId()%>"  class="form-control onlyNum mandatory" maxlength="16"/>
							      
							      </div> --%>
							      <div class="text-right">
								          <input type="button" onclick="submitVaForm('#dispatchSmart')" class="btn btn-info" value="Link prepaid card">
								  </div>
							     
							      </form> 
							      
							      </div>
							    </div>
							  </div>
							</div>
							
							
							<div id="changePin" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Get PIN</h4>
							      </div>
							      <div class="modal-body">
							      
   							      <form action="ChangePin" method="post" id="changePinMode">
							      <div class="form-group">
							      <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							       <input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
							      <label>
							      Select Mode
							      </label>
							      <select name="smartCardBean.changePinMode" class="form-control mandatory">
							      <option value="M">Email</option>
							      <option value="S">SMS</option> 
							      </select>
							      </div>
							    
							      <div class="text-right">
								          <input type="button" onclick="submitVaForm('#changePinMode')" class="btn btn-info" value="Get PIN">
								  </div>
							     
							      </form> 
							      
							      </div>
							    </div>
							  </div>
							</div>
							
							
								<div id="findCvv" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Change Pin</h4>
							      </div>
							      <div class="modal-body">
							      
   							      <form action="FindCvv" method="post" id="findCvvForm">
							      <div class="form-group">
							      <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							      <input type="hidden" name="smartCardBean.reqId" value="<%=smartCardList.getReqId()%>">
							      <label>
							      Select Mode
							      </label>
							      <select name="smartCardBean.changePinMode" class="form-control mandatory">
							      <option value="M">Email</option>
							     <!--  <option value="S">SMS</option> -->
							      </select>
							      </div>
							    
							      <div class="text-right">
								          <input type="button" onclick="submitVaForm('#changePinMode')" class="btn btn-info" value="Change Pin">
								  </div>
							     
							      </form> 
							      
							      </div>
							    </div>
							  </div>
							</div>
							
					</br>      
			        
			        
			        
		</div> <%
                  }
				%>
		
		<%} %>
</div>
</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->
<!-- ** -->
<script type="text/javascript">
$(document).on('hidden.bs.modal', '.modal',function () {
	
		
		    $(".errorMsg").hide()

	   $("font").html("");
	})
function getSmartCard(){
	
	
		$("#smardCardDetails").html("<span>Loading....</span>")
	$.post( "FetchingCardType", "reqId="+"<%=smartCardList.getReqId() %>",function(data){
		
		$("#smardCardDetails").html(data)
	}).fail(function(){
	    alert( "Oops! Something went wrong please try again after sometime." );
	  });
	
}
</script>



</body>
</html>



