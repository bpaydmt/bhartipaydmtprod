<%@page import="com.google.gson.Gson"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.Coupons"%>
<%@page import="java.util.List"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
	
	<script type="text/javascript">
	

	function genrateCoupon(id,data){
	    var data = data;

	   $(id + " .com-logo").css({"background": "url(" + data.store_image +") no-repeat center center/contain"});
	   $(id + " .title").text(data.coupon_title);
	   $(id + " .com-url").attr("href", data.store_link);
	   $(id).attr("title", data.coupon_description);
	   $(id + " .c-code").text(data.coupon_code);

	   }
	</script>

</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->


<%
List<Coupons> coupons=(List<Coupons>)request.getAttribute("coupons");

%>

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Offers</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		
	<%if(coupons!=null){
for(int i=0;i<coupons.size();i++){
Coupons coupons2=coupons.get(i);
Gson gson = new Gson();
String jsonText=gson.toJson(coupons2);
System.out.println(jsonText+"");
	%>
	<script type="text/javascript">
	
	var v=genrateCoupon('#<%=coupons2.getOfferId()%>',<%=jsonText%>);
	</script>
	
	
		<div class="coupon" id="<%=coupons2.getOfferId() %>" title="">
<a class="com-url" href="" title="" target="_blank">
<div class="com-logo"></div> <div class="c-code"></div>
 <h3 class="title"></h3>
 <div class="img"></div>
 </a>
</div>
<%
	}
}
%>
		
                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->



</body>
</html>


