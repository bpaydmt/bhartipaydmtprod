<%@page import="com.bhartipay.lean.OnboardStatusResponse"%>
<%@page import="com.bhartipay.customerCare.vo.AgentCustDetails"%>
<%@page import="com.bhartipay.customerCare.vo.CustomerCareResponse"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>


<%@page import="java.text.DecimalFormat"%> 
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 

<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<jsp:include page="gridJs.jsp"></jsp:include>
  <script type="text/javascript">
   function showSearchDetails(){

	var div = $("#searchDetails")
	
	if(div.is(":hidden")){
		div.show()
		$("#showDetailsLink span").text("Hide details")
		$("#showDetailsLink .fa").addClass("fa-minus-square")
		$("#showDetailsLink .fa").removeClass("fa-plus-square")
	}else{
		div.hide()

	
		$("#showDetailsLink span").text("Show details")
		$("#showDetailsLink .fa").removeClass("fa-minus-square")
		$("#showDetailsLink .fa").addClass("fa-plus-square")
	}
}

function walletHistory(userId){
	
	$.ajax({
		method:'POST',
		url:'CustomerPassbookById',
		data:"userId="+userId,
		success:function(data){
			$("#walletHistory").html(data);
		    
		    $('#example').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'Wallet History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'Wallet History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'Wallet History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'Wallet History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'Wallet History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#dpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#dpStart").blur(function(){
		       $('#dpEnd').val("")
		     $('#dpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function bhartiPayHistory(userId){
	
	$.ajax({
		method:'POST',
		url:'CustomerCashBackPassbookById',
		data:"userId="+userId,
		success:function(data){
			$("#oxyHistory").html(data);
		    
		    $('#oxyWalletTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'Oxycash Wallet History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'Oxycash Wallet History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'Oxycash Wallet History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'Oxycash Wallet History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'Oxycash Wallet History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#oxydpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#oxydpStart").blur(function(){
		       $('#oxydpEnd').val("")
		     $('#oxydpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#oxydpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}




function rechargeHistory(userId){

	$.ajax({
		method:'POST',
		url:'CustomerRechargeReport',
		data:"userId="+userId,
		success:function(data){
			$("#recharge").html(data);
		    
		    $('#rechargeTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'Recharge History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'Recharge History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'Recharge History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'Recharge History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'Recharge History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#rechargedpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#rechargedpStart").blur(function(){
		       $('#rechargedpEnd').val("")
		     $('#rechargedpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#rechargedpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function GetCustAepsReport(userId){
	
	$.ajax({
		method:'POST',
		url:'GetCustAepsReport',
		data:"userId="+userId,
		success:function(data){
			$("#aepsReport").html(data);
		    
		    $('#aepsReportTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'AEPS History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'AEPS History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'AEPS History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'AEPS History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'AEPS History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#aepsdpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#aepsdpStart").blur(function(){
		       $('#aepsdpEnd').val("")
		     $('#aepsdpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#aepsdpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function GetCustBBPSReport(userId){
	
	$.ajax({
		method:'POST',
		url:'GetCustBBPSReport',
		data:"userId="+userId,
		success:function(data){
			$("#bbpsReport").html(data);
		    
		    $('#bbpsReportTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'BBPS History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'BBPS History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'BBPS History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'BBPS History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'BBPS History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#bbpsdpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#bbpsdpStart").blur(function(){
		       $('#bbpsdpEnd').val("")
		     $('#bbpsdpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#bbpsdpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function GetCustMATMReport(userId){
	
	$.ajax({
		method:'POST',
		url:'GetCustMATMReport',
		data:"userId="+userId,
		success:function(data){
			$("#matmReport").html(data);
		    
		    $('#matmReportTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'MATM History - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'MATM History - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'MATM History - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'MATM History - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'MATM History - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#matmdpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#matmdpStart").blur(function(){
		       $('#matmdpEnd').val("")
		     $('#matmdpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#matmdpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}

function dmtHistory(userId){
	
	$.ajax({
		method:'POST',
		url:'GetCDmtDetailsReport',
		data:"userId="+userId,
		success:function(data){
			$("#dmt").html(data);
		    
		    $('#dmtTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'DMT Details - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'DMT Details - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'DMT Details - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'DMT Details - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'DMT Details - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#dmtdpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#dmtdpStart").blur(function(){
		       $('#dmtdpEnd').val("")
		     $('#dmtdpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#dmtdpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function custdmtHistory(userId){
	
	$.ajax({
		method:'POST',
		url:'GetCustDmtDetailsReport',
		data:"userId="+userId,
		success:function(data){
			$("#dmt").html(data);
		    
		    $('#dmtTable').DataTable( {
		        dom: 'Bfrtip',
		        autoWidth: false,
		        order: [[ 0, "desc" ]],
		        buttons: [
		             {
		             
		                extend: 'copy',
		                text: 'COPY',
		                title:'DMT Details - ' + '<%= currentDate %>',
		                message:'<%= currentDate %>',
		            },  {
		             
		                extend: 'csv',
		                text: 'CSV',
		                title:'DMT Details - ' + '<%= currentDate %>',
		              
		            },{
		             
		                extend: 'excel',
		                text: 'EXCEL',
		                title:'DMT Details - ' + '<%= currentDate %>',
		            
		            }, {
		             
		                extend: 'pdf',
		                text: 'PDF',
		                title:'DMT Details - ' + '<%= currentDate %>',
		                message:" "+ "<%= currentDate %>" + "",
		               
		            },  {
		             
		                extend: 'print',
		                text: 'PRINT',
		                title:'DMT Details - ' + '<%= currentDate %>',
		              
		            }
		        ]
		    } );
		    
		    
		    $('#dmtdpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		     

		    });
		 
		     

		     
		 
		    $("#dmtdpStart").blur(function(){
		       $('#dmtdpEnd').val("")
		     $('#dmtdpEnd').datepicker({
		          language: 'en',
		         autoClose:true,
		         minDate: new Date(converDateToJsFormat($('#dmtdpStart').val())),           
		         maxDate: new Date(),
		         
		        }); 
		    })
		}
		
		
	})
}


function converDateToJsFormat(date) {
   
 var sDay = date.slice(0,2);
 var sMonth = date.slice(3,6);
 var yYear = date.slice(7,date.length)

 return sDay + " " +sMonth+ " " + yYear;
}
</script>
 



 

</head>

<% 
AgentCustDetails agentCustDetails=null;
User user = (User) session.getAttribute("User");

WalletMastBean walletPlan=(WalletMastBean)request.getAttribute("plan");

OnboardStatusResponse onboardStatus=(OnboardStatusResponse)request.getAttribute("onboardStatus");

CustomerCareResponse customerCareResponse=(CustomerCareResponse)request.getAttribute("GETUSERDTL");
 if(customerCareResponse!=null  ){
agentCustDetails=(AgentCustDetails)customerCareResponse.getAgentCustDetails();
} 
//System.out.println(customerCareResponse.getWalletBalance());


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

 
<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">          
        
         
            

 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header   ">
			<h2>Customer Care Portal</h2>
		</div>
		
		<div class="box-content  ">
		
			<form action="GetUserDtl" method="post">
 <div class="row"> 
				<font color="red"><s:actionerror/> </font>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  -->
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{customerCareRequest.userId}'/>"
										name="customerCareRequest.userId" id="userId"
										class="form-control " placeholder="Please enter UserId"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								 </div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left 	">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>

 <%if(agentCustDetails!=null && agentCustDetails.getId()!=null) { %>
								<div class="box-content row" style="font-size: 12px;
border-top: 1px solid #ccc;
margin: -17px 0px auto !important;min-height: inherit;">
<div class="row"><a href="javascript:void(0)" id="showDetailsLink" onclick="showSearchDetails()" class="pull-right" style="margin-right: 16px;"><span>Hide Details</span><i class="fa fa-minus-square" style="margin-left:5px" aria-hidden="true"></i></a></div>

<div id="searchDetails">

									<div class="row row-m-15">
										<div class="col-md-4">
											<strong> User Id: </strong>
											<%=agentCustDetails.getId()%>

										</div>
										<div class="col-md-4">
											<strong> Wallet ID: </strong>
											<%=agentCustDetails.getWalletid()%>

										</div>
										
											<div class="col-md-4">
											<strong>Login Status: </strong>
											
											<%if (agentCustDetails.getUserstatus().equalsIgnoreCase("A")){ %>
											<span style="padding:5px;color:green">
											<b>Active</b>
											</span>
											<%}else if (agentCustDetails.getUserstatus().equalsIgnoreCase("D")){ %>
											<span style="padding:5px;color:teal">
											<b>Deactive</b>
											</span>
											<%}else if (agentCustDetails.getUserstatus().equalsIgnoreCase("S")){ %>
											<span style="padding:5px;color:yellow">
											<b>Suspended</b>
											</span>
											<%}else if (agentCustDetails.getUserstatus().equalsIgnoreCase("B")){ %>
											<span style="padding:5px;color:red">
											<b>Blocked</b>
											</span>
											<%} %>
											
											
											
											<strong> / Status: </strong>
											
											<%if(agentCustDetails.getApprovalRequired().equalsIgnoreCase("Y")){out.print("Pending");}else if(agentCustDetails.getApprovalRequired().equalsIgnoreCase("A")){out.print("Approved");}else if(agentCustDetails.getApprovalRequired().equalsIgnoreCase("C")){out.print("Accepted");}  else if(agentCustDetails.getApprovalRequired().equalsIgnoreCase("R")){out.print("Declined");}  else if(agentCustDetails.getApprovalRequired().equalsIgnoreCase("N")){out.print("Approved");}    else{out.print(" - ");}%>
											
											<%-- 
											<%if (agentCustDetails.getBlockStatus()!=null&&agentCustDetails.getBlockStatus().equalsIgnoreCase("A")){ %>
											<span style="padding:5px;color:green">
											<b>Active</b>
											</span>
											<%}else{ %>
											<span style="padding:5px;color:Red">
											<b>Blocked</b>
											</span>
											<%}%>
											
											 --%>
											
										</div>
										

										
									
									</div>


<div class="row row-m-15">

					<div class="col-md-4">
											<strong> Name: </strong>
											<%=agentCustDetails.getName()%>

										</div>
						<div class="col-md-4">
											<strong> Mobile No.: </strong>
											<%=agentCustDetails.getMobileno()%>

										</div>
										<div class="col-md-4">
											<strong> Email: </strong>
											<%=agentCustDetails.getEmailid()%>

										</div>
										

									
									</div>
									
										<div class="row row-m-15">
										<div class="col-md-4">
											<strong> Distributer ID: </strong>
											<%=agentCustDetails.getDistributerid()%>

										</div>
										
		<!-- ADD -->					<div class="col-md-4">
											<strong>Super Distributer ID: </strong>
											<%=walletPlan.getSuperdistributerid()!=null? walletPlan.getSuperdistributerid(): " - "%>

										</div>
										
										<div class="col-md-4">
											<strong> Aggregator ID: </strong>
											<%=agentCustDetails.getAggreatorid()%>

										</div>
										
										
							<!-- remove	1 -->	<%-- <div class="col-md-4">
											<strong>Distributer Name </strong>
											<%=agentCustDetails.getDistributername()%>

										</div>
										 --%>
										
										
											<%-- <strong>KYC Status: </strong>
											<%
											if(agentCustDetails.getKycstatus()!=null&&agentCustDetails.getKycstatus().equalsIgnoreCase("1")){
											%>
											FULL KYC
											<%	
											}else{
											%>
											MIN KYC
											<%
											}
											%> --%>
											
									<%-- 		<div class="col-md-4">
											<strong> SO Name: </strong>
											<%= agentCustDetails.getSoname() !=null ? agentCustDetails.getSoname() :"-"%>
										    </div>
									 --%>
									</div>
									
							<%-- <div class="row row-m-15">
										<div class="col-md-4">
											<strong> PAN: </strong>
											<%=agentCustDetails.getPan()%>

										</div>

										<div class="col-md-4">
											<strong> Address Proof  </strong>
											<%=agentCustDetails.getAddressprooftype()%>

										</div>
										<div class="col-md-4">
											<strong> <%=agentCustDetails.getAddressprooftype()%> </strong>
											<%=agentCustDetails.getAdhar()%>

								
									</div>
									</div> --%>			
		<div class="row row-m-15">
										<%-- <div class="col-md-4">
											<strong> Address : </strong>
											<%=agentCustDetails.getAddress1()%> <%=agentCustDetails.getAddress2()%>  

										</div> --%>
										
							<!-- 1 comment -->		<%-- 	<div class="col-md-4">
											<strong> Aggregator ID: </strong>
											<%=agentCustDetails.getAggreatorid()%>

										</div>
									 --%>	
									 
									 
										<div class="col-md-4">
											<strong> City-State: </strong>
											<%=agentCustDetails.getCity()%>-<%=agentCustDetails.getState()%> <%=agentCustDetails.getPin()%>

										</div>
										
										<div class="col-md-4">
											<strong> Shop Name: </strong>
											<%=agentCustDetails.getShopname()%>

										</div>

										<%-- <div class="col-md-4">
											<strong> Creation Date  </strong>
											<%=agentCustDetails.getCreationDate()%>

										</div> --%>
										
										<div class="col-md-4">
											<strong> SO Name: </strong>
											<%= agentCustDetails.getSoname() !=null ? agentCustDetails.getSoname() :"-"%>
										    </div>
									 
										
									</div>
									
								
										<div class="row row-m-15">
										<div class="col-md-4">
											<strong> Activation Date: </strong>
											<%=agentCustDetails.getApproveDate()%>

										</div>
										<div class="col-md-4">
											<strong> Wallet Balance: </strong>INR 
											<%=d.format(customerCareResponse.getWalletBalance())%>   

										</div>
										<div class="col-md-4">
											<%-- <strong> Bhartipay Cash: </strong>INR 
											<%=d.format(customerCareResponse.getOxyCashBalance())%> --%>
											<strong> Manager Name: </strong>
											<%=customerCareResponse.getAgentCustDetails().getManagername() !=null ? customerCareResponse.getAgentCustDetails().getManagername() :"-"%>
										</div>

										
									</div>
									<div class="row row-m-15">
										<div class="col-md-4">
											<strong> AEPS: </strong>
											<%= agentCustDetails.getAgentcode()!=null ? agentCustDetails.getAgentcode() : "<font color='red'>Not Active</font>"%>

										</div>
										<div class="col-md-4">
											<strong> BBPS: </strong>
											<%= agentCustDetails.getAsagentcode() != null ? agentCustDetails.getAsagentcode() : "<font color='red'>Not Active</font>" %>   

										</div>
										<div class="col-md-4">
											<%-- <strong> Bhartipay Cash: </strong>INR 
											<%=d.format(customerCareResponse.getOxyCashBalance())%> --%>
											<%-- <strong> SO Name: </strong>
											<%= agentCustDetails.getSoname() !=null ? agentCustDetails.getSoname() :"-"%>
											--%>
											
											<strong> PlanID: </strong>
											<%= walletPlan.getPlanId() != null ? walletPlan.getPlanId() : "-" %>   

	 							
											
											</div>

										
									</div>
											
											
			<!-- ------------Change By Mani on 16/12/2020-------------------- -->	
			
			                     <%if(!onboardStatus.getStatus().equalsIgnoreCase("A0001") && onboardStatus!=null){ %>
										
			                         <div class="row row-m-15">
									    <div class="col-md-4">
											<strong> Onboard Fino: </strong>
											<%= onboardStatus.getFinoonboardstatus()!=null ? onboardStatus.getFinoonboardstatus() : "-"%>

										</div>
										<div class="col-md-4">
											<strong> Onboard Yes: </strong>
											<%= onboardStatus.getYesonboardstatus() != null ? onboardStatus.getYesonboardstatus() : "-" %>   

										</div>
										<div class="col-md-4">
										 <strong> Onboard ICICI: </strong>
											<%= onboardStatus.getIcicionboardstatus() != null ? onboardStatus.getIcicionboardstatus() : "-" %>   
                                         </div>
                                       </div>
									
					              <%} %>												
											
											
											
											
													
																	
</div>

					


								</div>
<div class="box-content row searchTab" style="margin: auto 0px !important;">
  <ul class="nav nav-tabs" style="padding: 0 0 0 15px;">
  <li ><a data-toggle="tab" href="#walletHistory" onclick="walletHistory('<%=agentCustDetails.getId()%>')">Wallet History</a></li>
  <li><a data-toggle="tab" href="#recharge" onclick="rechargeHistory('<%=agentCustDetails.getId()%>')">Recharge</a></li>
  <%if(agentCustDetails!=null&&agentCustDetails.getUsertype()==1){ %>
  <li><a data-toggle="tab" href="#dmt" onclick="custdmtHistory('<%=agentCustDetails.getId()%>')">DMT</a></li>
  <%}else{ %>
  <li><a data-toggle="tab" href="#dmt" onclick="dmtHistory('<%=agentCustDetails.getId()%>')">DMT</a></li>
  <%} %>
  <li><a data-toggle="tab" href="#bbpsReport" onclick="GetCustBBPSReport('<%=agentCustDetails.getId()%>')">BBPS</a></li>
  <li><a data-toggle="tab" href="#aepsReport" onclick="GetCustAepsReport('<%=agentCustDetails.getId()%>')">AEPS</a></li>
  <li><a data-toggle="tab" href="#matmReport" onclick="GetCustMATMReport('<%=agentCustDetails.getId()%>')">m-ATM</a></li>
</ul>

<div class="tab-content">
  <div id="walletHistory" class="tab-pane fade in active">
    
  </div>
  <div id="oxyHistory" class="tab-pane fade">
   
  </div>
  <div id="recharge" class="tab-pane fade">
    
  </div>
  
   <div id="dmt" class="tab-pane fade">
    
  </div>
  <div id="aepsReport" class="tab-pane fade">
    
  </div>
  <div id="bbpsReport" class="tab-pane fade">
    
  </div>
  <div id="matmReport" class="tab-pane fade">
    
  </div>
</div>
								
								</div>
<%} %> 
							</div>
	</div>	
							
		
 
 
 

 
        
        

   		    </div>
        </div>
	</div> 
</div>      

       
 

<jsp:include page="footer.jsp"></jsp:include>

<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>


</body>
</html>


