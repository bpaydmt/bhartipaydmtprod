<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>


  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 


</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");
DecimalFormat d=new DecimalFormat("0.00");

%>
      
       




<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
		
		<div class="box-content row">
		
			<form  id="walletHistoryForm">
								
										
											
								<!-- <input type="hidden" value="%{inputBean.userId}" name="inputBean.userId">	 -->	
								
								<input	type="hidden" name="inputBean.userId" id="userid" value="<s:property value='%{inputBean.userId}'/>"/>	

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
																	
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="dpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Txn Date</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Description</u></th>
						<th><u>Debit</u></th>
						<th><u>Credit</u></th>
						<th><u>Closing Balance</u></th>
						
												
						
						
					</tr>
				</thead>
				<tbody>
				<%
							List<PassbookBean> pb=(List<PassbookBean>)request.getAttribute("listResult");

				if(pb!=null){
				
				
				for(int i=0; i<pb.size(); i++) {
					PassbookBean tdo=pb.get(i);%>
		          		  <tr>
		         
		          	   <td><%=tdo.getTxndate()%></td>
		         
		         <%-- <%if (user.getUsertype()==2 ){%>
		         	 <td><%=tdo.getResptxnid()%></td>
		         <%}else{ %>
		             <td><%=tdo.getTxnid()%></td>
		         
		         <%} %> --%>
		            <td><%=tdo.getResptxnid()%></td>
		            <td><%=tdo.getTxndesc()%><%if(tdo.getPayeedtl()!=null){%>-<%=tdo.getPayeedtl()%><%} %></td>
		            <td><%if(tdo.getTxndebit()==0){out.print("0.00");}else{out.print(d.format(tdo.getTxndebit()));}%></td>
		             <td><%if(tdo.getTxncredit()==0){out.print("0.00");}else{out.print(d.format(tdo.getTxncredit()));}%></td>
		             <td><%=d.format(Double.parseDouble(tdo.getClosingBal()!=null?tdo.getClosingBal():"0.0"))%></td>
		            
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>





<script>

$(function(){
	$("#walletHistoryForm").submit(function(e) {

	    

	    $.ajax({
	           type: "POST",
	           url: "CustomerPassbookById.action",
	           data: $("#walletHistoryForm").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	               $("#walletHistory").html(data);
	   		    $('#example').DataTable( {
			        dom: 'Bfrtip',
			        autoWidth: false,
			        order: [[ 0, "desc" ]],
			        buttons: [
			             {
			             
			                extend: 'copy',
			                text: 'COPY',
			                title:'Wallet History - ' + '<%= currentDate %>',
			                message:'<%= currentDate %>',
			            },  {
			             
			                extend: 'csv',
			                text: 'CSV',
			                title:'Wallet History - ' + '<%= currentDate %>',
			              
			            },{
			             
			                extend: 'excel',
			                text: 'EXCEL',
			                title:'Wallet History - ' + '<%= currentDate %>',
			            
			            }, {
			             
			                extend: 'pdf',
			                text: 'PDF',
			                title:'Wallet History - ' + '<%= currentDate %>',
			                message:" "+ "<%= currentDate %>" + "",
			               
			            },  {
			             
			                extend: 'print',
			                text: 'PRINT',
			                title:'Wallet History - ' + '<%= currentDate %>',
			              
			            }
			        ]
			    } );
			    
			    
			    $('#dpStart').datepicker({
			     language: 'en',
			     autoClose:true,
			     maxDate: new Date(),
			     

			    });
			 
			     

			     
			 
			    $("#dpStart").blur(function(){
			       $('#dpEnd').val("")
			     $('#dpEnd').datepicker({
			          language: 'en',
			         autoClose:true,
			         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
			         maxDate: new Date(),
			         
			        }); 
			    })
	               
	              
	           }
	         });

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});	
})



</script>



