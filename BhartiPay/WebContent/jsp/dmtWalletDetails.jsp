<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtUserDetails"%>
<%

DmtUserDetails userDetails=(DmtUserDetails)request.getAttribute("userDetail");
if(userDetails!=null&&userDetails.getHolderName()!=null&&userDetails.getCardNumber()!=null){
%>
<table class="table table-bordered table-striped">
<tr>
<td><strong>Holder Name</strong></td><td><%=userDetails.getHolderName() %></td>

</tr>
<tr>
<td><strong>Wallet Id</strong></td><td><%=userDetails.getCardNumber() %></td>
</tr>
<tr>
<td><strong>Fund Available</strong></td><td><%=userDetails.getFundAvailable() %></td>
</tr>
<tr>
<td><strong>Fund With Holding</strong></td><td><%=userDetails.getFundWithholding()%></td>
</tr>
<tr>
<td><strong>Status</strong></td><td><%=userDetails.getStatus()%></td>
</tr>

</table>
<%
}else{
%>
Detail not found.
<%}%>