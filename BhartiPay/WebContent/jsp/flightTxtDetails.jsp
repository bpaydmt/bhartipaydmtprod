<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.List" %>
<%@page import="com.bhartipay.wallet.report.bean.TravelTxn" %>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {

	     try{
	    
    $('#flight-txn').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Flight Transaction - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Flight Transaction - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Flight Transaction - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Flight Transaction - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Flight Transaction - ' + '<%= currentDate %>',
              
            }
        ]
    } );
   
  
	     }catch(err){
	    	 console.log(err)
	     }
	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   
 </script>
 

</head>


      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Flight Booking History</h2>
		</div>
		
		<div class="box-content row">
		
			<form action="getFlightDetail" method="post">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="inputBean.walletId" id="userid"
										value=""> <input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="dpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							


 <div class="table-responsive col-md-12">          
  <table class="table" id="flight-txn">
    <thead>
      <tr>
   <!--       <th>Transaction Details</th> -->
    	<th>Booking ID</th>
      <th>Date</th>
           <th>Journey Details</th>
      
        <th>Status</th>
        <th>Amount</th>

       <!--  <th>Cancellation-Status</th> -->
         <!-- 
          <th>Download Ticket</th> -->
         <th>Ticket Details</th>
        
      </tr>
    </thead>
    <tbody><tr>
				<%
							List<TravelTxn> txn=(List<TravelTxn>)request.getAttribute("listResult");

				if(txn!=null){
				
				
				for(int i=0; i<txn.size(); i++) {
					String cancellationStatus="NA";
					StringBuffer str = new StringBuffer();
					double amount = 0.0;
					
					TravelTxn tdo=txn.get(i);
					if(tdo.getType().equals("Flight"))
						{
							String[] strToken =  tdo.getDetails().split("#");
			         		for(int itr=0;itr<strToken.length;itr++)
			         		{
			         			//String[] strToken2 = strToken1[itr].split("|");
			         			StringTokenizer strToken2 = new StringTokenizer(strToken[itr],"|");
			         			strToken2.nextToken();strToken2.nextToken();strToken2.nextToken();strToken2.nextToken();
			         			strToken2.nextToken();strToken2.nextToken();
			         		    str.append(strToken2.nextToken()+"|");
			         		
			         		}
			         		cancellationStatus = str.toString();
			         		cancellationStatus = cancellationStatus.substring(0, cancellationStatus.length()-1);
							if( cancellationStatus.contains("TicketBooked")){%>
				          		 
			          		 
			          	<td><%=tdo.getTransid()%></td>
			          	<td>
			          	<%=df.format(tdo.getTxndate()) %>	          			     
			          			</td>
			          			<td>
			         		
			         	<%
			         		String[] strToken1 =  tdo.getDetails().split("#");
			         		for(int itr=0;itr<strToken1.length;itr++)
			         		{
			         			//String[] strToken2 = strToken1[itr].split("|");
			         			StringTokenizer strToken2 = new StringTokenizer(strToken1[itr],"|");
			         			strToken2.nextToken();
			         			String[] strToken3 = strToken2.nextToken().split(",");
			         			String[] strToken4 = strToken2.nextToken().split(",");
			         			amount += Double.parseDouble(strToken2.nextToken());
			         		 %>
			         		 <div style="margin:0 0 5px 0">
			         		 	<%=strToken3[1]+" - "+strToken4[1]%>
			         		 	</div>
			         		 <%
			         		}
			         	%>
			         		
			         		
			         	</td>
			         		
			         		<td  ><span class="<% if(tdo.getStatus().equalsIgnoreCase("Pending")){%> txtPending <% }
			         			else if(tdo.getStatus().equalsIgnoreCase("Success")){%> txtSuccess <%}
			         			else{ %> txtFailed <%} %>"> <%if(cancellationStatus.contains("TicketBooked")){ if(tdo.getCancellationstatus() == null){ %>Ticket Booked<%}else{ %><%=tdo.getCancellationstatus()%><%} }%></span></td>
			         		<%-- <td><%if(tdo.getCancellationstatus() == null){ %>NA<%}else{ %><%=tdo.getCancellationstatus()%><%} %></td> --%>
			         		<td>Rs. <%=/* tdo.getAmount() */ amount%></td>
			    <td>
			    <form action="showFlightDtl">    
			    
			    <input type="hidden" name="details" value="<%=tdo.getDetails()%>">
			        <input type="hidden" name="transactionId" value="<%=tdo.getTransid()%>">
			    <input type="submit" class="grid-btn2 btn" style="width: 110px;" value="View Details">
			     </form>
			    </td>
			          
	                  		  </tr>
				      <%}}}}%>	

			        </tbody>
  </table>
  </div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



