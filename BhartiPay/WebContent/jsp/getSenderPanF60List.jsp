<%@page import="com.bhartipay.wallet.user.persistence.vo.MudraSenderPanDetails"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Sender PAN List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Sender PAN List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Sender PAN List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Sender PAN List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Sender PAN List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
   
  

	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->
<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Sender PAN Request</h2>
		</div>
		
		<div class="box-content  ">
		
			<form action="GetSenderPanF60List" method="post">
  <div class="row">

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="mudraSenderPanDetails.aggreatorid" id="userid"
										value="<%=user.getAggreatorid()%>"> <input type="text"
										value="<s:property value='%{mudraSenderPanDetails.stDate}'/>"
										name="mudraSenderPanDetails.stDate" id="dpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{mudraSenderPanDetails.endDate}'/>"
													name="mudraSenderPanDetails.endDate" id="dpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
		<font color="red"><s:actionerror/> </font>
		<font color="blue"><s:actionmessage/> </font>
		<s:fielderror></s:fielderror>
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Sender ID</u></th>
						<th><u>Mobile No</u></th>
						<th><u>Sender Name</u></th>
						<th><u>Proof Type</u></th>
						<th><u>PAN Number</u></th>
						<th><u>Date</u></th>
						<th><u>Proof Pic</u></th>
						<th><u>Status</u></th>
						<th><u>Action</u></th>
						
						
												
						
						
					</tr>
				</thead>
				<tbody>
				<%
				List <MudraSenderPanDetails> list=(List<MudraSenderPanDetails>)request.getAttribute("senderPanF60List");

				if(list!=null){
				
				
				for(int i=0; i<list.size(); i++) {
					MudraSenderPanDetails tdo=list.get(i);%>
		          		  <tr>
		         
		       	  
		       	   <td><%=tdo.getSenderId()%></td>
		       	   <td><%=tdo.getMobileNo()%></td>
		       	   <td><%=tdo.getSenderName()%></td>
		       	   <td><%=tdo.getProofType()%></td>
		       	   <td><%=tdo.getPanNumber()%></td>
		       	   <td><%=tdo.getApproveDate()%></td>
		       	  <td class="image-td"> <a href="<%=tdo.getProofTypePic()%>" download><img style="cursor: pointer;" alt="userImg" src="<%=tdo.getProofTypePic()%>" width="40px" height="40px" data-toggle="modal" /></a></td>
		       	    <td>
		       	    
		       	    <%if(tdo.getStatus()!=null&&tdo.getStatus().equalsIgnoreCase("ACCEPT")){
		       	    out.print("ACCEPTED");
		       	    }else if(tdo.getStatus()!=null&&tdo.getStatus().equalsIgnoreCase("REJECT")){
		       	    	out.print("REJECTED");
		       	    }else if(tdo.getStatus()!=null&&tdo.getStatus().equalsIgnoreCase("Pending")){
		       	    	out.print("PENDING");
		       	    }%>
		       	    </td>
		       	    <td>
		       	    <%if(tdo.getStatus()!=null&&tdo.getStatus().equalsIgnoreCase("Pending")){ %>
		       	    <button id="<%=tdo.getId()%>acceptDoc" onclick="approveAction('<%=tdo.getId()%>','ACCEPT')" data-toggle="modal" data-target="#approveActionPop" class="grid-small-btn">Accept</button>
		       	    
		       	    	<button id="<%=tdo.getId()%>rejectDoc" onclick="approveAction('<%=tdo.getId()%>','REJECT')" data-toggle="modal" data-target="#approveActionPop" class="grid-small-btn">Reject</button>
		       	   <%} %>
		       	    </td>
		    	  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	 

 
        
        

        <!-- contents ends -->

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<div id="approveActionPop" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
        <form id="pan-action" method="post" action="AcceptedRejectedSenderPanF60" >
        <div class="form-group">
            <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
        	<input name="mudraSenderPanDetails.id" id="hidden-id" value="" type="hidden">
        	<input name="mudraSenderPanDetails.status" id="hidden-status" value="" type="hidden">
        	<input placeholder="Remark" name="mudraSenderPanDetails.comment" id="remoark-inp" class="form-control mandatory"  type="text"><span class="errorMsg"></span>
         	
        </div>
        
        <div class="form-group">
        	 <input placeholder="Sender Name" name="mudraSenderPanDetails.senderName" id="sender-inp"  class="form-control userName mandatory"  type="text" ><span class="errorMsg"></span>
        </div>
        
       
        
        
        </form>
        <input value="submit" class="btn btn-info btn-fill btn-block" type="Button"  onclick="submitVaForm('#pan-action',this)">
      </div>
   
    </div>

  </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>

<script>

function approveAction(reqId,action){
	if(action=="ACCEPT"){ 
		$("#sender-inp").show().addClass('mandatory');
}else{ 
		$("#sender-inp").hide().removeClass('mandatory');
		}
	
	$("#remoark-inp").val("")
	//$("#approveActionPop").model('show')
	$("#hidden-id").val(reqId)
	$("#hidden-status").val(action)
}
</script>

