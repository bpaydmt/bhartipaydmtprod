<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
<script src="js/handlebars-v4.0.10.js" type="text/javascript"></script>



<script type="text/javascript">

$(function(){
	$("#agent-mobile").blur(function(){
		setTimeout(function(){
			getAgentDetails()
		},100)
	})
})
	function getAgentDetails(){
		var agentMobile = $("#agent-mobile").val();
		
		if(agentMobile.length == 10 && $("#agent-mobile").hasClass("valid")){
			$.ajax({
				method:'post',
				url:'GetAgnDtlByUserId',
				data:'cmeBean.mobileNo='+agentMobile,
				success:function(data){
					console.log(data)
					//console.log(data.agentDetail.id)
					if(data.agentDetail  != undefined){  
					s
						$("#agentErrMobile").hide();
					$("#agent-id").val(data.agentDetail.id)
					//$("#agent-id").val(data.agentDetail.id)
					
					var theTemplateScript = $("#agnentDetail").html();
					
					var theTemplate = Handlebars.compile(theTemplateScript);	
					var theCompiledHtml = theTemplate(data.agentDetail);
					$("#agentDetailsTable").html(theCompiledHtml)
					}else{
						
						$("#agentErrMobile").show().text(data.msg)
					}
				}
			})
		}else{
			//alert(2)
		}
	}
function getAgentDetailsconfirm(){
		if($("#agent-mobile").val().length == 0 || $("#agent-id").val().length == 0 || $("#amount-inp").val().length == 0){
			
			
			alert("All field are required please fill the all fields.")
			
		}else{
			if( parseInt($("#amount-inp").val()) > 200000){
				alert("Amount can't be more the 2,00,000")
				
			}else{
				var output =  $("#amount-inp").val()
				
				if(output.lastIndexOf(".") == -1){
					output = output+ ".00"
				}else{
					
					if(output.length - output.lastIndexOf(".") == 1){
						output = output+ "00"
						
					}
					if(output.length - output.lastIndexOf(".") == 2){
						output = output+ "0"
						
					}
					//alert(output.length - output.lastIsndexOf("."))
				}
				$("#agent-mobile-td").text($("#agent-mobile").val());
				$("#agent-id-td").text($("#agent-id").val())
				$("#agent-amout-td").text( output);
				jQuery.noConflict(); 
				$('#agentDetials-popup').modal('show');
				
			}
			
		}
		
		

	
}
	
</script>
	<script id="agnentDetail" type="text/x-handlebars-template">
<h2>Agent Details</h2>
   
  <table class="table table-striped">
  
    <tbody>
  <tr>
        <td><strong>Agent ID</strong></td>
        <td>{{id}}</td>
     
      </tr>
      <tr>
        <td><strong>Name</strong></td>
        <td>{{name}}</td>
     
      </tr>
      <tr>
        <td><storng>Mobile</strong></td>
        <td>{{mobileno}}</td>
       
      </tr>
      <tr>
        <td><strong>Email ID</strong></td>
        <td>{{emailid}}</td>
           </tr>
<tr>
		<td><strong>Address</strong></td>
        <td>{{address1}} {{address2}}</td>
</tr>
<tr>
		<td><strong>City</strong></td>
        <td>{{city}} </td>
</tr>
<tr>
		<td><strong>State</strong></td>
        <td>{{state}} </td>
</tr>
<tr>
		<td><strong>Pincode</strong></td>
        <td>{{pin}} </td>
</tr>
    </tbody>
  </table>
</script>
</head>

<%
	User user = (User) session.getAttribute("User");
%>



<body>

<!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
<!-- topbar ends -->
  
<!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
<!-- left menu ends -->

 

	<div id="main" role="main"> 
	    <div id="content">  
			<div class=" row">  
			    <div class="col-md-12"> 
					<div class="box2">
						<div class="box-inner"> 
							<div class="box-header">
								<h2>CME to Agent Transfer</h2>
							</div>


							<div class="box-content">
								<font style="color: red;"> <s:actionerror />
								</font> <font style="color: blue;"> <s:actionmessage />
								</font>
								<form action="OpenMPinValidation" method="post" enctype="multipart/form-data" id="form_name">
								    <div class="row"> 
								        <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									 
										  
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										    <label>Agent Mobile No<font color="red"> *</font></label>
											<input type="text" class="mobile form-control mandatory" id="agent-mobile" name="cmeBean.mobileNo"/>
											<span style="color:red;display:none" id="agentErrMobile"></span>
										</div>
									    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										    <label>Agent ID<font color="red"> *</font></label>
											<input type="text" class="form-control mandatory" id="agent-id" name="cmeBean.agentId" readonly/>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										    <label>Amount <font color="red"> *</font></label>
											<input type="text" class="form-control onlyNum mandatory "  id="amount-inp" name="cmeBean.amount" maxlength="6"/>
										</div> 

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div id="agentDetailsTable">
											
											</div> 
										</div>

										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"> 
										</div>

                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											<input type="button" class="btn btn-success  btn-fill submit-form" value="Submit" value="submit" style="margin-bottom:15px;" onclick=" getAgentDetailsconfirm()"  /> 
											<input type="reset" style="margin-bottom:15px;" onclick="resetForm('#form_name')" class="btn btn-info btn-fill" />
									    </div>

									</div>
									 
						        </form>
							</div>
						</div>
					</div>  
			    </div>  

				<div id="agentDetials-popup" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">CME Money Transfer</h4>
				      </div>
				      <div class="modal-body">
				        <p>You are going to make a transfer with below details. Do you want to continue?</p>
				        	<table class="table">
				        		<tr>
				        			<td>
				        			<strong>Agent Mobile Number</strong>
				        			</td>
				        			<td id="agent-mobile-td">
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>
				        			<strong>Agent ID</strong>
				        			</td>
				        			<td id="agent-id-td">
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>
				        			<strong>Amount</strong>
				        			</td>
				        			<td id="agent-amout-td">
				        			</td>
				        		</tr>
				        	</table>
				        	<div class="button-wrapper">
				        		<button class="btn btn-default"  onclick="submitVaForm('#form_name')">ok</button>
				        		<button class="btn btn-default" data-dismiss="modal" >Cancel</button>
				        	
				        	</div>
				      </div>
				      
				    </div>

				  </div>
				</div> 
			</div>  
        </div>
    </div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->



	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="./newmis/js/charisma.js"></script>
	

	
</body>
</html>