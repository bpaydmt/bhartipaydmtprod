<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
	User user = (User) session.getAttribute("User");
%>


<jsp:include page="theams.jsp"></jsp:include>
<%@include file="reqFiles.jsp"%>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<link href='./css/rechargeUi.css' rel='stylesheet'>
<link href='./css/dd.css' rel='stylesheet'>
<jsp:include page="head.jsp"></jsp:include>
<script>
	function changeDTHPlaceholderAndLen(el, targetInp) {
		var elv = $(el).val()
		var tar = $(targetInp)
		var le = 12
		var ph = 'Enter your DTH number'
		switch (elv) {
		case 'AIRTEL DIGITAL TV':
			le = 10
			ph = 'Customer ID'
			tar.attr('Placeholder', 'Customer ID', 'maxlength', '10');
			break;
		case 'DISH TV':
			le = 11
			ph = 'Viewing Card Number'

			break;
		case 'RELIANCE DIGITAL TV':
			le = 12
			ph = 'Smart Card Number'

			break;
		case 'VIDEOCON D2H':
			le = 10
			ph = 'Customer ID'

			break;
		case 'TATA SKY':
			le = 10
			ph = 'Subscriber ID'

			break;
		case 'SUN DIRECT':
			le = 11
			ph = 'Smart Card Number'
			break;

		default:
			le = 12

		}
		tar.attr('Placeholder', ph)
		tar.attr('maxlength', le)
	}
	function resetTab(e) {
		e.preventDefault();

		$("input[type='text']").val('')
		$('select').val('')
		$(".errorMsg").hide()

		// Failing the above, you could use this, however the above is recommended
		return false;
	}
	$(
			function() {
				$(
						"#rechargeOperator, #circlei, #dth_operator, #datacard_operator, #datacard_circle")
						.msDropDown();
			})
</script>

<style>
.errorMsg {
	right: 12%;
	top: 57px;
}

.chosen-container {
	width: 90% !important;
}

.chosen-container-single .chosen-single {
	padding: 9px 0 8px 8px !important;
}

.chosen-container-active.chosen-with-drop .chosen-single div b {
	background-position: -18px 4px;
}

.col-7-width {
	/*border-top: none !important;
		border-left: none !important;
		border-right: none !important;
		border-bottom: 2px solid #ccc;*/
	border: 2px solid #ccc;
	background-color: transparent !important;
	color: #666 !important;
	border-radius: 25px !important;
	display: block !important;
	float: left !important;
	width: 100% !important;
	/*padding-top: 10px !important;*/
	/* padding-bottom: 10px !important;*/
}

.newTabButton {
	display: flex;
	justify-content: space-around;
	padding: 20px 0px;
}

.dd .ddTitle {
	border: 2px solid #ccc !important;
}

.newTabButtonWrapper {
	display: flex;
	justify-content: space-around;
	padding: 20px 0px;
}

.newTabButton {
	width: 10% !important;
}

.tab {
	display: block;
}

.tab:hover {
	color: #666 !important;
}

.dd .ddcommon .borderRadius {
	/*z-index: 1000 !important;*/
	
}

.bank-txt {
	margin-bottom: 10px;
	margin-top: 10px;
}

.dd {
	line-height: 10px;
	width: 100% !important;
}

.newRechargeUi .payplutus_tab_active a:hover, .newRechargeUi .payplutus_tab_active a,
	.newRechargeUi .payplutus_tab_active a:focus {
	/* position: relative;
        top: 11px;*/
	
}

.newRechargeUi .payplutus_tab a {
	/*position: relative;
        top: 11px;*/
	
}

.close {
	float: right !important;
	font-size: 16px;
	font-weight: bold;
	line-height: 1;
	color: #fff;
	text-shadow: none;
	opacity: 1;
	padding: 7px !important;
	background: #a57225 !important;
}

.newRechargeUi .payplutus_formshadow {
	border: 2px solid #d8d8d8 !important;
}

smart-style-3 input[type=text] {
	margin-top: 0px;
}

.popupBackgroundColor{
    position: fixed;
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    background-color: #00000042;
    display:none;
    z-index: 999;
}
.popupWrapper{
    position: fixed;
    z-index: 999;
    height: 100%;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
   display:none;
    justify-content: center;
    align-items: center;
    

}
.popupInnerWrapper{
    background-color: #fff;
    width: 40%;
    border-radius:3px;
    overflow:hidden;
    
}
.footer{
    float: left;
    width: 100%;
    padding: 10px;
    border-top: 1px solid #ccc;
}
.popupInnerWrapper button{
    float: right;
    padding: 10px;
    border-top: 1px solid #ccc;
    margin-top: 30px;
    margin-top:0px;
    text-align: right;
    width: 75px;
    text-align: center;
    height: 31px;
    padding: 0px;
    border-radius: 35px !important;
}
.popupInnerWrapper p{
    padding: 10px;
    font-size: 14px;
     padding: 10px 10px 0px 10px;
     margin-bottom:0px;
}
.popupInnerWrapper h6{
    padding: 10px;
    margin: 0px;
}
.header{
     float: left;
    width: 100%;
    padding: 10px;
    border-bottom: 1px solid #ccc;
    
}
.header h6{
float:left;
    padding: 0px;
        line-height: 25px;
    font-weight: 600;
}
.header button.close{
    width: 25px;
    height: 25px;
    padding: 0px !important;
    border-radius: 100% !important;
}
.popupInnerWrapper table{
    float: left;
    width: 94%;
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin-left: 3%;
    margin-top: 15px;
    margin-bottom: 15px;
}

.popupInnerWrapper table th,td{
    border: 1px solid #ccc;
    color: #000;
    padding: 6px;
}

.popupInnerWrapper table td{
padding-left:10px;
}

.popupInnerWrapper table  tr:nth-child(2n){
    background-color: #f9f9f9;
}
.err{
    display:none;
    color: #f05050;
    padding-left: 20px;
}

</style>

</head>
<script>
function closePopup(){
	document.getElementById('popupearapper').style.display = "none";
	document.getElementById('bgWrapper').style.display = "none";
}
function showPopup(){
   	    let recharge = document.getElementById('rechargeOperator').value;
		let circle = document.getElementById('circlei').value;
		let rechargeMobile = document.getElementById('rechargeMobile').value;
		let amount = document.getElementById('amount').value;
    if(recharge != -1 && circle != -1 && rechargeMobile && amount){
    	document.getElementById('popupearapper').style.display = "flex";
    	document.getElementById('bgWrapper').style.display = "block";
    	document.getElementById('popupoperater').innerHTML = recharge;
    	document.getElementById('popupcircle').innerHTML = circle;
    	document.getElementById('popupmobile').innerHTML = rechargeMobile;
    	 document.getElementById('popupamount').innerHTML = amount;
    }
    else{
    	document.getElementById('showerr').style.display = 'block';
    	setTimeout(function(){ document.getElementById('showerr').style.display = 'none'; }, 3000);
    }
}
</script>
<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->

	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<div id="main" role="main">
		<div id="content">
			<div class=" row">
				<div class="col-md-12">
					<div class="newRechargeUi no-chosen-drop">
						<h3 style="font-size: 18px">
							<font style="color: red;"> <%
 	if (session.getAttribute("rechargeError") != null) {
 %> <%=session.getAttribute("rechargeError")%> <%
 	session.removeAttribute("rechargeError");
 	}
 %>
							</font> <font style="color: green;"></font>
						</h3>
						<div
							class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_formshadow">
							<div class="loader"></div>


							<!-- Left Buttons Start -->
							<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 newTabButtonWrapper"  style="padding-left: 25px !important;justify-content: left;">
								<div class="col-7-width payplutus_tab_active newTabButton"
									id="mb" style="float: left;">
									<a href="#mobileRecharge" class="tab" onclick="resetTab(event)"
										id="mobile_recharge"> <span class="r-icon mobile"></span>
										Mobile
									</a>
								</div>
								<div class="popupBackgroundColor" id="bgWrapper"></div>
								<div class="popupWrapper" id="popupearapper">
								  <div class="popupInnerWrapper">
								   
								    <div class="col-md-12" style="padding:0px;">
								    <div class="header"><h6>Recharge Details</h6>
								      <button type="button" class="close" data-dismiss="modal" onclick="closePopup()">�</button></div>
								     
							           <table>
							              <tr>
							                  <th>Mobile number</th><td id="popupmobile"></td>
							              </tr>
							              <tr>
							                  <th>Amount</th><td id="popupamount"></td>
							              </tr>
							              <tr>
							                  <th>Operater</th><td id="popupoperater"></td>
							              </tr>
							              <tr>
							                  <th>Circle</th><td id="popupcircle"></td>
							              </tr>
							              
							           </table>
								      <div class="footer">
								       <button class="btn  btn-success" " name="proceed" id="proceed"
												data-open-popup="rechage" onclick="submitRechargeForm('#mobile_form',this)">Confirm</button>
								      </div>
								    </div>
								  </div>
								</div>
								<%--
								<div class="col-7-width payplutus_tab newTabButton" id="dh">
									<a href="#dthRecharge" onclick="resetTab(event)" class="tab"
										id="dth_recharge"><span class="r-icon dth"></span>DTH</a>
								</div>
								 <div class="col-7-width  payplutus_tab newTabButton" id="dc">
									<a href="#datacardRecharge" onclick="resetTab(event)"
										class="tab" id="datacard_recharge"><span
										class="r-icon datacard"></span>Data card</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="el">
									<a href="#electricityRecharge" onclick="resetTab(event)"
										class="tab" id="electricity_recharge"><span
										class="r-icon datacard"></span>Electricity</a>
								</div>
								<div class="col-7-width  payplutus_tab newTabButton" id="gr">
									<a href="#gasRecharge" onclick="resetTab(event)" class="tab"
										id="gas_recharge"><span class="r-icon datacard"></span>Gas</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="lb">
									<a href="#landlinebill" onclick="resetTab(event)" class="tab"
										id="landline_Recharge"><span class="r-icon datacard"></span>Landline</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="ir">
									<a href="#insuranceRecharge" onclick="resetTab(event)"
										class="tab" id="insurance_recharge"><span
										class="r-icon datacard"></span>Insurance</a>
								</div> --%>
							</div>
							<!-- Left Buttons End -->


							<!--  1st Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="mobile_recharge_form">
								<form method="post" id="mobile_form"
									action="PrepaidRechargeService.action">
									<!-- <div class="col-md-1"></div>-->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">


										<!-- Prepaid with radio button start -->

										<!-- <div class="col-md-6 bank-txt"> -->
											<input type="hidden" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type"
												value="PREPAID">
										<!-- </div> -->
										<!-- <div class="col-md-6 bank-txt">
											<input type="radio" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type"
												value="POSTPAID" onClick="hide_plan();"> <label
												class="">Postpaid</label>
										</div> -->
										<!--  End -->


										<!--  Enter your mobile number start -->
										<p class="err" id="showerr">Please fill all the field</p>
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber" 
												class="mandatory" placeholder="Enter your mobile number"
												autocomplete="off" id="rechargeMobile"
												 />
												 <!-- onkeyup="getOpretorDetails(this,'mobile')" -->
										</div>
										<!--  End -->

										<!-- Enter amount start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="amount"
												class="payplutus_textbox  form-control amount-vl mandatory"
												required placeholder="Enter amount" maxlength="4" />
											<div class=" customSelectErr" id="amountGreater10"
												style="display: none;">Amount should be greater than
												10.</div>

										</div>
										<!--  End -->

										<!-- dropdown Star -->

										<div class="col-md-6 bank-txt">
											<s:select list="%{prePaid}"
												name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;" cssClass="" id="rechargeOperator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);validateSelectOnChange(this,'#opratorErr');" />

											<div class="errorMsg customSelectErr" id="opratorErr"
												style="display: none;">Please select the operator</div>
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="circlei"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);validateSelectOnChange(this,'#opratoCireclrErr');" />

											<div class="errorMsg customSelectErr" id="opratoCireclrErr"
												style="display: none;">Please select the circle</div>
										</div>
										<!--  End -->



										<!-- Proceed to pay Submit Button start -->
										<div class="col-md-6 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												style="clear: left" value="Proceed to Pay"
												class="btn submit-form btn-success"
												data-open-popup="rechage"
												onclick="showPopup()">
												<!-- submitRechargeForm('#mobile_form',this) -->
										</div> 
										<!--  End -->

									</div>
									<!-- <div class="col-md-1"></div> -->
								</form>

							</div>
							<!-- 1st Body Contents End -->


							<!--  2nd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="d2h_recharge_form" style="display: none;">

								<form method="post" id="d2h" action="RechargeService.action">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">

										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>"> <input type="hidden"
											name="rechargeBean.rechargeType" value="DTH">



										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="dth_mobile_no" class="mandatory" required
												placeholder="Enter your DTH number" maxlength="12" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_mobile_noErr">Please Enter DTH Number</div>
										</div>

										<div>
											<input type="hidden" name="rechargeBean.rechargeCircle"
												value="Delhi">
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="dth_amount" class="mandatory" placeholder="Enter amount"
												maxlength="4" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErr">Please enter Amount</div>
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErrGreat10">Please Amount Greater than
												20.</div>
										</div>


										<div class="col-md-6 bank-txt">
											<s:select list="%{dth}" name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;"
												onchange="changeDTHPlaceholderAndLen(this,' #dth_mobile_no')"
												cssClass="mandatory" id="dth_operator" headerKey="-1"
												headerValue="Please select operator"
												onChange="change_operator(this);" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dthOpratorErr">Please select operator</div>
										</div>

										<div class="col-md-6 bank-txt" style="margin-top: 29px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" style="clear: left"
												class="btn submit-form btn-success" data-open-popup="DTH"
												onclick="submitVaForm('#d2h',this)">
										</div>

									</div>
								</form>

							</div>
							<!--  2nd Body Contents End -->


							<!-- Landline  3rd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="landline_recharge_form" style="display: none;">

								<form method="post" id="landline_bill"
									action="GetBillDetails.action"
									onsubmit="validateLandlineForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" name="billDetails.billerType"
											value="Landline"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">

										<!-- <div class="col-md-6 bank-txt"></div>
                                        <div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt">
											<select class=""
												onChange="landlinServiceProviderOnChange(this)"
												name="billDetails.billerId" id="landline_operator">
												<option value="-1">Select Operator</option>
											</select>
											<div class="errorMsg" id="landlineError"
												style="display: none;"></div>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="landlineNum" style="margin-top: 0px;"
												name="billDetails.number" placeholder="Enter Phone Number"
												maxlength="11" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left"
											id="landlineAmountWrapper">
											<input type="text" id="landlineAmount"
												style="margin-top: 0px;" maxlength="5"
												name="billDetails.amount" placeholder="Enter amount" />
										</div>

										<div class="form-group Bhartipay_mt10"
											id="landineAddInfoWrapper" style="display: none">
											<input type="text" name="rechargeBean.accountNumber"
												id="landline_add_info"
												class="payplutus_textbox onlyNum mandatory"
												name="billDetails.account"
												placeholder="Customer Account Number" maxlength="10" />
										</div>

										<div class="form-group Bhartipay_mt10" id="Authenticator"
											style="display: none">
											<select class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo">
												<option value="-1">Please Select Authenticator</option>
												<option value="LLI">LLI</option>
												<option value="LLC">LLC</option>
											</select>
											<div class="errorMsg" id="AuthenticatorError"
												style="display: none;"></div>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success" />
										</div>

									</div>
								</form>
							</div>
							<!--  3rd Body Contents End -->


							<!-- 4th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="datacard_recharge_form" style="display: none;">

								<form method="post" id="datacard"
									action="RechargeService.action">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">
										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>">


										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												id="connection_type" class="rechargetypeDataCard"
												value="Datacard" checked> <label class="">Prepaid</label>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												class="rechargetypeDataCard" id="connection_type"
												value="Postpaid-Datacard"> <label class="">Postpaid</label>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="datacard_mobile" class="" required
												placeholder="Enter your datacard number"
												onkeyup="getOpretorDetails(this,'datacard')" maxlength="10" />
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="datacard_amount" class="" required
												placeholder="Enter amount" maxlength="4" />
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{prePaidDataCard}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="datacard_operator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);" />
											</select>
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="datacard_circle"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);" />
										</div>

										<div class="col-md-6 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="datacard"
												onclick="submitVaForm('#datacard',this)">
										</div>

									</div>
								</form>

							</div>
							<!-- 4th Body Contents End -->


							<!-- 5th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="electricityRecharge_form" style="display: none;">

								<form method="post" id="electricityForm"
									action="GetBillDetails.action"
									onsubmit="electricityFormSubmit(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" name="billDetails.billerType"
											value="Electricity"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										<!--  <div class="col-md-6 bank-txt"></div>
                                        <div class="col-md-6 bank-txt"></div> -->
										<div class="col-md-6 bank-txt">
											<select class="payplutus_dropdown mandatory" id="eleOpretor"
												name="billDetails.billerId"
												onchange="electricityProviderChange()">
												<option value="-1">Select Operator</option>
											</select> <span class="errorMsg" id="oprErr" style="display: none;"></span>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="electricity_consumer"
												style="margin-top: 0px;" placeholder="Enter Consumer Number"
												name="billDetails.number" />
										</div>

										<div class="form-group Bhartipay_mt10" id="eleAccount"
											style="display: none;">
											<input type="text" id="electricity_account"
												class="payplutus_textbox onlyNum "
												placeholder="Enter Consumer Number"
												name="billDetails.account" />
										</div>

										<div class="form-group Bhartipay_mt10" id="elethirdOption"
											style="display: none;">
											<select id="thirdEleOption"
												class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo"></select>
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="Submit" name="proceed" st id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="electricity">
										</div>

									</div>
								</form>
							</div>
							<!-- 5th Body Contents End -->


							<!-- 6th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="gasRecharge_form" style="display: none;">

								<form method="post" id="gasRecharge"
									action="GetBillDetails.action"
									onsubmit="validateGasForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11">
										<input type="hidden" name="billDetails.billerType" value="Gas">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">

										<div class="col-md-6 bank-txt">
											<select class="" id="gas-opretor" style=""
												name="billDetails.billerId"
												onchange="gasOpratorChange(this)">
												<option value="-1">Select Operator</option>
											</select> <span id="gas-provider" class="errorMsg"
												style="display: none;"> Please Select The Service
												Provider. </span>
										</div>
										<!-- <div class="col-md-6 bank-txt"></div>  -->

										<div class="col-md-6 bank-txt">
											<input type="text" id="gas-consumer"
												name="billDetails.number" style="margin-top: 0px;"
												placeholder="Enter Operator Number" />
										</div>

										<div class="form-group Bhartipay_mt10" id="accoundBill"
											style="display: none">
											<input type="text" id="accoundBillInp"
												name="billDetails.account"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Operator Number" maxlength="8" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="gas">
										</div>

									</div>
								</form>
							</div>
							<!-- 6th Body Contents End -->


							<!-- 7th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="insurance_form" style="display: none;">

								<form method="post" id="insuranceRecharge"
									action="GetBillDetails.action"
									onsubmit="validateInsuranceForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" name="billDetails.billerType"
											value="Insurance"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<!-- <div class="col-md-6 bank-txt"></div>
										<div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt">
											<select class="" name="billDetails.billerId" id="insurance"
												onchange="changeInsurar()">
												<option value="-1">Please Select Operator</option>
											</select> <span id="insurance-error" class="errorMsg"></span>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="policy_number"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Policy Number" style="margin-top: 0px;"
												name="billDetails.number" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="text" id="insurer_dob"
												class="payplutus_textbox datepicker-here" placeholder="DOB"
												data-language='en' stylr="margin-top:0px"
												name="billDetails.account" readonly />
										</div>

										<div class="col-md-6 bank-txt">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="insurar" />
										</div>

									</div>
								</form>
							</div>
							<!-- 7th Body Contents End -->


						</div>
					</div>

					<div class="modal fade" id="rechagePop" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;" style="color:#111;">Recharge
														Type</th>
													<th id="rechargetype" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mobile Number</td>
													<td id="mobileNo"></td>
												</tr>
												<tr>
													<td>Service Provider</td>
													<td id="servicePovider"></td>
												</tr>
												<tr>
													<td>Circle</td>
													<td id="rechargecircle"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('mobile_form').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="electicityPop" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Electricity Bill Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Service Provider</td>
													<td id="electicityServicePovider"></td>

												</tr>
												<tr>
													<td>Consumer Number</td>
													<td id="consumerNumber"></td>

												</tr>
												<tr>
													<td>Bill Amount</td>
													<td id="electicityrechargeAmount"></td>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('electricityForm').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="gasPopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Gas Bill Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Service Provider</td>
													<td id="gasServicePovider"></td>
												</tr>
												<tr>
													<td>Consumer Number</td>
													<td id="gasConsumerNumber"></td>
												</tr>
												<tr>
													<td>Bill Amount</td>
													<td id="gasAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('gasRecharge').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="insurarPopUp" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Insurance Renew Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Insurer</td>
													<td id="insuranceServicePovider"></td>
												</tr>
												<tr>
													<td>Insurance Policy Number</td>
													<td id="insuranceConsumerNumber"></td>
												</tr>
												<tr>
													<td>Policy Renew Amount</td>
													<td id="insuranceAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('electricityForm').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="DTPpopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;">Service Provider</th>
													<th id="servicePoviderDTH" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>DTH Number</td>
													<td id="dthNo"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmountDTH"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('d2h').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="dataCardPopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;">Recharge Type</th>
													<th id="rechargetypeDataCard" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mobile Number</td>
													<td id="mobileNoDataCard"></td>
												</tr>
												<tr>
													<td>Service Provider</td>
													<td id="servicePoviderDataCard"></td>
												</tr>
												<tr>
													<td>Circle</td>
													<td id="rechargecircleDataCard"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmountDataCard"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('datacard').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!--/.fluid-container-->

	<!-- Biller Popup -->
	<div class="modal fade" id="billerPopup" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>

						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />


					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary btn-info"
						id="proceedBiller" style="display: none"
						onclick="billerConfirm('null','null','null','${csrfPreventionSalt}')">Proceed</button>
				</div>
			</div>

		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>


	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>

	<script src="./js/ie10-viewport-bug-workaround.js"></script>
	<script src="./js/payplutus.js<%=version%>" type="text/javascript"></script>


	<script src="./js/jquery.dd.min.js"></script>
</body>
</html>



