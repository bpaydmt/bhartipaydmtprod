<%@page import="com.bhartipay.wallet.aeps.AEPSLedger"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 1, "asc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'AEPS Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'AEPS Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'AEPS Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'AEPS Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'AEPS Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
  

    $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),


    });
    if($('#dpStart').val().length != 0){
    	var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
    	$('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
    	
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   
    
} );



	function converDateToJsFormat(date) {
	   
		var sDay = date.slice(0,2);
		var sMonth = date.slice(3,6);
		var yYear = date.slice(7,date.length)
	
		return sDay + " " +sMonth+ " " + yYear;
	}

 </script>
 <style>
#agentFooter, .paymentref{display:none}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>AEPS Report</h2>
		</div>
		
		<div class="box-content row">
		
			<form action="GetAepsReport" method="post" autocomplete="off">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="walletBean.id" id="id"
										value="<%=user.getSubAggregatorId()%>"> 
										
										<input type="text"
										value="<s:property value='%{walletBean.stDate}'/>"
										name="walletBean.stDate" id="dpStart"
										class="form-control" value=""  placeholder="Start Date"
										data-language="en"/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="dpEnd"
													class="form-control " placeholder="End Date"
													data-language="en" >
											</div>
											
											<%
											if( user.getUsertype() !=2 && user.getWhiteLabel()==0){
											%>
												<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br>
									<s:select list="%{aggrigatorList}" headerKey="All"
												headerValue="All" id="aggregatorid"
												name="walletBean.aggreatorid" cssClass="form-username"
												requiredLabel="true"/>
											</div>
										<%} %>		
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		<%
		List<AEPSLedger>list=(List<AEPSLedger>)request.getAttribute("agentList");
		int txnCount= 0;
		double txnAmount=0;
		
		for(int i=0; i<list.size(); i++) {
			AEPSLedger tdo=list.get(i);
			
			if(tdo.getPaymentStatus() !=  null && tdo.getPaymentStatus().equalsIgnoreCase("SUCCESS")){
				txnAmount=txnAmount+tdo.getAmount();
				txnCount=txnCount + 1;
				
				
			}
			
			
			/* serviceCharge=serviceCharge+tdo.gets(); */
		}	
		
		
		
		
		%>
	
		<div id="xyz">
				<table id="examplea" class="table table-bordered table-striped">
				<thead>
				
				
					<tr>
						<th style="text-align: center;">Total Successful Txn Count</th> 
						<th style="text-align: center;"><u>Total Successful Txn Amount</th>
						<!-- <th style="text-align: center;">Service Charge</th>
						<th style="text-align: center;"><u>GST</th>
						<th style="text-align: center;">Total Service Charge</th> -->
						
						
					</tr>
				</thead>
				<tbody>
		
		<tr>
					<td style="text-align: center;"><%=d.format(txnCount)%></td>
		          	     <td style="text-align: center;"><%=d.format(txnAmount)%></td>
		             
		            
                  	</tr>
			     
			        </tbody>		</table>
		</div>

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th width="5%" style="word-wrap: break-word;"><u>Txn Date</u></th>
						<%if(user.getUsertype() != 2){ %>
						<th width="5%" style="word-wrap: break-word;"><u>Agent Id</u></th>
						<%} %>
						<th width="5%" style="word-wrap: break-word;"><u>Txn Type</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Amount</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Txn Id</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Status</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Order Id</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Order Status</u></th>	
						<th width="5%" style="word-wrap: break-word;"><u>Payment Status</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Request id</u></th>
						<th width="5%" style="word-wrap: break-word;"><u>Stan</u></th>
						<th width="5%"><u>RRN</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;" ><u>Bankauth</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;" ><u>Processing Code</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Bank Response Code</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Bank Response Msg</th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Aadhar no</u></th>
						<th width="5%" style="word-wrap: break-word;display:none;"><u>Status Msg</u></th>

							
					</tr>
				</thead>
				<tbody>
				<%
				/* List<AEPSLedger> list=(List<AEPSLedger>)request.getAttribute("agentList"); */

				if(list!=null){
				
					int j=1;
				for(int i=0; i<list.size(); i++) {
					AEPSLedger tdo=list.get(i);
					
					%>

		          	 <tr>
		          	 <td ><%=tdo.getTxnDate()%></td>
		          	 <%if(user.getUsertype() != 2){ %>
		             <td><%=tdo.getAgentId()%></td>
		             <%} %>
		             <td><%=tdo.getTxnType()!=null?tdo.getTxnType():"-"%></td>
		             <td><%=tdo.getAmount()!=0?tdo.getAmount():"-"%></td>
		             <td><%=tdo.getTxnId()!=null?tdo.getTxnId():"-"%></td>
		             <td><%=tdo.getStatus()!=null?tdo.getStatus():"-"%></td>
		              <td><%=tdo.getOrderId()!=null?tdo.getOrderId():"-"%></td>
		             <td><%=tdo.getOrderStatus()!=null?tdo.getOrderStatus():"-"%></td>
		             <td><%=tdo.getPaymentStatus()!=null?tdo.getPaymentStatus():"-"%></td>
		             <td><%=tdo.getRequestId()!=null?tdo.getRequestId():"-"%></td>
		             <td><%=tdo.getStan()!=null?tdo.getStan():"-"%></td>
		             <td><%=tdo.getRrn()!=null?tdo.getRrn():"-"%></td>
		              
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankAuth()!=null?tdo.getBankAuth():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getProcessingCode()!=null?tdo.getProcessingCode():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankResponseCode()!=null?tdo.getBankResponseCode():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankResponseMsg()!=null?tdo.getBankResponseMsg():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getAadharNumber()!=null?tdo.getAadharNumber():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getStatusMessage()!=null?tdo.getStatusMessage():"-"%></td>

                  	</tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>


</body>
</html>



