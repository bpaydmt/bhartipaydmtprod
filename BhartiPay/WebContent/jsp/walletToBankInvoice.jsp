<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>
<%
WalletToBankTxnMast wtb=(WalletToBankTxnMast)session.getAttribute("wallettobank");

%>
    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->

<div class="row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            
            <div class="box-content row">
              
              
              
              <header class="clearfix">
      <div id="logo">
         </div>
      <div id="company">
        <h2 class="name"><span class="blue">Bhartipay</span></h2>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Transaction Receipt</div>
          <h2 class="name">Order No. <%=wtb.getWtbtxnid() %></h2>
          <%if(wtb.getBankrefNo()!=null){ %><div class="address">Bank Ref no.<%=wtb.getBankrefNo()%></div><%} %>
        <%--   <div class="email"><%=rechargeBean.getRechargeOperator() %>-<%=rechargeBean.getRechargeType() %></a></div> --%>
        </div>
        <div id="invoice">
          <h1>INVOICE:</h1>
          <%
          Date dNow = new Date();
          SimpleDateFormat ft=new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
          %>
          <div class="date">Date of Invoice: <%=ft.format(dNow) %></div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
             <th class="no">STATUS</th>
            <th class="desc">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td class="desc"><h3>Money transfer in a/c-<%=wtb.getAccno() %> </td>
            <td class="no"><%=wtb.getStatus() %></td>
            <td class="desc"><%=wtb.getAmount() %></td>
          </tr>
          </tbody>
        
      </table>
      <div id="thanks">Thank you!</div>
      
    </main>
    <footer>
     
    </footer>

    
            </div>
        </div>
  
</div>
</div>
</div>
</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>