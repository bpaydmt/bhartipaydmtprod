<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="com.bhartipay.wallet.report.bean.ResponseBean" %>
<%@page import="com.bhartipay.wallet.report.bean.RefundAgent" %>

<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
  
 
 
<% 
	 Date d1 = new Date();
	 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
	 DecimalFormat dformat = new DecimalFormat("###.00");
	 String currentDate = df.format(d1);
	 List<RefundAgent> refundList = new ArrayList<RefundAgent>();
	 ResponseBean refundAgent = null;
	 if(request.getAttribute("refundListResp") != null )
	 {
		 refundAgent = (ResponseBean)request.getAttribute("refundListResp");
	     refundList = refundAgent.getRefundList();
	 }
 %>
 
<script type="text/javascript">
   $(document).ready(function() {    
    $('#table').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wallet History - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
        ]
        } );    
	    
	} ); 
   
 </script>
 
<style type="text/css">
	.box-content {
	padding: 10px;
	min-height: auto;
	}
	.errorMessage li {
	padding-right: 20px;
	}
</style>
  
</head>

<% 
User user = (User) session.getAttribute("User");
%>
      
       

<body>

    <!-- topbar starts -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
        
    <!-- left menu starts -->
    <jsp:include page="mainMenu.jsp"></jsp:include>
    <!-- left menu ends -->

    <!-- contents starts -->
    <div id="main" role="main"> 
        <div id="content">       
            <div class="row">  

                <div class=" col-md-12">  
					<div class="box2 col-md-12"> 
						<div class="box-inner"> 
							<div class="box-header">
								<h2>Refund Transaction </h2>
							</div>
							
							<div class="box-content">
								<h5 style="margin:0px;">Please provide sender mobile no. and Transaction Id</h5>
								<form action="refundAgent" method="post">
									<div class="row"> 
							            <font color="red"><s:actionerror/></font>
										<font color="blue"><s:actionmessage/> </font>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
											<br>
											<input type="text" value="" name="senderMobile"  style="margin-left:0px;margin-top:0px;" class="form-control " placeholder="Sender Mobile Number" " required/>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										    <br> 
										    <input class="form-control" name="txnId" type="text" style="margin-left:0px;margin-top:0px;" placeholder="Transaction Id" "  required>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											<div  id="wwctrl_submit">
												<input class="btn btn-success submit-form" style="margin-top: 20px;" id="submit" type="submit" value="Submit">
												<input class="btn btn-primary btn-fill" style="margin-top: 20px;"  id="reset" type="reset" value="Reset">
											</div>
										</div>
									</div>
								</form> 
							</div>

						</div>

					</div>	 
                    
                    <div class="col-md-12">
						<div id="xyz">
							<table id="table" class="scrollD cell-border dataTable">
								<thead> 
									<tr>
										<th><u>Id</u></th>
										<th><u>MobileNo</u></th>
										<th><u>Transaction Type</u></th>
										<th><u>Txn Amount</u></th>
										<th><u>Refund Amount</u></th>
										<th>
											<tdo!=null&&tdo.getOtp()!=null&&tdo.getOtp().equalsIgnoreCase("0") u>Txn Status</u>
										</th>
										<th><u>Refund</u></th> 	 	
									</tr>
								</thead>
								<tbody>
								    <% 
								    if(refundList !=null && refundList.size() > 0){ 
									RefundAgent tdo=refundList.get(0);%>
						          	<tr> 
						          	    <td><%=tdo.getId()%></td> 
						         	    <td><%=tdo.getMobileno()%></td> 
										<td><%=tdo.getNarrartion()%></td>
										<td><%=dformat.format(Double.parseDouble(tdo.getTxnamount()))%></td>
						                <td><%=dformat.format(Double.parseDouble(tdo.getDramount()))%></td>
						                <td><%=tdo.getStatus()%></td>
						                <td>
								       	    <%if(tdo.getStatus().equals("Confirmation Awaited")){ %> 
								       	    <button class="btn btn-danger" style="background:red!important;padding: 5px 18px"disabled="disabled">Refund</button>
								       	    <%}else if(tdo.getStatus().equals("FAILED")) {%>
								       		<% if( tdo!=null&&tdo.getOtp()!=null){%>
								       		<button class="btn btn-info" onclick="verifyByOtp('<%=tdo.getMobileno() %>','<%=tdo.getId() %>','<%=tdo.getOtp()%>')" style="background:green!important;padding: 5px 18px"  >Refund</button>  
								          	<%} %>  
								          	<%} %>
						                </td>
					              	</tr>
							        <%} %>	
							    </tbody>		
							</table>
						</div> 
                    </div> 
				</div>

 


			<div id="otpPopup" class="modal fade" role="dialog">
			    <div class="modal-dialog" style="width:400px">  
			        <div class="modal-content">
					    <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Verify <span  class="t-isOtp"></span></h4>
					    </div>

					        <div class="modal-body">
						        <div class="form-group">
						            <form method="post" action="refundAgentRequest">
						                <input type="hidden" name="user.userId" id="otpMobile" />
						                <input type="hidden" name="transactionId" id="refund-id"  />
						                <input type="hidden" name="user.hceToken" id="isOtp" value="1"> 
						                <label class="t-isOtp">OTP/MPIN</label>
						      
						                <input class="form-control"  name="user.otp" type="password" id="r-otp" required  > 
						                <input class="btn btn-info btn-block" style="margin:20px 0 0 0" type="submit" value="Submit" / > 
						            </form> 
						       </div>
					       </div> 

			        </div> 
			    </div>
			</div>
        
        </div>   
    </div>
</div>
        <!-- contents ends -->

<%if(request.getAttribute("refundListResp") != null && refundAgent.getCode().equals("3000")){ %>

<script>
$(function(){
	$("#refundModal").modal()
});


</script>
<div id="refundModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Refund Response</h4>
      </div>
      <div class="modal-body">
        <p><%=refundAgent.getMessage() %> </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<%} %>
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->


<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<script>
    function verifyByOtp(m,r,isOtp){
	$("#r-otp").val('')
		
	$('#isOtp').val(isOtp);
	
	$('#otpMobile').val(m);
	$('#refund-id').val(r);
	if(isOtp == 0){
		$('#isOtp').attr('maxlength','4')	
		$(".t-isOtp").text('MPIN')
	}else{
		$(".t-isOtp").text('OTP');
		$('#isOtp').attr('maxlength','6')	
		$.ajax({
			method:'POST',
			url: 'RefundOtp', 
			data:"mastBean.mobileNo="+m+"&mastBean.id="+r,
			success:function(res){
				console.log(res)	
			}
			
		});
	}
	$("#otpPopup").modal('show');
	$("#otpMobile").val(m)
	
	}

	function summitOtp(m){
	var o = $("#r-otp").val()
	$.ajax({
		method:'POST',
		url: 'VerifyRefundOTP" ',
		data:"user.userId="+m+"&user.otp="+o,
		success:function(res){
			
			console.log(res)

		if(res.status == 'true'){
			
				//$("#otpPopup").modal('show');
				//$("#otpMobile").val(m)
			}
			
			
		}
		
	});
	
	}

</script>


</body>
</html>



