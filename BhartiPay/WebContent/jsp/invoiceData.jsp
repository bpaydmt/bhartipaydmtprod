<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.text.DecimalFormat"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.CouponsBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
RechargeTxnBean rechargeBean=(RechargeTxnBean)session.getAttribute("rechargeBean");
DecimalFormat d=new DecimalFormat("0.00");

String logo=(String)session.getAttribute("logo");


%>
    	
	<script type="text/javascript">
	
	function getOffer(data){
		
	    var items = [];
	    for (var key in data) {
	      items.push( 

	       "<div id='div" + key + "' title='"+  data[key].coupon_description + "' class='coupon'> <input type='checkbox' name='couponsBean.cpnids' value='"+ data[key].id +"' class='check-box' /><div class='com-logo' style=background-image:url(" + data[key].store_image + ")></div><div class='title'>" + data[key].coupon_title + "</div></div>" );
	   
	         }
	   
	    $(items.join( "" )).appendTo( ".offers-wrapper" );  	 
	 

	   }
	
	function selectOffer(){
		  var  le = $('.check-box:checked').length;
		  $('.check-box:checked').parent().css({background:"#f2f3f3"})
		  if(le > 5){
		    event.preventDefault();
		   alert("You can select only 5 offers please diselect one to choose this offer")
		   
		  }
	}
	</script>

<script type="text/javascript">
<%
List<CouponsBean> couponList=(List<CouponsBean>)session.getAttribute("couponList");
Gson gson = new Gson();
String jsonString=gson.toJson(couponList);
System.out.println(jsonString);
Map<String,String> couponCategory=(Map<String,String>) session.getAttribute("couponCategory");
if(jsonString!=null){
%>

$(function(){
	
	getOffer(<%=jsonString%>);

	$(document).on("click",".check-box", function(){
		selectOffer();
	    });
	
});
<%}%>
function addToWishList(userId,walletId,rechargeNumber,rechargeType,rechargeCircle,rechargeOperator,rechargeAmt)
{  
	
	
	
	$.ajax({
        url:"SaveWishList",
        cache:0,
        data:"wishBean.userId="+userId+"&wishBean.walletId="+walletId+"&wishBean.rechargeNumber="+rechargeNumber+"&wishBean.rechargeType="+rechargeType+"&wishBean.rechargeCircle="+rechargeCircle+"&wishBean.rechargeOperator="+rechargeOperator+"&wishBean.rechargeAmt="+rechargeAmt,
        success:function(result){
      		console.log(result)
              // document.getElementById('saveBean').innerHTML=result;
               
         }
  });  
	return false;
}

function categorySelect(){ 
	   var selVal = $("#category").val()
	  
	   $.ajax({
	   type: "POST",
	   url: "GetCouponsCategory",
	   data: {"couponsBean.category":selVal},
	   success: function(response){
		   var data = $.parseJSON(response)
		   var items = [];
		    for (var key in data) {
		      items.push( 

		       "<div id='div" + key + "' title='"+  data[key].coupon_description + "' class='coupon'> <input type='checkbox' name='couponsBean.cpnids' value='"+ data[key].id +"' class='check-box' /><div class='com-logo' style=background-image:url(" + data[key].store_image + ")></div><div class='title'>" + data[key].coupon_title + "</div></div>" );
		   
		         }
		   
		    $( ".offers-wrapper").html( items.join( "" ));  
	   },
	  error:function(xhr){
		  alert(xhr.status)
		  
	  }
	  
	   });
	  
	 }
</script>


<style>
 .invoiceContent .com-logo{width: 100%;
    height: 61px;
    border: 1px solid #ddd;
    margin: 0 0 14px 0;}
  .invoiceContent .coupon{  background: #fff!important;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: rgba(0,0,0,0.0980392) 0 0 3px 2px;
      box-shadow: rgba(0,0,0,0.0980392) 0 0 3px 2px;
      -moz-box-shadow: rgba(0,0,0,0.0980392) 0 0 3px 2px;
      text-align: left!important;
      width: 225px;
      height: 250px;
      padding:35px 15px 0 15px ;
      margin: 0 20px 20px 0;
      float: left;
   position:relative;
 }
 .invoiceContent  .check-box{
  position: absolute;
    right: 10px;
    top: 7px;
 }
  .invoiceContent .com-logo{background-size: contain;background-repeat: no-repeat;background-position: center center;}
 .invoiceContent  .title{
          color: #615e5d;
    margin: 0px;
    font-size: 15px;
    line-height: 23px;

 }
  .invoiceContent .com-url{display: block; text-decoration: none;}

  .invoiceContent .col-md-3.widget.widget1 {
    width: 31.8%;
}
  .invoiceContent .widget1 {
    margin-right: 4px;
	margin-left:6px;
}
 .invoiceContent .grow p {
    color: #fff;
    margin: 0;
    font-size: 12px;
    text-transform: uppercase;
}
.invoiceContent .map_container,  .invoiceContent .bs-example5,  .invoiceContent .panel_2,  .invoiceContent .content-box-wrapper,  .invoiceContent .grid_1,  .invoiceContent .grid_3,  .invoiceContent .no-padding,  .invoiceContent .bs-example4,  .invoiceContent .panel-body1,  .invoiceContent .editor,  .invoiceContent .banner-bottom-video-grid-left,  .invoiceContent #chart, .invoiceContent  .online,  .invoiceContent .widget_middle_left_slider, .invoiceContent  .grd_tble,  .invoiceContent .span_6,  .invoiceContent .col_2,  .invoiceContent .weather_list,  .invoiceContent .cal1,  .invoiceContent .skills-info,  .invoiceContent .stats-info,  .invoiceContent .r3_counter_box,  .invoiceContent .activity_box,  .invoiceContent .bs-example1,  .invoiceContent .cloud,  .invoiceContent .copy {
    -ms-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
    -o-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
    -webkit-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
    box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
}
 .invoiceContent .grow1,  .invoiceContent .grow3,  .invoiceContent .grow2 {
    left: 36%;
}
 .invoiceContent .grow1 {
    background: #00BCD4;
}
 .invoiceContent .r3_counter_box .fa-users {
    color: #00BCD4;
}
 .invoiceContent .grow {
    position: absolute;
    top: 44%;
    left: 14%;
    background: #EA8278;
    width: 150px;
    height: 22px;
    padding: .2em 0 0;
}
 .invoiceContent .r3_counter_box {
    min-height: 100px;
    background: #ffffff;
    text-align: center;
    position: relative;
}
 .invoiceContent .r3_counter_box .fa-mail-forward {
    color: #8BC34A;
}
 .invoiceContent .r3_counter_box .fa {
    margin-right: 0px;
    width: 66px;
    height: 65px;
    text-align: center;
    line-height: 60px;
    font-size: 2.5em;
}
 .invoiceContent .r3_counter_box h5 {
    margin: 10px 0;
    color: #575757;
    font-size: 1.5em;
}
 .invoiceContent .stats span {
    color: #999;
    font-size: 14px;
}

 .invoiceContent .r3_counter_box .fa-eye {
    color: #F44336;
}
 .invoiceContent .r3_counter_box .fa-usd {
    color: #FFCA28;
}
 .invoiceContent .stats {
    overflow: hidden;
    border-top: 1px solid #DDD;
    background: #f9f9f9;
    padding: 0.5em;
}
 </style>
 
     <style>
	@font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
.invoiceContent a {
  color: #0087C3;
  text-decoration: none;
}

 .invoiceContent header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}
 .invoiceContent #logo {
  float: left;
  margin-top: 8px;
}
 .invoiceContent #logo img {
  height: 70px;
}
 .invoiceContent #company {
  float: right;
  text-align: right;
}
 .invoiceContent #details {
  margin-bottom: 15px;
}
 .invoiceContent #client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}
 .invoiceContent #client .to {
  color: #777777;
}
 .invoiceContent h2.name {
      font-size: 16px;
    font-weight: normal;
    margin: 0;
}
 .invoiceContent #invoice {
  float: right;
  text-align: right;
}
 .invoiceContent #invoice h1 {
     color: #0087C3;
    font-size: 25px;
    line-height: 1em;
    font-weight: normal;
    margin: 0 0 10px 0;
}
 .invoiceContent #invoice .date {
  font-size: 1.1em;
  color: #777777;
}
 .invoiceContent table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}
 .invoiceContent table th,
 .invoiceContent table td {
  padding: 12px 7px;
  background: #EEEEEE;
  text-align: center;
  border-bottom: 1px solid #FFFFFF;
}
 .invoiceContent table th {
  white-space: nowrap;        
  font-weight: normal;
}
 .invoiceContent table td {
  text-align: right;
}
 .invoiceContent table td h3{
  color: #57B223;
  font-size: 14px!important;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}
 .invoiceContent table .no {
  color: #FFFFFF;
  font-size: 14px;
  text-align:center;
  background: #ff6e26;
}
 .invoiceContent table .desc {
      text-align: left;
    text-indent: 15px;
    padding: 12px 0;text-align: left;
}
 .invoiceContent table .unit {
  background: #DDDDDD;
}
 .invoiceContent table .qty {
}
 .invoiceContent table .total {
  background: #ff6e26;
  color: #FFFFFF;
}
 .invoiceContent table td.unit,
 .invoiceContent table td.qty,
 .invoiceContent table td.total {
      font-size: 12px;
    text-align: center;
}
 .invoiceContent table tbody tr:last-child td {
  border: none;
}
 .invoiceContent table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

 .invoiceContent table tfoot tr:first-child td {
  border-top: none; 
}

 .invoiceContent table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1.4em;
  border-top: 1px solid #57B223; 

}

 .invoiceContent table tfoot tr td:first-child {
  border: none;
}

 .invoiceContent #thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

 .invoiceContent #notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

 .invoiceContent #notices .notice {
  font-size: 1.2em;
}

 .invoiceContent footer {
  color: #777777;
  width: 100%;
  height: 10px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
</style>
<div class="box-inner container-fluid invoiceContent">
            
            <div class="box-content row">
            <div class="col-md-12"> 
             <%--       <header class="clearfix">
			      <div id="logo">
			         </div>
			      <div id="company">
			        <h2 class="name"><span class="blue">Oxy</span><span class="green">money</span></h2>
			      </div>
			      </div>
			    </header> --%>
			        <div id="details" class="clearfix">
        <div id="client">
          <div class="to">Recharge Status: <%=rechargeBean.getStatus()%> </div>
          <h2 class="name">Order No. <%=rechargeBean.getRechargeId() %></h2>
          <div class="address"><%=rechargeBean.getRechargeNumber() %></div>
          <div class="email"><%=rechargeBean.getRechargeOperator() %>-<%=rechargeBean.getRechargeType() %></a></div>
        </div>
        <div id="invoice">
         <!--  <h1>INVOICE:</h1> -->
          <%
          Date dNow = new Date();
          SimpleDateFormat ft=new SimpleDateFormat ("yyyy-MM-dd hh:mm a");
          %>
          <div class="date">Date: <%=ft.format(dNow) %></div>
        </div>
      </div>
           <div class="table-responsive" >
           			  <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td align="left"><h3 style="text-align: left!important"><%=rechargeBean.getRechargeOperator() %>-<%=rechargeBean.getRechargeType() %>-<%=rechargeBean.getRechargeNumber() %></td>
            <td class="total"><%=d.format(rechargeBean.getRechargeAmt()) %></td>
          </tr>
          </tbody>
        
      </table>
           </div>
           
     <%-- 
         <%
		User user=(User)session.getAttribute("User");
		%>
     <div id="saveBean" class="pull-right">
     <% if(rechargeBean.getStatus().equalsIgnoreCase("success")||rechargeBean.getStatus().equalsIgnoreCase("pending")){ %>
       <button type="submit" class="btn btn-info" value="Add to wish list" onclick="addToWishList('<%=user.getId()%>','<%=user.getWalletid()%>','<%=rechargeBean.getRechargeNumber()%>','<%=rechargeBean.getRechargeType() %>','<%=rechargeBean.getRechargeCircle()%>','<%=rechargeBean.getRechargeOperator()%>','<%=rechargeBean.getRechargeAmt()%>');">Add to wish list</button> 
      <%} %>
      </div>
     --%>  
  <div id="thanks">Thank you! 	</div>
  
  	<%-- <%if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>" class="oxyLogo pull-right" />
					<%
						}else{
					%>
					 <img alt="Bhartipay" src="./images/wallet_logo.png"  class="oxyLogo pull-right" />
						<%} %></div>
						
						<p> <%if(user.getSupportEmailId()!=null){%>
						For more information, contact:
						 <%=user.getSupportEmailId()%> <%} %>
						</p>
						 --%>
						
 <%-- <% if(rechargeBean.getStatus().equalsIgnoreCase("success")){%>
   <div class="offers">
      <div class="form-group col-md-4">
      <s:select list="%{couponCategory}" id="category" onchange="categorySelect();"></s:select>
     </div>
     
     <form action="SendCoupons.action" method="post">

     <input type="hidden" name="couponsBean.userId" value="<%=user.getId()%>">
     <input type="hidden" name="couponsBean.rechargeAmount" value="<%=rechargeBean.getRechargeAmt()%>">
     <input type="hidden" name="couponsBean.rechargeMobile" value="<%=rechargeBean.getRechargeNumber()%>">
     <div class="offers-wrapper col-md-12"></div>
      <input type="submit" class="btn btn-info" value="Mail me These offers" />
     </form>
     
     </div>
   <% } %> --%>
   
   
  
  
   </div>             
              
              
       
    
  
    
      

       
      
    
  
   

    
            </div>