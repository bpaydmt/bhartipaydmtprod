<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<a href="javascript:;" id="close_popup" class="pull-right"><i
		class="fa fa-close"></i></a>
</div>
<h3 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:center; text-decoration:none; padding:10px; border-bottom:1px solid #ccc; margin-top:0%;"><strong id="oeprator_bold"></strong> Online recharge plans for <strong id="circle_bold"></strong></h3>
<input type="hidden" name="pop_operator" id="pop_operator" value="" />
    <input type="hidden" name="pop_circle" id="pop_circle" value="" />
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_planstab">
	<div
		class="col-lg-2 col-md-2 col-sm-12 col-xs-12 payplutus_plan_tab_active"
		id="talktime_btn">
		<a href="#talktime" id="m_talktime" class="plan_tab_active">Topup</a>
	</div>
	<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="sms_btn">
		<a href="#sms" id="m_sms" class="plan_tab">SMS</a>
	</div>
	<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="twog_btn">
		<a href="#twog" id="m_twog" class="plan_tab">2G</a>
	</div>
	<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="threeg_btn">
		<a href="#threeg" id="m_threeg" class="plan_tab">3G</a>
	</div>
	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="local_btn">
		<a href="#local" id="m_local" class="plan_tab">Local</a>
	</div>
	<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="std_btn">
		<a href="#std" id="m_std" class="plan_tab">Std</a>
	</div>
	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="isd_btn">
		<a href="#isd" id="m_isd" class="plan_tab">ISD</a>
	</div>
	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 payplutus_plan_tab"
		id="other_btn">
		<a href="#other" id="m_other" class="plan_tab">OTHER</a>
	</div>
</div>
<div id="payplutus_popup_box">
<!-- Topup -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="topup_plan">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- SMS -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="sms_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>

</div>
<!-- 2G -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="twog_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- 3G -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="threeg_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- Local -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="local_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- STD -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="std_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- ISD -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="isd_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
</div>
<!-- OTHR -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="other_plan"
	style="display: none;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Price</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Validity</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h1">Talktime</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h1">Benefits</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>50
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">10 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>42.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">-</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plan_header_light">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>70
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">15 Days</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 plan_h2">
			<i class="fa fa-inr"></i>60.00
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 plan_h3">Magic
			Recharge. Customer gets surprise talktime between Rs.70 to Rs.120</div>
	</div>
	</div>
</div>