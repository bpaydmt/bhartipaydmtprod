<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>


<script type="text/javascript"> 
	$(document).ready(function() {

		    $('#dpStart').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		    });
	 }) 	
</script>

<style type="text/css">

	textarea.form-control {
        min-height: 35px;
	}

.custom-file-input {
  color: transparent;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'Upload Receipt';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 6px 12px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  /* font-weight: 700;
  font-size: 10pt; */
  border-radius: 15px;
  position: absolute;
  top: 22px;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active {
  outline: 0;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9); 
}


input[type=file] {
    display: inline-block !important;
}
</style>
</head>

<%
	User user = (User) session.getAttribute("User");
%>



<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends --> 

	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

			<!-- contents starts -->

	<div id="main" role="main"> 
	    <div id="content">  
			<div class=" row">  


					<div class=" col-md-12">  
						<div class="box2">
							<div class="box-inner">

								<div class="box-header">
									<h2>Limit Replenishment Request</h2>
								</div>

								<div class="box-content">
									<font style="color: red;"> <s:actionerror />
									</font> <font style="color: blue;"> <s:actionmessage />
									</font>
									<font color="red"><s:fielderror></s:fielderror></font>
									<form action="SaveCMERequest" method="post" enctype="multipart/form-data" id="form_name">
										<div class="row">   
												<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
                                        
												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
												 
														<label for="apptxt">Amount 
															<font color="red"> *</font>
														</label> 
														<input class="form-control onlyNum mandatory" type="text" id="amount" maxlength="7" placeholder="Enter Amount" name="cmeBean.amount" value="<s:property value='%{cmeBean.amount}'/>" required="required"> 
													 
												</div>

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
														<label>Transaction Type <font color="red"> *</font></label>
														<s:select list="%{documentTypeList}" id="drop-neft"
															onchange="dropInfo(this)" name="cmeBean.docType"
															headerKey="-1" headerValue="Select Transaction Type"
															class="mandatory">

														</s:select>
													 
												</div>

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
														<label for="apptxt">Txn Reference Number<font
															color="red"> *</font></label> <input
															class="form-control mandatory" type="text"
															id="amount" maxlength="20"
															placeholder="Txn Reference Number"
															name="cmeBean.txnRefNo"
															value="<s:property value='%{cmeBean.txnRefNo}'/>" required="required">

													 
												</div>

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group" style="padding-bottom:2px;">
												 
														<label>Bank Name <font color="red"> *</font></label>
														<s:select list="%{bankDetailList}" id="drop-neft"
															onchange="dropInfo(this)" name="cmeBean.bankName"
															headerKey="-1" headerValue="Select Bank"
															class="mandatory">
														</s:select>
													 
												</div>
												
												
												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
														<label for="apptxt">Branch Name<font
															color="red"> *</font></label> <input
															class="form-control mandatory userName" type="text"
															id="amount" maxlength="20"
															placeholder="Branch Name"
															name="cmeBean.branchName"
															value="<s:property value='%{cmeBean.branchName}'/>" required="required">

													 
												</div>

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
														<label for="apptxt">Deposit Date</label>  
															<input type="text"
															value="<s:property value='%{cmeBean.depositeDate}'/>"
															name="cmeBean.depositeDate" id="dpStart"
															class="form-control datepicker-here1 " placeholder="Deposit Date"
															data-language="en" /> 
													 
												</div> 

												  
												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group"> 
														<label for="apptxt">Remark<font color="red">
																*</font></label> <textarea class="form-control  mandatory" 
															id="branchName" maxlength="20" placeholder="Remark"
															name="cmeBean.remarks"
															><s:property value='%{cmeBean.remarks}'/></textarea> 
												</div>

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group" style="">
													<div id="RECIEPT" >
														<!-- <label>Upload Receipt</label> --> 
														<!-- <input type="file" class="form-file-inp" name="cmeBean.myFile1" 
														value="" /> -->
														<input type="file" style="margin-bottom: 30px;" class="custom-file-input form-file-inp" name="cmeBean.myFile1" 
														value="<s:property value='%{cmeBean.myFile1}'/>" /> 
													</div>
												</div> 

												<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													<button class="btn btn-success btn-fill submit-form" onclick="submitVaForm('#form_name',this)" >Sumbit </button>
													<input type="reset" onclick="resetForm('#form_name')" class="btn btn-info btn-fill" />
												</div> 
                                        </div>
									</form>
								</div>

							</div>
						</div> 
					</div> 


			</div>  
        </div>
    </div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%-- <script src="js/bootstrap.min.js"></script> --%>

	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
<!--  -->
	
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>


</body>
</html>



