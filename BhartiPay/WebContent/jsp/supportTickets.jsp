
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.bhartipay.wallet.report.bean.SupportTicketBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Support Tickets - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Support Tickets - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Support Tickets - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Support Tickets - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Support Tickets - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );

function closeSupportTicket(id){
	$("#supportticket").modal('show')
	$('#supportId').val(id)
}
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
List<SupportTicketBean> list=(List<SupportTicketBean>)request.getAttribute("supportList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->
 <div id="main" role="main"> 
  <div id="content">       
      <div class="row">    
        <div class="col-lg-12 col-md-12 col-sm-12"> 
        
        
 

 
 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Support Tickets</h2>
		</div>
	</div>
	</div>	
		<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
		<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
																					</div>

									</div>
								


					 
							
		<div id="xyz">
			<table id="example" class="display" style="table-layout: fixed; overflow:scroll;word-break: break-word;">
				<thead>
				
				
					<tr>
						<th><u>Reference Id</u></th>
						<th><u>Customer Id</u></th>
						<th><u>Mobile</u></th>
						<th><u>Email</u></th>
						<th><u>Ticket Type</u></th>
						<th><u>Ticket Description</u></th>
						<th><u>Date</u></th>
						<th><u></u></th>
					</tr>
				</thead>
				<tbody>
				<%
				for(SupportTicketBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getId()%></td>
		             <td><%=wtb.getUserId()%></td>
		             <td><%=wtb.getMobileNo()%></td>
		             <td><%=wtb.getEmailId()%></td>
		             <td><%=wtb.getTicketType()%></td>
		             <td><%=wtb.getDescription()%></td>
		          <td><%=wtb.getEntryon() %></td>
		               <td><button class="btn btn-sm btn-block btn-success" onclick="closeSupportTicket('<%=wtb.getId()%>')">Close</button></td>
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
 

 
 

</div>
      
      <div id="supportticket" class="modal fade" role="dialog">
  <div class="modal-dialog" style="    width: 350px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Close The Support Ticket </h4>
      </div>
      <div class="modal-body">
       <form action="CloseTickets" method="post">
		               <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
<input type="hidden" name="supBean.id" id="supportId" value="">
<div class="row">
<div class="form-group">
<label>Remark</label>
<input type="text" name="supBean.remarks"  class="form-control" required />
</div>
</div>
<div class="row">
<div class="form-group">
<input type="submit" value="Submit" style="padding: 10px;" class="btn btn-sm btn-block btn-success">
</div>
</div>

</div>

</form> 
      </div>
  
    </div>

  </div>
</div>  
        
 

       

          </div>
        </div>
  </div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


