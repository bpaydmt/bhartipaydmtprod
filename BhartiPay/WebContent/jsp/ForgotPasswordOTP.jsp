<%@page import="java.util.Map"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<% response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %> 

<%@taglib uri="/struts-tags" prefix="s" %>
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String  theams=mapResult.get("themes");
 String merchantName = mapResult.get("appname");
	String domainName = mapResult.get("domainName");

User user=(User)session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");

String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");
%>
<!DOCTYPE html>
<html class="<%=theams%>">
<head>

  <title><%=mapResult.get("caption") %></title>
  <meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>
<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
<link href="./css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="./css/LoginPage.css" rel='stylesheet' type='text/css' />
 <link rel="shortcut icon" href="./images/<%=favicon%>">
</head>
<body >
    
		 
		<div class="login-page">
  <div class="form">
     <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>" class="login-logo" />
					<%
						} else {
					%>
					 <img alt="Bhartipay" src="assets/img/info_logo.png"  class="login-logo" />
						<%} %>
            
    
           <s:form action="forgetPasswordConfirm" method="POST" id="forgetPwd" class="login-form" autocomplete="off">
<input type="passoword" style="display:none">

			                    <!-- <form role="form" action="" method="post" class="login-form"> -->
			                    <div class="form-group text-left">
			                   
			                    <font color="red"><s:actionerror></s:actionerror></font>
			                     <font color="blue"><s:actionmessage/></font>
			                  
			                    </div>
			                    	<div class="form-group">
			                    	<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
			                    		<input type="hidden" name="user.usertype" value=" <s:property value='%{user.usertype}'/>"/>
			                    	<input type="hidden" name="user.userId"  autocomplete="new-password" autocomplete="off" placeholder="Username" value="<s:property value='%{user.userId}'/>" class="form-username form-control " id="userid" >
			                    	<input type="password" name="user.password"  autocomplete="new-password" autocomplete="off" placeholder="Enter New Password" value="<s:property value='%{user.otp}'/>" class="form-username password-vl  " required="required" >
			                    	<input type="password" name="user.confirmPassword" autocomplete="off" placeholder="Enter Confirm New Password"" value="<s:property value='%{user.otp}'/>" class="form-username confirm-vl" required="required"  >
			                       <input type="text" autocomplete="new-password"  autocomplete="off" maxlength="6" name="user.otp" placeholder="Enter OTP" value="<s:property value='%{user.otp}'/>" class="form-username  otp-vl" required="required" id="otp" >
			                        	<div class="text-right"><a href="javascript:void(0)" onclick="document.getElementById('resendOtp').submit()" class="resendOtp">Resend OTP</a></div>
			                        </div>
			                        
			                       
                                    
                                    
                                   
			                    </s:form>
                                      
                                         
                                           <button  style="width:100%" name="login" class="submit" value="Login" onclick="submitVaForm('#forgetPwd',this)" class="btn">Submit</button>
                                       
  </div>
</div>
<div style="display:none">
<s:form action="OtpResend" method="POST" cssClass="login-form" id="resendOtp">
<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
                          <input type="hidden" name="user.userstatus" value="fpo"/>
                        <input type="hidden" name="user.userId" value="<s:property value='%{user.userId}'/>"/>
                        <input type="submit" class="login-form" style="background: #ff6e26;border: none;padding: 4px 7px;color: #FFF;font-size: 12px;" value="Resend OTP"/>
                        </s:form>
                        </div>

    </body>	 
    <script src="./js/jquery.min.js"></script>
   <script src="./js/bootstrap.min.js"></script>

  <script src="./js/validateWallet.js"></script>
  
  <script>
  
  $(function(){
	 $("#forgetPwd input[type='password']").val('')
  })
  
  </script>

</html>





