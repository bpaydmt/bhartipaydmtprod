<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.RefundMastBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Refund Requests - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Refund Requests - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Refund Requests - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Refund Requests - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Refund Requests - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 
function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		<div class="box-header well">
			<h2>Refund Requests</h2>
		
		</div>
		
	
		<div class="box-content row">
		<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color: blue;">
		<s:actionmessage/>
		</font>
		<%-- <form action="SaveCashDeposit.action"  method="post" enctype="multipart/form-data">
		<div class="col-md-4">
			<div class="form-group">
			<label>Select your transaction type</label>
				<s:select  list="%{txnTypeList}" id="drop-neft" onchange="dropInfo(this)" name="depositMast.type" headerKey="-1" headerValue="Select your transaction type">
					 
				</s:select>
			</div>
		
		
			
		</div>
			<div class="col-md-4">
				<div class="form-group">
				<label for="apptxt">Enter Amount </label> 
				<input class="form-control"  type="text" id="amount" maxlength="20" placeholder="Enter Amount" 
				name="depositMast.amount"
				value="<s:property value='%{depositMast.amount}'/>"
				>
	
				</div>
			</div>
			<div class="col-md-4" >
			<div id="NEFT" style="display:none;">
			
			
			
			 <div class="form=group">
			 <label>NEFT/IMPS/RTGS transaction Id</label>
			<input type="text" class="form-control"
			name="depositMast.neftRefNo"
				value="<s:property value='%{depositMast.neftRefNo}'/>"
			 />
			 </div>
			
	
			
			</div>
			<div id="RECIEPT" style="display:none;">
			
			
			<label >Upload cash deposit slip </label>
			<input type="file" class="form-file-inp" 
			name="depositMast.myFile1"
			value="<s:property value='%{depositMast.myFile1}'/>"
			/>
			
			
			
			</div>
			</div>
			
			<div class="col-md-12"> <input type="submit" class="btn btn-info" /></div>
			
		
		
		</form> --%>
		
		
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Id</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Amount</u></th>
						<th><u>Reason</u></th>
						<th><u>Status</u></th>						
						<th><u></u></th>
									
					</tr>
				</thead>
				<tbody>
				<!-- 

	-->		
				<%
		
			List<RefundMastBean> eList=(List<RefundMastBean>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					RefundMastBean tdo=eList.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getRefundId()%></td>
		             <td><%=tdo.getTxnid()%></td>
		              <td><%=d.format(tdo.getAmount())%></td>
		           <td><%=tdo.getReason()%></td> 
		             <td><%=tdo.getStatus()%></td> 
		             <td>
		                          
		             <form action="AcceptRefund" method="post"> 
			             <input type="hidden" name="refundBean.refundId" value="<%=tdo.getRefundId()%>">
			             <input type="submit" class="btn btn-sm btn-block btn-success grid-small-btn" value="Accept">
			             </form>
			              <form action="RejectRefund" method="post"> 
			               <input type="hidden" name="refundBean.refundId" value="<%=tdo.getRefundId()%>">
			             <input type="submit" class="btn btn-sm btn-block btn-success grid-small-btn" value="Reject">
			             </form> 
		             
			             </td>
		            
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
<!-- contents ends -->


</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



