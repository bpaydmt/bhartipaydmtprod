<!-- Call after login success -->

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>

<%
User user=(User)session.getAttribute("User");
//ServiceOperator operator=new ServiceOperator();
if(user!=null&&user.getEmailid()!=null&&user.getEmailid().length()>0){

%>
<!-- Call after login success -->


<div class="container"  id="after_login">
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: -3px; margin-left:-14px;">    	
    	<div class="container">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                	<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
             	</button>
                <a class="navbar-brand" href="home" style="color:#FFFFFF;font-family:'Bauhaus 93';font-size:28px;font-style:normal;font-weight:100;text-transform:uppercase;">
                <img src="./images/logo1.png" style="width:15%; height:auto; margin-left:7%;">Digital Wallet</a>
         	</div>
            <div id="navbar" class="navbar-collapse collapse pull-right" style="line-height:40px;">
            	<ul class="nav navbar-nav">
            		<li><a href="javascript:;" style="cursor:text;">Welcome &nbsp;<%=user.getName() %></a></li>
                    <li><a href="javascript:;" style="cursor:text;">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a></li>
                     <li><a href="javascript:;" id="login_popup">Add Money</a></li>
                    <!--  <li><a href="Pricing.action">Pricing</a></li> -->
            	      <li><a href="javascript:;" id="login_popup">Money Transfer</a></li>
                    <li><a href="Support">Support</a></li>
                    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account</a>
                    	<ul class="dropdown-menu">
                        	<li style="background-color:#ccc; padding:0px 20px;"><img src="../images/account.png" style="border:1px solid #ccc;">&nbsp;<%=user.getName() %></li>
                            <li style="background-color:#ccc; padding:0px 20px;"><%=user.getEmailid() %></li>
                        	<li><a href="ShowProfile">User Profile</a></li>
                        	<li><a href="ShowTransactions">Transaction History</a></li>
                            <li><a href="Logout">Logout</a></li>                            
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<%
}else{
%>

<div class="container" id="before_login" >
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: -3px; margin-left:-14px;">    	
    	<div class="container">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                	<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
             	</button>
                <a class="navbar-brand" href="home" style="color:#FFFFFF;font-family:'Bauhaus 93';font-size:28px;font-style:normal;font-weight:100;text-transform:uppercase;">
                <img src="./images/logo1.png" style="width:15%; height:auto; margin-left:7%;">Digital Wallet</a>
         	</div>
            <div id="navbar" class="navbar-collapse collapse pull-right" style="line-height:40px;">
            	<ul class="nav navbar-nav">
                    <li><a href="javascript:;" id="login_popup1">Add Money</a></li>
                    <!--  <li><a href="Pricing.action">Pricing</a></li> -->
            	      <li><a href="javascript:;" id="login_popup2">Money Transfer</a></li>
                    <li><a href="Support">Support</a></li>
                    <li><a href="javascript:;" id="login_popup">Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>


<%
}
%>