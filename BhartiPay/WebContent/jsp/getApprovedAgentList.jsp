<%@page import="com.bhartipay.security.EncryptionDecryption"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 



<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{walletBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{walletBean.stDate}"/>' + ' - ' + '<s:property value="%{walletBean.endDate}"/>');	
	  }
   }); 
</script> 
  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Approved Agent List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Approved Agent List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Approved Agent List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Approved Agent List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Approved Agent List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }

 </script>
 <style>
#agentFooter, .paymentref{display:none}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->


<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Approved Users List</h2>
		</div>
		
		<div class="box-content  ">
		
			<form action="GetApprovedAgentList" method="post">
<div class="row">    

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="walletBean.id" id="id"
										value="<%=user.getSubAggregatorId()%>"> 
										<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div>
										<input type="hidden"
										value="<s:property value='%{walletBean.stDate}'/>"
										name="walletBean.stDate" id="from"
										class="form-control " placeholder="Start Date"
										data-language="en" required/>
										<input
													type="hidden"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="to"
													class="form-control " placeholder="End Date"
													data-language="en"  required>
								</div>
								<%-- <div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="dpEnd"
													class="form-control " placeholder="End Date"
													data-language="en"  required>
											</div> --%>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
		<div id="xyz">
			<table id="example" class="scrollD cell-border dataTable no-footer">
				<thead>
				
				<!-- Created By	Creation Date	Status	Agent ID	Manager Owner	SO Owner	Territory	Distributor Owner	Distributor Name
				Distributor No	Agent Name	Gender	Date of Birth	Pan Card No	Aadhar No	Mobile No	Mail ID	Address 	City	State	Pin Code
				Shop Name	Shop_Address	Shop_City	Shop_State	Shop_Pin Code	Bank Name	IFSC Code	Accont No	Agg ID -->
					<tr>
					
					  
					  <th><u>Created By</u></th>
					  <th><u>SM Id</u></th>
					  <th><u>SO Id</u></th>
					  <th><u>User Type</u></th>
					  
					 						
					 <!--  <th><u>Name</u></th>        
					  <th><u>Mobile Number</u></th>   -->         
					  <th><u>Activation Date</u></th>
					  <th><u>Status</u></th>
					   
					  
					  <th style="display:none"><u>Distributor Owner</u></th>
					  <th><u>User Id</u></th> 
					    
					  <th style="display:none"><u>User Name</u></th>
					  <th style="display:none"><u>Gender</u></th>
					  <th style="display:none"><u>Date of Birth</u></th>
					  <th style="display:none"><u>Pan Card No</u></th>
					  <th style="display:none"><u>Aadhar No</u></th>
					  <th><u>Mobile No</u></th>
					  <th style="display:none"><u>Mail ID</u></th>
					  <th style="display:none"><u>Alternate No</u></th>
					  <th style="display:none"><u>Address</u></th>
					  <th style="display:none"><u>City</u></th>
					  <th style="display:none"><u>State</u></th>
					  <th style="display:none"><u>Pin Code</u></th>
					  <th style="display:none"><u>Shop Name</u></th>
					  <th style="display:none"><u>Address</u></th>
					  <th style="display:none"><u>City</u></th>
					  <th style="display:none"><u>State</u></th>
					  <th style="display:none"><u>Pin Code</u></th>
					  <th style="display:none"><u>Bank Name</u></th>
					  <th style="display:none"><u>Accont No</u></th>
					  <th style="display:none"><u>IFSC Code</u></th>
					  
					  
					  
					  
					 <!--  
					  <th><u>Created By</u></th>
					  <th><u>Activation Date</u></th>
					  <th><u>Status</u></th>
					  <th><u>Agent ID</u></th>
					  <th><u>Manager Owner</u></th>
					  <th><u>SO Owner</u></th>
					  <th><u>Territory</u></th>
					  <th><u>Distributor Owner</u></th>
					  <th><u>Distributor Name</u></th>
					  <th><u>Distributor No</u></th>
					  <th><u>Agent Name</u></th>
					  <th><u>Gender</u></th>
					  <th><u>Date of Birth</u></th>
					  <th><u>Pan Card No</u></th>
					  <th><u>Aadhar No</u></th>
					  <th><u>Mobile No</u></th>
					  <th><u>Mail ID</u></th>
					  <th style="display:none"><u>Alternate No</u></th>
					  <th style="display:none"><u>Address</u></th>
					  <th style="display:none"><u>City</u></th>
					  <th style="display:none"><u>State</u></th>
					  <th style="display:none"><u>Pin Code</u></th>
					  <th style="display:none"><u>Shop Name</u></th>
					  <th style="display:none"><u>Address</u></th>
					  <th style="display:none"><u>City</u></th>
					  <th style="display:none"><u>State</u></th>
					  <th style="display:none"><u>Pin Code</u></th>
					  <th style="display:none"><u>Bank Name</u></th>
					  <th style="display:none"><u>IFSC Code</u></th>
					  <th style="display:none"><u>Accont No</u></th>
					   -->
					  <th><u>Action</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				List<WalletMastBean>list=(List<WalletMastBean>)request.getAttribute("agentList");

				if(list!=null){
				
				
				for(int i=0; i<list.size(); i++) {
					WalletMastBean tdo=list.get(i);%>
		          		  <tr>
		          		  
		          	<td><%=tdo.getCreatedby()%></td>
					<td><%=tdo.getManagerId()%></td>
					<td><%=tdo.getSalesId()%></td>
					<td><% if(tdo.getUsertype()==2){out.print("Retailer");}else if(tdo.getUsertype()==3){out.print("Distributor");}else if(tdo.getUsertype()==7){out.print("SuperDistributor");}else{out.print("");}  %></td>
      	 	  
      	 	     	
					<%-- <td><%=tdo.getName()%></td>       
					<td><%=tdo.getMobileno()%></td> --%>
					
		          		  
		          	 <td><%=tdo.getApproveDate()%></td>
					 <td><% if("A".equalsIgnoreCase(tdo.getApprovalRequired())) {%>
		              APPROVED
		              <% } else if("R".equalsIgnoreCase(tdo.getApprovalRequired())) { %>
		              DECLINED
		              <% } else if("N".equalsIgnoreCase(tdo.getApprovalRequired())) {  %>
		              APPROVED
		              <% } else if("Y".equalsIgnoreCase(tdo.getApprovalRequired())) {  %>
		              PENDING
		              <%} %></td>
		             
		             <td style="display:none"><%=tdo.getDistributerid() != null ? tdo.getDistributerid() : ""%></td>
					 <td><b><%=tdo.getId() != null ? tdo.getId() : ""%></b></td>
					 
		             <td style="display:none"><%=tdo.getName() != null ? tdo.getName().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getGender() !=null ? tdo.getGender().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getDob() != null ? tdo.getDob() : ""%></td>
					  <td style="display:none"><%= tdo.getPan()!=null ?  EncryptionDecryption.getdecrypted(tdo.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=").toUpperCase() : "" %></td>
					  <td style="display:none"><%= tdo.getAdhar()!=null ?  EncryptionDecryption.getdecrypted(tdo.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=") : "" %></td>
					  <td ><%=tdo.getMobileno() != null ? tdo.getMobileno() : ""%></td>
					  <td style="display:none"><%=tdo.getEmailid() != null ? tdo.getEmailid().toLowerCase() : ""%></td>
					  <td  style="display:none">-</td>
					  <td style="display:none"><%=tdo.getAddress1() != null ? tdo.getAddress1().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getCity() != null ? tdo.getCity().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getState() != null ? tdo.getState().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getPin() != null ? tdo.getPin() : ""%></td>
					  <td style="display:none"><%=tdo.getShopName() != null ? tdo.getShopName().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopAddress1() != null ? tdo.getShopAddress1().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopCity() != null ? tdo.getShopCity().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopState() != null ? tdo.getShopState().toUpperCase(): ""%></td>
					  <td style="display:none"><%=tdo.getShopPin() != null ? tdo.getShopPin() : ""%></td>
					  <td style="display:none"><%=tdo.getBankName() != null ? tdo.getBankName().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getAccountNumber() != null ? tdo.getAccountNumber().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getIfscCode() != null ? tdo.getIfscCode() : ""%></td>
					  
					  
		             
		             
		              	  
		          		  
		          	<%-- 	  
		          		<td>-</td>
					  <td><%=tdo.getApproveDate()%></td>
					  <td><% if("A".equalsIgnoreCase(tdo.getApprovalRequired())) {%>
		              APPROVED
		              <% } else if("R".equalsIgnoreCase(tdo.getApprovalRequired())) { %>
		              ACCEPTED
		              <% } else if("N".equalsIgnoreCase(tdo.getApprovalRequired())) {  %>
		              REJECTED
		              <% } else if("Y".equalsIgnoreCase(tdo.getApprovalRequired())) {  %>
		              PENDING
		              <%} %></td>
					  <td><%=tdo.getId() != null ? tdo.getId() : ""%></td>
					  <td><%=tdo.getManagerName() != null ? tdo.getManagerName().toUpperCase() : ""%></td>
					  <td><%=tdo.getSoName() != null ? tdo.getSoName().toUpperCase() : ""%></td>
					  <td><%=tdo.getTerritory() != null ? tdo.getTerritory().toUpperCase() : ""%></td>
					  <td><%=tdo.getDistributerid() != null ? tdo.getDistributerid() : ""%></td>
					  <td><%=tdo.getDistributorName() != null ? tdo.getDistributorName().toUpperCase() : ""%></td>
					  <td><%=tdo.getDistributorMobileNo() != null ? tdo.getDistributorMobileNo() : ""%></td>
					  <td><%=tdo.getName() != null ? tdo.getName().toUpperCase() : ""%></td>
					  <td>-</td>
					  <td><%=tdo.getDob() != null ? tdo.getDob() : ""%></td>
					  <td><%=EncryptionDecryption.getdecrypted(tdo.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=")%></td>
					  <td><%=EncryptionDecryption.getdecrypted(tdo.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=")%></td>
					  <td><%=tdo.getMobileno() != null ? tdo.getMobileno() : ""%></td>
					  <td><%=tdo.getEmailid() != null ? tdo.getEmailid() : ""%></td>
					  <td  style="display:none">-</td>
					  <td style="display:none"><%=tdo.getAddress1() != null ? tdo.getAddress1().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getCity() != null ? tdo.getCity().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getState() != null ? tdo.getState().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getPin() != null ? tdo.getPin() : ""%></td>
					  <td style="display:none"><%=tdo.getShopName() != null ? tdo.getShopName().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopAddress1() != null ? tdo.getShopAddress1().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopCity() != null ? tdo.getShopCity().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getShopState() != null ? tdo.getShopState().toUpperCase(): ""%></td>
					  <td style="display:none"><%=tdo.getShopPin() != null ? tdo.getShopPin() : ""%></td>
					  <td style="display:none"><%=tdo.getBankName() != null ? tdo.getBankName().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getAccountNumber() != null ? tdo.getAccountNumber().toUpperCase() : ""%></td>
					  <td style="display:none"><%=tdo.getIfscCode() != null ? tdo.getIfscCode() : ""%></td>
		           	  --%>
		           	 
		           	  <td><a href="javascipt:void(0)" data-toggle="modal" data-target="#newAgentBox"><span onclick="getAgentDetails('<%=tdo.getId()%>','<%=tdo.getAggreatorid()%>')" ><input type='button' value='View Details' class='btn btn-sm  btn-success'></span></a></td>
		                
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
 

 
        
        

        <!-- contents ends -->

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "NewBpayAgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>


</body>
</html>



