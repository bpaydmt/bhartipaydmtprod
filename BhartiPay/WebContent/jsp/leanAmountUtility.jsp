<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.lean.LeanAccount"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 




 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             
        ]
    } );
    

    
} );









function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}





function myFunction()
{

	// document.getElementById("example").style.display = "none";
	 document.getElementById("leanAccountButton").style.display = "none";
	 document.getElementById("amount").style.display = "none";
	 document.getElementById("markedAslean").style.display = "none";

	 $("#amount").hide();
	 $("#leanAccountButton").hide();
	 $("#markedAslean").show();
}
function getLeanAccount(){

$("#amount").hide();
$("#leanAccountButton").hide();

var aggr = $('#selectAggregator').children("option:selected").val();
      if(aggr=='-1')
      {
  		alert("Please select Agent.");
  		return false;
  	  }
     
	$.ajax({
      		method:'Post',
      		cache:0,  		
      		url:'leanAccountDetails',
      		data:"customerId="+aggr,
      		success:function(data){
      		var json = JSON.parse(data);
      			    var userName=json.userName;
        			var agentId=json.agentId;
        			var walletId=json.walletId;
        			var aggregatorId=json.aggregatorId;	
        			document.getElementById("userName").value=userName;
        			document.getElementById("agentId").value=agentId;
        			document.getElementById("walletId").value=walletId;    		 
        			document.getElementById("aggregatorId").value=aggregatorId; 
        			
      		}
      	})
}



function markAsLean()
{
	  var idAgent=document.getElementById("agentId").value;
  	  var aggId = document.getElementById("aggregatorId").value; 
  	
		 $.ajax({
	      		method:'Post',
	      		cache:0,  		
	      		url:'findLeanRecord',
	      		data:"userId="+idAgent+"&aggregatorId="+aggId,
	      		success:function(response){
	      		
	                var json = JSON.parse(response);
	               
	                if(json.status==false)
	                 {
                       if(idAgent!="")
                         {
		                 $('#msg').html("Your record not found");
	                	 $("#leanAccountButton").show();
	             		 $("#amount").show();
	             		 document.getElementById("amount").style.display = "block";
	             		 document.getElementById("leanAccountButton").style.display = "block"; 
                         $("#msg").show();
	             		 setTimeout(function() { $("#msg").hide(); }, 3000);
	 	             	 }else
	 	             	 {
	 	             	 alert("OK")
	 	             	 $('#msg').html("Please Select User");
 	             	     $("#msg").show();
	             		 setTimeout(function() { $("#msg").hide(); }, 3000);
	 	             	 }
	                  }
	                if(json.status==true)
	                  {
	                	$('#msg').html("Your account is already leaned");
	                	$("#msg").show();
	             		setTimeout(function() { $("#msg").hide(); }, 3000);
	                  }
	      		}
		
	      	})	
}


function leanReports()
{
	  var idAgent=document.getElementById("agentId").value;
  	  var aggId = document.getElementById("aggregatorId").value; 
  	
		 $.ajax({
	      		method:'Post',
	      		cache:0,  		
	      		url:'findLeanRecord',
	      		data:"userId="+idAgent+"&aggregatorId="+aggId,
	      		success:function(response){
	      		 var json = JSON.parse(response);
	      		}
			
	      	  })	
}

function deleteData()
{
$("#example tbody tr").remove();
//$("#example tbody tr").empty();	
	/* 
    var myTable = document.getElementById("example");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
	 */
}



function saveRecord()
{debugger
	
 
	var agentName = document.getElementById("userName").value;
	var userId = document.getElementById("agentId").value;
	var walletId = document.getElementById("walletId").value;  
	var amount = document.getElementById("amount").value; 
	var aggId = document.getElementById("aggregatorId").value; 
    document.getElementById("amount").value="";   
 
	if (isNaN(amount)) 
	  {
	   return false;
	  } 
	
	if(amount=="")  
     {
		$('#msg').html("Please fill required amount to save.");
		 leanReports();
   	     alert("Please fill required amount to save.");
   	    }else{
	
    	 $.ajax({
      		method:'Post',
      		cache:0,  		
      		url:'saveLeanAccount',
      		data:'userId='+userId+'&walletId='+walletId+'&amount='+amount+'&aggregatorId='+aggId+'&name='+agentName,
      		//data:"userId="+userId+"&walletId="+walletId+"&amount="+amount,
      		
      		success:function(response){
      		    var json = JSON.parse(response);
                if(json.status==false)
                 {
                	$('#msg').html("Your account is already leaned");
                	document.getElementById("example").style.display = "block";
                	$("#msg").show();
             		setTimeout(function() { $("#msg").hide(); }, 3000);
                	//leanReports();
                 }else
                	 {
                	 $('#msg').html("Your account is  leaned");
                	 $("#msg").show();
	             	 setTimeout(function() { $("#msg").hide(); }, 3000);
             		 //markAsLean();
             		 location.reload()
                	 }
      		}
      	})
     }
}


function deleteRecord(id)
{debugger
	$.ajax({
  		url:'deleteLeanRecord',
  		method:'Post',
  		cache:0,  
		data:"userId="+id,
  	    success:function(response){
  		alert("Your record has been deleted");
  		$('#msg').html("Your account is deleted");
  		$("#msg").show();
 		setTimeout(function() { $("#msg").hide(); }, 3000);
 		location.reload()
  	  }
  	})
}


function updateLeanAmount(id){
debugger
    var amount = document.getElementById("input_field_1"+id.toString()).value; 
    
	$.ajax({
  		url:'updateLeanAmount',
  		method:'Post',
  		cache:0,  
 		data:"userId="+id+"&amount="+amount,
  	    success:function(response){
  	      alert("Your lean amount is updated")
  		$('#msg').html("Your lean amount is updated");	
  	    $("#msg").show();
 		setTimeout(function() { $("#msg").hide(); }, 3000);	
 		//location.reload()
   	    }
  	})
    
  }



 </script>
 

</head>

<body onload="myFunction()">

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  

<%
    User user = (User) session.getAttribute("User");
    List<LeanAccount> list=(List<LeanAccount>)request.getAttribute("leanResult");
    
   
%>             

 
 
<div class="box2">
	<div class="box-inner">
	
		<div class="box-header">
			<h2>Lean Amount</h2>
		</div>
		
		<div class="box-content">
		
			<form action="RechargeReportAggreator" method="post">
<div class="row">

								
							
								
								
											
	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
		<br>
	<div class="row">
	<div class="container">
	<div class="col-sm-4"> 
	<s:select list="%{agentList}" headerKey="All"
				headerValue="Select Agent" onchange="getLeanAccount()" id="selectAggregator"
				 cssClass="form-username" requiredLabel="true"/>
	<input type="hidden"    id="walletId" name="walletId"/>
    <input type="hidden"    id="userName" name="userName" />
    <input type="hidden"    id="agentId"  name="agentId" />
    <input type="hidden"    id="aggregatorId" name="aggregatorId"/>
	
	</div></div></div>							
				
			</div>
								
											
		  <!-- <div class="form-group col-md-3 txtnew col-sm-4 col-xs-6 text-left "> -->
			<div class="col-md-2">
			<input type="button" id="markedAslean" class="btn btn-success btn-fill" style="margin-top: 22px;"
						onclick="markAsLean()" value="Mark As lean"  />
			</div>
			
			<div class="col-md-2" id="amt">
			    <input type="number" id="amount" id="amount"   style="margin-top: 22px;"
						placeholder="Please enter amount" />
			</div>
			<div class="col-md-2" id="leanBtn">	
				<input type="button" id="leanAccountButton" class="btn btn-success btn-fill" style="margin-top: 22px;"
						onclick="saveRecord()" value="Save Lean Amount"   />
			</div>
		<!-- </div> -->
								
							 
   <div class="col-md-2" id="msg" style="color: red ;" ></div>
          
             						
							</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="example" class="display" width="100%">
		<!-- 	<table id="example" class="scrollD cell-border dataTable no-footer">
		 -->		<thead>
				
				
					<tr>
						<th width="5%"><u>Sr.No</u></th>
						<th><u>User Id</u></th>
						<th><u>User Name</u></th>
						<th><u>Wallet Id</u></th>
						<th><u>Amount</u></th>
						<th><u>Action</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				int count=1;
				for(LeanAccount wtb:list){
					
				%>
				  	<tr>
		             <td><%=count++%></td>
		             <td><%=wtb.getUserId()%></td>
		             <td><%=wtb.getName()%></td>
		             <td><%=wtb.getWalletId()%></td>
		             <td><input type="number" value="<%=wtb.getAmount()%>" id="input_field_1<%=wtb.getUserId()%>"/></td>
		             
		          <td> <input type="button" value="Update" onclick="updateLeanAmount('<%=wtb.getUserId()%>')"   id="myButton'+<%=wtb.getUserId()%>'" />  ||  <input type="button" value="Delete" onclick="deleteRecord('<%=wtb.getUserId()%>')" id="deleteButtons'+<%=wtb.getUserId()%>+'" /></td></td>
                    </tr>
                  <%
                  }
				%>
			 </tbody>
			</table>
		</div>
	 

 
 
 
        
        

        <!-- contents ends -->

       

  		</div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


