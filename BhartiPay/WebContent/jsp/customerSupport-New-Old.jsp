<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.report.bean.SupportTicketBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Customer Support</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="SaveSupportDetails" id="form_name" method="post" name="PG">
		
<%
User user=(User)session.getAttribute("User");
List<SupportTicketBean>list=(List<SupportTicketBean>)request.getAttribute("supportList");
%>          
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Agent Id</label>
															<input	class="form-control" name="supBean.userId" type="text" value="<%=user.getId() %>" readonly/>
															 <%-- <input	class="form-control" name="supBean.mobileNo" type="text" required="required"
																id="mobile" maxlength="20" placeholder="Mobile number" value="<%=user.getMobileno() %>"   readonly/> --%>

														</div>
													</div>
													
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Mobile Number</label>
															<%-- <input	class="form-control" name="supBean.userId" type="hidden" value="<%=user.getId() %>" /> --%>
															 <input	class="form-control" name="supBean.mobileNo" type="text" required="required"
																id="mobile" maxlength="20" placeholder="Mobile number" value="<%=user.getMobileno() %>"   readonly/>

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Email </label>
															 <input	class="form-control" name="supBean.emailId" type="text" required="required"
																id="email" maxlength="20"  placeholder="Email" value="<%=user.getEmailid() %>" readonly/>

														</div>
													</div>
													
				<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Ticket Type</label>
															 <select name="supBean.ticketType">
															 <option value="General problem">General problem</option>
															 <option value="Recharge problem">Recharge problem</option>
															 <option value="Refund problem">Refund problem</option>
															 <option value="Money transfer problem">Money transfer problem</option>
															 </select>

														</div>
													</div>


									
									
									
									
									
									
									


													<div class="col-md-8">
														<div class="form-group">
															<label for="apptxt">Description</label>
															 <textarea	class="form-control mandatory disc-vl" name="supBean.description" type="text"
																id="description"  placeholder="Descriptoin"  ></textarea>

														</div>
													</div>
													
											
									
									
									
					
									
									
									
								
									</div>
									
									
									</form>
		<div class="col-md-4">
										<div class="form-group">
										
										<button type="button" class="btn btn-info btn-fill"
											 style="margin-top: 20px;" onclick="submitVaForm('#form_name',this)">Submit
										</button>
										<button type="button" class="btn btn-info btn-fill"
											 style="margin-top: 20px;" onclick="resetForm('#form_name')">Reset
										</button>

									
									</div>
                </div>
                

    
            </div>
        </div>
  
</div>

<div id="pic">

<div id="xyz">
			<table id="example" class="display" style="table-layout: fixed; overflow:scroll;word-break: break-word;">
				<thead>
				
				
					<tr>
						<th><u>Id</u></th>
						<th><u>Customer Id</u></th>
						<!-- <th><u>Mobile</u></th>
						<th><u>Email</u></th> -->
						<th><u>Ticket Type</u></th>
						<th><u>Ticket Description</u></th>
						<th><u>Remark</u></th>
						<th><u>Date</u></th>
						<th><u>Status</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				for(SupportTicketBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getId()%></td>
		             <td><%=wtb.getUserId()%></td>
		             <%-- <td><%=wtb.getMobileNo()%></td>
		             <td><%=wtb.getEmailId()%></td> --%>
		             <td><%=wtb.getTicketType()%></td>
		             <td><%=wtb.getDescription()%></td>
		             <td><%=wtb.getRemarks()!=null?wtb.getRemarks():"-"%></td>
		          <td><%=wtb.getEntryon() %></td>
		               <td><%=wtb.getStatus()%></td>
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


