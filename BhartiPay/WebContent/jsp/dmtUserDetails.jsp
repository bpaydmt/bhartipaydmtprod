<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtUserDetails"%>
<%

DmtUserDetails userDetails=(DmtUserDetails)request.getAttribute("userDetail");
if(userDetails!=null&&userDetails.getFirstName()!=null&&userDetails.getEmail()!=null){
%>

<table class="table table-bordered table-striped">
<tr>
<td><strong>First Name</strong></td><td><%=userDetails.getFirstName() %></td>

</tr>
<tr>
<td><strong>Last Name</strong></td><td><%=userDetails.getLastName() %></td>
</tr>
<tr>
<td><strong>Preferred Name</strong></td><td><%=userDetails.getPreferredName() %></td>
</tr>
<tr>
<td><strong>Mobile Number</strong></td><td><%=userDetails.getMobileNumber() %></td>
</tr>
<tr>
<td><strong>Email</strong></td><td><%=userDetails.getEmail() %></td>
</tr>
<tr>
<td><strong>Date</strong></td><td><%=userDetails.getActiveDate() %></td>
</tr>
<tr>
<td><strong>Status</strong></td><td><%=userDetails.getStatus() %></td>
</tr>



</table>
<%
}else{
%>
Detail not found.
<%}%>