
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 <script type="text/javascript">

function getWalletConfigByUserId(userId)
{  
	$.ajax({
        url:"GetWalletConfigByUserId",
        cache:0,
        data:"config.id="+userId,
        success:function(result){
               document.getElementById('limitform').innerHTML=result;
               
         }
  });  
	return false;
}
</script>
</head>

<% 
User user = (User) session.getAttribute("User");
%>
       
       <body>
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Set Limits</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">  
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">

											<form action="GetWalletConfigByUserId" method="post">
													<div class="row" style="border-bottom: 1px solid blue;">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>User id</label>
																<div class="input-group">
																	
																 	<input type="hidden" name="config.walletid"
																		id="userid" value="<s:property value='%{config.walletid}'/>">
																		
																	<input type="text" id="customer"
																		name="config.id" Class="form-username form-control"
																		requiredLabel="true" readonly="readonly" 
																		value="<s:property value='%{config.id}'/>"/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Select transaction type</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<s:select list="%{txnType}" headerKey="0"
																		headerValue="Default" id="txntype"
																		name="config.txnType" cssClass="form-username"
																		requiredLabel="true"/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 50px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Search</button>
														</div>


											</div>	
																			
											</form>
											
				
							
							
	

<!-- *********************************************************************************************************************** -->
	

											
									<div id="limitform">
									
								
									
									<form action="Savelimit" method="post" id="setLimitForm">
												 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Minimum wallet balance</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="hidden" name="config.id" value="<s:property value='%{config.id}'/>">
																<input type="hidden" name="config.walletid" value="<s:property value='%{config.walletid}'/>">
																<input type="hidden" name="config.txnType" value="<s:property value='%{config.txnType}'/>">
																<input type="text"
																value="<s:property value='%{config.minbal}'/>"
																name="config.minbal" id="minbal" class="form-control mandatory amount-vl2 zero-allowed"
																placeholder="Minimum wallet balance" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per transaction</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtpertx}'/>"
																name="config.maxamtpertx" id="maxamtpertx" class="form-control mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/transaction" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per day</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperday}'/>"
																name="config.maxamtperday" id="maxamtperday" class="form-control  mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/day" />

																</div>
															</div>
														</div>

													</div>
													
												
													
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per week</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperweek}'/>"
																name="config.maxamtperweek" id="maxamtperweek" class="form-control  mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/week" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per month</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtpermonth}'/>"
																name="config.maxamtpermonth" id="maxamtpermonth" class="form-control  mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/month" />

																</div>
															</div>
														</div>
												
															<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per quarter</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperquarter}'/>"
																name="config.maxamtperquarter" id="maxtxperweek" class="form-control  mandatory amount-vl2 zero-allowed "
																placeholder="Max amount/quarter" />

																</div>
															</div>
														</div>

													</div>
													
													 <div class="row">
													
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per half yearly</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperhalfyearly}'/>"
																name="config.maxamtperhalfyearly" id="maxtxpermonth" class="form-control  mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/half yearly" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per year</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperyear}'/>"
																name="config.maxamtperyear" id="maxamtperacc" class="form-control  mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/year" />

																</div>
															</div>
														</div>
														
																<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per day</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxperday}'/>"
																name="config.maxtxperday" id="maxtxperday" class="form-control  mandatory onlyNum"
																placeholder="Max transaction/day"maxlength="5" />

																</div>
															</div>
														</div>

													</div>
													
												<!-- 
	private String id;
	private String walletid;
	private double minbal;
	private double maxamtpertx;
	private double maxamtperday;
	private double maxamtperweek;
	private double maxamtpermonth;
	private int maxtxperday;
	private int maxtxperweek;
	private int maxtxpermonth;
	private double maxamtperacc; -->		
													
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per week</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxperweek}'/>"
																name="config.maxtxperweek" id="maxtxperweek" class="form-control mandatory onlyNum"
																placeholder="Max transaction/week" maxlength="5" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per month</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxpermonth}'/>"
																name="config.maxtxpermonth" id="maxtxpermonth" class="form-control mandatory onlyNum"
																placeholder="Max transaction/month"  maxlength="5"/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per account</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperacc}'/>"
																name="config.maxamtperacc" id="maxamtperacc" class="form-control mandatory amount-vl2 zero-allowed"
																placeholder="Max amount/accounnt" />

																</div>
															</div>
														</div>

													</div>
													
<!-- 	private double maxamtperquarter;
	private double maxamtperhalfyearly;
	private double maxamtperyear; -->				
													
													
													
												<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<div class="clearfix"></div>
												</form>
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															 <button  class="btn btn-info" name="FrgtPwd" onclick="submitVaForm('#setLimitForm')">Submit</button>
															 <button  class="btn btn-info" onclick="resetForm('#setLimitForm')">Reset</button>
														</div>
													</div>
													

													<div class="row"></div>
									
									
									
									</div>
									</div>
									
								

                </div>
                

    
            </div>
            
            </br>
              
        </div>
  
</div>

</div>
</div>

</div>
        
        
</div>
        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


