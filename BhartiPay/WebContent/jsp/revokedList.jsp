<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.RevokeUserDtl"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wallet History - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wallet History - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wallet History - ' + '<%= currentDate %>',
              
            }
        ]
    } );
   
  

	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   function revokeAction(id,revokeId,rid,uid, amt){
	   $('#revoke-inp').val(id)
	   
	 
	 $("#revoke-id").val(revokeId)
          	  $("#requester-id").val(rid)
          	   $("#requester-user-id").val(uid)
          	    $("#requester-amount").val(amt)
          	         $('#revokeActionPopup').modal('show');
   }
 </script>
 

</head>

       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
 
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
        
 
            

 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Revoke List</h2>
		</div>
	<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color:green;">
		<s:actionmessage/>
		</font>	
		<div class="box-content  ">
		<div id="xyz" style="margin-top:20px">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Revoke ID</u></th>
						<th><u>Revoke UserID</u></th>
						<th><u>Requester ID</u></th>
						<th><u>Amount</u></th>
						<th><u>Requester Remark</u></th>
						<th><u>Request Date</u></th>
						<th><u>Approver Remark</u></th>
						<th><u>Approve Date</u></th>
						 <th><u>Status</u></th>
						 <th><u>Action</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				List<RevokeUserDtl> revokeListAgg=(List<RevokeUserDtl>)request.getAttribute("revokeList");
				if(revokeListAgg!=null){				
				for(int i=0; i<revokeListAgg.size(); i++) {
					RevokeUserDtl tdo=revokeListAgg.get(i);%>
		          		  <tr>
		         
		          	   <td><%=tdo.getId()%></td>
		     			<td><%=tdo.getUserId()%></td>
		     			<td><%=tdo.getRequestedBy()%></td>
		     			<td><%=tdo.getAmount()%></td>
		     			<td><%=tdo.getRequesterRemark()%></td>
		     			<td><%=tdo.getRequestDate()%></td>
		     			<% if(tdo.getApproverRemark() != null && !tdo.getApproverRemark().isEmpty())
		     			{%>
		     				<td><%=tdo.getApproverRemark()%></td>
		     				<td><%=tdo.getApproveDate()%></td>
		     			<%}else{%>
		     				<td>-</td>
		     				<td>-</td>
		     			<%}
		     			%>
		          		<td><%=tdo.getStatus()%></td>
		          		<td>
		          		<%if(tdo.getStatus().equals("REQUESTED")){ %>
		          		<button class="btn-info btn-block" onclick="revokeAction(0,'<%= tdo.getId() %>','<%= tdo.getRequestedBy() %>','<%=tdo.getUserId()%>', <%=tdo.getAmount()%>)">Accept</button>
		          		<button class="btn-info btn-block" onclick="revokeAction(1,'<%= tdo.getId() %>','<%= tdo.getRequestedBy() %>','<%=tdo.getUserId()%>', <%=tdo.getAmount()%>)">Reject</button>
		          		<%} %>
		          		</td>
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
		
		
		</div>
	</div>
	</div>	
	
		

							
		
 

 
 
   <div class="modal fade" id="revokeActionPopup" role="dialog">
    <div class="modal-dialog" style="width:400px">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Revoke Action</h4>
        </div>
        <div class="modal-body">
          <form action="rejectAcceptRevoke" method="post">
           <s:hidden name="csrfPreventionSalt"
					value="%{#session.csrfPreventionSalt}"></s:hidden>
          <div class="row">
          	<div class="col-md-12 form-group">
          	 <label>Remark</label>
          	 <input type="hidden" id="revoke-inp" name="revokeType" />
          	 <input type="hidden" id="revoke-id" name="revokeId" />
          	  <input type="hidden" id="requester-id" name="requesterId" />
          	            	  <input type="hidden" id="requester-user-id" name="revokeUserId" />
          	            	  <input type="hidden" id="requester-amount" name="amount" />
          <input class="form-control" name="remark" required/>
           
          
          	</div>
               
               	<div class="col-md-12 form-group">
          	
            <input type="Submit" class="btn-info btn btn-primary btn-info btn-block" value="Submit" />   
          
          	</div>
          </div>
         	
          
          </form>
        </div>
       
      </div>
      
    </div>
  </div>     
        

        <!-- contents ends -->

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<script src='js/bootstrap.min.js'></script>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



