<%@page import="com.bhartipay.recharge.request.AgentDeviceInfo"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%
WalletMastBean mastBean =(WalletMastBean) request.getAttribute("agentDetails");
String logo=(String)session.getAttribute("logo");
%>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<style type="text/css">
		*{
			margin: 0px;
			padding: 0px;
			box-sizing: border-box;
			font-family: 'Roboto Slab', serif;
		}
		.Wrapper{
			width: 800px;
			margin: auto;
			padding: 15px 15px 35px 15px;
		}
		.bhartipayBoarder{
			border-right: 7px solid #00994b;
			border-bottom: 7px solid #00994b;
			border-left: 7px solid #e88239;
			border-top: 7px solid #e88239;
		}

		.promoneyBoarder{
			border-right: 7px solid #122c56;
			border-bottom: 7px solid #1b9fda;
			border-left: 7px solid #d51450;
			border-top: 7px solid #122c56;
		}
		.logo{
			width: 135px;
			float: right;
		}

		h2{
			float: left;
			width: 100%;
			text-align: center;
			color: #ff9345;
			font-size: 33px;
			font-style: italic;
			margin-top: 28px;
			font-weight: 500;
		}
		.promoneyColor{
          color: #122c56 !important
		}
		.promoneyColor1{
          color: #1b9fda !important
		}
		.subheading{
			float: left;
			width: 100%;
			margin-top: 20px;
		}
		.subheading p{
			float: left;
			display: inline;
			width: 50%;
			font-weight: 500;
			font-size: 18px;
		}
		.subheading p .inputField{
			padding: 0px 5px;
		}
		.certificatetext{
			float: left;
			width: 100%;
			margin-top: 50px;
			font-size: 18px;
			line-height: 34px;
			font-weight: 500;
		}
		.certificatetext .inputField{
			border-bottom: 2px solid #000;
			padding: 0px 15px;
			word-break: keep-all;
		}
		.clearfix::after {
			content: "";
			clear: both;
			display: table;
		}
		.floatRight{
			float: right!important;
			    text-align: right;
		}
		.footer{
			float: right;
			margin-top: 35px;
			font-size: 18px;
			color: #1d4e88;
		}
		.qrCode{
		    float: left;
		    width: 100px;
		    float: right;
		    border: 2px solid #122c56;
		    padding: 9px 0px;
		}
		.qrCode img{ 
		   width: 100%;
		   float: left;
		}
	</style>
</head>
<body>

	<!-- ======================================-->
	<!-- bahrtipay certificate start -->
	<!-- ====================================== -->
	<% if("OAGG001050".equalsIgnoreCase(mastBean.getAggreatorid())){ %>
	<div class="Wrapper clearfix bhartipayBoarder" style="margin-top: 1%;">
		<span class="qrCode" style="float: left"><img src="./images/qrcode.jpg" ></span>
		<img src="<%=logo%>" class="logo" style="">
		<h2>Certificate of Authorization</h2>
		<div class="subheading">
			<p>Agent ID-<span class="inputField"><%=mastBean.getId() %> </span></p>

			<p class="floatRight">Date of issuance-<span class="inputField">31/3/2020 </span></p>
		</div>
		<p class="certificatetext">
			This is to certify that 
			<span class="inputField"> <%=mastBean.getName() %> </span> 
			located at 
			<span class="inputField"><%=mastBean.getShopAddress1()!=null?mastBean.getShopAddress1():"" %> </span>
			is an authorized <span class="inputField"> <%="Business Associate" %></span> of Bhartipay and is hereby authorized to conduct
			the business on its behalf with effect from the date of issuance.
		</p>
		<h1 class="footer">
			SAKSHI CHAWLA<br>
			CEO<br>
			BHARTIPAY SERVICES PVT. LTD
		</h1>

	</div>
	<!-- ======================================-->
	<!-- bahrtipay certificate start -->
	<!-- ====================================== -->
<%
}
if("OAGG001057".equalsIgnoreCase(mastBean.getAggreatorid())){
%>


	<!-- ======================================-->
	<!-- promoney certificate start -->
	<!-- ====================================== -->
	<div class="Wrapper clearfix promoneyBoarder" style="margin-top: 1%;">
		<img src="<%=logo%>" class="logo" style="float: left;width: 150px;position: relative;top: 24px;">
		<span class="qrCode" style="float: right"><img src="./images/qrcode.jpg" ></span>
		<h2 class="promoneyColor">Certificate of Authorization</h2>
		<div class="subheading">
			<p>Agent ID-<span class="inputField"><%=mastBean.getId() %> </span></p>

			<p class="floatRight">Date of issuance-<span class="inputField">31/3/2020 </span></p>
		</div>
		<p class="certificatetext">
			This is to certify that <span class="inputField"> <%=mastBean.getName() %> </span> located at <span class="inputField"><%=mastBean.getShopAddress1()!=null?mastBean.getShopAddress1():"" %> </span> is an authorized of <span class="inputField"><%="Business Associate" %></span> ProMoney and is hereby authorized to conduct the business on its behalf with effect from the date of issuance.

		</p>
		<h1 class="footer">
			SACHIN VERMA</br>
			<span class="promoneyColor">AUTHORIZED SIGNATORY</span><br>
			PRO<span class="promoneyColor1">MONEY</span>
		</h1>

	</div>
	<%} %>
	<!-- ======================================-->
	<!-- promoney certificate end -->
	<!-- ====================================== -->
</body>
</html>