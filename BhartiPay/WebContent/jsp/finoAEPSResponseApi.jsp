<%@page import="com.bhartipay.wallet.aeps.AepsResponseData"%>

<%
AepsResponseData encData=(AepsResponseData)request.getAttribute("data");
%>

</html>


<body onload="fnSubmit();">
	<form name="Ecom" method="post" action="<% encData.getRedirectionUrl(); %>">
		<table align="center">
			<tr>
				<td><STRONG>Response is being processed,</STRONG></td>
			</tr>
			<tr>
				<td><font color='blue'>Please wait ...</font></td>
			</tr>
			<tr>
				<td>(Please do not press "Refresh" or "Back" button)</td>
			</tr>
		</table>
		
		<input type="hidden" name="amount" id="amount" value="<% encData.getAmount();%>"> 
		<input type="hidden" name="adhaarNo" id="adhaarNo" value="<% encData.getAadharNumber(); %>">
		
		<input type="hidden" name="txnTime" id="txnTime" value="<% encData.getDateTime(); %>"> 
		
		<input type="hidden" name="bankName" id="bankName" value="<% encData.getBcname();  %>"> 
		<input type="hidden" name="rrn" id="rrn" value="<% encData.getRrn(); %>">

	    <input type="hidden" name="status" id="status" value="<%encData.getStatusCode();%>">
	    <input type="hidden" name="statusMessage" id="statusMessage" value="<%encData.getStatusMessage();%>"> 
		 
		<input type="hidden" name="availableBalance" id="availableBalance" value="<% encData.getAccountBalance();%>"> 
		<input type="hidden" name="txnId" id="txnId" value="<% encData.getTxnId(); %>">
		<input type="hidden" name="txnType" id="txnType" value="<% encData.getTxnType(); %>">
		
		
	</form>
</body>
<script>

function fnSubmit(){
	document.Ecom.submit();
}
</script>