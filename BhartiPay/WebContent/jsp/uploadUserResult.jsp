
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'User Creation Result - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'User Creation Result - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'User Creation Result - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'User Creation Result - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'User Creation Result - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function getSubagentByAgentId(agentId)
{  
	
	$.ajax({
        url:"GetSubagent",
        cache:0,
        data:"reportBean.agentId="+agentId,
        success:function(result){
        	
               document.getElementById('subagent').innerHTML=result;
               
         }
  });  
	return false;
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
		<h1>User Creation Result/h1>
		
		

							<%-- <div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
								<label for="dateFrom">Date From:</label> <br> <input
									type="text" value="<s:property value='%{inputBean.stDate}'/>"
									name="inputBean.stDate" id="dp"
									class="form-control datepicker-here" placeholder="Start Date"
									data-language="en" required readonly="readonly">
							</div>
							<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
								<label for="dateTo">Date To:</label> <br> <input
									type="text" value="<s:property value='%{inputBean.endDate}'/>"
									name="inputBean.endDate" id="dp1"
									class="form-control datepicker-here" placeholder="End Date"
									data-language="en" required readonly="readonly">
							</div> --%>
							
							
			<%
		List<WalletMastBean>userList=(List<WalletMastBean>)request.getAttribute("userUploadList");
	
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>ID</u></th>
						
						<th><u>Name</u></th>
						<th><u>Mobile</u></th>
						<th><u>Email</u></th>
						<th><u>Address 1</u></th>
						<th><u>Address 2</u></th>
						<th><u>City</u></th>
						<th><u>State</u></th>
						<th><u>PIN Code</u></th>
						<th><u>Status</u></th>
						
						
					</tr>
				</thead>
				<tbody>
				<%
				if(userList!=null&&userList.size()>0){	
				for(WalletMastBean wmb:userList){
				%>
				  	<tr>
		             <td><%=wmb.getId() %></td>
		             
		             <td><%=wmb.getName()%></td>
		             <td><%=wmb.getMobileno() %></td>
		              <td><%=wmb.getEmailid()%></td>
		              <td><%if(wmb.getAddress1()==null){out.print("-");}else{out.print(wmb.getAddress1());}%></td>
		               <td><%if(wmb.getAddress2()==null){out.print("-");}else{out.print(wmb.getAddress2());}%></td>
		                <td><%if(wmb.getCity()==null){out.print("-");}else{out.print(wmb.getCity());}%></td>
		                <td><%if(wmb.getState()==null){out.print("-");}else{out.print(wmb.getState());}%></td>
		                 <td><%if(wmb.getPin()==null){out.print("-");}else{out.print(wmb.getPin());}%></td>
		                  <td><%=wmb.getUserstatus()%></td>
                 		
 </tr>
                  <%
                  }}
				%>
			        </tbody>		</table>
		</div> 
		
		
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


