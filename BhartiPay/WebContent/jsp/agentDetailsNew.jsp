<%@page import="com.bhartipay.wallet.user.persistence.vo.AgentDetailsView"%>
<%  
AgentDetailsView adv=(AgentDetailsView)request.getAttribute("agentDetails");

%>

<div class="table-responsive">
<table class="table table-bordered table-striped">
	
		<tr>
			<td style="width: 25%"><strong>User Id</strong></td>
			<td style="width: 25%"><%=adv.getId() %></td>
			<td style="width: 25%"><strong>User Type</strong></td>
			<td style="width: 25%"><%=adv.getAgenttype()%></td>
		</tr>
		<tr>
			<td><strong>User Name</strong></td>
			<td><%=adv.getName() %></td>
			<td><strong>User DOB</strong></td>
			<td><%=adv.getDob()%></td>
		</tr>
		<tr>
			<td><strong>PAN</strong></td>
			<td><%=adv.getPan() %></td>
			<td><strong>User Mobile</strong></td>
			<td><%=adv.getMobileno()%></td>
		</tr>

		<tr>
			<td><strong>Address Proof Type</strong></td>
			<td><%=adv.getAddressprooftype() %></td>
			<td><strong>Address Proof Number</strong></td>
			<td><%=adv.getAdhar()%></td>
		</tr>
		<tr>
			<td><strong>User Email</strong></td>
			<td><%=adv.getEmailid() %></td>
			<td><strong>Distributor Id</strong></td>
			<td><%=adv.getDistributerid()%></td>
		</tr>
		
		<tr>
			<td><strong>Sales Manager</strong></td>
			<td><%=adv.getManagerId() %></td>
			<td><strong>Sales Officer</strong></td>
			<td><%=adv.getSalesId()%></td>
		</tr>
		
		<tr class="details-heading">
			<td colspan="4">Address</td>

		</tr>

		<tr>
			<td><strong>User Address</strong></td>
			<td><%=adv.getAddress1()%></td>
			<td><strong>User City</strong></td>
			<td><%=adv.getCity()%></td>
		</tr>
		<tr>
			<td><strong>User State</strong></td>
			<td><%=adv.getState()%></td>
			<td><strong>PIN code</strong></td>
			<td><%=adv.getPin()%></td>
		</tr>
	
	
	<tr class="details-heading">
			<td colspan="4">Shop Address</td>

		</tr>
		<tr>
			<td><strong>Shop Name</strong></td>
			<td><%=adv.getShopname()%></td>
			<td><strong>Shop Address</strong></td>
			<td><%=adv.getShopaddress1() %> <%=adv.getShopaddress2() %></td>

		</tr>
		<tr>
			<td><strong>Shop City</strong></td>
			<td><%=adv.getShopcity()%></td>
			<td><strong>Shop State</strong></td>
			<td><%=adv.getShopstate()%></td>

		</tr>
		<tr>
			<td><strong>PIN code</strong></td>
			<td><%=adv.getShoppin()%></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>

		</tr>
	
	
		
</table>

</div>
<div class="modal-footer" id="agentFooter">

								
													
											
											

      </div>