<%@page import="com.bhartipay.wallet.user.persistence.vo.MerchantOffersMast"%>
<%@page import="com.bhartipay.wallet.merchant.persistence.vo.MerchantSummary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Merchant List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Merchant List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Merchant List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Merchant List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
                orientation: 'landscape',
                pageSize: 'LEGAL'
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Merchant List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
/* Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in")); */

List<MerchantSummary> mList=(List<MerchantSummary>)request.getAttribute("mList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Upload Offer List</h2>
		</div>
		
		<div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">

												<form action="SaveUploadOffers.action" method="post" enctype="multipart/form-data">
													<div class="row">
								
														<div class="col-md-4" style="margin-top: 30px;">
														<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
															<input type="file"
																value="<s:property value='%{upload.myFile1}'/>"
																name="upload.myFile1" id="dp" class="form-control" required="true"
																placeholder="Select Address Proof" required readonly="readonly">
														</div>

													</div>
									
													
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Submit</button>
														</div>
													</div>
													

													<div class="row"></div>
													<div class="clearfix"></div>
												</form>


											</div>
									
									</div>
									
								

                </div>
                

    
            </div>
            
            
		
	</div>
	</div>	
	
		

							
				<%
	List<MerchantOffersMast> offer=(List<MerchantOffersMast>)request.getAttribute("userUploadList");
		if(offer!=null&&offer.size()>0){
		%>				
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>						
						<th><u>Merchant Name</u></th>
						<th><u>Merchant Id</u></th>
						<th><u>Terminal Id</u></th>
						<th><u>Cashback(%)</u></th>
						
									
					</tr>
				</thead>
				<tbody>
				<%
		
			
				if(offer!=null){
				
				
				for(int i=0; i<offer.size(); i++) {
					MerchantOffersMast tdo=offer.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getShopName()%></td>
		             <td><%=tdo.getMerchantId()%></td>
		              <td><%=tdo.getTerminalId()%></td>
		          <td><%=tdo.getCashBack()%></td>
		    
                  		  </tr>
			      <%} }}%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>





            