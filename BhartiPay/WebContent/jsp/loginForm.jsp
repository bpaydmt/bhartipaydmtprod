<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%
User user = (User) session.getAttribute("User");
%>
<jsp:include page="cssFiles.jsp"></jsp:include>
<div class="section demo">
	<!-- FORM POPUP -->
	<div class="form-popup">
		<!-- CLOSE BTN -->
		<!--<div class="close-btn">
					<svg class="svg-plus">
						<use xlink:href="#svg-plus"></use>
					</svg>
				</div>-->
		<!-- /CLOSE BTN -->

		<!-- FORM POPUP CONTENT -->
		<div class="form-popup-content">
			<h4 class="popup-title">Login to your Account</h4>
			<!-- LINE SEPARATOR -->
			<hr class="line-separator">
			<!-- /LINE SEPARATOR -->
			<form method="post" name="login" id="login_form"
				action="Login">
				
				<font color="red"> <s:actionerror /></font> 
				<font color="blue"> <s:actionmessage /></font>
				
				<input type="hidden" name="user.usertype" value="1"/>

				<s:hidden name="csrfPreventionSalt"
					value="%{#session.csrfPreventionSalt}"></s:hidden>
				<label for="username" class="rl-label">Email/Mobile Number</label> <input
					type="text" name="user.userId" id="username"
					class=" email-mobile no-space mandatory"
					autocomplete="off" placeholder="Email/Mobile Number">
				<label for="password" class="rl-label">Password</label> <input
					type="password" name="user.password" id="password"
					class="mandatory" autocomplete="off"
					placeholder="Password">

				<p class="marginBottom10">
					<a href="ForgotPassword?user.usertype=1" class="marginBottom10">Forgot Password?</a>
				</p>

				
				<hr class="line-separator ">
				
				<button type="button" class="button mid dark"
					onclick="submitVaForm('#login_form')">
					Login <span class="primary">Now!</span>
				</button>
			</form>
			<!-- LINE SEPARATOR -->
			
			<!-- /LINE SEPARATOR -->
			<!-- <a href="#" class="button mid fb half">Login with Facebook</a> 
			<a href="#" class="button mid twt half">Login with Twitter</a> -->
		</div>
		<!-- /FORM POPUP CONTENT -->
	</div>
	<!-- /FORM POPUP -->

</div>