
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommInputBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.LinkedHashSet"%>
<%@page import="java.util.Set"%>
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommPercRangeMaster"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">

function getWalletConfigByUserId(userId)
{  
	$.ajax({
        url:"GetWalletConfigByUserId",
        cache:0,
        data:"config.id="+userId,
        success:function(result){
               document.getElementById('limitform').innerHTML=result;
               
         }
  });  
	return false;
}

/* 
function addRow(event){
	event.preventDefault();
		var row = $(".first-raw:first-child").clone();
		$("#defineComRow").append(row);
	
		

}
function removeRow(event){
	event.preventDefault();

	if($(".first-raw").length > 1){
	$(".first-raw:last-child").remove();
	}

} */

function addTemp(){
	  // Grab the template script
	  var theTemplateScript = $("#address-template").html();

	  // Compile the template
	  var theTemplate = Handlebars.compile(theTemplateScript);

	  // Define our data object

	   return theTemplate();
	}
	$(function(){
	    $(".add-row").click(function(){
	        addrow();
	    });
	      $(document).on("click",".remove-row",function(){
	        removeRow();
	    });
	      $(document).on("blur",".inp1",function(){
	        
	        valideteInputAmountInp1(this)
	    });
	        $(document).on("blur",".inp2",function(){
	        	
	        valideteInputAmountInp2(this)
	    });
	    
	    
	});
	function addrow(){
	  $('#defineComRow').append(addTemp);
	  
	}
	function removeRow(){
	    if( $('#defineComRow .first-raw').length > 1){
	        $('#defineComRow .first-raw:last-child').remove();
	    }
	 
	}

	function valideteInputAmountInp1(elm){
	   var prev =  $(elm).parent().parent().parent().parent().prev().find(".inp2").val();
	  
	   var currentVal = $(elm).val();
	   
	   if(parseInt(prev) >= parseInt(currentVal)){
	      
	     
	           makeitInvalid(elm)
	          insertErrMsg(elm,"This range is already defined.",true);
	        
	    
	    }else{
	        insertErrMsg(elm,"",false);
	        makeitValid(elm)
	    }
	}
	function valideteInputAmountInp2(elm){
	  
	   var prev =  $(elm).parent().parent().parent().prev().find(".inp1").val();  
	   var currentVal = $(elm).val()
	    
	   if(parseInt(prev) >= parseInt(currentVal)){
	      
	           makeitInvalid(elm)
	          insertErrMsg(elm,"Range To should be greater than Range From.",true);
	        
	    
	    }else{
	        insertErrMsg(elm,"",false);
	        makeitValid(elm)
	    }
	}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");
%>
       
       <body>
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    <script id="address-template" type="text/x-handlebars-template">
 	 <div class="first-raw">
												 <div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="text"
																value=""
																name="cpmaster.fixedcharge" class="form-control mandatory amount-vl"
																placeholder="Fixed Charge" >
 <span class='errorMsg'></span>
																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
														
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value=""
																name="cpmaster.rangefrom"  class="form-control inp1 mandatory amount-vl"
																placeholder="Range From" />
																 <span class='errorMsg'></span>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value=""
																name="cpmaster.rangeto" id="rangeTo" class="form-control inp2 mandatory amount-vl"
																placeholder="Range To" />
																 <span class='errorMsg'></span>	
																</div>
															</div>
														</div>
												 </div>
</script>
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Define Commission</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">  
									<div class="row">
									
             					 <div class="col-md-12">
             					 <font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
<form action="GetCommPerc" method="post">
											
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Select Plan</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<s:select list="%{plans}" headerKey="-1"
																		headerValue="Select Plan" id="plan"
																		name="cpmaster.planid" cssClass="form-username"
																		/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Transaction Type</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<s:select list="%{txnType}" headerKey="-1"
																		headerValue="Select Transaction Type" id="txnType"
																		name="cpmaster.txnid" cssClass="form-username"
																		/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 50px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Search</button>
														</div>

													</div>

												</div>	
												 </form> 
<!-- *********************************************************************************************************************** -->
<%
String show=(String)request.getAttribute("show");
if(show!=null&&show.equalsIgnoreCase("true")){
%>
										
									<div id="limitform" >
								<form action="SaveAssignCommission" method="post" id="define-com"> 
																<input type="hidden" name="cpmaster.servicetax" value="14.00">
																<input type="hidden" name="cpmaster.sbcess" value=".50">
																<input type="hidden" name="cpmaster.kycess" value=".50">
																<!-- <input type="hidden" name="cpmaster.bankrefid" value="0"> -->
																<input type="hidden" name="cpmaster.commid" value="<s:property value='%{cpmaster.commid}'/>">
																<input type="hidden" name="cpmaster.planid" value="<s:property value='%{cpmaster.planid}'/>">
																<input type="hidden" name="cpmaster.txnid" value="<s:property value='%{cpmaster.txnid}'/>">
																
												 <div class="row" id="defineComRow">
												 <%
												 CommInputBean comminputbean=(CommInputBean)request.getAttribute("commList");
												 if(comminputbean!=null&&comminputbean.getCommPercRangeMaster()!=null&&comminputbean.getCommPercRangeMaster().size()>0){
												 Set<CommPercRangeMaster> cList=comminputbean.getCommPercRangeMaster();
												if(cList!=null){
												 ArrayList<CommPercRangeMaster> commList= new ArrayList(cList);
												 
												 if(commList!=null&&commList.size()>0){
													 for(int i=0;i<commList.size();i++){
														CommPercRangeMaster commRange=commList.get(i);
													
												 %>
												 <div class="first-raw">
												 <div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Fixed Charge</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="text"
																value="<%=commRange.getFixedcharge() %>"
																name="cpmaster.fixedcharge" id="fixedcharge" class="form-control mandatory amount-vl "
																placeholder="Fixed Charge" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range From</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<%=commRange.getRangefrom() %>"
																name="cpmaster.rangefrom" id="rangeFrom" class="form-control inp1 mandatory amount-vl"
																placeholder="Range From" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range To</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<%=commRange.getRangeto()%>"
																name="cpmaster.rangeto" id="rangeTo" class="form-control inp2 mandatory amount-vl"
																placeholder="Range To" required/>

																</div>
															</div>
														</div>
												 </div>
														
<%}}}}else{ %>
<div class="first-raw">
												 <div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Fixed Charge</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="text"
																value="<s:property value='%{cpmaster.fixedcharge}'/>"
																name="cpmaster.fixedcharge" id="fixedcharge" class="form-control  mandatory amount-vl"
																placeholder="Fixed Charge" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range From</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.rangefrom}'/>"
																name="cpmaster.rangefrom" id="rangeFrom" class="form-control inp1 mandatory amount-vl"
																placeholder="Range From" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range To</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.rangeto}'/>"
																name="cpmaster.rangeto" id="rangeTo" class="form-control inp2 mandatory amount-vl"
																placeholder="Range To" required/>

																</div>
															</div>
														</div>
												 </div>
														
														<%} %>
													</div>
													<div class="row">
													<div class="col-md-12 text-right">
													<a href="#"  class="add-row btn btn-info" >+</a>
														<a href="#" class="remove-row btn btn-info" >-</a></div>
													
													</div>
												
													
													
													
 <div class="row">
														<div class="col-md-8" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Description</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.description}'/>"
																name="cpmaster.description" id="desc" class="form-control"
																placeholder="Description" />

																</div>
															</div>
														</div>
															
					

													</div>
																			
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Commission %</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.commperc}'/>"
																name="cpmaster.commperc" id="commperc" class="form-control mandatory"
																placeholder="Commission %" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Aggregator %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.aggregatorperc}'/>"
																name="cpmaster.aggregatorperc" id="aggregatorperc" class="form-control mandatory"
																placeholder="Aggregator %" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Distributor %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.distributorperc}'/>"
																name="cpmaster.distributorperc" id="distributorperc" class="form-control mandatory"
																placeholder="Distributor %"/>

																</div>
															</div>
														</div>

													</div>
													

										
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Agent %</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.agentperc}'/>"
																name="cpmaster.agentperc" id="agentperc" class="form-control mandatory"
																placeholder="Agent %" />

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Sub-agent %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.subagentperc}'/>"
																name="cpmaster.subagentperc" id="subagentperc" class="form-control mandatory"
																placeholder="Sub-agent %" />

																</div>
															</div>
														</div>
													

													</div>
												
													
												
													

				
													<div class="clearfix"></div>
												</form>
										<div class="row">
										<div class="col-md-4" style="margin-top: 50px;">
															 <button class="btn btn-info submit-form" onclick="submitVaForm('#define-com')" name="FrgtPwd">Submit</button>
														 <button class="btn btn-info" onclick="resetForm('#define-com')">Reset</button>
														</div>
										</div>
									
									
									</div>
									
									<%
}
									%>
									</div>
									
								

                </div>
                

    
            </div>
            
            </br>
              
        </div>
  
</div>

</div>
</div>

</div>
        
        
</div>
        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


