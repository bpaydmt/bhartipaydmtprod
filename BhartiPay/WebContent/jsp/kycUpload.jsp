<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">

function getPassbookSummary(wallet,stDate,endDate)
{  
	
	$.ajax({
        url:"PassbookSummary",
        cache:0,
        data:"inputBean.walletId="+wallet+"&inputBean.stDate="+stDate+"&inputBean.endDate="+endDate,
        success:function(result){
               document.getElementById('transactionSummary').innerHTML=result;
         }
  });  
	return false;
}




$('#countrycode').change(function(){ 
	alert("dfdfsd");
	var selectedval = $(this).val();    
	if(selectedval == 0){
	  $('#desc1').attr('placeholder','dfdsfds');
	}else if(selectedval == 1){
	 $('#desc1').attr('placeholder','sddfsfgdhfg');
	}    
	});
	
/* 
function findAllCommDetails(userid,from,to,imid)
{  
	
	$.ajax({
        url:"FindAllCommDetails.action",
        cache:0,
        data:"userid="+userid+"&from="+from+"&to="+to+"&mid="+imid,
        success:function(result){
        	
               document.getElementById('findAllCommDetails').innerHTML=result;
         }
  });
	
	return false;
} */
</script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
%>
       
      <%--  <body onload="getTransactionSummary('<%=user.getWalletid() %>','<%=fromDate %>','<%=toDate%>');"> --%>
  
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Upload KYC documents</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		
		


            
		
									<div class="row">
										<div class="col-md-12">	<div class="alert alert-warning">
   
    <strong>Note:</strong>  Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.
</div></div>
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
									<s:fielderror></s:fielderror>
									
             					 <div class="col-md-12">

												<form action="SaveKycUpload" method="post" enctype="multipart/form-data">
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<input type="hidden" name="upload.userId"
																		id="userid" value="<%=user.getId()%>">
																		<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
																	<s:select list="%{kycOptions}" headerKey="-1"
																		headerValue="Select Address Proof" id="countrycode"
																		name="upload.addpkycid" cssClass="form-username"
																		requiredLabel="true" required="required"/>



																</div>
															</div>
														</div>


														<div class="col-md-4" style="margin-top: 30px;">
															<input type="file"
																value="<s:property value='%{upload.myFile1}'/>"
																name="upload.myFile1" id="dp"  required="true"
																placeholder="Select Address Proof" required readonly="readonly" required> 
																
														</div>

													</div>
												<div class="row">
														<div class="col-md-8" style="margin-top: 30px;">
															<div class="form-group">
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-message"></span>
																	</div>
																	<input type="text"
																		value="<s:property value='%{upload.addpdesc}'/>"
																		name="upload.addpdesc" id="desc1"
																		class="form-control"
																		placeholder="Enter Description" required>
																</div>
															</div>
														</div>
													</div>

<!-- *********************************************************************************************************************** -->


												<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	
																	<s:select list="%{kycIdOptions}" headerKey="-1" requiredLabel="true"
																		headerValue="Select Id Proof" id="countrycode1"
																		name="upload.idpkycid" cssClass="form-username" required="required"
																		 />


																</div>
															</div>
														</div>


														<div class="col-md-4" style="margin-top: 30px;">
															<input type="file"
																value="<s:property value='%{upload.myFile2}'/>"
																name="upload.myFile2" id="dp" 
																placeholder="Select Id Proof" required readonly="readonly">
														</div>

													</div>
												<div class="row">
														<div class="col-md-8" style="margin-top: 30px;">
															<div class="form-group">
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-message"></span>
																	</div>
																	<input type="text"
																		value="<s:property value='%{upload.idpdesc}'/>"
																		name="upload.idpdesc" id="d1"
																		class="form-control"
																		placeholder="Enter Description" required>
																</div>
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Submit</button>
														</div>
													</div>
													

													<div class="row"></div>
													<div class="clearfix"></div>
												</form>


											</div>
									
									</div>
									
								

                </div>
                

    
            </div>
            
            </br>
              
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


