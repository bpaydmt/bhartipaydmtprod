<%@page import="com.bhartipay.wallet.user.persistence.vo.AgentDetailsView"%>
<%  
AgentDetailsView adv=(AgentDetailsView)request.getAttribute("agentDetails");

%>

<div class="table-responsive">
<table class="table table-bordered table-striped">
	<tr>
	<td><strong>Agent Id</strong></td>
	<td><%=adv.getId() %></td>
	<td><strong>Agent Type</strong></td>
	<td><%=adv.getAgenttype()%></td>
	<td><strong>Agent Name</strong></td>
		<td><%=adv.getName() %></td>
	</tr>
	<tr>
		
		<td><strong>Agent DOB</strong></td>
		<td><%=adv.getDob()%></td>
		<td><strong>PAN</strong></td>
		<td><%=adv.getPan() %></td>
		<td><strong>Agent Mobile</strong></td>
		<td><%=adv.getMobileno()%></td>
	</tr>
	
	
		<tr>
		<td><strong>Address Proof Type</strong></td>
		<td><%=adv.getAddressprooftype() %></td>
		<td><strong>Address Proof Number</strong></td>
		<td><%=adv.getAdhar()%></td>


		<td><strong>Agent Email</strong></td>
		<td><%=adv.getEmailid() %></td>
		
	</tr>
	<tr class="details-heading">
		<td colspan="6">Address</td>
		
	</tr>
	
	<tr>
		<td><strong>Agent Address</strong></td>
		<td><%=adv.getAddress1()%></td>
		<td><strong>Agent City</strong></td>
		<td><%=adv.getCity()%></td>
		
		<td><strong>Agent State</strong></td>
		<td><%=adv.getState()%></td>
	</tr>
	<tr>
		<td><strong>PIN code</strong></td>
		<td><%=adv.getPin()%></td>
	</tr>
	<tr class="details-heading">
		<td colspan="6" >Shop Address</td>
		
	</tr>
	<tr>
		<td><strong>Shop Name</strong></td>
		<td><%=adv.getShopname()%> </td>
		<td><strong>Shop Address</strong> </td>
		<td><%=adv.getShopaddress1() %> <%=adv.getShopaddress2() %></td>
		<td><strong>Shop City</strong></td>
		<td><%=adv.getShopcity()%></td>
		
	</tr>
	<tr>
		
		<td><strong>Shop State</strong></td>
		<td><%=adv.getShopstate()%></td>
		<td ><strong>PIN code</strong></td>
		<td><%=adv.getShoppin()%></td>
		
	</tr>
	
	
	
		
</table>

</div>
<div class="modal-footer" id="agentFooter">

								
													
											
											

      </div>