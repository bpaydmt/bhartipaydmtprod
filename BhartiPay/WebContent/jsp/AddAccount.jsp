<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.lean.LeanAccount"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 

<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             
        ]
    } );
        
} );


function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}


</script>


<script>

(function () {
	 document.getElementById("data_table").style.display = "none";
	 document.getElementById("leanAccountTable").style.display = "none";
	 document.getElementById("scheme").style.display = "none";
	 document.getElementById("findAgent").style.display = "none";
})();


function getAgent()
{
debugger
	var list=document.getElementById("selectAgent");
    var id=list.options[list.selectedIndex].value;
    document.getElementById("agentId").value=id;
    $("#findAgent").show();

	$.ajax({
  		method:'Post',
  		cache:0,  		
  		url:'leanAccountDetails',
  		data:"customerId="+id,
  		success:function(data){
  		var json = JSON.parse(data);

  		var name=json.userName;
  		var walletId=json.walletId;
  		var mobileNo=json.mobileNo;
  		var agentId=json.agentId;
  		var aggregatorId=json.aggregatorId;
  			
		document.getElementById("userName").value=name;
		document.getElementById("walletId").value=walletId;
		document.getElementById("mobileNo").value=mobileNo;
		document.getElementById("agentId").value=agentId;
		document.getElementById("aggregatorId").value=aggregatorId;    		 
  	    }
  	})
}


function findAgent()
{debugger
	    $("#data_table").show(); 
		document.getElementById("data_table").style.display = "block";
		$("#leanAccountTable").show();
		document.getElementById("leanAccountTable").style.display = "block";
		$("#scheme").show(); 
		document.getElementById("scheme").style.display = "block";
		$('#loadDiv').load(document.URL +  '  #loadDiv');
		
		agentAccountDetails();

		
}


function add_row()
{debugger
 var newName=document.getElementById("newName").value;
 var newBank=document.getElementById("newBank").value;
 var newAccount=document.getElementById("newAccount").value;
 var newIfsc=document.getElementById("newIfsc").value;

 var table=document.getElementById("data_table");
 var table_len=(table.rows.length)-1;
 
 var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='name"+table_len+"'>"+newName+"</td>    <td id='bank"+table_len+"'>"+newBank+"</td>     <td id='account"+table_len+"'>"+newAccount+"</td>      <td id='ifsc"+table_len+"'>"+newIfsc+"</td> <td> <input type='button' id='save_button"+table_len+"' value='Save' class='save' onclick='save_row("+table_len+")'> <input type='button' id='delete_button"+table_len+"'  value='Delete' class='delete' onclick='delete_row("+table_len+")'></td>   </tr>";

 document.getElementById("delete_button"+table_len).style.display="none";
 
 var name=document.getElementById("name"+table_len);
 var bank=document.getElementById("bank"+table_len);
 var account=document.getElementById("account"+table_len);
 var ifsc=document.getElementById("ifsc"+table_len);
	
 var nameData=name.innerHTML;
 var bankData=bank.innerHTML;
 var accountData=account.innerHTML;
 var ifscData=ifsc.innerHTML;
	
 name.innerHTML="<input type='text' id='nameInput"+table_len+"' value='"+nameData+"' required >";
 bank.innerHTML="<input type='text' id='bankInput"+table_len+"' value='"+bankData+"'  required >";
 account.innerHTML="<input type='text' id='accountInput"+table_len+"' value='"+accountData+"' required  >";
 ifsc.innerHTML="<input type='text' id='ifscInput"+table_len+"' value='"+ifscData+"' required >";
 
 document.getElementById("newName").value="";
 document.getElementById("newBank").value="";
 document.getElementById("newAccount").value="";
 document.getElementById("newIfsc").value="";

 
}

function save_row(no)
{
debugger

 var userId=document.getElementById("agentId").value;
 var aggregatorId=document.getElementById("aggregatorId").value;
 var mobileNo=document.getElementById("mobileNo").value;
 var walletId=document.getElementById("walletId").value;
 var name=document.getElementById("nameInput"+no).value;
 var bank=document.getElementById("bankInput"+no).value;
 var account=document.getElementById("accountInput"+no).value;
 var ifsc=document.getElementById("ifscInput"+no).value;

 if (name == null || bank == null || account == null || ifsc == null) {      

    alert("fill all columns");
    $('#msg').html("fill all columns");
    $("#msg").show();
 	setTimeout(function() { $("#msg").hide(); }, 3000); 
    return false; 
 } 

 document.getElementById("save_button"+no).style.display="none"; 
// document.getElementById("delete_button"+no).style.display="block";
     $.ajax({
			method:'Post',
			cache:0,  		
			url:'saveAccount',
			data:'userId='+userId+'&name='+name+'&bank='+bank+'&account='+account+'&ifsc='+ifsc+'&aggregatorId='+aggregatorId+'&mobileNo='+mobileNo+'&walletId='+walletId,
			
			success:function(response){
			    var json = JSON.parse(response);
			    if(json.status==true)
                {
			     agentAccountDetails();
			     $('#msg').html("Account details saved");
            	 $("#msg").show();
             	 setTimeout(function() { $("#msg").hide(); }, 3000);
             	document.getElementById("row"+no+"").outerHTML="";
	            }else
	            {
                    if(json.accountNumber=="0")
		            {
                      $('#msg').html("Account details already exist.");
  		              $("#msg").show();
  		           	  setTimeout(function() { $("#msg").hide(); }, 3000); 
    		        }else{
            	      document.getElementById("save_button"+no).style.display="block"; 
	            	  document.getElementById("delete_button"+no).style.display="none";
		              $('#msg').html("You have already added maximum five Account details");
		               //$('#msg').html("Account details  not saved");
		           	  $("#msg").show();
		           	  setTimeout(function() { $("#msg").hide(); }, 3000);   
	            }
	          }
			}
		})
 
}


function agentAccountDetails()
{debugger
	var userId=document.getElementById("agentId").value;
	if(document.getElementById('leanAccountTable')){
		deleteData();
		}
	
	  $.ajax({
			method:'Post',
			cache:0,  		
			url:'agentAccountDetails',
			data:'userId='+userId,
			success:function(response){
	   		var count=0;
	   		$.each(JSON.parse(response), function(idx, obj) {
	   		   count++;
	   		  $('#leanAccountTable').find('tbody')
	            .append('<tr><td width="4%">'+count+'</td> <td width="10%" >'+obj.walletId+'</td> <td width="10%">'+obj.mobileNo+'</td>  <td width="10%"><input type="text" id="name'+obj.id+'"  value=\'' +obj.name+ '\' /></td> <td width="10%"><input type="text" id="bank'+obj.id+'" value=\'' +obj.bankName+ '\' /></td> <td width="10%"><input type="text" id="account'+obj.id+'"  value=\'' +obj.accountNumber+ '\' /></td> <td width="10%"><input type="text" id="ifsc'+obj.id+'" value=\'' +obj.ifscCode+ '\' /></td> <td align="center">  <input type="button" value="Update" onclick="updateRecord(\'' +obj.id+ '\')" id="updateButtons'+obj.id+'" />   <input type="button" value="Delete" onclick="deleteRecord(\'' +obj.id+ '\')" id="deleteButtons'+obj.id+'" /></td> <tr>');
	   
	   		});
	   		 
	   		}
	  })

}

function deleteData()
{
    var myTable = document.getElementById("leanAccountTable");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
}


function deleteDataRow()
{
    var myTable = document.getElementById("data_table");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
}

function updateRecord(id)
{
debugger

var name = document.getElementById("name"+id.toString()).value; 
var bank = document.getElementById("bank"+id.toString()).value; 
var account = document.getElementById("account"+id.toString()).value; 
var ifsc = document.getElementById("ifsc"+id.toString()).value; 

	
    $.ajax({
		method:'Post',
		cache:0,  		
		url:'updateUserAccount',
		data:'id='+id+'&name='+name+'&bank='+bank+'&account='+account+'&ifsc='+ifsc,
		
		success:function(response){
		    var json = JSON.parse(response);
		    if(json.status==true)
            {
		     $('#msg').html("Account details updated");
        	 $("#msg").show();
         	 setTimeout(function() { $("#msg").hide(); }, 3000);
            }else
            {
              $('#msg').html("Account details not updated");
           	  $("#msg").show();
           	  setTimeout(function() { $("#msg").hide(); }, 3000);   
            }
	        
		}
	})
	
}


function deleteRecord(id)
{
if(document.getElementById('leanAccountTable')){
	deleteData();
	}
	
    $.ajax({
		method:'Post',
		cache:0,  		
		url:'deleteUserAccount',
		data:'id='+id,
		
		success:function(response){
		    var json = JSON.parse(response);
		    if(json.status==true)
            {
		     agentAccountDetails();
		     $('#msg').html("Account details deleted");
        	 $("#msg").show();
         	 setTimeout(function() { $("#msg").hide(); }, 3000);
            }else
            {
              $('#msg').html("Account details not deleted");
           	  $("#msg").show();
           	  setTimeout(function() { $("#msg").hide(); }, 3000);   
            }
	        
		}
	})
	
}



</script>

</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
      
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  

<%
    User user = (User) session.getAttribute("User");

%>             
 
<div class="box2">
	<div class="box-inner">
	<div class="box-header">
		<h2>Add Account</h2>
	 </div>
	<div class="box-content">
	<div class="row">
	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
	<div class="row">
	<div class="container">
	<div class="col-sm-4"> 
	<s:select list="%{agentList}" headerKey="All"
				headerValue="Select Agent" onchange="getAgent()" id="selectAgent"
				 cssClass="form-username" requiredLabel="true"/>
				   <input type="hidden"    id="agentId"  name="agentId" /> 
				   
			     <input type="hidden"    id="walletId"  name="walletId" /> 
			     <input type="hidden"    id="mobileNo"  name="mobileNo" /> 
			     <input type="hidden"    id="userName"  name="userName" /> 
			     <input type="hidden"    id="aggregatorId"  name="aggregatorId" /> 
	
	</div></div></div>	</div>
																			
	  <!-- <div class="form-group col-md-3 txtnew col-sm-4 col-xs-6 text-left "> -->
		<div class="col-md-2">
		<input type="button" id="findAgent" class="btn btn-success btn-fill" style="margin-top: 22px;"
					onclick="findAgent()" value="Submit"  />
		</div>
		 <div class="col-md-2" id="msg" style="color: green ;" ></div>
        </div>
		</div>
	</div>
	</div>	
<div id="loadDiv">
<table align='center' cellspacing=2 cellpadding=5 id="data_table" border=1 >
<tr>
<td>Name</td>
<td>Bank Name</td>
<td>Account Number</td>
<td>Ifsc Code</td>
<td>Action</td>
</tr>

<tr>
<td><input type="text" id="newName" required   ></td>
<td><input type="text" id="newBank" required></td>
<td><input type="text" id="newAccount" required></td>
<td><input type="text" id="newIfsc" required></td>

<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
<div id="msg" style="color: red ;" ></div>
</tr>

</table></div>
<br><br>
<div class="box-header" id="scheme">
	<h2>Account Details</h2>
</div>
<div id="tableDiv">	
 <table align="center" border="5" cellpadding="5" cellspacing="5"  id="leanAccountTable" class="leanAccountTable" style="display: block;float: left;width: 100%;">
	
  <thead>  
      <tr>
		<td align="center" style="width: 3%;"><u><b>Sr.No<b></u></td>
		<td align="center" style="width: 15%;"><u><b>Wallet Id<b></u></td>
		<td align="center" style="width: 15%;"><u><b>Mobile No<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Name<b></u></td>
		<td align="center" style="width: 15%;"><u><b>Bank Name<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Account Number<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Ifsc Code<b></u></td>
		<td align="center" style="width: 10%;"><u><b>Action<b></u></td> 
		
	  </tr>
  </thead>				
 
 <tbody>
  
</tbody>
</table>	
        <!-- contents ends -->
  	 </div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<%-- <jsp:include page="footer.jsp"></jsp:include> --%>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<%--   <script src="./js/Price.js"></script> --%>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->



</body>
</html>


