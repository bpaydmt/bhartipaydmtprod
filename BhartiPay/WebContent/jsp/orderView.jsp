<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Orders View  - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Orders View  - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Orders View  - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Orders View  - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Orders View  - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
              
            }
        ]
    } );
    $('#dp').datepicker({
    	 language: 'en',
    	 maxDate: new Date(),
    	
    });
   $("#dp").blur(function(){
   	//alert()
   	  $('#dp1').datepicker({
   	     	 language: 'en',
   	     	 minDate: new Date($('#dp').val()),
   	     	maxDate: new Date(),
   	     });	
   })
    
} );
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Orders View </h2>
		</div>
		<div class="box-content row">
		<font color="red">
		<s:actionerror/>
		</font>
		<font color="blue">
		<s:actionmessage/>
		</font>
			<form action="OrdersViewSummary" method="post">
<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateFrom">Date From:</label> --> <br> <input
										type="hidden" name="inputBean.walletId" id="userid"
										value="<%=user.getWalletid()%>"> <input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dp"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</lmabel> --> <br> <input
										type="text" value="<s:property value='%{inputBean.endDate}'/>"
										name="inputBean.endDate" id="dp1"
										class="form-control datepicker-here2" placeholder="End Date"
										data-language="en" required>
								</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
											<input class="btn btn-info" id="submit"
											type="reset" value="Reset">
									</div>
								</div>
							</form>	
		</div>
	</div>
	</div>
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Txn Date</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Description</u></th>
						<th><u>Order Amount</u></th>
						<th class="hidden-col-pdf"><u></u></th>
						
												
						
						
					</tr>
				</thead>
				<tbody>
				<%
							List<PassbookBean> pb=(List<PassbookBean>)request.getAttribute("listResult");

				if(pb!=null){
				
				
				for(int i=0; i<pb.size(); i++) {
					PassbookBean tdo=pb.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getTxndate()%></td>
		             <td><%=tdo.getTxnid()%></td>
		              <td><%=tdo.getTxndesc()%><%if(tdo.getPayeedtl()!=null){%>-<%=tdo.getPayeedtl()%><%} %></td>
		           <td><%=d.format(tdo.getTxndebit())%></td>
		             <td>
		             <%if(tdo.getStatus()==1){ %>
		             <input type="submit" value="Refund" data-toggle="modal" data-target="#myModal<%=tdo.getTxnid()%>" class="btn btn-sm btn-block btn-success">
		           
					<%} %>
					<%if(tdo.getStatus()==2){ %>
		            <font color="green">Refund initiated.</font>
					<%} %>
					<%if(tdo.getStatus()==3){ %>
		            <font color="green">Refund rejected.</font>
					<%} %>
					<%if(tdo.getStatus()==4){ %>
		            <font color="green">Refund accepted.</font>
					<%} %>
					
					
					<div id="myModal<%=tdo.getTxnid()%>" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Refund Request</h4>
							      </div>
							      <div class="modal-body">
							      
							      <form action="RequestRefund" method="post">
							      <div class="form-group">
							      <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
	<!-- private String aggreatorId;
	private String userId;
	private String walletId;
	private String txnid;
	private String reason; -->
							     
							      <input type="hidden" name="refundBean.txnid" value="<%=tdo.getTxnid()%>">
							      
							      <label>
							      Specify reason
							      </label> <input type="text" name="refundBean.reason" id="card<%=tdo.getTxnid()%>" class="form-control" required/>
							      
							      </div>
							    
							      <div class="text-right">
								          <input type="submit" class="btn btn-info" value="Refund">
								  </div>
							     
							      </form> 
							      
							      </div>
							      
							     
							    </div>
							
							  </div>
							</div>
					</br>
		             
		             
		             </td>
		          
                  		  </tr>
			      <% }
			      }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>





