<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<% response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %> 
<%@ page session="true" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<jsp:include page="theams.jsp"></jsp:include>
<link href="./css/font-awesome.css" rel="stylesheet">
      <link rel="stylesheet" href="./css/bootstrap.min3.css">
            <link rel="stylesheet" href="./css/font-awesome.min.css">
        <link rel="stylesheet" href="./css/form-elements.css">
        <link rel="stylesheet" href="./css/style.css">
         <link rel="stylesheet" href="./css/customStyle.css">\
           <script src="./js/jquery.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/jquery.backstretch.min.js"></script>
        <script src="./js/scripts.js"></script>
         <script src="./js/validateWallet.js"></script>
<%-- <script type="text/javascript" src = "js/user/login.js"></script> --%>
</head>
<body >
    
		 
		 
	      <div class="container-fluid">
        <div class="navbar-header">
          <%
             User user=(User)session.getAttribute("User");
             String logo=(String)session.getAttribute("logo");
             String banner=(String)session.getAttribute("banner");
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>"  style="width:150px; height:auto; margin-left:7%;"/>
					<%
						}else{
					%>
					 <img alt="Bhartipay" src="./images/wallet_logo.png" style="width:150px; height:auto; margin-left:7%;"/>
					<%} %>
               
        </div>
       

    </div>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4 style="color:#999; padding-left:8px;">Please enter your OTP here.</h4>	
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-edit"></i>
                        			
                        		</div>
                            </div>
                            <div class="form-bottom">

<form id="otpForm" action="ValidateUOTPC" method="POST" class="login-form" >
			                   
			                    <div class="form-group">
			                   
			                    <font color="red"><s:actionerror></s:actionerror></font>
			                     <font color="blue"><s:actionmessage/></font>
			                   
			                    </div>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                    	<input type="hidden" name="user.userId" placeholder="Username" value="<s:property value='%{user.userId}'/>" class="form-username form-control" id="userid" >
			                       <input type="text" name="user.otp" placeholder="Enter OTP" value="<s:property value='%{user.otp}'/>" class="form-username form-control mandatory otp-vl" id="otp" >
			                        
			                        </div>
			                        
			                        <button class="btn" name="FrgtPwd"  onclick="submitVaForm('#otpFrom')">Submit</button>
                                    <br>
                                    <br>
                                    
                                   
			                    </form>
			                    
			                   
			                     <s:form action="uOtpResend" method="POST" cssClass="login-form">
			                     <input type="hidden" name="user.userId" value="<s:property value='%{user.userId}'/>"/>
			                     <input type="submit" class="login-form" style="background: #ff6e26;
border: none;
padding: 4px 7px;
color: #FFF;
font-size: 12px;" value="Resend OTP"/>
			                     </s:form>
		                    </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            
        </div>
<div style="margin-top: 70px;">
       <jsp:include page="footer1.jsp"/>
       </div>
        <!-- Javascript -->
   
        
        <!--[if lt IE 10]
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>	 

<head>
<meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>
<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
</head>
</html>





