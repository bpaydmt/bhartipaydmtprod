<!DOCTYPE html>



<html>
<head>

 <link href="../css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
<header>
<div class="container">
<a href="" class="logo"><img src="../images/logo116x50.png" style="margin-top: 15px;
    margin-bottom: 15px;"></a>
</div>
</header>
<div class="container" style="margin-top:2%; margin-bottom:1%;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" background-color:#0dc; padding:10px;">
    	<h1 class="BhartiPay_h1">Terms &amp; Conditions</h1>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">&nbsp;</div>
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
        	<h3>In using this website you are deemed to have read and agreed to the following terms and conditions:</h3>
    	<p style="font-family:Verdana, Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">BhartiPay (owned and operated by APPNIT Technologies Pvt. Ltd. ) offers services, which includes the recharge services of various telecom operators Post Paid Bill Payment, DTH Recharge and Electricity Bill Payment (collectively called "Services") to its registered / Unregistered users. By visiting or accessing our "Site / Application", the "User" (which means registered user as well as Unregistered User or visitor) agrees to be bound by the following terms and conditions and "User" acceptance of this agreement is reaffirmed each time "User" access the service. BhartiPay reserves the right, at its sole discretion, to change, modify, add, or delete portions of these Terms of Use at any time without further notice. The "User" shall re-visit the `Terms of Use' link from time to time to stay abreast of any changes that BhartiPay may introduce. By becoming a registered user of BhartiPay and our affiliate sites, you agree to be contacted by BhartiPay and our affiliates via communication modes including but not limited to email, SMS, MMS, and telephonic calls.</p>       
        <h3>Payment</h3>
         
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">We accept Visa and Master Credit Card, Debit Cards and Net banking as mode of payments which are processed through our payment gateway partner TimesOfMoney Ltd. None of the payment related information is shared with us as, you fill all the information on your bank's site only.</p>
        
        <h3>Recharge</h3>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">BhartiPay has tied up with a third party vendor for the recharge and Bill Payment. BhartiPay is not responsible for any delay, pricing or cancellation of recharge from mobile operator’s end. However, best efforts are made from our side to keep the service above acceptance levels of the User.</p>
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">
        	<ul style="list-style:inside decimal;">
            	<li>User is solely responsible for the selection of mobile operator and the recharge amount.</li>
                <li>BhartiPay is not responsible for talk-time given against recharge done through BhartiPay as this is purely at mobile operator's disposal.</li>
                <li>BhartiPay is not responsible for Network / Internet / Data Connection related issue on the side of the user while using the Payment Option.</li>
                <li>The user hereby agrees that by using the facility of online recharge / Bill Payment from BhartiPay, the user has agreed to pay out of will any extra charges on account of online service/ sale of online recharge.</li>
            </ul>
       	</p>
        
        <h3>Refund</h3>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">In case the user has completed the transaction and not received the selected recharge amount / Or Bill Payment is not updated after the due time period, the user will have to provide the complete transaction details along with contact details to customer support at BhartiPay. The matter will be resolved after cross checking with the concerned service provider vendor after which refund, if any, will be made to the users account. This process may take 7 days or longer depending on the response from the vendor. BhartiPay will not be liable for late action in such a case.</p>
        
        <h3>Channel Subscription</h3>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">By providing your data, you agree to be communicated by us via email and sms to facilitate your navigation of and shopping from the website. We may communicate promotional offers and transactional messages periodically and as and when required.</p>
        
        <h3>Coupon Redemption</h3>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">Coupon redemption by User is purely subjected to specified terms and conditions described by the concerned retailer. Coupons are issued on behalf of the respective retailer, whose coupon user has selected. Hence, any damages, losses incurred to the end user by using the selected coupon is not the responsibility of BhartiPay.</p>
        
        <h3>Disclaimer</h3>
        <h4>Exclusions and Limitations</h4>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">The information on this web site is provided on an "as is" basis. To the fullest extent permitted by law, this Company:<br> excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or omissions in this website and/or the Company's literature; and excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages.</p>
        
        <h4>Limitation of Liability</h4>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">All transactions on the BhartiPay, monetary and otherwise, are non-refundable, non-exchangeable and non-reversible, save and except in the event of proven gross negligence on the part of BhartiPay or its representatives, in which event the user agrees that the user shall only be entitled to a refund of the amount actually paid by the user and actually received by BhartiPay with respect to the recharge done by the user.</p>
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">This limitation applies whether the alleged liability is based on contract, tort, negligence, strict liability, or any other basis, even if BhartiPay has been advised of the possibility of such damage. Because some jurisdictions do not allow the exclusion or limitation of incidental or consequential damages, in such jurisdictions BhartiPay’s liability shall only be limited to the extent of the amount actually paid for, by any user while availing the services available on the BhartiPay website.</p>
        
        <h4>Changes to the Terms and conditions</h4>
        
        <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-style:normal; font-weight:normal; text-align:left; line-height:22px; word-spacing:1px; color:#333333;">The site may Change its terms and conditions at any time, at its sole discretion, without prior information. If there is any change made in the Terms and conditions that will be notified to the User by posting an announcement on the site so that the User is always aware of the Updating of the site from time to time. The User is responsible for regularly reviewing these Terms of Service to be updated about the changes.</p>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">&nbsp;</div>	
    </div>
</div>

<div class="container-fluid" style="/* background-image: url('./newmis/img/hbanner.png'); */">
<hr>

  

    <footer class="row">
        <p class="col-md-5 col-sm-5 col-xs-12 copyright">&copy;2017 BhartiPay All rights reserved.</p>

        <p class="col-md-6 col-sm-6 col-xs-12 pull-right">
      
                   
    </footer>
 </div>
<!-- Show the full image here -->
  						<div class="modal fade" id="myModal" role="dialog" tabindex='-1'>
    					<div class="modal-dialog modal-lg" style="width:800px;">
 <div class="modal-content">
        				<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          
        			</div>
       				 <div class="modal-body">
      
         				 <img src="./newmis/img/PCICertificate1.jpg" width="100%">
        			</div>
       				</div>
					</div>
					</div>
</body>
</html>