<%@page import="java.util.Map"%>
<% response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %> 
<%@ page session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script type="text/javascript" src = "js/user/login.js"></script>
<script type="text/javascript">
function passwordOption(countrycode)
{    
	alert(countrycode);
	alert(document.getElementById('countrycode').value);
	$.ajax({
        url:"GetWalletConfig",
        cache:0,
        data:"user.countrycode="+countrycode,
        success:function(result){
        	alert(result);
               document.getElementById('passwordoption').innerHTML=result;
         }
  });  

}
</script>
</head>
<body >
    
	 
		 
		 
	      <div class="container-fluid">
        <div class="navbar-header">
            <img src="assets/img/wallet_logo.png">
        </div>
      

    </div>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4 style="color:#999; padding-left:8px;">Please Enter your detail here</h4>	
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-edit"></i>
                        			
                        		</div>
                            </div>
                            <div class="form-bottom">
                            <%
    if(request.getParameter("msg")!= null)
        out.println("<center>"+request.getParameter("msg")+"</center><br><br>");
%> 
<s:form action="SaveSignUpUser" method="POST" cssClass="login-form" theme="css_xhtml">
			                    <!-- <form role="form" action="" method="post" class="login-form"> -->
			                    <div class="form-group">
			                   <font color="blue"><s:actionmessage></s:actionmessage></font>
			                    <font color="red"><s:actionerror></s:actionerror></font>

			                    </div>
			                    	<%-- <div class="form-group">
			                    		<label class="sr-only" for="form-username">Select Country</label>
			                       <input type="text" value="<s:property value='%{user.countryCode}'/>" name="user.countryCode" placeholder="Select Country" class="form-username form-control" id="u">
			                       <s:select list="%{countryCode}" headerKey="-1" headerValue="Select Country" id="countrycode" name="user.countrycode" cssClass="form-username form-control" onchange="passwordOption(document.getElementById('countrycode').value);"/> 
			                        
			                        
			                        </div> --%>
			                         <div class="form-group">
			                    		<label class="sr-only" for="form-username">Name</label>
			                       <input type="text" name="user.name" placeholder="Name" class="form-username form-control" id="name"  value="<s:property value='%{user.name}'/>" required >
			                        
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Email</label>
			                       <input type="text" name="user.emailid" placeholder="Email" class="form-username form-control" id="email"  value="<s:property value='%{user.emailid}'/>" required>
			                        
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Mobile Number</label>
			                       <input type="text" name="user.mobileno" placeholder="Mobile Number" class="form-username form-control" id="mobile"  value="<s:property value='%{user.mobileno}'/>" required >
			                        
			                        </div>
			                        
			                        <% Map<String,String> mapResult=(Map<String,String>)request.getSession().getAttribute("mapResult");
			                        if(mapResult!=null&&mapResult.get("emailvalid")!=null&&Double.parseDouble(String.valueOf(mapResult.get("emailvalid")))==0){
			                        %>
			                        <%= String.valueOf(mapResult.get("emailvalid"))%>
			                        <div id="passwordoption">
			                        <div class="form-group">
			                    	<label class="sr-only" for="form-username">Password</label>
			                       <input type="password"   name="user.password" placeholder="Password" class="form-username form-control" id="password"   required >
			                        
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Confirm Password</label>
			                       		<input type="password"   name="user.confirmPassword" placeholder="Confirm Password" class="form-username form-control" id="password" required>
			                        
			                        </div>
			                        </div>
			                        <%
			                        }
			                        %>
			                    <!--     <button type="submit" class="btn">Submit</button> -->
			                        <button type="submit" class="btn" name="FrgtPwd" onclick="return frmValidation();">Submit</button>
                                    <br>
                                    <br>
                                    <a href="Logoff">Click here to login</a>
			                    </s:form>
		                    </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            
        </div>
<div style="margin-top: 70px;">
       <jsp:include page="footer1.jsp"/>
       </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>	 

<head>
<meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>
<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
</head>
</html>





