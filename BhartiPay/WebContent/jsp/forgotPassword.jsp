

<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%
  response.setHeader("Strict-Transport-Security", "max-age=7776000; includeSubdomains");
  response.addHeader( "X-FRAME-OPTIONS", "DENY" );
  response.setHeader("X-Frame-Options", "SAMEORIGIN");
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer(); 
 // String pgToken=  net.pg.utility.Helper.getEncodedValue(); 
  %> 
<%@taglib uri="/struts-tags" prefix="s" %>

<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");


%>
<% String merchantName = mapResult.get("appname");
	String domainName = mapResult.get("domainName");
	String favicon=mapResult.get("favicon");
	String themes=mapResult.get("themes");
			
	%>
<!DOCTYPE html>
<html>
<head>
  <title><%=mapResult.get("caption") %></title>
  
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/whitelabel.css" />


<link href="./css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="./css/LoginPage.css" rel='stylesheet' type='text/css' />
<link rel="shortcut icon" href="./images/<%=favicon%>">

</head>

<body class="menu-style<%=themes%>">

<%
String logo=(String)mapResult.get("logopic");
String banner=(String)mapResult.get("bannerpic");
%>


<div class="login-page">
  <div class="form">
     <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>"  style="width: auto;
    max-height: 100px;
    margin-top: 10px;
    margin-left: 7%;"/>

					<%
						} else {
					%>

					 <img alt="Bhartipay" src="./images/wallet_logo.png" style="width: auto;
    max-height: 100px;
    margin-top: 10px;
    margin-left: 7%;"/>

						<%} %>
             <font color="red"><s:actionerror></s:actionerror></font>
  <h5 style="text-align:left">Please Recover Your Password Here</h5>
   <form action="ForgotPasswordOTP" method="POST" id="forget-pwd1" class="login-form" name="fpo"  autocomplete="off">

			                    <!-- <form role="form" action="" method="post" class="login-form"> -->
			                   
			                    		<label class="sr-only" for="form-username">Username</label>
			                       <input type="text" name="user.userId" placeholder="User Id" class="form-username  mandatory no-space " required="required" id="u">
			                        <%-- <s:textfield name="user.userName" cssClass="form-username form-control" id="form-username"  maxlength="20"/> --%>
			                        
			                 
			                        
			                    <!--     <button type="submit" class="btn">Submit</button> -->
			                        
			                    </form>
			                    <button  onclick="submitVaForm('#forget-pwd1',this)"  name="FrgtPwd" class="submit"> Submit</button>
                                 
                                    <a href="UserHome.action" style="padding: 10px;clear: both;float: left;width: 100%;">Click here to login</a>
                                      

  </div>
</div>


































        <!-- Javascript -->
       <script src="./js/jquery.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/jquery.backstretch.min.js"></script>
        <script src="./js/scripts.js"></script>
         <script src="./js/validateWallet.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>


<head>
<meta http-equiv='Pragma' content='no-cache'/>
<meta http-equiv='Cache-Control' content='no-cache'/>
<meta http-equiv="Expires" content="-1"/>
</head>
</html>
