<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >


<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>

<script type="text/javascript">
  function showSearchDetails(){

  	 var div = $("#searchDetails")
  	 
  	 if(div.is(":hidden")){
  	  div.show()
  	  $("#showDetailsLink span").text("Hide details")
  	  $("#showDetailsLink .fa").addClass("fa-minus-square")
  	  $("#showDetailsLink .fa").removeClass("fa-plus-square")
  	 }else{
  	  div.hide()

  	 
  	  $("#showDetailsLink span").text("Show details")
  	  $("#showDetailsLink .fa").removeClass("fa-minus-square")
  	  $("#showDetailsLink .fa").addClass("fa-plus-square")
  	 }
  	}

</script>


<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#to').val(start.format('DD-MMM-YYYY'));
         $('#from').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
   }); 
</script>

<script type="text/javascript"> 
	$(document).ready(function() {

		    $('#cashDepositDate').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		    });
	 }) 	
</script>

<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>
<style>
.row-m-15 {
	/*margin: 5px 8px 10px;*/
	border-bottom: 1px solid #ccc;
	padding: 1px 0 8px;
	float: left;
	width: 100%;
}

.box-content {
	padding: 10px;
	background-color: #fff;
	/*     margin-top: 15px;
    margin-bottom: 15px; */
}

.custom-file-input {
	color: transparent;
}

.custom-file-input::-webkit-file-upload-button {
	visibility: hidden;
}

.custom-file-input::before {
	content: 'Upload Payment Slip';
	color: black;
	display: inline-block;
	background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
	border: 1px solid #999;
	border-radius: 3px;
	padding: 6px 12px;
	outline: none;
	white-space: nowrap;
	-webkit-user-select: none;
	cursor: pointer;
	text-shadow: 1px 1px #fff;
	/* font-weight: 700;
  font-size: 10pt; */
	border-radius: 15px;
	position: absolute;
	top: 22px;
}

.custom-file-input:hover::before {
	border-color: black;
}

.custom-file-input:active {
	outline: 0;
}

.custom-file-input:active::before {
	background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}

input[type=file] {
	display: inline-block !important;
}

.custom-date {
	color: black;
	display: inline-block;
	/* background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
	 */
	 background:none;
	/*  border: 1px solid #999; */
	border-radius: 3px;
/* 	padding: 6px 12px; */
	outline: none;
	white-space: nowrap;
	-webkit-user-select: none;
	cursor: pointer;
	text-shadow: 1px 1px #fff;
	/* font-weight: 700;
  font-size: 10pt; */
	border-radius: 15px;
	/* position: absolute; */
	top: 22px;
	width: 320px;
    right: 50px;
}

</style>
</head>

<% 
  User user = (User) session.getAttribute("User");
  Date myDate = new Date();
  System.out.println(myDate);
  SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
  String toDate=format.format(myDate);
  Calendar cal = Calendar.getInstance();
  cal.add(Calendar.DATE, -0);
  Date from= cal.getTime();    
  String fromDate = format.format(from);
  Object[] commDetails=(Object[])session.getAttribute("commDetails");
  Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

  List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");  
%>



<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->



	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<!-- contents starts -->

	<div id="main" role="main">
		<div id="content">
			<div class="row">

				<div class="col-md-12 box" id="hidethis2">

					<%if("OAGG001059".equalsIgnoreCase(user.getAggreatorid()) || "OAGG001061".equalsIgnoreCase(user.getAggreatorid()) ){ %>

					<div class="box-inner">
						<div class="box-header">
							<h2>Load Wallet</h2>
						</div>
						<div class="box-content">
							<font style="color: red;"> <s:actionerror />
							</font> <font style="color: blue;"> <s:actionmessage />
							</font>

							<form action="SaveCashDeposit.action" method="post"
								enctype="multipart/form-data" id="form_name">

								<div class="row">

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <label>
											Mode <font color="red"> *</font>
										</label>
										<s:select list="%{txnTypeList}" id="drop-neft"
											onchange="dropInfo(this)" name="depositMast.type"
											headerKey="-1" headerValue="Select mode"
											class="mandatory" required="true">
										</s:select>
									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label>Bank <font color="red"> *</font>
										</label>
										<s:select list="%{bankNameList}" id="drop-neft"
											name=" depositMast.bankName" headerKey="-1"
											headerValue="Select Bank" class="mandatory"
											required="true" value="%{depositBank}"></s:select>

									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label for="apptxt"> Amount <font color="red">
												*</font>
										</label> <input class="form-control onlyNum mandatory" type="text"
											id="amount" maxlength="7" placeholder="Enter Amount"
											name="depositMast.amount" style="margin-left: 0px;"
											value="<s:property value='%{depositMast.amount}'/>" Required>

									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<div id="NEFT">
											<div class="form=group">
												<label>Transaction Id <font
													color="red"> *</font>
												</label> <input type="text" class="form-control mandatory" placeholder="NEFT/RTGS/IMPS Id"
													name="depositMast.neftRefNo" style="margin-left: 0px;"
													value="<s:property value='%{depositMast.neftRefNo}'/>"
													Required />
											</div>
										</div>
									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label for="apptxt">Remarks <font color="red">
												*</font>
										</label> <input class="form-control userName mandatory" type="text"
											id="branchName" maxlength="20" placeholder="Remarks"
											name="depositMast.branchName" style="margin-left: 0px;"
											value="<s:property value='%{depositMast.branchName}'/>"
											Required>

									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<div id="RECIEPT" style="">
											<label> </label> <input type="file"
												class="custom-file-input" name="depositMast.myFile1"
												value="<s:property value='%{depositMast.myFile1}'/>" />
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<!-- <input type="file" class="custom-file-input"> -->
										<button class="btn btn-success submit-form"
											style="margin-top: 25px;"
											onclick="submitVaForm('#form_name',this)">Submit</button>
										<input type="reset" style="margin-top: 25px;"
											onclick="resetForm('#form_name')"
											class="btn-primary btn btn-fill" />
									</div>

									<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
              	     %>


								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
									style="margin-top: -11px;">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px; color: #db791f;"> <span>Hide
											details</span> <i class="fa fa-minus-square"
										style="margin-left: 5px" aria-hidden="true"></i>
									</a>
								</div>
							</form>


							<div id="searchDetails" style="display: none; font-size: 12px">
								<div class="row-m-15" style="margin-top: 25px;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-2">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>

									<div class="col-md-4">
										<strong> Bank Branch</strong>
									</div>
								</div>
								<div class="row-m-15">
									<div class="col-md-3">State Bank Of India</div>
									<div class="col-md-2">SBIN0000691</div>
									<div class="col-md-3">37714892076</div>
									<div class="col-md-4">Main Branch Parliament Street New
										Delhi</div>
								</div>

								<div class="row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-2">ICIC0000031</div>
									<div class="col-md-3">003105035926</div>
									<div class="col-md-4">Sector 18 Noida</div>
								</div>

								<!-- <div class="row-m-15">
									<div class="col-md-3">Andhra Bank</div>
									<div class="col-md-2">ANDB0001106</div>
									<div class="col-md-3">110611100007550</div>
									<div class="col-md-4">Sector 19 Noida</div>
								</div> -->

								<div class="row-m-15">
									<div class="col-md-3">BANDHAN BANK</div>
									<div class="col-md-2">BDBL0001983</div>
									<div class="col-md-3">10200005359339</div>
									<div class="col-md-4">SECTOR 51 NOIDA, UTTAR PRADESH</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. AAHCB4709M and Name of the company
										Bhartipay Services Private Limited</p>
									<p>CIN NO:- U74999DL2017PTC324436</p>
									<p>GST No:- 09AAHCB4709M1ZB</p>
								</div>
							</div>

							<%}
              	%>

				<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001070")) {
              	     %>


								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
									style="margin-top: -11px;">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px; color: #db791f;"> <span>Hide
											details</span> <i class="fa fa-minus-square"
										style="margin-left: 5px" aria-hidden="true"></i>
									</a>
								</div>
							</form>


							<div id="searchDetails" style="display: none; font-size: 12px">
								<div class="row-m-15" style="margin-top: 25px;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-2">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>

									<div class="col-md-4">
										<strong> Bank Branch</strong>
									</div>
								</div>
								<div class="row-m-15">
									<div class="col-md-3">State Bank Of India</div>
									<div class="col-md-2">SBIN0000691</div>
									<div class="col-md-3">37714892076</div>
									<div class="col-md-4">Main Branch Parliament Street New
										Delhi</div>
								</div>

								<div class="row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-2">ICIC0000031</div>
									<div class="col-md-3">003105035926</div>
									<div class="col-md-4">Sector 18 Noida</div>
								</div>

								<!-- <div class="row-m-15">
									<div class="col-md-3">Andhra Bank</div>
									<div class="col-md-2">ANDB0001106</div>
									<div class="col-md-3">110611100007550</div>
									<div class="col-md-4">Sector 19 Noida</div>
								</div> -->

								<div class="row-m-15">
									<div class="col-md-3">BANDHAN BANK</div>
									<div class="col-md-2">BDBL0001983</div>
									<div class="col-md-3">10200005359339</div>
									<div class="col-md-4">SECTOR 51 NOIDA, UTTAR PRADESH</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. AAHCB4709M and Name of the company
										Bhartipay Services Private Limited</p>
									<p>CIN NO:- U74999DL2017PTC324436</p>
									<p>GST No:- 09AAHCB4709M1ZB</p>
								</div>
							</div>

							<%}
              	%>



							<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001054")) { %>
							<div class="row">
								<a href="javascript:void(0)" id="showDetailsLink"
									onclick="showSearchDetails()" class="pull-right"
									style="margin-right: 16px;"> <span>Hide details</span> <i
									class="fa fa-minus-square" style="margin-left: 5px"
									aria-hidden="true"></i>
								</a>
							</div>
							<div id="searchDetails" style="display: block; font-size: 12px">

								<div class="row row-m-15">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<!-- <div class="row row-m-15">
                    <div class="col-md-3">
                      ICICI Bank 
                    </div>      
                    <div class="col-md-3">
                      ICIC0002467 
                    </div>
                    <div class="col-md-3">
                      246705000218  
                    </div>
                    <div class="col-md-3">
                      Industrial Area, Jalandhar City, Punjab (144004).
                    </div>
                  </div> -->

								<div class="row row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-3">ICIC0002467</div>
									<div class="col-md-3">246705000376</div>
									<div class="col-md-3">SILVER PLAZA, PREET NAGAR, SODAL
										ROAD, JALANDHAR-144004, PUNJAB</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">Yes Bank</div>
									<div class="col-md-3">YESB0000544</div>
									<div class="col-md-3">054463300000851</div>
									<div class="col-md-3">Sodal Road, Jalandhar City,
										Punjab(144004)</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">Central Bank of India</div>
									<div class="col-md-3">CBIN0284807</div>
									<div class="col-md-3">3648180610</div>
									<div class="col-md-3">Guru Gobind Singh Avenue,
										Jalandhar(144009)</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">STATE BANK OF INDIA</div>
									<div class="col-md-3">SBIN0016848</div>
									<div class="col-md-3">38456769306</div>
									<div class="col-md-3">GOBIND SINGH AVENUE, JALANDHAR</div>
								</div>

								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN CARD No. AAFCT8493E Date of Issue 23/06/2016 and
										name of the company is TRANSFLEX PAYTECH SOLUTIONS PVT. LTD.</p>
									<p>CIN NO:- U74999 PB 2016 PTC O 45452</p>
									<p>GST No:- 03AAFCT8493E127</p>
								</div>

							</div>

							<%} if(user.getAggreatorid().equalsIgnoreCase("OAGG001057")) {              		%>
							<div class="row">

								<div class="col-md-12 col-sm-12">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px;"> <span>Hide details</span> <i
										class="fa fa-minus-square" style="margin-left: 5px"
										aria-hidden="true"></i>
									</a>

								</div>
							</div>
							<div class="container" id="searchDetails"
								style="display: block; font-size: 12px">

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-3">ICIC0001368</div>
									<div class="col-md-3">136805001540</div>
									<div class="col-md-3">Kotys Building Nr. Bhagwati Krupa
										Emporium Gheekanta Cross Roads Relief Road Ahmedabad</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">STATE BANK OF INDIA</div>
									<div class="col-md-3">SBIN0018813</div>
									<div class="col-md-3">39153558417</div>
									<div class="col-md-3">Vatva Ahmedabad</div>
								</div>
								
								<div class="row row-m-15">
									<div class="col-md-3">BANK OF INDIA</div>
									<div class="col-md-3">BKID0002053</div>
									<div class="col-md-3">205320110000330</div>
									<div class="col-md-3">AT & POST PIRANA,AHMEDABAD-382425</div>
								</div>

                                <div class="row row-m-15">
									<div class="col-md-3">THE COSMOS CO OPERATIVE BANK</div>
									<div class="col-md-3">COSB0000060</div>
									<div class="col-md-3">060100108365</div>
									<div class="col-md-3">THE COSMOS CO-OPERATIVE BANK LTD. Odhav Branch Ahmadabad-382415</div>
								</div>

                               <div class="row row-m-15">
									<div class="col-md-3">Axis Bank</div>
									<div class="col-md-3">UTIB0002642</div>
									<div class="col-md-3">921********8448</div>
									<div class="col-md-3">Ashram Road</div>
								</div>

								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. "AAKCP6621M" and Name of the company
										"PRO-MONEY PAY SOLUTION PRIVATE LIMITED</p>
									<p>CIN NO:- U67100GJ2019PTC109092</p>
									<p>GST No:- 24AAKCP6621M1Z3</p>
								</div>

							</div>
							<%} if(user.getAggreatorid().equalsIgnoreCase("OAGG001058")) {              		%>
							<div class="row">

								<div class="col-md-12 col-sm-12">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px;"> <span>Hide details</span> <i
										class="fa fa-minus-square" style="margin-left: 5px"
										aria-hidden="true"></i>
									</a>

								</div>
							</div>
							<div class="container" id="searchDetails"
								style="display: block; font-size: 12px">

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">BANDHAN BANK LIMITED</div>
									<div class="col-md-3">BDBL0001094</div>
									<div class="col-md-3">10170003873116</div>
									<div class="col-md-3">TRIVENI CAMPUS, RATAN GANJ,
										MIRZAPUR</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">AXIS BANK LIMITED</div>
									<div class="col-md-3">UTIB0000506</div>
									<div class="col-md-3">920020033621765</div>
									<div class="col-md-3">Dist. Bagalkot, Pin 587 312,uttar
										Pradesh</div>
								</div>


								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">INDUSIND BANK</div>
									<div class="col-md-3">INDB0001682</div>
									<div class="col-md-3">252106200300</div>
									<div class="col-md-3">MOHALLA PANSARI TOLA WARD8 TEHSIL
										AND DISTT MIRZAPUR</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. "XXXXXXXXXXX" and Name of the company "ATSV
										E-SOLUTION INDIA PRIVATE INDIA LIMITED"</p>
									<!-- <p>CIN NO:- U67100GJ2019PTC109092</p>
<p>GST No:- 24AAKCP6621M1Z3</p> -->
								</div>

							</div>
							<%} %>

						</div>

					</div>

					<%}else{ %>
					<div class="box-inner">
						<div class="box-header">
							<h2>Cash Deposit</h2>
						</div>
						<div class="box-content">
							<font style="color: red;"> <s:actionerror />
							</font> <font style="color: blue;"> <s:actionmessage />
							</font>

							<form action="SaveCashDeposit.action" method="post"
								enctype="multipart/form-data" id="form_name">

								<div class="row">

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <label>Deposit
											mode <font color="red"> *</font>
										</label>
										<s:select list="%{txnTypeList}" id="drop-neft"
											onchange="dropInfo(this)" name="depositMast.type"
											headerKey="-1" headerValue="Select your deposit mode"
											class="mandatory" required="true">
										</s:select>
									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label>Deposit Bank <font color="red"> *</font>
										</label>
										<s:select list="%{bankNameList}" id="drop-neft"
											name=" depositMast.bankName" headerKey="-1"
											headerValue="Select deposit Bank" class="mandatory"
											required="true" value="%{depositBank}"></s:select>

									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label for="apptxt">Deposit amount <font color="red">
												*</font>
										</label> <input class="form-control onlyNum mandatory" type="text"
											id="amount" maxlength="7" placeholder="Deposit amount"
											name="depositMast.amount" style="margin-left: 0px;"
											value="<s:property value='%{depositMast.amount}'/>" Required>

									</div>

	                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">

										<label for="apptxt">Deposit branch <!-- <font color="red"> *</font> -->
										</label> <input class="form-control userName mandatory" type="text"
											id="branchName" maxlength="20" placeholder="Deposit branch"
											name="depositMast.branchName" style="margin-left: 0px;"
											value="<s:property value='%{depositMast.branchName}'/>"
											>

									</div>


                                   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
                                    <label for="apptxt">Deposit Date <font color="red">*</font></label>  
										<input type="text"
										value="<s:property value='%{depositMast.cashDepositDate}'/>"
										name="depositMast.cashDepositDate" id="cashDepositDate"
										class="form-control datepicker-here1 " placeholder="Deposit Date"
										data-language="en" /> 
                                   
<!-- 
										<label for="apptxt">Deposit Date <font color="red">*</font></label> 
										  <input class="custom-date" type="date"
											id="cashDepositDate"   placeholder="Cash Deposit Date"
											name="depositMast.cashDepositDate" style="margin-left: 0px;"
											value="<s:property value='%{depositMast.cashDepositDate}'/>"
											Required>
-->
									</div>
                                   


									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<div id="NEFT">
											<div class="form=group">
												<label>NEFT/IMPS/RTGS transaction Id <font
													color="red"> *</font>
												</label> <input type="text" class="form-control mandatory"
													name="depositMast.neftRefNo" style="margin-left: 0px;" placeholder="Transaction Id"
													value="<s:property value='%{depositMast.neftRefNo}'/>"
													Required />
											</div>
										</div>
									</div>

                                 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<div id="NEFT">
											<div class="form=group">
												<label>Remark </label>
												 <input type="text" class="form-control mandatory" placeholder="Remarks "
													name="depositMast.remark" style="margin-left: 0px;"
													value="<s:property value='%{depositMast.remark}'/>"
													 />
											</div>
										</div>
									</div>								

                                   	

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
										<div id="RECIEPT" style="">
											<label> </label> <input type="file"
												class="custom-file-input" name="depositMast.myFile1"
												value="<s:property value='%{depositMast.myFile1}'/>" />
										</div>
									</div>
									
									
									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<!-- <input type="file" class="custom-file-input"> -->
										<button class="btn btn-success submit-form"
											style="margin-top: 25px;"
											onclick="submitVaForm('#form_name',this)">Submit</button>
										<input type="reset" style="margin-top: 25px;"
											onclick="resetForm('#form_name')"
											class="btn-primary btn btn-fill" />
									</div>

									<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
              	     %>


								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
									style="margin-top: -11px;">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px; color: #db791f;"> <span>Hide
											details</span> <i class="fa fa-minus-square"
										style="margin-left: 5px" aria-hidden="true"></i>
									</a>
								</div>
							</form>


							<div id="searchDetails" style="display: none; font-size: 12px">
								<div class="row-m-15" style="margin-top: 25px;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-2">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>

									<div class="col-md-4">
										<strong> Bank Branch</strong>
									</div>
								</div>
								<div class="row-m-15">
									<div class="col-md-3">State Bank Of India</div>
									<div class="col-md-2">SBIN0000691</div>
									<div class="col-md-3">37714892076</div>
									<div class="col-md-4">Main Branch Parliament Street New
										Delhi</div>
								</div>

								<div class="row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-2">ICIC0000031</div>
									<div class="col-md-3">003105035926</div>
									<div class="col-md-4">Sector 18 Noida</div>
								</div>

								<!-- <div class="row-m-15">
									<div class="col-md-3">Andhra Bank</div>
									<div class="col-md-2">ANDB0001106</div>
									<div class="col-md-3">110611100007550</div>
									<div class="col-md-4">Sector 19 Noida</div>
								</div> -->

								<div class="row-m-15">
									<div class="col-md-3">BANDHAN BANK</div>
									<div class="col-md-2">BDBL0001983</div>
									<div class="col-md-3">10200005359339</div>
									<div class="col-md-4">SECTOR 51 NOIDA, UTTAR PRADESH</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. AAHCB4709M and Name of the company
										Bhartipay Services Private Limited</p>
									<p>CIN NO:- U74999DL2017PTC324436</p>
									<p>GST No:- 09AAHCB4709M1ZB</p>
								</div>
							</div>

							<%}
              	%>

		<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001070")) {
              	     %>


								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
									style="margin-top: -11px;">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px; color: #db791f;"> <span>Hide
											details</span> <i class="fa fa-minus-square"
										style="margin-left: 5px" aria-hidden="true"></i>
									</a>
								</div>
							</form>


							<div id="searchDetails" style="display: none; font-size: 12px">
								<div class="row-m-15" style="margin-top: 25px;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-2">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>

									<div class="col-md-4">
										<strong> Bank Branch</strong>
									</div>
								</div>
								<div class="row-m-15">
									<div class="col-md-3">State Bank Of India</div>
									<div class="col-md-2">SBIN0000691</div>
									<div class="col-md-3">37714892076</div>
									<div class="col-md-4">Main Branch Parliament Street New
										Delhi</div>
								</div>

								<div class="row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-2">ICIC0000031</div>
									<div class="col-md-3">003105035926</div>
									<div class="col-md-4">Sector 18 Noida</div>
								</div>

								<!-- <div class="row-m-15">
									<div class="col-md-3">Andhra Bank</div>
									<div class="col-md-2">ANDB0001106</div>
									<div class="col-md-3">110611100007550</div>
									<div class="col-md-4">Sector 19 Noida</div>
								</div> -->

								<div class="row-m-15">
									<div class="col-md-3">BANDHAN BANK</div>
									<div class="col-md-2">BDBL0001983</div>
									<div class="col-md-3">10200005359339</div>
									<div class="col-md-4">SECTOR 51 NOIDA, UTTAR PRADESH</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. AAHCB4709M and Name of the company
										Bhartipay Services Private Limited</p>
									<p>CIN NO:- U74999DL2017PTC324436</p>
									<p>GST No:- 09AAHCB4709M1ZB</p>
								</div>
							</div>

							<%}
              	%>


							<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001054")) { %>
							<div class="row">
								<a href="javascript:void(0)" id="showDetailsLink"
									onclick="showSearchDetails()" class="pull-right"
									style="margin-right: 16px;"> <span>Hide details</span> <i
									class="fa fa-minus-square" style="margin-left: 5px"
									aria-hidden="true"></i>
								</a>
							</div>
							<div id="searchDetails" style="display: block; font-size: 12px">

								<div class="row row-m-15">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<!-- <div class="row row-m-15">
                    <div class="col-md-3">
                      ICICI Bank 
                    </div>      
                    <div class="col-md-3">
                      ICIC0002467 
                    </div>
                    <div class="col-md-3">
                      246705000218  
                    </div>
                    <div class="col-md-3">
                      Industrial Area, Jalandhar City, Punjab (144004).
                    </div>
                  </div> -->

								<div class="row row-m-15">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-3">ICIC0002467</div>
									<div class="col-md-3">246705000376</div>
									<div class="col-md-3">SILVER PLAZA, PREET NAGAR, SODAL
										ROAD, JALANDHAR-144004, PUNJAB</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">Yes Bank</div>
									<div class="col-md-3">YESB0000544</div>
									<div class="col-md-3">054463300000851</div>
									<div class="col-md-3">Sodal Road, Jalandhar City,
										Punjab(144004)</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">Central Bank of India</div>
									<div class="col-md-3">CBIN0284807</div>
									<div class="col-md-3">3648180610</div>
									<div class="col-md-3">Guru Gobind Singh Avenue,
										Jalandhar(144009)</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">STATE BANK OF INDIA</div>
									<div class="col-md-3">SBIN0016848</div>
									<div class="col-md-3">38456769306</div>
									<div class="col-md-3">GOBIND SINGH AVENUE, JALANDHAR</div>
								</div>

								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN CARD No. AAFCT8493E Date of Issue 23/06/2016 and
										name of the company is TRANSFLEX PAYTECH SOLUTIONS PVT. LTD.</p>
									<p>CIN NO:- U74999 PB 2016 PTC O 45452</p>
									<p>GST No:- 03AAFCT8493E127</p>
								</div>

							</div>

							<%} if(user.getAggreatorid().equalsIgnoreCase("OAGG001057")) {              		%>
							<div class="row">

								<div class="col-md-12 col-sm-12">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px;"> <span>Hide details</span> <i
										class="fa fa-minus-square" style="margin-left: 5px"
										aria-hidden="true"></i>
									</a>

								</div>
							</div>
							<div class="container" id="searchDetails"
								style="display: block; font-size: 12px">

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">ICICI Bank</div>
									<div class="col-md-3">ICIC0001368</div>
									<div class="col-md-3">136805001540</div>
									<div class="col-md-3">Kotys Building Nr. Bhagwati Krupa
										Emporium Gheekanta Cross Roads Relief Road Ahmedabad</div>
								</div>

								<div class="row row-m-15">
									<div class="col-md-3">STATE BANK OF INDIA</div>
									<div class="col-md-3">SBIN0018813</div>
									<div class="col-md-3">39153558417</div>
									<div class="col-md-3">Vatva Ahmedabad</div>
								</div>
								<div class="row row-m-15">
									<div class="col-md-3">BANK OF INDIA</div>
									<div class="col-md-3">BKID0002053</div>
									<div class="col-md-3">205320110000330</div>
									<div class="col-md-3">AT & POST PIRANA,AHMEDABAD-382425</div>
								</div>

                               <div class="row row-m-15">
									<div class="col-md-3">THE COSMOS CO OPERATIVE BANK</div>
									<div class="col-md-3">COSB0000060</div>
									<div class="col-md-3">060100108365</div>
									<div class="col-md-3">THE COSMOS CO-OPERATIVE BANK LTD. Odhav Branch Ahmadabad-382415</div>
								</div>

                                <div class="row row-m-15">
									<div class="col-md-3">Axis Bank</div>
									<div class="col-md-3">UTIB0002642</div>
									<div class="col-md-3">921********8448</div>
									<div class="col-md-3">Ashram Road</div>
								</div>


								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. "AAKCP6621M" and Name of the company
										"PRO-MONEY PAY SOLUTION PRIVATE LIMITED</p>
									<p>CIN NO:- U67100GJ2019PTC109092</p>
									<p>GST No:- 24AAKCP6621M1Z3</p>
								</div>

							</div>
							<%} if(user.getAggreatorid().equalsIgnoreCase("OAGG001058")) {              		%>
							<div class="row">

								<div class="col-md-12 col-sm-12">
									<a href="javascript:void(0)" id="showDetailsLink"
										onclick="showSearchDetails()" class="pull-right"
										style="margin-right: 16px;"> <span>Hide details</span> <i
										class="fa fa-minus-square" style="margin-left: 5px"
										aria-hidden="true"></i>
									</a>

								</div>
							</div>
							<div class="container" id="searchDetails"
								style="display: block; font-size: 12px">

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX;">
									<div class="col-md-3">
										<strong> Bank Name: </strong>
									</div>
									<div class="col-md-3">
										<strong>Bank IFSC Code</strong>
									</div>
									<div class="col-md-3">
										<strong>Bank Account Number </strong>
									</div>
									<div class="col-md-3">
										<strong> Bank Branch</strong>
									</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">BANDHAN BANK LIMITED</div>
									<div class="col-md-3">BDBL0001094</div>
									<div class="col-md-3">10170003873116</div>
									<div class="col-md-3">TRIVENI CAMPUS, RATAN GANJ,
										MIRZAPUR</div>
								</div>

								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">AXIS BANK LIMITED</div>
									<div class="col-md-3">UTIB0000506</div>
									<div class="col-md-3">920020033621765</div>
									<div class="col-md-3">Dist. Bagalkot, Pin 587 312,uttar
										Pradesh</div>
								</div>


								<div class="row"
									style="border-bottom: 1px solid #ccc; padding-bottom: 20PX; padding-top: 20px;">
									<div class="col-md-3">INDUSIND BANK</div>
									<div class="col-md-3">INDB0001682</div>
									<div class="col-md-3">252106200300</div>
									<div class="col-md-3">MOHALLA PANSARI TOLA WARD8 TEHSIL
										AND DISTT MIRZAPUR</div>
								</div>



								<div
									style="text-align: center; margin: 15px 0 0 0; float: left; width: 100%; color: #19aede;">
									<p>PAN Card No. "XXXXXXXXXXX" and Name of the company "ATSV
										E-SOLUTION INDIA PRIVATE INDIA LIMITED"</p>
									<!-- <p>CIN NO:- U67100GJ2019PTC109092</p>
<p>GST No:- 24AAKCP6621M1Z3</p> -->
								</div>

							</div>
							<%} %>

						</div>
					</div>


					<%} %>

					<!-- <div class="box-inner"> 
            	<div class="box-content"> 
            		
            	</div>  
          </div> -->

					<div id="xyz" class="box">
						<table id="example"
							class="scrollD cell-border dataTable no-footer" width="100%">
							<thead>
								<tr>
									<th><u>Id</u></th>
									<th><u>Type</u></th>
									<th><u>Amount</u></th>
									<th><u>Deposit date</u></th>
									<th><u>Bank Name</u></th>
									<th><u>Branch</u></th>
									<th><u>Transaction id</u></th>
									<th><u>Date</u></th>
									<th><u>Status</u></th>
									<th><u>Receipt</u></th>
								</tr>
							</thead>
							<tbody>
								<% 
          		      List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
          			    if(eList!=null){ 
          			      for(int i=0; i<eList.size(); i++) {
          				    CashDepositMast tdo=eList.get(i);%>
								<tr>
									<td><%=tdo.getDepositId()%></td>
									<td><%=tdo.getType()%></td>
									<td><%=tdo.getAmount()%></td>
									<td><%=tdo.getCashDepositDate()%></td>
									<td>
										<%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%>
										<%}%>
									</td>
									<td>
										<%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%>
										<%}%>

									</td>
									<td><%=tdo.getNeftRefNo()%></td>
									<td><%=tdo.getRequestdate()%></td>
									<td><%=tdo.getStatus()%></td>
									<td>
										<%if(tdo.getReciptPic()!=null&&!tdo.getReciptPic().isEmpty()){%>
										<a href="data:image/gif;base64,<%=tdo.getReciptPic()%>" download="data:image/gif;base64,<%=tdo.getReciptPic()%>">
										<img alt="userImg"
										src="data:image/gif;base64,<%=tdo.getReciptPic()%>"
										style="height:40px;width:40px;">
										</a>

										<div id="myModal<%=tdo.getDepositId()%>" class="modal fade"
											role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<img alt="userImg"
														src="data:image/ gif;base64,<%=tdo.getReciptPic()%>" />
												</div>
											</div>
										</div> <%} %>

									</td>
								</tr>
								<%}
              				  }%>
							</tbody>
						</table>
					</div>

				</div>

			</div>
		</div>
	</div>


	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%-- <script src="js/bootstrap.min.js"></script> --%>

	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<%-- <script src="./newmis/js/charisma.js"></script> --%>
	
	<script>
	function OpenInNewWindow(data){
		var image = new Image();
        image.src = "data:image/jpg;base64," + data;

        var w = window.open("");
        w.document.write(image.outerHTML);
	}
	</script>

</body>
</html>




