<%@page import="com.bhartipay.security.EncryptionDecryption"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.UserWishListBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 <script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
	
   }); 
</script>
<script type="text/javascript">
 $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Distributor List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Distributor List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Distributor List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Distributor List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Distributor List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function acceptAgent(){
var txt;
var utr = $("#utr").val();
$("#utr-hidden").val(utr)
/* if(utr.length == 0){
	alert("You haven't provided UTR value");
	 return false;
}else{ */

var r = confirm("Do you want to accept agent.");
if (r == true) {
   return true;
} else {
    return false;
    }
//}
}
function rejectAgent(){
	var dc = $("#decline-comment").val();
	if(dc.length == 0){		
		alert("Please provide the decline comment")
	    return false;

	}else{
		var txt;
		var r = confirm("Do you want to reject agent.");
		if (r == true) {
		   return true;
		} else {
			return false;
		}

		
	}
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
        
 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>Distributor List</h2>
		</div>
		<form method="post">
								<div class="row" style="padding: 10px;">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group"
										style="margin-bottom: 5px;">
										

										<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div>	 
										<input type="hidden" value="06-Sep-2020"
											name="inputBean.stDate" id="from"
											class="form-control datepicker-here1"
											placeholder="Start Date" data-language="en" required="">

										<input type="hidden" value="05-Oct-2020"
											name="inputBean.endDate" id="to"
											class="form-control datepicker-here2" placeholder="End Date"
											data-language="en" required="">
									</div>

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<div>
											<input class="btn btn-success submit-form" id="submit"
												type="submit" value="Submit"> <input
												class="btn btn-primary btn-fill" id="reset" type="reset"
												value="Reset">
										</div>
									</div>

								</div>
							</form>
	</div>
	</div>
		
		
		
		

						
							
							
			<%
		List<WalletMastBean>agentList=(List<WalletMastBean>)request.getAttribute("list");
		
			%>				
							
			 <div id="xyz">
			<table id="example" class="scrollD cell-border dataTable no-footer">
				<thead>

				<tr>
				<th><u>Type</u></th>
				<th><u>Creation Date</u></th>
				<th><u>Dist ID</u></th>
				<th style="display:  none;"><u>Manager Owner</u></th>
				<th><u>SO Owner</u></th>
				<th style="display:  none;"><u>Territory</u></th>	
				<th><u>Dist Name</u></th>	
				<th style="display:  none;"><u>Gender</u></th>
				<th style="display:  none;"><u>DOB</u></th>
				<th style="display:  none;"><u>Pan Card No</u></th>
				<th style="display:  none;"><u>Aadhar No</u></th>
				<th><u>Mobile No</u></th>
				<th><u>Mail ID</u></th>
				<th style="display:none"><u>Address </u></th>
				<th style="display:none"><u>City</u></th>
				<th style="display:none"><u>State</u></th>
				<th style="display:none"><u>Pin Code</u></th>
				<th style="display:none"><u>Shop Name</u></th>
				<th style="display:none"><u>Shop_Address</u></th>
				<th style="display:none"><u>Shop_City</u></th>
				<th style="display:none"><u>Shop_State</u></th>
				<th style="display:none"><u>Shop_Pin Code</u></th>
				<th style="display:none"><u>Bank Name</u></th>
				<th style="display:none"><u>IFSC Code</u></th>
				<th style="display:none"><u>Accont No</u></th>
				
						
						
						
						<th><u>Address proof</u></th>
						<th><u>ID proof</u></th>
						<th><u>Pic</u></th>
							
					</tr>
				</thead>
				<tbody>
				<%
				if(agentList!=null&&agentList.size()>0){	
				for(WalletMastBean wmb:agentList){
				%>
				<tr>
				<td><%
				if(wmb.getUsertype()==3){
				%>
					Distributor
				<%}else{
				%>	
				SuperDist
				<%}
				%></td>
				<td><%=wmb.getCreationDate() == null ? "":wmb.getCreationDate()%></td>
				<td><%=wmb.getId()!= null ?wmb.getId().toUpperCase() :""%></td>
				<td style="display:  none;"><%=wmb.getManagerName()==null?"":wmb.getManagerName().toUpperCase()%></td>
				<td><%=wmb.getSoName()==null?"":wmb.getSoName().toUpperCase()%></td>
				<td style="display:  none;"><%=wmb.getTerritory()==null?"":wmb.getTerritory().toUpperCase()%></td>	
				<td><%=wmb.getName()!= null ?wmb.getName().toUpperCase():""%></td>	
				<td style="display:  none;"><%=wmb.getGender()!= null ?wmb.getGender().toUpperCase():""%></td>
				<td style="display:  none;"><%=wmb.getDob()!= null ?wmb.getDob():""%></td>
				<td style="display:  none;"><%=EncryptionDecryption.getdecrypted(wmb.getPan(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=")%></td>
				<td style="display:  none;"><%=EncryptionDecryption.getdecrypted(wmb.getAdhar(), "km0sfQk7jAIPSicd/piye18Xcr6tVfCzwC6QPkt7CAw=")%></td>
				<td><%=wmb.getMobileno()!= null ?wmb.getMobileno() :""%></td>
				<td><%=wmb.getEmailid()!= null ?wmb.getEmailid() :""%></td>
				<td style="display:none"><%=wmb.getAddress1()!= null ?wmb.getAddress1().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getCity()!= null ?wmb.getCity().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getState()!= null ?wmb.getState().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getPin()!= null ?wmb.getPin() :""%></td>
				<td style="display:none"><%=wmb.getShopName()!= null ?wmb.getShopName().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getShopAddress1()!= null ?wmb.getShopAddress1().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getShopCity()!= null ?wmb.getShopCity().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getShopState()!= null ?wmb.getShopState().toUpperCase() :""%></td>
				<td style="display:none"><%=wmb.getShopPin()!= null ?wmb.getShopPin() :""%></td>
				<td style="display:none"><%=wmb.getBankName()==null?"":wmb.getBankName().toUpperCase()%></td>
				<td style="display:none"><%=wmb.getAccountNumber()==null?"":wmb.getAccountNumber()%></td>
				<td style="display:none"><%=wmb.getIfscCode()==null?"":wmb.getIfscCode()%></td>
		                     
		                     
										<%-- 	<td>
						<a href="javascipt:void(0)" data-toggle="modal" data-target="#newAgentBox"><span onclick="getAgentDetails('<%=wmb.getId()%>','<%=wmb.getAggreatorid()%>')" >View Details</span></a>
											</td> --%>
<%-- <td class="td-img"><img  src="<%=wmb.getAddressLoc() %>" /></td>
<td class="td-img"><img   src="<%=wmb.getIdLoc() %>" /></td>
<td class="td-img"><img src="<%=wmb.getFormLoc() %>" /></td> --%>

<td class="td-img"><%if(wmb.getAddressLoc()!= null){ %><a target="_blank" href="<%=wmb.getAddressLoc() %>" >Address</a><%} %></td>
<td class="td-img"><%if(wmb.getIdLoc()!= null){ %><a target="_blank" href="<%=wmb.getIdLoc() %>" >Id</a><%} %></td>
<td class="td-img"><%if(wmb.getFormLoc()!= null){ %><a target="_blank" href="<%=wmb.getFormLoc() %>" >Form</a><%} %></td> 			
											<%
                  }
				}                        
				%></tbody>		</table>
		</div> 
		
 
 
 

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>
</body>
</html>