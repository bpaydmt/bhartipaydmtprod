<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<jsp:include page="theams.jsp"></jsp:include>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
  
 
 
<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>
 
<script type="text/javascript">
  function showSearchDetails(){

  	 var div = $("#searchDetails")
  	 
  	 if(div.is(":hidden")){
  	  div.show()
  	  $("#showDetailsLink span").text("Hide details")
  	  $("#showDetailsLink .fa").addClass("fa-minus-square")
  	  $("#showDetailsLink .fa").removeClass("fa-plus-square")
  	 }else{
  	  div.hide()

  	 
  	  $("#showDetailsLink span").text("Show details")
  	  $("#showDetailsLink .fa").removeClass("fa-minus-square")
  	  $("#showDetailsLink .fa").addClass("fa-plus-square")
  	 }
  	}
  $(document).ready(function() {
      
      $('#example').DataTable( {
          dom: 'Bfrtip',
          autoWidth: false,
           "order": [[ 6, "desc" ]], 
          buttons: [
               {
               
                  extend: 'copy',
                  text: 'COPY',
                  title:'Cash Deposit - ' + '<%= currentDate %>',
                  message:'<%= currentDate %>',
              },  {
               
                  extend: 'csv',
                  text: 'CSV',
                  title:'Cash Deposit - ' + '<%= currentDate %>',
                
              },{
               
                  extend: 'excel',
                  text: 'EXCEL',
                  title:'Cash Deposit - ' + '<%= currentDate %>',
              
              }, {
               
                  extend: 'pdf',
                  text: 'PDF',
                  title:'Cash Deposit - ' + '<%= currentDate %>',
                  message:" "+ "<%= currentDate %>" + "",
                 
              },  {
               
                  extend: 'print',
                  text: 'PRINT',
                  title:'Cash Deposit - ' + '<%= currentDate %>',
                
              }
          ]
      } );
      $('#dpStart').datepicker({
      	 language: 'en',
      	 autoClose:true,
      	 maxDate: new Date(),

      	});
      	if($('#dpStart').val().length != 0){

      	 $('#dpEnd').datepicker({
      	       language: 'en',
      	       autoClose:true,
      	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
      	       maxDate: new Date(),
      	       
      	      }); 
      	 
      	}
      	$("#dpStart").blur(function(){

      	$('#dpEnd').val("")
      	 $('#dpEnd').datepicker({
      	      language: 'en',
      	     autoClose:true,
      	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
      	     maxDate: new Date(),
      	     
      	    }); 
      	})    
    } );
 

  function dropInfo(elm){
	 var el = $(elm).val();
	 if(el == "CASH"){
		 $("#RECIEPT").show(); 
		
	 }else{ 
		 $("#RECIEPT").hide(); 
	 } 
	}
 
  function converDateToJsFormat(date) { 
  var sDay = date.slice(0,2);
  var sMonth = date.slice(3,6);
  var yYear = date.slice(7,date.length) 
  return sDay + " " +sMonth+ " " + yYear;
  }

</script>

<style>
  .row-m-15{ 
    /*margin: 5px 8px 10px;*/
    border-bottom: 1px solid #ccc;
    padding: 1px 0 8px;
    float: left;
    width: 100%;
  }
</style>
</head>

<% 
  User user = (User) session.getAttribute("User");
  Date myDate = new Date();
  System.out.println(myDate);
  SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
  String toDate=format.format(myDate);
  Calendar cal = Calendar.getInstance();
  cal.add(Calendar.DATE, -0);
  Date from= cal.getTime();    
  String fromDate = format.format(from);
  Object[] commDetails=(Object[])session.getAttribute("commDetails");
  Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

  List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");  
%>
      
       

<body>

    <!-- topbar starts -->
  <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    

        
        <!-- left menu starts -->
  <jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->

  <div id="main" role="main"> 
    <div id="content">       
        <div class="row">   

          <div class="col-md-12" id="hidethis2">  
          	<div class="box-inner"> 
          		<div class="box-header">
          			<h2>Cash Deposit</h2> 
          		</div> 
          		<div class="box-content">
            		<font style="color:red;">
            		  <s:actionerror/>
            		</font>
            		<font style="color: blue;">
            		  <s:actionmessage/>
            		</font>

            		<form action="SaveCashDeposit.action"  method="post" enctype="multipart/form-data"  id="form_name">

              		<div class="row">

                    <div class="col-md-4">
                			<div class="form-group">
                  			<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
                  			<label>Select your transaction type 
                          <font color="red"> *</font>
                        </label>
                  			<s:select  list="%{txnTypeList}" id="drop-neft" onchange="dropInfo(this)" name="depositMast.type" headerKey="-1" headerValue="Select your transaction type" class="mandatory"> 
                  			</s:select>
                			</div> 
              		  </div>

              			<div class="col-md-4">
              				<div class="form-group">
                				<label for="apptxt">Enter Amount 
                          <font color="red"> *</font>
                        </label> 
                				<input class="form-control onlyNum mandatory"  type="text" id="amount" maxlength="7" placeholder="Enter Amount" name="depositMast.amount" style="margin-left:0px;" value="<s:property value='%{depositMast.amount}'/>" > 
              			  </div>
              			</div>

              			<div class="col-md-4">
                			<div id="NEFT" > 
                  			<div class="form=group">
                  			  <label>NEFT/IMPS/RTGS transaction Id
                            <font color="red"> *</font>
                          </label>
                  			  <input type="text" class="form-control mandatory" name="depositMast.neftRefNo" style="margin-left:0px;" 	value="<s:property value='%{depositMast.neftRefNo}'/>" />
                  			</div> 
                			</div>
                    </div>

              		</div>

                	<div class="row">
                		<div class="col-md-4">
                			<div class="form-group">
                  			<label>Select your Bank 
                          <font color="red"> *</font>
                        </label> 
                        <s:select  list="%{bankNameList}" id="drop-neft"  name=" depositMast.bankName" headerKey="-1" headerValue="Select your Bank" class="mandatory"></s:select> 
              			  </div> 
              		  </div>
              		
              		  <div class="col-md-4">
              				<div class="form-group">
                				<label for="apptxt">Enter Branch Name 
                          <font color="red"> *</font>
                        </label> 
                				<input class="form-control userName mandatory"  type="text" id="branchName" maxlength="20" placeholder="Enter Branch" name="depositMast.branchName"  style="margin-left:0px;" value="<s:property value='%{depositMast.branchName}'/>" > 
              				</div>
              			</div>
             
              			<div class="col-md-4" > 
                			<div id="RECIEPT" style="display:none;"> 
                  			<label >Upload cash deposit slip 
                          <font color="red"> *</font>
                        </label>
                  			<input type="file" class="form-file-inp" 
                  			name="depositMast.myFile1" value="<s:property value='%{depositMast.myFile1}'/>" /> 
                			</div> 
              		  </div>
              		
              		</div>

            		</form>

          			 
          			 
          		   

              	<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001050")) {
              	%> 

          		  <div class="row">
          		  <div class="col-md-4">
          		      <button  class="btn btn-infobtn btn-info btn-fill submit-form" onclick="submitVaForm('#form_name',this)">Submit</button>
          		      <input type="reset" onclick="resetForm('#form_name')" class="btn btn-info btn-fill" />
          		  </div>
          		   
                  <a href="javascript:void(0)" id="showDetailsLink" onclick="showSearchDetails()" class="pull-right" style="margin-right: 16px;">
                    <span>Hide details</span>
                    <i class="fa fa-minus-square" style="margin-left:5px" aria-hidden="true"></i>
                  </a>
                </div>
          		
          		  <div id="searchDetails" style="display: block;font-size:12px"> 
                  <div class="row-m-15" style="margin-top: 25px;">
                    <div class="col-md-3">
                      <strong> Bank Name: </strong>  
                    </div>
                    <div class="col-md-2">
                      <strong>Bank IFSC Code</strong> 
                    </div>
                    <div class="col-md-3">
                      <strong>Bank Account Number </strong>  
                    </div>
                    
                    <div class="col-md-4">
                      <strong> Bank Branch</strong> 
                    </div> 
                  </div> 
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      Axis Bank 
                    </div>    
                    <div class="col-md-2">
                      UTIB0000022 
                    </div> 
                    <div class="col-md-3">
                      916020076556799 
                    </div> 
                    <div class="col-md-4">
                      Sector 16 Noida 
                    </div> 
                  </div> 
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      State Bank Of India 
                    </div> 
                    <div class="col-md-2">
                      SBIN0004041 
                    </div> 
                    <div class="col-md-3">
                      36882771971 
                    </div>  
                    <div class="col-md-4">
                      Main Branch Parliament Street New Delhi 
                    </div> 
                  </div> 
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      Bandhan Bank 
                    </div>
                    <div class="col-md-2">
                      BDBL0001601  
                    </div> 
                    <div class="col-md-3">
                      10170003130745 
                    </div> 
                    <div class="col-md-4">
                      Sector 18 Noida 
                    </div>  
                  </div>   
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      ICICI Bank 
                    </div>
                    <div class="col-md-2">
                      ICIC0000031
                    </div>
                    <div class="col-md-3">
                      003105031101
                    </div>
                    <div class="col-md-4">
                      Sector 18 Noida
                    </div>
                  </div> 
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      Bank of Baroda 
                    </div>
                    <div class="col-md-2">
                      BARB0PARLIA
                    </div>
                    <div class="col-md-3">
                      05860200001476
                    </div>
                    <div class="col-md-4">
                      Parliament Street, New Delhi
                    </div>
                  </div> 
                  <div class="row-m-15"> 
                    <div class="col-md-3">
                      Saraswat Bank 
                    </div>   
                    <div class="col-md-2">
                      SRCB0000358 
                    </div>
                    <div class="col-md-3">
                      358100100000163 
                    </div> 
                    <div class="col-md-4">
                      Connaught Place, New Delhi 
                    </div> 
                  </div>    
                    
                  <div style="text-align: center;margin: 15px 0 0 0;float: left;width: 100%;color: #19aede;"> 
                    <p>
                      PAN Card No. AAMCA5082H and Name of the company Appnit Technologies Private Limited
                    </p>
                    <p>CIN NO:- U72900UP2014PTC063266</p>
                    <p>GST No:- 09AAMCA5082H1ZE</p>
                  </div>                
                </div>
          		
              	<%}
              	%>	
          		
            		<% if(user.getAggreatorid().equalsIgnoreCase("OAGG001054")) {
            		%> 
          			<div class="row">
                  <a href="javascript:void(0)" id="showDetailsLink" onclick="showSearchDetails()" class="pull-right" style="margin-right: 16px;">
                    <span>Hide details</span>
                    <i class="fa fa-minus-square" style="margin-left:5px" aria-hidden="true"></i>
                  </a>
                </div> 
          		  <div id="searchDetails" style="display: block;font-size:12px"> 

                  <div class="row row-m-15">
                    <div class="col-md-3">
                      <strong> Bank Name: </strong> 
                    </div>
                    <div class="col-md-3">
                      <strong>Bank IFSC Code</strong> 
                    </div>
                    <div class="col-md-3">
                      <strong>Bank Account Number </strong> 
                    </div> 
                    <div class="col-md-3">
                      <strong> Bank Branch</strong>  
                    </div> 
                  </div> 

                  <div class="row row-m-15">
                    <div class="col-md-3">
                      ICICI Bank 
                    </div>      
                    <div class="col-md-3">
                      ICIC0002467 
                    </div>
                    <div class="col-md-3">
                      246705000218  
                    </div>
                    <div class="col-md-3">
                      Industrial Area, Jalandhar City, Punjab (144004).
                    </div>
                  </div>
                           
                  <div class="row row-m-15"> 
                    <div class="col-md-3">
                      ICICI Bank
                    </div>         
                    <div class="col-md-3">
                      ICIC0002467
                    </div>
                    <div class="col-md-3">
                      246705000376
                    </div>
                    <div class="col-md-3">
                      SILVER PLAZA, PREET NAGAR, SODAL ROAD, JALANDHAR-144004, PUNJAB 
                    </div>
                  </div>

                  <div class="row row-m-15">  
                    <div class="col-md-3">
                      Yes Bank 
                    </div>    
                    <div class="col-md-3">
                      YESB0000544 
                    </div> 
                    <div class="col-md-3">
                      054463300000851 
                    </div>       
                    <div class="col-md-3">
                      Sodal Road, Jalandhar City, Punjab(144004)
                    </div>
                  </div>

                  <div class="row row-m-15">
                    <div class="col-md-3">
                      Central Bank of India
                    </div>
                    <div class="col-md-3">
                      CBIN0284807
                    </div>
                    <div class="col-md-3">
                      3648180610
                    </div>
                    <div class="col-md-3">
                      Guru Gobind Singh Avenue, Jalandhar(144009)
                    </div>
                  </div>    
                           
                  <div class="row row-m-15"> 
                    <div class="col-md-3">
                      Axis Bank
                    </div>
                    <div class="col-md-3">
                      UTIB0000692
                    </div>
                    <div class="col-md-3">
                      918020008736675
                    </div>
                    <div class="col-md-3">
                      Sodal Road, Jalandhar (144009)
                    </div>
                  </div>    

                  <div style="text-align: center;margin: 15px 0 0 0;float: left;width: 100%;color: #19aede;">
                    <p> 
                      PAN CARD No. AAFCT8493E Date of Issue 23/06/2016 and name of the company is TRANSFLEX PAYTECH SOLUTIONS PVT. LTD.
                    </p>
                    <p>CIN NO:- U74999 PB 2016 PTC O 45452</p>
                    <p>GST No:- 03AAFCT8493E127</p>
                  </div>          
                                     
                </div> 
          		
          		  <%} %>

          		</div>
          	</div> 

          	<div class="box-content row"> 
          		<form action="CashDeposit" method="post"> 

          			<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
          			  <br> 
          			  <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
          				<input type="text" value="<s:property value='%{depositMast.stDate}'/>" name="depositMast.stDate" id="dpStart" style="margin-left:0px;" class="form-control datepicker-here1" placeholder="Start Date" data-language="en"/>
          			</div>

          			<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6"> 
          				<br> 
                  <input type="text" value="<s:property value='%{depositMast.endDate}'/>" name="depositMast.endDate" id="dpEnd" style="margin-left:0px;" class="form-control datepicker-here2" placeholder="End Date" data-language="en">
          			</div>

          			<div class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17"> 
          				<div  id="wwctrl_submit">
          					<input class="btn btn-info" id="submit" type="submit" value="Submit">
          					<input class="btn btn-info" id="reset" type="reset" value="Reset">
          				</div>
          			</div>

          		</form>
          		
          	</div> 

          	<div id="xyz">
          		<table id="example" class="scrollD cell-border dataTable no-footer" width="100%">
          			<thead> 
          				<tr>
          					<th><u>Id</u></th>
          					<th><u>Type</u></th>
          					<th><u>Amount</u></th>
          					<th><u>Bank Name</u></th>
          					<th><u>Branch</u></th>
          					<th><u>Transaction id</u></th>
          					<th><u>Date</u></th>
          					<th><u>Status</u></th>						
          					<th><u>Receipt</u></th> 		
          				</tr>
          			</thead>
          			<tbody>
                  <% 
          		      List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
          			    if(eList!=null){ 
          			      for(int i=0; i<eList.size(); i++) {
          				    CashDepositMast tdo=eList.get(i);%>
          	          <tr>
          	          <td><%=tdo.getDepositId()%></td>
          	          <td><%=tdo.getType()%></td>
          	          <td><%=tdo.getAmount()%></td>  
          	          <td>
                        <%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%><%}%> 
                      </td>
          	          <td>

                        <%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%><%}%>
                          
                      </td>     
          	          <td><%=tdo.getNeftRefNo()%></td> 
          	          <td><%=tdo.getRequestdate()%></td> 
          	          <td><%=tdo.getStatus()%></td> 
          	          <td> 
          	            <%if(tdo.getReciptPic()!=null&&!tdo.getReciptPic().isEmpty()){%>
          	            <img alt="userImg" src="data:image/gif;base64,<%=tdo.getReciptPic()%>" data-toggle="modal" data-target="#myModal<%=tdo.getDepositId()%>" width="40px" height="40px"/>
          	            <div id="myModal<%=tdo.getDepositId()%>" class="modal fade" role="dialog">
                          <div class="modal-dialog">  
                      	    <div class="modal-content">
                	            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            	                <img alt="userImg" src="data:image/ gif;base64,<%=tdo.getReciptPic()%>" /> 
          	                </div> 
          	              </div>
                        </div>
          	            <%} %> 

          		          </td>  
                          </tr>
              			      <%}
              				  }%>	
          		  </tbody>		
              </table>
          	</div>

          </div>

        </div>     
    </div>
  </div>


  <jsp:include page="footer.jsp"></jsp:include>

  <!-- external javascript -->

  <script src="js/bootstrap.min.js"></script>

  <!-- library for cookie management -->
  <script src="./newmis/js/jquery.cookie.js"></script>
  <script src="./newmis/js/jquery.noty.js"></script>
  <script src="./newmis/js/jquery.history.js"></script>
  <!-- application script for Charisma demo -->
  <%-- <script src="./newmis/js/charisma.js"></script> --%>

</body>
</html>




