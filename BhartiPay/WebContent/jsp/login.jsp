<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Map"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>

<title>Bhartipay | Login</title>
<jsp:include page="cssFiles.jsp"></jsp:include>
</head>
<body>

	<!-- HEADER -->
	<jsp:include page="mainHeader.jsp"></jsp:include>
	<!-- /HEADER -->
	
	<!-- MENU BAR -->
		<jsp:include page="mainMenuOption.jsp"></jsp:include>
	<!-- /MENU BAR -->
	
	<div class="section-headline-wrap">
		<div class="section-headline">
			<h2>Login</h2>
			<p>Home<span class="separator">/</span><span class="current-section">Login</span></p>
		</div>
	</div>
	
	
	<!-- SECTION -->
	<div class="section-wrap">
		<jsp:include page="loginForm.jsp"></jsp:include>
		<!-- /FORM POPUP -->

		<div class="clearfix"></div>
	</div>
	</div>
	<!-- /SECTION -->

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
	<!-- /FOOTER -->

	
</body>
</html>
