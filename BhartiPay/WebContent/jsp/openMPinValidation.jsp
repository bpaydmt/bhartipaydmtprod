<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
<script src="js/handlebars-v4.0.10.js" type="text/javascript"></script>
<script id="agnentDetail" type="text/x-handlebars-template"></script>
</head>

<%
	User user = (User) session.getAttribute("User");
%>



<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- contents starts -->



			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->


				<div class=" row">
					<div class="row" id="hidethis2">
						<div id="container">
							<div class="box2 col-md-12">
								<div class="box-inner">



									<div class="box-header well">
										<h2>MPIN</h2>
									</div>


									<div class="box-content row">
										<font style="color: red;"> <s:actionerror />
										</font> <font style="color: blue;"> <s:actionmessage />
										</font>
										<form action="ValidateMPIN" method="post" id="valMpin">

										
							<div class="row">
							
								<div class="col-md-4">
								
									<div class="form-group">
									<label>MPIN<font color="red">
																*</font></label>
										<input type="text" name="cmeBean.mpin" class="form-control mandatory onlyNum mpin-vl" id="agent-mobile" maxlength="4" title="Please provide 4 digit valid MPIN" pattern="/[0-9]{4}/"/>
									</div>
								
								
								</div>
								<div class="col-md-8">
								
									
								
								</div>
								
							</div>
							<div class="col-md-12">
							</form>
												<input type="button" onclick="submitVaForm('#valMpin',this)"	class="btn btn-infobtn btn-info btn-fill submit-form"
													  value="submit" /> 
													 
													 
													 <input
													type="reset" onclick="resetForm('#form_name')"
													class="btn btn-info btn-fill" />
													</div>
										
									
											
									</div>
								</div>
							</div>


						</div>

					</div>
				</div>

			</div>



			<!-- contents ends -->



		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%-- <script src="js/bootstrap.min.js"></script> --%>

	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<%-- <script src="./newmis/js/charisma.js"></script> --%>
	

	
</body>
</html>