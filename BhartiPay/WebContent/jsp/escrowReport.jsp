<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d = new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Escrow Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Escrow Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Escrow Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Escrow Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Escrow Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    $('#dp').datepicker({
   	 language: 'en',
   	maxDate: new Date(),
   	
   });
  $("#dp").blur(function(){
  	//alert()
  	  $('#dp1').datepicker({
  	     	 language: 'en',
  	     	 minDate: new Date($('#dp').val()),
  	     	maxDate: new Date(),
  	     });	
  })
    
} );
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		<div class="box-header well">
			<h2>Escrow Report</h2>
		
		</div>
		
		<div class="box-content row">
		<div class="col-md-12">	<%	String eBalance=(String)request.getAttribute("eBalance");
		
		%>
<h4 ><span><b>Escrow Balance: </b><%=user.getCountrycurrency() %> <%=d.format(Double.parseDouble(eBalance)) %></span> </h4>
</div>
		<form action="EscrowReport.action" method="post">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
																		
										<input type="text"
										value="<s:property value='%{bean.stDate}'/>"
										name="bean.stDate" id="dp"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required >
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									 <input
										type="text" value="<s:property value='%{bean.endDate}'/>"
										name="bean.endDate" id="dp1"
										class="form-control datepicker-here1" placeholder="End Date"
										data-language="en" required >
								</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left">
									
									<div align="center" id="wwctrl_submit">
										<input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Submit">
									</div>
								</div>
							</form>
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Transaction Description</u></th>
						<th><u>Credit Amount(<%=user.getCountrycurrency() %>)</u></th>
						<th><u>Debit Amount(<%=user.getCountrycurrency() %>)</u></th>
						<!-- <th><u>Date</u></th> -->
									
					</tr>
				</thead>
				<tbody>
				<%
		
			List<EscrowBean> eList=(List<EscrowBean>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					EscrowBean tdo=eList.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getTxndesc()%></td>
		             <td><%if(tdo.getCredit()>0){out.print(d.format(tdo.getCredit()));}else{out.print("0.00");}%></td>
		              <td><%if(tdo.getDebit()>0){out.print(d.format(tdo.getDebit()));}else{out.print("0.00");}%></td>
		          <%--  <td><%=tdo.getStDate()%></td> --%>
		             
		            
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



