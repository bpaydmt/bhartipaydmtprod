<% response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %> 
<%@ page session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/whitelabel.css" />

<script type="text/javascript" src = "js/user/login.js"></script>
</head>


<body class="menu-style3">
    
		 
		 
	      <div class="container-fluid">
        <div class="navbar-header">
            <img src="assets/img/wallet_logo.png">
        </div>
       

    </div>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4 style="color:#999; padding-left:8px;">Please enter your OTP here.</h4>	
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-edit"></i>
                        			
                        		</div>
                            </div>
                            <div class="form-bottom">

<s:form action="ValidateOTP" method="POST" cssClass="login-form">
			                    <!-- <form role="form" action="" method="post" class="login-form"> -->
			                    <div class="form-group">
			                   
			                    <font color="red"><s:actionerror></s:actionerror></font>
			                     <font color="blue"><s:actionmessage/></font>
			                   
			                    </div>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                    	<input type="hidden" name="user.userId" placeholder="Username" value="<s:property value='%{user.userId}'/>" class="form-username form-control" id="userid" required>
			                       <input type="text" name="user.otp" placeholder="Enter OTP" value="<s:property value='%{user.otp}'/>" class="form-username form-control" id="otp" required>
			                        
			                        </div>
			                        
			                        <button type="submit" class="btn" name="FrgtPwd" onclick="return frmValidation();">Submit</button>
                                    <br>
                                    <br>
                                    
                                   
			                    </s:form>
			                   
			                     <s:form action="OtpResend" method="POST" cssClass="login-form">
			                     <input type="hidden" name="user.userId" value="<s:property value='%{user.userId}'/>"/>
			                     <input type="submit" class="login-form OTPreset" style="background: #ff6e26;border: none;padding: 4px 7px;color: #FFF;font-size: 12px;" value="Resend OTP"/>
			                     </s:form>
		                    </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            
        </div>
<div style="margin-top: 70px;">
       <jsp:include page="footer1.jsp"/>
       </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>	 

<head>
<meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>
<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
</head>
</html>





