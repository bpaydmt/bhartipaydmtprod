              
                <%@page import="com.bhartipay.wallet.transaction.persistence.vo.Recharge"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.MoneyRemittance"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.BankDetail"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TAG0"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Beneficiary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CardDetail"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.UserDetails"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard" class="wll">Home</a>
        </li>
        <li>
            <a href="Search" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->
 
 <%

 %>
<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Merchant Summary</h2>

                
            </div>
								<div class="box-content row">
								<font color="red"><s:actionerror/> </font>
								<font color="blue"><s:actionmessage/> </font>
									<div class="col-lg-12 col-md-12 box-inner">
									
									
									<div class="col-md-8" style="
    margin-left: 16.33333%;
">
                          <table class="table table-striped" style="
    border: 1px solid #ccc;
    margin: 20px 0 0 0;
    / border-radius: 19px; /">
    	
	
	 
                          <thead>
                        
                                <tr><th>Merchant Details </th><th></th>
                          </tr>
                        
                          </thead>
                        
                          <tbody><tr>
                              <td><strong>MID</strong></td><td>	<s:property value='%{merchantBean.merchantID}'/>
  
	 </td>
                          </tr>
                             <tr>
                              <td><strong>Username</strong></td><td><s:property value='%{merchantBean.merchantUser}'/></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Name</strong></td><td><s:property value='%{merchantBean.merchantName}'/></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Email</strong></td><td><s:property value='%{merchantBean.merchantEmail}'/></td>
                          </tr>
                          
                            <tr>
                              <td><strong>Mobile</strong></td><td><s:property value='%{merchantBean.merchantMobile}'/></td>
                          </tr>
                             <tr>
                              <td><strong>Business Web URL</strong></td><td><s:property value='%{merchantBean.merchantSite}'/></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Status</strong></td><td><s:property value='%{merchantBean.merchantStatus}'/></td>
                          </tr>
                            
                              
                              <tr>
                              <td><strong>Encryption Key</strong></td><td><s:property value='%{merchantBean.encryptionKey}'/></td>
                          </tr>

                            </tbody>
                          </table>
                      </div>
					</div>

<form action="AddMerchant">
<input type="submit" value="Add another merchant" class="btn form-btn btn-info btn-fill">
</form>
								</div>



							</div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


                