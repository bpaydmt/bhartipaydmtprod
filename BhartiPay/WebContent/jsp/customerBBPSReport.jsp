<%@page import="com.bhartipay.bbps.vo.BbpsPayment"%>
<%@page import="com.bhartipay.wallet.matm.MATMLedger"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
<% 
User user = (User) session.getAttribute("User");

DecimalFormat d=new DecimalFormat("0.00");

%>
      
       


            


<div class="box2 col-md-12">
	<div class="box-inner">
	
		
		
		<div class="box-content row">
		
			<form id="bbpsForm" >


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<input	type="hidden" name="inputBean.userId" id="userid" value="<s:property value='%{inputBean.userId}'/>"/>	
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="bbpsdpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									 <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="bbpsdpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left ">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="bbpsReportTable" class="display">
				<thead>


			<tr>
				<th><u>Sr.No</u></th>
				<th><u>Txn Date</u></th>
				<th><u>Txn Id</u></th>
				<th><u>Biller Type</u></th>
				<th><u>Biller Name</u></th>
				<th><u>Biller Id</u></th>
				<th><u>Resp Txn Id</u></th>
				<th><u>Amount</u></th>
				<th><u>Status</u></th>


			</tr>
		</thead>
				<tbody>
			<%
				List<BbpsPayment> list = (List<BbpsPayment>) request.getAttribute("bbpsList");

				int count = 1;
				for (BbpsPayment wtb : list) {
			%>
			<tr>
				<th><%=count++%></th>
				<th><%=wtb.getTxndate()%></th>
				<th><%=wtb.getTxnid() != null ? wtb.getTxnid() : "-"%></th>
				<th><%=wtb.getBillertype()%></th>
				<th><%=wtb.getBillername()%></th>
				<th><%=wtb.getBillerid()%></th>
				<th><%=wtb.getResptxnid() != null ? wtb.getResptxnid() : "-"%></th>
				<th><%=wtb.getTxnamount()%></th>

				<%
					if (wtb.getStatus().toUpperCase().contains("INSUFFICIENT")) {
				%>
				<th>Failed</th>
				<%
					} else {
				%>
				<th><%=wtb.getStatus()%></th>
				<%
					}
				%>

			</tr>
			<%
				}
			%>
		</tbody>
	</table>
		</div>
	</div>

<script>

$(function(){
	$("#bbpsForm").submit(function(e) {


	    $.ajax({
	           type: "POST",
	           url: "GetCustBBPSReport.action",
	           data: $("#bbpsForm").serialize(), 
	           success: function(data)
	           {
	        	   $("#bbpsReport").html(data);
	   		    
	   		    $('#bbpsReportTable').DataTable( {
	   		        dom: 'Bfrtip',
	   		        autoWidth: false,
	   		        order: [[ 0, "desc" ]],
	   		        buttons: [
	   		             {
	   		             
	   		                extend: 'copy',
	   		                text: 'COPY',
	   		                title:'BBPS Details - ' + '<%= currentDate %>',
	   		                message:'<%= currentDate %>',
	   		            },  {
	   		             
	   		                extend: 'csv',
	   		                text: 'CSV',
	   		                title:'BBPS Details - ' + '<%= currentDate %>',
	   		              
	   		            },{
	   		             
	   		                extend: 'excel',
	   		                text: 'EXCEL',
	   		                title:'BBPS Details - ' + '<%= currentDate %>',
	   		            
	   		            }, {
	   		             
	   		                extend: 'pdf',
	   		                text: 'PDF',
	   		                title:'BBPS Details - ' + '<%= currentDate %>',
	   		                message:" "+ "<%= currentDate %>" + "",
	   		               
	   		            },  {
	   		             
	   		                extend: 'print',
	   		                text: 'PRINT',
	   		                title:'BBPS Details - ' + '<%= currentDate %>',
	   		              
	   		            }
	   		        ]
	   		    } );
	   		    
	   		    
	   		    $('#matmdpStart').datepicker({
	   		     language: 'en',
	   		     autoClose:true,
	   		     maxDate: new Date(),
	   		     

	   		    });
	   		 
	   		     

	   		     
	   		 
	   		    $("#matmdpStart").blur(function(){
	   		       $('#matmdpEnd').val("")
	   		     $('#matmdpEnd').datepicker({
	   		          language: 'en',
	   		         autoClose:true,
	   		         minDate: new Date(converDateToJsFormat($('#matmdpStart').val())),           
	   		         maxDate: new Date(),
	   		         
	   		        }); 
	   		    })
	         	}
	   		
	   		
	    })
	           
	    

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});	
})



</script>






