<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script> 

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ > 

<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>
 
<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
   }); 
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>

</head>

<% 
	User user = (User) session.getAttribute("User");
	Date myDate = new Date(); 
	SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
	String toDate=format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from= cal.getTime();    
	String fromDate = format.format(from);
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

	//List<String>list=(List<String>)request.getAttribute("resultList");
	DecimalFormat d=new DecimalFormat("0.00"); 
%> 
<body>

    <!-- topbar starts -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends --> 

    <!-- left menu starts -->
    <jsp:include page="mainMenu.jsp"></jsp:include>
    <!-- left menu ends -->

 
          
	<div id="main" role="main"> 
	    <div id="content"> 

			<div class=" row"> 

				<div class="box col-md-12">
					<div class="box-inner"> 
						<div class="box-header">
							<h2>AEPS Aggregator Settlement</h2>
						</div> 
						<div class="box-content">  
							<div class="row"> 
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
									<label for="apptxt">Select Aggregator  </label>  
								    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
								    <s:select list="%{aggrigatorSettledMap}" headerKey="SelectAnyOne" headerValue="Please select" id="aggregatorid" onchange="prepareAggrigatorList();" name="aggregatorid" cssClass="form-username" requiredLabel="true" />
								 </div>
								<div class="col-md-4 col-sm-4 col-xs-12"> 
									<div  id="wwctrl_submit">
										<button class="btn btn-success submit-form" id="submit" onclick="prepareAepsSettelment();">Submit</button>
										<button class="btn btn-primary btn-fill" id="reset" onclick="resetAll();">Reset</button>
									</div>
								</div>
							</div>
					    </div>
					    <hr style = "border-width: 5px;"> 
					    <div  class="box-content" id="settlementDiv" style="display: none">
						    <div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="form-group">
											<label for="apptxt">Total Settlement Amount </label>
											<input id="totalSettlementAmountID" name="totalSettlementAmount" readonly="readonly" type="text" />
											<div class="errorMsg" style="display: none;"></div>
								
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="apptxt">Total Aeps Transaction Amount </label>
											<input id="aepsSettlementAmountID" name="aepsSettlementAmount" readonly="readonly" type="text" />
											<div class="errorMsg" style="display: none;"></div>
										</div>
									</div>
								
									<div class="col-md-4">
										<div class="form-group">
											<label for="apptxt">Amount to be Settled </label>
												<input id="settledAmountID" name="settledAmount" readonly="readonly"  type="text" />
											<div class="errorMsg"></div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="apptxt">Available Amount for Settlement</label>
												<input id="availableSettlementAmountID" name="availableSettlementAmount" onkeyup="return NumericValidation(this);" onblur="validateAmount(this);" type="text" />
											<div class="errorMsg"></div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button class="btn btn-success submit-form" style="margin-top: 20px;" onclick="submitAvailableSettlement()">Submit</button>
											<button onclick="resetAll();" class="btn reset-form btn-info btn-fill" style="margin-top: 20px;">Cancel</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div> 
        </div>
    </div>
        

        <!-- contents ends -->


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%-- <script src='js/bootstrap.min.js'></script> --%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<script>
function prepareAepsSettelment(){
	var aggregatorid = document.getElementById('aggregatorid').value;
	
	
	if(aggregatorid == 'SelectAnyOne'){
		alert('Please select aggregetor.');
	}else{
	
	$.ajax({
		 method:'Post',
		 url:'AepsAggrigatorSettledRequest',
		 cache:0,
		 async : false,
         data : "aggregatorid="+aggregatorid,
		 success:function(response){
		 
			 var resJson = $.parseJSON(response);
			 
			 if(resJson.status == "ERROR"){
				 alert(resJson.statusMsg);
			 }
			 else{
				var total = resJson.total.totalSettlementAmount;
				var aeps =	resJson.aeps.totalAepsTransactionAmount;
				
				
				if(total != null && total != "undefined" && aeps != null && aeps != "undefined"){
					if(parseFloat(total) > parseFloat(aeps))
					{
						var settledAmount = total - aeps;
						$("#totalSettlementAmountID").val(total);
						$("#aepsSettlementAmountID").val(aeps);
						$("#settledAmountID").val(settledAmount);
						
						$("#settlementDiv").show();
					}
					else
					{
						alert('\'Total Settlement Amount\' should be greater than \'Total Aeps Transaction Amount\'.');
					}
				}else
					alert('Amount is null.');
			 }
		}
	 });
	}
}


function prepareAggrigatorList(){
	$("#settlementDiv").hide();
}

function greaterThanZero(input){
    if(isNaN(input)){
            return false;
    }
    else{
         if(parseFloat(input)<=0){
                 return false;
         } else {
                 return true;
         }
    }
}

function validateAmount(amountInput) {
    var amount = amountInput.value;
    
    if(amount != "" && !isNaN(amount)){
            if(!greaterThanZero(amount)){
            	$("#availableSettlementAmountID").val('');
                    return false;
            }
    }
    
    var  settledAmount = $("#settledAmountID").val();
    
    if(parseFloat(amount) > parseFloat(settledAmount)){
    	$("#availableSettlementAmountID").val('');
    	alert('\'Available Amount for Settlement\' should not be greater than \'Amount to be Settled\'.');
    	document.getElementById("focus").focus();
    }
}

function NumericValidation(item) {
    var Number = item.value
    if (isNaN(Number) || Number == "") {
    	$("#availableSettlementAmountID").val('');
            return false;
    }
    return true;
}


function submitAvailableSettlement(){
	
	var totalSettlementAmount = $("#totalSettlementAmountID").val();
	var aepsSettlementAmount = $("#aepsSettlementAmountID").val();
	var settledAmount = $("#settledAmountID").val();
	var availableSettlementAmount = $("#availableSettlementAmountID").val();
	var aggregatorid = $('#aggregatorid').val();
	//var aggregatorid = 'OAGG001057';
	 
	if(	aggregatorid != null && aggregatorid != "undefined" && aggregatorid != '' && 
		totalSettlementAmount != null && totalSettlementAmount != "undefined" && totalSettlementAmount != '' &&
		aepsSettlementAmount != null && aepsSettlementAmount != "undefined" && aepsSettlementAmount != '' &&
		settledAmount != null && settledAmount != "undefined" && settledAmount != '' &&
		availableSettlementAmount != null && availableSettlementAmount != "undefined" && availableSettlementAmount != ''
	  ){
		
		var aggSettlementObj = {
			aggregatorid : aggregatorid,
			totalSettlementAmount : totalSettlementAmount,
			aepsSettlementAmount : aepsSettlementAmount,
			settledAmount : settledAmount,
			availableSettlementAmount : availableSettlementAmount
		};
		
		$.ajax({
			 method:'Post',
			 url:'submitAepsAggregatorSettlement',
			 cache:0,
			 async : false,
	        data : "aggSettlement="+JSON.stringify(aggSettlementObj),
			 success:function(response){
				 var resJson = $.parseJSON(response);
				 if(resJson.status == "ERROR"){
					 alert(resJson.statusMsg);
				 }
				 else
				 	aggSettlemtResponse(resJson);
			}
		 });
	}
}

function aggSettlemtResponse(json){
	alert(json.status.creditwallet);
	resetAll();
}	

function resetAll(){
	$('#aggregatorid').val('SelectAnyOne');
	$('#totalSettlementAmountID').val('');
	$('#aepsSettlementAmountID').val('');
	$('#settledAmountID').val('');
	$('#availableSettlementAmountID').val('');
	$("#settlementDiv").hide();
}
</script>
</body>
</html>



