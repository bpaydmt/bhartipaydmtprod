<%@page import="com.bhartipay.lean.CmsBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="com.google.gson.Gson"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="newThemeLinks.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>


<style>
.popup-overlay-other1 {
	visibility: hidden;
	position: fixed;
	width: 75%;
	height: 75%;
	left: 25%;
	top: 38%;
}

.overlay-other1 {
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0;
	left: 0;
	background: rgba(0, 0, 0, .6);
	/* z-index: 100000; */
}

.popup-overlay-other1.active {
	visibility: visible;
	z-index: 999;
}

.popup-content-other1 {
	visibility: hidden;
}

.popup-content-other1.active {
	visibility: visible;
}

.box-login-title-other1 {
	top: 30%;
	color: #111;
	position: absolute;
	width: 50%;
	height: auto;
	left: 25%;
	background-color: rgba(255, 255, 255, 0.8);
	padding-top: 10px;
	padding-bottom: 10px;
}

.MessageBoxMiddle-other1 {
	position: relative;
	left: 10%;
	width: 60%;
}

.close-other1 {
	float: none;
	font-size: 16px;
	font-weight: bold;
	line-height: 1;
	color: #fff;
	text-shadow: none;
	opacity: 1;
	background: #a57225 !important;
	padding: 10px;
}

.popup-btn-other1 {
	font-size: 16px;
	font-weight: bold;
	color: #fff;
	background: #a57225 !important;
	padding: 7px;
	/* cursor: pointer; */
}

.MessageBoxMiddle-other1 .MsgTitle-other1 {
	letter-spacing: -1px;
	font-size: 24px;
	font-weight: 300;
}

.txt-color-orangeDark-other1 {
	color: #a57225 !important;
}

.MessageBoxMiddle-other1 .pText-other1 {
	font-size: 16px;
	text-align: center;
	margin-top: 14px;
}

.MessageBoxButtonSection-other1 span {
	float: right;
	margin-right: 7px;
	padding-left: 15px;
	padding-right: 15px;
	font-size: 14px;
	font-weight: 700;
}

.popup-overlay-other1 input[type="text"] {
	border-bottom-color: #111 !important;
	margin-bottom: 15px;
	color: #111 !important;
}
</style>

<%
	Date d1 = new Date();
	SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
	String currentDate = df.format(d1);
%>

<script type="text/javascript">
 
   $(document).ready(function() {
    
        
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     );
  //Set the initial state of the picker label
  	if("<s:property value='%{inputBean.stDate}'/>"==""){
    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
    $("#to").val(moment().format('DD-MMM-YYYY'));
  	}else{
  	$('#reportrange span').html('<s:property value='%{inputBean.stDate}'/>' + ' - ' + '<s:property value='%{inputBean.endDate}'/>');	
  	}
 });


</script>

<script type="text/javascript">
    $(document).ready(function() { 
        
        $('#example').DataTable( {
        	dom: "<'datatableHeader'<'row'<'col-md-6 col-sm-12'f><'col-md-6 col-sm-12'B>>>" +
     		"<'row'<'col-sm-12'tr>>" +
     		"<'datatableFooter' <'row'<'col-md-6 col-sm-12'i> <'col-md-6 col-sm-12'p> >>",
            autoWidth: false,
            "order": [[ 0, "desc" ]],
            buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Money Transfer - ' + '<%=currentDate%>',
                message:'<%=currentDate%>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Money Transfer - ' + '<%=currentDate%>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Money Transfer - ' + '<%=currentDate%>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Money Transfer Report',
                message:"Generated on" + "<%=currentDate%>" + "",
             
              
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Money Transfer - ' + '<%=currentDate%>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
            ]
        } ); 
        } ); 
</script>

<style>
#agentFooter, .paymentref {
	display: none
}
</style>
</head>

<%
	User user = (User) session.getAttribute("User");
	Date myDate = new Date();
	System.out.println(myDate);
	SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
	String toDate = format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from = cal.getTime();
	String fromDate = format.format(from);
	Object[] commDetails = (Object[]) session.getAttribute("commDetails");
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
	DecimalFormat d = new DecimalFormat("0.00");
%>



<body onload="updateDate()">

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->

	<div class="">

		<!-- left menu starts -->
		<jsp:include page="mainMenu.jsp"></jsp:include>
		<!-- left menu ends -->

		<!-- contents starts -->

		<div id="mainWrapper">
			<div class="container-fluid">
				<div class="row">

					<div class="box2 col-md-12">
						<div class="box2">
							<!--Creates the popup body-->
							<div class="popup-overlay-other1 overlay-other1"
								style="display: block;">
								<div class="popup-content-other1">
									<div class='box-login-title-other1'>
										<div class="MessageBoxMiddle-other1">
											<span class="MsgTitle-other1"
												style="color: orange; font-size: 25px; font-weight: bold;">Do
												you want to print?</span>
											<div style="text-align: center; margin-bottom: 10px;">
												<div id="popupInfoDetails" style="width: 140%;"></div>
												<button type="button" class="btn btn-success"
													id="payPopUpYes" onclick="printNow();">Yes</button>
												<button type="button" class="close-other1 btn btn-primary"
													onclick="payPopUpNo();" style="padding: 6px 12px;">No</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Popup End -->




							<div class="box-inner">
								<div class="box-header">
									<h2>CMS Report</h2>
								</div>

								<div class="box-content">

									<form action="GetCmsReport" method="post" autocomplete="off">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<input type="hidden" value="${csrfPreventionSalt}"
													name="csrfPreventionSalt">
												<div id="reportrange"
													style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
													<i class="fa fa-calendar"></i>&nbsp; <span></span> <i
														class="fa fa-caret-down"></i>
												</div>

												<input type="hidden"
													value="<s:property value='%{inputBean.stDate}'/>"
													name="inputBean.stDate" id="from"
													class="form-control datepicker-here1"
													placeholder="Start Date" data-language="en" required /> <input
													type="hidden"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="to"
													class="form-control datepicker-here2"
													placeholder="End Date" data-language="en" required>
											</div>
											<%
													if (user.getUsertype() != 2 && user.getUsertype() != 3 && user.getWhiteLabel() == 0) {
												%>
											<div class="col-md-4 col-sm-6 col-xs-12 form-group">
												<s:select list="%{aggrigatorList}" headerKey="All"
													headerValue="All" id="aggregatorid"
													name="walletBean.aggreatorid" cssClass="form-username"
													requiredLabel="true" />
											</div>
											<%
													} else if (user.getUsertype() == 3) {
												%>
											<div class="col-md-4 col-sm-6 col-xs-12 form-group">
												<s:select list="%{agentList}" headerKey="-1"
													headerValue="Select Agent" id="agentsub"
													name="inputBean.userId" cssClass="form-username"
													requiredLabel="true" />
											</div>
											<%
													}
												%>
											<div class="col-md-2 col-sm-4 col-xs-12">
												<div id="wwctrl_submit">
													<input class="btn btn-success" id="submit" type="submit"
														value="Submit"> <input
														class="btn btn-primary btn-fill" id="reset" type="reset"
														value="Reset">
												</div>
											</div>

											<%
													List<CmsBean> list = (List<CmsBean>) request.getAttribute("agentList");
													int txnCount = 0;
													double txnAmount = 0;
												%>
										</div>

									</form>

								</div>
							</div>
						</div>
					</div>


					<div class="col-md-12">
						<div id="xyz">
							<table id="example"
								class="scrollD cell-border dataTable no-footer"
								style="width: 100%;">
								<thead>
									<tr>
										<th><u>Sr.No</u></th>
										<th><u>Txn Date</u></th>
										<% if (user.getUsertype() != 2) { %>
										<th><u>Agent Id</u></th>
										<% } %>
										<th><u>Txn Id</u></th>
										<th><u>Bank Name</u></th>
										<th><u>Account No</u></th>
										<th><u>Operator</u></th>
										<th><u>Action</u></th>
										<th><u>Amount</u></th>
										<th><u>Mobile No</u></th> 
										<th><u>Status</u></th> 
									</tr>
								</thead>
								<tbody>
									<%
											if (list != null) {

												int j = 1;
												for (int i = 0; i < list.size(); i++) {
													CmsBean tdo = list.get(i);
										%>

									<tr>
										<th><%=j++%></th>
										<th><%=tdo.getTxnDate()%></th>
										<% if (user.getUsertype() != 2) { %>
										<th><%=tdo.getAgentId()%></th>
										<%}
											Gson gson = new Gson();
											String userJson = gson.toJson(user);
											String aepsJson = gson.toJson(tdo);
										 %>
                                       <th><a href='javascript:;'
											onclick='getTxnDetails(<%=userJson%>,<%=aepsJson%>);'><%=tdo.getTxnId() != null ? tdo.getTxnId() : "-"%></a></th>

										 
										<th><%=tdo.getBankName() != null ? tdo.getBankName() : "-"%></th>
                                        <th><%=tdo.getAccountNo() != null ? tdo.getAccountNo() : "-"%></th>
										
										<th><%=tdo.getOperator() != null ? tdo.getOperator() : "-"%></th>
                                        
										<th><%=tdo.getAction()!=null ? tdo.getAction() : "-"%></th>
										<th><%=tdo.getAmount()%></th>
										<th><%=tdo.getMobileNo()%></th>
										<th><%=tdo.getStatus() != null ? tdo.getStatus() : "-"%></th>
										</tr>
									<%
											}
											}
										%>
								</tbody>
							</table>
						</div>
					</div>




				</div>



				<!-- contents ends -->


			</div>
		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


	<div id="newAgentBox" class="modal fade" role="dialog">
		<div class="modal-dialog model-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agent Details</h4>
				</div>
				<div class="modal-body">
					<p>Loading....</p>
				</div>

			</div>

		</div>
	</div>
	<script>


function getTxnDetails(userAgent, bbpsTxn){
	
var date=bbpsTxn.txnDate;
var txnId=bbpsTxn.txnId;
var wallet=bbpsTxn.walletId;
var agentId=bbpsTxn.agentId;
var txnType=bbpsTxn.txnType;
var mobile=wallet.substring(0, 10);
	
	//alert(bbpsTxn.status+"  "+bbpsTxn.agentId+"  "+bbpsTxn.walletId+"   "+bbpsTxn.commissionAmt+" "+bbpsTxn.rrn+"   "+bbpsTxn.amount+"    "+bbpsTxn.aadharNumber+"    "+bbpsTxn.mobileNo+"    "+bbpsTxn.bankName+"    "+bbpsTxn.txnType+"   "+bbpsTxn.txnId+"  "+bbpsTxn.txnDate)
	
	
	var result = '';
	result = result + '<div class="box box-primary" id="billPrintArea" style="width:100%;">';
	result = result + '<input id="printRefid" name="printRefid" type="hidden" />';
	result = result + '<input id="billerPrintResponseTags" name="billerPrintResponseTags" type="hidden" />';
	result = result + '<!-- BOX TITLE or SECTION TITLE START -->';
	result = result + '<div class="box-header with-border" id="billHeader" style="height: 70px; display:none;">';
	result = result + '	<table id="printPaymentHeader" width="100%">';
	result = result + '		<tr>';
	result = result + '			<td width="33%" style="text-align:left;"><img id="merchantLogo" style="width:20%;"></td>';
	result = result + '			<td width="33%" style="text-align:center;"><h3 class="box-title no-margin"><b> Receipt </b></h3></td>';
	result = result + '			<td width="33%" style="text-align:right;"><img id="bbpsLogo" src="./image/AEPS.png" style="width:80%;"></td>';
	result = result + '		</tr>';
	result = result + '	</table>';
	result = result + '</div>';
	result = result + '<!-- BOX TITLE or SECTION TITLE END -->';
	result = result + '<!-- BOX BODY START -->';
	result = result + '<div class="box-body">';
	result = result + '   <div class="row123">';
	result = result + '   <table id="printPayment" width="100%" style="border: 1px solid black;border-collapse: collapse;" >';
<!--
    result = result + '	<tr id="printAgentTR" style="display:none;">';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Txn Date</td>';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printShopName">'+bbpsTxn.txnDate+'</span></td>';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Txn ID</td>';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printAgentMobile">'+bbpsTxn.txnId+'</span></td>';
	result = result + '	</tr>';
	-->
	result = result + '	<tr>';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Agent ID</td>';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerNumber">'+bbpsTxn.agentId+'</span></td>';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Bank Name</td>   ';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerName">'+bbpsTxn.bcname+'</span></td>';
	result = result + '	</tr>';
	result = result + '   <tr>';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Mobile Number</td>    ';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillerName">'+mobile+'</span></td>';
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Aadhar No</td>   ';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;border: 1px solid black;"><span id="printBillerID">'+bbpsTxn.aadharNumber+'</span></td>';
	result = result + '	</tr>';
	result = result + '   <tr>';

	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Txn Type</td>    ';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillDate">'+bbpsTxn.txnType+'</span></td>';
	
	result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">RRN No</td>   ';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printDueDate">'+bbpsTxn.rrn+'</span></td>';
	result = result + '	</tr>';

	result = result + '	</table><br>';
	result = result + '	<table id="printPayment2" width="100%" style="border: 1px solid black;border-collapse: collapse;" >';
	result = result + '		<tr style="font-weight: bold;">';
	result = result + '			<td width="15%" style="text-align:left;border: 1px solid black;">Txn ID</td>';
/*	result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;">Txn Type</td>';
*/	result = result + '			<td width="18%" style="text-align:left;border: 1px solid black;">Amount</td>';
	result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;">Date & Time</td>';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;">Transaction Status</td>';
	result = result + '		</tr>';
 
	result = result + '		<tr>';
	result = result + '			<td width="15%" style="text-align:left;border: 1px solid black;"><span id="printOrderID">'+txnId+'</span></td>';
/*	result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTransactionID">'+bbpsTxn.txnType+'</span></td>';
*/	result = result + '			<td width="18%" style="text-align:left;border: 1px solid black;"><span id="printBillAmount">'+bbpsTxn.amount+'</span></td>';
	result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTxnDateTime">'+date+'</span></td>';
	result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;color:green;"><span id="printTxnStatus">'+bbpsTxn.status+'</span></td>';
	result = result + '		</tr>';
 	
	result = result + '	</table>';
	result = result + '	<table id="printPoweredBy" style="display:none;">';
	result = result + '		<tr>';
	result = result + '			<td width="30%">';
	//result = result + '				<img src="./image/BharatAssured.png" style="width:35%;">';
	result = result + '			</td>    ';
	result = result + '			<td width="70%" style="text-align:right; font-style: italic;"><!-- Powered by Bhartipay Services Pvt. Ltd. --></td>';
	result = result + '		</tr>';
	result = result + '	</table>';
   /* result = result + '	<table id="removePrint" class="scrollD cell-border no-footer" width="100%" style="border-top: 1px solid #fafafa;" >';
	result = result + '		<tr>';
	result = result + '			<td width="25%"></td>';
	result = result + '			<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-right" id="btn_bill_print" onclick="printNow();">Print</button></td>';
	result = result + '			<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-left" id="btn_cancel_print" onclick="cancelNow();">Cancel</button></td>';
	result = result + '			<td width="25%"></td>';
	result = result + '		</tr>';
	result = result + '	</table>'; */
	result = result + '	</div>';
	result = result + '	</div>';
	result = result + '	<!-- BOX BODY END -->';
	result = result + '</div>';

	$(".popup-overlay-other1, .popup-content-other1").addClass("active");
	$("#popupInfoDetails").empty().append(result);

}


function printNow()
{
	var originalContents = document.body.innerHTML;
	var divToPrint=document.getElementById("billPrintArea");
	
	//$('#bbpsLogo').attr('src','./image/AEPS.png');
	
	$("#printPoweredBy").show();
	$("#billHeader").show();
	$("#printAgentTR").show();
	
	/* $("#billPrintArea").find('#removePrint').remove(); */
	newWin= window.open("");
	newWin.document.write(divToPrint.outerHTML);
	newWin.print();
	newWin.close();
	document.body.innerHTML = originalContents;
}


function payPopUpNo(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
}


function getAepsDetails(txnId){

	$.ajax({
		  type: "POST",
		  url: "AepsDetailsView",
		  data: "txnId="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}




function updateDate() {
	
}

function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>
</body>
</html>



