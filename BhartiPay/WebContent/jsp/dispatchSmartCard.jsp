
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.SmartCardBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletKYCBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>


<%
	Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>

<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Dispatch Prepaid Card - ' + '<%=currentDate%>',
                message:'<%=currentDate%>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Dispatch Prepaid Card - ' + '<%=currentDate%>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Dispatch Prepaid Card - ' + '<%=currentDate%>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Dispatch Prepaid Card - ' + '<%=currentDate%>',
                message:" "+ "<%=currentDate%>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Dispatch Prepaid Card - ' + '<%=currentDate%>',

			} ]
		});

	});
</script>




</head>

<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- contents starts -->



			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->


				<div class=" row">
					<div class="row" id="hidethis2">
						<div id="container">
							<div class="box2 col-md-12">
								<div class="box-inner">

									<div class="box-header well">
										<h2>Card Dispatch</h2>
									</div>
								</div>
							</div>
							<font color="red"><s:actionerror /> </font> <font color="blue"><s:actionmessage />
							</font>



							<%
								List<SmartCardBean> smartCardList = (List<SmartCardBean>) request
										.getAttribute("smartCardList");
							%>

							<div id="xyz">
								<table id="example" class="display">
									<thead>


										<tr>
											<th><u>Req Id</u></th>
											<th><u>Card Type</u></th>
											<th><u>User Id</u></th>
											<th><u>First Name</u></th>
											<th><u>Last Name</u></th>
											<th><u>Preferred Name</u></th>
											<th><u>Email</u></th>
											<th><u>Mobile</u></th>
											<!-- <th><u>Card Pin</u></th>
											<th><u>Card Number</u></th> -->
											<th><u>Card Status</u></th>

											<th style="width: 120px"><u></u></th>

										</tr>
									</thead>
									<tbody>
										<%
											if (smartCardList != null && smartCardList.size() > 0) {
												for (SmartCardBean wmb : smartCardList) {
										%>
										<tr>
											<td><%=wmb.getReqId()%></td>
											<td>
												<%
													if (wmb.getCardType() != null
																	&& wmb.getCardType().equalsIgnoreCase("PC")) {
																out.print("Physical Card");
															} else if (wmb.getCardType() != null
																	&& wmb.getCardType().equalsIgnoreCase("VC")) {
																out.print("Virtual Card");
															}
												%>
											</td>
											<td><%=wmb.getUserId()%></td>
											<td><%=wmb.getName()%></td>
											<td><%=wmb.getLastName()%></td>
											<td><%=wmb.getPreferredName()%></td>
											<td><%=wmb.getEmailId()%></td>
											<td><%=wmb.getMobileNo()%></td>
											<%-- <td>
												<%
													if (wmb.getPrePaidCardPin() != null) {
																out.print(wmb.getPrePaidCardPin());
															}
												%>
											</td> 
											<td>
												<%
													if (wmb.getPrePaidCardNumber() != null) {
																out.print(wmb.getPrePaidCardNumber());
															}
												%>
											</td>--%>
											<td><%=wmb.getPrePaidStatus()%></td>
											<td>
												<%
													if (wmb.getPrePaidStatus() != null
																	&& wmb.getPrePaidStatus().equalsIgnoreCase(
																			"WalletCreate")) {
												%>
												
												<input type="submit" value="Dispatch" data-toggle="modal"
												data-target="#myModal<%=wmb.getReqId()%>"
												class="btn btn-sm btn-block btn-success"> <%
 	}
 			if (wmb.getPrePaidStatus() != null
 					&& wmb.getPrePaidStatus().equalsIgnoreCase(
 							"Card Linked")) {
 %>										<input type="submit" value="Block Card" data-toggle="modal"
												data-target="#myModal2<%=wmb.getReqId()%>"
												class="btn btn-sm btn-block btn-success">
 <%
 	}if(wmb.getCardType()!=null&&wmb.getCardType().equalsIgnoreCase("VC")&&wmb.getPrePaidStatus().equalsIgnoreCase("CradBlocked")){
 		%>
 		
 		<input type="button" value="Link Card" class="btn btn-sm btn-block btn-success" onclick="submitVaForm('#linkVirtualCard<%=wmb.getReqId()%>')">
 		<%
 	}
 %>
 <form action="LinkVirtualCard" id="linkVirtualCard<%=wmb.getReqId()%>">
 <input	type="hidden" name="smartCardBean.reqId" value="<%=wmb.getReqId()%>"> 
 </form>

												<div id="myModal2<%=wmb.getReqId()%>" class="modal fade"
													role="dialog">
													<div class="modal-dialog">

														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal">&times;</button>
																<h4 class="modal-title">Block prepaid Card</h4>
															</div>
															<div class="modal-body">

																<form action="BlockCardByAggregator"
																	method="post">
																	<div class="form-group">
																		<input type="hidden" value="${csrfPreventionSalt}"
																			name="csrfPreventionSalt"> <input
																			type="hidden" name="smartCardBean.reqId"
																			value="<%=wmb.getReqId()%>"> <label>
																			Select reason for blocking card </label> <select
																			name="smartCardBean.blockedType">
																			<option value="lost">Lost</option>
																			<option value="stolen">Stolen</option>
																			<option value="damaged">Damaged</option>

																		</select>
																	</div>

																	<div class="text-right">
																		<input type="submit" class="btn btn-info"
																			value="Block prepaid card">
																	</div>

																</form>

															</div>


														</div>

													</div>
												</div>




												<div id="myModal<%=wmb.getReqId()%>" class="modal fade"
													role="dialog">
													<div class="modal-dialog">

														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal">&times;</button>
																<h4 class="modal-title">Dispatch Prepaid Card</h4>
															</div>
															<div class="modal-body">

																<form action="DispatchSmartCardRequest"
																	method="post" id="dispatchSmart<%=wmb.getReqId()%>">
																	<div class="form-group">
																		<input type="hidden" name="smartCardBean.reqId"
																			value="<%=wmb.getReqId()%>"> <label>
																			Card Proxy Number </label> <input type="text"
																			name="smartCardBean.prePaidCardPin" maxlength="12"
																			id="card<%=wmb.getReqId()%>"
																			class="form-control onlyNum mandatory" />

																	</div>
																	<%-- <div class="form-group">
																		<label> Card Number </label> <input type="text"
																			name="smartCardBean.prePaidCardNumber" maxlength="16"
																			id="cardNum<%=wmb.getReqId()%>"
																			class="form-control onlyNum mandatory" />

																	</div> --%>
																	<div class="text-right">
																		<input type="button"
																			onclick="submitVaForm('#dispatchSmart<%=wmb.getReqId()%>')"
																			class="btn btn-info" value="Dispatch prepaid card">
																	</div>

																</form>

															</div>


														</div>

													</div>
												</div> </br>
											</td>

										</tr>
										<%
											}
											}
										%>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>

			</div>



			<!-- contents ends -->



		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->
	<!-- ** -->




</body>
</html>


