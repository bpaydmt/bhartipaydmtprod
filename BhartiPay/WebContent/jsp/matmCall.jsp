<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>




<style>

  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;  // Removehover 
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  
  .menueWrapper{
      margin: 0px;
    list-style-type: none;
    border: none;
    padding-left:0px;
  }
  .tabthenopen {
      border: 2px solid #ccc !important;
      text-align: center;
      text-align: center;
    border-radius: 30px;
    margin-bottom: 10px;
        cursor: pointer;
  }
  .tabthenopen:last-child{
  margin-bottom:0px;
  }
  .tabthenopen a{
      position: static;
          line-height: 35px;
    color: #000;
  }

</style>



</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row"> 
            <div class="col-md-12">
            <div style="margin-top: 15px;margin-bottom: 5px;">
                <img src="./image/clipboard-image.png" style="width:5%;">
            </div>
            	<div class="newRechargeUi no-chosen-drop">
	            	<div  id="OnlyForTAbAnchor"  style="padding:10px;" class="box-content col-lg-12 col-md-12 col-sm-12 col-xs-12 borderfortabbutton" style="">

		                <ul style="margin:0px" class="menueWrapper col-lg-2 col-md-2 col-sm-2 col-xs-12">

		                	<li class="tabthenopen">
						      	<a href="#aaa" data-toggle="tab">Withdrawal</a> 
						    </li>
		                	<li class="tabthenopen">
						      	<a href="#bbb" data-toggle="tab">Balance Enquiry</a> 
						    </li>
						    <li class="tabthenopen">  
						      	<a href="#ccc" data-toggle="tab">Transaction Status</a> 
						    </li>

						</ul>

					    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 tab-content" style="margin-top: 17px;"> 

					      <div class="tab-pane" id="aaa"> 
				            <div class="box-content">      
						        <form action="MatmTransactionCall" id="MatmTransactionCall" method="post" name="PG">
								    <div class="row">
										<font color="red"><s:actionerror/> </font>
										<font color="blue"><s:actionmessage/></font>
				             			<div class="col-md-12"> 

											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Enter Amount <font color="red"> *</font></label> <input
														class="form-control mandatory amount-vl" name="inputBean.trxAmount" type="text"
														id="amount" maxlength="6" placeholder="Enter Amount" />

												</div>
											</div> 
											<div class="col-md-4">
												<div class="form-group">
													<input type="hidden" name="inputBean.txnType" value="156">
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
													<button type="submit"  onclick="submitVaForm('#MatmTransactionCall',this)" class="btn btn-success submit-form" style="margin-top: 20px;">Submit
													</button>
													<button type="reset" onclick="resetForm('#MatmTransactionCall')" style="margin: 16px 19px 0 13px; position: relative; top: 1px;" class="btn btn-primary btn-fill" value="Reset">Reset</button> 
											    </div> 
											</div>

										</div>
									</div>
								</form> 
				            </div> 
					      </div>

					      <div class="tab-pane" id="bbb">
					        <div class="box-content">   
								<form action="MatmTransactionCall" id="matmBalance" method="post" name="PG">
									<div class="row">
										<font color="red"><s:actionerror/> </font>
										<font color="blue"><s:actionmessage/></font>
							         	<div class="col-md-12">  
											<div class="col-md-4">
												<div class="form-group">
													<input type="hidden" name="inputBean.txnType" value="157">
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<button type="submit"  onclick="submitVaForm('#matmBalance',this)" class="btn btn-success submit-form" style="margin-top: 20px;">Submit</button>
													<button type="reset" onclick="resetForm('#matmBalance')"  style="margin: 16px 19px 0 13px; position: relative; top: 1px;" class="btn btn-primary btn-fill" value="Reset">Reset</button> 
												</div>
											</div>
										</div>
									</div>
								</form> 
					        </div>  
					      </div>

					      <div class="tab-pane" id="ccc">
					        <div class="box-content"> 
								<form action="MatmStatusCall" id="matmstatus" method="post" name="PG"> 
									<div class="row">
										<font color="red"><s:actionerror/> </font>
										<font color="blue"><s:actionmessage/> </font>
										<div class="col-md-12"> 
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Enter Transaction id <font color="red"> *</font></label> 
													<input class="form-control" name="inputBean.txnId" type="text"
													id="amount" maxlength="12" placeholder="Enter Transaction id" /> 
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<input type="hidden" name="inputBean.txnType" value="158">
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<button type="submit"  onclick="submitVaForm('#matmstatus',this)" class="btn submit-form btn-success btn-fill" style="margin-top: 20px;">Submit</button>
													<button type="reset" onclick="resetForm('#aepsstatus')"  style="margin: 16px 19px 0 13px; position: relative; top: 1px;" class="btn btn-info btn-fill" value="Reset">Reset</button> 
												</div>
											</div>
										</div>
										</div>
								</form>  
					        </div> 				      	
					      </div>

				        </div>

			        </div>
                </div>
            </div>
        </div>
	</div> 
</div><!--/.fluid-container-->


<script>
    function activeTab(tab){
      $('.tabthenopen a[href="#' + tab + '"]').tab('show');
  };

  activeTab('aaa');
</script>


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>