 <%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 <script >
  function closePopup(){
	 $("#confirmation").fadeOut();

	 }
function submitCashInForm(){
	//var mobile = $("#mobileno").val();
	var amount = $("#trxAmount").val();
	

	$.ajax({
		  type: "POST",
		  url: 'CalculateCashOutSurcharge',
		  data:{"suBean.amount":amount},
		  success: function( data ) {
			  console.log(data)
			  if(data.status == 1000){
				 var surcharge =  data.samount;
				 var amount =  data.amount;
			   $("#confirmation").fadeIn(); 
			   $("#surcharge").text(surcharge)
			   $("#cash-in-amount").text(amount)
			   $("#total-cash-in").text(surcharge + amount );
			   $("#surChargeAmount").val(surcharge)
			  }else{
				  alert("Oops!!! something went wrong. Please try after sometime.")
			  }
		  }, 
		
		});
	
	
}
 </script>
  
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->


<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Cash Out</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<!-- <form action="PaymentGateway.action" id="form_name" method="post" name="PG"> -->
		<form action="CashOutRequest" id="cashout" method="post" name="cashout" class="entered-esc">
		


             <input type="hidden" name="inputBean.surChargeAmount" id="surChargeAmount" value="">
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payer Mobile Number<font color="red"> *</font></label> <input
																class="form-control mandatory mobile" name="inputBean.mobileNo" type="text"
																id="mobile" maxlength="11" placeholder="Payer Mobile Number" />

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Amount <font color="red"> *</font></label> <input
																class="form-control mandatory amount-vl" name="inputBean.trxAmount" type="text"
																id="trxAmount" maxlength="20" placeholder="Amount" />

														</div>
													</div>




													<div class="col-md-4">
										<div class="form-group">
										<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										<!-- <button type="submit" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button> -->
										<button type="button"  onclick="submitCashInForm()" class="btn submit-form btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button>
										 <button type="reset"  onclick="resetForm('#cashout')"  style="margin: 16px 19px 0 13px;
    										position: relative;
   										 top: 1px;" class="btn btn-info btn-fill" value="Reset">Reset</button>
   										 	<div class="popover confirmation left top in" id="confirmation">
   										 
               <h3 class="popover-title">Are you sure?</h3><div class="popover-content">
               <p class="confirmation-content" style="display: none;"></p><div class="confirmation-buttons text-center">
              You will be charge Rs. <span id="surcharge"> </span> as surcharge fee on your Rs. <span id="cash-in-amount"></span> amount and total
              total amount is Rs. <span id="total-cash-in"></span>.
              <br/>
               <div class="btn-group">
               <a href="#" class="btn btn-xs btn-primary"  onclick="submitVaForm('#cashout')" "><i class="glyphicon glyphicon-ok"></i> Yes</a>
               <a href="#" class="btn btn-xs btn-default" onclick="closePopup('#confirmation')"><i class="glyphicon glyphicon-remove"></i> No</a></div></div></div></div>
									
									</div>
									</div>
									</div>
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


