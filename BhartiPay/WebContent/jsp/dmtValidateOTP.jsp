
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->


<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>OTP Validation</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<!-- <form action="PaymentGateway.action" id="form_name" method="post" name="PG"> -->
		<form action="DmtOTPValidation" id="form_name" method="post" name="PG">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
									
									
											 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Mobile number </label> 
															<input class="form-control" name="dmtInputBean.mobile" type="text"
																id="amount" maxlength="20" placeholder="Please enter Amount"
																value="<s:property value='%{dmtInputBean.mobile}'/>" readonly="readonly"/>

														</div>
													</div>
													
													

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Request number</label> 
															
															<input class="form-control" name="dmtInputBean.requestNo" type="text"
																id="requestnumber" maxlength="20" placeholder="Request number"
																value="<s:property value='%{dmtInputBean.requestNo}'/>" readonly="readonly" />

														</div>
													</div>
													
													
													

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Please enter OTP</label> 
															<input class="form-control" name="dmtInputBean.otp" type="text"
																id="otp" maxlength="20" placeholder="Please enter OTP" required="required"/>

														</div>
													</div>
													


									</div>
             					 <div class="col-md-12">




													<div class="col-md-4">
														<div class="form-group">

															<button type="submit" class="btn btn-info btn-fill"
																style="margin-top: 20px;">Submit</button>


														</div>
													</div>
</form>

													<div class="col-md-4">
														<div class="form-group">
<form action="DmtResendOTP" method="post">
<input class="form-control" name="dmtInputBean.mobile" type="hidden"
																id="amount" maxlength="20" placeholder="Please enter Amount"
																value="<s:property value='%{dmtInputBean.mobile}'/>" />
															<input class="form-control" name="dmtInputBean.requestNo" type="hidden"
																id="reqnumber" maxlength="20" placeholder="Request number"
																value="<s:property value='%{dmtInputBean.requestNo}'/>" />
															<button type="submit" class="btn btn-info btn-fill"
																style="margin-top: 20px;">Resend OTP</button>
</form>

														</div>
													</div>
												</div>
									</div>
									
									

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


