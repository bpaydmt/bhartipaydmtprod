<%@page import="com.bhartipay.wallet.report.bean.RefundTransactionBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean"%>
<%@page import="com.bhartipay.customerCare.vo.AgentCustDetails"%>
<%@page import="com.bhartipay.customerCare.vo.CustomerCareResponse"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 

<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        

            

 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>DMT Refund/Status</h2>
		</div>
		
		<div class="box-content  ">
		
			<!-- <form action="InitiateDMTRefund" method="post"> -->
			<form action="CheckDMTStatus" id="dmtForm" method="post">
 <div class="row"> 
				<font color="red"><s:actionerror/> </font>
								<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  -->
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{dmtMast.txnid}'/>"
										name="dmtMast.txnid" id="userid"
										class="form-control " placeholder="Txnid"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-5 col-sm-3 txtnew col-xs-6">
								<%
								String status=(String)request.getAttribute("status");
								if(status!=null){
								out.print(status);
								}
								%>
								 </div>
								<div
									class="form-group col-md-12 txtnew col-sm-12 col-xs-12 text-left 	">
									<!--  -->
									<div  id="wwctrl_submit">
										<input class="btn btn-info" onclick="refundStatus('refund',event)"
											type="submit" value="Refund">
											<input class="btn btn-info" onclick="refundStatus('markfail',event)"
											type="submit" value="Mark Failed">
												<input class="btn btn-info" onclick="refundStatus('status',event)" 
											type="submit" value="Status">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
				<div class="col-md-12">
					<%
					//MudraMoneyTransactionBean 
					List<RefundTransactionBean> list=(List<RefundTransactionBean>)request.getAttribute("list");
					if(list!=null&&list.size()>0){
					%>
	<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Id</th>
        <th>Status</th>
        <th>Txndate</th>
        <th>Remark</th>
        <th>Amount</th>
        <th>Agentorsenderid</th>
        <th>Refundto</th>
        <th>Servicecharge</th>
        <th>Refundid</th>
        
     
      </tr>
    </thead>
    <tbody>
   <%for(RefundTransactionBean mbean:list){ 
   %>
      <tr>
        <td><%=mbean.getId()==null ? "":mbean.getId() %></td>
        <td><%=mbean.getStatus()==null ? "":mbean.getStatus()%></td>
        <td><%=mbean.getTxndate()==null ? "":mbean.getTxndate() %></td>
        <td><%=mbean.getRemark()==null ? "":mbean.getRemark()%></td>
        <td><%=mbean.getAmount()==0 ? "":mbean.getAmount()%></td>
        <td><%=mbean.getAgentorsenderid()==null ? "":mbean.getAgentorsenderid() %></td>
        <td><%=mbean.getRefundtosenderoragent()==null ? "":mbean.getRefundtosenderoragent()%></td>
        <td><%=mbean.getServicecharge()==0 ? "":mbean.getServicecharge()%></td>
        <td><%=mbean.getRefundid()==null ? "":mbean.getRefundid()%></td>
      
      </tr>
     <% }%> 
      
    </tbody>
  </table>
					
					
					<%	
					}
					%>		
		
		</div>
		</div>

	
							</div>
	</div>	

					
 

 


 


  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->


<script>

function refundStatus(flag,e){
	
	e.preventDefault();
	var f = $("#dmtForm")

	if(flag == "refund"){
		
		var r = confirm("Do you want to refund this transaction.");
		if (r == true) {
			f.attr("action", "InitiateDMTRefund")
		   f.submit()
		} 
		
		
	}else if(flag == "markfail"){
		var r = confirm("Do you want to Mark Failed this transaction.");
		if (r == true) {
			f.attr("action", "MarkFailedDmtTxn")
		   f.submit()
		} 
	
	}else{
		f.attr("action", "CheckDMTStatus")
		f.submit()
	}
	
	
	
}

</script>

<jsp:include page="footer.jsp"></jsp:include>

<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>


</body>
</html>


