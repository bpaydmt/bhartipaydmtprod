<%@page import="com.bhartipay.wallet.report.bean.TxnReportByAdmin"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.List"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>
<script src='./js/highcharts.js'></script>

<style>
.container {
	width: 100%;
}
</style>
<script type="text/javascript">
        
      function createGraph(id,date,credit,debit){
        var cr = parseInt(credit)
        var db = parseInt(debit)
    Highcharts.setOptions({ colors: ['#19aede', '#87c440']});
    $("#" + id).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Transaction Report'
        },
         subtitle: {
            text: id
        },       
        xAxis: {
            categories: [
                date,
                //obj.txndate,
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },

        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Credit Amount',
            data: [cr]

        }, {
            name: 'Debit Amount',
            data: [db]

        }]
    });
        }
    </script>

</head>
<% 
User user = (User) session.getAttribute("User");
List<TxnReportByAdmin> list=(List<TxnReportByAdmin>)request.getAttribute("txnList");

%>

<body>

	<!-- topbar starts -->

	<!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- dashboard starts -->
			<%
       // User user = (User) session.getAttribute("User");
        if(user.getUsertype()==99){
        %>

			<script src='./js/highcharts.js'></script>
			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->


				<div class=" row">
					<div class="row" id="hidethis2">

						<!--- Copy div from Here. Html edited By Arpan -->
						<div class="box2 col-md-12">

							<div class="box-header well">
								<h2>
									<i class="glyphicon "></i>Dashboard
								</h2>


							</div>
							<div class="box-content row">
								<div class="col-lg-12 col-md-12">
									<div class="col-lg-12 col-md-12"></div>
									<!--  <div id="container" style="min-width: 310px; height: 400px; padding: 0 0 45px 0;margin: 0 auto"></div> -->

								</div>



							</div>


						</div>

						<form>


							<%
	for(TxnReportByAdmin wtb:list){
	%>
							<div id="<%=wtb.getAggreatorid()%>"
								style="min-width: 310px; min-height: 400px; padding: 0 0 45px 0; margin: 0 auto"></div>
							<script>
  $(document).ready(function(){
	createGraph("<%=wtb.getAggreatorid()%>","<%=wtb.getTxndate()%>","<%=wtb.getTxncredit()%>","<%=wtb.getTxndebit()%>"); 
  });
  	</script>
							<%
                  }
				%>





						</form>

						<!-- contents ends -->
					</div>
				</div>
				<!--/.fluid-container-->
			</div>






			<%
        }else{
        %>
			<jsp:include page="dashBoard.jsp"></jsp:include>
			<%} %>
			<!-- dashboard ends -->



		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


</body>
</html>
