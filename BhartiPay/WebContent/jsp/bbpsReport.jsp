<%@page import="com.google.gson.Gson"%>
<%@page import="com.bhartipay.bbps.vo.BbpsPayment"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
 <script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
 
<%-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> > --%>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >  
 

<style>
	  
  .popup-overlay-other1{ 
    visibility:hidden;
    position:fixed; 
    width:75%;
    height:75%;
    left:25%;
    top: 38%; 

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content-other1 { 
   	visibility:hidden;
  }

  .popup-content-other1.active { 
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
	  width: 50%;
	  height: auto;
	  left: 25%;  
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px; 
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 10%;
      width: 60%;
  }
  .close-other1 {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  
  
.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important; 
        margin-bottom: 15px;
        color: #111 !important;
}
</style>

<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>

<script type="text/javascript"> 
    $(document).ready(function() {
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
   }); 
</script>


 
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'bbps - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'bbps - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'bbps - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'bbps Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'bbps - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>

 <style>
.paddinBoxContainer{
    padding: 20px 20px;

} 

</style>
</head>

<% 
	User user = (User) session.getAttribute("User");
	List<BbpsPayment>list=(List<BbpsPayment>)request.getAttribute("resultList"); 
%>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        

<div id="main" role="main">

	<div id="content">       
	    <div class="row">    
			<div class="col-lg-12 col-md-12 col-sm-12">  
				<!-- <div class="row"  id="hidethis2"> -->
					<div class="box2">
					
					<!--Creates the popup body-->
						      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
						        <div class="popup-content-other1"> 
						         <div class='box-login-title-other1'> 
						           <div class="MessageBoxMiddle-other1">  
						            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Do you want to print?</span>
							            <div style="text-align:center;margin-bottom: 10px;">
							            	<div id="popupInfoDetails" style="width:140%;"> </div>
								           <button type="button" class="btn btn-success" id="payPopUpYes" onclick="printNow();">Yes</button> 
								           <button type="button" class="close-other1 btn btn-primary" onclick="payPopUpNo();" style="padding: 6px 12px;">No</button>
							            </div> 
						          </div>
						         </div>   
						        </div>
						      </div> 
						   <!-- Popup End -->
					
					
						<div class="box-inner"> 
							<div class="box-header">
								<h2>BBPS</h2>
							</div> 
							<div class="box-content "> 
								<form action="BBPSReport" method="post"> 
								    <div class="row">  
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bank-txt">  
									    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 
									    
									    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div>	
									    
										<input type="hidden" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="from" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>

										<input type="hidden" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="to" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
									</div>
									
									<div class="col-md-2 col-xs-12"> 
										<div  id="wwctrl_submit">
											<input class="btn btn-success submit-form" id="submit" type="submit" value="Submit">
											<input class="btn btn-primary btn-fill" id="reset" type="reset" value="Reset">
										</div>
									</div>
									
									<% 
								        int txnCount= 0;
										double txnAmount=0; 
										for(int i=0; i<list.size(); i++) {
									    BbpsPayment tdo=list.get(i); 
									    if(tdo.getStatus() !=  null && tdo.getStatus().equalsIgnoreCase("Successful")){
										txnAmount=txnAmount+tdo.getTxnamount();
										txnCount=txnCount + 1; 	
											} 
										} 
								    %>
				    
									<div class="col-md-2 col-xs-12" style="padding:10px 0px;"> 
										<strong>Successful Count -  <span><%=d.format(txnCount)%></span></strong>
									</div>
									
									<div class="col-md-4 col-xs-12" style="padding:10px 0px;"> 
										<strong>Successful Amount -  <span><%=d.format(txnAmount)%></span></strong>
									</div>
									
								</div>
								</form> 
							</div>
						</div>
					</div>	
						
					

					<%-- <div id="xyz" class="box2">
						<table id="examplea" class="scrollD cell-border dataTable table table-bordered table-striped" width="100%">
							<thead> 
								<tr>
									<th style="text-align: center;">Total Successful Txn Count</th> 
									<th style="text-align: center;"><u>Total Successful Txn Amount</th>  
								</tr>
							</thead>
							<tbody> 
					            <tr>
								    <td style="text-align: center;"><%=d.format(txnCount)%>
								    </td>
					          	    <td style="text-align: center;"><%=d.format(txnAmount)%></td>  
				              	</tr> 
						    </tbody>		
					    </table>
					</div> --%>

										
					<div id="xyz">
						<table id="example" class="scrollD cell-border dataTable" width="100%">
							<thead>  
								<tr>
<!-- <th><u>Txn Id</u></th>      
<th><u>Resp Txn Id</u></th> 
<th><u>walletid</u></th> 
<th><u>agentid</u></th>   
<th><u>aggreatorid</u></th>
<th><u>txndate</u></th>
<th><u>txnamount</u></th> 
<th><u>requestid</u></th> 
<th><u>request</u></th>  
<th><u>requestdate</u></th>
<th><u>response</u></th>
<th><u>responsedate</u></th>
<th><u>status</u></th> 
<th><u>billertype</u></th>
<th><u>billername</u></th>  
<th><u>billerid</u></th>    
<th><u>quickpaytype</u></th> -->
						<th style="width: 1%;"><u>Sr.No</u></th>
						<th><u>Txn Date</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Consumer Number</u></th>
						<th><u>Biller Type</u></th>
						<th><u>Biller Name</u></th> 
						<th><u>Biller Id</u></th>
						<th><u>Resp Txn Id</u></th>
						<th style="width: 2%;"><u>Amount</u></th> 
						<th><u>Status</u></th>    

										 	
								</tr>
							</thead>
							<tbody>
								<%
								int count=1;
								for(BbpsPayment wtb:list){
								%>
							  	<tr>
						           	<th><%= count++ %></th>
						            <th><%=wtb.getTxndate() %></th>
						            <%-- <%if(wtb.getStatus().toUpperCase().contains("INSUFFICIENT")){%> --%>
						            <%if(wtb.getStatus() !=  null && wtb.getStatus().equalsIgnoreCase("Successful")){
						            	Gson gson = new Gson();
						            	String userJson = gson.toJson(user);
						            	String bbpsPaymentJson = gson.toJson(wtb);
						            %>
								  		<th><a href='javascript:;' onclick='getTxnDetails(<%=userJson%>,<%=bbpsPaymentJson%>);'><%=wtb.getTxnid()!=null? wtb.getTxnid():"-" %></a></th>
									<%}else {%>  
								  		<th><%=wtb.getTxnid()!=null? wtb.getTxnid():"-" %></th>
									<%}%>
									<th><%=wtb.getConsumerNumber()!=null? wtb.getConsumerNumber():"-" %></th>
								  	<th><%=wtb.getBillertype()!=null?wtb.getBillertype():"-" %></th>
									<th><%=wtb.getBillername()!=null?wtb.getBillername():"-"  %></th>  
									<th><%=wtb.getBillerid()!=null?wtb.getBillerid():"-"  %></th>
						            <th><%=wtb.getResptxnid()!=null? wtb.getResptxnid():"-" %></th>      
									<th><%=wtb.getTxnamount() %></th>
									
									<%if(wtb.getStatus().toUpperCase().contains("INSUFFICIENT")){%>
										<th>Failed</th>
									<%}else {%>  
										<th><%=wtb.getStatus() %></th>
									<%}%>
									
				              	</tr>
				                <%
				                }
							    %>
						    </tbody>		
						</table>
					</div>

				<!-- </div> --> 
			</div>
        </div>
	</div> 
</div>
 
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<script>
    function getTxnDetails(userAgent, bbpsTxn){


    	var xmlRequestDoc = parseXml(bbpsTxn.request);
    	var xmlResponseDoc = parseXml(bbpsTxn.response);

		var xmlRequestChildrens = xmlRequestDoc.documentElement.children;
		var xmlResponseChildrens = xmlResponseDoc.documentElement.children;
		var i;

		var printCustomerNumber = '';
		var printCustomerName =''; 
		var printBillDate = '';
		var printDueDate = '';
		var printBillNumber = '';

		for (i = 0; i < xmlRequestChildrens.length; i++) {
		  if(xmlRequestChildrens[i].tagName == 'customerInfo'){
			  var customerInfoChildrens = xmlRequestChildrens[i].children;
			  for (i = 0; i < customerInfoChildrens.length; i++) {
				  if(customerInfoChildrens[i].tagName == 'customerMobile')
				  	printCustomerNumber = customerInfoChildrens[i].textContent;
			  }
		  }
		}
		
		for (i = 0; i < xmlResponseChildrens.length; i++) {
		  if(xmlResponseChildrens[i].tagName == 'RespCustomerName')
			  printCustomerName = xmlResponseChildrens[i].textContent;
		  if(xmlResponseChildrens[i].tagName == 'RespBillDate')
			  printBillDate = xmlResponseChildrens[i].textContent;
		  if(xmlResponseChildrens[i].tagName == 'RespDueDate')
			  printDueDate = xmlResponseChildrens[i].textContent;
		  if(xmlResponseChildrens[i].tagName == 'RespBillNumber')
			  printBillNumber = xmlResponseChildrens[i].textContent;
		}
		
    	var result = '';
		result = result + '<div class="box box-primary" id="billPrintArea" style="width:100%;">';
		result = result + '<input id="printRefid" name="printRefid" type="hidden" />';
		result = result + '<input id="billerPrintResponseTags" name="billerPrintResponseTags" type="hidden" />';
		result = result + '<!-- BOX TITLE or SECTION TITLE START -->';
		result = result + '<div class="box-header with-border" id="billHeader" style="height: 70px; display:none;">';
		result = result + '	<table id="printPaymentHeader" width="100%">';
		result = result + '		<tr>';
		result = result + '			<td width="33%" style="text-align:left;"><img id="merchantLogo" style="width:20%;"></td>';
		result = result + '			<td width="33%" style="text-align:center;"><h3 class="box-title no-margin">Bill Receipt</h3></td>';
		result = result + '			<td width="33%" style="text-align:right;"><img id="bbpsLogo" src="./image/BBPSLOGO.png" style="width:10%;"></td>';
		result = result + '		</tr>';
		result = result + '	</table>';
		result = result + '</div>';
		result = result + '<!-- BOX TITLE or SECTION TITLE END -->';
		result = result + '<!-- BOX BODY START -->';
		result = result + '<div class="box-body">';
		result = result + '   <div class="row123">';
		result = result + '   <table id="printPayment" width="100%" style="border: 1px solid black;border-collapse: collapse;" >';
		result = result + '	<tr id="printAgentTR" style="display:none;">';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Shop Name</td>';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printShopName">'+userAgent.shopName+'</span></td>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Agent Mobile</td>   ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printAgentMobile">'+userAgent.mobileno+'</span></td>';
		result = result + '	</tr>';
		result = result + '	<tr>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Customer Mobile</td>';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerNumber">'+printCustomerNumber+'</span></td>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Customer Name</td>   ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerName">'+printCustomerName+'</span></td>';
		result = result + '	</tr>';
		result = result + '   <tr>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Biller Name</td>    ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillerName">'+bbpsTxn.billername+'</span></td>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Biller ID</td>   ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;border: 1px solid black;"><span id="printBillerID">'+bbpsTxn.billerid+'</span></td>';
		result = result + '	</tr>';
		result = result + '   <tr>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Bill Date</td>    ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillDate">'+printBillDate+'</span></td>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Due Date</td>   ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printDueDate">'+printDueDate+'</span></td>';
		result = result + '	</tr>';
		result = result + '   <tr>';
		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Bill Number</td>    ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillNumber">'+printBillNumber+'</span></td>';

		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Consumer Number</td>    ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printConsumerNumber">'+bbpsTxn.consumerNumber+'</span></td>';

		
/*		result = result + '		<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Charges/Commission</td>   ';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;"><span id="">As Applicable</span></td>';
*/

		result = result + '	</tr>';

		result = result + '	</table><br>';
		result = result + '	<table id="printPayment2" width="100%" style="border: 1px solid black;border-collapse: collapse;" >';
		result = result + '		<tr style="font-weight: bold;">';
		result = result + '			<td width="15%" style="text-align:left;border: 1px solid black;">Order ID</td>';
		result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;">Transaction ID</td>';
		result = result + '			<td width="18%" style="text-align:left;border: 1px solid black;">Bill Amount</td>';
		result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;">Date & Time</td>';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;">Transaction Status</td>';
		result = result + '		</tr>';
		result = result + '		<tr>';
		result = result + '			<td width="15%" style="text-align:left;border: 1px solid black;"><span id="printOrderID">'+bbpsTxn.resptxnid+'</span></td>';
		result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTransactionID">'+bbpsTxn.txnid+'</span></td>';
		result = result + '			<td width="18%" style="text-align:left;border: 1px solid black;"><span id="printBillAmount">'+bbpsTxn.txnamount+'</span></td>';
		result = result + '			<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTxnDateTime">'+bbpsTxn.txndate+'</span></td>';
		result = result + '			<td width="30%" style="text-align:left;border: 1px solid black;color:green;"><span id="printTxnStatus">'+bbpsTxn.status+'</span></td>';
		result = result + '		</tr>';
		result = result + '	</table>';
		result = result + '	<table id="printPoweredBy" style="display:none;">';
		result = result + '		<tr>';
		result = result + '			<td width="30%">';
		result = result + '				<img src="./image/BharatAssured.png" style="width:35%;">';
		result = result + '			</td>    ';
		result = result + '			<td width="70%" style="text-align:right; font-style: italic;"><!-- Powered by Bhartipay Services Pvt. Ltd. --></td>';
		result = result + '		</tr>';
		result = result + '	</table>';
		/* result = result + '	<table id="removePrint" class="scrollD cell-border no-footer" width="100%" style="border-top: 1px solid #fafafa;" >';
		result = result + '		<tr>';
		result = result + '			<td width="25%"></td>';
		result = result + '			<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-right" id="btn_bill_print" onclick="printNow();">Print</button></td>';
		result = result + '			<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-left" id="btn_cancel_print" onclick="cancelNow();">Cancel</button></td>';
		result = result + '			<td width="25%"></td>';
		result = result + '		</tr>';
		result = result + '	</table>'; */
		result = result + '	</div>';
		result = result + '	</div>';
		result = result + '	<!-- BOX BODY END -->';
		result = result + '</div>';

    	$(".popup-overlay-other1, .popup-content-other1").addClass("active");
		$("#popupInfoDetails").empty().append(result);
    }

    function printNow()
    {
    	var originalContents = document.body.innerHTML;
    	var divToPrint=document.getElementById("billPrintArea");
    	
    	$('#bbpsLogo').attr('src','./image/BBPSLOGO.png');
    	
    	$("#printPoweredBy").show();
    	$("#billHeader").show();
    	$("#printAgentTR").show();
    	
    	/* $("#billPrintArea").find('#removePrint').remove(); */
    	newWin= window.open("");
    	newWin.document.write(divToPrint.outerHTML);
    	newWin.print();
    	newWin.close();
    	document.body.innerHTML = originalContents;
    }


    function payPopUpNo(){
    	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
    }
</script>


<script type="text/javascript">
var parseXml;

if (window.DOMParser) {
    parseXml = function(xmlStr) {
        return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
    };
} else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
    parseXml = function(xmlStr) {
        var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlStr);
        return xmlDoc;
    };
} else {
    parseXml = function() { return null; }
}

</script>
</body>
</html>


