<%@page import="java.util.Map"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="js/validateWallet.js"></script>
<div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
	<a href="javascript:;" id="close_login_popup" class="pull-right"><i
		class="fa fa-close"  style="border:1px solid #131313;padding:5px 7px;border-radius:50%;background-color:#131313;color:#FFF;"></i></a>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="login_box">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h1 class="payplutus_h1log">Login to your Account</h1>
	</div>
	<form method="post" name="login" id="login" action="Login">
	<s:hidden name="csrfPreventionSalt"	value="%{#session.csrfPreventionSalt}"></s:hidden>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="text" name="user.userId" id="username"
				class="payplutus_textbox_login" autocomplete="off" required
				placeholder="Email/Mobile number">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="password"   name="user.password" id="password"
				class="payplutus_textbox_login" autocomplete="off" required
				placeholder="Password">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<p class="para">
				<a href="javascript:;" id="forgot_link">Forgot Password?</a>
			</p>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="submit" id="submit" name="submit" value="Log In"
				class="payplutus_log_btn">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="checkbox" name="remember" id="remember" value="remember"><label
				class="para">Remember Me</label>
		</div>
		<div class="clear_2"></div>
	</form>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<a href="javascript:;" id="fb" class="facebook_btn"><i
			class="fa fa-facebook"></i>Log in with Facebook</a>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<a href="javascript:;" id="fb" class="google_btn"><i
			class="fa fa-google-plus"></i>Log in with Google</a>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<p class="login_footer">
			New to Digital Wallet?<a href="javascript:;" id="register_user"
				class="login_link">Sign up now</a>
		</p>
	</div>
</div>
<!-- Registration Box -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
	id="registration_box">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h1 class="payplutus_h1log">Sign up for Digital Wallet Account</h1>
	</div>
	<form method="post" name="register" id="register"
		action="SaveSignUpUser1">			                        
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
		<input type="hidden" name="user.usertype" value="1">
			<input type="text" name="user.name" id="name"
				class="payplutus_textbox_login mandatory userName" autocomplete="off" 
				placeholder="Your Name" value="<s:property value='%{user.name}'/>">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="email" name="user.emailid" id="email"
				class="payplutus_textbox_login emailid mandatory" autocomplete="off" 
				placeholder="Your Email" value="<s:property value='%{user.emailid}'/>">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="text" name="user.mobileno" id="mobile"
				class="payplutus_textbox_login" autocomplete="off"
				placeholder="Your Mobile No."  value="<s:property value='%{user.mobileno}'/>">
		</div>
<%
Map<String,String> mapResult=(Map<String,String>)request.getSession().getAttribute("mapResult");
if(mapResult!=null&&mapResult.get("emailvalid")!=null&&Double.parseDouble(String.valueOf(mapResult.get("emailvalid")))==0)
{
%>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="password"   name="user.password" id="passw"
				class="payplutus_textbox_login mandatory password-vl" autocomplete="off" 
				placeholder="Password">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="password"   name="user.confirmPassword" id="passw"
				class="payplutus_textbox_login mandatory confirm-vl" autocomplete="off" 
				placeholder="Confirm Password">
		</div>
<%
}
%>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="button" id="signup" name="signup" onclick="submitVaForm('#register')" value="Sign Up"
				class="payplutus_log_btn">
		</div>
	</form>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
		style="margin-top: 25%;">
		<p class="login_footer">
			Already Registered?<a href="javascript:;" id="login_user"
				class="login_link">Login here</a>
		</p>
	</div>
</div>
<!-- FORGOT PASSWORD -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="forgot_box">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h1 class="payplutus_h1log">Forget Your Password?</h1>
	</div>
	<form method="post" name="login" id="login"
		action="#">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="email" name="email" id="emailid"
				class="payplutus_textbox_login" autocomplete="off" required
				placeholder="Enter Your Email Id">
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_mt5">
			<input type="submit" id="submit" name="submit" value="Submit"
				class="payplutus_log_btn">
		</div>
	</form>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
		style="margin-top: 55%;">
		<p class="login_footer">
			Already registered?<a href="javascript:;" id="login_user_2"
				class="login_link">Login Here</a>
		</p>
	</div>
</div>