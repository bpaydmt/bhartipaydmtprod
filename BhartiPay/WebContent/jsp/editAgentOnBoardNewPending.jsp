<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript">
 $(function(){
	$("#dob, #paymentDate").datepicker({
	    	 language: 'en',
	    	 maxDate: new Date(),
	    	
	    });
		

});

function validatePancardApi(elm){

	//var reg ="";
	var inp = $(elm).val();
	//sssssssalert(inp);
	if(inp.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){
		//alert(5555);
		$.ajax({
			method:'Post',
			url:'ValidatePan',
			data:"pan="+inp+"&userType=2",
			success:function(result){
				console.log(result.status)
				
				if(result.status == "true"){
					$(elm).addClass("registerPanCard")
					$("#panError").text("This pan card number is already register with another agent");
				}else{
					$("#panError").text("");
				}
			}
		})
	}else{
		
		
	}
	
	
}
function copyAddress(){
	
	if($("#addressCheckbox").prop("checked")){
			$("#shopaddress1").val($("#address1").val())
			$("#shopaddress2").val($("#address2").val())
			$("#shopstate").val($("#state").val())
			$("#shopcity").val($("#city").val())
			$("#shoppin").val($("#pin").val())
	}else{
	
			$("#shopaddress1").val("")
			$("#shopaddress2").val("")
			$("#shopstate").val("-1")
			$("#shopcity").val("")
			$("#shoppin").val("")
	}
	
}




function getDistributorDetail()
{
debugger
	  var dist = $('#usertype').children("option:selected").val();
      if(dist=='-1')
      {
  		alert("Please select Distributor.");
  		return false;
  	  }
    
	  $.ajax({
      		method:'Post',
      		cache:0,  		
      		url:'DistributorDetails',
      		data:"distributorId="+dist,
      		success:function(data){
      		var json = JSON.parse(data);
			var distId=json.agentId;
			var mobileNo=json.mobileNo;
			var name=json.userName;
			document.getElementById("distributorName").value=name;
			document.getElementById("distributorMobileNo").value=mobileNo; 
      	
      		}
      	})
	
}




</script>


</head>
<%
WalletMastBean mast=(WalletMastBean)request.getAttribute("mastBean");


String userType=(String)request.getAttribute("userType");
if(userType!=null&&!userType.equalsIgnoreCase("-1")&&!userType.isEmpty())
%>
<style>
.upperCase {
	text-transform: uppercase;
}
</style>
<body>



	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->



	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<div id="main" role="main">
		<div id="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">


					<%
User user=(User)session.getAttribute("User");
%>



					<div class="box">
						<div class="box-inner">
							<div class="box-header">
								<h2>
									<i class="glyphicon "></i>User On Board
								</h2>

							</div>
							<div class="box-content">

								<form action="EditAgentNewDecPending" id="form_name" enctype="multipart/form-data"  method="post" name="PG">


									<div class="row">
										<font id="err" color="red"><s:actionerror /> </font> <font
											id="suc" color="blue"><s:actionmessage /> </font> <input
											type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">
										<div class="row">
											<div class="col-md-12">

												<div class="col-sm-4">
													<div class="form-group   txtnew ">
														<label for="email">Agent Id<font color='red'>
																*</font></label> <br> <input type="text" name="walletBean.id"
															readonly placeholder="Agent Id"
															class="form-control mandatory" id="id"
															value="<s:property value='%{walletBean.id}'/>">
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group   txtnew ">
														<label for="email">Sales Officer<font color='red'>
																*</font></label> <br> <input type="text" name="walletBean.salesId"
															readonly placeholder="Sales Id"
															class="form-control " id="salesid"
															value="<s:property value='%{walletBean.salesId}'/>">
													</div>
												</div>
											 
											 <%if(mast.getUsertype()==2) { %>
												<div class="col-sm-4">
													<div class="form-group   txtnew ">
														<label for="email">Distributor Id<font color='red'>
																*</font></label> <br> <input type="text"
															name="walletBean.distributerid" readonly
															placeholder="Distributor Id"
															class="form-control" id="distributerid"
															value="<s:property value='%{walletBean.distributerid}'/>">
													</div>
                                                </div>
                                            <%} %>
                                            
                                             <%if(mast.getUsertype()==3) { %>
												<div class="col-sm-4">
													<div class="form-group   txtnew ">
														<label for="email">Super Distributor Id<font color='red'>
																*</font></label> <br> <input type="text"
															name="walletBean.superdistributerid" readonly
															placeholder="Super Distributor Id"
															class="form-control" id="superdistributerid"
															value="<s:property value='%{walletBean.superdistributerid}'/>">
													</div>
                                                </div>
                                            <%} %>
                                            
											</div>

											<input class="form-control"
												name="walletBean.addressProofType" id="addressProofType"
												type="hidden" value="Aadhar" /> <input class="form-control"
												name="walletBean.agentType" id="agentType" type="hidden"
												value="Individual" /> <input class="form-control"
												name="walletBean.createdby" id="createdby" type="hidden"
												value="<s:property value='%{walletBean.createdby}'/>" /> <input
												class="form-control" name="walletBean.agentid"
												id="createdby" type="hidden" value="-1" /> <input
												class="form-control" name="walletBean.subAgentId"
												id="createdby" type="hidden" value="-1" />


											<div class="col-md-12">

												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Name <fRont color='red'>
															*</font></label> <input type="text" name="walletBean.name"
															placeholder="Name"
															class="form-control upperCase  userName mandatory"
															id="name"
															value="<s:property value='%{walletBean.name}'/>">

													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Email <font color='red'>
																*</font></label> <input type="text" name="walletBean.emailid"
															placeholder="Email"
															class=" form-control  upperCase emailid mandatory"
															id="email"
															value="<s:property value='%{walletBean.emailid}'/>">

													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Mobile Number <font
															color='red'> *</font></label> <input type="text"
															name="walletBean.mobileno" placeholder="Mobile Number"
															class=" form-control mobile mandatory" id="mobile"
															value="<s:property value='%{walletBean.mobileno}'/>"
															title="Mobile number must have 10 digits and should start with 7,8 or 9."
															maxlength="10" pattern="[6789][0-9]{9}"
															requiredmessage="invalid mobile number">

													</div>
												</div>
											</div>


											<div class="col-md-12">

												
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">PAN <font color='red'>
																*</font></label> <input type="text" name="walletBean.pan"
															placeholder="PAN"
															class=" form-control mandatory pancard-vl" id="pan"
															value="<s:property value='%{walletBean.pan}'/>"
															onblur="validatePancardApi(this)">

													</div>
												</div>

                                               <div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Date Of Birth <font
															color='red'> *</font></label> <input type="text"
															name="walletBean.dob" placeholder="DD/MM/YYYY"
															class="form-control mandatory datepicker-here1" id="dob"
															value="<s:property value='%{walletBean.dob}'/>">

													</div>
												</div>
 
                                          <div class="col-md-4">
											<div class="form-group">
															<label for="apptxt">Select Gender <font color='red'> *</font></label>
														<input type="text"
															name="walletBean.gender" placeholder="Gender"
															class="form-control mandatory datepicker-here1" id="gender"
															value="<s:property value='%{walletBean.gender}'/>">	
															 
										     </div>
										   </div>
										  
												
											</div>

											 
											<div class="col-md-12">

                                             <div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Aadhar Number <font
															color='red'> *</font></label> <input type="text"
															name="walletBean.adhar" placeholder="Aadhar Number"
															class=" form-control mandatory" id="adhar"
															value="<s:property value='%{walletBean.adhar}'/>">

													</div>
												</div>

												<div class="col-md-8">
													<div class="form-group">
														<label for="apptxt">Address<font color='red'>
																*</font></label> <input type="text" name="walletBean.address1"
															placeholder="Address"
															value="<s:property value='%{walletBean.address1}'/>"
															class=" form-control address-vl mandatory upperCase"
															maxlength="100" id="address1" />

													</div>
												</div>

  
												</div>

 

											<div class="col-md-12">

                                                <div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">City<font color='red'>
																*</font></label> <input type="text" name="walletBean.city"
															placeholder="City"
															class=" form-control userName mandatory upperCase"
															id="city"
															value="<s:property value='%{walletBean.city}'/>" />

													</div>
                                                  </div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">State<font color='red'>
																*</font></label> <input type="text" name="walletBean.state"
															placeholder="State" class=" form-control mandatory"
															id="state"
															value="<s:property value='%{walletBean.state}'/>" />



													</div>
												</div>



												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">PIN Code<font color='red'>
																*</font></label> <input type="text" name="walletBean.pin"
															placeholder="PIN Code" maxlength='6' min='100000'
															class=" form-control pincode mandatory" id="pin"
															value="<s:property value='%{walletBean.pin}'/>" />

													</div>
												</div>
 
											</div>

											<div class="col-md-12">


												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Shop Name<font color='red'>
																*</font></label> <input type="text" name="walletBean.shopName"
															placeholder="Shop Name"
															value="<s:property value='%{walletBean.shopName}'/>"
															class=" form-control address-vl mandatory upperCase"
															maxlength="100" id="shopname" />

													</div>
												</div>
												<div class="col-md-8">
													<div class="form-group">
														<label for="apptxt">Shop Address</label> <input
															type="text" name="walletBean.shopAddress1"
															placeholder="Shop Address"
															value="<s:property value='%{walletBean.shopAddress1}'/>"
															class=" form-control address-vl upperCase"
															maxlength="100" id="shopaddress1" />

													</div>
												</div>

 
											</div>
											
											<div class="col-md-12">
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Shop City</label> <input type="text"
															name="walletBean.shopCity" placeholder="Shop City"
															class=" form-control userName upperCase" id="shopcity"
															value="<s:property value='%{walletBean.shopCity}'/>" />

													</div>
												</div>

 
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Shop State</label> <input type="text"
															name="walletBean.shopState" placeholder="Shop State"
															class=" form-control mandatory" id="shopState"
															value="<s:property value='%{walletBean.shopState}'/>" />


													</div>
												</div>



												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Shop PIN Code</label> <input
															type="text" name="walletBean.shopPin"
															placeholder="Shop PIN Code" maxlength='6' min='100000'
															class=" form-control pincode" id="shoppin"
															value="<s:property value='%{walletBean.shopPin}'/>" />

													</div>
												</div>
											</div>



											<div class="form-group  col-md-12  txtnew  col-xs-6">
												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Bank Name</label> <input type="text"
															name="walletBean.bankName" placeholder="Bank Name"
															class=" form-control userName upperCase" id="bankName"
															value="<s:property value='%{walletBean.bankName}'/>" />

													</div>
												</div>


												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">Account Number</label> <input
															type="text" name="walletBean.accountNumber"
															placeholder="Account Number"
															class=" form-control number upperCase" id="accountNumber"
															value="<s:property value='%{walletBean.accountNumber}'/>" />

													</div>
												</div>


												<div class="col-md-4">
													<div class="form-group">
														<label for="apptxt">IFSC Code</label> <input type="text"
															name="walletBean.ifscCode" placeholder="IFSC Code"
															class=" form-control  upperCase" id="ifscCode"
															value="<s:property value='%{walletBean.ifscCode}'/>" />

													</div>
												</div>


											</div>
											
											
										<div class="col-md-12">
											 
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Proof of Identity (PAN)<font color='red'>
															*</font></label>
													<a href="<s:property value='%{walletBean.idLoc}'/>" download>ID Proof</a> 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile1}'/>"
														name="walletBean.file1" id="dp" required="true"
														placeholder="Select Address Proof" required
														readonly="readonly" required>

												</div>
												
												
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Proof of Address<font color='red'>
															*</font></label>
													<a href="<s:property value='%{walletBean.addressLoc}'/>" download>Address Proof</a> 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile2}'/>"
														name="walletBean.file2" id="dp"
														placeholder="Select Id Proof" required readonly="readonly">
												</div>
												

											 

                                        </div>


                                       <div class="col-md-12">
											 
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Bank Account Details</label>
													<a href="<s:property value='%{walletBean.passbookLoc}'/>" download>Passbook</a>	 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile3}'/>"
														name="walletBean.file3" id="dp" required="true"
														placeholder="Select Address Proof" required
														readonly="readonly" required>

												</div>
												
												
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Payment Details</label>
													<a href="<s:property value='%{walletBean.paymentLoc}'/>" download>Payment</a>	 	 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile4}'/>"
														name="walletBean.file4" id="dp"
														placeholder="Select Id Proof" required readonly="readonly">
												</div>
												
                                        </div>
											
											
											


											<div class="col-md-12">
												<div class="col-md-4">
													<div class="form-group">

														<input type="button" class="btn btn-success  submit-form"
															style="margin-top: 20px;" value="Submit"
															onclick="submitVaForm('#form_name')" />



														<button type="reset" onclick="resetForm('#form_name')"
															class="btn reset-form btn-info btn-fill"
															style="margin-top: 20px;">Reset</button>


													</div>
												</div>

											</div>

										</div>
										</br> </br>


									</div>

								</form>



							</div>



						</div>
					</div>












				</div>
			</div>
		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



