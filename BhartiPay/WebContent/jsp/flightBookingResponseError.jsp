


<jsp:include page="theams.jsp"></jsp:include>

<%
String logo=(String)session.getAttribute("logo");

%>

<%@taglib prefix="s" uri="/struts-tags"%>

<head>
  <jsp:include page="reqFiles.jsp"></jsp:include>
  	<link href='./css/jquery.datetimepicker.min.css' rel='stylesheet'> 
  	  	<link href='./css/chosen.css' rel='stylesheet'> 
  
  	 	<script src="./js/flight.js"></script>
 

</head>
   

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            		
            		

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		
	
		<div class="box-content row">
		<div class="box-header well">
                <h2><i class="glyphicon "></i>Flight Booking</h2>

                
            </div>
	<div id="fliight-wrapper" class="returnFlightTrue">

<div id="booking-details">
<div class="row">
<div class="tickets-block">

<div class="block-head">
<span class="step-count">01</span>
Ticket Details
</div>

<div class="details-block">
		
		

<h2>
<span id="origin0">Delhi</span> 
<i class="fa fa-plane" style="margin: 0 10px;"></i> 
 <span id="destina0">Mumbai</span> 
<span class="f-date"></span>
</h2>

		<div class="ticket-block">

				<div class="ticket-single">
					<div class="col-md-3">
						<img src="./images/flights/SG.png" class="f-img" alt="SpiceJet">
					<div class="airline-name">SpiceJet</div>	
<div class="airline-code">SG-161</div>
					</div>
				<div class="col-md-3">
					<div class="time">Fri, 27 Apr 2018 10:20</div>
					<div class="airport-code">DEL</div>
				<div class="airport-name">Indira Gandhi Airport</div>
				</div>
	<div class="col-md-3">
					---
				</div>
	<div class="col-md-3">
					<div class="time">Fri, 27 Apr 2018 12:40</div>
					<div class="airport-code">BOM</div>
				<div class="airport-name">Mumbai</div>
				</div>
				</div>

		</div>

<h2>
<span id="origin1">Mumbai</span> 
<i class="fa fa-plane" style="margin: 0 10px;"></i> 
 <span id="destina1">Delhi</span> 
<span class="f-date"></span>
</h2>

		<div class="ticket-block">

				<div class="ticket-single">
					<div class="col-md-3">
						<img src="./images/flights/SG.png" class="f-img" alt="SpiceJet">
					<div class="airline-name">SpiceJet</div>	
<div class="airline-code">SG-158</div>
					</div>
				<div class="col-md-3">
					<div class="time">Fri, 27 Apr 2018 13:15</div>
					<div class="airport-code">BOM</div>
				<div class="airport-name">Mumbai</div>
				</div>
	<div class="col-md-3">
					---
				</div>
	<div class="col-md-3">
					<div class="time">Fri, 27 Apr 2018 15:25</div>
					<div class="airport-code">DEL</div>
				<div class="airport-name">Indira Gandhi Airport</div>
				</div>
				</div>

		</div>

</div>



<div class="block-head">
<span class="step-count">02</span>
Traveller Details
</div>
<div class="details-block" id="Travallers-details">
<div id="flight-err"></div>

<form action="bookFlight" method="post" id="ticketingForm">
	
	
<input type="hidden" id="bookingadultCount" name="pass.adultCount" value="1">
<input type="hidden" id="bookingchildCount" name="pass.childCount" value="1">
<input type="hidden" id="bookingInfantCount" name="pass.infantCount" value="1">
<input type="hidden" id="bookingFare" name="pass.fare" value="{&quot;TransactionFee&quot;:0,&quot;AirTransFee&quot;:0,&quot;AdditionalTxnFee&quot;:0,&quot;Currency&quot;:&quot;INR&quot;,&quot;BaseFare&quot;:5800,&quot;Tax&quot;:1256,&quot;TaxBreakup&quot;:[{&quot;key&quot;:&quot;K3&quot;,&quot;value&quot;:324},{&quot;key&quot;:&quot;YQTax&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;YR&quot;,&quot;value&quot;:100},{&quot;key&quot;:&quot;PSF&quot;,&quot;value&quot;:308},{&quot;key&quot;:&quot;UDF&quot;,&quot;value&quot;:24},{&quot;key&quot;:&quot;INTax&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;TransactionFee&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;OtherTaxes&quot;,&quot;value&quot;:400}],&quot;YQTax&quot;:0,&quot;AdditionalTxnFeeOfrd&quot;:0,&quot;AdditionalTxnFeePub&quot;:0,&quot;PGCharge&quot;:0,&quot;OtherCharges&quot;:3.54,&quot;ChargeBU&quot;:[{&quot;key&quot;:&quot;TBOMARKUP&quot;,&quot;value&quot;:&quot;0&quot;},{&quot;key&quot;:&quot;CONVENIENCECHARGE&quot;,&quot;value&quot;:&quot;0&quot;},{&quot;key&quot;:&quot;OTHERCHARGE&quot;,&quot;value&quot;:&quot;3.54&quot;}],&quot;Discount&quot;:0,&quot;PublishedFare&quot;:7059.54,&quot;CommissionEarned&quot;:96.19,&quot;PLBEarned&quot;:61.91,&quot;IncentiveEarned&quot;:62.78,&quot;OfferedFare&quot;:6838.66,&quot;TdsOnCommission&quot;:28.86,&quot;TdsOnPLB&quot;:18.57,&quot;TdsOnIncentive&quot;:18.83,&quot;ServiceFee&quot;:0,&quot;TotalBaggageCharges&quot;:0,&quot;TotalMealCharges&quot;:0,&quot;TotalSeatCharges&quot;:0,&quot;TotalSpecialServiceCharges&quot;:0,&quot;ConvenienceFee&quot;:200,&quot;ChargedFare&quot;:7259.54}">
<input type="hidden" id="bookingFareReturn" value="{&quot;TransactionFee&quot;:0,&quot;AirTransFee&quot;:0,&quot;AdditionalTxnFee&quot;:0,&quot;Currency&quot;:&quot;INR&quot;,&quot;BaseFare&quot;:5742,&quot;Tax&quot;:1500,&quot;TaxBreakup&quot;:[{&quot;key&quot;:&quot;K3&quot;,&quot;value&quot;:322},{&quot;key&quot;:&quot;YQTax&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;YR&quot;,&quot;value&quot;:100},{&quot;key&quot;:&quot;PSF&quot;,&quot;value&quot;:300},{&quot;key&quot;:&quot;UDF&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;INTax&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;TransactionFee&quot;,&quot;value&quot;:0},{&quot;key&quot;:&quot;OtherTaxes&quot;,&quot;value&quot;:678}],&quot;YQTax&quot;:0,&quot;AdditionalTxnFeeOfrd&quot;:0,&quot;AdditionalTxnFeePub&quot;:0,&quot;PGCharge&quot;:0,&quot;OtherCharges&quot;:3.54,&quot;ChargeBU&quot;:[{&quot;key&quot;:&quot;TBOMARKUP&quot;,&quot;value&quot;:&quot;0&quot;},{&quot;key&quot;:&quot;CONVENIENCECHARGE&quot;,&quot;value&quot;:&quot;0&quot;},{&quot;key&quot;:&quot;OTHERCHARGE&quot;,&quot;value&quot;:&quot;3.54&quot;}],&quot;Discount&quot;:0,&quot;PublishedFare&quot;:7245.54,&quot;CommissionEarned&quot;:96.19,&quot;PLBEarned&quot;:61.91,&quot;IncentiveEarned&quot;:62.78,&quot;OfferedFare&quot;:7024.66,&quot;TdsOnCommission&quot;:28.86,&quot;TdsOnPLB&quot;:18.57,&quot;TdsOnIncentive&quot;:18.83,&quot;ServiceFee&quot;:0,&quot;TotalBaggageCharges&quot;:0,&quot;TotalMealCharges&quot;:0,&quot;TotalSeatCharges&quot;:0,&quot;TotalSpecialServiceCharges&quot;:0,&quot;ConvenienceFee&quot;:100,&quot;ChargedFare&quot;:7345.54}" name="pass.fareReturn">
<input type="hidden" id="bookingFareBreakDown" name="pass.fareBreakDown" value="[{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;1&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;2300&quot;,&quot;Tax&quot;:&quot;598&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;},{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;2&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;2300&quot;,&quot;Tax&quot;:&quot;598&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;},{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;3&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;1200&quot;,&quot;Tax&quot;:&quot;60&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;}]">
<input type="hidden" id="bookingFareBreakDownReturn" value="[{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;1&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;2300&quot;,&quot;Tax&quot;:&quot;721&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;},{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;2&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;2300&quot;,&quot;Tax&quot;:&quot;721&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;},{&quot;Currency&quot;:&quot;INR&quot;,&quot;PassengerType&quot;:&quot;3&quot;,&quot;PassengerCount&quot;:1,&quot;BaseFare&quot;:&quot;1142&quot;,&quot;Tax&quot;:&quot;58&quot;,&quot;YQTax&quot;:&quot;0&quot;,&quot;AdditionalTxnFeeOfrd&quot;:&quot;0&quot;,&quot;AdditionalTxnFeePub&quot;:&quot;0&quot;,&quot;PGCharge&quot;:&quot;0&quot;}]" name="pass.fareBreakDownReturn">
<input type="hidden" id="bookingTravelId" name="pass.travelId" value="OXTL001397">
<input type="hidden" id="BookingTraceId" name="pass.traceId" value="0de72c0c-27a0-4c21-bf7a-d980e1f15e13">
<input type="hidden" id="BookingResultIndex" name="pass.resultIndex" value="OB5">

			

<h3>Adult(s)</h3><div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<select class="form-control adult-gender gender-select" name="pass.gender">
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control adult-inp first-Name alph-vl" placeholder="First Name" name="pass.firstName">
			<div class="errorMsg"></div>
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control adult-inp last-Name alph-vl" placeholder="Last Name" name="pass.lastName">
			<div class="errorMsg"></div>
			
		
		</div>
		
			<div class="col-md-3">
			<input type="date" style="display:none" class="form-control adult-inp dob" placeholder="DOB" name="pass.dateOfBirth">
			<div class="errorMsg"></div>
			
		
		</div>
		
	
	</div>
</div><h3>Child(ren)</h3><div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<select class="form-control child-gender gender-select" name="pass.gender">
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control child-inp first-Name alph-vl" placeholder="First Name" name="pass.firstName">
			<div class="errorMsg"></div>
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control child-inp last-Name alph-vl" placeholder="Last Name" name="pass.lastName">
<div class="errorMsg"></div>
			
		
		</div>
		
			<div class="col-md-3">
			<input type="text" class="form-control child-inp dob" value="" placeholder="Date of Birth" name="pass.dateOfBirth">
				<input type="hidden" class="dobHidden" value="" placeholder="Date of Birth">

<div class="errorMsg"></div>
			
		
		</div>
		
	
	</div>
</div><h3>Infant(s)</h3><div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<select class="form-control infant-gender gender-select" name="pass.gender">
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control infant-inp first-Name alph-vl" placeholder="First Name" name="pass.firstName">
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control infant-inp last-Name alph-vl" placeholder="Last Name" name="pass.lastName">
			
		
		</div>
		<div class="col-md-3">
			<input type="text" class="form-control infant-inp dob" value="" placeholder="Date of Birth" name="pass.dateOfBirth">
			
		
		</div>
		</div>
			
		
	
	
</div></form>
</div>

<div class="block-head">
<span class="step-count">03</span>
Confirmation &amp; Payment
</div>
<div class="details-block" id="Travallers-p-details">
<div class="container-fluid">

	
	
		<div class="row">
			<div class="col-md-4">
				<label></label>
			</div>
			<div class="col-md-4">
				<input type="button" class="btn-block btn-primary" value="Confirm and Pay" onclick="ticketBooking()" id="confirmBtn">
			</div>
	</div>
</div>
</div>
</div>
<div class=" fare-summary">
<div class="summary-header"></div>
<h4>Fare Summary</h4>
<div class="travaller-summary">
<h5>Travellers</h5>

<div class="info-block"><label> Adult(s) </label> <span class="s-value">01 <span class="currency">Pax </span></span></div>
<div class="info-block" id="child-pax"><label>Child(ren) </label> <span class="s-value"> 01<span class="currency">Pax</span></span></div>
<div class="info-block" id="infant-pax"><label> Infant(s) </label> <span class="s-value">01<span class="currency"> pax</span></span></div>


</div>



<div class="fare-break-total">
<h5>Fare </h5>



</div>



<div class="fare-break">
<div class="info-block"><label>Base Fare</label> <span class="s-value">  <span class="currency">INR</span>11542.00</span></div>
<div class="info-block"><label>Taxes and surcharges</label> <span class="s-value"> <span class="currency">INR</span>3363.08</span></div>
<div class="info-block"><label>Convenience Fee</label> <span class="s-value"> <span class="currency">INR</span>300.00</span><div>
</div>


</div>

<div class="bottom-total">
<div class="info-block">
<label>Total Amount</label><span class="s-value">
 <span class="currency">INR</span>
<span class="amount-span">14605.08</span>
<span>

</span></span></div>


</div>


</div>
</div>

</div>


		
</div>


</div>
		
	
		</div>
	
	


		</div>
	</div>
	</div>	
		
		

							
		
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       <input type="hidden" id="fareBreakDown" /> 
<div class="bookingFooter">
	<div class="ch-container">
	<div class="row ">
		
		<div class="col-lg-12 col-sm-12">
		<div class="container-fluid">
		<div style="width:calc(50% - 100px);float:left" id="booksingle-flight"></div>
		<div style="width:calc(50% - 100px);float:left" id="bookreturn-flight"></div>
		<div class="final-booking" >
		<input type="hidden" id="singleFlightPrice" />
		<input type="hidden" id="returnFlightPrice" />
		
		<input type="hidden" id="AdultCount" />
		<input type="hidden" id="ChildCount" value="0" />
		<input type="hidden" id="InfantCount" value="0" />
		<input type="hidden" id="TraceId" />
			<input type="hidden" id="JourneyType" value="1" />
		<input type="hidden" id="IsLCC" />
			
		<input type="hidden" id="AirlineCode" />
		<input type="hidden" id="ResultIndex" />
		<input type="hidden" id="IsLCCR" />
			
		<input type="hidden" id="AirlineCodeR" />
		<input type="hidden" id="ResultIndexR" />
		
	
		<div id="total-amount"><span id="curr"></span><span id="cost"></span></div>
			<button class="btn submit-form btn-info btn-fill" onclick="bookFlightAction()" id="booking-btn">Book</button>
		</div>
		</div>
		
		
		</div>
	
	</div>
	
	
	</div>

</div>


</div>
</div>

<!-- Modal -->
<div id="errModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
  
  
      <div class="modal-body" style="padding: 0;margin: 0px;">
          <div class="alert alert-danger" style="margin:0px;display: block;padding:20px!important">
    <strong>Failed!</strong> <span id="errMsgFli"></span>
    <button type="button" class="close" style=" color: #650000;"data-dismiss="modal">&times;</button>
  </div>
      </div>
        <div class="alert alert-danger">
    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
    </div>

  </div>
</div>

<!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

 	 	<script src="./js/jquery.datetimepicker.full.min.js"></script>
 	 		<script src="./js/chosen.jquery.min.js"></script>
 	 			<script src="./js/prism.js"></script>

<script src="js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="./newmis/js/jquery.cookie.js"></script>
<script src="./newmis/js/jquery.noty.js"></script>
<script src="./newmis/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<%-- <script src="./newmis/js/charisma.js"></script> --%>
<div class="flight-loading-div">


<div class="loading-centerdiv">
	 <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>" />
					<%
						}
	 
	 %>
	 <h2>Hang on! Preparing your Itinerary</h2>
	 <div class="container-fluid">
	 	<div class="col-md-5" id="load-origin">
	 	
	 	</div>
	 	<div class="col-md-2">
	 		<i class="fa fa-plane flight-animation" id="loading-f-animation"></i>
	 	</div>
	 	<div class="col-md-5" id="load-destination">
	 	
	 	</div>
	 
	 </div>
	 <div class="progress-bar-div">
	 
	 </div>
	

</div>

</body>
</html>



