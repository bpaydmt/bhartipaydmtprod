              
                <%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TransDetail"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TAG0"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Beneficiary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CardDetail"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.UserDetails"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
   
   
 <script type="text/javascript">
 function submitRemoveForm(event, id){
	
	 event.preventDefault();
	  $(id).submit();
	 
	}
 

 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- contents starts -->



			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->

				<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->

				<%
				
UserDetails userDetails=(UserDetails)request.getAttribute("userDetails");
List<TransDetail> transDetails=null;
if(userDetails!=null){
transDetails=userDetails.getTransDetails();
DecimalFormat d=new DecimalFormat("0.00");
System.out.println(transDetails.size());
%>
				<div class=" row">
					<div class="row" id="hidethis2">
						<div class="box col-md-12">
							<div class="box-inner">
								<div class="box-header well">
									<h2>
										<i class="glyphicon "></i>DMT Transaction History
									</h2>


								</div>
								<div class="box-content row">
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
									<div class="col-lg-12 col-md-12 box-inner">
										<div class="row">
											<div class="table-responsive b-details">
												<table class="table table-striped table-bordered">
													<thead>
														<tr>
															<th>Trans date</th>
															<th>Wallet trans id</th>
															<th>Merchant trans id</th>
															<th>Topup trans id</th>
															<th>Account no</th>
															<th>Amount</th>
															<th>Status</th>
															<th>Remark</th>
															<th>Action</th>
															
														</tr>
													</thead>
													<tbody>

														<%
														if(transDetails!=null){
                              Iterator<TransDetail> tite=transDetails.iterator();
                              while(tite.hasNext()){
                            	  TransDetail t=tite.next();
                            	  if(t!=null){
                              %>
														<tr>
														<td><%=t.getTransDateTime() %></td>
															<td><%=t.getAgentTransId() %></td>
															<td><%=t.getMrTransId()%></td>

															<td><%=t.getTopupTransId()%></td>
															<td><%=t.getBenefAccNo()%></td>
															<td><%=d.format(Double.parseDouble(t.getAmount())) %></td>
															<td><%=t.getStatus() %></td>
															<td><%=t.getRemark() %></td>
															<td>
															<form action="TransStatus" id="<%=t.getAgentTransId() %>" method="post">
															<input type="hidden" name="dmtInputBean.agentTransId" value="<%=t.getAgentTransId()%>">
															<a class="btn small-btn" onclick="submitRemoveForm(event,'#<%=t.getAgentTransId() %>')"
															 href="#"> 
																		Check Status</a>
															</form>
															
															
															</td>
														</tr>
														<%}
                            	  }
                              }
                              } %>
													</tbody>
												</table>
											</div>
											
										</div>

									</div>






<form action="Dmt">
<input type="submit" value="Back" class="btn form-btn btn-info btn-fill">
</form>



								</div>



							</div>



						</div>
					</div>

				</div>

			</div>
		</div>

	</div>



	<!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


                