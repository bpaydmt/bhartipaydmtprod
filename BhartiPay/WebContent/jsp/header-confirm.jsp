<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="head.jsp"></jsp:include>
</head>
<body>
	<!-- NAVBAR ================================================== -->
	<div class="container">
		<nav class="navbar navbar-inverse navbar-static-top" role="navigation"
			style="margin-bottom: 0px;">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"
						style="color: #FFFFFF; font-family: 'Bauhaus 93'; font-size: 28px; font-style: normal; font-weight: 100; text-transform: uppercase;">
						<img src="./images/logo1.png" style="width: 15%; height: auto;">Digital Wallet
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse pull-right"
					style="line-height: 40px;">
					<ul class="nav navbar-nav">
						<li><a href="plan-scheme.php">Plans &amp; Schemes</a></li>
						<li><a href="support.php">Support</a></li>
						<li><a href="javascript:;" id="login_popup">Login</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<!-- Call after login success -->
	<div class="container" style="display: none;">
		<nav class="navbar navbar-inverse navbar-static-top" role="navigation"
			style="margin-bottom: 0px;">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"
						style="color: #FFFFFF; font-family: 'Bauhaus 93'; font-size: 28px; font-style: normal; font-weight: 100; text-transform: uppercase;">
						<img src="./images/logo1.png" style="width: 15%; height: auto;">Digital Wallet
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse pull-right"
					style="line-height: 40px;">
					<ul class="nav navbar-nav">
						<li><a href="plan-scheme.php">Plans &amp; Schemes</a></li>
						<li><a href="support.php">Support</a></li>
						<li><a href="user-profile.php" id="login_popup">My
								Account</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>