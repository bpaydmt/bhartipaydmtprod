<%@page import="com.bhartipay.wallet.kyc.bean.EkycResponseBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    
    
    EkycResponseBean kycbean=(EkycResponseBean)session.getAttribute("kycbean");
    %>
<!DOCTYPE html>
<html>
<head>
	<title>KYC Update</title>

<link rel="stylesheet" type="text/css" href="jspas/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">
	 
	 <script src="./js/jquery.min.js"></script> 
		<link rel="stylesheet" type="text/css" href="jspas/mainStyle.css">

</head>
<body>
	<div class="container midSection">

 <div class="row mainRow">

 	<div class="container-fluid ">
 		<div class="box-inner row">
 			<div class="grid-box col-md-12">
 				<div class="box-header ">
	

Proof of Indentity  
</div>
			<div class="box-inner-content "> 
					<div class="" style=" max-width: 800px;
    margin: auto;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 8px;">
<div class="row">
		<div class="col-md-3 pull-right"><img src="data:image/gif;base64,<%=kycbean.getPht()%>" style="width: 100%; max-width: 160px">
	</div>
	<div class="col-md-9  pull-left">
		 

					<table class="table table-responsive table-bordered table-striped text-center">
   
    <tbody>
    
      <tr>
      	<td><strong>Name</strong></td>
      	<td><%=kycbean.getPoiname()==null?"N/A":kycbean.getPoiname()%></td>

      </tr>
      </tr>
      <tr>

        <td><strong>Gender</strong></td>
        <td><%=kycbean.getPoigender()==null?"N/A":kycbean.getPoigender()%></td>
      </tr>
      <tr>
        <td><strong>Proof of Address</strong> </td>
        <td><%=kycbean.getPoahouse()==null?"N/A":kycbean.getPoahouse()%>- <%=kycbean.getPoaloc()==null?"N/A":kycbean.getPoaloc()%>- <%=kycbean.getPoasubdist()==null?"N/A":kycbean.getPoasubdist()%>- <%=kycbean.getPoadist()==null?"N/A":kycbean.getPoadist() %>- <%=kycbean.getPoavtc()==null?"N/A":kycbean.getPoavtc() %>-<%=kycbean.getPoastate()==null?"N/A":kycbean.getPoastate() %>-<%=kycbean.getPoapc()==null?"N/A":kycbean.getPoapc() %></td>
      </tr>
       <tr>
        <td><strong>Phone</strong> </td>
        <td><%=kycbean.getPoiphone()==null?"N/A":kycbean.getPoiphone()%></td>
      </tr>
       
       <tr>
        <td><strong>Father Name</strong> </td>
        <td><%=kycbean.getPoaco()==null?"N/A":kycbean.getPoaco()%></td>
      </tr>
       
       <tr>
        <td><strong>Email Id</strong> </td>
        <td><%=kycbean.getPoiemail()==null?"N/A":kycbean.getPoiemail()%> </td>
      </tr>
       <tr>
        <td><strong>DOB</strong> </td>
        <td><%=kycbean.getPoidob()==null?"N/A":kycbean.getPoidob()%></td>
      </tr>
       
    </tbody>
  </table>

	</div>



</div>
					<div class="row">

	<div class="col-md-12"></div><a href="MoneyTransfer?kycForm=true" class="btn btn-primary btn-info">Back</a></div>
	
<%-- 	
	
	<div class="col-md-6">
					<form action="kycfConfirm" method="post">
	<input type="hidden" name="kyctxnid" value="<%=kycbean.getKyctxnid()%>" >
	<input type="hidden" name="status" value="Confirm" >
	<input type="submit" value="submit" class="pull-right btn btn-primary btn-info"/>
	</form>
					</div>
		<div class="col-md-6">
		<form action="kycfReject" method="post">
	<input type="hidden" name="kyctxnid" value="<%=kycbean.getKyctxnid()%>" >
	<input type="hidden" name="status" value="Reject" >
	<input type="submit" value="submit" class="pull-left btn btn-primary btn-info"/>
	
	</form>
		</div> --%>

					</div>

						
					</div>
			</div>
 				
 			</div>
 			
 		</div>
 </div>
</div>
</body>
</html>