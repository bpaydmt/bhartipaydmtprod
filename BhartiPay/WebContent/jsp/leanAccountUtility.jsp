<%@page import="com.bhartipay.wallet.report.bean.TxnReportByAdmin"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="com.bhartipay.lean.LeanAccount"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src='./js/highcharts.js'></script>
	

<style>
 #leanAccountButton{
 display:none;
 }
 .leanAccountTable{
 display:none;
 }
 
 #errmsg
{
color: red;
}
 
</style>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
 


/*
$('#amount').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
*/


$(document).ready(function () { 
    $("markedAslean").click(function () { 
        location.reload(true); 
        alert('Reloading Page'); 
    }); 
});

  </script>
    
  
</head>

<body>
		<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

          <jsp:include page="dashBoard.jsp"></jsp:include>

			
	 <div class="row">
			<div class="container">
				<div class="col-md-4">
                        <s:select list="subAggregatorList" listKey="key"
						onchange="getLeanAccount()" id="selectAggregator" listValue="key"
						headerValue="Select Agent" headerKey="-1" />
						
			   <input type="hidden"     id="nameAgent" />
               <input type="hidden"    id="idAgent"  name="userId" />
               <input type="hidden"    id="walletIdAgent" name="walletId"/>
						
				</div>
				<div class="col-md-4">
				<input type="button" id="markedAslean"
						onclick="markAsLean()" value="Mark As lean" style="margin-top: 15px;" />
				</div>
				
				<div id="msg" style="color: red ;" ></div>
				</div>
				</div>
				<br> 
			<div class="row">
				<div class="container">
				<div class="col-md-4">
					<input type="number" id="amount" id="amount" style="margin-top: 10px; width: 100%;  "
						placeholder="Please enter amount to be leaned" />
				</div>
				
				<div class="col-md-4">
					<input type="button" id="leanAccountButton"
						onclick="saveRecord()" value="Save Lean Amount" style="margin-top: 10px;" />
				</div>



               </div>
			</div>


    
 <br><br><br>  <span id="test">
 <table align="center" border="5" cellpadding="5" cellspacing="5" width="100%"  id="leanAccountTable" class="leanAccountTable" style="display: block;float: left;width: 100%;">
	
 
  <thead>  
      <tr>
		<td align="center" style="width: 3%;"><u>Sr.No</u></td>
		<td align="center" style="width: 10%;"><u>Agent ID</u></td>
		<td align="center" style="width: 15%;"><u>Wallet ID</u></td>
		<td align="center" style="width: 10%;"><u>Amount</u></td>
		<td align="center" style="width: 10%;"><u>Action</u></td> 
		
	  </tr>
  </thead>				
 
 <tbody>
  
</tbody>
						
 </table>
       
       

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->


	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<script src="./js/subAggUtil.js"></script>
	<!-- application script for Charisma demo -->
	


</body>
</html>