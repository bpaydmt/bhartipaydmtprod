<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript"
	src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style>
.popup-overlay-other1 {
	visibility: hidden;
	position: fixed;
	width: 50%;
	height: 50%;
	left: 25%;
	top: 38%;
}

.overlay-other1 {
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0;
	left: 0;
	background: rgba(0, 0, 0, .6);
	/* z-index: 100000; */
}

.popup-overlay-other1.active {
	visibility: visible;
	z-index: 999;
}

.popup-content-other1 {
	visibility: hidden;
}

.popup-content-other1.active {
	visibility: visible;
}

.box-login-title-other1 {
	top: 30%;
	color: #111;
	position: absolute;
	width: 50%;
	height: auto;
	left: 25%;
	background-color: rgba(255, 255, 255, 0.8);
	padding-top: 10px;
	padding-bottom: 10px;
}

.MessageBoxMiddle-other1 {
	position: relative;
	left: 20%;
	width: 60%;
}

.close-other1 {
	float: none;
	font-size: 16px;
	font-weight: bold;
	line-height: 1;
	color: #fff;
	text-shadow: none;
	opacity: 1;
	background: #a57225 !important;
	padding: 10px;
}

.popup-btn-other1 {
	font-size: 16px;
	font-weight: bold;
	color: #fff;
	background: #a57225 !important;
	padding: 7px;
	/* cursor: pointer; */
}

.MessageBoxMiddle-other1 .MsgTitle-other1 {
	letter-spacing: -1px;
	font-size: 24px;
	font-weight: 300;
}

.txt-color-orangeDark-other1 {
	color: #a57225 !important;
}

.MessageBoxMiddle-other1 .pText-other1 {
	font-size: 16px;
	text-align: center;
	margin-top: 14px;
}

.MessageBoxButtonSection-other1 span {
	float: right;
	margin-right: 7px;
	padding-left: 15px;
	padding-right: 15px;
	font-size: 14px;
	font-weight: 700;
}

.popup-overlay-other1 input[type="text"] {
	border-bottom-color: #111 !important;
	margin-bottom: 15px;
	color: #111 !important;
}

.bbpsloader {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	display: none;
	background: url('images/bbps/ring_loader.gif') 50% 50% no-repeat
		rgba(255, 255, 255, 0.8);
}
</style>


<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 
 
 
 
%>





<script type="text/javascript"> 
    $(document).ready(function() {
    	
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
   }); 
</script>
<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>

</head>

<% 
	User user = (User) session.getAttribute("User");
	Date myDate = new Date(); 
	SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
	String toDate=format.format(myDate);
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -0);
	Date from= cal.getTime();    
	String fromDate = format.format(from);
	Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

	//List<String>list=(List<String>)request.getAttribute("resultList");
	DecimalFormat d=new DecimalFormat("0.00"); 
%>



<body>
<%
String txnAmount="";
String agentId="";
List response2=(List)request.getAttribute("listResult");

%>

<script type="text/javascript">
function getAQCList(){
	 var txnId = $("#txnId").val();
	
	 $.ajax({
		url:"getAmountPopup",
		method:"post",
		data:"txnId="+txnId,
		success:function(response_data_json) {
			
			
			
			if(confirm(response_data_json)){
				creditWallet(txnId);
			}
			
	    }
	 });

	
}



function creditWallet(txnId){
	
	 $.ajax({
			url:"pgTransactionsUpdate",
			method:"post",
			data:"txnId="+txnId,
			success:function(resp){
			
				alert(resp)
			}
			
		 });

}

</script>
 

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->

	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->



	<div id="main" role="main">
		<div id="content">

			<div class=" row">

				<div class="box col-md-12">
					<div class="bbpsloader" id="loaderDiv"></div>
					<div class="box-inner">

						<!--Creates the popup body-->
						<div class="popup-overlay-other1 overlay-other1"
							style="display: block;">
							<div class="popup-content-other1">
								<div class='box-login-title-other1'>
									<div class="MessageBoxMiddle-other1">
										<span class="MsgTitle-other1"
											style="color: orange; font-size: 25px; font-weight: bold;">Do
											you want to proceed?</span>
										<div style="text-align: center; margin-bottom: 10px;">
											<div id="popupInfoDetails"></div>
											<button type="button" class="btn btn-success"
												id="payPopUpYes">Yes</button>
											<button type="button" class="close-other1 btn btn-primary"
												onclick="payPopUpNo();" style="padding: 6px 12px;">No</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Popup End -->

						<div class="box-header">
							<h2>PG Transaction</h2>
						</div>

						<div class="container">

							
								TxnId: <input type="text" id="txnId" name="txnId" /> <br>
								
								<br>
								<input  type="submit" name="submit" id="submit" onclick="getAQCList()"/>
								<!-- <input type="submit" name="submit" id="submit" value="submit" /> -->
								<br>
						
						</div>

						<div class="container">
							<div id="currBalance" style="display: none;"
								class="alert alert-info"></div>
						</div>


					</div>
				</div>
			</div>
		</div>
		<!-- contents ends -->
	</div>
	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%-- <script src='js/bootstrap.min.js'></script> --%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->







</body>
</html>



