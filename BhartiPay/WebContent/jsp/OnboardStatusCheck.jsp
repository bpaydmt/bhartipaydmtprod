<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<jsp:include page="/jsp/reqFiles.jsp"></jsp:include>
<jsp:include page="/jsp/gridJs.jsp"></jsp:include>
<%
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String  theams=mapResult.get("themes");

String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");

String version = "?i=1000tuesday20191";
String sessionid=(String)session.getAttribute("sessionid");

%>

<!DOCTYPE html>
<html>
<head>
<title><%=mapResult.get("caption") %></title>
 <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<%=mapResult.get("caption") %>">
    <meta name="author" content="<%=mapResult.get("caption") %>">   
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/whitelabel.css" />

<link rel="shortcut icon" href="jspbk/assets/img/<%=favicon%>"> 
<!-- <link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css"> --> 
<!-- <link rel="stylesheet" type="text/css" href="jspbk/mainStyle.css"> -->
 
<!-- <link href="./css/datepicker.min.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<link href="jspbk/assets/cssLibs/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jspbk/assets/jsLibs/angular.min.js"></script>
<script type="text/javascript"  src="jspbk/assets/jsLibs/angular-route.js"></script>
<!-- <script type="text/javascript"  src="jspbk/assets/jsLibs/jquery-3.1.1.min.js"></script> -->
<script type="text/javascript"  src="jspbk/assets/jsLibs/angu-fixed-header-table.js"></script>
<script  type="text/javascript" src="jspbk/assets/jsLibs/dirPagination.js"></script>

<!-- <script type="text/javascript" src="jspbk/assets/jsLibs/pdfmake.min.js"></script>
<script type="text/javascript" src="jspbk/assets/jsLibs/vfs_fonts.js"></script> -->
<script type="text/javascript" src="jspbk/assets/cssLibs/bootstrap-3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="jspbk/app/dmt.module.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/dmt.routes.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/services/appServices.js<%=version %>"></script>  
<script type="text/javascript" src="jspbk/app/components/newUser/senderFormCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/error/errorMsgCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/addBeneficiary/addBeneficiaryCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/regUser/regUserCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/components/importSender/importSenderCtrl.js<%=version %>"></script>
<script type="text/javascript" src="jspbk/app/components/favGrid/favListCtrl.js<%=version %>"></script> 
<script type="text/javascript" src="jspbk/app/components/reports/reportCtrl.js<%=version %>"></script> 


<!-- <script  type="text/javascript" src="./js/datepicker.js"></script>
<script  type="text/javascript"  src="./js/datepicker.en.js"></script> -->

<script  type="text/javascript"  src="jspbk/assets/jsLibs/Blob.min.js"></script>
<script  type="text/javascript" src="jspbk/assets/jsLibs/FileSaver.min.js"></script>
<script  type="text/javascript"  src="jspbk/assets/jsLibs/tableexport.min.js"></script>
<script  type="text/javascript"  src="jspbk/assets/jsLibs/alasql.min.js"></script>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.7/xlsx.core.min.js"></script>
<script type="text/javascript" src="jspbk/assets/jsLibs/chosen.jquery.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >
<script src="/js/Newtheme-column-visibility.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="jspbk/assets/cssLibs/newtheme.css" /> 



<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>




<script type="text/javascript"> 
    $(document).ready(function() { 
        
    $('#ta-data').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
       
        ]
    } ); 
    } );  
	
</script>

<style> 
#bankNames_chosen{
width:100%!important
}

.menu-on-top #main { 
    padding-top: 30px;
    padding-bottom: 30px;
}

	
	.smart-style-3 .nav>li>a:focus, .smart-style-3 .nav>li>a { 
	    border-color: rgba(255, 255, 225, .15);
	    /* color: #666!important; */
	}
	
	.form-control[readonly] {
       background-color: transparent!important;
	}

.panel {
   margin-bottom: 0px !important;
}


body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 35%;
  margin-left: auto;
  margin-right: auto;
}

/* The Close Button */
.closes {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.closes:hover,
.closes:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}











  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  
  
  .popup-overlay-other1{ 
    visibility:hidden;
    position:fixed; 
    width:50%;
    height:50%;
    left:25%;
    top: 38%; 

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content-other1 { 
   	visibility:hidden;
  }

  .popup-content-other1.active { 
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
	  width: 50%;
	  height: auto;
	  left: 25%;  
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px; 
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  
  
.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important; 
        margin-bottom: 15px;
        color: #111 !important;
}
</style>





</style>

<script type="text/javascript">

function manage(txt) {
	 debugger
   var bt = document.getElementById('submit');
   if (txt.value != '') {
       bt.disabled = false;
   }
   else {
       bt.disabled = true;
   }
}

function showNotification(icici,fino,yes)
{
	$(".popup-overlay-other1, .popup-content-other1").addClass("active");
	  
    var result = '';
	  result = result + '<table style="font-size:150%">';
	  if(icici==0)
		 result = result + '<tr ><td style="color:red;">ICICI &nbsp;&nbsp; &nbsp;</td><td style="color:red;">'+icici+'</td></tr>';
		else
		 result = result + '<tr ><td style="text-align:left;">ICICI &nbsp;&nbsp; &nbsp; </td><td style="text-align:left;"><span style="color:blue;">'+icici+'</span></td></tr>';
		
	  if(fino==0)
		    result = result + '<tr ><td style="color:red;">FINO &nbsp;&nbsp; &nbsp;</td><td style="color:red;">'+fino+'</td></tr>';
		else
		result = result + '<tr ><td style="text-align:left;">FINO &nbsp;&nbsp; &nbsp;</td><td style="text-align:left;"><span style="color:blue;">'+fino+'</span></td></tr>';
		
	  if(yes==0)
		    result = result + '<tr ><td style="color:red;">YES &nbsp;&nbsp; &nbsp;</td><td style="color:red;">'+yes+'</td></tr>';
		else
			result = result + '<tr ><td style="text-align:left;">YES &nbsp;&nbsp; &nbsp;</td><td style="text-align:left;"><span style="color:blue;">'+yes+'</span></td></tr>';
		
	  result = result + '</table><br>';
	  
	  $("#popupConfirmation").empty().append(result);

}

function showNotify(status, text){
	
	$(".popup-overlay-other1, .popup-content-other1").addClass("active");
	  
	     var result = '';
		 result = result + '<table style="font-size:150%">';
		 if(status==true)
		 {
		 result = result + '<tr ><td style="color:red;"></td></tr>';
		 result = result + '<tr ><td style="color:red;">'+text+'</td></tr>';
		 }else
		 {
		 result = result + '<tr ><td style="color:red;">'+text+'</td></tr>'; 
		 }
		  result = result + '</table><br>';
		  
		  $("#popupConfirmation").empty().append(result);

		  
}

function popupOk(){
	$(".popup-overlay-other1, .popup-content-other1").removeClass("active");
	document.getElementById("txnId").value=""; 
	  var bt = document.getElementById('submit');
	  bt.disabled = true;
}


function  statusCheck()
{debugger

	var id = document.getElementById("txnId").value; 

	 $.ajax({
		method:'Post',
		cache:0,  		
		url:'onboardStatusCheck',
		data:'txnId='+id,
		success:function(response){
        var json = JSON.parse(response);
		var status=json.status;
		
			if(status=="00")
		    {
                var icici=json.icicionboardstatus;
				var fino=json.finoonboardstatus;
				var yesBank=json.yesonboardstatus;

				  showNotification(icici,fino,yesBank);
			 // $('#msg').html("Your Agent ID "+id+" is  Onboard"+"\n"+ "ICICI "+icici+"  FINO "+fino+"  Yes "+yesBank);
		    }else if(status=="A0001")
		    {
		    	var msg=json.message;
		    	showNotify(true,msg);
		    }else
		    {
		    	 showNotify(false,response);  
		    }
       
		}
	})
		   
}

</script>


</head>
<body data-ng-app="oxyModule" >
	
	 <!-- topbar starts -->
<jsp:include page="/jsp/header.jsp"></jsp:include>
    <!-- topbar ends -->        
        <!-- left menu starts -->
<jsp:include page="/jsp/mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->


<div id="main" role="main"> 
    <div id="content">  
		<div class=""> 
		
			<div class="" data-ng-controller="mainCtrl as main" data-ng-init="main.getTheUserData()"  data-ng-click="main.getAgentSenderBalance()"> 
				<div class="midSection" >  
				    
					    <div class="grid-box"> 
							<div class="box-inner" >
<!-- 
							    <div class="box-header ng-binding"> 
							         <h2>Onboard Status Check</h2>
							        <a class="pull-right" href="#/"></a>
							    </div> 
 -->
						 <div class="row">
									
							<div class="box-header">
							 <h2>Onboard Status Check</h2>
						     </div>
						     <form>  <!-- action="onboardStatusCheck" -->
                                <div class="container">
                                <div class="row">
                                <div class="col-md-4"> <input type="text" id="txnId" name="txnId" placeholder="Enter AgentId"  onkeyup="manage(this)"/> <br>
									</div><br>
									<div class="col-md-2">
									<input  type="submit"   name="submit" id="submit" onclick="statusCheck()"  disabled  />
									
									</div><br>
									</div>
								</div>
							  </form>	
							</div> 

							</div> 
					    </div>    <div class="col-md-2" id="msg" style="color: red ;" ></div>
				    
				 </div> 
			</div> 
		</div>  
    </div>
</div>


<!--Creates the popup body-->
					      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
					        <div class="popup-content-other1"> 
					         <div class='box-login-title-other1'> 
					           <div class="MessageBoxMiddle-other1">  
					            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Onboard Status Details</span> <br><br>
						            <div style="text-align:center;margin-bottom: 10px;">
						            	<div id="popupConfirmation"> </div>
							           <button type="button" class="close-other1 btn btn-primary" onclick="popupOk();" style="padding: 6px 12px;">OK</button>
						            
							        </div> 
					          </div>
					         </div>   
					        </div>
					      </div> 
					   <!-- Popup End -->


<script type="text/javascript" type="text/javascript">
    $(function(){
	SESSIONID = '<%=sessionid %>' 
		$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			
			if(data.sessionId != SESSIONID){

				window.close();
				window.location.replace("/BhartiPay/UserHome");
			}
		 })
		 })
		 
		})
		window.onpopstate = function (e) { window.history.forward(1); } 
</script>

<jsp:include page="/jsp/footer.jsp"></jsp:include>
</body>
</html> 