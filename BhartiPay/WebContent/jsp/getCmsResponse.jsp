<%@page import="com.bhartipay.lean.CmsEnquiryResponse"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<style>
table {
    font-family: arial, sans-serif;
    width: 100%;
    font-size:small;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;

}

tr:nth-child(even) {
    background-color: #dddddd;
}






  .popup-overlay-other1{
    visibility:hidden;
    position:fixed;
    width:50%;
    height:50%;
    left:25%;
    top: 38%;

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{
    visibility:visible;
    z-index: 999;
  }

  .popup-content-other1 {
   visibility:hidden;
  }

  .popup-content-other1.active {
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
          width: 50%;
          height: auto;
          left: 25%;
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none;
       font-size: 16px;
       font-weight: bold;
      line-height: 1;
      color: #fff;
      text-shadow:none;
      opacity:1;
      background: #a57225!important;
      padding: 10px;
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  }


.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important;
        margin-bottom: 15px;
        color: #111 !important;
}
</style>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

<br><br><br><br><br><br>
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="mainWrapper" role="main"> 
	    <div id="content" class="container-fluid"> 
            <div class="row">
            <h3>CMS</h3>
            <div class="col-lg-12 col-md-12 col-sm-12">
            
            <form action="GetCmsResponse" id="cms" method="post" name="PG">
	            TxnId: 
	            <div class="row"> <div class="col-sm-6">
	            <input type="text" id="txnId"  class="formInputBox" 
	                   placeholder="Please enter transaction Id" name="dmtInputBean.agentTransId"  
						value="<s:property value='%{dmtInputBean.agentTransId}'/>" /> </div>
				<div class="col-sm-4">							
	            <input type="submit" name="submit" id="submit" class="btn btn-success btn-fill" /></div>
            </div>
            </form>
            
            <!--    <div class="row" class="box-content">
					<div class="col-lg-4 col-md-4 col-sm-12"> TxnId: <input type="text" id="txnId" name="txnId"
							class="formInputBox" />
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12"><div>    <br></div>
							<input type="submit" name="submit" id="submit"
								class="btn btn-success btn-fill" onclick="getAQCList()" />
						</div>
			   </div> -->
			 
 <div class="box" id="aepsPrintArea">
               
<%
CmsEnquiryResponse data=(CmsEnquiryResponse)request.getAttribute("cmsLedger");
if(data!=null && !data.getStatus().equalsIgnoreCase("V0022")){
%>
<div class="">
        <table id="example" class="scrollD cell-border dataTable no-footer" style="width:100%; border: 1px solid black;border-collapse: collapse;color:green;">
               
                <tr>
                   <td style="border:1px solid black;font-weight: bold;">UserId</td>
                   <td style="border:1px solid black;"><%=data.getAgent_Id()!=null ? data.getAgent_Id():"-"%></td>
                   <td style="border:1px solid black;font-weight: bold;">Transaction Id</td>
                   <td style="border:1px solid black;"><%=data.getTxnId()!=null ? data.getTxnId():"-"%></td>
                </tr>

                <tr>
                    <td style="border:1px solid black;font-weight: bold;">Transaction Amount</td>
                        <td style="border:1px solid black;"><%=data.getAmount()%></td>
                        <td style="border:1px solid black;font-weight: bold;">Status</td>
                        <td style="border:1px solid black;"><%=data.getTxnStatus()%></td>
                </tr>
                
                <tr>
                    <td style="border:1px solid black;font-weight: bold;">Mobile Number</td>
                    <td style="border:1px solid black;"><%=data.getMobileNo()!=null ? data.getMobileNo():"-"%></td>
                    <td style="border:1px solid black;font-weight: bold;"> RRN </td>
                    <td style="border:1px solid black;"><%=data.getRrn()!=null ? data.getRrn():"-"%></td>
               </tr>

                <tr>
                        <td style="border:1px solid black;font-weight: bold;">Bank Name</td>
                        <td style="border:1px solid black;"><%=data.getBankName()!=null ? data.getBankName():"-" %></td>
                        <td style="border:1px solid black;font-weight: bold;">Transaction date</td>
                        <td style="border:1px solid black;"><%=data.getDateOfTransaction()!=null ? data.getDateOfTransaction():"-"%></td>
                </tr>

               

                <tr>
                        <td style="border:1px solid black;font-weight: bold;">Service</td>
                        <td style="border:1px solid black;"><%=data.getService()!=null ? data.getService():"-"%></td>
                        <td style="border:1px solid black;font-weight: bold;">Action</td>
                        <td style="border:1px solid black;"><%=data.getAction()!=null ? data.getAction():"-"%></td>
                </tr>
 
        </table>

        <div id="tbl-powered-by" style="display:none;"><span style="float:right;margin-top:3px;text-align:right;font-style: italic;">Powered by Bhartipay Services Pvt. Ltd.</span></div>


        <div style="text-align: center;padding: 10px;" id="print-sms-close">
                <a class="print-other1 btn btn-info" tabindex="0" aria-controls="example" href="#">
                  <span>Print</span>
                </a>
                 
                <a class="btn btn-info" tabindex="0" aria-controls="example" href="GetCmsResponse">
                  <span>Close</span>
                </a>
        </div>
</div>
<%} else if(data!=null && data.getStatus().equalsIgnoreCase("V0022")){%>
       
                 <tr>
                        <td style="border:1px solid black;font-weight: bold;">Status</td>
                        <td style="border:1px solid black;"><%=data.getTxnStatus()%></td>
                </tr>

<%} %>
                                                                        </div>
                                                                        </div>
 

 
            </div>
        </div>

</div>


 

<jsp:include page="footer.jsp"></jsp:include>


<script type="text/javascript">

function getAQCList() {
debugger	
	var txnId = $("#txnId").val();
debugger
	$.ajax({
		url : "GetCmsResponse",
		method : "post",
		data : "txnId=" + txnId,
		success : function(response) {
 
alert(response);
		}
	});

}

</script>



<script>
  $(".open-other1").on("click", function(){
    $(".popup-overlay-other1, .popup-content-other1").addClass("active");
  });
  $(".close-other1").on("click", function(){
    $(".popup-overlay-other1, .popup-content-other1").removeClass("active");
  });

  $(".print-other1").on("click", function(){
          printNow();
  });

  function sendSMS(txnId, mobileNo, txnType, channel)
  {
  mobileNo = $('#mobileNo').val();
  $.ajax({url: "sendSMSAlert",
          data:"inputBean.mobileNo="+mobileNo+"&inputBean.txnId="+txnId+"&inputBean.txnType="+txnType+"&inputBean.pgtxn="+channel,
          success: function(result){
            $("#message").html(result);
          }});
  }

/*   $("sendsms").click(function(){
          $.ajax({url: "demo_test.txt", success: function(result){
            $("#message").html(result);
          }});
        });
   */



   
   function printNow()
   {
        var originalContents = document.body.innerHTML;

        $("#tbl-powered-by").show();
        $("#logoPics").show();
        $("#aepsPics").attr('style', 'width:20%');

        var divToPrint=document.getElementById("aepsPrintArea");

        $("#print-sms-close").hide();
        $("#sms-popup").hide();

        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
        document.body.innerHTML = originalContents;
   }

   $(function(){
                $.get('GetUserConfigByDomain',function(data){
                        var img = $.parseJSON(data.result);
                        console.log(img);
                        $('#logoPics').attr('src',img.logopic);

                })
        });

</script>

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


