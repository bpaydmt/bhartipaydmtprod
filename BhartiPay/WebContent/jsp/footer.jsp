<%@page import="java.util.Map"%>

<!-- <hr> --> 
  <%
  Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
  String merchantName = mapResult.get("appname");
  %>  

  <footer class="page-footer"> 
    <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-12 align-self-center" style="text-align: center;/* padding: 15px; */">
                <a href="#" data-toggle="modal" data-target="#aboutus">About US</a> | 
                <a href="#" data-toggle="modal" data-target="#privacypolicy">Privacy Policy</a> | 
                <a href="#" data-toggle="modal" data-target="#cancellation">Cancellation &amp; Refunds Policy</a> 
        </div>
      <div class="col-xs-7 col-sm-7" style="text-align: right ;">
        <span class="txt-color-white">
          &copy;2019<%--  <%=mapResult.get("appname")%> --%> All rights reserved.<!--  <font size="1">Powered by BhartiPay.</font> -->
        </span>
      </div>  
    </div>  
  </footer> 
  </div>


  <div id="aboutus" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><%=mapResult.get("appname")%>
            - About US.
          </h4>
        </div>
        <div class="modal-body" style="max-height: 400px; overflow: auto">
         <p>
<%=mapResult.get("appname")%> is an innovative and ultra-proficient financial service provider. It offers Fintech services across the globe using the internet and top-grade payment tools. Our online Recharge services encompass B2B Services & white label Recharge and Bill Payment solutions
</p>
<p>
We are rapidly thriving as an independent and well-reputed services provider on the global level. Our efficient financial service provider bestow Agents with top-notch services that are designed according to them and is ultra-safe and secure.
</p>
<p>
With a multiyear experience and proficient squad, we determine the needs and requirements of our clients before they realize them.
</p>
<p>
our know-how predictability helps us in delivering the above par result while retaining the economy.
</p>
 <p>
Our online payment gateway services allow you to take and make transactions with utmost ease and flawlessness like never before.
</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>


  <div id="privacypolicy" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><%=mapResult.get("appname")%>
            - Privacy Policy.
          </h4>
        </div>
        <div class="modal-body" style="max-height: 400px; overflow: auto">
       
<div class="entry-content">
		<p><strong><%=merchantName%> </strong>is committed to protecting the privacy of your information. This Privacy Statement describes the company's Web site privacy practices.</p>
<ol>
<li><strong> Website</strong></li>
</ol>
<p>This Privacy Statement covers the information practices of:</p>
<p><%=merchantName%></p>
<ol start="2">
<li><strong> Information Collected &amp; Use</strong></li>
</ol>
<p><strong><%=merchantName%> </strong>offers a variety of services that are collectively referred to as the "Services". <strong><%=merchantName%> </strong>collects information from individuals who visit the Web sites ("Visitors") and individuals who register to use the Services ("Customers").</p>
<p><strong><%=merchantName%> </strong>is committed to protecting your data and complying to all appropriate legislation. This document details:</p>
<p>2.1 What personal information we collect?<br>
2.2 Why and how do we use the information?<br>
2.3 With whom do we share the information?<br>
2.4 Your rights relating to the data we collect?<br>
2.5 When do we delete the information?<br>
2.6 What choices do we give you, and how can you correct or review your personal information?<br>
2.7 Changes and adjustments</p>
<p>Notwithstanding any other provision in this Privacy Policy, you agree that <strong><%=merchantName%> </strong>shall be entitled to use and disclose any data, information and feedback arising from your use of the Service, subject to such data being anonymized and such use and disclosure being in compliance with applicable Privacy Laws.</p>
<p><strong>2.1 What personal information we collect?</strong></p>
<p><strong><%=merchantName%> </strong>collects selected personal information when you register with us, request information, or when you use <strong><%=merchantName%> </strong>products or services. When you register or request information we request the following:</p>
<ul>
<li>First and Last Names</li>
<li>Contact email address</li>
<li>Contact number</li>
<li>Company Name</li>
<li>Industry</li>
</ul>
<p>As you use our products and services we may also request additional information in order to deliver the product or services. We will also collect payment information, e.g. your credit card number, and billing address if you purchase or license products and services from us. If you visit our website(s), we also record information about the browser, device type, browser type and language, your Internet Protocol (IP) address, and geographic data associated with the IP address etc. Other data as you choose to upload into <strong><%=merchantName%></strong> product(s),</p>
<p><strong>2.2 Why and how do we use the information?</strong></p>
<p>The information collected as described above is used to:</p>
<ul>
<li>Prepare and deliver services or product you've requested.</li>
<li>Create user profiles and give you access to our range of products and services</li>
<li>For billing and invoicing purposes</li>
<li>Operating our internal operations, systems, products, and services</li>
<li>Understanding &nbsp;user experience with a view to enhance the user experience of our services and products.</li>
<li>Provide customer support related to our services and product.</li>
<li>Send service-related information associated with our products and services, including confirmation but not limited to of actions taken (e.g. change of password), invoices raised, updates and enhancements, security alerts plus support and administrative messages</li>
<li>Updates about promotions, upcoming events, and news about products and services</li>
</ul>
<p>In addition, Data that is uploaded by the user (either directly by the user using CSV, Google Sheets or via an authenticated API connection) is used to derive KPIs (Key Performance Indicators). These KPIs are either predefined by <strong><%=merchantName%> </strong>or defined by the end user.</p>
<p><strong>2.3 With whom do we share the information?</strong></p>
<p>We may share your personal data with service providers we hire for to perform services on our behalf, for example generation of invoices and bills. These service providers do not have the right to use your data for their own purposes.</p>
<p><strong><%=merchantName%> </strong>does not rent, sell, or share data about you with other people or non-affiliated companies.</p>
<p>From time to time, <strong><%=merchantName%></strong> may partner with other companies to jointly offer products or services. If you purchase or specifically express interest in a jointly-offered product or service from <strong><%=merchantName%> </strong>the Group may share Data about <strong><%=merchantName%> </strong>Customers collected in <strong><%=merchantName%></strong>, connection with your purchase or expression of interest with our joint promotion partner(s). <strong><%=merchantName%> </strong>does not control our business partners' use of the Data about <strong><%=merchantName%> </strong>Customers we collect, and their use of the information will be in accordance with their own privacy policies. If you do not wish for your information to be shared in this manner, you may opt not to purchase or specifically express interest in a jointly offered product or service.</p>
<p><strong><%=merchantName%> </strong>shall be entitled to use any data, information and feedback arising from your use of the Service, subject to use of the data arising from your use of the Service being anonymized and in compliance with applicable Privacy Laws.</p>
<p><strong><%=merchantName%> </strong>reserves the right to use or disclose information provided if required by law or if the Company reasonably believes that use or disclosure is necessary to protect the Company's rights and/or to comply with a judicial proceeding, court order, or legal process.</p>
<p><strong>2.4 Your rights relating to the data we collect?</strong></p>
<p>You have Right to:</p>
<p><strong>Be informed</strong></p>
<p>We are obliged to 'fair processing information' on how your typically how personal data will be used. We are using this declaration for this purpose.</p>
<p><strong>Access</strong></p>
<p>Your have the right to obtain confirmation as to whether personal data concerning is being processed, where and for what purpose. We are using this declaration for this purpose.</p>
<p><strong>Rectification</strong></p>
<p>You are entitled to have personal data rectified if it is inaccurate or incomplete</p>
<p><strong>Erasure (right to be forgotten)</strong></p>
<p>This right entitles you to:</p>
<ul>
<li>Have your personal data erased</li>
<li>Cease further dissemination of the data</li>
<li>Potentially have third parties halt processing of the data</li>
</ul>
<p><strong>Restrict Processing</strong></p>
<p>You have the right to request the restriction or suppression of the processing of your personal data.</p>
<p><strong>Data Portability</strong></p>
<p>You have the right to obtain and reuse their personal data. It allows you to move, copy or transfer your personal data easily from one IT environment to another in a safe and secure way, without hindrance to usability.</p>
<p><strong>Object</strong></p>
<p>You have the right to object to the use of your personal data for direct marketing.</p>
<p><strong>Automated Deletion Marking &amp; Profiling</strong></p>
<p>We use anonymised data in our profiling activities</p>
<ol start="3">
<li><strong> Web Site Navigational Information</strong></li>
</ol>
<p><strong><%=merchantName%> </strong>uses commonly-used information-gathering tools, such as cookies and Web beacons, to collect information as you navigate the Company's Web site ("Web Site Navigational Information"). This section describes the types of Web Site Navigational Information that may be collected on the company's Web site and how this information may be used.</p>
<p><strong>3.1 Cookies</strong></p>
<p><strong><%=merchantName%> </strong>uses cookies to make interactions with the Company's Web site easy and meaningful. When you visit the Company's Web site, <strong><%=merchantName%> 's </strong>servers send a cookie to your computer. Standing alone, cookies do not personally identify you. They merely recognise your Web browser. Unless you choose to identify yourself to <strong><%=merchantName%> </strong>either by responding to a promotional offer, opening an account, or filling out a Web form (such as a "Contact Me"), you remain anonymous to the Group. <strong><%=merchantName%> </strong>uses cookies that are session-based and persistent-based. Session cookies exist only during one session. They disappear from your computer when you close your browser software or turn off your computer. Persistent cookies remain on your computer after you close your browser or turn off your computer.</p>
<p>If you have chosen to identify yourself to <%=merchantName%> the Group uses session cookies containing encrypted information to allow the Group to uniquely identify you. Each time you log into the Services, a session cookie containing an encrypted, unique identifier that is tied to your account is placed your browser. These session cookies allow the Group to uniquely identify you when you are logged into the Services and to process your online transactions and requests. Session cookies are required to use the Services.</p>
<p><%=merchantName%> uses persistent cookies that only the Group can read and use to identify browsers that have previously visited the Group's Web site. When you purchase the Services or provide the Group with personal information, a unique identifier is assigned you. This unique identifier is associated with a persistent cookie that the Group places on your Web browser. The Group is especially careful about the security and confidentiality of the information stored in persistent cookies. For example, the Group does not store account numbers or passwords in persistent cookies. If you disable your Web browser's ability to accept cookies, you will be able to navigate the Group's Web site, but you will not be able to successfully use the Services.</p>
<p><%=merchantName%> may use information from session and persistent cookies in combination with Data about <%=merchantName%> Customers to provide you with information about the Company and the Services.</p>
<p><strong>3.2 Web Beacons</strong></p>
<p><%=merchantName%> uses web beacons alone or in conjunction with cookies to compile information about Customers and Visitors' usage of the Group's Web site and interaction with emails from the Group. Web beacons are clear electronic images that can recognize certain types of information on your computer, such as cookies, when you viewed a particular Web site tied to the Web beacon, and a description of a Web site tied to the Web beacon. For example, <%=merchantName%> may place web beacons in marketing emails that notify the Group when you click on a link in the email that directs you to one of the Group's Web site. <%=merchantName%> uses web beacons to operate and improve the Group's Web site and email communications.</p>
<p><%=merchantName%> may use information from web beacons in combination with Data about <%=merchantName%> Customers to provide you with information about the Group and the Services.</p>
<p><strong>3.3 Flash Cookies</strong></p>
<p>may use local shared objects, also known as flash cookies, to store your preferences or display content based upon what you view on our site to personalise your visit. Third parties, with whom the Group partners to provide certain features on our site or to display advertising based upon <%=merchantName%> your Web browsing activity, use flash cookies to collect and store information.</p>
<p>flash cookies are different from browser cookies because of the amount of, type of, and how data is stored. Cookie management tools provided by your browser will not remove flash cookies. To learn how to manage privacy and storage settings for flash cookies click here.</p>
<p><strong>3.4 IP Addresses</strong></p>
<p>When you visit <%=merchantName%> 's Web site, the Group collects your Internet Protocol ("IP") addresses to track and aggregate non-personal information. For example, <%=merchantName%> uses IP addresses to monitor the regions from which Customers and Visitors navigate the Group's Web site.</p>
<p><strong>3.5 Third Party Cookies</strong></p>
<p>From time-to-time, <%=merchantName%> engages third parties to track and analyses usage and volume statistical information from individuals who visit the Group's Web site. <%=merchantName%> may also use other third-party cookies to track the performance of Group advertisements. The information provided to third parties does not include personal information, but this information may be re-associated with personal information after the Group receives it.</p>
<p><%=merchantName%> may also contract with third-party advertising networks that collect IP addresses and other Web Site Navigational Information on the Group's Web site and emails and on third-party Web sites. Ad networks follow your online activities over time by collecting Web Site Navigational Information through automated means, including through the use of cookies. They use this information to provide advertisements about products and services tailored to your interests. You may see these advertisements on other Web sites. This process also helps us manage and track the effectiveness of our marketing efforts. To learn more about these and other advertising networks and their opt-out instructions, click here.</p>
<p><strong>3.6 Links to other websites</strong></p>
<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>
<ol start="4">
<li><strong> Public Forums and Customer Testimonials</strong></li>
</ol>
<p><%=merchantName%> may provide bulletin boards, blogs, or chat rooms on the Group's Web site. Any personal information you choose to submit in such a forum may be read, collected, or used by others who visit these forums, and may be used to send you unsolicited messages. <%=merchantName%> is not responsible for the personal information you choose to submit in these forums.</p>
<p><%=merchantName%> may post a list of Customers and testimonials on the Group's Web site that contains information such as Customer names and titles. <%=merchantName%> obtains the consent of each Customer prior to posting any information on such a list or posting testimonials.</p>
<ol start="5">
<li><strong> International Transfer of Information Collected</strong></li>
</ol>
<p>To facilitate In.Corp's global operations, the Group may transfer and access Data about <%=merchantName%> Customers from around the world. This Privacy Statement shall apply even if <%=merchantName%> transfers Data about <%=merchantName%> Customers to other countries.</p>
<ol start="6">
<li><strong> Communications Preferences</strong></li>
</ol>
<p><%=merchantName%> offers Customers and Visitors who provide contact information a means to choose how the Group uses the information provided. You may manage your receipt of marketing and non-transactional communications by clicking on the "unsubscribe" link located on the bottom of the Group's marketing emails. Additionally, you may send a request specifying your communications preferences to&nbsp;Contact@<%=merchantName.replace("-","").toLowerCase()%>.com Customers cannot opt out of receiving transactional emails related to their account with <%=merchantName%> or the Services.</p>
<ol start="7">
<li><strong> Security</strong></li>
</ol>
<p><%=merchantName%> uses appropriate administrative, technical, and physical security measures to protect Data About <%=merchantName%> Customers.</p>
<ol start="8">
<li><strong> Changes to this Privacy Statement</strong></li>
</ol>
<p><%=merchantName%> reserves the right to change this Privacy Statement. <%=merchantName%> will provide notification of the material changes to this Privacy Statement through the Group's Web site at least thirty (30) business days prior to the change taking effect.</p>
<ol start="9">
<li><strong> Contacting Us</strong></li>
</ol>
<p>Questions regarding this Privacy Statement or the information practices of the Group's Web site should be directed to&nbsp;<strong>support@<%=merchantName.replace("-", "").toLowerCase() %>.com</strong></p>
<p>&nbsp;</p>
	</div>



				</div>
       
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>


  <div id="cancellation" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><%=mapResult.get("appname")%>
            - Term & conditions for business associate.
          </h4>
        </div>
        <div class="modal-body" style="max-height: 400px; overflow: auto">
          <ul>
            <li><strong class="number">1</strong> <strong class="heading">GENERAL</strong>


              <p>
                Pursuant to Reserve Bank of India (<strong>"BANK"</strong>)
                grating authorisation to Bhartipay Service Private Limited (<strong>"<%=mapResult.get("appname")%>"
                </strong> or <strong>"Us"</strong>) for issuance and operation of
                Semi-closed Prepaid Payment Instruments, <strong><%=mapResult.get("appname")%></strong>
                proposes to appoint you (<strong>"You"</strong>) as the business
                associate of <strong><%=mapResult.get("appname")%> ("Business
                  Associate")</strong> to carry out the Services (as defined herein),
                subject to Your acceptance and adherence to the terms contained
                herein. Before logging on to the web portal at <strong><%=mapResult.get("domainname")%></strong>
                or the mobile application offered by the <strong><%=mapResult.get("appname")%></strong>
                namely <strong>"Bhartipay Service Private Limited"</strong>
                for the purpose of acting as a Business Associate, You are
                required to read these terms of use (<strong>"Terms"</strong>).
                In case You are agree to these Terms, You can proceed to log-in.
                In case You are do not agree to these Terms, You should not
                proceed to log-in. The web-portal at <strong><%=mapResult.get("domainname")%></strong>
                and/or mobile application namely <strong>"<%=mapResult.get("appname")%>
                  Agent App"
                </strong> shall hereinafter be referred to as <strong>"Terminal"</strong>.

                By logging into the Terminal as aforesaid, You agree to be bound
                by these Terms. You agree and acknowledge that the access and
                use of the Terminal by You is voluntary and of Your own accord,
                and that the Terms shall be binding on You, for access and use
                of the Terminal for providing the Services, in India (<strong>"Territory"</strong>).
                Your access and use of the Terminal shall be subject to the
                applicable laws, rules and regulations in force in the
                Territory, and as may be amended from time to time (<strong>"Applicable
                  Laws"</strong>).
              </p></li>

            <li><strong class="number">2</strong> <strong class="heading">DEFINITIONS</strong>

              <p>The words and expressions beginning with capital letters
                in these Terms shall, unless the context otherwise requires,
                have the meaning ascribed thereto herein:</p>
              <ul>
                <li><strong class="number">(a)</strong>
                  <p>Applicable Law: shall mean all applicable laws,
                    statutes, rules, regulations, directions, guidelines,
                    ordinances, orders, policies, judgments, decrees or other
                    requirements or official directive of the applicable
                    Governmental authorities, statutory authorities, regulatory
                    authorities, BANK or courts of law or any person or entity
                    acting through or under such authorities.</p></li>
                <li><strong class="number">(b)</strong>
                  <p>
                    Bank: shall mean the sponsor bank who is direct member of
                    immediate payment service (<strong>"IMPS"</strong>) network
                    provided by the National Payments Corporation of India (<strong>"NPCI"</strong>)
                    and with whom <strong><%=mapResult.get("appname")%></strong> has tied up
                    for facilitating the participation of
                    <%=mapResult.get("appname")%>
                    for the provisioning of Services.
                  </p></li>
                <li><strong class="number">(c)</strong>
                  <p>Customers: shall mean the end users availing the
                    Services.</p></li>
                <li><strong class="number">(d)</strong>
                  <p>Business Associate Point Location: shall mean the shop
                    or office premises owned or rented by the Business Associate,
                    to be used for the purpose of rendering of Services to the
                    Customers.</p></li>
                <li><strong class="number">(e)</strong>
                  <p>
                    Services: shall mean the services under <strong>"<%=mapResult.get("appname")%>"
                    </strong> brand of
                    <%=mapResult.get("appname")%>, facilitating the domestic remittances of
                    the Customers via Terminal.
                  </p></li>
                <li><strong class="number">(f)</strong>
                  <p>
                    Software Program: shall refer to various software programme /
                    application provided by <strong><%=mapResult.get("appname")%></strong>
                    which can be accessed via the Terminal or installed into a
                    computer or mobile device enabling the dispensation of
                    Services electronically.
                  </p></li>
                <li><strong class="number">(g)</strong>
                  <p>Transactions: shall mean the electronic transactions
                    pertaining to rendering of Services to the Customers through
                    the Terminals.</p></li>
              </ul></li>

            <li><strong class="number">3</strong> <strong class="heading">YOUR
                OBLIGATIONS </strong>
              <ul>
                <li><strong class="number">3.1</strong>
                  <p>You shall at all times comply with all the Applicable
                    Laws relevant to the obligations of the Business Associate as
                    per terms of this Agreement.</p></li>
                <li><strong class="number">3.2</strong>
                  <p>
                    You shall at all times strictly comply with all the
                    instructions, directions, guidelines and policies issued by <strong><%=mapResult.get("appname")%></strong>,
                    as the case may be, from time to time.
                  </p></li>
                <li><strong class="number">3.3</strong>
                  <p>
                    You shall maintain with <strong><%=mapResult.get("appname")%></strong>, at
                    all times, a minimum amount as directed by <strong><%=mapResult.get("appname")%></strong>
                    from time to time (<strong>"Float Amount"</strong>), towards
                    rendering of Services. Float Amount shall be used/ adjusted
                    towards the settlement of the Transactions made by in relation
                    to the Services. You agree that the value of the Transactions
                    under the Terms shall not exceed the Float Amount at any point
                    of time in any circumstance. The quantum of the Float Amount
                    shall be replenished from time to time by You in order to
                    maintain the minimum balance. Maintenance of such Float Amount
                    is the essential condition. Without prejudice to its other
                    rights, <strong><%=mapResult.get("appname")%></strong> reserves the right
                    to set-off or adjust dues or loss suffered by it on account of
                    breach or non-compliance of the Terms by You or on account of
                    fine/penalty imposed by BANK or any other regulatory authority
                    due to acts of commission or omission on Your part in
                    providing the Services, from the Float Amount deposited by You
                    with <strong><%=mapResult.get("appname")%></strong>.
                  </p></li>
                <li><strong class="number">3.4</strong>
                  <p>
                    You shall ensure correctness of all details received from the
                    Customers and <strong><%=mapResult.get("appname")%></strong> will not be
                    liable for any wrong or illegal or incorrect Transaction.
                  </p></li>
                <li><strong class="number">3.5</strong>
                  <p>You shall not hold or retain cash received from
                    Customers without completing the Transaction.</p></li>
                <li><strong class="number">3.6</strong>
                  <p>
                    You shall resolve all queries, clarifications, inquiries,
                    disputes and / or complaints of the Customers in relation to
                    the Services offered. It being clarified that, You shall act
                    as instructed by
                    <%=mapResult.get("appname")%>
                    in this regard and shall be required to confirm with
                    <%=mapResult.get("appname")%>
                    for any resolution that You may undertake in this regard.
                  </p></li>
                <li><strong class="number">3.7</strong>
                  <p>
                    You agree to provide adequate space at the Business Associate
                    Point Location for display/selling/ branding/marketing of the
                    Services as per the guidelines of <strong><%=mapResult.get("appname")%></strong>
                    or/and statutory regulations of BANK.
                  </p></li>
                <li><strong class="number">3.8</strong>
                  <p>You agree to take all necessary care and observe
                    diligence while carrying out Transactions and shall be wholly
                    and exclusively responsible for any liability arising due to
                    failure to do so.</p></li>
                <li><strong class="number">3.9</strong>
                  <p>
                    You in Your all dealings with the third parties shall describe
                    Yourself only as "Business Associate" of <strong><%=mapResult.get("appname")%></strong>
                    and under no other description or nomenclature. You shall not
                    describe Yourself as the partner or employee of <strong><%=mapResult.get("appname")%></strong>
                    nor shall it be entitled to represent <strong><%=mapResult.get("appname")%></strong>
                    in any manner whatsoever.
                  </p></li>
                <li><strong class="number">3.10</strong>
                  <p>
                    You shall not (except in the normal course of
                    <%=mapResult.get("appname")%>'s business) publish any article or
                    statement, deliver any lecture or broadcast or make any
                    communication to the press, including magazine publication
                    relating to
                    <%=mapResult.get("appname")%>'s services or to any matter with which
                    <%=mapResult.get("appname")%>
                    may be concerned, unless You have previously applied to and
                    obtained the written permission from
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.11</strong>
                  <p>
                    You shall not give or make any guarantees, warranties or
                    representations on behalf of
                    <%=mapResult.get("appname")%>
                    as to the conditions, quality, durability, performance,
                    Business Associate's ability of the Services other than or
                    different from those provided by
                    <%=mapResult.get("appname")%>
                    in writing.
                  </p></li>
                <li><strong class="number">3.12</strong>
                  <p>You shall not engage, deal with or enter in to an
                    agreement with any other party or any company/organization
                    engaged in rendering/sale/promotion of same/similar services.
                  </p></li>
                <li><strong class="number">3.13</strong>
                  <p>
                    You shall not accept any gift, commission or any sort of
                    gratification in cash or kind from any person, party or firm
                    or other company having dealing with
                    <%=mapResult.get("appname")%>
                    and if You are offered any of such, You shall immediately
                    report the same to the management of
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.14</strong>
                  <p><%=mapResult.get("appname")%>, at its sole discretion, may provide
                    to You suitable advertisement support, technical and marketing
                    guidance and staff training as and when it may deem necessary.
                    You shall not make use of
                    <%=mapResult.get("appname")%>'s logo/mark on any slips/receipts, visiting
                    cards, letter heads etc. without written approval/consent of
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.15</strong>
                  <p>
                    You shall be solely responsible to pay the salaries and other
                    employees related payments to Your personnel/employees and for
                    taking policies such as medical insurance, life insurance etc.
                    for them and to comply with all Applicable Laws relating to
                    employment of such personnel/employees of You and
                    <%=mapResult.get("appname")%>
                    shall have no responsibility or liability of any kind
                    whatsoever for the same.
                  </p></li>
                <li><strong class="number">3.16</strong>
                  <p>You undertake that You shall at Your own risk and cost
                    keep and maintain the Business Associate Point Location in
                    proper manner for rendering of the Services to the Customers
                    and procure/ facilitate sufficient man power for the same and
                    keep the complete and proper accounts relating to rendering of
                    the Services.</p></li>
                <li><strong class="number">3.17</strong>
                  <p>
                    You shall integrate all Your IT equipment (computer/mobile
                    devices) with <strong><%=mapResult.get("appname")%></strong> for seamless
                    rendering of the Services.
                  </p></li>
                <li><strong class="number">3.18</strong>
                  <p>
                    You undertake that You shall not do anything that damages the
                    name, goodwill and reputation of
                    <%=mapResult.get("appname")%>, its subsidiaries, and its clients, its
                    affiliates and shall protect and enhance the name, goodwill
                    and reputation of
                    <%=mapResult.get("appname")%>
                    during the course of the fulfillment of its obligations under
                    this Agreement.
                  </p></li>
                <li><strong class="number">3.19</strong>
                  <p>
                    You shall not render the Services at prices above the marked
                    price as indicated by
                    <%=mapResult.get("appname")%>. You will also be directly liable to the
                    sponsor bank or NPCI or BANK, in case of breach of this
                    condition.
                  </p></li>
                <li><strong class="number">3.20</strong>
                  <p>You shall not indulge in or promote any unlawful,
                    illicit or illegal activity or purposes pertaining to the
                    rendering of the Services.</p></li>
                <li><strong class="number">3.21</strong>
                  <p>
                    You shall ensure that You shall carry out all verifications of
                    the Customer as may be required by Applicable Law from time to
                    time and as per the instructions of
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.22</strong>
                  <p>
                    You shall only levy service charges as specified by
                    <%=mapResult.get("appname")%>.
                    <%=mapResult.get("appname")%>
                    shall have right to terminate Your appointment as Business
                    Associate at any time without notice and may take legal action
                    if You are found to have collected unauthorized fees/charges
                    other than specified by
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.23</strong>
                  <p>
                    You shall take all care in entering the Customer's details
                    correctly for carrying out Transactions. You are solely
                    responsible to resolve issues arising out of any incorrect
                    details and
                    <%=mapResult.get("appname")%>
                    shall not have any liability or responsibility in such cases.
                  </p></li>
                <li><strong class="number">3.24</strong>
                  <p>
                    You shall be responsible for every Transaction done through
                    You and You shall maintain books, papers, records, registers
                    and accounts in accordance with the requirements of Applicable
                    Laws and/or as required by
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.25</strong>
                  <p>
                    You shall provide Services to the Customers on the spot as and
                    when asked/required by the Customers during working hours.
                    <%=mapResult.get("appname")%>
                    shall not be responsible for any dispute arising between You
                    and Customer (s). In the event it is found/brought to notice
                    of
                    <%=mapResult.get("appname")%>
                    that You are misbehaving with any Customer,
                    <%=mapResult.get("appname")%>
                    may take any strict action, that may include termination of
                    Your appointment as Business Associate.
                  </p></li>
                <li><strong class="number">3.26</strong>
                  <p>
                    Unless otherwise agreed in writing, You agrees to give
                    non-refundable charge to
                    <%=mapResult.get("appname")%>, as intimated by
                    <%=mapResult.get("appname")%>
                    for the purpose of provisioning of Services through the
                    Terminal. This charge will not be refundable in any
                    circumstance and is a charge towards Services setup and
                    maintenance costs of
                    <%=mapResult.get("appname")%>.
                  </p></li>
                <li><strong class="number">3.27</strong>
                  <p>
                    You agree to maintain the strict secrecy and confidentiality
                    of log-in user-id and password provided to You and You shall
                    not disclose the same to any third party. You agree that You
                    shall be solely responsible for any unauthorised use or
                    disclosure of Your user-id and password and
                    <%=mapResult.get("appname")%>
                    shall not be liable in any manner whatsoever for any losses,
                    claims, liabilities arising out of or in connection with such
                    use and/or disclosure.
                  </p></li>
                <li><strong class="number">3.28</strong>
                  <p>Exclusive Use of Business Associate Point Location for
                    Services - You undertake to use Business Associate Point
                    Location solely for the Services and confirm that Business
                    Associate Point Location are neither being currently used and
                    nor will be simultaneously used in future for any similar
                    services and/or product offerings of any other company/person.
                  </p></li>
                <li><strong class="number">3.29</strong>
                  <p>Code of Conduct</p>
                  <ul>
                    <li><strong class="number">A.</strong>
                      <p>
                        <span class="underline">CUSTOMER SERVICE</span>
                      </p></li>
                    <li><strong class="number">A.1.</strong>
                      <p>The Customer communication information, facilities
                        including but not limited to, Services information, fees,
                        charges and transaction limits, terms and conditions needs
                        to be displayed at the Business Associate Point Location in
                        such a way that it is prominently visible to the approaching
                        Customers.</p></li>
                    <li><strong class="number">A.2.</strong>
                      <p>Customers need to be attentively attended and all
                        their doubts and queries should be clarified with the best
                        effort by You.</p></li>
                    <li><strong class="number">A.3.</strong>
                      <p>
                        Any dispute or transaction errors at the counter shall be
                        resolved or raised to
                        <%=mapResult.get("appname")%>
                        before the Customer leaving the counter.
                      </p></li>
                    <li><strong class="number">A.4.</strong>
                      <p>Business Associate shall prominently display at
                        Business Associate Point Location, the details of the
                        Customer grievance policy, Customer complaint redressal
                        mechanism including the contact details and phone number for
                        complaint redressal, for the benefits of the Customers.</p></li>
                    <li><strong class="number">B.</strong>
                      <p>
                        <span class="underline">WORKPLACE RESPONSIBILITIES -
                          DO'S AND DON'TS</span>
                      </p></li>
                    <li><strong class="number">B.1.</strong>
                      <p>
                        <span class="underline">Do's:</span>
                      </p></li>
                    <li><strong class="number">B.1.1.</strong>
                      <p>Personal dignity, privacy, and personal rights of
                        every individual should be maintained.</p></li>
                    <li><strong class="number">B.1.2.</strong>
                      <p>Work together with women and men of various
                        nationalities, cultures, religions, and races in a
                        professional manner.</p></li>
                    <li><strong class="number">B.1.3.</strong>
                      <p>Maintaining honesty and transparency at every stage of
                        carrying out the Services.</p></li>
                    <li><strong class="number">B.2.</strong>
                      <p>
                        <span class="underline">Don'ts:</span>
                      </p></li>
                    <li><strong class="number">B.2.1.</strong>
                      <p>Do not discriminate, harass or offend anybody by
                        whatever means.</p></li>
                    <li><strong class="number">B.2.1.</strong>
                      <p>Do not engage in contacts with competitors that could
                        create even an appearance of improper arrangement, whether
                        the contact is in person, in writing, by telephone or
                        through e-mail.</p></li>
                    <li><strong class="number">C.</strong>
                      <p>
                        <span class="underline">CONFLICT OF INTEREST AND
                          OUTSIDE ACTIVITIES</span>
                      </p></li>
                    <li><strong class="number">1.</strong>
                      <p>Conflicts of interest can occur if business practices
                        sacrifice interests of one set of Customers in favour of
                        another or place business interests ahead of Customers.</p></li>
                    <li><strong class="number">2.</strong>
                      <p>Business Associate shall be sensitive and responsible
                        to any activities, interests or relationships that might
                        interfere with or even appear to interfere with, his/her
                        ability to act in the best interests of all stakeholders.</p></li>
                    <li><strong class="number">D.</strong>
                      <p>
                        <span class="underline">PRIVACY - DO'S AND DON'TS</span>
                      </p></li>
                    <li><strong class="number">D.1.</strong>
                      <p>
                        <span class="underline">Do's</span>
                      </p></li>
                    <li><strong class="number">D.1.1.</strong>
                      <p>Properly control access to your work areas and
                        computers.</p></li>
                    <li><strong class="number">D.1.2.</strong>
                      <p>Protect the physical security of official information.
                      </p></li>
                    <li><strong class="number">D.1.3.</strong>
                      <p>Limit access to information strictly to those with a
                        legitimate business reason for seeking that information.</p></li>






                    <li><strong class="number">D.2.</strong>
                      <p>
                        <span class="underline">Don'ts</span>
                      </p></li>


                    <li><strong class="number">D.2.1.</strong>
                      <p>Do not discuss sensitive matters or confidential
                        information in public places.</p></li>
                    <li><strong class="number">D.2.2.</strong>
                      <p>Do not transfer official information into personal
                        databases or carry hard copies of official information,
                        otherwise than for official purposes outside the office.</p></li>
                    <li><strong class="number">D.2.3.</strong>
                      <p>Do not disclose the username and password of the
                        Terminal to any one.</p></li>
                    <li><strong class="number">E.</strong>
                      <p>
                        <span class="underline">KNOW YOUR CUSTOMER (KYC)/
                          ANTI MONEY LAUNDERING</span>
                      </p></li>
                    <li><strong class="number">1.</strong>
                      <p>
                        Business Associate shall always and strictly follow the KYC
                        policy of
                        <%=mapResult.get("appname")%>, made specifically for the purpose of
                        providing the Services. Business Associate shall keep in
                        proper condition and safe custody all the documentations
                        with regard to Customers as per KYC policy and will provide
                        the same to
                        <%=mapResult.get("appname")%>
                        promptly from time to time or as directed by
                        <%=mapResult.get("appname")%>.
                      </p></li>
                    <li><strong class="number">2.</strong>
                      <p>Business Associate acknowledges that Money Laundering
                        legislations criminalize money laundering in respect of all
                        crimes including drug trafficking, terrorism, theft, tax
                        evasion, fraud, handling of stolen goods, counterfeiting and
                        blackmail. It is also an offence to undertake and/or
                        facilitate transactions with individuals and entities
                        involved in criminal activities.</p></li>
                    <li><strong class="number">3.</strong>
                      <p>
                        Business Associate is aware of the Anti Money Laundering
                        Policy as adopted by
                        <%=mapResult.get("appname")%>.
                      </p></li>
                    <li><strong class="number">4.</strong>
                      <p>Business Associate has to escalate all suspicious
                        activities/transactions in respect of money laundering
                        regardless of the amount involved or the nature of the
                        offence as per the applicable procedures. Failure to report
                        suspicious transactions despite having knowledge is an
                        offence.</p></li>
                    <li><strong class="number">5.</strong>
                      <p>Business Associate shall not provide assistance to any
                        person to launder proceeds of any criminal conduct.
                        Prejudice an investigation by informing (i.e., tipping off)
                        the person who is the subject of a suspicious transaction.</p></li>
                    <li><strong class="number">F.</strong>
                      <p>
                        <span class="underline">GENERAL</span>
                      </p></li>
                    <li><strong class="number">1.</strong>
                      <p>
                        Business Associate shall provide full support and
                        cooperation to
                        <%=mapResult.get("appname")%>, and furnish all the information, papers
                        and documentation promptly in connection with any enquiry,
                        investigation or audit done by
                        <%=mapResult.get("appname")%>
                        or any external agency or regulatory or governmental
                        authority in connection with provisioning of the Services.
                      </p></li>
                    <li><strong class="number">2.</strong>
                      <p>
                        Business Associate shall insure that the Customers visiting
                        the Business Associate Point Location for the purpose of the
                        availing the Services are intimated and educated about the
                        full details of the Services, the terms and conditions
                        governing the Services, Customer grievance redressal
                        mechanism, forfeiture policy and/or any other details as
                        required by BANK and/or
                        <%=mapResult.get("appname")%>
                        from time to time.


                      </p></li>
                    <li><strong class="number">3.</strong>
                      <p>
                        Business Associate shall always ensure to intimate the
                        Customers that the Services are being provided under the
                        brand <strong><%=mapResult.get("appname")%></strong> which is owned and
                        powered by
                        <%=mapResult.get("appname")%>.
                      </p></li>
                    <li><strong class="number">4.</strong>
                      <p>Business Associate agrees that all the terms and
                        conditions mentioned herein are all material to this
                        agreement and agrees to comply therewith.</p></li>
                    <li><strong class="number">5.</strong>
                      <p>The obligations herein shall apply jointly and
                        severally to the Business Associate and his permitted
                        assigns.</p></li>
                    <li><strong class="number">6.</strong>
                      <p>The provisions of these Terms shall, as far as
                        permitted by law, be binding upon the parties, executors,
                        trustees, curators, legatees, heirs and other successors in
                        title.</p></li>
                    <li><strong class="number">7.</strong>
                      <p>
                        Business Associate shall not cede or assign any of its
                        rights or obligations under the Terms without the prior
                        written consent of
                        <%=mapResult.get("appname")%>.
                      </p></li>

                  </ul></li>
              </ul></li>

            <li><strong class="number">4</strong> <strong class="heading">SYSTEM
                REQUIREMENTS</strong>
              <ul>
                <li><strong class="number">4.1</strong>
                  <p><%=mapResult.get("appname")%>
                    may levy software charges for the software if any supplied to
                    You by
                    <%=mapResult.get("appname")%>, for enabling the provisioning of the
                    Services.
                  </p></li>
                <li><strong class="number">4.2</strong>
                  <p>In order to access the Terminal and provide the
                    Services, You are required to have an internet compatible and
                    internet enabled desktop/laptop/mobile device.</p></li>
                <li><strong class="number">4.3</strong>
                  <p>It is Your responsibility to ensure Your mobile
                    device/desktop/laptop meets all the necessary technical
                    specifications to enable You to access the Terminal and
                    provide the Services.</p></li>
                <li><strong class="number">4.4</strong>
                  <p>It is Your responsibility to ensure that the operating
                    system, antivirus software any other software is licensed and
                    original.</p></li>
                <li><strong class="number">4.5</strong>
                  <p>You shall maintain secrecy of Transactions according to
                    Fair Practice Code of Indian Bank Association (IBA).</p></li>
                <li><strong class="number">4.6</strong>
                  <p>
                    You shall issue receipts of Transactions to Customers
                    generated through system and no manual receipt will be valid.
                    Responsibility of issuing receipt shall be of You and
                    non-compliance will result in termination of Your engagement
                    with
                    <%=mapResult.get("appname")%>
                    as Business Associate.
                  </p></li>
              </ul></li>
            <li><strong class="number">5</strong> <strong class="heading">CHARGES
                AND PAYMENTS </strong>
              <ul>
                <li><strong class="number">5.1</strong>
                  <p>
                    Once a Transaction is executed by You and the Services are
                    provided, all risks pertaining to that Services shall lie with
                    You.
                    <%=mapResult.get("appname")%>
                    shall be entitled to charge You any cash handling fee and/or
                    other banking fees and/or other relevant administration fees
                    that may be incurred by
                    <%=mapResult.get("appname")%>
                    as a result of You depositing funds into
                    <%=mapResult.get("appname")%>'s account and/or for any debit instructions
                    that the Bank may charge.
                  </p></li>
                <li><strong class="number">5.2</strong>
                  <p>
                    You shall be paid fees and commission by
                    <%=mapResult.get("appname")%>
                    as announced and communicated through the Terminal or email or
                    SMS or any other mode from time to time or as amended from
                    time to time and the same will be subject to TDS and other
                    taxes as per the laws applicable.
                  </p></li>
                <li><strong class="number">5.3</strong>
                  <p><%=mapResult.get("appname")%>
                    may charge fees from You like software or Terminal
                    up-gradation fee or any other fee to recover the cost of
                    up-gradation/installation of software/Terminal/any other
                    equipment.
                  </p></li>

              </ul></li>
            <li><strong class="number">6</strong> <strong class="heading">TERMINATION
                AND PENALTY</strong>
              <ul>
                <li><strong class="number">6.1</strong>
                  <p>
                    Notwithstanding any remedies that may be available under any
                    Applicable Law,
                    <%=mapResult.get("appname")%>
                    may at its sole discretion temporarily or permanently deny,
                    limit, suspend, or terminate the Services provided to You
                    (without any notice), if
                    <%=mapResult.get("appname")%>
                    believes that: (a) You have abused Your rights to use the
                    Services; or (b) You are in breach of Terms or Privacy Policy;
                    or, (c) You have performed any act or omission that violates
                    any Applicable Law, rules, or regulations, or which is
                    fraudulent, illegal, unlawful or undesirable; or, (d) You have
                    performed any act or omission which is harmful or likely to be
                    harmful to
                    <%=mapResult.get("appname")%>, or any other third party, including other
                    users or suppliers of
                    <%=mapResult.get("appname")%>; or, (e) You made use of the Services to
                    perform an illegal act, or for the purpose of enabling,
                    facilitating, assisting or inducing the performance of such an
                    act; or, (f) You made any confidential and/or proprietary
                    information pertaining to
                    <%=mapResult.get("appname")%>
                    and/or the Services public without the prior written consent
                    of
                    <%=mapResult.get("appname")%>. You agree that notwithstanding the
                    termination,
                    <%=mapResult.get("appname")%>
                    will continue to be entitled to use the information supplied
                    by You or collected from You during Your use of the Terminal
                    and provision of the Services by You.
                  </p></li>
                <li><strong class="number">6.2</strong>
                  <p>
                    Upon any damages occurring to
                    <%=mapResult.get("appname")%>
                    due to Business Associate's negligence in rendering of the
                    Services and/or due to Business Associate's non-compliance of
                    Applicable Laws and/or due to non-adherence to the terms and
                    conditions of the Agreement and/or due to any fraudulent,
                    illegal, unlawful or undesirable practices carried by Business
                    Associate or any of its employees,
                    <%=mapResult.get("appname")%>
                    has the right, without prejudice to its right to terminate the
                    Agreement forthwith, to impose liquidated damages which shall
                    be not less than 10,00,000/- ( Rupees Ten Lacs only) per
                    instance and Business Associate agrees to pay the same on
                    demand without any demur or contest.
                  </p></li>
                <li><strong class="number">6.3</strong>
                  <p>
                    Notwithstanding anything to the contrary in these Terms,
                    Business Associate shall be liable to pay/indemnify
                    <%=mapResult.get("appname")%>
                    for any fine or penalty imposed by any regulatory/governmental
                    authority/BANK/court in connection with the acts of commission
                    and omission of the Business Associates while providing the
                    Services.
                    <%=mapResult.get("appname")%>
                    shall be entitled at its sole discretion to setoff/adjust such
                    fine/penalty from any payments due by
                    <%=mapResult.get("appname")%>
                    to You.
                  </p></li>
                <li><strong class="number">6.4</strong>
                  <p>
                    Upon the termination of the Agreement for any cause or without
                    cause whatsoever, Business Associate shall return all
                    <%=mapResult.get("appname")%>
                    assets, inventories, services/ material, all instructions
                    books and manuals, technical catalogues and other material,
                    documents and papers etc whatsoever provided to Business
                    Associate by
                    <%=mapResult.get("appname")%>, or as directed by
                    <%=mapResult.get("appname")%>. Business Associate shall be responsible for
                    safekeeping and returning
                    <%=mapResult.get("appname")%>'s aforesaid assets, inventories etc. in good
                    condition till the date of termination of the Agreement.
                  </p></li>
              </ul></li>

            <li class="upperCaseLi"><strong class="number">7</strong> <strong
              class="heading">LIMITATION OF LIABILITY</strong>


              <p><%=mapResult.get("appname")%>, INCLUDING ITS OFFICERS, DIRECTORS,
                SHAREHOLDERS, EMPLOYEES, SUB-CONTRACTORS, BUSINESS ASSOCIATES,
                PARENT COMPANIES, SISTER COMPANIES, SUBSIDIARIES AND OTHER
                AFFILIATES WILL NOT BE LIABLE TO THE MAXIMUM EXTENT PERMITTED BY
                THE APPLICABLE LAW, FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
                CONSEQUENTIAL DAMAGE, OR ANY OTHER DAMAGE AND LOSS (INCLUDING
                LOSS OF PROFIT, LOSS OF DATA AND WORK STOPPAGE), COSTS, EXPENSES
                AND PAYMENTS, REGARDLESS OF THE ALLEGED LIABILITY OR FORM OF
                ACTION, WHETHER IN CONTRACT, TORT OR OTHERWISE, INCLUDING
                NEGLIGENCE, INTELLECTUAL PROPERTY INFRINGEMENT, PRODUCT
                LIABILITY AND STRICT LIABILITY, THAT MAY RESULT FROM, OR IN
                CONNECTION WITH THE USE OF TERMINAL OR THE INABILITY TO ACCESS
                THE TERMINAL AND PROVISION OF THE SERVICES, OR FROM ANY FAILURE,
                ERROR, OR DOWNTIME IN THE FUNCTION OF THE SERVICES, OR FROM ANY
                FAULT OR ERROR MADE BY
                <%=mapResult.get("appname")%>'S STAFF, OR FROM YOUR RELIANCE ON CONTENT
                DELIVERED THROUGH THE SERVICES, OR FROM THE NATURE OF CONTENT
                DELIVERED THROUGH THE SERVICES, OR FROM ANY COMMUNICATION WITH
                <%=mapResult.get("appname")%>
                OR FROM ANY DENIAL OR CANCELLATION OF REQUEST FOR INFORMATION
                THROUGH THE SERVICES, OR FROM RETENTION, DELETION, DISCLOSURE OR
                ANY OTHER USE OR LOSS OF CONTENT THROUGH THE SERVICES,
                REGARDLESS OF WHETHER
                <%=mapResult.get("appname")%>
                HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN ANY
                EVENT, YOUR SOLE REMEDY WILL BE LIMITED TO THE CORRECTIONS OF
                SUCH ERRORS, AS DEEMED FIT BY
                <%=mapResult.get("appname")%>
                IN ITS SOLE DISCRETION. WITHOUT PREJUDICE TO THE AFORESAID, IT
                IS HEREBY ACKNOWLEDGED BY YOU THAT THE AGGREGATE LIABILITY OF
                <%=mapResult.get("appname")%>, FOR ANY REASONS WHATSOEVER, WILL NOT EXCEED
                RS. 100/- (RUPEES ONE HUNDRED ONLY) OR THE TOTAL COST PAID BY
                THE CUSTOMER UNDER THE TRANSACTION IN DISPUTE, WHICHEVER IS
                LOWER.
              </p></li>


            <li class="upperCaseLi"><strong class="number">8</strong> <strong
              class="heading">DISCLAIMER OF WARRANTIES</strong>
              <ul>
                <li><strong class="number">8.1</strong>
                  <p>
                    EXCEPT AS EXPRESSLY SET FORTH IN THIS TERMS,
                    <%=mapResult.get("appname")%>
                    EXPRESSLY DISCLAIMS ANY OTHER WARRANTY WITH RESPECT TO THE USE
                    OF THE SERVICES OR ANY CONTENT OR INFORMATION DELIVERED OR
                    SENT THROUGH THE SERVICES TO YOU. THE SERVICES ARE PROVIDED
                    WITHOUT ANY EXPRESS OR IMPLIED GUARANTEE OR ASSURANCE OF
                    QUALITY, RELIABILITY OF THE CONTENT DELIVERED THROUGH THE
                    SERVICES.
                    <%=mapResult.get("appname")%>
                    DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES WITH REGARD TO
                    THE SERVICES.
                    <%=mapResult.get("appname")%>
                    DOES NOT WARRANT OR GUARANTEE THAT THE USE OF THE SERVICES
                    WILL NOT CAUSE ANY DAMAGES TO YOUR DEVICE OR SYSTEM OR TO ANY
                    OTHER SERVICES PROVIDED TO YOUR DEVICE OR APPLICATIONS AND
                    CONTENT THAT RESIDE ON YOUR DEVICE. YOU AGREE AND ACKNOWLEDGE
                    THAT THE USE OF THE TERMINAL, AND PROVIDING THE SERVICES IS
                    ENTIRELY, OR AT THE MAXIMUM PERMITTED BY THE APPLICABLE LAW,
                    AT YOUR OWN RISK.
                  </p></li>
                <li><strong class="number">8.2</strong>
                  <p><%=mapResult.get("appname")%>
                    DOES NOT WARRANT OR GUARANTEE THAT THE SERVICES WILL OPERATE
                    IN AN UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE MANNER, OR
                    THAT THE SERVICES WILL ALWAYS BE AVAILABLE OR FREE FROM ERRORS
                    OR THAT THE SERVICES WILL BE IMMUNE FROM UNAUTHORIZED ACCESS.
                  </p></li>
                <li><strong class="number">8.3</strong>
                  <p><%=mapResult.get("appname")%>
                    DOES NOT WARRANT, GUARANTEE OR MAKE ANY REPRESENTATIONS THAT
                    YOU WILL FIND THE SERVICES SUITABLE FOR YOUR NEEDS.
                    <%=mapResult.get("appname")%>
                    DOES NOT WARRANT OR GUARANTEE THAT THE SERVICES YOU RECEIVE
                    WILL BE FREE FROM TECHNICAL INACCURACIES OR THAT THE CONTENT
                    WILL BE LEGAL, NON INFRINGING OR WILL NOT VIOLATE ANY RIGHTS
                    OR APPLICABLE LAWS OR THAT THE CONTENT WILL NOT CONTAIN ANY
                    OBJECTIONABLE MATERIALS.
                  </p></li>
                <li><strong class="number">8.4</strong>
                  <p>
                    YOU AGREE AND ACKNOWLEDGE THAT THE ROLE OF
                    <%=mapResult.get("appname")%>
                    IS LIMITED TO PROVIDING SERVICES AND THAT
                    <%=mapResult.get("appname")%>
                    DOES NOT IN ANY MANNER WARRANTY, GUARANTEE OR MAKE ANY
                    REPRESENTATIONS IN RESPECT OF THE ACCURACY AND/OR VERACITY OF
                    THE INFORMATION PROVIDED IN RESPECT OF SERVICES.
                  </p></li>
                <li><strong class="number">8.5</strong>
                  <p>THE SERVICES AND THE CONTENT THEREUNDER ARE NOT INTENDED
                    TO CONSTITUTE OR FORM THE BASIS OF ANY ADVICE (PROFESSIONAL OR
                    OTHERWISE) OR TO BE USED IN, OR IN RELATION TO, ANY DECISION
                    OR TRANSACTION. WE DO NOT ACCEPT ANY LIABILITY (REGARLESS OF
                    HOW IT MIGHT ARISE) FOR ANY CLAIM OR LOSS ARISING FROM: ANY
                    ADVICE GIVEN; ANY DECISION MADE; OR ANY TRANSATION MADE OR
                    EFFECTED. IN RELIANCE ON, OR ON THE BASIS OF THE SERVICES AND
                    THE CONTENT THEREUNDER NOR ANY SUCH LIABILITY ARISING FROM ANY
                    OTHER USE OF, OR RELIANCE ON, THE SERVICES AND THE CONTENT
                    THEREUNDER.</p></li>
                <li><strong class="number">8.6</strong>
                  <p><%=mapResult.get("appname")%>
                    CANNOT AND DOES NOT GUARANTEE THAT THE SERVICES AND ANY
                    CONTENT THEREUNDER WILL BE FREE FROM VIRUSES AND/OR OTHER CODE
                    THAT MAY HAVE CONTAMINATING OR DESTRUCTIVE ELEMENTS. IT IS
                    YOUR RESPONSIBILITY TO IMPLEMENT APPROPRIATE SECURITY
                    SAFEGUARDS (INCLUDING ANTI-VIRUS AND OTHER SECURITY CHECKS) TO
                    SATISY YOUR PARTICULAR REQUIREMENTS AS TO THE SAFETY AND
                    RELIABILITY OF THE CONTENT.
                  </p></li>
                <li><strong class="number">8.7</strong>
                  <p><%=mapResult.get("appname")%>
                    WILL BE ENTITLED TO USE, SUBJECT TO THE PRIVACY POLICY AS
                    DEFINED ABOVE, ANY INFORMATION SUPPLIED BY YOU DURING THE
                    COURSE OF ACCESSING THE TERMINAL AND PROVIDING THE SERVICES.
                  </p></li>
              </ul></li>


            <li class="upperCaseLi"><strong class="number">9</strong> <strong
              class="heading">INDEMNIFICATION</strong>


              <p>
                You agree to indemnify and hold
                <%=mapResult.get("appname")%>, its licensors, business partners of
                <%=mapResult.get("appname")%>
                and their respective officers, directors, shareholders,
                employees, sub-contractors, merchants, parent companies, sister
                companies, subsidiaries and other affiliates, indemnified and
                harmless from any claim, liabilities, damages, costs, losses,
                demands, expenses, charges and penalties, including reasonable
                attorneys' fees, made by any third party in connection with or
                arising out of, provisioning of the Services, or any act of
                commission or omission by You in adhering to these Terms or any
                breach of Applicable Laws or any fraudulent, illegal, unlawful
                or undesirable practices or negligence or mischief on Your part
                or any infringement of intellectual property rights of any third
                party or any unauthorised use of the Services by You.
              </p></li>
            <li class="upperCaseLi"><strong class="number">10</strong> <strong
              class="heading">CONFIDENTIALITY</strong>


              <p>
                You undertake that You shall treat as confidential all
                Confidential Information of
                <%=mapResult.get("appname")%>
                and shall not disclose such Confidential Information to any
                third party without the written consent of
                <%=mapResult.get("appname")%>. "Confidential Information" herein shall mean
                any technical, business, or proprietary information disclosed by
                <%=mapResult.get("appname")%>
                to You or which may come into Your knowledge or possession,
                directly or indirectly, including, but not limited to,
                information regarding business strategies and practices,
                methodologies, trade secrets, know-how, pricing, technology, and
                software. Further,
                <%=mapResult.get("appname")%>
                proprietary technology and software products, and the pricing
                and these Terms are Confidential Information of
                <%=mapResult.get("appname")%>.
              </p></li>
            <li class="upperCaseLi"><strong class="number">11</strong> <strong
              class="heading">AMENDMENTS TO THE TERMS</strong>


              <p><%=mapResult.get("appname")%>
                may amend and modify Terms from time to time, including any and
                all documents and policies incorporated thereto without any
                prior notice to You. You agree to be bound by any of the changes
                made in the Terms, including changes to any and all documents
                and policies incorporated thereto. Continuing to access the
                Terminal and to provide the Services will indicate Your
                acceptance of the amended Terms. If You do not agree with any of
                the amended Terms, then You must avoid any further use of the
                Terminal and provision of the Services.
                <%=mapResult.get("appname")%>
                advises You to periodically read these Terms, as it may change
                from time to time.
              </p></li>
            <li class="upperCaseLi"><strong class="number">12</strong> <strong
              class="heading">ENTIRE AGREEMENT</strong></li>

            <li class="upperCaseLi"><strong class="number">13</strong> <strong
              class="heading">The "Terms" including the Privacy Policy
                constitutes the entire agreement between You and <%=mapResult.get("appname")%>
                and governs Your access of the Terminal and providing the
                Services, superseding any prior agreements between You and <%=mapResult.get("appname")%>
                with respect to the Services. GOVERNING LAW AND JURISDICTION
            </strong>
              <ul>
                <li><strong class="number">13.1</strong>
                  <p>
                    These Terms shall be governed by the laws of India. You and
                    <%=mapResult.get("appname")%>
                    agree to submit to the personal and exclusive jurisdiction of
                    the courts of Noida. In case of any dispute or other matter
                    arising in reference to the Terms and/or the Services, such
                    dispute or other matter shall be referred to a sole arbitrator
                    appointed by
                    <%=mapResult.get("appname")%>
                    and shall be governed by the Arbitration and Conciliation Act,
                    1996, amended from time to time. The venue for arbitration
                    shall be Noida, and shall be conducted in English language.
                    All the costs, charges and expenses in connection to the
                    arbitration shall be solely borne by the user who raised the
                    dispute.
                  </p></li>
                <li><strong class="number">13.2</strong>
                  <p>Subject to the provisions of Clause 13.1 above, the
                    courts having jurisdiction under the provisions of the
                    Arbitration and Conciliation Act, 1996, to determine all the
                    matters which the court is entitled to determine under the
                    Act, including, without limitation, provision of interim
                    reliefs under the provisions of section 9 of the Arbitration
                    and Conciliation Act, 1996, shall exclusively be the courts at
                    Noida, India.</p></li>
              </ul></li>

            <li class="upperCaseLi"><strong class="number">14</strong> <strong
              class="heading">WAIVER AND SEVERABILITY OF TERMS</strong>


              <p>
                The failure of
                <%=mapResult.get("appname")%>
                to exercise or enforce any right or provision of the Terms shall
                not constitute a waiver of such right or provision. If any
                provision of the Terms is found by a court of competent
                jurisdiction to be invalid, the parties nevertheless agree that
                the court should endeavour to give effect to the parties'
                intentions as reflected in the provision, and the other
                provisions of the Terms remain in full force and effect.
              </p></li>

            <li class="upperCaseLi"><strong class="number">15</strong> <strong
              class="heading">SURVIVAL</strong>


              <p>You agree and confirm that the indemnities,
                confidentiality obligations, limitation of liability, disclaimer
                of warranties, dispute resolution mechanism, shall survive the
                efflux of time (Business Associate Signature) (Employee
                Signature) ************************* **********************
                Date: ____________________ Date: _________________</p></li>


          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>



  <!-- Show the full image here -->
	<div class="modal fade print-enable" id="myModal" role="dialog" tabindex='-1'>
  	<div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
      		<div class="modal-header">
      	   <button type="button" class="close" data-dismiss="modal">&times;</button> 
      	  </div> 
    		</div>
  	</div>
	</div>