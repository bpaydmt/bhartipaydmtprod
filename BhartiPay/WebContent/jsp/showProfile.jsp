<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript"> <%@ include  file="../js/common.js"%></script>
<script type="text/javascript"> <%@ include  file="../js/user/validationFunctions.js"%></script>
<script type="text/javascript"> <%@ include  file="../js/user/create.js"%></script>
</head>
<!DOCTYPE html>
<jsp:include page="theams.jsp"></jsp:include> 
<head>
   
   <jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

	  <script type="text/javascript">
	    $(function() {
	    	//alert(11)
	      $(".js__p_start, .js__p_another_start").simplePopup();
	    });
	    
	</script>

<style>

	.col-md-3.widget.widget1 {
	    width: 31.8%;
	}
	 .widget1 {
	    margin-right: 4px;
		margin-left:6px;
	}
	.grow p {
	    color: #fff;
	    margin: 0;
	    font-size: 12px;
	    text-transform: uppercase;
	}
	.map_container, .bs-example5, .panel_2, .content-box-wrapper, .grid_1, .grid_3, .no-padding, .bs-example4, .panel-body1, .editor, .banner-bottom-video-grid-left, #chart, .online, .widget_middle_left_slider, .grd_tble, .span_6, .col_2, .weather_list, .cal1, .skills-info, .stats-info, .r3_counter_box, .activity_box, .bs-example1, .cloud, .copy {
	    -ms-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
	    -o-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
	    -moz-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
	    -webkit-box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
	    box-shadow: 0 1px 3px 0px rgba(0, 0, 0, 0.2);
	}
	.grow1, .grow3, .grow2 {
	    left: 36%;
	}
	.grow1 {
	    background: #00BCD4;
	}
	.r3_counter_box .fa-users {
	    color: #00BCD4;
	}
	.grow {
	    position: absolute;
	    top: 44%;
	    left: 14%;
	    background: #EA8278;
	    width: 150px;
	    height: 22px;
	    padding: .2em 0 0;
	}
	.r3_counter_box {
	    min-height: 100px;
	    background: #ffffff;
	    text-align: center;
	    position: relative;
	}
	.r3_counter_box .fa-mail-forward {
	    color: #8BC34A;
	}
	.r3_counter_box .fa {
	    margin-right: 0px;
	    width: 66px;
	    height: 65px;
	    text-align: center;
	    line-height: 60px;
	    font-size: 2.5em;
	}
	.r3_counter_box h5 {
	    margin: 10px 0;
	    color: #575757;
	    font-size: 1.5em;
	}
	.stats span {
	    color: #999;
	    font-size: 14px;
	}

	.r3_counter_box .fa-eye {
	    color: #F44336;
	}
	.r3_counter_box .fa-usd {
	    color: #FFCA28;
	}
	.stats {
	    overflow: hidden;
	    border-top: 1px solid #DDD;
	    background: #f9f9f9;
	    padding: 0.5em;
	}
</style>

</head>
<%
		User user = (User) session.getAttribute("User");
%>
<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
<div id="main" role="main"> 
  <div id="content">         
            

<div class=" row"> 
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header">
                <h2><i class="glyphicon "></i>Profile</h2>

                
            </div>
            <div class="box-content">                    
		<form action="EditProfile" id="form_name" method="post" name="PG">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
									<font color="red"><s:fielderror></s:fielderror></font>
             					 <!-- <div class="col-md-12"> -->
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">Mobile Number</label>
															<% 
															   String userStatus=(String)request.getAttribute("statusflag");
															if(userStatus==null||!userStatus.equalsIgnoreCase("True")){
																if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype() != 7){
															
															%>
															 <a href="javascript:void(0)" id="newPhoneModelLink" class="js__p_start edit-link">Edit Mobile</a>
															<%}} %>
															<input	class="form-control" name="walletBean.id" type="hidden" value="<s:property value='%{walletBean.id}'/>" />
															 <input	class="form-control " name="walletBean.mobileno" type="text"
																id="cur-mob" maxlength="20" disabled="disabled" placeholder="Name" value="<s:property value='%{walletBean.mobileno}'/>" />

														 
													</div>
													
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">Email </label>
															<%
														/* 	if(userEdit==null||!userEdit.equals("userEdit")){ */
															if(userStatus==null||!userStatus.equalsIgnoreCase("True")){
																if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype() != 7){
															
															%>
															 <a href="javascript:void(0)" id="newMailModelLink" class="js__p_another_start edit-link">Edit Email</a>
															<%
																}}
															%>
															 <input	class="form-control" name="walletBean.emailid" type="text"
																id="cur-email" maxlength="20"  disabled="disabled" placeholder="Email" value="<s:property value='%{walletBean.emailid}'/>" />

													 
													</div> 
									
									<!-- </div> -->
									
									
									
									
									<!--  <div class="col-md-12"> -->

                                               <%--  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">KYC Status</label>
															 <input	class="form-control" name="walletBean.kycstatus" type="text"
																id="kyc" maxlength="20" placeholder="KYC Status" disabled="disabled" value="<s:property value='%{walletBean.kycstatus}'/>" />

														 
													</div> --%>

													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">Name<font color='red'> *</font></label>
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<input	class="form-control userName mandatory" name="walletBean.name" type="text"
																id="name" placeholder="Name"  value="<s:property value='%{walletBean.name}'/>" />
															<%}else{ %>
															 <input	class="form-control userName mandatory" name="walletBean.name" type="text"
																id="name" placeholder="Name" disabled="disabled" value="<s:property value='%{walletBean.name}'/>" />
																<%} %>
													 
													</div> 
									<!-- </div> -->
									
									
									
												 <!-- <div class="col-md-12"> -->
                                                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">Address 1</label>
															 
															 	<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<input	class="form-control address-vl" name="walletBean.address1" type="text"
																id="address1" maxlength="100" placeholder="Address 1" value="<s:property value='%{walletBean.address1}'/>" />

															<%}else{ %>
															 <input	class="form-control address-vl" name="walletBean.address1" type="text"
																id="address1" maxlength="100" placeholder="Address 1" disabled="disabled" value="<s:property value='%{walletBean.address1}'/>" />

																<%} %>
															 
															 
															 
														 
													</div>
													
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">Address 2 </label>
															
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<input	class="form-control address-vl" name="walletBean.address2" type="text"
																id="address2" maxlength="100" placeholder="Address 2" value="<s:property value='%{walletBean.address2}'/>" />
															<%}else{ %>
															<input	class="form-control address-vl" name="walletBean.address2" type="text"
																id="address2" maxlength="100" placeholder="Address 2" disabled="disabled" value="<s:property value='%{walletBean.address2}'/>" />
																<%} %>
															
															
															 

													 
													</div> 

									<!-- </div> -->
									
									
									
			<!-- 	<div class="col-md-12"> -->

													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">City</label>
															
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<input	class="form-control userName" name="walletBean.city" type="text"
																id="city" maxlength="20" placeholder="City" value="<s:property value='%{walletBean.city}'/>" />
																<%}else{ %>
															<input	class="form-control userName" name="walletBean.city" type="text"
																id="city" maxlength="20" placeholder="City" disabled="disabled" value="<s:property value='%{walletBean.city}'/>" />	
																<%} %>
															
															
															 

													 
													</div>
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">State</label>
															
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<input	class="form-control userName" name="walletBean.state" type="text"
																id="state" maxlength="20" placeholder="State" value="<s:property value='%{walletBean.state}'/>" />
																<%}else{ %>
															<input	class="form-control userName" name="walletBean.state" type="text"
																id="state" maxlength="20" placeholder="State" disabled="disabled" value="<s:property value='%{walletBean.state}'/>" />	
																<%} %>
															
															 

														 
													</div>
													
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<label for="apptxt">PIN Code</label>
															
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															 <input	class="form-control pincode" name="walletBean.pin" type="text"
																id="pincode" maxlength="20" placeholder="PIN Code" value="<s:property value='%{walletBean.pin}'/>" />
																<%}else{ %>
															 <input	class="form-control pincode" name="walletBean.pin" type="text"
																id="pincode" maxlength="20" placeholder="PIN Code" disabled="disabled" value="<s:property value='%{walletBean.pin}'/>" />	
																<%} %>
															
															
															
															

													 
													</div>

													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">PAN Card</label>
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															 <input	class="form-control pancard-vl" name="walletBean.pan" type="text"
																id="pancard" maxlength="20" placeholder="PAN Card" value="<s:property value='%{walletBean.pan}'/>" />
															<%}else{ %>
															 <input	class="form-control pancard-vl" name="walletBean.pan" type="text"
																id="pancard" maxlength="20" placeholder="PAN Card" disabled="disabled" value="<s:property value='%{walletBean.pan}'/>" />
	
																<%} %> 
													</div>
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
													 
															<!-- <label for="apptxt">Aadhaar Card</label> -->
															
															<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>
															<label for="apptxt">Aadhaar Card</label>
															 <input	class="form-control aadhar-vl" name="walletBean.adhar" type="text"
																id="aadhar" maxlength="20" placeholder="Aadhaar Card" value="<s:property value='%{walletBean.adhar}'/>" />
															<%}else{ %>
															<label for="apptxt"><s:property value='%{walletBean.getAddressprooftype}'/></label>
															<%-- <label for="apptxt"><%=walletBean.getAddressprooftype()%></label> --%>
															
															 <input	class="form-control aadhar-vl" name="walletBean.adhar" type="text"
																id="aadhar" maxlength="20" placeholder="Aadhaar Card" disabled="disabled" value="<s:property value='%{walletBean.adhar}'/>" />
																<%} %> 
													</div>
													
													
										<%if(user.getUsertype() != 2&& user.getUsertype() != 3&& user.getUsertype() != 5&&user.getUsertype()!=7){		
															%>		
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
									                    
									                        <%
						   
									                         if(userStatus!=null&&userStatus.equalsIgnoreCase("True")){
								                          	%>
															<label for="apptxt">User Status</label>
															<input type="hidden" name="statusflag" value="true">
															<s:select list="%{userStatusList}"
																	name="walletBean.userstatus">
																</s:select>
														<%}else{ %>
														<input type="hidden" name="walletBean.userstatus" value="<s:property value='%{walletBean.userstatus}'/>">
														<%} %>
														 
									</div>


									<!-- </div> -->
									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									
									<!-- <div class="col-md-6 col-sm-6 col-xs-12 form-group"> -->
								  
										
										<input type="button" class="btn btn-success submit-form"
																style="margin-top: 20px;"  value="Submit" onclick="submitVaForm('#form_name')" />
										
<button type="reset" class="btn btn-info btn-fill" onclick="resetForm('#form_name')" 		style="margin-top: 20px;">Reset</button>
									
									 
									<!-- </div> -->
									</div>
									
									<%
						     
									if(userStatus==null||!userStatus.equalsIgnoreCase("True")){
									%>
									
									<div class="col-md-12">
											<div class="col-md-6">
												<a onclick="document.getElementById('pic').style.display='block'">Save/Update profile pic</a>
											</div>
										</div>
											
									
									<%} %>
							<%} %>		
									</div>
									</form>

                
                

    
            </div>
        </div>
  
</div>









<%if(user!=null&&user.getUsertype()==2){ %>

<div class="box col-md-12" style="margin-top: 0px;">
  <div class="box-inner">
    <div class="box-header">
        <h2><i class="glyphicon "></i>Parent User Detail</h2> 
    </div>
    <div class="box-content">                    
      <form action="EditProfile" id="form_name" method="post" name="PG" autocomplete="off"> 
        <div class="row"> 
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
            <label for="apptxt">Name<font color="red"> *</font></label> 
            <input class="form-control userName mandatory" name="#" type="text" id="#" placeholder="Name" disabled="disabled" value="<s:property value='%{parentWalletBean.name}'/>" maxlength="50"><div class="errorMsg"></div>
          </div> 
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
            <label for="apptxt">Mobile Number</label> 
            <input class="form-control" name="walletBean.id" type="hidden" value="OXMA001002">
            <input class="form-control " name="#" type="text" id="#" maxlength="20" disabled="disabled" placeholder="Name" value="<s:property value='%{parentWalletBean.mobileno}'/>"><div class="errorMsg"></div>
          </div>
            
            
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group"> 
            <label for="apptxt">Email</label> 
            <input class="form-control" name="#" type="text" id="#" maxlength="20" disabled="disabled" placeholder="Email" value="<s:property value='%{parentWalletBean.emailid}'/>"><div class="errorMsg"></div>
          </div> 

        </div> 
      </form> 
    </div>
  </div> 
</div>
<%} %>

<div class="box col-md-12" style="margin-top: 0px;">
  <div class="box-inner">
    <div class="box-header">
        <h2><i class="glyphicon "></i>Account Detail</h2> 
    </div>
    <div class="box-content">                    
      <form action="EditProfile" id="form_name" method="post" name="PG" autocomplete="off"> 
        <div class="row"> 
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
            <label for="apptxt">Bank Name<font color="red"> *</font></label> 
            <input class="form-control userName mandatory" name="walletBean.bankName" type="text" id="bankname" placeholder="Name" disabled="disabled" value="<s:property value='%{walletBean.bankName}'/>" maxlength="50"><div class="errorMsg"></div>
          </div> 
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
            <label for="apptxt">Account Number</label> 
            <input class="form-control " name="walletBean.accountNumber" type="text" id="accountnumber" maxlength="20" disabled="disabled" placeholder="Name" value="<s:property value='%{walletBean.accountNumber}'/>"><div class="errorMsg"></div>
          </div>
            
            
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group"> 
            <label for="apptxt">IFSC Code  </label> 
            <input class="form-control" name="walletBean.ifscCode" type="text" id="ifsccode" maxlength="20" disabled="disabled" placeholder="Email" value="<s:property value='%{walletBean.ifscCode}'/>"><div class="errorMsg"></div>
          </div> 

        </div> 
      </form> 
    </div>
  </div> 
</div>



<div id="pic" style="display: none">

<div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Save/Update profile picture.</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="SaveProfilePic" id="form_name1" method="post" enctype="multipart/form-data">
		


            
		
									<div class="row">
									
									
									
									
						
				<div class="col-md-12">
				
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<div class="col-md-6">
														
															<label for="apptxt">Select profile picture</label>
															 <input	class="form-control" name="upload.myFile1" type="file"/>

														
													</div>
													
													<div class="col-md-12">	<div class="alert alert-warning">
   
    <strong>Note:</strong>  Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.
</div></div>
												
													
													<div class="col-md-6">
									
										
										<button  class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button> 
										

									
								
									</div>


									</div>
									
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>
</div>

 

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<!-- <script src="js/bootstrap.min.js"></script> -->

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="./newmis/js/charisma.js"></script>


  <div class="p_body js__p_body js__fadeout"></div>

       <div class="popup js__popup js__slide_top">
    <a href="#" class="p_close js__p_close" title="aaaa">
      <span></span><span></span>
    </a>
    <div class="p_content">
                                                       
                                                             <div class="row">
                                                                <div class="col-md-12" id="newMobileNo">
                                                                <div class="col-md-9">
                                                               
                                                                    <label>Enter New Moble Number</label>
                                                                    <input	class="form-control mobile"  name="walletBean.newMobileno" id="newMobileno" maxlength="10" placeholder="Mobile Number"  value="<s:property value='%{walletBean.newMobileno}'/>" />
                                                                    <div class="alert alert-danger "  id="newMobileErr"></div>

                                                             
                                                                </div>
                                                                <div class="col-md-3">
                                                                  
                                                                      <button type="button" onClick="addNewNumber()" id="newMobileBtn" class="btn form-btn btn-info btn-fill">Submit
                                                                    </button>
                                                           

                                                                     </div>
                                                                </div>

                                                                <div class="col-md-12" id="optWrapper" style="display:none">
                                                                <div class="col-md-12 alert alert-success" id="OptSuccessMsg" >OTP has been sent to your mobile number</div>
                                                                <div class="col-md-9">
                                                    
                                                          
                                                            <label>Enter OTP </label>
                                                            <input	 name="walletBean.newMobileno" type="hidden" id="newMobilenoHidden" maxlength="12" placeholder="Mobile No"  value="<s:property value='%{walletBean.newMobileno}'/>" />
                                                                   
                                                            <input	class="form-control" name="walletBean.otp" id="otp" maxlength="6" placeholder="OTP"  value="<s:property value='%{walletBean.otp}'/>" />
                                                            <a href="javascript: void(0)" onclick="resendMobileOtp()">Resend OTP</a>
                                                             <div class="alert alert-danger "  id="newotpMobileErr"></div>
                                                            
                                                             <div class="alert alert-danger" id="otpMobileErr"></div>

                                                    
                                                       
                                                       
                                                    </div>
                                                    <div class="col-md-3">
                                                      
                                                    <button type="button" id="submitBeneficiary" onClick="validateOtp()" class="btn form-btn btn-info btn-fill">Submit
                                        </button>
                                                                                
                                    </div>
                                   
                                                    </div>
                                                     <div class="" id="successsMsgMobile">
                                                        <div  class="col-md-12 alert alert-success"></div>
                                                        </div>
                                                             </div>
                                                             


                                                        </div>
  </div>
  
  
  
 
  
  <div class="popup js__another_popup js__slide_top">
    <a href="#" class="p_close js__p_close" title="aaaa">
      <span></span><span></span>
    </a>
    <div class="p_content">
                                                       
                                                             <div class="row">
                                                         
                                                                <div class="col-md-12" id="newEmailId">
                                                                
                                                                <div class="col-md-9">
                                                       
                                                                    <label>Enter New Email Id</label>
                                                                <input	class="form-control emailid" id="newEmailId1" name="walletBean.newEmailId"  maxlength="100" placeholder="Email Id"  value="<s:property value='%{walletBean.newEmailId}'/>" /> 
                                                                    <!-- <input	class="form-control" id="newEmailId1" name="walletBean.newEmailId"  maxlength="100" placeholder="Email Id"  /> -->
                                                                    <div class="alert alert-danger "  id="newEmailIdErr"></div>

                                                           
                                                                </div>
                                                                <div class="col-md-3">
                                                       
                                                                      <button type="button" onClick="addNewId()"  id="emailBtn" class="btn form-btn btn-info btn-fill">Submit
                                                                    </button>
                                                        

                                                                     </div>
                                                                </div>

                                                                <div class="col-md-12" id="optEmailWrapper" style="display:none">
                                                                <div class="col-md-12 alert alert-success" id="successsMsg" >OTP has been sent to your new email id.</div>
                                                                <div class="col-md-9">
                                                
                                                            <label>Enter OTP </label>
                                                            <input	 name="walletBean.newEmailId" type="hidden" id="newEmailIdHidden" maxlength="12" placeholder="Email Id"  value="<s:property value='%{walletBean.newEmailId}'/>" />
                                                                   
                                                            <input	class="form-control" name="walletBean.otp" id="otpemail" maxlength="6" placeholder="OTP"  value="<s:property value='%{walletBean.otp}'/>" />
                                                            <a href="javascript: void(0)" onclick="resendEmailOtp()">Resend OTP</a> <span class="alert alert-danger " id="emailOtpErr"></span>
 																<div class="alert alert-danger "  id="newotpEmailErr"></div>
                                                    
                                                    </div>
                                                    <div class="col-md-3">
                                                  
                                                    <button type="button" id="submitEmail" onClick="validateEmailOtp()" class="btn form-btn btn-info btn-fill">Submit
                                        </button>
                                                                            
                                    </div>
                                    
                                                    </div>
                                                    <div class="" id="successsMsgEmailID">
                                                        <div  class="col-md-12 alert alert-success"></div>
                                                        </div>
                                                             </div>


    </div>
  </div>

</body>

<script type="text/javascript"> 
	function addNewNumber(){
		var inp = $("#newMobileno").val();
		var oldInp =   $("#cur-mob").val();
		if(inp == oldInp ){
			$("#newMobileErr").show().text("Current mobile number can't be used.");
		}else{
		if(inp.length != 0 || inp.length == 10){
		 $.post( "ValidateChangeMobile", { newMobileno:inp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
		    
		    if(type == "Success"){
		    	$("#newMobilenoHidden").val(inp)
		    	$("#newMobileNo").hide();
		    	$("#optWrapper").fadeIn();
		    	$("#OptSuccessMsg").text(msg).fadeIn();
		    	
		    }else{
		    	$("#newMobileErr").text(msg).show();
		    	
		    }
		  });
		}
		else{
			$("#newMobileErr").show().text("Not Valid Mobile Number.")
		}
		}
	}
		
		
	function addNewId(){
		setTimeout(function(){
		var inp = $("#cur-email").val();
		var newInp =   $("#newEmailId1").val();
		if(inp == newInp ){
			$("#newEmailIdErr").show().text("Current email id can't be used.");
		}else{
		
		if(newInp.length == 0 || $("#newEmailId1").hasClass("invalid") ){
			
				$("#newEmailIdErr").show().text("Please enter a valid email address.");
		}else{
			
		 $.post( "ValidateChangeEmail", { newEmailId:newInp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
				    if(type == "Success"){
		    	$("#newEmailIdHidden").val(newInp)
		    	$("#newEmailId").hide();
		    	$("#optEmailWrapper, #successsMsg").fadeIn();
		    
		    	
		    }else{
		    	$("#newEmailIdErr").text(msg).show();
		    	
		    }
		  });
		}
		}
		},100);
	}



	/* otp validate */
	 
	function validateOtp(){
		var inp = $("#newMobilenoHidden").val();
		var otp = $("#otp").val();
		if(otp.length != 0 || otp.length == 6){
		 $.post( "ChangeMobileNo", { newMobileno:inp,otp:otp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
		    if(type == "Success"){
		    	//$("#newMobileNo").hide();
		    	$("#optWrapper").hide();	    	
		    	$("#successsMsgMobile, .alert").fadeIn().find(".alert").text(msg);	    	
		    	$("#newotpMobileErr").hide();
		    	
		    }else{
		    	$("#newotpMobileErr").text(msg).show();
		    	$("#OptSuccessMsg").hide();
		    	
		    }
		  });
		}else{
			$("#newotpMobileErr").show().text("Not Valid OTP.")
		}
		}
		
		
	function validateEmailOtp(){
		var inp = $("#newEmailIdHidden").val();
		var otp = $("#otpemail").val();
		if(otp.length != 0 || otp.length == 6){
		 $.post( "ChangeEmailId", { newEmailId:inp,otp:otp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
			 //alert(msg);
			 if(type == "Success"){
		    	$("#newEmailId").hide();
		    	$("#optEmailWrapper").hide();
		    	
		    	$("#successsMsgEmailID, .alert").fadeIn().find(".alert").text(msg);
		    	$("#newotpEmailErr").hide();
		    }else{
		    	$("#newotpEmailErr").text(msg).show();
		    	$("#successsMsg").hide()
		    }
		  });
		}else{
			$("#newotpEmailErr").show().text("Not Valid OTP.")
		}
		}
		
		
	/* RESEND */	

		
	function resendMobileOtp(){
		var inp = $("#newMobilenoHidden").val();
		$("#newotpMobileErr").hide();
		 $.post( "ChangeOtpResend", { newEmailMobile:inp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
			 if(type == "Success"){
		    	$("#OptSuccessMsg").fadeIn().text(msg);
		     }else{
		    	$("#otpMobileErr").text(msg).show();
		    	
		    }
		  });

		}
		
		
	function resendEmailOtp(){
		var inp = $("#newEmailIdHidden").val();
		$("#newotpEmailErr").hide();
		 $.post( "ChangeOtpResend", { newEmailMobile:inp})
		  .done(function( data ) {
			 var type=data.type;
			 var msg=data.msg;
			
		    if(type == "Success"){
		    	//$("#optEmailWrapper").hide();
		    	$("#successsMsg").fadeIn().text(msg);
		    	
		    }else{
		    	$("#newEmailIdErr").text(msg).show();
		    	
		    }
		  });

		}


	$("#newMobileno").keyup(function(event){
		//alert();
			if(event.keyCode == 13){
			
				$("#newMobileBtn").click();
		    }
		});

	$("#otp").keyup(function(event){
		
			if(event.keyCode == 13){
				$("#submitBeneficiary").click();
		    }
		});	
		
		
	$("#newEmailId1").keyup(function(event){

			if(event.keyCode == 13){
				addNewId();
				
		    }
		});

	$("#otpemail").keyup(function(event){
		//alert();
			if(event.keyCode == 13){
				$("#submitEmail").click();
		    }
		});		 	
</script>

</html>


