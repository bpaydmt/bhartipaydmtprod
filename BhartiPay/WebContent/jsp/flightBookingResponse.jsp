<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<jsp:include page="gridJs.jsp"></jsp:include>
 <%@page import="com.bhartipay.airTravel.bean.BookingResponse"%>
 <%@page import="java.util.Iterator" %>
 <%@page import="com.bhartipay.airTravel.bean.Segment" %>
 <%@page import="com.bhartipay.airTravel.bean.FlightBookingList" %>
<%@page import="com.bhartipay.airTravel.bean.Passenger" %>
<%@page import="java.util.Properties" %>
<%@page import="java.io.InputStream" %>
<%@page import="java.text.DecimalFormat" %>

</head>

<%
BookingResponse resp = new BookingResponse();
resp = (BookingResponse)request.getAttribute("flightResponse");

DecimalFormat df2 = new DecimalFormat(".00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Tickets Details</h2>
		</div>
		
		<div class="box-content row">
		<div id="fliight-wrapper" class="returnFlightTrue">

<div id="booking-details">
<div class="row">

<div class="tickets-block">

<div class="block-head">
<span class="step-count">01</span>
Ticket Details
</div>

<div class="details-block">
		
		
<%
SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
 SimpleDateFormat f2 = new SimpleDateFormat("E, dd MMM yyyy HH:mm");
Iterator<FlightBookingList> itr = resp.getFlightBookingList().iterator();
while(itr.hasNext()){
FlightBookingList flightList=itr.next();
Segment segSource = flightList.getResponse().getResponse().getFlightItinerary().getSegments().get(0);
Segment segDest =  flightList.getResponse().getResponse().getFlightItinerary().getSegments().get(flightList.getResponse().getResponse().getFlightItinerary().getSegments().size()-1);
%>
<h2><span id="origin0"><%=segSource.getOrigin().getAirport().getCityName() %></span> <i class="fa fa-plane" style="margin: 0 10px;"></i>  <span id="destina0"><%=segDest.getDestination().getAirport().getCityName() %></span> <span class="f-date"></span></h2>

		<div class="ticket-block">

<%Iterator<Segment> segItr =  flightList.getResponse().getResponse().getFlightItinerary().getSegments().iterator();
while(segItr.hasNext()){
	Segment segSingle = segItr.next();
%>
				<div class="ticket-single">
					<div class="col-md-3">
						<img src="./images/flights/<%=segSingle.getAirline().getAirlineCode()%>.png" class="f-img" alt="SpiceJet">
					<div class="airline-name"><%=segSingle.getAirline().getAirlineName() %></div>	
<div class="airline-code"><%=segSingle.getAirline().getAirlineCode()%></div>
					</div>
				<div class="col-md-3">
					<div class="time"><%=f2.format(format.parse(segSingle.getOrigin().getDepTime())) %> Hrs</div>
					<div class="airport-code"><%=segSingle.getOrigin().getAirport().getAirportCode() %></div>
				<div class="airport-name"><%=segSingle.getOrigin().getAirport().getAirportName()%></div>
				</div>
	<div class="col-md-3">
					---
				</div>
	<div class="col-md-3">
					<div class="time"><%=f2.format(format.parse(segSingle.getDestination().getArrTime()))%> Hrs</div>
					<div class="airport-code"><%=segSingle.getDestination().getAirport().getAirportCode()%></div>
				<div class="airport-name"><%=segSingle.getDestination().getAirport().getAirportName()%></div>
				</div>
				</div>

		<%} %>	

		</div>
		
		<%} %>

</div>



<div class="block-head">
<span class="step-count">02</span>
Traveller Details
</div>
<div class="details-block" id="Travallers-details" style="font-size:12px">
<div id="flight-err"></div>




			


	<div class="row" style="    font-weight: bold;">
		<div class="col-md-1">
			Title
		
		</div>
			<div class="col-md-2">
			First Name
			
		
		</div>
			<div class="col-md-2">
			Last Name
			
		
		</div>
		
		<div class="col-md-3">
			Booking Id
			
		
		</div>
		<div class="col-md-3">
			Return Booking Id
			
		
		</div>
		</div>
<%
Iterator<Passenger> passList = resp.getFlightBookingList().get(0).getResponse().getResponse().getFlightItinerary().getPassenger().iterator();
int flightsize=resp.getFlightBookingList().size();
Iterator<Passenger> passListReturn = null;
if(flightsize == 2 && resp.getStatusCode().equals("300"))
{
	 passListReturn = resp.getFlightBookingList().get(1).getResponse().getResponse().getFlightItinerary().getPassenger().iterator();
}
while (passList.hasNext())
{
	Passenger pass = passList.next();
	String returnBookingId="NA";
	String bookingId="NA";
	Passenger passReturn = new Passenger();
	if(flightsize == 2 && resp.getStatusCode().equals("300"))
	{
		passReturn =  passListReturn.next();
		returnBookingId = passReturn.getTicket().getTicketNumber();
		bookingId=pass.getTicket().getTicketNumber();
	}
	else if((flightsize == 2 && (resp.getStatusCode().equals("8") || resp.getStatusCode().equals("301") || resp.getStatusCode().equals("304"))) || (flightsize == 1 && (resp.getStatusCode().equals("300") || resp.getStatusCode().equals("8") || resp.getStatusCode().equals("301") || resp.getStatusCode().equals("304"))))
	{
		bookingId=pass.getTicket().getTicketNumber();
	}
	
%>		
	
	
		<div class="row">
		<div class="col-md-1">
			<%=pass.getTitle() %>
		
		</div>
			<div class="col-md-2">
			<%=pass.getFirstName() %>
			
		
		</div>
			<div class="col-md-2">
			<%=pass.getLastName() %>
			
	</div>
		<div class="col-md-3">
			<%=bookingId %>
			
		
		</div>
		<div class="col-md-3">
			<%=returnBookingId %>
			
		
		</div>
		
	
	</div>

	<%} %>
	</div>



</div>
<div class=" fare-summary">
<div class="summary-header"></div>
<h4>Fare Summary</h4>

<div class="fare-break-total">

</div>



<div class="fare-break">
<div class="info-block"><label>Fare</label> <span class="s-value">  <span class="currency">INR</span><%=df2.format(resp.getFlight1TotalFare() + resp.getFlight2TotalFare() - resp.getFlight1ConvienceFee() - resp.getFlight2ConvienceFee()) %></span></div>
<div class="info-block"><label>Convenience Fee</label> <span class="s-value"> <span class="currency">INR</span><%=df2.format(resp.getFlight1ConvienceFee() + resp.getFlight2ConvienceFee())%><div>
</div>


</div>

<div class="bottom-total">
<div class="info-block">
<label>Total Amount</label><span class="s-value">
 <span class="currency">INR</span>
<span class="amount-span"><%=df2.format(resp.getFlight1TotalFare() + resp.getFlight2TotalFare()) %></span>
<span>

</span></span></div>


</div>


</div>
<%
String fileName=session.getAttribute("flightPath").toString();
String fName="Bhartipay_Ticket_"+session.getAttribute("TripId")+".pdf";
%>
<%-- <a href="<%=fileName%>" download> --%>
 <a href="./flightPDF/<%=fName %>" download> 
Download Ticket
 
</a>

</div>

</div>


		
</div>


</div>
 
			
		</div>
		
							</div>
	</div>	
							
		
	</div>

</div>
</div>

</div>
        
        
<script>



$(function(){
	$("#priceChangePopup").modal('show')
});


</script>


<!-- Modal -->
<div id="priceChangePopup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Booking Response</h4>
      </div>
      <div class="modal-body">
       <h3 style="font-size:18px">
	<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color:green;">
		<s:actionmessage/>
		</font>
		</h3>
        
    
      </div>
      <div class="modal-footer">
      <%if(resp.getStatusCode().equals("302") || resp.getStatusCode().equals("303") || resp.getStatusCode().equals("301") || resp.getStatusCode().equals("304")){%>
        <a href="releasePNR" class="btn btn-default">Cancel</a>
        <a href="bookFlight" class="btn btn-info">Proceed</a>
        <%}else{ %>
         <button type="button" class="btn btn-default" data-dismiss="modal" >Done</button>
        <%} %>
      </div>
    </div>

  </div>
</div>
       

</div>

<jsp:include page="footer.jsp"></jsp:include>

<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>


</body>
</html>


