<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<script type="text/javascript">
 $(function(){
	$("#dob, #paymentDate").datepicker({
	    	 language: 'en',
	    	 maxDate: new Date(),
	    	
	    });
		

});

function validatePancardApi(elm){

	//var reg ="";
	var inp = $(elm).val();
	//sssssssalert(inp);
	if(inp.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){
		//alert(5555);
		$.ajax({
			method:'Post',
			url:'ValidatePan',
			data:"pan="+inp+"&userType=2",
			success:function(result){
				console.log(result.status)
				
				if(result.status == "true"){
					$(elm).addClass("registerPanCard")
					$("#panError").text("This pan card number is already register with another agent");
				}else{
					$("#panError").text("");
				}
			}
		})
	}else{
		
		
	}
	
	
}
function copyAddress(){
	
	if($("#addressCheckbox").prop("checked")){
			$("#shopaddress1").val($("#address1").val())
			$("#shopaddress2").val($("#address2").val())
			$("#shopstate").val($("#state").val())
			$("#shopcity").val($("#city").val())
			$("#shoppin").val($("#pin").val())
	}else{
	
			$("#shopaddress1").val("")
			$("#shopaddress2").val("")
			$("#shopstate").val("-1")
			$("#shopcity").val("")
			$("#shoppin").val("")
	}
	
}
</script>

<script type="text/javascript">

function getDistributorDetail()
{
debugger
	  var dist = $('#usertype').children("option:selected").val();
      if(dist=='-1')
      {
  		alert("Please select Distributor.");
  		return false;
  	  }
    
	  $.ajax({
      		method:'Post',
      		cache:0,  		
      		url:'DistributorDetails',
      		data:"distributorId="+dist,
      		success:function(data){
      		var json = JSON.parse(data);
			var distId=json.agentId;
			var mobileNo=json.mobileNo;
			var name=json.userName;
			document.getElementById("distributorName").value=name;
			document.getElementById("distributorMobileNo").value=mobileNo; 
      	
      		}
      	})
	
}



</script>



	
</head>
<%
String userType=(String)request.getAttribute("userType");
if(userType!=null&&!userType.equalsIgnoreCase("-1")&&!userType.isEmpty())
%>
<style>
.upperCase
{
    text-transform: uppercase;
}
</style>
	<body>



    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
 
            
<%
User user=(User)session.getAttribute("User");
%>
    
 
 
    <div class="box">
        <div class="box-inner">
            <div class="box-header">
                <h2><i class="glyphicon "></i>Edit Agent Details</h2>
            
            </div>
            <div class="box-content">
                                  
		<form action="UpdateAgentOnBoard" id="update_form_name" method="post" name="PG" >
	            
		
									<div class="row">
									<font id="err" color="red"><s:actionerror/> </font>
									<font id="suc" color="blue"><s:actionmessage/> </font>
									<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
						<div class="row">			
							<%-- <div class="form-group  col-md-12  txtnew  col-xs-6">
							
							<div class="col-sm-4">
									<label for="email">Select Agent Type<font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{agentTypeList}" headerKey="-1"
												headerValue="Select Agent Type" id="agentType"
												name="walletBean.agentType" cssClass=""
												requiredLabel="true"
												 />

										</div>
									</div>
								</div>
							</div> --%>

														    <input	class="form-control" name="walletBean.id" id="createdby" type="hidden" value="<s:property value='%{walletBean.id}'/>" />
															<input	class="form-control" name="walletBean.createdby" id="createdby" type="hidden" value="<%=user.getSubAggregatorId() %>" />
															<input	class="form-control" name="walletBean.agentid" id="createdby" type="hidden" value="-1" />
															<input	class="form-control" name="walletBean.subAgentId" id="createdby" type="hidden" value="-1" />
									
									
             					 <div class="col-md-12">
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Name <fRont color='red'> *</font></label>
															
															<input type="text" name="walletBean.name" placeholder="Name" class="form-control upperCase  userName mandatory" id="name"  value="<s:property value='%{walletBean.name}'/>"  >

														</div>
													</div>
														<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Email <font color='red'> *</font></label>
			                       <input type="text" name="walletBean.emailid" placeholder="Email" class=" form-control  upperCase emailid mandatory" id="email"  value="<s:property value='%{walletBean.emailid}'/>" readonly="readonly" >

														</div>
													</div>
														<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Mobile Number <font color='red'> *</font></label>
			                       <input type="text" name="walletBean.mobileno" placeholder="Mobile Number" class=" form-control mobile mandatory" id="mobile"  value="<s:property value='%{walletBean.mobileno}'/>" title="Mobile number must have 10 digits and should start with 7,8 or 9."  maxlength="10"  pattern="[6789][0-9]{9}" requiredmessage="invalid mobile number"  readonly="readonly" >

														</div>
													</div>
									</div>
									
											
             					 <div class="col-md-12">
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Date Of Birth <font color='red'> *</font></label>
															
															<input type="text" name="walletBean.dob" placeholder="DD/MM/YYYY" class="form-control mandatory datepicker-here1" id="dob"  value="<s:property value='%{walletBean.dob}'/>"  >

														</div>
													</div>
														<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">PAN <font color='red'> *</font></label>
			                       <input type="text" name="walletBean.pan" placeholder="PAN" class=" form-control mandatory" id="pan"  value="<s:property value='%{walletBean.pan}'/>"  readonly="readonly">

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Aadhar Number <font color='red'> *</font></label>
			                       <input type="text" name="walletBean.adhar" placeholder="Aadhar Number" class=" form-control mandatory" id="adhar"  value="<s:property value='%{walletBean.adhar}'/>" readonly="readonly"  >

														</div>
													</div>
													
											</div>
									 <div class="col-md-12">
										</div>
												<div class="col-md-12">

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Address 1<font color='red'> *</font></label> <input
																type="text" name="walletBean.address1"
																placeholder="Address 1" value="<s:property value='%{walletBean.address1}'/>"
																class=" form-control address-vl mandatory upperCase" maxlength="100" id="address1"
																/>

														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Address 2<font color='red'> *</font></label> <input
																type="text" name="walletBean.address2"
																placeholder="Address 2" maxlength="100"
																class=" form-control address-vl upperCase" id="address2"
																value="<s:property value='%{walletBean.address2}'/>"/>

														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">City<font color='red'> *</font></label> <input
																type="text" name="walletBean.city"
																placeholder="City"
																class=" form-control userName mandatory upperCase" id="city"
																value="<s:property value='%{walletBean.city}'/>"
																/>

														</div>
													</div>


												</div>


												<div class="col-md-12">

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">State<font color='red'> *</font></label> 
															
															
															<input
																type="text" name="walletBean.state"
																placeholder="State"
																class=" form-control userName mandatory upperCase" id="state"
																value="<s:property value='%{walletBean.state}'/>"
																/>
															<%-- <select name="walletBean.state"class=" form-control mandatory" id="state">
																<option value="-1">------------Select State------------</option>
																<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
																<option value="Andhra Pradesh">Andhra Pradesh</option>
																<option value="Arunachal Pradesh">Arunachal Pradesh</option>
																<option value="Assam">Assam</option>
																<option value="Bihar">Bihar</option>
																<option value="Chandigarh">Chandigarh</option>
																<option value="Chhattisgarh">Chhattisgarh</option>
																<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
																<option value="Daman and Diu">Daman and Diu</option>
																<option value="Delhi">Delhi</option>
																<option value="Goa">Goa</option>
																<option value="Gujarat">Gujarat</option>
																<option value="Haryana">Haryana</option>
																<option value="Himachal Pradesh">Himachal Pradesh</option>
																<option value="Jammu and Kashmir">Jammu and Kashmir</option>
																<option value="Jharkhand">Jharkhand</option>
																<option value="Karnataka">Karnataka</option>
																<option value="Kerala">Kerala</option>
																<option value="Lakshadweep">Lakshadweep</option>
																<option value="Madhya Pradesh">Madhya Pradesh</option>
																<option value="Maharashtra">Maharashtra</option>
																<option value="Manipur">Manipur</option>
																<option value="Meghalaya">Meghalaya</option>
																<option value="Mizoram">Mizoram</option>
																<option value="Nagaland">Nagaland</option>
																<option value="Orissa">Orissa</option>
																<option value="Pondicherry">Pondicherry</option>
																<option value="Punjab">Punjab</option>
																<option value="Rajasthan">Rajasthan</option>
																<option value="Sikkim">Sikkim</option>
																<option value="Tamil Nadu">Tamil Nadu</option>
																<option value="Tripura">Tripura</option>
																<option value="Uttaranchal">Uttaranchal</option>
																<option value="Telangana">Telangana</option>
																<option value="Uttar Pradesh">Uttar Pradesh</option>
																<option value="West Bengal">West Bengal</option>
																</select> --%>
													
														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">PIN Code<font color='red'> *</font></label> 
															<input
																type="text" name="walletBean.pin"
																placeholder="PIN Code" maxlength='6' min='100000'
																class=" form-control pincode mandatory" id="pin"
																value="<s:property value='%{walletBean.pin}'/>"
																/>

														</div>
													</div>
												
												
												<!-- 	<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Same as above</label> 
															<input type="checkbox" onchange="copyAddress()" id="addressCheckbox" value="copy above addresss"  />

														</div>
													</div> -->
												
											

												</div>

<div class="col-md-12">


<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop Name<font color='red'> *</font></label> <input
																type="text" name="walletBean.shopName"
																placeholder="Shop Name" value="<s:property value='%{walletBean.shopName}'/>"
																class=" form-control address-vl mandatory upperCase" maxlength="100" id="shopname"
																/>

														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop Address 1</label> <input
																type="text" name="walletBean.shopAddress1"
																placeholder="Shop Address 1" value="<s:property value='%{walletBean.shopAddress1}'/>"
																class=" form-control address-vl upperCase" maxlength="100" id="shopaddress1"
																/>

														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop Address 2</label> <input
																type="text" name="walletBean.shopAddress2"
																placeholder="Shop Address 2" maxlength="100"
																class=" form-control address-vl upperCase" id="shopaddress2"
																value="<s:property value='%{walletBean.shopAddress2}'/>"/>

														</div>
													</div>
</div>
	<div class="col-md-12">										<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop City</label> <input
																type="text" name="walletBean.shopCity"
																placeholder="Shop City"
																class=" form-control userName upperCase" id="shopcity"
																value="<s:property value='%{walletBean.shopCity}'/>"
																/>

														</div>
													</div>


												


												

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop State</label>
																
																<input
																type="text" name="walletBean.shopState"
																placeholder="Shop State"
																class=" form-control userName mandatory upperCase" id="shopstate"
																value="<s:property value='%{walletBean.shopState}'/>"
																/>
																<%-- <select  name="walletBean.shopState" id="shopstate" >
																<option value="-1">------------Select State------------</option>
																<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
																<option value="Andhra Pradesh">Andhra Pradesh</option>
																<option value="Arunachal Pradesh">Arunachal Pradesh</option>
																<option value="Assam">Assam</option>
																<option value="Bihar">Bihar</option>
																<option value="Chandigarh">Chandigarh</option>
																<option value="Chhattisgarh">Chhattisgarh</option>
																<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
																<option value="Daman and Diu">Daman and Diu</option>
																<option value="Delhi">Delhi</option>
																<option value="Goa">Goa</option>
																<option value="Gujarat">Gujarat</option>
																<option value="Haryana">Haryana</option>
																<option value="Himachal Pradesh">Himachal Pradesh</option>
																<option value="Jammu and Kashmir">Jammu and Kashmir</option>
																<option value="Jharkhand">Jharkhand</option>
																<option value="Karnataka">Karnataka</option>
																<option value="Kerala">Kerala</option>
																<option value="Lakshadweep">Lakshadweep</option>
																<option value="Madhya Pradesh">Madhya Pradesh</option>
																<option value="Maharashtra">Maharashtra</option>
																<option value="Manipur">Manipur</option>
																<option value="Meghalaya">Meghalaya</option>
																<option value="Mizoram">Mizoram</option>
																<option value="Nagaland">Nagaland</option>
																<option value="Orissa">Orissa</option>
																<option value="Pondicherry">Pondicherry</option>
																<option value="Punjab">Punjab</option>
																<option value="Rajasthan">Rajasthan</option>
																<option value="Sikkim">Sikkim</option>
																<option value="Tamil Nadu">Tamil Nadu</option>
																<option value="Tripura">Tripura</option>
																<option value="Uttaranchal">Uttaranchal</option>
																<option value="Uttar Pradesh">Uttar Pradesh</option>
																<option value="West Bengal">West Bengal</option>
																</select> --%>
																

														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Shop PIN Code</label> 
															<input
																type="text" name="walletBean.shopPin"
																placeholder="Shop PIN Code" maxlength='6' min='100000'
																class=" form-control pincode" id="shoppin"
																value="<s:property value='%{walletBean.shopPin}'/>"
																/>

														</div>
													</div>
						</div>
						
						
						<div class="col-md-12">		
												<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Territory</label> <input
																type="text" name="walletBean.territory"
																placeholder="Territory"
																class=" form-control userName upperCase" id="territory"
																value="<s:property value='%{walletBean.territory}'/>"
																/>

														</div>
													</div>


												
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Manager Name</label> <input
																type="text" name="walletBean.managerName"
																placeholder="Manager Name"
																class=" form-control userName upperCase" id="managerName"
																value="<s:property value='%{walletBean.managerName}'/>"
																/>

														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">SO Name</label> <input
																type="text" name="walletBean.soName"
																placeholder="SO Name"
																class=" form-control userName upperCase" id="soName"
																value="<s:property value='%{walletBean.soName}'/>"
																/>

														</div>
									</div>
						</div>
						
						
						<div class="form-group  col-md-12  txtnew  col-xs-6">
							<div class="col-sm-4">
									<label for="email">Select Distributor<font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{distributorList}" headerKey="-1"
												headerValue="Select Distributor" id="usertype" onchange="getDistributorDetail()"
												name="walletBean.distributerid" cssClass=""
												requiredLabel="true"
												 />

										</div>
									</div>
								</div>
								
								<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Distributor Name</label> <input
																type="text" name="walletBean.distributorName"
																placeholder="Distributor Name"
																class=" form-control userName upperCase" id="distributorName"
																value="<s:property value='%{walletBean.distributorName}'/>"
																/>

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Distributor Mobile No</label> <input
																type="text" name="walletBean.distributorMobileNo"
																placeholder="Distributor Mobile No"
																class="  form-control mobile mandatory" id="distributorMobileNo"
																value="<s:property value='%{walletBean.distributorMobileNo}'/>"
																/>

														</div>
													</div>
								
								
								</div>
								
														
						<div class="form-group  col-md-12  txtnew  col-xs-6">
							<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Bank Name</label> <input
																type="text" name="walletBean.bankName"
																placeholder="Bank Name"
																class=" form-control userName upperCase" id="bankName"
																value="<s:property value='%{walletBean.bankName}'/>"
																/>

														</div>
													</div>
													
								
								<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Account Number</label> <input
																type="text" name="walletBean.accountNumber"
																placeholder="Account Number"
																class=" form-control number upperCase" id="accountNumber"
																value="<s:property value='%{walletBean.accountNumber}'/>"
																/>

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">IFSC Code</label> <input
																type="text" name="walletBean.ifscCode"
																placeholder="IFSC Code"
																class=" form-control  upperCase" id="ifscCode"
																value="<s:property value='%{walletBean.ifscCode}'/>"
																/>

														</div>
													</div>
								
								
								</div>
								
								<%-- <div class="form-group  col-md-12  txtnew  col-xs-6">
								<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Mode</label> 
															<s:select list="%{paymentModeList}" headerKey="-1"
												headerValue="Select Payment Mode" id="paymentMode"
												name="walletBean.paymentMode" cssClass=""
												requiredLabel="true"
												 />

														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Bank Name</label> 
															<input type="text" name="walletBean.bankName"  <s:property value='%{walletBean.bankName}'/> placeholder="Bank Name" class="form-control upperCase  userName" value="<s:property value='%{walletBean.bankNamee}'/>">	
														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Amount<font color='red'> *</font></label> 
															<input type="text" name="walletBean.applicationFee" <s:property value='%{walletBean.applicationFee}'/> placeholder="Payment Amount" class="form-control amount-vl mandatory" value="<s:property value='%{walletBean.applicationFee}'/>" >	
														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Date<font color='red'> *</font></label> 
															<input type="text" id="paymentDate" name="walletBean.paymentDate" <s:property value='%{walletBean.paymentDate}'/> placeholder="DD/MM/YYYY" class="form-control mandatory datepicker-here1" value="<s:property value='%{walletBean.paymentDate}'/>">	
														</div>
													</div>
								</div> --%>
													<div class="col-md-12">	
													 <div class="col-md-4">
														<div class="form-group" style="padding-bottom: 50px;">

															<input type="submit" class="btn btn-success  submit-form" id="updateOnboardForm"
																style="margin-top: 20px;"  value="Submit" onclick="updateOnboardFormSubmit('#update_form_name')" />
																


															<button type="reset" onclick="resetForm('#form_name')"  class="btn reset-form btn-info btn-fill"
																style="margin-top: 20px;">Reset</button>


														</div>
													</div> 

												</div>
												</div><br><br>


									</div>
									
									</form>
								
									

                </div>
                

    
            </div>
        </div>
  
 

 

 
        
        
 

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>
 <script src="./js/validateWallet.js"></script>
<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>
