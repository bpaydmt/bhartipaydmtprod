
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletKYCBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'User KYC List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'User KYC List - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'User KYC List - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'User KYC List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                        columns: "thead th:not(.hidden-col-pdf)",
                    }
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'User KYC List - ' + '<%= currentDate %>',
                exportOptions: {
                    columns: "thead th:not(.hidden-col-pdf)",
                }
            }
        ]
    } );
    
} );
function openPopup(id){
	$(".confirmation").hide();
	$(id).fadeIn();
	
}
function closePopup(id){
	$(id).fadeOut();

	}
function submitKYCForm(id){
	$(id).submit();
	
}
 </script>
 

 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function getSubagentByAgentId(agentId)
{  
	
	$.ajax({
        url:"GetSubagent.action",
        cache:0,
        data:"reportBean.agentId="+agentId,
        success:function(result){
        	
               document.getElementById('subagent').innerHTML=result;
               
         }
  });  
	return false;
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");


%>
<style>
table.display thead th {
    padding: 3px 5px 3px 0px!Important;
 
}
</style>      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>User KYC List</h2>
		</div>
		<div class="box-content row">
		<font color="red"><s:actionerror/> </font>
		<font color="blue"><s:actionmessage/> </font>
		<form action="UserKycList" method="post">
							<div class="form-group  col-md-12  txtnew  col-xs-6">
							<%if(user.getUsertype()==99){ %>
								<div class="col-sm-3">
									<label for="email">Select Aggregator</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{aggrigatorList}" headerKey="-1"
												headerValue="Select Aggregator" id="aggregator"
												name="reportBean.aggId" cssClass="form-username"
												requiredLabel="true"
												onchange="getDistributerByAggId(document.getElementById('aggregator').value);" />


	<!--
	private String aggId;
	private String distId;
	private String agentId; -->
										</div>
									</div>
								</div>

<%}if(user.getUsertype()==99||user.getUsertype()==4){ %>
								<div class="col-sm-3">
									<label for="email">Select Distributor</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="distributor">

											<s:select list="%{distributorList}" headerKey="-1"
												headerValue="Select Distributor" id="distri"
												name="reportBean.distId" cssClass="form-username"
												requiredLabel="true"
												onchange="getAgentByDistId(document.getElementById('distri').value);" />

										</div>
									</div>

								</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3){ %>
								<div class="col-sm-3">
									<label for="email">Select Agent</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="agent">

											<s:select list="%{agentList}" headerKey="-1"
												headerValue="Select Agent" id="agentsub"
												name="reportBean.agentId" cssClass="form-username"
												requiredLabel="true"
												onchange="getSubAgentByAgentId(document.getElementById('agentsub').value);"
												/>

										</div>
									</div>

								</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3||user.getUsertype()==2){ %>
								<div class="col-sm-3">
								<label for="email">Select Sub-agent</label> <br>
								<div class="wwgrp" id="wwgrp_paymentMethods">
									<div class="wwctrl" id="subagent">

										<s:select list="%{subagentList}" headerKey="-1"
											headerValue="Select Sub-agent" id="subagentsel"
											name="reportBean.subagentId" cssClass="form-username"
											requiredLabel="true"											
											/>

									</div>
								</div>

							</div>
<%} %>

								<div
									class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit">
										<input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Search">
									</div>
								</div>
							</div>

</form>
</div>		
	</div>
	</div>	
		
		
							<%-- <div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
								<label for="dateFrom">Date From:</label> <br> <input
									type="text" value="<s:property value='%{inputBean.stDate}'/>"
									name="inputBean.stDate" id="dp"
									class="form-control datepicker-here" placeholder="Start Date"
									data-language="en" required readonly="readonly">
							</div>
							<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
								<label for="dateTo">Date To:</label> <br> <input
									type="text" value="<s:property value='%{inputBean.endDate}'/>"
									name="inputBean.endDate" id="dp1"
									class="form-control datepicker-here" placeholder="End Date"
									data-language="en" required readonly="readonly">
							</div> --%>
							
							
			<%
		List<WalletKYCBean>userList=(List<WalletKYCBean>)request.getAttribute("userL");
	
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
					<th><u>Ref Id</u></th>
						<th><u>Id</u></th>	
						<th><u>Name</u></th>	
						<th><u>Mobile</u></th>	
						<th><u>Email</u></th>						
						<th><u>Id proof</u></th>
						<th><u>Id Proof Desc</u></th>
						<th><u>Id Proof Doc</u></th>
						<th><u>Address Proof</u></th>
						<th><u>Address Proof Desc</u></th>
						<th><u>Address Proof Doc</u></th>
						<th><u>Status</u></th>
						<th><u>Date</u></th>
						<th width="12%" class="hidden-col-pdf"><u></u></th>
						
					</tr>
				</thead>
				<tbody>
				<%
				if(userList!=null&&userList.size()>0){	
				for(WalletKYCBean wmb:userList){
				%>
				  <tr>				  	
		             <td><%=wmb.getRefid() %></td>
		             <td><%=wmb.getUserId() %></td>
		              <td><%=wmb.getUserName() %></td>
		               <td><%=wmb.getMobileNo() %></td>
		              <td><%=wmb.getEmailId() %></td>
		             
		           	 <td><%=wmb.getIdprofkycid() %></td>		             
		             <td><%=wmb.getIdprofdesc() %></td>
		             <td>
		     

 					
		               <img style="cursor: pointer;" alt="userImg" src="<%=wmb.getIdprofkycpic()%>" width="40px" height="40px" data-toggle="modal" data-target="#myModal2<%=wmb.getRefid() %>"/>
		             		             	                        <div id="myModal2<%=wmb.getRefid() %>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content" style="text-align: center;padding: 10px;">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	   
	  
	       <img alt="userImg" src="<%=wmb.getIdprofkycpic()%>"  />
	     
	      
	    </div>
	
	  </div>
	</div>
		             </td>
		             <td><%=wmb.getAddprofkycid() %></td>
		             <td><%=wmb.getAddprofdesc()%></td>
		             <td>
		          
		             <img style="cursor: pointer;" alt="userImg" data-toggle="modal" src="<%=wmb.getAddprofkycpic()%>" width="40px" height="40px" data-target="#myModal<%=wmb.getRefid() %>" />
		           
		             	                        <div id="myModal<%=wmb.getRefid() %>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content" style="text-align: center;padding: 10px;">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	   
	  
	       <img alt="userImg" src="<%=wmb.getAddprofkycpic()%>"  />
	     
	      
	    </div>
	
	  </div>
	</div>
		             </td>
		             <td><%=wmb.getStatus()%></td>
		             <td><%=wmb.getLoaddate()%></td>
		             
		             <td style="position:relative">
		             
		       
				
					<input type="button" value="Accept" class="btn btn-sm btn-block btn-success" onclick="openPopup('#confirmation<%=wmb.getRefid()%>')">
												<div class="popover confirmation left top in" id="confirmation<%=wmb.getRefid() %>">
														      <form action="AcceptKyc" method="post" id="kycForm<%=wmb.getRefid()%>">
														      	<input type="hidden" name="reportBean.refid" value="<%=wmb.getRefid()%>">
															<h3 class="popover-title">Are you sure?</h3>
															<div class="popover-content">
															
															<div class="btn-group">
															<a href="#" class="btn btn-xs btn-primary"  onclick="submitKYCForm('#kycForm<%=wmb.getRefid()%>')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
															<a href="#" class="btn btn-xs btn-default" onclick="closePopup('#confirmation<%=wmb.getRefid()%>')"><i class="glyphicon glyphicon-remove"></i> No</a>
															</div>
															</div>
															</form>
															</div>
															
					 
					</br>
			
					
					<input type="button" value="Reject" class="btn btn-sm btn-block btn-success" onclick="openPopup('#rejectPopup<%=wmb.getRefid()%>')">
				
					<div class="popover confirmation left top in" id="rejectPopup<%=wmb.getRefid() %>">
														      <form action="RejectKyc" method="post" id="reject<%=wmb.getRefid()%>">
														      	<input type="hidden" name="reportBean.refid" value="<%=wmb.getRefid()%>">
															<h3 class="popover-title">Are you sure?</h3>
															<div class="popover-content">
															
															<div class="btn-group">
															<a href="#" class="btn btn-xs btn-primary"  onclick="submitKYCForm('#reject<%=wmb.getRefid()%>')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
															<a href="#" class="btn btn-xs btn-default" onclick="closePopup('#rejectPopup<%=wmb.getRefid()%>')"><i class="glyphicon glyphicon-remove"></i> No</a>
															</div>
															</div>
															</form>
															</div>
															 </td> 
		             
		             
		             
		             
		             
                  </tr>
                  <%
                  }}
				%>
			        </tbody>		</table>
		</div> 
		
</div>
</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->
<!-- ** -->




</body>
</html>


