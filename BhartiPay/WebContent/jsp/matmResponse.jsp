<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.matm.MatmResponseData"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    font-size:small;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;

}

tr:nth-child(even) {
    background-color: #dddddd;
}






  .popup-overlay-other1{
    visibility:hidden;
    position:fixed;
    width:50%;
    height:50%;
    left:25%;
    top: 38%;

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{
    visibility:visible;
    z-index: 999;
  }

  .popup-content-other1 {
   visibility:hidden;
  }

  .popup-content-other1.active {
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
          width: 50%;
          height: auto;
          left: 25%;
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none;
       font-size: 16px;
       font-weight: bold;
      line-height: 1;
      color: #fff;
      text-shadow:none;
      opacity:1;
      background: #a57225!important;
      padding: 10px;
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  }


.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important;
        margin-bottom: 15px;
        color: #111 !important;
}
</style>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->


        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->
<div >
<div id="main" role="main">
        <div id="content">
            <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">


    <div class="box" id="">
        <div class="box-inner">

            <div class="box-content">


                                                                        <div class="">
                                                                        <font color="red"><s:actionerror/> </font>
                                                                        <font color="blue"><s:actionmessage/> </font>
                                                 <div class="">
<%
MatmResponseData data=(MatmResponseData)request.getAttribute("data");
if(data!=null){
%>
<div class="" style="margin-top:5px;">
<img src="./image/matm.png" style="width:5%;margin-top:-15px;" id="aepsPics">
<div  id="matmPrintArea">
  <div >

                                  <img src="" style="width:12%;float:right;margin-top:-15px;display:none;" id="logoPics">
                                </div>
 <div class="box-header" style="padding-top: 0px;">
              <font>m-ATM</font>
            </div>
        <table id="example" class="scrollD cell-border dataTable no-footer" width="100%" style="border-top: 1px solid #ccc;" >
                <tr>
                        <td style="font-weight: bold;">Type</td>
                        <td><%=data.getTxnType()!= null ? data.getTxnType() : "-"%></td>
                    <td style="font-weight: bold;">Date & Time</td>
                        <td ><%=data.getDateTime()%></td>

                </tr>

                <tr>
                        <td style="font-weight: bold;">Transaction Id</td>
                        <td><%=data.getRequestId()!= null ? data.getRequestId() : "-"%></td>
                    <td style="font-weight: bold;">Mobile Number</td>
                        <td><%=data.getPayeeId()!= null ? data.getPayeeId() : "-"%></td>
                </tr>

                <tr>
                        <td style="font-weight: bold;">Bank Name</td>
                        <td><%=data.getBcname() != null ? data.getBcname() : "-" %></td>
                        <td style="font-weight: bold;">Available Balance</td>
                        <td><%=data.getAccountBalance()!= null ? data.getAccountBalance() : "-"%></td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Transaction Amount</td>
                        <td><%=data.getAmount()%></td>
                        <td style="font-weight: bold;">Commission Amount</td>
                        <td></td>
                </tr>

                <tr>
                        <td style="font-weight: bold;">Card Number</td>
                        <td><%=data.getCardNumber()!= null ? data.getCardNumber() : "-"%></td>
                        <td style="font-weight: bold;">RRN</td>
                        <td><%=data.getRrn() != null ? data.getRrn() : "-"%></td>
                </tr>

                <tr>
                        <td style="font-weight: bold;">Transaction Status</td>

                        <td><%
if(data.getPaymentStatus() !=null){
if(data.getPaymentStatus().equalsIgnoreCase("Success")){%>
<img src="./images/success.svg" style="width: 19px;margin-right: 5px;"><b style="color:green">Success</b>
<%}else if (data.getPaymentStatus().equalsIgnoreCase("Failed")){%>
<img src="./images/fail.svg" style="width: 19px;margin-right: 5px;"><b style="color:red">Failed</b>
<%}else{%>
<b>--</b>
<%}}%></td>


                        <td style="font-weight: bold;">Response Message</td>
                        <td><%=data.getStatusMessage() != null ? data.getStatusMessage() : "-"%></td>
                </tr>
        </table>
</div>

        <div style="text-align: center;padding: 10px;">
                <button class="btn-primary btn" onclick="printNow()">Print</button>

                <a class="open-other1 btn btn-info" tabindex="0" aria-controls="example" href="#">
                  <span>Send SMS</span>
                </a>
                <a class="btn btn-info" tabindex="0" aria-controls="example" href="#">
                  <span>Close</span>
                </a>
        </div>
</div>
<%} %>
                                                                        </div>
                                                                        </div>




      <!--Creates the popup body-->
      <div class="popup-overlay-other1 overlay-other1" style="display: block;">
        <div class="popup-content-other1">
         <div class='box-login-title-other1'>
              <div class="MessageBoxMiddle-other1">
            <span class="MsgTitle-other1">
             Send Transaction Details via SMS
            </span>
            <p class="pText-other1">Customer Mobile Number</p>
            <input type="text" name="mobileNo" class="mobile " required="true" value="<%=data.getPayeeId()!=null?data.getPayeeId():"" %>" placeholder="Enter your mobile number" id="mobileNo">

           <p style="text-align:center;margin-bottom: 10px;">
                   <button type="button" class="btn btn-success" onclick="sendSMS('<%=data.getRequestId()%>','<%=data.getPayeeId()!=null?data.getPayeeId():"" %>','<%=data.getTxnType()%>','<%=data.getName()%>')">Send SMS</button>
                   <button type="button" class="close-other1 btn btn-primary" style="padding: 6px 12px;">Close</button>
           </p>
            <font color="red"><p id="message"></p></font>
            <p style="font-size: 11px;font-weight: bold;text-align: justify;">
               Please Click on send SMS to get details via SMS notification on provided number.
               SMS charges Rs.0.12 will be debited from your wallet.
            </p>
          </div>
         </div>
        </div>
      </div>
      <!-- Popup End -->


            </div>
        </div>

</div>







                    </div>
        </div>
        </div>
</div><!--/.fluid-container-->
</div>



<jsp:include page="footer.jsp"></jsp:include>

<script>
  $(".open-other1").on("click", function(){
    $(".popup-overlay-other1, .popup-content-other1").addClass("active");
  });
  $(".close-other1").on("click", function(){
    $(".popup-overlay-other1, .popup-content-other1").removeClass("active");
  });

  function sendSMS(txnId, mobileNo, txnType, channel)
  {
  mobileNo = $('#mobileNo').val();
  $.ajax({url: "sendSMSAlert",
          data:"inputBean.mobileNo="+mobileNo+"&inputBean.txnId="+txnId+"&inputBean.txnType="+txnType+"&inputBean.pgtxn="+channel,
          success: function(result){
            $("#message").html(result);
          }});
  }


function printContent(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var mywindow = window.open('', '', '');
    mywindow.document.write('<html><head>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />');
    mywindow.document.write('<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.print();
    mywindow.close();

    return true;
}


function printNow()
{
        var originalContents = document.body.innerHTML;

        $("#tbl-powered-by").show();
        $("#logoPics").show();
        $("#aepsPics").attr('style', 'width:20%');

        var divToPrint=document.getElementById("matmPrintArea");

        $("#print-sms-close").hide();
        $("#sms-popup").hide();

        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
        document.body.innerHTML = originalContents;
}

$(function(){
                $.get('GetUserConfigByDomain',function(data){
                        var img = $.parseJSON(data.result);
                        console.log(img);
                        $('#logoPics').attr('src',img.logopic);

                })
        });


</script>

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


