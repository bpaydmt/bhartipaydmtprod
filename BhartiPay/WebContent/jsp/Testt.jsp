
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
  <title>Startup-signup</title>
  <link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/css/reset.min.css?v=2.2.1" id="reset-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/roboto/roboto.css?v=2.2.1" id="roboto-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/bootstrap/css/bootstrap.min.css?v=2.2.1" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/datetimepicker/jquery.datetimepicker.min.css?v=2.2.1" id="datetimepicker-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css?v=2.2.1" id="colorpicker-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/font-awesome/css/font-awesome.min.css?v=2.2.1" id="fontawesome-css">
<link rel="stylesheet" type="text/css" href="https://support.bhartipay.com/assets/plugins/bootstrap-select/css/bootstrap-select.min.css?v=2.2.1" id="bootstrap-select-css">
<link href="https://support.bhartipay.com/assets/css/forms.min.css?v=2.2.1" rel="stylesheet">
<style class="custom_style_links-color">
a{
color:#024d8f;
}
</style>
<style class="custom_style_links-hover-focus">
a:hover,a:focus{
color:#032fcc;
}
</style>
<style class="custom_style_admin-login-background">
body.login_admin{
background:#c8e5ca;
}
</style>
<style class="custom_style_btn-default">
.btn-default{
background-color:#a8a8a8;
}

                        .btn-default:focus,.btn-default.focus,.btn-default:hover,.btn-default:active,
                        .btn-default.active,
                        .open > .dropdown-toggle.btn-default,.btn-default:active:hover,
                        .btn-default.active:hover,
                        .open > .dropdown-toggle.btn-default:hover,
                        .btn-default:active:focus,
                        .btn-default.active:focus,
                        .open > .dropdown-toggle.btn-default:focus,
                        .btn-default:active.focus,
                        .btn-default.active.focus,
                        .open > .dropdown-toggle.btn-default.focus,
                        .btn-default:active,
                        .btn-default.active,
                        .open > .dropdown-toggle.btn-default{background-color:#767676;color:#fff;border-color:#767676}
                        .btn-default.disabled,
                        .btn-default[disabled],
                        fieldset[disabled] .btn-default,
                        .btn-default.disabled:hover,
                        .btn-default[disabled]:hover,
                        fieldset[disabled] .btn-default:hover,
                        .btn-default.disabled:focus,
                        .btn-default[disabled]:focus,
                        fieldset[disabled] .btn-default:focus,
                        .btn-default.disabled.focus,
                        .btn-default[disabled].focus,
                        fieldset[disabled] .btn-default.focus,
                        .btn-default.disabled:active,
                        .btn-default[disabled]:active,
                        fieldset[disabled] .btn-default:active,
                        .btn-default.disabled.active,
                        .btn-default[disabled].active,
                        fieldset[disabled] .btn-default.active {
                            background-color: #dadada;color:#fff;border-color:#dadada;}.btn-default {
border-color:#a8a8a8;
}
</style>
<style class="custom_style_btn-info">
.btn-info{
background-color:#00628c;
}

                        .btn-info:focus,.btn-info.focus,.btn-info:hover,.btn-info:active,
                        .btn-info.active,
                        .open > .dropdown-toggle.btn-info,.btn-info:active:hover,
                        .btn-info.active:hover,
                        .open > .dropdown-toggle.btn-info:hover,
                        .btn-info:active:focus,
                        .btn-info.active:focus,
                        .open > .dropdown-toggle.btn-info:focus,
                        .btn-info:active.focus,
                        .btn-info.active.focus,
                        .open > .dropdown-toggle.btn-info.focus,
                        .btn-info:active,
                        .btn-info.active,
                        .open > .dropdown-toggle.btn-info{background-color:#00305a;color:#fff;border-color:#00305a}
                        .btn-info.disabled,
                        .btn-info[disabled],
                        fieldset[disabled] .btn-info,
                        .btn-info.disabled:hover,
                        .btn-info[disabled]:hover,
                        fieldset[disabled] .btn-info:hover,
                        .btn-info.disabled:focus,
                        .btn-info[disabled]:focus,
                        fieldset[disabled] .btn-info:focus,
                        .btn-info.disabled.focus,
                        .btn-info[disabled].focus,
                        fieldset[disabled] .btn-info.focus,
                        .btn-info.disabled:active,
                        .btn-info[disabled]:active,
                        fieldset[disabled] .btn-info:active,
                        .btn-info.disabled.active,
                        .btn-info[disabled].active,
                        fieldset[disabled] .btn-info.active {
                            background-color: #3294be;color:#fff;border-color:#3294be;}.btn-info {
border-color:#00628c;
}
</style>
<style class="custom_style_btn-success">
.btn-success{
background-color:#64bf62;
}

                        .btn-success:focus,.btn-success.focus,.btn-success:hover,.btn-success:active,
                        .btn-success.active,
                        .open > .dropdown-toggle.btn-success,.btn-success:active:hover,
                        .btn-success.active:hover,
                        .open > .dropdown-toggle.btn-success:hover,
                        .btn-success:active:focus,
                        .btn-success.active:focus,
                        .open > .dropdown-toggle.btn-success:focus,
                        .btn-success:active.focus,
                        .btn-success.active.focus,
                        .open > .dropdown-toggle.btn-success.focus,
                        .btn-success:active,
                        .btn-success.active,
                        .open > .dropdown-toggle.btn-success{background-color:#328d30;color:#fff;border-color:#328d30}
                        .btn-success.disabled,
                        .btn-success[disabled],
                        fieldset[disabled] .btn-success,
                        .btn-success.disabled:hover,
                        .btn-success[disabled]:hover,
                        fieldset[disabled] .btn-success:hover,
                        .btn-success.disabled:focus,
                        .btn-success[disabled]:focus,
                        fieldset[disabled] .btn-success:focus,
                        .btn-success.disabled.focus,
                        .btn-success[disabled].focus,
                        fieldset[disabled] .btn-success.focus,
                        .btn-success.disabled:active,
                        .btn-success[disabled]:active,
                        fieldset[disabled] .btn-success:active,
                        .btn-success.disabled.active,
                        .btn-success[disabled].active,
                        fieldset[disabled] .btn-success.active {
                            background-color: #96f194;color:#fff;border-color:#96f194;}.btn-success {
border-color:#64bf62;
}
</style>
<style class="custom_style_btn-danger">
.btn-danger{
background-color:#fc3636;
}

                        .btn-danger:focus,.btn-danger.focus,.btn-danger:hover,.btn-danger:active,
                        .btn-danger.active,
                        .open > .dropdown-toggle.btn-danger,.btn-danger:active:hover,
                        .btn-danger.active:hover,
                        .open > .dropdown-toggle.btn-danger:hover,
                        .btn-danger:active:focus,
                        .btn-danger.active:focus,
                        .open > .dropdown-toggle.btn-danger:focus,
                        .btn-danger:active.focus,
                        .btn-danger.active.focus,
                        .open > .dropdown-toggle.btn-danger.focus,
                        .btn-danger:active,
                        .btn-danger.active,
                        .open > .dropdown-toggle.btn-danger{background-color:#ca0404;color:#fff;border-color:#ca0404}
                        .btn-danger.disabled,
                        .btn-danger[disabled],
                        fieldset[disabled] .btn-danger,
                        .btn-danger.disabled:hover,
                        .btn-danger[disabled]:hover,
                        fieldset[disabled] .btn-danger:hover,
                        .btn-danger.disabled:focus,
                        .btn-danger[disabled]:focus,
                        fieldset[disabled] .btn-danger:focus,
                        .btn-danger.disabled.focus,
                        .btn-danger[disabled].focus,
                        fieldset[disabled] .btn-danger.focus,
                        .btn-danger.disabled:active,
                        .btn-danger[disabled]:active,
                        fieldset[disabled] .btn-danger:active,
                        .btn-danger.disabled.active,
                        .btn-danger[disabled].active,
                        fieldset[disabled] .btn-danger.active {
                            background-color: #ff6868;color:#fff;border-color:#ff6868;}.btn-danger {
border-color:#fc3636;
}
</style>
  </head>
<body class="web-to-lead">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div id="response"></div>
        <form action="NewRegs" id="35c1b2140bc605a8596d29119fbee981" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                
         <input type="hidden" name="key" value="35c1b2140bc605a8596d29119fbee981" />
          <div class="row">
          <div class="col-md-12">
		  <div class="form-group" data-type="text" data-name="name" data-required="1">
		  <label class="control-label" for="name"> <span class="text-danger">* </span> Name</label>
		  <input required="true" placeholder="Name" type="text" name="name" id="name" class="form-control" value="">
		  </div>
		  </div>
		  <div class="col-md-12">
		  <div class="form-group" data-type="text" data-name="email" data-required="1">
		  <label class="control-label" for="email"> <span class="text-danger">* </span> Email</label>
		  <input required="true" placeholder="Email Address" type="email" name="email" id="email" class="form-control" value="">
		  </div>
		  </div>
		  <div class="col-md-12">
		  <div class="form-group" data-type="text" data-name="phonenumber" data-required="1">
		  <label class="control-label" for="phonenumber"> <span class="text-danger">* </span> Contact</label>
		  <input required="true" placeholder="Phone" type="text" name="phonenumber" id="phonenumber" class="form-control" value="">
		  </div>
		  </div>
		  <div class="col-md-12">
		  <div class="form-group" data-type="text" data-name="website" data-required="false">
		  <label class="control-label" for="website">Your Website</label>
		  <input placeholder="Website" type="text" name="website" id="website" class="form-control" value="">
		  </div>
		  </div>
		  <div class="col-md-12">
		  <div class="form-group" data-type="textarea" data-name="description" data-required="false">
		  <label class="control-label" for="description">Description</label>
		  <textarea id="description" name="description" rows="4" class="form-control" placeholder="Description">
		  </textarea>
		  </div>
		  </div>                           
		  <div class="clearfix"></div>
         <div class="text-left col-md-12 submit-btn-wrapper">
          <button class="btn btn-success" id="form_submit" type="submit">Sign Up</button>
        </div>
      </div>

            </form>    
            
      </div>
  </div>
</div>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/jquery/jquery.min.js?v=2.2.1" id="jquery-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/bootstrap/js/bootstrap.min.js?v=2.2.1" id="bootstrap-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/jquery-validation/jquery.validate.min.js?v=2.2.1" id="jquery-validation-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/app-build/moment.min.js?v=2.2.1" id="moment-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/app-build/bootstrap-select.min.js?v=2.2.1" id="bootstrap-select-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/datetimepicker/jquery.datetimepicker.full.min.js?v=2.2.1" id="datetimepicker-js"></script>
<script type="text/javascript" src="https://support.bhartipay.com/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js?v=2.2.1" id="colorpicker-js"></script>
    <script>
    $(function(){
     var time_format = '24';
     var date_format = 'd-m-Y';

     $('body').tooltip({
       selector: '[data-toggle="tooltip"]'
     });

     $('body').find('.colorpicker-input').colorpicker({
       format: "hex"
     });

     var date_picker_options = {
       format: date_format,
       timepicker: false,
       lazyInit: true,
       dayOfWeekStart: '0',
     }

    $('.datepicker').datetimepicker(date_picker_options);
     var date_time_picker_options = {
      lazyInit: true,
      scrollInput: false,
      validateOnBlur :false,
      dayOfWeekStart: '0',
    }
    if(time_format == 24){
      date_time_picker_options.format = date_format + ' H:i';
    } else {
      date_time_picker_options.format =  date_format + ' g:i A';
      date_time_picker_options.formatTime = 'g:i A';
    }
    $('.datetimepicker').datetimepicker(date_time_picker_options);

    $('body').find('select').selectpicker({
      showSubtext: true,
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, "File size must be less than 20 MB");

    $.validator.addMethod( "extension", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, $.validator.format( "File extension not allowed" ) );

    $.validator.setDefaults({
     highlight: function(element) {
       $(element).closest('.form-group').addClass('has-error');
     },
     unhighlight: function(element) {
       $(element).closest('.form-group').removeClass('has-error');
     },
     errorElement: 'p',
     errorClass: 'text-danger',
     errorPlacement: function(error, element) {
            if (element.parent('.input-group').length || element.parents('.chk').length) {
                if (!element.parents('.chk').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element.parents('.chk'));
                }
            } else if (element.is('select') && (element.hasClass('selectpicker') || element.hasClass('ajax-search'))) {
               error.insertAfter(element.parents('.form-group *').last());
            } else {
                error.insertAfter(element);
            }
        }
       });
    });
 </script>
 <script>
 var form_id = '#35c1b2140bc605a8596d29119fbee981';
 $(function() {
   $(form_id).validate({

    submitHandler: function(form) {

     $("input[type=file]").each(function() {
          if($(this).val() === "") {
              $(this).prop('disabled', true);
          }
      });

     var formURL = $(form).attr("action");
     var formData = new FormData($(form)[0]);

     $('#form_submit').prop('disabled', true);

     $.ajax({
       type: $(form).attr('method'),
       data: formData,
       mimeType: $(form).attr('enctype'),
       contentType: false,
       cache: false,
       processData: false,
       url: formURL
     }).done(function(response){
      $('#form_submit').prop('disabled', false);
      response = JSON.parse(response);
                 // In case action hook is used to redirect
                 if (response.redirect_url) {
                     window.top.location.href = response.redirect_url;
                     return;
                 }
                 if (response.success == false) {
                     $('#recaptcha_response_field').html(response.message); // error message
                   } else if (response.success == true) {
                     $(form_id).remove();
                     $('#response').html('<div class="alert alert-success">'+response.message+'</div>');
                     $('html,body').animate({
                       scrollTop: $("#online_payment_form").offset().top
                     },'slow');
                   } else {
                     $('#response').html('Something went wrong...');
                   }
                   if (typeof(grecaptcha) != 'undefined') {
                     grecaptcha.reset();
                   }
                 }).fail(function(data){
                 if (typeof(grecaptcha) != 'undefined') {
                   grecaptcha.reset();
                 }
                 if(data.status == 422) {
                    $('#response').html('<div class="alert alert-danger">Some fields that are required are not filled properly.</div>');
                 } else {
                    $('#response').html(data.responseText);
                 }
               });
                 return false;
               }
             });
 });

 
</script>

<script type="text/javascript">

function saveNewRegs()
{
debugger
	var name=document.getElementById("name").value;
	var email=document.getElementById("email").value;
	var phonenumber=document.getElementById("phonenumber").value;
	var website=document.getElementById("website").value;
	var description=document.getElementById("description").value;
debugger	
	$.ajax({
  		method:'Post',
  		cache:0,  		
  		url:'NewRegs',
  		data:"name="+name+"&email="+email+"&phonenumber="+phonenumber+"&website="+website+"&description="+description,
  		success:function(data){
 /*  		var json = JSON.parse(data);
 
  		var name=json.name;

  		document.getElementById("name").value="";
  		document.getElementById("email").value="";
  		document.getElementById("phonenumber").value="";
  		wdocument.getElementById("website").value="";
  		document.getElementById("description").value="";

 */  		
  		});

	}
}

</script>
</body>
</html>
