<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.cme.vo.CMERequestBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>

<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">


	
	     

$(document).ready(function() {
	/* var currentDate = new Date();
    var year = currentDate.getFullYear() - 1;
    var month = currentDate.getMonth() < 10 ? "0" + currentDate.getMonth() : currentDate.getMonth();
   // var month1 = currentDate.getMonth() +1;
    var day =  currentDate.getDate() < 10 ? "0" + currentDate.getDate() :  currentDate.getDate();
   // var day1 = currentDate.getDate() - 1;
 */
    $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),
    // minDate: new Date(year+"-"+month+"-"+day),


    });
    if($('#dpStart').val().length != 0){
     /* var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30); */
     $('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
     
    }
    $("#dpStart").blur(function(){
    /* var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
    var setDate = selectedDate.setDate(selectedDate.getDate() + 30); */
   // console.log(setDate)
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   




    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Cash Deposit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 

function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}



function setMonth(month){
	 var m = parseInt(month)
	 if(month == 11){
	  return 0;
	 }else{
	  console.log(month)
	  return month + 2;
	 }

	 }
	 function setYear(month,year){

	 if(month == 11){
	  return year;
	 }else{
	  console.log(month)
	  return year + 1;
	 }

	 }
	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		<div class="box-header well">
			<h2>CME Deposit</h2>
		
		</div>
		
	
		<div class="box-content row">
		
			<form action="CMERequestReport" method="post">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
															
										
										 <input type="text"
										value="<s:property value='%{cmeBean.stDate}'/>"
										name="cmeBean.stDate" id="dpStart"
										class="form-control datepicker-here1 " placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{cmeBean.endDate}'/>"
													name="cmeBean.endDate" id="dpEnd"
													class="form-control datepicker-here2 " placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Id</u></th>
						<th><u>User Id</u></th>
						<th><u>Type</u></th>
						<th><u>Amount</u></th>
						<th><u>Reference No.</u></th>
						<th><u>Bank Name</u></th>
						<th><u>Branch Name</u></th>
						<th><u>Status</u></th>
						<th><u>Remark</u></th>
						<th><u>Request Date</u></th>
						<th><u>Receipt</u></th>
						
									
					</tr>
				</thead>
				<tbody>
				
				<%
		
			List<CMERequestBean> eList=(List<CMERequestBean>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					CMERequestBean tdo=eList.get(i);%>
		          		  <tr>
		          	   <td><%=tdo.getCmeRequestid()%></td>
		          	   <td><%=tdo.getUserId()%></td>
		               <td><%=tdo.getDocType()%></td>
		               <td><%=d.format(tdo.getAmount())%></td>
		               <td><%=tdo.getTxnRefNo()%></td> 
		               <td><%=tdo.getBankName()%></td>
		               <td><%=tdo.getBranchName()%></td>
		          		<td><%=tdo.getStatus()%></td>
		          <%--  <td><%=tdo.getRemarksReason()%></td>  --%>
		            <td><%if(tdo.getRemarksReason()==null){}else{%><%=tdo.getRemarksReason()%><%}%></td>
		          
		            <td><%=tdo.getReqestdate()%></td> 
		         <td>
		             
		             
		             <%if(tdo.getReciptDoc()!=null&&!tdo.getReciptDoc().isEmpty()){%>
		             <img alt="userImg" src="<%=tdo.getReciptDoc()%>" data-toggle="modal" data-target="#myModal<%=tdo.getCmeRequestid()%>" width="40px" height="40px"/>
		                        <div id="myModal<%=tdo.getCmeRequestid()%>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	          
	
	    <!-- Modal content-->
	    <div class="modal-content">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	   
	  
	       <img alt="userImg" src="<%=tdo.getReciptDoc()%>" style="    width: 545px;
    margin: 20px;" />
	     
	      
	    </div>
	
	  </div>
	</div>
		            <%} %>
		
			             </td> 
		          
                  		  </tr>
          <div id="cmeAction" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
        <form id="cmeForm" method="post">
        <div class="form-group">
        	  <input type="hidden" name="cmeBean.cmeRequestid"  id="hidden-cme" >
        	<input type="text" placeholder="Remark" name="cmeBean.remarksReason" id="remoark-inp" class="form-control" required/>
        	
        	
        </div>
        <input type="submit" value="submit" class="btn btn-info btn-fill btn-block"/>
        </form>
      </div>
     
    </div>

  </div>
</div>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
<!-- contents ends -->


</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->



<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<script src="./js/datepicker.js"></script>
<script src="./js/datepicker.en.js"></script>

</script>

</body>
</html>



