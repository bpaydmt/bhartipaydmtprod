
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.UserWishListBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wish List - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wish List - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wish List - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wish List - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wish List - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
function openPopup(id){
	$(".confirmation").hide();
	$(id).fadeIn();
	
}
function closePopup(id){
	$(id).fadeOut();

	}
function submitKYCForm(id){
	$(id).submit();
	
}
 </script>
 

 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}


</script>
</head>

<% 
User user = (User) session.getAttribute("User");
List<UserWishListBean>list=(List<UserWishListBean>)request.getAttribute("wishList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Wish List</h2>
		</div>
	</div>
	</div>	
		
		
		

						
							
							
			<%
		List<UserWishListBean>userList=(List<UserWishListBean>)request.getAttribute("wishList");
	
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Recharge Number</u></th>						
						<th><u>Recharge Type</u></th>
						<th><u>Operator</u></th>
						<th><u>Circle</u></th>
						<th><u>Amount</u></th>
						<th><u></u></th>
						
						
						
					</tr>
				</thead>
				<tbody>
				<%
				if(userList!=null&&userList.size()>0){	
				for(UserWishListBean wmb:userList){
				%>
				  	<tr>
		             <td><%=wmb.getRechargeNumber() %></td>		             
		             <td><%=wmb.getRechargeType()%></td>
		             <td><%=wmb.getRechargeOperator()%></td>
		              <td><%if(!wmb.getRechargeType().equalsIgnoreCase("DTH")){out.print(wmb.getRechargeCircle());}%></td>
		               <td><%=d.format(wmb.getRechargeAmt())%></td>
		                <td style="position:relative">
		                <div>
		             	<input type="button" class="btn btn-sm btn-block btn-success" value="Recharge" onclick="openPopup('#recharge<%=wmb.getWishId()%>')"/>
		                
					
						       <div class="popover confirmation left top in" id="recharge<%=wmb.getWishId()%>" style=" margin-top: 40px;">
														         <form method="post" action="RechargeService" id="wishlist<%=wmb.getWishId()%>">
														      	<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
						<input type="hidden" name="rechargeBean.userId" value="<%=user.getId()%>">
						<input type="hidden" name="rechargeBean.walletId" value="<%=user.getWalletid()%>">	
						<input type="hidden" name="rechargeBean.rechargeType" value="<%=wmb.getRechargeType()%>">						
						<input type="hidden" name="rechargeBean.rechargeNumber" value="<%=wmb.getRechargeNumber()%>"/>					
						<input type="hidden" name="rechargeBean.rechargeOperator" value="<%=wmb.getRechargeOperator()%>"/>			
						<input type="hidden" name="rechargeBean.rechargeCircle" value="<%=wmb.getRechargeCircle()%>"/>
						<input type="hidden" name="rechargeBean.rechargeAmt" value="<%=wmb.getRechargeAmt()%>"/>	
					
		        
															<h3 class="popover-title">Are you sure?</h3>
															<div class="popover-content">
															
															<div class="btn-group">
															<a href="#" class="btn btn-xs btn-primary"  onclick="submitKYCForm('#wishlist<%=wmb.getWishId()%>')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
															<a href="#" class="btn btn-xs btn-default" onclick="closePopup('#recharge<%=wmb.getWishId()%>')"><i class="glyphicon glyphicon-remove"></i> No</a>
															</div>
															</div>
															</form>
		                </div>
		                </div>
		                 
		                <div style="margin-top: 5px;">
		           
		                 <input type="button" class="btn btn-sm btn-block btn-success" value="Delete" onclick="openPopup('#confirmation<%=wmb.getWishId()%>')">
		                <div class="popover confirmation left top in" id="confirmation<%=wmb.getWishId()%>" style=" margin-top: 40px;">
														      <form action="DeleteWishList" method="post" id="kycForm<%=wmb.getWishId()%>">
														      	    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
		               								 <input type="hidden" name="wishBean.wishId" value="<%=wmb.getWishId()%>">
		        
															<h3 class="popover-title">Are you sure?</h3>
															<div class="popover-content">
															
															<div class="btn-group">
															<a href="#" class="btn btn-xs btn-primary"  onclick="submitKYCForm('#kycForm<%=wmb.getWishId()%>')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
															<a href="#" class="btn btn-xs btn-default" onclick="closePopup('#confirmation<%=wmb.getWishId()%>')"><i class="glyphicon glyphicon-remove"></i> No</a>
															</div>
															</div>
															</form>
		                </div>
		                </div>
		                </td>
		               </tr>  
		                 
		              
                  <%
                  }
				}
                  %>
			        </tbody>		</table>
		</div> 
		
		
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


