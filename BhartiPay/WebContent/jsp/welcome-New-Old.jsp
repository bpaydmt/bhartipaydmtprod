
<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<%

Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");

%>
<title>Bhartipay</title>

    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bhartipay">
    <meta name="author" content="Bhartipay">   
    <link rel="shortcut icon" href="./images/<%=favicon%>">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
<jsp:include page="../newUiJsp/reqFiles.jsp"></jsp:include>
<jsp:include page="../jsp/gridJs.jsp"></jsp:include>
<link href='./css/nivo-slider.css' rel='stylesheet'> 
<link href='./css/dd.css' rel='stylesheet'>	
	<link href="./css/datepicker.min.css" rel="stylesheet" type="text/css">
	<script src="./js/jquery.nivo.slider.js"></script> 	
		<script src="./js/jquery.nivo.slider.js"></script> 	
	<script src="./js/customer.js"></script> 	
			  
	<script src="./js/datepicker.js"></script>

	
	<script src="./js/datepicker.en.js"></script>
	<script src="js/sql.injection.js"></script>
	
<%-- 	<%
	String billerType="";
	if(session.getAttribute("billerType") != null)
		{billerType=(String)session.getAttribute("billerType");}
	session.removeAttribute("billerType");
	/* if())Electricity  Gas   Insurance   Landline */

	if(billerType.equalsIgnoreCase("Electricity")) {
		%>
		<script>
		getPage('Electricity','#form-section','#electricityLink','GetBillerDetailsB2CElectricity');
		</script>
	<% 
	}
	else if(billerType.equalsIgnoreCase("Gas")) {
		
		%>
		<script>
		getPage('Gas','#form-section','#gasLink','GetBillerDetailsB2CGas');
		</script>
	<% 
	}
	else if(billerType.equalsIgnoreCase("Insurance")) {
		
		%>
		<script>
		getPage('Insurance','#form-section','#insurancLink','GetBillerDetailsB2CInsurance')
		</script>
	<%
	}
	else if(billerType.equalsIgnoreCase("Landline")){ 
		
		%>
		<script>
		getPage('Landline','#form-section','#landlineLink','GetBillerDetailsB2CLandline')
		</script>
	<%
	}else{%>
			<script>
				getPage('Landline','#form-section','#landlineLink','GetBillerDetailsB2CLandline')
				</script>
				<%}
	%>
 --%>
</head>
<body>

<div class="mainContainer">

<jsp:include page="../newUiJsp/header.jsp"></jsp:include>

<div class="contentContainer">
<jsp:include page="../newUiJsp/leftCol.jsp"></jsp:include>
<jsp:include page="../newUiJsp/rightCol.jsp"></jsp:include>




</div>
</div>
<%
	String flag ="";
	String errorsignup="";
	if(session.getAttribute("loginStatus") != null)
	{
		flag=(String)session.getAttribute("loginStatus");
	}
	if(session.getAttribute("errorsignup") !=null)
	{
		errorsignup=(String)session.getAttribute("errorsignup");
	}
	String rechargeFlow = (String)session.getAttribute("rechargeFlow");
	String electriFlow = (String)session.getAttribute("electFlow");
	session.removeAttribute("loginStatus");
	session.removeAttribute("rechargeFlow");

	if(rechargeFlow =="TRUE" ){
		
		%>
		
		<%
	}
%>
<%
	if(flag.equalsIgnoreCase("fail")   || flag.equalsIgnoreCase("first") || flag.equalsIgnoreCase("logoff") || errorsignup.equalsIgnoreCase("errorsignup")){%>
		<script>
	
		$(function(){
			
			

			<%
				
			
			if(flag.equalsIgnoreCase("fail") || flag.equalsIgnoreCase("logoff")  || errorsignup.equalsIgnoreCase("errorsignup") ){
			if(flag.equalsIgnoreCase("logoff"))	{
					session.setAttribute("loginStatus", "logoff");
				}%>
			openLoginPopup("Login");
			
			<%if(errorsignup.equalsIgnoreCase("errorsignup")){
				%>
				
				setTimeout(function(){
					
					$("#signUp-link").trigger("click");	
					
				},500)
			
				
				
			<%
			}
			}else{%>
			openLoginPopup("otp");
			<%}%>
			// $("#login_form").attr('action','IsCustomer.action');
		})
		
		</script>
	<% 

	}
%>
<!-- Modal -->
<div id="popupLogin" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

   
    </div>

  </div>
</div>



  <div class="modal fade oxyPopup" id="rechagePop" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            <h3 class="confirmationHeading">Please confirm the below details to proceed further for payment.</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Recharge Type </th>
        <th id="rechargetype">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mobile Number</td>
        <td id="mobileNo"></td>
        
      </tr>
      <tr>
        <td >Service Provider</td>
        <td id="servicePovider"></td>
        
      </tr>
      <tr>
        <td>Circle </td>
        <td id="rechargecircle"></td>
        
      </tr>
      <tr>
        <td>Amount </td>
        <td id="rechargeAmount"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('mobile_form').submit()">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
<%  

if(rechargeFlow =="TRUE" ){
	
		
		%>
		<div class="modal fade " id="InvoicePop" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
  		<jsp:include page="invoiceData.jsp"></jsp:include>
        </div>
        <div class="modal-footer">
      
          <button type="submit" class="btn btn-default" data-dismiss="modal" >Done</button>
   
        
          
        </div>
      </div>
      
    </div>
  </div>
  <script>
  $(function(){
	  $("#InvoicePop").modal('show')
  })
  </script>
		
		<%
		
	}
%>

  

      <div class="modal fade oxyPopup " id="DTPpopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
             <h3 class="confirmationHeading">Please confirm the below details to proceed further for payment.</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Service Provider </th>
        <th id="servicePoviderDTH">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>DTH Number</td>
        <td id="dthNo"></td>
        
      </tr>
     
      <tr>
        <td>Amount </td>
        <td id="rechargeAmountDTH"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('d2h').submit()">Confirm</button>
        </div>
      </div>
      
      
    </div>
  </div>
  
   <div class="modal fade oxyPopup" id="dataCardPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recharge Confirmation</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
            
             <h3 class="confirmationHeading">Please confirm the below details to proceed further for payment.</h3>
            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th >Recharge Type </th>
        <th id="rechargetypeDataCard">Recharge Type </th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mobile Number</td>
        <td id="mobileNoDataCard"></td>
        
      </tr>
      <tr>
        <td >Service Provider</td>
        <td id="servicePoviderDataCard"></td>
        
      </tr>
      <tr>
        <td>Circle </td>
        <td id="rechargecircleDataCard"></td>
        
      </tr>
      <tr>
        <td>Amount </td>
        <td id="rechargeAmountDataCard"></td>
        
      </tr>
    </tbody>
  </table>
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('datacard').submit();">Confirm</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
  
  <div class="modal fade oxyPopup" id="ifscCodePopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
   <div class="modal-content" style="width: 1000px;
    margin-left: -153px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">IFSC Search</h4>
        </div>
        <div class="modal-body">
       <div class="container-fluid">
  
       	<div class="row oxyForm">
   
       	<div class="col-md-2">
       			<input type="text" placeholder="IFSC" class="transformToUpperCase" id="ifsCode">
       	
       	</div>
       		<div class="col-md-4">
       				<select id="ifscBankName">
       			</select>
       	
       	</div>
       		<div class="col-md-2">
       		 
       	
       	<input type="text" placeholder="BANK BRANCH" id="ifscbranchName" class="transformToUpperCase" >
       	</div>
       	
       		<div class="col-md-2">
       			<input type="text" placeholder="STATE" class="transformToUpperCase" id="ifscState">
       	
       	</div>
       	<div class="col-md-2">
       	<button  class="oxySubmitBtn" style="padding: 7px;" onclick="searchIFSCode(true)" >Search </button>
       	</div>
       </div>
    
   
       
       <div class="row">
       	 <table class="table table-bordered table-striped" id="IfscTable">
    <thead>
      <tr>
        <th>Bank Name</th>
          <th>IFSC Code</th>
        <th>Branch Name</th>
          <th>City</th>
          <th>State</th>
            <th>Select</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
       
       
       </div>
     
            
 
</div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         
        </div>
      </div>
      
    </div>
  </div>
  

 <%session.removeAttribute("electFlow");
 if(electriFlow =="TRUE" ){
		
		%>
		
		
		<div class="modal fade" id="billerPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Biller Information</h4>
        </div>
        <div class="modal-body">
        
   <jsp:include page="../newUiJsp/billerInfoTable.jsp"></jsp:include>

<%@page import="com.bhartipay.biller.vo.PayLoadBean" %>

<%
PayLoadBean resPayload=(PayLoadBean)session.getAttribute("resPayLoad");

%>
        </div>
        <div class="modal-footer">
     
<!--         	<input type="hidden" id="txnId" value="" />
        	<input type="hidden" id="billerAmount" value="" />
        	  	<input type="hidden" id="billerType" value="" />
        	 -->
     
          <button type="button" class="btn btn-default" id="doneBTN" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary btn-info" id="proceedBiller" onclick="billerConfirm('<%=resPayload.getBillerAmount()%>','<%=resPayload.getTxnId()%>','<%=resPayload.getBillerType()%>', '${csrfPreventionSalt}'  );">Proceed</button>
        </div>
      </div>
      
    </div>
  </div>
    <script>
  $(function(){
	  $("#billerPopup").modal('show')
  })
  </script>
		<%
	
		session.removeAttribute("resPayLoad");
	}%>

  

  <div id="TransactionPop" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Transaction Details</h4>
      </div>
      <div class="modal-body">
      <table class="table table-bordered">
 
    <tbody>

    </tbody>
  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




	
			  <div id="profileInfo" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:350px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">My Account</h4> 
      </div>

      <div class="modal-body">
     

   
      </div>
  
      <div class="modal-footer">
      <button type="button" class="btn btn-default" id="savEditedChanges" style="display:none" onclick="submitVaForm('#saveEditChanges',this)">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<%
	if(request.getAttribute("editMsg") != null){
		session.setAttribute("editMsg",(String)request.getAttribute("editMsg"));
		%>
		
		<script>
		$(function(){
			getUserInfo();
		})
	
		</script>
		<%
	}
%> 
<script src="./js/jquery.dd.min.js"></script>
<script src="./js/payplutus.js" type="text/javascript"></script>



</body>
</html>

