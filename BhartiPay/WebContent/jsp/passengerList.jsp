
<%@page import="com.bhartipay.airTravel.bean.Response" %>
<%@page import="com.bhartipay.airTravel.bean.PassangerDetails" %>
<%@page import="java.util.Iterator" %>
<%@taglib prefix="s" uri="/struts-tags"%>

<%
Response resp = new Response();
int attCan=0;
if(request.getAttribute("cancelTicket") != null)
{
	attCan=1;
	resp =(Response)request.getAttribute("cancelTicket");
}
else 
{
	resp =(Response)session.getAttribute("ticketDetails");
}
%>

		 <form action="cancelTicket" method="post" id="passengerDetails<%=resp.getPassengerList().get(0).getPnr()%>"  onsubmit="cancelTicket('<%=resp.getPassengerList().get(0).getPnr()%>',event)">
		 <input type="hidden" value="<%=resp.getPassengerList().get(0).getPnr()%>" class="cnPnr" name="cancelReq.pnr" />
			<input type="hidden" value="<%=resp.getPassengerList().get(0).getTripId()%>" class="cnId" name="cancelReq.travelId" />
<div class="container-fluid">

<div class="table-responsive">  
<font style="color:red;">

		<s:actionerror/>
		</font>
		<font style="color: green;">
		<s:actionmessage/>
		</font>
<h2>Passenger List <div style="color: #989595;
    font-size: 12px;
    margin: 6px 0;"><% if(attCan == 0)
    {
    	if(resp.getCancel().getIsRefundable().equalsIgnoreCase("true")){%> Refundable <%}else{%> Non-Refundable<%} 
    	}%></div></h2>        


  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Ticket No</th>
        <th>Passenger Name</th>
        <th>PNR</th>
        <th>Ticket Status</th>
        
      </tr>
    </thead>
    <tbody>
      
      <%Iterator<PassangerDetails> itr = resp.getPassengerList().iterator();
      int remainingPass=0;
      while(itr.hasNext()){
      PassangerDetails pass = itr.next();
      %><tr>
        <td><%=pass.getTicketId() %></td>
        <td><%=pass.getPassangername() %></td>
        <td><%=pass.getPnr() %></td>
        <td><%if(pass.getIsPnrCancelled().equals("false")){remainingPass++;%><input type="checkbox" value="<%=pass.getTicketId() %>" name="cancelReq.ticketIdList">
      <%}else if(pass.getIsPnrCancelled().equals("true")){%>Cancelled<%}else{%><%=pass.getIsPnrCancelled() %> <span class="glyphicon glyphicon-repeat" style="cursor:pointer;font-size: 11px;
    margin: 0 0 0px 28px;" onclick="refreshStatus('<%=pass.getTicketId() %>')" title="Refresh Status"> </span><%} %>
      
    
         </td>
           </tr>
       <%} %>
  
    </tbody>
  </table>

  </div>
  
<%if(remainingPass > 0){ %>

   <select name="cancelReq.cancellationType" style="width: 100px;
    margin: 0 0 0px 9px;">
        <option value="0" >Not Set</option>
          <option value="1" >No Show</option>
            <option value="2" >Flight Cancelled</option>
              <option value="3" >Others</option>
        </select>
<input type="hidden" id="ticketId" name="cancelReq.ticketIdList" > 
<input class="btn btn-info pull-right "  type="submit" style="margin: 0 0 29px 0;" value="Cancel Ticket(s)"><%} %>


</div>
</form>
<script>
function refreshStatus(id){
	$("#ticketId").val(id);
	$("#passengerDetails<%=resp.getPassengerList().get(0).getPnr()%>").submit();
}


</script>

