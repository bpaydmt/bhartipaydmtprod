
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 <script type="text/javascript">

function getWalletConfigByUserId(userId)
{  
	$.ajax({
        url:"GetWalletConfigByUserId",
        cache:0,
        data:"config.id="+userId,
        success:function(result){
               document.getElementById('limitform').innerHTML=result;
               
         }
  });  
	return false;
}
</script>
</head>

<% 
User user = (User) session.getAttribute("User");
%>
       
       <body>
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Edit Commission</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">  
									<div class="row">
									
             					 <div class="col-md-12">
             					 <font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>

											
												<%-- 	<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Select Plan</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<s:select list="%{plans}" headerKey="-1"
																		headerValue="Select plan" id="plan"
																		name="cpmaster.planid" cssClass="form-username"
																		/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Transaction Type</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																	<s:select list="%{txnType}" headerKey="-1"
																		headerValue="Select transaction type" id="txnType"
																		name="cpmaster.txnid" cssClass="form-username"
																		/>

																</div>
															</div>
														</div>
														
														<div class="col-md-4" style="margin-top: 50px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Submit</button>
														</div>

													</div> --%>

												</div>	
												  
<!-- *********************************************************************************************************************** -->
<%
String show=(String)request.getAttribute("show");
if(show!=null&&show.equalsIgnoreCase("true")){
%>
										
									<div id="limitform" >
								<form action="SaveAssignCommission" method="post"> 
				
												 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Fixed charge</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="hidden" name="cpmaster.servicetax" value="14.00">
																<input type="hidden" name="cpmaster.sbcess" value=".50">
																<input type="hidden" name="cpmaster.kycess" value=".50">
																<input type="hidden" name="cpmaster.bankrefid" value="0">
																<input type="hidden" name="cpmaster.planid" value="<s:property value='%{cpmaster.planid}'/>">
																<input type="hidden" name="cpmaster.txnid" value="<s:property value='%{cpmaster.txnid}'/>">
																<input type="hidden" name="cpmaster.commid" value="<s:property value='%{cpmaster.commid}'/>">
																<input type="text"
																value="<s:property value='%{cpmaster.fixedcharge}'/>"
																name="cpmaster.fixedcharge" id="fixedcharge" class="form-control"
																placeholder="Fixed charge" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range from</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.rangefrom}'/>"
																name="cpmaster.rangefrom" id="rangeFrom" class="form-control"
																placeholder="Range from" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Range to</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.rangeto}'/>"
																name="cpmaster.rangeto" id="rangeTo" class="form-control"
																placeholder="Range to" required/>

																</div>
															</div>
														</div>

													</div>
													
												
													
													
													
														 <div class="row">
														<div class="col-md-8" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Description</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.description}'/>"
																name="cpmaster.description" id="desc" class="form-control"
																placeholder="Description" required/>

																</div>
															</div>
														</div>
															
					

													</div>
																			
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Commission %</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.commperc}'/>"
																name="cpmaster.commperc" id="commperc" class="form-control"
																placeholder="Commission %" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Aggregator %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.aggregatorperc}'/>"
																name="cpmaster.aggregatorperc" id="aggregatorperc" class="form-control"
																placeholder="Aggregator %" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Distributor %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.distributorperc}'/>"
																name="cpmaster.distributorperc" id="distributorperc" class="form-control"
																placeholder="Distributor %" required/>

																</div>
															</div>
														</div>

													</div>
													

										
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Agent %</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.agentperc}'/>"
																name="cpmaster.agentperc" id="agentperc" class="form-control"
																placeholder="Agent %" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Sub-Agent %</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{cpmaster.subagentperc}'/>"
																name="cpmaster.subagentperc" id="subagentperc" class="form-control"
																placeholder="Sub-Agent %" required/>

																</div>
															</div>
														</div>
													<div class="col-md-4" style="margin-top: 50px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Submit</button>
														</div>

													</div>
												
													
													<div class="row">
														
													</div>
													

													<div class="row"></div>
													<div class="clearfix"></div>
												</form>
									
									
									
									</div>
									
									<%
}
									%>
									</div>
									
								

                </div>
                

    
            </div>
            
            </br>
              
        </div>
  
</div>

</div>
</div>

</div>
        
        
</div>
        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


