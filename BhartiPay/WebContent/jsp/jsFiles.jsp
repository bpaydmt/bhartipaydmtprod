<!-- <script src="./js/jquery.min.js"></script> -->


<!-- Latest compiled JavaScript -->
<script src="./js/vendor/jquery-3.1.0.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!-- jQuery -->

<!-- Tooltipster -->
<script src="./js/vendor/jquery.tooltipster.min.js"></script>
<!-- Owl Carousel -->
<script src="./js/vendor/owl.carousel.min.js"></script>
<!-- Tweet -->
<script src="./js/vendor/twitter/jquery.tweet.min.js"></script>
<!-- Side Menu -->
<script src="./js/side-menu.js"></script>
<!-- Home -->
<script src="./js/home.js"></script>
<!-- Tooltip -->
<script src="./js/tooltip.js"></script>
<!-- User Quickview Dropdown -->
<script src="./js/user-board.js"></script>
<script src="./js/vendor/jquery.xmtab.min.js"></script>
<script src="./js/post-tab.js"></script>
<!-- Footer -->
<script src="./js/footer.js"></script>
<script src="./js/validateWallet.js"></script>