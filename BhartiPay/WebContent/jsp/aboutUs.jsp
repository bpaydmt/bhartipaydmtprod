<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">


<title>Bhartipay | About Us</title>
<jsp:include page="cssFiles.jsp"></jsp:include>
</head>
<body>

	<!-- HEADER -->
		<jsp:include page="mainHeader.jsp"></jsp:include>
	<!-- /HEADER -->

	<!-- MAIN MENU -->
		<jsp:include page="mainMenuOption.jsp"></jsp:include>
	<!-- /MAIN MENU -->

	<!-- SECTION -->

	<div class="section-wrap">
		<div class="section">
			<!-- CONTENT -->
			<div class="content center">
				<!-- POST -->
				<article class="post blog-post">
					<!-- POST IMAGE 
					<div class="post-image">
						<figure class="product-preview-image large liquid">
							<img src="" alt="">
						</figure>
					</div>
					< /POST IMAGE -->

					<!-- POST CONTENT -->
					<div class="post-content with-title">
						<p class="text-header big">About Us</p>
						
						<!-- POST PARAGRAPH -->
						<div class="post-paragraph">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. Ut enim ad minim veniam, quis nostrud exercitation
								ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
								aute irure dolor in henderit in voluptate velit esse cillum
								dolore eu fugiat nulla pariatur. Excepteur sint occaecat
								cupidatat non proident, sunt in culpa qui officia deserunt
								mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. Ut enim ad minim veniam, quis nostrud exercitation
								ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
								aute irure dolor in reprehenderit in voluptate velit esse cillum
								dolore eu fugiat nulla pariatur.</p>
						</div>
						<!-- /POST PARAGRAPH -->

						<!-- POST PARAGRAPH -->
						<div class="post-paragraph">
							<h3 class="post-title"></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. Ut enim ad minim veniam, quis nostrud exercitation
								ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
								aute irure dolor in henderit in voluptate velit esse cillum
								dolore eu fugiat nulla pariatur. Excepteur sint occaecat
								cupidatat non proident, sunt in culpa qui officia deserunt
								mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. Ut enim ad minim veniam, quis nostrud exercitation
								ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
								aute irure dolor in reprehenderit in voluptate velit esse cillum
								dolore eu fugiat nulla pariatur.</p>
						</div>
						<!-- /POST PARAGRAPH -->
					</div>
					<!-- /POST CONTENT -->

					<hr class="line-separator">
				</article>
				<!-- /POST -->


			</div>
			<!-- CONTENT -->


		</div>
	</div>

	
	
		<jsp:include page="footer.jsp"></jsp:include>
	

</body>
</html>