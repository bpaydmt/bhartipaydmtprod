<%@page import="com.bhartipay.wallet.aeps.AEPSLedger"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
<% 
User user = (User) session.getAttribute("User");

DecimalFormat d=new DecimalFormat("0.00");

%>
      
       


            


<div class="box2 col-md-12">
	<div class="box-inner">
	
		
		
		<div class="box-content row">
		
			<form id="aepsForm" >


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<input	type="hidden" name="inputBean.userId" id="userid" value="<s:property value='%{inputBean.userId}'/>"/>	
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="aepsdpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									 <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="aepsdpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left ">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="aepsReportTable" class="display">
				<thead>
				
				
					<tr>
							<th width="3%" style="word-wrap: break-word;"><u>Sr.No</u></th>
							<th width="6%" style="word-wrap: break-word;"><u>Txn Date</u></th>
							<%if(user.getUsertype() != 2){ %>
							<th width="5%" style="word-wrap: break-word;"><u>Agent Id</u></th>
							<%} %>
							<th width="5%" style="word-wrap: break-word;"><u>Txn Id</u></th>
							
							<th width="5%" style="word-wrap: break-word;"><u>Txn Type</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Bank Name</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Mobileno</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Aadhar no</u></th>
							<th width="5%"><u>RRN</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Amount</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Commission</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Status</u></th>
							<th width="5%" style="word-wrap: break-word;display:none;"><u>Bank Response Msg</th>

								
						</tr>
				</thead>
				<tbody>
				<%
				List<AEPSLedger>list=(List<AEPSLedger>)request.getAttribute("aepsList");

				if(list!=null){
					
					int j=1;
				for(int i=0; i<list.size(); i++) {
					AEPSLedger tdo=list.get(i);
					
					%>

		          	 <tr>
		          	 <td><%=j++ %></td>
		          	 <td ><%=tdo.getTxnDate()%></td>
		          	 <%if(user.getUsertype() != 2){ %>
		             <td><%=tdo.getAgentId()%></td>
		             <%} %>
		             <td><%=tdo.getTxnId()!=null?tdo.getTxnId():"-"%></td>
		             <td><%=tdo.getTxnType()!=null?tdo.getTxnType():"-"%></td>
		             <td><%=tdo.getBcname()!=null?tdo.getBcname():"-"%></td>
		             
		             <td><%=tdo.getWalletId()!=null?tdo.getWalletId().substring(0, 10):"-"%></td>
		              <td style="word-wrap: break-word;"><%=tdo.getAadharNumber()!=null?tdo.getAadharNumber():"-"%></td>
		             <td><%=tdo.getRrn()!=null?tdo.getRrn():"-"%></td>
		             
		             <td><%=tdo.getAmount()%></td>
		             <td><%=tdo.getCommissionAmt()!=0?tdo.getCommissionAmt():"-"%></td>
		             
		             <td><%=tdo.getStatus()!=null?tdo.getStatus():"-"%></td>
		             
		              
		             
		              
		             <%-- <td style="word-wrap: break-word;display:none;"><%=tdo.getBankAuth()!=null?tdo.getBankAuth():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getProcessingCode()!=null?tdo.getProcessingCode():"-"%></td>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankResponseCode()!=null?tdo.getBankResponseCode():"-"%></td> --%>
		             <td style="word-wrap: break-word;display:none;"><%=tdo.getBankResponseMsg()!=null?tdo.getBankResponseMsg():"-"%></td>
		             <%-- <td style="word-wrap: break-word;display:none;"><%=tdo.getStatusMessage()!=null?tdo.getStatusMessage():"-"%></td> --%>

                  	</tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

<script>

$(function(){
	$("#aepsForm").submit(function(e) {


	    $.ajax({
	           type: "POST",
	           url: "GetCustAepsReport.action",
	           data: $("#aepsForm").serialize(), 
	           success: function(data)
	           {
	        	   $("#aepsReport").html(data);
	   		    
	   		    $('#aepsReportTable').DataTable( {
	   		        dom: 'Bfrtip',
	   		        autoWidth: false,
	   		        order: [[ 0, "desc" ]],
	   		        buttons: [
	   		             {
	   		             
	   		                extend: 'copy',
	   		                text: 'COPY',
	   		                title:'AEPS Details - ' + '<%= currentDate %>',
	   		                message:'<%= currentDate %>',
	   		            },  {
	   		             
	   		                extend: 'csv',
	   		                text: 'CSV',
	   		                title:'AEPS Details - ' + '<%= currentDate %>',
	   		              
	   		            },{
	   		             
	   		                extend: 'excel',
	   		                text: 'EXCEL',
	   		                title:'AEPS Details - ' + '<%= currentDate %>',
	   		            
	   		            }, {
	   		             
	   		                extend: 'pdf',
	   		                text: 'PDF',
	   		                title:'AEPS Details - ' + '<%= currentDate %>',
	   		                message:" "+ "<%= currentDate %>" + "",
	   		               
	   		            },  {
	   		             
	   		                extend: 'print',
	   		                text: 'PRINT',
	   		                title:'AEPS Details - ' + '<%= currentDate %>',
	   		              
	   		            }
	   		        ]
	   		    } );
	   		    
	   		    
	   		    $('#aepsdpStart').datepicker({
	   		     language: 'en',
	   		     autoClose:true,
	   		     maxDate: new Date(),
	   		     

	   		    });
	   		 
	   		     

	   		     
	   		 
	   		    $("#aepsdpStart").blur(function(){
	   		       $('#aepsdpEnd').val("")
	   		     $('#aepsdpEnd').datepicker({
	   		          language: 'en',
	   		         autoClose:true,
	   		         minDate: new Date(converDateToJsFormat($('#aepsdpStart').val())),           
	   		         maxDate: new Date(),
	   		         
	   		        }); 
	   		    })
	         	}
	   		
	   		
	    })
	           
	    

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});	
})



</script>




