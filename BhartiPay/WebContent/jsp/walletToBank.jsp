<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 <script type="text/javascript">

function getPassbookSummary(wallet,stDate,endDate)
{  
	alert(wallet);
	alert(stDate);
	alert(endDate);
	$.ajax({
        url:"PassbookSummary",
        cache:0,
        data:"inputBean.walletId="+wallet+"&inputBean.stDate="+stDate+"&inputBean.endDate="+endDate,
        success:function(result){
               document.getElementById('transactionSummary').innerHTML=result;
         }
  });  
	return false;
}
/* 
function findAllCommDetails(userid,from,to,imid)
{  
	
	$.ajax({
        url:"FindAllCommDetails.action",
        cache:0,
        data:"userid="+userid+"&from="+from+"&to="+to+"&mid="+imid,
        success:function(result){
        	
               document.getElementById('findAllCommDetails').innerHTML=result;
         }
  });
	
	return false;
} */
</script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Wallet to Bank money transfer</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">
													
 <form action="SendWtoBMoney" method="post">
 <%
 if(user.getUsertype()!=1){
 %>
 Sender details
  <div class="row" style="border-top: 2px solid #87c440;">
                                      
                                     <div class="col-md-4" style="margin-top:30px;">
                                            <div class=" form-group">
                                            <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div>


                                                   
                                                <input type="text" value="<s:property value='%{wtob.sendarname}'/>" name="wtob.sendarname" id="ifsccode" class="form-control"  placeholder="Sender Name" required>
                                                
                                            </div> 
                                            </div>       
                                        </div>

                                        
                                        	<div class="col-md-4" style="margin-top:30px;">
                                            	<div class="form-group">
                                                 <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-money"></span>
    </div>
                                                <input type="text" value="<s:property value='%{wtob.sendarmobile}'/>" name="wtob.sendarmobile" id="trxAmount" class="form-control"  placeholder="Sender mobile number" required>
                                               </div>
                                            </div>
                                          </div>
                                     
                                          <div class="col-md-4" style="margin-top:30px;">
                                            	<div class="form-group">
                                                 <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-money"></span>
    </div>
                                                <input type="text" value="<s:property value='%{wtob.sendaremail}'/>" name="wtob.sendaremail" id="trxAmount" class="form-control"  placeholder="Sender email" required>
                                               </div>
                                            </div>
                                          </div>
                                          
                                          
                                   
                                  
                                    </div>
 <%} %>
 Reciever details
                                    <div class="row" style="border-top: 2px solid #87c440;">
                                        <div class="col-md-4" style="margin-top:30px;">
                                            <div class=" form-group">
                                            <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div>

<!-- private String wtbtxnid;
	private String payeename;
	private String accno;
	private String ifsccode;
	private double amount;
	private String mobileno;
	private String status;
	private String wallettxnstatus;
	private String bankrefNo;
	private String banktxnstatus;
	private String ipimei;
	private Date txndate; -->

                                                   <input type="hidden" name="wtob.userId" id="userid" value="<%=user.getId()%>">
                                                    <input type="hidden" name="wtob.walletId" id="userid" value="<%=user.getWalletid()%>">
                                                   
                                                <input type="text" value="<s:property value='%{wtob.payeename}'/>" name="wtob.payeename" id="payeename" class="form-control"  placeholder="Receiver Name" required>
                                                
                                            </div> 
                                            </div>       
                                        </div>

                                        
                                        	<div class="col-md-4" style="margin-top:30px;">
                                            	<div class="form-group">
                                                 <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-money"></span>
    </div>
                                                <input type="text" value="<s:property value='%{wtob.accno}'/>" name="wtob.accno" id="accno" class="form-control"  placeholder="Receiver Account Number" required>
                                               </div>
                                            </div>
                                          </div>
                                     
                                          
                                               <div class="col-md-4" style="margin-top:30px;">
                                            <div class=" form-group">
                                            <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div>


                                                   
                                                <input type="text" value="<s:property value='%{wtob.ifsccode}'/>" name="wtob.ifsccode" id="ifsccode" class="form-control"  placeholder="Bank IFSC" required>
                                                
                                            </div> 
                                            </div>       
                                        </div>
                               
                                           </div>
                                           
                                        <div class="row">
                                      
                                

                                        
                                        	<div class="col-md-4" style="margin-top:30px;">
                                            	<div class="form-group">
                                                 <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-money"></span>
    </div>
                                                <input type="text" value="<s:property value='%{wtob.amount}'/>" name="wtob.amount" id="trxAmount" class="form-control"  placeholder="Amount" required>
                                               </div>
                                            </div>
                                          </div>
                                     
                                          
                                          
                                          
                                   
                                  
                                   
                                        <div class="col-md-4" style="margin-top:30px;">
                                            <div class=" form-group">
                                            <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div>


                                                <input type="text" value="<s:property value='%{wtob.mobileno}'/>" name="wtob.mobileno" id="mobileno" class="form-control"  placeholder="Receiver Mobile No" required>
                                                
                                            </div> 
                                            </div>       
                                        </div>
                                        
                                           <div class="col-md-4" style="margin-top:30px;">
                                            <div class=" form-group">
                                            <div class="input-group">
                                                 <div class="input-group-addon">
      <span class="glyphicon glyphicon-user"></span>
    </div>


                                                <input type="text" value="<s:property value='%{wtob.email}'/>" name="wtob.email" id="email" class="form-control"  placeholder="Receiver Email" required>
                                                
                                            </div> 
                                            </div>       
                                        </div>

                                           </div>


													<div class="row">
														
														<div class="col-md-4" style="margin-top: 30px;">
															<div class="form-gr">

																<input type="submit"
																	class="btn btn-info btn-fill pull-left" value="Send">


															</div>
														</div>
													</div>
													<div class="clearfix"></div>
                                </form>

									
									</div>
									
									</div>
									
								

                </div>
                

    
            </div>
            
              
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


