<%@page import="org.omg.PortableInterceptor.USER_EXCEPTION"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.bhartipay.biller.bean.BillDetails"%>

<%
	User user = (User) session.getAttribute("User");
%>


<jsp:include page="theams.jsp"></jsp:include>
<%@include file="reqFiles.jsp"%>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<link href='./css/rechargeUi.css' rel='stylesheet'>
<link href='./css/dd.css' rel='stylesheet'>
<jsp:include page="head.jsp"></jsp:include>


<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
	rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src='https://kit.fontawesome.com/a076d05399.js'></script>



<script>
	function changeDTHPlaceholderAndLen(el, targetInp) {
		var elv = $(el).val()
		var tar = $(targetInp)
		var le = 12
		var ph = 'Enter your DTH number'
		switch (elv) {
		case 'AIRTEL DIGITAL TV':
			le = 10
			ph = 'Customer ID'
			tar.attr('Placeholder', 'Customer ID', 'maxlength', '10');
			break;
		case 'DISH TV':
			le = 11
			ph = 'Viewing Card Number'

			break;
		case 'RELIANCE DIGITAL TV':
			le = 12
			ph = 'Smart Card Number'

			break;
		case 'VIDEOCON D2H':
			le = 10
			ph = 'Customer ID'

			break;
		case 'TATA SKY':
			le = 10
			ph = 'Subscriber ID'

			break;
		case 'SUN DIRECT':
			le = 11
			ph = 'Smart Card Number'
			break;

		default:
			le = 12

		}
		tar.attr('Placeholder', ph)
		tar.attr('maxlength', le)
	}
	
	function resetTab(e) {

		e.preventDefault();
		e.preventDefault();

		
		var activeMenueList =  document.getElementsByClassName('activeSubmenue');
		for(let i = 0; i < activeMenueList.length; i++ ){
			activeMenueList[i].classList.remove('activeSubmenue');
		}

		e.target.classList.add('activeSubmenue');
		

		$("input[type='text']").val('')
		$('select').val('')
		$(".errorMsg").hide()

		// Failing the above, you could use this, however the above is recommended
		return false;
	}
	$(
			function() {
				$(
						"#rechargeOperator, #circlei, #dth_operator, #datacard_operator, #datacard_circle")
						.msDropDown();
			})
</script>


<script >
<!--

//-->
$(document).ready(function() {
    $("input[type=radio]").change(function() {
      $("select").removeClass('myVisible myHide');
      if (connection_type_prepaid.checked) {
        $('#rechargeOperator').addClass('myVisible');
        $('#rechargeOperatorPost').addClass('myHide'); 
      }
      if (connection_type_postpaid.checked) {
        $('#rechargeOperator').addClass('myHide');
        $('#rechargeOperatorPost').addClass('myVisible'); 
      }
      
    });

  });

 

 function hideFieldData()
 {
	 debugger
	// document.getElementById('rcpost').style.visibility = 'hidden';
	 document.getElementById("rcpost").style.display = 'none';    
	 

  }



</script>


<style>

.myHide {
  display: none;
}

.myVisible {
  display: block;
}

.errorMsg {
	right: 12%;
	top: 57px;
}

.chosen-container {
	width: 90% !important;
}

.chosen-container-single .chosen-single {
	padding: 9px 0 8px 8px !important;
}

.chosen-container-active.chosen-with-drop .chosen-single div b {
	background-position: -18px 4px;
}

.col-7-width {
	/*border-top: none !important;
		border-left: none !important;
		border-right: none !important;
		border-bottom: 2px solid #ccc;*/
	border: 2px solid #ccc;
	background-color: transparent !important;
	color: #666 !important;
	border-radius: 25px !important;
	display: block !important;
	float: left !important;
	width: 100% !important;
	/*padding-top: 10px !important;*/
	/* padding-bottom: 10px !important;*/
}

.newTabButton {
	display: flex;
	justify-content: space-around;
	padding: 20px 0px;
}

.dd .ddTitle {
	border: 2px solid #ccc !important;
}

.newTabButtonWrapper {
	display: flex;
	justify-content: space-around;
	padding: 20px 0px;
}

.newTabButton {
	width: 10% !important;
}

.tab {
	display: block;
}

.tab:hover {
	color: #666 !important;
}

.dd .ddcommon .borderRadius {
	/*z-index: 1000 !important;*/
	
}

.bank-txt {
	margin-bottom: 10px;
	margin-top: 10px;
}

.dd {
	line-height: 10px;
	width: 100% !important;
}

.newRechargeUi .payplutus_tab_active a:hover, .newRechargeUi .payplutus_tab_active a,
	.newRechargeUi .payplutus_tab_active a:focus {
	/* position: relative;
        top: 11px;*/
	
}

.newRechargeUi .payplutus_tab a {
	/*position: relative;
        top: 11px;*/
	
}

.close {
	float: right !important;
	font-size: 16px;
	font-weight: bold;
	line-height: 1;
	color: #fff;
	text-shadow: none;
	opacity: 1;
	padding: 7px !important;
	background: #a57225 !important;
}

.newRechargeUi .payplutus_formshadow {
	border: 2px solid #d8d8d8 !important;
}

smart-style-3 input[type=text] {
	margin-top: 0px;
}


		
		
/* 		=============================================================
 */		
		/* recharge menue styling start  */
/* ===========================================================
 */		
		
		
		
		
		.submenuOuterWrapper{
			float: left;
			width: 100%;
			background-color: #2a204a;
			height: 200px;

		}
		.subMenueWrapper{
			float: left;
			width: 100%;
			display: flex;
			justify-content: flex-start;
			align-items: center;
			color: #fff;
			list-style-type: none;
			padding: 20px;
		}
		.subMenueWrapper li{

		}
		.subMenueWrapper li a{
			color: #fff;
			display: flex;
			justify-content: center;
			align-items: center;
			flex-flow: column;
			text-decoration: none;
			font-size: 12px;
			text-transform: uppercase;
			padding: 10px;
			opacity: .8;
			transition: all .2s;

		}
		.subMenueWrapper li a.activeSubmenue{
		opacity:1;
		font-weight:600;
		}
		
		.subMenueWrapper li a:hover{
			opacity: 1;
		}
		.subMenueWrapper li a i{
			font-size: 15px;
			margin-bottom: 15px;
		}
		.rechargeFormWrapper{
		    box-shadow: 0 0 15px 0 rgba(0,0,0,.08);
		    margin-top: 20px;
		    padding: 20px 0px 20px 0px;
		    width: calc(100% - 40px);
		    margin: auto;
		    background-color: #fff;
		    overflow: hidden;
		    min-height: 500px;
		        position: relative;
    top: -100px;
    z-index: 1;
		}
		.clearfix::after {
			content: "";
			clear: both;
			display: table;
		}
				.modal{
			    background-color: #00000059;
          z-index: 99999999999;
		}
		.modal-dialog{
		top:200px;
		
		}
		
		
</style>

</head>

<body onload="hideFieldData();">

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->

	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<%-- 	<div id="main" role="main">
		<div id="content">
			<div class=" row">
				<div class="col-md-12">
					<div class="newRechargeUi no-chosen-drop">
						<h3 style="font-size: 18px">
							<font style="color: red;"> <%
 	if (session.getAttribute("rechargeError") != null) {
 %> <%=session.getAttribute("rechargeError")%> <%
 	session.removeAttribute("rechargeError");
 	}
 %>
							</font> <font style="color: green;"></font>
						</h3>
						<div
							class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payplutus_formshadow">
							<div class="loader"></div>


							<!-- Left Buttons Start -->
							<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 newTabButtonWrapper">
								<div class="col-7-width payplutus_tab_active newTabButton"
									id="mb">
									<a href="#mobileRecharge" class="tab" onclick="resetTab(event)"
										id="mobile_recharge"> <span class="r-icon mobile"></span>
										Mobile
									</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="dh">
									<a href="#dthRecharge" onclick="resetTab(event)" class="tab"
										id="dth_recharge"><span class="r-icon dth"></span>DTH</a>
								</div>
								<div class="col-7-width  payplutus_tab newTabButton" id="dc">
									<a href="#datacardRecharge" onclick="resetTab(event)"
										class="tab" id="datacard_recharge"><span
										class="r-icon datacard"></span>Data card</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="el">
									<a href="#electricityRecharge" onclick="resetTab(event)"
										class="tab" id="electricity_recharge"><span
										class="r-icon datacard"></span>Electricity</a>
								</div>
								<div class="col-7-width  payplutus_tab newTabButton" id="gr">
									<a href="#gasRecharge" onclick="resetTab(event)" class="tab"
										id="gas_recharge"><span class="r-icon datacard"></span>Gas</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="lb">
									<a href="#landlinebill" onclick="resetTab(event)" class="tab"
										id="landline_Recharge"><span class="r-icon datacard"></span>Landline</a>
								</div>
								<div class="col-7-width payplutus_tab newTabButton" id="ir">
									<a href="#insuranceRecharge" onclick="resetTab(event)"
										class="tab" id="insurance_recharge"><span
										class="r-icon datacard"></span>Insurance</a>
								</div>
							</div>
							<!-- Left Buttons End -->


							<!--  1st Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="mobile_recharge_form">
								<form method="post" id="mobile_form"
									action="RechargeService.action">
									<!-- <div class="col-md-1"></div>-->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">


										<!-- Prepaid with radio button start -->

										<div class="col-md-6 bank-txt">
											<input type="radio" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type"
												value="PREPAID" checked onClick="hide_plan();"> <label
												class="">Prepaid</label>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="radio" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type"
												value="POSTPAID" onClick="hide_plan();"> <label
												class="">Postpaid</label>
										</div>
										<!--  End -->


										<!--  Enter your mobile number start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												class="mandatory" placeholder="Enter your mobile number"
												autocomplete="off" id="rechargeMobile"
												onkeyup="getOpretorDetails(this,'mobile')" />
										</div>
										<!--  End -->

										<!-- Enter amount start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="amount"
												class="payplutus_textbox  form-control amount-vl mandatory"
												required placeholder="Enter amount" maxlength="4" />
											<div class=" customSelectErr" id="amountGreater10"
												style="display: none;">Amount should be greater than
												10.</div>

										</div>
										<!--  End -->

										<!-- dropdown Star -->

										<div class="col-md-6 bank-txt">
											<s:select list="%{prePaid}"
												name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;" cssClass="" id="rechargeOperator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);validateSelectOnChange(this,'#opratorErr');" />

											<div class="errorMsg customSelectErr" id="opratorErr"
												style="display: none;">Please select the operator</div>
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="circlei"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);validateSelectOnChange(this,'#opratoCireclrErr');" />

											<div class="errorMsg customSelectErr" id="opratoCireclrErr"
												style="display: none;">Please select the circle</div>
										</div>
										<!--  End -->



										<!-- Proceed to pay Submit Button start -->
										<div class="col-md-6 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												style="clear: left" value="Proceed to Pay"
												class="btn submit-form btn-success"
												data-open-popup="rechage"
												onclick="submitVaForm('#mobile_form',this)">
										</div>
										<!--  End -->

									</div>
									<!-- <div class="col-md-1"></div> -->
								</form>

							</div>
							<!-- 1st Body Contents End -->


							<!--  2nd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="d2h_recharge_form" style="display: none;">

								<form method="post" id="d2h" action="RechargeService.action">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">

										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>"> <input type="hidden"
											name="rechargeBean.rechargeType" value="DTH">



										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="dth_mobile_no" class="mandatory" required
												placeholder="Enter your DTH number" maxlength="12" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_mobile_noErr">Please Enter DTH Number</div>
										</div>

										<div>
											<input type="hidden" name="rechargeBean.rechargeCircle"
												value="Delhi">
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="dth_amount" class="mandatory" placeholder="Enter amount"
												maxlength="4" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErr">Please enter Amount</div>
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErrGreat10">Please Amount Greater than
												20.</div>
										</div>


										<div class="col-md-6 bank-txt">
											<s:select list="%{dth}" name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;"
												onchange="changeDTHPlaceholderAndLen(this,' #dth_mobile_no')"
												cssClass="mandatory" id="dth_operator" headerKey="-1"
												headerValue="Please select operator"
												onChange="change_operator(this);" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dthOpratorErr">Please select operator</div>
										</div>

										<div class="col-md-6 bank-txt" style="margin-top: 29px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" style="clear: left"
												class="btn submit-form btn-success" data-open-popup="DTH"
												onclick="submitVaForm('#d2h',this)">
										</div>

									</div>
								</form>

							</div>
							<!--  2nd Body Contents End -->


							<!-- Landline  3rd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="landline_recharge_form" style="display: none;">

								<form method="post" id="landline_bill"
									action="GetBillDetails.action"
									onsubmit="validateLandlineForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" name="rechargeBean.rechargeType"
											value="Landline"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">

										<!-- <div class="col-md-6 bank-txt"></div>
                                        <div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt">
											<select class=""
												onChange="landlinServiceProviderOnChange(this)"
												name="rechargeBean.rechargeOperator" id="landline_operator">
												<option value="-1">Select Operator</option>
											</select>
											<div class="errorMsg" id="landlineError"
												style="display: none;"></div>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="landlineNum" style="margin-top: 0px;"
												name="rechargeBean.rechargeNumber" placeholder="Enter Phone Number"
												maxlength="11" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left"
											id="landlineAmountWrapper">
											<input type="text" id="landlineAmount"
												style="margin-top: 0px;" maxlength="5"
												name="billDetails.amount" placeholder="Enter amount" />
										</div>

										<div class="form-group Bhartipay_mt10"
											id="landineAddInfoWrapper" style="display: none">
											<input type="text" name="rechargeBean.accountNumber"
												id="landline_add_info"
												class="payplutus_textbox onlyNum mandatory"
												name="billDetails.account"
												placeholder="Customer Account Number" maxlength="10" />
										</div>

										<div class="form-group Bhartipay_mt10" id="Authenticator"
											style="display: none">
											<select class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo">
												<option value="-1">Please Select Authenticator</option>
												<option value="LLI">LLI</option>
												<option value="LLC">LLC</option>
											</select>
											<div class="errorMsg" id="AuthenticatorError"
												style="display: none;"></div>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success" />
										</div>

									</div>
								</form>
							</div>
							<!--  3rd Body Contents End -->


							<!-- 4th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="datacard_recharge_form" style="display: none;">

								<form method="post" id="datacard"
									action="RechargeService.action">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">
										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>">


										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												id="connection_type" class="rechargetypeDataCard"
												value="Datacard" checked> <label class="">Prepaid</label>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												class="rechargetypeDataCard" id="connection_type"
												value="Postpaid-Datacard"> <label class="">Postpaid</label>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="datacard_mobile" class="" required
												placeholder="Enter your datacard number"
												onkeyup="getOpretorDetails(this,'datacard')" maxlength="10" />
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="datacard_amount" class="" required
												placeholder="Enter amount" maxlength="4" />
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{prePaidDataCard}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="datacard_operator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);" />
											</select>
										</div>

										<div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="datacard_circle"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);" />
										</div>

										<div class="col-md-6 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="datacard"
												onclick="submitVaForm('#datacard',this)">
										</div>

									</div>
								</form>

							</div>
							<!-- 4th Body Contents End -->


							<!-- 5th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="electricityRecharge_form" style="display: none;">

								<form method="post" id="electricityForm"
									action="GetBillDetails.action"
									onsubmit="electricityFormSubmit(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.rechargeType"
											value="Electricity"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										<!--  <div class="col-md-6 bank-txt"></div>
                                        <div class="col-md-6 bank-txt"></div> -->
										<div class="col-md-6 bank-txt">
											<select class="payplutus_dropdown mandatory" id="eleOpretor"
												name="rechargeBean.rechargeOperator"
												onchange="electricityProviderChange()">
												<option value="-1">Select Operator</option>
											</select> <span class="errorMsg" id="oprErr" style="display: none;"></span>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="electricity_consumer"
												style="margin-top: 0px;" placeholder="Enter Consumer Number"
												name="rechargeBean.rechargeNumber" />
										</div>

										<div class="form-group Bhartipay_mt10" id="eleAccount"
											style="display: none;">
											<input type="text" id="electricity_account"
												class="payplutus_textbox onlyNum "
												placeholder="Enter Consumer Number"
												name="billDetails.account" />
										</div>

										<div class="form-group Bhartipay_mt10" id="elethirdOption"
											style="display: none;">
											<select id="thirdEleOption"
												class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo"></select>
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="Submit" name="proceed" st id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="electricity">
										</div>

									</div>
								</form>
							</div>
							<!-- 5th Body Contents End -->


							<!-- 6th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="gasRecharge_form" style="display: none;">

								<form method="post" id="gasRecharge"
									action="GetBillDetails.action"
									onsubmit="validateGasForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11">
										<input type="hidden" name="rechargeBean.rechargeType" value="Gas">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">

										<div class="col-md-6 bank-txt">
											<select class="" id="gas-opretor" style=""
												name="rechargeBean.rechargeOperator"
												onchange="gasOpratorChange(this)">
												<option value="-1">Select Operator</option>
											</select> <span id="gas-provider" class="errorMsg"
												style="display: none;"> Please Select The Service
												Provider. </span>
										</div>
										<!-- <div class="col-md-6 bank-txt"></div>  -->

										<div class="col-md-6 bank-txt">
											<input type="text" id="gas-consumer"
												name="rechargeBean.rechargeNumber" style="margin-top: 0px;"
												placeholder="Enter Operator Number" />
										</div>

										<div class="form-group Bhartipay_mt10" id="accoundBill"
											style="display: none">
											<input type="text" id="accoundBillInp"
												name="billDetails.account"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Operator Number" maxlength="8" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="gas">
										</div>

									</div>
								</form>
							</div>
							<!-- 6th Body Contents End -->


							<!-- 7th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="insurance_form" style="display: none;">

								<form method="post" id="insuranceRecharge"
									action="GetBillDetails.action"
									onsubmit="validateInsuranceForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.rechargeType"
											value="Insurance"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<!-- <div class="col-md-6 bank-txt"></div>
										<div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt">
											<select class="" name="rechargeBean.rechargeOperator" id="insurance"
												onchange="changeInsurar()">
												<option value="-1">Please Select Operator</option>
											</select> <span id="insurance-error" class="errorMsg"></span>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="policy_number"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Policy Number" style="margin-top: 0px;"
												name="rechargeBean.rechargeNumber" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="text" id="insurer_dob"
												class="payplutus_textbox datepicker-here" placeholder="DOB"
												data-language='en' stylr="margin-top:0px"
												name="billDetails.account" readonly />
										</div>

										<div class="col-md-6 bank-txt">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="insurar" />
										</div>

									</div>
								</form>
							</div>
							<!-- 7th Body Contents End -->


						</div>
					</div>

					<div class="modal fade" id="rechagePop" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;" style="color:#111;">Recharge
														Type</th>
													<th id="rechargetype" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mobile Number</td>
													<td id="mobileNo"></td>
												</tr>
												<tr>
													<td>Service Provider</td>
													<td id="servicePovider"></td>
												</tr>
												<tr>
													<td>Circle</td>
													<td id="rechargecircle"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('mobile_form').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="electicityPop" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Electricity Bill Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Service Provider</td>
													<td id="electicityServicePovider"></td>

												</tr>
												<tr>
													<td>Consumer Number</td>
													<td id="consumerNumber"></td>

												</tr>
												<tr>
													<td>Bill Amount</td>
													<td id="electicityrechargeAmount"></td>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('electricityForm').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="gasPopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Gas Bill Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Service Provider</td>
													<td id="gasServicePovider"></td>
												</tr>
												<tr>
													<td>Consumer Number</td>
													<td id="gasConsumerNumber"></td>
												</tr>
												<tr>
													<td>Bill Amount</td>
													<td id="gasAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('gasRecharge').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="insurarPopUp" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Insurance Renew Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>Insurer</td>
													<td id="insuranceServicePovider"></td>
												</tr>
												<tr>
													<td>Insurance Policy Number</td>
													<td id="insuranceConsumerNumber"></td>
												</tr>
												<tr>
													<td>Policy Renew Amount</td>
													<td id="insuranceAmount"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal"
										onclick="document.getElementById('electricityForm').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="DTPpopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;">Service Provider</th>
													<th id="servicePoviderDTH" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>DTH Number</td>
													<td id="dthNo"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmountDTH"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('d2h').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="dataCardPopup" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Recharge Confirmation</h4>
								</div>
								<div class="modal-body">
									<div class="container-fluid">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="color: #111;">Recharge Type</th>
													<th id="rechargetypeDataCard" style="color: #111;">Recharge
														Type</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mobile Number</td>
													<td id="mobileNoDataCard"></td>
												</tr>
												<tr>
													<td>Service Provider</td>
													<td id="servicePoviderDataCard"></td>
												</tr>
												<tr>
													<td>Circle</td>
													<td id="rechargecircleDataCard"></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td id="rechargeAmountDataCard"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success"
										data-dismiss="modal"
										onclick="document.getElementById('datacard').submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div> --%>

	<!--/.fluid-container-->
	<!-- =================================================================================================================
new ui start
================================================================================================================= -->


	<div id="main" role="main" style="background-color: #fff;">
			<div class=" row">
				<div class="submenuOuterWrapper">
					<ul class="subMenueWrapper">
						<li><a href="#mobileRecharge" onclick="resetTab(event)"
							id="mobile_recharge"> <i class="fa fa-mobile"
								aria-hidden="true"></i> Mobile
						</a></li>
						<li><a href="#dthRecharge" onclick="resetTab(event)"
							id="dth_recharge"> <i class='fas fa-broadcast-tower'
								aria-hidden="true"></i> DTH
						</a></li>
						<li><a href="#datacardRecharge" onclick="resetTab(event)"
							id="datacard_recharge"> <i class="fa fa-credit-card"
								aria-hidden="true"></i> DATA CARD
						</a></li>
						<li><a href="#electricityRecharge" onclick="resetTab(event)"
							id="electricity_recharge"> <i class='far fa-lightbulb'
								aria-hidden="true"></i> Electricity
						</a></li>
						<li><a href="#gasRecharge" onclick="resetTab(event)"
							id="gas_recharge"> <i class='far fa-sun' aria-hidden="true"></i>
								Gas
						</a></li>
						<li><a href="#landlinebill" onclick="resetTab(event)"
							id="landline_Recharge"> <i class="fa fa-phone"
								aria-hidden="true"></i> LandLine
						</a></li>
						<li><a href="#insuranceRecharge" onclick="resetTab(event)"
							id="insurance_recharge"> <i class='fas fa-seedling'
								aria-hidden="true"></i> Insurance
						</a></li>
					</ul>
				</div>

				<h3 style="font-size: 18px">
					<font style="color: red;"> <%
 	if (session.getAttribute("rechargeError") != null) {
 %> <%=session.getAttribute("rechargeError")%> <%
 	session.removeAttribute("rechargeError");
 	}
 %>
					</font> <font style="color: green;"></font>
				</h3>

				<div class="rechargeFormWrapper clearfix">

					<div id="content">
						<div class=" row">

							<!--  1st Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="mobile_recharge_form">
								<form method="post" id="mobile_form"
									action="RechargeService">
									<!-- <div class="col-md-1"></div>-->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">


										<!-- Prepaid with radio button start -->

										<div class="col-md-6 bank-txt">
											<input type="radio" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type_prepaid"
												value="PREPAID" checked onClick="hide_plan();"> <label
												class="" style="border-radius: 1px !important;">Prepaid</label>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="radio" class="rechargeTypeRadio"
												name="rechargeBean.rechargeType" id="connection_type_postpaid"
												value="POSTPAID" onClick="hide_plan();"> <label
												class=""  style="border-radius: 1px !important;">Postpaid</label>
										</div>
										<!--  End -->


										<!--  Enter your mobile number start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												class="mandatory" placeholder="Enter your mobile number"
												autocomplete="off" id="rechargeMobile"
												onkeyup="getOpretorDetails(this,'mobile')"  style="border-radius: 1px !important;"/>
										</div>
										<!--  End -->

										<!-- Enter amount start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="phone_amount"
												class="payplutus_textbox  form-control amount-vl mandatory"
												required placeholder="Enter amount" maxlength="4"  style="border-radius: 1px !important;" />
											<div class=" customSelectErr" id="amountGreater10"
												style="display: none;">Amount should be greater than
												10.</div>

										</div>
										<!--  End -->

										<!-- dropdown Star -->

                                         <div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="circlei"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);validateSelectOnChange(this,'#opratoCireclrErr');" />

											<div class="errorMsg customSelectErr" id="opratoCireclrErr"
												style="display: none;">Please select the circle</div>
										</div>

										<div class="col-md-6 bank-txt" id="rc">
											<s:select list="%{prePaid}"
												name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;" cssClass="" id="rechargeOperator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);validateSelectOnChange(this,'#opratorErr');" />

											<div class="errorMsg customSelectErr" id="opratorErr"
												style="display: none;">Please select the operator</div>
										</div>

                                        <div class="col-md-6 bank-txt" id="rcpost">
											<s:select list="%{postPaid}"
												name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;" cssClass="" id="rechargeOperatorPost"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);validateSelectOnChange(this,'#opratorErr');" />

											<div class="errorMsg customSelectErr" id="opratorErr"
												style="display: none;">Please select the operator</div>
										</div>


										
										<!--  End -->



										<!-- Proceed to pay Submit Button start -->
										<!-- <div class="col-md-12 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												style="clear: left" value="Proceed to Pay"
												class="btn submit-form btn-success"
												data-open-popup="rechage"  data-keyboard="true" data-backdrop="static"
												onclick="submitVaForm('#mobile_form',this)">
										</div> -->
										<div class="col-md-12 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												style="clear: left" value="Proceed to Pay"
												class="btn submit-form btn-success"
												data-open-popup="rechage"  data-keyboard="true" data-backdrop="static"
												onclick="check_Connection_type(0)">
										<br><br>
										<input type="button" name="proceeds" id="proceeds"
												style="clear: right" value="Quick Pay"
												class="btn submit-form btn-success"
												data-open-popup="rechage"  data-keyboard="true" data-backdrop="static"
												onclick="check_Connection_type(1)">
										
										</div>
										<!--  End -->

									</div><div class="col-md-2" id="msg" style="color: red ;" ></div>
									 <!-- <div class="col-md-1"></div> -->
								</form>

							</div>
							<!-- 1st Body Contents End -->



							<!--  2nd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="d2h_recharge_form" style="display: none;">

								<form method="post" id="d2h" action="RechargeService">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">

										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>"> <input type="hidden"
											name="rechargeBean.rechargeType" value="DTH"  style="border-radius: 1px !important;">



										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="dth_mobile_no" class="mandatory" required
												placeholder="Enter your DTH number" maxlength="12"  style="border-radius: 1px !important;"/>
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_mobile_noErr">Please Enter DTH Number</div>
										</div>

										<div>
											<input type="hidden" name="rechargeBean.rechargeCircle"
												value="Delhi">
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="dth_amount" class="mandatory" placeholder="Enter amount"
												maxlength="4"   style="border-radius: 1px !important;"/>
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErr">Please enter Amount</div>
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dth_amountrErrGreat10">Please Amount Greater than
												20.</div>
										</div>


										<div class="col-md-6 bank-txt">
											<s:select list="%{dth}" name="rechargeBean.rechargeOperator"
												style="margin-top: 20px;"
												onchange="changeDTHPlaceholderAndLen(this,' #dth_mobile_no')"
												cssClass="mandatory" id="dth_operator" headerKey="-1"
												headerValue="Please select operator"
												onChange="change_operator(this);" />
											<div class="errorMsg customSelectErr" style="display: none;"
												id="dthOpratorErr">Please select operator</div>
										</div>

										<div class="col-md-12 bank-txt" style="margin-top: 29px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" style="clear: left"
												class="btn submit-form btn-success" data-open-popup="DTH" 
												onclick="submitVaForm('#d2h',this)">
										</div>

									</div>
								</form>

							</div>
							<!--  2nd Body Contents End -->


							<!-- Landline  3rd Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="landline_recharge_form" style="display: none;">

								<form method="post" id="landline_bill"
									action="RechargeService"
									onsubmit="validateLandlineForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">

										<input type="hidden" name="rechargeBean.rechargeType"
											value="Landline"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<input type="hidden" name="rechargeBean.userId"
											value="<%=user.getId()%>"> <input type="hidden"
											name="rechargeBean.walletId" value="<%=user.getWalletid()%>">

										<!-- <div class="col-md-6 bank-txt"></div>
                                        <div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt" style="margin-top:0px;">
											<%-- <select class=""
												onChange="landlinServiceProviderOnChange(this)"
												name="rechargeBean.rechargeOperator" id="landline_operator">
												<option value="-1">Select Operator</option>
											</select> --%>
											
											<s:select list="%{landLine}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="landline_operator"
												headerKey="-1" headerValue="Please select operator"
												onchange="landlinServiceProviderDropdownOnChange(event)"/>
											</select>
											
											
											<div class="errorMsg" id="landlineError"
												style="display: none;"></div>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="landlineNum" style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.rechargeNumber" placeholder="Enter Phone Number"
												maxlength="11"  />
										</div>
										
										
										
										<div class="col-md-6 bank-txt">
											<input type="text" id="landline_amount"
												style="margin-top: 0px;border-radius: 1px !important;display:none" 
												name="rechargeBean.rechargeAmt"/>
										</div>

										<div class="col-md-6 bank-txt" style="clear: left"
											id="landlineAmountWrapper">
											<input type="text" id="landlineAmount"
												style="margin-top: 0px;border-radius: 1px !important;" maxlength="5"
												name="billDetails.amount" placeholder="Enter amount" />
										</div>
										
										<div class="col-md-6 bank-txt hide " id="landlineNumStdCode">
											<input type="text" style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.value1" placeholder="Enter STD Code"
												maxlength="11"  />
										</div>
										
										<div class="col-md-6 bank-txt hide " id="landlineNumAccountNumber">
											<input type="text" style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.value2" placeholder="Enter Account Number"
												maxlength="11"  />
										</div>
										
										<div class="col-md-6 bank-txt hide " id="landlineCANumber">
											<input type="text" style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.value1" placeholder="Enter CA Number"
												maxlength="11"  />
										</div>

										<div class="form-group Bhartipay_mt10"
											id="landineAddInfoWrapper" style="display: none">
											<input type="text" name="rechargeBean.accountNumber"
												id="landline_add_info"
												class="payplutus_textbox onlyNum mandatory"
												name="billDetails.account"
												placeholder="Customer Account Number" maxlength="10"  style="border-radius: 1px !important;" />
										</div>

										<div class="form-group Bhartipay_mt10" id="Authenticator"
											style="display: none">
											<select class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo">
												<option value="-1">Please Select Authenticator</option>
												<option value="LLI">LLI</option>
												<option value="LLC">LLC</option>
											</select>
											<div class="errorMsg" id="AuthenticatorError"
												style="display: none;"></div>
										</div>

										<div class="col-md-12 bank-txt">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success" onclick="onSubLandlineForm()" />
										</div>

									</div>
								</form>
							</div>
							<!--  3rd Body Contents End -->


							<!-- 4th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="datacard_recharge_form" style="display: none;">

								<form method="post" id="datacard"
									action="RechargeService">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input type="hidden"
											name="rechargeBean.userId" value="<%=user.getId()%>">
										<input type="hidden" name="rechargeBean.walletId"
											value="<%=user.getWalletid()%>">


										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												id="connection_type" class="rechargetypeDataCard"
												value="Datacard" checked > <label class="">Prepaid</label>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="radio" name="rechargeBean.rechargeType"
												class="rechargetypeDataCard" id="connection_type"
												value="Postpaid-Datacard"> <label class="">Postpaid</label>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeNumber"
												id="datacard_mobile" class="" required
												placeholder="Enter your datacard number"
												onkeyup="getOpretorDetails(this,'datacard')" maxlength="10" style="border-radius: 1px !important;"/>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.rechargeAmt"
												id="datacard_amount" class="" required
												placeholder="Enter amount" maxlength="4"  style="border-radius: 1px !important;"/>
										</div>

										 <div class="col-md-6 bank-txt">
											<s:select list="%{prePaidDataCard}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="datacard_operator"
												headerKey="-1" headerValue="Please select operator"
												onChange="change_operator(this);" />
											</select>
										</div> 
										
										

										<div class="col-md-6 bank-txt">
											<s:select list="%{circle}" name="rechargeBean.rechargeCircle"
												style="margin-top: 20px;" cssClass="" id="datacard_circle"
												headerKey="-1" headerValue="Please select a circle"
												onChange="change_circle(this);" />
										</div>

										<div class="col-md-12 bank-txt" style="margin-top: 20px;">
											<input type="button" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="datacard"
												onclick="submitVaForm('#datacard',this)">
										</div>

									</div>
								</form>

							</div>
							<!-- 4th Body Contents End -->


							<!-- 5th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="electricityRecharge_form" style="display: none;">

								<form method="post" id="electricityForm"
									action="RechargeService"
									onsubmit="electricityFormSubmit(event)">
									
									<div class="col-md-11" >
										<input type="hidden" name="rechargeBean.rechargeType"
											value="Electricity"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">
										
										<div class="col-md-6 bank-txt" style="margin-top:0px;">
											<s:select list="%{electricity}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="eleOpretor"
												headerKey="-1" headerValue="Please select operator"
												onChange="electricityProviderdropdownChange(event);" /> 
											
														<span class="errorMsg" id="oprErr" style="display: none;"></span>
										</div>
										
										

										<div class="col-md-6 bank-txt">
											<input type="text" id="electricity_consumer"
												style="margin-top: 0px;border-radius: 1px !important;" placeholder="Enter Consumer Number"
												name="rechargeBean.rechargeNumber"/>
										</div>
										<div class="col-md-6 bank-txt">
											<input type="text" id="electricity_amount"
												style="margin-top: 0px;border-radius: 1px !important;display:none" 
												name="rechargeBean.rechargeAmt"/>
										</div>

										<div class="form-group Bhartipay_mt10" id="eleAccount"
											style="display: none;">
											<input type="text" id="electricity_account"
												class="payplutus_textbox onlyNum "
												placeholder="Enter Consumer Number"
												name="billDetails.account"  style="border-radius: 1px !important;"/>
										</div>

										<div class="form-group Bhartipay_mt10" id="elethirdOption"
											style="display: none;">
											<select id="thirdEleOption"
												class="payplutus_dropdown mandatory"
												name="billDetails.additionalInfo"></select>
										</div>
										<div class="col-md-6 bank-txt hide" id="electricityBillingUnit">
											<input type="text"
												style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.value1" placeholder="Enter Billing Unit"/>
										</div>
										
										<div class="col-md-6 bank-txt hide" id="electricityProcessingCycle">
											<input type="text"
												style="margin-top: 0px;border-radius: 1px !important;" 
												name="rechargeBean.value2" placeholder="Enter Processing Cycle"/>
										</div>
										
										<div class="col-md-6 bank-txt hide" id="electricityCycleNumber">
											<input type="text"
												style="margin-top: 0px;border-radius: 1px !important;" 
												name="rechargeBean.value1" placeholder="Enter Cycle Number"/>
										</div>

										<div class="col-md-12 bank-txt" style="clear: left">
											<input type="Submit" name="proceed" st id="proceed"
												value="Fetch & Pay" class="btn submit-form btn-success"
												data-open-popup="electricity" onclick="electricityFormSub()">
										 
										 <input type="Submit" name="proceed" st id="proceed"
											value="Quick Pay" class="btn submit-form btn-success"
											data-open-popup="electricity" onclick="electricityFormQuickPay()">
									     </div>

									</div>
								</form>
							</div>
							<!-- 5th Body Contents End -->


							<!-- 6th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="gasRecharge_form" style="display: none;">

								<form method="post" id="gasRecharge"
									action="RechargeService"
									onsubmit="validateGasForm(event)">
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11">
										<input type="hidden" name="rechargeBean.rechargeType" value="Gas">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt">

										<div class="col-md-6 bank-txt" style="margin:0px;">
											<%-- <select class="" id="gas-opretor" style=""
												name="rechargeBean.rechargeOperator"
												onchange="gasOpratorChange(this)">
												<option value="-1">Select Operator</option>
											</select> --%>
											
											<s:select list="%{gas}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="gas-operator"
												headerKey="-1" headerValue="Please select operator"
												onChange="gaseProviderDropdownOnChange(event);" />
											
											
											
											 <span id="gas-provider" class="errorMsg"
												style="display: none;"> Please Select The Service
												Provider. </span>
										</div>
										<!-- <div class="col-md-6 bank-txt"></div>  -->

										<div class="col-md-6 bank-txt">
											<input type="text" id="gas-consumer"
												name="rechargeBean.rechargeNumber" style="margin-top: 0px;border-radius: 1px !important;"
												placeholder="Enter Operator Number"  />
										</div>
										
										<div class="col-md-6 bank-txt hide" id="gasBillGroupNumber">
											<input type="text" 
												style="margin-top: 0px;border-radius: 1px !important;" 
												name="rechargeBean.value1" placeholder="Enter Bill Group Number"/>
										</div>
										
										<div class="col-md-6 bank-txt">
											<input type="text" id="gas_amount"
												style="margin-top: 0px;border-radius: 1px !important;display:none" 
												name="rechargeBean.rechargeAmt"/>
										</div>
										
										

										<div class="form-group Bhartipay_mt10" id="accoundBill"
											style="display: none">
											<input type="text" id="accoundBillInp"
												name="billDetails.account"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Operator Number" maxlength="8"  style="border-radius: 1px !important;"/>
										</div>

										<div class="col-md-12 bank-txt" style="clear: left">
											<input type="submit" name="proceed" id="proceed"
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="gas" onclick="gasFormOnSubmit()">
										</div>

									</div>
								</form>
							</div>
							<!-- 6th Body Contents End -->


							<!-- 7th Body Contents Start -->
							<div class="col-lg-8 col-md-8 r-tab-content col-sm-8 col-xs-12"
								id="insurance_form" style="display: none;">

								<form method="post" id="insuranceRechargeForm"
									action="RechargeService"
									onsubmit="validateInsuranceForm(event)">
									<!-- <form method="post" id="insuranceRecharge"
									action="RechargeService"> -->
									<!-- <div class="col-md-1"></div> -->
									<div class="col-md-11" style="">
										<input type="hidden" name="rechargeBean.rechargeType"
											value="Insurance"> <input type="hidden"
											value="${csrfPreventionSalt}" name="csrfPreventionSalt">

										<!-- <div class="col-md-6 bank-txt"></div>
										<div class="col-md-6 bank-txt"></div> -->

										<div class="col-md-6 bank-txt" style="margin-top:0px;">
											<%-- <select class="" name="rechargeBean.rechargeOperator" id="insurance"
												onchange="changeInsurar()">
												<option value="-1">Please Select Operator</option>
											</select>  --%>
											
											<s:select list="%{insurance}"
												name="rechargeBean.rechargeOperator" cssClass=""
												style="margin-top: 20px;" id="insurance"
												headerKey="-1" headerValue="Please select operator"
												 onchange="changeInsurar()"/>
											</select>
											
											<span id="insurance-error" class="errorMsg"></span>
										</div>

										<div class="col-md-6 bank-txt">
											<input type="text" id="policy_number"
												class="payplutus_textbox alphaNum-vl "
												placeholder="Enter Policy Number" style="margin-top: 0px;border-radius: 1px !important;"
												name="rechargeBean.rechargeNumber" />
										</div>

										<div class="col-md-6 bank-txt" style="clear: left">
											<input type="date" id="insurer_dob"
												class="payplutus_textbox alphaNum-vl " placeholder="DOB"
												 stylr="margin-top:0px" data-date-format="DD MMMM YYYY"
												name="rechargeBean.other" />
										</div>
									<!--  Enter your mobile number start -->
										<div class="col-md-6 bank-txt">
											<input type="text" name="rechargeBean.mobileNumber"
												class="mandatory" placeholder="Enter your mobile number"
												autocomplete="off" id="insuranceMobileNumber"
												onkeyup="getOpretorDetails(this,'mobile')"  style="border-radius: 1px !important;"/>
										</div>										
										<div class="col-md-6 bank-txt">
											<input type="text" id="insurance_amount"
												style="margin-top: 0px;border-radius: 1px !important;display:none" 
												name="rechargeBean.rechargeAmt"/>
										</div>

										<div class="col-md-12 bank-txt">
											<input type="submit" name="proceed" 
												value="Proceed to Pay" class="btn submit-form btn-success"
												data-open-popup="insurar" onclick="onSubmitInsuranceForm()"/>
										</div>

									</div>
								</form>
							</div>
							<!-- 7th Body Contents End -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- =================================================================================================================
new ui start
================================================================================================================= -->
<div class="modal fade" id="gasPopup" role="dialog" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Gas Bill Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td>Service Provider</td>
															<td id="gasServicePovider"></td>
														</tr>
														<tr>
															<td>Consumer Number</td>
															<td id="gasConsumerNumber"></td>
														</tr>
														<tr>
															<td>Bill Amount</td>
															<td id="gasAmount"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal"
												onclick="document.getElementById('gasRecharge').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="insurarPopUp" role="dialog" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Insurance Renew Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td>Insurer</td>
															<td id="insuranceServicePovider"></td>
														</tr>
														<tr>
															<td>Insurance Policy Number</td>
															<td id="insuranceConsumerNumber"></td>
														</tr>
														<tr>
															<td>Policy Renew Amount</td>
															<td id="insuranceAmount"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal"
												onclick="document.getElementById('electricityForm').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="DTPpopup" role="dialog" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Recharge Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="color: #111;">Service Provider</th>
															<th id="servicePoviderDTH" style="color: #111;">Recharge
																Type</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>DTH Number</td>
															<td id="dthNo"></td>
														</tr>
														<tr>
															<td>Amount</td>
															<td id="rechargeAmountDTH"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-success"
												data-dismiss="modal"
												onclick="document.getElementById('d2h').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="dataCardPopup" role="dialog" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Recharge Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="color: #111;">Recharge Type</th>
															<th id="rechargetypeDataCard" style="color: #111;">Recharge
																Type</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Mobile Number</td>
															<td id="mobileNoDataCard"></td>
														</tr>
														<tr>
															<td>Service Provider</td>
															<td id="servicePoviderDataCard"></td>
														</tr>
														<tr>
															<td>Circle</td>
															<td id="rechargecircleDataCard"></td>
														</tr>
														<tr>
															<td>Amount</td>
															<td id="rechargeAmountDataCard"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-success"
												data-dismiss="modal"
												onclick="document.getElementById('datacard').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>

	<div class="modal fade" id="electicityPop" role="dialog" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Electricity Bill Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td>Service Provider</td>
															<td id="electicityServicePovider"></td>

														</tr>
														<tr>
															<td>Consumer Number</td>
															<td id="consumerNumber"></td>

														</tr>
														<tr>
															<td>Bill Amount</td>
															<td id="electicityrechargeAmount"></td>

														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal"
												onclick="document.getElementById('electricityForm').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>
    
    
    <div class="modal fade" id="rechagePop" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="z-index:999999">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Recharge Confirmation</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="color: #111;" style="color:#111;">Recharge
																Type</th>
															<th id="rechargetype" style="color: #111;">Recharge
																Type</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Mobile Number</td>
															<td id="mobileNo"></td>
														</tr>
														<tr>
															<td>Service Provider</td>
															<td id="servicePovider"></td>
														</tr>
														<tr>
															<td>Circle</td>
															<td id="rechargecircle"></td>
														</tr>
														<tr>
															<td>Amount</td>
															<td id="rechargeAmount"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-success"
												data-dismiss="modal"
												onclick="document.getElementById('mobile_form').submit();">Confirm</button>
										</div>
									</div>
								</div>
							</div>
							
							
	<!-- Biller Popup -->
	<div class="modal" id="AlreadyPaidPopup" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Operater Name:</th>
						<th style="color: #000;" id="AlreadyPaidOperaterName"></th>		
						</tr>
						<tr>
						<th style="color: #000;">Operator Number:</th>
						<th style="color: #000;" id="AlreadyPaidOperatorNumber"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Message:</th>
						<th style="color: #000;" id="AlreadyPaidDescMessage"></th>		
						</tr>
						
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					

					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					
				</div>
			</div>

		</div>
	</div>
								
<!-- Biller Popup -->
	<div class="modal" id="NotAuthorizedPopup" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Msg:</th>
						<th style="color: #000;" id="notAuthorizedMsg"></th>		
						</tr>
						
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					

					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					
				</div>
			</div>

		</div>
	</div>
							
	<!-- Biller Popup -->
	<div class="modal" id="FailStatusPopup" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Operater Name:</th>
						<th style="color: #000;" id="FailStatusOperaterName"></th>		
						</tr>
						<tr>
						<th style="color: #000;">Customer  no:</th>
						<th style="color: #000;" id="FailStatuscustomermobileno"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Message:</th>
						<th style="color: #000;" id="FailStatusmessage"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Status</th>
						<th style="color: #000;" id="FailStatusstatus"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary btn-info"
						id="proceedBiller" style="display: none"
						onclick="billerConfirm('null','null','null','${csrfPreventionSalt}')">Proceed</button>
				</div>
			</div>

		</div>
	</div>

		<!-- Biller Popup -->
	<div class="modal" id="GasSuccessTable" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Consumer No:</th>
						<th style="color: #000;" id="consumerNo"></th>		
						</tr>
						<tr>
						<th style="color: #000;">Operator:</th>
						<th style="color: #000;" id="gas_operator"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CustomerName:</th>
						<th style="color: #000;" id="gas_customer_name"></th>		
						</tr>
						<tr>
						<th style="color: #000;">BillAmount:</th>
						<th style="color: #000;" id="gasBillAmount"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >DueAmount</th>
						<th style="color: #000;" id="gas_due_Amount"></th>		
						</tr>
						<tr>
						<th style="color: #000;" id="gasDueDate">DueDate:</th>
						<th style="color: #000;" id="gas_due_date"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="gasBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitgasForm()">submit</button>
					
					
				</div>
			</div>

		</div>
	</div>
	

		<!-- Biller Popup -->
	<div class="modal" id="postPaidSuccessTable" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Billamount:</th>
						<th style="color: #000;" id="billamount"></th>		
						</tr>
						<tr>
						<th style="color: #000;">NetAmount:</th>
						<th style="color: #000;" id="netAmount"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Billdate:</th>
						<th style="color: #000;" id="billdate"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Duedate</th>
						<th style="color: #000;" id="duedate"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitPhoneRechargeForm()">submit</button>
					
					
				</div>
			</div>

		</div>
	</div>

<!-- New Postpaid quick pay -->

<div class="modal" id="postPaidSuccessTableNew" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Mobile:</th>
						<th style="color: #000;" id="mobileNew"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Operator:</th>
						<th style="color: #000;" id="operatorNew"></th>
						</tr>
						<tr>
						<th style="color: #000;" >Circle</th>
						<th style="color: #000;" id="circleNew"></th>		
						</tr>
						<tr>
						<th style="color: #000;">Billamount:</th>
						<th style="color: #000;" id="billAmountNew"></th>		
						</tr>
						
						 
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitPhoneRechargeForm()">submit</button>
					
					
				</div>
			</div>

		</div>
	</div>




<div class="modal" id="InsuranceSuccessTable" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Operator:</th>
						<th style="color: #000;" id="Insurance_operator"></th>		
						</tr>
						<tr>
						<th style="color: #000;">Policy Number:</th>
						<th style="color: #000;" id="insurance_policy_number"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CustomerName:</th>
						<th style="color: #000;" id="insurance_customer_name"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Net amount:</th>
						<th style="color: #000;" id="insurance_amount_pay"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Mobile number</th>
						<th style="color: #000;" id="insurance_mobile_number"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Due Date</th>
						<th style="color: #000;" id="insurance_due_date"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">
<!-- 
					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" /> -->

					<!-- <button id="insurenceBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitinsurenceForm()">submit</button> -->
					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitinsurenceForm()">submit</button>
					
				</div>
			</div>

		</div>
	</div>

<!--  New Changes  -->

<div class="modal" id="electricitySuccessTableNew" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTableNew">
						<tbody>
						<tr>
						<th style="color: #000;">Operator:</th>
						<th style="color: #000;" id="electricty_operatorNew"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CA Number:</th>
						<th style="color: #000;" id="electricity_ca_numberNew"></th>		
						</tr>
						 
						<tr>
						<th style="color: #000;" >BillAmount</th>
						<th style="color: #000;"> 
						<input type="text" id="electricity_bill_amountNew" />
						
						<!-- 
						<input type="text" id="electricity_amount" name="rechargeBean.rechargeAmt"/>
						<input type="text" id="electricity_bill_amountNew" > --></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					 <input type="hidden" id="txnId" value="" />
					 <input type="hidden" id="billerAmount" value="" />
					 <input type="hidden" id="billerType" value="" /> 
					 <input type="hidden" id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButtonNew" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitelectricityFormNew()">submit</button>
					
					
				</div>
			</div>

		</div>
	</div>


<!-- New Changes -->

<div class="modal" id="electricitySuccessTable" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Operator:</th>
						<th style="color: #000;" id="electricty_operator"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CA Number:</th>
						<th style="color: #000;" id="electricity_ca_number"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CustomerName:</th>
						<th style="color: #000;" id="electricity_customer_name"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Billdate:</th>
						<th style="color: #000;" id="electricity_billdate"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Duedate</th>
						<th style="color: #000;" id="electricity_duedate"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >BillAmount</th>
						<th style="color: #000;" id="electricity_bill_amount"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitelectricityForm()">submit</button>
					
					
				</div>
			</div>

		</div>
	</div>

	
	<!-- Biller Popup -->
	<div class="modal" id="billerSuccessTable" role="dialog" style="    z-index: 9999999;top: -100px;" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biller Information</h4>
				</div>
				<div class="modal-body">
					<div class="responseMsg"></div>
					<table class="table table-bordered" id="billerTable">
						<tbody>
						<tr>
						<th style="color: #000;">Billamount:</th>
						<th style="color: #000;" id="billamount"></th>		
						</tr>
						<tr>
						<th style="color: #000;">CustomerName:</th>
						<th style="color: #000;" id="customerName"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Billdate:</th>
						<th style="color: #000;" id="billdate"></th>		
						</tr>
						<tr>
						<th style="color: #000;" >Duedate</th>
						<th style="color: #000;" id="duedate"></th>		
						</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">

					<input type="hidden" id="txnId" value="" /> <input type="hidden"
						id="billerAmount" value="" /> <input type="hidden"
						id="billerType" value="" /> <input type="hidden"
						id="billerTypePlatForm" value="B2B" />

					<button id="elecBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitelectricityForm()">submit</button>
					<button id="gasBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitgasForm()">submit</button>
					<button id="landlineBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitlandlineForm()">submit</button>
					<!-- <button id="insurenceBillButton" type="button" class="btn btn-default" data-dismiss="modal" onclick="submitinsurenceForm()">submit</button> -->
					
					
				</div>
			</div>

		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>


	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>

	<script src="./js/ie10-viewport-bug-workaround.js"></script>
	<script src="./js/payplutus.js<%=version%>" type="text/javascript"></script>


	<script src="./js/jquery.dd.min.js"></script>
	
	<script>
	function gasFormOnSubmit(){
		
		var consumerNumber = document.getElementById("gas-consumer").value;
		var operaterName = document.getElementById("gas-operator").value;
		var body = {};
		body['rechargeBean.rechargeNumber'] = consumerNumber;
		body['rechargeBean.rechargeOperator'] = operaterName;
		console.log(body);
       if(!consumerNumber && !operaterName){
			return;
		}
		
       $.ajax({
           type : "POST",
           url: 'GasBillerDetails',
           data : body,
           success : function(result){
           console.log(result)
               	$("#GasSuccessTable").modal();
               	document.getElementById("consumerNo").innerText = result.tel;
               	document.getElementById("gas_operator").innerText = result.operator;
       			document.getElementById("gas_customer_name").innerText = result.records.CustomerName;
       			document.getElementById("gasBillAmount").innerText = result.records.BillAmount;
       			document.getElementById("gas_due_Amount").innerText = result.records.DueAmt;
       			document.getElementById("gas_amount").value=result.records.DueAmt;
       			if(result.records.Duedate == ""){
       				$("#gasDueDate").hide();
       				$("#gas_due_date").hide();
       			}
       			else{
       				document.getElementById("gas_due_date").innerText = result.records.Duedate;
       			}
       			
       		     $("#landlineBillButton").hide();
       		     $("#elecBillButton").hide();
       		     $("#gasBillButton").show();
       		     $("#insurenceBillButton").hide();
           
           },
           error : function(xhr, errmsg) {alert("Nothing found!!");}
       });
		
		
	}

	function electricityFormQuickPay(){ 
		debugger
		 
		var consumerNumber = document.getElementById("electricity_consumer").value;
		var operaterName = document.getElementById("eleOpretor").value;

		 
		$("#electricitySuccessTableNew").modal();
    	console.log("Bill is due");
    	 
	    document.getElementById("electricty_operatorNew").innerText = operaterName;
	    document.getElementById("electricity_ca_numberNew").innerText = consumerNumber;
	    
		
	}

function submitelectricityFormNew()
{
	debugger
	var consumerNumber = document.getElementById("electricity_consumer").value;
	var operaterName = document.getElementById("eleOpretor").value;

	var amount = document.getElementById("electricity_bill_amountNew").value;

	//alert(consumerNumber+"     "+amount+"   "+operaterName);
	document.getElementById("electricity_amount").value=amount;
	
	document.getElementById("electricityForm").submit();
	
	
}
	
	
	function electricityFormSub(){ debugger
		var CustomerName;
		var amount;
		var dueDate;
		var billDate;
		
		var consumerNumber = document.getElementById("electricity_consumer").value;
		var operaterName = document.getElementById("eleOpretor").value;
		console.log(consumerNumber);
		console.log(operaterName);
		 var body = {};
		body['rechargeBean.rechargeNumber'] = consumerNumber;
		body['rechargeBean.rechargeOperator'] = operaterName;
        if(!consumerNumber && !operaterName){
			return;
		}
		$.ajax({
            type : "POST",
            url: 'BillerDetails',
            data : body,
            success : function(result){
            	console.log(result);
            	//alert(result.status);
            	//var result = {"status":"true","Billamount":"10.00","CustomerName":"RAJENDER","Billdate":"30-12-0001","Duedate":"11-09-2020"};
            	console.log("response from url iss:  "+result);
            	if(result.records[0].status == "0"){
            		console.log("Already paid case")
            		$("#AlreadyPaidPopup").modal();
                	document.getElementById("AlreadyPaidOperaterName").innerText = result.operator;
        			document.getElementById("AlreadyPaidOperatorNumber").innerText = result.tel;
        			document.getElementById("AlreadyPaidDescMessage").innerText = result.records[0].desc;
        			
            	}
            	if( result.status == "false"){
            		console.log("if");
                	$("#FailStatusPopup").modal();
                	document.getElementById("FailStatusOperaterName").innerText = operaterName;
        			document.getElementById("FailStatuscustomermobileno").innerText = consumerNumber;
        			document.getElementById("FailStatusmessage").innerText = result.message;
        			document.getElementById("FailStatusstatus").innerText = result.status;
                	
                }
                if(result.records[0].status == "1"){
                	$("#electricitySuccessTable").modal();
                	console.log("Bill is due");
                	
                	//console.log("result status is:  "+result.status)
                	//console.log("tel is : "+result.tel)
                	//console.log("Operator name is :"+result.operator)
                	//console.log("Records are : "+result.records[0])
                	//console.log("Records status:  "+result.records[0].status)
                	//console.log("Records description : "+result.records[0].desc)
                
                document.getElementById("electricty_operator").innerText = result.operator;
                document.getElementById("electricity_ca_number").innerText = result.tel;
                document.getElementById("electricity_customer_name").innerText = result.records[0].CustomerName;
        			document.getElementById("electricity_billdate").innerText = result.records[0].Billdate;
        			document.getElementById("electricity_duedate").innerText = result.records[0].Duedate;
                	document.getElementById("electricity_bill_amount").innerText = result.records[0].Billamount;
                	
                	document.getElementById("electricity_amount").value = result.records[0].Billamount;
                }
            },
            error : function(xhr, errmsg) {alert("Nothing found!!");}
        });
	}
	
	function check_Connection_type(i){
debugger

		var connectionTypePostpaid=mobile_form.connection_type_postpaid;
		var connectionTypePrepaid=mobile_form.connection_type_prepaid;
		var checkedConnectionType;
		if(connectionTypePostpaid.checked == true){
			checkedConnectionType="POSTPAID";

			var proceed = document.getElementById("proceeds").value;
           
			var operaterName = document.getElementById("rechargeOperatorPost").value;
			var phoneNo=document.getElementById("rechargeMobile").value;
			var amount=document.getElementById("phone_amount").value;
			var circle=document.getElementById("circlei").value;
			if(operaterName!="" && phoneNo!="" && amount!="" && circle!="-1" ){
                   if("1"==i)
					{
                	$("#postPaidSuccessTableNew").modal();
	            	console.log("Bill is due");
	            	
	    			document.getElementById("mobileNew").innerText = phoneNo;
	    			document.getElementById("operatorNew").innerText = operaterName;
	    			document.getElementById("circleNew").innerText = circle;
                    document.getElementById("billAmountNew").innerText = amount;
 
		    		}else{
				      PostPaidFormSub(checkedConnectionType);
		    		 }
				}else
				{
					 $('#msg').html("Please fill all details");
	            	 $("#msg").show();
	           	     setTimeout(function() { $("#msg").hide(); }, 3000);
				 return false;
				}
			
		}
		else{
			if(connectionTypePrepaid.checked == true){
				var operaterName = document.getElementById("rechargeOperator").value;
				var phoneNo=document.getElementById("rechargeMobile").value;
				var amount=document.getElementById("phone_amount").value;
				var circle=document.getElementById("circlei").value;
				if(operaterName!="" && phoneNo!="" && amount!="" && circle!="-1" ){
				submitPhoneRechargeForm();
				}else
				{
				 $('#msg').html("Please fill all details");
             	 $("#msg").show();
            	 setTimeout(function() { $("#msg").hide(); }, 3000);
				 return false;
				}
			}
		}
	}
	
	function PostPaidFormSub(checkedConnectionType){
		var CustomerName;
		
		var amount;
		var dueDate;
		var billDate;
		
		
		var operaterName = document.getElementById("rechargeOperatorPost").value;
		var phoneNo=document.getElementById("rechargeMobile").value;
		var amount=document.getElementById("phone_amount").value;
		var circle=document.getElementById("circlei").value;
		/* var connectionType=document.getElementById("connection_type_postpaid").value; */
		var connectionType=checkedConnectionType;
		console.log(phoneNo);
		console.log(operaterName);
		console.log(circle);
		console.log(connectionType);
		 var body = {};
		body['rechargeBean.rechargeType'] = connectionType;
		body['rechargeBean.rechargeOperator'] = operaterName;
		
		body['rechargeBean.rechargeNumber'] = phoneNo;
		body['rechargeBean.rechargeAmt'] = amount;
		body['rechargeBean.rechargeCircle'] = circle;
        if(!phoneNo && !operaterName && !circle){
			return;
		}

		debugger
		$.ajax({
            type : "POST",
            url: 'PostPaidBillerDetails',
            data : body,
            success : function(result){
            	console.log(result);
            	
            	console.log("response from url iss:  "+result);
            	if(result.records.status == 0){
            		console.log("Already paid case")
            		$("#AlreadyPaidPopup").modal();
                	document.getElementById("AlreadyPaidOperaterName").innerText = result.operator;
        			document.getElementById("AlreadyPaidOperatorNumber").innerText = result.tel;
        			document.getElementById("AlreadyPaidDescMessage").innerText = result.records.desc;
        			
            	}
            	
            	else{
                	$("#postPaidSuccessTable").modal();
                	console.log("Bill is due");
                	
                	//console.log("result status is:  "+result.status)
                	//console.log("tel is : "+result.tel)
                	//console.log("Operator name is :"+result.operator)
                	//console.log("Records are : "+result.records[0])
                	//console.log("Records status:  "+result.records[0].status)
                	//console.log("Records description : "+result.records[0].desc)
                
                	document.getElementById("billamount").innerText = result.records[0].Billamount;
        			document.getElementById("netAmount").innerText = result.records[0].Netamount;
        			document.getElementById("billdate").innerText = result.records[0].Billdate;
        			document.getElementById("duedate").innerText = result.records[0].Duedate;
        			$("#landlineBillButton").hide();
          		     $("#elecBillButton").show();
          		     $("#gasBillButton").hide();
          		     $("#insurenceBillButton").hide();
                }
            },
            error : function(xhr, errmsg) {alert("Nothing found!!");}
        });
	}
	
	function onSubLandlineForm(){
		var consumerNumber = document.getElementById("landlineNum").value;
		var amount = document.getElementById("landlineAmount").value;
		var operaterName = document.getElementById("landline_operator").value;
		var body = {};
		body['rechargeBean.rechargeNumber'] = consumerNumber;
		body['rechargeBean.rechargeOperator'] = operaterName;
		body['rechargeBean.rechargeAmt'] = amount;
		
		console.log(body);
        
		if(!consumerNumber && !amount && !operaterName){
			return;
		}
		$.ajax({
            type : "POST",
            url: 'BillerDetails',
            data : body,
            success : function(result){
            	//var result = {"status":"true","Billamount":"10.00","CustomerName":"RAJENDER","Billdate":"30-12-0001","Duedate":"11-09-2020"};
            	if( result.status == "false"){
                	$("#FailStatusPopup").modal();
                	document.getElementById("FailStatusOperaterName").innerText = operaterName;
        			document.getElementById("FailStatuscustomermobileno").innerText = consumerNumber;
        			document.getElementById("FailStatusmessage").innerText = result.message;
        			document.getElementById("FailStatusstatus").innerText = result.status;
                	
                }
                else{
                	$("#billerSuccessTable").modal();
                	document.getElementById("landline_amount").value = result.Billamount;
                	document.getElementById("customerName").innerText = result.CustomerName;
        			document.getElementById("billamount").innerText = result.Billamount;
        			document.getElementById("billdate").innerText = result.Billdate;
        			document.getElementById("duedate").innerText = result.Duedate;
        			$("#landlineBillButton").show();
         		     $("#elecBillButton").hide();
         		     $("#gasBillButton").hide();
         		     $("#insurenceBillButton").hide();
                }
            },
            error : function(xhr, errmsg) {alert("Nothing found!!");}
        });
	}
	
	function onSubmitInsuranceForm(){
		var policyNumber = document.getElementById("policy_number").value;
		console.log(policyNumber);
 		var insurer_dob = document.getElementById("insurer_dob").value;
 		var operaterName = document.getElementById("insurance").value;
 		var mobileNumber = document.getElementById("insuranceMobileNumber").value;
		var body = {};
		
		body['rechargeBean.rechargeNumber'] = policyNumber;
		body['rechargeBean.rechargeOperator'] = operaterName;
	    body['rechargeBean.other'] = insurer_dob;
	    body['rechargeBean.mobileNumber'] = mobileNumber;
		console.log(body);
		if(!policyNumber && !insurer_dob && !operaterName && !mobileNumber){
			
			return;
		}
		$.ajax({
            type : "POST",
            url: 'InsuranceBillerDetails',
            data : body,
            success : function(result){
            	//var result = {"status":"true","Billamount":"10.00","CustomerName":"RAJENDER","Billdate":"30-12-0001","Duedate":"11-09-2020"};
            	//if(result.status == "0"){
            		
            		//$("#NotAuthorizedPopup").modal();
            		//document.getElementById("notAuthorizedMsg").innerText = result.records.msg;
            	//}
            	
            	//if( result.records[0].status == "0"){
                	
                //	$("#AlreadyPaidPopup").modal();
                //	document.getElementById("AlreadyPaidOperaterName").innerText = result.operator;
        		//	document.getElementById("AlreadyPaidOperatorNumber").innerText = result.tel;
        		//	document.getElementById("AlreadyPaidDescMessage").innerText = result.records[0].desc;
                	
                //}
                if(result.records[0].status == 1){
                	$("#InsuranceSuccessTable").modal();
                	document.getElementById("Insurance_operator").innerText = result.operator;
                	document.getElementById("insurance_policy_number").innerText = result.tel;
        			document.getElementById("insurance_customer_name").innerText = result.records[0].CustomerName;
        			document.getElementById("insurance_amount_pay").innerText = result.records[0].Netamount;
        			document.getElementById("insurance_mobile_number").innerText = result.mobilenumber;
        			document.getElementById("insurance_due_date").innerText = result.records[0].Duedate;
        			document.getElementById("insurance_amount").value=result.records[0].Netamount;
                }
                
                else{
                	if(result.status == 0){
                		
                		$("#NotAuthorizedPopup").modal();
                		document.getElementById("notAuthorizedMsg").innerText = result.records.msg;
                	}
                	else{
                		$("#AlreadyPaidPopup").modal();
                      	document.getElementById("AlreadyPaidOperaterName").innerText = result.operator;
                		document.getElementById("AlreadyPaidOperatorNumber").innerText = result.tel;
                		document.getElementById("AlreadyPaidDescMessage").innerText = result.records[0].desc;
                	}
                }
            },
            error : function(xhr, errmsg) {alert("Nothing found!!");}
        });
	}
	
	async function postData(url, data) {
		  /* // Default options are marked with *
		  const response = await fetch(url, {
		    method: 'POST', // *GET, POST, PUT, DELETE, etc.
		    mode: 'cors', // no-cors, *cors, same-origin
		    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		    credentials: 'same-origin', // include, *same-origin, omit
		    processData: false,
		    mimeType: "multipart/form-data",
		    contentType: false,
		    data: form // body data type must match "Content-Type" header
		  });
		  return response.json(); // parses JSON response into native JavaScript objects */
		 console.log(data);
		 console.log(url);
		 $.ajax({
             type : "POST",
             url: url,
             data : data,
             success : function(result){
               
            	
            	return result;
              
             },
             error : function(xhr, errmsg) {alert("Nothing found!!");}
         });    
		}
	
	function submitelectricityForm(){
		document.getElementById("electricityForm").submit();
		
	}
	
	function submitPhoneRechargeForm(){
		document.getElementById("mobile_form").submit();
		
	}
	function submitlandlineForm(){
		document.getElementById("landline_bill").submit();
	}
	function submitgasForm(){
		console.log("submitgasForm");
		document.getElementById("gasRecharge").submit();
	}
	function submitinsurenceForm(){
		console.log("submitting insurance form");
		document.getElementById("insuranceRechargeForm").submit();
	}
	
	function gaseProviderDropdownOnChange(event){
		console.log(event.target.value);
		if(event.target.value == "Mahanagar Gas"){
			$( "#gasBillGroupNumber" ).removeClass( "hide" ).addClass( "show" );
		}
		else{
			$( "#gasBillGroupNumber" ).removeClass( "show" ).addClass( "hide" );
		}
		
	}
	
	function landlinServiceProviderDropdownOnChange(event){
		console.log(event.target.value);
		
		if(event.target.value == "Airtel Landline"){
			$( "#landlineCANumber" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineNumStdCode" ).removeClass( "hide" ).addClass( "show" );
			$( "#landlineNumAccountNumber" ).removeClass( "show" ).addClass( "hide" );
		}
		else if(event.target.value == "Bsnl Landline"){
			$( "#landlineNumStdCode" ).removeClass( "hide" ).addClass( "show" );
			$( "#landlineCANumber" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineNumAccountNumber" ).removeClass( "hide" ).addClass( "show" );

		}
		else if(event.target.value == "MTNL Delhi Landline"){
			$( "#landlineNumStdCode" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineNumAccountNumber" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineCANumber" ).removeClass( "hide" ).addClass( "show" );
			
		}
		else{
			$( "#landlineNumStdCode" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineNumAccountNumber" ).removeClass( "show" ).addClass( "hide" );
			$( "#landlineCANumber" ).removeClass( "show" ).addClass( "hide" );
		}
	}
	
	function electricityProviderdropdownChange(event){
		console.log(event.target.value);
		if(event.target.value == "MSEDC MAHARASHTRA"){
			$( "#electricityProcessingCycle" ).removeClass( "hide" ).addClass( "show" );
			$( "#electricityBillingUnit" ).removeClass( "hide" ).addClass( "show" );
			$( "#electricityCycleNumber" ).removeClass( "show" ).addClass( "hide" );
			
		}else if(event.target.value == "Reliance Energy Limited"){
			$( "#electricityCycleNumber" ).removeClass( "hide" ).addClass( "show" );
			$( "#electricityProcessingCycle" ).removeClass( "show" ).addClass( "hide" );
			$( "#electricityBillingUnit" ).removeClass( "show" ).addClass( "hide" );
			
			
		}else{
			$( "#electricityProcessingCycle" ).removeClass( "show" ).addClass( "hide" );
			$( "#electricityBillingUnit" ).removeClass( "show" ).addClass( "hide" );
			$( "#electricityCycleNumber" ).removeClass( "show" ).addClass( "hide" );
		}
		
	}
	</script>
</body>
</html>



