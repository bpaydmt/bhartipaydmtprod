<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 



<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  }
   }); 
</script> 
 

 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {
   /*  $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	})
     */
    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Recharge - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Recharge - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Recharge - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Recharge Report',
                message:"Generated on" + "<%= currentDate %>" + "",
             
              
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Recharge - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    

    
} );









function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}



 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
 
            

 
 
<div class="box2">
	<div class="box-inner">
	
		<div class="box-header">
			<h2>Recharges and bill payments</h2>
		</div>
		
		<div class="box-content">
		
			<form action="RechargeReportAggreator" method="post">
<div class="row">

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										    <i class="fa fa-calendar"></i>&nbsp;
										    <span></span> <i class="fa fa-caret-down"></i>
										</div> 
										
										<input type="hidden"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="from"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
										
										<input
													type="hidden"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="to"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
								</div>
								<%-- <div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="hidden"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="to"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div> --%>
								
								
								<%-- 	<%if(user.getWhiteLabel()<1){ %>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									
									<br> 	<s:select list="#{'OAGG001050':'BHARTIPAY B2B','OAGG001052':'BHARTIPAY B2C','OAGG001053':'Go2Pay Wallet'}" name="inputBean.aggreatorid" />
											</div>
									<!-- headerKey="-1" headerValue="Select Search Engines" -->		
								<%} %>
							 --%>
								
								
											<%
											if(user.getWhiteLabel()==0){
											%>
												<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br>
									<s:select list="%{aggrigatorList}" headerKey="All"
												headerValue="All" id="aggregatorid"
												name="inputBean.aggreatorid" cssClass="form-username"
												requiredLabel="true"/>
											</div>
										<%} %>
											
								<div
									class="form-group col-md-3 txtnew col-sm-4 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="example" class="display" width="100%">
				<thead>
				
				
					<tr>
					
						<th><u>Date</u></th>
					<th><u>Agent Id</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Type</u></th>
						<th><u>Operator</u></th>
						<th><u>Number</u></th>
						<th><u>Amount</u></th>
						<th><u>Status</u></th>
						
						<th><u>Aggregator</u></th>
						<th><u>Operator Id</u></th>
						
						
					</tr>
				</thead>
				<tbody>
				<%
				for(RechargeTxnBean wtb:list){
				%>
				  	<tr>
				  	
		             <td><%=wtb.getRechargeDate()  %></td>
		            <td><%=wtb.getUserId()%></td>
		             <td><%=wtb.getRechargeId()%></td>
		             <td><%=wtb.getRechargeType() %></td>
		              <td><%=wtb.getRechargeOperator() %></td>
		              <td><%=wtb.getRechargeNumber() %></td>
		               <td><%=d.format(wtb.getRechargeAmt())%></td>
		                <td><%=wtb.getStatus()%></td>
		                 
		                 <td><%=wtb.getAggreatorid()%></td>
		                 <td><%=wtb.getOperatorTxnId()%></td>
		                
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
	 

 
 
 
        
        

        <!-- contents ends -->

       

  		</div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


