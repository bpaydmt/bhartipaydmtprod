              
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Recharge"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.MoneyRemittance"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.BankDetail"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TAG0"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Beneficiary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CardDetail"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.UserDetails"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            
<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->
 
 <%
 UserDetails userDetails=(UserDetails)request.getAttribute("userDetails");
 BankDetail bankDetail=null;
 Beneficiary beneficiary=null;
 CardDetail cardDetail=null;
 MoneyRemittance remittance=null;
 Recharge recharge=null;
 if(userDetails!=null){
	bankDetail=userDetails.getBankDetail();
	beneficiary= userDetails.getBeneficiary();
	cardDetail=userDetails.getCardDetail();
	remittance= userDetails.getMoneyRemittance();
	recharge=userDetails.getRecharge();
 }
 %>
<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Transaction Report</h2>

                
            </div>
								<div class="box-content row">
								<font color="red"><s:actionerror/> </font>
								<font color="blue"><s:actionmessage/> </font>
									
									
									
									<div class="col-md-8" style="
    margin-left: 16.33333%;
">
                          <table class="table table-striped" style="
    border: 1px solid #ccc;
    margin: 20px 0 0 0;
    / border-radius: 19px; /
">
                          <thead>
                          <%if(userDetails!=null&&recharge!=null){ %>
                                <tr><th>Transaction id  :  <%=userDetails.getAgentTransId() %></th><th>Date : <%=recharge.getTransDate() %></th>
                          </tr>
                          <%} %>
                          </thead>
                         <%if(beneficiary!=null){ %> 
                          <tbody><tr>
                              <td><strong>Beneficiary name</strong></td><td><%=beneficiary.getBeneficiaryName() %></td>
                          </tr>
                             <tr>
                              <td><strong>Beneficiary account no.</strong></td><td><%=beneficiary.getAccountNo() %></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Bank IFSC</strong></td><td><%=beneficiary.getiFSC() %></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Account type</strong></td><td><%=beneficiary.getAccountType() %></td>
                          </tr>
                          <%}if(remittance!=null){ %>
                            <tr>
                              <td><strong>Payment id</strong></td><td><%=remittance.getPaymentId()%></td>
                          </tr>
                             <tr>
                              <td><strong>Payment status</strong></td><td><%=remittance.getPaymentStatus() %></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Fund transfer no.</strong></td><td><%=remittance.getFundTransno() %></td>
                          </tr>
                            
                               <tr>
                              <td><strong>Transfer status</strong></td><td><%=remittance.getTransferStatus() %></td>
                          </tr>
                              <tr>
                              <td><strong>Message</strong></td><td><%=remittance.getMessage() %></td>
                          </tr>
<%} %>
                            </tbody>
                          </table>
                          <form action="Dmt" class="pull-left">
<input type="submit" value="Make another trasaction" class="btn form-btn btn-info btn-fill">
</form>
<%if(request.getAttribute("reinitflag")!=null&&((String)request.getAttribute("reinitflag")).equalsIgnoreCase("true")){ %>
 <%if(userDetails!=null&&recharge!=null){ %>
                        <form action="FundTransfer" class="pull-left" style="margin-left:15px">
                        
                        <input type="hidden" value="VALIDATEBENEF" name="dmtInputBean.type">
                        <input type="hidden" value="TRUE" name="dmtInputBean.reinitiate">
                        <input type="hidden" value="<%=userDetails.getAgentTransId()%>" name="dmtInputBean.previousAgentTransId">
                     
<input type="submit" value="Reinitiate trasaction" class="btn form-btn btn-info btn-fill">
</form>
<%} }%>
                   <br>   </div>
					


								</div>



							</div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


                