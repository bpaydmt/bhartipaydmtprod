<%@page import="java.text.DecimalFormat"%> 
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="com.bhartipay.lean.AssignedUnit"%>
 
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
  
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {

	     
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'AssignedUnit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'AssignedUnit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'AssignedUnit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'AssignedUnit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'AssignedUnit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
   
  

	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   function revokeAction(id,revokeId,rid,uid, amt){
	   $('#revoke-inp').val(id)
	   
	 
	 $("#revoke-id").val(revokeId)
          	  $("#requester-id").val(rid)
          	   $("#requester-user-id").val(uid)
          	    $("#requester-amount").val(amt)
          	         $('#revokeActionPopup').modal('show');
   }
 </script>
 

</head>

       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
 
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
        
 
            

 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>User Assigned Unit</h2>
		</div>
	<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color:green;">
		<s:actionmessage/>
		</font>	
		<div class="box-content  ">
		<div id="xyz" style="margin-top:20px">
			<table id="example" class="display">
				<thead>
				  <tr>
						<th width="5%"><u>Sr.No.</u></th>
						<th><u>User Type</u></th>
						<th><u>UserId</u></th>
						<th><u>SM Id</u></th>
						<th><u>SO ID</u></th>
						<th><u>Total Count</u></th>
						<th><u>Consume Count</u></th>
						<th><u>Remaining Count</u></th> 
					</tr>
				</thead>
				<tbody>
				<%
				List<AssignedUnit> count=(List<AssignedUnit>)request.getAttribute("countList");
				int counts=0;
				if(count!=null){				
				for(int i=0; i<count.size(); i++) {
					counts++;
					AssignedUnit tdo=count.get(i);
				
					%>
		          		  <tr>
		                <td><%=counts %></td>
		                <td><% if(tdo.getUsertype()==2){out.print("Retailer");}else if(tdo.getUsertype()==3){out.print("Distributor");}else if(tdo.getUsertype()==7){out.print("SuperDistributor");}else{out.print("");}  %></td>
      	 	            <td><%=tdo.getUserId()%></td>
		     			<td><%=tdo.getManagerId()%></td>
		     			<td><%=tdo.getSalesId()%></td>
		     			<td><%=tdo.getAllToken()%></td>
		     			<td><%=tdo.getConsumeToken()%></td>
		     			<td><%=tdo.getRemainingToken()%></td>
		          		  
                  	</tr>
			      <%} }%>	
			        </tbody>
			       </table>
		        </div>
		 
		</div>
	</div>
	</div>	
	
		

							
		
  

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<script src='js/bootstrap.min.js'></script>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



