<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript">
	
 $(function(){
		$("#dob, #paymentDate").datepicker({
		    	 language: 'en',
		    	 maxDate: new Date(),
		    	
		    });
			

	});
 
 function validatePancardApi(elm){
debugger
var aggr = $('#usertype').children("option:selected").val();
var type = parseInt(aggr);
	   //alert();
		var inp = $(elm).val();
		//sssssssalert(inp);
		if(inp.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){
			//alert(5555);
			$.ajax({
				method:'Post',
				url:'ValidatePan',
				data:"pan="+inp+"&userType="+type,
				success:function(result){
					console.log(result.status)
					
					if(result.status == "true"){
						if(type==3)
						insertErrMsg(elm,"This pan card number is already register with another Distributer.",true);
						else if(type==7)
						insertErrMsg(elm,"This pan card number is already register with another SuperDistributer.",true);
						  
						$(elm).addClass("registerPanCard")
						$("#panError").text("This pan card number is already register with another Distributer");
					}else{
						$("#panError").text("");
					}
				}
			})
		}else{
			
			
		}
		
		
	}


 function validateAdharApi(elm){
		debugger
		var aggr = $('#usertype').children("option:selected").val();
		var type = parseInt(aggr);
		
			//var reg ="";
			var inp = $(elm).val();
			//sssssssalert(inp);
			if(inp.match(/^[0-9]{12}$/)){
				//alert(5555);
				$.ajax({
					method:'Post',
					url:'ValidateAadhaar',
					data:"adhar="+inp+"&userType="+type,
					success:function(result){
						console.log(result.status)
						
						if(result.status == "true"){
							if(type==3)
							insertErrMsg(elm,"This aadhaar number is already register with another Distributer.",true);
							else if(type==7)
						    insertErrMsg(elm,"This aadhaar number is already register with another SuperDistributer.",true);
							
								
							$(elm).addClass("registerPanCard")
							$("#panError").text("This pan card number is already register with another agent");
						}else{
							$("#panError").text("");
						}
					}
				})
			}else{
				 if(inp.length == 0){
	 			  insertErrMsg(elm,"Please enter valid Aadhaar Number.",true);
	 			  fadeOutNonmandatory(elm)
	 			}
			}
			
			
		}




 function validateMobileApi(elm){
	 debugger
	 	 	var inp = $(elm).val();
	 	 	if(inp.match(/^[6789]\d{9}$/)){
	 	 		$.ajax({
	 	 			method:'Post',
	 	 			url:'ValidateMobile',
	 	 			data:"mobile="+inp,
	 	 			success:function(result){
	 	 				console.log(result.status)
	 	 				
	 	 				if(result.status == "true"){
	 	 					insertErrMsg(elm,"This mobile no is already register with us.",true);
	 	 					
	 	 				}else{
	 	 					$("#panError").text("");
	 	 				}
	 	 			}
	 	 		})
	 	 	}else{
	 	 		 if(inp.length == 0){
	 	 			  insertErrMsg(elm,"Please enter valid Mobile Number.",true);
	 	 			  fadeOutNonmandatory(elm)
	 	 			}
	 	 		//$("#panError").text("Please enter emailId");
	 	 	}
	  }

/* 

function validateEmailApi(elm){
debugger
	 	var inp = $(elm).val();
	 	if(inp.match(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i)){
	 		$.ajax({
	 			method:'Post',
	 			url:'ValidateEmail',
	 			data:"email="+inp,
	 			success:function(result){
	 				console.log(result.status)
	 				
	 				if(result.status == "true"){
	 					insertErrMsg(elm,"This email Id is already register with us.",true);
	 					$(elm).addClass("registerPanCard")
	 					$("#panError").text("This email is already register with another agent");
	 				}else{
	 					$("#panError").text("");
	 				}
	 			}
	 		})
	 	}else{
	 		 if(inp.length == 0){
	 			  insertErrMsg(elm,"Please enter valid Email ID.",true);
	 			  fadeOutNonmandatory(elm)
	 			}
	 	}
 }
  

*/

	
 function copyAddress(){
		
		if($("#addressCheckbox").prop("checked")){
				$("#shopaddress1").val($("#address1").val())
				$("#shopaddress2").val($("#address2").val())
				$("#shopstate").val($("#state").val())
				$("#shopcity").val($("#city").val())
				$("#shoppin").val($("#pin").val())
		}else{
		
				$("#shopaddress1").val("")
				$("#shopaddress2").val("")
				$("#shopstate").val("-1")
				$("#shopcity").val("")
				$("#shoppin").val("")
		}
		
	}


function  getSuperDistributer()
{
debugger
var list=document.getElementById("salesId");
var soId=list.options[list.selectedIndex].value;

	$.ajax({
        url:"GetSuperDist",
        cache:0,
        data:"userId="+soId,
        success:function(result){
        //var	s=JSON.stringify(result);   
       //var json = JSON.parse(result);
        //alert(json);

         document.getElementById('superdistributerid').innerHTML=result;
               
         }
   }); 

}


function changeSuperDist(aggId)
{  
debugger
var list=document.getElementById("usertype");
var usertype=list.options[list.selectedIndex].value;

if(usertype==3){
	$.ajax({
        url:"GetSuperDist",
        cache:0,
        data:"walletBean.salesId="+aggId,
        success:function(result){
        
               document.getElementById('superdis').innerHTML=result;
               
         }
  }); 
}else if(usertype==2)
{
	$.ajax({
        url:"GetDistributor",
        cache:0,
        data:"walletBean.salesId="+aggId,
        success:function(result){
        
               document.getElementById('disList').innerHTML=result;
               
         }
  }); 
	
}
	   
	return false;
}




 function showDiv(usertype){  
		if( usertype==7){
			document.getElementById("distb").style.display ="none";
			document.getElementById("superD").style.display ="none";
		}else if(usertype==3 ){
			document.getElementById("distb").style.display ="none";
			document.getElementById("superD").style.display ="block";
		}else if(usertype==2 )
		{
		   document.getElementById("distb").style.display ="block";
		   document.getElementById("superD").style.display ="none";
		} 
		
		
   }

function onLoadData()
{
	 
	var list=document.getElementById("usertype");
    var usertype=list.options[list.selectedIndex].value;
//alert(usertype);
	showDiv(usertype);
}
 
</script>
</head>

<body onload="onLoadData()">



	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->


	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<div id="main" role="main">
		<div id="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">



					<%
User user=(User)session.getAttribute("User");
%>

					<div class="box ">
						<div class="box-inner">
							<div class="box-header  ">
								<h2>
									<i class="glyphicon "></i>Create User
								</h2>

							</div>
							<div class="box-content">


								<form action="OnboardByBpmu" id="form_name"
									method="post" enctype="multipart/form-data" name="PG">

									<div class="row">
										<div class="row" align="left">
											<font id="err" color="red"><s:actionerror /> </font>
											<s:fielderror />
											<font id="suc" color="blue"><s:actionmessage /> </font> <input
												type="hidden" value="${csrfPreventionSalt}"
												name="csrfPreventionSalt">

										</div>
										 <% if(user.getUsertype()==9){ %>
										<input class="form-control" name="walletBean.createdby"
											id="createdby" type="hidden" value="<%=user.getId() %>" />		
										
										<input class="form-control" name="walletBean.managerId"
											id="managerId" type="hidden" value="<%=user.getId() %>" />	
										
										
										<input class="form-control" name="walletBean.subAggregatorId"
											id="subAggregatorId" type="hidden" value="-1" />
											
											
										<%}else{ %>
										<input class="form-control" name="walletBean.createdby"
											id="createdby" type="hidden"
											value="<%=user.getAggreatorid() %>" />
										<%} %>


										<input class="form-control" name="walletBean.agentid" type="hidden" value="-1" /> 
										<input class="form-control" name="walletBean.subAgentId" type="hidden" value="-1" />
										
										<!-- <input class="form-control" name="walletBean.distributerid" type="hidden" value="-1" /> -->
										
										<%if(user.getUsertype()==6 || user.getUsertype()==8 || user.getUsertype()==9){ %>

										<div class="col-md-12">

											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Select User Type <font
														color='red'> *</font></label>
													<s:select
														list="#{'7':'Super Distributor','3':'Distributor','2':'Retailer'}"
														name="walletBean.usertype" id="usertype" onchange="showDiv(document.getElementById('usertype').value);" >

													</s:select>
												</div>
											</div>

                                            
                                            <div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Sales Officer <font
														color='red'> *</font></label>
													 
													  <s:select list="%{soList}" headerKey="-1"
														headerValue="Select User Type" id="salesId"
														name="walletBean.salesId" 
														onchange="changeSuperDist(document.getElementById('salesId').value);" />
														
											   <%--  onchange="changeSuperDist(document.getElementById('salesId').value);" --%> 
													 
												</div>
											</div>


                                            <div class="col-md-4" id="distb">
												<div class="form-group">
													<label for="apptxt">Distributor <font
														color='red'> *</font></label>
												<div id="disList">
													  <s:select list="%{disList}" headerKey="-1"
												headerValue="Select User Type" id="distributerid"
												name="walletBean.distributerid" cssClass=""
												requiredLabel="true" />
												</div>
													 
												</div>
											</div>
										
											 <div class="col-md-4" id="superD">
												<div class="form-group">
													<label for="apptxt">Super Distributor <font
														color='red'> *</font></label>
													 <div id="superdis">
													  <s:select list="%{sdList}" headerKey="-1"
												headerValue="Select User Type" id="superdistributerid"
												name="walletBean.superdistributerid" cssClass=""
												requiredLabel="true"  />
													 </div>
												</div>
											</div>
											
									
										</div>

										<%}else{ %>
										<!-- 
										<input class="form-control" name="walletBean.usertype" id="usertype" type="hidden" value="3" />
										 -->
										<%} %>
										
										<br><br><br></br>
										
										<div class="col-md-12">

											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Name <font color='red'>
															*</font></label> <input type="text" name="walletBean.name"
														placeholder="Name" class="form-control userName mandatory"
														id="name" value="<s:property value='%{walletBean.name}'/>">

												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Email <font color='red'>
															*</font></label> <input type="text" name="walletBean.emailid"
														placeholder="Email"
														class=" form-control emailid mandatory" id="email"  onblur="validateEmailApi(this)"
														value="<s:property value='%{walletBean.emailid}'/>">

												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Mobile Number <font color='red'>
															*</font></label> <input type="text" name="walletBean.mobileno"
														placeholder="Mobile Number"
														class=" form-control mobile mandatory" id="mobile" onblur="validateMobileApi(this)"
														value="<s:property value='%{walletBean.mobileno}'/>"
														title="Mobile number must have 10 digits and should start with 6,7,8 or 9."
														maxlength="10" pattern="[6789][0-9]{9}"
														requiredmessage="invalid mobile number">

												</div>
											</div>
										</div>

										<div class="col-md-12">

                                          <div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">PAN <font color="red"> *</font></label>
													<input type="text" name="walletBean.pan"
														placeholder="PAN Number "
														class=" form-control mandatory pancard-vl" id="pan"
														required value="<s:property value='%{walletBean.pan}'/>"
														onblur="validatePancardApi(this)"> 
												<%-- 		
														<span id="panError" style="margin: 27px 0 0 0; float: left; font-size: 11px; color: red;"></span>
												 --%>
												</div>

											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Date Of Birth <font color='red'>
															*</font></label> <input type="text" name="walletBean.dob"
														placeholder="DD/MM/YYYY"
														class="form-control mandatory datepicker-here1" id="dob"
														value="<s:property value='%{walletBean.dob}'/>">

												</div>
											</div>
											
											<div class="col-md-4">
											<div class="form-group">
															<label for="apptxt">Select Gender <font color='red'> *</font></label>
															 <select name="walletBean.gender">
															 <option>Male</option>
															 <option>Female</option>
															 </select>	
										  </div>
										  </div>



										</div>
									
									
										<div class="col-md-12">
											
 											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">AADHAR <font color="red">
															*</font></label> <input type="text" name="walletBean.adhar"
														placeholder="Aadhar Card "
														class=" form-control  mandatory aadhar-vl" id="adhar" onblur="validateAdharApi(this)"
														required value="<s:property value='%{walletBean.adhar}'/>">

												</div>
											</div>

											<div class="col-md-8">
												<div class="form-group">
													<label for="apptxt">Address<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.address1" placeholder="Address"
														value="<s:property value='%{walletBean.address1}'/>"
														class=" form-control address-vl" maxlength="100"
														id="address1" />

												</div>
											</div>											
										</div>

 

										<div class="col-md-12">
 
                                            <div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">City<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.city" placeholder="City"
														class=" form-control userName" id="city"
														value="<s:property value='%{walletBean.city}'/>" />

												</div>
											</div>
 
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">State<font color='red'>
															*</font></label> <select name="walletBean.state"
														class=" form-control mandatory" id="state">
														<option value="-1">------------Select
															State------------</option>
														<option value="Andaman and Nicobar Islands">Andaman
															and Nicobar Islands</option>
														<option value="Andhra Pradesh">Andhra Pradesh</option>
														<option value="Arunachal Pradesh">Arunachal
															Pradesh</option>
														<option value="Assam">Assam</option>
														<option value="Bihar">Bihar</option>
														<option value="Chandigarh">Chandigarh</option>
														<option value="Chhattisgarh">Chhattisgarh</option>
														<option value="Dadra and Nagar Haveli">Dadra and
															Nagar Haveli</option>
														<option value="Daman and Diu">Daman and Diu</option>
														<option value="Delhi">Delhi</option>
														<option value="Goa">Goa</option>
														<option value="Gujarat">Gujarat</option>
														<option value="Haryana">Haryana</option>
														<option value="Himachal Pradesh">Himachal Pradesh</option>
														<option value="Jammu and Kashmir">Jammu and
															Kashmir</option>
														<option value="Jharkhand">Jharkhand</option>
														<option value="Karnataka">Karnataka</option>
														<option value="Kerala">Kerala</option>
														<option value="Lakshadweep">Lakshadweep</option>
														<option value="Madhya Pradesh">Madhya Pradesh</option>
														<option value="Maharashtra">Maharashtra</option>
														<option value="Manipur">Manipur</option>
														<option value="Meghalaya">Meghalaya</option>
														<option value="Mizoram">Mizoram</option>
														<option value="Nagaland">Nagaland</option>
														<option value="Orissa">Orissa</option>
														<option value="Pondicherry">Pondicherry</option>
														<option value="Punjab">Punjab</option>
														<option value="Rajasthan">Rajasthan</option>
														<option value="Sikkim">Sikkim</option>
														<option value="Tamil Nadu">Tamil Nadu</option>
														<option value="Tripura">Tripura</option>
														<option value="Uttaranchal">Uttaranchal</option>
														<option value="Telangana">Telangana</option>
														<option value="Uttar Pradesh">Uttar Pradesh</option>
														<option value="West Bengal">West Bengal</option>
													</select>

												</div>
											</div>


											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">PIN Code<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.pin" placeholder="PIN Code" maxlength='6'
														min='100000' class=" form-control pincode" id="pin"
														value="<s:property value='%{walletBean.pin}'/>" />

												</div>
											</div>

											
											 
										</div>


										<div class="col-md-12">


											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Shop Name<font color='red'>
															*</font></label> <input type="text" name="walletBean.shopName"
														placeholder="Shop Name"
														value="<s:property value='%{walletBean.shopName}'/>"
														class=" form-control address-vl mandatory upperCase"
														maxlength="100" id="shopname" />

												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="apptxt">Shop Address<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.shopAddress1" placeholder="Shop Address"
														value="<s:property value='%{walletBean.shopAddress1}'/>"
														class=" form-control address-vl upperCase" maxlength="100"
														id="shopaddress1" />

												</div>
											</div>
 
                                            <div class="col-md-2">
												<div class="form-group">
													<label for="apptxt">Same as above</label> <input
														type="checkbox" onchange="copyAddress()"
														id="addressCheckbox" value="copy above addresss" />

												</div>
											</div> 
 
										</div>
										<div class="col-md-12">
											
										<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Shop City<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.shopCity" placeholder="Shop City"
														class=" form-control userName upperCase" id="shopcity"
														value="<s:property value='%{walletBean.shopCity}'/>" />

												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Shop State<font color='red'>
															*</font></label> <select
														name="walletBean.shopState" id="shopstate">
														<option value="-1">------------Select
															State------------</option>
														<option value="Andaman and Nicobar Islands">Andaman
															and Nicobar Islands</option>
														<option value="Andhra Pradesh">Andhra Pradesh</option>
														<option value="Arunachal Pradesh">Arunachal
															Pradesh</option>
														<option value="Assam">Assam</option>
														<option value="Bihar">Bihar</option>
														<option value="Chandigarh">Chandigarh</option>
														<option value="Chhattisgarh">Chhattisgarh</option>
														<option value="Dadra and Nagar Haveli">Dadra and
															Nagar Haveli</option>
														<option value="Daman and Diu">Daman and Diu</option>
														<option value="Delhi">Delhi</option>
														<option value="Goa">Goa</option>
														<option value="Gujarat">Gujarat</option>
														<option value="Haryana">Haryana</option>
														<option value="Himachal Pradesh">Himachal Pradesh</option>
														<option value="Jammu and Kashmir">Jammu and
															Kashmir</option>
														<option value="Jharkhand">Jharkhand</option>
														<option value="Karnataka">Karnataka</option>
														<option value="Kerala">Kerala</option>
														<option value="Lakshadweep">Lakshadweep</option>
														<option value="Madhya Pradesh">Madhya Pradesh</option>
														<option value="Maharashtra">Maharashtra</option>
														<option value="Manipur">Manipur</option>
														<option value="Meghalaya">Meghalaya</option>
														<option value="Mizoram">Mizoram</option>
														<option value="Nagaland">Nagaland</option>
														<option value="Orissa">Orissa</option>
														<option value="Pondicherry">Pondicherry</option>
														<option value="Punjab">Punjab</option>
														<option value="Rajasthan">Rajasthan</option>
														<option value="Sikkim">Sikkim</option>
														<option value="Tamil Nadu">Tamil Nadu</option>
														<option value="Tripura">Tripura</option>
														<option value="Uttaranchal">Uttaranchal</option>
														<option value="Uttar Pradesh">Uttar Pradesh</option>
														<option value="West Bengal">West Bengal</option>
													</select>


												</div>
											</div>



											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Shop PIN Code<font color='red'>
															*</font></label> <input
														type="text" name="walletBean.shopPin"
														placeholder="Shop PIN Code" maxlength='6' min='100000'
														class=" form-control pincode" id="shoppin"
														value="<s:property value='%{walletBean.shopPin}'/>" />

												</div>
											</div>
										</div>
 
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Bank Name<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.bankName" placeholder="Bank Name"
														class=" form-control userName upperCase" id="bankName"
														value="<s:property value='%{walletBean.bankName}'/>" />

												</div>
											</div>


											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">Account Number<font color='red'>
															*</font></label> <input
														type="text" name="walletBean.accountNumber"
														placeholder="Account Number"
														class=" form-control number upperCase" id="accountNumber"
														value="<s:property value='%{walletBean.accountNumber}'/>" />

												</div>
											</div>


											<div class="col-md-4">
												<div class="form-group">
													<label for="apptxt">IFSC Code<font color='red'>
															*</font></label> <input type="text"
														name="walletBean.ifscCode" placeholder="IFSC Code"
														class=" form-control  upperCase" id="ifscCode"
														value="<s:property value='%{walletBean.ifscCode}'/>" />

												</div>
											</div>


										</div>




										<div class="col-md-12">
											 
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Proof of Identity (PAN)<font color='red'>
															*</font></label>
													 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile1}'/>"
														name="walletBean.file1" id="dp" required="true"
														placeholder="Select Address Proof" required
														readonly="readonly" required>

												</div>
												
												
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Proof of Address<font color='red'>
															*</font></label>
														 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile2}'/>"
														name="walletBean.file2" id="dp"
														placeholder="Select Id Proof" required readonly="readonly">
												</div>
												

											 

                                        </div>


                                       <div class="col-md-12">
											 
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Bank Account Details</label>
														 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile3}'/>"
														name="walletBean.file3" id="dp" required="true"
														placeholder="Select Address Proof" required
														readonly="readonly" required>

												</div>
												
												
												<div class="col-md-3" style="margin-top: 30px;">
													<div class=" form-group">
													<label for="apptxt">Payment Details</label>
														 
													</div>
												</div>


												<div class="col-md-3" style="margin-top: 30px;">
													<input type="file"
														value="<s:property value='%{upload.myFile4}'/>"
														name="walletBean.file4" id="dp"
														placeholder="Select Id Proof" required readonly="readonly">
												</div>
												
                                     </div>
 

 
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">

													<input type="button" class="btn btn-success  submit-form"
														style="margin-top: 20px;" value="Submit"
														 onclick="agentOnboardFormSubmit('#form_name')" />



													<button type="reset" onclick="resetForm('#form_name')"
														class="btn reset-form btn-info btn-fill"
														style="margin-top: 20px;">Reset</button>


												</div>
											</div>

										</div>

										<br> <br>
									</div>
								</form>







							</div>
						</div>

					</div>





				</div>
			</div>
		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

 <script src="./js/validateWallet.js"></script>
	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


