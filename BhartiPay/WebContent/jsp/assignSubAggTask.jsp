<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 
	function getMenuList(aggId)
	{  
		
		$.ajax({
	        url:"GetUserMenuMapping",
	        cache:0,
	        data:"menuMapping.userId="+aggId,
	        success:function(result){
	      
	               document.getElementById('wconfig').innerHTML=result;
	               
	         }
	  });  
		return false;
	}
	
	
	
	function changeTheme(id){
		 var rd = $(id).find("input:radio"); 
		 rd.prop("checked", true)
		 $('html').attr("class",rd.val())

		}
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>User Configuration</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12"> 
               
		<form action="SaveUserMenuMapping" id="form_name" method="post">
				<div class="row">
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>

												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Sub-aggregator</label>
																	
													<s:select list="%{subAggList}" headerKey="-1"
												headerValue="Please select Sub-aggregator" id="aggregatorid"
												name="menuMapping.userId" cssClass="form-control"
												requiredLabel="true"
												onchange="getMenuList(document.getElementById('aggregatorid').value);"
												 />
															
															
														</div>
													</div>
											

            
<div id="wconfig">
					
												
											
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Menu</label>
													<s:select list="%{aggMenuList}" size="10"
																id="appIdsSelect" name="menuMapping.menuType"
																
																multiple="true" placeholder="Allowed transaction type"/>
													
													</div>
													</div>
									</div>
												
            							</div>		

					<div class="col-md-12">

			
											
													
													<div class="col-md-4">
										<div class="form-group">
										
										<button type="submit" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button>

									
									</div>
									</div>


									</div>
									
									
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->




</body>
</html>


