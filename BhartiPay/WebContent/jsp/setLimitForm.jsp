		<%@taglib prefix="s" uri="/struts-tags" %>
					<form action="Savelimit.action" method="post">
												 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Minimum wallet balance</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																<input type="hidden" name="config.id" value="<s:property value='%{config.id}'/>">
																<input type="hidden" name="config.walletid" value="<s:property value='%{config.walletid}'/>">
																<input type="text"
																value="<s:property value='%{config.minbal}'/>"
																name="config.minbal" id="minbal" class="form-control"
																placeholder="Minimum wallet balance" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per transaction</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtpertx}'/>"
																name="config.maxamtpertx" id="maxamtpertx" class="form-control"
																placeholder="Max amount/transaction" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per day</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperday}'/>"
																name="config.maxamtperday" id="maxamtperday" class="form-control"
																placeholder="Max amount/day" required/>

																</div>
															</div>
														</div>

													</div>
													
												
													
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per week</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperweek}'/>"
																name="config.maxamtperweek" id="maxamtperweek" class="form-control"
																placeholder="Max amount/week" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per month</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtpermonth}'/>"
																name="config.maxamtpermonth" id="maxamtpermonth" class="form-control"
																placeholder="Max amount/month" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per quarter</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperquarter}'/>"
																name="config.maxamtperquarter" id="maxtxperweek" class="form-control"
																placeholder="Max transaction amount/quarter" required/>

																</div>
															</div>
														</div>

													</div>
													
													 <div class="row">
													
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per half yearly</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperhalfyearly}'/>"
																name="config.maxamtperhalfyearly" id="maxtxpermonth" class="form-control"
																placeholder="Max transaction/half yearly" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per year</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperyear}'/>"
																name="config.maxamtperyear" id="maxamtperacc" class="form-control"
																placeholder="Max amount/year" required/>

																</div>
															</div>
														</div>
														
																<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per day</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxperday}'/>"
																name="config.maxtxperday" id="maxtxperday" class="form-control"
																placeholder="Max transaction/day" required/>

																</div>
															</div>
														</div>

													</div>
													
													
												<!-- 
	private String id;
	private String walletid;
	private double minbal;
	private double maxamtpertx;
	private double maxamtperday;
	private double maxamtperweek;
	private double maxamtpermonth;
	private int maxtxperday;
	private int maxtxperweek;
	private int maxtxpermonth;
	private double maxamtperacc; -->		
													
													
													
														 <div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per week</label>
															
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxperweek}'/>"
																name="config.maxtxperweek" id="maxtxperweek" class="form-control"
																placeholder="Max transaction/week" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max transaction per month</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxtxpermonth}'/>"
																name="config.maxtxpermonth" id="maxtxpermonth" class="form-control"
																placeholder="Max transaction/month" required/>

																</div>
															</div>
														</div>
														<div class="col-md-4" style="margin-top: 30px;">
															<div class=" form-group">
															<label>Max amount per account</label>
																<div class="input-group">
																	<div class="input-group-addon">
																		<span class="glyphicon glyphicon-calendar"></span>
																	</div>
																
																<input type="text"
																value="<s:property value='%{config.maxamtperacc}'/>"
																name="config.maxamtperacc" id="maxamtperacc" class="form-control"
																placeholder="Max amount/accounnt" required/>

																</div>
															</div>
														</div>

													</div>
													
												
													
													<div class="row">
														<div class="col-md-4" style="margin-top: 30px;">
															 <button type="submit" class="btn btn-info" name="FrgtPwd">Submit</button>
														</div>
													</div>
													

													<div class="row"></div>
													<div class="clearfix"></div>
												</form>