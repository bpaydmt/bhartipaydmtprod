<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>

<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Cash Deposit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 

function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}
function cashDepositAction(id,type){
	$("#hidden-cashval").val(id)
	$('#cashAction').modal('show');
	$('#remoark-inp').val('')
	
	console.log(type)
	console.log(id)
	if(type=="Accept"){
		$("#cash-action").attr("action","AcceptCashDeposit")
		
	}else{
		
		$("#cash-action").attr("action","RejectCashDeposit")
	}
	//var con = confirm("Are you sure you want to accept the request?")
	
	/* if(prom != ""){
		
		$(form).submit();
		
	}else{
		return false;
	} */
}

function cashDepositReject(form){
	
	var con = confirm("Are you sure you want to reject the request?")
	if(con){		
		$(form).submit();
		
	}else{
		return false;
	}
}
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       <script>
       
       function acceptMoney(){
    	   
       }
       
       
       </script>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		<div class="box-header well">
			<h2>Cash Deposit</h2>
		
		</div>
		
	
		<div class="box-content row">
		<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color: blue;">
		<s:actionmessage/>
		</font>
		<%-- <form action="SaveCashDeposit.action"  method="post" enctype="multipart/form-data">
		<div class="col-md-4">
			<div class="form-group">
			<label>Select your transaction type</label>
				<s:select  list="%{txnTypeList}" id="drop-neft" onchange="dropInfo(this)" name="depositMast.type" headerKey="-1" headerValue="Select your transaction type">
					 
				</s:select>
			</div>
		
		
			
		</div>
			<div class="col-md-4">
				<div class="form-group">
				<label for="apptxt">Enter Amount </label> 
				<input class="form-control"  type="text" id="amount" maxlength="20" placeholder="Enter Amount" 
				name="depositMast.amount"
				value="<s:property value='%{depositMast.amount}'/>"
				>
	
				</div>
			</div>
			<div class="col-md-4" >
			<div id="NEFT" style="display:none;">
			
			
			
			 <div class="form=group">
			 <label>NEFT/IMPS/RTGS transaction Id</label>
			<input type="text" class="form-control"
			name="depositMast.neftRefNo"
				value="<s:property value='%{depositMast.neftRefNo}'/>"
			 />
			 </div>
			
	
			
			</div>
			<div id="RECIEPT" style="display:none;">
			
			
			<label >Upload cash deposit slip </label>
			<input type="file" class="form-file-inp" 
			name="depositMast.myFile1"
			value="<s:property value='%{depositMast.myFile1}'/>"
			/>
			
			
			
			</div>
			</div>
			
			<div class="col-md-12"> <input type="submit" class="btn btn-info" /></div>
			
		
		
		</form> --%>
		
		
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>User Type</u></th>
						<th><u>Agent Name</u></th>
						<th><u>Id</u></th>
						<th><u>Agent Id</u></th>
						<th><u>Type</u></th>
						<th><u>Amount</u></th>
						<th><u>Reference No.</u></th>
						<th><u>Bank Name</u></th>
						<th><u>Branch Name</u></th>	
						<th><u>Deposit Date</u></th>					
						<th><u>Receipt</u></th>
						
						<th><u></u></th>
									
					</tr>
				</thead>
				<tbody>

				<%
		
			List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					CashDepositMast tdo=eList.get(i);%>
		          		  <tr>
		          		<td><%=tdo.getUserId().contains("OAGG")==true?"Aggregator":tdo.getUserId().contains("OXMA")==true?"Agent":tdo.getUserId().contains("OXMC")==true?"Customer":tdo.getUserId().contains("OXMD")==true?"Distributor":"-"%></td> 
		          	   <td><%=tdo.getAgentname()%></td>
		          	    <td><%=tdo.getDepositId()%></td>
		          	    <td><%=tdo.getUserId()%></td>
		          	    <td><%=tdo.getType()%></td>
		              <td><%=d.format(tdo.getAmount())%></td>
		               <td><%=tdo.getNeftRefNo()%></td>
		              <td><%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%><%}%></td>
		               <td><%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%><%}%></td>
		               <td><%if(tdo.getDepositDate()==null){}else{%><%=tdo.getDepositDate()%><%}%></td>
		           
		             
		             <td>
		             
		             
		             
		             
		             <%if(tdo.getReciptPic()!=null&&!tdo.getReciptPic().isEmpty()){%>
		             <img alt="userImg" src="<%if(tdo.getReciptPic()!=null&&tdo.getReciptPic().length()>200){%>data:image/gif;base64,<%} %><%=tdo.getReciptPic()%>" data-toggle="modal" data-target="#myModal<%=tdo.getDepositId()%>" width="40px" height="40px"/>
		                        <div id="myModal<%=tdo.getDepositId()%>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	   
	  
	       <img alt="userImg" src="data:image/gif;base64,<%=tdo.getReciptPic()%>"  />
	     
	      
	    </div>
	
	  </div>
	</div>
		            <%} %>
		
			             </td> 
			             <td>
			            
			            
			             <input type="button"  onclick="cashDepositAction('<%=tdo.getDepositId()%>','Accept')" class="btn btn-sm btn-block btn-success grid-small-btn" value="Accept">
			         
			              
			             <input type="button" onclick="cashDepositAction('<%=tdo.getDepositId()%>','Reject')" class="btn btn-sm btn-block btn-success grid-small-btn" value="Reject">
			         

			             </td>
		            
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
	</div>

</div>
</div>

</div>
        
<!-- contents ends -->


</div>
</div><!--/.fluid-container-->
<div id="cashAction" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 416px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remark</h4>
      </div>
      <div class="modal-body">
        <form id="cash-action" method="post">
        <div class="form-group">
        	<input type="hidden" name="depositMast.depositId" id="hidden-cashval" />
        	<input type="text" placeholder="Remark" name="depositMast.remark" id="remoark-inp" class="form-control" required/>
        	
        	
        </div>
        <input type="submit" value="submit" class="btn btn-info btn-fill btn-block"/>
        </form>
      </div>
     
    </div>

  </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->



<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



