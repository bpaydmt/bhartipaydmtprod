<%@page import="com.bhartipay.wallet.kyc.Utils.KYCRES"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    
    
    KYCRES  Authresponse=(KYCRES)request.getAttribute("result");
    %>


<!DOCTYPE html>
<html>
<head>
<script>
 function showAndroid(result) {
	 alert(result);
	 BhartipayKyc.setKycResults(result);
 }
</script>
	<title>KYC Update</title>


	 <script src="./js/jquery.min.js"></script> 
	  
 <link rel="stylesheet" type="text/css" href="jspas/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">
	 
		<link rel="stylesheet" type="text/css" href="css/mainStyle.css">

</head>
<body onload="showAndroid('<%=Authresponse.getUUID()%>|<%=Authresponse.getERRCODE()%>|<%=Authresponse.getERRMSG()%>|<%=Authresponse.getSERVICETYPE()%>|<%=Authresponse.getRRN()%>')">

<div class="container" style="max-width:500px">
<h4 style="color:red;font-size:18px;text-align:center;padding:20px 0">KYC authentication transaction has been failed</h4>
<table style="width:100%" class="table table-bordered table-striped">

  <tr>
    <td><strong>Error Code</strong></td>
    <td><%=Authresponse.getERRMSG()%></td> 
  
  </tr>
  <tr>
    <td><strong>Transaction ID</strong></td>
    <td><%=Authresponse.getRRN()%></td> 
   
  </tr>
</table>

<a href="MoneyTransfer?kycForm=true">Back</a>
</div>

</body>
</html>
