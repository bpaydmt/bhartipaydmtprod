<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.MudraMoneyTransactionBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.FundTransactionSummaryBean"%>

<%
Date date=new Date();
SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
String currentDate=sdf.format(date);

Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");

String merchantName = mapResult.get("appname");
	String domainName = mapResult.get("domainName");
	FundTransactionSummaryBean transSummary=(FundTransactionSummaryBean)request.getAttribute("result");
	
	String logo=(String)session.getAttribute("logo");
	%>
<div class="print-receipt">
<div id="transLogo" style="display:none;margin:20px 0 20px">
<img src="<%=logo%>" style="width:150px" id=""/>

<span class="dateStapm"><strong> </strong></span>
</div>



<div id="transSummary">

      <h2>Transaction Summary</h2>
  <table class="table table-bordered table-striped">
   
    <tbody>
      <tr class="onlyprint">
     
        <td ><strong>Agent Name :</strong></td>
        
        <td ><%=transSummary.getAgentName()%></td>
          <td ><strong>Agent Mobile :</strong></td>
        <td ><%=transSummary.getAgentMobileNo()%></td>
        
         
      </tr> 
      
      
      <tr>
        <td ><strong>Sender Mobile</strong></td>
        <td ><%=transSummary.getSenderMobile()%></td>
          <td ><strong>Beneficiary Name</strong></td>
        <td ><%=transSummary.getBeneficiaryName()%></td>
      </tr>
   
     
    
       <tr>
          <td ><strong>Beneficiary IFSC</strong></td>
        <td ><%=transSummary.getBeneficiaryIFSC()%></td>
          <td ><strong>Beneficiary Account No.</strong></td>
        <td ><%=transSummary.getBeneficiaryAccNo()%></td>
     </tr>
        <tr>
         <td ><strong>Amount</strong></td>
        <td ><%="INR "+transSummary.getAmount()%></td>
          <td ><strong>Beneficiary Bank :</strong></td>
        <td ><%=transSummary.getBankName()%></td>
   
      </tr>
        <tr>
      <tr>
         <td ><strong>Transaction Mode</strong></td>
        <td ><%=transSummary.getMode()%></td>
          <td ><strong>Date & Time :<strong></td>
        <td ><%
        
        List<MudraMoneyTransactionBean> mudraTransaction1 =transSummary.getMudraMoneyTransactionBean();
        MudraMoneyTransactionBean mudraTrans1= mudraTransaction1.get(0);
        %>
        <%-- <%=mudraTrans1.getPtytransdt() %>   --%><%=transSummary.getPtDate() %></td>
     </tr>
       
  </table>
    <table class="table table-bordered table-striped">
   <thead stype="color:black!important">
   		<tr>
   			<!--   <th><strong>Beneficiary Account No.</strong></th> -->
             <th>Transaction Number</th>
   			<!-- 
   			<th>Transaction Type</th>
   			<th>Credit Amount</th>
   			<th>Debit Amount</th>
   			 -->
   			 <th>Bank Ref. No.</th>
   			<th>Transaction Amount</th>
   			<th>Status</th>
   			<th>Remark</th>
   		</tr>
   
    </thead>
      <tbody>
      <%
     List<MudraMoneyTransactionBean> mudraTransaction=transSummary.getMudraMoneyTransactionBean();
      
      for(int i=0;mudraTransaction.size()>i;i++){
    	  MudraMoneyTransactionBean mudraTrans=mudraTransaction.get(i);
       if(!mudraTrans.getNarrartion().equals("WALLET LOADING")){
      %>
      
      <tr>
        
          <td ><%=mudraTrans.getId()%></td>
          <td ><%=mudraTrans.getBankRrn()%></td>
          
         <%--   <td ><%=mudraTrans.getNarrartion()%></td>
          <td ><%="INR "+mudraTrans.getCrAmount() %></td> --%>
          
          <td ><%="INR "+mudraTrans.getDrAmount()%></td>
          <td ><strong><%=mudraTrans.getStatus()%></strong></td>
          
         <%--  <td ><%=mudraTrans.getRemark()%></td>   !mudraTrans.getId().startsWith("TR") --%>
           <td>
              <% if(!"Transaction SUCCESSFUL(REFUND To AGENT)".equalsIgnoreCase(mudraTrans.getRemark())){
        	        out.print(mudraTrans.getRemark());
                 }else{
                	out.print("Refund to Agent");
            	 
                 }
               %>
        	   </td>
         
        </tr>  
      
      <%} } %>
       
       </tbody></table>
<div class="row">
<div class="col-xs-4">Charges :As Applicable</div>
<div class="col-xs-8" style="text-align:right;">    <strong>Thanks for using <%=merchantName %>!</div>
</div>

  
</div>
 <div class="modal-footer">
 
          <button class="btn-primary pull-right btn" onclick="window.print('.modal-dialog')">Print</button>
 </div>
 </div>
 <style>
 
 .dateStapm{display:none}

 tr.onlyprint{display:none !important;}
 #transSummary table th{color:#000000;}
 @media print {
    .btn-primary.pull-right.btn, .mainRow, header, footer, .modal-header,h2 {
       display:none !important;
    }
    .print-receipt{width:100%; float:none; margin-left:14%;}
    .modal-dialog{width:100%}
    .noprint{display:none !important; }
 tr.onlyprint{ display:contents !important;}


#transSummary .table-bordered>tbody>tr>td, #transSummary .table-bordered>tbody>tr>th, #transSummary .table-bordered>tfoot>tr>td, #transSummary .table-bordered>tfoot>tr>th, #transSummary .table-bordered>thead>tr>td, #transSummary .table-bordered>thead>tr>th {
    border: 1px solid #999!important;
    font-size:13px!important;
    padding:10px!important
}
 
    #transLogo, .dateStapm{display:block!important}
      #transLogo img{  margin: 10px 0 22px auto;}
   #transLogo{    display:none;margin: 20px 0 11px;text-align: center;position: relative;}
    .modal-content{
    box-shadow: none!important;
    border: none;
    }
    .table > thead > tr > th {
    background: #f36d25;
    color: #000 !important;
    font-size:24px;
}
.alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 19px!important;}
  .dateStapm{  font-size: 20px;
    float: right;
    position: absolute;
    right: 0px;
    top: 71px;
    }
    table-bordered>tbody>tr>td{
    	font-size:29px!important
    }
     table th{
    	font-size:18px!important;
    	
    }
}
 
 @page {
   size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 0mm 5mm 5mm 5mm;  
    font-size:29px!important;
   
}
 
 
 </style>