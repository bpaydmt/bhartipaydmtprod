<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<% response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
  response.setHeader("Pragma","no-cache"); //HTTP 1.0 
  response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
  response.flushBuffer();
  %> 
<%@ page session="true" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="theams.jsp"></jsp:include> 
<%

User user=(User)session.getAttribute("User");
String logo=(String)session.getAttribute("logo");
String banner=(String)session.getAttribute("banner");
Map<String,String> mapResult=(Map<String,String>)session.getAttribute("mapResult");
String favicon="";
if(mapResult!=null)
favicon=mapResult.get("favicon");
%>
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- CSS -->
    
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="shortcut icon" href="./images/<%=favicon%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
		<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>	
		<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
		<!--<title>Powerpay</title>-->
		<script src="assets/js/jquery-1.11.1.min.js"></script>
		
		<!-- <link href="css/base.css" rel="stylesheet" type="text/css" /> -->
		<script type="text/javascript" src = "js/user/login.js"></script>
		 <script src="js/validateWallet.js"></script>
		</head>
		<body >
    
	 
		 
	      <div class="container-fluid">
        <div class="navbar-header">
             <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>"  style="width:150px; height:auto; margin-left:7%;"/>
					<%
						}else{
					%>
					 <img alt="Bhartipay" src="./newmis/img/wallet_logo.png" style="width:150px; height:auto; margin-left:7%;"/>
					<%} %>
               
        </div>
        <!-- <div class="form-top-rightl">
            <img src="assets/img/logo1.png">
        </div> -->

    </div>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4 style="color:#999; padding-left:8px;">Please Change Your Password.</h4>	
                        		</div>
                        		<div class="form-top-right">
                                	<i class="fa fa-edit"></i>
                        			
                        		</div>
                            </div>
                            <div class="form-bottom">
                           
<s:form action="ChangePassword" method="POST" cssClass="login-form" id="first_change">
			                    <!-- <form role="form" action="" method="post" class="login-form"> -->
			                    <div class="form-group">
			                   
			                    <font color="red"><s:actionerror></s:actionerror></font>
			                    <font color="blue"><s:actionmessage></s:actionmessage></font>
			                   
			                    </div>
			                    	<div class="form-group">
			                		<input type="hidden" name="user.usertype" value="<s:property value='%{user.usertype}'/>">
			                    	<input type="hidden" name="user.userId"  value="<s:property value='%{user.userId}'/>">
			                        
			                        </div>
			                        
			                       <%--   <div class="form-group">
			                    	
			              			<input type="password"   name="user.oldpassword" placeholder="Current Password" class="form-username mandatory form-control" id="u" value="<s:property value='%{user.oldpassword}'/>">
			                        
			                        </div> --%>
			                        
			                        <div class="form-group">
			                    	
			              			<input type="password"   name="user.password" placeholder="New Password" class="form-username mandatory password-vl mandatory form-control" id="u" value="<s:property value='%{user.password}'/>">
			                        
			                        </div>
			                        <div class="form-group">
			                    	
			              			<input type="password"   name="user.confirmPassword" placeholder="Confirm New Password" class="form-username confirm-vl mandatory form-control" value="<s:property value='%{user.confirmPassword}'/>">
			                        
			                        </div>
			                        
			                    <!--     <button type="submit" class="btn">Submit</button> -->
			                        <!-- <button type="submit" class="btn" name="FrgtPwd" onclick="return frmValidation();">Submit</button> -->
			                        
                                    <br>
                                    <br>
                                    
			                    </s:form>
			                    <button onclick="submitVaForm('#first_change')" class="btn submit-form" name="FrgtPwd" >Submit</button>
		                    </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            
        </div>
<div style="margin-top: 70px;">
       <jsp:include page="footer1.jsp"/>
       </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>	 

<head>
<meta HTTP-EQUIV='Pragma' CONTENT='no-cache'/>
<meta HTTP-EQUIV='Cache-Control' CONTENT='no-cache'/>
<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
</head>
</html>





