


<jsp:include page="theams.jsp"></jsp:include>

<%
String logo=(String)session.getAttribute("logo");

%>

<%@taglib prefix="s" uri="/struts-tags"%>

<head>
  <jsp:include page="reqFiles.jsp"></jsp:include>
  	<link href='./css/jquery.datetimepicker.min.css' rel='stylesheet'> 
  	  	<link href='./css/chosen.css' rel='stylesheet'> 
  
  	 	<script src="./js/flight.js?i=10012222"></script>
 

</head>
   

<body>
<script id="adult-field" type="text/x-handlebars-template">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<select class="form-control adult-gender gender-select f-required" >
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3 ">
			<input type="text" class="form-control adult-inp first-Name alph-vl f-required" placeholder="First Name"/>
			<div class='errorMsg'></div>
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control adult-inp last-Name alph-vl f-required" placeholder="Last Name"/>
			<div class='errorMsg'></div>
			
		
		</div>
		
			<div class="col-md-3">
			<input type="date" style="display:none" class="form-control adult-inp dob" placeholder="DOB"/>
			<div class='errorMsg'></div>
			
		
		</div>
		
	
	</div>
</div>

      </script>
      <script id="children-field" type="text/x-handlebars-template">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<select class="form-control child-gender gender-select f-required">
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control child-inp first-Name alph-vl f-required" placeholder="First Name"/>
			<div class='errorMsg'></div>
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control child-inp last-Name alph-vl f-required" placeholder="Last Name"/>
<div class='errorMsg'></div>
			
		
		</div>
		
			<div class="col-md-3">
			<input type="text"  class="form-control child-inp dob f-required" value=""   placeholder="Date of Birth"/>
				<input type="hidden" class="dobHidden"  value=""   placeholder="Date of Birth"/>

<div class='errorMsg'></div>
			
		
		</div>
		
	
	</div>
</div>

      </script>
      <script id="infant-field" type="text/x-handlebars-template">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<select class="form-control infant-gender gender-select f-required">
				<option value="-1">Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control infant-inp first-Name alph-vl f-required" placeholder="First Name"/>
			
		
		</div>
			<div class="col-md-3">
			<input type="text" class="form-control infant-inp last-Name alph-vl f-required" placeholder="Last Name"/>
			
		
		</div>
		<div class="col-md-3">
			<input type="text"  class="form-control infant-inp dob f-required" value="" placeholder="Date of Birth"/>
			
		
		</div>
		</div>
			
		
	
	
</div>

      </script>
<script id="booking-details" type="text/x-handlebars-template">

<div id="booking-details" >
<div class="row">
<div class="tickets-block">

<div class="block-head">
<span class="step-count">01</span>
Ticket Details
</div>

<div class="details-block">
		
		
		{{#each fareQuote.Response.Results.Segments}}

<h2>
<span id="origin{{@index}}">{{@index}}</span> 
<i class="fa fa-plane" style="margin: 0 10px;"></i> 
 <span id="destina{{@index}}"></span> 
<span class="f-date"></span>
</h2>

		<div class="ticket-block">

			{{#each this}}
				<div class="ticket-single">
					<div class="col-md-3">
						<img  src="./images/flights/{{Airline.AirlineCode}}.png" class="f-img" alt="{{Airline.AirlineName}}" />
					<div class="airline-name">{{Airline.AirlineName}}</div>	
<div class="airline-code">{{Airline.AirlineCode}}-{{Airline.FlightNumber}}</div>
					</div>
				<div class="col-md-3">
					<div class="time">{{Origin.DepTime}} Hrs</div>
					<div class="airport-code">{{Origin.Airport.AirportCode}}</div>
				<div class="airport-name">{{Origin.Airport.AirportName}}</div>
				</div>
	<div class="col-md-3">
					---
				</div>
	<div class="col-md-3">
					<div class="time">{{Destination.ArrTime}} Hrs</div>
					<div class="airport-code">{{Destination.Airport.AirportCode}}</div>
				<div class="airport-name">{{Destination.Airport.AirportName}}</div>
				</div>
				</div>

				{{/each}}
		</div>
		{{/each}}

</div>



<div class="block-head">
<span class="step-count">02</span>
Traveller Details
</div>
<div class="details-block" id="Travallers-details">
<div id="flight-err"></div>

<form action="bookFlight" method="post" id="ticketingForm">
	
	
<input type="hidden"  id="bookingadultCount" name="pass.adultCount" />
<input type="hidden"  id="bookingchildCount" name="pass.childCount" />
<input type="hidden"  id="bookingInfantCount" name="pass.infantCount" />
<input type="hidden"  id="bookingFare" name="pass.fare" />
<input type="hidden"  id="bookingFareReturn" value="" name="pass.fareReturn" />
<input type="hidden"  id="bookingFareBreakDown"  name="pass.fareBreakDown" />
<input type="hidden"  id="bookingFareBreakDownReturn" value="" name="pass.fareBreakDownReturn" />
<input type="hidden"  id="bookingTravelId" name="pass.travelId" />
<input type="hidden" id="BookingTraceId" name="pass.traceId" />
<input type="hidden" id="BookingResultIndex" name="pass.resultIndex" />

			

</form>
</div>

<div class="block-head">
<span class="step-count">03</span>
Confirmation & Payment
</div>
<div class="details-block" id="Travallers-p-details">
<div class="container-fluid">

	
	
		<div class="row">
			<div class="col-md-4">
				<label></label>
			</div>
			<div class="col-md-4">
				<input type="button" class="btn-block btn-primary" value="Confirm and Pay" onclick="ticketBooking()" id="confirmBtn" >
			</div>
	</div>
</div>
</div>
</div>
<div class=" fare-summary">
<div class="summary-header"></div>
<h4>Fare Summary</h3>
<div class="travaller-summary">
<h5>Travellers</h5>

<div class="info-block"><label> Adult(s) </label> <span class="s-value"> 0{{fareQuote.Response.AdultCount}} <span class="currency">Pax </span></span></div>
<div class="info-block" id="child-pax"><label>Child(ren) </label> <span class="s-value"> 0{{fareQuote.Response.ChildCount}}<span class="currency">Pax</span></span></div>
<div class="info-block" id="infant-pax"><label> Infant(s) </label> <span class="s-value"> 0{{fareQuote.Response.InfantCount}}<span class="currency"> Pax</span></span></div>


</div>



<div class="fare-break-total">
<h5>Fare </h5>



</div>



<div class="fare-break">
<div class="info-block"><label>Base Fare</label> <span class="s-value">  <span class="currency">{{fareQuote.Response.Results.Fare.Currency}}</span>{{fareQuote.Response.Results.Fare.BaseFare}}</span></div>
<div class="info-block"><label>Taxes and surcharges</label> <span class="s-value"> <span class="currency">{{fareQuote.Response.Results.Fare.Currency}}</span>{{fareQuote.Response.surchargesTaxes }}</span></div>
<div class="info-block"><label>Convenience Fee</label> <span class="s-value"> <span class="currency">{{fareQuote.Response.Results.Fare.Currency}}</span>{{fareQuote.Response.Results.Fare.ConvenienceFee}}</span><div>
</div>


</div>

<div class="bottom-total">
<div class="info-block">
<label>Total Amount</label><span class="s-value">
 <span class="currency">{{fareQuote.Response.Results.Fare.Currency}}</span>
<span class="amount-span">{{fareQuote.Response.Results.Fare.ChargedFare}}</span>
<span>

</div>


</div>


</div>

</div>

</div>


		
</div>


</script>


<script id="flight-template" type="text/x-handlebars-template">
  <div class="flights-wrapper">
<span id="flightBookingError" style="color:red"></span>
    <div class="f-header row">
<div class="col-md-12">
<h2><span class="travel-info"><span class="trav-heading"> 
<div class="city-code">{{Response.Origin}}</div>
<div class="city-name">{{Response.OriginCity}}</div>
 </span> <i class="fa fa-plane" style="margin: 0 20px;"></i>
<span class="trav-heading"> 
<div class="city-code">{{Response.Destination}}</div>
<div class="city-name">{{Response.DestiCity}}</div>
</span>
</span>

<a href="flightBooking"><span class="search-modifiy"> Modify Search</span> </a>

</h2>

<h4 style="font-size: 13px;
    color: #8a8a8a;
    /* border-bottom: 1px solid #ddd; */
    padding: 0 0 17px 0">{{Response.leavingtDate}}</h4>

</div>
<div class="col-md-4">

</div>

</div>
    
<div class="single-flight"  >

<div class="row" id="singleDetails">
	<div class="col-md-12">
<h3><span class="travel-info">
<span class="trav-heading"> 
<div class="city-code">{{Response.Origin}}</div>
<div class="city-name">{{Response.OriginCity}}</div>
 </span> <i class="fa fa-plane" style="margin: 0 20px;"></i>
<span class="trav-heading"> 
<div class="city-code">{{Response.Destination}}</div>
<div class="city-name">{{Response.DestiCity}}</div>
</span></span></h3>
<h5 style="font-size: 11px;
    color: #8a8a8a;
    /* border-bottom: 1px solid #ddd; */
    padding: 0 0 17px 0">{{Response.leavingtDate}}</h5>
	</div>

</div>
<div class="row">
<div class="col-md-3">
<label class="text-left sortedPrice" id="airlinesingle" style="text-align: left;"   onclick="sortByAirline('single','#airlinesingle')">Airline</label>
</div>

<div class="col-md-2">
<label id="departTab" class="sortedPrice"  onclick="sortTime('single','#departTab','depart')">Depart</label>
</div>
<div class="col-md-2">
<label id="arrivalTab" class="sortedPrice" onclick="sortTime('single','#arrivalTab','arrival')">Arrive</label>
</div>
<div class="col-md-2">
<label id="durationTab" class="sortedPrice" onclick="sortDuration('single','#durationTab')">Duration</label>
</div>
<div class="col-md-3 text-right">
<label  style="text-align: right;    padding-right: 9px;" id="priceTab" class="sortedPrice" onclick="sortPrice('single','#priceTab')">Price</label>
</div>



</div>

      {{#each Response.Results.[0]}}

<div class="flight-block">
<div class="flight-left col-md-3">
<span class="f-timing">AIRLINE</span>
<div class="img-holder">

<img class="img" src="./images/flights/{{AirlineCode}}.png" alt="{{Segments.[0].Airline.AirlineName}}" />
</div>

<div class="fl-pry">
<div class="fl-name">
{{Segments.[0].[0].Airline.AirlineName}}
</div>
<div class="fl-code">
{{AirlineCode}}-{{Segments.[0].[0].Airline.FlightNumber}}
</div>

</div>

</div>

 <div class="col-md-2 text-center">
<span class="f-timing">DEPART</span>
{{departureTime}}
</div>
 <div class="col-md-2 text-center">
<span class="f-timing">ARRIVE</span>
{{arrivalTime}}
</div>
 <div class="col-md-2 text-center">
<span class="f-timing">DURATION</span>
{{travalDuration}}
<div class="stopsCount"  data-toggle="tooltip" data-placement="bottom" title="{{via}}">{{stops}}</div>
{{#if Segments.[0].[0].NoOfSeatAvailable}}
<div class="available-seats">Seats left:<span>{{Segments.[0].[0].NoOfSeatAvailable}}</span></div>
{{/if}}
</div>
 <div class="col-md-3 text-right">
<span class="f-timing">PRICE</span>

<button class="flight-btn btn submit-form btn-info btn-fill" onclick="selectFlight('single-flight',this,{{flightPrice }},'{{FareBreakdown.[0].Currency}}', '{{AirlineCode}}',{{IsLCC}}, '{{ResultIndex}}')">
<span class="f-amount">{{FareBreakdown.[0].Currency}} {{flightPrice}}</span>
<i class="fa fa-check-circle"></i>
</button>
<div class="stopsCount" style="
    margin:3px 0 0 0">{{refundalbe}}</div>
</div>
</div>
{{/each}}
</div>
{{#if Response.Results.[1]}}

<div class="return-flight" >
<div class="row" id="returnDetails">
	<div class="col-md-12">
<h3>
<span class="travel-info"><span class="trav-heading"> 
<div class="city-code">{{Response.Destination}}</div>
<div class="city-name">{{Response.DestiCity}}</div>
 </span> <i class="fa fa-plane" style="margin: 0 20px;"></i>
<span class="trav-heading"> 
<div class="city-code">{{Response.Origin}}</div>
<div class="city-name">{{Response.OriginCity}}</div>
</span>
<a id="searchModi" href="flightBooking"><span class="search-modifiy"> Modify Search</span> </a>
<span>
</h3>
<h5 style="font-size: 11px;
    color: #8a8a8a;
    /* border-bottom: 1px solid #ddd; */
    padding: 0 0 17px 0">{{Response.leavingtDateReturn}}</h5>
	</div>

</div>
<div class="row">
<div class="col-md-3">
<label id="airlineReturn" class="text-left sortedPrice" style="text-align: left;    margin: 0 0 0 10px;" onclick="sortByAirline('return','#airlineReturn')">Airline</label>
</div>

<div class="col-md-2">
<label id="departTabReturn" class="sortedPrice" onclick="sortTime('return','#departTabReturn','depart')">Depart</label>
</div>
<div class="col-md-2">
<label id="arrivalTabReturn" class="sortedPrice" onclick="sortTime('return','#arrivalTabReturn','arrival')">Arrive</label>
</div>
<div class="col-md-2">
<label id="durationTabReturn" class="sortedPrice" onclick="sortDuration('return','#durationTabReturn')">Duration</label>
</div>
<div class="col-md-3 text-right">
<label  id="priceTabReturn" class="sortedPrice" style="text-align: right;    padding-right: 14px;" onclick="sortPrice('return','#priceTabReturn')">Price</label>
</div>



</div>

      {{#each Response.Results.[1]}}

<div class="flight-block">
<div class="flight-left col-md-3">
<span class="f-timing">AIRLINE</span>
<div class="img-holder">

<img class="img" src="./images/flights/{{AirlineCode}}.png" alt="{{Segments.[0].Airline.AirlineName}}" />
</div>

<div class="fl-pry">
<div class="fl-name">
{{Segments.[0].[0].Airline.AirlineName}}
</div>
<div class="fl-code">
{{AirlineCode}}-{{Segments.[0].[0].Airline.FlightNumber}}
</div>

</div>

</div>

 <div class="col-md-2 text-center">
<span class="f-timing">DEPART</span>
{{departureTime}}
</div>
 <div class="col-md-2 text-center">
<span class="f-timing">ARRIVE</span>
{{arrivalTime}}
</div>
 <div class="col-md-2 text-center">
<span class="f-timing">DURATION</span>
{{travalDuration}}
<div class="stopsCount"  data-toggle="tooltip" data-placement="bottom" title="{{via}}">{{stops}}</div>
{{#if Segments.[0].[0].NoOfSeatAvailable}}
<div class="available-seats">Seats left:<span>{{Segments.[0].[0].NoOfSeatAvailable}}</span></div>
{{/if}}
</div>

 <div class="col-md-3 text-right">
<span class="f-timing">PRICE</span>
<button class="flight-btn btn submit-form btn-info btn-fill" onclick="selectFlight('return-flight',this,{{flightPrice }},'{{FareBreakdown.[0].Currency}}', '{{AirlineCode}}',{{IsLCC}}, '{{ResultIndex}}')">
{{FareBreakdown.[0].Currency}} {{flightPrice }}
<i class="fa fa-check-circle"></i>
</button>
<div class="stopsCount" style="margin: 3px 0 0 0;">{{refundalbe}}</div>
</div>
</div>
{{/each}}

</div> 
{{/if}}  
  </div>
</script>
    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            		
            		

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
	
	
		
	
		<div class="box-content row">
		<div class="box-header well">
                <h2><i class="glyphicon "></i>Flight Booking</h2>

                
            </div>
	<div id="fliight-wrapper">
	<div class="flight-form">
			
		<form action="flightSearch"  method="post"  id="flightForm">
		<h1>Book flights easily and hassle free </h1>
		<font style="color:red;">
		<s:actionerror/>
		</font>
		<font style="color: green;">
		<s:actionmessage/>
		</font>
				<div class="container-fluid">
			
			<div class="row">
			<div class="col-md-12"><span id="errSpanFlight" ></span></div>
			<div class="col-md-12">
			
			<label class="booking-type fbtn-active booking-tab">One way<input type="radio" name="trip" value="1" class="radioBtn" checked></label>
			
			<label class="booking-type  booking-tab">Round trip <input type="radio" name="trip" value="2" class="radioBtn" id="roundTripRadio"></label>
			
			
			
    		
			</div>
			<div class="col-md-4">
			
			
			</div>
				<div class="col-md-12">
				<div class="form-group flight-inp1">
				<label>From</label>
					<select class="form-control airport-select " id="flight-origin" data-placeholder="To" >
					<option value="-1">Departure </option>
					 </select>
					</div><div class="exchange-icon"> <i class="fa fa-exchange"></i></div> <div class="form-group flight-inp1">
				<label>To</label>
						<select class="form-control airport-select " id="flight-destination" data-placeholder="From.">
					<option value="-1">Destination </option>
					 </select>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group flight-inp1">
				<label>Depart Date and Preferred Time</label>
					<input class="form-control" type="text" id="departureDate"  /> 
					</div>
						<div class="form-group flight-inp1" id="returnFlight" style="float:right;display:none">
				<label>Return Date and Preferred Time</label>
					<input class="form-control" type="text" id="returnDate"  /> 
					</div>
				
				</div>
				
			
			
			</div>
			<div class="row">
			<div class="flight-inp1" style="
  width: 278px;
    margin: 0 40px 0 0;;
			">
				<div class="col-md-4">
					<div class="form-group ">
				<label>Adult(s)</label>
					<input class="form-control passanger" id="adult"  value="1" min="1"  max="9" type="number" /> 
					<div class="number-incre">
					<i class="fa fa-minus-circle" onclick="travallerCount('#adult-t','Minus')"></i><span class="t-num adult-t" id="adult-t">1</span>
					<i class="fa fa-plus-circle"  onclick="travallerCount('#adult-t','Plus')"></i>
					
					</div>
					
					
					</div>
				
				</div>
					<div class="col-md-4">
					<div class="form-group ">
				<label>Children</label>
					<input class="form-control passanger" id="children" min="0" value="0" max="9"  type="number" /> 
					<div class="number-incre">
					<i class="fa fa-minus-circle" onclick="travallerCount('#children-t','Minus')"></i><span class="t-num children-t" id="children-t">0</span>
					<i class="fa fa-plus-circle" onclick="travallerCount('#children-t','Plus')"></i>
					
					</div>
					<span class="t-msg">2-12 Yrs</span>
					</div>
				
				</div>
				
					<div class="col-md-4">
					<div class="form-group ">
				<label>Infant(s)</label>
					<input class="form-control passanger"  id="infant" min="0" value="0" max="9"  type="number" /> 
					<div class="number-incre">
					<i class="fa fa-minus-circle" onclick="travallerCount('#infant-t','Minus')"></i><span class="t-num infant-t" id="infant-t">0</span>
					<i class="fa fa-plus-circle" onclick="travallerCount('#infant-t','Plus')"></i>
					
					</div>
					<span class="t-msg">Below 2 Yrs</span>
					</div>
				
				</div>
				</div>
					<div class="form-group flight-inp1" style="    width: calc(49% - 38px);">
				<label>Class</label>
					<select id="travelClass">
						<option value="2">Economy
						</option>
						<option value="4">Business
						</option>
						<option value="3">Premium Economy
						</option>
					
					</select>
						<label  id="directFlight"">Non-Stop Flight<input type="checkbox" name="FlightStop"  value="true"  > </label>
					</div>
				
			
			
			</div>
			<div class="row" style="margin: 0 0 23px 0;
    float: right;">
    <div class="col-md-12">

				<input type="submit"  value="Search flights" class="btn submit-form btn-info btn-fill" />
				 
			
			</div>
			
			</div>
						</div>
						</form>
						
						
		</div>
	</div>
		
		
	
		</div>
	
	


		</div>
	</div>
	</div>	
		
		

							
		
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       <input type="hidden" id="fareBreakDown" /> 
<div class="bookingFooter">
	<div class="ch-container">
	<div class="row ">
		
		<div class="col-lg-12 col-sm-12">
		<div class="container-fluid">
		<div style="width:calc(50% - 100px);float:left" id="booksingle-flight"></div>
		<div style="width:calc(50% - 100px);float:left" id="bookreturn-flight"></div>
		<div class="final-booking" >
		<input type="hidden" id="singleFlightPrice" />
		<input type="hidden" id="returnFlightPrice" />
		
		<input type="hidden" id="AdultCount" />
		<input type="hidden" id="ChildCount" value="0" />
		<input type="hidden" id="InfantCount" value="0" />
		<input type="hidden" id="TraceId" />
			<input type="hidden" id="JourneyType" value="1" />
		<input type="hidden" id="IsLCC" />
			
		<input type="hidden" id="AirlineCode" />
		<input type="hidden" id="ResultIndex" />
		<input type="hidden" id="IsLCCR" />
			
		<input type="hidden" id="AirlineCodeR" />
		<input type="hidden" id="ResultIndexR" />
		
	
		<div id="total-amount"><span id="curr"></span><span id="cost"></span></div>
			<button class="btn submit-form btn-info btn-fill" onclick="bookFlightAction()" id="booking-btn">Book</button>
		</div>
		</div>
		
		
		</div>
	
	</div>
	
	
	</div>

</div>


</div>
</div>

<!-- Modal -->
<div id="errModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
  
  
      <div class="modal-body" style="padding: 0;margin: 0px;">
          <div class="alert alert-danger" style="margin:0px;display: block;padding:20px!important">
    <strong>Failed!</strong> <span id="errMsgFli"></span>
    <button type="button" class="close" style=" color: #650000;"data-dismiss="modal">&times;</button>
  </div>
      </div>
        <div class="alert alert-danger">
    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
    </div>

  </div>
</div>

<!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

 	 	<script src="./js/jquery.datetimepicker.full.min.js"></script>
 	 		
 	 			<script src="./js/prism.js"></script>
<script>

$(function(){
	 var d = new Date(),
     date = (d.getFullYear())+'/'+(d.getMonth()+1)+'/'+(d.getDate());
	$("#departureDate, #returnDate").val(date + " 00:00")
	$("#departureDate").datetimepicker({
	
		allowTimes: ['00:00', '08:00', '14:00','19:00', '01:00'],
		defaultTime : '00:00',
		 minDate:new Date(),
		
		 onChangeDateTime:function(dp,$input){
			 $("#returnDate").val($input.val())
			}
		
	});
	$("#returnDate").datetimepicker({
	
		allowTimes: ['00:00', '08:00', '14:00','19:00', '01:00'],
		defaultTime : '00:00',
		defaultDate:new Date(),
		minDate:new Date($("#departureDate").val()),
		
	});
});
 </script>
<script src="js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="./newmis/js/jquery.cookie.js"></script>
<script src="./newmis/js/jquery.noty.js"></script>
<script src="./newmis/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<%-- <script src="./newmis/js/charisma.js"></script> --%>
<div class="flight-loading-div">


<div class="loading-centerdiv">
	 <%
 				if(logo != null && !logo.isEmpty()) {
 					%>
				 <img  src="<%=logo%>" />
					<%
						}
	 
	 %>
	 <h2>Hang on! Preparing your Itinerary</h2>
	 <div class="container-fluid">
	 	<div class="col-md-5" id="load-origin">
	 	
	 	</div>
	 	<div class="col-md-2">
	 		<i class="fa fa-plane flight-animation" id="loading-f-animation"></i>
	 	</div>
	 	<div class="col-md-5" id="load-destination">
	 	
	 	</div>
	 
	 </div>
	 <div class="progress-bar-div">
	 
	 </div>
	

</div>

</body>
</html>



