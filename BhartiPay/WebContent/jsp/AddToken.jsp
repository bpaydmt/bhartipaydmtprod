<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="com.bhartipay.lean.DistributorIdAllot"%>

<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/ >


<% 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
%>

<script type="text/javascript">
  function showSearchDetails(){

  	 var div = $("#searchDetails")
  	 
  	 if(div.is(":hidden")){
  	  div.show()
  	  $("#showDetailsLink span").text("Hide details")
  	  $("#showDetailsLink .fa").addClass("fa-minus-square")
  	  $("#showDetailsLink .fa").removeClass("fa-plus-square")
  	 }else{
  	  div.hide()

  	 
  	  $("#showDetailsLink span").text("Show details")
  	  $("#showDetailsLink .fa").removeClass("fa-minus-square")
  	  $("#showDetailsLink .fa").addClass("fa-plus-square")
  	 }
  	}

</script>


<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#to').val(start.format('DD-MMM-YYYY'));
         $('#from').val(end.format('DD-MMM-YYYY'));

        }
     ); 
    $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
   }); 
</script>

<script type="text/javascript"> 
	$(document).ready(function() {

		    $('#cashDepositDate').datepicker({
		     language: 'en',
		     autoClose:true,
		     maxDate: new Date(),
		    });
	 }) 	
</script>

<script type="text/javascript"> 
    $(document).ready(function() { 
    /* $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	}) */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        "order": [[ 0, "desc" ]],
        buttons: [
         {
         
            extend: 'copy',
            text: 'COPY',
            title:'Recharge - ' + '<%= currentDate %>',
            message:'<%= currentDate %>',
        },  {
         
            extend: 'csv',
            text: 'CSV',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
         
            extend: 'excel',
            text: 'EXCEL',
            title:'Recharge - ' + '<%= currentDate %>',
        
        }, {
         
            extend: 'pdf',
            text: 'PDF',
            title:'Recharge Report',
            message:"Generated on" + "<%= currentDate %>" + "",
         
          
        },  {
         
            extend: 'print',
            text: 'PRINT',
            title:'Recharge - ' + '<%= currentDate %>',
          
        },{
            extend: 'colvis',
            columnText: function ( dt, idx, title ) 
            {
                return (idx+1)+': '+title;
            }
        }
        ]
    } ); 
    } );  
	/* function converDateToJsFormat(date) {

	var sDay = date.slice(0,2);
	var sMonth = date.slice(3,6);
	var yYear = date.slice(7,date.length)

	return sDay + " " +sMonth+ " " + yYear;
	}  */
</script>
<style>
.row-m-15 {
	/*margin: 5px 8px 10px;*/
	border-bottom: 1px solid #ccc;
	padding: 1px 0 8px;
	float: left;
	width: 100%;
}

.box-content {
	padding: 10px;
	background-color: #fff;
	/*     margin-top: 15px;
    margin-bottom: 15px; */
}

.custom-file-input {
	color: transparent;
}

.custom-file-input::-webkit-file-upload-button {
	visibility: hidden;
}

.custom-file-input::before {
	content: 'Upload Payment Slip';
	color: black;
	display: inline-block;
	background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
	border: 1px solid #999;
	border-radius: 3px;
	padding: 6px 12px;
	outline: none;
	white-space: nowrap;
	-webkit-user-select: none;
	cursor: pointer;
	text-shadow: 1px 1px #fff;
	/* font-weight: 700;
  font-size: 10pt; */
	border-radius: 15px;
	position: absolute;
	top: 22px;
}

.custom-file-input:hover::before {
	border-color: black;
}

.custom-file-input:active {
	outline: 0;
}

.custom-file-input:active::before {
	background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}

input[type=file] {
	display: inline-block !important;
}

.custom-date {
	color: black;
	display: inline-block;
	/* background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
	 */
	 background:none;
	/*  border: 1px solid #999; */
	border-radius: 3px;
/* 	padding: 6px 12px; */
	outline: none;
	white-space: nowrap;
	-webkit-user-select: none;
	cursor: pointer;
	text-shadow: 1px 1px #fff;
	/* font-weight: 700;
  font-size: 10pt; */
	border-radius: 15px;
	/* position: absolute; */
	top: 22px;
	width: 320px;
    right: 50px;
}

</style>

<script type="text/javascript">

function addId(){
	 debugger

	var a= document.getElementById("assignId").value;
	var b= document.getElementById("idCharge").value;
    
	var c=a*b;
	 document.getElementById("totalCharge").value=c;
	 
	}

/* 
function onlyNumberKey(evt) {
   debugger 
    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        {
        return false;
        }else{
debugger
        	var a= document.getElementById("assignId").value;
        	var b= document.getElementById("idCharge").value;
             
        	
        	var c=a*b;
        	 document.getElementById("totalCharge").value=c;
                  
    return true;
    }
}
 */
</script>


</head>

<% 
  User user = (User) session.getAttribute("User");
  Date myDate = new Date();
  System.out.println(myDate);
  SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
  String toDate=format.format(myDate);
  Calendar cal = Calendar.getInstance();
  cal.add(Calendar.DATE, -0);
  Date from= cal.getTime();    
  String fromDate = format.format(from);
  Object[] commDetails=(Object[])session.getAttribute("commDetails");
  Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
 
 %>



<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->



	<!-- left menu starts -->
	<jsp:include page="mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<!-- contents starts -->

	<div id="main" role="main">
		<div id="content">
			 
			 <div class="box-inner">
						<div class="box-header">
							<h2>Add Token</h2>
						</div>
					<div class="box-content">
						<font style="color: red;"> <s:actionerror />
						</font> <font style="color: blue;"> <s:actionmessage />
						</font>
 
                    <%
					DistributorIdAllot allotId=(DistributorIdAllot)request.getAttribute("allotId");
					 
					
					if(allotId!=null){	 %>
					<div class="col-md-2 col-xs-12" style="padding:10px 0px;"> 
						<h6><strong><span style="green"> Total Count -  <%=allotId.getAllToken() %></span></strong></h6>
					</div>
					
					<div class="col-md-2 col-xs-12" style="padding:10px 0px;"> 
						<h6><strong><span style="color:red"> Used Count -  <%=allotId.getConsumeToken() %></span></strong></h6>
					</div>	
						
					<div class="col-md-2 col-xs-12" style="padding:10px 0px;"> 
					   <h6><strong> <span style="color:blue"> Remaining Count -  <%=allotId.getRemainingToken() %></span></strong></h6>
					</div> 
					
					<div class="col-md-4 col-xs-12" style="padding:10px 0px;"> 
					   <h6><strong> <span style="color:blue">  <% if(allotId.getAggregatorId().contains("OAGG")){}else{ allotId.getAggregatorId();} %></span></strong></h6>
					</div>
					
					<%} %>
               <br><br><br>
 
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
                   <form action="UpdateTokenId" id="form_name" method="post" >

                               <div class="col-md-12">
                              <%-- 
								<div class="col-md-4">
									<div class="form-group">
										<label for="apptxt">Assigned Token From  </label> 
										<input type="text" name="walletBean.name"
											placeholder="Name" class="form-control userName"
											id="name" value="<%=user.getId() %>" readonly="readonly">

									</div>
								</div>
								 --%>
								<div class="col-md-3">
									<div class="form-group">
										<label for="apptxt">Assigned Token To <font color='red'>
												*</font></label> <input type="text" name="walletBean.name"
											placeholder="Name" class="form-control"
											id="name" value="<%=allotId.getUserId() %>" readonly="readonly">
                                     </div>
								</div>
							 
								<div class="col-md-3">
									<div class="form-group">
										<label for="apptxt">ID Charges <font color='red'> *</font></label> 
												<input type="text" name="walletBean.mobileno"  class="form-control number"
											placeholder="ID Charges"  id="idCharge" onblur="addId()"   >

									</div>
								</div>
                        
                                  <div class="col-md-3">
									<div class="form-group">
										<label for="apptxt">Assign ID <font color='red'> *</font></label> 
											<input type="text" name="walletBean.utrNo"  placeholder="Assign Id" class="form-control" 
											id="assignId"  onblur="addId()">    <!-- onkeypress="return onlyNumberKey(event)" -->
                                     </div>
								   </div>
                
                                  <div class="col-md-3">
									<div class="form-group">
										<label for="apptxt">Total Charges <font color='red'> *</font></label> 
										 <input type="text"   id="totalCharge" readonly="readonly">
                                     </div>
								   </div>
                
                               </div>


                                 <div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">

													<input type="button" class="btn btn-success  submit-form"
														style="margin-top: 20px;" value="Submit"
														 onclick="agentOnboardFormSubmit('#form_name')" />



													<button type="reset" onclick="resetForm('#form_name')"
														class="btn reset-form btn-info btn-fill"
														style="margin-top: 20px;">Reset</button>


												</div>
											</div>

										</div>
                 
                 
                 </form>
   
   
               </div>
           </div>



					 </div>
			</div>
 

			</div>

		</div>
		 

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%-- <script src="js/bootstrap.min.js"></script> --%>

	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<%-- <script src="./newmis/js/charisma.js"></script> --%>
	
	<script>
	function OpenInNewWindow(data){
		var image = new Image();
        image.src = "data:image/jpg;base64," + data;

        var w = window.open("");
        w.document.write(image.outerHTML);
	}
	</script>

</body>
</html>




