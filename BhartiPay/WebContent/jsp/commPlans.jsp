
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommPercViewMast"%>
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommPercRangeMaster"%>
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommInputBean"%>
<%@page import="com.bhartipay.wallet.commission.persistence.vo.CommPercMaster"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
 
<%
DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Commission Plans - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Commission Plans - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Commission Plans - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Commission Plans - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Commission Plans - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

 
  <script type="text/javascript">

function getDistributerByAggId(aggId)
{  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function getSubagentByAgentId(agentId)
{  
	
	$.ajax({
        url:"GetSubagent",
        cache:0,
        data:"reportBean.agentId="+agentId,
        success:function(result){
        	
               document.getElementById('subagent').innerHTML=result;
               
         }
  });  
	return false;
}

function searchSubmit(act){
	 
	  if(act=='search'){
	  document.spt.action='ShowCommByPlanId';
	  document.spt.submit();
	  }else if(act=='define'){
		  document.spt.action='GetCommPerc';
		  document.spt.submit();
	  }
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>Commission Plans</h2>
		</div>
		
		<div class="box-content row">
					<form  method="post" name="spt">
		</br>
		<font color="red"><s:actionerror/> </font>
		<font color="blue"><s:actionmessage/> </font>		
		</br><input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
							<div class="form-group  col-md-12  txtnew  col-xs-6">
								<div class="col-sm-3">
									<label for="email">Select Plan</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{plans}" headerKey="-1"
												headerValue="Select Plan" id="aggregator"
												name="reportBean.planId" cssClass="form-username"
												requiredLabel="true"
												/>
										</div>
									</div>
								</div>
								
								<div class="col-sm-3">
									<label for="email">Transaction Type</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
<s:select list="%{txnType}" headerKey="-1"
																		headerValue="All" id="txnType"
																		name="reportBean.txnType" cssClass="form-username"
																		/>
										</div>
									</div>
								</div>
							
								<div	class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit">
										<!-- <input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Search"> -->
											<button class="btn btn-sm btn-block btn-success" onclick="searchSubmit('search');">Search</button>
									</div>
								</div>
								
								<%-- <div	class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit">
									<!-- <form action="GetCommPerc.action" method="post"> -->
																		<s:select list="%{plans}" headerKey="-1"
																		headerValue="Select plan" id="plan"
																		name="cpmaster.planid" cssClass="form-username"
																		/>
																		<s:select list="%{txnType}" headerKey="-1"
																		headerValue="Select transaction type" id="txnType"
																		name="cpmaster.txnid" cssClass="form-username"
																		/>
										<button class="btn btn-sm btn-block btn-success" style="width:174px" onclick="searchSubmit('define');">Define Commission</button>
											
									</div>
								</div>
								 --%>
							</div>

</form>
	<form action="UpdatePlan" method="post">
<div class="form-group  col-md-12  txtnew  col-xs-2">
<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
	<input type="hidden" name="reportBean.planId" value="<s:property value='%{reportBean.planId}'/>"/>
	<input type="hidden" name="reportBean.txnType" value="<s:property value='%{reportBean.txnType}'/>"/>
								<div class="col-sm-2">
									<label for="email">Plan Id</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
											<input type="text" name="commBean.planId" class="form-control form-username" placeholder="Plan Id"
											value="<s:property value='%{commBean.planId}'/>" readonly="readonly"
												/>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<label for="email">Plan Name</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
											<input type="text" name="commBean.planType" class="form-control form-username" placeholder="Plan Name"
											value="<s:property value='%{commBean.planType}'/>"	readonly="readonly"
												/>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<label for="email">Description</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
											<input type="text" name="commBean.planDescription" class="form-control form-username" placeholder="Description"
											value="<s:property value='%{commBean.planDescription}'/>" readonly="readonly"	
											/>
										</div>
									</div>
								</div>
								
								<div class="col-sm-2">
									<label for="email">Status</label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
											<s:select list="%{statusList}" headerKey="-1"
												headerValue="Status" id="aggregator"
												name="commBean.version" cssClass="form-username"
												requiredLabel="true"
												/>
										</div>
									</div>
								</div>
								

								<div class="form-group col-md-2 txtnew col-sm-2 col-xs-6 text-left">
									<label for="dateTo">&nbsp;</label> <br>
									<div align="center" id="wwctrl_submit">
										<input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Update Status">
									</div>
								</div>
							</div>
</form>
						
				
		
		</div>
	</div>
	</div>	

		
		
		
	
							
							
			<%
		List<CommPercViewMast> cpList=(List<CommPercViewMast>)request.getAttribute("cpList");

			%>				
							
			  <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Plan Id</u></th>						
						<th><u>Fixed Charge</u></th>
						<th><u>Range from</u></th>
						<th><u>Range to</u></th>
						<th><u>Description</u></th>
						<th><u>commission %</u></th>
						<th><u>Aggregator %</u></th>
						<th><u>Distributor %</u></th>
						<th><u>Agent %</u></th>
						<th><u>Sub-agent %</u></th>
					<!-- 	<th><u></u></th>	 -->
						
					</tr>
				</thead>
				<tbody>
				<%
				if(cpList!=null){
				for(int i=0;i<cpList.size();i++){
					CommPercViewMast cp=cpList.get(i);
				//List<CommPercRangeMaster> comRange=(List<CommPercRangeMaster>)cp.getCommPercRangeMaster();
				%>
				  	<tr>
		             <td><%=cp.getPlanid()%></td>		             
		             <td><%=d.format(cp.getFixedcharge())%></td>
		             <td><%=d.format(cp.getRangefrom())%></td>
		              <td><%=d.format(cp.getRangeto())%></td> 
		            <%--   <td><%=comRange.get(i).getFixedcharge()%></td>
		             <td><%=comRange.get(i).getRangefrom()%></td>
		              <td><%=comRange.get(i).getRangeto()%></td> --%>
		              <td><%if(cp.getDescription()==null||cp.getDescription().equalsIgnoreCase("NULL")){out.print(cp.getDescription());}else{out.print("-");}%></td>
		               <td><%=d.format(cp.getCommperc())%></td>
		                <td><%=d.format(cp.getAggregatorperc())%></td>
		                <td><%=d.format(cp.getDistributorperc())%></td>
		                 <td><%=d.format(cp.getAgentperc())%></td>
		                  <td><%=d.format(cp.getSubagentperc())%></td>
		                  
		             <%--      <td>
		                  <form action="EditShowCommByCommId.action">
		                  <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
		                  <input type="hidden" name="cpmaster.commid" value="<%=cp.getCommid() %>" readonly="readonly"/>
<input type="submit" class="btn btn-sm btn-block btn-success" value="Edit">	                  
		                  </form>
		                  </td> --%>
                  		  </tr>
                  <%
                  }
				}
				%>
			        </tbody>		</table>
		</div> 
		
		
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


