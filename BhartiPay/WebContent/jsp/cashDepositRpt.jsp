<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CashDepositMast"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.EscrowBean"%>
<%@page import="com.bhartipay.wallet.report.bean.SMSSendDetails"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>

<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 



<script type="text/javascript"> 
    $(document).ready(function() { 
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     ); 
	if("<s:property value='%{depositMast.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{depositMast.stDate}"/>' + ' - ' + '<s:property value="%{depositMast.endDate}"/>');	
	  }
   }); 
</script> 


<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 DecimalFormat d=new DecimalFormat("0.00");
 %>
 
<script type="text/javascript">


	
	     

 $(document).ready(function() {
	

    /* $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date()
   

    });
    if($('#dpStart').val().length != 0){
      $('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
     
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   
    




 function converDateToJsFormat(date) {
    
  var sDay = date.slice(0,2);
  var sMonth = date.slice(3,6);
  var yYear = date.slice(7,date.length)
 
  return sDay + " " +sMonth+ " " + yYear;
 } */
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Cash Deposit - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Cash Deposit - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Cash Deposit - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 

function dropInfo(elm){
	 var el = $(elm).val();
	 if(el != "-1"){
		 $("#" + el).fadeIn().siblings().hide();  
	 }else{
		 
		 $("#RECIEPT, #NEFT").hide();
	 }
	    

	}
function cashDepositAccept(form){
	
	var con = confirm("Are you sure you want to accept the request?")
	if(con){
		
		$(form).submit();
		
	}else{
		return false;
	}
}

function cashDepositReject(form){
	
	var con = confirm("Are you sure you want to reject the request?")
	if(con){		
		$(form).submit();
		
	}else{
		return false;
	}
}


function setMonth(month){
    var m = parseInt(month)
    if(month == 11){
     return 0;
    }else{
     console.log(month)
     return month + 2;
    }

    }
    function setYear(month,year){

    if(month == 11){
     return year;
    }else{
     console.log(month)
     return year + 1;
    }

    }
    function converDateToJsFormat(date) {
       
    var day = date.slice(0,2);
    var month = date.slice(3,6);
    var year = date.slice(7,date.length)

    return month + "/" + day + "/" + year;
    }
 </script>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

List<WalletToBankTxnMast>list=(List<WalletToBankTxnMast>)request.getAttribute("resultList");


%>
      
       <script>
       
       function acceptMoney(){
    	   
       }
       
       
       </script>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->
 <div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
     
            

 
 
 
<div class="box2">
	<div class="box-inner">
	
	
	
		<div class="box-header ">
			<h2>Cash Deposit</h2>
		
		</div>
		
	
		<div class="box-content ">
		
			<form action="CashDepositsRpt" method="post">

	    <div class="row"> 
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
											
								<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
								    <i class="fa fa-calendar"></i>&nbsp;
								    <span></span> <i class="fa fa-caret-down"></i>
								</div>				
										
										 <input type="hidden"
										value="<s:property value='%{depositMast.stDate}'/>"
										name="depositMast.stDate" id="from"
										class="form-control datepicker-here1 " placeholder="Start Date"
										data-language="en" required/>
										<input
													type="hidden"
													value="<s:property value='%{depositMast.endDate}'/>"
													name="depositMast.endDate" id="to"
													class="form-control datepicker-here2 " placeholder="End Date"
													data-language="en"  required>
								</div>
							<%-- 	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{depositMast.endDate}'/>"
													name="depositMast.endDate" id="to"
													class="form-control datepicker-here2 " placeholder="End Date"
													data-language="en"  required>
											</div> --%>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>User Type</u></th>
						<%if(user.getUsertype()==7){ %>
						<th><u>Distributor Name</u></th>
						<%}else{ %>
						<th><u>User Name</u></th>
						<%} %>
						<th><u>Id</u></th>
						<%if(user.getUsertype()==7){ %>
						<th><u>Distributor Id</u></th>
						<%}else{ %>
						<th><u>User Id</u></th>
						<%} %>
						
						<th><u>Type</u></th>
						<th><u>Amount</u></th>
						<th style="display: none"><u>Reference No.</u></th>
						<th style="display: none"><u>Bank Name</u></th>
						<th style="display: none"><u>Branch Name</u></th>						
						<th><u>Status</u></th>
						<th><u>Remark</u></th>
						
						<th><u>Checker Remark</u></th>	
						<th><u>Approvar Remark</u></th>
						<th style="display: none"><u>Deposit Date</u></th>
						
						<th><u>Request Date</u></th>
						<th><u>Approve Date</u></th>
						<th><u>Reciept</u></th>
						
									
					</tr>
				</thead>
				<tbody>

				<%
		
			List<CashDepositMast> eList=(List<CashDepositMast>)request.getAttribute("eList");
				if(eList!=null){
				
				
				for(int i=0; i<eList.size(); i++) {
					CashDepositMast tdo=eList.get(i);%>
		          		  <tr>
		          		 <td><%=tdo.getUserId().contains("OAGG")==true?"Aggregator":tdo.getUserId().contains("A")==true?"Agent":tdo.getUserId().contains("M")==true?"CME":tdo.getUserId().contains("D")==true?"Distributor":tdo.getUserId().contains("OXSD")==true?"Super-Distributor":"-"%></td> 
		          	    <td><%=tdo.getAgentname()%></td>
		          	    <td><%=tdo.getDepositId()%></td>
		          	    <td><%=tdo.getUserId()%></td>
		          	    <td><%=tdo.getType()%></td>
		              <td><%=d.format(tdo.getAmount())%></td>
		               <td style="text-align: center; display: none"><%=tdo.getNeftRefNo()%></td>
		               <td style="text-align: center; display: none"><%if(tdo.getBankName()==null){}else{%><%=tdo.getBankName()%><%}%></td>
		               <td style="text-align: center; display: none"><%if(tdo.getBranchName()==null){}else{%><%=tdo.getBranchName()%><%}%></td>
		                <td><%=tdo.getStatus()%></td>
		          	    <td><%=tdo.getRemark()%></td>
		          	    
		          	    <td><%=tdo.getRemarkChecker()%></td>
		          	    <td><%=tdo.getRemarkApprover()%></td>
		          	    <td style="text-align: center; display: none"><%if(tdo.getCashDepositDate()==null){}else{%><%=tdo.getCashDepositDate()%><%}%></td>
		          	    
		          	    <td><%if(tdo.getRequestdate()==null){}else{%><%=tdo.getRequestdate()%><%}%></td>
		            <td><%if(tdo.getApproveDate()==null){}else{%><%=tdo.getApproveDate()%><%}%></td>
		            <td class="img-cell"><img alt="userImg" src="<%if(tdo.getReciptPic()!=null&&tdo.getReciptPic().length()>200){%>data:image/gif;base64,<%} %><%=tdo.getReciptPic() %>" width="40px" height="40px"/></td>
			           
		            
		          
                  		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
 

  
 

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->



<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>


</body>
</html>
