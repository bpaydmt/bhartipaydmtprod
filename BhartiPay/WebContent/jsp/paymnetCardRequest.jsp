<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript"> <%@ include  file="../js/common.js"%></script>
<script type="text/javascript"> <%@ include  file="../js/user/validationFunctions.js"%></script>
<script type="text/javascript"> <%@ include  file="../js/user/create.js"%></script>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 
	function getSMSConfiguration(aggId)
	{  
		
		$.ajax({
	        url:"GetSMSConfiguration.action",
	        cache:0,
	        data:"sconfig.aggreatorid="+aggId,
	        success:function(result){
	      
	               document.getElementById('sconfig').innerHTML=result;
	               
	         }
	  });  
		return false;
	}
	
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Payment Card Request</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="PaymnetCardRequest" id="form_name" method="post">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>

											<%-- 	<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Aggregator</label>
															
													<s:select list="%{aggrigatorList}" headerKey="admin"
												headerValue="Common" id="aggregatorid"
												name="sconfig.aggreatorid" cssClass="form-username"
												requiredLabel="true"
												onchange="getSMSConfiguration(document.getElementById('aggregatorid').value);"
												 />
															
															
														</div>
													</div>
												</div>
 --%>
<div id="sconfig">
<%
User user=(User)session.getAttribute("User");
%>
													<div class="col-md-12">
														<div class="col-md-4">
															<div class="form-group">
															<input type="hidden" name="pcuser.uid" value="<%=user.getId()%>">
															<input type="hidden" name="pcuser.mobile_country_code" value="+91">

																<label for="apptxt">First name</label> <input
																	class="form-control" name="pcuser.first_name" type="text"
																	id="smsurl" placeholder="First name"
																	value="<s:property value='%{pcuser.first_name}'/>" />
															</div>
														</div>


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">Last name</label> <input
																	class="form-control" name="pcuser.last_name"
																	type="text" id="smsfeedid" placeholder="Last name"
																	value="<s:property value='%{pcuser.last_name}'/>" />

															</div>
														</div>


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">Preferred name</label> <input
																	class="form-control" name="pcuser.preferred_name"
																	type="text" id="smsusername" placeholder="Preferred name"
																	value="<s:property value='%{pcuser.preferred_name}'/>" />

															</div>
														</div>



													</div>




													<div class="col-md-12">


														<div class="col-md-4">
															<div class="form-group">
																<label for="apptxt">Email</label> <input
																	class="form-control" name="pcuser.email"
																	type="text" id="smssenderid" placeholder="Email"
																	value="<s:property value='%{pcuser.email}'/>" />

															</div>
														</div>

														<div class="col-md-4">
															<div class="form-group">
																	<label for="apptxt">Mobile</label> <input
																	class="form-control" name="pcuser.mobile"
																	type="text" id="smsurl" placeholder="SMS password"
																	value="<s:property value='%{pcuser.mobile}'/>" />
															</div>
														</div>

														<div class="col-md-4">
															<div class="form-group">
																	<label for="apptxt">Password</label> <input
																	class="form-control" name="pcuser.password"
																	type="password" id="smsurl" placeholder="SMS password"
																	value="<s:property value='%{pcuser.password'/>" />
															</div>
														</div>


													</div>


												</div>



												<div class="col-md-12">

			
											
													
													<div class="col-md-4">
										<div class="form-group">
										
										<input type="submit" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">
										

									
									</div>
									</div>


									</div>
									
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


