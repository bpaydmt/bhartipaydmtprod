<%@taglib prefix="s" uri="/struts-tags" %>

<s:select list="%{distributorList}" headerKey="-1"
headerValue="Select Distributor" id="distri"
name="reportBean.distId" cssClass="form-username"
requiredLabel="true"
onchange="getAgentByDistId(document.getElementById('distri').value);" />

