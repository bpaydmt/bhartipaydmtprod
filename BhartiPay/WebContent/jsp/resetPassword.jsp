<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<jsp:include page="theams.jsp"></jsp:include> 
<head>
   <jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script type="text/javascript">
    function resendOTP(){
	 
	 $.get("PasswordOtpsend",function(result){
		 console.log(result)
		  var timeLeft = 90
		var data = $.parseJSON(JSON.stringify(result))
		console.log(data)
	
		if(data.type == "Success"){
			$("#otpConfirmMsgErr").hide();
			$("#resendOtp").hide().html("Resend OTP");
			$("#otpConfirmMsg").show();
			$("#otpConfirmMsg").css("color","green");
					var resetOTPTimer = setInterval(function() {
					  timeLeft = timeLeft - 1;
					  
					  $("#timerSpan").html(timeLeft);
					 
					  if (timeLeft < 1) {
						  $("#resendOtp").show();
					      clearInterval(resetOTPTimer);
					      $("#otpConfirmMsg").hide();
					  }
					}, 1000);
		}else{
			$("#otpConfirmMsgErr").css("color","red").html(data.msg).show;
		}
		
		 }
	 )
	 
    } 
</script>

<style>
	.resend-otp-wrapper{
	margin-top:33px;
	float:left;
	}
</style>

</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
  
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
<div id="main" role="main"> 
  <div id="content">    
            

<div class=" row"> 
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header">
                <h2><i class="glyphicon "></i>Reset Password</h2>
<%
User user=(User)session.getAttribute("User");
%>
                
            </div>
            <div class="box-content">                   
		<form action="SaveResetPassword" id="form_name" method="post" name="PG">
		


            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
									
             					 <div class="col-md-12">
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
														<div class="form-group">
															<label for="apptxt">Current Password<font color="red"> *</font></label>
															<input	class="form-control" name="walletBean.userId" type="hidden" value="<%=user.getId() %>" />
															 <input	class="form-control mandatory" name="walletBean.oldpassword" type="password"
																id="currentpassword" maxlength="20"  placeholder="Current Password" <%-- value="<s:property value='%{walletBean.oldpassword}'/>" --%> />

														</div>
													</div>


													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
														<div class="form-group">
															<label for="apptxt">New Password<font color="red"> *</font></label>
															 <input	class="form-control mandatory password-vl" name="walletBean.password" type="password"
																id="newpassword" maxlength="20" placeholder="New Password" <%-- value="<s:property value='%{walletBean.name}'/>" --%> />

														</div>
													</div>

													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
														<div class="form-group">
															<label for="apptxt">Confirm New Password<font color="red"> *</font></label>
															 <input	class="form-control mandatory confirm-vl" name="walletBean.confirmPassword" type="password"
																id="confirmnewpassword" maxlength="20" placeholder="Confirm New Password"  />

														</div>
													</div>	
													
													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"> 
															 	
																<button  onclick="submitVaForm('#form_name',this)" style="margin-top: 20px;" class="btn btn-success btn-fill " >Submit</button>
																<input type="button" onclick="resetForm('#form_name');" style="margin-top: 20px;" class="btn btn-info btn-fill reset-form" value="Reset" />	 	
															 
													</div>												
											</div>
									
										
										<!-- <div class="col-md-12"> -->


										
									
									
									
									

									
									<!-- </div> -->
									
								
									</div>
									</form>
									

			                   									  
			                   									  
										
									

                
                

    
            </div>
        </div>
  
</div> 
</div>


        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->



<!-- library for cookie management -->
<script src="./newmis/js/jquery.cookie.js"></script>
<script src="./newmis/js/jquery.noty.js"></script>
<script src="./newmis/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<%-- <script src="./newmis/js/charisma.js"></script> --%>


</body>
</html>

