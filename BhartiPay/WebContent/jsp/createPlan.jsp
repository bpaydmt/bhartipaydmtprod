<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Create New Plan</h2>
<%
User user=(User)session.getAttribute("User");
%>
                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">                    
		<form action="SaveCreatePlan.action" id="form_name" method="post" name="PG">
		

<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
            
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					 <div class="col-md-12">
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Plan Name</label>
															 <input	class="form-control mandatory alphaNum-vl" name="commBean.planType" type="text"
																id="planType" maxlength="20"  placeholder="Plan Name"  value="<s:property value='%{commBean.planType}'/>" />

														</div>
													</div>
									</div>
									
									
									
<!-- 
	
	private String planType ; 
	private String planDescription ; 	
	private int version ;
	
							-->		 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Status</label>
															<select	 name="commBean.version" >
															<option value="1" selected="selected">ACTIVE</option>
															<option value="0">INACTIVE</option>	
															</select>
														</div>
													</div>
													
											</div>
									
									
									
												 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Plan Description</label>
															 <input	class="form-control mandatory" name="commBean.planDescription" type="text"
																id="planDescription" maxlength="200" placeholder="Plan Description"  
																value="<s:property value='%{commBean.planDescription}'/>"
																/>

														</div>
													</div>
													
										</div>
									
									
									
				<div class="col-md-12">


						
													
													
												
													
										


									</div>
									
									</div>
									
									</form>
							<div class="col-md-4">
										<div class="form-group">
										
										<button class="btn btn-info btn-fill submit-form"
											 style="margin-top: 20px;" onclick="submitVaForm('#form_name')">Submit
										</button>
											<button onclick="resetForm('#form_name')" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Reset
										</button>
									
									</div>
									</div>
                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


