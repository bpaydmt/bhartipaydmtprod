<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%
List<PassbookBean> pb=(List<PassbookBean>)request.getAttribute("listResult");
%>
 <div class="table-responsive">
						<table class="table table-bordered">
										<thead>
											<tr align="center" bgcolor="#B9B9DD" style="height: 12px"
												class="success">
												<th align="center">Txn Date</th>
												<th align="center">Txn ID</th>
												<th align="center">Description</th>
												<th align="center">Debit</th>
												<th align="center">Credit</th>
												<th align="center">Closing Balance</th>												
										</thead>


<%
				if(pb!=null){
				
				
				for(int i=0; i<pb.size(); i++) {
					PassbookBean tdo=pb.get(i);%>
	
										

											<tr class="tr" bgcolor="#f1f1f1" style="height: 12px">
												<td valign='middle' align="center" class="dat"><%=tdo.getTxndate()%></td>
												<td valign='middle' align="center" class="dat"><%=tdo.getTxnid()%></td>
												<td valign='middle' align="center" class="dat"><%=tdo.getTxndesc()%><%if(tdo.getPayeedtl()!=null){%>-<%=tdo.getPayeedtl()%><%} %></td>
											    <td valign='middle' align="center" class="dat"><%=tdo.getTxndebit()%></td>
												<td valign='middle' align="center" class="dat"><%=tdo.getTxncredit()%></td>
												<td valign='middle' align="center" class="dat"><%=tdo.getClosingBal()%></td>
        										

											</tr>
										  <%} }%>	
									</table>
								</div>

 
<%-- <head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js" type="text/javascript"></script>


<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.8.24.custom.css" rel="stylesheet"	type="text/css" media="all" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
 </script>
</head>
<body id="dt_example">
	<div id="container">
		<h1>Transaction Details</h1>
		<div id="xyz">
			<table id="example" class="display">
				<thead>
				
				<%
			/* 	GenericSqlReportData griddata=(GenericSqlReportData)session.getAttribute("GridData");
				LinkedList<String> heading=griddata.getMetaContent();
				LinkedList<Object> data=griddata.getTableData(); */
				%>
					<tr>

						<th><u>Txn ID</u></th>
						<th><u>Txn Date</u></th>
					<!-- 	<th><u>Wallet Id</u></th> -->
						<th><u>Credit Amount</u></th>
						<th><u>Debit Amount</u></th>
						<th><u>Pay Details</u></th>
						<th><u>Description</u></th>
						
						
					</tr>
				</thead>
				<tbody>
				<%
				if(pb!=null){
				
				
				for(int i=0; i<pb.size(); i++) {
					PassbookBean tdo=pb.get(i);%>
		          		  <tr>
		          	  
		             <td><%=tdo.getTxnid()%></td>
		             <td><%=tdo.getTxndate()%></td>
		             <td><%=tdo.getWalletid()%></td>
		             <td><%=tdo.getTxncredit()%></td>
		             <td><%=tdo.getTxndebit()%></td>
		             <td><%=tdo.getPayeedtl()%></td>
		             <td><%=tdo.getTxndesc()%></td>
                    
                  		  </tr>
			      <%} }%>			
		            </tbody>		</table>
		</div>
	</div>
</body>

  --%>