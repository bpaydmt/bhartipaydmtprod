
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.cme.vo.CMEAgentDetails"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="theams.jsp"></jsp:include>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<head>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>
<script src="js/handlebars-v4.0.10.js" type="text/javascript"></script>



<script type="text/javascript">

$(function(){
	$("#agent-mobile").blur(function(){
		setTimeout(function(){
			getAgentDetails()
		},100)
	})
})
	function getAgentDetails(){
		var agentMobile = $("#agent-mobile").val();
		
		if(agentMobile.length == 10 && $("#agent-mobile").hasClass("valid")){
			$.ajax({
				method:'post',
				url:'GetAgnDtlByUserId.action',
				data:'cmeBean.mobileNo='+agentMobile,
				success:function(data){
					console.log(data)
					var theTemplateScript = $("#agnentDetail").html();
					
					var theTemplate = Handlebars.compile(theTemplateScript);	
					var theCompiledHtml = theTemplate(data.agentDetail);
					$("#agentDetailsTable").html(theCompiledHtml)
				}
			})
		}else{
			alert(2)
		}
	}
function getAgentDetailsconfirm(){
	
		
		$("#agent-mobile-td").text($("#agent-mobile").val());
		$("#agent-id-td").text($("#agent-id").val())
		$("#agent-amout-td").text( $("#amount-inp").val())

	
}
	
</script>
	<script id="agnentDetail" type="text/x-handlebars-template">

</script>
</head>

<%-- <%
	User user = (User) session.getAttribute("User");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
DecimalFormat d=new DecimalFormat("0.00");
%> --%>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);

Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>



<body>

	<!-- topbar starts -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- contents starts -->



			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->


				<div class=" row">
					<div class="row" id="hidethis2">
						<div id="container">
							<div class="box2 col-md-12">
								<div class="box-inner">



									<div class="box-header well">
										<h2>Fund Transfer Status</h2>
									</div>


									<div class="box-content row">
										<font style="color: red;"> <s:actionerror />
										</font> <font style="color: blue;"> <s:actionmessage />
										</font>
										<%
										CMEAgentDetails agent=(CMEAgentDetails)request.getAttribute("agentDtl");
										if(agent!=null){
										%>
										
										<ul>
										<li><font style="size: 10px;font-weight: bold;">Funds have been successfully transfered to Agent ID :</font> <%=agent.getId()%></li>
										<li><font style="size: 10px;font-weight: bold;">Transaction Amount :</font>  <%=agent.getTransferAmount()%></li>
										<li><font style="size: 10px;font-weight: bold;">Closing Balance :</font> <%=agent.getClosingBal()%> </li>
										</ul>
										
										
										<%
										}
										%>
									</div>
								</div>
							</div>


						</div>

					</div>
				</div>

			</div>



			<!-- contents ends -->

		</div>
	</div>
	<!--/.fluid-container-->

	<jsp:include page="footer.jsp"></jsp:include>

	<!-- external javascript -->



	<!-- library for cookie management -->
	<script src="./newmis/js/jquery.cookie.js"></script>
	<script src="./newmis/js/jquery.noty.js"></script>
	<script src="./newmis/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->

	

	
</body>
</html>