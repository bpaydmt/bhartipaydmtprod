
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.CustomerProfileBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.UserWishListBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'New Agents - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'New Agents - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'New Agents - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'New Agents - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'New Agents - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    
} );
 </script>
 

 
  <script type="text/javascript">

  function getDistributerByAggId(aggId)
   {  
	
	$.ajax({
        url:"GetDistributerByAggId",
        cache:0,
        data:"reportBean.aggId="+aggId,
        success:function(result){
        
               document.getElementById('distributor').innerHTML=result;
               
         }
  });  
	return false;
}

function getAgentByDistId(distId)
{  
	
	$.ajax({
        url:"GetAgentByDistId",
        cache:0,
        data:"reportBean.distId="+distId,
        success:function(result){
        	
               document.getElementById('agent').innerHTML=result;
               
         }
  });  
	return false;
}

function acceptAgent(){
debugger	
var txt;
var utr = $("#utr").val();
var otp = $("#otp").val();
$("#utr-hidden").val(utr);
$("#otp-hidden").val(otp);
/* if(utr.length == 0){
	alert("You haven't provided UTR value");
	 return false;
}else{ */

var r = confirm("Do you want to accept User.");
if (r == true) {
   return true;
} else {
    return false;
    }
//}
}
function rejectAgent(){
	var dc = $("#decline-comment").val();
	if(dc.length == 0){		
		alert("Please provide the decline comment")
	    return false;

	}else{
		var txt;
		var r = confirm("Do you want to reject User.");
		if (r == true) {
		   return true;
		} else {
			return false;
		}

		
	}
}

</script>
</head>

<% 
User user = (User) session.getAttribute("User");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
 
 
<div class="box2 ">
	<div class="box-inner">
	
		<div class="box-header  ">
			<h2>New Users</h2>
		</div>
	</div>
	</div>
		
		
		
		

						
							
							
			<%
		List<WalletMastBean>agentList=(List<WalletMastBean>)request.getAttribute("agentList");
		
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
					    <th><u>Created By</u></th>
					    <th><u>SM Id</u></th>
					    <th><u>SO Id</u></th>
					    <th><u>User Type</u></th>
					 
						<th><u>User Id</u></th>						
						<th><u>Name</u></th>
						<th><u>Mobile Number</u></th>
						<!-- <th><u>Email</u></th>
						<th><u>Agent Type</u></th>
						<th><u>Distributer Id</u></th> -->
						<th><u>Date</u></th>
						<th><u>Action</u></th>
						
						
					</tr>
				</thead>
				<tbody>
				<%
				if(agentList!=null&&agentList.size()>0){	
				for(WalletMastBean wmb:agentList){
				%>
										<tr>
										    <td><%=wmb.getCreatedby()%></td>
										 	<td><%=wmb.getManagerId()%></td>
		          		                    <td><%=wmb.getSalesId()%></td>
										 	
										    <td><% if(wmb.getUsertype()==2){out.print("Retailer");}else if(wmb.getUsertype()==3){out.print("Distributor");}else if(wmb.getUsertype()==7){out.print("SuperDistributor");}else{out.print("");}  %></td>
		          	
											<td><%=wmb.getId()%></td>
											<td><%=wmb.getName()%></td>
											<td><%=wmb.getMobileno()%></td>
											<%-- 
											<td><%=wmb.getEmailid()%></td>
											<td><%=wmb.getAgentType() != null ? wmb.getAgentType() :"-"%></td>
											<td><%=wmb.getDistributerid()  != null ? wmb.getDistributerid():"-"%></td>
											 --%>
											<td><%=wmb.getCreationDate() == null ? "":wmb.getCreationDate()%></td>
											<td>
						<a href="javascipt:void(0)" data-toggle="modal" data-target="#newAgentBox"><span onclick="getAgentDetails('<%=wmb.getId()%>','<%=wmb.getAggreatorid()%>')" >View Details</span></a>
											</td>



											<%
                  }
				}                        
				%></tbody>		</table>
		</div> 
		
	
 
 
 

       

  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->

<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg" style="width:800px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Users Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){
debugger
	$.ajax({
		  type: "POST",
		  url: "NewBpayAgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>
</body>
</html>