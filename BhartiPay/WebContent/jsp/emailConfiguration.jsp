<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 
	function getMailConfiguration(aggId)
	{  
		
		$.ajax({
	        url:"GetMailConfiguration",
	        cache:0,
	        data:"mconfig.aggreatorid="+aggId,
	        success:function(result){
	       
	               document.getElementById('mconfig').innerHTML=result;
	               intialErrSpan();
	         }
	  });  
		return false;
	}
	
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>Email Configuration</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">      
                <font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
                	<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Aggregator</label>
															
													<s:select list="%{aggrigatorList}" headerKey="admin"
												headerValue="Common" id="aggregatorid"
												name="mconfig.aggreatorid" cssClass="form-username"
												requiredLabel="true"
												onchange="getMailConfiguration(document.getElementById('aggregatorid').value);checkSelectVal(this,'#hiddenConfig')"
												 />
															
															
														</div>
													</div>
												</div>
                              
		<form action="SaveMailConfiguration" id="email_config" method="post" enctype="multipart/form-data" name="PG">
		
<input type="hidden" name="mconfig.aggreatorid" id='hiddenConfig' value="<s:property value="%{mconfig.aggreatorid}"/>">

            
		
									<div class="row">
									

											

<div id="mconfig">

												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Host URL<font color="red"> *</font></label>
													
<input	class="form-control mandatory soft-url" name="mconfig.hosturl" type="text" id="hosturl" placeholder="Host URL" value="<s:property value='%{mconfig.hosturl}'/>" />

					</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">From Email<font color="red"> *</font></label>
<input	class="form-control mandatory emailid" name="mconfig.from_mail" type="text" id="from_mail" placeholder="From Email" value="<s:property value='%{mconfig.from_mail}'/>" />

														</div>
													</div>
													
													
												<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">From Name<font color="red"> *</font></label>
<input	class="form-control mandatory alphaNum-vl no-space" name="mconfig.fromname" type="text" id="fromname" placeholder="From Name" value="<s:property value='%{mconfig.fromname}'/>" />

														</div>
													</div>


								
									</div>
									
									
									
									
									 <div class="col-md-12">


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Username<font color="red"> *</font></label>
<input	class="form-control mandatory emailid" name="mconfig.username" type="text" id="username" placeholder="Username" value="<s:property value='%{mconfig.username}'/>" />

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Password<font color="red"> *</font></label>
<input	class="form-control mandatory" name="mconfig.password" type="password" id="password" placeholder="Password" value="<s:property value='%{mconfig.password}'/>" />

														</div>
													</div>
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Port<font color="red"> *</font></label>
<input	class="form-control onlyNum mandatory no-space" name="mconfig.port" type="text" id="port" placeholder="Port" value="<s:property value='%{mconfig.port}'/>" />
														</div>
													</div>
									</div>


</div>



												<div class="col-md-12">

			
											
													
													<div class="col-md-4">
										<div class="form-group">
										
										<button type="button" class="btn btn-info btn-fill submit-form"
											 style="margin-top: 20px;" onclick="submitVaForm('#email_config')">Submit
										</button>
										<button type="reset"  onclick="resetForm('#email_config')" style="margin: 16px 19px 0 0;
    position: relative;
    top: 3px;" class="btn btn-info btn-fill" value="Reset">Reset</button>

									
									</div>
									</div>


									</div>
									
									</div>
									
									</form>

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


